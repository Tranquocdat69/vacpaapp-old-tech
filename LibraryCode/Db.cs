﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Project_VACPA.LibraryCode
{
    public class Db
    {
        private SqlConnection _con = new SqlConnection();
        public Db(string connectionString)
        {
            _con = new SqlConnection(connectionString);
        }
        public void OpenConnection()
        {
            if (_con.State == ConnectionState.Broken || _con.State == ConnectionState.Closed)
                _con.Open();
        }

        public void CloseConnection()
        {
            if (_con.State == ConnectionState.Open || _con.State == ConnectionState.Connecting || _con.State == ConnectionState.Executing)
                _con.Close();
        }

        //public SqlDataReader GetData(string query)
        //{
        //    SqlCommand cmd = new SqlCommand(query, _con);
        //    SqlDataReader dr = cmd.ExecuteReader();
        //    dr.Get
        //    return dr;
        //}

        public List<Hashtable> GetListData(string query)
        {
            List<Hashtable> listData = new List<Hashtable>();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            try
            {
                cmd.CommandText = query;
                cmd.Connection = _con;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Hashtable ht = new Hashtable();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            string columnName = dr.GetName(i);
                            if (!ht.ContainsKey(columnName))
                                ht[columnName] = dr[columnName];
                        }
                        listData.Add(ht);
                    }
                }
                return listData;
            }
            catch
            {
                dr.Close();
                cmd.Cancel();
            }
            finally
            {
                dr.Close();
                cmd.Cancel();
            }
            return listData;
        }

        public List<Hashtable> GetListData(string procName, List<string> paramName, List<object> paramValue)
        {
            List<Hashtable> listData = new List<Hashtable>();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            try
            {
                cmd.CommandText = procName;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = _con;
                for (int i = 0; i < paramName.Count; i++)
                {
                    cmd.Parameters.AddWithValue(paramName[i], paramValue[i]);
                }
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Hashtable ht = new Hashtable();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            string columnName = dr.GetName(i);
                            if (!ht.ContainsKey(columnName))
                                ht[columnName] = dr[columnName];
                        }
                        listData.Add(ht);
                    }
                }
                return listData;
            }
            catch
            {
                dr.Close();
                cmd.Cancel();
            }
            finally
            {
                dr.Close();
                cmd.Cancel();
            }
            return listData;
        }

        public DataTable GetDataTable(string query)
        {
            DataTable dt = new DataTable();
            List<Hashtable> listData = new List<Hashtable>();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            try
            {
                cmd.CommandText = query;
                cmd.Connection = _con;
                dr = cmd.ExecuteReader();
                dt.Load(dr);
                return dt;
            }
            catch
            {
                dr.Close();
                cmd.Cancel();
            }
            finally
            {
                dr.Close();
                cmd.Cancel();
            }
            return dt;
        }

        public DataTable GetDataTable(string procName, List<string> paramName, List<object> paramValue)
        {
            DataTable dt = new DataTable();
            List<Hashtable> listData = new List<Hashtable>();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            try
            {
                cmd.CommandText = procName;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = _con;
                for (int i = 0; i < paramName.Count; i++)
                {
                    cmd.Parameters.AddWithValue(paramName[i], paramValue[i]);
                }
                dr = cmd.ExecuteReader();
                dt.Load(dr);
                return dt;
            }
            catch
            {
                dr.Close();
                cmd.Cancel();
            }
            finally
            {
                dr.Close();
                cmd.Cancel();
            }
            return dt;
        }

        public int ExecuteNonQuery(string query)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandText = query;
                cmd.Connection = _con;
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch
            {
                cmd.Cancel();
                return 0;
            }
            finally
            {
                cmd.Cancel();
            }
        }

        public int ExecuteNonQuery(string procName, List<string> paramName, List<object> paramValue)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandText = procName;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = _con;
                for (int i = 0; i < paramName.Count; i++)
                {
                    cmd.Parameters.AddWithValue(paramName[i], paramValue[i]);
                }
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch
            {
                cmd.Cancel();
                return 0;
            }
            finally
            {
                cmd.Cancel();
            }
        }
    }
}