﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Project_VACPA.LibraryCode
{
    public class ListName
    {
        public static string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["netTiersConnectionString"].ConnectionString;

        // Update by HUNGNM - 2014/11/18
        // List table name in Database
        #region DAO TAO - CNKT
        public const string Table_CNKTLopHoc = "tblCNKTLopHoc";
        public const string Table_CNKTLopHocChuyenDe = "tblCNKTLopHocChuyenDe";
        public const string Table_CNKTCoCauSoGioHocToiThieu = "tblCNKTCoCauSoGioHocToiThieu";
        public const string Table_CNKTLopHocBaoCao = "tblCNKTLopHocBaoCao";
        public const string Table_CNKTLopHocBaoCaoChiTiet = "tblCNKTLopHocBaoCaoChiTiet";
        #endregion

        #region KIEM SOAT CHAT LUONG

        public const string Table_KSCLDSBaoCaoTuKiemTra = "tblKSCLDSBaoCaoTuKiemTra";
        public const string Table_KSCLDSBaoCaoTuKiemTraChiTiet = "tblKSCLDSBaoCaoTuKiemTraChiTiet";
        public const string Table_KSCLDSKiemTraVuViec = "tblKSCLDSKiemTraVuViec";
        public const string Table_KSCLDSKiemTraVuViecChiTiet = "tblKSCLDSKiemTraVuViecChiTiet";
        public const string Table_KSCLDSKiemTraTrucTiep = "tblKSCLDSKiemTraTrucTiep";
        public const string Table_KSCLDSKiemTraTrucTiepChiTiet = "tblKSCLDSKiemTraTrucTiepChiTiet";
        public const string Table_KSCLDoanKiemTra = "tblKSCLDoanKiemTra";
        public const string Table_KSCLDoanKiemTraCongTy = "tblKSCLDoanKiemTraCongTy";
        public const string Table_KSCLDoanKiemTraThanhVien = "tblKSCLDoanKiemTraThanhVien";
        public const string Table_KSCLDoanKiemTraSoGioCNKT = "tblKSCLDoanKiemTraSoGioCNKT";

        public const string Table_KSCLHoSo = "tblKSCLHoSo";
        
        #endregion

        public const string Table_DMTrangThai = "tblDMTrangThai";
        public const string Table_DMGiangVien = "tblDMGiangVien";

        // Update by HUNGNM - 2014/11/18
        // List function name 
        public const string Func_CNKT_QuanLyLopHoc = "QuanLyLopHoc";
        public const string Func_CNKT_QuanLyChuyenDe = "QuanLyChuyenDe";
        public const string Func_CNKT_QuanLyDangKyHoc = "QuanLyDangKyHoc";
        public const string Func_CNKT_CapNhatSoGioCNKT = "CapNhatSoGioCNKT";
        public const string Func_CNKT_BaoCaoKetQuaToChucLopHoc = "BaoCaoKetQuaToChucLopHoc";
        public const string Func_CNKT_TongHop_SoGioCNKTCuaHocVien = "TongHopSoGioCNKT";
        public const string Func_CNKT_TongHop_SoGioCNKTConThieuCuaHocVien = "TongHopSoGioCNKTConThieu";

        public const string Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra = "DSCongTyKTNopBCTuKiemTra";
        public const string Func_KSCL_ChuanBi_DSCongTyKTTheoVuViec = "DSCongTyKTTheoVuViec";
        public const string Func_KSCL_ChuanBi_DSBaoCaoTuKiemTra = "DSBaoCaoTuKiemTra";
        public const string Func_KSCL_ChuanBi_DSCongTyKiemTraTrucTiep = "DSCongTyKiemTraTrucTiep";
        public const string Func_KSCL_ChuanBi_DSDoanKiemTra = "DsDoanKiemTra";
        public const string Func_KSCL_ChuanBi_GioDTThanhVienDoanKT = "GioDTThanhVienDoanKT";
        

        /// <summary>
        /// Update by NGUYEN MANH HUNG - 2014/11/20
        /// Danh sach cac gia tri can luu co dinh
        /// </summary> 
        public const string Type_HoiVienCaNhan = "1";
        public const string Type_KTV = "2";
        public const string Type_TroLyKTV = "3";
        public const string Type_KhongPhaiTroLyKTV = "4";

        public const string Type_LoaiChuyenDe_KeToanKiemToan = "1";
        public const string Type_LoaiChuyenDe_DaoDucNgheNghiep = "2";
        public const string Type_LoaiChuyenDe_ChuyenDeKhac = "3";

        public const string Type_LoaiHocVien_NguoiQuanTam = "0";
        public const string Type_LoaiHocVien_HoiVien = "1";
        public const string Type_LoaiHocVien_KiemToanVien = "2";

        public const string Type_LoaiCongTy_CtyKTChuaDuocCapID = "0";
        public const string Type_LoaiCongTy_HoiVienTapThe = "1";
        public const string Type_LoaiCongTy_CtyKTDaDuocCapID = "2";

        public const string Status_ChoTiepNhan = "1";
        public const string Status_TuChoi = "2";
        public const string Status_ChoDuyet = "3";
        public const string Status_KhongPheDuyet = "4";
        public const string Status_DaPheDuyet = "5";
        public const string Status_ThoaiDuyet = "6";


        /// <summary>
        /// Update by NGUYEN MANH HUNG - 2014/12/09
        /// 
        /// </summary>
        public const string Proc_procTongHopSoGioCNKT = "procTongHopSoGioCNKT";
        public const string Proc_procTongHopSoGioCNKTConThieu = "procTongHopSoGioCNKTConThieu";
        public const string Proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc = "proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc";

        public const string Proc_KSCL_SelectDanhSachCongTyKiemToanNopBaoCaoTuKiemTra = "proc_KSCL_SelectDanhSachCongTyKiemToanNopBaoCaoTuKiemTra";
        public const string Proc_KSCL_SearchDanhSachCongTyKiemToan = "proc_KSCL_SearchDanhSachCongTyKiemToan";
        public const string Proc_KSCL_SearchDanhSachCongTyKiemToan_1 = "proc_KSCL_SearchDanhSachCongTyKiemToan_1";
        public const string Proc_KSCL_SearchDanhSachCongTyKiemToan_YeuCauKiemTraVuViec = "proc_KSCL_SearchDanhSachCongTyKiemToan_YeuCauKiemTraVuViec";
        public const string Proc_KSCL_SearchDanhSachCongTyKiemToan_DSKiemTraTrucTiep = "proc_KSCL_SearchDanhSachCongTyKiemToan_DSKiemTraTrucTiep";
        public const string Proc_KSCL_SearchDanhSachCongTyKiemToan_DoanKiemTra = "proc_KSCL_SearchDanhSachCongTyKiemToan_DoanKiemTra";
        public const string Proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra = "proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra";
        public const string Proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec = "proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec";
        public const string Proc_KSCL_SearchDanhSachBaoCaoTuKiemTra = "proc_KSCL_SearchDanhSachBaoCaoTuKiemTra";
        public const string Proc_KSCL_SearchDanhSachCongTyKTKiemTraTrucTiep = "proc_KSCL_SearchDanhSachCongTyKTKiemTraTrucTiep";

        public const string Proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra_Mini1 = "proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra_Mini1"; // Dùng cho select dữ liệu hiển thị trong cửa sổ nhỏ chọn danh sách
        public const string Proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec_Mini1 = "proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec_Mini1"; // Dùng cho select dữ liệu hiển thị trong cửa sổ nhỏ chọn danh sách
        public const string Proc_KSCL_SearchDanhSachDoanKiemTra = "proc_KSCL_SearchDanhSachDoanKiemTra";
        public const string Proc_KSCL_DeleteDanhSachDoanKiemTra = "proc_KSCL_DeleteDanhSachDoanKiemTra";
        public const string Proc_KSCL_SearchDanhSachThanhVienDoanKiemTra_1 = "proc_KSCL_SearchDanhSachThanhVienDoanKiemTra_1";
        public const string Proc_KSCL_SelectDanhSachThanhVienTrongDoanKiemTra = "proc_KSCL_SelectDanhSachThanhVienTrongDoanKiemTra";
        public const string Proc_KSCL_SelectDsKiemTraTrucTiepDuDKDeLapDoanKiemTra = "proc_KSCL_SelectDsKiemTraTrucTiepDuDKDeLapDoanKiemTra";
        
        // Updated by NGUYEN MANH HUNG - 2014/12/17
        // Quy định các từ khóa để kiểm tra quyền
        #region Quyền
        public const string PERMISSION_Xem = "XEM|";
        public const string PERMISSION_Them = "THEM|";
        public const string PERMISSION_Sua = "SUA|";
        public const string PERMISSION_Xoa = "XOA|";
        public const string PERMISSION_Duyet = "DUYET|";
        public const string PERMISSION_TraLai = "TRALAI|";
        public const string PERMISSION_KetXuat = "KETXUAT|"; 
        #endregion

        public static CultureInfo CI_US = CultureInfo.CreateSpecificCulture("en-US");
    }
}
