﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using VACPA.Entities;
using VACPA.Data.SqlClient;

namespace Project_VACPA.LibraryCode
{
    public class Utility
    {
        /// <summary>
        /// Created by NGUYEN MANH HUNG - 2014/12/17
        /// Lấy ID trạng thái theo mã trạng thái
        /// </summary>
        /// <param name="status">Mã trạng thái</param>
        /// <param name="db">Biến khởi tạo cho class DB đã mở connection</param>
        /// <returns></returns>
        public string GetStatusID(string status, Db db)
        {
            string query = "SELECT TrangThaiID FROM " + ListName.Table_DMTrangThai + " WHERE MaTrangThai = " + status;
            List<Hashtable> listData = db.GetListData(query);
            if (listData.Count > 0)
                return listData[0]["TrangThaiID"].ToString();
            return "";
        }

        /// <summary>
        /// Created by NGUYEN MANH HUNG - 2014/12/17
        /// Lấy tên trạng thái theo ID trạng thái
        /// </summary>
        /// <param name="idTrangThai">ID trạng thái</param>
        /// <param name="db">Biến khởi tạo cho class DB đã mở connection</param>
        /// <returns></returns>
        public string GetStatusNameById(string idTrangThai, Db db)
        {
            string query = "SELECT TenTrangThai FROM " + ListName.Table_DMTrangThai + " WHERE TrangThaiID = " + idTrangThai;
            List<Hashtable> listData = db.GetListData(query);
            if (listData.Count > 0)
                return listData[0]["TenTrangThai"].ToString();
            return "";
        }

        /// <summary>
        /// Created by NGUYEN MANH HUNG - 2014/12/17
        /// Lấy mã trạng thái theo ID trạng thái
        /// </summary>
        /// <param name="idTrangThai">ID trạng thái</param>
        /// <param name="db">Biến khởi tạo cho class DB đã mở connection</param>
        /// <returns></returns>
        public string GetStatusCodeById(string idTrangThai, Db db)
        {
            string query = "SELECT MaTrangThai FROM " + ListName.Table_DMTrangThai + " WHERE TrangThaiID = " + idTrangThai;
            List<Hashtable> listData = db.GetListData(query);
            if (listData.Count > 0)
                return listData[0]["MaTrangThai"].ToString();
            return "";
        }

        /// <summary>
        /// Created by NGUYEN MANH HUNG - 2014/12/17
        /// Gửi email
        /// </summary>
        /// <returns></returns>
        public bool SendEmail(List<string> listMailReceive, string subject, string content)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailServerAddress"]);
                foreach (string mailAddress in listMailReceive)
                {
                    if(!string.IsNullOrEmpty(mailAddress))
                        mail.To.Add(new MailAddress(mailAddress));   
                }
                mail.Subject = subject;
                mail.Body = content;

                SmtpClient SmtpServer = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["HostMail"]);
                SmtpServer.Port = Library.Int32Convert(System.Configuration.ConfigurationManager.AppSettings["HostMailPort"]);
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["MailServerUsername"], System.Configuration.ConfigurationManager.AppSettings["MailServerPassword"]);
                SmtpServer.EnableSsl = false;

                SmtpServer.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }
            return false;
        }

        /// <summary>
        /// Created by NGUYEN MANH HUNG - 2014/12/17
        /// Gửi email
        /// </summary>
        /// <returns></returns>
        public bool SendEmail(List<string> listMailReceive, List<string> listSubject, List<string> listContent)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailServerAddress"]);
                for (int i = 0; i < listMailReceive.Count; i++)
                {
                    if (!string.IsNullOrEmpty(listMailReceive[i]))
                        mail.To.Add(new MailAddress(listMailReceive[i]));
                    mail.Subject = listSubject[i];
                    mail.Body = listContent[i];
                }

                SmtpClient SmtpServer = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["HostMail"]);
                SmtpServer.Port = Library.Int32Convert(System.Configuration.ConfigurationManager.AppSettings["HostMailPort"]);
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["MailServerUsername"], System.Configuration.ConfigurationManager.AppSettings["MailServerPassword"]);
                SmtpServer.EnableSsl = false;

                SmtpServer.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }
            return false;
        }
    }
}