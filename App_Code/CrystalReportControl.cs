﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer.Controllers;
using CrystalDecisions.ReportAppServer.ClientDoc;
using CrystalDecisions.Web;
using ReportDocument = CrystalDecisions.CrystalReports.Engine.ReportDocument;
using TextObject = CrystalDecisions.CrystalReports.Engine.TextObject;
using System.Drawing;
using Section = CrystalDecisions.ReportAppServer.ReportDefModel.Section;

/// <summary>
/// Summary description for CrystalReportControl
/// </summary>
public class CrystalReportControl
{
    public static string Section_ReportHeader = "ReportHeaderSection";
    public static string Section_Detail = "DetailSection";
    public static string Section_ReportFooter = "ReportFooterSection";
    public static string Section_PageHeader = "PageHeader";
    public static string Section_PageFooter = "PageFooter";
    public static string Section_GroupHeader = "GroupHeader";

    public static string HorizontalAlign_Default = "HorizontalAlign_Default";
    public static string HorizontalAlign_Left = "HorizontalAlign_Left";
    public static string HorizontalAlign_Center = "HorizontalAlign_Center";
    public static string HorizontalAlign_Right = "HorizontalAlign_Right";
    public static string HorizontalAlign_Justified = "HorizontalAlign_Justified";

    public static string TextFormat_Standard = "TextFormat_Standard";
    public static string TextFormat_RTF = "TextFormat_RTF";
    public static string TextFormat_HTML = "TextFormat_HTML";

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/03/25
    /// Tao TextObject
    /// </summary>
    /// <param name="rpt">ReportDocument</param>
    /// <param name="section">Loai Section chua TextObject</param>
    /// <param name="sectionIndex">So thu tu cua Section chua TextObject</param>
    /// <param name="controlName">Ten TextObject</param>
    /// <param name="text">Gia tri trong TextObject</param>
    /// <param name="left">Vi tri can trai</param>
    /// <param name="top">Vi tri can tren</param>
    /// <param name="width">Do rong</param>
    /// <param name="height">Chieu cao</param>
    /// <param name="fontFamily">Font chu</param>
    /// <param name="fontSize">Kich co Font chu</param>
    /// <param name="fontStyle">Kieu font chu</param>
    /// <param name="enableCanGrow">Cho phep tu tang kich thuoc</param>
    /// <param name="enableKeepTogether">Cho phep bat buoc du lieu nam trong 1 trang</param>
    /// <param name="horizontalAlign">Can trai, phai, giua</param>
    public static void DrawTextObjectCrystalReport(ReportDocument rpt, string section, int sectionIndex, string controlName, string text, int? left, int? top, int? width, int? height, string fontFamily, float? fontSize, FontStyle? fontStyle, bool enableCanGrow, bool enableKeepTogether, string horizontalAlign)
    {
        ISCDReportClientDocument boReportClientDocument = rpt.ReportClientDocument;
        Section boSection = null;
        if (section == Section_ReportHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.ReportHeaderArea.Sections[sectionIndex];
        if (section == Section_Detail)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.DetailArea.Sections[sectionIndex];
        if (section == Section_ReportFooter)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.ReportFooterArea.Sections[sectionIndex];
        if (section == Section_PageHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.PageHeaderArea.Sections[sectionIndex];
        if (section == Section_PageFooter)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.PageFooterArea.Sections[sectionIndex];
        if (section == Section_GroupHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.GroupHeaderArea[sectionIndex].Sections[0];
        //CrystalDecisions.ReportAppServer.ReportDefModel.FieldObject boFieldObject = new CrystalDecisions.ReportAppServer.ReportDefModel.FieldObject();
        CrystalDecisions.ReportAppServer.ReportDefModel.TextObject txtObject = new CrystalDecisions.ReportAppServer.ReportDefModel.TextObject();

        txtObject.Name = controlName;
        if(left != null)
            txtObject.Left = left.Value;
        if(top != null)
            txtObject.Top = top.Value;
        if(width != null)
            txtObject.Width = width.Value;
        if(height != null)
            txtObject.Height = height.Value;
        
        txtObject.Format.EnableCanGrow = enableCanGrow;
        txtObject.Format.EnableKeepTogether = enableKeepTogether;
        boReportClientDocument.ReportDefController.ReportObjectController.Add(txtObject, boSection, -1);
        ((TextObject)rpt.ReportDefinition.ReportObjects[controlName]).Text = text;
        if (horizontalAlign == HorizontalAlign_Left)
            ((TextObject)rpt.ReportDefinition.ReportObjects[controlName]).ObjectFormat.HorizontalAlignment = Alignment.LeftAlign;
        else if (horizontalAlign == HorizontalAlign_Default)
            ((TextObject)rpt.ReportDefinition.ReportObjects[controlName]).ObjectFormat.HorizontalAlignment = Alignment.DefaultAlign;
        else if (horizontalAlign == HorizontalAlign_Right)
            ((TextObject)rpt.ReportDefinition.ReportObjects[controlName]).ObjectFormat.HorizontalAlignment = Alignment.RightAlign;
        else if (horizontalAlign == HorizontalAlign_Center)
            ((TextObject)rpt.ReportDefinition.ReportObjects[controlName]).ObjectFormat.HorizontalAlignment = Alignment.HorizontalCenterAlign;
        else if (horizontalAlign == HorizontalAlign_Justified)
            ((TextObject)rpt.ReportDefinition.ReportObjects[controlName]).ObjectFormat.HorizontalAlignment = Alignment.Justified;
        fontFamily = string.IsNullOrEmpty(fontFamily) ? "Times New Roman" : fontFamily;
        fontSize = fontSize ?? 10;
        System.Drawing.Font font = null;
        if(fontStyle != null)
            font = new System.Drawing.Font(fontFamily, fontSize.Value, fontStyle.Value);
        else
            font = new System.Drawing.Font(fontFamily, fontSize.Value);
        ((TextObject)rpt.ReportDefinition.ReportObjects[controlName]).ApplyFont(font);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/03/25
    /// Tao FieldObject
    /// </summary>
    /// <param name="rpt">ReportDocument</param>
    /// <param name="section">Loai Section chua TextObject</param>
    /// <param name="sectionIndex">So thu tu cua Section chua TextObject</param>
    /// <param name="fieldValueType">Kieu du lieu cua FieldObject</param>
    /// <param name="dataSourceName">Field gia tri can lay</param>
    /// <param name="controlName">Ten FieldObject</param>
    /// <param name="left">Vi tri can trai</param>
    /// <param name="top">Vi tri can tren</param>
    /// <param name="width">Do rong</param>
    /// <param name="height">Chieu cao</param>
    /// <param name="fontFamily">Font chu</param>
    /// <param name="fontSize">Kich co font chu</param>
    /// <param name="fontStyle">Kieu font chu</param>
    /// <param name="enableCanGrow">Cho phep tu tang kich thuoc</param>
    /// <param name="enableKeepTogether">Cho phep bat buoc du lieu nam trong 1 trang</param>
    /// <param name="horizontalAlign">Can trai, phai, giua</param>
    /// <param name="textFormat">Kieu format gia tri</param>
    public static void DrawFieldObjectCrystalReport(ReportDocument rpt, string section, int sectionIndex, string fieldValueType, string dataSourceName, string controlName, int? left, int? top, int? width, int? height, string fontFamily, float? fontSize, FontStyle? fontStyle, bool enableCanGrow, bool enableKeepTogether, string horizontalAlign, string textFormat)
    {
        ISCDReportClientDocument boReportClientDocument = rpt.ReportClientDocument;
        Section boSection = null;
        if (section == Section_ReportHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.ReportHeaderArea.Sections[sectionIndex];
        if (section == Section_Detail)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.DetailArea.Sections[sectionIndex];
        if (section == Section_ReportFooter)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.ReportFooterArea.Sections[sectionIndex];
        if (section == Section_PageHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.PageHeaderArea.Sections[sectionIndex];
        if (section == Section_PageFooter)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.PageFooterArea.Sections[sectionIndex];
        if (section == Section_GroupHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.GroupHeaderArea[sectionIndex].Sections[0];
        CrystalDecisions.ReportAppServer.ReportDefModel.FieldObject fieldObject = new CrystalDecisions.ReportAppServer.ReportDefModel.FieldObject();

        fieldObject.DataSourceName = "{"+dataSourceName+"}";
        fieldObject.FieldValueType = CrystalDecisions.ReportAppServer.DataDefModel.CrFieldValueTypeEnum.crFieldValueTypeStringField;
        fieldObject.Name = controlName;
        if (left != null)
            fieldObject.Left = left.Value;
        if (top != null)
            fieldObject.Top = top.Value;
        if (width != null)
            fieldObject.Width = width.Value;
        if (height != null)
            fieldObject.Height = height.Value;
        fieldObject.FontColor = new CrystalDecisions.ReportAppServer.ReportDefModel.FontColor();
        fontFamily = string.IsNullOrEmpty(fontFamily) ? "Times New Roman" : fontFamily;
        fontSize = fontSize ?? 10;
        fieldObject.FontColor.Font.Name = fontFamily;
        fieldObject.FontColor.Font.Size = Library.DecimalConvert(fontSize.Value);
        if (string.IsNullOrEmpty(horizontalAlign) || horizontalAlign == HorizontalAlign_Default)
            fieldObject.Format.HorizontalAlignment = CrystalDecisions.ReportAppServer.ReportDefModel.CrAlignmentEnum.crAlignmentDefault;
        else if(horizontalAlign == HorizontalAlign_Left)
            fieldObject.Format.HorizontalAlignment = CrystalDecisions.ReportAppServer.ReportDefModel.CrAlignmentEnum.crAlignmentLeft;
        else if (horizontalAlign == HorizontalAlign_Right)
            fieldObject.Format.HorizontalAlignment = CrystalDecisions.ReportAppServer.ReportDefModel.CrAlignmentEnum.crAlignmentRight;
        else if (horizontalAlign == HorizontalAlign_Center)
            fieldObject.Format.HorizontalAlignment = CrystalDecisions.ReportAppServer.ReportDefModel.CrAlignmentEnum.crAlignmentHorizontalCenter;
        else if (horizontalAlign == HorizontalAlign_Justified)
            fieldObject.Format.HorizontalAlignment = CrystalDecisions.ReportAppServer.ReportDefModel.CrAlignmentEnum.crAlignmentJustified;
        fieldObject.Format.EnableCanGrow = enableCanGrow;
        fieldObject.Format.EnableKeepTogether = enableKeepTogether;
        if(string.IsNullOrEmpty(textFormat) || textFormat == TextFormat_Standard)
            fieldObject.FieldFormat.StringFormat.TextFormat = CrystalDecisions.ReportAppServer.ReportDefModel.CrTextFormatEnum.crTextFormatStandardText;
        else if (textFormat == TextFormat_RTF)
            fieldObject.FieldFormat.StringFormat.TextFormat = CrystalDecisions.ReportAppServer.ReportDefModel.CrTextFormatEnum.crTextFormatRTFText;
        else if (textFormat == TextFormat_HTML)
            fieldObject.FieldFormat.StringFormat.TextFormat = CrystalDecisions.ReportAppServer.ReportDefModel.CrTextFormatEnum.crTextFormatHTMLText;
        
        boReportClientDocument.ReportDefController.ReportObjectController.Add(fieldObject, boSection, -1);
    }

    public static void DrawLineCrystalReport(ReportDocument rpt, string section, int sectionIndex, string controlName, int? left, int? right, int? top, int? bottom, bool enableExtendToBottomOfSection)
    {
        ISCDReportClientDocument boReportClientDocument = rpt.ReportClientDocument;
        Section boSection = null;
        if (section == Section_ReportHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.ReportHeaderArea.Sections[sectionIndex];
        if (section == Section_Detail)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.DetailArea.Sections[sectionIndex];
        if (section == Section_ReportFooter)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.ReportFooterArea.Sections[sectionIndex];
        if (section == Section_PageHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.PageHeaderArea.Sections[sectionIndex];
        if (section == Section_PageFooter)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.PageFooterArea.Sections[sectionIndex];
        if (section == Section_GroupHeader)
            boSection = boReportClientDocument.ReportDefController.ReportDefinition.GroupHeaderArea[sectionIndex].Sections[0];
        CrystalDecisions.ReportAppServer.ReportDefModel.LineObject lineObject = new CrystalDecisions.ReportAppServer.ReportDefModel.LineObject();

        lineObject.Name = controlName;
        if (left != null)
            lineObject.Left = left.Value;
        if (right != null)
            lineObject.Right = right.Value;
        if (top != null)
            lineObject.Top = top.Value;
        if (bottom != null)
            lineObject.Bottom = bottom.Value;
        lineObject.LineStyle = CrLineStyleEnum.crLineStyleSingle;
        lineObject.SectionName = boSection.Name;
        lineObject.EndSectionName = boSection.Name;
        lineObject.EnableExtendToBottomOfSection = enableExtendToBottomOfSection;
        boReportClientDocument.ReportDefController.ReportObjectController.Add(lineObject, boSection, -1);
    }

    protected static Queue reportQueue = new Queue();

    protected static ReportDocument CreateReportDocument(Type reportDocument)
    {
        object report = Activator.CreateInstance(reportDocument);
        reportQueue.Enqueue(report);
        return (ReportDocument)report;
    }

    public static ReportDocument GetReportDocument(Type reportDocument)
    {
        if (reportQueue.Count > 50)
        {
            for (int i = 0; i < reportQueue.Count; i++)
            {
                ReportDocument rpt = (ReportDocument) reportQueue.Dequeue();
                rpt.Close();
                rpt.Dispose();
            }
        }
        return CreateReportDocument(reportDocument);
    }

    public static void ResetReportToNull(CrystalReportViewer crv)
    {
        crv.ReportSource = null;
        crv.RefreshReport();
        GC.Collect();
    }
}