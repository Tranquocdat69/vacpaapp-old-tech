﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
/// <summary>
/// Summary description for Converter
/// </summary>
public class Converter
{
    //private static char[] tcvnchars = {


    //    'µ', '¸', '¶', '·', '¹', 


    //    '¨', '»', '¾', '¼', '½', 'Æ', 


    //    '©', 'Ç', 'Ê', 'È', 'É', 'Ë', 


    //    '®', 'Ì', 'Ð', 'Î', 'Ï', 'Ñ', 


    //    'ª', 'Ò', 'Õ', 'Ó', 'Ô', 'Ö', 


    //    '×', 'Ý', 'Ø', 'Ü', 'Þ', 


    //    'ß', 'ã', 'á', 'â', 'ä', 


    //    '«', 'å', 'è', 'æ', 'ç', 'é', 


    //    '¬', 'ê', 'í', 'ë', 'ì', 'î', 


    //    'ï', 'ó', 'ñ', 'ò', 'ô', 


    //    '*', 'õ', 'ø', 'ö', '÷', 'ù', 


    //    'ú', 'ý', 'û', 'ü', 'þ', 


    //    '¡', '¢', '§', '£', '¤', '¥', '¦'


    //};
    //private static char[] unichars = {


    //    'à', 'á', 'ả', 'ã', 'ạ', 


    //    'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ', 


    //    'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ', 


    //    'đ', 'è', 'é', 'ẻ', 'ẽ', 'ẹ', 


    //    'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ', 


    //    'ì', 'í', 'ỉ', 'ĩ', 'ị', 


    //    'ò', 'ó', 'ỏ', 'õ', 'ọ', 


    //    'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ', 


    //    'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ', 


    //    'ù', 'ú', 'ủ', 'ũ', 'ụ', 


    //    'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự', 


    //    'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ', 

    //    'À', 'Á', 'Ả', 'Ã', 'Ạ',
        
    //    'Ă', 'Ằ', 'Ắ', 'Ẳ', 'Ẵ', 'Ặ',


    //    'Â', 'Ầ', 'Ấ', 'Ẩ', 'Ẫ', 'Ậ', 


    //    'Đ', 'È', 'É', 'Ẻ', 'Ẽ', 'Ẹ', 


    //    'Ê', 'Ề', 'Ế', 'Ể', 'Ễ', 'Ệ', 
        
    //    'Ì', 'Í', 'Ỉ', 'Ĩ', 'Ị', 

    //    'Ò', 'Ó', 'Ỏ', 'Õ', 'Ọ', 


    //    'Ô', 'Ồ', 'Ố', 'Ổ', 'Ỗ', 'Ộ', 


    //    'Ơ', 'Ờ', 'Ớ', 'Ở', 'Ỡ', 'Ợ', 


    //    'Ù', 'Ú', 'Ủ', 'Ũ', 'Ụ', 


    //    'Ư', 'Ừ', 'Ứ', 'Ử', 'Ữ', 'Ự', 


    //    'Ỳ', 'Ý', 'Ỷ', 'Ỹ', 'Ỵ',


    //    'Ă', 'Â', 'Đ', 'Ê', 'Ô', 'Ơ', 'Ư'


    //};






    //private static char[] convertTable;






    //static Converter()
    //{

    //    try
    //    {


    //        convertTable = new char[9999];


    //        for (int i = 0; i < 9999; i++)


    //            convertTable[i] = (char)i;


    //        for (int i = 0; i < unichars.Length; i++)


    //            convertTable[unichars[i]] = tcvnchars[i];
    //    }
    //    catch (Exception)
    //    {

    //        throw;
    //    }

    //}






    //public static string UnicodeToTCVN3(string value)
    //{


    //    char[] chars = value.ToCharArray();


    //    for (int i = 0; i < chars.Length; i++)


    //        if (chars[i] < (char)256)


    //            chars[i] = convertTable[chars[i]];


    //    return new string(chars);


    //}

    public static string ConvertToTCVN3(string value)
    {
        string text = "";
        int i = 0;
        while (i < value.Length)
        {
            char value2 = char.Parse(value.Substring(i, 1));
            short num = System.Convert.ToInt16(value2);
            if (num <= 259)
            {
                if (num <= 202)
                {
                    if (num != 194)
                    {
                        switch (num)
                        {
                            case 200: text += "Ì"; break;
                            case 201: text += "Ð"; break;
                            case 202: text += "£"; break;
                            
                            case 192: text += "µ"; break;
                            case 193: text += "¸"; break;
                            case 195: text += "·"; break;
                            default: goto IL_7AF;
                        }
                    }
                    else text += "¢";
                }
                else
                {
                    if (num != 212)
                    {
                        switch (num)
                        {
                            case 204: text += "×"; break;
                            case 205: text += "Ý"; break;
                            case 210: text += "ß"; break;
                            case 211: text += "ã"; break;
                            //case 212: text += "¤"; break;
                            case 213: text += "â"; break;
                            case 217: text += "ï"; break;
                            case 218: text += "ó"; break;
                            case 221: text += "Ý"; break;
                            case 258: text += "¡"; break;

                            case 224: text += "µ"; break;
                            case 225: text += "¸"; break;
                            case 226: text += "©"; break;
                            case 227: text += "·"; break;
                            case 228:
                            case 229:
                            case 230:
                            case 231:
                            case 235:
                            case 238:
                            case 239:
                            case 240:
                            case 241:
                            case 246:
                            case 247:
                            case 248:
                            case 251:
                            case 252: goto IL_7AF;
                            case 232: text += "Ì"; break;
                            case 233: text += "Ð"; break;
                            case 234: text += "ª"; break;
                            //case 236: text += "ỡ"; break;
                            case 236: text += "×"; break;
                            case 237: text += "Ý"; break;
                            case 242: text += "ß"; break;
                            case 243: text += "ã"; break;
                            case 244: text += "«"; break;
                            case 245: text += "â"; break;
                            case 249: text += "ï"; break;
                            case 250: text += "ó"; break;
                            case 253: text += "ý"; break;
                            default:
                                switch (num)
                                {
                                    case 258: text += "¡"; break;
                                    case 259: text += "¨"; break;
                                    default: goto IL_7AF;
                                }
                                break;
                        }
                    }
                    else text += "¤";
                }
            }
            else
            {
                if (num <= 361)
                {
                    switch (num)
                    {
                        case 272: text += "§"; break;
                        case 273: text += "®"; break;
                        case 296: text += "Ü"; break;
                        case 360: text += "ò"; break;
                        default:
                            if (num != 297)
                            {
                                if (num != 361) goto IL_7AF;
                                text += "ò";
                            }
                            else text += "Ü";
                            break;
                    }
                }
                else
                {
                    switch (num)
                    {
                        case 416: text += "¥"; break;
                        case 417: text += "¬"; break;
                        case 431: text += "¦"; break;
                        case 432: text += "­"; break;
                        default:
                            if (num != 431)
                            {
                                switch (num)
                                {
                                    case 7840: text += "¹"; break;
                                    case 7841: text += "¹"; break;
                                    case 7842: text += "¶"; break;
                                    case 7844: text += "Ê"; break;
                                    case 7846: text += "Ç"; break;
                                    case 7848: text += "È"; break;
                                    case 7850: text += "É"; break;
                                    case 7852: text += "Ë"; break;
                                    case 7854: text += "¾"; break;
                                    case 7856: text += "»"; break;
                                    case 7858: text += "¼"; break;
                                    case 7860: text += "½"; break;
                                    case 7862: text += "Æ"; break;
                                    case 7864: text += "Ñ"; break;
                                    case 7866: text += "Î"; break;
                                    case 7868: text += "Ï"; break;
                                    case 7870: text += "Õ"; break;
                                    case 7872: text += "Ò"; break;
                                    case 7874: text += "Ó"; break;
                                    case 7876: text += "Ô"; break;
                                    case 7878: text += "Ö"; break;
                                    case 7880: text += "Ø"; break;
                                    case 7882: text += "Þ"; break;
                                    case 7884: text += "ä"; break;
                                    case 7886: text += "á"; break;
                                    case 7888: text += "è"; break;
                                    case 7890: text += "å"; break;
                                    case 7892: text += "æ"; break;
                                    case 7894: text += "ç"; break;
                                    case 7896: text += "é"; break;
                                    case 7898: text += "í"; break;
                                    case 7900: text += "ê"; break;
                                    case 7902: text += "ë"; break;
                                    case 7904: text += "ì"; break;
                                    case 7906: text += "î"; break;
                                    case 7908: text += "ô"; break;
                                    case 7910: text += "ñ"; break;
                                    case 7912: text += "ø"; break;
                                    case 7914: text += "õ"; break;
                                    case 7916: text += "ö"; break;
                                    case 7918: text += "÷"; break;
                                    case 7920: text += "ù"; break;
                                    case 7922: text += "ú"; break;
                                    case 7924: text += "þ"; break;
                                    case 7926: text += "û"; break;
                                    case 7928: text += "ü"; break;
                                    case 7843: text += "¶"; break;
                                    case 7845: text += "Ê"; break;
                                    case 7847: text += "Ç"; break;
                                    case 7849: text += "È"; break;
                                    case 7851: text += "É"; break;
                                    case 7853: text += "Ë"; break;
                                    case 7855: text += "¾"; break;
                                    case 7857: text += "»"; break;
                                    case 7859: text += "¼"; break;
                                    case 7861: text += "½"; break;
                                    case 7863: text += "Æ"; break;
                                    case 7865: text += "Ñ"; break;
                                    case 7867: text += "Î"; break;
                                    case 7869: text += "Ï"; break;
                                    case 7871: text += "Õ"; break;
                                    case 7873: text += "Ò"; break;
                                    case 7875: text += "Ó"; break;
                                    case 7877: text += "Ô"; break;
                                    case 7879: text += "Ö"; break;
                                    case 7881: text += "Ø"; break;
                                    case 7883: text += "Þ"; break;
                                    case 7885: text += "ä"; break;
                                    case 7887: text += "á"; break;
                                    case 7889: text += "è"; break;
                                    case 7891: text += "å"; break;
                                    case 7893: text += "æ"; break;
                                    case 7895: text += "ç"; break;
                                    case 7897: text += "é"; break;
                                    case 7899: text += "í"; break;
                                    case 7901: text += "ê"; break;
                                    case 7903: text += "ë"; break;
                                    case 7905: text += "ì"; break;
                                    case 7907: text += "î"; break;
                                    case 7909: text += "ô"; break;
                                    case 7911: text += "ñ"; break;
                                    case 7913: text += "ø"; break;
                                    case 7915: text += "õ"; break;
                                    case 7917: text += "ö"; break;
                                    case 7919: text += "÷"; break;
                                    case 7921: text += "ù"; break;
                                    case 7923: text += "ú"; break;
                                    case 7925: text += "þ"; break;
                                    case 7927: text += "û"; break;
                                    case 7929: text += "ü"; break;
                                    default: goto IL_7AF;
                                }
                            }
                            else text += "¦";
                            break;
                    }
                }
            }
        IL_7BF:
            i++;
            continue;
        IL_7AF:
            text += value2.ToString();
            goto IL_7BF;
        }
        return text;
    }
}