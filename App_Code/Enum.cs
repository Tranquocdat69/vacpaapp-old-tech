﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.ComponentModel;
using System.Reflection;
/// <summary>
/// Summary description for Enum
/// </summary>
public class EnumVACPA
{

    public enum TinhTrang : int
    {
        ChoTiepNhan = 1,

        TuChoiDon = 2,

        ChoDuyet = 3,

        KhongPheDuyet = 4,

        DaPheDuyet = 5,

        ThoaiDuyet = 6
    }

    public enum DoiTuongNopPhi : int
    {
        HoiVienCaNhan = 0,

        HoiVienTapThe = 1,
    }

    public enum DoiTuongNopPhiCNKTTapThe : int
    {
        HoiVienTapThe = 1,

        CongTyKiemToan = 2
    }

    public enum LoaiHoiVien : int
    {
        NguoiQuanTam = 0,

        HoiVienCaNhan = 1,
        
        KiemToanVien = 2,

        CongTyKiemToan = 3,

        HoiVienTapThe = 4
    }

    public enum Mode : int
    {
        ThemMoi = 1,

        Xem = 2,

        Sua = 3,
    }

    public enum LoaiPhi : int
    {
        PhiHoiVien = 1,

        PhiDangLogo = 2,

        PhiKhac = 3,

        PhiCapNhatKienThuc = 4,

        PhiKiemSoatChatLuong = 5,
        
        PhiCapNhatKienThucTapThe = 6,
    }

    public enum DoiTuongApDung : int
    {
        HvcnLamViecTaiCtkt = 1,

        HvcnKhongLamViecTaiCtkt = 2,

        HvttDuDieuKienKiTLinhVucChungKhoan = 3,

        HvttKhongDuDieuKienKiTLinhVucChungKhoan = 4
    }

    public enum LoaiHoiVienHoiVienCaNhanTapThe : int
    {
        CongTyKiemToanChuaDuocCapID = 0,

        HoiVienTapThe = 1,

        CongTyKiemToanDaDuocCapIDDangNhap = 2
    }

    public enum TenLoaiPhi
    {
        [DescriptionAttribute("Phí Hội viên")]
        PhiHoiVien,
        [DescriptionAttribute("Phí đăng logo")]
        PhiDangLogo,
        [DescriptionAttribute("Phí khác")]
        PhiKhac,
        [DescriptionAttribute("Phí cập nhật kiến thức")]
        PhiCapNhatKienThuc,
        [DescriptionAttribute("Phí kiểm soát chất lượng")]
        PhiKiemSoatChatLuong,
        [DescriptionAttribute("Phí CNKT (công ty nộp)")]
        PhiCapNhatKienThucTapThe,
    }

    public enum TenTinhTrang
    {
        [DescriptionAttribute("Chờ tiếp nhận")]
        ChoTiepNhan,
        [DescriptionAttribute("Từ chối đơn")]
        TuChoiDon,
        [DescriptionAttribute("Chờ duyệt")]
        ChoDuyet,
        [DescriptionAttribute("Không phê duyệt")]
        KhongPheDuyet,
        [DescriptionAttribute("Đã phê duyệt")]
        PheDuyet,
        [DescriptionAttribute("Thoái duyệt")]
        ThoaiDuyet
    }

    public static string stringValueOf(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());
        DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
        if (attributes.Length > 0)
        {
            return attributes[0].Description;
        }
        else
        {
            return value.ToString();
        }
    }

}