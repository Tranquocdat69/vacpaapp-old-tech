﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsThongTinHoiVien
/// </summary>
/// 

public class objThongTinHoiVien_ChiTiet
{
    public string HoTen { set; get; }
    public string GioiTinh { set; get; }
    public string NgaySinh { set; get; }
    public string QueQuan { set; get; }
    public string ChoOHienNay { set; get; }
    public string BangCap { set; get; }
    public string ChuyenNganh { set; get; }
    public string Nam_Bang { set; get; }
    public string HocHam { set; get; }
    public string Nam_HocHam { set; get; }
    public string HocVi { set; get; }
    public string Nam_HocVi { set; get; }
    public string CMT { set; get; }
    public string NgayCap { set; get; }
    public string NoiCap { set; get; }
    public string SoTK { set; get; }
    public string TaiNganHang { set; get; }
    public string SoChungChi { set; get; }
    public string NgayCap_CT { set; get; }
    public string NoiCap_CT { set; get; }
    public string GiayDangKy { set; get; }
    public string NoiCapGiayDangKy { set; get; }
    public string GiayCN { set; get; }
    public string NgayCap_GiayCN { set; get; }
    public string ChungChiNN { set; get; }
    public string NgayCap_ChungChiNN { set; get; }
    public string NoiCap_ChungChiNN { set; get; }
    public string ChucVu { set; get; }
    public string DonViCongTac { set; get; }
    public string Email { set; get; }
    public string DienThoai { set; get; }
    public string DD { set; get; }
    public string SoThich { set; get; }
    
    //public string lst
    //public List<lstOBJQuaTrinhLamViec> lstQuaTrinhLamViec { get; set; }
    //public List<lstOBJKhenThuong> lstKhenThuong { get; set; }
}

public class Anh_Tr
{
    public byte[] arr { set; get; }
}

public class lstOBJQuaTrinhLamViec
{
    public string HoTen { set; get; }
    public string ThoiGian { set; get; }
    public string ChucVu { set; get; }
    public string DonViCongTac { set; get; }
}

public class lstOBJKhenThuong
{
    public string HoTen { set; get; }
    public string NgayThang { set; get; }
    public string HinhThuc { set; get; }
    public string LyDo { set; get; }
}

public class lstOBJKyLuat
{
    public string HoTen { set; get; }
    public string NgayThang { set; get; }
    public string HinhThuc { set; get; }
    public string LyDo { set; get; }
}

public class lst_KetQuaKiemSoatChatLuong
{
    public string HoTen { set; get; }
    public string STT { set; get; }
    public string HoSo { set; get; }
    public string KetQua { set; get; }
}

public class KetQuaKiemSoatChatLuong
{
    public string HoTen { set; get; }
    public string TongKiemTra { set; get; }
    public string TongDaKy { set; get; }
    public string SoLuongBC { set; get; }
}

public class ThongTinThamGiaDoanKiemTra
{
    public string HoTen { set; get; }
    public string Nam { set; get; }
    public string Ma { set; get; }
    public string Ngay { set; get; }
    public string DanhSach { set; get; }
}

public class objThongTinDoanhNghiep_CT
{

    public string STT { set; get; }
    public string MaHoiVienTapThe { set; get; }
    public string LoaiHoiVienChiTiet { set; get; }
    public string SoHieu { set; get; }
    public string TenDoanhNghiep { set; get; }
    public string TenVietTat { set; get; }
    public string TenTiengAnh { set; get; }
    public string DiaChi { set; get; }
    public string DienThoai { set; get; }
    public string Fax { set; get; }
    public string Email { set; get; }
    public string Website { set; get; }
    public string SoDK { set; get; }
    public string NgayDK { set; get; }
    public string SoGiayCN { set; get; }
    public string NgayCN { set; get; }
    public string NguoiDaiDien_Ten { set; get; }
    public string NguoiDaiDien_mobile { set; get; }
    public string NguoiDaiDien_Email { set; get; }
    public string NguoiDaiDienLL_Ten { set; get; }
    public string NguoiDaiDienLL_mobile { set; get; }
    public string NguoiDaiDienLL_Email { set; get; }
    public string SoQD { set; get; }
    public string NgayKyQD { set; get; }

}

public class objDonXinGiaNhapHoiVienTapThe
{
   
    public string SoHieu { set; get; }
    public string TenDoanhNghiep { set; get; }
    public string TenVietTat { set; get; }
    public string TenTiengAnh { set; get; }
    public string DiaChi { set; get; }
    public string DienThoai { set; get; }
    public string Fax { set; get; }
    public string Email { set; get; }
    public string Website { set; get; }
    public string SoDK { set; get; }
    public string NgayDK { set; get; }
    public string SoGiayCN { set; get; }
    public string NgayCN { set; get; }
    public string LoaiHinhDoanhNghiep1 { set; get; }
    public string LoaiHinhDoanhNghiep2 { set; get; }
    public string LoaiHinhDoanhNghiep3 { set; get; }
    public string LoaiHinhDoanhNghiep4 { set; get; }
    public string LoaiHinhDoanhNghiep5 { set; get; }
    public string MaSoThue { set; get; }
    public string LinhVucHoatDong { set; get; }
    public string NamCTyDuDKKiemToan { set; get; }
    public string CoLoiIchNam { set; get; }
    public string LinhVucCKNam { set; get; }
    public string LaThanhVienCuaHangKiemToan { set; get; }
    public string TongSoNgLamViecTaiCty { set; get; }
    public string TongSoNgCCKTV { set; get; }
    public string TongSoKTVDKHN { set; get; }
    public string TongSoNgLaHoiVienCaNhan { set; get; }
    public string NguoiDaiDien_Ten { set; get; }
    public string NguoiDaiDien_DienThoai { set; get; }
    public string NguoiDaiDien_mobile { set; get; }
    public string NguoiDaiDien_Email { set; get; }
    public string NgaySinh { set; get; }
    public string GioiTInhNam { set; get; }
    public string GioiTInhNu { set; get; }
    public string NguoiDaiDien_ChucVu { set; get; }
    public string NguoiDaiDien_SoCCKTV { set; get; }
    public string NguoiDaiDien_NgayCCKTV { set; get; }
    public string NguoiDaiDienLL_Ten { set; get; }
    public string NguoiDaiDienLL_DienThoai { set; get; }
    public string NguoiDaiDienLL_mobile { set; get; }
    public string NguoiDaiDienLL_Email { set; get; }
    public string NguoiDaiDienLL_ChuVu { set; get; }
    public string NguoiDaiDienLL_NGaySinh { set; get; }
    public string NguoiDaiDienLL_GioiTinhNam { set; get; }
    public string NguoiDaiDienLL_GioiTinhNu { set; get; }
    public string SoQD { set; get; }
    public string NgayKyQD { set; get; }

    public string NgayThangNam { set; get; }
    public string temp1 { set; get; }
    public string temp2 { set; get; }
    public string temp3 { set; get; }
    public string temp4 { set; get; }
    public string temp5 { set; get; }

}

public class objDonXinGiaNhapHoiVienCaNhan
{
    public string HoTen { set; get; }
    public string GioiTinh_Nam { set; get; }
    public string GioiTinh_Nu { set; get; }
    public string NgaySinh { set; get; }
    public string QueQuan { set; get; }
    public string ChoOHienNay { set; get; }
    public string BangCap { set; get; }
    public string ChuyenNganh { set; get; }
    public string Nam_Bang { set; get; }
    public string HocHam { set; get; }
    public string Nam_HocHam { set; get; }
    public string HocVi { set; get; }
    public string Nam_HocVi { set; get; }
    public string CMT { set; get; }
    public string NgayCap { set; get; }
    public string NoiCap { set; get; }
    public string SoTK { set; get; }
    public string TaiNganHang { set; get; }
    public string SoChungChi { set; get; }
    public string NgayCap_CT { set; get; }
    public string NoiCap_CT { set; get; }
    public string GiayDangKy { set; get; }
    public string NoiCapGiayDangKy { set; get; }
    public string GiayCN { set; get; }
    public string NgayCap_GiayCN { set; get; }
    public string ChungChiNN { set; get; }
    public string NgayCap_ChungChiNN { set; get; }
    public string NoiCap_ChungChiNN { set; get; }
    public string ChucVu { set; get; }
    public string DonViCongTac { set; get; }
    public string Email { set; get; }
    public string DienThoai { set; get; }
    public string DD { set; get; }
    public string DiaChi { set; get; }
    public string SoThich { set; get; }
    public string NgayThangNam { set; get; }
}

public class NguoiLapBieu
{
    public string nguoiLapBieu { set; get; }
}

public class TenBaoCao
{
    public string TenBC { set; get; }
    public string ngayBC { set; get; }
    public string NgayTinh { set; get; }
    public string SoLuong { set; get; }
    public string TongCong { set; get; }
    public string NguoiLapBieu { set; get; }
}

public class DanhSachKetNapHoiVienCaNhan
{
    public string TenCT { set; get; }
    public string STT { set; get; }
    public string MaSo { set; get; }
    public string LoaiHoiVien { set; get; }   
    public string HoTen { set; get; }
    public string NgaySinh_Nam { set; get; }
    public string NgaySinh_Nu { set; get; }
    public string QueQuan { set; get; }
    public string CuNhan { set; get; }
    public string ThacSi { set; get; }
    public string TienSi { set; get; }
    public string GS { set; get; }
    public string CMT { set; get; }
    public string NgayCap { set; get; }
    public string SoTK { set; get; }
    public string TaiNH { set; get; }
    public string So_CCKTV { set; get; }
    public string Ngay_CCKTV { set; get; }
    public string SoGiayCN { set; get; }
    public string GiayCN_Tu { set; get; }
    public string GiayCN_Den { set; get; }
    public string ChungChiQT_So { set; get; }
    public string ChungChiQT_Ngay { set; get; }
    public string ChungChiQT_NoiCap { set; get; }
    public string ChucVuHienNay { set; get; }
    public string DonViCongTac { set; get; }
    public string ChoOHienNay { set; get; }
    public string DTCD { set; get; }
    public string Mobile { set; get; }
    public string Email { set; get; }
    public string SoQDKetNap { set; get; }
    public string NgayQDKetNap { set; get; }
}

public class groupLyDo
{
    public string gLyDo { set; get; }
}

public class DanhSachBiXoaTen
{
    public string gLyDo { set; get; }
    public string XacNhan { set; get; }
    public string STT { set; get; }
    public string Ten { set; get; }
    public string NgaySinh_Nam { set; get; }
    public string NgaySinh_Nu { set; get; }
    public string So_KTV { set; get; }
    public string NgayCap_KTV { set; get; }
}


    