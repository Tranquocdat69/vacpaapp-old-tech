﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for objQuanLyThongTinChung
/// </summary>
public class objDanhSachCongTyKiemToanTrongCaNuoc
{
    public string strTenDonVi { set; get; }
    public string NgayThangNam { set; get; }
    public string STT { set; get; }
    public string SoHieuCongTy { set; get; }
    public string TenCongTy { set; get; }
    public string TenVietTat { set; get; }
    public string NgayQuyetDinhKetNapHoiVienTapThe { set; get; }
    public string DiaChiTruSoChinh { set; get; }
    public string HoVaTenNguoiDaiDien { set; get; }
    public string MobileNguoiDaiDien { set; get; }
    public string SoGiayChungNhanDuDKKDDichVuKiT { set; get; }
    public string NgayCapGiayChungNhanDuDKKDDichVuKiT { set; get; }
    public string TenChiNhanh { set; get; }
    public string DiaChiChiNhanh { set; get; }
    public string HoVaTenGDChiNhanh { set; get; }
    public string MobileGDChiNhanh { set; get; }
}

public class objGiayChungNhanGioCNKT_All
{
    public string HoiVienId { set; get; }
    public string HoTen { set; get; }
    public string ChungChiSo { set; get; }
    public string NgayCap { set; get; }
    public string TenChuyenDe { set; get; }
    public string Ngay { set; get; }
    public string Buoi { set; get; }
    public string Loai_KTKIT { set; get; }
    public string Loai_DD { set; get; }
    public string Loai_Khac { set; get; }
    public string STT { set; get; }
    public string TongSoGio { set; get; }
    public string Cong_KTKIT { set; get; }
    public string Cong_DD { set; get; }
    public string Cong_Khac { set; get; }
    public string Cong_Tong { set; get; }
    public string TT_Nam { set; get; }
    public string TT_LopHoc { set; get; }
    public string TT_NgayKy { set; get; }
    public string TT_SoVaoSo { set; get; }
}

public class objDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet
{
    public string strTenDonVi { set; get; }
    public string NgayThangNam { set; get; }
    public string STT { set; get; }
    public string TenIDHVTT_IDCTKT { set; get; }
    public string SoHieuCongTy { set; get; }
    public string TenDoanhNghiep { set; get; }
    public string TenVietTat { set; get; }
    public string TenTiengAnh { set; get; }
    public string SoQuyetDinhKetNapHVTT { set; get; }
    public string NgayKyQuyetDinhKetNapHVTT { set; get; }
    public string LoaiHinhDoanhNghiep_THHH_HD_DNTN { set; get; }
    public string LoaiHinhDoanhNghiepTrongNuoc { set; get; }
    public string LoaiHinhDoanhNghiepCoVonDTNN{ set; get; }
    public string VonDieuLe { set; get; }
    public string TongDoanhThu { set; get; }
    public string DoanhThuDichVuKiemToan { set; get; }
    public string TongSoKhachHangTrongNam { set; get; }
    public string SoLuongKHLaDonViCoLoiIchCongChungThuocLinhVucChungKhoan { set; get; }
    public string DiaChiTruSoChinh { set; get; }
    public string NgayThanhLap { set; get; }
    public string DTCD { set; get; }
    public string Fax { set; get; }
    public string Email { set; get; }
    public string Website { set; get; }
    public string SoGiayCNDangKyKD_GiayCNDauTu { set; get; }
    public string NgayCapGiayCNDangKyKD_GiayCNDauTu { set; get; }
    public string SoGiayChungNhanDuDKKDDichVuKiT { set; get; }
    public string NgayCapGiayChungNhanDuDKKDDichVuKiT { set; get; }
    public string MaSoThue { set; get; }
    public string LinhVucHoatDongChinh { set; get; }
    public string DonViDuDKKitTrongLinhVucChungKhoan { set; get; }
    public string DonViDuDKKiTCongChungKhacNam { set; get; }
    public string CongTyDuDKKiemToanDonvi { set; get; }
    public string CongTyLaThanhVienHangKiemToanQuocTe { set; get; }
    public string TongSoNguoiLamVieccTaiDN { set; get; }
    public string TongSoNguoiCoChungChiKTV { set; get; }
    public string TongSoKTVDangKyHanhNgheKiemToan { set; get; }
    public string TongSoHoiVienCaNhan { set; get; }
    public string NDDPL_HoVaTen { set; get; }
    public string NDDPL_NgaySinh { set; get; }
    public string NDDPL_ChucVu { set; get; }
    public string NDDPL_GioiTinh { set; get; }
    public string NDDPL_CCKTV { set; get; }
    public string NDDPL_NgayCapCCKTV { set; get; }
    public string NDDPL_NoiCap { set; get; }
    public string NDDPL_DienThoaiCoDinh_DiDong { set; get; }
    public string NDDPL_Email { set; get; }
    public string NDDLL_HoVaTen { set; get; }
    public string NDDLL_NgaySinh { set; get; }
    public string NDDLL_ChucVu { set; get; }
    public string NDDLL_GioiTinh { set; get; }
    public string NDDLL_DienThoaiCoDinh_DiDong { set; get; }
    public string NDDLL_Email { set; get; }
    public string TTLLCN_TenChiNhanh { set; get; }
    public string TTLLCN_DiaChiChiNhanh { set; get; }
    public string TTLLCN_DienThoaiCoDinh_DiDong { set; get; }
    public string TTLLCN_EmailChiNhanh { set; get; }
    public string TTLLCN_HoVaTenGiamDocChiNhanh { set; get; }
    public string TTLLCN_MobileGD { set; get; }
    public string TTLLCN_EmailGiamDocChiNhanh { set; get; }
    public string TTLLVPDD_TenVPDD { set; get; }
    public string TTLLVPDD_DiaChiVPDD { set; get; }
    public string TTLLVPDD_DienThoaiCoDinh_DiDong { set; get; }
    public string TTLLVPDD_EmailVPDD { set; get; }
    public string TTLLVPDD_HoVaTenGiamDocVPDD { set; get; }
    public string TTLLVPDD_MobileGD { set; get; }
    public string TTLLVPDD_EmailGDVPDD { set; get; }
    public string KQKSCL_TongSobaoCaoKiemToanDaKyTrongNam { set; get; }
    public string KQKSCL_SoLuongBaoCaoKiemToanChoDonViCoLoiIchCongChungTrongLinhVucChungKhoan { set; get; }
    public string KQKSCL_SoLuongBaoCaoKiemToanDuocKiemTra { set; get; }
    public string KhenThuong_NgayThang { set; get; }
    public string KhenThuong_CapVaHinhThucKhenThuongKyLuat { set; get; }
    public string KhenThuong_LyDo { set; get; }
    public string KyLuat_NgayThang { set; get; }
    public string KyLuat_CapVaHinhThucKhenThuongKyLuat { set; get; }
    public string KyLuat_LyDo { set; get; }
}

public class objDanhSachKiemToanVienTrongCaNuoc
{
    public string TenDonVi { set; get; }
    public string NgayThangNam { set; get; }
    public string STT { set; get; }
    public string SoHieuCongTy { set; get; }
    public string TenCongTy { set; get; }
    public string HoVaTen { set; get; }
    public string NgaySinhNam { set; get; }
    public string NgaySinhNu { set; get; }
    public string HoiVienVACPA { set; get; }
    public string QueQuanQuocTich { set; get; }
    public string ChucVu { set; get; }
    public string SoChungChiKTV { set; get; }
    public string NgayCapChungChiKTV { set; get; }
    public string SoGiayChungNhanDKHNKiT { set; get; }
    public string ThoiHanGiayChungNhan_BatDau { set; get; }
    public string ThoiHanGiayChungNhan_KetThuc { set; get; }
    public string DienThoaiCoDinhDiDong { set; get; }
    public string Email { set; get; }
}

public class objDanhSachKiemToanVienTrongCaNuoc_ChiTiet
{
    public string TenDonVi { set; get; }
    public string NgayThangNam { set; get; }
    public string SoHieuCongTy { set; get; }
    public string TenCongTy { set; get; }
    public string STT { set; get; }
    public string IDHVCN_IDKTV { set; get; }
    public string HoVaTen { set; get; }
    public string NgaySinhNam { set; get; }
    public string NgaySinhNu { set; get; }
    public string SoQuyetDinhKetNapHVCN { set; get; }
    public string NgayKyQuyetDinhKetNapHVCN { set; get; }
    public string QueQuanQuocTich { set; get; }
    public string DiaChiNoiOHienTai { set; get; }
    public string TDCM_TotNghiepDaiHoc { set; get; }
    public string TDCM_ChuyenNganh { set; get; }
    public string TDCM_NamTotNghiep { set; get; }
    public string TDCM_HocVi { set; get; }
    public string TDCM_NamHocVi { set; get; }
    public string TDCM_HocHam { set; get; }
    public string TDCM_NamHocHam { set; get; }
    public string SoCMND_HoChieu { set; get; }
    public string NgayCapCMND_HoChieu { set; get; }
    public string SoTaiKhoanNganHang { set; get; }
    public string TaiNganHang { set; get; }
    public string SoChungChiKTV { set; get; }
    public string NgayCapChungChiKTV { set; get; }
    public string SoGiayCNDKHNKiT { set; get; }
    public string NgayCapGiayCNDKHNKiT { set; get; }
    public string HanCapGiayCNDKHNKT_TuNgay { set; get; }
    public string HanCapGiayCNDKHNKT_DenNgay { set; get; }
    public string TenChungChiNuocNgoai { set; get; }
    public string NgayCapChungChiNuocNgoai { set; get; }
    public string ChucVuHienNay { set; get; }
    public string Email { set; get; }
    public string Mobile_DienThoaiCoDinh { set; get; }
    public string SoThich { set; get; }
    public string KQKSCL_TongBaoCaoKiemToanDaKyTrongNam { set; get; }
    public string KQKSCL_SoLuongBaoCaoKiemToanChoDonViCoLoiIchCongChung { set; get; }
    public string KSKSCL_SoLuongBaoCaoKiemToanDuocKiemTra { set; get; }
    public string KhenThuong_NgayThang { set; get; }
    public string KhenThuong_CapVaHinhThucKhenThuongKyLuat { set; get; }
    public string KhenThuong_LyDo { set; get; }
    public string KyLuat_NgayThang { set; get; }
    public string KyLuat_CapVaHinhThucKhenThuongKyLuat { set; get; }
    public string KyLuat_LyDo { set; get; }
}

public class objDanhSachNguoiQuanTamTrongCaNuoc
{
    public string TenDonVi { set; get; }
    public string NgayThangNam { set; get; }
    public string STT { set; get; }
    public string IDNguoiQuanTam { set; get; }
    public string HoVaTen { set; get; }
    public string NgaySinhNam { set; get; }
    public string NgaySinhNu { set; get; }
    public string ChucVu { set; get; }
    public string DonViCongTac { set; get; }
    public string TroLyKTVCTKT { set; get; }
    public string DienThoaiCoDinhDiDong { set; get; }
    public string Email { set; get; }
}

public class objGiayChungNhanHoiVienChinhThuc
{
    public string HoVaTen { set; get; }
    public string SoCCKTV { set; get; }
    public string NgayCap { set; get; }
    public string HoVaTenTiengAnh { set; get; }
    public string SoCCKTVTiengAnh { set; get; }
    public string NgayCapTiengAnh { set; get; }
    public string NgayThangNamRaQuyetDinh { set; get; }
    public string SoQuyetDinh { set; get; }
}