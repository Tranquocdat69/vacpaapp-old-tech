﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ListName
/// </summary>
public class ListName
{
    public static string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["netTiersConnectionString"].ConnectionString;

    // Update by HUNGNM - 2014/11/18
    // List table name in Database
    #region DAO TAO - CNKT
    public const string Table_CNKTLopHoc = "tblCNKTLopHoc";
    public const string Table_CNKTLopHocBuoi = "tblCNKTLopHocBuoi";
    public const string Table_CNKTLopHocPhi = "tblCNKTLopHocPhi";
    public const string Table_CNKTLopHocChuyenDe = "tblCNKTLopHocChuyenDe";
    public const string Table_CNKTCoCauSoGioHocToiThieu = "tblCNKTCoCauSoGioHocToiThieu";
    public const string Table_CNKTLopHocBaoCao = "tblCNKTLopHocBaoCao";
    public const string Table_CNKTLopHocBaoCaoChiTiet = "tblCNKTLopHocBaoCaoChiTiet";
    #endregion

    #region KIEM SOAT CHAT LUONG

    public const string Table_KSCLDSBaoCaoTuKiemTra = "tblKSCLDSBaoCaoTuKiemTra";
    public const string Table_KSCLDSBaoCaoTuKiemTraChiTiet = "tblKSCLDSBaoCaoTuKiemTraChiTiet";
    public const string Table_KSCLDSKiemTraVuViec = "tblKSCLDSKiemTraVuViec";
    public const string Table_KSCLDSKiemTraVuViecChiTiet = "tblKSCLDSKiemTraVuViecChiTiet";
    public const string Table_KSCLDSKiemTraTrucTiep = "tblKSCLDSKiemTraTrucTiep";
    public const string Table_KSCLDSKiemTraTrucTiepChiTiet = "tblKSCLDSKiemTraTrucTiepChiTiet";
    public const string Table_KSCLDoanKiemTra = "tblKSCLDoanKiemTra";
    public const string Table_KSCLDoanKiemTraCongTy = "tblKSCLDoanKiemTraCongTy";
    public const string Table_KSCLDoanKiemTraThanhVien = "tblKSCLDoanKiemTraThanhVien";
    public const string Table_KSCLDoanKiemTraSoGioCNKT = "tblKSCLDoanKiemTraSoGioCNKT";

    public const string Table_KSCLHoSo = "tblKSCLHoSo";
    public const string Table_KSCLHoSoFile = "tblKSCLHoSoFile";
    public const string Table_KSCLCamKet = "tblKSCLCamKet";

    public const string Table_DMNhomCauHoiKSCL = "tblDMNhomCauHoiKSCL";
    public const string Table_KSCLCauHoiKT = "tblKSCLCauHoiKT";
    public const string Table_KSCLBaoCaoKTHT = "tblKSCLBaoCaoKTHT";
    public const string Table_KSCLBaoCaoKTHTChiTiet = "tblKSCLBaoCaoKTHTChiTiet";
    public const string Table_KSCLBaoCaoKTKT = "tblKSCLBaoCaoKTKT";
    public const string Table_KSCLBaoCaoKTKTChiTiet = "tblKSCLBaoCaoKTKTChiTiet";
    public const string Table_KSCLBaoCaoTongHop = "tblKSCLBaoCaoTongHop";
    public const string Table_KSCLXuLySaiPham = "tblKSCLXuLySaiPham";
    public const string Table_KSCLXuLySaiPhamChiTiet = "tblKSCLXuLySaiPhamChiTiet";

    public const string Table_DMTaiLieu = "tblDMTaiLieu";
    public const string Table_TTDangKyLogo = "tblTTDangKyLogo";
    public const string Table_TTPhatSinhPhi = "tblTTPhatSinhPhi";

    #endregion

    public const string Table_DMTrangThai = "tblDMTrangThai";
    public const string Table_DMGiangVien = "tblDMGiangVien";

    public const string Table_ThamSoHeThong = "tblThamSoHeThong";

    // Update by HUNGNM - 2014/11/18
    // List function name 
    public const string Func_CNKT_QuanLyLopHoc = "QuanLyLopHoc";
    public const string Func_CNKT_QuanLyChuyenDe = "QuanLyChuyenDe";
    public const string Func_CNKT_QuanLyDangKyHoc = "QuanLyDangKyHoc";
    public const string Func_CNKT_CapNhatSoGioCNKT = "CapNhatSoGioCNKT";
    public const string Func_CNKT_BaoCaoKetQuaToChucLopHoc = "BaoCaoKetQuaToChucLopHoc";
    public const string Func_CNKT_TongHop_SoGioCNKTCuaHocVien = "TongHopSoGioCNKT";
    public const string Func_CNKT_TongHop_SoGioCNKTConThieuCuaHocVien = "TongHopSoGioCNKTConThieu";

    public const string Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra = "DSCongTyKTNopBCTuKiemTra";
    public const string Func_KSCL_ChuanBi_DSCongTyKTTheoVuViec = "DSCongTyKTTheoVuViec";
    public const string Func_KSCL_ChuanBi_DSBaoCaoTuKiemTra = "DSBaoCaoTuKiemTra";
    public const string Func_KSCL_ChuanBi_DSCongTyKiemTraTrucTiep = "DSCongTyKiemTraTrucTiep";
    public const string Func_KSCL_ChuanBi_DSDoanKiemTra = "DsDoanKiemTra";
    public const string Func_KSCL_ChuanBi_GioDTThanhVienDoanKT = "GioDTThanhVienDoanKT";
    public const string Func_KSCL_ThucHien_CamKetBaoMat = "CamKetBaoMat";
    public const string Func_KSCL_ThucHien_QuanLyHoSoKSCL = "QuanLyHoSoKSCL";
    public const string Func_KSCL_ThucHien_CauHoiKTHT = "CauHoiKTHT";
    public const string Func_KSCL_ThucHien_CauHoiKTKT = "CauHoiKTKT";
    public const string Func_KSCL_ThucHien_BCKetQuaKTHT = "BCKetQuaKTHT";
    public const string Func_KSCL_ThucHien_BCKetQuaKTKT = "BCKetQuaKTKT";
    public const string Func_KSCL_ThucHien_TongHopKetQuaKT = "TongHopKetQuaKT";
    public const string Func_KSCL_KetThuc_XuLySaiPham = "XuLySaiPham";
    public const string Func_QTHT_DMTaiLieu = "DMTaiLieu";
    public const string Func_QLHV_DSCongTyDangKyDangLogo = "DSCongTyDangKyDangLogo";

    /// <summary>
    /// Update by NGUYEN MANH HUNG - 2014/11/20
    /// Danh sach cac gia tri can luu co dinh
    /// </summary> 
    public const string Type_HoiVienCaNhan = "1";
    public const string Type_KTV = "2";
    public const string Type_TroLyKTV = "3";
    public const string Type_KhongPhaiTroLyKTV = "4";

    public const string Type_LoaiChuyenDe_KeToanKiemToan = "1";
    public const string Type_LoaiChuyenDe_DaoDucNgheNghiep = "2";
    public const string Type_LoaiChuyenDe_ChuyenDeKhac = "3";

    public const string Type_LoaiHocVien_NguoiQuanTam = "0";
    public const string Type_LoaiHocVien_HoiVien = "1";
    public const string Type_LoaiHocVien_KiemToanVien = "2";

    public const string Type_LoaiCongTy_CtyKTChuaDuocCapID = "0";
    public const string Type_LoaiCongTy_HoiVienTapThe = "1";
    public const string Type_LoaiCongTy_CtyKTDaDuocCapID = "2";

    public const string Status_ChoTiepNhan = "1";
    public const string Status_TuChoi = "2";
    public const string Status_ChoDuyet = "3";
    public const string Status_KhongPheDuyet = "4";
    public const string Status_DaPheDuyet = "5";
    public const string Status_ThoaiDuyet = "6";
    public const string Status_ChoPheDuyetLai = "7";

    public const string Param_SoNguoiHocToiDa_CD = "001";


    /// <summary>
    /// Update by NGUYEN MANH HUNG - 2014/12/09
    /// 
    /// </summary>
    public const string Proc_procTongHopSoGioCNKT = "procTongHopSoGioCNKT";
    public const string Proc_procTongHopSoGioCNKTConThieu = "procTongHopSoGioCNKTConThieu";
    public const string Proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc = "proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc";
    public const string Proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc_DanhGiaGiangVien = "proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc_DanhGiaGiangVien";

    public const string Proc_KSCL_SelectDanhSachCongTyKiemToanNopBaoCaoTuKiemTra = "proc_KSCL_SelectDanhSachCongTyKiemToanNopBaoCaoTuKiemTra";
    public const string Proc_KSCL_SearchDanhSachCongTyKiemToan = "proc_KSCL_SearchDanhSachCongTyKiemToan";
    public const string Proc_KSCL_SearchDanhSachCongTyKiemToan_1 = "proc_KSCL_SearchDanhSachCongTyKiemToan_1";
    public const string Proc_KSCL_SearchDanhSachCongTyKiemToan_YeuCauKiemTraVuViec = "proc_KSCL_SearchDanhSachCongTyKiemToan_YeuCauKiemTraVuViec";
    public const string Proc_KSCL_SearchDanhSachCongTyKiemToan_DSKiemTraTrucTiep = "proc_KSCL_SearchDanhSachCongTyKiemToan_DSKiemTraTrucTiep";
    public const string Proc_KSCL_SearchDanhSachCongTyKiemToan_DoanKiemTra = "proc_KSCL_SearchDanhSachCongTyKiemToan_DoanKiemTra";
    public const string Proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra = "proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra";
    public const string Proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec = "proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec";
    public const string Proc_KSCL_SearchDanhSachBaoCaoTuKiemTra = "proc_KSCL_SearchDanhSachBaoCaoTuKiemTra";
    public const string Proc_KSCL_SearchDanhSachCongTyKTKiemTraTrucTiep = "proc_KSCL_SearchDanhSachCongTyKTKiemTraTrucTiep";

    public const string Proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra_Mini1 = "proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra_Mini1"; // Dùng cho select dữ liệu hiển thị trong cửa sổ nhỏ chọn danh sách
    public const string Proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec_Mini1 = "proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec_Mini1"; // Dùng cho select dữ liệu hiển thị trong cửa sổ nhỏ chọn danh sách
    public const string Proc_KSCL_SearchDanhSachDoanKiemTra = "proc_KSCL_SearchDanhSachDoanKiemTra";
    public const string Proc_KSCL_DeleteDanhSachDoanKiemTra = "proc_KSCL_DeleteDanhSachDoanKiemTra";
    public const string Proc_KSCL_SearchDanhSachThanhVienDoanKiemTra_1 = "proc_KSCL_SearchDanhSachThanhVienDoanKiemTra_1";
    public const string Proc_KSCL_SelectDanhSachThanhVienTrongDoanKiemTra = "proc_KSCL_SelectDanhSachThanhVienTrongDoanKiemTra";
    public const string Proc_KSCL_SelectDsKiemTraTrucTiepDuDKDeLapDoanKiemTra = "proc_KSCL_SelectDsKiemTraTrucTiepDuDKDeLapDoanKiemTra";
    public const string Proc_KSCL_SearchDanhSachHoSoKiemSoatChatLuong = "proc_KSCL_SearchDanhSachHoSoKiemSoatChatLuong";

    public const string Proc_KSCL_TinhTongDiemDaQuyDoiKTHT = "proc_KSCL_TinhTongDiemDaQuyDoiKTHT";
    public const string Proc_KSCL_TinhTongDiemDaQuyDoiKTKT = "proc_KSCL_TinhTongDiemDaQuyDoiKTKT";
    public const string Proc_QTHT_SearchDanhSachTaiLieu = "proc_QTHT_SearchDanhSachTaiLieu";
    public const string Proc_QLHV_SearchDanhSachDangKyDangLogo = "proc_QLHV_SearchDanhSachDangKyDangLogo";

    #region  Module báo cáo

    public const string Proc_BAOCAO_KSCL_DanhSachCongTyKiemToanPhaiNopBaoCaoTuKiemTra = "proc_BAOCAO_KSCL_DanhSachCongTyKiemToanPhaiNopBaoCaoTuKiemTra";
    public const string Proc_BAOCAO_KSCL_BangTinhHinhNopBaoCaoTuKiemTra = "proc_BAOCAO_KSCL_BangTinhHinhNopBaoCaoTuKiemTra";
    public const string Proc_BAOCAO_KSCL_DanhSachCongTyKiemTraTrucTiep = "proc_BAOCAO_KSCL_DanhSachCongTyKiemTraTrucTiep";
    public const string Proc_BAOCAO_KSCL_DanhSachHoiVienKiemTraChatLuongDichVuNam = "proc_BAOCAO_KSCL_DanhSachHoiVienKiemTraChatLuongDichVuNam";
    public const string Proc_BAOCAO_KSCL_DanhSachThanhVienDoanKiemTra = "proc_BAOCAO_KSCL_DanhSachThanhVienDoanKiemTra";
    public const string Proc_BAOCAO_KSCL_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien = "proc_BAOCAO_KSCL_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien";
    public const string Proc_BAOCAO_CNKT_TongHopSoGioCNKTCuaKTV = "proc_BAOCAO_CNKT_TongHopSoGioCNKTCuaKTV";
    public const string Proc_BAOCAO_CNKT_TongHopSoGioCNKTCuaKTVChiTiet = "proc_BAOCAO_CNKT_TongHopSoGioCNKTCuaKTVChiTiet";
    public const string Proc_BAOCAO_QLDT_TONGHOPSOGIOCNKT_DYNAMICCOLUMN = "proc_BAOCAO_QLDT_TONGHOPSOGIOCNKT_DYNAMICCOLUMN";
    
    #endregion

    // Updated by NGUYEN MANH HUNG - 2014/12/17
    // Quy định các từ khóa để kiểm tra quyền
    #region Quyền
    public const string PERMISSION_Xem = "XEM|";
    public const string PERMISSION_Them = "THEM|";
    public const string PERMISSION_Sua = "SUA|";
    public const string PERMISSION_Xoa = "XOA|";
    public const string PERMISSION_Duyet = "DUYET|";
    public const string PERMISSION_TraLai = "TRALAI|";
    public const string PERMISSION_TuChoi = "TUCHOI|";
    public const string PERMISSION_ThoaiDuyet = "THOAIDUYET|";
    public const string PERMISSION_KetXuat = "KETXUAT|";
    #endregion

    public static CultureInfo CI_US = CultureInfo.CreateSpecificCulture("en-US");
    public static CultureInfo CI_VN = CultureInfo.CreateSpecificCulture("vi-VN");
}