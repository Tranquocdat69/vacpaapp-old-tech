﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using HiPT.VACPA.DL;
using System.Net;
using System.IO;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using CommonLayer;
using System.Text.RegularExpressions;
using System.Web.SessionState;

/// <summary>
/// Summary description for Commons
/// </summary>
public class Commons
{
    public string connstr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;



    public string Admin_NguoiDungID { get; set; }
    public string Admin_TenDangNhap { get; set; }
    public string Admin_ChucVu { get; set; }
    public string Admin_HoVaTen { get; set; }
    public string Admin_BoPhanCongTac { get; set; }
    public string Admin_Email { get; set; }
    public string Admin_DienThoai { get; set; }
    public string Khachhang_KhachHangID { get; set; }
    public string Khachhang_TenToChuc { get; set; }
    public string Khachhang_TenTaiKhoan { get; set; }
    public string Khachhang_Email { get; set; }
    public string Khachhang_HoVaTenNguoiDangKy { get; set; }
    public string login_link { get; set; }



    public string HienThiMaChuaDuyet { get; set; }

    public Commons()
    {
        try
        {
            HttpSessionState session = System.Web.HttpContext.Current.Session;
            if (session != null)
            {
                string Admin_NguoiDungIDFromSession = session["Admin_NguoiDungID"] != null ? session["Admin_NguoiDungID"].ToString() : String.Empty;
                string Admin_TenDangNhapFromSession = session["Admin_TenDangNhap"] != null ? session["Admin_TenDangNhap"].ToString() : String.Empty;
                string Admin_ChucVuFromSession = session["Admin_ChucVu"] != null ? session["Admin_ChucVu"].ToString() : String.Empty;
                string Admin_HoVaTenFromSession = session["Admin_HoVaTen"] != null ? session["Admin_HoVaTen"].ToString() : String.Empty;
                string Admin_BoPhanCongTacFromSession = session["Admin_BoPhanCongTac"] != null ? session["Admin_BoPhanCongTac"].ToString() : String.Empty;
                string Admin_EmailFromSession = session["Admin_Email"] != null ? session["Admin_Email"].ToString() : String.Empty;
                string Admin_DienThoaiFromSession = session["Admin_DienThoai"] != null ? session["Admin_DienThoai"].ToString() : String.Empty;
                string Khachhang_KhachHangIDFromSession = session["Khachhang_KhachHangID"] != null ? session["Khachhang_KhachHangID"].ToString() : String.Empty;
                string Khachhang_TenToChucFromSession = session["Khachhang_TenToChuc"] != null ? session["Khachhang_TenToChuc"].ToString() : String.Empty;
                string Khachhang_TenTaiKhoanFromSession = session["Khachhang_TenTaiKhoan"] != null ? session["Khachhang_TenTaiKhoan"].ToString() : String.Empty;
                string Khachhang_EmailFromSession = session["Khachhang_Email"] != null ? session["Khachhang_Email"].ToString() : String.Empty;
                string Khachhang_HoVaTenNguoiDangKyFromSession = session["Khachhang_HoVaTenNguoiDangKy"] != null ? session["Khachhang_HoVaTenNguoiDangKy"].ToString() : String.Empty;

                Admin_NguoiDungID = Admin_NguoiDungIDFromSession;
                Admin_TenDangNhap = Admin_TenDangNhapFromSession;
                Admin_ChucVu = Admin_ChucVuFromSession;
                Admin_HoVaTen = Admin_HoVaTenFromSession;
                Admin_BoPhanCongTac = Admin_BoPhanCongTacFromSession;
                Admin_Email = Admin_EmailFromSession;
                Admin_DienThoai = Admin_DienThoaiFromSession;
                Khachhang_KhachHangID = Khachhang_KhachHangIDFromSession;
                Khachhang_TenToChuc = Khachhang_TenToChucFromSession;
                Khachhang_TenTaiKhoan = Khachhang_TenTaiKhoanFromSession;
                Khachhang_Email = Khachhang_EmailFromSession;
                Khachhang_HoVaTenNguoiDangKy = Khachhang_HoVaTenNguoiDangKyFromSession;
            }


            login_link = System.Configuration.ConfigurationSettings.AppSettings["vacpaweblogin_link"];



        }
        catch(Exception ex)
        {
        }

    }


    public string AddSlashes(string InputTxt)
    {
        string Result = InputTxt;

        try
        {

            Result = System.Text.RegularExpressions.Regex.Replace(InputTxt, @"[\000\010\011\015\032\042\047\134\140]", "\\$0");
            Result = Result.Replace(System.Environment.NewLine, @"n");

        }
        catch (Exception Ex)
        {

        }
        return Result;
    }

    public void write_ajaxvar(string varname, string value)
    {
        try
        {
            value = value.Trim();
            value = AddSlashes(value);
            System.Web.HttpContext.Current.Response.Write("var " + varname + " = \"" + value + "\"; \r\n");
        }
        catch (Exception Ex)
        {
            System.Web.HttpContext.Current.Response.Write("var " + varname + " = \"" + "\"; \r\n");
        }
    }

    public void chondiaphuong_script(string id, int mode = 0)
    {
        string output_html = @"
var timestamp = Number(new Date());

$('#TinhID_" + id + @"').change(function() {		 
     $('#HuyenID_" + id + @"').html(''); 
     $('#HuyenID_" + id + @"').load('noframe.aspx?page=chondiaphuong&default=0&type=qh&id_thanhpho='+$('#TinhID_" + id + @"').val()+'&time='+timestamp);
     $('#XaID_" + id + @"').html(''); 
     $('#PhoID_" + id + @"').html(''); 
});

$('#HuyenID_" + id + @"').change(function() {	 
     $('#XaID_" + id + @"').html(''); 
     $('#PhoID_" + id + @"').html(''); 
     $('#XaID_" + id + @"').load('noframe.aspx?page=chondiaphuong&default=0&type=px&id_quanhuyen='+$('#HuyenID_" + id + @"').val()+'&time='+timestamp);
$('#PhoID_" + id + @"').html(''); 
     $('#PhoID_" + id + @"').load('noframe.aspx?page=chondiaphuong&default=0&type=p&id_phuongxa='+$('#HuyenID_" + id + @"').val()+'&time='+timestamp);
});


    ";

        if (mode == 1)

            output_html = @"

var timestamp = Number(new Date());

$('#" + id + @"_TinhID').change(function() {		 
     $('#" + id + @"_HuyenID').html(''); 
     $('#" + id + @"_HuyenID').load('noframe.aspx?page=chondiaphuong&default=0&type=qh&id_thanhpho='+$('#" + id + @"_TinhID').val()+'&time='+timestamp);
     $('#" + id + @"_XaID').html(''); 
     $('#" + id + @"_PhoID').html(''); 
});

$('#" + id + @"_HuyenID').change(function() {	 
     $('#" + id + @"_XaID').html(''); 
     $('#" + id + @"_PhoID').html(''); 
     $('#" + id + @"_XaID').load('noframe.aspx?page=chondiaphuong&default=0&type=px&id_quanhuyen='+$('#" + id + @"_HuyenID').val()+'&time='+timestamp);	 
$('#" + id + @"_PhoID').html(''); 
     $('#" + id + @"_PhoID').load('noframe.aspx?page=chondiaphuong&default=0&type=p&id_phuongxa='+$('#" + id + @"_HuyenID').val()+'&time='+timestamp);
});


    ";

        if (mode == 2)

            output_html = @"

var timestamp = Number(new Date());

$('#TinhID').change(function() {		 
     $('#HuyenID').html(''); 
     $('#HuyenID').load('noframe.aspx?page=chondiaphuong&default=0&type=qh&id_thanhpho='+$('#TinhID').val()+'&time='+timestamp);
     $('#XaID').html(''); 
     $('#PhoID').html(''); 
});

$('#HuyenID').change(function() {	 
     $('#XaID').html(''); 
     $('#PhoID').html(''); 
     $('#XaID').load('noframe.aspx?page=chondiaphuong&default=0&type=px&id_quanhuyen='+$('#HuyenID').val()+'&time='+timestamp);	 
     $('#PhoID').html(''); 
     $('#PhoID').load('noframe.aspx?page=chondiaphuong&default=0&type=p&id_phuongxa='+$('#HuyenID').val()+'&time='+timestamp);	 
});



    ";

        System.Web.HttpContext.Current.Response.Write(output_html);

    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ORDER BY TenTinh ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim() == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void Load_QuanHuyen(string MaHuyen = "000", string MaTinh = "00")
    {
        string output_html = "";

        DataSet ds = new DataSet();


        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaHuyen,TenHuyen FROM tblDMHuyen WHERE (MaTinh='" + MaTinh.Trim() + "') AND HieuLuc='1' ORDER BY TenHuyen ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        output_html += @"<option value=""""><< Lựa chọn >></option>";
        foreach (DataRow Huyen in dt.Rows)
        {



            if (MaHuyen.Trim() == Huyen["MaHuyen"].ToString().Trim())
            {
                output_html += @"
                     <option  selected=""selected"" value=""" + Huyen["MaHuyen"].ToString().Trim() + @""">" + Huyen["TenHuyen"] + @"</option>";
            }
            else
            {
                output_html += @"
                    <option  value=""" + Huyen["MaHuyen"].ToString().Trim() + @""">" + Huyen["TenHuyen"] + @"</option>";
            }


        }

        ds = null;
        dt = null;


        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void Load_PhuongXa(string MaXa = "00000", string MaHuyen = "000")
    {
        string output_html = "";


        DataSet ds = new DataSet();


        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaXa,TenXa FROM tblDMXa WHERE (MaHuyen='" + MaHuyen.Trim() + "') ORDER BY TenXa ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        output_html += @"<option value=""""><< Lựa chọn >></option>";
        foreach (DataRow Xa in dt.Rows)
        {

            if (MaXa.Trim() == Xa["MaXa"].ToString().Trim())
            {
                output_html += @"
                     <option  selected=""selected"" value=""" + Xa["MaXa"].ToString().Trim() + @""">" + Xa["TenXa"] + @"</option>";
            }
            else
            {
                output_html += @"
                    <option  value=""" + Xa["MaXa"].ToString().Trim() + @""">" + Xa["TenXa"] + @"</option>";
            }

        }


        ds = null;
        dt = null;


        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public string Call_WS_return_XML(string soapmsg = "")
    {
        string text = "";
        string ServicesURL = "";

        try
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ServicesURL);

            req.ContentType = "text/xml;charset=\"utf-8\"";
            req.Accept = "text/xml";
            req.Method = "POST";

            using (Stream stm = req.GetRequestStream())
            {
                using (StreamWriter stmw = new StreamWriter(stm))
                {
                    stmw.Write(soapmsg);
                }
            }

            WebResponse response = req.GetResponse();

            Stream responseStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(responseStream);
            text = reader.ReadToEnd();
        }
        catch (Exception Ex)
        {
            text = Ex.Message.ToString();
        }

        return text;
    }

    public string loadcauhinh(string Ma)
    {
        string giatri = "";
        try
        {
            DataTable dt = new DataTable();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT TOP 1 GiaTri FROM tblCauHinhHeThong WHERE MaCauHinh = '" + Ma + "'  ";
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];

            giatri = dt.Rows[0][0].ToString();

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;
        }
        catch
        {
            giatri = "";
        }
        return giatri;
    }

    public void ghilog(string machucnang = "", string log = "")
    {
        SqlNhatKyHeThongProvider NhatKy_provider = new SqlNhatKyHeThongProvider(connstr, true, "");

        NhatKyHeThong NhatKy = new NhatKyHeThong();
        if (!string.IsNullOrEmpty(Admin_TenDangNhap))
        {
            NhatKy.TenDangNhap = Admin_TenDangNhap;
            NhatKy.CaNhanToChuc = Admin_TenDangNhap;
        }
        else
        {
            NhatKy.TenDangNhap = Khachhang_KhachHangID;
            NhatKy.CaNhanToChuc = Khachhang_TenToChuc;
        }
        NhatKy.ChucNang = machucnang;
        NhatKy.HanhDong = log;
        NhatKy.ThoiGian = DateTime.Now;

        NhatKy_provider.Insert(NhatKy);

    }

    public void PheDuyetHoiVien(string DonHoiVienCaNhanID, string TinhTrangID, ref bool IsSendMail, string LyDoTuChoi = "") // TinhTrangID: 3-tiếp nhận, 2-từ chối, 4-từ chối duyệt, 5-phê duyệt;
    {

        SqlDonHoiVienCaNhanProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connstr, true, "");
        DonHoiVienCaNhan donhoivien = DonHoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(DonHoiVienCaNhanID));

        if (TinhTrangID == "3")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 3, NgayTiepNhan = '" + DateTime.Now.ToString("yyyy-MM-dd") + "', NguoiTiepNhan = N'" + Admin_HoVaTen + "' WHERE DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;

            DataAccess.RunActionCmd(cmd);
        }

        if (TinhTrangID == "2")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 2, LyDoTuChoi = N'" + LyDoTuChoi + "' WHERE DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;

            DataAccess.RunActionCmd(cmd);

            // Gửi mail

            string body = "";
            body += "Đơn xin kết nạp hội viên cá nhân của anh/chị không được tiếp nhận. Lý do:<BR/>";
            body += LyDoTuChoi;

            string msg = "";
            IsSendMail = SmtpMail.Send("BQT WEB VACPA", donhoivien.Email, "Yêu cầu sửa đổi, bổ sung hồ sơ tiếp nhận đơn xin kết nạp hội viên cá nhân", body, ref msg);
        }

        if (TinhTrangID == "4")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 4, LyDoTuChoi = N'" + LyDoTuChoi + "' WHERE DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;

            DataAccess.RunActionCmd(cmd);

            // Gửi mail

            string body = "";
            body += "Đơn xin kết nạp hội viên cá nhân của anh/chị không được phê duyệt. Lý do:<BR/>";
            body += LyDoTuChoi;

            string msg = "";
            IsSendMail = SmtpMail.Send("BQT WEB VACPA", donhoivien.Email, "Yêu cầu sửa đổi, bổ sung hồ sơ phê duyệt đơn xin kết nạp hội viên cá nhân", body, ref msg);
        }

        if (TinhTrangID == "5")
        {
            SqlDonHoiVienCaNhanChungChiProvider DonHoiVienCaNhanChungChi_provider = new SqlDonHoiVienCaNhanChungChiProvider(connstr, true, "");
            SqlDonHoiVienCaNhanQuaTrinhCongTacProvider DonHoiVienCaNhanQuaTrinh_provider = new SqlDonHoiVienCaNhanQuaTrinhCongTacProvider(connstr, true, "");
            SqlDonHoiVienFileProvider DonHoiVienFile_provider = new SqlDonHoiVienFileProvider(connstr, true, "");

            SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connstr, true, "");
            SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connstr, true, "");
            SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connstr, true, "");
            SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connstr, true, "");

            if (donhoivien.HoiVienCaNhanId != null)
            {
                #region người quan tâm và kiểm toán viên
                HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(donhoivien.HoiVienCaNhanId.Value);

                hoivien.LoaiHoiVienCaNhan = "1";
                hoivien.HoDem = donhoivien.HoDem;
                hoivien.Ten = donhoivien.Ten;
                hoivien.GioiTinh = donhoivien.GioiTinh;
                hoivien.NgaySinh = donhoivien.NgaySinh;
                hoivien.QuocTichId = donhoivien.QuocTichId;
                hoivien.QueQuanTinhId = donhoivien.QueQuanTinhId;
                hoivien.QueQuanHuyenId = donhoivien.QueQuanHuyenId;
                hoivien.QueQuanXaId = donhoivien.QueQuanXaId;
                hoivien.DiaChi = donhoivien.DiaChi;
                hoivien.DiaChiTinhId = donhoivien.DiaChiTinhId;
                hoivien.DiaChiHuyenId = donhoivien.DiaChiHuyenId;
                hoivien.DiaChiXaId = donhoivien.DiaChiXaId;
                hoivien.SoGiayChungNhanDkhn = donhoivien.SoGiayChungNhanDkhn;
                hoivien.NgayCapGiayChungNhanDkhn = donhoivien.NgayCapGiayChungNhanDkhn;
                hoivien.HanCapTu = donhoivien.HanCapTu;
                hoivien.HanCapDen = donhoivien.HanCapDen;
                hoivien.SoChungChiKtv = donhoivien.SoChungChiKtv;
                hoivien.NgayCapChungChiKtv = donhoivien.NgayCapChungChiKtv;
                hoivien.Email = donhoivien.Email;
                hoivien.DienThoai = donhoivien.DienThoai;
                hoivien.Mobile = donhoivien.Mobile;
                hoivien.ChucVuId = donhoivien.ChucVuId;
                hoivien.TruongDaiHocId = donhoivien.TruongDaiHocId;
                hoivien.ChuyenNganhDaoTaoId = donhoivien.ChuyenNganhDaoTaoId;
                hoivien.ChuyenNganhNam = donhoivien.ChuyenNganhNam;
                hoivien.HocViId = donhoivien.HocViId;
                hoivien.HocViNam = donhoivien.HocViNam;
                hoivien.HocHamId = donhoivien.HocHamId;
                hoivien.HocHamNam = donhoivien.HocHamNam;
                hoivien.SoCmnd = donhoivien.SoCmnd;
                hoivien.CmndNgayCap = donhoivien.CmndNgayCap;
                hoivien.CmndTinhId = donhoivien.CmndTinhId;
                hoivien.SoTaiKhoanNganHang = donhoivien.SoTaiKhoanNganHang;
                hoivien.NganHangId = donhoivien.NganHangId;
                hoivien.HoiVienTapTheId = donhoivien.HoiVienTapTheId;
                hoivien.DonViCongTac = donhoivien.DonViCongTac;
                hoivien.SoThichId = donhoivien.SoThichId;

                hoivien.NgayGiaNhap = DateTime.Now;
                hoivien.NgayDuyet = DateTime.Now;
                hoivien.NguoiCapNhat = Admin_HoVaTen;
                hoivien.NguoiDuyet = Admin_HoVaTen;
                hoivien.TinhTrangHoiVienId = 1;
                HoiVienCaNhan_provider.Update(hoivien);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE tblHoiVienCaNhanChungChiQuocTe WHERE HoiVienCaNhanID = " + donhoivien.HoiVienCaNhanId + ";";
                cmd.CommandText += "DELETE tblHoiVienCaNhanQuaTrinhCongTac WHERE HoiVienCaNhanID = " + donhoivien.HoiVienCaNhanId + ";";
                cmd.CommandText += "DELETE tblHoiVienFile WHERE HoiVienCaNhanID = " + donhoivien.HoiVienCaNhanId + ";";

                DataAccess.RunActionCmd(cmd);

                // insert chứng chỉ  
                TList<DonHoiVienCaNhanChungChi> chungchi_data;
                DonHoiVienCaNhanChungChiParameterBuilder filter_chungchi = new DonHoiVienCaNhanChungChiParameterBuilder();
                filter_chungchi.AppendEquals(DonHoiVienCaNhanChungChiColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                chungchi_data = DonHoiVienCaNhanChungChi_provider.Find(filter_chungchi);

                foreach (DonHoiVienCaNhanChungChi donchungchi in chungchi_data)
                {
                    HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                    chungchi.HoiVienCaNhanId = donhoivien.HoiVienCaNhanId.Value;
                    chungchi.SoChungChi = donchungchi.SoChungChi;
                    chungchi.NgayCap = donchungchi.NgayCap;
                    HoiVienCaNhanChungChi_provider.Insert(chungchi);
                }

                // insert quá trình làm việc
                TList<DonHoiVienCaNhanQuaTrinhCongTac> quatrinh_data;
                DonHoiVienCaNhanQuaTrinhCongTacParameterBuilder filter_quatrinh = new DonHoiVienCaNhanQuaTrinhCongTacParameterBuilder();
                filter_quatrinh.AppendEquals(DonHoiVienCaNhanQuaTrinhCongTacColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                quatrinh_data = DonHoiVienCaNhanQuaTrinh_provider.Find(filter_quatrinh);

                foreach (DonHoiVienCaNhanQuaTrinhCongTac donquatrinh in quatrinh_data)
                {
                    HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
                    quatrinh.HoiVienCaNhanId = donhoivien.HoiVienCaNhanId.Value;
                    quatrinh.ThoiGianCongTac = donquatrinh.ThoiGianCongTac;
                    quatrinh.ChucVu = donquatrinh.ChucVu;
                    quatrinh.NoiLamViec = donquatrinh.NoiLamViec;

                    HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
                }

                // file đính kèm
                TList<DonHoiVienFile> file_data;
                DonHoiVienFileParameterBuilder filter_file = new DonHoiVienFileParameterBuilder();
                filter_file.AppendEquals(DonHoiVienFileColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                file_data = DonHoiVienFile_provider.Find(filter_file);

                foreach (DonHoiVienFile donfile in file_data)
                {
                    HoiVienFile file = new HoiVienFile();
                    file.HoiVienCaNhanId = donhoivien.HoiVienCaNhanId.Value;
                    file.LoaiGiayToId = donfile.LoaiGiayToId;
                    file.TenFile = donfile.TenFile;
                    file.FileDinhKem = donfile.FileDinhKem;

                    HoiVienFile_provider.Insert(file);
                }


                // update vào bảng tblNguoiDung
                cmd = new SqlCommand();
                cmd.CommandText = "SELECT NguoiDungID FROM tblNguoiDung WHERE HoiVienId = " + donhoivien.HoiVienCaNhanId.Value;
                string NguoiDungID = DataAccess.DLookup(cmd);

                SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connstr, true, "");

                NguoiDung new_user = NguoiDung_provider.GetByNguoiDungId(int.Parse(NguoiDungID));
                new_user.HoVaTen = donhoivien.HoDem + " " + donhoivien.Ten;
                new_user.NgaySinh = donhoivien.NgaySinh;

                new_user.GioiTinh = donhoivien.GioiTinh;
                new_user.Email = donhoivien.Email;
                new_user.DienThoai = donhoivien.DienThoai;
                new_user.SoCmnd = donhoivien.SoCmnd;
                new_user.CauHoiBiMatId = donhoivien.CauHoiBiMatId;
                new_user.CauTraLoiBiMat = donhoivien.CauTraLoiBiMat;
                new_user.LoaiHoiVien = "1";

                NguoiDung_provider.Update(new_user);

                donhoivien.TinhTrangId = 5;
                DonHoiVienCaNhan_provider.Update(donhoivien);

                // Send mail
                string body = "";
                body += "Anh/chị đã được phê duyệt kết nạp hội viên cá nhân, có thể sử dụng tài khoản để đăng nhập vào phần mềm với tư cách là hội viên cá nhân";

                string msg = "";
                IsSendMail = SmtpMail.Send("BQT WEB VACPA", donhoivien.Email, "Thông tin tài khoản hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg);

                #endregion
            }
            else
            {
                #region hội viên mới

                // tạo tài khoản nếu chưa có
                string HoiVienID = SinhID(donhoivien.HoDem, donhoivien.Ten, donhoivien.SoChungChiKtv);

                HoiVienCaNhan hoivien = new HoiVienCaNhan();
                hoivien.LoaiHoiVienCaNhan = "1";
                hoivien.MaHoiVienCaNhan = HoiVienID;
                hoivien.HoDem = donhoivien.HoDem;
                hoivien.Ten = donhoivien.Ten;
                hoivien.GioiTinh = donhoivien.GioiTinh;
                hoivien.NgaySinh = donhoivien.NgaySinh;
                hoivien.QuocTichId = donhoivien.QuocTichId;
                hoivien.QueQuanTinhId = donhoivien.QueQuanTinhId;
                hoivien.QueQuanHuyenId = donhoivien.QueQuanHuyenId;
                hoivien.QueQuanXaId = donhoivien.QueQuanXaId;
                hoivien.DiaChi = donhoivien.DiaChi;
                hoivien.DiaChiTinhId = donhoivien.DiaChiTinhId;
                hoivien.DiaChiHuyenId = donhoivien.DiaChiHuyenId;
                hoivien.DiaChiXaId = donhoivien.DiaChiXaId;
                hoivien.SoGiayChungNhanDkhn = donhoivien.SoGiayChungNhanDkhn;
                hoivien.NgayCapGiayChungNhanDkhn = donhoivien.NgayCapGiayChungNhanDkhn;
                hoivien.HanCapTu = donhoivien.HanCapTu;
                hoivien.HanCapDen = donhoivien.HanCapDen;
                hoivien.SoChungChiKtv = donhoivien.SoChungChiKtv;
                hoivien.NgayCapChungChiKtv = donhoivien.NgayCapChungChiKtv;
                hoivien.Email = donhoivien.Email;
                hoivien.DienThoai = donhoivien.DienThoai;
                hoivien.Mobile = donhoivien.Mobile;
                hoivien.ChucVuId = donhoivien.ChucVuId;
                hoivien.TruongDaiHocId = donhoivien.TruongDaiHocId;
                hoivien.ChuyenNganhDaoTaoId = donhoivien.ChuyenNganhDaoTaoId;
                hoivien.ChuyenNganhNam = donhoivien.ChuyenNganhNam;
                hoivien.HocViId = donhoivien.HocViId;
                hoivien.HocViNam = donhoivien.HocViNam;
                hoivien.HocHamId = donhoivien.HocHamId;
                hoivien.HocHamNam = donhoivien.HocHamNam;
                hoivien.SoCmnd = donhoivien.SoCmnd;
                hoivien.CmndNgayCap = donhoivien.CmndNgayCap;
                hoivien.CmndTinhId = donhoivien.CmndTinhId;
                hoivien.SoTaiKhoanNganHang = donhoivien.SoTaiKhoanNganHang;
                hoivien.NganHangId = donhoivien.NganHangId;
                hoivien.HoiVienTapTheId = donhoivien.HoiVienTapTheId;
                hoivien.DonViCongTac = donhoivien.DonViCongTac;
                hoivien.SoThichId = donhoivien.SoThichId;

                hoivien.NgayGiaNhap = DateTime.Now;
                hoivien.NgayDuyet = DateTime.Now;
                hoivien.NguoiCapNhat = Admin_HoVaTen;
                hoivien.NguoiDuyet = Admin_HoVaTen;
                hoivien.TinhTrangHoiVienId = 1;
                HoiVienCaNhan_provider.Insert(hoivien);

                // insert chứng chỉ  
                TList<DonHoiVienCaNhanChungChi> chungchi_data;
                DonHoiVienCaNhanChungChiParameterBuilder filter_chungchi = new DonHoiVienCaNhanChungChiParameterBuilder();
                filter_chungchi.AppendEquals(DonHoiVienCaNhanChungChiColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                chungchi_data = DonHoiVienCaNhanChungChi_provider.Find(filter_chungchi);

                foreach (DonHoiVienCaNhanChungChi donchungchi in chungchi_data)
                {
                    HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                    chungchi.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
                    chungchi.SoChungChi = donchungchi.SoChungChi;
                    chungchi.NgayCap = donchungchi.NgayCap;
                    HoiVienCaNhanChungChi_provider.Insert(chungchi);
                }

                // insert quá trình làm việc
                TList<DonHoiVienCaNhanQuaTrinhCongTac> quatrinh_data;
                DonHoiVienCaNhanQuaTrinhCongTacParameterBuilder filter_quatrinh = new DonHoiVienCaNhanQuaTrinhCongTacParameterBuilder();
                filter_quatrinh.AppendEquals(DonHoiVienCaNhanQuaTrinhCongTacColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                quatrinh_data = DonHoiVienCaNhanQuaTrinh_provider.Find(filter_quatrinh);

                foreach (DonHoiVienCaNhanQuaTrinhCongTac donquatrinh in quatrinh_data)
                {
                    HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
                    quatrinh.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
                    quatrinh.ThoiGianCongTac = donquatrinh.ThoiGianCongTac;
                    quatrinh.ChucVu = donquatrinh.ChucVu;
                    quatrinh.NoiLamViec = donquatrinh.NoiLamViec;

                    HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
                }

                // file đính kèm
                TList<DonHoiVienFile> file_data;
                DonHoiVienFileParameterBuilder filter_file = new DonHoiVienFileParameterBuilder();
                filter_file.AppendEquals(DonHoiVienFileColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                file_data = DonHoiVienFile_provider.Find(filter_file);

                foreach (DonHoiVienFile donfile in file_data)
                {
                    HoiVienFile file = new HoiVienFile();
                    file.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
                    file.LoaiGiayToId = donfile.LoaiGiayToId;
                    file.TenFile = donfile.TenFile;
                    file.FileDinhKem = donfile.FileDinhKem;

                    HoiVienFile_provider.Insert(file);
                }

                // insert vào bảng tblNguoiDung
                SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connstr, true, "");

                NguoiDung new_user = new NguoiDung();
                new_user.HoVaTen = donhoivien.HoDem + " " + donhoivien.Ten;
                new_user.NgaySinh = donhoivien.NgaySinh;
                new_user.GioiTinh = donhoivien.GioiTinh;
                new_user.Email = donhoivien.Email;
                new_user.DienThoai = donhoivien.DienThoai;

                new_user.TenDangNhap = HoiVienID;

                Random random = new Random();
                int randomPassword = random.Next(10000000, 99999999);

                new_user.MatKhau = randomPassword.ToString();
                new_user.LoaiHoiVien = "1";
                new_user.HoiVienId = hoivien.HoiVienCaNhanId;

                NguoiDung_provider.Insert(new_user);

                donhoivien.TinhTrangId = 5;
                donhoivien.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;

                DonHoiVienCaNhan_provider.Update(donhoivien);

                // Send mail
                string body = "";
                //body += "Anh/chị đã được phê duyệt kết nạp hội viên cá nhân, có thể sử dụng tài khoản sau để đăng nhập vào phần mềm theo đường link " + login_link + " :<BR/>";
                //body += "Tên đăng nhập: <B>" + HoiVienID + "</B><BR/>";
                //body += "Mật khẩu: <B>" + randomPassword + "</B><BR/>";
                //body += "Anh/chị vui lòng đăng nhập trong vòng 60 ngày, sau thời hạn trên anh/chị vui lòng liên hệ với VACPA để cấp lại tài khoản.<BR/>";

                body += "Thân chào Anh/chị <span style=\"color:red\">" + new_user.HoVaTen + "</span><BR/>";
                body += "Ban quản trị xin thông báo tài khoản đăng nhập phần mềm quản lý Hội viên VACPA như sau:<BR/>";
                body += "------ID đăng nhập------<BR/>";
                body += "Tên đăng nhập: " + HoiVienID + "<BR/>";
                body += "Mật khẩu: " + randomPassword + "<BR/>";
                body += "<span style=\"text-decoration:underline;font-weight:bold\">Lưu ý:</span><BR/>";
                body += " - Link đăng nhập phần mềm quản lý hội viên: http://vacpa.org.vn/Page/Login.aspx" + "<BR/>";
                body += " - Link xin cấp lại mật khẩu: http://vacpa.org.vn/Page/GetPassword.aspx" + "<BR/>";
                body += " - Ngay sau khi đăng nhập Anh/Chị hãy đổi mật khẩu truy cập." + "<BR/>";
                body += " - Để đảm bảo được nhận các thông báo thường xuyên với VACPA, Anh/Chị nhớ cập nhật những thông tin về sự thay đổi công việc, đơn vị công tác, số điện thoại di động, email, nơi thường trú, v.v… qua profile cá nhân." + "<BR/>";
                
                body += "━━━━━━━━━━━━━━━<BR/>";
                body += "Trân trọng,<BR/>";
                body += "Hội kiểm toán viên hành nghề Việt Nam (VACPA)<BR/>";
                body += "Địa chỉ: Phòng 304, Nhà Dự án, Số 4, Ngõ Hàng Chuối I, Hà Nội<BR/>";
                body += "Email: quantriweb@vacpa.org.vn";

                string msg = "";
                IsSendMail = SmtpMail.Send("BQT WEB VACPA", donhoivien.Email, "Thông tin tài khoản hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg);

                #endregion
            }
        }

    }

    public void XoaDonXinKetNapHoiVienCaNhan(string DonHoiVienCaNhanID)
    {
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd_delete = new SqlCommand();
        string[] lstID = DonHoiVienCaNhanID.Split(',');
        foreach (string ID in lstID)
        {
            //cmd.CommandText = "SELECT HinhThucNop FROM tblDonHoiVienCaNhan WHERE DonHoiVienCaNhanID = " + ID;
            //string HinhThucNop = DataAccess.DLookup(cmd);
            //if (HinhThucNop == "2") // nộp trực tiếp mới được xóa
            //{
            if (Admin_TenDangNhap == "dinhsinhhuan")
            {
                cmd_delete.CommandText = "DELETE tblDonHoiVienCaNhanChungChi WHERE DonHoiVienCaNhanID = " + ID + ";";
                cmd_delete.CommandText += "DELETE tblDonHoiVienCaNhanQuaTrinhCongTac WHERE DonHoiVienCaNhanID = " + ID + ";";
                cmd_delete.CommandText += "DELETE tblDonHoiVienFile WHERE DonHoiVienCaNhanID = " + ID + ";";
                cmd_delete.CommandText += "DELETE tblDonHoiVienCaNhan WHERE DonHoiVienCaNhanID = " + ID + ";";
            }
            //}
        }
        if (!string.IsNullOrEmpty(cmd_delete.CommandText))
            DataAccess.RunActionCmd(cmd_delete);
    }

    public string SinhID(string HoDem, string Ten, string SoChungChiKTV)
    {
        string result = "";

        string sql = "SELECT dbo.BoDau(N'" + Ten + "')";
        SqlCommand cmd = new SqlCommand(sql);
        string ten = DataAccess.DLookup(cmd);

        cmd.CommandText = "SELECT dbo.BoDau(N'" + HoDem + "')";
        string hodem = DataAccess.DLookup(cmd);

        string[] array = hodem.Split(' ');
        hodem = "";
        foreach (string s in array)
        {
            if (!string.IsNullOrEmpty(s))
                hodem += s.Substring(0, 1).ToLower();
        }

        result = ten + hodem;

        for (int i = 0; i < SoChungChiKTV.Length; i++)
        {
            if (Regex.IsMatch(SoChungChiKTV.Substring(i, 1), @"^[0-9]\d*\.?[0]*$"))
                result += SoChungChiKTV.Substring(i, 1);
        }

        return result;
    }

    public void PheDuyetYeuCauCapNhatHoiVienCaNhan(string CapNhatYeuCauID, string TinhTrangID, string LyDoTuChoi = "") // TinhTrangID: 4-từ chối; 5-phê duyệt
    {
        SqlCapNhatHoSoHvcnProvider CapNhat_HoiVienCaNhan_provider = new SqlCapNhatHoSoHvcnProvider(connstr, true, "");
        CapNhatHoSoHvcn capnhat = CapNhat_HoiVienCaNhan_provider.GetByCapNhatHoSoHvcnid(int.Parse(CapNhatYeuCauID));

        SqlCapNhatHoSoHvcnChungChiProvider CapNhatHoiVienCaNhanChungChi_provider = new SqlCapNhatHoSoHvcnChungChiProvider(connstr, true, "");
        SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connstr, true, "");
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connstr, true, "");
        HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(capnhat.HoiVienCaNhanId.Value);

        if (TinhTrangID == "4")
        {
            capnhat.TinhTrangId = 4;
            capnhat.LyDoTuChoi = LyDoTuChoi;
            CapNhat_HoiVienCaNhan_provider.Update(capnhat);

            GuiThongBao(capnhat.HoiVienCaNhanId.Value.ToString(), "1", "Từ chối phê duyệt yêu cầu cập nhật thông tin", LyDoTuChoi);
            ghilog("CapNhatHoSoHoiVien", Admin_TenDangNhap + " " + "Từ chối phê duyệt yêu cầu cập nhật thông tin hội viên " + hoivien.HoDem + " " + hoivien.Ten);
            // Send mail
            string body = "";
            body += "Yêu cầu cập nhật thông tin của anh/chị không được phê duyệt, lý do: " + LyDoTuChoi;

            string msg = "";
            SmtpMail.Send("BQT WEB VACPA", hoivien.Email, "Từ chối phê duyệt yêu cầu cập nhật thông tin", body, ref msg);
        }

        if (TinhTrangID == "5")
        {
            if (!string.IsNullOrEmpty(capnhat.SoGiayChungNhanDkhn))
                hoivien.SoGiayChungNhanDkhn = capnhat.SoGiayChungNhanDkhn;
            if (capnhat.NgayCapGiayChungNhanDkhn != null)
                hoivien.NgayCapGiayChungNhanDkhn = capnhat.NgayCapGiayChungNhanDkhn;
            if (capnhat.HanCapTu != null)
                hoivien.HanCapTu = capnhat.HanCapTu;
            if (capnhat.HanCapDen != null)
                hoivien.HanCapDen = capnhat.HanCapDen;
            if (capnhat.ChucVuId != null)
                hoivien.ChucVuId = capnhat.ChucVuId;
            if (capnhat.HocViId != null)
                hoivien.HocViId = capnhat.HocViId;
            if (!string.IsNullOrEmpty(capnhat.HocViNam))
                hoivien.HocViNam = capnhat.HocViNam;
            if (capnhat.HocHamId != null)
                hoivien.HocHamId = capnhat.HocHamId;
            if (!string.IsNullOrEmpty(capnhat.HocHamNam))
                hoivien.HocHamNam = capnhat.HocHamNam;

            HoiVienCaNhan_provider.Update(hoivien);

            capnhat.NgayDuyet = DateTime.Now;
            capnhat.NguoiDuyet = Admin_HoVaTen;
            capnhat.TinhTrangId = 5;
            CapNhat_HoiVienCaNhan_provider.Update(capnhat);

            string sql = "";
            SqlCommand cmd;
            // insert chứng chỉ
            
            TList<CapNhatHoSoHvcnChungChi> chungchi_data;
            CapNhatHoSoHvcnChungChiParameterBuilder filter_chungchi = new CapNhatHoSoHvcnChungChiParameterBuilder();
            filter_chungchi.AppendEquals(CapNhatHoSoHvcnChungChiColumn.CapNhatHoSoHvcnid, CapNhatYeuCauID);
            chungchi_data = CapNhatHoiVienCaNhanChungChi_provider.Find(filter_chungchi);

            if (chungchi_data.Count != 0)
            {
                sql = "DELETE tblHoiVienCaNhanChungChiQuocTe WHERE HoiVienCaNhanId = " + capnhat.HoiVienCaNhanId.Value;
                cmd = new SqlCommand(sql);
                DataAccess.RunActionCmd(cmd);

                foreach (CapNhatHoSoHvcnChungChi donchungchi in chungchi_data)
                {
                    HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                    chungchi.HoiVienCaNhanId = capnhat.HoiVienCaNhanId.Value;
                    chungchi.SoChungChi = donchungchi.SoChungChi;
                    chungchi.NgayCap = donchungchi.NgayCap;
                    HoiVienCaNhanChungChi_provider.Insert(chungchi);
                }
            }

            // insert khen thưởng
            
            sql = "SELECT HinhThucKhenThuongID, LyDo, CONVERT(VARCHAR, NgayThang, 103) AS NgayThang FROM tblCapNhatHoSoHVCNKhenThuongKyLuat WHERE Loai = 1 AND CapNhatHoSoHVCNID = " + CapNhatYeuCauID;
            cmd = new SqlCommand(sql);

            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
            if (dtb.Rows.Count != 0)
            {
                sql = "DELETE tblKhenThuongKyLuat WHERE Loai = 1 AND HoiVienCaNhanId = " + capnhat.HoiVienCaNhanId.Value;
                cmd = new SqlCommand(sql);
                DataAccess.RunActionCmd(cmd);

                sql = "";
                foreach (DataRow row in dtb.Rows)
                {
                    sql += "INSERT INTO tblKhenThuongKyLuat (HinhThucKhenThuongID, HoiVienCaNhanID, Loai, NgayThang, LyDo) VALUES (";
                    sql += row["HinhThucKhenThuongID"].ToString() + ", ";
                    sql += capnhat.HoiVienCaNhanId.Value + ", '1', ";
                    if (!string.IsNullOrEmpty(row["NgayThang"].ToString()))
                        sql += "'" + DateTime.ParseExact(row["NgayThang"].ToString(), "dd/MM/yyyy", null).ToString("yyyy-MM-dd") + "', ";
                    sql += "N'" + row["LyDo"].ToString() + "');";
                }
                cmd = new SqlCommand(sql);
                DataAccess.RunActionCmd(cmd);
            }
            dtb.Clear();

            // insert kỷ luật
            
            sql = "SELECT HinhThucKyLuatID, LyDo, CONVERT(VARCHAR, NgayThang, 103) AS NgayThang FROM tblCapNhatHoSoHVCNKhenThuongKyLuat WHERE Loai = 2 AND CapNhatHoSoHVCNID = " + CapNhatYeuCauID;
            cmd = new SqlCommand(sql);

            dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
            if (dtb.Rows.Count != 0)
            {
                sql = "DELETE tblKhenThuongKyLuat WHERE Loai = 2 AND HoiVienCaNhanId = " + capnhat.HoiVienCaNhanId.Value;
                cmd = new SqlCommand(sql);
                DataAccess.RunActionCmd(cmd);

                foreach (DataRow row in dtb.Rows)
                {
                    sql += "INSERT INTO tblKhenThuongKyLuat (HinhThucKyLuatID, HoiVienCaNhanID, Loai, NgayThang, LyDo) VALUES (";
                    sql += row["HinhThucKyLuatID"].ToString() + ", ";
                    sql += capnhat.HoiVienCaNhanId.Value + ", '2', ";
                    if (!string.IsNullOrEmpty(row["NgayThang"].ToString()))
                        sql += "'" + DateTime.ParseExact(row["NgayThang"].ToString(), "dd/MM/yyyy", null).ToString("yyyy-MM-dd") + "', ";
                    sql += "N'" + row["LyDo"].ToString() + "');";
                }
                cmd = new SqlCommand(sql);
                DataAccess.RunActionCmd(cmd);
            }
            // Gửi thông báo
            GuiThongBao(capnhat.HoiVienCaNhanId.Value.ToString(), "1", "Phê duyệt yêu cầu cập nhật thông tin", "Yêu cầu cập nhật thông tin (Mã yêu cầu : " + capnhat.MaYeuCau + ") của bạn đã được phê duyệt");
            ghilog("CapNhatHoSoHoiVien", Admin_TenDangNhap + " " + "Phê duyệt yêu cầu cập nhật thông tin hội viên " + hoivien.HoDem + " " + hoivien.Ten);
            // Send mail
            string body = "";
            body += "Yêu cầu cập nhật thông tin của anh/chị đã được phê duyệt (Mã yêu cầu : " + capnhat.MaYeuCau + ")";

            string msg = "";
            SmtpMail.Send("BQT WEB VACPA", hoivien.Email, "Phê duyệt yêu cầu cập nhật thông tin", body, ref msg);
        }
    }

    public void XoaYeuCauCapNhatHoiVienCaNhan(string CapNhatYeuCauID)
    {
        SqlCommand cmd = new SqlCommand();

        string[] lstID = CapNhatYeuCauID.Split(',');
        foreach (string ID in lstID)
        {
            cmd.CommandText = "DELETE tblCapNhatHoSoHVCNChungChi WHERE CapNhatHoSoHVCNID = " + ID + ";";
            cmd.CommandText += "DELETE tblCapNhatHoSoHVCNKhenThuongKyLuat WHERE CapNhatHoSoHVCNID = " + ID + ";";
            cmd.CommandText += "DELETE tblCapNhatHoSoHVCN WHERE CapNhatHoSoHVCNID = " + ID + ";";
        }

        DataAccess.RunActionCmd(cmd);
    }

    public void GuiThongBao(string HoiVienID, string LoaiHoiVien, string TieuDe, string NoiDung)
    {
        SqlThongBaoProvider thongbao_provider = new SqlThongBaoProvider(connstr, true, "");

        VACPA.Entities.ThongBao thongbao = new VACPA.Entities.ThongBao();
        thongbao.HoiVienIdNhan = HoiVienID + ","; 
        thongbao.LoaiHoiVien = LoaiHoiVien;
        thongbao.NgayThang = DateTime.Now;
        thongbao.TieuDe = TieuDe;
        thongbao.NoiDung = NoiDung;

        thongbao_provider.Insert(thongbao);
    }

    public void GuiThongBaoVoiFileDinhKem(string HoiVienID, string LoaiHoiVien, string TieuDe, string NoiDung, byte[] datainput1, string TenFileDinhKem)
    {
        SqlThongBaoProvider thongbao_provider = new SqlThongBaoProvider(connstr, true, "");
        SqlThongBaoFileProvider file_provider = new SqlThongBaoFileProvider(connstr, true, "");

        VACPA.Entities.ThongBao thongbao = new VACPA.Entities.ThongBao();
        thongbao.HoiVienIdNhan = HoiVienID + ",";
        thongbao.LoaiHoiVien = LoaiHoiVien;
        thongbao.NgayThang = DateTime.Now;
        thongbao.TieuDe = TieuDe;
        thongbao.NoiDung = NoiDung;

        thongbao_provider.Insert(thongbao);

        ThongBaoFile file = new ThongBaoFile();
        file.ThongBaoId = thongbao.ThongBaoId;
        file.File = datainput1;
        file.TenFile = TenFileDinhKem;

        file_provider.Insert(file);
    }

    public string RemoveChar(string value)
    {
        string result = "";
        try
        {
            result = value.Replace("%", "").Replace("'", "").Replace(";", "").Replace("=", "").Replace("+", "").Replace("-", "").Replace("*", "").Replace("select", "").Replace("update", "").Replace("insert", "").Replace(" or ", "");
        }
        catch
        { }
        return result;
    }

    public string MD5(string password)
    {
        byte[] textBytes = System.Text.Encoding.Default.GetBytes(password);
        try
        {
            System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
            cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hash = cryptHandler.ComputeHash(textBytes);
            string ret = "";
            foreach (byte a in hash)
            {
                if (a < 16)
                    ret += "0" + a.ToString("x");
                else
                    ret += a.ToString("x");
            }
            return ret;
        }
        catch
        {
            throw;
        }
    }
}