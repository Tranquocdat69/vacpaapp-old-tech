﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using System.Net.Mime;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Lấy ID trạng thái theo mã trạng thái
    /// </summary>
    /// <param name="status">Mã trạng thái</param>
    /// <param name="db">Biến khởi tạo cho class DB đã mở connection</param>
    /// <returns></returns>
    public string GetStatusID(string status, Db db)
    {
        string query = "SELECT TrangThaiID FROM " + ListName.Table_DMTrangThai + " WHERE MaTrangThai = " + status;
        List<Hashtable> listData = db.GetListData(query);
        if (listData.Count > 0)
            return listData[0]["TrangThaiID"].ToString().Trim();
        return "";
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Lấy tên trạng thái theo ID trạng thái
    /// </summary>
    /// <param name="idTrangThai">ID trạng thái</param>
    /// <param name="db">Biến khởi tạo cho class DB đã mở connection</param>
    /// <returns></returns>
    public string GetStatusNameById(string idTrangThai, Db db)
    {
        string query = "SELECT TenTrangThai FROM " + ListName.Table_DMTrangThai + " WHERE TrangThaiID = " + idTrangThai;
        List<Hashtable> listData = db.GetListData(query);
        if (listData.Count > 0)
            return listData[0]["TenTrangThai"].ToString().Trim();
        return "";
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Lấy mã trạng thái theo ID trạng thái
    /// </summary>
    /// <param name="idTrangThai">ID trạng thái</param>
    /// <param name="db">Biến khởi tạo cho class DB đã mở connection</param>
    /// <returns></returns>
    public string GetStatusCodeById(string idTrangThai, Db db)
    {
        string query = "SELECT MaTrangThai FROM " + ListName.Table_DMTrangThai + " WHERE TrangThaiID = " + idTrangThai;
        List<Hashtable> listData = db.GetListData(query);
        if (listData.Count > 0)
            return listData[0]["MaTrangThai"].ToString().Trim();
        return "";
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Gửi email
    /// </summary>
    /// <returns></returns>
    public bool SendEmail(List<string> listMailReceive, string subject, string content)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["smtp_FromAddress"]);
            mail.IsBodyHtml = true;
            foreach (string mailAddress in listMailReceive)
            {
                if (!string.IsNullOrEmpty(mailAddress))
                    mail.To.Add(new MailAddress(mailAddress));
            }
            mail.Subject = subject;
            mail.Body = content;

            SmtpClient SmtpServer = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["smtp_Host"]);
            SmtpServer.Port = Library.Int32Convert(System.Configuration.ConfigurationManager.AppSettings["smtp_Port"]);
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtp_Username"], System.Configuration.ConfigurationManager.AppSettings["smtp_Password"]);
            // Nếu dùng Gmail thì để True
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);
            return true;
        }
        catch
        {
            return false;
        }
        return false;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Gửi email
    /// </summary>
    /// <returns></returns>
    public bool SendEmail(List<string> listMailReceive, List<string> listSubject, List<string> listContent)
    {
        try
        {
            for (int i = 0; i < listMailReceive.Count; i++)
            {
                if (!string.IsNullOrEmpty(listMailReceive[i]))
                {
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["smtp_FromAddress"]);
                    mail.IsBodyHtml = true;

                    mail.To.Add(new MailAddress(listMailReceive[i]));
                    mail.Subject = listSubject[i];
                    mail.Body = listContent[i];

                    SmtpClient SmtpServer = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["smtp_Host"]);
                    SmtpServer.Port = Library.Int32Convert(System.Configuration.ConfigurationManager.AppSettings["smtp_Port"]);
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtp_Username"], System.Configuration.ConfigurationManager.AppSettings["smtp_Password"]);
                    // Nếu dùng Gmail thì để True
                    SmtpServer.EnableSsl = false;

                    SmtpServer.Send(mail);
                }
            }
            return true;
        }
        catch
        {
            return false;
        }
        return false;
    }

    // SonNQ
    public bool SendEmail_CNKT(List<string> listMailReceive, List<string> listSubject, List<string> listContent)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["smtp_FromAddress"]);
            mail.IsBodyHtml = true;

            for (int i = 0; i < listMailReceive.Count; i++)
            {
                if (!string.IsNullOrEmpty(listMailReceive[i]))
                {                   
                    mail.To.Add(new MailAddress(listMailReceive[i]));
                    mail.Subject = listSubject[i];
                    mail.Body = listContent[i];                   
                }
            }

            SmtpClient SmtpServer = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["smtp_Host"]);
            SmtpServer.Port = Library.Int32Convert(System.Configuration.ConfigurationManager.AppSettings["smtp_Port"]);
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtp_Username"], System.Configuration.ConfigurationManager.AppSettings["smtp_Password"]);
            // Nếu dùng Gmail thì để True
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);

            return true;
        }
        catch
        {
            return false;
        }
        return false;
    }

    public bool SendEmailWithAttach(List<string> listMailReceive, List<string> listSubject, List<string> listContent, Attachment Data)
    {
        try
        {
            for (int i = 0; i < listMailReceive.Count; i++)
            {
                if (!string.IsNullOrEmpty(listMailReceive[i]))
                {
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["smtp_FromAddress"]);
                    mail.IsBodyHtml = true;

                    mail.To.Add(new MailAddress(listMailReceive[i]));
                    mail.Subject = listSubject[i];
                    mail.Body = listContent[i];
                    mail.Attachments.Add(Data);

                    SmtpClient SmtpServer = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["smtp_Host"]);
                    SmtpServer.Port = Library.Int32Convert(System.Configuration.ConfigurationManager.AppSettings["smtp_Port"]);
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtp_Username"], System.Configuration.ConfigurationManager.AppSettings["smtp_Password"]);
                    // Nếu dùng Gmail thì để True
                    SmtpServer.EnableSsl = false;

                    SmtpServer.Send(mail);
                }
            }
            return true;
        }
        catch
        {
            return false;
        }
        return false;
    }

    public bool SendWithImage(List<string> listMailReceive, List<string> listSubject, List<string> listContent, string ImageURL)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["smtp_FromAddress"]);
            mail.IsBodyHtml = true;

            for (int i = 0; i < listMailReceive.Count; i++)
            {
                if (!string.IsNullOrEmpty(listMailReceive[i]))
                {                  
                    mail.To.Add(new MailAddress(listMailReceive[i]));
                    mail.Subject = listSubject[i];
                    mail.Body = listContent[i];
                    mail.AlternateViews.Add(getEmbeddeImage(ImageURL));
                    
                }
            }

            SmtpClient SmtpServer = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["smtp_Host"]);
            SmtpServer.Port = Library.Int32Convert(System.Configuration.ConfigurationManager.AppSettings["smtp_Port"]);
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtp_Username"], System.Configuration.ConfigurationManager.AppSettings["smtp_Password"]);
            // Nếu dùng Gmail thì để True
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);

            return true;
        }
        catch
        {
            return false;
        }
        return false;
    }

    private static AlternateView getEmbeddeImage(string imageurl)
    {
        LinkedResource inline = new LinkedResource(imageurl);
        inline.ContentId = Guid.NewGuid().ToString();
        string htmlBody = @"<img style='width:100%;' src='cid:" + inline.ContentId + @"'/>";
        AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
        alternateView.LinkedResources.Add(inline);
        return alternateView;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/04/13
    /// Get gia tri tham so bang code
    /// </summary>
    /// <param name="code">Code cua tham so</param>
    /// <param name="db">Bien khoi tao cho Class Db()</param>
    /// <returns>Gia tri tham so</returns>
    public string GetParamByCode(string code, Db db)
    {
        string query = "SELECT GiaTri FROM " + ListName.Table_ThamSoHeThong + " WHERE MaThamSo = '" + code + "'";
        List<Hashtable> listData = db.GetListData(query);
        if (listData.Count > 0)
            return listData[0]["GiaTri"].ToString().Trim();
        return "";
    }
}