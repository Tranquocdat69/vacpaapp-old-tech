﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;

  public  class DataBaseDb
    {
        static SqlConnection connect = new SqlConnection(ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter sda;
        SqlTransaction sts;
        private void OpenConnect()
        {
            try
            {
                if (connect.State != ConnectionState.Open)
                    connect.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private void CloseConnect()
        {
            try{
            if (connect.State != ConnectionState.Closed)
                connect.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetData2(string sql, string strTableName)
        {
            try
            {
                OpenConnect();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                dt.TableName = strTableName;
                ds.Tables.Add(dt);
                cmd = new SqlCommand(sql, connect);

                sda = new SqlDataAdapter(cmd);
                sda.Fill(ds, strTableName);
                CloseConnect();
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
                // CloseConnect();
            }
            finally
            {
                CloseConnect();
            }
        }
        public List<object> GetValueExecuteReader(string sql)
        {
            try
            {
                OpenConnect();
                DataTable dt = new DataTable();
                cmd = new SqlCommand(sql, connect);
                List<object> list = new List<object>();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    list.Add(read[0]);
                    list.Add(read[1]);
                }
                CloseConnect();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
                // CloseConnect();
            }
            finally
            {
                CloseConnect();
            }
        }
        public object GetValue(string sql)
        {
            try
            {
                OpenConnect();
                DataTable dt = new DataTable();
                cmd = new SqlCommand(sql, connect);

                object value=  cmd.ExecuteScalar();
                CloseConnect();
                return value;
            }
            catch (Exception ex)
            {
                throw ex;
                // CloseConnect();
            }
            finally
            {
                CloseConnect();
            }
        }
        public DataTable GetData(string sql)
        {
            try
            {
                OpenConnect();
                DataTable dt = new DataTable();
                cmd = new SqlCommand(sql, connect);

                sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                
                CloseConnect();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
                // CloseConnect();
            }
            finally
            {
                CloseConnect();
            }
        }
        public DataTable GetDataTable(object[] value, string[] par, string namePro, string strTableName)
        {
            try
            {
                OpenConnect();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                dt.TableName = strTableName;
                ds.Tables.Add(dt);
                cmd = new SqlCommand(namePro, connect);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter[] param = new SqlParameter[value.Length];
                for (int i = 0; i < param.Length; i++)
                {
                    param[i] = new SqlParameter(par[i], value[i]);
                }
                cmd.Parameters.AddRange(param);
                sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                CloseConnect();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
                // CloseConnect();
            }
            finally
            {
                CloseConnect();
            }
        }
        /// <summary>
        /// Phương thức lấy dữ liệu, gọi thủ tục
        /// </summary>
        /// <param name="namePro"></param>
        /// <returns></returns>
        public DataSet GetData(object []value,string []par,string namePro,string strTableName)
        {
            try{
            OpenConnect();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.TableName = strTableName;
            ds.Tables.Add(dt);
            cmd = new SqlCommand(namePro, connect);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter[] param = new SqlParameter[value.Length];
            for (int i = 0; i < param.Length; i++)
            {
                param[i] = new SqlParameter(par[i], value[i]);
            }
            cmd.Parameters.AddRange(param);
            sda = new SqlDataAdapter(cmd);
            sda.Fill(ds, strTableName);
            CloseConnect();
            return ds;
            }
            catch (Exception ex)
            {
                throw ex;
                // CloseConnect();
            }
            finally
            {
                CloseConnect();
            }
        }
             

        

    }

