﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Aspose.Words;

/// <summary>
/// Summary description for Library
/// </summary>
public class Library
{
    static Commons cm = new Commons();
    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/19
    /// Format datetime from dd?mm?yyyy to yyyy-mm-dd
    /// </summary>
    /// <param name="thoigian">Datetime value need convert</param>
    /// <param name="kytu">Char separator</param>
    /// <returns></returns>
    public static string ConvertDatetime(string thoigian, char kytu)
    {
        string result = "";
        string[] temp = thoigian.Split(kytu);
        if (temp.Length == 3)
        {
            result = temp[2] + "-" + temp[1] + "-" + temp[0];
        }
        return result;
    }

    public static string FormatDateTime(DateTime obj, string format)
    {
        return obj != new DateTime(1, 1, 1) ? obj.ToString(format) : "";
    }

    public static DateTime DateTimeConvert(object obj)
    {
        try
        {
            return Convert.ToDateTime(obj);
        }
        catch
        {
            return new DateTime(1, 1, 1);
        }
    }

    public static DateTime DateTimeConvert(string value, string format)
    {
        try
        {
            return DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);
        }
        catch
        {
            return new DateTime(1, 1, 1);
        }
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/19
    /// Convert some value to DateTime format 
    /// </summary>
    /// <param name="obj">value need to format</param>
    /// <param name="sepa">Char separator</param>
    /// <param name="type">Định dạng ngày tháng truyền vào 1:dd?mm?yyyy 2:mm?dd?yyyy 3:yyyy?mm?dd</param>
    /// <returns></returns>
    public static DateTime DateTimeConvert(string obj, char sepa, int type)
    {
        if (string.IsNullOrEmpty(obj))
            return new DateTime(1, 1, 1);
        try
        {
            string[] arrObj = obj.Split(sepa);

            // dd?mm?yyyy
            switch (type)
            {
                case 1:
                    return new DateTime(Convert.ToInt32(arrObj[2]), Convert.ToInt32(arrObj[1]), Convert.ToInt32(arrObj[0]));
                case 2:
                    return new DateTime(Convert.ToInt32(arrObj[2]), Convert.ToInt32(arrObj[0]), Convert.ToInt32(arrObj[1]));
                case 3:
                    return new DateTime(Convert.ToInt32(arrObj[0]), Convert.ToInt32(arrObj[1]), Convert.ToInt32(arrObj[2]));
            }
        }
        catch
        {
            return new DateTime(1, 1, 1);
        }
        return new DateTime(1, 1, 1); ;
    }

    public static Int32 Int32Convert(object obj)
    {
        try
        {
            return Convert.ToInt32(obj);
        }
        catch
        {
            return 0;
        }
    }

    public static Double DoubleConvert(object obj)
    {
        try
        {
            return Convert.ToDouble(obj);
        }
        catch
        {
            return 0;
        }
    }

    public static Decimal DecimalConvert(object obj)
    {
        try
        {
            return Convert.ToDecimal(obj, ListName.CI_VN);
        }
        catch
        {
            return 0;
        }
    }

    public static string RemoveFormatNumber(object obj)
    {
        string value = obj.ToString().Replace(".", "");
        value = value.Replace(",", ".");
        return value;
    }

    public static string ChangeFormatNumber(object obj, string type)
    {
        if (type == "vn")
        {
            string value = obj.ToString().Replace(".", ",");
            return value;
        }
        if(type == "us")
        {
            string value = obj.ToString().Replace(",", ".");
            return value;
        }
        return obj.ToString();
    }

    public static string CheckNull(object value)
    {
        if (value == null)
            return "";
        return value.ToString();
    }

    public static bool CheckIsInt32(object obj)
    {
        try
        {
            int temp = Convert.ToInt32(obj);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static bool CheckIsDecimal(object obj)
    {
        try
        {
            decimal temp = Convert.ToDecimal(obj);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static bool CheckDateTime(string value, string format)
    {
        try
        {
            DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static void GenColums(DataTable dt, string[] fields)
    {
        if (fields.Length > 0)
        {
            foreach (var field in fields)
            {
                string[] split = field.Split(':');
                if (split.Length > 1)
                {
                    Type t = Type.GetType("System." + split[1]);
                    dt.Columns.Add(split[0], t);
                }
                else
                    dt.Columns.Add(field, typeof(string));
            }
        }
    }

    public static object CheckKeyInHashtable(Hashtable ht, object key)
    {
        if (ht.ContainsKey(key))
            return ht[key];
        return "";
    }

    public static string GetValueFromArray(string[] arr, int index)
    {
        if (arr.Length > index)
            return arr[index];
        return "";
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra đúng định dang Email hay ko
    /// </summary>
    /// <param name="email">Địa chỉ email cần kiểm tra</param>
    /// <returns></returns>
    public static bool CheckIsValidEmail(string email)
    {
        string pattern = @"^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$";
        //Regular expression object
        Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
        //boolean variable to return to calling method
        bool valid = false;

        if (!String.IsNullOrEmpty(email))
        {
            valid = check.IsMatch(email);
        }
        //return the value to the calling method
        return valid;
    }

    public static string FormatMoney(object value)
    {
        string result = "";
        if (value.ToString().Length > 0)
            result = string.Format(ListName.CI_VN, "{0:0,0}", Library.DoubleConvert(value));
        if (result.StartsWith("0"))
            result = result.Substring(1, 1);
        return result;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/02
    /// Get tên loại học viên dựa theo từ khóa truyền vào
    /// </summary>
    /// <param name="value">Loại học viên</param>
    /// <returns></returns>
    public static string GetTenLoaiHoiVienCaNhan(string value)
    {
        switch (value)
        {
            case ListName.Type_LoaiHocVien_NguoiQuanTam:
                return "Người quan tâm";
            case ListName.Type_LoaiHocVien_HoiVien:
                return "Hội viên";
            case ListName.Type_LoaiHocVien_KiemToanVien:
                return "Kiểm toán viên";
        }
        return "";
    }

    public static string GetTenLoaiChuyenDe(string valueDb)
    {
        if (valueDb == ListName.Type_LoaiChuyenDe_KeToanKiemToan)
            return "Kế toán, kiểm toán";
        if (valueDb == ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep)
            return "Đạo đức nghề nghiệp";
        if (valueDb == ListName.Type_LoaiChuyenDe_ChuyenDeKhac)
            return "Khác";
        return "";
    }

    public static string GetTenXepLoai(string maXepLoai)
    {
        string xepLoai = "";
        switch (maXepLoai)
        {
            case "1":
                xepLoai = "Tốt";
                break;
            case "2":
                xepLoai = "Đạt yêu cầu";
                break;
            case "3":
                xepLoai = "Không đạt yêu cầu";
                break;
            case "4":
                xepLoai = "Yếu kém";
                break;
        }
        return xepLoai;
    }

    public static void ExportToExcel(string fileName, string htmlData)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Charset = "";
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
        HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
        string strFileName = fileName + ".xls";
        //HttpContext.Current.Response.AddHeader("Content-Disposition", ";filename=" + strFileName);
        //EnableViewState = false;
        StringWriter oStringWriter = new StringWriter();
        HtmlTextWriter oHtmlTextWriter = new HtmlTextWriter(oStringWriter);
        string strHTMLContent = htmlData;
        oHtmlTextWriter.Write(strHTMLContent);
        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", strFileName));

        HttpContext.Current.Response.Write(oStringWriter.ToString());
        HttpContext.Current.Response.End();
        HttpContext.Current.Response.Flush();
    }

    public static void ExportToWord(string fileName, string htmlData)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Charset = "";
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.ContentType = "application/vnd.ms-word";
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
        HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
        string strFileName = fileName + ".doc";
        //HttpContext.Current.Response.AddHeader("Content-Disposition", ";filename=" + strFileName);
        //EnableViewState = false;
        StringWriter oStringWriter = new StringWriter();
        HtmlTextWriter oHtmlTextWriter = new HtmlTextWriter(oStringWriter);
        string strHTMLContent = htmlData;
        oHtmlTextWriter.Write(strHTMLContent);
        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", strFileName));

        HttpContext.Current.Response.Write(oStringWriter.ToString());
        HttpContext.Current.Response.End();
        HttpContext.Current.Response.Flush();
    }

    public static string GetUrl(string imagepath)
    {
        string[] splits = HttpContext.Current.Request.Url.AbsoluteUri.Split('/');
        if (splits.Length >= 2)
        {
            string url = splits[0] + "//";
            for (int i = 2; i < splits.Length - 1; i++)
            {
                url += splits[i];
                url += "/";
            }
            return url + imagepath;
        }
        return imagepath;
    }

    public static string RemoveSpecCharaterWhenSendByJavascript(object str)
    {
        //str = str.Replace("\r\n", "<br />");
        return cm.AddSlashes(str.ToString().Replace("\r\n", "\n").Replace("\n", "\r\n"));
    }

    public static string CompactText(string str, int numberCharater)
    {
        string result = str.Replace("\r\n", "");
        if (result.Length > numberCharater)
            result = result.Substring(0, numberCharater) + " ...";
        return result;
    }

    
}