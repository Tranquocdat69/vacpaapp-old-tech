﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HiPT.VACPA.DL;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsXacDinhQuyen
/// </summary>
public class clsXacDinhQuyen
{
	public clsXacDinhQuyen()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string fcnXDQuyen(int nguoidungid,string machucnang, string connectionstr)
    {
        string quyen = "";
        int i;

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT Quyen FROM tblNhomQuyenChiTiet WHERE ChucNangID='" + machucnang + "' AND NhomQuyenID IN (SELECT NhomQuyenID FROM tblNguoiDungNhomQuyen WHERE NguoiDungID=" + nguoidungid +")";
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);

        for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            quyen += ds.Tables[0].Rows[i][0].ToString();

        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        return quyen;
    }
}