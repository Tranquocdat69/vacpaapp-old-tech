﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public class SmtpMail
{
    public static string smtp_UserName
    {
        get
        {
            try
            {
                return (string)System.Configuration.ConfigurationSettings.AppSettings["smtp_Username"];
            }
            catch
            {
                return null;
            }
        }
    }

    public static string smtp_FromAddress
    {
        get
        {
            try
            {
                return (string)System.Configuration.ConfigurationSettings.AppSettings["smtp_FromAddress"];
            }
            catch
            {
                return null;
            }
        }
    }

    public static string smtp_Password
    {
        get
        {
            return (string)System.Configuration.ConfigurationSettings.AppSettings["smtp_Password"];
        }
    }

    public static string smtp_Domain
    {
        get
        {
            return (string)System.Configuration.ConfigurationSettings.AppSettings["smtp_Domain"];
        }
    }

    public static string smtp_host
    {
        get
        {
            return (string)System.Configuration.ConfigurationSettings.AppSettings["smtp_Host"];
        }
    }

    public static string smtp_port
    {
        get
        {
            return (string)System.Configuration.ConfigurationSettings.AppSettings["smtp_Port"];
        }
    }

    public static Boolean Send(
        string SendFromUserName,
        string ToAddress,
        string Subject, 
        string Body,
        ref string strMsg
        )
    {
        // TODO: Add error handling for invalid arguments

            
        if (smtp_UserName==null) throw new Exception("Chưa thiết lập tài khoản gửi thư!");


        // To
        MailMessage mailMsg = new MailMessage();
        mailMsg.To.Add(ToAddress);
        // From
        MailAddress mailFromAddress = new MailAddress(smtp_FromAddress, SendFromUserName);
        mailMsg.From = mailFromAddress;
        //mailMsg.To = new MailAddress(ToAddress, toAddressName);

        // Subject and Body
        mailMsg.Subject = Subject;
        mailMsg.Body = Body;
        mailMsg.IsBodyHtml = true;
        mailMsg.BodyEncoding = System.Text.UTF8Encoding.UTF8;

        //mailMsg.BodyEncoding.CodePage = 65001;

        // Init SmtpClient and send
        SmtpClient smtpClient = new SmtpClient(smtp_host, int.Parse(smtp_port));
        System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(smtp_UserName, smtp_Password, smtp_Domain);
        smtpClient.Credentials = credentials;
        smtpClient.EnableSsl = false;

        System.Net.ServicePointManager.ServerCertificateValidationCallback =
                      delegate(object sender, X509Certificate certificate, X509Chain chain,
                               SslPolicyErrors sslPolicyErrors) { return true; };

        try
        {
            smtpClient.Send(mailMsg);
            strMsg = "Email đã được gửi thành công!";
            return true;
        }
        catch (Exception ex)
        {
            strMsg = "Có lỗi xảy ra trong quá trình gửi email: " + ex.Message;
            return false;
        }
    }

    public static Boolean SendMulti(
        string SendFromUserName,
        List<string> ToAddress,
        string Subject,
        string Body,
        ref string strMsg
        )
    {
        // TODO: Add error handling for invalid arguments


        if (smtp_UserName == null) throw new Exception("Chưa thiết lập tài khoản gửi thư!");


        // To
        MailMessage mailMsg = new MailMessage();
        foreach (string email in ToAddress)
        {
            if (!string.IsNullOrEmpty(email))
                mailMsg.To.Add(email);
        }
        // From
        MailAddress mailFromAddress = new MailAddress(smtp_FromAddress, SendFromUserName);
        mailMsg.From = mailFromAddress;
        //mailMsg.To = new MailAddress(ToAddress, toAddressName);

        // Subject and Body
        mailMsg.Subject = Subject;
        mailMsg.Body = Body;
        mailMsg.IsBodyHtml = true;
        mailMsg.BodyEncoding = System.Text.UTF8Encoding.UTF8;

        //mailMsg.BodyEncoding.CodePage = 65001;

        // Init SmtpClient and send
        SmtpClient smtpClient = new SmtpClient(smtp_host, int.Parse(smtp_port));
        System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(smtp_UserName, smtp_Password, smtp_Domain);
        smtpClient.Credentials = credentials;
        smtpClient.EnableSsl = false;

        System.Net.ServicePointManager.ServerCertificateValidationCallback =
                      delegate(object sender, X509Certificate certificate, X509Chain chain,
                               SslPolicyErrors sslPolicyErrors) { return true; };

        try
        {
            smtpClient.Send(mailMsg);
            strMsg = "Email đã được gửi thành công!";
            return true;
        }
        catch (Exception ex)
        {
            strMsg = "Có lỗi xảy ra trong quá trình gửi email: " + ex.Message;
            return false;
        }
    }

    public static Boolean SendWithAttach(
        string SendFromUserName,
        string ToAddress,
        string Subject,
        string Body,
        Attachment Data,
        ref string strMsg
        )
    {
        // TODO: Add error handling for invalid arguments


        if (smtp_UserName == null) throw new Exception("Chưa thiết lập tài khoản gửi thư!");


        // To
        MailMessage mailMsg = new MailMessage();
        mailMsg.To.Add(ToAddress);
        // From
        MailAddress mailFromAddress = new MailAddress(smtp_FromAddress, SendFromUserName);
        mailMsg.From = mailFromAddress;
        //mailMsg.To = new MailAddress(ToAddress, toAddressName);

        // Subject and Body
        mailMsg.Subject = Subject;
        mailMsg.Body = Body;
        mailMsg.IsBodyHtml = true;
        mailMsg.BodyEncoding = System.Text.UTF8Encoding.UTF8;
        mailMsg.Attachments.Add(Data);
        //mailMsg.BodyEncoding.CodePage = 65001;

        // Init SmtpClient and send
        SmtpClient smtpClient = new SmtpClient(smtp_host, int.Parse(smtp_port));
        System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(smtp_UserName, smtp_Password, smtp_Domain);
        smtpClient.Credentials = credentials;
        smtpClient.EnableSsl = false;

        System.Net.ServicePointManager.ServerCertificateValidationCallback =
                      delegate(object sender, X509Certificate certificate, X509Chain chain,
                               SslPolicyErrors sslPolicyErrors) { return true; };

        try
        {
            smtpClient.Send(mailMsg);
            strMsg = "Email đã được gửi thành công!";
            return true;
        }
        catch (Exception ex)
        {
            strMsg = "Có lỗi xảy ra trong quá trình gửi email: " + ex.Message;
            return false;
        }
    }

    public static Boolean SendMultiWithAttach(
        string SendFromUserName,
        List<string> ToAddress,
        string Subject,
        string Body,
        Attachment Data,
        ref string strMsg
        )
    {
        // TODO: Add error handling for invalid arguments


        if (smtp_UserName == null) throw new Exception("Chưa thiết lập tài khoản gửi thư!");


        // To
        MailMessage mailMsg = new MailMessage();
        foreach (string email in ToAddress)
        {
            if (!string.IsNullOrEmpty(email))
                mailMsg.To.Add(email);
        }
        // From
        MailAddress mailFromAddress = new MailAddress(smtp_FromAddress, SendFromUserName);
        mailMsg.From = mailFromAddress;
        //mailMsg.To = new MailAddress(ToAddress, toAddressName);

        // Subject and Body
        mailMsg.Subject = Subject;
        mailMsg.Body = Body;
        mailMsg.IsBodyHtml = true;
        mailMsg.BodyEncoding = System.Text.UTF8Encoding.UTF8;
        mailMsg.Attachments.Add(Data);
        //mailMsg.BodyEncoding.CodePage = 65001;

        // Init SmtpClient and send
        SmtpClient smtpClient = new SmtpClient(smtp_host, int.Parse(smtp_port));
        System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(smtp_UserName, smtp_Password, smtp_Domain);
        smtpClient.Credentials = credentials;
        smtpClient.EnableSsl = false;

        System.Net.ServicePointManager.ServerCertificateValidationCallback =
                      delegate(object sender, X509Certificate certificate, X509Chain chain,
                               SslPolicyErrors sslPolicyErrors) { return true; };

        try
        {
            smtpClient.Send(mailMsg);
            strMsg = "Email đã được gửi thành công!";
            return true;
        }
        catch (Exception ex)
        {
            strMsg = "Có lỗi xảy ra trong quá trình gửi email: " + ex.Message;
            return false;
        }
    }
}