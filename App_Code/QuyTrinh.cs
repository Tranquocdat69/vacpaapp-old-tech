﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using VACPA.Entities;

/// <summary>
/// Summary description for QuyTrinh
/// </summary>
public class QuyTrinh
{
	public QuyTrinh()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public int IDBuocDauTienCuaQuyTrinh(int QuyTrinhID)
    {
        int i=0;

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT TOP 1 BuocQuyTrinhID FROM tblBuocQuyTrinh WHERE QuyTrinhID='" + QuyTrinhID + "' ORDER BY ThuTu ASC";
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);

        if (ds.Tables[0].Rows.Count > 0)
            i = Convert.ToInt32(ds.Tables[0].Rows[0][0]);

        ds.Dispose();
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        return i;
    }
    
    public string TenBuocDauTienCuaQuyTrinh(int QuyTrinhID)
    {
        string i = "";

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT TOP 1 TenBuocQuyTrinh FROM tblBuocQuyTrinh WHERE QuyTrinhID='" + QuyTrinhID + "' ORDER BY ThuTu ASC";
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);

        if (ds.Tables[0].Rows.Count > 0)
            i = ds.Tables[0].Rows[0][0].ToString();

        ds.Dispose();
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        return i;
    }



}