﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="iframe.aspx.cs" Inherits="iframe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9">
<link rel="stylesheet" href="css/style.iframe.css" type="text/css" />
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/blue.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap-fileupload.min.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/jquery.alerts.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</head>
<body style=" background-color:#fff;">

                   <!-- NGUYEN MANH HUNG 2014/12/18 -->
                    <!-- Thông báo nổi ở góc bên phải trang -->
                    <div id="AlertMessage-Success-FloatRightBottom" name="AlertMessage-Success-FloatRightBottom" style="display: none;" class="alertFloatRightBottom alertFloatRightBottom-success">
                    </div>
                    <div id="AlertMessage-Error-FloatRightBottom" name="AlertMessage-Error-FloatRightBottom" style="display: none;" class="alertFloatRightBottom alertFloatRightBottom-error">
                    </div>

      <asp:PlaceHolder ID="mainpanel" runat="server"></asp:PlaceHolder>
                                               

</body>
</html>
