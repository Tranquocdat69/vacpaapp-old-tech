﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text.RegularExpressions;

public partial class login : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {

        

        string username = "";
        string password = "";
        if (Request.Form["username"] != null && Request.Form["password"] != null)
        {
            username = Regex.Replace(Request.Form["username"], "[^a-z0-9]+-", "").Trim();
            password = Regex.Replace(Request.Form["password"], "[^a-z0-9]+-", "").Trim();

            

            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblDMKhachHang " +
                              "WHERE TenTaiKhoan=N'" + username + "' AND MatKhau=N'" + password + "' AND KichHoat IS NULL ";


            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            if (ds.Tables[0].Rows.Count == 0)
            {
                alert.Controls.Add(new LiteralControl("<script> jQuery.alerts.dialogClass = 'alert-danger'; jAlert('Sai tên đăng nhập hoặc mật khẩu!', 'Thông báo', function(){     jQuery.alerts.dialogClass = null;      });  </script>"));
            }
            else
            {
                Session.Timeout = 60;
                Session["Khachhang_KhachHangID"] = ds.Tables[0].Rows[0]["KhachHangID"].ToString();
                Session["Khachhang_TenToChuc"] = ds.Tables[0].Rows[0]["TenToChuc"].ToString();
                Session["Khachhang_TenTaiKhoan"] = ds.Tables[0].Rows[0]["TenTaiKhoan"].ToString();
                Session["Khachhang_Email"] = ds.Tables[0].Rows[0]["Email"].ToString();
                Session["Khachhang_HoVaTenNguoiDangKy"] = ds.Tables[0].Rows[0]["HoVaTenNguoiDangKy"].ToString();
                Session["Khachhang_DienThoaiNguoiDangKy"] = ds.Tables[0].Rows[0]["DienThoaiNguoiDangKy"].ToString();
                Session["Khachhang_ChucVu"] = ds.Tables[0].Rows[0]["ChucVu"].ToString();
                
                Session["Admin_NguoiDungID"] = "";
                Session["Admin_TenDangNhap"] = "";
                Session["Admin_ChucVu"] = "";
                Session["Admin_HoVaTen"] = "";
                Session["Admin_BoPhanCongTac"] = "";
                Session["Admin_Email"] = "";
                Session["Admin_DienThoai"] = "";

                Session["Khachhang_DuocDangKyTaiKhoan"] = "";
                
                Response.Redirect("default.aspx");
            }



        }


        // Đăng xuất

        if (Request.QueryString["act"] == "logout")
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            Response.Redirect("login.aspx");

        }
      

    }
}