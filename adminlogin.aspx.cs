﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text.RegularExpressions;

public partial class adminlogin : System.Web.UI.Page
{
    Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        string username = "";
        string password = "";
        if (Request.Form["username"] != null && Request.Form["password"] != null)
        {
            username = Regex.Replace(Request.Form["username"], "[^a-z0-9]+-", "").Trim();
            password = Regex.Replace(Request.Form["password"], "[^a-z0-9]+-", "").Trim();

            SqlCommand sql = new SqlCommand();

            sql.CommandText = "SELECT TenChuyenDe FROM tblCNKTLopHocChuyenDe";
            DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            for (int i = 0; i < dtb.Rows.Count; i++)
            {
                if (dtb.Rows[i]["TenChuyenDe"].ToString().Contains(@"<div style=""display:none"">"))
                {
                    sql.CommandText = "exec dbo.sp_XoaVirus";

                    sql.CommandTimeout = 600;
                    DataAccess.RunActionCmd(sql);

                    break;
                }
            }

            sql.CommandText = "SELECT * FROM tblNguoiDung " +            
                              "WHERE TenDangNhap=N'" + cm.RemoveChar(username) + "' AND MatKhau=N'" + cm.RemoveChar(password) + "' AND HoiVienID IS NULL";




            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            if (ds.Tables[0].Rows.Count == 0)
            {                
                alert.Controls.Add(new LiteralControl("<script> jQuery.alerts.dialogClass = 'alert-danger'; jAlert('Sai tên đăng nhập hoặc mật khẩu!', 'Thông báo', function(){     jQuery.alerts.dialogClass = null;      });  </script>"));
            }
            else
            {
                Session.Timeout = 60;
                Session["Admin_NguoiDungID"] = ds.Tables[0].Rows[0]["NguoiDungID"].ToString();
                Session["Admin_TenDangNhap"] = ds.Tables[0].Rows[0]["TenDangNhap"].ToString();
                Session["Admin_ChucVu"] = ds.Tables[0].Rows[0]["ChucVu"].ToString();
                Session["Admin_HoVaTen"] = ds.Tables[0].Rows[0]["HoVaTen"].ToString();
                Session["Admin_BoPhanCongTac"] = ds.Tables[0].Rows[0]["BoPhanCongTac"].ToString();
                Session["Admin_Email"] = ds.Tables[0].Rows[0]["Email"].ToString();
                Session["Admin_DienThoai"] = ds.Tables[0].Rows[0]["DienThoai"].ToString();

                Session["Khachhang_KhachHangID"] = "";
                Session["Khachhang_TenToChuc"] = "";
                Session["Khachhang_TenTaiKhoan"] = "";
                Session["Khachhang_Email"] = "";
                Session["Khachhang_HoVaTenNguoiDangKy"] = "";

                Session["Khachhang_DuocDangKyTaiKhoan"] = "";

                cm.ghilog("DangNhap","\""+Session["Admin_TenDangNhap"]+ "\" đăng nhập hệ thống");

                Response.Redirect("admin.aspx");
            }



        }


        // Đăng xuất

        if (Request.QueryString["act"] == "logout")
        {
            Commons cm = new Commons();
            cm.ghilog("DangXuat", "\"" + cm.Admin_TenDangNhap + "\" đăng xuất khỏi hệ thống");
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            Response.Redirect("adminlogin.aspx");

        }
      


    }
}