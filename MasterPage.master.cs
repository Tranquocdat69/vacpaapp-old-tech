﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            if (Request.Path.Contains("admin.aspx") ) leftmenu.Controls.Add(Page.LoadControl("usercontrols/adminleftmenu.ascx"));
            if (Request.Path.Contains("default.aspx") ) leftmenu.Controls.Add(Page.LoadControl("usercontrols/leftmenu.ascx"));


            string pagetoload = Request.QueryString["page"];
            if (pagetoload != null)
            {

                string folder = "";



            // UC DanhMuc
                string UC_DanhMuc = "dmchung,dmchung_add,dmchung_edit,dmcocautochuc,dmcocautochuc_add,dmcocautochuc_edit,dmdangykienkt,dmdangykienkt_add,dmdangykienkt_edit,dmgiangvien,dmgiangvien_add,dmgiangvien_edit,dmhuyen,dmhuyen_add,dmhuyen_edit,dmloaitailieu,dmloaitailieu_add,dmloaitailieu_edit,dmsothich,dmsothich_add,dmsothich_edit,dmtinh,dmtinh_add,dmtinh_edit,dmxa,dmxa_add,dmxa_edit,DanhMuc_Process";
                if (UC_DanhMuc.ToLower().Split(',').Contains(pagetoload.ToLower())) folder = "DanhMuc/";
                // UC QTHT
                string UC_QTHT = "account,account_add,account_edit,cauhinhhethong,nhatkysudung,role,role_add,role_edit,user,user_add,user_edit,QTHT_QuanLyTaiLieu,QTHT_QuanLyTaiLieu_Update";
                if (UC_QTHT.ToLower().Split(',').Contains(pagetoload.ToLower())) folder = "QTHT/";
                // UC WorkFlow
                string UC_WorkFlow = "action_add,action_edit,khaibaoquytrinh,step_add,step_edit,step_sort";
                if (UC_WorkFlow.ToLower().Split(',').Contains(pagetoload.ToLower())) folder = "WorkFlow/";
                // UC CNKT
                string UC_CNKT = "CNKT_BaoCaoKetQuaToChucLopHoc,CNKT_BaoCaoKetQuaToChucLopHoc_Process,CNKT_CapNhatSoGioDaoTaoCNKT,CNKT_CapNhatSoGioDaoTaoCNKT_Process,CNKT_DangKyHocCaNhan_Add,CNKT_DangKyHocCaNhan_Process,CNKT_DangKyHocCongTy_Add,CNKT_QuanLyDangKyHoc,CNKT_QuanLyLopHoc,CNKT_QuanLyLopHoc_Add,CNKT_QuanLyLopHoc_Details,CNKT_QuanLyLopHoc_MiniList,CNKT_QuanLyLopHoc_Process,CNKT_TongHop_GioCNKT,CNKT_TongHop_GioCNKTConThieu,dmgiangvien_List,Export_Process,GetFile,HoiVienCaNhan_MiniList,HoiVienTapThe_DanhSachCongTyKiemToan,HoiVienTapThe_DanhSachCongTyKiemToan_Choice,HoiVienTapThe_MiniList,CNKT_QuanLyLopHoc_Email,CNKT_QuanLyLopHoc_ViewEmailForAnonymous,CNKT_QuanLyDangKyHoc_PheDuyet";
                if (UC_CNKT.ToLower().Split(',').Contains(pagetoload.ToLower())) folder = "CNKT/";
                // UC QLHV
                string UC_QLHV = "thongbao_add,yeucaucnkttaidonvikhac_ChonHoiVien,yeucaucnkttaidonvikhac_add,yeucaucnkttaidonvikhac,capnhathoso,capnhathoso_pheduyet,capnhathoso_pheduyet_hvcn,danhsachkhachhang,hoiviencanhan_add,hoiviencanhan_edit,hosohoiviencanhan,hosohoivientapthe,hosohoivientapthe_edit,ketnaphoivien,ketnaphoivientapthe_edit,nguoiquantam_add,xoatenhoiviencanhan,xoatenhoiviencanhan_add,xoatenhoivientapthe,xoatenhoivientapthe_add,QLHV_DanhSachCongTyDangKyDangLogo,QLHV_DanhSachCongTyDangKyDangLogo_Update,QLHV_Process,guithongbaoxoaten,getmail,ketnapchinhanh";
                if (UC_QLHV.ToLower().Split(',').Contains(pagetoload.ToLower())) folder = "QLHV/";
                // UC QLP
                string UC_QLP = "danhsachhoiviencanhan_ajax,danhsachoiviencanhan,doingay,guimail,quanlyphidanglogo,quanlyphidanhsachphi,quanlyphihoivien,quanlyphikhac,quanlyphikiemsoatchatluong,thanhtoanphicanhan,thanhtoanphicanhan_chitiet,thongbaonophoiphi,thongbaonopphicapnhatkienthuc,thongbaonopphikiemsoatchatluong,thongbaonopphidanglogo,thanhtoanphicanhan,thanhtoanphicanhan_chitiet,thanhtoanphitapthe,thanhtoanphicanhan_chitiet1,thanhtoanphi_quanly,dmNhanSuHoiVien_Add,dmChonCongTy,danhsachhoiviencanhan,danhsachhoivientapthe,quanlyphikscl_danhsachcongty_canhan,danhsachcongtykiemtratructiep,danhsachlophoc";
                if (UC_QLP.ToLower().Split(',').Contains(pagetoload.ToLower())) folder = "QLP/";
                // UC KSCL
                string UC_KSCL = "KSCL_ChuanBi_CapNhatGioDaoTaoChoThanhVienDoanKiemTra,KSCL_ChuanBi_DanhSachBaoCaoTuKiemTra,KSCL_ChuanBi_DanhSachBaoCaoTuKiemTra_NhapBaoCao,KSCL_ChuanBi_DanhSachCongTyKiemTraTrucTiep,KSCL_ChuanBi_DanhSachCongTyKiemTraTrucTiep_FormChonDSTuKiemTra,KSCL_ChuanBi_DanhSachCongTyKiemTraTrucTiep_LapDanhSach,KSCL_ChuanBi_DanhSachCongTyKTTheoYeuCauKiemTraVuViec,KSCL_ChuanBi_DanhSachCongTyKTTheoYeuCauKiemTraVuViec_LapDanhSach,KSCL_ChuanBi_DanhSachCongTyTuKiemTra,KSCL_ChuanBi_DanhSachCongTyTuKiemTra_LapDanhSach,KSCL_ChuanBi_DanhSachCongTyTuKiemTra_Process,KSCL_ChuanBi_DanhSachDoanKiemTra,KSCL_ChuanBi_DanhSachDoanKiemTra_ChonCongTyKiemToan,KSCL_ChuanBi_DanhSachDoanKiemTra_ChonThanhVien,KSCL_ChuanBi_DanhSachDoanKiemTra_LapDoan,KSCL_ChuanBi_Process,KSCL_ChuanBi_XacNhanDanhSachGuiEmail,KSCL_ThucHien_BaoCaoKetQuaKiemTraHeThong,KSCL_ThucHien_BaoCaoKetQuaKiemTraKyThuat,KSCL_ThucHien_BoCauHoiKiemTraHeThong,KSCL_ThucHien_BoCauHoiKiemTraKyThuat,KSCL_ThucHien_CamKetDocLapBaoMatThongTin,KSCL_ThucHien_Process,KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong,KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_CapNhat,KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_MiniList,KSCL_ThucHien_TongHopKetQuaKiemTra,KSCL_KetThuc_XuLySaiPham,KSCL_KetThuc_XuLySaiPham_ChonHoiVienCaNhan,KSCL_KetThuc_XuLySaiPham_NoiDung,KSCL_KetThuc_XuLySaiPham_UpdateHoiVienCaNhan,KSCL_KetThuc_Process,KSCL_KetThuc_XuLySaiPham_ChonDanhSachKiemTraTrucTiep";
                if (UC_KSCL.ToLower().Split(',').Contains(pagetoload.ToLower())) folder = "KSCL/";
                // UC BaoCao
                string UC_BaoCao = "CNKT_BaoCao_ThuMoiThamDuLopHoc,CNKT_BaoCao_InGCNGioCNKT,CNKT_BaoCao_MauDangKyThamDuCNKT,BTC_BaoCao_KetQuaToChucLopHocCNKTKTV,BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT,BTC_BaoCao_VeViecToChucLopHocCNKT,TT_BaoCao_NopPhiCNKTcuaKTV,TT_BaoCao_NopPhiDangLogo,TT_BaoCao_NopPhiHoiVienCaNhan,TT_BaoCao_NopPhiHoiVienTapThe,TT_BaoCao_NopPhiKhacCuaTapTheVaCongTy,TT_BaoCao_NopPhiKhacHoiVienvaNQT,TT_BaoCao_NopPhiKSCLcuaKTV,TT_BaoCao_NopPhiKSCLtheoCongTy,KSCL_BaoCao_BangCauHoiKiemTra_ChonCongTy,KSCL_BaoCao_BangCauHoiKiemTra_ChonDoanKiemTra,KSCL_BaoCao_BangCauHoiKiemTraHeThong,KSCL_BaoCao_BangCauHoiKiemTraKyThuat,KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra,KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep,KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra,KSCL_BaoCao_DanhSachHoiVienKiemTraChatLuongDichVu,KSCL_BaoCao_DanhSachThanhVienDoanKiemTraNam,KSCL_BaoCao_Proccess,KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien,KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien_ChonDanhSachKTTT,KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien_ChonHoiVienCaNhan,KSCL_BaoCao_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien,QLHV_BaoCao_DanhSachKetNapHoiVienCaNhan,QLHV_BaoCao_DanhSachKetNapHoiVienTapThe,QLHV_BaoCao_QuyetDinhKetNapHoiVienCaNhan,KSCL_BaoCao_BangChamDiemKiemTraHeThong,KSCL_BaoCao_BangChamDiemKiemTraKyThuat,KSCL_BaoCao_BangTongHopSaiPhamHeThong,KSCL_BaoCao_BangTongHopSaiPhamKyThuat,KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam,QLHV_BC_QuyetDinhKetNapHoiVienTapThe,QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan,QLHV_BC_QuyetDinhXoaTenHoiVien,QLHV_BC_DanhSachKhenThuongHoiVienCaNhan,QLHV_BC_DanhSachKhenThuongHoiVienTapThe,QLHV_BC_DanhSachKyLuatHoiVienCaNhan,QLHV_BC_DanhSachKyLuatHoiVienTapThe,BaoCaoDanhSachCongTyKiemToanTrongCaNuoc,BaoCaoDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet,BaoCaoDanhSachKiemToanVienTrongCaNuoc,BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet,BaoCaoDanhSachNguoiQuanTamTrongCaNuoc,CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet,dbChonHoiVienCaNhan_BC,QLHV_BC_Chon,QLHV_BC_DonXinGiaNhapHoiVienCaNhan,QLHV_BC_DonXinGiaNhapHoiVienTapThe,QLHV_BC_GiayChungNhanHoiVienTapThe,QLHV_BC_ThongTinHoiVien,CNKT_BaoCao_InGCNGioCNKT_ChonHocVien,CNKT_BaoCao_DanhSachKTVThamDuCNKT,CNKT_BaoCao_DanhSachNQTThamDuCNKT,CNKT_BaoCao_TongHopSoGioCNKTCuaKTV,KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra_ChonDanhSach,QLHV_BC_TheHoiVienCaNhan,KSCL_BaoCao_BangTongHopSaiPhamKyThuat_TungCongTy";
                if (UC_BaoCao.ToLower().Split(',').Contains(pagetoload.ToLower())) folder = "BaoCao/";

                maincontent.Controls.Add(Page.LoadControl("usercontrols/" + folder + pagetoload + ".ascx"));

            }
            else
            {
                if (Request.Path.Contains("admin.aspx")) maincontent.Controls.Add(Page.LoadControl("usercontrols/adminhome.ascx"));
                if (Request.Path.Contains("default.aspx")) maincontent.Controls.Add(Page.LoadControl("usercontrols/home.ascx"));

                
            }
        }
        catch (Exception ex)
        {
             contentmessage.Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> "+ ex.Message +"</div> "));
        }



       
    }


}
