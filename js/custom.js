﻿//jQuery.noConflict();
function openInNewTab(url) {
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function removeItem(array, item) {
    for (var i in array) {
        if (array[i] == item) {
            array.splice(i, 1);
            break;
        }
    }
}

function close_leftpanel() {
    $(".leftpanel").fadeOut(200);
    $(".rightpanel").animate({ "margin-left": "0px" }, 300);
    $("#momenu").show();
    $('body').css('background-image', 'url("/apo/1.jpg")');

}

function show_leftpanel() {
    $(".leftpanel").fadeIn(200);
    $(".rightpanel").animate({ "margin-left": "260px" }, 300);
    $('.leftpanel').css('background-image', 'url("../images/leftpanelbg.png")');
    $("#momenu").hide();
}

jQuery(document).ready(function () {

    // dropdown in leftmenu
    jQuery('.leftmenu .dropdown > a').click(function () {
        if (!jQuery(this).next().is(':visible'))
            jQuery(this).next().slideDown('fast');
        else
            jQuery(this).next().slideUp('fast');
        return false;
    });

    if (jQuery.uniform)
        jQuery('input:checkbox, input:radio, .uniform-file').uniform();

    if (jQuery('.widgettitle .close').length > 0) {
        jQuery('.widgettitle .close').click(function () {
            jQuery(this).parents('.widgetbox').fadeOut(function () {
                jQuery(this).remove();
            });
        });
    }


    // change skin color
    jQuery('.skin-color a').click(function () { return false; });
    jQuery('.skin-color a').hover(function () {
        var s = jQuery(this).attr('href');
        if (jQuery('#skinstyle').length > 0) {
            if (s != 'default') {
                jQuery('#skinstyle').attr('href', 'css/style.' + s + '.css');
                jQuery.cookie('skin-color', s, { path: '/' });
            } else {
                jQuery('#skinstyle').remove();
                jQuery.cookie("skin-color", '', { path: '/' });
            }
        } else {
            if (s != 'default') {
                jQuery('head').append('<link id="skinstyle" rel="stylesheet" href="css/style.' + s + '.css" type="text/css" />');
                jQuery.cookie("skin-color", s, { path: '/' });
            }
        }
        return false;
    });



    // expand/collapse boxes
    if (jQuery('.minimize').length > 0) {

        jQuery('.minimize').click(function () {
            if (!jQuery(this).hasClass('collapsed')) {
                jQuery(this).addClass('collapsed');
                jQuery(this).html("&#43;");
                jQuery(this).parents('.widgetbox')
										      .css({ marginBottom: '20px' })
												.find('.widgetcontent')
												.hide();
            } else {
                jQuery(this).removeClass('collapsed');
                jQuery(this).html("&#8211;");
                jQuery(this).parents('.widgetbox')
										      .css({ marginBottom: '0' })
												.find('.widgetcontent')
												.show();
            }
            return false;
        });

    }

});

// NGUYEN MANH HUNG - 2014/11/20
// Ham chuyen kieu chu -> dang hien thi kieu so (0,000.0)
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~ ~n)).replace(new RegExp(re, 'g'), '$&.');
};

function DisplayTypeNumber(obj) {
    var ori_value = obj.value;
    if (ori_value != "" && !isNaN(ori_value)) {
        var value = parseFloat(ori_value).format(0, 3);
        obj.value = value;
        obj.style.textAlign = 'right';
    }
}

function UnDisplayTypeNumber(obj) {
    var ori_value = obj.value;
    if (ori_value != "") {
        var value = ori_value.replace(/\./g, '');
        if (!isNaN(value)) {
            obj.value = value;
        }
        obj.style.textAlign = 'left';
    }
}

function ChangeFormatNumber(value, type) {
    if (value != undefined) {
        alert(value);
        if (type == "vn")
            return value.replace(/\./g, ',');
        if (type == "us")
            return value.replace(/\,/g, '.');
    }
    return value;
}

function CheckIsNumber(obj) {
    if (obj != undefined && obj != null) {
        var id = obj.id;
        if (isNaN(obj.value)) {
            obj.value = '';
            alert('Phải nhập số.');
            var t = setTimeout('document.getElementById("' + id + '").focus()', 1);
            return false;
        }
    }
    return true;
}

function CheckIsNumberPositive(obj) {
    if (obj != undefined && obj != null) {
        var id = obj.id;
        if (parseFloat(obj.value) < 0) {
            obj.value = '';
            alert('Phải nhập số nguyên không âm!');
            var t = setTimeout('document.getElementById("' + id + '").focus()', 1);
            return false;
        }
    }
    return true;
}

function CheckIsInteger(obj) {
    if (obj != undefined && obj != null) {
        var id = obj.id;
        if (isNaN(obj.value) || obj.value.indexOf('.') > -1) {
            obj.value = '';
            alert('Phải nhập số.');
            var t = setTimeout('document.getElementById("' + id + '").focus()', 1);
            return false;
        }
    }
    return true;
}

function CustomParseFloat(value) {
    value = value.replace(/\,/g, '.');
    return parseFloat(value);
}

function CheckFormatDate(obj, format) {
    if (obj != undefined && obj != null) {
        var id = obj.id;
        if (obj.value.length > 0) {
            var regex;
            if (format == 'dd/MM/yyyy')
                regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
            if (!regex.test(obj.value)) {
                obj.value = '';
                alert('Phải nhập số.');
                var t = setTimeout('document.getElementById("' + id + '").focus()', 1);
                return false;
            }
        }
    }
    return true;
}

function CallAlertSuccessFloatRightBottom(msg) {
    jQuery('#AlertMessage-Success-FloatRightBottom').html("<button class='close' type='button' data-dismiss='alert'>×</button>" + msg).show();
    setTimeout(function () { $('#AlertMessage-Success-FloatRightBottom').hide(); }, 10000);
}

function CallAlertErrorFloatRightBottom(msg) {
    jQuery('#AlertMessage-Error-FloatRightBottom').html("<button class='close' type='button' data-dismiss='alert'>×</button>" + msg).show();
    setTimeout(function () { $('#AlertMessage-Error-FloatRightBottom').hide(); }, 10000);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}