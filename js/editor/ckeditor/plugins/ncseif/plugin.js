/**
 * Basic sample plugin inserting current date and time into CKEditor editing area.
 */

// Register the plugin with the editor.
// http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.plugins.html
CKEDITOR.plugins.add( 'ncseif',
{
	// The plugin initialization logic goes inside this method.
	// http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.pluginDefinition.html#init
	init: function( editor )
	{
		// Define an editor command that inserts a ncseif. 
		// http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#addCommand
		editor.addCommand( 'insertNcseif',
			{
				// Define a function that will be fired when the command is executed.
				// http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.commandDefinition.html#exec
				exec : function( editor )
				{    
					var ncseif = new Date();
					// Insert the ncseif into the document.
					// http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#insertHtml
					//editor.insertHtml('The current date and time is: <em>' + ncseif.toString() + '</em>');
//// 					editor.focus();

////					 var editorData = editor.getData();

////					var selection = editor.getSelection();
////					var range = selection.getRanges()[0];
////					var pCon = range.startContainer.getAscendant({ p: 2 }, true); //getAscendant('p',true);
////					  var pCon = range.startContainer.getAscendant('p',true);
////					var newRange = new CKEDITOR.dom.range(range.document);
////					newRange.moveToPosition(pCon, CKEDITOR.POSITION_BEFORE_START);
////				 
////					newRange.select();
//////					var text = 'NCSEIF55 ' + editorData ;
//////					editor.setData(text.replace(/(?:\r\n)|\n|\r/g, ' '), function () {
//////					    this.checkDirty();  // true
//////					});
////					editor.insertHtml('NCSEIF22 ' + editorData);

					editor.insertText('NCSEIF ');
				}
			});
		// Create a toolbar button that executes the plugin command. 
		// http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.ui.html#addButton
		editor.ui.addButton( 'Ncseif',
		{
			// Toolbar button tooltip.
			label: 'Insert Ncseif',
			// Reference to the plugin command name.
			command: 'insertNcseif',
			// Button's icon file path.
			icon: this.path + 'images/ncseif.png'
		} );
	}
} );