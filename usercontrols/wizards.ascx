﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wizards.ascx.cs" Inherits="usercontrols_wizards" %>



<script type="text/javascript" src="js/jquery.smartWizard.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {

        // Smart Wizard 	
        jQuery('#wizard').smartWizard({ onFinish: onFinishCallback });
        jQuery('#wizard2').smartWizard({ onFinish: onFinishCallback });
        jQuery('#wizard3').smartWizard({ onFinish: onFinishCallback });

        function onFinishCallback() {
            alert('Finish Clicked');
        }

       

    });
</script>


  <!-- START OF DEFAULT WIZARD -->
                <h4 class="subtitle2">Các bước thực hiện</h4>
                    <form class="stdform" method="post" action="wizards.html">
                    <div id="wizard" class="wizard">
                    	<br />
                        <ul class="hormenu">
                            <li>
                            	<a href="#wiz1step1">
                                	<span class="h2">Bước 1</span>
                                    <span class="label">Basic Information</span>
                                </a>
                            </li>
                            <li>
                            	<a href="#wiz1step2">
                                	<span class="h2">Bước 2</span>
                                    <span class="label">Account Information</span>
                                </a>
                            </li>
                            <li>
                            	<a href="#wiz1step3">
                                	<span class="h2">Bước 3</span>
                                    <span class="label">Terms of Agreement</span>
                                </a>
                            </li>
                            
                      </ul>
                                                	
                        <div id="wiz1step1" class="formwiz">
                        	<h4 class="widgettitle">Step 1: Basic Information</h4>
                        	
                                <p>
                                    <label>First Name</label>
                                    <span class="field"><input type="text" name="firstname" id="firstname" class="input-xxlarge" /></span>
                                </p>
                                
                                <p>
                                    <label>Last Name</label>
                                    <span class="field"><input type="text" name="lastname" id="lastname" class="input-xxlarge" /></span>
                                </p>
                                                                
                                <p>
                                    <label>Gender</label>
                                    <span class="field">
                                    	<select name="selection" id="selection" class="uniformselect">
                                        	<option value="">Choose One</option>
                                        	<option value="1">Female</option>
                                        	<option value="2">Male</option>
                                    	</select>
                                    </span>
                                </p>
                                
                        	
                            
                        </div><!--#wiz1step1-->
                        
                        <div id="wiz1step2" class="formwiz">
                        	<h4 class="widgettitle">Step 2: Account Information</h4>
                            
                                <p>
                                    <label>Account No</label>
                                    <span class="field"><input type="text" name="lastname" class="input-xxlarge" /></span>
                                </p>
                                <p>
                                    <label>Address</label>
                                    <span class="field"><textarea cols="80" rows="5" name="location" class="span6"></textarea></span>
                                </p>
                                                                                               
                        </div><!--#wiz1step2-->
                        
                        <div id="wiz1step3">
                        	<h4 class="widgettitle">Step 3: Terms of Agreement</h4>
                            <div class="par terms" style="padding: 0 20px;">
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                                <p><input type="checkbox"  /> I agree with the terms and agreement...</p>
                            </div>
                        </div><!--#wiz1step3-->
                        
                    </div><!--#wizard-->
                    </form>
                    <!-- END OF DEFAULT WIZARD -->
                    
                    <div class="clearfix"></div><br />

                <!-- START OF DEFAULT WIZARD (INVERSE)-->
                <h4 class="subtitle2"><br />
                    
                    <!-- START OF TABBED WIZARD -->                </h4>
                <h4 class="subtitle2">Tabbed Wizard</h4>
                    <br />
                    <form class="stdform" method="post" action="wizards.html">
                    <div id="wizard3" class="wizard tabbedwizard">
                    	
                        <ul class="tabbedmenu">
                            <li>
                            	<a href="#wiz3step1">
                                	<span class="h2">STEP 1</span>
                                    <span class="label">Basic Information</span>
                                </a>
                            </li>
                            <li>
                            	<a href="#wiz3step2">
                                	<span class="h2">STEP 2</span>
                                    <span class="label">Account Information</span>
                                </a>
                            </li>
                            <li>
                            	<a href="#wiz3step3">
                                	<span class="h2">STEP 3</span>
                                    <span class="label">Terms of Agreement</span>
                                </a>
                            </li>
                        </ul>
                        
                        	
                        <div id="wiz3step1" class="formwiz">
                        	<h4>Step 1: Basic Information</h4>
                        	
                                <p>
                                    <label>First Name</label>
                                    <span class="field"><input type="text" name="firstname" class="input-xxlarge" /></span>
                                </p>
                                
                                <p>
                                    <label>Last Name</label>
                                    <span class="field"><input type="text" name="lastname" class="input-xxlarge" /></span>
                                </p>
                                                                
                                <p>
                                    <label>Gender</label>
                                    <span class="field"><select name="" class="uniformselect">
                                        <option value="">Choose One</option>
                                        <option value="1">Female</option>
                                        <option value="2">Male</option>
                                    </select></span>
                                </p>
                                
                        	
                            
                        </div><!--#wiz13tep1-->
                        
                        <div id="wiz3step2" class="formwiz">
                        	<h4>Step 2: Account Information</h4> 
                            
                                <p>
                                    <label>Account No</label>
                                    <span class="field"><input type="text" name="lastname" class="input-xxlarge" /></span>
                                </p>
                                <p>
                                    <label>Address</label>
                                    <span class="field"><textarea cols="80" rows="5" class="span6" name="location"></textarea></span>
                                </p>
                                                                                               
                        </div><!--#wiz3step2-->
                        
                        <div id="wiz3step3">
                        	<h4>Step 3: Terms of Agreement</h4>
                            <div class="par terms">
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                                <p><input type="checkbox"  /> I agree with the terms and agreement...</p>
                            </div>
                        </div><!--#wiz3step3-->
                        
                    </div><!--#wizard-->
                    </form>
                                        
                    <!-- END OF TABBED WIZARD -->

