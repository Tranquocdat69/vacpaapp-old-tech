﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_QTHT_QuanLyTaiLieu_Update : System.Web.UI.UserControl
{
    protected string tenchucnang = "Thêm mới, xem, sửa thông tin tài liệu";
    Db _db = new Db(ListName.ConnectionString);
    protected string _perOnFunc_DSTaiLieu = "";
    private Commons cm = new Commons();
    private string _id = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        _id = Library.CheckNull(Request.QueryString["Id"]);
        try
        {
            _db.OpenConnection();
            if (Request.Form["hdAction"] != null && Request.Form["hdAction"] == "update")
                Save();
            if (Request.Form["hdAction"] != null && Request.Form["hdAction"] == "delete")
                Delete();
            if (!string.IsNullOrEmpty(_id))
                SpanCacNutChucNang.Visible = true;
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void LoadOneData()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            if (!string.IsNullOrEmpty(_id))
            {
                SqlDmTaiLieuProvider pro = new SqlDmTaiLieuProvider(ListName.ConnectionString, false, string.Empty);
                DmTaiLieu obj = pro.GetByTaiLieuId(Library.Int32Convert(_id));
                if (obj != null && obj.TaiLieuId > 0)
                {
                    js += "$('#ddlLoaiTaiLieu').val('" + obj.LoaiTaiLieuId + "');" + Environment.NewLine;
                    js += "$('#txtCoQuanBanHanh').val('" + obj.CoQuanBh + "');" + Environment.NewLine;
                    js += "$('#txtMaTaiLieu').val('" + obj.MaTaiLieu + "');" + Environment.NewLine;
                    js += "$('#txtNgayNhapThongTin').val('" + obj.NgayNhap.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#txtTenTaiLieu').val('" + obj.TenTaiLieu + "');" + Environment.NewLine;
                    js += "$('#txtDienGiai').val('" + obj.DienGiai + "');" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(obj.TenFileDinhKem))
                    {
                        js += "$('#spanTenFile').html('" + obj.TenFileDinhKem + "');" + Environment.NewLine;
                        js += "$('#btnDownload').prop('href', '/admin.aspx?page=getfile&id=" + _id + "&type=" + ListName.Table_DMTaiLieu + "');" + Environment.NewLine;
                        js += "$('#btnDownload').html('Tải về');" + Environment.NewLine;
                    }
                }
                else
                {
                    js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Không tìm thấy thông tin tài liệu. Đề nghị thử lại.\").show();" + Environment.NewLine;
                    js += "$('#" + Form1.ClientID + "').remove();" + Environment.NewLine;
                }
            }
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Save()
    {
        bool success = false;

        HttpPostedFile fileCanCu = Request.Files["FileTaiLieu"];

        SqlDmTaiLieuProvider pro = new SqlDmTaiLieuProvider(ListName.ConnectionString, false, string.Empty);
        SqlDmLoaiTaiLieuProvider proLoaiTaiLieu = new SqlDmLoaiTaiLieuProvider(ListName.ConnectionString, false, string.Empty);
        DmTaiLieu obj;
        if (string.IsNullOrEmpty(_id)) // Insert
        {
            obj = new DmTaiLieu();
        }
        else // Update
        {
            obj = pro.GetByTaiLieuId(Library.Int32Convert(_id));
        }
        // Kiểm tra xem có thay đổi mã tài liệu ko
        if (obj.NgayNhap != Library.DateTimeConvert(Request.Form["txtNgayNhapThongTin"], "dd/MM/yyyy") || obj.LoaiTaiLieuId != Library.Int32Convert(Request.Form["ddlLoaiTaiLieu"]))
        {
            // Gen mã tài liệu
            DmLoaiTaiLieu objLoaiTaiLieu = proLoaiTaiLieu.GetByLoaiTaiLieuId(Library.Int32Convert(Request.Form["ddlLoaiTaiLieu"]));
            obj.MaTaiLieu = GenTaiLieu(objLoaiTaiLieu, Library.DateTimeConvert(Request.Form["txtNgayNhapThongTin"], "dd/MM/yyyy"));
        }
        obj.TenTaiLieu = Request.Form["txtTenTaiLieu"];
        obj.LoaiTaiLieuId = Library.Int32Convert(Request.Form["ddlLoaiTaiLieu"]);
        obj.CoQuanBh = Request.Form["txtCoQuanBanHanh"];
        obj.NgayNhap = Library.DateTimeConvert(Request.Form["txtNgayNhapThongTin"], "dd/MM/yyyy");
        obj.DienGiai = Request.Form["txtDienGiai"];
        if (fileCanCu.ContentLength > 0)
        {
            BinaryReader br = new BinaryReader(fileCanCu.InputStream);
            byte[] fileByte = br.ReadBytes(fileCanCu.ContentLength);
            obj.FileDinhKem = fileByte;
            obj.TenFileDinhKem = fileCanCu.FileName;
        }
        if (string.IsNullOrEmpty(_id)) // Insert
        {
            if (pro.Insert(obj))
                success = true;
        }
        else // Update
        {
            if (pro.Update(obj))
                success = true;
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        if (success)
        {
            // Đóng form mà refresh số liệu
            js += "parent.CloseFormUpdate();" + Environment.NewLine;
            js += "parent.Search();" + Environment.NewLine;
        }
        else
        {
            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Có lỗi trong qua trình thêm dữ liệu.\").show();" + Environment.NewLine;
        }
        js += "</script>";
        Page.RegisterStartupScript("UpdateSuccess", js);
    }

    private string GenTaiLieu(DmLoaiTaiLieu objLoaiTaiLieu, DateTime ngayNhap)
    {
        string maTaiLieu = "";
        if (objLoaiTaiLieu != null && objLoaiTaiLieu.LoaiTaiLieuId > 0)
        {
            maTaiLieu += objLoaiTaiLieu.MaLoaiTaiLieu.Trim();
            maTaiLieu += ngayNhap.ToString("yy");
            // Lấy số thứ tự mới nhất của mã tài liệu theo mã loại tài liệu và năm nhập
            string query = "SELECT TOP 1 MaTaiLieu FROM tblDMTaiLieu WHERE LoaiTaiLieuID = " + objLoaiTaiLieu.LoaiTaiLieuId + " AND YEAR(NgayNhap) = " + ngayNhap.Year + " ORDER BY MaTaiLieu DESC";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                string tempMaTaiLieu = listData[0]["MaTaiLieu"].ToString().Trim();
                string index = tempMaTaiLieu.Substring(tempMaTaiLieu.Length - 3, 3);
                if (Library.CheckIsInt32(index))
                {
                    int currentIndex = Library.Int32Convert(index);
                    int nextIndex = currentIndex + 1;
                    if (nextIndex < 10)
                        maTaiLieu += "00" + nextIndex;
                    else if (nextIndex > 9 && nextIndex < 100)
                        maTaiLieu += "0" + nextIndex;
                    else
                        maTaiLieu += nextIndex;
                }
            }
            else
            {
                maTaiLieu += "001";
            }
        }
        return maTaiLieu;
    }

    protected void Delete()
    {
        bool success = false;
        SqlDmTaiLieuProvider pro = new SqlDmTaiLieuProvider(ListName.ConnectionString, false, string.Empty);
        
        if (pro.Delete(Library.Int32Convert(_id)))
        {
            success = true;
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        if (success)
        {
            // Đóng form mà refresh số liệu
            js += "parent.CloseFormUpdate();" + Environment.NewLine;
            js += "parent.Search();" + Environment.NewLine;
        }
        else
        {
            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Có lỗi trong qua trình xóa dữ liệu.\").show();" + Environment.NewLine;
        }
        js += "</script>";
        Page.RegisterStartupScript("DeleteSuccess", js);
    }

    protected void GetListDMLoaiTaiLieu()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            string query = "SELECT LoaiTaiLieuID, TenLoaiTaiLieu FROM tblDMLoaiTaiLieu ORDER BY TenLoaiTaiLieu";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    js += "<option value='" + ht["LoaiTaiLieuID"] + "'>" + ht["TenLoaiTaiLieu"] + "</option>" + Environment.NewLine;
                }
            }
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }
}