﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using System.IO;

public partial class usercontrols_cauhinhhethong : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");
        // Tên chức năng
        string tenchucnang = "Cấu hình hệ thống";
        // Icon CSS Class  
        string IconClass = "iconfa-cogs";

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"
      <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));


        if (!string.IsNullOrEmpty(Request.Form["HienThiMaChuaDuyet"]))
        {
            try
            {
                
              //  ghicauhinh("HienThiMaChuaDuyet", Request.Form["HienThiMaChuaDuyet"]);
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));

                if (Request.Files["AnhBanner"] != null)
                {
                    try
                    {
                        HttpPostedFile fileupload = Request.Files["AnhBanner"];
                        if (fileupload.FileName != "")
                        {
                            string fname = Path.GetFileName(fileupload.FileName);                            
                            fileupload.SaveAs(Server.MapPath(Path.Combine("~/images/", "banner.png")));
                        }
                    }
                    catch 
                    {                        
                    }
                }

              
            }
            catch (Exception ex)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
            }
            




        }
     

    }

    protected string loadcauhinh(string Ma)
    {
        string giatri = "";
        try
        {
            DataTable dt = new DataTable();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT TOP 1 GiaTri FROM tblCauHinhHeThong WHERE MaCauHinh = '" + Ma + "'  ";
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];

            giatri = dt.Rows[0][0].ToString();

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
        }
        catch
        {
            giatri = "";
        }
        return giatri;
    }

    protected void ghicauhinh(string Ma, string GiaTri)
    {
        try
        {

            SqlCommand sql = new SqlCommand();
            sql.CommandText = " UPDATE tblCauHinhHeThong SET GiaTri=@giatri WHERE MaCauHinh = '" + Ma + "'  ";
            sql.Parameters.AddWithValue("@giatri", GiaTri);

            DataAccess.RunActionCmd(sql);

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

        }
        catch
        {

        }
    }

   
}
