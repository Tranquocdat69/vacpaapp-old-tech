﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CodeEngine.Framework.QueryBuilder.Enums;
using HiPT.VACPA.DL;

using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_QTHT_QuanLyTaiLieu : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách tài liệu";
    protected string _listPermissionOnFunc_QuanLyTaiLieu = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    Db _db = new Db(ListName.ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _listPermissionOnFunc_QuanLyTaiLieu = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_QTHT_DMTaiLieu, cm.connstr);
        try
        {
            if (!IsPostBack || Request.Form["hdAction"] == "paging")
            {
                LoadListLoaiTieuChi();
                if (_listPermissionOnFunc_QuanLyTaiLieu.Contains("XEM|"))
                    LoadData();
                else
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem danh sách tài liệu!</div>"));
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }

    /// <summary>
    /// create by HUNGNM - 2014/11/18
    /// Load list class from db
    /// </summary>
    private void LoadData()
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "TaiLieuID", "MaTaiLieu", "LoaiTaiLieu", "TenTaiLieu", "NgayNhap", "FileTaiLieu" });

        try
        {
            _db.OpenConnection();
            List<string> param = new List<string> { "@MaTaiLieu", "@LoaiTaiLieuID", "@CoQuanBanHanh", "@TenTaiLieu", "@TuNgay", "@DenNgay" };
            List<object> value = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value };
            if (!string.IsNullOrEmpty(txtMaTaiLieu.Text))
                value[0] = txtMaTaiLieu.Text;
            if (!string.IsNullOrEmpty(ddlLoaiTaiLieu.SelectedValue))
                value[1] = ddlLoaiTaiLieu.SelectedValue;
            if (!string.IsNullOrEmpty(txtCoQuanBanHanh.Text))
                value[2] = txtCoQuanBanHanh.Text;
            if (!string.IsNullOrEmpty(txtTenTaiLieu.Text))
                value[3] = txtTenTaiLieu.Text;
            if (!string.IsNullOrEmpty(txtTuNgay.Text))
                value[4] = Library.DateTimeConvert(txtTuNgay.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtDenNgay.Text))
                value[5] = Library.DateTimeConvert(txtDenNgay.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            List<Hashtable> listData = _db.GetListData(ListName.Proc_QTHT_SearchDanhSachTaiLieu, param, value);
            if (listData.Count > 0)
            {
                DataRow dataRow;
                foreach (Hashtable ht in listData)
                {
                    dataRow = dt.NewRow();
                    dataRow["TaiLieuID"] = ht["TaiLieuID"];
                    dataRow["MaTaiLieu"] = ht["MaTaiLieu"];
                    dataRow["LoaiTaiLieu"] = ht["LoaiTaiLieu"];
                    dataRow["TenTaiLieu"] = ht["TenTaiLieu"];
                    dataRow["NgayNhap"] = !string.IsNullOrEmpty(ht["NgayNhap"].ToString()) ? Library.DateTimeConvert(ht["NgayNhap"]).ToString("dd/MM/yyyy") : "";
                    dataRow["FileTaiLieu"] = "<a href='/admin.aspx?page=getfile&id=" + dataRow["TaiLieuID"] + "&type=" + ListName.Table_DMTaiLieu + "'>" + ht["TenFileDinhKem"] + "</a>";
                    dt.Rows.Add(dataRow);
                }
            }
            DataView dv = dt.DefaultView;
            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"] + " " + ViewState["sortdirection"];
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            Pager.Items.Clear();
            for (int i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
                hf_TrangHienTai.Value = Request.Form["tranghientai"];
            if (!string.IsNullOrEmpty(hf_TrangHienTai.Value))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(hf_TrangHienTai.Value);
                Pager.SelectedIndex = Convert.ToInt32(hf_TrangHienTai.Value);
            }
            gv_DanhSachTaiLieu.DataSource = objPds;
            gv_DanhSachTaiLieu.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_QuanLyTaiLieu.Contains("XEM|"))
        {
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        }

        if (!_listPermissionOnFunc_QuanLyTaiLieu.Contains("SUA|"))
            Response.Write("$('a[data-original-title=\"Xem/Sửa\"]').remove();");

        if (!_listPermissionOnFunc_QuanLyTaiLieu.Contains("XOA|"))
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");

        if (!_listPermissionOnFunc_QuanLyTaiLieu.Contains("THEM|"))
            Response.Write("$('#btn_them').remove();");

        if (!_listPermissionOnFunc_QuanLyTaiLieu.Contains("XOA|"))
            Response.Write("$('#" + lbtDelete.ClientID + "').remove();");
    }

    protected void gv_DanhSachTaiLieu_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_DanhSachTaiLieu.PageIndex = e.NewPageIndex;
        gv_DanhSachTaiLieu.DataBind();
    }

    protected void gv_DanhSachTaiLieu_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        LoadData();
    }

    #region Gen control in "Search Form"

    private void LoadListLoaiTieuChi()
    {
        try
        {
            _db.OpenConnection();
            ddlLoaiTaiLieu.Items.Clear();
            ddlLoaiTaiLieu.Items.Add(new ListItem("Tất cả", ""));
            string query = "SELECT LoaiTaiLieuID, TenLoaiTaiLieu FROM tblDMLoaiTaiLieu ORDER BY TenLoaiTaiLieu";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    ddlLoaiTaiLieu.Items.Add(new ListItem(ht["TenLoaiTaiLieu"].ToString(), ht["LoaiTaiLieuID"].ToString()));
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    #endregion
    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadData();
    }
    protected void gv_DanhSachTaiLieu_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            string id = e.CommandArgument.ToString();
            Delete(new List<string> { id });
        }
    }
    protected void lbtDelete_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachTaiLieu.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachTaiLieu.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        Delete(listIdDanhSach);
    }

    protected void Delete(List<string> listId)
    {
        bool success = false;
        SqlDmTaiLieuProvider pro = new SqlDmTaiLieuProvider(ListName.ConnectionString, false, string.Empty);
        foreach (string id in listId)
        {
            if (pro.Delete(Library.Int32Convert(id)))
            {
                success = true;
            }
            else
            {
                success = false;
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        if (success)
        {
            js += "CallAlertSuccessFloatRightBottom('Xóa tài liệu thành công!');" + Environment.NewLine;
            LoadData();
        }
        else
        {
            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Có lỗi trong qua trình xóa dữ liệu.\").show();" + Environment.NewLine;
        }
        js += "</script>";
        Page.RegisterStartupScript("DeleteSuccess", js);
    }
}