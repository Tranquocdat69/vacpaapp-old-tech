﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="nhatkysudung.ascx.cs" Inherits="usercontrols_nhatkysudung" %>

<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>


<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

   <form id="Form1" runat="server">
   <h4 class="widgettitle">Nhật ký sử dụng phần mềm</h4>
   <div>
        <div  class="dataTables_length">
        
        
        <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="if (Users_grv_selected!='') confirm_delete_user(Users_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
        <a id="btn_xoatatca"  href="#none" class="btn btn-rounded" onclick="confirm_delete_all();"><i class="iconfa-trash"></i> Xóa tất cả</a>
        <a href="#none" class="btn btn-rounded" onclick="open_user_search();"><i class="iconfa-search"></i> Tìm kiếm</a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
  
        </div>
        
   </div>

<asp:GridView ClientIDMode="Static" ID="Users_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="Users_grv_PageIndexChanging" onsorting="Users_grv_Sorting"    
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval("NhatKyHeThongID")%>" />
                  </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="TenChucNang" HeaderText="Tên chức năng" 
                  SortExpression="TenChucNang" />
              <asp:BoundField DataField="TenDangNhap" HeaderText="Tên tài khoản" 
                  SortExpression="TenDangNhap" />
              <asp:BoundField DataField="HanhDong" HeaderText="Hành động" 
                  SortExpression="HanhDong" />
              <asp:BoundField DataField="ThoiGian" HeaderText="Thời gian" 
                  SortExpression="ThoiGian" DataFormatString="{0:hh:mm:ss - dd/MM/yyyy}" />              
              <asp:TemplateField HeaderText='Thao tác' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                      <a onclick="confirm_delete_user(<%# Eval("NhatKyHeThongID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xóa"  rel="tooltip" class="btn" ><i class="iconsweets-trashcan" ></i></a>                                                   

                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>


          </Columns>
</asp:GridView>

<div class="dataTables_info" id="dyntable_info" >
<div class="pagination pagination-mini">Chuyển đến trang: 
                            <ul>
                               
                                <li><a href="#" onclick="$('#tranghientai').val(0); $('#user_search').submit();"><< Đầu tiên</a></li>
                                <li><a href="#"  onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager').val()-1); $('#user_search').submit();}" >< Trước</a></li>
                                <li><a style=" border: none;  background-color:#eeeeee"><asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" 
                                        style=" width:55px; height:22px; "
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList></a>  </li>
                                <li style="margin-left:5px; "><a href="#"  onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) {$('#tranghientai').val(parseInt($('#Pager').val())+1); $('#user_search').submit();}  ;"   style="border-left: 1px solid #ccc;">Sau ></a></li>
                                <li><a href="#"  onclick="$('#tranghientai').val($('#Pager option').length-1); $('#user_search').submit();">Cuối cùng >></a></li>
                            </ul>
                        </div>                      
</div>


</form>





<script type="text/javascript">

<% annut(); %>
    // Array ID được check
    var Users_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#Users_grv .checkall").bind("click", function () {
            Users_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#Users_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });

        $('#Users_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(Users_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });
    });


    
    function confirm_delete_all() {

        $("#div_delete_all").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                    window.location = 'admin.aspx?page=nhatkysudung&act=deleteall';
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_delete_all').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_delete_all').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    function confirm_delete_user(idxoa) {

        $("#div_user_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                    window.location = 'admin.aspx?page=nhatkysudung&act=delete&id=' + idxoa;
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

   


   
    // Tìm kiếm
    function open_user_search() {
        var timestamp = Number(new Date());

        $("#div_user_search").dialog({
            resizable: true,
            width: 800,
            height: 300,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm nhật ký sử dụng phần mềm</b>",
            modal: true,
            bgiframe: true,
       
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_user_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('.btn-group').length > 0) jQuery('.btn-group').tooltip({ selector: "a[rel=tooltip]" });




</script>

<div id="div_user_add" >
</div>

<div id="div_user_search" style="display:none" >
<form id="user_search" method="post" enctype="multipart/form-data" action="">
<table  width="100%" border="0" class="formtbl"  >
    <tr>
        <td style="width:120px"><label>Tên tài khoản: </label></td>
        <td>
            <select id="timkiem_TenTaiKhoan" name="timkiem_TenTaiKhoan" >
                <option value="ALL">Tất cả tài khoản</option>     
                <asp:PlaceHolder ID="timkiem_TenTaiKhoan" runat="server"></asp:PlaceHolder>           
            </select></td>
    </tr>
    <tr>
        <td><label>Thời gian thao tác: </label></td>
        <td align="right">
        <nobr>
        Từ ngày: 
        <input type="text" name="timkiem_TuNgay" id="timkiem_TuNgay" class="input-xlarge" style="width:100px;">
        Đến ngày:
        <input type="text" name="timkiem_DenNgay" id="timkiem_DenNgay" class="input-xlarge" style="width:100px;">
        </nobr>
        </td>
    </tr>  

    <tr>
        <td><label>Chức năng thao tác:</label></td>
        <td><select id="timkiem_ChucNang" name="timkiem_ChucNang" >
                <option value="ALL">Tất cả chức năng</option>     
                <asp:PlaceHolder ID="timkiem_ChucNang" runat="server"></asp:PlaceHolder>           
            </select>
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
        </td>
    </tr>


</table>
</form>
</div>


<script language="javascript">


    $('#timkiem_TenTaiKhoan').val('<%=Request.Form["timkiem_TenTaiKhoan"]%>');
    $('#timkiem_TuNgay').val('<%=Request.Form["timkiem_TuNgay"]%>');
    $('#timkiem_DenNgay').val('<%=Request.Form["timkiem_DenNgay"]%>');
    $('#timkiem_ChucNang').val('<%=Request.Form["timkiem_ChucNang"]%>');


     jQuery("#user_search").validate({
        rules: {
            timkiem_TuNgay: {
                required: false,
                dateITA:true
            },
            timkiem_DenNgay: {
                required: false,
                dateITA:true
            },

           
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery("#thongbaoloi_form_themuser").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_themuser").hide()
            }
        }
    });

          $.datepicker.setDefaults($.datepicker.regional['vi']);


                                        $(function () {
                                            $("#timkiem_TuNgay,#timkiem_DenNgay").datepicker({
                                                changeMonth: true,
                                                changeYear: true,
                                                dateFormat: "dd/mm/yy"
                                            }).datepicker();


                                        });


 </script>

<div id="div_user_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?</p>

</div>

<div id="div_delete_all" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa toàn bộ nhật ký sử dụng phần mềm?</p>

</div>







