﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="account_add.ascx.cs" Inherits="usercontrols_account_add" %>

<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<% 
    if (!String.IsNullOrEmpty(cm.Admin_NguoiDungID))
    {
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyTaiKhoanKH", cm.connstr).Contains("THEM|"))
        {
            Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");
            return;
        }
    }
    
    %>


<% if (string.IsNullOrEmpty(Request.QueryString["mode"]))
   {  %>

<div class="widgetbox">
  <h4 class="widgettitle">Đăng ký tài khoản sử dụng hệ thống Dịch vụ công trực tuyến<a class="close">×</a> <a class="minimize">–</a></h4>
  <div class="widgetcontent">
  <% } %>
    <form id="form_account_add" clientidmode="Static" runat="server"   method="post">

    <div id="thongbaoloi_form_themuser" name="thongbaoloi_form_themuser" style="display:none" class="alert alert-error"></div>

      <table id="Table1" width="100%" border="0" class="formtbl" runat="server">
        <tr class="trbgr">
          <td colspan="7"><h6>1. Thông tin đơn vị / tổ chức</h6></td>
        </tr>
        <tr>
          <td><asp:Label ID="Label1" runat="server" Text="Tên đơn vị: "></asp:Label>
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="6"><input type="text" name="TenToChuc" id="TenToChuc" value="<%=TenDonVi%>"></td>
        </tr>
        
        <tr>
          <td><asp:Label ID="Label3" runat="server" Text='Số quyết định thành lập:'></asp:Label></td>
          <td style="width:20%"><input type="text" name="SoDKKD" id="SoDKKD" value="<%=SoQD%>"></td>
          <td><asp:Label ID="Label4" runat="server" Text='Ngày quyết định:'></asp:Label></td>
          <td style="width:20%" ><input type="text" name="NgayCapDKKD" id="NgayCapDKKD"  value="<%=NgayQD%>"></td>
          <td colspan="2" ><asp:Label ID="Label5" runat="server" Text='Cơ quan ra QĐ:' ></asp:Label></td>
          <td style="width:20%"><input type="text" name="NoiCapDKKD" id="NoiCapDKKD"  value="<%=CoQuanRaQD%>"></td>
        </tr>
        <tr>
          <td><asp:Label ID="Label6" runat="server" Text='Địa chỉ:'></asp:Label></td>
          <td colspan="6">
          <input type="text" name="DiaChi" id="DiaChi"  value="<%=DiaChi%>" /></td>
        </tr>
        
         <tr>
          <td><asp:Label ID="Label7" runat="server" Text='Tỉnh/Thành phố:'></asp:Label></td>
          <td><select name="TinhID_ToChuc" id="TinhID_ToChuc">
          <% cm.Load_ThanhPho(TPID); %>
          </select></td>
          <td><asp:Label ID="Label8" runat="server" Text='Quận/Huyện:'></asp:Label></td>
          <td ><select name="HuyenID_ToChuc" id="HuyenID_ToChuc">
               <%  
              try
              {
                  cm.Load_QuanHuyen(QHID, TPID);
              }
              catch 
              {
                  
              }
              
              %>
          </select></td>
          <td colspan="2" ><asp:Label ID="Label9" runat="server" Text='Phường/Xã:'></asp:Label></td>
          <td><select name="XaID_ToChuc" id="XaID_ToChuc">
               <%  
                     try
                     {
                         cm.Load_PhuongXa(PXID,QHID);
                     }
                     catch
                     {

                     }
              
              %>  
          </select></td>
        </tr>
        
        
         <tr>
          <td><asp:Label ID="Label10" runat="server" Text='Người đại diện:'></asp:Label></td>
          <td><input type="text" name="NguoiDaiDien" id="NguoiDaiDien" value="<%=NguoiDaiDien%>"></td>
          <td><asp:Label ID="Label11" runat="server" Text='Điện thoại:'  ></asp:Label></td>
          <td ><input type="text" name="DienThoaiToChuc" id="DienThoaiToChuc" value="<%=DienThoai%>"></td>
          <td colspan="2" ><asp:Label ID="Label12" runat="server" Text='Fax:' ></asp:Label></td>
          <td><input type="text" name="FaxToChuc" id="FaxToChuc" value="<%=Fax%>"></td>
        </tr>
        
         <tr class="trbgr">
          <td colspan="7"><h6>2. Thông tin người đăng ký tài khoản</h6></td>
        </tr>
        
       <tr>
          <td><asp:Label ID="Label13" runat="server" Text='Họ và tên:'></asp:Label>
          <asp:Label ID="Label14" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td><input type="text" name="HoVaTenNguoiDangKy" id="HoVaTenNguoiDangKy"></td>
          <td><asp:Label ID="Label15" runat="server" Text='Bộ phận:'></asp:Label></td>
          <td ><input type="text" name="PhongBan" id="PhongBan"></td>
          <td colspan="2" ><asp:Label ID="Label17" runat="server" Text='Chức vụ:'></asp:Label></td>
          <td><input type="text" name="ChucVu" id="ChucVu"></td>
        </tr>
        
        <tr>
          <td>Điện thoại:</td>
          <td><input type="text" name="DienThoaiNguoiDangKy" id="DienThoaiNguoiDangKy"></td>
          <td>Email:
          <asp:Label ID="Label20" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td ><input type="text" name="Email" id="Email"></td>
          <td colspan="3" >&nbsp;</td>
        </tr>
        
        <tr>
          <td>Tên tài khoản:
          <asp:Label ID="Label16" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="2"><input type="text" name="TenTaiKhoan" id="TenTaiKhoan"></td>
          <td >&nbsp;</td>
          <td >Số CMND:</td>
          <td colspan="2" ><input type="text" name="SoCMND" id="SoCMND"></td>
        </tr>
        
        <tr>
          <td>Mật khẩu:
          <asp:Label ID="Label18" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="2"><input type="password" name="MatKhau" id="MatKhau"></td>
          <td >&nbsp;</td>
          <td >Ngày cấp:</td>
          <td colspan="2" ><input type="text" name="NgayCap" id="NgayCap"></td>
        </tr>
        
        <tr>
          <td>Nhập lại mật khẩu:
          <asp:Label ID="Label19" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="2"><input type="password" name="MatKhauNhapLai" id="MatKhauNhapLai"></td>
          <td >&nbsp;</td>
          <td >Nơi cấp:</td>
          <td colspan="2" ><input type="text" name="NoiCap" id="NoiCap"></td>
        </tr>                        
        


        <tr id="nut" runat="server">
          <td colspan="7" align="center"><p>&nbsp;</p>            
          
          
          <a  class="btn btn-rounded"><i class="iconfa-ok"></i> <asp:Button ID="Button2" Text="Đăng ký" runat="server" /> </a>    
          
                  <a  class="btn btn-rounded"><i class="iconfa-minus-sign"></i> <input type="reset" value="Nhập lại" /> </a>       
          
          </td>
        </tr>
        
        
        
        
        
        
      </table>
    </form>

<% if (string.IsNullOrEmpty(Request.QueryString["mode"]))
   {  %>

  </div>
</div>
 <%} %>

           
                                                    
<script type="text/javascript">
       

        function submitform() {        
             jQuery("#form_account_add").submit();
        }



    jQuery("#form_account_add").validate({
        rules: {
            HoVaTenNguoiDangKy: {
                required: true
            },
            TenDonVi: {
                required: true
            },

            TenTaiKhoan: {
                required: true,
                minlength: 6
            },
            MatKhau: {
                required: true,
                minlength: 6
            },
            MatKhauNhapLai: {
                required: true,
                minlength: 6,
                equalTo: "#MatKhau"
            },
            Email: {
                required: true,
                email: true
            },
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery("#thongbaoloi_form_themuser").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_themuser").hide()
            }
        }
    });

    <% cm.chondiaphuong_script("ToChuc",0); %>
    

    $.datepicker.setDefaults($.datepicker.regional['vi']);


    $(function () {
        $("#NgayCapDKKD,#NgayCap").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"          
        }).datepicker("option", "maxDate", '+0m +0w -1d');

        
    });


    

</script>