﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="role_add.ascx.cs" Inherits="usercontrols_role_add" %>

<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>


<script src="js/jquery.stickytableheaders.min.js"></script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyNhomQuyen", cm.connstr).Contains("THEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <form id="form_account_add" clientidmode="Static" runat="server"   method="post">

    <div id="thongbaoloi_form_themuser" name="thongbaoloi_form_themuser" style="display:none" class="alert alert-error"></div>

      <table id="Table1" width="100%" border="0" class="formtbl" >
        <tr class="trbgr">
          <td colspan="7"><h6>Thông tin phân quyền:</h6></td>
        </tr>
        <tr>
          <td><asp:Label ID="Label1" runat="server" Text="Tên vai trò: "></asp:Label>
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="6"><input type="text" name="TenNhomQuyen" id="TenNhomQuyen"></td>
        </tr>
        
        </table>
        
        <% form_phanquyen(); %>
        
    </form>



           
                                                    
<script type="text/javascript">
       

        function submitform() {        
                jQuery("#form_account_add").submit();
        }



    jQuery("#form_account_add").validate({
        rules: {
            TenNhomQuyen: {
                required: true
            },
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery(".alert-success").remove();
                jQuery("#thongbaoloi_form_themuser").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_themuser").hide()
            }
        }
    });

   


    

</script>


