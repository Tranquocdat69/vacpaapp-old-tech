﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using HiPT.VACPA.DL;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Configuration;

public partial class usercontrols_account_add : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public string TenDonVi = "";
    public string DienThoai = "";
    public string Fax = "";
    public string DiaChi = "";
    public string NguoiDaiDien = "";
    public string SoQD = "";
    public string NgayQD = "";
    public string CoQuanRaQD = "";
    public string TPID = "00";
    public string QHID = "000";
    public string PXID = "00000";



    
    protected void Page_Load(object sender, EventArgs e)
    {

   

     //   if (string.IsNullOrEmpty(cm.Admin_NguoiDungID))
     //   if (Session["Khachhang_DuocDangKyTaiKhoan"] != "OK") Response.Redirect("default.aspx");
        

        // Tên chức năng
        string tenchucnang = "Đăng ký tài khoản sử dụng Dịch vụ công trực tuyến";
        // Icon CSS Class  
        string IconClass = "iconfa-group";
        nut.Visible = false;
        if (string.IsNullOrEmpty(Request.QueryString["mode"]))
        {
            nut.Visible = true;
            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        }

        // Thêm tài khoản

        if (!string.IsNullOrEmpty(Request.Form["TenTaiKhoan"]))
        {
            try
            {
               

                if (check_user(Request.Form["TenTaiKhoan"], Request.Form["Email"]))
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Tài khoản đã tồn tại trong hệ thống! Hãy chọn một tên tài khoản hoặc địa chỉ email khác!</div>"));
                    return;
                }

                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"INSERT INTO tblDMKhachHang
           (TenToChuc
           ,SoDKKD
           ,NgayCapDKKD
           ,NoiCapDKKD
           ,DiaChi
           ,DBHC_TinhID_ToChuc
           ,DBHC_HuyenID_ToChuc
           ,DBHC_XaID_ToChuc
           ,NguoiDaiDien
           ,DienThoaiToChuc
           ,FaxToChuc
           ,HoVaTenNguoiDangKy
           ,PhongBan
           ,ChucVu
           ,DienThoaiNguoiDangKy
           ,Email
           ,TenTaiKhoan
           ,MatKhau           

           ,SoCMND
           ,NgayCap
           ,NoiCap

)
     VALUES
           (@TenToChuc
           ,@SoDKKD
           ,@NgayCapDKKD
           ,@NoiCapDKKD
           ,@DiaChi
           ,@DBHC_TinhID_ToChuc
           ,@DBHC_HuyenID_ToChuc
           ,@DBHC_XaID_ToChuc
           ,@NguoiDaiDien
           ,@DienThoaiToChuc
           ,@FaxToChuc
           ,@HoVaTenNguoiDangKy
           ,@PhongBan
           ,@ChucVu
           ,@DienThoaiNguoiDangKy
           ,@Email
           ,@TenTaiKhoan
           ,@MatKhau

           ,@SoCMND
           ,@NgayCap
           ,@NoiCap
)";

                sql.Parameters.AddWithValue("@TenToChuc", Request.Form["TenToChuc"]);

                sql.Parameters.AddWithValue("@SoCMND", Request.Form["SoCMND"]);
                sql.Parameters.AddWithValue("@NoiCap", Request.Form["NoiCap"]);

                sql.Parameters.AddWithValue("@SoDKKD", Request.Form["SoDKKD"]);

                if (!String.IsNullOrEmpty(Request.Form["NgayCapDKKD"]) && Request.Form["NgayCapDKKD"].Length == 10)
                   sql.Parameters.AddWithValue("@NgayCapDKKD", DateTime.ParseExact(Request.Form["NgayCapDKKD"], "dd/MM/yyyy", null));                                     
                else
                    sql.Parameters.AddWithValue("@NgayCapDKKD", DBNull.Value);

                if (!String.IsNullOrEmpty(Request.Form["NgayCap"]) && Request.Form["NgayCap"].Length == 10)
                    sql.Parameters.AddWithValue("@NgayCap", DateTime.ParseExact(Request.Form["NgayCap"], "dd/MM/yyyy", null));
                else
                    sql.Parameters.AddWithValue("@NgayCap", DBNull.Value);


           


                sql.Parameters.AddWithValue("@NoiCapDKKD", Request.Form["NoiCapDKKD"]);
                sql.Parameters.AddWithValue("@DiaChi", Request.Form["DiaChi"]);
                sql.Parameters.AddWithValue("@DBHC_TinhID_ToChuc", Request.Form["TinhID_ToChuc"]);
                sql.Parameters.AddWithValue("@DBHC_HuyenID_ToChuc", Request.Form["HuyenID_ToChuc"]);
                sql.Parameters.AddWithValue("@DBHC_XaID_ToChuc", Request.Form["XaID_ToChuc"]);
                sql.Parameters.AddWithValue("@NguoiDaiDien", Request.Form["NguoiDaiDien"]);
                sql.Parameters.AddWithValue("@DienThoaiToChuc", Request.Form["DienThoaiToChuc"]);
                sql.Parameters.AddWithValue("@FaxToChuc", Request.Form["FaxToChuc"]);
                sql.Parameters.AddWithValue("@HoVaTenNguoiDangKy", Request.Form["HoVaTenNguoiDangKy"]);
                sql.Parameters.AddWithValue("@PhongBan", Request.Form["PhongBan"]);
                sql.Parameters.AddWithValue("@ChucVu", Request.Form["ChucVu"]);
                sql.Parameters.AddWithValue("@DienThoaiNguoiDangKy", Request.Form["DienThoaiNguoiDangKy"]);
                sql.Parameters.AddWithValue("@Email", Request.Form["Email"]);
                sql.Parameters.AddWithValue("@TenTaiKhoan", Request.Form["TenTaiKhoan"]);
                sql.Parameters.AddWithValue("@MatKhau", Request.Form["MatKhau"]);

               
                foreach (SqlParameter Parameter in sql.Parameters)
                {
                    if (Parameter.Value == null)
                    {
                        Parameter.Value = DBNull.Value;
                    }
                }

                DataSet ds = DataAccess.RunCMDGetDataSet(sql);

                cm.ghilog("QuanLyTaiKhoanKH", "Thêm tài khoản hội viên \"" + Request.Form["TenTaiKhoan"] + "\"");
    
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;
                ds = null;
         
            
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
             


            }
            catch (Exception ex)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
            }

        }

    }

    bool check_user(string login, string email)
    {
        bool check = false;
        try
        {

            SqlCommand sql = new SqlCommand();

            if (!string.IsNullOrEmpty(email))
                sql.CommandText = "SELECT KhachHangID FROM tblDMKhachHang WHERE UPPER(TenTaiKhoan)=@TenTaiKhoan OR UPPER(Email)=@Email";
            else
                sql.CommandText = "SELECT KhachHangID FROM tblDMKhachHang WHERE UPPER(TenTaiKhoan)=@TenTaiKhoan";
            sql.Parameters.AddWithValue("@TenTaiKhoan", login.ToUpper());
            sql.Parameters.AddWithValue("@Email", email.ToUpper());

            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            if (ds.Tables[0].Rows.Count > 0) check = true;
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;

        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
        return check;
    }




    public static string MD5(string password)
    {
        byte[] textBytes = System.Text.Encoding.Default.GetBytes(password);
        try
        {
            System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
            cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hash = cryptHandler.ComputeHash(textBytes);
            string ret = "";
            foreach (byte a in hash)
            {
                if (a < 16)
                    ret += "0" + a.ToString("x");
                else
                    ret += a.ToString("x");
            }
            return ret;
        }
        catch
        {
            throw;
        }
    }


  

}