﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="user_edit.ascx.cs" Inherits="usercontrols_user_edit" %>

<script type="text/javascript" src="js/chosen.jquery.min.js"></script>

<style>
     .require {color:Red;}
     
</style>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyTaiKhoanCB", cm.connstr).Contains("SUA|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

<form class="stdform stdform2" id="form_themuser" name="form_themuser" method="post" action="" enctype="multipart/form-data">

<div id="thongbaoloi_form_themuser" class="alert alert-error" style="display:none;">

</div>

<table id="Table1" width="100%" border="0" class="formtbl" runat="server">
    <tr>
        <td style=" min-width:150px"><label>Họ và tên: <span class="require">*</span></label></td>
        <td><input type="text" name="HoVaTen" id="HoVaTen" class="input-xlarge"></td>
    </tr>
    <tr>
        <td><label>Tên đăng nhập: <span class="require">*</span></label></td>
        <td><input type="text" name="TenDangNhap" id="TenDangNhap" class="input-xlarge" readonly="readonly"></td>
    </tr>
    <tr>
        <td><label>Mật khẩu: </label></td>
        <td><input type="password" name="MatKhau" id="MatKhau" class="input-xlarge"><label id="Pass"></label></td>
    </tr>
    <tr>
        <td><label>Nhập lại mật khẩu: </label></td>
        <td><input type="password" name="MatKhauNhapLai" id="MatKhauNhapLai" class="input-xlarge"></td>
    </tr>

        <tr>
        <td><label>Điện thoại:</label></td>
        <td><input type="text" name="DienThoai" id="DienThoai" class="input-xlarge"></td>
    </tr>

    <tr>
        <td><label>Email: <span class="require">*</span></label></td>
        <td><input type="text" name="Email" id="Email" class="input-xlarge"></td>
    </tr>
    <tr>
        <td><label>Chức vụ:</label></td>
        <td><input type="text" name="ChucVu" id="ChucVu" class="input-xlarge"></td>
    </tr>
    <tr>
        <td><label>Bộ phận công tác:</label></td>
        <td> <select name="BoPhanCongTac" id="BoPhanCongTac"  style="width:430px" >
                                    <asp:PlaceHolder ID="BoPhanCongTac" runat="server"></asp:PlaceHolder>
                                </select></td>
    </tr>
    <tr>
        <td><label>Nhóm quyền: </label></td>
        <td><select name="NhomQuyenID" id="NhomQuyenID"  style="width:430px" class="chzn-select"  multiple="multiple">
                                    <asp:PlaceHolder ID="NhomQuyenID" runat="server"></asp:PlaceHolder>
                                </select></td>
    </tr>
</table>




  

     </form>                       
                            
                           
                 
                                                    
<script type="text/javascript">
        jQuery(".chzn-select").chosen();

        function submitform() {        
                jQuery("#form_themuser").submit();
        }

   <% Load_user(); %>

    jQuery.validator.addMethod("CheckMatKhau", function (value, element, param) {
        if (value != "") {
            var strongRegex = new RegExp("^(?=.*[a-zA-Z])(?=.*[0-9])");

            return strongRegex.test(value);
        }
        else
            return true;

    }, "Mật khẩu phải bao gồm cả chữ và số.");

    jQuery("#form_themuser").validate({
        rules: {
            HoVaTen: {
                required: true
            },

            TenDangNhap: {
                required: true,
                minlength: 6
            },
            MatKhau: {
                required: false,
                minlength: 6,
                CheckMatKhau: true
            },
            MatKhauNhapLai: {
                required: false,
                minlength: 6,
                equalTo: "#MatKhau"
            },
            Email: {
                required: true,
                email: true
            },
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery("#thongbaoloi_form_themuser").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_themuser").hide()
            }
        }
    });





</script>
                    
               


