﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;


public partial class usercontrols_role_add : System.Web.UI.UserControl
{
    DataSet dscn;
    DataSet dstt;
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
  
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {


            SqlCommand sql = new SqlCommand();
            //sql.CommandText = "SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang ORDER BY ISNULL(ChucNangCapTrenID,ChucNangID), ChucNangCapTrenID ";
            sql.CommandText = @"SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangID='QuanTriHeThong' 
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangCapTrenID='QuanTriHeThong' 
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangID='QuanTriDanhMuc' 
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangCapTrenID='QuanTriDanhMuc' 
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangID='QuanLyHoiVien' 
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangCapTrenID='QuanLyHoiVien'
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangID='QuanLyDaoTaoCNKT' 
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangCapTrenID='QuanLyDaoTaoCNKT'
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangID='KiemSoatChatLuong' 
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangCapTrenID='KiemSoatChatLuong'
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangID='ThanhToan' 
UNION ALL SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang WHERE ChucNangCapTrenID='ThanhToan'";
            dscn = DataAccess.RunCMDGetDataSet(sql);

            sql = new SqlCommand();
            sql.CommandText = "SELECT TenThaoTac,MaThaoTac FROM tblDMThaoTac ORDER BY ThaoTacID";
            dstt = DataAccess.RunCMDGetDataSet(sql);

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;



            if (!string.IsNullOrEmpty(Request.Form["TenNhomQuyen"]))
            {
                int i = 0; int j = 0;
                Int32 idnhomquyen;
                NhomQuyen nhomquyen = new NhomQuyen();
                string quyen = "";
                SqlNhomQuyenProvider nhomquyen_provider = new SqlNhomQuyenProvider(cm.connstr, true, "");

                nhomquyen.TenNhomQuyen = Request.Form["TenNhomQuyen"];
                nhomquyen_provider.Insert(nhomquyen);

                idnhomquyen = Convert.ToInt32(nhomquyen.EntityTrackingKey.Replace("NhomQuyen|", ""));

                NhomQuyenChiTiet nhomquyenchitiet = new NhomQuyenChiTiet();
                SqlNhomQuyenChiTietProvider nhomquyenchitiet_provider = new SqlNhomQuyenChiTietProvider(cm.connstr, true, "");

                for (i = 0; i < dscn.Tables[0].Rows.Count; i++)
                {
                    quyen = "";
                    for (j = 0; j < dstt.Tables[0].Rows.Count; j++)
                    {
                        if (!string.IsNullOrEmpty(Request.Form[dstt.Tables[0].Rows[j][1] + "_" + i]))
                            quyen += Request.Form[dstt.Tables[0].Rows[j][1] + "_" + i] + "|";                        
                    }

                    nhomquyenchitiet.NhomQuyenId = idnhomquyen;
                    nhomquyenchitiet.Quyen = quyen;
                    nhomquyenchitiet.ChucNangId = dscn.Tables[0].Rows[i][1].ToString();
                    nhomquyenchitiet_provider.Insert(nhomquyenchitiet);
                    
                }

                cm.ghilog("QuanLyNhomQuyen", "Thêm nhóm quyền \"" + Request.Form["TenNhomQuyen"] + "\"");

                nhomquyen = null;
                nhomquyen_provider = null;
                nhomquyenchitiet = null;
                nhomquyenchitiet_provider = null;

                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));

            }


           
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    protected void form_phanquyen()
    {
        int i = 0; int j = 0;

        string checkallscript = "<script>";

        Response.Write("<table width='100%' border='0' class='formtbl' id='pqtable' >");



        Response.Write("<thead  ><tr>");
        Response.Write("<th  ><b>Tên chức năng</b></th>");

        for (i = 0; i < dstt.Tables[0].Rows.Count; i++)
        {
            Response.Write("<th align='center'  ><b>" + dstt.Tables[0].Rows[i][0] + "</b><br><input type='checkbox' id='checkall_" + dstt.Tables[0].Rows[i][1] + "'></th>");
            checkallscript += @"
        $(""#checkall_" + dstt.Tables[0].Rows[i][1] + @""").click(function() {
            for (i=0;i<" + dscn.Tables[0].Rows.Count + @";i++) 
             {
                checkBoxes = $(""#" + dstt.Tables[0].Rows[i][1] + @"_""+i);
        		checkBoxes.prop(""checked"", $(""#checkall_" + dstt.Tables[0].Rows[i][1] + @""").prop(""checked""));        		
             }     
        });
        ";
        }

        checkallscript += @"

$(""#pqtable"").stickyTableHeaders();

</script>";


        Response.Write("</tr></thead><tbody >  ");

        

        for (i = 0; i < dscn.Tables[0].Rows.Count; i++)
        {
            Response.Write("<tr>");

            if (dscn.Tables[0].Rows[i][2] != DBNull.Value)
            {

                Response.Write("<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dscn.Tables[0].Rows[i][0] + "</td>");

                for (j = 0; j < dstt.Tables[0].Rows.Count; j++)
                {
                    Response.Write("<td align='center'><input type='checkbox' id='" + dstt.Tables[0].Rows[j][1] + "_" + i + "' name='" + dstt.Tables[0].Rows[j][1] + "_" + i + "' value='" + dstt.Tables[0].Rows[j][1] + "'></td>");
                }
            }
            else
            {
                Response.Write("<td style='font-size:14px; font-weight:bold; background-color:#C1E4F5;' colspan='" + (dstt.Tables[0].Rows.Count + 1) + "'>" + dscn.Tables[0].Rows[i][0] + "</td>");

            }

            Response.Write("</tr></tbody>");
        }

    
        Response.Write("</table>");
        Response.Write(checkallscript);


    }
}