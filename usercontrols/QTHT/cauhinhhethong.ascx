﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cauhinhhethong.ascx.cs" Inherits="usercontrols_cauhinhhethong" %>


<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<%
    if (!String.IsNullOrEmpty(cm.Admin_NguoiDungID))
    {
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CauHinhHeThong", cm.connstr).Contains("XEM|"))
        {
            Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");
            return;
        }
    }

 %>

 <div class="widgetbox">
                <h4 class="widgettitle">Thiết lập thông tin cấu hình hệ thống</h4>
                <div class="widgetcontent nopadding" >
                    <form class="stdform stdform2" method="post" enctype="multipart/form-data">
                   
                            
                           


                                    <p>
                                <label>Ảnh banner: <small>Ảnh hiển thị trên banner phía trên của website. Hỗ trợ định dạng PNG, ấn Ctrl+F5 từ trình duyệt để thấy ảnh mới sau khi tải banner khác lên.</small></label>
                                <span class="field">
                                    <input id="AnhBanner" name="AnhBanner" type="file"  style="width:100%" />
                                <br><br><br><br>
                                </span>
                                
                            </p>
                            
                                                    
                            <p class="stdformbutton">
                                <button class="btn btn-primary">Ghi cấu hình</button>
                                
                            </p>
                    
                </div><!--widgetcontent-->
            </div><!--widget-->


          

