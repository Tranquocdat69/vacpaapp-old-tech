﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Globalization;

public partial class usercontrols_nhatkysudung : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected void Page_Load(object sender, EventArgs e)
    {
        Commons cm = new Commons();
        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

        // Tên chức năng
        string tenchucnang = "Nhật ký sử dụng phần mềm";
        // Icon CSS Class  
        string IconClass = "iconfa-list-alt";

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyTaiKhoanKH", cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            Form1.Visible = false;
            return;
        }




        ///////////

        load_users();
        loaddropdown();

       

        try
        {
            if (Request.QueryString["act"] == "delete")
            {
                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "NhatKyHeThong", cm.connstr).Contains("XOA|"))
                {
                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "DELETE FROM tblNhatKyHeThong WHERE NhatKyHeThongID IN (" + Request.QueryString["id"] + ")";
                    DataAccess.RunActionCmd(sql);
                   
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    Response.Redirect("admin.aspx?page=nhatkysudung");
                }
            }


            if (Request.QueryString["act"] == "deleteall")
            {
                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "NhatKyHeThong", cm.connstr).Contains("XOA|"))
                {
                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "truncate table tblNhatKyHeThong ";
                    DataAccess.RunActionCmd(sql);

                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    Response.Redirect("admin.aspx?page=nhatkysudung");
                }
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }

    protected void load_users()
    {
        try
        {
            Commons cm = new Commons();
            // Tìm kiếm
            SelectQueryBuilder query = new SelectQueryBuilder();
            query.SelectFromTable("tblNhatKyHeThong");
            query.SelectColumns("tblNhatKyHeThong.NhatKyHeThongID", "tblNhatKyHeThong.TenDangNhap", "tblNhatKyHeThong.HanhDong", "tblNhatKyHeThong.ThoiGian", "tblDMChucNang.TenChucNang");
            query.AddJoin(JoinType.LeftJoin, "tblDMChucNang", "ChucNangID", Comparison.Equals, "tblNhatKyHeThong", "ChucNang");
            query.AddOrderBy("NhatKyHeThongID", Sorting.Descending);
            query.TopRecords = 1000;

            if ((Request.Form["timkiem_TenTaiKhoan"] != "ALL") && (!string.IsNullOrEmpty(Request.Form["timkiem_TenTaiKhoan"])))
                query.AddWhere("TenDangNhap", Comparison.Equals, Request.Form["timkiem_TenTaiKhoan"] );

            if ((Request.Form["timkiem_ChucNang"] != "ALL") && (!string.IsNullOrEmpty(Request.Form["timkiem_ChucNang"])))
                query.AddWhere("ChucNang", Comparison.Equals,Request.Form["timkiem_ChucNang"] );

            
            if (!string.IsNullOrEmpty(Request.Form["timkiem_TuNgay"]))
                query.AddWhere("ThoiGian", Comparison.GreaterOrEquals, DateTime.ParseExact(Request.Form["timkiem_TuNgay"] + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null).ToString("yyyy-MM-dd HH:mm:ss"));

            if (!string.IsNullOrEmpty(Request.Form["timkiem_DenNgay"]))
                query.AddWhere("ThoiGian", Comparison.LessOrEquals, DateTime.ParseExact(Request.Form["timkiem_DenNgay"] + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null).ToString("yyyy-MM-dd HH:mm:ss"));

            SqlCommand sql = new SqlCommand();
            sql.CommandText = query.BuildQuery();
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;

            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;
            int i;
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "NhatKySuDung" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "NhatKyHeThong", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "NhatKyHeThong", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "NhatKyHeThong", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "NhatKyHeThong", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
            Response.Write("$('#btn_xoatatca').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "NhatKyHeThong", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }
    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }

    protected void loaddropdown()
    {
        int i;
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "Select TenDangNhap From tblNguoiDung ORDER BY TenDangNhap ";
        DataSet ds = new DataSet();
        ds = DataAccess.RunCMDGetDataSet(sql);

        for (i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            timkiem_TenTaiKhoan.Controls.Add(new LiteralControl("<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][0].ToString() + "</option>"));
        }


        sql = new SqlCommand();
        sql.CommandText = "SELECT TenChucNang,ChucNangID,ChucNangCapTrenID FROM tblDMChucNang ORDER BY ISNULL(ChucNangCapTrenID,ChucNangID), ChucNangCapTrenID ";
        ds = new DataSet();
        ds = DataAccess.RunCMDGetDataSet(sql);

        for (i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (ds.Tables[0].Rows[i]["ChucNangCapTrenID"]== DBNull.Value)
                timkiem_ChucNang.Controls.Add(new LiteralControl("<optgroup label='" + ds.Tables[0].Rows[i]["TenChucNang"].ToString() + "'>"));
                

            timkiem_ChucNang.Controls.Add(new LiteralControl("<option value='" + ds.Tables[0].Rows[i]["ChucNangID"].ToString() + "'>" + ds.Tables[0].Rows[i]["TenChucNang"].ToString() + "</option>"));
        }


        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds.Dispose();


        

    }
}