﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QTHT_QuanLyTaiLieu_Update.ascx.cs" Inherits="usercontrols_QTHT_QuanLyTaiLieu_Update" %>

<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<form id="Form1" name="Form1" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<input type="hidden" id="hdAction" name="hdAction"/>
<table style="max-width: 750px;" border="0" class="formtbl">
    <tr>
        <td style="min-width: 120px;">
            Loại tài liệu<span class="starRequired">(*)</span>:
        </td>
        <td style="width: 200px;">
            <select id="ddlLoaiTaiLieu" name="ddlLoaiTaiLieu">
                <% GetListDMLoaiTaiLieu();%>
            </select>
        </td>
        <td>Cơ quan ban hành<span class="starRequired">(*)</span>:</td>
        <td>
            <input type="text" id="txtCoQuanBanHanh" name="txtCoQuanBanHanh"/>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Mã tài liệu:</td>
        <td><input type="text" id="txtMaTaiLieu" name="txtMaTaiLieu" readonly="readonly"/></td>
        <td>
            Ngày ban hành<span class="starRequired">(*)</span>:
        </td>
        <td>
            <input type="text" id="txtNgayNhapThongTin" name="txtNgayNhapThongTin"/>
        </td>
        <td><img src="/images/icons/calendar.png" id="imgNgayNhapThongTin" /></td>
    </tr>
    <tr>
        <td>Tên tài liệu<span class="starRequired">(*)</span>:</td>
        <td colspan="3">
            <input type="text" id="txtTenTaiLieu" name="txtTenTaiLieu"></input>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Diễn giải:</td>
        <td colspan="3">
            <textarea id="txtDienGiai" name="txtDienGiai" rows="5"></textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>
            File tài liệu đính kèm<span class="starRequired">(*)</span>:
        </td>
        <td>
            <input type="file" id="FileTaiLieu" name="FileTaiLieu" style="width: 210px;"></input>
        </td>
        <td colspan="3">
            <span id="spanTenFile" style="font-style: italic;"></span>&nbsp;<a href="javascript:;"
                id="btnDownload" target="_blank"></a>
        </td>
    </tr>
</table>
<div style="text-align: center; width: 100%; margin-top: 10px;">
    <a id="btnSave" href="javascript:;" class="btn btn-rounded" onclick="Save();"><i
        class="iconfa-plus-sign"></i>Lưu</a> <span id="SpanCacNutChucNang" runat="server"
            visible="False">&nbsp;
            <a id="btnDelete" href="javascript:;" class="btn" onclick="Delete();"><i class="iconfa-remove-sign"></i>Xóa</a>&nbsp;
        </span><a id="A1" href="javascript:;" class="btn btn-rounded" onclick="parent.CloseFormUpdate();">
            <i class="iconfa-save"></i>Đóng</a>
</div>
<iframe name="iframeProcess" width="0px" height="0px;"></iframe>
</form>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#txtNgayNhapThongTin").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgayNhapThongTin").click(function () {
        $("#txtNgayNhapThongTin").datepicker("show");
    });
    
    $('#<%=Form1.ClientID %>').validate({
        rules: {
            ddlLoaiTaiLieu: {
                required: true
            },
            txtCoQuanBanHanh: {
                required: true
            },
            txtTenTaiLieu: {
                required: true
            },
            txtNgayNhapThongTin: {
                required: true, dateITA: true
            },
            FileTaiLieu:{
                required: true, extension: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }
    });

    function Save() {
        $('#hdAction').val('update');
        if($('#spanTenFile').html() != '')
            $('#FileTaiLieu').rules('remove');
        $('#Form1').submit();
    }
    
    function Delete() {
        if(confirm('Bạn chắc chắn muốn xóa tài liệu này chứ?')) {
            $('#hdAction').val('delete');
            $('#FileTaiLieu').rules('remove');
            $('#Form1').submit();    
        }
    }
    
    <% LoadOneData();%>
</script>
<div id="DivDanhSachCongTyKiemToan">
</div>