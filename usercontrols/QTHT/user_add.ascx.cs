﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using VACPA.Entities;

public partial class usercontrols_user_add : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    protected void Page_Load(object sender, EventArgs e)
    {

        Loaddrop_bophancongtac(0);

        Load_nhomquyen();

        if (!string.IsNullOrEmpty(Request.Form["TenDangNhap"]))
        {
            try
            {
                string lastid;
                if (check_user(Request.Form["TenDangNhap"], Request.Form["Email"]))
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Tài khoản đã tồn tại trong hệ thống! Hãy chọn một tên tài khoản hoặc địa chỉ email khác!</div>"));     
                    return;
                }

                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"INSERT INTO tblNguoiDung 
                                 (HoVaTen
                                  ,TenDangNhap
                                  ,MatKhau
                                  ,Email
                                  ,DienThoai
                                  ,BoPhanCongTac
                                  ,ChucVu
                                  )
                            VALUES
                                  (@HoVaTen,
                                   @TenDangNhap,
                                   @MatKhau,
                                   @Email,
                                   @DienThoai,
                                   @BoPhanCongTac,
                                   @ChucVu) SELECT SCOPE_IDENTITY()";

                sql.Parameters.AddWithValue("@HoVaTen", Request.Form["HoVaTen"]);
                sql.Parameters.AddWithValue("@TenDangNhap", Request.Form["TenDangNhap"]);
                sql.Parameters.AddWithValue("@MatKhau", Request.Form["MatKhau"]);
                sql.Parameters.AddWithValue("@DienThoai", Request.Form["DienThoai"]);
                sql.Parameters.AddWithValue("@BoPhanCongTac", Request.Form["BoPhanCongTac"]);
                sql.Parameters.AddWithValue("@ChucVu", Request.Form["ChucVu"]);
                sql.Parameters.AddWithValue("@Email", Request.Form["Email"]);
               

                foreach (SqlParameter Parameter in sql.Parameters)
                {
                    if (Parameter.Value == null)
                    {
                        Parameter.Value = DBNull.Value;
                    }
                }
                DataSet ds = DataAccess.RunCMDGetDataSet(sql);
                lastid = ds.Tables[0].Rows[0][0].ToString();

                if (!string.IsNullOrEmpty(Request.Form["NhomQuyenID"]))
                {
                    string[] nhomquyen = Request.Form["NhomQuyenID"].Split(',');
                    foreach (string quyen in nhomquyen)
                    {
                        sql = new SqlCommand();
                        sql.CommandText = @"INSERT INTO tblNguoiDungNhomQuyen (NguoiDungID,NhomQuyenID) VALUES (@NguoiDungID,@NhomQuyenID) ";
                        sql.Parameters.AddWithValue("@NguoiDungID", lastid);
                        sql.Parameters.AddWithValue("@NhomQuyenID", quyen);
                        DataAccess.RunActionCmd(sql);
                        sql.Connection.Close();
                        sql.Connection.Dispose();  
                    }
                }

                cm.ghilog("QuanLyTaiKhoanCB", "Thêm tài khoản cán bộ VACPA \"" + Request.Form["TenDangNhap"] + "\"");

                sql.Connection.Close();
                sql.Connection.Dispose();   
                sql = null;
                ds = null;
                Response.Clear();
                Response.Write("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>");

       
            }
            catch (Exception ex)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
            }

        }

    }

    protected void Load_nhomquyen()
    {
        try
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblNhomQuyen ";
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataTable dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {                
                NhomQuyenID.Controls.Add(new LiteralControl("<option value='" + row["NhomQuyenID"] + "'>" + row["TenNhomQuyen"] + "</option>"));
            }
            sql.Connection.Close();
            sql.Connection.Dispose();  
            sql = null;
            ds = null;
            dt = null;
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    bool check_user(string login, string email)
    {
        bool check = false;
        try
        {

            SqlCommand sql = new SqlCommand();

            if (!string.IsNullOrEmpty(email))
                sql.CommandText = "SELECT NguoiDungID FROM tblNguoiDung WHERE UPPER(TenDangNhap)=@TenDangNhap OR UPPER(Email)=@Email";
            else
                sql.CommandText = "SELECT NguoiDungID FROM tblNguoiDung WHERE UPPER(TenDangNhap)=@TenDangNhap"; 
            sql.Parameters.AddWithValue("@TenDangNhap", login.ToUpper());
            sql.Parameters.AddWithValue("@Email", email.ToUpper());
            
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            if (ds.Tables[0].Rows.Count > 0) check = true;
            sql.Connection.Close();
            sql.Connection.Dispose();  
            sql = null;
            ds = null;
            
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
        return check;
    }



    protected void Loaddrop_bophancongtac(int pid, int level = 0)
    {
        try
        {            
            
            SqlCoCauToChucProvider CoCauToChuc_provider = new SqlCoCauToChucProvider(cm.connstr, true, "");
            TList<CoCauToChuc> CoCauToChuc_data;

            VACPA.Data.Bases.CoCauToChucParameterBuilder filter = new VACPA.Data.Bases.CoCauToChucParameterBuilder();
            if (pid==0)
                filter.AppendIsNull(CoCauToChucColumn.CoCauToChucCapTrenId);
            else
                filter.AppendInQuery(CoCauToChucColumn.CoCauToChucCapTrenId, "SELECT CoCauToChucCapTrenID FROM tblCoCauToChuc WHERE CoCauToChucCapTrenID = " + pid.ToString());

            CoCauToChuc_data = CoCauToChuc_provider.Find(filter);



            foreach (CoCauToChuc CoCauToChuc in CoCauToChuc_data)
            {
                string sep = new String('−', level * 2);


                BoPhanCongTac.Controls.Add(new LiteralControl("<option value='" + CoCauToChuc.CoCauToChucId.ToString() + "'>" + sep + CoCauToChuc.TenBoPhan + "</option>"));

                Loaddrop_bophancongtac(CoCauToChuc.CoCauToChucId, level + 1);
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }


}