﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QTHT_QuanLyTaiLieu.ascx.cs"
    Inherits="usercontrols_QTHT_QuanLyTaiLieu" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" name="Form1" ClientIDMode="Static" runat="server">
<h4 class="widgettitle">
    Danh sách tài liệu</h4>
<div>
    <div class="dataTables_length">
        <a id="btn_them" href="javascript:;" class="btn btn-rounded" onclick="OpenFormUpdate();">
            <i class="iconfa-plus-sign"></i>Thêm mới</a>
        <asp:LinkButton ID="lbtDelete" runat="server" OnClick="lbtDelete_Click" CssClass="btn"
            OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}">
            <i class="iconfa-remove-sign"></i>Xóa
        </asp:LinkButton>
        <a href="javascript:;" id="btn_search" class="btn btn-rounded" onclick="OpenFormSearch();">
            <i class="iconfa-search"></i>Tìm kiếm</a>
    </div>
</div>
<asp:GridView ClientIDMode="Static" ID="gv_DanhSachTaiLieu" runat="server" AutoGenerateColumns="False"
    class="table table-bordered responsive dyntable" AllowPaging="True" AllowSorting="True"
    OnPageIndexChanging="gv_DanhSachTaiLieu_PageIndexChanging" OnSorting="gv_DanhSachTaiLieu_Sorting"
    OnRowCommand="gv_DanhSachTaiLieu_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
            ItemStyle-Width="30px">
            <ItemTemplate>
                <input type="checkbox" id="checkbox" runat="server" class="colcheckbox" value='<%# Eval("TaiLieuID")%>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="STT">
            <ItemTemplate>
                <span>
                    <%# Container.DataItemIndex + 1 %></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Mã tài liệu" SortExpression="MaTaiLieu">
            <ItemTemplate>
                <span>
                    <%# Eval("MaTaiLieu")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Loại tài liệu" SortExpression="LoaiTaiLieu">
            <ItemTemplate>
                <span>
                    <%# Eval("LoaiTaiLieu") %></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên tài liệu" SortExpression="TenTaiLieu">
            <ItemTemplate>
                <span>
                    <%# Eval("TenTaiLieu") %></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Ngày ban hành" SortExpression="NgayNhap">
            <ItemTemplate>
                <span>
                    <%# Eval("NgayNhap") %></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="File đính kèm">
            <ItemTemplate>
                <span>
                    <%# Eval("FileTaiLieu")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Thao tác" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <div class="btn-group">
                    <a href='javascript:;' data-placement="top" data-rel="tooltip" onclick="OpenFormUpdate(<%# Eval("TaiLieuID") %>);"
                        data-original-title="Xem/Sửa" rel="tooltip" class="btn"><i class="iconsweets-create">
                        </i></a>&nbsp;
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="del" CommandArgument='<%# Eval("TaiLieuID") %>'
                        data-placement="top" data-rel="tooltip" data-original-title="Xóa" rel="tooltip"
                        class="btn" OnClientClick="return confirm('Bạn chắc chắn muốn xóa tài liệu này chứ?');"><i class="iconsweets-trashcan"></i></asp:LinkButton>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:HiddenField ID="hf_TrangHienTai" runat="server" />
<div class="dataTables_info" id="dyntable_info">
    <div class="pagination pagination-mini">
        Chuyển đến trang:
        <ul>
            <li><a href="javascript:;" onclick="$('#tranghientai').val(0);$('#hdAction').val('paging'); $('#user_search').submit();"><<
                Đầu tiên</a></li>
            <li><a href="javascript:;" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager option:selected').val()-1); $('#hdAction').val('paging'); $('#user_search').submit();}">
                < Trước</a>
            </li>
            <li><a style="border: none; background-color: #eeeeee">
                <asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" Style="width: 55px;
                    height: 22px;" onchange="$('#tranghientai').val(this.value);$('#hdAction').val('paging'); $('#user_search').submit();">
                </asp:DropDownList>
            </a></li>
            <li style="margin-left: 5px;"><a href="javascript:;" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) { $('#tranghientai').val(parseInt($('#Pager option:selected').val())+1); $('#hdAction').val('paging'); $('#user_search').submit();} ;"
                style="border-left: 1px solid #ccc;">Sau ></a></li>
            <li><a href="javascript:;" onclick="$('#tranghientai').val($('#Pager option').length-1);$('#hdAction').val('paging'); $('#user_search').submit();">
                Cuối cùng >></a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">

    <% CheckPermissionOnPage(); %>

    jQuery(document).ready(function () {
        // dynamic table      

        $("#gv_DanhSachTaiLieu .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#gv_DanhSachTaiLieu :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });
    
    function CheckHasChooseItem() {
        var flag = false;
        $('#gv_DanhSachTaiLieu :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all') {
                flag = true;
                return;
            }
        });
        if (!flag)
            alert('Phải chọn ít nhất một bản ghi để thực hiện thao tác!');
        return flag;
    }
    
    function OpenFormUpdate(id) {
        $("#DivFormUpdate").empty();
        if (id == undefined || id.length == 0)
            $("#DivFormUpdate").append($("<iframe width='100%' height='100%' id='ifNhap' name='ifNhap' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=QTHT_QuanLyTaiLieu_Update"));
        if (id != undefined && parseInt(id) > 0)
            $("#DivFormUpdate").append($("<iframe width='100%' height='100%' id='ifNhap' name='ifNhap' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=QTHT_QuanLyTaiLieu_Update&Id=" + id));
        $("#DivFormUpdate").dialog({
            resizable: true,
            width: 760,
            height: 400,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Thêm mới, xem, sửa thông tin tài liệu</b>",
            modal: true,
            zIndex: 1000
        });
    }

    function CloseFormUpdate() {
        $("#DivFormUpdate").dialog('close');
    }
    
    function OpenFormSearch() {
        $("#DivSearch").dialog({
            resizable: true,
            width: 700,
            height: 300,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm tài liệu</b>",
            modal: true,
            zIndex: 1000
        });
        $("#DivSearch").parent().appendTo($("#Form1"));
    }

    function Search() {
        document.getElementById('<%= lbtTruyVan.ClientID %>').click();
    }

    // Hiển thị tooltip
    if (jQuery('#gv_DanhSachTaiLieu').length > 0) jQuery('#gv_DanhSachTaiLieu').tooltip({ selector: "a[rel=tooltip]" });
</script>
<div id="DivSearch" style="display: none;">
    <fieldset class="fsBlockInfor">
        <legend>Tiêu chí tìm kiếm</legend>
        <table style="width: 600px;" border="0" class="formtblInforWithoutBorder">
            <tr>
                <td style="width: 120px;">
                    Mã tài liệu:
                </td>
                <td>
                    <asp:TextBox ID="txtMaTaiLieu" runat="server"></asp:TextBox>
                </td>
                <td style="width: 120px;"></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    Loại tài liệu:
                </td>
                <td>
                    <asp:DropDownList ID="ddlLoaiTaiLieu" runat="server">
                    </asp:DropDownList>
                </td>
                <td>Cơ quan ban hành:</td>
                <td><asp:TextBox ID="txtCoQuanBanHanh" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Tên tài liệu:</td>
                <td colspan="3"><asp:TextBox ID="txtTenTaiLieu" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Ngày ban hành:</td>
                <td colspan="3">
                    <asp:TextBox ID="txtTuNgay" runat="server" Width="80px" CssClass="dateITA"></asp:TextBox>&nbsp;<img src="/images/icons/calendar.png" id="imgTuNgay" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtDenNgay" runat="server" Width="80px" CssClass="dateITA"></asp:TextBox>&nbsp;<img src="/images/icons/calendar.png" id="imgDenNgay" />
                </td>
            </tr>
        </table>
    </fieldset>
    <div style="width: 100%; margin-top: 10px; text-align: right;">
        <asp:LinkButton ID="lbtTruyVan" runat="server" CssClass="btn" OnClientClick="return CheckValidFormSearch();"
            OnClick="lbtTruyVan_Click"><i class="iconfa-search"></i>Tìm</asp:LinkButton>&nbsp;
        <a href="javascript:;" id="A4" class="btn btn-rounded" onclick="$('#DivSearch').dialog('close');">
            <i class="iconfa-search"></i>Bỏ qua</a>
    </div>
</div>
<div id="DivFormUpdate">
</div>
</form>
<form id="user_search" method="post" enctype="multipart/form-data">
    <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
    <input type="hidden" value="0" id="hdAction" name="hdAction" />
</form>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#<%= txtTuNgay.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgTuNgay").click(function () {
        $("#<%= txtTuNgay.ClientID %>").datepicker("show");
    });

    $("#<%= txtDenNgay.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgDenNgay").click(function () {
        $("#<%= txtDenNgay.ClientID %>").datepicker("show");
    });

    $(document).ready(function () {
        $("#Form1").validate({
            onsubmit: false
        });
    });

    function CheckValidFormSearch() {
        var isValid = $("#Form1").valid();
        if (isValid) {
            $('#DivSearch').dialog('close');
            return true;
        } else {
            return false;
        }
    }

    $("#DivSearch").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtTruyVan.ClientID %>').attr('href'));
        }
    });
</script>
