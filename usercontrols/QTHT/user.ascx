﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="user.ascx.cs" Inherits="usercontrols_user" %>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

  


   <form id="Form1" runat="server">
   <h4 class="widgettitle">Danh sách cán bộ VACPA</h4>
   <div>
        <div  class="dataTables_length">
        
        <a id="btn_them"  href="#none" class="btn btn-rounded" onclick="open_user_add();"><i class="iconfa-plus-sign"></i> Thêm</a>
        <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="if (Users_grv_selected!='') confirm_delete_user(Users_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
        <a href="#none" class="btn btn-rounded" onclick="open_user_search();"><i class="iconfa-search"></i> Tìm kiếm</a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
        
        </div>
        
   </div>

<asp:GridView ClientIDMode="Static" ID="Users_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" onpageindexchanging="Users_grv_PageIndexChanging"   onsorting="Users_grv_Sorting" 
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval("NguoiDungID")%>" />
                  </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="HoVaTen" HeaderText="Họ và tên" 
                  SortExpression="HoVaTen" />
              <asp:BoundField DataField="TenDangNhap" HeaderText="Tên đăng nhập" 
                  SortExpression="TenDangNhap" />
              <asp:BoundField DataField="ChucVu" HeaderText="Chức vụ" 
                  SortExpression="ChucVu" />
              <asp:BoundField DataField="TenBoPhan" HeaderText="Bộ phận công tác" 
                  SortExpression="TenBoPhan" />              
              <asp:TemplateField HeaderText='Thao tác' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                      <a onclick="open_user_view(<%# Eval("NguoiDungID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xem"  rel="tooltip" class="btn"><i class="iconsweets-trashcan2" ></i></a>
                      <a onclick="open_user_edit(<%# Eval("NguoiDungID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Sửa"  rel="tooltip" class="btn"><i class="iconsweets-create" ></i></a>
                      <a id="btn_xoa" onclick="confirm_delete_user(<%# Eval("NguoiDungID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xóa"  rel="tooltip" class="btn" ><i class="iconsweets-trashcan" ></i></a>                                                   
                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>


          </Columns>
</asp:GridView>

<div class="dataTables_info" id="dyntable_info" >
<div class="pagination pagination-mini">Chuyển đến trang: 
                            <ul>
                               
                                <li><a href="#" onclick="$('#tranghientai').val(0); $('#user_search').submit();"><< Đầu tiên</a></li>
                                <li><a href="#"  onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager').val()-1); $('#user_search').submit();}" >< Trước</a></li>
                                <li><a style=" border: none;  background-color:#eeeeee"><asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" 
                                        style=" width:55px; height:22px; "
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList></a>  </li>
                                <li style="margin-left:5px; "><a href="#"  onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) {$('#tranghientai').val(parseInt($('#Pager').val())+1); $('#user_search').submit();}  ;"   style="border-left: 1px solid #ccc;">Sau ></a></li>
                                <li><a href="#"  onclick="$('#tranghientai').val($('#Pager option').length-1); $('#user_search').submit();">Cuối cùng >></a></li>
                            </ul>
                        </div>                      
</div>


</form>





<script type="text/javascript">

<% annut(); %>

    

    
    // Array ID được check
    var Users_grv_selected = new Array();  

    jQuery(document).ready(function () {
        // dynamic table      

        $("#Users_grv .checkall").bind("click", function () {
            Users_grv_selected = new Array();
            checked = $(this).prop("checked");            
            $('#Users_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });

        $('#Users_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(Users_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa người dùng
    function confirm_delete_user(idxoa) {

        $("#div_user_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                    window.location = 'admin.aspx?page=user&act=delete&id=' + idxoa;
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Sửa thông tin người dùng
    function open_user_edit(id) {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=user_edit&id="+id+"&mode=edit&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 660,
            height: 550,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Sửa thông tin tài khoản người dùng</b>",
            modal: true,
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_user_add'].submitform();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Xem thông tin người dùng
    function open_user_view(id) {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=user_edit&id=" + id + "&mode=view&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 660,
            height: 550,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Xem thông tin tài khoản người dùng</b>",
            modal: true,
            buttons: {

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        
        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



    // mở form thêm người dùng
    function open_user_add() {       
            var timestamp = Number(new Date());

            $("#div_user_add").empty();
            $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=user_add&time=" + timestamp));
            $("#div_user_add").dialog({
                resizable: true,
                width: 660,
                height: 550,
                title: "<img src='images/icons/user_business.png'>&nbsp;<b>Thêm tài khoản người dùng mới</b>",
                modal: true,
                close: function (event, ui) { window.location = 'admin.aspx?page=user'; },
                buttons: {
            
                    "Ghi": function () {
                        window.frames['iframe_user_add'].submitform();
                    },                

                    "Bỏ qua": function () {
                        $(this).dialog("close");
                    }
                }

            });

            $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
            $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    // Tìm kiếm
    function open_user_search() {
        var timestamp = Number(new Date());

        $("#div_user_search").dialog({
            resizable: true,
            width: 400,
            height: 210,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Tìm tài khoản người dùng</b>",
            modal: true,            
            buttons: {

                "Tìm kiếm": function () {                    
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_user_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('.btn-group').length > 0) jQuery('.btn-group').tooltip({ selector: "a[rel=tooltip]" });


</script>

<div id="div_user_add" >
</div>

<div id="div_user_search" style="display:none" >
<form id="user_search" method="post" enctype="multipart/form-data" action="">
<table  width="100%" border="0" class="formtbl"  >
    <tr>
        <td style="width:100px"><label>Họ và tên: </label></td>
        <td><input type="text" name="timkiem_HoVaTen" id="timkiem_HoVaTen" class="input-xlarge"></td>
    </tr>
    <tr>
        <td><label>Tên đăng nhập: </label></td>
        <td><input type="text" name="timkiem_TenDangNhap" id="timkiem_TenDangNhap" class="input-xlarge"></td>
    </tr>  

    <tr>
        <td><label>Email:</label></td>
        <td><input type="text" name="timkiem_Email" id="timkiem_Email" class="input-xlarge">
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
        
        </td>
    </tr>
</table>
</form>
</div>


<script language="javascript">

    $('#timkiem_HoVaTen').val('<%=Request.Form["timkiem_HoVaTen"]%>');
    $('#timkiem_TenDangNhap').val('<%=Request.Form["timkiem_TenDangNhap"]%>');
    $('#timkiem_Email').val('<%=Request.Form["timkiem_Email"]%>');

 </script>

<div id="div_user_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?</p>

</div>


