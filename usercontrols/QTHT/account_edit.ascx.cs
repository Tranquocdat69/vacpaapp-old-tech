﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_account_edit : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM tblDMKhachHang WHERE KhachHangID=" + Request.QueryString["id"];
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        dt = ds.Tables[0];






        // Sửa tài khoản

        if (!string.IsNullOrEmpty(Request.Form["TenTaiKhoan"]))
        {
            try
            {

                if (check_user(Request.Form["TenTaiKhoan"], Request.Form["Email"]))
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Tài khoản đã tồn tại trong hệ thống! Hãy chọn một tên tài khoản hoặc địa chỉ email khác!</div>"));
                    return;
                }

                if (!string.IsNullOrEmpty(Request.Form["MatKhau"]))
                {

           sql.CommandText = @"UPDATE tblDMKhachHang
           SET TenToChuc=@TenToChuc
           ,SoDKKD=@SoDKKD
           ,NgayCapDKKD=@NgayCapDKKD
           ,NoiCapDKKD=@NoiCapDKKD
           ,DiaChi=@DiaChi
           ,DBHC_TinhID_ToChuc=@DBHC_TinhID_ToChuc
           ,DBHC_HuyenID_ToChuc=@DBHC_HuyenID_ToChuc
           ,DBHC_XaID_ToChuc=@DBHC_XaID_ToChuc
           ,NguoiDaiDien=@NguoiDaiDien
           ,DienThoaiToChuc=@DienThoaiToChuc
           ,FaxToChuc=@FaxToChuc
           ,HoVaTenNguoiDangKy=@HoVaTenNguoiDangKy
           ,PhongBan=@PhongBan
           ,ChucVu=@ChucVu
           ,DienThoaiNguoiDangKy=@DienThoaiNguoiDangKy
           ,Email=@Email
           ,TenTaiKhoan=@TenTaiKhoan
           ,MatKhau=@MatKhau   
           ,SoCMND=@SoCMND
           ,NgayCap=@NgayCap
           ,NoiCap=@NoiCap       
           
     WHERE KhachHangID=" + Request.QueryString["id"] + @"
           ";
           sql.Parameters.AddWithValue("@MatKhau", Request.Form["MatKhau"]);
                }
                else
                {
                    sql.CommandText = @"UPDATE tblDMKhachHang
           SET TenToChuc=@TenToChuc
           ,SoDKKD=@SoDKKD
           ,NgayCapDKKD=@NgayCapDKKD
           ,NoiCapDKKD=@NoiCapDKKD
           ,DiaChi=@DiaChi
           ,DBHC_TinhID_ToChuc=@DBHC_TinhID_ToChuc
           ,DBHC_HuyenID_ToChuc=@DBHC_HuyenID_ToChuc
           ,DBHC_XaID_ToChuc=@DBHC_XaID_ToChuc
           ,NguoiDaiDien=@NguoiDaiDien
           ,DienThoaiToChuc=@DienThoaiToChuc
           ,FaxToChuc=@FaxToChuc
           ,HoVaTenNguoiDangKy=@HoVaTenNguoiDangKy
           ,PhongBan=@PhongBan
           ,ChucVu=@ChucVu
           ,DienThoaiNguoiDangKy=@DienThoaiNguoiDangKy
           ,Email=@Email
           ,TenTaiKhoan=@TenTaiKhoan
           ,SoCMND=@SoCMND
           ,NgayCap=@NgayCap
           ,NoiCap=@NoiCap
           
           
     WHERE KhachHangID=" + Request.QueryString["id"] + @"
           ";
                }

                sql.Parameters.AddWithValue("@TenToChuc", Request.Form["TenToChuc"]);
                sql.Parameters.AddWithValue("@SoDKKD", Request.Form["SoDKKD"]);

                if (!String.IsNullOrEmpty(Request.Form["NgayCapDKKD"]) && Request.Form["NgayCapDKKD"].Length == 10)
                    sql.Parameters.AddWithValue("@NgayCapDKKD", DateTime.ParseExact(Request.Form["NgayCapDKKD"], "dd/MM/yyyy", null));
                else
                    sql.Parameters.AddWithValue("@NgayCapDKKD", DateTime.ParseExact("01/01/1970", "dd/MM/yyyy", null));

                sql.Parameters.AddWithValue("@NoiCapDKKD", Request.Form["NoiCapDKKD"]);
                sql.Parameters.AddWithValue("@DiaChi", Request.Form["DiaChi"]);
                sql.Parameters.AddWithValue("@DBHC_TinhID_ToChuc", Request.Form["TinhID_ToChuc"]);
                sql.Parameters.AddWithValue("@DBHC_HuyenID_ToChuc", Request.Form["HuyenID_ToChuc"]);
                sql.Parameters.AddWithValue("@DBHC_XaID_ToChuc", Request.Form["XaID_ToChuc"]);
                sql.Parameters.AddWithValue("@NguoiDaiDien", Request.Form["NguoiDaiDien"]);
                sql.Parameters.AddWithValue("@DienThoaiToChuc", Request.Form["DienThoaiToChuc"]);
                sql.Parameters.AddWithValue("@FaxToChuc", Request.Form["FaxToChuc"]);
                sql.Parameters.AddWithValue("@HoVaTenNguoiDangKy", Request.Form["HoVaTenNguoiDangKy"]);
                sql.Parameters.AddWithValue("@PhongBan", Request.Form["PhongBan"]);
                sql.Parameters.AddWithValue("@ChucVu", Request.Form["ChucVu"]);
                sql.Parameters.AddWithValue("@DienThoaiNguoiDangKy", Request.Form["DienThoaiNguoiDangKy"]);
                sql.Parameters.AddWithValue("@Email", Request.Form["Email"]);
                sql.Parameters.AddWithValue("@TenTaiKhoan", Request.Form["TenTaiKhoan"]);

                if (!String.IsNullOrEmpty(Request.Form["NgayCap"]) && Request.Form["NgayCap"].Length == 10)
                    sql.Parameters.AddWithValue("@NgayCap", DateTime.ParseExact(Request.Form["NgayCap"], "dd/MM/yyyy", null));
                else                    
                    sql.Parameters.AddWithValue("@NgayCap", DBNull.Value);

                sql.Parameters.AddWithValue("@SoCMND", Request.Form["SoCMND"]);
                sql.Parameters.AddWithValue("@NoiCap", Request.Form["NoiCap"]);
                

                

                foreach (SqlParameter Parameter in sql.Parameters)
                {
                    if (Parameter.Value == null)
                    {
                        Parameter.Value = DBNull.Value;
                    }
                }

                DataAccess.RunActionCmd(sql);

                cm.ghilog("QuanLyTaiKhoanKH", "Cập nhật tài khoản hội viên \"" + Request.Form["TenTaiKhoan"] + "\"");
    

                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;
                ds = null;
                if (cm.Khachhang_KhachHangID!="")
                    Response.Write("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>");
                else
                    Response.Write("<script>parent.window.location='admin.aspx?page=account';</script>");


            }
            catch (Exception ex)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
            }

        }
    }


    bool check_user(string login, string email)
    {
        bool check = false;
        try
        {

            SqlCommand sql = new SqlCommand();

            if (!string.IsNullOrEmpty(email))
                sql.CommandText = "SELECT KhachHangID FROM tblDMKhachHang WHERE (UPPER(TenTaiKhoan)=@TenTaiKhoan OR UPPER(Email)=@Email) AND (KhachHangID<>" + Request.QueryString["id"] + ")";
            else
                sql.CommandText = "SELECT KhachHangID FROM tblDMKhachHang WHERE (UPPER(TenTaiKhoan)=@TenTaiKhoan) AND (KhachHangID<>" + Request.QueryString["id"] + ")";            

            
            sql.Parameters.AddWithValue("@TenTaiKhoan", login.ToUpper());
            sql.Parameters.AddWithValue("@Email", email.ToUpper());

            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            if (ds.Tables[0].Rows.Count > 0) check = true;
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;

        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
        return check;
    }


    protected void Load_user()
    {
        try
        {
            

            if (dt.Rows.Count > 0)
            {
                Response.Write("$('#TenToChuc').val('" + dt.Rows[0]["TenToChuc"] + "');" + System.Environment.NewLine);
                Response.Write("$('#SoDKKD').val('" + dt.Rows[0]["SoDKKD"] + "');" + System.Environment.NewLine);
                if (dt.Rows[0]["NgayCapDKKD"] != DBNull.Value) Response.Write("$('#NgayCapDKKD').val('" + Convert.ToDateTime(dt.Rows[0]["NgayCapDKKD"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                Response.Write("$('#NoiCapDKKD').val('" + dt.Rows[0]["NoiCapDKKD"] + "');" + System.Environment.NewLine);
                Response.Write("$('#DiaChi').val('" + dt.Rows[0]["DiaChi"] + "');" + System.Environment.NewLine);
                Response.Write("$('#TinhID_ToChuc').val('" + dt.Rows[0]["DBHC_TinhID_ToChuc"].ToString().Trim() + "');" + System.Environment.NewLine);
                Response.Write("$('#HuyenID_ToChuc').val('" + dt.Rows[0]["DBHC_HuyenID_ToChuc"].ToString().Trim() + "');" + System.Environment.NewLine);
                Response.Write("$('#XaID_ToChuc').val('" + dt.Rows[0]["DBHC_XaID_ToChuc"].ToString().Trim() + "');" + System.Environment.NewLine);
                Response.Write("$('#NguoiDaiDien').val('" + dt.Rows[0]["NguoiDaiDien"] + "');" + System.Environment.NewLine);
                Response.Write("$('#DienThoaiToChuc').val('" + dt.Rows[0]["DienThoaiToChuc"] + "');" + System.Environment.NewLine);
                Response.Write("$('#FaxToChuc').val('" + dt.Rows[0]["FaxToChuc"] + "');" + System.Environment.NewLine);
                Response.Write("$('#HoVaTenNguoiDangKy').val('" + dt.Rows[0]["HoVaTenNguoiDangKy"] + "');" + System.Environment.NewLine);
                Response.Write("$('#PhongBan').val('" + dt.Rows[0]["PhongBan"] + "');" + System.Environment.NewLine);
                Response.Write("$('#ChucVu').val('" + dt.Rows[0]["ChucVu"] + "');" + System.Environment.NewLine);
                Response.Write("$('#DienThoaiNguoiDangKy').val('" + dt.Rows[0]["DienThoaiNguoiDangKy"] + "');" + System.Environment.NewLine);
                Response.Write("$('#Email').val('" + dt.Rows[0]["Email"] + "');" + System.Environment.NewLine);
                Response.Write("$('#TenTaiKhoan').val('" + dt.Rows[0]["TenTaiKhoan"] + "');" + System.Environment.NewLine);

                Response.Write("$('#SoCMND').val('" + dt.Rows[0]["SoCMND"] + "');" + System.Environment.NewLine);
                if (dt.Rows[0]["NgayCap"] != DBNull.Value) Response.Write("$('#NgayCap').val('" + Convert.ToDateTime(dt.Rows[0]["NgayCap"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                Response.Write("$('#NoiCap').val('" + dt.Rows[0]["NoiCap"] + "');" + System.Environment.NewLine);
                
            }

            if (Request.QueryString["mode"] == "view")
            {
                Response.Write("$('#form_account_add input,select,textarea').attr('disabled', true);");
            }


        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
}