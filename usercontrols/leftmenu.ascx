﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="leftmenu.ascx.cs" Inherits="usercontrols_leftmenu"  %>

    <div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header" style="cursor:pointer" title="Ẩn menu" onClick="close_leftpanel();">Menu chức năng <span  class="iconsweets-arrowleft"></span></li>
                <li <% checkmenu("|/default.aspx?|"); %> ><a href="default.aspx"><span class="iconfa-laptop"></span> Trang chủ</a></li>
      

         

                
                    
            <% if (String.IsNullOrEmpty(cm.Khachhang_KhachHangID))
               { %>
            <li <% checkmenu("|adminlogin.aspx|"); %> ><a href="adminlogin.aspx"><span class="iconfa-signin"></span> Đăng nhập</a></li>
            <%--<li <% checkmenu("|adminlogin.aspx|"); %> ><a href="adminlogin.aspx"><span class="iconfa-user"></span> Cán bộ Sở đăng nhập</a></li>--%>
               <% }
               else
               { %>



             <li  <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=0|/default.aspx?page=dangkythikiemtoan_danhsach&type=1|/default.aspx?page=dangkythikiemtoan_danhsach&type=2|/default.aspx?page=dangkythikiemtoan_danhsach&type=3|/default.aspx?page=dangkythikiemtoan_danhsach&type=4|/default.aspx?page=dangkythikiemtoan_danhsach&type=5|/default.aspx?page=dangkythikiemtoan_danhsach&type=6|/default.aspx?page=dangkythikiemtoan_danhsach&type=ALL|",1); %> ><a href=""><span class="iconfa-briefcase"></span> Khay hồ sơ</a>
                	<ul <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=0|/default.aspx?page=dangkythikiemtoan_danhsach&type=1|/default.aspx?page=dangkythikiemtoan_danhsach&type=2|/default.aspx?page=dangkythikiemtoan_danhsach&type=3|/default.aspx?page=dangkythikiemtoan_danhsach&type=4|/default.aspx?page=dangkythikiemtoan_danhsach&type=5|/default.aspx?page=dangkythikiemtoan_danhsach&type=6|/default.aspx?page=dangkythikiemtoan_danhsach&type=ALL|",2); %> >
                    	<li class="dropdown"    ><a href="#">Đăng ký dự thi lấy chứng chỉ kiểm toán viên, chứng chỉ hành nghề kế toán</a>
                        
                        <ul <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=0|/default.aspx?page=dangkythikiemtoan_danhsach&type=1|/default.aspx?page=dangkythikiemtoan_danhsach&type=2|/default.aspx?page=dangkythikiemtoan_danhsach&type=3|/default.aspx?page=dangkythikiemtoan_danhsach&type=4|/default.aspx?page=dangkythikiemtoan_danhsach&type=5|/default.aspx?page=dangkythikiemtoan_danhsach&type=6|/default.aspx?page=dangkythikiemtoan_danhsach&type=ALL|",2); %> >
                        
                            <li <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=0|"); %> ><a href="default.aspx?page=dangkythikiemtoan_danhsach&type=0">Hồ sơ tạm lưu (<b style="color:#ff0000; font-size:16px;"><% Response.Write(get_sohoso("0"));%></b>)</a></li>
                            <li <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=1|"); %> ><a href="default.aspx?page=dangkythikiemtoan_danhsach&type=1">Hồ sơ chờ tiếp nhận (<b style="color:#ff0000; font-size:16px;"><% Response.Write(get_sohoso("1"));%></b>)</a></li>
                            <li <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=2|"); %> ><a href="default.aspx?page=dangkythikiemtoan_danhsach&type=2">Hồ sơ cần bổ sung (<b style="color:#ff0000; font-size:16px;"><% Response.Write(get_sohoso("2"));%></b>)</a></li>
                            <li <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=3|"); %> ><a href="default.aspx?page=dangkythikiemtoan_danhsach&type=3">Hồ sơ tạm chờ xử lý (<b style="color:#ff0000; font-size:16px;"><% Response.Write(get_sohoso("3"));%></b>)</a></li>                            
                            <li <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=5|"); %> ><a href="default.aspx?page=dangkythikiemtoan_danhsach&type=4">Hồ sơ đã được tiếp nhận (<b style="color:#ff0000; font-size:16px;"><% Response.Write(get_sohoso("4"));%></b>)</a></li>
                            <li <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=5|"); %> ><a href="default.aspx?page=dangkythikiemtoan_danhsach&type=5">Hồ sơ từ chối tiếp nhận (<b style="color:#ff0000; font-size:16px;"><% Response.Write(get_sohoso("5"));%></b>)</a></li>
                            <li <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=6|"); %> ><a href="default.aspx?page=dangkythikiemtoan_danhsach&type=6">Hồ sơ đã được phê duyệt (<b style="color:#ff0000; font-size:16px;"><% Response.Write(get_sohoso("6"));%></b>)</a></li>                            
                            <li <% checkmenu("|/default.aspx?page=dangkythikiemtoan_danhsach&type=ALL|"); %> ><a href="default.aspx?page=dangkythikiemtoan_danhsach&type=ALL">Hồ sơ chung (<b style="color:#ff0000; font-size:16px;"><% Response.Write(get_sohoso("ALL"));%></b>)</a></li>                            
                        </ul>

                        </li>
                        
                     </ul>
                </li>                                
             </li>


             

             <li <% checkmenu("|/users.aspx?|/accounts.aspx?|/admin.aspx?page=role|",1); %> ><a href=""><span class="iconfa-cogs"></span> Web services</a>
                	<ul <% checkmenu("|/users.aspx?|/accounts.aspx?|/admin.aspx?page=role|",2); %> >
                    	<li id="mnu_tkcb" <% checkmenu("|/users.aspx?|"); %> ><a href="DVHCC_BOTAICHINHServices.asmx">Web services trao đổi thông tin với hệ thống</a></li>
                        <li id="mnu_tkkh" <% checkmenu("|/accounts.aspx?|"); %> ><a href="service.asmx">Web services xây dựng theo yêu cầu</a></li>
                   
                    </ul>
                </li>
               

                <% } %>

            </ul>
        </div><!--leftmenu-->
        

      


    </div><!-- leftpanel -->



