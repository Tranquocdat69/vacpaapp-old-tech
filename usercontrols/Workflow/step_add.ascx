﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_add.ascx.cs" Inherits="usercontrols_step_add" %>

<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("THEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <form id="form_dmchung_add" clientidmode="Static" runat="server"   method="post">

    <div id="thongbaoloi_form_themdmchung" name="thongbaoloi_form_themdmchung" style="display:none" class="alert alert-error"></div>

      <table id="Table1" width="100%" border="0" class="formtbl" >
     
        <tr>
          <td><asp:Label ID="Label1" runat="server" >Tên bước quy trình:</asp:Label>
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="6"><input type="text" name="TenBuocQuyTrinh" id="TenBuocQuyTrinh"></td>
        </tr>
        
        </table>
        
       
        
    </form>



           
                                                    
<script type="text/javascript">
       

        function submitform() {        
                jQuery("#form_dmchung_add").submit();
        }



    jQuery("#form_dmchung_add").validate({
        rules: {
            TenBuocQuyTrinh: {
                required: true
            },
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery("#thongbaoloi_form_themdmchung").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_themdmchung").hide()
            }
        }
    });

   


    

</script>