﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_sort.ascx.cs" Inherits="usercontrols_step_sort" %>

<script type="text/javascript" src="js/chosen.jquery.min.js"></script>

<style>
     .require {color:Red;}
     
     #sortable { list-style-type: none;  padding: 0; width: 90%; margin: 0 auto;
 }

  #sortable li { margin: 0 5px 5px 5px; padding: 5px; font-size: 1.2em; height: 1.5em; }

  html>body #sortable li { height: 1.5em; line-height: 1.2em; }

  .ui-state-highlight { height: 1.5em; line-height: 1.2em; }


.ui-state-default {
    background: url("images/ui-bg_glass_75_e6e6e6_1x400.png") repeat-x scroll 50% 50% #e6e6e6;
    border: 1px solid #d3d3d3;
    color: #555555;
    font-weight: normal;
}

</style>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("SUA|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>


<form class="stdform stdform2" id="form_themuser" name="form_themuser" method="post" action="" enctype="multipart/form-data">

<div id="thongbaoloi_form_themuser" class="alert alert-error" style="display:none;">

</div>

<div >

  <ul id="sortable">
  
  <asp:PlaceHolder ID="BuocTiepTheo" runat="server"></asp:PlaceHolder>
                               
</ul>

<input type="hidden" id="ThuTu" name="ThuTu"  />

</div>
  

     </form>                       
                            
                           
                 
                                                    
<script type="text/javascript">


    

    function submitform() {
        var sortedIDs = $("#sortable").sortable("toArray");
        $("#ThuTu").val(sortedIDs); 
                jQuery("#form_themuser").submit();
        }


        $(function () {

            $("#sortable").sortable({

                placeholder: "ui-state-highlight"

            });

            $("#sortable").disableSelection();

        });




</script>
                    
               


