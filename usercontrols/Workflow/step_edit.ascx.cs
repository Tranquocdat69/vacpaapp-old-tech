﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using VACPA.Entities;
public partial class usercontrols_step_edit : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public BuocQuyTrinh buocquytrinh = new BuocQuyTrinh();

    protected void Page_Load(object sender, EventArgs e)
    {

        SqlBuocQuyTrinhProvider buocquytrinh_provider = new SqlBuocQuyTrinhProvider(cm.connstr, true, "");
        buocquytrinh = buocquytrinh_provider.GetByBuocQuyTrinhId(Convert.ToInt32(Request.QueryString["id"]));


        try
        {

            if (!string.IsNullOrEmpty(Request.Form["TenBuocQuyTrinh"]))
            {
                buocquytrinh.TenBuocQuyTrinh = Request.Form["TenBuocQuyTrinh"];
                buocquytrinh.QuyTrinhId = Convert.ToInt32(Request.Cookies["quytrinh"].Value);

                buocquytrinh_provider.Update(buocquytrinh);
                cm.ghilog("KhaiBaoQuyTrinhHoatDong", "Sửa thông tin bước quy trình \"" + Request.Form["TenBuocQuyTrinh"] + "\" vào quy trình");
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));

            }


        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }

    }


    protected void Load_buocquytrinh()
    {

        try
        {
            Response.Write("$('#TenBuocQuyTrinh').val('" + cm.AddSlashes(buocquytrinh.TenBuocQuyTrinh) + "');" + System.Environment.NewLine);


            if (Request.QueryString["mode"] == "view")
            {
                Response.Write("$('#form_dmchung_add input,select,textarea').attr('disabled', true);");
            }

        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }


}