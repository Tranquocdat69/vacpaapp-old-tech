﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using VACPA.Entities;

public partial class usercontrols_step_add : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    protected void Page_Load(object sender, EventArgs e)
    {
      

        try
        {

            if (!string.IsNullOrEmpty(Request.Form["TenBuocQuyTrinh"]))
            {
                SqlBuocQuyTrinhProvider buocquytrinh_provider = new SqlBuocQuyTrinhProvider(cm.connstr, true, "");
                BuocQuyTrinh buocquytrinh = new BuocQuyTrinh();
                buocquytrinh.TenBuocQuyTrinh = Request.Form["TenBuocQuyTrinh"];
                buocquytrinh.QuyTrinhId = Convert.ToInt32(Request.Cookies["quytrinh"].Value);

                buocquytrinh_provider.Insert(buocquytrinh);
                cm.ghilog("KhaiBaoQuyTrinhHoatDong", "Thêm bước quy trình \"" + Request.Form["TenBuocQuyTrinh"] + "\" vào quy trình");
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));

            }


        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }

    }

   



}