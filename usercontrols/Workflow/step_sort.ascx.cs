﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using VACPA.Entities;

public partial class usercontrols_step_sort : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    protected void Page_Load(object sender, EventArgs e)
    {
        Load_dm();

        try
        {

            if (!string.IsNullOrEmpty(Request.Form["ThuTu"]))
            {
                SqlBuocQuyTrinhProvider buocquytrinh_provider = new SqlBuocQuyTrinhProvider(cm.connstr, true, "");
                BuocQuyTrinh buocquytrinh = new BuocQuyTrinh();               


                string[] daythutu = Request.Form["ThuTu"].Split(',');
                int i = 0;

                for (i = 0; i < daythutu.Count(); i++)
                {
                    buocquytrinh = buocquytrinh_provider.GetByBuocQuyTrinhId(Convert.ToInt32(daythutu[i]));
                    buocquytrinh.ThuTu = i + 1;
                    buocquytrinh_provider.Update(buocquytrinh);
                }

                Load_dm();
                cm.ghilog("KhaiBaoQuyTrinhHoatDong", "Sắp xếp lại các bước của quy trình có ID \"" + Request.Cookies["quytrinh"].Value + "\" ");
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));

            }



        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }

    }

    protected void Load_dm()
    {
        try
        {
            BuocTiepTheo.Controls.Clear();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblBuocQuyTrinh WHERE QuyTrinhID= " + Request.Cookies["quytrinh"].Value + " ORDER BY ThuTu ASC";
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataTable dt = ds.Tables[0];

     
            foreach (DataRow row in dt.Rows)
            {
                BuocTiepTheo.Controls.Add(new LiteralControl("<li class='ui-state-default' id=" + row["BuocQuyTrinhID"]  +">" + row["TenBuocQuyTrinh"] + "</li>"));
            }


        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }



}