﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using VACPA.Entities;

public partial class usercontrols_action_edit : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public BuocQuyTrinhHanhDong hanhdong = new BuocQuyTrinhHanhDong();

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlBuocQuyTrinhHanhDongProvider hanhdong_provider = new SqlBuocQuyTrinhHanhDongProvider(cm.connstr, true, "");
        hanhdong = hanhdong_provider.GetByBuocQuyTrinhHanhDongId(Convert.ToInt32(Request.QueryString["id"]));

        Load_dm();

        try
        {
     
            if (!string.IsNullOrEmpty(Request.Form["TenHanhDong"]))
            {
                hanhdong.TenHanhDong = Request.Form["TenHanhDong"];
                hanhdong.BuocTiepTheoId = Convert.ToInt32(Request.Form["BuocTiepTheoID"]);
                hanhdong.MaTrangThai = Request.Form["MaTrangThai"];
                hanhdong.NguoiDungId = Request.Form["NguoiDungID"];
                hanhdong.NhomQuyenId = Request.Form["NhomQuyenID"];
                hanhdong_provider.Update(hanhdong);
                cm.ghilog("KhaiBaoQuyTrinhHoatDong", "Sửa thông tin hành động \"" + Request.Form["TenHanhDong"] + "\" ");
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                Load_dm();
            }



        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }

    }

    protected void Load_dm()
    {
        try
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblNhomQuyen ";
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataTable dt = ds.Tables[0];
            string ssid = "";
            ssid = "," + hanhdong.NhomQuyenId + ",";

            NhomQuyenID.Controls.Clear();
            NguoiDung.Controls.Clear();
            BuocTiepTheo.Controls.Clear();
            MaTrangThai.Controls.Clear();

            foreach (DataRow row in dt.Rows)
            {
                if (ssid.Contains("," + row["NhomQuyenID"].ToString() + ","))           
                    NhomQuyenID.Controls.Add(new LiteralControl("<option value='" + row["NhomQuyenID"] + "' selected >" + row["TenNhomQuyen"] + "</option>"));
                else
                    NhomQuyenID.Controls.Add(new LiteralControl("<option value='" + row["NhomQuyenID"] + "'>" + row["TenNhomQuyen"] + "</option>"));

            }


            sql = new SqlCommand();
            sql.CommandText = "SELECT QuyTrinhID FROM tblBuocQuyTrinh WHERE BuocQuyTrinhID= " + hanhdong.BuocQuyTrinhId;
            ds = DataAccess.RunCMDGetDataSet(sql);
      

            string quytrinhid = ds.Tables[0].Rows[0][0].ToString();
            

            sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblNguoiDung ";
            ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];

            ssid="," + hanhdong.NguoiDungId + ",";

            

            foreach (DataRow row in dt.Rows)
            {
                if (ssid.Contains(","+row["NguoiDungID"].ToString()+","))
                    NguoiDung.Controls.Add(new LiteralControl("<option value='" + row["NguoiDungID"] + "' selected  >" + row["HoVaTen"] + "</option>"));
                else
                    NguoiDung.Controls.Add(new LiteralControl("<option value='" + row["NguoiDungID"] + "'   >" + row["HoVaTen"] + "</option>"));
            }


            sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblBuocQuyTrinh WHERE QuyTrinhID= " + quytrinhid + " ORDER BY ThuTu ASC";
            ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                BuocTiepTheo.Controls.Add(new LiteralControl("<option value='" + row["BuocQuyTrinhID"] + "'>" + row["TenBuocQuyTrinh"] + "</option>"));
            }


            sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblDMTrangThai ORDER BY TenTrangThai ";
            ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                MaTrangThai.Controls.Add(new LiteralControl("<option value='" + row["MaTrangThai"] + "'>" + row["TenTrangThai"] + "</option>"));
            }

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    protected void Load_hanhdong()
    {

        try
        {
            Response.Write("$('#TenHanhDong').val('" + cm.AddSlashes(hanhdong.TenHanhDong) + "');" + System.Environment.NewLine);
            Response.Write("$('#BuocTiepTheoID').val('" + hanhdong.BuocTiepTheoId + "');" + System.Environment.NewLine);
            Response.Write("$('#MaTrangThai').val('" + hanhdong.MaTrangThai + "');" + System.Environment.NewLine);
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }


}