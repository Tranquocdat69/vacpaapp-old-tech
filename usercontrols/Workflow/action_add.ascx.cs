﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using VACPA.Entities;

public partial class usercontrols_action_add : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    protected void Page_Load(object sender, EventArgs e)
    {
        Load_dm();

        try
        {

            if (!string.IsNullOrEmpty(Request.Form["TenHanhDong"]))
            {
                SqlBuocQuyTrinhHanhDongProvider hanhdong_provider = new SqlBuocQuyTrinhHanhDongProvider(cm.connstr, true, "");
                BuocQuyTrinhHanhDong hanhdong = new BuocQuyTrinhHanhDong();
                hanhdong.TenHanhDong = Request.Form["TenHanhDong"];
                hanhdong.BuocQuyTrinhId = Convert.ToInt32(Request.QueryString["buocquytrinhid"]);
                hanhdong.BuocTiepTheoId = Convert.ToInt32(Request.Form["BuocTiepTheoID"]);
                hanhdong.MaTrangThai = Request.Form["MaTrangThai"];
                hanhdong.NguoiDungId = Request.Form["NguoiDungID"];
                hanhdong.NhomQuyenId = Request.Form["NhomQuyenID"];
                hanhdong_provider.Insert(hanhdong);                
                cm.ghilog("KhaiBaoQuyTrinhHoatDong", "Thêm hành động \"" + Request.Form["TenHanhDong"] + "\" vào bước của quy trình");
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
               
            }



        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }

    }

    protected void Load_dm()
    {
        try
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblNhomQuyen ";
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataTable dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                NhomQuyenID.Controls.Add(new LiteralControl("<option value='" + row["NhomQuyenID"] + "'>" + row["TenNhomQuyen"] + "</option>"));
            }


            sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblNguoiDung ";
            ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                NguoiDung.Controls.Add(new LiteralControl("<option value='" + row["NguoiDungID"] + "'>" + row["HoVaTen"] + "</option>"));
            }


            sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblBuocQuyTrinh WHERE QuyTrinhID= "+ Request.QueryString["quytrinhid"] + " ORDER BY ThuTu ASC";
            ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                BuocTiepTheo.Controls.Add(new LiteralControl("<option value='" + row["BuocQuyTrinhID"] + "'>" + row["TenBuocQuyTrinh"] + "</option>"));
            }


            sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblDMTrangThai ORDER BY TenTrangThai ";
            ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                MaTrangThai.Controls.Add(new LiteralControl("<option value='" + row["MaTrangThai"] + "'>" + row["TenTrangThai"] + "</option>"));
            }

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }



}