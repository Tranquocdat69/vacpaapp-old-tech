﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;

public partial class usercontrols_khaibaoquytrinh : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected void Page_Load(object sender, EventArgs e)
    {
        Commons cm = new Commons();
        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

        // Tên chức năng
        string tenchucnang = "Khai báo quy trình hoạt động";
        // Icon CSS Class  
        string IconClass = "iconfa-tasks";

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            BuocQuyTrinh.Visible = false;
            return;
        }


        ///////////
        load_dsquytrinh();
        load_users();
        
        

        try
        {
            if (Request.QueryString["act"] == "delete")
            {
                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("XOA|"))
                {
                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "DELETE FROM tblBuocQuyTrinh WHERE BuocQuyTrinhID IN (" + Request.QueryString["id"] + ")";
                    DataAccess.RunActionCmd(sql);

                    sql = new SqlCommand();
                    sql.CommandText = "DELETE FROM tblBuocQuyTrinhHanhDong WHERE BuocQuyTrinhID IN (" + Request.QueryString["id"] + ")";
                    DataAccess.RunActionCmd(sql);

                    cm.ghilog("KhaiBaoQuyTrinhHoatDong", "Xóa bước quy trình có ID: " + Request.QueryString["id"]);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    Response.Redirect("admin.aspx?page=khaibaoquytrinh");
                }
            }


            if (Request.QueryString["act"] == "deleteact")
            {
                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("XOA|"))
                {
                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "DELETE FROM tblBuocQuyTrinhHanhDong WHERE BuocQuyTrinhHanhDongID IN (" + Request.QueryString["id"] + ")";
                    DataAccess.RunActionCmd(sql);
                    cm.ghilog("KhaiBaoQuyTrinhHoatDong", "Xóa hành động có ID: " + Request.QueryString["id"]);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    Response.Redirect("admin.aspx?page=khaibaoquytrinh");
                }
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }

    protected void load_users()
    {
        

        try
        {
            
            Commons cm = new Commons();
            // Tìm kiếm
            SelectQueryBuilder query = new SelectQueryBuilder();
            query.SelectFromTable("tblBuocQuyTrinh");
            query.SelectColumns("BuocQuyTrinhID", "TenBuocQuyTrinh", "QuyTrinhID");
            query.AddOrderBy("ThuTu", Sorting.Ascending);

            string quytrinhid=DmQuyTrinh.SelectedValue;

            try
            {
                if (!string.IsNullOrEmpty(Request.Cookies["quytrinh"].Value)) quytrinhid = Request.Cookies["quytrinh"].Value;
            }
            catch { }

       
            query.AddWhere("QuyTrinhID", Comparison.Equals, quytrinhid);

          

            SqlCommand sql = new SqlCommand();
            sql.CommandText = query.BuildQuery();
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;

            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 30;
            int i;
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "BuocQuyTrinh" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }
    
    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }

        
    }
        
    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }

    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }

    protected void load_dsquytrinh()
    {
        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM tblDMQuyTrinh ORDER BY TenQuyTrinh ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DmQuyTrinh.DataSource = ds.Tables[0];
        DmQuyTrinh.DataTextField = "TenQuyTrinh";
        DmQuyTrinh.DataValueField = "QuyTrinhID";
        DmQuyTrinh.DataBind();

        ds.Dispose();
        try
        {
            if (!string.IsNullOrEmpty(Request.Cookies["quytrinh"].Value)) DmQuyTrinh.Items.FindByValue(Request.Cookies["quytrinh"].Value).Selected = true;
        }
        catch { }
    }

    public string load_hanhdong(string idhanhdong)
    {

        string output_html = "";

        DataSet ds = new DataSet();


        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT BuocQuyTrinhHanhDongID,TenHanhDong FROM tblBuocQuyTrinhHanhDong WHERE BuocQuyTrinhID = " + idhanhdong + " ORDER BY TenHanhDong ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];


        foreach (DataRow hanhdong in dt.Rows)
        {
            output_html += "<a  class='btn btn-rounded' style='margin-bottom:5px;' >" + hanhdong["TenHanhDong"] + "</a><br>";

        }


        ds = null;
        dt = null;



        return output_html;
    }
    
    public string load_dsnguoidung(string idnguoidung)
    {

         string output_html = "";

        DataSet ds = new DataSet();

      
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT NguoiDungID,HoVaTen FROM tblNguoiDung WHERE NguoiDungID IN (" + idnguoidung + ") ORDER BY HoVaTen ASC"; 
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
        

        DataTable dt = ds.Tables[0];

        
        foreach (DataRow NguoiDung in dt.Rows)
        {
            output_html += "<a href='#none' onclick=\"open_vacpa_user_view(" + NguoiDung["NguoiDungID"] + ");\">" + NguoiDung["HoVaTen"] + "</a><br>";
            
        }

       
        ds = null;
        dt = null;

    

        return output_html;
    }
    
    public string load_dsvaitro(string idvaitro)
    {

        string output_html = "";

        DataSet ds = new DataSet();


        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT NhomQuyenID,TenNhomQuyen FROM tblNhomQuyen WHERE NhomQuyenID IN (" + idvaitro + ") ORDER BY TenNhomQuyen ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];


        foreach (DataRow VaiTro in dt.Rows)
        {
            output_html += "<a href='#none' onclick=\"open_role_view(" + VaiTro["NhomQuyenID"] + ");\">" + VaiTro["TenNhomQuyen"] + "</a><br>";

        }


        ds = null;
        dt = null;



        return output_html;
    }
        
    public string load_hanhdong_action(string idhanhdong)
    {

        string output_html = "";

        DataSet ds = new DataSet();
        
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT BuocQuyTrinhHanhDongID FROM tblBuocQuyTrinhHanhDong WHERE BuocQuyTrinhID = " + idhanhdong + " ORDER BY TenHanhDong ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        
        DataTable dt = ds.Tables[0];
        
        foreach (DataRow hanhdong in dt.Rows)
        {
            output_html += "<div class=\"btn-group\"><a  style='margin-bottom:5px;'  onclick=\"open_action_edit(" + hanhdong["BuocQuyTrinhHanhDongID"] + ");\" data-placement=\"top\" data-rel=\"tooltip\" href=\"#none\" data-original-title=\"Sửa hành động \"  rel=\"tooltip\" class=\"btn\"><i class=\"iconsweets-create\" ></i></a>";
            output_html += "<a  style='margin-bottom:5px;'  onclick=\"confirm_delete_action(" + hanhdong["BuocQuyTrinhHanhDongID"] + ");\" data-placement=\"top\" data-rel=\"tooltip\" href=\"#none\" data-original-title=\"Xóa hành động \"  rel=\"tooltip\" class=\"btn\"><i class=\"iconsweets-trashcan\" ></i></a></div><br>";
        }
        
        ds = null;
        dt = null;
        
        return output_html;
    }
}