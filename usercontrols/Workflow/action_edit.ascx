﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="action_edit.ascx.cs" Inherits="usercontrols_action_edit" %>

<script type="text/javascript" src="js/chosen.jquery.min.js"></script>

<style>
     .require {color:Red;}
</style>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KhaiBaoQuyTrinhHoatDong", cm.connstr).Contains("SUA|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>


<form class="stdform stdform2" id="form_themuser" name="form_themuser" method="post" action="" enctype="multipart/form-data">

<div id="thongbaoloi_form_themuser" class="alert alert-error" style="display:none;">

</div>

<table id="Table1" width="100%" border="0" class="formtbl" runat="server">
    <tr>
        <td style=" min-width:150px"><label>Tên hành động: <span class="require">*</span></label></td>
        <td><input type="text" name="TenHanhDong" id="TenHanhDong" class="input-xlarge"></td>
    </tr>
    <tr>
        <td><label>Bước tiếp theo: <span class="require">*</span></label></td>
        <td>   <select name="BuocTiepTheoID" id="BuocTiepTheoID" ><asp:PlaceHolder ID="BuocTiepTheo" runat="server"></asp:PlaceHolder>
                                </select></td>
    </tr>
    <tr>
        <td><label>Cập nhật trạng thái hồ sơ về: <span class="require">*</span></label></td>
        <td><select name="MaTrangThai" id="MaTrangThai" ><asp:PlaceHolder ID="MaTrangThai" runat="server"></asp:PlaceHolder>
                                </select></td>
    </tr>

      <tr>
        <td><label>Tài khoản thực hiện hành động: </label></td>
        <td><select name="NguoiDungID" id="NguoiDungID"  style="width:430px" class="chzn-select"  multiple="multiple">
                                    <asp:PlaceHolder ID="NguoiDung" runat="server"></asp:PlaceHolder>
                                </select></td>
    </tr>


    <tr>
        <td><label>Vai trò thực hiện hành động: </label></td>
        <td><select name="NhomQuyenID" id="NhomQuyenID"  style="width:430px" class="chzn-select"  multiple="multiple">
                                    <asp:PlaceHolder ID="NhomQuyenID" runat="server"></asp:PlaceHolder>
                                </select></td>
    </tr>
</table>




  

     </form>                       
                            
                           
                 
                                                    
<script type="text/javascript">
        jQuery(".chzn-select").chosen();

        function submitform() {        
                jQuery("#form_themuser").submit();
        }



    jQuery("#form_themuser").validate({
        rules: {
            TenHanhDong: {
                required: true
            },

           
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery("#thongbaoloi_form_themuser").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_themuser").hide()
            }
        }
    });


    <% Load_hanhdong(); %>


</script>
                    
               



