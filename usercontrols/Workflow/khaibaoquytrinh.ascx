﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="khaibaoquytrinh.ascx.cs" Inherits="usercontrols_khaibaoquytrinh" %>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

   <form id="BuocQuyTrinh" name="BuocQuyTrinh" clientidmode="Static" runat="server">
   <h4 class="widgettitle">Danh sách các bước trong quy trình</h4>
   <div>
    <div  class="dataTables_length">
       Quy trình: <asp:DropDownList ID="DmQuyTrinh" runat="server" ClientIDMode="Static" 
         onchange="setCookie('quytrinh', this.value,  10); location.reload();"
             >
        </asp:DropDownList>
     
    </div>
        <div  class="dataTables_length">
        
        <a id="btn_them" href="#none" class="btn btn-rounded" onclick="open_user_add();"><i class="iconfa-plus-sign"></i> Thêm</a>
        <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="if (Users_grv_selected!='') confirm_delete_user(Users_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>        
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
        <a id="btn_sapxep"   href="#none" class="btn btn-rounded" onclick="open_step_sort()" ><i class="iconfa-sort"></i> Sắp xếp thứ tự</a>
  
        </div>
        
   </div>

<asp:GridView ClientIDMode="Static" ID="Users_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="Users_grv_PageIndexChanging" onsorting="Users_grv_Sorting"    
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval("BuocQuyTrinhID")%>" />
                  </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="TenBuocQuyTrinh" HeaderText="Tên bước quy trình" 
                  SortExpression="TenBuocQuyTrinh" />
              
             <asp:TemplateField HeaderText='Hành động' >
                  <ItemTemplate >
                  <span >
                      <%#  load_hanhdong(Eval("BuocQuyTrinhID").ToString())%>
                      </span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField ItemStyle-Width="10px"  >
                  <ItemTemplate >
                  <span >
                      <%#  load_hanhdong_action(Eval("BuocQuyTrinhID").ToString())%>
                      </span>
                  </ItemTemplate>
              </asp:TemplateField>

           



         
              
              <asp:TemplateField HeaderText='' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                      <a onclick="open_user_view(<%# Eval("BuocQuyTrinhID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xem"  rel="tooltip" class="btn"><i class="iconsweets-trashcan2" ></i></a>
                      <a onclick="open_user_edit(<%# Eval("BuocQuyTrinhID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Sửa"  rel="tooltip" class="btn"><i class="iconsweets-create" ></i></a>
                      <a onclick="confirm_delete_user(<%# Eval("BuocQuyTrinhID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xóa"  rel="tooltip" class="btn" ><i class="iconsweets-trashcan" ></i></a>                                                   
                      <a onclick="open_action_add(<%# Eval("QuyTrinhID")%>,<%# Eval("BuocQuyTrinhID")%>);" data-placement="top" data-rel="tooltip"  data-original-title="Thêm hành động"  rel="tooltip" class="btn" ><i class="iconfa-plus-sign" ></i></a>                                                   

                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>


          </Columns>
</asp:GridView>

<div class="dataTables_info" id="dyntable_info">Chuyển đến trang: 

<asp:DropDownList ID="Pager" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList>

</div>


</form>





<script type="text/javascript">

<% annut(); %>
    // Array ID được check
    var Users_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#Users_grv .checkall").bind("click", function () {
            Users_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#Users_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });

        $('#Users_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(Users_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa bước quy trình
    function confirm_delete_user(idxoa) {

        $("#div_user_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                    window.location = 'admin.aspx?page=khaibaoquytrinh&act=delete&id=' + idxoa;
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


        // Xác nhận xóa hành động
    function confirm_delete_action(idxoa) {

        $("#div_user_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                    window.location = 'admin.aspx?page=khaibaoquytrinh&act=deleteact&id=' + idxoa;
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    // Sửa thông tin bước quy trình
    function open_user_edit(id) {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=step_edit&id=" + id + "&mode=edit&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 550,
            height: 240,
            title: "<img src='images/icons/application_form_edit.png'>&nbsp;<b>Sửa thông tin bước quy trình</b>",
            modal: true,
            close: function (event, ui) { window.location = 'admin.aspx?page=khaibaoquytrinh'; },
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_user_add'].submitform();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Xem thông tin bước quy trình
    function open_user_view(id) {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=step_edit&id=" + id + "&mode=view&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 550,
            height: 240,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Xem thông tin bước quy trình</b>",
            modal: true,
            buttons: {

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



        // mở form thêm bước quy trình
    function open_step_sort() {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=step_sort&mode=iframe&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 650,
            height: 500,
            title: "<img src='images/icons/application_form_edit.png'>&nbsp;<b>Sắp xếp các bước của quy trình </b>",
            modal: true,
            close: function (event, ui) { window.location = 'admin.aspx?page=khaibaoquytrinh'; },
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_user_add'].submitform();
                   // $("#iframe_user_add")[0].contentWindow.submitform();
                   
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }




    // mở form thêm bước quy trình
    function open_user_add() {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=step_add&mode=iframe&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 550,
            height: 240,
            title: "<img src='images/icons/add.png'>&nbsp;<b>Thêm bước quy trình mới</b>",
            modal: true,
            close: function (event, ui) { window.location = 'admin.aspx?page=khaibaoquytrinh'; },
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_user_add'].submitform();
                   // $("#iframe_user_add")[0].contentWindow.submitform();
                   
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


     // Xem thông tin người dùng
    function open_vacpa_user_view(id) {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=user_edit&id=" + id + "&mode=view&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 660,
            height: 550,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Xem thông tin tài khoản người dùng</b>",
            modal: true,
            buttons: {

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        
        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


      function open_role_view(id) {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=role_edit&id=" + id + "&mode=view&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 850,
            height: 640,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Xem thông tin vai trò</b>",
            modal: true,
            buttons: {

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



      // mở form thêm hành động
    function open_action_add(quytrinhid,buocquytrinhid) {
         var timestamp = Number(new Date());

            $("#div_user_add").empty();
            $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=action_add&quytrinhid="+quytrinhid+"&buocquytrinhid="+buocquytrinhid+"&time=" + timestamp));
            $("#div_user_add").dialog({
                resizable: true,
                width: 700,
                height: 550,
                title: "<img src='images/icons/add.png'>&nbsp;<b>Thêm hành động</b>",
                modal: true,
                close: function (event, ui) { window.location = 'admin.aspx?page=khaibaoquytrinh'; },
                buttons: {
            
                    "Ghi": function () {
                        window.frames['iframe_user_add'].submitform();
                    },                

                    "Bỏ qua": function () {
                        $(this).dialog("close");
                    }
                }

            });

            $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
            $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



    // Sửa thông tin hành động
    function open_action_edit(id) {
        var timestamp = Number(new Date());

        $("#div_user_add").empty();
        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=action_edit&id=" + id + "&mode=edit&time=" + timestamp));
        $("#div_user_add").dialog({
            resizable: true,
            width: 850,
            height: 640,
            title: "<img src='images/icons/application_form_edit.png'>&nbsp;<b>Sửa thông tin hành động</b>",
            modal: true,
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_user_add'].submitform();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }






    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    function setCookie(cname, cvalue, exdays) {
     var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
} 

    // Hiển thị tooltip
    if (jQuery('.btn-group').length > 0) jQuery('.btn-group').tooltip({ selector: "a[rel=tooltip]" });


</script>

<div id="div_user_add" >
</div>


<div id="div_dmchung_search" style="display:none" >
<form id="user_search" method="post" enctype="multipart/form-data" >
<table  width="100%" border="0" class="formtbl"  >
    <tr>        
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
        
        </td>
    </tr>
    
</table>
</form>
</div>



<div id="div_user_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?</p>

</div>



