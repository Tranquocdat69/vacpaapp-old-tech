﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyLopHoc.ascx.cs"
    Inherits="usercontrols_CNKT_QuanLyLopHoc" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" name="Form1" runat="server">
<h4 class="widgettitle">
    Danh sách lớp học</h4>
<div>
    <div class="dataTables_length">
        <a id="btn_them" href="/admin.aspx?page=cnkt_quanlylophoc_add" class="btn btn-rounded">
            <i class="iconfa-plus-sign"></i>Thêm mới</a> <a id="btn_xoa" href="javascript:;"
                class="btn btn-rounded" onclick="confirm_delete_QuanLyLopHoc(gv_DanhSachLopHoc_selected);">
                <i class="iconfa-trash"></i>Xóa</a> <a href="javascript:;" id="btn_search"
                    class="btn btn-rounded" onclick="open_dmgiangvien_search();"><i class="iconfa-search">
                    </i>Tìm kiếm</a> <a id="btn_ketxuat" href="javascript:;" class="btn btn-rounded"
                        onclick="CallExportPage();"><i class="iconfa-download"></i>Kết xuất</a>
    </div>
</div>
<asp:GridView ClientIDMode="Static" ID="gv_DanhSachLopHoc" runat="server" AutoGenerateColumns="False"
    class="table table-bordered responsive dyntable" AllowPaging="True" AllowSorting="True"
    OnPageIndexChanging="gv_DanhSachLopHoc_PageIndexChanging" OnSorting="gv_DanhSachLopHoc_Sorting"
    OnRowDataBound="gv_DanhSachLopHoc_RowDataBound">
    <Columns>
        <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
            ItemStyle-Width="30px">
            <ItemTemplate>
                <input type="checkbox" class="colcheckbox" value="<%# Eval("LopHocID") + ";#" + Eval("MaLopHoc")%>" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="STT">
            <ItemTemplate>
                <span>
                    <%# Container.DataItemIndex + 1%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Mã lớp học" SortExpression="MaLopHoc">
            <ItemTemplate>
                <span>
                    <%# Eval("MaLopHoc")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Từ ngày" SortExpression="TuNgay">
            <ItemTemplate>
                <span>
                    <%# Library.DateTimeConvert(Eval("TuNgay")).ToString("dd/MM/yyyy")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Đến ngày" SortExpression="DenNgay">
            <ItemTemplate>
                <span>
                    <%# Library.DateTimeConvert(Eval("DenNgay")).ToString("dd/MM/yyyy")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Thời lượng (buổi)" SortExpression="SoBuoi">
            <ItemTemplate>
                <span>
                    <%# Eval("SoBuoi") %></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Giảng viên" SortExpression="TenGiangVien">
            <ItemTemplate>
                <span>
                    <%# Eval("TenGiangVien") %></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Trạng thái" SortExpression="TenTrangThai">
            <ItemTemplate>
                <span>
                    <%# Eval("TenTrangThai")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Thao tác" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <div class="btn-group">
                    <a data-placement="top" data-rel="tooltip" href='/admin.aspx?page=cnkt_quanlylophoc_details&id=<%# Eval("LopHocID") %>'
                        data-original-title="Xem" rel="tooltip" class="btn"><i class="iconsweets-trashcan2">
                        </i></a>
                    <asp:HyperLink ID="hplEdit" runat="server" data-placement="top" data-rel="tooltip"
                        data-original-title="Sửa" rel="tooltip" class="btn"><i class="iconsweets-create">
                            </i></asp:HyperLink>
                    <asp:LinkButton ID="lbtDelete" runat="server" data-placement="top" data-rel="tooltip"
                        data-original-title="Xóa" rel="tooltip" class="btn"><i class="iconsweets-trashcan"></i></asp:LinkButton>
                    <asp:HiddenField ID="hfLopHocID" runat="server" Value='<%# Eval("LopHocID") %>' />
                    <asp:HiddenField ID="hfMaTrangThai" runat="server" Value='<%# Eval("MaTrangThai") %>' />
                </div>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div class="dataTables_info" id="dyntable_info">
    <div class="pagination pagination-mini">
        Chuyển đến trang:
        <ul>
            <li><a href="#" onclick="$('#tranghientai').val(0); $('#user_search').submit();"><<
                Đầu tiên</a></li>
            <li><a href="#" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager').val()-1); $('#user_search').submit();}">
                < Trước</a></li>
            <li><a style="border: none; background-color: #eeeeee">
                <asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" Style="width: 55px;
                    height: 22px;" onchange="$('#tranghientai').val(this.value); $('#user_search').submit();">
                </asp:DropDownList>
            </a></li>
            <li style="margin-left: 5px;"><a href="#" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) {$('#tranghientai').val(parseInt($('#Pager').val())+1); $('#user_search').submit();} ;"
                style="border-left: 1px solid #ccc;">Sau ></a></li>
            <li><a href="#" onclick="$('#tranghientai').val($('#Pager option').length-1); $('#user_search').submit();">
                Cuối cùng >></a></li>
        </ul>
    </div>
</div>
</form>
<script type="text/javascript">

<% CheckPermissionOnPage(); %>

    // Array ID được check
    var gv_DanhSachLopHoc_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#gv_DanhSachLopHoc .checkall").bind("click", function () {
            gv_DanhSachLopHoc_selected = new Array();
            checked = $(this).prop("checked");
            $('#gv_DanhSachLopHoc :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) gv_DanhSachLopHoc_selected.push($(this).val().split(';#')[0]);
            });
        });

        $('#gv_DanhSachLopHoc :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(gv_DanhSachLopHoc_selected, $(this).val().split(';#')[0]);
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) gv_DanhSachLopHoc_selected.push($(this).val().split(';#')[0]);
            });
        });
    });


    // Xác nhận xóa
    function confirm_delete_QuanLyLopHoc(idxoa) {
        $("#div_QuanLyLopHoc_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                    window.location = 'admin.aspx?page=cnkt_quanlylophoc&act=delete&id=' + idxoa;
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_QuanLyLopHoc_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_QuanLyLopHoc_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
        }

    // Tìm kiếm
    $(function() {
        $("#textFromDate").datepicker({
            dateFormat: 'dd/mm/yy'
        });
        $("#textToDate").datepicker({
            dateFormat: 'dd/mm/yy'
        });
    });
    
    function open_dmgiangvien_search() {
        $("#div_dmgiangvien_search").dialog({
            resizable: true,
            width: 700,
            height: 200,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }
        });

        $('#div_dmgiangvien_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_dmgiangvien_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function CallExportPage()
    {
        if(gv_DanhSachLopHoc_selected.length > 0) {
            if(gv_DanhSachLopHoc_selected.length > 1) {
                alert('Chỉ được chọn 1 lớp học để kết xuất thư mời!');
            } else {
                $('#gv_DanhSachLopHoc :checkbox').each(function () {
                var checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) {
                    var url = '/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc&idlh=' + $(this).val().split(';#')[0] + '&malh=' + $(this).val().split(';#')[1];
                    window.open(url, '_blank');
                    return;
                }
            });
            }
        } else {
            alert('Phải chọn 1 lớp học để kết xuất thư mời!');
        }
    }


    // Hiển thị tooltip
    if (jQuery('#gv_DanhSachLopHoc').length > 0) jQuery('#gv_DanhSachLopHoc').tooltip({ selector: "a[rel=tooltip]" });
    
    
</script>
<div id="div_dmgiangvien_search" style="display: none">
    <form id="user_search" method="post" enctype="multipart/form-data">
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td style="width: 100px;">
                Kế hoạch năm
            </td>
            <td>
                <select name="ddlNam" id="ddlNam" style="min-width: 100px;">
                    <% LoadListYear(); %>
                </select>
                <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
                <input type="hidden" value="0" id="ketxuat" name="ketxuat" />
            </td>
            <td style="width: 80px;">
                Tháng
            </td>
            <td>
                <select name="ddlThang" id="ddlThang" style="min-width: 100px;">
                    <% LoadListMonth(); %>
                </select>
            </td>
            <td style="width: 80px;">
                Từ ngày
            </td>
            <td>
                <input type="text" name="textFromDate" id="textFromDate" class="input-xlarge" value="<%=Request.Form["textFromDate"]%>" />
            </td>
        </tr>
        <tr>
            <td>
                Mã lớp học
            </td>
            <td>
                <select name="ddlClassID" id="ddlClassID" style="min-width: 100px;">
                    <% LoadListClassID(); %>
                </select>
            </td>
            <td>
                Tỉnh/Thành
            </td>
            <td>
                <select name="ddlProvince" id="ddlProvince" style="min-width: 150px;">
                    <% LoadProvince(); %>
                </select>
            </td>
            <td>
                Đến ngày
            </td>
            <td>
                <input type="text" name="textToDate" id="textToDate" value="<%=Request.Form["textToDate"]%>"
                    class="input-xlarge" />
            </td>
        </tr>
    </table>
    </form>
</div>
<div id="div_QuanLyLopHoc_delete" style="display: none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">
    <p>
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
        Xóa (các) bản ghi được chọn?</p>
</div>
<script type="text/javascript">

    $('#timkiem_Ten').val('<%=Request.Form["timkiem_Ten"]%>');

    $("#div_dmgiangvien_search").keypress(function (event) {
        if (event.which == 13) {
            $("#user_search").submit();
        }
    });

</script>
