﻿using HiPT.VACPA.DL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_CNKT_getsongayhoc_ajax : System.Web.UI.Page
{
    Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            string sql = "SELECT COUNT(*) FROM (SELECT DISTINCT Ngay FROM tblCNKTLopHocBuoi WHERE LopHocID = " + Request.QueryString["lophocid"] + " AND ChuyenDeId IN (" + Request.QueryString["chuyendeid"].TrimEnd(',') + ")) AS X";
            SqlCommand cmd = new SqlCommand(sql);
            string songay = DataAccess.DLookup(cmd);
            if (string.IsNullOrEmpty(songay))
                songay = "0";

            cm.write_ajaxvar("SoNgay", songay);

        }
        catch (Exception ex)
        {
            cm.write_ajaxvar("SoNgay", "");

        }
    }

}