﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_CNKT_QuanLyLopHoc_Process : System.Web.UI.UserControl
{
    private string _tenChucNang = "Quản lý chuyên đề";
    private Db _db = new Db(ListName.ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        string lopHocID = Request.QueryString["lophocid"];
        if (!string.IsNullOrEmpty(lopHocID))
        {
            string action = Request.Form["hdAction"];
            if (action == "update")
                UpdateChuyenDe(lopHocID);
            if (action == "delete")
            {
                DeleteChuyenDe();
            }
            LoadListChuyenDe(lopHocID);
        }
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/20
    /// Update thong tin chuyen de vao DB
    /// </summary>
    private void UpdateChuyenDe(string lopHocID)
    {
        Commons cm = new Commons();
        SqlCnktLopHocChuyenDeProvider cnktLopHocChuyenDePro = new SqlCnktLopHocChuyenDeProvider(ListName.ConnectionString, false, string.Empty);
        CnktLopHocChuyenDe cnktLopHocChuyenDe = new CnktLopHocChuyenDe();

        string chuyenDeId = Request.Form["hdChuyenDeID"];
        if (chuyenDeId.Split(',').Length > 1)
            return;
        string loaiChuyenDe = Request.Form["hdLoaiChuyenDe"];
        string tenChuyenDe = Request.Form["hdTenChuyenDe"];

        DateTime tuNgay = Library.DateTimeConvert(Request.Form["hdTuNgay"], '/', 1);
        DateTime denNgay = Library.DateTimeConvert(Request.Form["hdDenNgay"], '/', 1);
        string soBuoi = Request.Form["hdSoBuoi"];
        string giangVienID = Request.Form["hdGiangVienID"];

        string maLopHoc = Request.Form["hdMaLopHoc"];

        if (!string.IsNullOrEmpty(loaiChuyenDe))
        {
            if (!string.IsNullOrEmpty(chuyenDeId)) // Update
            {
                if (Library.CheckIsInt32(chuyenDeId.Split(',')[0]))
                    cnktLopHocChuyenDe = cnktLopHocChuyenDePro.GetByChuyenDeId(Library.Int32Convert(chuyenDeId.Split(',')[0]));
            }
            else //Insert
            {
                // Gen Ma Chuyen De
                // QUY TAC: [Ma lop hoc] + [Loai chuyen de (KT, DD, KH)] + [01 - 99]
                string maChuyenDe = maLopHoc;
                switch (loaiChuyenDe)
                {
                    case ListName.Type_LoaiChuyenDe_KeToanKiemToan:
                        maChuyenDe += "KT";
                        break;
                    case ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep:
                        maChuyenDe += "DD";
                        break;
                    case ListName.Type_LoaiChuyenDe_ChuyenDeKhac:
                        maChuyenDe += "KH";
                        break;
                }
                maChuyenDe += GetSTT(lopHocID);
                cnktLopHocChuyenDe.MaChuyenDe = maChuyenDe;
                cnktLopHocChuyenDe.LoaiChuyenDe = loaiChuyenDe;
                if (Library.CheckIsInt32(lopHocID))
                    cnktLopHocChuyenDe.LopHocId = Library.Int32Convert(lopHocID);
            }

            cnktLopHocChuyenDe.TenChuyenDe = tenChuyenDe;
            if (tuNgay != new DateTime(1, 1, 1))
                cnktLopHocChuyenDe.TuNgay = tuNgay;
            if (denNgay != new DateTime(1, 1, 1))
                cnktLopHocChuyenDe.DenNgay = denNgay;
            cnktLopHocChuyenDe.SoBuoi = Library.DecimalConvert(soBuoi);
            if (Library.CheckIsInt32(giangVienID))
                cnktLopHocChuyenDe.GiangVienId = Library.Int32Convert(giangVienID);
            if (string.IsNullOrEmpty(chuyenDeId)) //Insert
            {
                if (cnktLopHocChuyenDePro.Insert(cnktLopHocChuyenDe))
                {
                    cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Thêm giá trị \"" + tenChuyenDe + "\" vào danh mục " + this._tenChucNang);
                    Response.Write("<script>parent.DisplayAlertMessage_ChuyenDe(1, 'Cập nhật chuyên đề');parent.ResetControl_ChuyenDe(); parent._danhSachChuyenDen_selected = new Array();</script>");
                }
                else
                {
                    Response.Write("<script>parent.DisplayAlertMessage_ChuyenDe(0, 'Cập nhật chuyên đề');parent.ResetControl_ChuyenDe(); parent._danhSachChuyenDen_selected = new Array();</script>");
                }
            }
            else //Update
            {
                if (cnktLopHocChuyenDePro.Update(cnktLopHocChuyenDe))
                {
                    cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Cập nhật giá trị \"" + tenChuyenDe + "\" vào danh mục " + this._tenChucNang);
                    Response.Write("<script>parent.DisplayAlertMessage_ChuyenDe(1, 'Cập nhật chuyên đề');parent.ResetControl_ChuyenDe(); parent._danhSachChuyenDen_selected = new Array();</script>");
                }
                else
                {
                    Response.Write("<script>parent.DisplayAlertMessage_ChuyenDe(0, 'Cập nhật chuyên đề');parent.ResetControl_ChuyenDe(); parent._danhSachChuyenDen_selected = new Array();</script>");
                }
            }
        }
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/20
    /// Load du lieu Chuyen De tra ve = javascript
    /// </summary>
    private void LoadListChuyenDe(string lopHocID)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var _arrDataChuyenDe = []" + Environment.NewLine;

        try
        {
            _db.OpenConnection();
            string command = @"SELECT tblCNKTLopHocChuyenDe.ChuyenDeID as ChuyenDeID, tblCNKTLopHocChuyenDe.MaChuyenDe as MaChuyenDe, tblCNKTLopHocChuyenDe.TenChuyenDe as TenChuyenDe, tblCNKTLopHocChuyenDe.LoaiChuyenDe as LoaiChuyenDe,
                                                tblCNKTLopHocChuyenDe.TuNgay as TuNgay, tblCNKTLopHocChuyenDe.DenNgay as DenNgay,tblCNKTLopHocChuyenDe.SoBuoi as SoBuoi, tblCNKTLopHocChuyenDe.GiangVienID as GiangVienID, tblCNKTLopHocChuyenDe.SoNguoiHocToiDa,
		                                        tblDMGiangVien.TenGiangVien as TenGiangVien, tblDMTrinhDoChuyenMon.TenTrinhDoChuyenMon as TrinhDoChuyenMon, tblDMGiangVien.DonViCongTac as DonViCongTac, tblDMGiangVien.ChungChi as ChungChi, tblDMChucVu.TenChucVu as TenChucVu
	                                        FROM tblCNKTLopHocChuyenDe
	                                         LEFT JOIN tblDMGiangVien on tblCNKTLopHocChuyenDe.GiangVienID = tblDMGiangVien.GiangVienID
	                                         LEFT JOIN tblDMChucVu on tblDMGiangVien.ChucVuID = tblDMChucVu.ChucVuID
	                                         LEFT JOIN tblDMTrinhDoChuyenMon on tblDMGiangVien.TrinhDoChuyenMonID = tblDMTrinhDoChuyenMon.TrinhDoChuyenMonID
	                                         WHERE tblCNKTLopHocChuyenDe.LopHocID = " + lopHocID + @" ORDER BY tblCNKTLopHocChuyenDe.ChuyenDeID ASC";
            List<Hashtable> listData = _db.GetListData(command);
            if (listData.Count > 0)
            {
                int index = 0;
                foreach (Hashtable ht in listData)
                {
                    string tuNgay = Library.FormatDateTime(Library.DateTimeConvert(ht["TuNgay"]), "dd/MM/yyyy");
                    string denNgay = Library.FormatDateTime(Library.DateTimeConvert(ht["DenNgay"]), "dd/MM/yyyy");
                    double soBuoi = Library.DoubleConvert(ht["SoBuoi"].ToString());
                    js += @"_arrDataChuyenDe[" + index + "] = ['" + ht["ChuyenDeID"] + "','" + ht["MaChuyenDe"] + "','" + ht["TenChuyenDe"] + "','" + ht["LoaiChuyenDe"] + "', '" + GetTenLoaiChuyenDe(ht["LoaiChuyenDe"].ToString()) + "'," +
                          "'" + tuNgay + "','" + denNgay + "','" + Library.ChangeFormatNumber(soBuoi, "vn") + "','" + ht["GiangVienID"] + "','" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenGiangVien"]) + "'," +
                          " '" + ht["TrinhDoChuyenMon"] + "', '" + ht["DonViCongTac"] + "','" + ht["TenChucVu"] + "', '" + ht["ChungChi"] + "', '" + GetLichHocChuyenDe(ht["ChuyenDeID"].ToString()) + "', '" + ht["SoNguoiHocToiDa"] + "'];" + Environment.NewLine;
                    index++;
                }
            }
            _db.CloseConnection();
            js += "parent.DrawData_ChuyenDe(_arrDataChuyenDe);" + Environment.NewLine;
            js += "</script>";
            Response.Write(js);
        }
        catch (Exception)
        {
            _db.CloseConnection();
            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    private string GetLichHocChuyenDe(string idChuyenDe)
    {
        string fullLichHoc = "";
        string query = "SELECT Ngay, Sang, Chieu FROM " + ListName.Table_CNKTLopHocBuoi + " WHERE ChuyenDeID = " + idChuyenDe;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                if (!string.IsNullOrEmpty(ht["Ngay"].ToString()))
                    fullLichHoc += Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy") + ";#" + ht["Sang"] + ";#" + ht["Chieu"] + ";#";
            }
        }

        return fullLichHoc;
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/20
    /// Get so thu tu lon nhat tu chuoi "Ma chuyen de" cua ban ghi moi nhat
    /// </summary>
    /// <param name="lopHocID">Ma Lop Hoc</param>
    /// <returns>So thu tu lon nhat</returns>
    private string GetSTT(string lopHocID)
    {
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                string maChuyenDe = "";
                con.Open();
                string command = @"SELECT TOP 1 MaChuyenDe FROM tblCNKTLopHocChuyenDe WHERE
                                        LopHocID = @LopHocID 
                                        ORDER BY ChuyenDeID DESC";
                SqlCommand cmd = new SqlCommand(command, con);
                cmd.Parameters.Add("@LopHocID", SqlDbType.NVarChar).Value = lopHocID;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        maChuyenDe = dr["MaChuyenDe"].ToString();
                    }
                }
                cmd.Cancel();
                dr.Close();
                con.Close();
                if (string.IsNullOrEmpty(maChuyenDe))
                    return "01";
                string index = maChuyenDe.Substring(maChuyenDe.Length - 2, 2);
                if (!Library.CheckIsInt32(index))
                    return "01";
                int stt = Library.Int32Convert(index) + 1;
                if (stt < 10)
                    return "0" + stt.ToString();
                return stt.ToString();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
        return "01";
    }

    private string GetTenLoaiChuyenDe(string loaiChuyenDe)
    {
        string tenLoaiChuyenDe = "";
        switch (loaiChuyenDe)
        {
            case ListName.Type_LoaiChuyenDe_KeToanKiemToan:
                tenLoaiChuyenDe = "Kế toán, kiểm toán";
                break;
            case ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep:
                tenLoaiChuyenDe = "Đạo đức nghề nghiệp";
                break;
            case ListName.Type_LoaiChuyenDe_ChuyenDeKhac:
                tenLoaiChuyenDe = "Chuyên đề khác";
                break;
        }
        return tenLoaiChuyenDe;
    }

    private void DeleteChuyenDe()
    {
        Commons cm = new Commons();
        SqlCnktLopHocChuyenDeProvider cnktLopHocChuyenDePro = new SqlCnktLopHocChuyenDeProvider(ListName.ConnectionString, false, string.Empty);
        string listChuyenDeId = Request.Form["hdChuyenDeID"];
        if (!string.IsNullOrEmpty(listChuyenDeId))
        {
            string[] arrChuyenDeId = listChuyenDeId.Trim().Split(',');
            foreach (string chuyenDenId in arrChuyenDeId)
            {
                if (Library.CheckIsInt32(chuyenDenId))
                {
                    if (cnktLopHocChuyenDePro.Delete(Library.Int32Convert(chuyenDenId)))
                    {
                        cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Xóa bản ghi có ID \"" + chuyenDenId + "\" của danh mục " + this._tenChucNang);
                    }
                }
            }
            Response.Write("<script>parent.DisplayAlertMessage_ChuyenDe(1, 'Xóa chuyên đề');parent._danhSachChuyenDen_selected = new Array();</script>");
        }
    }
}