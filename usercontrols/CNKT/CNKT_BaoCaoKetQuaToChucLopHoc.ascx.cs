﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodeEngine.Framework.QueryBuilder.Enums;
using HiPT.VACPA.DL;
using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_CNKT_BaoCaoKetQuaToChucLopHoc : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo kết quả tổ chức lớp học";
    protected string _listPermissionOnFunc_BaoCaoKetQuaToChucLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private string _lopHocId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _listPermissionOnFunc_BaoCaoKetQuaToChucLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_BaoCaoKetQuaToChucLopHoc, cm.connstr);

        this._lopHocId = Request.QueryString["lophocid"];
        if (_listPermissionOnFunc_BaoCaoKetQuaToChucLopHoc.Contains("XEM|"))
        {

        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem " + tenchucnang + " !</div>"));
        }
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_BaoCaoKetQuaToChucLopHoc.Contains("SUA|"))
            Response.Write("$('#btnSave').remove();");

        if (!_listPermissionOnFunc_BaoCaoKetQuaToChucLopHoc.Contains("THEM|"))
            Response.Write("$('#btnSave').remove();");

        if (!_listPermissionOnFunc_BaoCaoKetQuaToChucLopHoc.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");

        if (!_listPermissionOnFunc_BaoCaoKetQuaToChucLopHoc.Contains("KETXUAT|"))
            Response.Write("$('#btnExport').remove();");

        if (!_listPermissionOnFunc_BaoCaoKetQuaToChucLopHoc.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
    }
}