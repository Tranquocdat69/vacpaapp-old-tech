﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_GetFile : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = Library.CheckNull(Request.QueryString["id"]); // ID của bản ghi chứa file cần tải
        string type = Library.CheckNull(Request.QueryString["type"]); // Bảng chứa dữ liệu cần tải

        if (!string.IsNullOrEmpty(id) && Library.CheckIsInt32(id))
        {
            string fileName = "";
            Byte[] bytes = null;
            if (type == ListName.Table_KSCLDSKiemTraVuViec) // Kiểm soát chất lượng - Danh sách kiểm tra theo yêu cầu
            {
                SqlKscldsKiemTraVuViecProvider pro = new SqlKscldsKiemTraVuViecProvider(ListName.ConnectionString, false, string.Empty);
                KscldsKiemTraVuViec obj = pro.GetByDsYeuCauKiemTraVuViecId(Library.Int32Convert(id));
                if (obj != null && obj.DsYeuCauKiemTraVuViecId > 0)
                {
                    fileName = obj.TenFile;
                    bytes = obj.FileCanCu;
                }
            }
            if (type == ListName.Table_KSCLDSBaoCaoTuKiemTraChiTiet) // Kiểm soát chất lượng - Danh sách báo cáo tự kiểm tra chi tiết
            {
                SqlKscldsBaoCaoTuKiemTraChiTietProvider pro = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
                KscldsBaoCaoTuKiemTraChiTiet obj = pro.GetByDsBaoCaoTuKiemTraChiTietId(Library.Int32Convert(id));
                if (obj != null && obj.DsBaoCaoTuKiemTraChiTietId > 0)
                {
                    fileName = obj.TenFile;
                    bytes = obj.FileBaoCao;
                }
            }
            if (type == ListName.Table_KSCLCamKet) // Kiểm soát chất lượng - cam kết
            {
                string typeCamKet = Library.CheckNull(Request.QueryString["typecamket"]);
                SqlKsclCamKetProvider pro = new SqlKsclCamKetProvider(ListName.ConnectionString, false, string.Empty);
                KsclCamKet obj = pro.GetByCamKetId(Library.Int32Convert(id));
                if (obj != null && obj.CamKetId > 0)
                {
                    if (typeCamKet == "bmtt")
                    {
                        fileName = obj.TenFileCamKetBaoMat;
                        bytes = obj.CamKetBaoMat;
                    }
                    if (typeCamKet == "dl")
                    {
                        fileName = obj.TenFileCamKetDocLap;
                        bytes = obj.CamKetDocLap;
                    }
                }
            }
            if (type == ListName.Table_KSCLHoSoFile) // Hồ sơ kiểm soát chất lượng - File
            {
                SqlKsclHoSoFileProvider pro = new SqlKsclHoSoFileProvider(ListName.ConnectionString, false, string.Empty);
                KsclHoSoFile obj = pro.GetByHoSoFileId(Library.Int32Convert(id));
                if (obj != null && obj.HoSoFileId > 0)
                {
                    fileName = obj.TenFile;
                    bytes = obj.FileTaiLieu;
                }
            }
            if (type == ListName.Table_KSCLBaoCaoTongHop) // Báo cáo tổng hợp kết quả kiểm tra
            {
                SqlKsclBaoCaoTongHopProvider pro = new SqlKsclBaoCaoTongHopProvider(ListName.ConnectionString, false, string.Empty);
                KsclBaoCaoTongHop obj = pro.GetByBaoCaoTongHopId(Library.Int32Convert(id));
                if (obj != null && obj.BaoCaoTongHopId > 0)
                {
                    fileName = obj.TenFileBienBanKscl;
                    bytes = obj.BienBanKscl;
                }
            }
            if (type == ListName.Table_DMTaiLieu) // Quản lý tài liệu
            {
                SqlDmTaiLieuProvider pro = new SqlDmTaiLieuProvider(ListName.ConnectionString, false, string.Empty);
                DmTaiLieu obj = pro.GetByTaiLieuId(Library.Int32Convert(id));
                if (obj != null && obj.TaiLieuId > 0)
                {
                    fileName = obj.TenFileDinhKem;
                    bytes = obj.FileDinhKem;
                }
            }
            if (type == ListName.Table_TTDangKyLogo) // Quản lý tài liệu
            {
                SqlTtDangKyLogoProvider pro = new SqlTtDangKyLogoProvider(ListName.ConnectionString, false, string.Empty);
                TtDangKyLogo obj = pro.GetByDangKyLoGoId(Library.Int32Convert(id));
                if (obj != null && obj.DangKyLoGoId > 0)
                {
                    fileName = obj.TenFileLogo;
                    bytes = obj.FileLogo;
                }
            }
            if (bytes.Length > 0)
            {
                Response.ClearContent();
                // Send the file to the browser
                //Response.AddHeader("Content-type", contentType);
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }
        }
    }
}