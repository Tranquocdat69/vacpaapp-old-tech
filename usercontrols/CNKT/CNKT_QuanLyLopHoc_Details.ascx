﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyLopHoc_Details.ascx.cs"
    Inherits="usercontrols_CNKT_QuanLyLopHoc_Details" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<form id="form_CreateLopHoc" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Thông tin lớp đào tạo, cập nhật kiến thức</h4>
<div id="thongbaoloi_form_themlophoc" name="thongbaoloi_form_themlophoc" style="display: none;
    margin-top: 5px;" class="alert alert-error">
</div>
<div style="width: 100%; margin-top: 10px; text-align: center;">
    <asp:LinkButton ID="lbtDuyet" runat="server" CssClass="btn" Visible="False" OnClientClick="return confirm('Bạn chắc chắn muốn duyệt bản ghi này chứ?');"
        OnClick="lbtDuyet_Click"><i class="iconfa-thumbs-up"></i>Duyệt</asp:LinkButton>&nbsp;
    <asp:LinkButton ID="lbtTuChoi" runat="server" CssClass="btn" Visible="False" OnClientClick="return confirm('Bạn chắc chắn muốn từ chối bản ghi này chứ?');"
        OnClick="lbtTuChoi_Click"><i class="iconfa-thumbs-down"></i>Từ chối</asp:LinkButton>&nbsp;
    <asp:LinkButton ID="lbtThoaiDuyet" runat="server" CssClass="btn" OnClientClick="return confirm('Bạn chắc chắn muốn thoái duyệt bản ghi này chứ?');"
        Visible="False" OnClick="lbtThoaiDuyet_Click"><i class="iconfa-thumbs-down"></i>Thoái duyệt</asp:LinkButton>&nbsp;
    <asp:HyperLink ID="lbtExport" runat="server" CssClass="btn" Visible="False" NavigateUrl="/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc"
        Target="_blank"><i class="iconfa-download"></i>Kết xuất</asp:HyperLink>&nbsp;
    <a href="javascript:;" id="lbtSendEmail" runat="server" Visible="False" class="btn btn-rounded" onclick="OpenViewEmailPopup();">
        <i class="iconfa-share"></i>Gửi thư mời</a>
        <input type="hidden" id="hdAction" name="hdAction"/>
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td>
                Loại lớp học:
            </td>
            <td colspan="3">
                <asp:Label ID="lblLoaiLopHoc" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Mã lớp học:
            </td>
            <td>
                <asp:Label ID="lblMaLopHoc" runat="server" Text=""></asp:Label>
            </td>
            <td>
                Ngày tạo:
            </td>
            <td>
                <asp:Label ID="lblNgayTao" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Tên lớp học:
            </td>
            <td colspan="3">
                <asp:Label ID="lblTenLopHoc" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Từ ngày:
            </td>
            <td>
                <asp:Label ID="lblTuNgay" runat="server" Text=""></asp:Label>
            </td>
            <td>
                Đến ngày:
            </td>
            <td>
                <asp:Label ID="lblDenNgay" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Hạn đăng ký:
            </td>
            <td>
                <asp:Label ID="lblHanDangKy" runat="server" Text=""></asp:Label>
            </td>
            <td>
                Tỉnh thành:
            </td>
            <td>
                <asp:Label ID="lblTinhThanh" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Địa chỉ lớp học:
            </td>
            <td colspan="3">
                <asp:Label ID="lblDiaChiLopHoc" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
</fieldset>
<div id="DivPhiThanhToan" runat="server" visible="False">
    <fieldset class="fsBlockInfor">
        <legend>Phí thanh toán (*)</legend>
        <div>
            <table id="tblPhiThanhToan" style="width: 870px;" border="0" class="formtbl">
                <tr>
                    <th style="width: 50px;">
                        Số ngày
                    </th>
                    <th style="width: 180px;">
                        Hội viên chính thức
                    </th>
                    <th style="width: 180px;">
                        Hội viên liên kết
                    </th>
                    <th style="width: 180px;">
                        Hội viên danh dự
                    </th>
                    <th style="width: 180px;">
                        Kiểm toán viên
                    </th>
                    <th style="width: 180px;">
                        Người quan tâm là trợ lý KTV
                    </th>
                    <th style="width: 180px;">
                        Người quan tâm không là trợ lý KTV
                    </th>
                </tr>
                <asp:Repeater ID="rpPhiThanhToan" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center;">
                                <%# Eval("SoNgay") %>
                            </td>
                            <td style="text-align: right;">
                                <%# Eval("SoTienPhiHVCNCT") %>
                            </td>
                            <td style="text-align: right;">
                                <%# Eval("SoTienPhiHVCNLK") %>
                            </td>
                            <td style="text-align: right;">
                                <%# Eval("SoTienPhiHVCNDD") %>
                            </td>
                            <td style="text-align: right;">
                                <%# Eval("SoTienPhiKTV") %>
                            </td>
                            <td style="text-align: right;">
                                <%# Eval("SoTienPhiNQT_TroLyKTV") %>
                            </td>
                            <td style="text-align: right;">
                                <%# Eval("SoTienPhiNTQ") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </fieldset>
</div>
</form>
<form id="FormChuyenDe" clientidmode="Static" method="post" enctype="multipart/form-data">
<div id="AlertMessageChuyenDe">
</div>
<div id="thongbaoloi_form_themchuyende" name="thongbaoloi_form_themchuyende" style="display: none;
    margin-top: 5px;" class="alert alert-error">
</div>
<div id="DivChuyenDe" runat="server" visible="False">
    <fieldset class="fsBlockInfor">
        <legend>Danh sách chuyên đề</legend>
        <table id="tblDanhSachChuyenDe" width="100%" border="0" class="formtbl">
            <tr>
                <th>
                    STT
                </th>
                <th>
                    Mã CĐ
                </th>
                <th>
                    Tên chuyên đề
                </th>
                <th>
                    Loại
                </th>
                <th>
                    Giảng viên
                </th>
                <th>
                    Trình độ chuyên môn
                </th>
                <th>
                    Đơn vị công tác
                </th>
                <th>
                    Chức vụ
                </th>
                <th>
                    Lịch học
                </th>
            </tr>
            <asp:Repeater ID="rpChuyenDe" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="text-align: center;">
                            <%# Eval("MaChuyenDe")%>
                        </td>
                        <td style="text-align: center;">
                            <%# Eval("TenChuyenDe")%>
                        </td>
                        <td style="text-align: center;">
                            <%# GetTextLoaiChuyenDe(Eval("LoaiChuyenDe").ToString())%>
                        </td>
                        <td style="text-align: center;">
                            <%# Eval("TenGiangVien") %>
                        </td>
                        <td style="text-align: center;">
                            <%# Eval("TrinhDoChuyenMon") %>
                        </td>
                        <td style="text-align: center;">
                            <%# Eval("DonViCongTac")%>
                        </td>
                        <td style="text-align: center;">
                            <%# Eval("TenChucVu")%>
                        </td>
                        <td style="text-align: center;">
                            <%# GetLichHocChuyenDe(Eval("ChuyenDeID").ToString())%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </fieldset>
</div>
</form>
<iframe name="iframeProcessChuyenDe" width="0px" height="0px"></iframe>
<script type="text/javascript">
    function OpenViewEmailPopup()
    {
        $("#divViewEmail").empty();
        $("#divViewEmail").append($("<iframe width='100%' height='100%' id='ifViewEmail' name='ifViewEmail' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyLopHoc_Email&idlh=" + getParameterByName('id')));
        $("#divViewEmail").dialog({
            resizable: true,
            width: 950,
            height: 750,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Nội dung Email mời tham dự lớp học</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Gửi Email": function () {
                    //$('#hdAction').val('SendEmail');
                    //$("#form_CreateLopHoc").submit();
                    window.frames['ifViewEmail'].submitform();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#divViewEmail').parent().find('button:contains("Gửi Email")').addClass('btn btn-rounded').prepend('<i class="iconfa-share"> </i>&nbsp;');
        $('#divViewEmail').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    <% CheckPermissionOnPage(); %>

</script>
<div id="divViewEmail">
</div>