﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyLopHoc_MiniList.ascx.cs" Inherits="usercontrols_CNKT_QuanLyLopHoc_MiniList" %>

<style type="text/css">
    .tdTable
    {
        width: 120px;
    }
</style>
<form id="FormDanhSachGiangVien" runat="server" method="post" enctype="multipart/form-data">
<fieldset class="fsBlockInfor">
    <legend>Tiêu chí tìm kiếm</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td>
                ID<span class="starRequired">(*)</span>:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtMaLopHoc" runat="server"></asp:TextBox>
            </td>
            <td>
                Từ ngày:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtTuNgay" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Tên lớp:</td>
            <td>
                <asp:TextBox ID="txtTenLop" runat="server"></asp:TextBox>
            </td>
            <td>Đến ngày:</td>
            <td>
                <asp:TextBox ID="txtDenNgay" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearchPopup" runat="server" CssClass="btn" 
        onclick="lbtSearch_Click" ><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách lớp học</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <th style="width: 30px;">
                
            </th>
            <th style="width: 50px;">
                STT
            </th>
            <th style="width: 100px;">
                Mã lớp
            </th>
            <th style="min-width: 150px;">
                Tên lớp
            </th>
            <th style="width: 80px;">
                Từ ngày
            </th>
            <th style="width: 80px;">
                Đến ngày
            </th>
        </tr>
    </table>
    <div id='DivListLopHoc' style="max-height: 300px;">
        <table id="tblLopHoc" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpLopHoc" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="width: 30px; vertical-align: middle;">
                            <input type="radio" value='<%# Eval("LopHocID") + ";#" + Eval("MaLopHoc") + ";#" + Eval("TenLopHoc") + ";#"
                             + (!string.IsNullOrEmpty(Eval("TuNgay").ToString()) ? Library.DateTimeConvert(Eval("TuNgay")).ToString("dd/MM/yyyy") : "")
                              + ";#" + (!string.IsNullOrEmpty(Eval("DenNgay").ToString()) ? Library.DateTimeConvert(Eval("DenNgay")).ToString("dd/MM/yyyy") : "")
                             + ";#" + (!string.IsNullOrEmpty(Eval("HanDangKy").ToString()) ? Library.DateTimeConvert(Eval("HanDangKy")).ToString("dd/MM/yyyy") : "") + ";#" + Eval("TenTinh") + ";#" + Eval("DiaChiLopHoc") %>'
                                name="LopHoc" />
                        </td>
                        <td style="width: 50px; text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("MaLopHoc")%>
                        </td>
                        <td style="min-width: 150px;">
                            <%# Eval("TenLopHoc") + "   " + GetStatusLopHoc(Eval("TuNgay"), Eval("DenNgay"))%>
                        </td>
                        <td style="width: 80px;">
                            <%# !string.IsNullOrEmpty(Eval("TuNgay").ToString()) ? Library.DateTimeConvert(Eval("TuNgay")).ToString("dd/MM/yyyy") : ""%>
                        </td>
                        <td style="width: 80px;">
                            <%# !string.IsNullOrEmpty(Eval("DenNgay").ToString()) ? Library.DateTimeConvert(Eval("DenNgay")).ToString("dd/MM/yyyy") : ""%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</fieldset>
<div style="width: 100%; text-align: right; margin-top: 5px;">
    <a id="btnChoose" href="javascript:;" class="btn" onclick="ChooseLopHoc();"><i
        class="iconfa-ok"></i>Chọn</a> <a id="btnExit" href="javascript:;" class="btn" onclick="parent.CloseFormDanhSachLopHoc();">
            <i class="iconfa-stop"></i>Đóng</a>
</div>
</form>
<script type="text/javascript">
    $("#<%= FormDanhSachGiangVien.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtSearchPopup.ClientID %>').attr('href'));
        }
    });

    // Tìm kiếm
    $(function () {
        $("#<%= txtTuNgay.ClientID %>").datepicker({
            dateFormat: 'dd/mm/yy'
        });
        $("#<%= txtDenNgay.ClientID %>").datepicker({
            dateFormat: 'dd/mm/yy'
        });
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm lấy thông tin lớp học được chọn từ danh sách
    function ChooseLopHoc() {
        var flag = false;
        $('#tblLopHoc :radio').each(function () {
            checked = $(this).prop("checked");
            if (checked) {
                // Lấy chuỗi thông tin lớp học -> gọi hàm nhận giá trị ở trang cha
                var chooseItem = $(this).val();
                parent.DisplayInforLopHoc(chooseItem);
                parent.CloseFormDanhSachLopHoc();
                flag = true;
                return;
            }
        });
        // Bắt buộc phải chọn 1 học viên -> nếu ko thì bật cảnh báo
        if (!flag)
            alert('Phải chọn lớp học!');
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm kiểm tra trình duyệt để set thuộc tính overflow cho thẻ DIV hiển thị danh sách bản ghi
    // Chỉ trình duyệt nền Webkit mới có thuộc tính overflow: overlay
    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivListLopHoc').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivListLopHoc').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivListLopHoc').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }

    myFunction();
</script>