﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
public partial class usercontrols_CNKT_BaoCao_GiayChungNhanGioCNKTChoKTV : System.Web.UI.UserControl
{
    private string _idCongTy = "", _idLopHoc = "";
    protected string tenchucnang = "Mẫu đăng ký tham dự cập nhật kiến thức";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        _idCongTy = Library.CheckNull(Request.QueryString["idct"]);
        _idLopHoc = Library.CheckNull(Request.QueryString["idlh"]);
        try
        {
            _db.OpenConnection();
            ReportDocument rpt = new ReportDocument();
            rpt.Load(Server.MapPath("Report/CNKT/MauDangKyThamDuCapNhatKienThuc.rpt"));
            string query = "select * from tblCNKTLopHoc WHERE LopHocID = " + _idLopHoc;
            ;
            rpt.Database.Tables["tblCNKTLopHoc"].SetDataSource(_db.GetDataTable("select * from tblCNKTLopHoc"));
            rpt.Database.Tables["tblHoiVienTapThe"].SetDataSource(_db.GetDataTable("select * from tblHoiVienTapThe"));
            CrystalReportViewer1.ReportSource = rpt;

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }
    DataSet GetData(string sql,string tableName)
    {
        DataSet ds=new DataSet();
        DataTable dt=new DataTable();
        dt.TableName=tableName;
        SqlConnection connect = new SqlConnection("Data Source=vacpa;Initial Catalog=VACPA;User ID=sa;Password=vacpa@123;");
        SqlCommand cmd;
        SqlDataAdapter sda;

        if (connect.State != ConnectionState.Open)
        {
            connect.Open();
        }
        cmd = new SqlCommand(sql,connect);
        sda = new SqlDataAdapter(cmd);
        sda.Fill(dt);
        ds.Tables.Add(dt);

        if (connect.State != ConnectionState.Closed)
        {
            connect.Close();
        }
        return ds;
    }



    protected void lbtExportWord_Click(object sender, EventArgs e)
    {
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("Report/CNKT/MauDangKyThamDuCapNhatKienThuc.rpt"));

        //report.SetDataSource(Cnkt.GetThongTinCongTyById(_idCongTy));
        CrystalReportViewer1.ReportSource = report;

        report.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "MauDangKyThamDuCapNhatKienThuc");
    }
    protected void lbtExportPdf_Click(object sender, EventArgs e)
    {
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("Report/CNKT/MauDangKyThamDuCapNhatKienThuc.rpt"));

        //report.SetDataSource(Cnkt.GetThongTinCongTyById(_idCongTy));
        CrystalReportViewer1.ReportSource = report;

        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "MauDangKyThamDuCapNhatKienThuc");
    }
}