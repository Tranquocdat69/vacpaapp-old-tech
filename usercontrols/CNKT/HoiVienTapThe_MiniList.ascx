﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HoiVienTapThe_MiniList.ascx.cs" Inherits="usercontrols_HoiVienTapThe_MiniList" %>

<style type="text/css">
    .tdTable
    {
        width: 120px;
    }
</style>
<form id="FormDanhSachCongTy" runat="server" method="post" enctype="multipart/form-data">
<fieldset class="fsBlockInfor">
    <legend>Tiêu chí tìm kiếm</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td>
                Mã<span class="starRequired">(*)</span>:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtMaCongTy" runat="server"></asp:TextBox>
            </td>
            <td>
                Tên:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtTenCongTy" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Phân loại:
            </td>
            <td>
                <asp:DropDownList ID="ddlPhanLoai" runat="server" Width="120px">
                    <asp:ListItem Value="">Tất cả</asp:ListItem>
                    <asp:ListItem Value="<%= ListName.Type_LoaiCongTy_CtyKTDaDuocCapID %>">Công ty kiểm toán đã được cấp ID đăng nhập</asp:ListItem>
                    <asp:ListItem Value="<%= ListName.Type_LoaiCongTy_HoiVienTapThe %>">Hội viên tổ chức</asp:ListItem>
                    <asp:ListItem Value="<%= ListName.Type_LoaiCongTy_CtyKTChuaDuocCapID %>">Công ty kiểm toán chưa được cấp ID đăng nhập</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2">
            </td>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearchPopup" runat="server" CssClass="btn" OnClick="lbtSearch_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách công ty</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <th style="width: 30px;">
            </th>
            <th style="width: 35px;">
                STT
            </th>
            <th style="width: 80px;">
                ID
            </th>
            <th style="min-width: 150px;">
                Tên công ty
            </th>
            <th style="width: 200px;">
                Địa chỉ
            </th>
            <th style="width: 150px;">
                Người đại diện
            </th>
        </tr>
    </table>
    <div id='DivListCongTy' style="max-height: 200px;">
        <table id="tblCongTy" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpCongTy" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="width: 30px; vertical-align: middle;">
                            <input type="radio" value='<%# Eval("HoiVienTapTheID") + ";#" + Eval("MaHoiVienTapThe") + ";#" + Eval("TenDoanhNghiep")
                             + ";#" + Eval("TenVietTat") + ";#" + Eval("MaSoThue") + ";#" + Eval("SoGiayChungNhanDKKD")
                             + ";#" + (!string.IsNullOrEmpty(Eval("NgayGiaNhap").ToString()) ? Library.DateTimeConvert(Eval("NgayGiaNhap")).ToString("dd/MM/yyyy") : "")
                             + ";#" + (!string.IsNullOrEmpty(Eval("NgayCapGiayChungNhanDKKD").ToString()) ? Library.DateTimeConvert(Eval("NgayCapGiayChungNhanDKKD")).ToString("dd/MM/yyyy") : "")
                             + ";#" + Eval("NguoiDaiDienLL_Ten") + ";#" + Eval("NguoiDaiDienLL_DiDong") + ";#" + Eval("NguoiDaiDienLL_Email") %>'
                                name="HocVien" />
                        </td>
                        <td style="width: 35px; text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="width: 80px;">
                            <%# Eval("MaHoiVienTapThe")%>
                        </td>
                        <td style="min-width: 150px;">
                            <%# Eval("TenDoanhNghiep") + " <i>(" + Eval("TenVietTat") + ")</i>"%>
                        </td>
                        <td style="width: 200px;">
                            <%# Eval("DiaChi") %>
                        </td>
                        <td style="width: 150px;">
                            <%# Eval("NguoiDaiDienLL_Ten")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</fieldset>
<div style="width: 100%; text-align: right; margin-top: 5px;">
    <a id="btnChoose" href="javascript:;" class="btn" onclick="ChooseCongTy();"><i class="iconfa-ok">
    </i>Chọn</a> <a id="btnExit" href="javascript:;" class="btn" onclick="parent.CloseFormDanhSachCongTy();">
        <i class="iconfa-stop"></i>Đóng</a>
</div>
</form>
<script type="text/javascript">
    $("#<%= FormDanhSachCongTy.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtSearchPopup.ClientID %>').attr('href'));
        }
    });

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm lấy thông tin công ty được chọn từ danh sách
    function ChooseCongTy() {
        var flag = false;
        $('#tblCongTy :radio').each(function () {
            checked = $(this).prop("checked");
            if (checked) {
                // Lấy chuỗi thông tin công ty -> gọi hàm nhận giá trị ở trang cha
                var chooseItem = $(this).val();
                parent.DisplayInforCongTy(chooseItem);
                parent.CloseFormDanhSachCongTy();
                flag = true;
                return;
            }
        });
        // Bắt buộc phải chọn 1 công ty -> nếu ko thì bật cảnh báo
        if (!flag)
            alert('Phải chọn công ty!');
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm kiểm tra trình duyệt để set thuộc tính overflow cho thẻ DIV hiển thị danh sách bản ghi
    // Chỉ trình duyệt nền Webkit mới có thuộc tính overflow: overlay
    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivListCongTy').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivListCongTy').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivListCongTy').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }

    myFunction();
</script>