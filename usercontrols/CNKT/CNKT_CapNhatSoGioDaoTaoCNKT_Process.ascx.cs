﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_CNKT_CapNhatSoGioDaoTaoCNKT_Process : System.Web.UI.UserControl
{
    private Db _db = new Db(ListName.ConnectionString);
    private Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.QueryString["action"];
        try
        {
            _db.OpenConnection();
            string lopHocId = Request.QueryString["idlh"];
            if (action == "loaddanhsachhoivienthamdulophoc")
                GetDanhSachHoiVienThamDuLopHoc(lopHocId);
            if (action == "loaddanhsachnguoiquantamthamdulophoc")
                GetDanhSachNguoiQuanTamThamDuLopHoc(lopHocId);
            if (action == "delete")
            {
                if (!string.IsNullOrEmpty(lopHocId))
                {
                    Delete(lopHocId, true);
                    SqlCnktLopHocProvider lopHoc_provider = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
                    cm.ghilog("CapNhatSoGioCNKT", "\"" + Session["Admin_TenDangNhap"] + "\" xóa dữ liệu giờ CNKT của lớp " + lopHoc_provider.GetByLopHocId(int.Parse(lopHocId)).TenLopHoc);
                }
            }

            string hdLopHocId = Request.Form["hdLopHocId"];
            string hdValue = Request.Form["hdValue"];
            if (!string.IsNullOrEmpty(hdLopHocId)) // Update bản ghi số giờ học thực tế
            {
                Save(hdLopHocId, hdValue);
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/08
    /// Lấy danh sách hội viên cá nhân, kiểm toán viên tham dự lớp học
    /// </summary>
    /// <param name="lopHocId">ID lớp học</param>
    private void GetDanhSachHoiVienThamDuLopHoc(string lopHocId)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        js += "var arrData_LopHocHocVien = [];" + Environment.NewLine;
        js += "var arrData_GioHocThucTe = [];" + Environment.NewLine;
        string query = @"SELECT tblCNKTDangKyHocCaNhan.DangKyHocCaNhanID as dkhcnid, tblHoiVienCaNhan.HoiVienCaNhanID AS HoiVienCaNhanID,
                                 tblHoiVienCaNhan.MaHoiVienCaNhan AS mhvcn, (tblHoiVienCaNhan.HoDem + ' ' + tblHoiVienCaNhan.Ten) AS FullName,
                                 tblHoiVienCaNhan.LoaiHoiVienCaNhan AS LoaiHoiVienCaNhan, tblHoiVienCaNhan.SoChungChiKTV AS SoChungChiKTV, tblHoiVienCaNhan.NgayCapChungChiKTV AS ngayCapChungChiKTV,
                                 tblHoiVienTapThe.HoiVienTapTheID AS hoiVienTapTheID, tblHoiVienTapThe.MaHoiVienTapThe AS maHoiVienTapThe,
                                 tblHoiVienTapThe.SoHieu + ' - ' + tblHoiVienTapThe.TenDoanhNghiep AS TenDoanhNghiep, tblCNKTLopHoc.MaLopHoc AS MaLopHoc
                                FROM tblCNKTDangKyHocCaNhan 
                                LEFT JOIN tblHoiVienCaNhan ON tblCNKTDangKyHocCaNhan.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID
                                LEFT JOIN tblHoiVienTapThe ON tblHoiVienCaNhan.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
                                LEFT JOIN tblCNKTLopHoc ON tblCNKTDangKyHocCaNhan.LopHocID = tblCNKTLopHoc.LopHocID
                                WHERE tblCNKTDangKyHocCaNhan.LopHocID = " + lopHocId + @" AND (tblHoiVienCaNhan.LoaiHoiVienCaNhan = 1 OR tblHoiVienCaNhan.LoaiHoiVienCaNhan = 2)
                                ORDER BY tblHoiVienTapThe.SoHieu ASC, tblHoiVienCaNhan.SoChungChiKTV ASC";
        List<Hashtable> list = _db.GetListData(query);
        if (list.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in list)
            {
                js += "arrData[" + index + "] = ['" + ht["HoiVienCaNhanID"] + "','" + ht["mhvcn"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["FullName"]) + "','" + ht["LoaiHoiVienCaNhan"] + "','" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + "', '" + ht["SoChungChiKTV"] + "'," +
                                                "'" + (!string.IsNullOrEmpty(ht["ngayCapChungChiKTV"].ToString()) ? Library.DateTimeConvert(ht["ngayCapChungChiKTV"]).ToString("dd/MM/yyyy") : "") + "'];" + Environment.NewLine;
                index++;
            }
        }

        query = "SELECT HoiVienCaNhanID, ChuyenDeID FROM tblCNKTLopHocHocVien LHHV LEFT JOIN tblDMTrangThai TT ON LHHV.TinhTrangID = TT.TrangThaiID WHERE LopHocID = " + lopHocId + @" AND TT.MaTrangThai = '" + ListName.Status_DaPheDuyet + @"'";
        List<Hashtable> listDataLopHocHocVien = _db.GetListData(query);
        if (listDataLopHocHocVien.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in listDataLopHocHocVien)
            {
                js += "arrData_LopHocHocVien[" + index + "] = ['" + ht["HoiVienCaNhanID"] + "','" + ht["ChuyenDeID"] + "'];" + Environment.NewLine;
                index++;
            }
        }

        // Lấy số giờ học thực tế của các học viên
        query = @"SELECT LopHocSoGioID, HoiVienCaNhanID, tblCNKTLopHocSoGio.ChuyenDeID AS CDID , tblCNKTLopHocChuyenDe.TenChuyenDe AS TenChuyenDe, tblCNKTLopHocChuyenDe.LoaiChuyenDe AS LoaiChuyenDe, SoGioThucTe
                        FROM tblCNKTLopHocSoGio
                        LEFT JOIN tblCNKTLopHocChuyenDe ON tblCNKTLopHocSoGio.ChuyenDeID = tblCNKTLopHocChuyenDe.ChuyenDeID
                        WHERE tblCNKTLopHocSoGio.LopHocID = " + lopHocId + @" ORDER BY LopHocSoGioID DESC";
        List<Hashtable> list_GioHocThucTe = _db.GetListData(query);
        if (list.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in list_GioHocThucTe)
            {
                js += "arrData_GioHocThucTe[" + index + "] = ['" + ht["LopHocSoGioID"] + "','" + ht["HoiVienCaNhanID"] + "', '" + ht["CDID"] + "','" + ht["TenChuyenDe"] + "','" + ht["LoaiChuyenDe"] + "','" + Library.DoubleConvert(ht["SoGioThucTe"]) + "'];" + Environment.NewLine;
                index++;
            }
        }

        js += "parent.DrawData_DanhSachHoiVienThamDuLopHoc(arrData, arrData_LopHocHocVien, arrData_GioHocThucTe);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/08
    /// Lấy danh sách người quan tâm tham dự lớp học
    /// </summary>
    /// <param name="lopHocId">ID lớp học</param>
    private void GetDanhSachNguoiQuanTamThamDuLopHoc(string lopHocId)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        js += "var arrData_LopHocHocVien = [];" + Environment.NewLine;
        js += "var arrData_GioHocThucTe = [];" + Environment.NewLine;
        string query = @"SELECT tblCNKTDangKyHocCaNhan.DangKyHocCaNhanID as dkhcnid, tblHoiVienCaNhan.HoiVienCaNhanID AS HoiVienCaNhanID,
                                 tblHoiVienCaNhan.MaHoiVienCaNhan AS mhvcn, (tblHoiVienCaNhan.HoDem + ' ' + tblHoiVienCaNhan.Ten) AS FullName,
                                 tblHoiVienCaNhan.LoaiHoiVienCaNhan AS LoaiHoiVienCaNhan, tblDMChucVu.TenChucVu AS TenChucVu,
                                 tblHoiVienTapThe.HoiVienTapTheID AS hoiVienTapTheID, tblHoiVienTapThe.MaHoiVienTapThe AS maHoiVienTapThe,
                                 tblHoiVienTapThe.TenDoanhNghiep AS TenDoanhNghiep, tblCNKTLopHoc.MaLopHoc AS MaLopHoc
                                FROM tblCNKTDangKyHocCaNhan 
                                LEFT JOIN tblHoiVienCaNhan ON tblCNKTDangKyHocCaNhan.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID
                                LEFT JOIN tblHoiVienTapThe ON tblHoiVienCaNhan.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
                                LEFT JOIN tblCNKTLopHoc ON tblCNKTDangKyHocCaNhan.LopHocID = tblCNKTLopHoc.LopHocID
                                LEFT JOIN tblDMChucVu ON tblHoiVienCaNhan.ChucVuID = tblDMChucVu.ChucVuID
                                WHERE tblCNKTDangKyHocCaNhan.LopHocID = " + lopHocId + @" AND tblHoiVienCaNhan.LoaiHoiVienCaNhan = 0
                                ORDER BY tblHoiVienTapThe.SoHieu ASC, tblHoiVienCaNhan.SoChungChiKTV ASC";
        List<Hashtable> list = _db.GetListData(query);
        if (list.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in list)
            {
                js += "arrData[" + index + "] = ['" + ht["HoiVienCaNhanID"] + "','" + ht["mhvcn"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["FullName"]) + "','" + ht["TenChucVu"] + "','" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + "'];" + Environment.NewLine;
                index++;
            }
        }

        query = "SELECT HoiVienCaNhanID, ChuyenDeID FROM tblCNKTLopHocHocVien LHHV LEFT JOIN tblDMTrangThai TT ON LHHV.TinhTrangID = TT.TrangThaiID WHERE LopHocID = " + lopHocId + @" AND TT.MaTrangThai = '" + ListName.Status_DaPheDuyet + @"'";
        List<Hashtable> listDataLopHocHocVien = _db.GetListData(query);
        if (listDataLopHocHocVien.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in listDataLopHocHocVien)
            {
                js += "arrData_LopHocHocVien[" + index + "] = ['" + ht["HoiVienCaNhanID"] + "','" + ht["ChuyenDeID"] + "'];" + Environment.NewLine;
                index++;
            }
        }

        // Lấy số giờ học thực tế của các học viên
        query = @"SELECT LopHocSoGioID, HoiVienCaNhanID, tblCNKTLopHocSoGio.ChuyenDeID AS CDID , tblCNKTLopHocChuyenDe.TenChuyenDe AS TenChuyenDe, tblCNKTLopHocChuyenDe.LoaiChuyenDe AS LoaiChuyenDe, SoGioThucTe
                        FROM tblCNKTLopHocSoGio
                        LEFT JOIN tblCNKTLopHocChuyenDe ON tblCNKTLopHocSoGio.ChuyenDeID = tblCNKTLopHocChuyenDe.ChuyenDeID
                        WHERE tblCNKTLopHocSoGio.LopHocID = " + lopHocId + @" ORDER BY LopHocSoGioID DESC";
        List<Hashtable> list_GioHocThucTe = _db.GetListData(query);
        if (list.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in list_GioHocThucTe)
            {
                js += "arrData_GioHocThucTe[" + index + "] = ['" + ht["LopHocSoGioID"] + "','" + ht["HoiVienCaNhanID"] + "', '" + ht["CDID"] + "','" + ht["TenChuyenDe"] + "','" + ht["LoaiChuyenDe"] + "','" + Library.DoubleConvert(ht["SoGioThucTe"]) + "'];" + Environment.NewLine;
                index++;
            }
        }

        js += "parent.DrawData_DanhSachNguoiQuanTam(arrData, arrData_LopHocHocVien, arrData_GioHocThucTe);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/08
    /// Lưu số giờ học thực tế của các học viên trong lớp
    /// </summary>
    /// <param name="lopHocId">ID lớp học</param>
    /// <param name="hdValue">Chuỗi giá trị (dạng: HocVienID1_ChuyenDeID1_SoGioHoc1;#HocVienID2_ChuyenDeID2_SoGioHoc2;# ...)</param>
    private void Save(string lopHocId, string hdValue)
    {
        Delete(lopHocId, false);
        SqlCnktLopHocSoGioProvider lopHocSoGioPro = new SqlCnktLopHocSoGioProvider(ListName.ConnectionString, false, string.Empty);
        // Phân tích giá trị trong hdVualue (có dạng: HocVienID1_ChuyenDeID1_SoGioHoc1;#HocVienID2_ChuyenDeID2_SoGioHoc2;# ...)
        string[] arrHdValue = hdValue.Split(new string[] { ";#" }, StringSplitOptions.None);
        for (int i = 0; i < arrHdValue.Length; i++)
        {
            if (!string.IsNullOrEmpty(arrHdValue[i]))
            {
                string[] arrValue = arrHdValue[i].Split('_');
                if (!string.IsNullOrEmpty(arrValue[2]))
                {
                    CnktLopHocSoGio lopHocSoGio = new CnktLopHocSoGio();
                    lopHocSoGio.LopHocId = Library.Int32Convert(lopHocId);
                    lopHocSoGio.HoiVienCaNhanId = Library.Int32Convert(arrValue[0]);
                    lopHocSoGio.ChuyenDeId = Library.Int32Convert(arrValue[1]);
                    lopHocSoGio.SoGioThucTe = Library.DecimalConvert(arrValue[2]);
                    lopHocSoGio.NgayNhap = DateTime.Now;
                    lopHocSoGio.NguoiNhap = cm.Admin_TenDangNhap;
                    lopHocSoGioPro.Insert(lopHocSoGio);
                }
            }
        }

        SqlCnktLopHocProvider lopHoc_provider = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
        cm.ghilog("CapNhatSoGioCNKT", "\"" + Session["Admin_TenDangNhap"] + "\" cập nhật giờ CNKT cho lớp " + lopHoc_provider.GetByLopHocId(int.Parse(lopHocId)).TenLopHoc);

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.SaveSuccess(1);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/09
    /// Xóa tất cả dữ liệu số giờ theo lớp học
    /// </summary>
    /// <param name="lopHocId">ID lớp học cần xóa dữ liệu</param>
    /// <param name="returnJS">Có trả về thông báo qua JS ko (0: không; 1: Có)</param>
    private void Delete(string lopHocId, bool returnJS)
    {
        string deleteCommand = "DELETE FROM tblCNKTLopHocSoGio WHERE LopHocID = " + lopHocId;
        _db.ExecuteNonQuery(deleteCommand);
        if (returnJS)
        {
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            js += "parent.DeleteSuccess(1);" + Environment.NewLine;
            js += "</script>";
            Response.Write(js);
        }
    }
}