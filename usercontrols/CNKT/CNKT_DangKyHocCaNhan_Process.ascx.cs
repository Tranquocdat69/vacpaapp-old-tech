﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_CNKT_DangKyHocCaNhan_Process : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        string lopHocID = Request.QueryString["lophocid"];
        string maHocVien = Request.QueryString["mahv"];
        string maLopHoc = Request.QueryString["malh"];
        string maCongTy = Request.QueryString["mact"];
        string hocVienId = Request.QueryString["idhv"];
        string lopHocId = Request.QueryString["idlh"];
        string congTyId = Request.QueryString["idct"];
        string action = Request.QueryString["action"];
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(lopHocID) && action == "loadcd")
                GetListChuyenDe(lopHocID);
            if (action == "loadhv")
                GetInforHocVien(maHocVien, congTyId);
            if (action == "loadlisthv")
                GetListHocVienByCongTy(congTyId);
            if (action == "loadhvbyid")
                GetInforHocVienById(hocVienId);
            if (action == "loadlh")
                GetInforLopHoc(maLopHoc);
            if (action == "loadlhhv")
                GetLopHoc_HocVien(lopHocId, hocVienId);
            if (action == "loadlhct")
                GetLopHoc_CongTy(lopHocId, congTyId);
            if (action == "loadct")
                GetInforCongTyKiemToan_HVTT(maCongTy);
            if (action == "checkcd")
                CheckSoNguoiHocChuyenDe(lopHocId);
            if (action == "savedangkyhocvien")
            {
                string listChuyenDeID = Request.QueryString["listchuyendeid"];
                string ngayDangKy = Request.QueryString["ngaydangky"];
                string layGiayChungNhan = Request.QueryString["laygcn"];
                string dangKyHocCaNhanID = Request.QueryString["dangkyhoccanhanid"];
                string soGioCNKT = Request.QueryString["sogiocnkt"];
                string tongPhi = Request.QueryString["tongphi"];
                string isClose = Request.QueryString["isclose"];
                SaveDangKyCaNhan(lopHocId, hocVienId, listChuyenDeID, ngayDangKy, layGiayChungNhan, congTyId, dangKyHocCaNhanID, soGioCNKT, tongPhi, isClose);
            }
            if (action == "deletedangkyhoc")
            {
                string idDangKyHoc = Request.QueryString["iddangkyhoc"];
                Delete(idDangKyHoc, true);
                GetLopHoc_CongTy(lopHocId, congTyId);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/20
    /// Load du lieu Chuyen De tra ve = javascript
    /// </summary>
    private void GetListChuyenDe(string lopHocID)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var _arrDataChuyenDe = [];" + Environment.NewLine;
        js += "var arrDataHocPhi = [];" + Environment.NewLine;
        string command = @"SELECT tblCNKTLopHocChuyenDe.ChuyenDeID as ChuyenDeID, tblCNKTLopHocChuyenDe.MaChuyenDe as MaChuyenDe, tblCNKTLopHocChuyenDe.TenChuyenDe as TenChuyenDe, tblCNKTLopHocChuyenDe.LoaiChuyenDe as LoaiChuyenDe,
                                    tblCNKTLopHocChuyenDe.TuNgay as TuNgay, tblCNKTLopHocChuyenDe.DenNgay as DenNgay,tblCNKTLopHocChuyenDe.SoBuoi as SoBuoi, tblCNKTLopHocChuyenDe.GiangVienID as GiangVienID, tblCNKTLopHocChuyenDe.SoNguoiHocToiDa,
                                    (SELECT COUNT(LopHocHocVienID) FROM tblCNKTLopHocHocVien INNER JOIN tblDMTrangThai on tblCNKTLopHocHocVien.TinhTrangID = tblDMTrangThai.TrangThaiID INNER JOIN tblHoiVienCaNhan ON tblCNKTLopHocHocVien.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID AND tblHoiVienCaNhan.LoaiHoiVienCaNhan != '0' WHERE LopHocID = " + lopHocID + @" AND tblCNKTLopHocHocVien.ChuyenDeID = tblCNKTLopHocChuyenDe.ChuyenDeID AND tblDMTrangThai.MaTrangThai = '5') SoNguoiDaDangKy,
		                            tblDMGiangVien.TenGiangVien as TenGiangVien, tblDMTrinhDoChuyenMon.TenTrinhDoChuyenMon as TrinhDoChuyenMon, tblDMGiangVien.DonViCongTac as DonViCongTac, tblDMGiangVien.ChungChi as ChungChi, tblDMChucVu.TenChucVu as TenChucVu
	                            FROM tblCNKTLopHocChuyenDe
	                                LEFT JOIN tblDMGiangVien on tblCNKTLopHocChuyenDe.GiangVienID = tblDMGiangVien.GiangVienID
	                                LEFT JOIN tblDMChucVu on tblDMGiangVien.ChucVuID = tblDMChucVu.ChucVuID
	                                LEFT JOIN tblDMTrinhDoChuyenMon on tblDMGiangVien.TrinhDoChuyenMonID = tblDMTrinhDoChuyenMon.TrinhDoChuyenMonID                
	                                WHERE tblCNKTLopHocChuyenDe.LopHocID = " + lopHocID + @" ORDER BY tblCNKTLopHocChuyenDe.ChuyenDeID ASC";
        List<Hashtable> listData = _db.GetListData(command);
        if (listData.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in listData)
            {
                string ptt_HoiVienCaNhanCT = "0", ptt_HoiVienCaNhanLK = "0", ptt_HoiVienCaNhanDD = "0", ptt_KTV = "0", ptt_TroLyKTV = "0", ptt_KoPhaiTroLyKTV = "0";
                string tuNgay = Library.FormatDateTime(Library.DateTimeConvert(ht["TuNgay"]), "dd/MM/yyyy");
                string denNgay = Library.FormatDateTime(Library.DateTimeConvert(ht["DenNgay"]), "dd/MM/yyyy");
                double soBuoi = Library.DoubleConvert(ht["SoBuoi"].ToString());
                List<Hashtable> listData_PhiThanhToan = GetPhiThanhToan(lopHocID, ht["SoBuoi"].ToString());
                if (listData_PhiThanhToan.Count > 0)
                {
                    ptt_HoiVienCaNhanCT = !string.IsNullOrEmpty(listData_PhiThanhToan[0]["SoTienPhiHVCNCT"].ToString()) ? listData_PhiThanhToan[0]["SoTienPhiHVCNCT"].ToString() : "0";
                    ptt_HoiVienCaNhanLK = !string.IsNullOrEmpty(listData_PhiThanhToan[0]["SoTienPhiHVCNLK"].ToString()) ? listData_PhiThanhToan[0]["SoTienPhiHVCNLK"].ToString() : "0";
                    ptt_HoiVienCaNhanDD = !string.IsNullOrEmpty(listData_PhiThanhToan[0]["SoTienPhiHVCNDD"].ToString()) ? listData_PhiThanhToan[0]["SoTienPhiHVCNDD"].ToString() : "0";
                    ptt_KTV = !string.IsNullOrEmpty(listData_PhiThanhToan[0]["SoTienPhiKTV"].ToString()) ? listData_PhiThanhToan[0]["SoTienPhiKTV"].ToString() : "0";
                    ptt_TroLyKTV = !string.IsNullOrEmpty(listData_PhiThanhToan[0]["SoTienPhiNQT_TroLyKTV"].ToString()) ? listData_PhiThanhToan[0]["SoTienPhiNQT_TroLyKTV"].ToString() : "0";
                    ptt_KoPhaiTroLyKTV = !string.IsNullOrEmpty(listData_PhiThanhToan[0]["SoTienPhiNTQ"].ToString()) ? listData_PhiThanhToan[0]["SoTienPhiNTQ"].ToString() : "0";
                }

                js += @"_arrDataChuyenDe[" + index + "] = ['" + ht["ChuyenDeID"] + "','" + ht["MaChuyenDe"].ToString().Trim() + "','" + ht["TenChuyenDe"] + "','" + ht["LoaiChuyenDe"] + "', '" + GetTenLoaiChuyenDe(ht["LoaiChuyenDe"].ToString()) + "'," +
                      "'" + tuNgay + "','" + denNgay + "','" + Library.ChangeFormatNumber(soBuoi, "vn") + "','" + ht["GiangVienID"] + "','" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenGiangVien"]) + "'," +
                      " '" + ht["TrinhDoChuyenMon"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["DonViCongTac"]) + "','" + ht["TenChucVu"] + "', '" + ht["ChungChi"] + "'," +
                      " '" + ptt_HoiVienCaNhanCT + "', '" + ptt_HoiVienCaNhanLK + "', '" + ptt_HoiVienCaNhanDD + "', '" + ptt_KTV + "', '" + ptt_TroLyKTV + "', '" + ptt_KoPhaiTroLyKTV + "', '" + ht["SoNguoiHocToiDa"] + "', '" + ht["SoNguoiDaDangKy"] + "'];" + Environment.NewLine;
                index++;
            }
        }

        // Lay du lieu bang hoc phi cua lop -> tra ve cho trang danh ky de xu ly tinh toan bang js
        string query = "SELECT SoNgay, SoTienPhiHVCNCT, SoTienPhiHVCNLK, SoTienPhiHVCNDD, SoTienPhiKTV, SoTienPhiNQT_TroLyKTV, SoTienPhiNTQ FROM tblCNKTLopHocPhi WHERE LopHocID = " + lopHocID;
        List<Hashtable> listDataHocPhi = _db.GetListData(query);
        if (listDataHocPhi.Count > 0)
        {
            foreach (Hashtable ht in listDataHocPhi)
            {
                js += "arrDataHocPhi.push(['" + (Library.DoubleConvert(ht["SoNgay"]) * 2) + "','" + ht["SoTienPhiHVCNCT"] + "', '" + ht["SoTienPhiHVCNLK"] + "', '" + ht["SoTienPhiHVCNDD"] + "', '" + ht["SoTienPhiKTV"] + "', '" + ht["SoTienPhiNTQ"] + "', '" + ht["SoTienPhiNQT_TroLyKTV"] + "']);" + Environment.NewLine;
            }
        }

        js += "parent.DrawData_ChuyenDe(_arrDataChuyenDe, arrDataHocPhi);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Lấy số tiền phí của chuyên đề
    /// </summary>
    /// <param name="lopHocID">ID Lop hoc</param>
    /// <param name="soBuoi">Số buổi của chuyên đề</param>
    /// <returns></returns>
    private List<Hashtable> GetPhiThanhToan(string lopHocID, string soBuoi)
    {
        string query = "SELECT SoTienPhiHVCNCT, SoTienPhiHVCNLK, SoTienPhiHVCNDD, SoTienPhiKTV, SoTienPhiNQT_TroLyKTV, SoTienPhiNTQ FROM tblCNKTLopHocPhi WHERE LopHocID = " + lopHocID + " AND (SoNgay * 2) = " + soBuoi;
        List<Hashtable> listData = _db.GetListData(query);
        return listData;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Lấy tên loại chuyên đề theo mã lấy ra từ DB
    /// </summary>
    /// <param name="loaiChuyenDe">mã loại chuyên đề</param>
    /// <returns>Tên loại chuyên đề</returns>
    private string GetTenLoaiChuyenDe(string loaiChuyenDe)
    {
        string tenLoaiChuyenDe = "";
        switch (loaiChuyenDe)
        {
            case ListName.Type_LoaiChuyenDe_KeToanKiemToan:
                tenLoaiChuyenDe = "Kế toán, kiểm toán";
                break;
            case ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep:
                tenLoaiChuyenDe = "Đạo đức nghề nghiệp";
                break;
            case ListName.Type_LoaiChuyenDe_ChuyenDeKhac:
                tenLoaiChuyenDe = "Chuyên đề khác";
                break;
        }
        return tenLoaiChuyenDe;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/27
    /// Lấy thông tin học viên dựa theo mã - trả về dạng javascript cho trang cha
    /// </summary>
    /// <param name="maHocVien">Mã học viên</param>
    /// /// <param name="congTyId">ID của công ty hoặc hội viên tổ chức</param>
    private void GetInforHocVien(string maHocVien, string congTyId)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#;#;#;#;#;#;#;#;#;#;#;#;#';" + Environment.NewLine;
        if (!string.IsNullOrEmpty(maHocVien))
        {
            string query = @"SELECT HoiVienCaNhanID, tblHoiVienCaNhan.HoiVienTapTheID AS HVTCID, MaHoiVienCaNhan, LoaiHoiVienCaNhan, LoaiHoiVienCaNhanChiTiet, HoDem + ' ' + Ten as FullName, SoChungChiKTV, NgayCapChungChiKTV, 
                                GioiTinh, tblHoiVienCaNhan.Mobile AS Mobile, tblHoiVienCaNhan.Email AS Email, tblHoiVienTapThe.TenDoanhNghiep AS DonViCongTac,
                                tblHoiVienCaNhan.TroLyKTV AS TroLyKTV, NgaySinh, ChucVuID
	                            FROM tblHoiVienCaNhan LEFT JOIN tblHoiVienTapThe ON tblHoiVienCaNhan.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
	                            WHERE MaHoiVienCaNhan = N'" + maHocVien + @"'";
            if (!string.IsNullOrEmpty(congTyId))
                query += " AND tblHoiVienTapThe.HoiVienTapTheID = " + congTyId;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "value = '" + Library.CheckKeyInHashtable(ht, "HoiVienCaNhanID") + ";#" + Library.CheckKeyInHashtable(ht, "MaHoiVienCaNhan") + ";#" + Library.CheckKeyInHashtable(ht, "LoaiHoiVienCaNhan") + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(Library.CheckKeyInHashtable(ht, "FullName")) + ";#" + Library.CheckKeyInHashtable(ht, "SoChungChiKTV")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "NgayCapChungChiKTV").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "NgayCapChungChiKTV")).ToString("dd/MM/yyyy") : "") + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(Library.CheckKeyInHashtable(ht, "DonViCongTac")) + ";#" + Library.CheckKeyInHashtable(ht, "GioiTinh") + ";#" + Library.CheckKeyInHashtable(ht, "Email") + ";#" + Library.CheckKeyInHashtable(ht, "Mobile")
                             + ";#" + Library.CheckKeyInHashtable(ht, "TroLyKTV")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "NgaySinh").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "NgaySinh")).ToString("dd/MM/yyyy") : "") + ";#" + Library.CheckKeyInHashtable(ht, "ChucVuID") + ";#" + Library.CheckKeyInHashtable(ht, "LoaiHoiVienCaNhanChiTiet") + "';" + Environment.NewLine;
            }
            else
                js += "parent.alert('Không tìm thấy thông tin với mã vừa nhập!');" + Environment.NewLine;
        }
        js += "parent.DisplayInforHocVien(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/04/10
    /// Lấy thông tin học viên theo ID học viên - Trả về dạng javascript cho trang cha
    /// </summary>
    /// <param name="idHocVien">id Học viên</param>
    private void GetInforHocVienById(string idHocVien)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#;#;#;#;#;#;#;#;#;#;#;#;#';" + Environment.NewLine;
        if (!string.IsNullOrEmpty(idHocVien))
        {
            string query = @"SELECT HoiVienCaNhanID, tblHoiVienCaNhan.HoiVienTapTheID AS HVTCID, MaHoiVienCaNhan, LoaiHoiVienCaNhan, LoaiHoiVienCaNhanChiTiet, HoDem + ' ' + Ten as FullName, SoChungChiKTV, NgayCapChungChiKTV, 
                                GioiTinh, tblHoiVienCaNhan.Mobile AS Mobile, tblHoiVienCaNhan.Email AS Email, tblHoiVienTapThe.TenDoanhNghiep AS DonViCongTac,
                                tblHoiVienCaNhan.TroLyKTV AS TroLyKTV, NgaySinh, ChucVuID
	                            FROM tblHoiVienCaNhan LEFT JOIN tblHoiVienTapThe ON tblHoiVienCaNhan.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
	                            WHERE HoiVienCaNhanID = " + idHocVien + @"";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "value = '" + Library.CheckKeyInHashtable(ht, "HoiVienCaNhanID") + ";#" + Library.CheckKeyInHashtable(ht, "MaHoiVienCaNhan") + ";#" + Library.CheckKeyInHashtable(ht, "LoaiHoiVienCaNhan") + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(Library.CheckKeyInHashtable(ht, "FullName")) + ";#" + Library.CheckKeyInHashtable(ht, "SoChungChiKTV")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "NgayCapChungChiKTV").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "NgayCapChungChiKTV")).ToString("dd/MM/yyyy") : "") + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(Library.CheckKeyInHashtable(ht, "DonViCongTac")) + ";#" + Library.CheckKeyInHashtable(ht, "GioiTinh") + ";#" + Library.CheckKeyInHashtable(ht, "Email") + ";#" + Library.CheckKeyInHashtable(ht, "Mobile")
                             + ";#" + Library.CheckKeyInHashtable(ht, "TroLyKTV")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "NgaySinh").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "NgaySinh")).ToString("dd/MM/yyyy") : "") + ";#" + Library.CheckKeyInHashtable(ht, "ChucVuID") + ";#" + Library.CheckKeyInHashtable(ht, "LoaiHoiVienCaNhanChiTiet") + "';" + Environment.NewLine;
            }
            else
                js += "parent.alert('Không tìm thấy thông tin với mã vừa nhập!');" + Environment.NewLine;
        }
        js += "parent.DisplayInforHocVien(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/04/10
    /// Lấy danh sách học viên theo công ty - Trả về dạng javascript cho trang cha
    /// </summary>
    /// <param name="idCongTy">ID Công ty</param>
    private void GetListHocVienByCongTy(string idCongTy)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        if (!string.IsNullOrEmpty(idCongTy))
        {
            string query = @"SELECT HoiVienCaNhanID, MaHoiVienCaNhan, LoaiHoiVienCaNhan, HoDem + ' ' + Ten as FullName, SoChungChiKTV, NgayCapChungChiKTV,
                                tblHoiVienCaNhan.TroLyKTV AS TroLyKTV
                                FROM tblHoiVienCaNhan INNER JOIN tblHoiVienTapThe ON tblHoiVienCaNhan.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
	                            WHERE tblHoiVienTapThe.HoiVienTapTheID = " + idCongTy + "ORDER BY Ten";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    js += "arrData.push(['" + Library.CheckKeyInHashtable(ht, "HoiVienCaNhanID") + "','" + Library.CheckKeyInHashtable(ht, "MaHoiVienCaNhan") + "','" + Library.RemoveSpecCharaterWhenSendByJavascript(Library.CheckKeyInHashtable(ht, "FullName")) + "']);" + Environment.NewLine;
                }
            }
        }
        js += "parent.DisplayListHocVien(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/27
    /// Lấy thông tin lớp học dựa theo mã - trả về dạng javascript cho trang cha
    /// </summary>
    /// <param name="maHocVien">Mã lớp học</param>
    private void GetInforLopHoc(string maLopHoc)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#;#;#;#;#;#;#;#;#;#';" + Environment.NewLine;
        if (!string.IsNullOrEmpty(maLopHoc))
        {
            string query = @"SELECT LopHocID, MaLopHoc, TenLopHoc , TuNgay, DenNgay, HanDangKy, tblDMTinh.TenTinh AS TenTinh, DiaChiLopHoc
                                        FROM tblCNKTLopHoc LEFT JOIN tblDMTinh ON tblCNKTLopHoc.TinhThanhID = tblDMTinh.TinhID
                                        LEFT JOIN tblDMTrangThai ON tblCNKTLopHoc.TinhTrangID = tblDMTrangThai.TrangThaiID
                                         WHERE MaLopHoc = N'" + maLopHoc + @"' AND tblDMTrangThai.MaTrangThai = '" + ListName.Status_DaPheDuyet + @"' ORDER BY NgayTao DESC";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "value = '" + Library.CheckKeyInHashtable(ht, "LopHocID") + ";#" + Library.CheckKeyInHashtable(ht, "MaLopHoc") + ";#" + Library.CheckKeyInHashtable(ht, "TenLopHoc")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "TuNgay").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "TuNgay")).ToString("dd/MM/yyyy") : "")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "DenNgay").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "DenNgay")).ToString("dd/MM/yyyy") : "")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "HanDangKy").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "HanDangKy")).ToString("dd/MM/yyyy") : "")
                             + ";#" + Library.CheckKeyInHashtable(ht, "TenTinh") + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(Library.CheckKeyInHashtable(ht, "DiaChiLopHoc")) + "';" + Environment.NewLine;
            }
            else
                js += "parent.alert('Không tìm thấy thông tin với mã vừa nhập!');" + Environment.NewLine;
        }
        else // Không truyền vào maLopHoc  thì mặc định lấy ra thông tin lớp học mới nhất
        {
            string query = @"SELECT TOP 1 LopHocID, MaLopHoc, TenLopHoc , TuNgay, DenNgay, HanDangKy, tblDMTinh.TenTinh AS TenTinh, DiaChiLopHoc                             
                                        FROM tblCNKTLopHoc LEFT JOIN tblDMTinh ON tblCNKTLopHoc.TinhThanhID = tblDMTinh.TinhID
                                        LEFT JOIN tblDMTrangThai ON tblCNKTLopHoc.TinhTrangID = tblDMTrangThai.TrangThaiID
                                        WHERE tblDMTrangThai.MaTrangThai = '" + ListName.Status_DaPheDuyet + @"'
                                         ORDER BY LopHocID DESC";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "value = '" + Library.CheckKeyInHashtable(ht, "LopHocID") + ";#" + Library.CheckKeyInHashtable(ht, "MaLopHoc") + ";#" + Library.CheckKeyInHashtable(ht, "TenLopHoc")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "TuNgay").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "TuNgay")).ToString("dd/MM/yyyy") : "")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "DenNgay").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "DenNgay")).ToString("dd/MM/yyyy") : "")
                             + ";#" + (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(ht, "HanDangKy").ToString()) ? Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "HanDangKy")).ToString("dd/MM/yyyy") : "")
                             + ";#" + Library.CheckKeyInHashtable(ht, "TenTinh") + ";#" + Library.CheckKeyInHashtable(ht, "DiaChiLopHoc") + "';" + Environment.NewLine;
            }
        }
        js += "parent.DisplayInforLopHoc(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/27
    /// Lấy danh sách các chuyên đề đã đăng ký học
    /// </summary>
    /// <param name="idlh">ID lớp học</param>
    /// <param name="idhv">ID học viên</param>
    private void GetLopHoc_HocVien(string idlh, string idhv)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        string coNhanGCN = "true";
        string ngayDangKy = DateTime.Now.ToString("dd/MM/yyyy");
        string soGioCNKT = "";
        string tongPhi = "";
        string dangKyHocCaNhanID = "";
        if (!string.IsNullOrEmpty(idlh) && !string.IsNullOrEmpty(idhv))
        {
            string query = @"SELECT DangKyHocCaNhanID, LayGiayChungNhan, NgayDangKy, TongSoGioCNKT, TongPhi FROM tblCNKTDangKyHocCaNhan WHERE HoiVienCaNhanID = " + idhv + @" AND LopHocID = " + idlh + @" AND (HoiVienTapTheDangKy is null  OR HoiVienTapTheDangKy != 0)";
            List<Hashtable> listData_DangKyHocCaNhan = _db.GetListData(query);
            if (listData_DangKyHocCaNhan.Count > 0)
            {
                Hashtable ht_DangKyHocCaNhan = listData_DangKyHocCaNhan[0];
                dangKyHocCaNhanID = ht_DangKyHocCaNhan["DangKyHocCaNhanID"].ToString();
                if (Library.CheckKeyInHashtable(ht_DangKyHocCaNhan, "LayGiayChungNhan").ToString() != "1" && coNhanGCN == "true")
                    coNhanGCN = "false";
                ngayDangKy = !string.IsNullOrEmpty(ht_DangKyHocCaNhan["NgayDangKy"].ToString())
                                 ? Library.DateTimeConvert(ht_DangKyHocCaNhan["NgayDangKy"]).ToString("dd/MM/yyyy")
                                 : "";
                soGioCNKT = ht_DangKyHocCaNhan["TongSoGioCNKT"].ToString();
                tongPhi = ht_DangKyHocCaNhan["TongPhi"].ToString();

                query = @"SELECT ChuyenDeID FROM tblCNKTLopHocHocVien WHERE DangKyHocCaNhanID = " + dangKyHocCaNhanID;
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    js += "arrData = [";
                    foreach (Hashtable ht in listData)
                    {
                        js += "'" + Library.CheckKeyInHashtable(ht, "ChuyenDeID") + "',";
                    }
                    js = js.TrimEnd(',');
                    js += "];" + Environment.NewLine;
                }
            }
        }
        js += "parent.TickCheckBox(arrData,'" + dangKyHocCaNhanID + "', " + coNhanGCN + ", '" + ngayDangKy + "', '" + soGioCNKT + "', '" + tongPhi + "');" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/02
    /// </summary>
    /// <param name="idlh">ID lớp học</param>
    /// <param name="idct">ID công ty</param>
    protected void GetLopHoc_CongTy(string idlh, string idct)
    {
        if (string.IsNullOrEmpty(idlh) || string.IsNullOrEmpty(idct))
            return;
        // Những đoạn truy vấn dài sẽ đưa vào store khi giải quyết đc vấn đề nettier
        string query = @"IF OBJECT_ID('tempdb..#Temp') IS NOT NULL DROP TABLE #Temp
                                go
                                SELECT tblCNKTDangKyHocCaNhan.DangKyHocCaNhanID as dkhcnid, tblHoiVienCaNhan.MaHoiVienCaNhan AS mhvcn, (tblHoiVienCaNhan.HoDem + ' ' + tblHoiVienCaNhan.Ten) AS FullName,
                                tblHoiVienCaNhan.LoaiHoiVienCaNhan AS LoaiHoiVienCaNhan, tblHoiVienCaNhan.SoChungChiKTV AS SoChungChiKTV, tblHoiVienCaNhan.NgayCapChungChiKTV AS ngayCapChungChiKTV,
                                tblCNKTDangKyHocCaNhan.LayGiayChungNhan AS layGCN, tblCNKTLopHocChuyenDe.ChuyenDeID, tblCNKTLopHocChuyenDe.TenChuyenDe AS TenChuyenDe, tblCNKTDangKyHocCaNhan.TongSoGioCNKT AS soGioCNKT, tblCNKTDangKyHocCaNhan.TongPhi AS TongPhi
                                INTO #Temp
                                FROM tblCNKTDangKyHocCaNhan 
                                LEFT JOIN tblCNKTLopHocHocVien ON tblCNKTDangKyHocCaNhan.DangKyHocCaNhanID = tblCNKTLopHocHocVien.DangKyHocCaNhanID
                                LEFT JOIN tblHoiVienCaNhan ON tblCNKTDangKyHocCaNhan.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID
                                LEFT JOIN tblCNKTLopHocChuyenDe ON tblCNKTLopHocHocVien.ChuyenDeID = tblCNKTLopHocChuyenDe.ChuyenDeID
                                WHERE tblHoiVienCaNhan.HoiVienTapTheID = " + idct + @" AND tblCNKTLopHocHocVien.LopHocID = " + idlh + @"
                                ORDER BY tblCNKTLopHocChuyenDe.ChuyenDeID ASC
                                SELECT dkhcnid, mhvcn, FullName,
                                LoaiHoiVienCaNhan, SoChungChiKTV, NgayCapChungChiKTV,
                                layGCN, soGioCNKT, TongPhi,
	                                STUFF(
                                         (SELECT DISTINCT ';#' + b.TenChuyenDe
                                          FROM #Temp as b
                                          WHERE b.mhvcn = a.mhvcn
                                          FOR XML PATH (''))
                                          , 1, 2, '')  AS ChuyenDe
                                 FROM
                                  #Temp as a	
                                  GROUP BY dkhcnid, mhvcn, FullName,
                                LoaiHoiVienCaNhan, SoChungChiKTV, NgayCapChungChiKTV,
                                layGCN, soGioCNKT, TongPhi
                                ORDER BY FullName, ChuyenDeID
                                go
                                drop table #Temp
                            ";

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var _arrDataDangKyHoc = []" + Environment.NewLine;
        double tongPhi = 0;
        List<Hashtable> listData = _db.GetListData("GetDangKyTheoCongTy", new List<string> { "HoiVienTapTheID", "LopHocID" }, new List<object> { idct, idlh });
        if (listData.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in listData)
            {
                string ngayCapChungChiKTV = Library.FormatDateTime(Library.DateTimeConvert(ht["NgayCapChungChiKTV"]), "dd/MM/yyyy");
                js += @"_arrDataDangKyHoc[" + index + "] = ['" + ht["dkhcnid"] + "', '" + ht["mhvcn"] + "','" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["FullName"]) + "','" + ht["LoaiHoiVienCaNhan"] + "', '" + Library.GetTenLoaiHoiVienCaNhan(ht["LoaiHoiVienCaNhan"].ToString()) + "'," +
                      "'" + ht["SoChungChiKTV"] + "','" + ngayCapChungChiKTV + "','" + ht["layGCN"] + "','" + ht["soGioCNKT"] + "','" + ht["TongPhi"] + "'," +
                      " '" + ht["ChuyenDe"].ToString().Replace(";#", "<br />") + "'];" + Environment.NewLine;
                index++;
                tongPhi += Library.DoubleConvert(ht["TongPhi"]);
            }
        }

        js += "parent.DrawData_DangKyHoc(_arrDataDangKyHoc, '" + tongPhi + "');" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Lấy thông tin công ty kiểm toán/hội viên tổ chức dựa theo mã - Trả về dạng javascript cho trang cha
    /// </summary>
    /// <param name="maCongTy">Mã công ty/Mã hội viên tổ chức</param>
    private void GetInforCongTyKiemToan_HVTT(string maCongTy)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#;#;#;#;#;#;#;#;#;#';" + Environment.NewLine;
        if (!string.IsNullOrEmpty(maCongTy))
        {
            string query = @"SELECT HoiVienTapTheID, MaHoiVienTapThe, TenDoanhNghiep, TenVietTat, TenTiengAnh, DiaChi, MaSoThue, 
                                SoGiayChungNhanDKKD, NgayGiaNhap, NgayCapGiayChungNhanDKKD, NguoiDaiDienLL_Ten, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email
	                            FROM tblHoiVienTapThe
	                            WHERE MaHoiVienTapThe = N'" + maCongTy + @"' ORDER BY TenDoanhNghiep";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "value = '" + ht["HoiVienTapTheID"] + ";#" + ht["MaHoiVienTapThe"] + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"])
                         + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenVietTat"]) + ";#" + ht["MaSoThue"] + ";#" + ht["SoGiayChungNhanDKKD"]
                         + ";#" + (!string.IsNullOrEmpty(ht["NgayGiaNhap"].ToString()) ? Library.DateTimeConvert(ht["NgayGiaNhap"]).ToString("dd/MM/yyyy") : "")
                         + ";#" + (!string.IsNullOrEmpty(ht["NgayCapGiayChungNhanDKKD"].ToString()) ? Library.DateTimeConvert(ht["NgayCapGiayChungNhanDKKD"]).ToString("dd/MM/yyyy") : "")
                         + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienLL_Ten"]) + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienLL_DiDong"]) + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienLL_Email"]) + "';" + Environment.NewLine;
            }
            else
                js += "parent.alert('Không tìm thấy thông tin với mã vừa nhập!');" + Environment.NewLine;
        }
        js += "parent.DisplayInforCongTy(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Save thông tin đăng ký học theo từng học viên
    /// </summary>
    /// <param name="idLopHoc"></param>
    /// <param name="idHocVien"></param>
    private void SaveDangKyCaNhan(string idLopHoc, string idHocVien, string listChuyenDeID, string ngayDangKy, string layGiayChungNhan, string hoiVienTapTheID, string dangKyHocCaNhanID, string soGioCNKT, string tongPhi, string isClose)
    {
        Commons cm = new Commons();
        if (!string.IsNullOrEmpty(idHocVien))
        {
            SqlCnktDangKyHocCaNhanProvider dangKyHocCaNhanPro = new SqlCnktDangKyHocCaNhanProvider(ListName.ConnectionString, false, string.Empty);
            SqlTtPhatSinhPhiProvider proPhatSinhPhi = new SqlTtPhatSinhPhiProvider(ListName.ConnectionString, false, string.Empty);
            SqlHoiVienCaNhanProvider proHVCN = new SqlHoiVienCaNhanProvider(ListName.ConnectionString, false, string.Empty);
            CnktDangKyHocCaNhan objDangKyHocCaNhan = new CnktDangKyHocCaNhan(); // Insert
            if (!string.IsNullOrEmpty(dangKyHocCaNhanID) && Library.CheckIsInt32(dangKyHocCaNhanID))// Update
                objDangKyHocCaNhan = dangKyHocCaNhanPro.GetByDangKyHocCaNhanId(Library.Int32Convert(dangKyHocCaNhanID));
            objDangKyHocCaNhan.LopHocId = Library.Int32Convert(idLopHoc);
            if (!string.IsNullOrEmpty(ngayDangKy))
                objDangKyHocCaNhan.NgayDangKy = Library.DateTimeConvert(ngayDangKy, '/', 1);
            objDangKyHocCaNhan.LayGiayChungNhan = layGiayChungNhan;
            objDangKyHocCaNhan.HoiVienCaNhanId = Library.Int32Convert(idHocVien);
            objDangKyHocCaNhan.TongSoGioCnkt = Library.DecimalConvert(soGioCNKT);
            objDangKyHocCaNhan.TongPhi = Library.DecimalConvert(GetTongHocPhi(idLopHoc, idHocVien, (Library.DoubleConvert(soGioCNKT) / 4).ToString()));
            if (!string.IsNullOrEmpty(hoiVienTapTheID))
            {
                objDangKyHocCaNhan.HoiVienTapTheId = Library.Int32Convert(hoiVienTapTheID);
                objDangKyHocCaNhan.HoiVienTapTheDangKy = Library.Int32Convert(hoiVienTapTheID);
            }
            if (!string.IsNullOrEmpty(dangKyHocCaNhanID) && Library.CheckIsInt32(dangKyHocCaNhanID))
            {
                if (dangKyHocCaNhanPro.Update(objDangKyHocCaNhan))
                {
                    // Lấy ID bản ghi phát sinh phí từ trường hợp đăng ký học này
                    string query = "SELECT PhatSinhPhiID FROM " + ListName.Table_TTPhatSinhPhi + " WHERE DoiTuongPhatSinhPhiID = " + objDangKyHocCaNhan.DangKyHocCaNhanId + " AND LoaiPhi = '4'";
                    List<Hashtable> listData = _db.GetListData(query);
                    if (listData.Count > 0)
                    {
                        TtPhatSinhPhi objPsp = proPhatSinhPhi.GetByPhatSinhPhiId(Library.Int32Convert(listData[0]["PhatSinhPhiID"]));
                        if (objPsp != null && objPsp.PhatSinhPhiId > 0)
                        {
                            objPsp.HoiVienId = Library.Int32Convert(idHocVien);
                            // Lấy loại hội viên cá nhân
                            HoiVienCaNhan objHVCN = proHVCN.GetByHoiVienCaNhanId(Library.Int32Convert(idHocVien));
                            if (objHVCN != null && objHVCN.HoiVienCaNhanId > 0)
                                objPsp.LoaiHoiVien = objHVCN.LoaiHoiVienCaNhan;
                            objPsp.DoiTuongPhatSinhPhiId = objDangKyHocCaNhan.DangKyHocCaNhanId;
                            objPsp.LoaiPhi = "4";
                            objPsp.NgayPhatSinh = DateTime.Now;
                            objPsp.SoTien = objDangKyHocCaNhan.TongPhi;
                            proPhatSinhPhi.Update(objPsp);
                        }
                    }
                }
            }
            else
            {
                if (dangKyHocCaNhanPro.Insert(objDangKyHocCaNhan))
                {
                    TtPhatSinhPhi objPsp = new TtPhatSinhPhi();
                    objPsp.HoiVienId = Library.Int32Convert(idHocVien);
                    // Lấy loại hội viên cá nhân
                    HoiVienCaNhan objHVCN = proHVCN.GetByHoiVienCaNhanId(Library.Int32Convert(idHocVien));
                    if (objHVCN != null && objHVCN.HoiVienCaNhanId > 0)
                        objPsp.LoaiHoiVien = objHVCN.LoaiHoiVienCaNhan;
                    objPsp.DoiTuongPhatSinhPhiId = objDangKyHocCaNhan.DangKyHocCaNhanId;
                    objPsp.LoaiPhi = "4";
                    objPsp.NgayPhatSinh = DateTime.Now;
                    objPsp.SoTien = objDangKyHocCaNhan.TongPhi;
                    proPhatSinhPhi.Insert(objPsp);
                }
            }

            SqlCnktLopHocHocVienProvider sqlCnktLopHocHocVienPro = new SqlCnktLopHocHocVienProvider(ListName.ConnectionString, false, string.Empty);

            // Xóa hết dữ liệu đăng ký chuyên đề của học viên trong lớp này
            Delete(objDangKyHocCaNhan.DangKyHocCaNhanId.ToString(), false);

            // Lấy danh sách chuyên đề đã chọn -> mỗi chuyên đề sẽ lưu thành 1 bản ghi trong bảng "tblCNKTLopHocHocVien"
            string status_DaDuyet = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
            // Giá trị ListChuyenDeID = ID1,ID2,ID3,...
            string[] arrChuyenDeID = listChuyenDeID.Split(',');
            foreach (string chuyenDeID in arrChuyenDeID)
            {
                if (!string.IsNullOrEmpty(chuyenDeID))
                {
                    CnktLopHocHocVien cnktLopHocHocVien = new CnktLopHocHocVien();
                    cnktLopHocHocVien.DangKyHocCaNhanId = objDangKyHocCaNhan.DangKyHocCaNhanId;
                    cnktLopHocHocVien.HoiVienCaNhanId = Library.Int32Convert(idHocVien);
                    if (!string.IsNullOrEmpty(hoiVienTapTheID))
                        cnktLopHocHocVien.HoiVienTapTheId = Library.Int32Convert(hoiVienTapTheID);
                    cnktLopHocHocVien.LopHocId = Library.Int32Convert(idLopHoc);
                    cnktLopHocHocVien.ChuyenDeId = Library.Int32Convert(chuyenDeID);
                    cnktLopHocHocVien.TinhTrangId = Library.Int32Convert(status_DaDuyet);
                    sqlCnktLopHocHocVienPro.Insert(cnktLopHocHocVien);
                }
            }
            string tenHocVien = "";
            string maHocVien = "";
            string tenLopHoc = "";
            string maLopHoc = "";
            cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Cập nhật thông tin đăng ký học của \"" + tenHocVien + "(" + maHocVien + ")\" với lớp \"" + tenLopHoc + "\"(" + maLopHoc + ")");
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            js += "parent.DisplayResultAfterSave('" + isClose + "')" + Environment.NewLine;
            js += "</script>";
            Response.Write(js);
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Xóa thông tin đăng ký học
    /// </summary>
    /// <param name="dangKyHocCaNhanID">ID bản đăng ký học cá nhân</param>
    /// <param name="deleteAll">Xóa cả bản đăng ký học cá nhân hoặc chỉ xóa những chuyên đề đã chọn</param>
    private int Delete(string dangKyHocCaNhanID, bool deleteAll)
    {
        if (!string.IsNullOrEmpty(dangKyHocCaNhanID))
        {
            Db db = new Db(ListName.ConnectionString);
            try
            {
                db.OpenConnection();
                string[] arrDangKyHocCaNhanID = dangKyHocCaNhanID.Split(',');
                string where = "";
                string where_psp = "";
                foreach (string str in arrDangKyHocCaNhanID)
                {
                    if (string.IsNullOrEmpty(str))
                        continue;
                    if (!string.IsNullOrEmpty(where))
                    {
                        where += " OR ";
                        where_psp += " OR ";
                    }
                    where += " DangKyHocCaNhanID = " + str;
                    where_psp += " DoiTuongPhatSinhPhiID = " + str;
                }
                string query = "";
                if (deleteAll)
                {
                    query = "DELETE FROM tblCNKTDangKyHocCaNhan WHERE " + where + ";";
                    query += "DELETE FROM tblTTPhatSinhPhi WHERE " + where_psp;
                    db.ExecuteNonQuery(query);
                }

                query = "DELETE FROM tblCNKTLopHocHocVien WHERE " + where;
                db.ExecuteNonQuery(query);
            }
            catch { }
            finally
            {
                db.CloseConnection();
            }
        }
        return 0;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/04/13
    /// Kiem tra cac chuyen de da bi vuot gioi han nguoi dang ky -> tra ve mang cac chuyen de bi vuot
    /// </summary>
    /// <param name="lopHocID">id lop hoc dang ky</param>
    private void CheckSoNguoiHocChuyenDe(string lopHocID)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var _arrChuyenDeVuotQuaSoNguoiHoc = [];" + Environment.NewLine;
        string command = @"SELECT ChuyenDeID, MaChuyenDe, TenChuyenDe, SoNguoiHocToiDa,
                                    (SELECT COUNT(LopHocHocVienID) FROM tblCNKTLopHocHocVien INNER JOIN tblDMTrangThai on tblCNKTLopHocHocVien.TinhTrangID = tblDMTrangThai.TrangThaiID INNER JOIN tblHoiVienCaNhan ON tblCNKTLopHocHocVien.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID AND tblHoiVienCaNhan.LoaiHoiVienCaNhan != '0' WHERE LopHocID = " + lopHocID + @" AND tblCNKTLopHocHocVien.ChuyenDeID = tblCNKTLopHocChuyenDe.ChuyenDeID AND tblDMTrangThai.MaTrangThai = '5') SoNguoiDaDangKy
	                            FROM tblCNKTLopHocChuyenDe WHERE LopHocID = " + lopHocID + @" ORDER BY ChuyenDeID ASC";
        List<Hashtable> listData = _db.GetListData(command);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                int soNguoiHocToiDa = Library.Int32Convert(ht["SoNguoiHocToiDa"]);
                int soNguoiDaDangKy = Library.Int32Convert(ht["SoNguoiDaDangKy"]);
                if(soNguoiDaDangKy >= soNguoiHocToiDa)
                {
                    js += "_arrChuyenDeVuotQuaSoNguoiHoc.push('" + ht["TenChuyenDe"] + "');" + Environment.NewLine;
                }
            }
        }

        js += "parent.SubmitEventSaveForm(_arrChuyenDeVuotQuaSoNguoiHoc);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/04/15
    /// Ham tinh tong so hoc phi phai nop theo so buoi dang ky
    /// </summary>
    /// <param name="idLopHoc">ID lop hoc dang ky</param>
    /// <param name="idHoiVienCaNhan">ID hoi vien ca nhan dang ky</param>
    /// <param name="tongSoBuoi">tong so buoi hoc</param>
    /// <returns></returns>
    private double GetTongHocPhi(string idLopHoc, string idHoiVienCaNhan, string tongSoBuoi)
    {
        double tongHocPhi = 0;
        string loaiHvcn = "", troLyKtv = "", loaiHvcnct = "";
        // Lay loai hoi vien ca nhan
        string query = "SELECT LoaiHoiVienCaNhan, LoaiHoiVienCaNhanChiTiet, TroLyKTV FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + idHoiVienCaNhan;
        List<Hashtable> listDataHvcn = _db.GetListData(query);
        if (listDataHvcn.Count > 0)
        {
            loaiHvcn = listDataHvcn[0]["LoaiHoiVienCaNhan"].ToString();
            loaiHvcnct = listDataHvcn[0]["LoaiHoiVienCaNhanChiTiet"].ToString();
            troLyKtv = listDataHvcn[0]["TroLyKTV"].ToString();
        }

        // Lay bang hoc phi
        query = "SELECT SoTienPhiHVCNCT, SoTienPhiHVCNLK, SoTienPhiHVCNDD, SoTienPhiKTV, SoTienPhiNQT_TroLyKTV, SoTienPhiNTQ FROM tblCNKTLopHocPhi WHERE LopHocID = " + idLopHoc + " AND (SoNgay * 2) = " + tongSoBuoi;
        List<Hashtable> listDataHocPhi = _db.GetListData(query);
        if (listDataHocPhi.Count > 0)
        {
            switch (loaiHvcn)
            {
                case "1":
                    if (loaiHvcnct == "0" || loaiHvcnct == "2" || loaiHvcnct == "4" || loaiHvcnct == "5" || loaiHvcnct == "6")
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNCT"]);
                    else if (loaiHvcnct == "1")
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNLK"]);
                    else
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNDD"]);
                    break;
                case "2":
                    tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiKTV"]);
                    break;
                case "0":
                    if (troLyKtv == "0")
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiNTQ"]);
                    else
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiNQT_TroLyKTV"]);
                    break;
            }
        }
        return tongHocPhi;
    }


}