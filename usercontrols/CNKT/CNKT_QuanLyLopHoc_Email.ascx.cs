﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using HiPT.VACPA.DL;
using System.Data.SqlClient;

public partial class usercontrols_CNKT_QuanLyLopHoc_Email : System.Web.UI.UserControl
{
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string idLopHoc = Request.Form["hdLopHocID"];
            if (string.IsNullOrEmpty(idLopHoc))
                idLopHoc = Library.CheckNull(Request.QueryString["idlh"]);
            if (!string.IsNullOrEmpty(idLopHoc))
            {
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "SendEmail")
                    SendEmail(idLopHoc);

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/CNKT/ThuMoiThamDuLopDT_CNKT.rpt"));
                rpt.Database.Tables["dtNoiDung_GiangVienLopHoc"].SetDataSource(GetNoiDung_GiangVienLopHoc(idLopHoc));
                GetThongTinLopHoc(idLopHoc, rpt);
                GetPhiThamGia(idLopHoc, rpt);
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "ThuMoiThamDuLopDTCNKT");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "ThuMoiThamDuLopDTCNKT");
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    private DataTable GetNoiDung_GiangVienLopHoc(string idLopHoc)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "Ngay", "Buoi", "TenChuyenDe", "LoaiChuyenDe", "TenGiangVien", "ChucVu", "DonViCongTac" });
        if (string.IsNullOrEmpty(idLopHoc))
            return dt;
        string query = @"SELECT LHB.Ngay, LHB.Sang, LHB.Chieu, LHCD.TenChuyenDe, LHCD.LoaiChuyenDe, DMGV.TenGiangVien, DMCV.TenChucVu, DMGV.DonViCongTac FROM tblCNKTLopHocBuoi LHB
                        LEFT JOIN tblCNKTLopHocChuyenDe LHCD ON LHB.ChuyenDeID = LHCD.ChuyenDeID
                        LEFT JOIN tblDMGiangVien DMGV ON LHCD.GiangVienID = DMGV.GiangVienID
                        LEFT JOIN tblDMChucVu DMCV ON DMGV.ChucVuID = DMCV.ChucVuID
                        WHERE LHB.LopHocID = " + idLopHoc + @"
                        ORDER BY Ngay, Chieu";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            DataRow dr;
            foreach (Hashtable ht in listData)
            {
                if (!string.IsNullOrEmpty(Library.CheckNull(ht["Sang"])))
                {
                    dr = dt.NewRow();
                    dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                    dr["Buoi"] = "Sáng";
                    dr["TenChuyenDe"] = ht["TenChuyenDe"] + "<br><i>(Chuyên đề " + Library.GetTenLoaiChuyenDe(ht["LoaiChuyenDe"].ToString()) + ")</i>";
                    dr["TenGiangVien"] = "<b>" + ht["TenGiangVien"] + "</b> - " + ht["TenChucVu"] + "<br>" + ht["DonViCongTac"];
                    dt.Rows.Add(dr);
                }
                if (!string.IsNullOrEmpty(Library.CheckNull(ht["Chieu"])))
                {
                    dr = dt.NewRow();
                    dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                    dr["Buoi"] = "Chiều";
                    dr["TenChuyenDe"] = ht["TenChuyenDe"] + "<br><i>(Chuyên đề " + Library.GetTenLoaiChuyenDe(ht["LoaiChuyenDe"].ToString()) + ")</i>";
                    dr["TenGiangVien"] = "<b>" + ht["TenGiangVien"] + "</b> - " + ht["TenChucVu"] + " - " + ht["DonViCongTac"];
                    dt.Rows.Add(dr);
                }
            }
        }
        return dt;
    }

    private void GetThongTinLopHoc(string idLopHoc, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idLopHoc))
            return;
        string query = "SELECT MaLopHoc, TenLopHoc, TuNgay, DenNgay, HanDangKy, TenTinh, DiaChiLopHoc FROM tblCNKTLopHoc LH LEFT JOIN tblDMTinh ON LH.TinhThanhID = tblDMTinh.TinhID WHERE LopHocID = " + idLopHoc;
        List<Hashtable> listData = _db.GetListData(query);
        {
            Hashtable ht = listData[0];
            DateTime fromDate = Library.DateTimeConvert(ht["TuNgay"]);
            DateTime toDate = Library.DateTimeConvert(ht["DenNgay"]);
            ((TextObject)rpt.ReportDefinition.ReportObjects["Text2"]).Text = "V/v: Mời tham dự lớp cập nhật kiến thức KTV số " + ht["MaLopHoc"];
            TextObject text3 = (TextObject)rpt.ReportDefinition.ReportObjects["Text3"];
            text3.Text = ht["TenTinh"] + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            ((TextObject)rpt.ReportDefinition.ReportObjects["Text6"]).Text =
                "     Thực hiện Kế hoạch đào tạo cập nhật kiến thức năm " + Library.DateTimeConvert(ht["TuNgay"]).Year +
                ", Quyết định số 1634/QĐ-BTC ngày 13/08/2015 của Bộ Tài chính về việc chấp thuận cho Hội kiểm toán viên hành nghề Việt Nam" +
                " (VACPA) được tổ chức cập nhật kiến thức cho kiểm toán viên đăng ký hành nghề, VACPA sẽ tổ chức lớp học số " + ht["MaLopHoc"] + " tại " + ht["TenTinh"] + ", cụ thể như sau:";
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtSoNgayHoc"]).Text = ((toDate - fromDate).TotalDays + 1).ToString() + " ngày, bắt đầu từ";
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNgayHoc"]).Text = "8h30, ngày " + fromDate.ToString("dd/MM/yyyy") + " đến " + toDate.ToString("dd/MM/yyyy");
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtDiaDiem"]).Text = ht["DiaChiLopHoc"].ToString();
            ReportDocument subRpt = rpt.Subreports[0];
            subRpt.DataDefinition.FormulaFields["UnBoundString1"].Text = "\"<b>6. Tính thời gian cập nhật kiến thức:</b> KTV phải tham gia đủ thời lượng của một chuyên đề học thì mới được tính số giờ CNKT của chuyên đề đó. VACPA sẽ cấp &ldquo;<b>Giấy chứng nhận giờ CNKT năm " + Library.DateTimeConvert(ht["TuNgay"]).Year.ToString() + "</b>&rdquo; cho từng học viên.\"";
            subRpt.DataDefinition.FormulaFields["UnBoundString3"].Text = "\"Đề nghị các học viên đăng ký sớm <span style='color: blue'>trước 17h00 giờ ngày " + Library.DateTimeConvert(ht["HanDangKy"]).ToString("dd/MM/yyyy") + ".</span>. Thời hạn đăng ký có thể kết thúc sớm nếu như lớp học đã đủ số lượng.\"";
            subRpt.DataDefinition.FormulaFields["UnBoundString4"].Text = "\"Đề nghị thông báo việc hủy tham dự trước ngày " + Library.DateTimeConvert(ht["HanDangKy"]).ToString("dd/MM/yyyy") + " bằng email, điện thoại hoặc nhắn tin. Nếu Quý học viên không thông báo việc hủy tham dự theo thời hạn trên, VACPA sẽ tính 30% phí tham dự/01 học viên để bù đắp một phần chi phí hành chính, tổ chức. Công ty vui lòng nộp 30% phí không thông báo hủy trước ngày 15/6/2018.\"";
            if (ht["TenTinh"].ToString().Trim() == "Hà Nội")
                subRpt.DataDefinition.FormulaFields["UnBoundString5"].Text = "\"Đăng ký tham dự và chi tiết xin liên hệ Chị Bùi Thị Bích Thủy hoặc chị Nguyễn Diệu Linh.                                  Văn phòng VACPA HN, ĐT: 024.39724334/ Máy lẻ: 103                                                                                 Email: trungtamdaotao@vacpa.org.vn. Xem thông tin trên web: www.vacpa.org.vn.\"";
            else
                subRpt.DataDefinition.FormulaFields["UnBoundString5"].Text = "\"Văn Phòng Đại Diện Hội Kiểm Toán Viên Hành Nghề Việt Nam tại Tp HCM                            Phòng 24, Lầu 1, 138 Nguyễn Thị Minh Khai, P.6, Q.3, TP.HCM                                                       ĐT: 028 3930 6435 - 028 39307236     Fax: 028 39306442     Email: hcmc@vacpa.org.vn.\"";

            string content = "<b>HỘI KIỂM TOÁN VIÊN HÀNH NGHỀ VIỆT NAM - VACPA xin thông báo:</b><br />";
            content += "Về việc tổ chức lớp học \"<b>" + ht["TenLopHoc"] + "</b>\". Từ ngày " +
                                                " " + fromDate.ToString("dd/MM/yyyy") + " đến " + toDate.ToString("dd/MM/yyyy");
            content += "<br />Kính mời các cá nhân và công ty nếu quan tâm xin hãy đăng ký trước ngày <b>" + Library.DateTimeConvert(ht["HanDangKy"]).ToString("dd/MM/yyyy") + "</b>";
            lblTitle.Text = content;
        }
    }

    private void GetPhiThamGia(string idLopHoc, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idLopHoc))
            return;
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "Ngay", "HVCNCT", "HVCNLK", "HVCNDD", "KTV", "TroLyKTV", "NQT" });
        string query = "SELECT SoNgay Ngay, SoTienPhiHVCNCT HVCNCT, SoTienPhiHVCNLK HVCNLK, SoTienPhiHVCNDD HVCNDD, SoTienPhiKTV KTV, SoTienPhiNQT_TroLyKTV TroLyKTV, SoTienPhiNTQ NQT FROM tblCNKTLopHocPhi WHERE LopHocID = " + idLopHoc + " ORDER BY Ngay";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                DataRow dr = dt.NewRow();
                dr["Ngay"] = Library.DoubleConvert(ht["Ngay"]);
                dr["HVCNCT"] = Library.FormatMoney(ht["HVCNCT"]);
                dr["HVCNLK"] = Library.FormatMoney(ht["HVCNLK"]);
                dr["HVCNDD"] = Library.FormatMoney(ht["HVCNDD"]);
                dr["KTV"] = Library.FormatMoney(ht["KTV"]);
                dr["TroLyKTV"] = Library.FormatMoney(ht["TroLyKTV"]);
                dr["NQT"] = Library.FormatMoney(ht["NQT"]);
                dt.Rows.Add(dr);
            }
        }
        ReportDocument subRpt = rpt.Subreports[0];
        subRpt.SetDataSource(dt);
    }

    private void SendEmail(string _IDLopHoc)
    {
        try
        {
            HttpPostedFile file = Request.Files["FileDinhKem"];

            SqlCnktLopHocProvider pro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc obj = pro.GetByLopHocId(Library.Int32Convert(_IDLopHoc));

            List<string> listMailAddress = new List<string>(); // List Địa chỉ email hợp lệ sẽ được gửi
            List<string> listSubject = new List<string>();
            List<string> listContent = new List<string>();
            List<string> listHoiVienID = new List<string>();
            List<string> listLoaiHoiVien = new List<string>();

            string query = @"SELECT COUNT(*) FROM (SELECT Email, HoiVienTapTheID AS ID, LoaiHoiVienTapThe AS LoaiHV, 'tt' AS Loai FROM tblHoiVienTapThe
                                        UNION
                                        SELECT Email, HoiVienCaNhanID AS ID, LoaiHoiVienCaNhan AS LoaiHV, 'cn' AS Loai FROM tblHoiVienCaNhan) AS X";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = query;
            int tongso = int.Parse(DataAccess.DLookup(cmd));
            int sovonglap = tongso / 350;
            if (tongso % 350 != 0)
                sovonglap++;
            for (int k = 0; k < sovonglap; k++)
            {
                listMailAddress.Clear();
                listSubject.Clear();
                listContent.Clear();
                listHoiVienID.Clear();
                listLoaiHoiVien.Clear();

                query = @"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY ID DESC) AS Row, Email, ID, LoaiHV, Loai FROM (SELECT Email, HoiVienTapTheID AS ID, LoaiHoiVienTapThe AS LoaiHV, 'tt' AS Loai FROM tblHoiVienTapThe
                                        UNION
                                        SELECT Email, HoiVienCaNhanID AS ID, LoaiHoiVienCaNhan AS LoaiHV, 'cn' AS Loai FROM tblHoiVienCaNhan) AS X) AS XX";
                query += " WHERE XX.Row > " + k * 350 + " AND XX.Row <= " + (k + 1) * 350;
                //string query = "select top 1 'son-nq@hipt.com.vn' AS Email, 1 AS ID, 1 AS LoaiHV, 'cn' AS Loai from tblHoiVienCaNhan";
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    int index = 0;
                    foreach (Hashtable ht in listData)
                    {
                        listHoiVienID.Add(ht["ID"].ToString());
                        listLoaiHoiVien.Add("");
                        if (ht["Loai"].ToString() == "cn")
                        {
                            listLoaiHoiVien[index] = ht["LoaiHV"].ToString();
                        }
                        else if (ht["Loai"].ToString() == "tt")
                        {
                            if (ht["LoaiHV"].ToString() == "0" || ht["LoaiHV"].ToString() == "2")
                                listLoaiHoiVien[index] = "3";
                            if (ht["LoaiHV"].ToString() == "1")
                                listLoaiHoiVien[index] = "4";
                        }
                        string email = ht["Email"].ToString();
                        if (!string.IsNullOrEmpty(email) && Library.CheckIsValidEmail(email))
                        {
                            listMailAddress.Add(email);
                            listSubject.Add("VACPA - Thư mời tham gia lớp học mới");
                            string content = "<b>HỘI KIỂM TOÁN VIÊN HÀNH NGHỀ VIỆT NAM - VACPA xin thông báo:</b><br />";
                            content += "Về việc tổ chức lớp học \"<b>" + obj.TenLopHoc + "</b>\". Từ ngày " +
                                                    " " + obj.TuNgay.Value.ToString("dd/MM/yyyy") + " đến " + obj.DenNgay.Value.ToString("dd/MM/yyyy");
                            content += "<br />Kính mời các cá nhân và công ty nếu quan tâm xin hãy đăng ký trước ngày <b>" + obj.HanDangKy.Value.ToString("dd/MM/yyyy") + "</b><br />";
                            content += "Chi tiết nội dung thư mời xem tại đường dẫn sau: <a href='https://" + Request.Url.Authority + "/noframe.aspx?page=CNKT_QuanLyLopHoc_ViewEmailForAnonymous&idlh=" + _IDLopHoc + "' target='_blank'>(Nội dung thư mời tham dự lớp đào tạo, cập nhật kiến thức)</a>";
                            listContent.Add(content);
                        }
                        index++;
                    }
                }
                if (listMailAddress.Count > 0)
                {
                    if (file.ContentLength > 0)
                    {
                        string MediaPathRoot = ConfigurationSettings.AppSettings["MediaPathRoot"];
                        string MediaPath = ConfigurationSettings.AppSettings["MediaPath"];
                        string FullFilePath = "";
                        string FileName = "";

                        FileName = Path.GetFileName(file.FileName);
                        FullFilePath = MediaPathRoot;
                        if (!File.Exists(FullFilePath))
                        {
                            try
                            {
                                System.IO.Directory.CreateDirectory(FullFilePath);
                            }
                            catch
                            {
                            }
                        }


                        FullFilePath = FullFilePath + "\\" + FileName;

                        file.SaveAs(FullFilePath);

                        if (utility.SendWithImage(listMailAddress, listSubject, listContent, MediaPath + "/" + FileName))
                        {
                            // Gui thong bao trong he thong
                            for (int i = 0; i < listHoiVienID.Count; i++)
                            {
                                cm.GuiThongBao(listHoiVienID[i], listLoaiHoiVien.Count > i ? listLoaiHoiVien[i] : "", listSubject[0], listContent[0]);
                            }

                            cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Gửi thư mời tham dự lớp học \"" + obj.TenLopHoc + "\"");
                            string js = "<script type='text/javascript'>" + Environment.NewLine;
                            string msg = "Đã gửi Email tới các công ty trong danh sách.";
                            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
                            js += "</script>";
                            Page.RegisterStartupScript("SendEmailSuccess", js);
                        }
                    }
                    else
                    {
                        if (utility.SendEmail_CNKT(listMailAddress, listSubject, listContent))
                        {
                            // Gui thong bao trong he thong
                            for (int i = 0; i < listHoiVienID.Count; i++)
                            {
                                cm.GuiThongBao(listHoiVienID[i], listLoaiHoiVien.Count > i ? listLoaiHoiVien[i] : "", listSubject[0], listContent[0]);
                            }

                            cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Gửi thư mời tham dự lớp học \"" + obj.TenLopHoc + "\"");
                            string js = "<script type='text/javascript'>" + Environment.NewLine;
                            string msg = "Đã gửi Email tới các công ty trong danh sách.";
                            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
                            js += "</script>";
                            Page.RegisterStartupScript("SendEmailSuccess", js);
                        }
                    }
                }
            }
        }
        catch { }

    }

}