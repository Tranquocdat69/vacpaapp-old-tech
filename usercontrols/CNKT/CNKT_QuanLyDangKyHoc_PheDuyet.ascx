﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyDangKyHoc_PheDuyet.ascx.cs"
    Inherits="usercontrols_CNKT_QuanLyDangKyHoc_PheDuyet" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
    
    .tdInput
    {
        max-width: 100px;
    }
</style>
<form id="Form1" name="Form1" runat="server">
<div>
    <fieldset class="fsBlockInfor" style="margin-top: 0px;">
        <legend>Thông tin chuyên đề:</legend>
        <asp:HiddenField ID="hf_TenChuyenDe" runat="server" />
        <asp:HiddenField ID="hf_TenLopHoc" runat="server" />
        <asp:HiddenField ID="hf_TuNgay" runat="server" />
        <asp:HiddenField ID="hf_DenNgay" runat="server" />
        <table width="100%" border="0" class="formtblInfor">
            <tr>
                <td>
                    Mã chuyên đề:
                </td>
                <td>
                    <asp:DropDownList ID="ddlMaChuyenDe" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlMaChuyenDe_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    Tên chuyên đề:
                </td>
                <th>
                    <asp:TextBox ID="txtTenChuyenDe" runat="server" Enabled="False"></asp:TextBox>
                </th>
            </tr>
            <tr>
                <td rowspan="2">
                    Lịch học:
                </td>
                <td rowspan="2">
                    <asp:Label ID="lblLichHoc" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    Giảng viên:
                </td>
                <th>
                    <asp:TextBox ID="txtGiangVien" runat="server" Enabled="False"></asp:TextBox>
                </th>
            </tr>
            <tr>
                <td>
                    Số học viên đã đăng ký:
                </td>
                <th>
                    <asp:TextBox ID="txtSoHocVienDaDangKy" runat="server" Enabled="False"></asp:TextBox>
                </th>
            </tr>
        </table>
    </fieldset>
    <fieldset class="fsBlockInfor" style="margin-top: 0px;">
        <legend>Danh sách học viên đăng ký cần phê duyệt:</legend>
        <div style="width: 100%; margin: 10px 0 10px 0;">
            <asp:LinkButton ID="lbtApproveAll" runat="server" CssClass="btn" 
                OnClientClick="return SubmitAction('Bạn chắc chắn muốn duyệt những đăng ký này chứ?');" 
                onclick="lbtApproveAll_Click"><i class="iconfa-thumbs-up"></i>Duyệt</asp:LinkButton>
            &nbsp;
            <asp:LinkButton ID="lbtRejectAll" runat="server" CssClass="btn" 
                OnClientClick="return SubmitAction('Bạn chắc chắn muốn từ chối những đăng ký này chứ?');" 
                onclick="lbtRejectAll_Click"><i class="iconfa-remove-sign"></i>Từ chối</asp:LinkButton>
        </div>
        <table width="100%" border="0" class="formtbl">
            <tr>
                <th style="width: 30px;">
                    <input type='checkbox' id="checkall" value="all" onchange="CheckAllCheckbox();"/>
                </th>
                <th style="width: 100px;">
                    Mã
                </th>
                <th style="min-width: 150px;">
                    Họ và tên
                </th>
                <th style="width: 100px;">
                    Loại
                </th>
                <th style="width: 100px;">
                    Số chứng chỉ KTV
                </th>
                <th style="width: 80px;">
                    Ngày đăng ký
                </th>
                <th style="width: 150px;">
                    Thao tác
                </th>
            </tr>
        </table>
        <div id='DivDanhSachDangKyHoc' style="max-height: 300px;">
            <table id="tblDanhSachDangKyHoc" width="100%" border="0" class="formtbl">
                <asp:Repeater ID="rpDanhSachDangKyHoc" runat="server" OnItemCommand="rpDanhSachDangKyHoc_ItemCommand"
                    OnItemDataBound="rpDanhSachDangKyHoc_ItemDataBound">
                    <ItemTemplate>
                        <%# Eval("HeaderType") %>
                        <tbody <%# Eval("HiddenRow") %>>
                            <tr <%# Eval("trStyle") %>>
                                <td style="text-align: center; width: 30px;">
                                    <input type="checkbox" id="checkbox" runat="server" value='<%# Eval("LopHocHocVienID") %>'/>
                                </td>
                                <td style="width: 100px;">
                                    <%# Eval("mhvcn")%>
                                </td>
                                <td style="min-width: 150px;">
                                    <%# Eval("FullName")%>
                                </td>
                                <td style="width: 100px;">
                                    <%# Eval("LoaiHoiVienCaNhan")%>
                                </td>
                                <td style="width: 100px;">
                                    <%# Eval("SoChungChiKTV")%>
                                </td>
                                <td style="width: 80px;">
                                    <%# Eval("NgayDangKy")%>
                                </td>
                                <td style="width: 150px;">
                                    &nbsp;
                                    <asp:LinkButton ID="lbtApprove" runat="server" CssClass="btn" CommandName="approve"
                                        CommandArgument='<%# Eval("LopHocHocVienID") %>' OnClientClick="return confirm('Bạn chắc chắn muốn duyệt đăng ký này chứ?');">Duyệt</asp:LinkButton>
                                    &nbsp;
                                    <asp:LinkButton ID="lbtReject" runat="server" CssClass="btn" CommandName="reject"
                                        CommandArgument='<%# Eval("LopHocHocVienID") %>' OnClientClick="return confirm('Bạn chắc chắn muốn từ chối đăng ký này chứ?');">Từ chối</asp:LinkButton>
                                    <asp:HiddenField ID="hf_LopHocHocVien" runat="server" Value='<%# Eval("LopHocHocVienID") %>' />
                                </td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </fieldset>
</div>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">

<% CheckPermissionOnPage(); %>

    function SubmitAction(msg) {
        if(confirm(msg)) {
            var flag = false;
            $('#tblDanhSachDangKyHoc :checkbox').each(function () {
                if($(this).prop("checked"))
                    flag = true;
            });
            if(!flag) {
                alert('Phải chọn một bản đăng ký để thực hiện thao tác!');
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    function CheckAllCheckbox() {
        var checked = $('#checkall').prop('checked');
        $('#tblDanhSachDangKyHoc :checkbox').each(function () {
            $(this).prop("checked", checked);
        });
    }
    

// Hiển thị tooltip
    if (jQuery('#tblDanhSachDangKyHoc').length > 0) jQuery('#tblDanhSachDangKyHoc').tooltip({ selector: "a[rel=tooltip]" });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm kiểm tra trình duyệt để set thuộc tính overflow cho thẻ DIV hiển thị danh sách bản ghi
    // Chỉ trình duyệt nền Webkit mới có thuộc tính overflow: overlay
    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivDanhSachDangKyHoc').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivDanhSachDangKyHoc').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivDanhSachDangKyHoc').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }

    myFunction();
</script>
<div id="DivDanhSachLopHoc">
</div>
