﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_dmgiangvien_List : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadListChucVu();
            LoadListTrinhDoChuyenMon();
            LoadListGiangVien();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/20
    /// Load danh sach giang vien theo dieu kien search
    /// </summary>
    private void LoadListGiangVien()
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "GiangVienID", "MaGiangVien", "TenGiangVien", "ChucVu", "DonViCongTac", "TrinhDoChuyenMon", "ChungChi" });
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                con.Open();
                string command = GetQueryString();
                SqlCommand cmd = new SqlCommand(command, con);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    DataRow dataRow;
                    int index = 1;
                    while (dr.Read())
                    {
                        dataRow = dt.NewRow();
                        dataRow["STT"] = index;
                        dataRow["GiangVienID"] = dr["GiangVienID"];
                        dataRow["TenGiangVien"] = dr["TenGiangVien"];
                        dataRow["ChucVu"] = dr["TenChucVu"];
                        dataRow["DonViCongTac"] = dr["DonViCongTac"];
                        dataRow["TrinhDoChuyenMon"] = dr["TenTrinhDoChuyenMon"];
                        dataRow["ChungChi"] = dr["ChungChi"];
                        dt.Rows.Add(dataRow);
                        index++;
                    }
                }
                rpGiangVien.DataSource = dt.DefaultView;
                rpGiangVien.DataBind();
                dr.Close();
                cmd.Cancel();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/24
    /// Get cau lenh query theo dieu kien tim kiem
    /// </summary>
    /// <returns></returns>
    private string GetQueryString()
    {
        string maGiangVien = txtMaGiangVien.Text;
        string trinhDoChuyenMonID = ddlTrinhDoChuyenMon.SelectedValue;
        string hoTen = txtHoTen.Text;
        string chungChi = txtChungChi.Text;
        string chucVu = ddlChucVu.SelectedValue;
        string donViCongTac = txtDonViCongTac.Text;
        string gioiTinh = ddlGioiTinh.SelectedValue;
        string query = @"SELECT tblDMGiangVien.GiangVienID as GiangVienID, tblDMGiangVien.TenGiangVien as TenGiangVien, tblDMGiangVien.DonViCongTac as DonViCongTac,
                                tblDMGiangVien.ChungChi as ChungChi, tblDMChucVu.TenChucVu as TenChucVu, tblDMTrinhDoChuyenMon.TenTrinhDoChuyenMon as TenTrinhDoChuyenMon
                                 FROM tblDMGiangVien
                                 LEFT JOIN tblDMChucVu ON tblDMGiangVien.ChucVuID = tblDMChucVu.ChucVuID
                                 LEFT JOIN tblDMTrinhDoChuyenMon on tblDMGiangVien.TrinhDoChuyenMonID = tblDMTrinhDoChuyenMon.TrinhDoChuyenMonID
                                 WHERE {MaGiangVien} AND {TrinhDoChuyenMon} AND {TenGiangVien} AND {ChungChi} AND {ChucVu} AND {DonViCongTac} AND {GioiTinh}";
        query = !string.IsNullOrEmpty(maGiangVien) ? query.Replace("{MaGiangVien}", "tblDMGiangVien.MaGiangVien like '%" + maGiangVien + "%'") : query.Replace("{MaGiangVien}", "1=1");

        query = !string.IsNullOrEmpty(trinhDoChuyenMonID) ? query.Replace("{TrinhDoChuyenMon}", "tblDMGiangVien.TrinhDoChuyenMonID = '" + trinhDoChuyenMonID + "'") : query.Replace("{TrinhDoChuyenMon}", "1=1");

        query = !string.IsNullOrEmpty(hoTen) ? query.Replace("{TenGiangVien}", "tblDMGiangVien.TenGiangVien like N'%" + hoTen + "%'") : query.Replace("{TenGiangVien}", "1=1");
        query = !string.IsNullOrEmpty(chungChi) ? query.Replace("{ChungChi}", "tblDMGiangVien.ChungChi = '" + chungChi + "'") : query.Replace("{ChungChi}", "1=1");
        query = !string.IsNullOrEmpty(chucVu) ? query.Replace("{ChucVu}", "tblDMGiangVien.ChucVuID = '" + chucVu + "'") : query.Replace("{ChucVu}", "1=1");
        query = !string.IsNullOrEmpty(donViCongTac) ? query.Replace("{DonViCongTac}", "tblDMGiangVien.DonViCongTac like N'%" + donViCongTac + "%'") : query.Replace("{DonViCongTac}", "1=1");
        query = !string.IsNullOrEmpty(gioiTinh) ? query.Replace("{GioiTinh}", "tblDMGiangVien.GioiTinh = '" + gioiTinh + "'") : query.Replace("{GioiTinh}", "1=1");
        query += " ORDER BY tblDMGiangVien.TenGiangVien";
        return query;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/24
    /// Load danh sach chuc vu do vao DropDownList
    /// </summary>
    private void LoadListChucVu()
    {
        if (ddlChucVu.Items.Count > 0)
            return;
        ddlChucVu.Items.Clear();
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                con.Open();
                string query = "SELECT ChucVuID, TenChucVu FROM tblDMChucVu ORDER BY TenChucVu";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    ddlChucVu.Items.Add(new ListItem("Tất cả", ""));
                    while (dr.Read())
                    {
                        ddlChucVu.Items.Add(new ListItem(dr["TenChucVu"].ToString(), dr["ChucVuID"].ToString()));
                    }
                }
                dr.Close();
                cmd.Cancel();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/24
    /// Load danh sach trinh do chuyen mon do vao DropDownList
    /// </summary>
    private void LoadListTrinhDoChuyenMon()
    {
        if (ddlTrinhDoChuyenMon.Items.Count > 0)
            return;
        ddlTrinhDoChuyenMon.Items.Clear();
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                con.Open();
                string query = "SELECT TrinhDoChuyenMonID, TenTrinhDoChuyenMon FROM tblDMTrinhDoChuyenMon ORDER BY TenTrinhDoChuyenMon";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    ddlTrinhDoChuyenMon.Items.Add(new ListItem("Tất cả", ""));
                    while (dr.Read())
                    {
                        ddlTrinhDoChuyenMon.Items.Add(new ListItem(dr["TenTrinhDoChuyenMon"].ToString(), dr["TrinhDoChuyenMonID"].ToString()));
                    }
                }
                dr.Close();
                cmd.Cancel();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LoadListGiangVien();
    }
}