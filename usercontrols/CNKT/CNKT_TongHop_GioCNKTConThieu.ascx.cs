﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodeEngine.Framework.QueryBuilder.Enums;
using HiPT.VACPA.DL;
using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_CNKT_TongHop_GioCNKTConThieu : System.Web.UI.UserControl
{
    protected string tenchucnang = "Tổng hợp số giờ CNKT còn thiếu của học viên";
    protected string _listPermissionOnFunc_TongHop_SoGioCNKT = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private string _hocVienId = "", _congTyId = "", _year = "", _tenHoiVien = "", _soCCKTV = "", _fromDate = "", _toDate = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        _listPermissionOnFunc_TongHop_SoGioCNKT = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_TongHop_SoGioCNKTConThieuCuaHocVien, cm.connstr);


        this._hocVienId = Library.CheckNull(Request.QueryString["idhv"]);
        this._congTyId = Library.CheckNull(Request.QueryString["idct"]);
        this._year = Library.CheckNull(Request.QueryString["year"]);
        this._tenHoiVien = Library.CheckNull(Request.QueryString["tenhv"]);
        this._soCCKTV = Library.CheckNull(Request.QueryString["soccktv"]);
        this._fromDate = Library.CheckNull(Request.QueryString["fromdate"]);
        this._toDate = Library.CheckNull(Request.QueryString["todate"]);
        if (_listPermissionOnFunc_TongHop_SoGioCNKT.Contains("XEM|"))
        {
            try
            {
                _db.OpenConnection();
                if (!string.IsNullOrEmpty(this._year))
                    LoadSoLieuTongHop(_hocVienId, _congTyId, _year, _tenHoiVien, _soCCKTV, _fromDate, _toDate);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _db.CloseConnection();
            }
        }
        else
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem tổng hợp số giờ CNKT còn thiếu của học viên trong năm!</div>"));
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/02
    /// Tổng hợp số liệu theo yêu cầu truyền vào
    /// </summary>
    /// <param name="hocvienId">ID học viên cần tìm</param>
    /// <param name="congTyId">ID công ty cần tìm</param>
    /// <param name="year">Năm cần tổng hợp số liệu</param>
    private void LoadSoLieuTongHop(string hocvienId, string congTyId, string year, string tenHoiVien, string soCCKTV, string fromDate, string toDate)
    {
        // Lấy số liệu "Cơ cấu số giờ học tối thiếu trong năm"
        List<int> listCoCauSoGioHocToiThieu = LoadCoCauSoGioHocToiThieu(year);

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        int soHocVien = 0, soHocVienCaNhan = 0, soHocVienTapThe = 0;
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "mhvcn", "FullName", "SoChungChiKTV", "NgayCapChungChiKTV", "trStyle", "HeaderType", "HiddenRow", "SoGioKT", "SoGioDD", "SoGioKhac", "SoGioKTConThieu", "SoGioDDConThieu", "SoGioKhacConThieu" });
        
        // Kiểm tra định dạng ngày tháng đúng chưa
        if (!string.IsNullOrEmpty(fromDate))
        {
            fromDate = !Library.CheckDateTime(fromDate, "dd/MM/yyyy") ? "" : Library.DateTimeConvert(fromDate, "dd/MM/yyyy").ToString("yyyy-MM-dd");
        }

        if (!string.IsNullOrEmpty(toDate))
        {
            toDate = !Library.CheckDateTime(toDate, "dd/MM/yyyy") ? "" : Library.DateTimeConvert(toDate, "dd/MM/yyyy").ToString("yyyy-MM-dd");
        }

        // Nếu ko tìm từ ngày -> đến ngày thì lấy ngày trong năm đc chọn để tìm
        if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
        {
            if (!string.IsNullOrEmpty(year))
            {
                if (!string.IsNullOrEmpty(year))
                {
                    fromDate = (int.Parse(year) - 1) + "-08-16";
                    toDate = year + "-08-15";
                }
            }
        }

        List<string> paramNames = new List<string> { "@FromDate", "@ToDate", "@HoiVienTapTheID", "@HoiVienCaNhanID", "@TenHoiVienCaNhan", "@SoChungChiKTV" };
        List<object> paramValues = new List<object> { fromDate, toDate, congTyId, hocvienId, tenHoiVien, soCCKTV };

        List<Hashtable> list = _db.GetListData(ListName.Proc_procTongHopSoGioCNKTConThieu, paramNames, paramValues);

        if (list.Count > 0)
        {
            DataRow dr;

            // Thêm đoạn header những học viên cá nhân
            dr = dt.NewRow();
            dr["HeaderType"] = "<tr style='background-color: lavender; font-weight: bold;'><td colspan='11'>HỌC VIÊN CÁ NHÂN (<span name=\"spanSoHocVienCaNhan\" id=\"spanSoHocVienCaNhan\"></span> học viên)</td></tr>";
            dr["HiddenRow"] = "style ='display:none;'";
            dt.Rows.Add(dr);

            string temp_HoiVienCaNhan = ""; // Biến tạm để xác định những bản ghi dữ liệu của cùng 1 học viên
            string temp_HoiVienTapThe = ""; // Biến tạm để xác định những học viên cùng công ty
            string space = ""; // Thêm khoảng trống để các ô dữ liệu đăng ký dịch lùi vào
            bool flag_HeaderCongTy = false;
            int index = 1;
            int sogioKhacCongThem = 0;
            foreach (Hashtable ht in list)
            {
                string hoiVienTapTheID = Library.CheckKeyInHashtable(ht, "hoiVienTapTheID").ToString(); // Chỉ những học viên thuộc các công ty mới có trường này

                // Đoạn code xác định nhưng đơn đăng ký theo công ty và hiển thị 1 dòng tên công ty ở đầu tiên
                if (!string.IsNullOrEmpty(hoiVienTapTheID) && hoiVienTapTheID != temp_HoiVienTapThe)
                {
                    // Thêm đoạn header những đơn đăng ký theo công ty hoặc tổ chức
                    if (!flag_HeaderCongTy)
                    {
                        dr = dt.NewRow();
                        dr["HeaderType"] = "<tr style='background-color: lavender; font-weight: bold;'><td colspan='11'>HỌC VIÊN TRONG CÔNG TY HOẶC HỘI VIÊN TẬP THỂ (<span name=\"spanSoHocVienTapThe\" id=\"spanSoHocVienTapThe\"></span> học viên)</td></tr>";
                        dr["HiddenRow"] = "style ='display:none;'";
                        dt.Rows.Add(dr);
                        flag_HeaderCongTy = true;
                    }

                    // Đoạn code add các dòng hội viên tổ chức
                    space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    dr = dt.NewRow();
                    dr["mhvcn"] = Library.CheckKeyInHashtable(ht, "maHoiVienTapThe");
                    dr["FullName"] = Library.CheckKeyInHashtable(ht, "TenDoanhNghiep");
                    dr["trStyle"] = "style='font-weight: bold;'";
                    dt.Rows.Add(dr);
                    temp_HoiVienTapThe = hoiVienTapTheID;
                }

                // Add các đơn đăng ký vào danh sách bình thường
                string hoiVienCaNhanID = Library.CheckKeyInHashtable(ht, "HoiVienCaNhanID").ToString();
                if (temp_HoiVienCaNhan != hoiVienCaNhanID)
                {
                    sogioKhacCongThem = 0;
                    // Đếm số học viên đăng ký theo cá nhân và tổ chức
                    if (string.IsNullOrEmpty(hoiVienTapTheID))
                        soHocVienCaNhan++;
                    else
                    {
                        soHocVienTapThe++;
                    }

                    soHocVien++;
                    dr = dt.NewRow();
                    dr["STT"] = soHocVien;
                    dr["mhvcn"] = Library.CheckKeyInHashtable(ht, "mhvcn");
                    dr["FullName"] = space + Library.CheckKeyInHashtable(ht, "FullName");
                    dr["SoChungChiKTV"] = Library.CheckKeyInHashtable(ht, "SoChungChiKTV");
                    dr["NgayCapChungChiKTV"] = Library.FormatDateTime(Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "NgayCapChungChiKTV")), "dd/MM/yyyy");
                    temp_HoiVienCaNhan = hoiVienCaNhanID;
                }
                string loaiChuyenDe = Library.CheckKeyInHashtable(ht, "LoaiChuyenDe").ToString();
                int tongSoGioThucTe = Library.Int32Convert(Library.CheckKeyInHashtable(ht, "TongSoGioThucTe"));
                if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_KeToanKiemToan)
                {
                    dr["SoGioKT"] = tongSoGioThucTe;
                    if (listCoCauSoGioHocToiThieu.Count > 0)
                        dr["SoGioKTConThieu"] = listCoCauSoGioHocToiThieu[0] - tongSoGioThucTe;
                    if (tongSoGioThucTe > listCoCauSoGioHocToiThieu[0])
                    {
                        dr["SoGioKTConThieu"] = 0;
                        sogioKhacCongThem += tongSoGioThucTe - listCoCauSoGioHocToiThieu[0];

                        dr["SoGioKhac"] = "0";
                        dr["SoGioKhacConThieu"] = listCoCauSoGioHocToiThieu[2] - sogioKhacCongThem;
                        if (sogioKhacCongThem >= listCoCauSoGioHocToiThieu[2])
                            dr["SoGioKhacConThieu"] = 0;
                    }

                }
                if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep)
                {
                    dr["SoGioDD"] = tongSoGioThucTe;
                    dr["SoGioDDConThieu"] = listCoCauSoGioHocToiThieu[1] - tongSoGioThucTe;
                    if (tongSoGioThucTe > listCoCauSoGioHocToiThieu[1])
                    {
                        dr["SoGioDDConThieu"] = 0;
                        sogioKhacCongThem += tongSoGioThucTe - listCoCauSoGioHocToiThieu[1];

                        dr["SoGioKhac"] = "0";
                        dr["SoGioKhacConThieu"] = listCoCauSoGioHocToiThieu[2] - sogioKhacCongThem;
                        if (sogioKhacCongThem >= listCoCauSoGioHocToiThieu[2])
                            dr["SoGioKhacConThieu"] = 0;
                    }
                }
               
                if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_ChuyenDeKhac)
                {
                    dr["SoGioKhac"] = tongSoGioThucTe;
                    dr["SoGioKhacConThieu"] = listCoCauSoGioHocToiThieu[2] - tongSoGioThucTe - sogioKhacCongThem;
                    if (tongSoGioThucTe + sogioKhacCongThem >= listCoCauSoGioHocToiThieu[2])
                        dr["SoGioKhacConThieu"] = 0;
                }
                if (index < list.Count)
                {
                    if (hoiVienCaNhanID != list[index]["HoiVienCaNhanID"].ToString())
                    {
                        if (string.IsNullOrEmpty(dr["SoGioKT"].ToString()))
                        {
                            dr["SoGioKT"] = "0";
                            dr["SoGioKTConThieu"] = listCoCauSoGioHocToiThieu[0];
                        }
                        if (string.IsNullOrEmpty(dr["SoGioDD"].ToString()))
                        {
                            dr["SoGioDD"] = "0";
                            dr["SoGioDDConThieu"] = listCoCauSoGioHocToiThieu[1];
                        }
                        if (string.IsNullOrEmpty(dr["SoGioKhac"].ToString()) && sogioKhacCongThem == 0)
                        {
                            dr["SoGioKhac"] = "0";
                            dr["SoGioKhacConThieu"] = listCoCauSoGioHocToiThieu[2];
                        }
                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(dr["SoGioKT"].ToString()))
                    {
                        dr["SoGioKT"] = "0";
                        dr["SoGioKTConThieu"] = listCoCauSoGioHocToiThieu[0];
                    }
                    if (string.IsNullOrEmpty(dr["SoGioDD"].ToString()))
                    {
                        dr["SoGioDD"] = "0";
                        dr["SoGioDDConThieu"] = listCoCauSoGioHocToiThieu[1];
                    }
                    if (string.IsNullOrEmpty(dr["SoGioKhac"].ToString()) && sogioKhacCongThem == 0)
                    {
                        dr["SoGioKhac"] = "0";
                        dr["SoGioKhacConThieu"] = listCoCauSoGioHocToiThieu[2];
                    }
                    dt.Rows.Add(dr);
                }
                index++;
            }
        }
        rpDanhSachKetQuaTongHop.DataSource = dt.DefaultView;
        rpDanhSachKetQuaTongHop.DataBind();

        js += "$('#spanSoHocVienCaNhan').html('" + soHocVienCaNhan + "');" + Environment.NewLine;
        js += "$('#spanSoHocVienTapThe').html('" + soHocVienTapThe + "');" + Environment.NewLine;
        js += "</script>";
        Page.RegisterStartupScript("LoadInforLopHoc2", js);
    }

    private List<int> LoadCoCauSoGioHocToiThieu(string year)
    {
        List<int> list = new List<int>();
        string query = "SELECT TOP 1 KeToanKiemToan, DaoDucNgheNghiep, Khac FROM " + ListName.Table_CNKTCoCauSoGioHocToiThieu + " " +
                       "WHERE NamBatDau <= " + year + " AND NamKetThuc >= " + year;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            list = new List<int> { Library.Int32Convert(ht["KeToanKiemToan"]), Library.Int32Convert(ht["DaoDucNgheNghiep"]), Library.Int32Convert(ht["Khac"]) };
        }
        return list;
    }

    protected void FirstLoad()
    {
        // Đổ giá trị tìm kiếm vào các ô search
        string js = "";
        js += "$('#txtMaHocVien').val('" + Library.CheckNull(Request.QueryString["mahv"]) + "');" + Environment.NewLine;
        js += "$('#txtMaCongTy').val('" + Library.CheckNull(Request.QueryString["mact"]) + "');" + Environment.NewLine;
        js += "CallActionGetInfor('ct');" + Environment.NewLine;
        js += "CallActionGetInfor('hv');" + Environment.NewLine;
        js += "$('#txtTenHocVien').val('" + _tenHoiVien + "');" + Environment.NewLine;
        js += "$('#txtSoChungChiKTV').val('" + _soCCKTV + "');" + Environment.NewLine;
        Response.Write(js);
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_TongHop_SoGioCNKT.Contains("KETXUAT|"))
            Response.Write("$('#btn_ketxuat').remove();");

        if (!_listPermissionOnFunc_TongHop_SoGioCNKT.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
    }

    #region Gen control in "Search Form"

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get List of year from 2000 - 2030 to fill into dropdownlist search by Year.
    /// </summary>
    protected void LoadListYear()
    {
        string output_html = "";
        int selected = DateTime.Now.Year;
        if (!string.IsNullOrEmpty(_year) && Library.CheckIsInt32(_year))
            selected = Library.Int32Convert(_year);
        for (int i = 1940; i <= 2050; i++)
        {

            output_html += i == selected
                               ? "<option selected='selected' value='" + i + "'>" + i + "</option>"
                               : "<option value='" + i + "'>" + i + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }
    #endregion
    protected void lbtKetXuat_Click(object sender, EventArgs e)
    {
        //Library.ExportToExcel("TongHopSoGioCNKTConThieuCuaHocVien.xls", DivContent.InnerHtml);
    }
}