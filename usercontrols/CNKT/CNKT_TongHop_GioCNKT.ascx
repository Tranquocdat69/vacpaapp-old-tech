﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_TongHop_GioCNKT.ascx.cs"
    Inherits="usercontrols_CNKT_TongHop_GioCNKT" %>
<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
    
    .tdInput
    {
        max-width: 100px;
    }
</style>
<form id="Form1" name="Form1" runat="server">
<h4 class="widgettitle">
    Danh sách tổng hợp số giờ CNKT của học viên</h4>
<div>
    <div class="dataTables_length">
        <fieldset class="fsBlockInfor" style="margin-top: 0px;">
            <legend>Tìm kiếm danh sách học viên</legend>
            <table width="100%" border="0" class="formtblInfor">
                <tr>
                    <td>
                        Năm:
                    </td>
                    <td>
                        <select id="ddlYear" name="ddlYear">
                            <%LoadListYear(); %>
                        </select>
                    </td>
                    <td>
                        Từ ngày:
                    </td>
                    <td>
                        <input type="text" id="txtTuNgay" name="txtTuNgay" style="width: 80px;" /><img src="/images/icons/calendar.png"
                            id="imgCalendarTuNgay" />
                    </td>
                    <td>
                        Đến ngày:
                    </td>
                    <td>
                        <input type="text" id="txtDenNgay" name="txtDenNgay" style="width: 80px;" /><img
                            src="/images/icons/calendar.png" id="imgCalendarDenNgay" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Mã lớp học:
                    </td>
                    <td>
                        <input type="text" id="txtMaLopHoc" name="txtMaLopHoc" onchange="CallActionGetInfor('lh');"
                            class="tdInput" />&nbsp;
                        <input type="button" value="---" onclick="OpenDanhSach('DivDanhSachLopHoc', 'CNKT_QuanLyLopHoc_MiniList', 'Danh sách lớp học');"
                            style="border: 1px solid gray;" />
                        <input type="hidden" name="hdLopHocID" id="hdLopHocID" />
                    </td>
                    <td>
                        Tên lớp học:
                    </td>
                    <th colspan="3">
                        <span id="spanTenLopHoc"></span>
                    </th>
                </tr>
                <tr>
                    <td>
                        Mã đơn vị công tác:
                    </td>
                    <td>
                        <input type="text" id="txtMaCongTy" name="txtMaCongTy" class="tdInput" onchange="CallActionGetInfor('ct');" />&nbsp;
                        <input type="button" value="---" onclick="OpenDanhSach('DivDanhSachCongTy', 'HoiVienTapThe_MiniList', 'Danh sách công ty');"
                            style="border: 1px solid gray;" />
                        <input type="hidden" name="hdCongTyID" id="hdCongTyID" />
                    </td>
                    <td>
                        Tên đơn vị:
                    </td>
                    <th colspan="3">
                        <span id="spanTenCongTy"></span>
                    </th>
                </tr>
                <tr>
                    <td>
                        Mã học viên:
                    </td>
                    <td>
                        <input type="text" id="txtMaHocVien" name="txtMaHocVien" class="tdInput" onchange="CallActionGetInfor('hv');" />&nbsp;
                        <input type="button" value="---" onclick="OpenDanhSach('DivDanhSachHocVien', 'hoiviencanhan_minilist', 'Danh sách học viên');"
                            style="border: 1px solid gray;" />
                        <input type="hidden" name="hdHocVienID" id="hdHocVienID" />
                    </td>
                    <td>
                        Tên học viên:
                    </td>
                    <td colspan="3">
                        <input type="text" id="txtTenHocVien" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <div class="dataTables_length" style="text-align: center;">
            <a id="btnSearch" href="javascript:;" class="btn" onclick="Search();"><i class="iconfa-search">
            </i>Tìm kiếm</a>&nbsp;<a id="btn_ketxuat" href="javascript:;" class="btn" onclick="CallExportPage();">
                <i class="iconfa-download"></i>Kết xuất</a>
        </div>
    </div>
    <div id="DivReportContent" class="dataTables_length">
        <table id="tblDanhSachKetQuaTongHop" width="100%" border="0" class="formtbl" style="font-family: Times New Roman;">
            <thead>
                <tr>
                    <th rowspan="2" style="width: 30px;">
                        STT
                    </th>
                    <th rowspan="2" style="width: 100px;">
                        Mã
                    </th>
                    <th rowspan="2" style="min-width: 250px;">
                        Tên công ty/Học viên
                    </th>
                    <th rowspan="2" style="width: 100px;">
                        Số chứng chỉ KTV
                    </th>
                    <th rowspan="2" style="width: 80px;">
                        Ngày cấp chứng chỉ KTV
                    </th>
                    <th colspan="3">
                        Tổng số giờ CNKT
                    </th>
                </tr>
                <tr>
                    <th style="width: 50px;">
                        KT, KiT
                    </th>
                    <th style="width: 50px;">
                        ĐĐ
                    </th>
                    <th style="width: 50px;">
                        Khác
                    </th>
                </tr>
            </thead>
            <asp:Repeater ID="rpDanhSachKetQuaTongHop" runat="server">
                <ItemTemplate>
                    <%# Eval("HeaderType") %>
                    <tbody <%# Eval("HiddenRow") %>>
                        <tr <%# Eval("trStyle") %>>
                            <td style="text-align: center; width: 30px;">
                                <%# Eval("STT") %>
                            </td>
                            <td style="width: 100px;">
                                <%# Eval("mhvcn")%>
                            </td>
                            <td style="min-width: 250px;">
                                <%# Eval("FullName")%>
                            </td>
                            <td style="width: 100px;">
                                <%# Eval("SoChungChiKTV")%>
                            </td>
                            <td style="width: 80px;">
                                <%# Eval("NgayCapChungChiKTV")%>
                            </td>
                            <td style="text-align: center; width: 50px;">
                                <%# Eval("SoGioKT")%>
                            </td>
                            <td style="text-align: center; width: 50px;">
                                <%# Eval("SoGioDD")%>
                            </td>
                            <td style="text-align: center; width: 50px;">
                                <%# Eval("SoGioKhac")%>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</div>
</form>
<iframe name="iframeProcess_LoadLopHoc" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadHocVien" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadCongTy" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_Export" width="0px" height="0px" src="/iframe.aspx?page=Export_Process"></iframe>
<script type="text/javascript">
$(document).ready( function () {
        $('#tblDanhSachKetQuaTongHop').stickyTableHeaders();
});
// add open Datepicker event for calendar inputs
    $("#txtTuNgay").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarTuNgay").click(function () {
        $("#txtTuNgay").datepicker("show");
    });
    $("#txtDenNgay").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarDenNgay").click(function () {
        $("#txtDenNgay").datepicker("show");
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách lớp học
    function OpenDanhSach(objId, pageName, title) {
        $("#" + objId).empty();
        $("#" + objId).append($("<iframe width='100%' height='100%' id='ifDanhSachLopHoc' name='ifDanhSachLopHoc' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=" + pageName));
        $("#" + objId).dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>"+title+"</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu lớp học theo mã lớp khi NSD nhập trực tiếp mã lớp vào ô textbox
    function CallActionGetInfor(type) {
        if(type == "lh"){
            var maLopHoc = $('#txtMaLopHoc').val();
            if(maLopHoc.length > 0)
                iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh='+maLopHoc+'&action=loadlh';
        }
        if(type == "ct"){
            var maCongTy = $('#txtMaCongTy').val();
            iframeProcess_LoadCongTy.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mact='+maCongTy+'&action=loadct';
        }
        if(type == "hv"){
            var maHocVien = $('#txtMaHocVien').val();
            iframeProcess_LoadHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mahv='+maHocVien+'&action=loadhv';
        }
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin lớp học từ Dialog danh sách
    function DisplayInforLopHoc(value) {
        var arrValue = value.split(';#');
        $('#hdLopHocID').val(arrValue[0]);
        $('#txtMaLopHoc').val(arrValue[1]);
        $('#spanTenLopHoc').html(arrValue[2]);
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách lớp học (Gọi hàm này từ trang con)
    function CloseFormDanhSachLopHoc() {
            $("#DivDanhSachLopHoc").dialog('close');
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin học viên từ Dialog danh sách
    function DisplayInforHocVien(value) {
        var arrValue = value.split(';#');
        $('#hdHocVienID').val(arrValue[0]);
        $('#txtMaHocVien').val(arrValue[1]);
        $('#txtTenHocVien').val(arrValue[3]);
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách học viên (Gọi hàm này từ trang con)
    function CloseFormDanhSachHocVien() {
        $("#DivDanhSachHocVien").dialog('close');
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm nhận giá trị thông tin công ty từ Dialog danh sách
    function DisplayInforCongTy(value) {
        var arrValue = value.split(';#');
        $('#hdCongTyID').val(arrValue[0]);
        $('#txtMaCongTy').val(arrValue[1]);
        $('#spanTenCongTy').html(arrValue[2]);
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm đóng Dialog danh sách công ty (Gọi hàm này từ trang con)
    function CloseFormDanhSachCongTy() {
        $("#DivDanhSachCongTy").dialog('close');
    }
    
    function Search() {
        var lopHocID = $('#hdLopHocID').val();
        var congTyID = $('#hdCongTyID').val();
        var hocVienID = $('#hdHocVienID').val();
        var year = $('#ddlYear').val();
        var fromDate = $('#txtTuNgay').val();
        var toDate = $('#txtDenNgay').val();
        window.location = window.location.href.split('?page=cnkt_tonghop_giocnkt')[0] + '?page=cnkt_tonghop_giocnkt&idlh=' + lopHocID + '&idhv='+hocVienID+'&idct='+congTyID+'&year='+year+'&fromdate='+fromDate+'&todate=' + toDate + '&malh='+$('#txtMaLopHoc').val()+'&mact=' + $('#txtMaCongTy').val() + '&mahv=' + $('#txtMaHocVien').val();
    }

    function CallExportPage() {
        //var url = '/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet&nam=' + $('#ddlYear option:selected').val() + '&tungay=' + $('#txtTuNgay').val() + '&denngay=' + $('#txtDenNgay').val() + '&idct=' + $('#hdCongTyID').val() + '&mact=' + $('#txtMaCongTy').val();
        //window.open(url, '_blank');
        $('.tableFloatingHeader').html('');
        var html = $('#tblDanhSachKetQuaTongHop').html();
        //alert(html);
        html = html.replace('<thead style="display: none;" class="tableFloatingHeader"><tr><th rowspan="2" style="width: 30px;">STT</th><th rowspan="2" style="width: 100px;">Mã</th><th rowspan="2" style="min-width: 250px;">Tên công ty/Học viên</th><th rowspan="2" style="width: 100px;">Số chứng chỉ KTV</th><th rowspan="2" style="width: 80px;">Ngày cấp chứng chỉ KTV</th><th colspan="3">Tổng số giờ CNKT đã tham dự</th><th colspan="3">Tổng số giờ CNKT còn thiếu</th></tr><tr><th style="width: 50px;">KT, KiT</th><th style="width: 50px;">ĐĐ</th><th style="width: 50px;">Khác</th><th style="width: 50px;">KT, KiT</th><th style="width: 50px;">ĐĐ</th><th style="width: 50px;">Khác</th></tr></thead>', '');
        var exportContent = "";
        exportContent += "<table border='1'>" + html + "</table>";
        window.iframeProcess_Export.AddContent(exportContent, 'Excel', 'TongHopSoGioCNKTCuaHocVien'); // Gọi trang xử lý kết xuất dữ liệu
    }
    
    <% FirstLoad(); CheckPermissionOnPage(); %>
</script>
<div id="DivDanhSachLopHoc">
</div>
<div id="DivDanhSachCongTy">
</div>
<div id="DivDanhSachHocVien">
</div>
