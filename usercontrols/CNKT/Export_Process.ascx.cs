﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

public partial class usercontrols_Export_Process : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string content = Request.Form["hdContent"];
        string typeExport = Request.Form["hdTypeExport"];
        string fileName = Request.Form["hdFileName"];
        if (!string.IsNullOrEmpty(content))
        {
            if (typeExport.ToLower() == "excel")
            {
                Library.ExportToExcel(fileName, content);
            }
            if (typeExport.ToLower() == "word")
            {
                Library.ExportToWord(fileName, content);
            }
            if(typeExport.ToLower() == "pdf")
            {
                ExportToPdf(fileName, content);
            }
        }
    }

    public static void ExportToPdf(string fileName, string htmlData)
    {
        string strFileName = fileName + ".pdf";
        HttpContext.Current.Response.ContentType = "application/pdf";
        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", strFileName));
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        string strHTMLContent = htmlData;
        hw.Write(strHTMLContent);

        StringReader sr = new StringReader(sw.ToString());
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream);
        pdfDoc.Open();
        FontFactory.Register(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "Tahoma.TTF"),  "Tahoma"); 
        StyleSheet css = new StyleSheet();
        css.LoadTagStyle("body", "face", "Tahoma");
        css.LoadTagStyle("body", "encoding", "Identity-H");
        //css.LoadTagStyle("body", "size", "11pt");
        Dictionary<string, string> dic = new Dictionary<string, string>();
        //dic.Add("border-collapse", "collapse");
        //dic.Add("border", "solid #ddd 1px");
        //css.LoadStyle("formtbl", dic);
        css.LoadStyle("formtbl", "border", "1px");
        htmlparser.SetStyleSheet(css);
        htmlparser.Parse(sr);
        pdfDoc.Close();
        HttpContext.Current.Response.Write(pdfDoc);
        HttpContext.Current.Response.End();
        HttpContext.Current.Response.Flush();
    }
}