﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodeEngine.Framework.QueryBuilder.Enums;
using HiPT.VACPA.DL;

using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_CNKT_QuanLyLopHoc : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý lớp học";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if(Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            if (Request.QueryString["act"] == "delete")
            {

                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr).Contains("XOA|"))
                {
                    string idTrangThaiPheDuyet = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
                    string msgError = "";
                    string[] arrIDDelete = Request.QueryString["id"].Split(',');
                    foreach (string id in arrIDDelete)
                    {
                        if (Library.CheckIsInt32(id))
                        {
                            string error = DeleteLopHoc(id, idTrangThaiPheDuyet);
                            if(!string.IsNullOrEmpty(error))
                                msgError += (!string.IsNullOrEmpty(msgError) ? "<br />" : "") + error;
                        }
                    }
                    if (!string.IsNullOrEmpty(msgError))
                        Session["MsgError"] = msgError;
                    Response.Redirect("admin.aspx?page=cnkt_quanlylophoc");
                }
            }
            if (_listPermissionOnFunc_QuanLyLopHoc.Contains("XEM|"))
                LoadListClass();
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu lớp đào tạo, cập nhật kiến thức!</div>"));
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// create by HUNGNM - 2014/11/18
    /// Load list class from db
    /// </summary>
    private void LoadListClass()
    {
        // use DataTable to contain list value from Database
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "LopHocID", "MaLopHoc", "TuNgay", "DenNgay", "SoBuoi", "TenGiangVien", "TenTrangThai", "MaTrangThai" });

        //SelectQueryBuilder query = new SelectQueryBuilder();
        //query.SelectFromTable(ListName.Table_CNKTLopHoc);
        //query.AddJoin(JoinType.LeftJoin, ListName.Table_CNKTLopHocChuyenDe, "LopHocID", Comparison.Equals, ListName.Table_CNKTLopHoc, "LopHocID");
        //query.AddJoin(JoinType.LeftJoin, ListName.Table_DMGiangVien, "GiangVienID", Comparison.Equals, ListName.Table_CNKTLopHocChuyenDe, "GiangVienID");
        //query.SelectColumns(ListName.Table_CNKTLopHoc + ".LopHocID", ListName.Table_CNKTLopHoc + ".MaLopHoc", ListName.Table_CNKTLopHoc + ".TuNgay",
        //    ListName.Table_CNKTLopHoc + ".DenNgay", ListName.Table_CNKTLopHocChuyenDe + ".SoBuoi as SoBuoi", ListName.Table_DMGiangVien + ".TenGiangVien as TenGiangVien");

        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                con.Open();
                SqlCommand sql = GetSqlCommandLoadListClass(con);
                SqlDataReader dr = sql.ExecuteReader();
                if (dr.HasRows)
                {
                    string temp_MaLopHoc = "";
                    DataRow dataRow;
                    int index = 1;
                    while (dr.Read())
                    {
                        string maLopHoc = dr["LopHocID"].ToString();
                        if (temp_MaLopHoc == maLopHoc && !string.IsNullOrEmpty(temp_MaLopHoc))
                        {
                            dt.Rows[dt.Rows.Count - 1]["SoBuoi"] = Library.ChangeFormatNumber(Library.DoubleConvert(Library.ChangeFormatNumber(dt.Rows[dt.Rows.Count - 1]["SoBuoi"], "us")) + Library.DoubleConvert(dr["SoBuoi"]), "vn");
                            dt.Rows[dt.Rows.Count - 1]["TenGiangVien"] += "<br />" + dr["TenGiangVien"];
                        }
                        if (temp_MaLopHoc != maLopHoc)
                        {
                            temp_MaLopHoc = maLopHoc;
                            dataRow = dt.NewRow();
                            dataRow["STT"] = index;
                            dataRow["LopHocID"] = dr["LopHocID"];
                            dataRow["MaLopHoc"] = dr["MaLopHoc"];
                            dataRow["TuNgay"] = dr["TuNgay"];
                            dataRow["DenNgay"] = dr["DenNgay"];
                            dataRow["SoBuoi"] = Library.ChangeFormatNumber(Library.DoubleConvert(dr["SoBuoi"]), "vn");
                            dataRow["TenGiangVien"] = dr["TenGiangVien"];
                            dataRow["TenTrangThai"] = dr["TenTrangThai"];
                            dataRow["MaTrangThai"] = dr["MaTrangThai"].ToString().Trim();
                            dt.Rows.Add(dataRow);
                            index++;
                        }
                    }
                }

                DataView dv = dt.DefaultView;
                if (ViewState["sortexpression"] != null)
                    dv.Sort = ViewState["sortexpression"] + " " + ViewState["sortdirection"];
                // Phân trang
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = dv;
                objPds.AllowPaging = true;
                objPds.PageSize = 10;

                // danh sách trang
                Pager.Items.Clear();
                for (int i = 0; i < objPds.PageCount; i++)
                {
                    Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
                }
                // Chuyển trang
                if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
                {
                    objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                    Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                }
                gv_DanhSachLopHoc.DataSource = objPds;
                gv_DanhSachLopHoc.DataBind();
                con.Close();
            }
            catch (Exception)
            {
            }
            finally
            {
                con.Close();
            }
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/19
    /// Process and Create Sql command text
    /// </summary>
    /// <param name="con">SqlConnection already open</param>
    /// <returns>SqlCommand with Sql command text</returns>
    private SqlCommand GetSqlCommandLoadListClass(SqlConnection con)
    {
        string month = Request.Form["ddlThang"];
        string year = Request.Form["ddlNam"];
        string province = Request.Form["ddlProvince"];
        string classID = Request.Form["ddlClassID"];
        string txtFromDate = Request.Form["textFromDate"];
        string txtToDate = Request.Form["textToDate"];
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = con;
//        string command = @"SELECT tblCNKTLopHoc.LopHocID AS LopHocID , tblCNKTLopHoc.MaLopHoc , tblCNKTLopHoc.TuNgay, tblCNKTLopHoc.DenNgay,
//                                        tblCNKTLopHocChuyenDe.SoBuoi AS SoBuoi, tblDMGiangVien.TenGiangVien AS TenGiangVien, tblDMTrangThai.TenTrangThai, tblDMTrangThai.MaTrangThai
//                                        FROM tblCNKTLopHoc
//                                         LEFT JOIN tblCNKTLopHocChuyenDe ON tblCNKTLopHoc.LopHocID = tblCNKTLopHocChuyenDe.LopHocID
//                                         LEFT JOIN tblDMGiangVien ON tblCNKTLopHocChuyenDe.GiangVienID = tblDMGiangVien.GiangVienID
//                                         LEFT JOIN tblDMTrangThai ON tblCNKTLopHoc.TinhTrangID = tblDMTrangThai.TrangThaiID
//                                         WHERE (
//                                          (tblCNKTLopHoc.TuNgay >= @FromDate AND tblCNKTLopHoc.TuNgay <= @ToDate)
//                                          OR
//                                          (tblCNKTLopHoc.DenNgay >= @FromDate AND tblCNKTLopHoc.DenNgay <= @ToDate)) AND {ClassID} AND {Province} AND {Datetime1} AND {Datetime2}
//                                         ORDER BY tblCNKTLopHoc.HanDangKy DESC";

        // sonnq sửa, bỏ đk lọc theo năm
        string command = @"SELECT tblCNKTLopHoc.LopHocID AS LopHocID , tblCNKTLopHoc.MaLopHoc , tblCNKTLopHoc.TuNgay, tblCNKTLopHoc.DenNgay,
                                        tblCNKTLopHocChuyenDe.SoBuoi AS SoBuoi, tblDMGiangVien.TenGiangVien AS TenGiangVien, tblDMTrangThai.TenTrangThai, tblDMTrangThai.MaTrangThai
                                        FROM tblCNKTLopHoc
                                         LEFT JOIN tblCNKTLopHocChuyenDe ON tblCNKTLopHoc.LopHocID = tblCNKTLopHocChuyenDe.LopHocID
                                         LEFT JOIN tblDMGiangVien ON tblCNKTLopHocChuyenDe.GiangVienID = tblDMGiangVien.GiangVienID
                                         LEFT JOIN tblDMTrangThai ON tblCNKTLopHoc.TinhTrangID = tblDMTrangThai.TrangThaiID
                                         
                                         ORDER BY tblCNKTLopHoc.HanDangKy DESC";

        //DateTime fromDate = new DateTime(DateTime.Now.Year, 1, 1);
        //DateTime toDate = new DateTime(DateTime.Now.Year, 12, 31);
        //if (!string.IsNullOrEmpty(year))
        //{
        //    fromDate = new DateTime(Library.Int32Convert(year), 1, 1);
        //    toDate = new DateTime(Library.Int32Convert(year), 12, 31);
        //}
        //if (!string.IsNullOrEmpty(month))
        //{
        //    fromDate = new DateTime(Library.Int32Convert(year), Library.Int32Convert(month), 1);
        //    toDate = new DateTime(Library.Int32Convert(year), Library.Int32Convert(month), DateTime.DaysInMonth(Library.Int32Convert(year), Library.Int32Convert(month)));
        //}

        //if (!string.IsNullOrEmpty(classID))
        //    command = command.Replace("{ClassID}", "tblCNKTLopHoc.MaLopHoc = '" + classID + "'");
        //else
        //    command = command.Replace("{ClassID}", "1=1");

        //if (!string.IsNullOrEmpty(province))
        //    command = command.Replace("{Province}", "tblCNKTLopHoc.TinhThanhID = " + province);
        //else
        //    command = command.Replace("{Province}", "1=1");

        //if (!string.IsNullOrEmpty(txtFromDate))
        //{
        //    txtFromDate = Library.ConvertDatetime(txtFromDate, '/');
        //    command = command.Replace("{Datetime1}", "(tblCNKTLopHoc.TuNgay >= '" + txtFromDate + "' OR tblCNKTLopHoc.DenNgay >= '" + txtFromDate + "')");
        //}
        //else
        //    command = command.Replace("{Datetime1}", "1=1");
        //if (!string.IsNullOrEmpty(txtToDate))
        //{
        //    txtToDate = Library.ConvertDatetime(txtToDate, '/');
        //    command = command.Replace("{Datetime2}", "(tblCNKTLopHoc.TuNgay <= '" + txtToDate + "' OR tblCNKTLopHoc.DenNgay <= '" + txtToDate + "')");
        //}
        //else
        //    command = command.Replace("{Datetime2}", "1=1");

        sqlCommand.CommandText = command;
        //sqlCommand.Parameters.Add("FromDate", SqlDbType.Date).Value = fromDate;
        //sqlCommand.Parameters.Add("ToDate", SqlDbType.Date).Value = toDate;

        return sqlCommand;
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("SUA|"))
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("XOA|"))
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("THEM|"))
            Response.Write("$('#btn_them').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("XOA|"))
            Response.Write("$('#btn_xoa').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("KETXUAT|"))
            Response.Write("$('#btn_ketxuat').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("XEM|"))
            Response.Write("$('#btn_search').remove();");
    }

    protected void gv_DanhSachLopHoc_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_DanhSachLopHoc.PageIndex = e.NewPageIndex;
        gv_DanhSachLopHoc.DataBind();
    }

    protected void gv_DanhSachLopHoc_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        LoadListClass();
    }

    protected void gv_DanhSachLopHoc_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hfLopHocId = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("hfLopHocID") as HiddenField;
            HiddenField hfMaTrangThai = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("hfMaTrangThai") as HiddenField;
            LinkButton lbtDelete = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("lbtDelete") as LinkButton;
            HyperLink hplEdit = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("hplEdit") as HyperLink;
            lbtDelete.OnClientClick = "confirm_delete_QuanLyLopHoc(" + hfLopHocId.Value + "); return false;";
            hplEdit.NavigateUrl = "/admin.aspx?page=cnkt_quanlylophoc_add&id=" + hfLopHocId.Value;
            if (hfMaTrangThai != null)
            {
                if (hfMaTrangThai.Value == ListName.Status_DaPheDuyet)
                {
                    lbtDelete.Visible = false;
                    hplEdit.Visible = false;
                }
                if (hfMaTrangThai.Value == ListName.Status_ThoaiDuyet)
                    lbtDelete.Visible = false;
            }
        }
    }

    private string DeleteLopHoc(string idLopHoc, string idTrangThaiDaPheDuyet)
    {
        SqlCnktLopHocProvider cnktLopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
        if (!Library.CheckIsInt32(idLopHoc))
            return "";

        // Lấy trạng thái để kiểm tra xem được xóa ko
        CnktLopHoc cnktLopHoc = cnktLopHocPro.GetByLopHocId(Library.Int32Convert(idLopHoc));
        if(cnktLopHoc != null && cnktLopHoc.LopHocId > 0)
        {
            if (cnktLopHoc.TinhTrangId.ToString() == idTrangThaiDaPheDuyet)
                return "Lớp học có mã: " + cnktLopHoc.MaLopHoc + " không thể xóa vì đã được phê duyệt!";
            if (cnktLopHocPro.Delete(Library.Int32Convert(idLopHoc)))
            {
                cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Xóa bản ghi có ID \"" + Request.QueryString["id"] + "\" của danh mục " + tenchucnang);
                string command = @"DELETE FROM tblCNKTLopHocChuyenDe WHERE LopHocID = " + idLopHoc;
                if (_db.ExecuteNonQuery(command) > 0)
                {
                    command = "DELETE FROM " + ListName.Table_CNKTLopHocBuoi + " WHERE LopHocID = " + idLopHoc;
                    _db.ExecuteNonQuery(command);
                }
                command = "DELETE FROM " + ListName.Table_CNKTLopHocPhi + " WHERE LopHocID = " + idLopHoc;
                _db.ExecuteNonQuery(command);
            }   
        }
        return "";
    }

    #region Gen control in "Search Form"

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get List of year from 2000 - 2030 to fill into dropdownlist search by Year.
    /// </summary>
    protected void LoadListYear()
    {
        string output_html = "";
        for (int i = 1940; i <= 2040; i++)
        {
            output_html += i == DateTime.Now.Year
                               ? "<option selected='selected' value='" + i + "'>" + i + "</option>"
                               : "<option value='" + i + "'>" + i + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get list of month to fill into dropdownlist search by Month
    /// </summary>
    protected void LoadListMonth()
    {
        string output_html = "";
        output_html += "<option value=''>Tất cả</option>";
        for (int i = 1; i <= 12; i++)
        {
            output_html += "<option value='" + i + "'>" + i + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get list of Class ID already exists in Database
    /// </summary>
    protected void LoadListClassID()
    {
        string output_html = "";
        output_html += "<option value=''>Tất cả</option>";
        SqlCnktLopHocProvider cnktLopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, "");
        TList<CnktLopHoc> list = cnktLopHocPro.GetAll();
        foreach (CnktLopHoc item in list)
        {
            output_html += "<option value='" + item.MaLopHoc + "'>" + item.MaLopHoc + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get list of Province 
    /// </summary>
    protected void LoadProvince()
    {
        string output_html = "";
        output_html += "<option value=''>Tất cả</option>";
        SqlDmTinhProvider dmTinhPro = new SqlDmTinhProvider(ListName.ConnectionString, false, "");
        TList<DmTinh> list = dmTinhPro.GetAll();
        foreach (DmTinh item in list)
        {
            output_html += "<option value='" + item.TinhId + "'>" + item.TenTinh + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }

    #endregion
}