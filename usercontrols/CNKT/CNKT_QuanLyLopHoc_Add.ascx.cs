﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_CNKT_QuanLyLopHoc_Add : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý lớp học";
    protected int _rowCount_PhiThanhToan = 0; // Dung de ghi lai so dong  cua bang "Phi thanh toan"
    protected string _listPermissionOnFunc_QuanLyLopHoc = "", _listPermissionOnFunc_ChuyenDe = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected string _IDLopHoc = "";
    Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1><a href='admin.aspx?page=cnkt_quanlylophoc' class='MenuFuncLv1'>" + tenchucnang + @"</a>&nbsp;<img src='/images/next.png' style='margin-top:3px; height: 18px;' />&nbsp;<span class='MenuFuncLv2'>Tạo lớp đào tạo, cập nhật kiến thức</span></h1> </div>"));
            _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
            _listPermissionOnFunc_ChuyenDe = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyChuyenDe, cm.connstr);

            if (Session["MsgError"] != null)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
                Session.Remove("MsgError");
            }
            if (Session["MsgSuccess"] != null)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + Session["MsgSuccess"] + "</div>"));
                Session.Remove("MsgSuccess");
            }

            if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("XEM|"))
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu lớp đào tạo, cập nhật kiến thức!</div>"));
                form_CreateLopHoc.Visible = false;
                return;
            }
            _IDLopHoc = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(_IDLopHoc))
            {
                SqlCnktLopHocProvider cnktLopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
                CnktLopHoc cnktLopHoc = cnktLopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
                //if (cnktLopHoc != null)
                //{
                //    lbtExport.NavigateUrl = "/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc&idlh=" + this._IDLopHoc + "&malh=" + cnktLopHoc.MaLopHoc;
                //    lbtExport.Visible = true;
                //}
            }

            if (Request.Form["hdAction"] == "save")
                SaveLopHoc();
            if (Request.Form["hdAction"] == "delete")
                DeleteLopHoc();
            if (Request.Form["hdAction"] == "sendemail")
                SendEmail();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("SUA|"))
            Response.Write("$('#btn_save').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("THEM|"))
            Response.Write("$('#btn_save').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("KETXUAT|"))
            Response.Write("$('#btnExport').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains(ListName.PERMISSION_Duyet))
            Response.Write("$('#" + lbtDuyet.ClientID + "').remove();");

        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains(ListName.PERMISSION_TuChoi))
            Response.Write("$('#" + lbtTuChoi.ClientID + "').remove();");

        if (!_listPermissionOnFunc_ChuyenDe.Contains("XEM|"))
            Response.Write("$('#DivChuyenDe').remove();");

        if (!_listPermissionOnFunc_ChuyenDe.Contains("THEM|") && !_listPermissionOnFunc_ChuyenDe.Contains("SUA|"))
            Response.Write("$('#btnSaveChuyenDe').remove();");

        if (!_listPermissionOnFunc_ChuyenDe.Contains("XOA|"))
            Response.Write("$('#btnDeleteChuyenDe').remove();");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/20
    /// Load du lieu cu cua ban ghi
    /// </summary>
    protected void LoadOldData()
    {
        string maTrangThai = "";
        string js = "";
        if (!string.IsNullOrEmpty(_IDLopHoc))
        {
            // Lay du lieu theo ID ban ghi
            SqlCnktLopHocProvider cnktLopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc cnktLopHoc = cnktLopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
            if (cnktLopHoc != null)
            {
                js = "var _lopHocID=" + this._IDLopHoc + ";" + Environment.NewLine;
                js += "iframeProcessChuyenDe.location = '/admin.aspx?page=cnkt_quanlylophoc_process&lophocid=" + this._IDLopHoc + "'" + Environment.NewLine;
                if (cnktLopHoc.Loai == "CNKT")
                    js += "$('#rbLoaiLopHoc_CNKT').attr('checked','checked');" + Environment.NewLine;
                else
                    js += "$('#rbLoaiLopHoc_DT').attr('checked','checked');" + Environment.NewLine;
                js += "$('#rbLoaiLopHoc_CNKT').prop('readonly', true);" + Environment.NewLine;
                js += "$('#rbLoaiLopHoc_DT').prop('readonly', true);" + Environment.NewLine;
                js += "$('#txtMaLopHoc').val('" + cnktLopHoc.MaLopHoc + "');" + Environment.NewLine;
                js += "$('#txtNgayTao').val('" + cnktLopHoc.NgayTao.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                js += "$('#txtNgayTao').prop('readonly', true);" + Environment.NewLine;
                js += "$('#txtTenLopHoc').val('" + cnktLopHoc.TenLopHoc + "');" + Environment.NewLine;
                js += "$('#txtTuNgay').val('" + cnktLopHoc.TuNgay.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                js += "$('#txtDenNgay').val('" + cnktLopHoc.DenNgay.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                js += "$('#txtHanDangKy').val('" + cnktLopHoc.HanDangKy.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                js += "$('#txtDiaChiLopHoc').val('" + cnktLopHoc.DiaChiLopHoc + "');" + Environment.NewLine;

                if (Library.CheckIsInt32(cnktLopHoc.TinhThanhId))
                {
                    SqlDmTinhProvider dmTinhPro = new SqlDmTinhProvider(ListName.ConnectionString, false, string.Empty);
                    DmTinh dmTinh = dmTinhPro.GetByTinhId(Library.Int32Convert(cnktLopHoc.TinhThanhId));
                    if (dmTinh != null)
                        js += "$('#ddlTinhThanh').val('" + dmTinh.MaTinh.Trim() + "');" + Environment.NewLine;
                }
                js += "$('#ddlTinhThanh').prop('readonly', true);" + Environment.NewLine;

                SqlDmTrangThaiProvider trangThaiPro = new SqlDmTrangThaiProvider(ListName.ConnectionString, false, string.Empty);
                DmTrangThai trangThai = trangThaiPro.GetByTrangThaiId(cnktLopHoc.TinhTrangId.Value);
                if (trangThai != null && trangThai.TrangThaiId > 0)
                {
                    maTrangThai = trangThai.MaTrangThai.Trim();
                    if (trangThai.MaTrangThai.Trim() == ListName.Status_ChoPheDuyetLai || trangThai.MaTrangThai.Trim() == ListName.Status_ThoaiDuyet)
                    {
                        js += "$('#btnDelete').remove();" + Environment.NewLine;
                        js += "$('#" + lbtTuChoi.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#btnDeleteChuyenDe').remove();" + Environment.NewLine;

                        // Khoá những trường thông tin ko được phép sửa
                        // sonnq sửa lại
                        //js += "$('#txtTuNgay').prop('readonly', true);" + Environment.NewLine;
                        //js += "$('#txtDenNgay').prop('readonly', true);" + Environment.NewLine;
                        //js += "$('#txtTenLopHoc').prop('readonly', true);" + Environment.NewLine;
                        //js += "$('#ddlTinhThanh').prop('readonly', true);" + Environment.NewLine;
                        //js += "$('#ddlLoaiChuyenDe').prop('readonly', true);" + Environment.NewLine;
                        //js += "$('#txtTuNgay_CD').prop('readonly', true);" + Environment.NewLine;
                        //js += "$('#txtDenNgay_CD').prop('readonly', true);" + Environment.NewLine;
                        //js += "$('#txtSoNguoiHocToiDa').prop('readonly', true);" + Environment.NewLine;
                        js += "$('#hdIsThoaiDuyet').val('1');" + Environment.NewLine;
                    }
                    if (trangThai.MaTrangThai.Trim() == ListName.Status_KhongPheDuyet || trangThai.MaTrangThai.Trim() == ListName.Status_ThoaiDuyet)
                    {
                        js += "$('#" + lbtDuyet.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + lbtTuChoi.ClientID + "').remove();" + Environment.NewLine;
                    }
                    if (trangThai.MaTrangThai.Trim() == ListName.Status_TuChoi)
                    {
                        js += "$('#" + lbtDuyet.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + lbtTuChoi.ClientID + "').remove();" + Environment.NewLine;
                    }
                    if (trangThai.MaTrangThai.Trim() == ListName.Status_DaPheDuyet)
                    {
                        js += "$('#btn_save').remove();" + Environment.NewLine;
                        js += "$('#btnDelete').remove();" + Environment.NewLine;
                        js += "$('#" + lbtDuyet.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + lbtTuChoi.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#btnSaveChuyenDe').remove();" + Environment.NewLine;
                        js += "$('#btnDeleteChuyenDe').remove();" + Environment.NewLine;
                    }
                    else
                    {
                        js += "$('#btnSendEmail').remove();" + Environment.NewLine;
                    }
                }

                Response.Write(js);
                GetPhiThanhToan(cnktLopHoc.TuNgay.Value, cnktLopHoc.DenNgay.Value, maTrangThai);
            }
        }
        else
        {
            js = "$('#txtNgayTao').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
            js += "$('#txtTuNgay').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
            js += "$('#txtDenNgay').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
            js += "ValidateBangThanhToanPhiTheoThoiGianLopHoc();" + Environment.NewLine;
            js += "$('#spanCacNutChucNang').remove();" + Environment.NewLine;
            Response.Write(js);
        }

        // Load tham so "So nguoi hoc toi da moi chuyen de" dua vao o text chua gia tri so nguoi hoc toi da
        try
        {
            _db.OpenConnection();
            string giaTriThamSo = utility.GetParamByCode(ListName.Param_SoNguoiHocToiDa_CD, _db);
            js = "var soNguoiHocToiDa = " + giaTriThamSo + ";" + Environment.NewLine;
            js += "$('#txtSoNguoiHocToiDa').val('" + giaTriThamSo + "');" + Environment.NewLine;
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }

    private void GetPhiThanhToan(DateTime fromDate, DateTime toDate, string maTrangThai)
    {
        Hashtable ht = new Hashtable();
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                con.Open();
                string command = "SELECT SoNgay, SoTienPhiHVCNCT, SoTienPhiHVCNLK, SoTienPhiHVCNDD, SoTienPhiKTV, SoTienPhiNQT_TroLyKTV, SoTienPhiNTQ FROM tblCNKTLopHocPhi WHERE LopHocID = @LopHocID ORDER BY SoNgay";
                SqlCommand cmd = new SqlCommand(command, con);
                cmd.Parameters.Add("@LopHocID", SqlDbType.Int).Value = this._IDLopHoc;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string key = Library.DoubleConvert(dr["SoNgay"]).ToString();
                        List<string> value = new List<string> { Library.FormatMoney(dr["SoTienPhiHVCNCT"]), Library.FormatMoney(dr["SoTienPhiHVCNLK"]), Library.FormatMoney(dr["SoTienPhiHVCNDD"]), Library.FormatMoney(dr["SoTienPhiKTV"]), Library.FormatMoney(dr["SoTienPhiNQT_TroLyKTV"]), Library.FormatMoney(dr["SoTienPhiNTQ"]) };
                        if (!ht.ContainsKey(key))
                            ht.Add(key, value);
                    }
                }
                cmd.Cancel();
                dr.Close();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        double countDays = (toDate - fromDate).TotalDays + 1;
        string js = "";
        js += "_arrDataPhiThanhToan = [];" + Environment.NewLine;
        int index = 0;
        for (double i = 0.5; i <= countDays; i = i + 0.5)
        {
            List<string> listData = Library.CheckKeyInHashtable(ht, i.ToString()) as List<string>;
            if (listData == null)
                listData = new List<string> { "", "", "", "" };
            js += @"_arrDataPhiThanhToan[" + index + "] = ['" + i + "', '" + listData[0] + "','" + listData[1] + "','" + listData[2] + "'," + " '" + listData[3] + "', '" + listData[4] + "'," + "'" + listData[5] + "'];" + Environment.NewLine;
            index++;
        }
        string flagAllowEdit = "1"; // Đặt readonly cho các ô text nếu trạng thái = thoái duyệt, chờ phê duyệt lại
        if (maTrangThai == ListName.Status_ThoaiDuyet || maTrangThai == ListName.Status_ChoPheDuyetLai)
            flagAllowEdit = "0";
        js += "DrawRow_PhiThanhToan(_arrDataPhiThanhToan, " + flagAllowEdit + ");";
        this._rowCount_PhiThanhToan = index;
        Response.Write(js);
    }

    private void SaveLopHoc()
    {
        string loaiLopHoc = Request.Form["rbLoaiLopHoc"];
        if (!string.IsNullOrEmpty(loaiLopHoc))
        {
            SqlCnktLopHocProvider cnktLopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc cnktLopHoc = new CnktLopHoc();
            cnktLopHoc.Loai = loaiLopHoc;
            DateTime ngayTao = Library.DateTimeConvert(Request.Form["txtNgayTao"], '/', 1);
            string tenLopHoc = Request.Form["txtTenLopHoc"];
            DateTime tuNgay = Library.DateTimeConvert(Request.Form["txtTuNgay"], '/', 1);
            DateTime denNgay = Library.DateTimeConvert(Request.Form["txtDenNgay"], '/', 1);
            DateTime hanDangKy = Library.DateTimeConvert(Request.Form["txtHanDangKy"], '/', 1);
            string tinhThanh = GetIDTinhThanhByMa(Request.Form["ddlTinhThanh"]);
            string diaChiLopHoc = Request.Form["txtDiaChiLopHoc"];

            if (!string.IsNullOrEmpty(_IDLopHoc)) // Update
            {
                if (Library.CheckIsInt32(this._IDLopHoc))
                    cnktLopHoc = cnktLopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
            }
            else // Insert
            {
                // Gen Ma Lop Hoc
                // QUY TAC:
                // Lop CNKT: [0-50]+[A(Ha Noi, Da Nang) hoac B(TP Ho Chi Minh)]+[2 so cuoi cua nam tao lop hoc]
                // Lop DT: [51-99]+[A(Ha Noi, Da Nang) hoac B(TP Ho Chi Minh)]+[2 so cuoi cua nam tao lop hoc]
                //string maLopHoc = GetSTT(loaiLopHoc, ngayTao);
                //maLopHoc += Request.Form["ddlTinhThanh"] == "01" || Request.Form["ddlTinhThanh"] == "48" ? "A" : "B";
                //maLopHoc += ngayTao.ToString("yy");
                //cnktLopHoc.Loai = loaiLopHoc;
                //cnktLopHoc.MaLopHoc = maLopHoc;

                // sonnq sửa lại, tự nhập mã lớp học chứ không tự sinh

                if (ngayTao != new DateTime(1, 1, 1))
                    cnktLopHoc.NgayTao = ngayTao;

            }
            if (Library.CheckIsInt32(tinhThanh))
                cnktLopHoc.TinhThanhId = Library.Int32Convert(tinhThanh);
            cnktLopHoc.MaLopHoc = Request.Form["txtMaLopHoc"];
            cnktLopHoc.TenLopHoc = tenLopHoc;
            if (tuNgay != new DateTime(1, 1, 1))
                cnktLopHoc.TuNgay = tuNgay;
            if (denNgay != new DateTime(1, 1, 1))
                cnktLopHoc.DenNgay = denNgay;
            if (hanDangKy != new DateTime(1, 1, 1))
                cnktLopHoc.HanDangKy = hanDangKy;
            cnktLopHoc.DiaChiLopHoc = diaChiLopHoc;
            if (Request.Form["hdIsThoaiDuyet"] == "1")
                cnktLopHoc.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ChoPheDuyetLai, _db));
            else
                cnktLopHoc.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ChoDuyet, _db));
            if (!string.IsNullOrEmpty(_IDLopHoc)) // Update
            {
                if (cnktLopHocPro.Update(cnktLopHoc))
                {
                    SavePhiThanhToan(cnktLopHoc.LopHocId.ToString(), tuNgay, denNgay);
                    SaveListChuyenDe(cnktLopHoc.LopHocId.ToString(), cnktLopHoc.MaLopHoc);
                    cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Cập nhật giá trị \"" + tenLopHoc + "\" vào danh mục " + tenchucnang);
                    Session["MsgSuccess"] = "Cập nhật thông tin thành công!";
                    Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + cnktLopHoc.LopHocId);
                }
            }
            else // Insert
            {
                if (cnktLopHocPro.Insert(cnktLopHoc))
                {
                    SavePhiThanhToan(cnktLopHoc.LopHocId.ToString(), tuNgay, denNgay);
                    SaveListChuyenDe(cnktLopHoc.LopHocId.ToString(), cnktLopHoc.MaLopHoc);
                    cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Thêm giá trị \"" + tenLopHoc + "\" vào danh mục " + tenchucnang);
                    Session["MsgSuccess"] = "Cập nhật thông tin thành công!";
                    Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + cnktLopHoc.LopHocId);
                }
            }
        }
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/20
    /// Luu du lieu Phi Thanh Toan (Luu y: day chi la cach tam thoi. Can xem xet lai lam cach khac nhanh hon)
    /// </summary>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    private void SavePhiThanhToan(string idLopHoc, DateTime fromDate, DateTime toDate)
    {
        // Xoa du lieu cu
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                con.Open();
                string command = "DELETE FROM tblCNKTLopHocPhi WHERE LopHocID = @LopHocID";
                SqlCommand cmd = new SqlCommand(command, con);
                cmd.Parameters.Add("@LopHocID", SqlDbType.Int).Value = idLopHoc;
                cmd.ExecuteNonQuery();
                cmd.Cancel();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        SqlCnktLopHocPhiProvider cnktLopHocPhiPro = new SqlCnktLopHocPhiProvider(ListName.ConnectionString, false, string.Empty);
        double countDays = (toDate - fromDate).TotalDays + 1;
        int index = 0;
        for (double i = 0.5; i <= countDays; i = i + 0.5)
        {
            string hoiVienCaNhanCT = Request.Form["txtHoiVienCaNhanCT" + index];
            string hoiVienCaNhanLK = Request.Form["txtHoiVienCaNhanLK" + index];
            string hoiVienCaNhanDD = Request.Form["txtHoiVienCaNhanDD" + index];
            string ktv = Request.Form["txtKTV" + index];
            string troLyKtv = Request.Form["txtTroLyKTV" + index];
            string koPhaiTroLyKtv = Request.Form["txtKoPhaiTroLyKTV" + index];
            CnktLopHocPhi cnktLopHocPhi = new CnktLopHocPhi();
            cnktLopHocPhi.SoNgay = Convert.ToDecimal(i);
            if (!string.IsNullOrEmpty(hoiVienCaNhanCT))
                cnktLopHocPhi.SoTienPhiHvcnct = Library.DecimalConvert(hoiVienCaNhanCT);
            if (!string.IsNullOrEmpty(hoiVienCaNhanLK))
                cnktLopHocPhi.SoTienPhiHvcnlk = Library.DecimalConvert(hoiVienCaNhanLK);
            if (!string.IsNullOrEmpty(hoiVienCaNhanDD))
                cnktLopHocPhi.SoTienPhiHvcndd = Library.DecimalConvert(hoiVienCaNhanDD);
            if (!string.IsNullOrEmpty(ktv))
                cnktLopHocPhi.SoTienPhiKtv = Library.DecimalConvert(ktv);
            if (!string.IsNullOrEmpty(troLyKtv))
                cnktLopHocPhi.SoTienPhiNqtTroLyKtv = Library.DecimalConvert(troLyKtv);
            if (!string.IsNullOrEmpty(koPhaiTroLyKtv))
                cnktLopHocPhi.SoTienPhiNtq = Library.DecimalConvert(koPhaiTroLyKtv);
            //.Replace(",", "")
            cnktLopHocPhi.LopHocId = Library.Int32Convert(idLopHoc);

            // Một trong các cột có số liệu thì mới Insert vào DB
            if (!string.IsNullOrEmpty(hoiVienCaNhanCT) || !string.IsNullOrEmpty(hoiVienCaNhanLK) || !string.IsNullOrEmpty(hoiVienCaNhanDD) || !string.IsNullOrEmpty(ktv) || !string.IsNullOrEmpty(troLyKtv) || !string.IsNullOrEmpty(koPhaiTroLyKtv))
                cnktLopHocPhiPro.Insert(cnktLopHocPhi);
            index++;
        }
    }

    private void SaveListChuyenDe(string idLopHoc, string maLopHoc)
    {
        // Lấy danh sách ID chuyên đề trong lớp học -> để so sánh với danh sách ID khi update tìm ra những ID bị xóa
        List<string> listIdDangLuu = new List<string>(); // Danh sách những ID chuyên đề sẽ bị xóa sau khi kết thúc tác vụ
        string query = "SELECT ChuyenDeID FROM " + ListName.Table_CNKTLopHocChuyenDe + " WHERE LopHocID = " + idLopHoc;
        List<Hashtable> listData = _db.GetListData(query);
        foreach (Hashtable ht in listData)
        {
            listIdDangLuu.Add(ht["ChuyenDeID"].ToString());
        }

        Commons cm = new Commons();
        SqlCnktLopHocChuyenDeProvider cnktLopHocChuyenDePro = new SqlCnktLopHocChuyenDeProvider(ListName.ConnectionString, false, string.Empty);
        CnktLopHocChuyenDe cnktLopHocChuyenDe = new CnktLopHocChuyenDe();

        NameValueCollection nvc = Request.Form;
        string[] arrKeys = nvc.AllKeys;
        for (int i = 0; i < arrKeys.Length; i++)
        {
            string key = arrKeys[i];
            if (key.StartsWith("hfValueChuyenDe"))
            {
                string fullValue = Request.Form[key];
                if (string.IsNullOrEmpty(fullValue))
                    continue;
                string[] arrFullValue = fullValue.Split(new string[] { ";#" }, StringSplitOptions.None);

                string chuyenDeId = Library.GetValueFromArray(arrFullValue, 0);
                string loaiChuyenDe = Library.GetValueFromArray(arrFullValue, 1);
                string tenChuyenDe = Library.GetValueFromArray(arrFullValue, 2);

                DateTime tuNgay = Library.DateTimeConvert(Library.GetValueFromArray(arrFullValue, 3), '/', 1);
                DateTime denNgay = Library.DateTimeConvert(Library.GetValueFromArray(arrFullValue, 4), '/', 1);
                string soBuoi = Library.GetValueFromArray(arrFullValue, 5);
                string giangVienID = Library.GetValueFromArray(arrFullValue, 6);
                string soHocVienToiDa = Library.GetValueFromArray(arrFullValue, 7);
                string fullLichHoc = Request.Form["hfValueLichHoc" + key.Split(new string[] { "hfValueChuyenDe" }, StringSplitOptions.None)[1]];

                if (!string.IsNullOrEmpty(loaiChuyenDe))
                {
                    if (!string.IsNullOrEmpty(chuyenDeId)) // Update
                    {
                        if (Library.CheckIsInt32(chuyenDeId.Split(',')[0]))
                        {
                            listIdDangLuu.Remove(chuyenDeId.Split(',')[0]); // Loại bỏ những ID chuyên đề không bị xóa khỏi danh sách
                            cnktLopHocChuyenDe = cnktLopHocChuyenDePro.GetByChuyenDeId(Library.Int32Convert(chuyenDeId.Split(',')[0]));
                        }
                    }
                    else //Insert
                    {
                        cnktLopHocChuyenDe = new CnktLopHocChuyenDe();
                        // Gen Ma Chuyen De
                        // QUY TAC: [Ma lop hoc] + [Loai chuyen de (KT, DD, KH)] + [01 - 99]
                        string maChuyenDe = maLopHoc;
                        switch (loaiChuyenDe)
                        {
                            case ListName.Type_LoaiChuyenDe_KeToanKiemToan:
                                maChuyenDe += "KT";
                                break;
                            case ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep:
                                maChuyenDe += "DD";
                                break;
                            case ListName.Type_LoaiChuyenDe_ChuyenDeKhac:
                                maChuyenDe += "KH";
                                break;
                        }
                        maChuyenDe += GetSTTChuyenDe(idLopHoc);
                        cnktLopHocChuyenDe.MaChuyenDe = maChuyenDe;
                        cnktLopHocChuyenDe.LoaiChuyenDe = loaiChuyenDe;
                        if (Library.CheckIsInt32(idLopHoc))
                            cnktLopHocChuyenDe.LopHocId = Library.Int32Convert(idLopHoc);
                    }

                    cnktLopHocChuyenDe.LoaiChuyenDe = loaiChuyenDe;
                    cnktLopHocChuyenDe.TenChuyenDe = tenChuyenDe;
                    if (tuNgay != new DateTime(1, 1, 1))
                        cnktLopHocChuyenDe.TuNgay = tuNgay;
                    if (denNgay != new DateTime(1, 1, 1))
                        cnktLopHocChuyenDe.DenNgay = denNgay;
                    cnktLopHocChuyenDe.SoBuoi = Library.DecimalConvert(soBuoi);
                    if (Library.CheckIsInt32(giangVienID))
                        cnktLopHocChuyenDe.GiangVienId = Library.Int32Convert(giangVienID);
                    cnktLopHocChuyenDe.SoNguoiHocToiDa = Library.Int32Convert(soHocVienToiDa);
                    if (string.IsNullOrEmpty(chuyenDeId)) //Insert
                    {
                        if (cnktLopHocChuyenDePro.Insert(cnktLopHocChuyenDe))
                        {
                            SaveLichHoc(idLopHoc, cnktLopHocChuyenDe.ChuyenDeId.ToString(), fullLichHoc);
                            cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Thêm giá trị \"" + tenChuyenDe + "\" vào danh mục chuyên đề lớp học");
                        }
                    }
                    else //Update
                    {
                        if (cnktLopHocChuyenDePro.Update(cnktLopHocChuyenDe))
                        {
                            SaveLichHoc(idLopHoc, cnktLopHocChuyenDe.ChuyenDeId.ToString(), fullLichHoc);
                            cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Cập nhật giá trị \"" + tenChuyenDe + "\" vào danh mục chuyên đề lớp học");
                        }
                    }
                }
            }
        }

        // Xóa những chuyên đề theo list ID bị xóa sau khi update
        foreach (string id in listIdDangLuu)
        {
            if (cnktLopHocChuyenDePro.Delete(Library.Int32Convert(id)))
            {
                // Xóa hết lịch học trong chuyên đề
                string command = "DELETE FROM " + ListName.Table_CNKTLopHocBuoi + " WHERE ChuyenDeID = " + id;
                _db.ExecuteNonQuery(command);

                cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Xóa bản ghi có ID \"" + id + "\" của danh mục chuyên đề lớp học");
            }
        }
    }

    private string GetSTT(string loaiLopHoc, DateTime ngayTao)
    {
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                string maLopHoc = "";
                con.Open();
                string command = @"SELECT TOP 1 MaLopHoc FROM tblCNKTLopHoc WHERE
                                        (Convert(nvarchar(max),Loai) = @LoaiLopHoc AND (NgayTao >= @FromDate AND NgayTao <= @ToDate))
                                        ORDER BY LopHocID DESC";
                SqlCommand cmd = new SqlCommand(command, con);
                cmd.Parameters.Add("@LoaiLopHoc", SqlDbType.NVarChar).Value = loaiLopHoc;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = new DateTime(ngayTao.Year, 1, 1);
                cmd.Parameters.Add("@ToDate", SqlDbType.Date).Value = new DateTime(ngayTao.Year, 12, 31);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        maLopHoc = dr["MaLopHoc"].ToString();
                    }
                }
                cmd.Cancel();
                dr.Close();
                con.Close();
                if (string.IsNullOrEmpty(maLopHoc))
                    return loaiLopHoc == "CNKT" ? "01" : "51";
                string index = maLopHoc.Substring(0, 2);
                if (!Library.CheckIsInt32(index))
                    return loaiLopHoc == "CNKT" ? "01" : "51";
                int stt = Library.Int32Convert(index) + 1;
                if (stt < 10)
                    return "0" + stt.ToString();
                return stt.ToString();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
        return loaiLopHoc == "CNKT" ? "01" : "51";
    }

    private string GetSTTChuyenDe(string lopHocID)
    {
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                string maChuyenDe = "";
                con.Open();
                string command = @"SELECT TOP 1 MaChuyenDe FROM tblCNKTLopHocChuyenDe WHERE
                                        LopHocID = @LopHocID 
                                        ORDER BY ChuyenDeID DESC";
                SqlCommand cmd = new SqlCommand(command, con);
                cmd.Parameters.Add("@LopHocID", SqlDbType.NVarChar).Value = lopHocID;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        maChuyenDe = dr["MaChuyenDe"].ToString();
                    }
                }
                cmd.Cancel();
                dr.Close();
                con.Close();
                if (string.IsNullOrEmpty(maChuyenDe))
                    return "01";
                string index = maChuyenDe.Substring(maChuyenDe.Length - 2, 2);
                if (!Library.CheckIsInt32(index))
                    return "01";
                int stt = Library.Int32Convert(index) + 1;
                if (stt < 10)
                    return "0" + stt.ToString();
                return stt.ToString();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
        return "01";
    }

    private void SaveLichHoc(string idLopHoc, string idChuyenDe, string fullLichHoc)
    {
        // Xóa hết lịch học cũ
        string command = "DELETE FROM " + ListName.Table_CNKTLopHocBuoi + " WHERE ChuyenDeID = " + idChuyenDe;
        _db.ExecuteNonQuery(command);

        SqlCnktLopHocBuoiProvider cnktLopHocBuoiPro = new SqlCnktLopHocBuoiProvider(ListName.ConnectionString, false, string.Empty);
        string[] arrFullLichHoc = fullLichHoc.Split(new string[] { ";#" }, StringSplitOptions.None);
        for (var i = 0; i < arrFullLichHoc.Length; i = i + 3)
        {
            if (arrFullLichHoc[i].Length > 0)
            {
                CnktLopHocBuoi obj = new CnktLopHocBuoi();
                obj.LopHocId = Library.Int32Convert(idLopHoc);
                obj.ChuyenDeId = Library.Int32Convert(idChuyenDe);
                obj.Ngay = Library.DateTimeConvert(arrFullLichHoc[i], "dd/MM/yyyy");
                if (!string.IsNullOrEmpty(arrFullLichHoc[i + 1]))
                    obj.Sang = arrFullLichHoc[i + 1];
                if (!string.IsNullOrEmpty(arrFullLichHoc[i + 2]))
                    obj.Chieu = arrFullLichHoc[i + 2];
                cnktLopHocBuoiPro.Insert(obj);
            }
        }
    }

    private string GetIDTinhThanhByMa(string maTinh)
    {
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                string id = "";
                con.Open();
                string command = @"SELECT TOP 1 TinhID FROM tblDMTinh WHERE MaTinh = @MaTinh";
                SqlCommand cmd = new SqlCommand(command, con);
                cmd.Parameters.Add("@MaTinh", SqlDbType.NChar).Value = maTinh;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = dr["TinhID"].ToString();
                    }
                }
                dr.Close();
                cmd.Cancel();
                con.Close();
                return id;
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }

    protected void ValidateByJQuery(int rowCount)
    {
        string js = "";
        //for (int i = 0; i < rowCount; i++)
        //{
        //    js += "txtHoiVienCaNhan" + i + ": {required: true,digitsWithOutSeparatorCharacterVN: true}," + Environment.NewLine;
        //    js += "txtKTV" + i + ": {required: true,digitsWithOutSeparatorCharacterVN: true}," + Environment.NewLine;
        //    js += "txtTroLyKTV" + i + ": {required: true,digitsWithOutSeparatorCharacterVN: true}," + Environment.NewLine;
        //    js += "txtKoPhaiTroLyKTV" + i + ": {required: true,digitsWithOutSeparatorCharacterVN: true}," + Environment.NewLine;
        //}
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/24
    /// Xóa lớp học và các thông tin đi kèm như "Chuyên đề"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void DeleteLopHoc()
    {
        SqlCnktLopHocProvider cnktLopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
        if (!Library.CheckIsInt32(this._IDLopHoc))
            return;
        if (cnktLopHocPro.Delete(Library.Int32Convert(this._IDLopHoc)))
        {
            cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Xóa bản ghi có ID \"" + Request.QueryString["id"] + "\" của danh mục Quản lý lớp học");
            string command = @"DELETE FROM tblCNKTLopHocChuyenDe WHERE LopHocID = " + this._IDLopHoc;
            if (_db.ExecuteNonQuery(command) > 0)
            {
                command = "DELETE FROM " + ListName.Table_CNKTLopHocBuoi + " WHERE LopHocID = " + this._IDLopHoc;
                _db.ExecuteNonQuery(command);
            }
            command = "DELETE FROM " + ListName.Table_CNKTLopHocPhi + " WHERE LopHocID = " + this._IDLopHoc;
            _db.ExecuteNonQuery(command);
            Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc");
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/09
    /// Gửi Email tới tất cả các công ty, hội viên cá nhân về thông tin lớp học.
    /// </summary>
    private void SendEmail()
    {
        try
        {
            _db.OpenConnection();
            SqlCnktLopHocProvider pro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc obj = pro.GetByLopHocId(Library.Int32Convert(_IDLopHoc));

            List<string> listMailAddress = new List<string>(); // List Địa chỉ email hợp lệ sẽ được gửi
            List<string> listSubject = new List<string>();
            List<string> listContent = new List<string>();
            List<string> listHoiVienID = new List<string>();
            List<string> listLoaiHoiVien = new List<string>();
            string query = @"SELECT Email, HoiVienTapTheID AS ID, LoaiHoiVienTapThe AS LoaiHV, 'tt' AS Loai, TenDoanhNghiep AS Ten FROM tblHoiVienTapThe
                                        UNION
                                        SELECT Email, HoiVienCaNhanID AS ID, LoaiHoiVienCaNhan AS LoaiHV, 'cn' AS Loai, HoDem + ' ' + Ten AS Ten FROM tblHoiVienCaNhan";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                int index = 0;
                foreach (Hashtable ht in listData)
                {
                    listHoiVienID.Add(ht["ID"].ToString());
                    listLoaiHoiVien.Add("");
                    if (ht["Loai"].ToString() == "cn")
                    {
                        listLoaiHoiVien[index] = ht["LoaiHV"].ToString();
                    }
                    else if (ht["Loai"].ToString() == "tt")
                    {
                        if (ht["LoaiHV"].ToString() == "0" || ht["LoaiHV"].ToString() == "2")
                            listLoaiHoiVien[index] = "3";
                        if (ht["LoaiHV"].ToString() == "1")
                            listLoaiHoiVien[index] = "4";
                    }
                    string email = ht["Email"].ToString();
                    if (!string.IsNullOrEmpty(email) && Library.CheckIsValidEmail(email))
                    {
                        listMailAddress.Add(email);
                        listSubject.Add("VACPA - Mời tham dự lớp CNKT cho Kiểm toán viên năm " + obj.TuNgay.Value.Year);
                        string content = "Kính gửi: " + ht["Ten"].ToString() + "<br />";
                        content += "Thực hiện Kế hoạch đào tạo cập nhật kiến thức năm <b>" + obj.TuNgay.Value.Year + "</b> Hội Kiểm toán viên hành nghề Việt Nam (VACPA) tổ chức cập nhật kiến thức cho kiểm toán viên đăng ký hành nghề, VACPA sẽ tổ chức lớp học số <b>" + obj.TenLopHoc + "</b> tại <b>" + obj.DiaChiLopHoc + "</b>, chi tiết nội dung thư mời xem tại đường dẫn sau: http://vacpa.org.vn/Page/CatNews.aspx?catid=149" + "<br />";
                        content += "Kính mời các cá nhân và công ty nếu quan tâm xin hãy đăng ký trước ngày <b>" + obj.HanDangKy.Value.ToString("dd/MM/yyyy") + "</b><br />";
                        //content += "Chi tiết nội dung thư mời xem tại đường dẫn sau: <a href='http://" + Request.Url.Authority + "/noframe.aspx?page=CNKT_QuanLyLopHoc_ViewEmailForAnonymous&idlh=" + _IDLopHoc + "' target='_blank'>(Nội dung thư mời tham dự lớp đào tạo, cập nhật kiến thức)</a>";
                        content += "<b>Liên hệ đăng ký tại Hà Nội:</b><br />";
                        content += "* Văn phòng TW VACPA<br />";
                        content += "Phòng 202, Nhà Dự án, số 4, ngõ Hàng Chuối 1, Hà Nội<br />";
                        content += "ĐT: 04.3 972 4334, máy lẻ: (103)<br />";
                        content += "Email:trungtamdaotao@vacpa.org.vn.<br />";

                        content += "<b>Liên hệ đăng ký tại Tp. Hồ Chí Minh:</b><br />";
                        content += "Văn phòng đại diện VACPA Tp.HCM<br />";
                        content += "P24, Lầu 1, số 138 Nguyễn Thị Minh Khai, P6, Q3, Tp.HCM<br />";
                        content += "ĐT: 08.3 930 6435, máy lẻ: (102)<br />";
                        content += "Email: vacpahcm@mof.gov.vn.<br />";

                        content += "<b>Hỗ trợ kỹ thuật đăng ký CNKT online</b><br />";
                        content += "Văn phòng TW VACPA<br />";
                        content += "ĐT: 04 3.972 4334, máy lẻ: (106)<br />";
                        content += "Email: quantriweb@vacpa.org.vn.<br />";
                        listContent.Add(content);

                    }
                    index++;
                }
            }
            if (listMailAddress.Count > 0)
            {
                if (utility.SendEmail(listMailAddress, listSubject, listContent))
                {
                    // Gui thong bao trong he thong
                    for (int i = 0; i < listHoiVienID.Count; i++)
                    {
                        cm.GuiThongBao(listHoiVienID[i], listLoaiHoiVien.Count > i ? listLoaiHoiVien[i] : "", listSubject[0], listContent[0]);
                    }

                    cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Gửi thư mời tham dự lớp học \"" + obj.TenLopHoc + "\"");
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    string msg = "Đã gửi Email tới các công ty trong danh sách.";
                    js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("SendEmailSuccess", js);
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }
    protected void lbtDuyet_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            SqlCnktLopHocProvider lopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc lopHoc = lopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
            if (lopHoc != null && lopHoc.LopHocId > 0)
            {
                if (lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_ChoDuyet, _db) || lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_ChoPheDuyetLai, _db))
                {
                    lopHoc.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_DaPheDuyet, _db));
                    if (lopHocPro.Update(lopHoc))
                    {
                        Session["MsgSuccess"] = "Duyệt thông tin lớp học thành công!";
                        Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + lopHoc.LopHocId);
                    }
                }
                else
                {
                    Session["MsgError"] = "Không được duyệt thông tin của lớp học này!";
                    Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + lopHoc.LopHocId);
                }
            }
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }
    protected void lbtTuChoi_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            SqlCnktLopHocProvider lopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc lopHoc = lopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
            if (lopHoc != null && lopHoc.LopHocId > 0)
            {
                if (lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_ChoDuyet, _db) || lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_ChoPheDuyetLai, _db))
                {
                    lopHoc.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_KhongPheDuyet, _db));
                    if (lopHocPro.Update(lopHoc))
                    {
                        Session["MsgSuccess"] = "Từ chối thông tin lớp học thành công!";
                        Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + lopHoc.LopHocId);
                    }
                }
                else
                {
                    Session["MsgError"] = "Không được từ chối thông tin của lớp học này!";
                    Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + lopHoc.LopHocId);
                }
            }
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }
}