﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_CapNhatSoGioDaoTaoCNKT.ascx.cs" Inherits="usercontrols_CNKT_CapNhatSoGioDaoTaoCNKT" %>

<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .tdInput
    {
        width: 130px;
    }

    .back-to-top {
			position: fixed;
			bottom: 2em;
			right: 7em;
			
			z-index:9999;
		}
</style>
<form id="form_CapNhat" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Cập nhật số giờ đào tạo, CNKT thực tế</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin lớp học</legend>
    <table style="width: 100%; min-width: 800px" border="0" class="formtblInfor">
        <tr>
            <td style="width: 100px;">
                Mã lớp học<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtMaLopHoc" id="txtMaLopHoc" onchange="CallActionGetInforLopHoc();" />
                <input type="hidden" name="hdLopHocID" id="hdLopHocID" />
            </td>
            <td>
                <input type="button" value="---" onclick="OpenDanhSachLopHoc();" style="border: 1px solid gray;" />
            </td>
            <td style="width: 100px;">
                Từ ngày:
            </td>
            <th>
                <span type="text" name="spanTuNgay" id="spanTuNgay" readonly="readonly" />
            </th>
            <td style="width: 100px;">
                Đến ngày:
            </td>
            <th>
                <span type="text" name="spanDenNgay" id="spanDenNgay" readonly="readonly" />
            </th>
        </tr>
        <tr>
            <td>
                Tên lớp học:
            </td>
            <th colspan="2">
                <inspanput type="text" name="spanTenLopHoc" id="spanTenLopHoc" readonly="readonly" />
            </th>
            <td>
                Địa điểm:
            </td>
            <th colspan="3">
                <span type="text" name="spanDiaDiem" id="spanDiaDiem" readonly="readonly"></span>
            </th>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Danh sách chuyên đề</legend>
    <input type="hidden" id="hdListChuyenDeID" name="hdListChuyenDeID" />
    <table id="tblDanhSachChuyenDe" style="width: 100%; min-width: 800px;" border="0"
        class="formtbl">
        <tr>
            <th>
                STT
            </th>
            <th>
                Mã CĐ
            </th>
            <th>
                Tên chuyên đề
            </th>
            <th>
                Loại
            </th>
            <th>
                Thời lượng (buổi)
            </th>
            <th>
                Giảng viên
            </th>
            <th>
                Trình độ chuyên môn
            </th>
            <th>
                Đơn vị công tác
            </th>
            <th>
                Chức vụ
            </th>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor" id="FieldSetDanhSachHoiVienCaNhan" style="display: none;">
    <legend>Danh sách hội viên cá nhân và kiểm toán viên tham dự lớp học</legend>
    <div style="padding: 5px;">
        <input type="text" id="txtSearchDanhSachHoiVienCaNhan" name="txtSearchDanhSachHoiVienCaNhan"
            placeholder=" Tìm kiếm" style="width: 100px;" /><a id="A1" href="javascript:;" style="position: relative;
                padding-bottom: 6px; padding-top: 3px; padding-left: 10px; padding-right: 10px;
                left: -33px; text-decoration: none;" onclick="Search(1);"><i class="iconfa-search"></i></a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="fnExcelReport()" ><i class="iconfa-save"></i> Kết xuất</a>
    </div>
    <table id="tblDanhSachHoiVienCaNhan" style="min-width: 100%;" border="0" class="formtbl">
        <thead>
            <tr>
                <th rowspan="2" style="width: 30px;">
                    STT
                </th>
                <th rowspan="2" style="width: 150px;">
                    Mã
                </th>
                <th rowspan="2" style="width: 150px;">
                    Họ và tên
                </th>
                <th rowspan="2" style="width: 50px;">
                    Hội viên
                </th>
                <th rowspan="2" style="width: 150px;">
                    Đơn vị công tác
                </th>
                <th rowspan="2" style="width: 100px;">
                    Số CC KTV
                </th>
                <th rowspan="2" style="width: 80px;">
                    Ngày cấp CC KTV
                </th>
                <th colspan="3">
                    Tổng số giờ CNKT
                </th>
                <th id="ThHeaderListChuyenDe_DanhSachHoiVienCaNhan" colspan="3">
                    Số giờ tham dự từng chuyên đề
                </th>
            </tr>
            <tr id="TrHeaderListChuyenDe_DanhSachHoiVienCaNhan">
                <th style="width: 70px;">
                    KT,KiT
                </th>
                <th style="width: 70px;">
                    ĐĐ
                </th>
                <th style="width: 70px;">
                    #
                </th>
            </tr>
        </thead>
        <tbody id="tBodyDanhSachHoiVienCaNhan">
        </tbody>
    </table>
</fieldset>
<fieldset class="fsBlockInfor" id="FieldSetDanhSachNguoiQuanTam" style="display: none;">
    <legend>Danh sách người quan tâm tham dự lớp học</legend>
    <div style="padding: 5px;">
        <input type="text" id="txtSearchDanhSachNguoiQuanTam" name="txtSearchDanhSachNguoiQuanTam"
            placeholder=" Tìm kiếm" style="width: 100px;" />
        <a id="A2" href="javascript:;" style="position: relative; padding-bottom: 6px; padding-top: 3px;
            padding-left: 10px; padding-right: 10px; left: -33px; text-decoration: none;"
            onclick="Search(2);"><i class="iconfa-search"></i></a><a id="A3"   href="#none" class="btn btn-rounded" onclick="fnExcelReport_NQT()" ><i class="iconfa-save"></i> Kết xuất</a>
    </div>
    <table id="tblDanhSachNguoiQuanTam" style="min-width: 100%;" border="0" class="formtbl">
        <thead>
            <tr>
                <th rowspan="2" style="width: 30px;">
                    STT
                </th>
                <th rowspan="2" style="width: 150px;">
                    Mã
                </th>
                <th rowspan="2" style="width: 150px;">
                    Họ và tên
                </th>
                <th rowspan="2" style="width: 50px;">
                    Chức vụ
                </th>
                <th rowspan="2" style="width: 150px;">
                    Đơn vị công tác
                </th>
                <th colspan="3">
                    Tổng số giờ CNKT
                </th>
                <th id="ThHeaderListChuyenDe_DanhSachNguoiQuanTam" colspan="3">
                    Số giờ tham dự từng chuyên đề
                </th>
            </tr>
            <tr id="TrHeaderListChuyenDe_DanhSachNguoiQuanTam">
                <th style="width: 70px;">
                    KT,KiT
                </th>
                <th style="width: 70px;">
                    ĐĐ
                </th>
                <th style="width: 70px;">
                    #
                </th>
            </tr>
        </thead>
        <tbody id="tBodyDanhSachNguoiQuanTam">
        </tbody>
    </table>
</fieldset>
<div style="width: 100%; margin-top: 10px; background-color: white;">
    <a id="btn_save" href="javascript:;" class="btn btn-rounded back-to-top" onclick="Save();"><i
        class="iconfa-save"></i>Lưu</a>&nbsp; <a id="btn_delete" href="javascript:;" class="btn btn-rounded"
            onclick="Delete();"><i class="iconfa-trash"></i>Xóa</a>&nbsp;
    <asp:LinkButton ID="lbtExport" runat="server" CssClass="btn" Visible="false"><i class="iconfa-save"></i>Kết xuất</asp:LinkButton>&nbsp;
    <div id="DivLoading">
    </div>
</div>
</form>
<iframe name="iframeProcess_LoadLopHoc" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_DanhSachHocVienThamDuLopHoc" width="0px" height="0px">
</iframe>
<iframe name="iframeProcess_DanhSachNguoiQuanTam" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_Update" src="/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process"
    width="0px" height="0px"></iframe>
<script type="text/javascript">
    function fnExcelReport()
    {
        $("#tblDanhSachHoiVienCaNhan :input").each(function () {
            this.outerHTML = "<span id=" + this.id + " class=" + this.className + ">" + this.value+ "</span>";
        });

        var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j=0;
        tab = document.getElementById('tblDanhSachHoiVienCaNhan'); // id of table

        for(j = 2 ; j < tab.rows.length ; j++) 
        {     
            tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text=tab_text+"</table>";
        tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
        //tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE "); 

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html","replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus(); 
            sa=txtArea1.document.execCommand("SaveAs",true,"CNKT.xls");
        }  
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

        return (sa);

    }

    function fnExcelReport_NQT()
    {
        $("#tblDanhSachNguoiQuanTam :input").each(function () {
            this.outerHTML = "<span id=" + this.id + " class=" + this.className + ">" + this.value+ "</span>";
        });

        var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j=0;
        tab = document.getElementById('tblDanhSachNguoiQuanTam'); // id of table

        for(j = 2 ; j < tab.rows.length ; j++) 
        {     
            tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text=tab_text+"</table>";
        tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
        //tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE "); 

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html","replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus(); 
            sa=txtArea1.document.execCommand("SaveAs",true,"CNKT.xls");
        }  
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

        return (sa);
    }

    var _arrDataChuyenDe = [];
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách lớp học
    function OpenDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").empty();
        $("#DivDanhSachLopHoc").append($("<iframe width='100%' height='100%' id='ifDanhSachLopHoc' name='ifDanhSachLopHoc' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyLopHoc_MiniList"));
        $("#DivDanhSachLopHoc").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách lớp học</b>",
            modal: true,
            zIndex: 1000
        });
    }

    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu lớp học theo mã lớp khi NSD nhập trực tiếp mã lớp vào ô textbox
    function CallActionGetInforLopHoc() {
        var maLopHoc = $('#txtMaLopHoc').val();
        iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh=' + maLopHoc + '&action=loadlh';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin lớp học từ Dialog danh sách
    function DisplayInforLopHoc(value) {
        var arrValue = value.split(';#');
        $('#hdLopHocID').val(arrValue[0]);
        $('#txtMaLopHoc').val(arrValue[1]);
        $('#spanTenLopHoc').html(arrValue[2]);
        $('#spanTuNgay').html(arrValue[3]);
        $('#spanDenNgay').html(arrValue[4]);
        $('#spanDiaDiem').html(arrValue[7]);
        
        // Gọi phần xử lý load danh sách chuyên đề trong iframe
        iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&lophocid=' + arrValue[0] + '&action=loadcd';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách lớp học (Gọi hàm này từ trang con)
    function CloseFormDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").dialog('close');
    }

    var _arrSoGioToiDaMoiChuyenDe = [];
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm vẽ dữ liệu chuyên đề từ mảng dữ liệu trả về từ iFrame
    function DrawData_ChuyenDe(arrData) {
        _arrDataChuyenDe = arrData;
        var tbl = document.getElementById('tblDanhSachChuyenDe');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }

        var trHeaderListChuyenDe_DanhSachHoiVienCaNhan = document.getElementById('TrHeaderListChuyenDe_DanhSachHoiVienCaNhan');
        var ths1 = trHeaderListChuyenDe_DanhSachHoiVienCaNhan.getElementsByTagName("th");
        for (var i = 3; i < ths1.length; i++) {
            ths1[i].parentNode.removeChild(ths1[i]);
            i--;
        }
        
        var trHeaderListChuyenDe_DanhSachNguoiQuanTam = document.getElementById('TrHeaderListChuyenDe_DanhSachNguoiQuanTam');
        var ths2 = trHeaderListChuyenDe_DanhSachNguoiQuanTam.getElementsByTagName("th");
        for (var i = 3; i < ths2.length; i++) {
            ths2[i].parentNode.removeChild(ths2[i]);
            i--;
        }
        
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var chuyenDeID = arrData[i][0];
                var maChuyenDe = arrData[i][1];
                var tenChuyenDe = arrData[i][2];
                var loaiChuyenDe = arrData[i][3];
                var tenLoaiChuyenDe = arrData[i][4];
                var tuNgay = arrData[i][5];
                var denNgay = arrData[i][6];
                var soBuoi = arrData[i][7];
                var giangVienID = arrData[i][8];
                var tenGiangVien = arrData[i][9];
                var trinhDoChuyenMon = arrData[i][10];
                var donViCongTac = arrData[i][11];
                var chucVu = arrData[i][12];
                var chungChi = arrData[i][13];
                
                var tr = document.createElement('TR');

                var arrColumn = [(i + 1), maChuyenDe, tenChuyenDe, tenLoaiChuyenDe, soBuoi, tenGiangVien, trinhDoChuyenMon, donViCongTac, chucVu];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
                
                var tdHeaderChuyenDe = document.createElement("TH");
                tdHeaderChuyenDe.style.width = "80px";
                tdHeaderChuyenDe.innerHTML = maChuyenDe + "<br /><i>("+(parseFloat(soBuoi) * 4)+" giờ)</i>";
                trHeaderListChuyenDe_DanhSachHoiVienCaNhan.appendChild(tdHeaderChuyenDe);
                tdHeaderChuyenDe = document.createElement("TH");
                tdHeaderChuyenDe.style.width = "80px";
                tdHeaderChuyenDe.innerHTML = maChuyenDe + "<br /><i>("+(parseFloat(soBuoi) * 4)+" giờ)</i>";
                trHeaderListChuyenDe_DanhSachNguoiQuanTam.appendChild(tdHeaderChuyenDe); 
                
                // Thêm số giờ tối đa của mỗi chuyên đề để kiểm tra
                _arrSoGioToiDaMoiChuyenDe.push(chuyenDeID + "_" + (parseFloat(soBuoi) * 4));
            }

            $('#ThHeaderListChuyenDe_DanhSachHoiVienCaNhan').prop('colspan', arrData.length);
            $('#ThHeaderListChuyenDe_DanhSachNguoiQuanTam').prop('colspan', arrData.length);
            var widthtbl_hvcn = 920 + (parseInt(arrData.length) * 80);
            var widthtbl_nqt = 740 + (parseInt(arrData.length) * 80);
            $('#tblDanhSachHoiVienCaNhan').width(widthtbl_hvcn);
            $('#tblDanhSachNguoiQuanTam').width(widthtbl_nqt);
        }
        
        // Gọi phần xử lý load danh sách học viên/Kế toán viên tham dự lớp học
        iframeProcess_DanhSachHocVienThamDuLopHoc.location = '/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process&idlh=' + $('#hdLopHocID').val() + '&action=loaddanhsachhoivienthamdulophoc';
        iframeProcess_DanhSachNguoiQuanTam.location = '/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process&idlh=' + $('#hdLopHocID').val() + '&action=loaddanhsachnguoiquantamthamdulophoc';
    }
    
    // Created by NGUYEN MANH HUNG - 2014/12/08
    // Hàm vẽ dữ liệu chuyên đề từ mảng dữ liệu trả về từ iFrame
    function DrawData_DanhSachHoiVienThamDuLopHoc(arrData, arrData_LopHocHocVien, arrData_SoGioThucTe) {
        var tbl = document.getElementById('tBodyDanhSachHoiVienCaNhan');
//        var tbody = document.createElement('TBODY');
//        tbl.appendChild(tbody);
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 0; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        
        if(arrData.length > 0) {
            $('#FieldSetDanhSachHoiVienCaNhan').css('display', '');
            for(var i = 0; i < arrData.length; i ++) {
                var idHoiVienCaNhan = arrData[i][0];
                var maHoiVienCaNhan = arrData[i][1];
                var tenHoiVienCaNhan = arrData[i][2];
                var loaiHoiVienCaNhan = arrData[i][3];
                var tenCongTy = arrData[i][4];
                var soChungChiKTV = arrData[i][5];
                var ngayCapChungChiKTV = arrData[i][6];

                var tenLoaiHoiVienCaNhan = "";
                if(loaiHoiVienCaNhan == "1")
                    tenLoaiHoiVienCaNhan = "x";
                
                // Xử lý với các cột số giờ học thực tế tiếp theo
                var loaiChuyenDe_KT = 0, loaiChuyenDe_DD = 0, loaiChuyenDe_Khac = 0;
                for(var j = 0; j < arrData_SoGioThucTe.length; j ++) {
                    var temp_idHoiVienCaNhan = arrData_SoGioThucTe[j][1];
                    if(idHoiVienCaNhan == temp_idHoiVienCaNhan) {
                        var temp_LoaiChuyenDe = arrData_SoGioThucTe[j][4];
                        if(arrData_SoGioThucTe[j][5] != '' && !isNaN(arrData_SoGioThucTe[j][5])) {
                            if(temp_LoaiChuyenDe == <%= ListName.Type_LoaiChuyenDe_KeToanKiemToan %>) 
                                loaiChuyenDe_KT += parseFloat(arrData_SoGioThucTe[j][5]);
                            if(temp_LoaiChuyenDe == <%= ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep %>)
                                loaiChuyenDe_DD += parseFloat(arrData_SoGioThucTe[j][5]);
                            if(temp_LoaiChuyenDe == <%= ListName.Type_LoaiChuyenDe_ChuyenDeKhac %>)
                                loaiChuyenDe_Khac += parseFloat(arrData_SoGioThucTe[j][5]);
                        }
                    }
                }

                var tr = document.createElement('TR');
                // Thêm các cột thông tin học viên ở đầu
                var arrColumn = [(i + 1), maHoiVienCaNhan, tenHoiVienCaNhan, tenLoaiHoiVienCaNhan, tenCongTy, soChungChiKTV, ngayCapChungChiKTV];
                var arrWidth = ['30px', '150px', '150px', '50px', '150px', '100px', '80px'];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.style.width = arrWidth[j];
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                
                // Thêm 3 cột tổng của 3 loại chuyên đề
                var td_KeToan = document.createElement("TD");
                td_KeToan.style.textAlign = 'center';
                td_KeToan.style.width = '70px';
                td_KeToan.innerHTML = "<b><span id='span_"+idHoiVienCaNhan+"_<%= ListName.Type_LoaiChuyenDe_KeToanKiemToan %>'>" + loaiChuyenDe_KT + "</span></b>";
                tr.appendChild(td_KeToan); 
                
                var td_DD = document.createElement("TD");
                td_DD.style.textAlign = 'center';
                td_DD.style.width = '70px';
                td_DD.innerHTML = "<b><span id='span_"+idHoiVienCaNhan+"_<%= ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep %>'>" + loaiChuyenDe_DD + "</span></b>";
                tr.appendChild(td_DD);
                
                var td_Khac = document.createElement("TD");
                td_Khac.style.textAlign = 'center';
                td_Khac.style.width = '70px';
                td_Khac.innerHTML = "<b><span id='span_"+idHoiVienCaNhan+"_<%= ListName.Type_LoaiChuyenDe_ChuyenDeKhac %>'>" + loaiChuyenDe_Khac + "</span></b>";
                tr.appendChild(td_Khac);
                
                // Thêm các cột động theo danh sách chuyên đề phía sau
                if(_arrDataChuyenDe.length > 0) {
                    for(var j = 0; j < _arrDataChuyenDe.length; j ++) {
                        var td = document.createElement("TD");
                        td.style.textAlign = 'center';
                        td.style.width = '80px';
                        var flag = false;
                        for(var a = 0 ; a < arrData_LopHocHocVien.length ; a ++) {
                            if(_arrDataChuyenDe[j][0] == arrData_LopHocHocVien[a][1] && arrData_LopHocHocVien[a][0] == idHoiVienCaNhan) {
                                flag = true;   
                            }
                        }
                        if(flag) {
                            var soGioThucTeMoiChuyenDe = '';
                            var temp_LoaiChuyenDe = _arrDataChuyenDe[j][3];
                            for(var x = 0; x < arrData_SoGioThucTe.length; x ++) {
                                var temp_idHoiVienCaNhan = arrData_SoGioThucTe[x][1];
                                if(idHoiVienCaNhan == temp_idHoiVienCaNhan && _arrDataChuyenDe[j][0] == arrData_SoGioThucTe[x][2]) {
                                    soGioThucTeMoiChuyenDe = arrData_SoGioThucTe[x][5];
                                }
                            }
                            if(soGioThucTeMoiChuyenDe.length == 0) { // Nếu số giờ thực tế = '' -> Chưa được set số giờ thực tế thì lấy số giờ tối đa để set
                                for (var x = 0; x < _arrSoGioToiDaMoiChuyenDe.length; x++) {
                                    if(_arrSoGioToiDaMoiChuyenDe[x].split('_')[0] == _arrDataChuyenDe[j][0])
                                        soGioThucTeMoiChuyenDe = parseFloat(_arrSoGioToiDaMoiChuyenDe[x].split('_')[1]);
                                }
                            }
                            td.innerHTML = "<input type='text' id='txt_"+idHoiVienCaNhan+"_"+temp_LoaiChuyenDe+"_"+_arrDataChuyenDe[j][0]+"' onchange='SumTongSoGioTungLoaiChuyenDe(this);' class='"+idHoiVienCaNhan + "_" + temp_LoaiChuyenDe+"' value='"+soGioThucTeMoiChuyenDe+"' />";
                        }
                        tr.appendChild(td);  
                    }
                }
                tbl.appendChild(tr);
            }
        } else {
            $('#FieldSetDanhSachHoiVienCaNhan').css('display', 'none');
        }
    }

    function Search(type) {
        var txtSearchName = '';
        var tBodyDanhSach = '';
        if(type == 1) {
            txtSearchName = 'txtSearchDanhSachHoiVienCaNhan';
            tBodyDanhSach = 'tBodyDanhSachHoiVienCaNhan';
        }
        if(type == 2) {
            txtSearchName = 'txtSearchDanhSachNguoiQuanTam';
            tBodyDanhSach = 'tBodyDanhSachNguoiQuanTam';
        }
        var key = $('#'+txtSearchName).val();
        $('#'+ tBodyDanhSach +' tr').each(function(rowIndex, row){
            var tds = $(row).find('td');
            var flag = false;
            tds.each(function (cellIndex, cell) {
                if(!flag) {
                    if(cellIndex == 1 || cellIndex == 2) {
                        if($(cell).html().toLowerCase().indexOf(key.toLowerCase()) > - 1) {
                            $(row).css('display', '');
                            flag = true;
                        }
                    }
                }
            });
            if(!flag){
                $(row).css('display', 'none');
            }
        });
    }
    
    // Created by NGUYEN MANH HUNG - 2014/12/08
    // Hàm vẽ dữ liệu chuyên đề từ mảng dữ liệu trả về từ iFrame
    function DrawData_DanhSachNguoiQuanTam(arrData, arrData_LopHocHocVien, arrData_SoGioThucTe) {
        var tbl = document.getElementById('tBodyDanhSachNguoiQuanTam');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 0; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        
        if(arrData.length > 0) {
            $('#FieldSetDanhSachNguoiQuanTam').css('display', '');
            for(var i = 0; i < arrData.length; i ++) {
                var idHoiVienCaNhan = arrData[i][0];
                var maHoiVienCaNhan = arrData[i][1];
                var tenHoiVienCaNhan = arrData[i][2];
                var chucVu = arrData[i][3];
                var tenCongTy = arrData[i][4];
                
                // Xử lý với các cột số giờ học thực tế tiếp theo
                var loaiChuyenDe_KT = 0, loaiChuyenDe_DD = 0, loaiChuyenDe_Khac = 0;
                for(var j = 0; j < arrData_SoGioThucTe.length; j ++) {
                    var temp_idHoiVienCaNhan = arrData_SoGioThucTe[j][1];
                    if(idHoiVienCaNhan == temp_idHoiVienCaNhan) {
                        var temp_LoaiChuyenDe = arrData_SoGioThucTe[j][4];
                        if(arrData_SoGioThucTe[j][5] != '' && !isNaN(arrData_SoGioThucTe[j][5])) {
                            if(temp_LoaiChuyenDe == <%= ListName.Type_LoaiChuyenDe_KeToanKiemToan %>) 
                                loaiChuyenDe_KT += parseFloat(arrData_SoGioThucTe[j][5]);
                            if(temp_LoaiChuyenDe == <%= ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep %>)
                                loaiChuyenDe_DD += parseFloat(arrData_SoGioThucTe[j][5]);
                            if(temp_LoaiChuyenDe == <%= ListName.Type_LoaiChuyenDe_ChuyenDeKhac %>)
                                loaiChuyenDe_Khac += parseFloat(arrData_SoGioThucTe[j][5]);
                        }
                    }
                }

                var tr = document.createElement('TR');
                // Thêm các cột thông tin học viên ở đầu
                var arrColumn = [(i + 1), maHoiVienCaNhan, tenHoiVienCaNhan, chucVu, tenCongTy];
                var arrWidth = ['30px', '150px', '150px', '50px', '150px', '100px', '80px'];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.style.width = arrWidth[j];
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                
                // Thêm 3 cột tổng của 3 loại chuyên đề
                var td_KeToan = document.createElement("TD");
                td_KeToan.style.textAlign = 'center';
                td_KeToan.style.width = '70px';
                td_KeToan.innerHTML = "<b><span id='span_"+idHoiVienCaNhan+"_<%= ListName.Type_LoaiChuyenDe_KeToanKiemToan %>'>" + loaiChuyenDe_KT + "</span></b>";
                tr.appendChild(td_KeToan); 
                
                var td_DD = document.createElement("TD");
                td_DD.style.textAlign = 'center';
                td_DD.style.width = '70px';
                td_DD.innerHTML = "<b><span id='span_"+idHoiVienCaNhan+"_<%= ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep %>'>" + loaiChuyenDe_DD + "</span></b>";
                tr.appendChild(td_DD);
                
                var td_Khac = document.createElement("TD");
                td_Khac.style.textAlign = 'center';
                td_Khac.style.width = '70px';
                td_Khac.innerHTML = "<b><span id='span_"+idHoiVienCaNhan+"_<%= ListName.Type_LoaiChuyenDe_ChuyenDeKhac %>'>" + loaiChuyenDe_Khac + "</span></b>";
                tr.appendChild(td_Khac);
                
                // Thêm các cột động theo danh sách chuyên đề phía sau
                if(_arrDataChuyenDe.length > 0) {
                    for(var j = 0; j < _arrDataChuyenDe.length; j ++) {
                        var td = document.createElement("TD");
                        td.style.textAlign = 'center';
                        td.style.width = '80px';
                        var flag = false;
                        for(var a = 0 ; a < arrData_LopHocHocVien.length ; a ++) {
                            if(_arrDataChuyenDe[j][0] == arrData_LopHocHocVien[a][1] && arrData_LopHocHocVien[a][0] == idHoiVienCaNhan) {
                                flag = true;   
                            }
                        }
                        if(flag) {
                            var soGioThucTeMoiChuyenDe = '';
                            var temp_LoaiChuyenDe = _arrDataChuyenDe[j][3];
                            for(var x = 0; x < arrData_SoGioThucTe.length; x ++) {
                                var temp_idHoiVienCaNhan = arrData_SoGioThucTe[x][1];
                                if(idHoiVienCaNhan == temp_idHoiVienCaNhan && _arrDataChuyenDe[j][0] == arrData_SoGioThucTe[x][2]) {
                                    soGioThucTeMoiChuyenDe = arrData_SoGioThucTe[x][5];
                                }
                            }
                            if(soGioThucTeMoiChuyenDe.length == 0) { // Nếu số giờ thực tế = '' -> Chưa được set số giờ thực tế thì lấy số giờ tối đa để set
                            
                                for (var x = 0; x < _arrSoGioToiDaMoiChuyenDe.length; x++) {
                                    if(_arrSoGioToiDaMoiChuyenDe[x].split('_')[0] == _arrDataChuyenDe[j][0])
                                        soGioThucTeMoiChuyenDe = parseFloat(_arrSoGioToiDaMoiChuyenDe[x].split('_')[1]);
                                }
                            }
                            td.innerHTML = "<input type='text' id='txt_"+idHoiVienCaNhan+"_"+temp_LoaiChuyenDe+"_"+_arrDataChuyenDe[j][0]+"' onchange='SumTongSoGioTungLoaiChuyenDe(this);' class='"+idHoiVienCaNhan + "_" + temp_LoaiChuyenDe+"' value='"+soGioThucTeMoiChuyenDe+"' />";
                        }
                        tr.appendChild(td);  
                    }
                }
                tbl.appendChild(tr);
            }
        } else {
                $('#FieldSetDanhSachNguoiQuanTam').css('display', 'none');
        }
    }

    function SumTongSoGioTungLoaiChuyenDe(obj) {
        if(!CheckIsNumber(obj))
            return;
        // Kiểm tra số nhập vào có vượt quá số giờ tối đa của chuyên đề ko
        //if(_arrSoGioToiDaMoiChuyenDe.length > 0){
        //    var max = 0;
        //    for (var i = 0; i < _arrSoGioToiDaMoiChuyenDe.length; i++) {
        //        if(_arrSoGioToiDaMoiChuyenDe[i].split('_')[0] == obj.id.split('_')[3])
        //            max = parseFloat(_arrSoGioToiDaMoiChuyenDe[i].split('_')[1]);
        //    }
        //    if(parseFloat(obj.value) > parseFloat(max)) {
        //        obj.value = '';
        //        alert('Không được vượt quá số giờ học tối đa của chuyên đề.');
        //        var t = setTimeout('document.getElementById("' + obj.id + '").focus()', 1);
        //    }
        //}
        var className= $(obj).attr('class');
        var sum = 0;
        $('#tblDanhSachHoiVienCaNhan .' + className).each(function() {
            var value = $(this).val();
            if(value != '' && !isNaN(value))
                sum += parseFloat(value);
        });
        $('#tblDanhSachNguoiQuanTam .' + className).each(function() {
            var value = $(this).val();
            if(value != '' && !isNaN(value))
                sum += parseFloat(value);
        });
        $('#span_' + className).html(sum);
    }
    
    $(document).ready( function () {
//    var table = $('#tblDanhSachHoiVienCaNhan').DataTable({
//        "scrollY": "100px",
//        "scrollCollapse": true,
//        "paging": false
//    });
//    new $.fn.dataTable.FixedHeader(table);
        $('#tblDanhSachHoiVienCaNhan').stickyTableHeaders();
        $('#tblDanhSachNguoiQuanTam').stickyTableHeaders();
});

    function Save() {
        var lopHocId = $('#hdLopHocID').val();
        if(lopHocId == '') {
            alert('Phải chọn lớp học để cập nhật thông tin!');
            return;
        }
        var temp = '';
        $('#tBodyDanhSachHoiVienCaNhan :text').each(function(){
            var value = $(this).val();
            var id = this.id;
            var arrId = id.split('_');
            temp += arrId[1] + "_" + arrId[3] + "_"+value+";#";
        });
        $('#tBodyDanhSachNguoiQuanTam :text').each(function(){
            var value = $(this).val();
            var id = this.id;
            var arrId = id.split('_');
            temp += arrId[1] + "_" + arrId[3] + "_"+value+";#";
        });
        $('#DivLoading').html('<img src="/images/loaders/loader11.gif" /> Đang xử lý ...');
        var arrData = [lopHocId, temp];
        iframeProcess_Update.location = '/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process';
        window.iframeProcess_Update.Save(arrData);
    }
    
    function SaveSuccess(type) {
        $('#DivLoading').html('');
        if(type == 1){
            jQuery("#thongbaothanhcong_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Lưu số giờ học thành công!").show();
        } else {
            jQuery("#thongbaoloi_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Không thể lưu số giờ học!").show();
        }
        // Reset dữ liệu trên Form
        iframeProcess_DanhSachHocVienThamDuLopHoc.location = '/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process&idlh=' + $('#hdLopHocID').val() + '&action=loaddanhsachhoivienthamdulophoc';
        iframeProcess_DanhSachNguoiQuanTam.location = '/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process&idlh=' + $('#hdLopHocID').val() + '&action=loaddanhsachnguoiquantamthamdulophoc';
        window.scrollTo(0, 0);
    }

    function Delete() {
        if(confirm('Bạn chắc chắn với thao tác này chứ?')){
            $('#DivLoading').html('<img src="/images/loaders/loader11.gif" /> Đang xử lý ...');
            iframeProcess_Update.location = '/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process&idlh='+$('#hdLopHocID').val()+'&action=delete';
        }
    }
    
    function DeleteSuccess(type) {
        $('#DivLoading').html('');
        if(type == 1){
            jQuery("#thongbaothanhcong_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Xóa tất cả thông tin số giờ học của học viên trong lớp thành công!").show();
        } else {
            jQuery("#thongbaoloi_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Không thể xóa thông tin số giờ học của học viên trong lớp!").show();
        }
        // Reset dữ liệu trên Form
        iframeProcess_DanhSachHocVienThamDuLopHoc.location = '/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process&idlh=' + $('#hdLopHocID').val() + '&action=loaddanhsachhoivienthamdulophoc';
        iframeProcess_DanhSachNguoiQuanTam.location = '/iframe.aspx?page=CNKT_CapNhatSoGioDaoTaoCNKT_Process&idlh=' + $('#hdLopHocID').val() + '&action=loaddanhsachnguoiquantamthamdulophoc';
        window.scrollTo(0, 0);
    }

    <% FirstLoadPage(); CheckPermissionOnPage();%>
</script>
<div id="DivDanhSachLopHoc">
</div>