﻿using HiPT.VACPA.DL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_CNKT_getphi_ajax : System.Web.UI.Page
{
    Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
            string sql = "SELECT " + Request.QueryString["phi"] + " FROM tblCNKTLopHocPhi WHERE LopHocID = " + Request.QueryString["lophocid"] + " AND SoNgay = " + Request.QueryString["tongsongayhoc"];
            SqlCommand cmd = new SqlCommand(sql);
            string phi = DataAccess.DLookup(cmd);
            
            cm.write_ajaxvar("Phi", phi);

        }
        catch (Exception ex)
        {
            cm.write_ajaxvar("Phi", "");

        }
    }
}