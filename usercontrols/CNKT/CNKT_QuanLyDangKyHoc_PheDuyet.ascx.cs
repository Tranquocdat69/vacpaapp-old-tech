﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CodeEngine.Framework.QueryBuilder.Enums;
using HiPT.VACPA.DL;

using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_CNKT_QuanLyDangKyHoc_PheDuyet : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý danh sách đăng ký học";
    protected string _listPermissionOnFunc_QuanLyDangKyHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    private string _lopHocId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        this._lopHocId = Request.QueryString["idlh"];
        _listPermissionOnFunc_QuanLyDangKyHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyDangKyHoc, cm.connstr);
        if (_listPermissionOnFunc_QuanLyDangKyHoc.Contains("XEM|"))
        {
            try
            {
                _db.OpenConnection();
                if (!IsPostBack)
                {
                    GetListChuyenDe();
                    GetThongTinChuyenDe();
                    LoadDangKyHoc(_lopHocId, ddlMaChuyenDe.SelectedValue);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _db.CloseConnection();
            }
        }
        else
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem danh sách đăng ký học!</div>"));
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/02
    /// Load Danh sách đăng ký học từ DB
    /// </summary>
    private void LoadDangKyHoc(string lopHocId, string idChuyenDe)
    {
        if (string.IsNullOrEmpty(lopHocId))
            return;
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        string query = "";
        int soHocVien = 0, soHocVienCaNhan = 0, soHocVienTapThe = 0;
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "LopHocHocVienID", "mhvcn", "FullName", "LoaiHoiVienCaNhan", "SoChungChiKTV", "NgayDangKy", "trStyle", "HeaderType", "HiddenRow", "LinkEdit" });
        query = @"SELECT LHHV.LopHocHocVienID, tblHoiVienCaNhan.HoiVienCaNhanID HoiVienCaNhanID,
                    tblHoiVienCaNhan.MaHoiVienCaNhan AS mhvcn, (tblHoiVienCaNhan.HoDem + ' ' + tblHoiVienCaNhan.Ten) AS FullName,
                    tblHoiVienCaNhan.LoaiHoiVienCaNhan AS LoaiHoiVienCaNhan, tblHoiVienCaNhan.SoChungChiKTV AS SoChungChiKTV, tblHoiVienCaNhan.NgayCapChungChiKTV AS ngayCapChungChiKTV,
                    DKHCN.NgayDangKy AS NgayDangKy, tblHoiVienTapThe.HoiVienTapTheID AS hoiVienTapTheID, tblHoiVienTapThe.MaHoiVienTapThe AS maHoiVienTapThe,
                    tblHoiVienTapThe.TenDoanhNghiep AS TenDoanhNghiep, tblCNKTLopHoc.MaLopHoc AS MaLopHoc
                    FROM tblCNKTLopHocHocVien LHHV
                    INNER JOIN tblCNKTDangKyHocCaNhan DKHCN ON LHHV.DangKyHocCaNhanID = DKHCN.DangKyHocCaNhanID
                    INNER JOIN tblDMTrangThai TT ON LHHV.TinhTrangID = TT.TrangThaiID
                    LEFT JOIN tblHoiVienCaNhan ON DKHCN.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID
                    LEFT JOIN tblHoiVienTapThe ON DKHCN.HoiVienTapTheDangKy = tblHoiVienTapThe.HoiVienTapTheID
                    LEFT JOIN tblCNKTLopHoc ON DKHCN.LopHocID = tblCNKTLopHoc.LopHocID
                    WHERE LHHV.LopHocID = " + lopHocId + @" AND TT.MaTrangThai = '" + ListName.Status_ChoDuyet + @"'  AND LHHV.ChuyenDeID = " + idChuyenDe + @"
                    AND (DKHCN.HoiVienTapTheDangKy IS NOT NULL AND DKHCN.HoiVienTapTheDangKy != '')
                    ORDER BY tblHoiVienTapThe.SoHieu ASC, tblHoiVienCaNhan.SoChungChiKTV ASC";
        List<Hashtable> list = _db.GetListData(query);
        AddDataToDataTable(list, ref soHocVien, ref soHocVienCaNhan, ref soHocVienTapThe, ref js, dt);

        query = @"SELECT LHHV.LopHocHocVienID, tblHoiVienCaNhan.HoiVienCaNhanID HoiVienCaNhanID,
                    tblHoiVienCaNhan.MaHoiVienCaNhan AS mhvcn, (tblHoiVienCaNhan.HoDem + ' ' + tblHoiVienCaNhan.Ten) AS FullName,
                    tblHoiVienCaNhan.LoaiHoiVienCaNhan AS LoaiHoiVienCaNhan, tblHoiVienCaNhan.SoChungChiKTV AS SoChungChiKTV, tblHoiVienCaNhan.NgayCapChungChiKTV AS ngayCapChungChiKTV,
                    DKHCN.NgayDangKy AS NgayDangKy, tblHoiVienTapThe.HoiVienTapTheID AS hoiVienTapTheID, tblHoiVienTapThe.MaHoiVienTapThe AS maHoiVienTapThe,
                    tblHoiVienTapThe.TenDoanhNghiep AS TenDoanhNghiep, tblCNKTLopHoc.MaLopHoc AS MaLopHoc
                    FROM tblCNKTLopHocHocVien LHHV
                    INNER JOIN tblCNKTDangKyHocCaNhan DKHCN ON LHHV.DangKyHocCaNhanID = DKHCN.DangKyHocCaNhanID
                    INNER JOIN tblDMTrangThai TT ON LHHV.TinhTrangID = TT.TrangThaiID
                    LEFT JOIN tblHoiVienCaNhan ON DKHCN.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID
                    LEFT JOIN tblHoiVienTapThe ON DKHCN.HoiVienTapTheDangKy = tblHoiVienTapThe.HoiVienTapTheID
                    LEFT JOIN tblCNKTLopHoc ON DKHCN.LopHocID = tblCNKTLopHoc.LopHocID
                    WHERE LHHV.LopHocID = " + lopHocId + @" AND TT.MaTrangThai = '" + ListName.Status_ChoDuyet + @"'  AND LHHV.ChuyenDeID = " + idChuyenDe + @"
                    AND (DKHCN.HoiVienTapTheDangKy IS NULL OR DKHCN.HoiVienTapTheDangKy = '')
                    ORDER BY tblHoiVienCaNhan.SoChungChiKTV ASC";
        list = _db.GetListData(query);
        AddDataToDataTable(list, ref soHocVien, ref soHocVienCaNhan, ref soHocVienTapThe, ref js, dt);

        rpDanhSachDangKyHoc.DataSource = dt.DefaultView;
        rpDanhSachDangKyHoc.DataBind();

        js += "$('#spanSoHocVienDaDangKy').html('" + soHocVien + "');" + Environment.NewLine;
        js += "$('#spanSoHocVienCaNhan').html('" + soHocVienCaNhan + "');" + Environment.NewLine;
        js += "$('#spanSoHocVienTapThe').html('" + soHocVienTapThe + "');" + Environment.NewLine;
        js += "</script>";
        Page.RegisterStartupScript("LoadInforLopHoc2", js);
    }

    private void AddDataToDataTable(List<Hashtable> list, ref int soHocVien, ref int soHocVienCaNhan, ref int soHocVienTapThe, ref string js, DataTable dt)
    {
        if (list.Count > 0)
        {
            DataRow dr;

            string temp_HoiVienTapThe = ""; // Biến tạm để xác định những đơn đăng ký cùng công ty
            string space = ""; // Thêm khoảng trống để các ô dữ liệu đăng ký dịch lùi vào
            bool flag_HeaderCongTy = false, flag_HeaderCaNhan = false;

            foreach (Hashtable ht in list)
            {
                string hoiVienTapTheID = Library.CheckKeyInHashtable(ht, "hoiVienTapTheID").ToString(); // Chỉ những bản ghi đăng ký theo công ty mới có giá trị trường này

                // Đếm số học viên đăng ký theo cá nhân và tổ chức
                if (string.IsNullOrEmpty(hoiVienTapTheID))
                    soHocVienCaNhan++;
                else
                {
                    soHocVienTapThe++;
                }

                // Đoạn code xác định nhưng đơn đăng ký theo công ty và hiển thị 1 dòng tên công ty ở đầu tiên
                if (!string.IsNullOrEmpty(hoiVienTapTheID) && hoiVienTapTheID != temp_HoiVienTapThe)
                {
                    // Thêm đoạn header những đơn đăng ký theo công ty hoặc tổ chức
                    if (!flag_HeaderCongTy)
                    {
                        dr = dt.NewRow();
                        dr["HeaderType"] = "<tr style='background-color: lavender; font-weight: bold;'><td colspan='10'>ĐĂNG KÝ THEO CÔNG TY HOẶC HỘI VIÊN TẬP THỂ (<span name=\"spanSoHocVienTapThe\" id=\"spanSoHocVienTapThe\"></span> học viên)</td></tr>";
                        dr["HiddenRow"] = "style ='display:none;'";
                        dt.Rows.Add(dr);
                        flag_HeaderCongTy = true;
                    }

                    // Đoạn code add các dòng hội viên tổ chức
                    space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    dr = dt.NewRow();
                    dr["mhvcn"] = Library.CheckKeyInHashtable(ht, "maHoiVienTapThe");
                    dr["FullName"] = Library.CheckKeyInHashtable(ht, "TenDoanhNghiep");
                    dr["LoaiHoiVienCaNhan"] = "Công ty/Hội viên tổ chức";
                    dr["trStyle"] = "style='font-weight: bold;'";
                    dt.Rows.Add(dr);
                    temp_HoiVienTapThe = hoiVienTapTheID;
                }

                if (string.IsNullOrEmpty(hoiVienTapTheID) && !flag_HeaderCaNhan)
                {
                    // Thêm đoạn header những đơn đăng ký theo cá nhân
                    dr = dt.NewRow();
                    dr["HeaderType"] = "<tr style='background-color: lavender; font-weight: bold;'><td colspan='10'>ĐĂNG KÝ THEO CÁ NHÂN (<span name=\"spanSoHocVienCaNhan\" id=\"spanSoHocVienCaNhan\"></span> học viên)</td></tr>";
                    dr["HiddenRow"] = "style ='display:none;'";
                    dt.Rows.Add(dr);
                    flag_HeaderCaNhan = true;
                }

                // Add các đơn đăng ký vào danh sách bình thường
                soHocVien++;
                dr = dt.NewRow();
                dr["STT"] = soHocVien;
                dr["LopHocHocVienID"] = Library.CheckKeyInHashtable(ht, "LopHocHocVienID");
                dr["mhvcn"] = Library.CheckKeyInHashtable(ht, "mhvcn");
                dr["FullName"] = space + Library.CheckKeyInHashtable(ht, "FullName");
                dr["LoaiHoiVienCaNhan"] = Library.GetTenLoaiHoiVienCaNhan(Library.CheckKeyInHashtable(ht, "LoaiHoiVienCaNhan").ToString());
                dr["SoChungChiKTV"] = Library.CheckKeyInHashtable(ht, "SoChungChiKTV");
                dr["NgayDangKy"] = Library.FormatDateTime(Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "NgayDangKy")), "dd/MM/yyyy");
                dt.Rows.Add(dr);
            }
        }
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("XOA|"))
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");

        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("THEM|") && !_listPermissionOnFunc_QuanLyDangKyHoc.Contains("SUA|"))
        {
            Response.Write("$('#Btn_ThemCaNhan').remove();");
            Response.Write("$('#Btn_ThemTapThe').remove();");
            Response.Write("$('a[data-original-title=\"Xem/Sửa\"]').remove();");
        }

        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("XOA|"))
            Response.Write("$('#btn_xoa').remove();");

        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("KETXUAT|"))
            Response.Write("$('#btn_ketxuat').remove();");

        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
    }

    private void GetListChuyenDe()
    {
        ddlMaChuyenDe.Items.Clear();
        string query = "SELECT ChuyenDeID, MaChuyenDe FROM tblCNKTLopHocChuyenDe WHERE LopHocID = " + _lopHocId;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                ddlMaChuyenDe.Items.Add(new ListItem(ht["MaChuyenDe"].ToString(), ht["ChuyenDeID"].ToString()));
            }
        }
    }

    private void GetThongTinChuyenDe()
    {
        string query = @"SELECT ChuyenDeID, MaChuyenDe, TenChuyenDe, DMGV.TenGiangVien, SoNguoiHocToiDa, LH.TenLopHoc, LH.TuNgay, LH.DenNgay,
                                (SELECT COUNT(LopHocHocVienID) FROM tblCNKTLopHocHocVien INNER JOIN tblDMTrangThai on tblCNKTLopHocHocVien.TinhTrangID = tblDMTrangThai.TrangThaiID WHERE LopHocID = " + _lopHocId + @" AND tblCNKTLopHocHocVien.ChuyenDeID = LHCD.ChuyenDeID AND tblDMTrangThai.MaTrangThai = '5') SoNguoiDaDangKy
                                FROM tblCNKTLopHocChuyenDe LHCD
                                LEFT JOIN tblDMGiangVien DMGV ON LHCD.GiangVienID = DMGV.GiangVienID
                                LEFT JOIN tblCNKTLopHoc LH ON LHCD.LopHocID = LH.LopHocID
                                WHERE LHCD.LopHocID = " + _lopHocId + @" AND LHCD.ChuyenDeID = " + ddlMaChuyenDe.SelectedValue;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            txtTenChuyenDe.Text = ht["TenChuyenDe"].ToString();
            txtGiangVien.Text = ht["TenGiangVien"].ToString();
            txtSoHocVienDaDangKy.Text = ht["SoNguoiDaDangKy"] + "/" + ht["SoNguoiHocToiDa"];
            hf_TenChuyenDe.Value = ht["TenChuyenDe"].ToString();
            hf_TenLopHoc.Value = ht["TenLopHoc"].ToString();
            hf_TuNgay.Value = Library.DateTimeConvert(ht["TuNgay"]).ToString("dd/MM/yyyy");
            hf_DenNgay.Value = Library.DateTimeConvert(ht["DenNgay"]).ToString("dd/MM/yyyy");
        }
    }

    protected void ddlMaChuyenDe_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            LoadDangKyHoc(_lopHocId, ddlMaChuyenDe.SelectedValue);
            GetThongTinChuyenDe();
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void rpDanhSachDangKyHoc_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HiddenField hf = (HiddenField)e.Item.FindControl("hf_LopHocHocVien");
        if (hf != null)
        {
            if (string.IsNullOrEmpty(hf.Value))
            {
                HtmlInputCheckBox checkbox = e.Item.FindControl("checkbox") as HtmlInputCheckBox;
                checkbox.Visible = false;
                ((LinkButton)e.Item.FindControl("lbtApprove")).Visible = false;
                ((LinkButton)e.Item.FindControl("lbtReject")).Visible = false;

            }
        }
    }

    protected void rpDanhSachDangKyHoc_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string id = e.CommandArgument.ToString();
            if (e.CommandName == "approve")
            {
                if (Library.Int32Convert(id) > 0)
                    Approve(id);
            }
            if (e.CommandName == "reject")
            {
                if (Library.Int32Convert(id) > 0)
                    Reject(id);
            }
            LoadDangKyHoc(_lopHocId, ddlMaChuyenDe.SelectedValue);
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Approve(string idLopHocHocVien)
    {
        string status_approve = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
        // Duyet ban ghi tblCNKTLopHocHocVien
        SqlCnktLopHocHocVienProvider pro = new SqlCnktLopHocHocVienProvider(ListName.ConnectionString, false, string.Empty);
        SqlCnktDangKyHocCaNhanProvider proDkhcn = new SqlCnktDangKyHocCaNhanProvider(ListName.ConnectionString, false, string.Empty);
        CnktLopHocHocVien obj = pro.GetByLopHocHocVienId(Library.Int32Convert(idLopHocHocVien));
        if (obj != null && obj.LopHocHocVienId > 0)
        {
            obj.TinhTrangId = Library.Int32Convert(status_approve);
            if (pro.Update(obj))
            {
                // Tinh lai so gio hoc + so tien -> Cap nhat lai vao bang tblCNKTDangKyHocCaNhan
                // Tinh tong so gio hoc truoc
                string query = @"SELECT SUM(LHCD.SoBuoi) TongSoBuoi FROM tblCNKTLopHocHocVien LHHV
                                        INNER JOIN tblCNKTLopHocChuyenDe LHCD ON LHHV.ChuyenDeID = LHCD.ChuyenDeID
                                        LEFT JOIN tblDMTrangThai TT ON LHHV.TinhTrangID = TT.TrangThaiID
                                        WHERE LHHV.DangKyHocCaNhanID = " + obj.DangKyHocCaNhanId + @" AND TT.MaTrangThai = '" + ListName.Status_DaPheDuyet + @"'";
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    double tongSoBuoi = Library.DoubleConvert(listData[0]["TongSoBuoi"]);
                    double tongHocPhi = 0;
                    string loaiHvcn = "", troLyKtv = "", email = "", loaiHvcnct = "";
                    // Lay loai hoi vien ca nhan
                    query = "SELECT LoaiHoiVienCaNhan, LoaiHoiVienCaNhanChiTiet, TroLyKTV, Email FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + obj.HoiVienCaNhanId;
                    List<Hashtable> listDataHvcn = _db.GetListData(query);
                    if (listDataHvcn.Count > 0)
                    {
                        loaiHvcn = listDataHvcn[0]["LoaiHoiVienCaNhan"].ToString();
                        loaiHvcnct = listDataHvcn[0]["LoaiHoiVienCaNhanChiTiet"].ToString();
                        troLyKtv = listDataHvcn[0]["TroLyKTV"].ToString();
                        email = listDataHvcn[0]["Email"].ToString();
                    }

                    // Lay bang hoc phi
                    query = "SELECT SoTienPhiHVCNCT, SoTienPhiHVCNLK, SoTienPhiHVCNDD, SoTienPhiKTV, SoTienPhiNQT_TroLyKTV, SoTienPhiNTQ FROM tblCNKTLopHocPhi WHERE LopHocID = " + obj.LopHocId + " AND (SoNgay * 2) = " + tongSoBuoi;
                    List<Hashtable> listDataHocPhi = _db.GetListData(query);
                    if (listDataHocPhi.Count > 0)
                    {
                        switch (loaiHvcn)
                        {
                            case "1":
                                if (loaiHvcnct == "0" || loaiHvcnct == "2" || loaiHvcnct == "4" || loaiHvcnct == "5" || loaiHvcnct == "6")
                                    tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNCT"]);
                                else if (loaiHvcnct == "1")
                                    tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNLK"]);
                                else
                                    tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNDD"]);
                                break;
                            case "2":
                                tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiKTV"]);
                                break;
                            case "0":
                                if (troLyKtv == "0")
                                    tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiNTQ"]);
                                else
                                    tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiNQT_TroLyKTV"]);
                                break;
                        }
                    }

                    // Cap nhat lai vao bang tblCNKTDangKyHocCaNhan
                    CnktDangKyHocCaNhan objDkhcn = proDkhcn.GetByDangKyHocCaNhanId(obj.DangKyHocCaNhanId.Value);
                    if (objDkhcn != null && objDkhcn.DangKyHocCaNhanId > 0)
                    {
                        objDkhcn.TongSoGioCnkt = Library.DecimalConvert(tongSoBuoi * 4);
                        objDkhcn.TongPhi = Library.DecimalConvert(tongHocPhi);
                        if (proDkhcn.Update(objDkhcn))
                        {
                            // Cập nhật lại phát sinh phí
                            // Lấy ID bản ghi phát sinh phí từ trường hợp đăng ký học này
                            query = "SELECT PhatSinhPhiID FROM " + ListName.Table_TTPhatSinhPhi + " WHERE DoiTuongPhatSinhPhiID = " + objDkhcn.DangKyHocCaNhanId + " AND LoaiPhi = '4'";
                            List<Hashtable> listDataPsp = _db.GetListData(query);
                            if (listDataPsp.Count > 0)
                            {
                                SqlTtPhatSinhPhiProvider proPhatSinhPhi = new SqlTtPhatSinhPhiProvider(ListName.ConnectionString, false, string.Empty);
                                TtPhatSinhPhi objPsp = proPhatSinhPhi.GetByPhatSinhPhiId(Library.Int32Convert(listDataPsp[0]["PhatSinhPhiID"]));
                                if (objPsp != null && objPsp.PhatSinhPhiId > 0)
                                {
                                    objPsp.LoaiPhi = "4";
                                    objPsp.NgayPhatSinh = DateTime.Now;
                                    objPsp.SoTien = objDkhcn.TongPhi;
                                    proPhatSinhPhi.Update(objPsp);
                                }
                            }

                            // Gửi mail thông báo thành công và cập nhật log
                            string subject = "VACPA - Thông báo đăng ký chuyên đề học thành công";
                            string content = "<b>HỘI KIỂM TOÁN VIÊN HÀNH NGHỀ VIỆT NAM - VACPA xin thông báo:</b><br />";
                            content += "Quý công ty/Anh/Chị đã đăng ký tham dự lớp học <b>" + hf_TenLopHoc.Value + "</b>. Từ ngày " + hf_TuNgay.Value + " đến ngày " + hf_DenNgay.Value + ".<br />";
                            content += "VACPA chấp nhận đăng ký chuyên đề <b>" + hf_TenChuyenDe.Value + "</b> của anh/chị.<br />";
                            content += "Mời anh/chị hoàn tất các thủ tục nộp phí và tham dự lớp học đúng thời gian quy định.<br />";
                            content += "Xin cảm ơn!";
                            utility.SendEmail(new List<string> { email }, subject, content);
                            cm.GuiThongBao(obj.HoiVienCaNhanId.ToString(), loaiHvcn, subject, content);
                            cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Duyệt bản ghi đăng ký học cá nhân có ID = " + objDkhcn.DangKyHocCaNhanId + " trong danh mục " + tenchucnang);
                            cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Gửi thư thông báo duyệt bản ghi đăng ký học cá nhân có ID = " + objDkhcn.DangKyHocCaNhanId);
                        }
                    }
                }
                cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Duyệt bản ghi đăng ký lớp học học viên có ID = " + idLopHocHocVien + " trong danh mục " + tenchucnang);

            }
        }

    }

    private void Reject(string idLopHocHocVien)
    {

        // Tu choi -> xoa ban ghi dang ky hoc chuyen de cua hoc vien
        SqlCnktLopHocHocVienProvider pro = new SqlCnktLopHocHocVienProvider(ListName.ConnectionString, false, string.Empty);
        CnktLopHocHocVien obj = pro.GetByLopHocHocVienId(Library.Int32Convert(idLopHocHocVien));
        string email = "", loaihv = "", hoiVienCaNhanId = "";
        if (obj != null && obj.LopHocHocVienId > 0)
        {
            hoiVienCaNhanId = obj.HoiVienCaNhanId.ToString();
            string query = "SELECT LoaiHoiVienCaNhan, TroLyKTV, Email FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + obj.HoiVienCaNhanId;
            List<Hashtable> listDataHvcn = _db.GetListData(query);
            if (listDataHvcn.Count > 0)
            {
                loaihv = listDataHvcn[0]["LoaiHoiVienCaNhan"].ToString();
                email = listDataHvcn[0]["Email"].ToString();
            }
        }
        if (pro.Delete(Library.Int32Convert(idLopHocHocVien)))
        {
            string subject = "VACPA - Thông báo từ chối đăng ký chuyên đề học";
            string content = "<b>HỘI KIỂM TOÁN VIÊN HÀNH NGHỀ VIỆT NAM - VACPA xin thông báo:</b><br />";
            content += "Quý công ty/Anh/Chị đã đăng ký tham dự lớp học <b>" + hf_TenLopHoc.Value + "</b>. Từ ngày " + hf_TuNgay.Value + " đến ngày " + hf_DenNgay.Value + ".<br />";
            content += "Chuyên đề <b>" + hf_TenChuyenDe.Value + "</b> hiện đã nhận đăng ký vượt mức tối đa. VACPA không thể chấp nhận đăng ký của anh/chị.<br />";
            content += "Để đảm bảo chất lượng học tập, VACPA mời anh chị theo dõi và đăng ký tham gia các lớp học khác của VACPA tổ chức.<br />";
            content += "Xin cảm ơn!";
            utility.SendEmail(new List<string> { email }, subject, content);
            cm.GuiThongBao(hoiVienCaNhanId, loaihv, subject, content);
            cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Xóa bản ghi đăng ký lớp học học viên có ID = " + idLopHocHocVien + " trong danh mục " + tenchucnang);
            cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Gửi thư thông báo từ chối bản ghi đăng ký học cá nhân có ID = " + idLopHocHocVien);
        }
    }
    protected void lbtApproveAll_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            for (int i = 0; i < rpDanhSachDangKyHoc.Items.Count; i++)
            {
                HtmlInputCheckBox checkbox = rpDanhSachDangKyHoc.Items[i].FindControl("checkbox") as HtmlInputCheckBox;
                if (checkbox != null && checkbox.Checked && checkbox.Visible)
                    Approve(checkbox.Value);
            }
            LoadDangKyHoc(_lopHocId, ddlMaChuyenDe.SelectedValue);
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }
    protected void lbtRejectAll_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            for (int i = 0; i < rpDanhSachDangKyHoc.Items.Count; i++)
            {
                HtmlInputCheckBox checkbox = rpDanhSachDangKyHoc.Items[i].FindControl("checkbox") as HtmlInputCheckBox;
                if (checkbox != null && checkbox.Checked && checkbox.Visible)
                    Reject(checkbox.Value);
            }
            LoadDangKyHoc(_lopHocId, ddlMaChuyenDe.SelectedValue);
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }
}