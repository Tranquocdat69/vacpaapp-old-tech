﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyLopHoc_Add.ascx.cs"
    Inherits="usercontrols_CNKT_QuanLyLopHoc_Add" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<form id="form_CreateLopHoc" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Tạo lớp đào tạo, cập nhật kiến thức</h4>
<div id="thongbaoloi_form_themlophoc" name="thongbaoloi_form_themlophoc" style="display: none;
    margin-top: 5px;" class="alert alert-error">
</div>
<input type="hidden" id="hdIsThoaiDuyet" name="hdIsThoaiDuyet" />
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td>
                Loại lớp học:
            </td>
            <td colspan="5">
                <input type="radio" value="CNKT" id="rbLoaiLopHoc_CNKT" name="rbLoaiLopHoc" checked="checked" />Cập
                nhật kiến thức
                <input type="radio" value="DT" id="rbLoaiLopHoc_DT" name="rbLoaiLopHoc" />Đào tạo
            </td>
        </tr>
        <tr>
            <td>
                Mã lớp học<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" name="txtMaLopHoc" id="txtMaLopHoc" />
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
            <td>
            </td>
            <td>
                Ngày tạo<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" name="txtNgayTao" id="txtNgayTao" />
            </td>
            <td>
                <img id="imgCalendarNgayTao" src="/images/icons/calendar.png" />
            </td>
        </tr>
        <tr>
            <td>
                Tên lớp học<span class="starRequired">(*)</span>:
            </td>
            <td colspan="4">
                <input type="text" name="txtTenLopHoc" id="txtTenLopHoc" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Từ ngày<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" name="txtTuNgay" id="txtTuNgay" />
            </td>
            <td>
                <img id="imgCalendarTuNgay" src="/images/icons/calendar.png" />
            </td>
            <td>
                Đến ngày<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" name="txtDenNgay" id="txtDenNgay" />
            </td>
            <td>
                <img id="imgCalendarDenNgay" src="/images/icons/calendar.png" />
            </td>
        </tr>
        <tr>
            <td>
                Hạn đăng ký<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" name="txtHanDangKy" id="txtHanDangKy" />
            </td>
            <td>
                <img id="imgCalendarHanDangKy" src="/images/icons/calendar.png" />
            </td>
            <td>
                Tỉnh thành<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select name="ddlTinhThanh" id="ddlTinhThanh">
                    <option value="01">TP.Hà Nội</option>
                    <option value="79">TP.Hồ Chí Minh</option>
                    <option value="48">TP.Đà Nẵng</option>
                </select>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Địa chỉ lớp học<span class="starRequired">(*)</span>:
            </td>
            <td colspan="4">
                <input type="text" name="txtDiaChiLopHoc" id="txtDiaChiLopHoc" style="width: 100%;" />
            </td>
            <td>
            </td>
        </tr>
    </table>
</fieldset>
<div id="DivPhiThanhToan">
    <fieldset class="fsBlockInfor">
        <legend>Phí thanh toán (*)</legend>
        <div style="max-height: 200px; overflow: scroll;">
            <table id="tblPhiThanhToan" style="width: 860px;" border="0" class="formtbl">
                <tr>
                    <th style="width: 50px;">
                        Số ngày
                    </th>
                    <th style="width: 180px;">
                        Hội viên chính thức
                    </th>
                    <th style="width: 180px;">
                        Hội viên liên kết
                    </th>
                    <th style="width: 180px;">
                        Hội viên danh dự
                    </th>
                    <th style="width: 180px;">
                        Kiểm toán viên
                    </th>
                    <th style="width: 180px;">
                        Người quan tâm là trợ lý KTV
                    </th>
                    <th style="width: 180px;">
                        Người quan tâm không là trợ lý KTV
                    </th>
                </tr>
            </table>
        </div>
    </fieldset>
</div>
<%--</form>
<form id="FormChuyenDe" clientidmode="Static" method="post" enctype="multipart/form-data">--%>
<div id="DivQuanLyChuyenDe">
    <fieldset class="fsBlockInfor">
        <legend>Quản lý chuyên đề</legend>
        <div id="AlertMessageChuyenDe">
        </div>
        <div id="thongbaoloi_form_themchuyende" name="thongbaoloi_form_themchuyende" style="display: none;
            margin-top: 5px;" class="alert alert-error">
        </div>
        <div id="DivChuyenDe">
            <fieldset class="fsBlockInfor">
                <legend>Thông tin chuyên đề</legend>
                <table id="tblThongTinChuyenDe" width="700px" border="0" class="formtbl">
                    <tr>
                        <td>
                            Mã chuyên đề:
                        </td>
                        <td>
                            <input type="text" name="txtMaChuyenDe" id="txtMaChuyenDe" readonly="readonly" />
                            <input type="hidden" name="hdChuyenDeID" id="hdChuyenDeID" />
                            <input type="hidden" name="hdIndexInList" id="hdIndexInList" />
                        </td>
                        <td>
                        </td>
                        <td>
                            Loại chuyện đề<span class="starRequired">(*)</span>:
                        </td>
                        <td>
                            <select name="ddlLoaiChuyenDe" id="ddlLoaiChuyenDe">
                                <option value="<%= ListName.Type_LoaiChuyenDe_KeToanKiemToan %>">Kế toán, kiểm toán</option>
                                <option value="<%= ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep %>">Đạo đức nghề nghiệp</option>
                                <option value="<%= ListName.Type_LoaiChuyenDe_ChuyenDeKhac %>">Chuyên đề khác</option>
                            </select>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tên chuyên đề<span class="starRequired">(*)</span>:
                        </td>
                        <td colspan="4">
                            <input type="text" name="txtTenChuyenDe" id="txtTenChuyenDe" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Từ ngày<span class="starRequired">(*)</span>:
                        </td>
                        <td>
                            <input type="text" id="txtTuNgay_CD" name="txtTuNgay_CD" />
                        </td>
                        <td>
                            <img id="imgCalendarTuNgay_CD" src="/images/icons/calendar.png" />
                        </td>
                        <td>
                            Đến ngày<span class="starRequired">(*)</span>:
                        </td>
                        <td>
                            <input type="text" id="txtDenNgay_CD" name="txtDenNgay_CD" />
                        </td>
                        <td>
                            <img id="imgCalendarDenNgay_CD" src="/images/icons/calendar.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Chọn lịch học:
                        </td>
                        <td colspan="5">
                            <div style="max-height: 200px; overflow: scroll;">
                                <table id="tblLichHocChuyenDe" style="width: 100%;" border="0" class="formtbl">
                                    <tr>
                                        <th>
                                            Ngày
                                        </th>
                                        <th>
                                            Sáng
                                        </th>
                                        <th>
                                            Chiều
                                        </th>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Số buổi<span class="starRequired">(*)</span>:
                        </td>
                        <td>
                            <input type="text" name="txtSoBuoi_CD" id="txtSoBuoi_CD" readonly="readonly" />
                        </td>
                        <td>
                        </td>
                        <td>Số người học tối đa<span class="starRequired">(*)</span>:
                        </td>
                        <td>
                            <input type='text' id="txtSoNguoiHocToiDa" name="txtSoNguoiHocToiDa"/>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Giảng viên<span class="starRequired">(*)</span>:
                        </td>
                        <td>
                            <input type="text" name="txtGiangVien" id="txtGiangVien" readonly="readonly" />
                            <input type="hidden" name="hdGiangVienID" id="hdGiangVienID" />
                        </td>
                        <td>
                            <input type="button" value="---" onclick="OpenFormDMGiangVien();" style="border: 1px solid gray;" />
                        </td>
                        <td>
                            Chức vụ:
                        </td>
                        <td>
                            <span id="spanChucVu"></span>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trình độ chuyên môn:
                        </td>
                        <td>
                            <span id="spanTrinhDoChuyenMon"></span>
                        </td>
                        <td>
                        </td>
                        <td>
                            Đơn vị công tác:
                        </td>
                        <td>
                            <span id="spanDonViCongTac"></span>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Chứng chỉ:
                        </td>
                        <td>
                            <span id="spanChungChi"></span>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <div style="width: 100%; margin: 10px; text-align: center;">
                <a id="btnSaveChuyenDe" href="javascript:;" class="btn btn-rounded"><i class="iconfa-save">
                </i>Lưu</a> <a id="btnDeleteChuyenDe" href="javascript:;" class="btn btn-rounded"
                    onclick="CallMethodDeleteChuyenDe();"><i class="iconfa-remove-circle"></i>Xóa</a>
            </div>
            <fieldset class="fsBlockInfor">
                <legend>Danh sách chuyên đề</legend>
                <table id="tblDanhSachChuyenDe" width="100%" border="0" class="formtbl">
                    <tr>
                        <th>
                            <input type="checkbox" class="checkall" value="all" />
                        </th>
                        <th>
                            STT
                        </th>
                        <th>
                            Mã CĐ
                        </th>
                        <th>
                            Tên chuyên đề
                        </th>
                        <th>
                            Loại
                        </th>
                        <th>
                            Giảng viên
                        </th>
                        <th>
                            Trình độ chuyên môn
                        </th>
                        <th>
                            Đơn vị công tác
                        </th>
                        <th>
                            Chức vụ
                        </th>
                    </tr>
                </table>
            </fieldset>
        </div>
    </fieldset>
</div>
<div style="width: 100%; margin: 10px; text-align: center;">
    <a id="btn_save" href="javascript:;" class="btn btn-rounded" onclick="SaveLopHoc();">
        <i class="iconfa-save"></i>Lưu</a> <span id="spanCacNutChucNang"><a href="javascript:;"
            id="btnDelete" class="btn" onclick="DeleteLopHoc();"><i class="iconfa-trash">
            </i>Xóa</a>
            <asp:LinkButton ID="lbtDuyet" runat="server" CssClass="btn" OnClientClick="return confirm('Bạn chắc chắn muốn duyệt bản ghi này chứ?');"
        onclick="lbtDuyet_Click"><i class="iconfa-thumbs-up"></i>Duyệt</asp:LinkButton>   
    <asp:LinkButton ID="lbtTuChoi" runat="server" CssClass="btn"  OnClientClick="return confirm('Bạn chắc chắn muốn từ chối bản ghi này chứ?');"
        onclick="lbtTuChoi_Click"><i class="iconfa-remove-sign"></i>Từ chối</asp:LinkButton> 
        <asp:HyperLink ID="lbtExport" runat="server" CssClass="btn" Visible="False" NavigateUrl="/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc" Target="_blank"><i class="iconfa-download"></i>Kết xuất</asp:HyperLink>&nbsp;
        <a href="javascript:;" id="btnSendEmail" class="btn" onclick="OpenViewEmailPopup();">
                <i class="iconfa-inbox"></i>Gửi thư mời</a> </span>
</div>
</form>
<iframe name="iframeProcessChuyenDe" width="0px" height="0px"></iframe>
<script type="text/javascript">
<% CheckPermissionOnPage(); %>
    var _arrDataPhiThanhToan = [];
    function DrawRow_PhiThanhToan(arrData, flagAllowEdit) {

        var readonly = "";
        //if(parseInt(flagAllowEdit) == 0)
        //    readonly = "readonly='readonly'";
        var tbl = document.getElementById('tblPhiThanhToan');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        
        for (var i = 0; i < arrData.length; i++) {

            var soNgay = arrData[i][0];
            var hoiVienCaNhanCT = arrData[i][1];
            var hoiVienCaNhanLK = arrData[i][2];
            var hoiVienCaNhanDD = arrData[i][3];
            var ktv = arrData[i][4];
            var troLyKtv = arrData[i][5];
            var koPhaiTroLyKtv = arrData[i][6];

            var tr = document.createElement('TR');

            var tdId = document.createElement("TD");
            tdId.innerHTML = soNgay;
            tr.appendChild(tdId);
            
            var tdHoiVienCaNhan_CT = document.createElement("TD");
            tdHoiVienCaNhan_CT.innerHTML = "<input type='text' id='txtHoiVienCaNhanCT" + i + "' name='txtHoiVienCaNhanCT" + i + "' " + readonly + " onblur='DisplayTypeNumber(this);' onfocus='UnDisplayTypeNumber(this);' style='text-align:right;' value='" + hoiVienCaNhanCT + "' class='required digitsWithOutSeparatorCharacterVN' />";
            tr.appendChild(tdHoiVienCaNhan_CT);

            var tdHoiVienCaNhan_LK = document.createElement("TD");
            tdHoiVienCaNhan_LK.innerHTML = "<input type='text' id='txtHoiVienCaNhanLK" + i + "' name='txtHoiVienCaNhanLK" + i + "' " + readonly + " onblur='DisplayTypeNumber(this);' onfocus='UnDisplayTypeNumber(this);' style='text-align:right;' value='" + hoiVienCaNhanLK + "' class='required digitsWithOutSeparatorCharacterVN' />";
            tr.appendChild(tdHoiVienCaNhan_LK);

            var tdHoiVienCaNhan_DD = document.createElement("TD");
            tdHoiVienCaNhan_DD.innerHTML = "<input type='text' id='txtHoiVienCaNhanDD" + i + "' name='txtHoiVienCaNhanDD" + i + "' " + readonly + " onblur='DisplayTypeNumber(this);' onfocus='UnDisplayTypeNumber(this);' style='text-align:right;' value='" + hoiVienCaNhanDD + "' class='required digitsWithOutSeparatorCharacterVN' />";
            tr.appendChild(tdHoiVienCaNhan_DD);

            var tdKTV = document.createElement("TD");
            tdKTV.innerHTML = "<input type='text' id='txtKTV"+i+"' name='txtKTV"+i+"' "+readonly+" onblur='DisplayTypeNumber(this);' onfocus='UnDisplayTypeNumber(this);' style='text-align:right;' value='"+ktv+"'  class='required digitsWithOutSeparatorCharacterVN' />";
            tr.appendChild(tdKTV);
            
            var tdTroLyKTV = document.createElement("TD");
            tdTroLyKTV.innerHTML = "<input type='text' id='txtTroLyKTV"+i+"' name='txtTroLyKTV"+i+"' "+readonly+" onblur='DisplayTypeNumber(this);' onfocus='UnDisplayTypeNumber(this);' style='text-align:right;' value='"+troLyKtv+"'  class='required digitsWithOutSeparatorCharacterVN' />";
            tr.appendChild(tdTroLyKTV);
            
            var tdKoPhaiTroLyKTV = document.createElement("TD");
            tdKoPhaiTroLyKTV.innerHTML = "<input type='text' id='txtKoPhaiTroLyKTV"+i+"' name='txtKoPhaiTroLyKTV"+i+"' "+readonly+" onblur='DisplayTypeNumber(this);' onfocus='UnDisplayTypeNumber(this);' style='text-align:right;' value='"+koPhaiTroLyKtv+"'  class='required digitsWithOutSeparatorCharacterVN' />";
            tr.appendChild(tdKoPhaiTroLyKTV);

            tbl.appendChild(tr);
        }
    }

    // add open Datepicker event for calendar inputs
    $("#txtDenNgay").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarDenNgay").click(function () {
        $("#txtDenNgay").datepicker("show");
    });

    $("#txtTuNgay").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarTuNgay").click(function () {
        $("#txtTuNgay").datepicker("show");
    });

    $("#txtNgayTao").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarNgayTao").click(function () {
        $("#txtNgayTao").datepicker("show");
    });

    $("#txtHanDangKy").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarHanDangKy").click(function () {
        $("#txtHanDangKy").datepicker("show");
    });    

    $("#txtTuNgay_CD").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarTuNgay_CD").click(function () {
        $("#txtTuNgay_CD").datepicker("show");
    });
    
    $("#txtDenNgay_CD").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarDenNgay_CD").click(function () {
        $("#txtDenNgay_CD").datepicker("show");
    });

    function OpenFormDMGiangVien() {
        $("#divDanhSachGiangVien").empty();
        $("#divDanhSachGiangVien").append($("<iframe width='100%' height='100%' id='ifDanhSachGiangVien' name='ifDanhSachGiangVien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmgiangvien_list"));
        $("#divDanhSachGiangVien").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách giảng viên</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    function CloseFormDMGiangVien() {
        $("#divDanhSachGiangVien").dialog("close");
    }

    $('#txtTuNgay_CD, #txtDenNgay_CD, #txtSoBuoi_CD, #txtTuNgay, #txtDenNgay, #txtHanDangKy').change(function() {
        ValidateLogic_LopHoc();
        ValidateLogic_ChuyenDe(true);
    });
    
    function ValidateLogic_LopHoc() {
        var startLopHoc = $('#txtTuNgay').datepicker('getDate');
        var endLopHoc = $('#txtDenNgay').datepicker('getDate');
        var hanDangKy = $('#txtHanDangKy').datepicker('getDate');
        
        if(startLopHoc != null && endLopHoc != null) {
            endLopHoc.setDate(endLopHoc.getDate() + 1);
            var minus = (endLopHoc - startLopHoc) / 1000 / 60 / 60 / 24;
            if(minus <= 0){
                $('#txtDenNgay').val('');
                $('#txtDenNgay').focus();
                alert('Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc!');
            }
        }
        
        if(startLopHoc != null && hanDangKy != null && hanDangKy >= startLopHoc) {
            $('#txtHanDangKy').val('');
                $('#txtHanDangKy').focus();
                alert('Hạn đăng ký không được vượt qua ngày bắt đầu lớp học!');
        }
    }

    function ValidateLogic_ChuyenDe(IsGenBangLichHocChuyenDe) {
        var flag = true;
        var startLopHoc = $('#txtTuNgay').datepicker('getDate');
        var endLopHoc = $('#txtDenNgay').datepicker('getDate');
        var start = $('#txtTuNgay_CD').datepicker('getDate');
        var end   = $('#txtDenNgay_CD').datepicker('getDate');
        var soBuoi = $('#txtSoBuoi_CD').val();
        
        // Phần bắt lỗi cho các control ở khung "Chuyên đề"
        if(start != null && ((startLopHoc != null && startLopHoc > start) || (endLopHoc != null && endLopHoc < start))){
            $('#txtTuNgay_CD').focus();
            $('#txtTuNgay_CD').val('');
            if(flag)
                alert('Ngày bắt đầu chuyên đề đã vượt quá thời gian lớp học!');
            flag = false;
        }
        if(end != null && (startLopHoc > end || endLopHoc < end)){
            $('#txtDenNgay_CD').focus();
            $('#txtDenNgay_CD').val('');
            if(flag)
                alert('Ngày kết thúc chuyên đề đã vượt quá thời gian lớp học!');
            flag = false;
        }
        if(start != null && end != null){
        end.setDate(end.getDate() + 1);
            var minus = (end - start) / 1000 / 60 / 60 / 24;
            if(minus <= 0){
                $('#txtDenNgay_CD').val('');
                $('#txtDenNgay_CD').focus();
                if(flag)
                    alert('Ngày bắt đầu chuyên đề phải nhỏ hơn hoặc bằng ngày kết thúc chuyên đề!');
                flag = false;
            }
            if(soBuoi != null && soBuoi != '' && !isNaN(soBuoi)) {
                if(parseFloat(soBuoi) > (minus*2)){
                    $('#txtSoBuoi_CD').val('');
                    if(flag)
                        alert('Số buổi phải nhỏ hơn (số ngày đào tạo x 2)!');
                    flag = false;
                }
            }
        }
        
        // Chỉ tạo bảng khi thay đổi giá trị ngày tháng (Khi bấm save sẽ ko tạo lại bảng này)
        if(IsGenBangLichHocChuyenDe)
            GenBangLichHocChuyenDe(new Array());
        return flag;
    }
    
    $('#txtTuNgay, #txtDenNgay').change(function() {
        ValidateBangThanhToanPhiTheoThoiGianLopHoc();
    });

    function ValidateBangThanhToanPhiTheoThoiGianLopHoc() {
        var startLopHoc = $('#txtTuNgay').datepicker('getDate');
        var endLopHoc = $('#txtDenNgay').datepicker('getDate');
        if(startLopHoc != null && endLopHoc != null) {
            endLopHoc.setDate(endLopHoc.getDate() + 1);
            if(endLopHoc > startLopHoc) {
                var count = ((endLopHoc - startLopHoc) / 1000 / 60 / 60 / 24) * 2;
                var tempArrDataPhiThanhToan = [];
                if(count <= _arrDataPhiThanhToan.length) { // Trường hợp số ngày vừa đổi ít hơn số đã save
                    for (var i = 0; i < count; i++) {
                        tempArrDataPhiThanhToan.push(_arrDataPhiThanhToan[i]);
                    }
                } else { // Trường hợp số ngày vừa đổi nhiều hơn số đã save
                    tempArrDataPhiThanhToan = _arrDataPhiThanhToan;
                    for (var i = (_arrDataPhiThanhToan.length + 1); i <= count; i++) {
                        tempArrDataPhiThanhToan.push([(i/2), '', '', '', '','','']);   
                    }
                }
                DrawRow_PhiThanhToan(tempArrDataPhiThanhToan, 1);
            }
        }
    }
    
    // Tính số ngày học dựa theo ngày bắt đầu và kết thúc chuyên đề
    function GenBangLichHocChuyenDe(arrDataLichHoc) {
        var tempArrDataLichHoc = [];
        var startChuyenDe = $('#txtTuNgay_CD').datepicker('getDate');
        var endChuyenDe = $('#txtDenNgay_CD').datepicker('getDate');

        if (startChuyenDe != null && endChuyenDe != null) {
            endChuyenDe.setDate(endChuyenDe.getDate() + 1);
            if (endChuyenDe > startChuyenDe) {
                var count = ((endChuyenDe - startChuyenDe) / 1000 / 60 / 60 / 24);
                for (var i = 0; i < count; i++) {

                    var ngayHoc = new Date(startChuyenDe.getFullYear() + '-' + pad(startChuyenDe.getMonth() + 1, 2) + '-' + pad(startChuyenDe.getDate(), 2));

                    ngayHoc.setDate(ngayHoc.getDate() + i);

                    var ngay = ngayHoc.getDate();
                    var thang = ngayHoc.getMonth() + 1;

                    if (thang < 10)
                        thang = '0' + thang;
                    if ((ngayHoc.getDate()) < 10)
                        ngay = '0' + ngayHoc.getDate();
                    var strNgayHoc = ngay + "/" + thang + "/" + ngayHoc.getFullYear();

                    // Kiểm tra để lấy dữ liệu đã có: nếu có thì biến index = index của bản ghi trong mảng dữ liệu cũ
                    var index = -1;
                    for (var j = 0; j < arrDataLichHoc.length; j++) {
                        if (arrDataLichHoc[j][0] == strNgayHoc)
                            index = j;
                    }
                    if (index > -1)
                        tempArrDataLichHoc.push(arrDataLichHoc[index]);
                    else
                        tempArrDataLichHoc.push([strNgayHoc, '', '']);
                }
            }
        }
        DrawRow_LichHocChuyenDe(tempArrDataLichHoc);
    }
    
    function pad(value, length) {
        return (value.toString().length < length) ? pad("0" + value, length) : value;
    }

    // Đếm số buổi theo các ô checkbox trong lịch học
    function CountSoBuoiHocChuyenDe() {
        var countChecked = 0;
         $('#tblLichHocChuyenDe :checkbox').each(function () {
            var checked = $(this).prop("checked");
                if (checked){
                    countChecked++;
                }
            });
        if(countChecked > 0)
            $('#txtSoBuoi_CD').val(countChecked);
        else {
            $('#txtSoBuoi_CD').val('');
        }
    }
    
    // Hàm vẽ dữ liệu bảng lịch học chuyên đề
    function DrawRow_LichHocChuyenDe(arrData) {
        var tbl = document.getElementById('tblLichHocChuyenDe');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        
        for (var i = 0; i < arrData.length; i++) {

            var ngay = arrData[i][0];
            var sang = arrData[i][1];
            var chieu = arrData[i][2];

            var tr = document.createElement('TR');
            var tdNgay = document.createElement("TD");
            tdNgay.innerHTML = "<span id='spanNgay"+i+"' class='spanNgayLichHocChuyenDe'>" + ngay + "</span>";
            tr.appendChild(tdNgay);

            var khoaBuoiSang = '';
            var khoaBuoiChieu = '';
            
            //if($('#hdIsThoaiDuyet').val() == "1") {
            //    khoaBuoiSang = "disabled='disabled'";
            //    khoaBuoiChieu = "disabled='disabled'";
            //}
            
            var indexInList = $('#hdIndexInList').val();
            //if(indexInList.length > 0) { // Trường hợp đang cập nhật 1 chuyên đề
            //    for (var j = 0; j < _arrDataDanhSachChuyenDe.length; j++) {
            //        if(indexInList == j) // Nếu đúng chuyên đề đang sửa thì bỏ qua
            //            continue;
            //        // Chuyển chuỗi giá trị lịch học chuyên đề thành dạng []
            //        var fullLichHoc = _arrDataDanhSachChuyenDe[j][14];
            //        var arrFullLichHoc = fullLichHoc.split(';#');
            //        for (var x = 0; x < arrFullLichHoc.length; x = x + 3) {
            //            if(arrFullLichHoc[x].length > 0) {
            //                if(ngay == arrFullLichHoc[x]) {
            //                    if(arrFullLichHoc[x + 1] == "1")
            //                        khoaBuoiSang = "disabled='disabled'";
            //                    if(arrFullLichHoc[x + 2] == "1")
            //                        khoaBuoiChieu = "disabled='disabled'";
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //} else { // Trường hợp tạo mới chuyên đề
            //    for (var j = 0; j < _arrDataDanhSachChuyenDe.length; j++) {
            //        // Chuyển chuỗi giá trị lịch học chuyên đề thành dạng []
            //        var fullLichHoc = _arrDataDanhSachChuyenDe[j][14];
            //        var arrFullLichHoc = fullLichHoc.split(';#');
            //        for (var x = 0; x < arrFullLichHoc.length; x = x + 3) {
            //            if(arrFullLichHoc[x].length > 0) {
            //                if(ngay == arrFullLichHoc[x]) {
            //                    if(arrFullLichHoc[x + 1] == "1")
            //                        khoaBuoiSang = "disabled='disabled'";
            //                    if(arrFullLichHoc[x + 2] == "1")
            //                        khoaBuoiChieu = "disabled='disabled'";
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //}
            
            var tdSang = document.createElement("TD");
            tdSang.style.textAlign = 'center';
            if(sang == "1")
                tdSang.innerHTML = "<input type='checkbox' id='cboSang"+i+"' name='cboSang"+i+"' checked='checked' "+khoaBuoiSang+" value='1' onclick='CountSoBuoiHocChuyenDe();' />";
            else {
                tdSang.innerHTML = "<input type='checkbox' id='cboSang"+i+"' name='cboSang"+i+"'  value='1' "+khoaBuoiSang+" onclick='CountSoBuoiHocChuyenDe();' />";
            }
            tr.appendChild(tdSang);
            
            var tdChieu = document.createElement("TD");
            tdChieu.style.textAlign = 'center';
            if(chieu == "1")
                tdChieu.innerHTML = "<input type='checkbox' id='cboChieu"+i+"' name='cboChieu"+i+"' checked='checked' "+khoaBuoiChieu+" value='1' onclick='CountSoBuoiHocChuyenDe();' />";
            else
                tdChieu.innerHTML = "<input type='checkbox' id='cboChieu"+i+"' name='cboChieu"+i+"' "+khoaBuoiChieu+" value='1' onclick='CountSoBuoiHocChuyenDe();' />";
            tr.appendChild(tdChieu);

            tbl.appendChild(tr);
        }
        CountSoBuoiHocChuyenDe();
    }
    
    function DisplayInforGiangVien(value) {
        var arrValue = value.split(';#');
        $('#hdGiangVienID').val(arrValue[0]);
        $('#txtGiangVien').val(arrValue[1]);
        $('#spanChucVu').html(arrValue[2]);
        $('#spanTrinhDoChuyenMon').html(arrValue[3]);
        $('#spanDonViCongTac').html(arrValue[4]);
        $('#spanChungChi').html(arrValue[5]);
        $("#divDanhSachGiangVien").dialog("close");
    }
    
    function SaveChuyenDe() {
        var chuyenDeId = $('#hdChuyenDeID').val();
        var maChuyenDe = $('#txtMaChuyenDe').val();
        var loaiChuyenDe = $('#ddlLoaiChuyenDe').val();
        var tenChuyenDe = $('#txtTenChuyenDe').val();
        var tuNgay = $('#txtTuNgay_CD').val();
        var denNgay = $('#txtDenNgay_CD').val();
        var soBuoi = $('#txtSoBuoi_CD').val();
        var giangVienID = $('#hdGiangVienID').val();
        var tenGiangVien = $('#txtGiangVien').val();
        var trinhDoChuyenMon = $('#spanTrinhDoChuyenMon').html();
        var donViCongTac = $('#spanDonViCongTac').html();
        var chucVu = $('#spanChucVu').html();
        var chungChi = $('#spanChungChi').html();
        var maLopHoc = $('#txtMaLopHoc').val();
        var soNguoiHocToiDa = $('#txtSoNguoiHocToiDa').val();
        
        var tenLoaiChuyenDe = '';
        if(loaiChuyenDe == "1")
            tenLoaiChuyenDe = "Kế toán, kiểm toán";
        if(loaiChuyenDe == "2")   
            tenLoaiChuyenDe = "Đạo đức nghề nghiệp";
        if(loaiChuyenDe == "3")
            tenLoaiChuyenDe = "Chuyên đề khác";

        var fullLichHoc = '';
        $('#tblLichHocChuyenDe .spanNgayLichHocChuyenDe').each(function() {
            var id = $(this).attr('id');
            var flagSang = '', flagChieu = '';
            if($('#cboSang' + id.split('spanNgay')[1]).prop('checked'))
                flagSang = '1';
            if($('#cboChieu' + id.split('spanNgay')[1]).prop('checked'))
                flagChieu = '1';
            fullLichHoc += $(this).html() + ";#" + flagSang + ";#" + flagChieu + ";#";
        });

        var indexInList = $('#hdIndexInList').val();
        var _tempArrDataDanhSachChuyenDe = [];
        if(indexInList.length > 0) // Update
        {
            var arrDataOneRow = [chuyenDeId, maChuyenDe, tenChuyenDe, loaiChuyenDe, tenLoaiChuyenDe, tuNgay, denNgay, soBuoi, giangVienID, tenGiangVien, trinhDoChuyenMon, donViCongTac, chucVu, chungChi, fullLichHoc, soNguoiHocToiDa];
            for (var i = 0; i < _arrDataDanhSachChuyenDe.length; i++) {
                if(i != parseInt(indexInList))
                    _tempArrDataDanhSachChuyenDe.push(_arrDataDanhSachChuyenDe[i]);
                else {
                    _tempArrDataDanhSachChuyenDe.push(arrDataOneRow);
                }
            }
        } else // Add
        {
            var arrDataOneRow = [chuyenDeId, maChuyenDe, tenChuyenDe, loaiChuyenDe, tenLoaiChuyenDe, tuNgay, denNgay, soBuoi, giangVienID, tenGiangVien, trinhDoChuyenMon, donViCongTac, chucVu, chungChi, fullLichHoc, soNguoiHocToiDa];
            for (var i = 0; i < _arrDataDanhSachChuyenDe.length; i++) {
                _tempArrDataDanhSachChuyenDe.push(_arrDataDanhSachChuyenDe[i]);
            }
            _tempArrDataDanhSachChuyenDe.push(arrDataOneRow);
        }
        
        DrawData_ChuyenDe(_tempArrDataDanhSachChuyenDe);
    }

    function DisplayAlertMessage_ChuyenDe(type, msg) {
        var html = "";
        if(type == 1)
            html = "<div class='alert alert-success' style=''><button class='close' type='button' >×</button>"+msg+" thành công!</div>";
        if(type == 0)
            html = "<div class='alert alert-error' style=''><button class='close' type='button' >×</button>"+msg+" thất bại!</div>";
        $('#AlertMessageChuyenDe').html(html);
    }
    
    function CallMethodDeleteChuyenDe() {
        if(_danhSachChuyenDen_selected.length > 0) {
            var tempArrDataDanhSachChuyenDe = [];
            for (var i = 0; i < _arrDataDanhSachChuyenDe.length; i++) {
                if(_danhSachChuyenDen_selected.indexOf(i) < 0)
                    tempArrDataDanhSachChuyenDe.push(_arrDataDanhSachChuyenDe[i]);
            }
            DrawData_ChuyenDe(tempArrDataDanhSachChuyenDe);
        }
            //window.iframeProcessChuyenDe.DeleteChuyenDe(_danhSachChuyenDen_selected);
        else
            alert('Chưa chọn bản ghi để xóa!');
    }

    var _danhSachChuyenDen_selected = new Array();

    $('#FormChuyenDe').validate({
        rules: {
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form_themchuyende").html(e).show()

            } else {
                jQuery("#thongbaoloi_form_themchuyende").hide()
            }
        }  
    });

    jQuery(document).ready(function () {
        // dynamic table      

        $("#tblDanhSachChuyenDe .checkall").bind("click", function () {
            _danhSachChuyenDen_selected = new Array();
            checked = $(this).prop("checked");
            ResetControl_ChuyenDe();
            $('#tblDanhSachChuyenDe :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) _danhSachChuyenDen_selected.push(parseInt($(this).val().split(',')[0]));
            });
        });

        $("#btnSaveChuyenDe").click(function() {
            var countChecked = 0;
            $('#tblDanhSachChuyenDe :checkbox').each(function () {
                var checked = $(this).prop("checked");
                    if ((checked) && ($(this).val() != "all")){
                        countChecked++;
                    }
                });
            if(countChecked > 1) {
                alert('Chỉ được chọn 1 bản ghi để cập nhật!');
                return;
            }
            if($("#<%=form_CreateLopHoc.ClientID %>").valid()){
                if(ValidateLogic_ChuyenDe(false)){
                    SaveChuyenDe();
                    jQuery("#thongbaoloi_form_themchuyende").hide();
                    return false;
                }
            } else {
                return false;
            }
        });
    });

    var _arrDataDanhSachChuyenDe = [];
    function DrawData_ChuyenDe(arrData) {
        parent.ResetControl_ChuyenDe();
        var tbl = document.getElementById('tblDanhSachChuyenDe');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            _arrDataDanhSachChuyenDe = arrData;
            for(var i = 0; i < arrData.length; i ++) {
                var chuyenDeID = arrData[i][0];
                var maChuyenDe = arrData[i][1];
                var tenChuyenDe = arrData[i][2];
                var loaiChuyenDe = arrData[i][3];
                var tenLoaiChuyenDe = arrData[i][4];
                var tuNgay = arrData[i][5];
                var denNgay = arrData[i][6];
                var soBuoi = arrData[i][7];
                var giangVienID = arrData[i][8];
                var tenGiangVien = arrData[i][9];
                var trinhDoChuyenMon = arrData[i][10];
                var donViCongTac = arrData[i][11];
                var chucVu = arrData[i][12];
                
                var fullLichHoc = arrData[i][14];
                var soNguoiHocToiDa = arrData[i][15];
                
                var tr = document.createElement('TR');
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                var checkbox = document.createElement('input');
                checkbox.type = "checkbox";
                checkbox.value = i + ',' + chuyenDeID;
                checkbox.onclick = function() { GetOneChuyenDe(arrData); };
                tdCheckBox.appendChild(checkbox);
                
                // HiddenField để chứa các giá trị của chuyên đề
                tr.appendChild(tdCheckBox); 

                var arrColumn = [(i + 1), maChuyenDe, tenChuyenDe, tenLoaiChuyenDe, tenGiangVien, trinhDoChuyenMon, donViCongTac, chucVu];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    if(j == 0) {
                        td.innerHTML += '<input type="hidden" name="hfValueChuyenDe'+i+'" value="'+ chuyenDeID + ';#' + loaiChuyenDe + ';#' + tenChuyenDe + ';#' + tuNgay + ';#' + denNgay + ';#' + soBuoi + ';#' + giangVienID +';#'+soNguoiHocToiDa+'" />';
                        td.innerHTML += '<input type="hidden" name="hfValueLichHoc'+i+'" value="'+ fullLichHoc + '" />';
                    }
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
    }

    function GetOneChuyenDe(arrData) {
        var countChecked = 0;
        var tempObj;
        $('#tblDanhSachChuyenDe :checkbox').each(function () {
            removeItem(_danhSachChuyenDen_selected, parseInt($(this).val().split(',')[0]));
            var checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")){
                    countChecked++;
                    tempObj = this;
                    _danhSachChuyenDen_selected.push(parseInt($(this).val().split(',')[0]));
                }
            });
        if(countChecked == 1 && arrData.length > 0) {
            var value = $(tempObj).val().split(',')[0];
            $('#hdIndexInList').val(value);
            $('#hdChuyenDeID').val(arrData[value][0]);
            $('#txtMaChuyenDe').val(arrData[value][1]);
            $('#txtTenChuyenDe').val(arrData[value][2]);
            $('#ddlLoaiChuyenDe').val(arrData[value][3]);
            $('#txtTuNgay_CD').val(arrData[value][5]);
            $('#txtDenNgay_CD').val(arrData[value][6]);
            $('#txtSoBuoi_CD').val(arrData[value][7]);
            $('#txtGiangVien').val(arrData[value][9]);
            $('#hdGiangVienID').val(arrData[value][8]);
            $('#spanChucVu').html(arrData[value][12]);
            $('#spanTrinhDoChuyenMon').html(arrData[value][10]);
            $('#spanDonViCongTac').html(arrData[value][11]);
            $('#spanChungChi').html(arrData[value][13]);
            $('#txtSoNguoiHocToiDa').val(arrData[value][15]);
            
            // Chuyển chuỗi giá trị lịch học chuyên đề thành dạng []
            var fullLichHoc = arrData[value][14];
            var arrFullLichHoc = fullLichHoc.split(';#');
            var arrDataLichHoc = [];
            for (var i = 0; i < arrFullLichHoc.length; i = i + 3) {
                if(arrFullLichHoc[i].length > 0)
                    arrDataLichHoc.push([arrFullLichHoc[i], arrFullLichHoc[i + 1], arrFullLichHoc[i + 2]]);
            }
            GenBangLichHocChuyenDe(arrDataLichHoc);
        } else {
            ResetControl_ChuyenDe();
        }
    }
    
    function ResetControl_ChuyenDe() {
        $('#hdIndexInList').val('');
        $('#hdChuyenDeID').val('');
        $('#txtMaChuyenDe').val('');
        $('#txtTenChuyenDe').val('');
        $('#ddlLoaiChuyenDe').val($("#ddlLoaiChuyenDe option:first").val());
        $('#txtTuNgay_CD').val('');
        $('#txtDenNgay_CD').val('');
        $('#txtSoBuoi_CD').val('');
        $('#txtGiangVien').val('');
        $('#hdGiangVienID').val('');
        $('#spanChucVu').html('');
        $('#spanTrinhDoChuyenMon').html('');
        $('#spanDonViCongTac').html('');
        $('#spanChungChi').html('');
        $('#txtSoNguoiHocToiDa').val(soNguoiHocToiDa);
        GenBangLichHocChuyenDe(new Array());
    }

    $('#<%=form_CreateLopHoc.ClientID %>').validate({
        rules: {
            txtNgayTao: {
                required: true, dateITA: true
            },
            txtMaLopHoc: {
                required: true
            },
            txtTenLopHoc: {
                required: true
            },
            txtTuNgay: {
                required: true, dateITA: true
            },
            txtDenNgay: {
                required: true, dateITA: true
            },
            txtHanDangKy: {
                required: true, dateITA: true
            },
            txtDiaChiLopHoc: {
                required: true
            },
            txtTenChuyenDe: {
                required: true
            },
            txtTuNgay_CD: {
                required: true, dateITA: true
            },
            txtDenNgay_CD: {
                required: true, dateITA: true
            },
            txtSoBuoi_CD: {
                required: true, digits: true
            },
            txtGiangVien: {
                required: true
            },
            txtSoNguoiHocToiDa: {
                required: true, digits: true
            }    
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form_themlophoc").html(e).show();

            } else {
                jQuery("#thongbaoloi_form_themlophoc").hide();  
            }
        }
    });

    function SaveLopHoc() {
        if(_arrDataDanhSachChuyenDe.length == 0) {
            alert('Chưa có thông tin chuyên đề trong lớp học!');
            var e = "<button class='close' type='button' data-dismiss='alert'>×</button>Chưa có thông tin chuyên đề trong lớp học";
            jQuery("#thongbaoloi_form_themlophoc").html(e).show();
            return;
        } else {
            jQuery("#thongbaoloi_form_themlophoc").hide();
        }
        
        $('#hdAction').val('save');
        
        // bỏ validate các control ở phần chuyên đề trước khi save lớp học
        $('#txtTenChuyenDe').rules('remove');
        $('#txtTuNgay_CD').rules('remove');
        $('#txtDenNgay_CD').rules('remove');
        $('#txtSoBuoi_CD').rules('remove');
        $('#txtGiangVien').rules('remove');
        
        $('#<%=form_CreateLopHoc.ClientID %>').submit();
    }  
    
    function DeleteLopHoc() {
        if(confirm('Bạn chắc chắn muốn xóa lớp học này chứ?')){
            $('#hdAction').val('delete');
            jQuery('#<%=form_CreateLopHoc.ClientID %>').validate().currentForm = '';
            $('#<%=form_CreateLopHoc.ClientID %>').submit();
        }
    }  
    
    function OpenViewEmailPopup()
    {
        $("#divViewEmail").empty();
        $("#divViewEmail").append($("<iframe width='100%' height='100%' id='ifViewEmail' name='ifViewEmail' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyLopHoc_Email&idlh=" + getParameterByName('id')));
        $("#divViewEmail").dialog({
            resizable: true,
            width: 950,
            height: 750,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Nội dung Email mời tham dự lớp học</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Gửi Email": function () {
                    $('#hdAction').val('sendemail');
                    jQuery('#<%=form_CreateLopHoc.ClientID %>').validate().currentForm = '';
                    $('#<%=form_CreateLopHoc.ClientID %>').submit();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#divViewEmail').parent().find('button:contains("Gửi Email")').addClass('btn btn-rounded').prepend('<i class="iconfa-share"> </i>&nbsp;');
        $('#divViewEmail').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    
    <% LoadOldData(); %>
</script>
<div id="divDanhSachGiangVien">
</div>
<div id="divViewEmail">
</div>
