﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DAL;
using System.IO;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using System.Configuration;
using System.Net.Mail;

public partial class usercontrols_CNKT_cnkt_thongbao_add : System.Web.UI.UserControl
{
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request.Form["NoiDungThongBao"]))
        {

            SqlCommand sql = new SqlCommand();
            DataTable dtb = new DataTable();

            string noidung = Request.Form["NoiDungThongBao"];
            string tieude = Request.Form["TieuDeThongBao"];
            HttpPostedFile file = Request.Files["FileDinhKem"];
            
            string err = "Lỗi gửi email: ";

            List<string> lstEmail = new List<string>();
            sql.CommandText = "SELECT Email FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID IN (SELECT HoiVienCaNhanID FROM tblCNKTLopHocHocVien WHERE LopHocID = " + Request.QueryString["lophocid"] + " AND ChuyenDeID = " + Request.Form["drChuyenDe"] + ")";
            dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            foreach (DataRow row in dtb.Rows)
            {

                if (!string.IsNullOrEmpty(row["Email"].ToString()))
                {
                    lstEmail.Add(row["Email"].ToString());

                }
            }

            string msg = "";
            if (file.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(file.InputStream);
                byte[] fileByte = br.ReadBytes(file.ContentLength);
                MemoryStream ms = new MemoryStream(fileByte);
                Attachment at = new Attachment(ms, file.FileName);
                SmtpMail.SendMultiWithAttach("BQT WEB VACPA", lstEmail, tieude, noidung, at, ref msg);
            }
            else
                SmtpMail.SendMulti("BQT WEB VACPA", lstEmail, tieude, noidung, ref msg);
            if (msg != "Email đã được gửi thành công!")
                err += msg + ";";


            if (err != "Lỗi gửi email: ")
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + err + "</div>"));
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Gửi thông báo thành công</div>"));
        }

    }

    public void LoadChuyenDe()
    {
        SqlCommand sql = new SqlCommand();
        sql.CommandText = @"SELECT ChuyenDeID, TenChuyenDe FROM tblCNKTLopHocChuyenDe WHERE LopHocID=" + Request.QueryString["lophocid"];
        DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];

        foreach (DataRow chuyende in dtb.Rows)
        {

            System.Web.HttpContext.Current.Response.Write("<option value='" + chuyende["ChuyenDeID"].ToString() + "'>" + chuyende["TenChuyenDe"].ToString() + "</option>");

        }

        sql.Connection.Close();
        sql = null;
    }
}