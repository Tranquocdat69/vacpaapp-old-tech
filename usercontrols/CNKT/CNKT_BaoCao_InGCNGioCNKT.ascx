﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_BaoCao_InGCNGioCNKT.ascx.cs"
    Inherits="usercontrols_CNKT_BaoCao_InGCNGioCNKT" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Giấy chứng nhận giờ cập nhật kiến thức kiểm toán viên</h4>
<div id="thongbaoloi_form_ingcn" name="thongbaoloi_form_ingcn" style="display: none;
    margin-top: 5px;" class="alert alert-error">
</div>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="100%" border="0" class="formtbl">
        <tr>
            <td>
                Ngày lập báo cáo<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtNgayLapBC" name="txtNgayLapBC" style="max-width: 100px;" />
                <img src="/images/icons/calendar.png" id="imgCalendarNgayLapBC" />
            </td>
            <td>Số vào sổ:</td>
            <td><input type="text" id="txtSoVaoSo" name="txtSoVaoSo"/></td>
        </tr>
        <tr>
            <td>
                Mã lớp học<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtMaLopHoc" name="txtMaLopHoc" style="max-width: 100px;" onchange="CallActionGetInforLopHoc();"
                    class="tdInput" />
                <input type="hidden" name="hdLopHocID" id="hdLopHocID" />
                <input type="button" value="---" onclick="OpenDanhSachLopHoc();" style="border: 1px solid gray;" />
            </td>
            <td>Tên lớp học:
            </td>
            <td><input type="text" id="txtTenLopHoc" name="txtTenLopHoc" readonly="readonly"/></td>
        </tr>
        <tr>
            <td>
                ID Học viên<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtMaHocVien" name="txtMaHocVien" style="max-width: 100px;" readonly="readonly"
                    onchange="CallActionGetInforHocVien();" class="tdInput" />
                <input type="hidden" name="hdHocVienID" id="hdHocVienID" />
                <input type="button" value="---" onclick="OpenDanhSachHocVien();" style="border: 1px solid gray;" />
            </td>
            <td>
                Tên học viên:
            </td>
            <td>
                <input type="text" id="txtTenHocVien" name="txtTenHocVien" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('word')"><i class="iconsweets-word2"></i>Kết xuất Word</a>
                <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="Export('pdf')"><i
                    class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Giấy chứng nhận giờ cập nhật kiến thức kiểm toán viên</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" />
</fieldset>
</form>
<iframe name="iframeProcess_HocVien" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LopHoc" width="0px" height="0px"></iframe>
<script type="text/javascript">
     $("#txtNgayLapBC").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarNgayLapBC").click(function () {
        $("#txtNgayLapBC").datepicker("show");
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách học viên
    function OpenDanhSachHocVien() {
        if($('#hdLopHocID').val().length == 0) {
            alert('Phải chọn thông tin lớp học trước!');
            return;
        }
        $("#DivDanhSachHocVien").empty();
        $("#DivDanhSachHocVien").append($("<iframe width='100%' height='100%' id='ifDanhSachHocVien' name='ifDanhSachHocVien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_BaoCao_InGCNGioCNKT_ChonHocVien&idlh=" + $('#hdLopHocID').val()));
        $("#DivDanhSachHocVien").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách học viên</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu học viện theo mã học viên khi NSD nhập trực tiếp mã học viên vào ô textbox
    function CallActionGetInforHocVien() {
        var maHocVien = $('#txtMaHocVien').val();
        iframeProcess_HocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mahv='+maHocVien+'&action=loadhv';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin học viên từ Dialog danh sách
    function DisplayInforHocVien(value) {
        var arrValue = value.split(';#');
        $('#hdHocVienID').val(arrValue[0]);
        $('#txtMaHocVien').val(arrValue[1]);
        $('#txtTenHocVien').val(arrValue[3]);
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách học viên (Gọi hàm này từ trang con)
    function CloseFormDanhSachHocVien() {
        $("#DivDanhSachHocVien").dialog('close');
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách lớp học
    function OpenDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").empty();
        $("#DivDanhSachLopHoc").append($("<iframe width='100%' height='100%' id='ifDanhSachLopHoc' name='ifDanhSachLopHoc' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyLopHoc_MiniList"));
        $("#DivDanhSachLopHoc").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách lớp học</b>",
            modal: true,
            zIndex: 1000
        });
    }

    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu lớp học theo mã lớp khi NSD nhập trực tiếp mã lớp vào ô textbox
    function CallActionGetInforLopHoc() {
        var maLopHoc = $('#txtMaLopHoc').val();
        iframeProcess_LopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh=' + maLopHoc + '&action=loadlh';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin lớp học từ Dialog danh sách
    function DisplayInforLopHoc(value) {
        var arrValue = value.split(';#');
        $('#hdLopHocID').val(arrValue[0]);
        $('#txtMaLopHoc').val(arrValue[1]);
        $('#txtTenLopHoc').val(arrValue[2]);
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách lớp học (Gọi hàm này từ trang con)
    function CloseFormDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").dialog('close');
    }
    
    $('#Form1').validate({
        rules: {
            txtNgayLapBC: {
                required: true, dateITA: true
            },
            txtMaLopHoc: {
                required: true
            },
            txtMaHocVien: {
                required: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form_ingcn").html(e).show();

            } else {
                jQuery("#thongbaoloi_form_ingcn").hide();  
            }
        }
    });

    function Export(type) {
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
    
    <% SetDataJavascript();%>
</script>
<div id="DivDanhSachHocVien">
</div>
<div id="DivDanhSachLopHoc">
</div>
