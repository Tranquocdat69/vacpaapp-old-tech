﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_DangKyHocCaNhan_Add.ascx.cs"
    Inherits="usercontrols_CNKT_DangKyHocCaNhan_Add" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .tdInput
    {
        width: 130px;
    }
</style>
<form id="form_DangKyCaNhan" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Đăng ký học theo cá nhân</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <table id="tblThongTinChung" width="800px" border="0" class="formtbl">
        <tr>
            <td>
                ID<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtMaHocVien" id="txtMaHocVien" onchange="CallActionGetInforHocVien();" />
                <input type="hidden" name="hdHocVienID" id="hdHocVienID" />
                <input type="hidden" name="hdHoiVienTapTheID" id="hdHoiVienTapTheID" />
                <input type="hidden" name="hdLoaiHocVien" id="hdLoaiHocVien" />
                <input type="hidden" name="hdTroLyKTV" id="hdTroLyKTV" />
                <input type="hidden" name="hdDangKyHocCaNhanID" id="hdDangKyHocCaNhanID" />
                <input type="hidden" name="hdLoaiHoiVienChiTiet" id="hdLoaiHoiVienChiTiet" />
            </td>
            <td>
                <input type="button" value="---" onclick="OpenDanhSachHocVien();" style="border: 1px solid gray;" />
            </td>
            <td>
                Số CCKTV:
            </td>
            <td class="tdInput">
                <input type="text" name="txtSoCCKTV" id="txtSoCCKTV" readonly="readonly" />
            </td>
            <td>
                Mobile:
            </td>
            <td class="tdInput">
                <input type="text" name="txtMobile" id="txtMobile" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Tên học viên:
            </td>
            <td colspan="2">
                <input type="text" name="txtTenHocVien" id="txtTenHocVien" readonly="readonly" />
            </td>
            <td>
                Ngày cấp CCKTV:
            </td>
            <td>
                <input type="text" name="txtNgayCapCCKTC" id="txtNgayCapCCKTV" readonly="readonly" />
            </td>
            <td>
                Email:
            </td>
            <td>
                <input type="text" name="txtEmail" id="txtEmail" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Đơn vị công tác:
            </td>
            <td colspan="2">
                <input type="text" name="txtDonViCongTac" id="txtDonViCongTac" readonly="readonly" />
            </td>
            <td>
                Giới tính:
            </td>
            <td>
                <input type="text" name="txtGioiTinh" id="txtGioiTinh" readonly="readonly" />
            </td>
            <td>
                Hội viên:
            </td>
            <td>
                <input type="checkbox" name="cboHoiVien" id="cboHoiVien" readonly="readonly" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Thông tin lớp học</legend>
    <table width="800px" border="0" class="formtbl">
        <tr>
            <td>
                Mã lớp học<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtMaLopHoc" id="txtMaLopHoc" onchange="CallActionGetInforLopHoc();" />
                <input type="hidden" name="hdLopHocID" id="hdLopHocID" />
            </td>
            <td>
                <input type="button" value="---" onclick="OpenDanhSachLopHoc();" style="border: 1px solid gray;" />
            </td>
            <td>
                Tên lớp học:
            </td>
            <td class="tdInput" colspan="3">
                <input type="text" name="txtTenLopHoc" id="txtTenLopHoc" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
            <td>
                Từ ngày:
            </td>
            <td class="tdInput">
                <input type="text" name="txtTuNgay" id="txtTuNgay" readonly="readonly" />
            </td>
            <td>
                Hạn đăng ký:
            </td>
            <td class="tdInput">
                <input type="text" name="txtHanDangKy" id="txtHanDangKy" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Ngày đăng ký<span class="starRequired">(*)</span>:
            </td>
            <td colspan="2">
                <input type="text" name="txtNgayDangKy" id="txtNgayDangKy" />
            </td>
            <td>
                Đến ngày:
            </td>
            <td>
                <input type="text" name="txtDenNgay" id="txtDenNgay" readonly="readonly" />
            </td>
            <td>
                Tỉnh/thành:
            </td>
            <td>
                <input type="text" name="txtTinhThanh" id="txtTinhThanh" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Số giờ CNKT:
            </td>
            <td colspan="2">
                <input type="text" name="txtSoGioCNKT" id="txtSoGioCNKT" readonly="readonly" />
            </td>
            <td colspan="2">
                <input type="checkbox" name="cboCoGCNGioCNKT" id="cboCoGCNGioCNKT" checked="checked"
                    value="1" />
                Có nhận giấy chứng nhận giờ CNKT
            </td>
            <td>
                Tổng phí (VNĐ):
            </td>
            <td>
                <input type="text" name="txtTongPhi" id="txtTongPhi" readonly="readonly" style="text-align: right;" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Danh sách chuyên đề</legend>
    <input type="hidden" id="hdListChuyenDeID" name="hdListChuyenDeID" />
    <input type="hidden" id="hdSoNgay" name="hdSoNgay" />
    <table id="tblDanhSachChuyenDe" style="width: 100%; min-width: 800px;" border="0"
        class="formtbl">
        <tr>
            <th>
                <input type="checkbox" class="checkall" value="all" />
            </th>
            <th>
                STT
            </th>
            <th>
                Mã CĐ
            </th>
            <th>
                Tên chuyên đề
            </th>
            <th>
                Loại
            </th>
            <th>
                Ngày học
            </th>
            <th>
                Thời lượng (buổi)
            </th>
            <th>
                Người trình bày
            </th>
            <th>
                Trình độ chuyên môn
            </th>
            <th>
                Đơn vị công tác
            </th>
            <th>
                Chức vụ
            </th>
            <th>
                Chứng chỉ
            </th>
            <th>
                Số học viên
            </th>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; margin-top: 10px; text-align: center;">
    <input type="hidden" id="hdIsCloseForm" name="hdIsCloseForm" />
    <!-- control này để xác định có đóng Form sau khi save ko -->
    <input type="hidden" id="hdPostAction" name="hdPostAction" />
    <!-- control này để xác định hành động khi post form (Save hay Delete) -->
    <a id="A1" href="javascript:;" class="btn" onclick="SaveDangKy('0');"><i class="iconfa-save">
    </i>Lưu và tiếp tục</a> <a id="btnSave" href="javascript:;" class="btn" onclick="SaveDangKy('1');">
        <i class="iconfa-save"></i>Lưu và kết thúc</a> <a id="btnDelete" href="javascript:;"
            class="btn" onclick="if(confirm('Bạn chắc chắn với thao tác này chứ?'))DeleteDangKy('0');">
            <i class="iconfa-trash"></i>Xóa</a> <a id="btnBack" href="/admin.aspx?page=CNKT_QuanLyDangKyHoc" class="btn"><i class="iconfa-off">
    </i>Trở lại danh sách đăng ký</a>
</div>
</form>
<iframe name="iframeProcess_LoadHocVien" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadLopHoc" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadLopHocHocVien" width="0px" height="0px"></iframe>
<script type="text/javascript">

    var _arrDataChuyenDe = [];
    var _arrDataHocPhi = [];

    $(function () {
        $("#txtNgayDangKy").datepicker({
            dateFormat: 'dd/mm/yy'
        });
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách học viên
    function OpenDanhSachHocVien() {
        $("#DivDanhSachHocVien").empty();
        $("#DivDanhSachHocVien").append($("<iframe width='100%' height='100%' id='ifDanhSachHocVien' name='ifDanhSachHocVien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hoiviencanhan_minilist"));
        $("#DivDanhSachHocVien").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách học viên</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu học viện theo mã học viên khi NSD nhập trực tiếp mã học viên vào ô textbox
    function CallActionGetInforHocVien() {
        var maHocVien = $('#txtMaHocVien').val();
        iframeProcess_LoadHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mahv='+maHocVien+'&action=loadhv';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin học viên từ Dialog danh sách
    function DisplayInforHocVien(value) {      
        var arrValue = value.split(';#');
        $('#hdHocVienID').val(arrValue[0]);
        $('#txtMaHocVien').val(arrValue[1]);
        $('#txtTenHocVien').val(arrValue[3]);
        $('#txtSoCCKTV').val(arrValue[4]);
        $('#txtNgayCapCCKTV').val(arrValue[5]);
        $('#txtDonViCongTac').val(arrValue[6]);
        $('#txtEmail').val(arrValue[8]);
        $('#txtMobile').val(arrValue[9]);
        $('#hdTroLyKTV').val(arrValue[10]);
        $('#hdHoiVienTapTheID').val(arrValue[11]);

        var gioiTinh = arrValue[7];
        if (gioiTinh == "0")
            $('#txtGioiTinh').val("Nữ");
        else
            $('#txtGioiTinh').val("Nam");
        var loaiHocVien = arrValue[2];
        $('#hdLoaiHocVien').val(loaiHocVien);
        if(loaiHocVien == "1")
            $('#cboHoiVien').prop("checked", true);
        
        $('#hdLoaiHoiVienChiTiet').val(arrValue[13]);
        // Lấy danh sách các chuyên đề đã đăng ký của học viên với lớp học
        iframeProcess_LoadLopHocHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&idhv='+$('#hdHocVienID').val()+'&action=loadlhhv';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách học viên (Gọi hàm này từ trang con)
    function CloseFormDanhSachHocVien() {
        $("#DivDanhSachHocVien").dialog('close');
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách lớp học
    function OpenDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").empty();
        $("#DivDanhSachLopHoc").append($("<iframe width='100%' height='100%' id='ifDanhSachLopHoc' name='ifDanhSachLopHoc' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyLopHoc_MiniList"));
        $("#DivDanhSachLopHoc").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách lớp học</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu lớp học theo mã lớp khi NSD nhập trực tiếp mã lớp vào ô textbox
    function CallActionGetInforLopHoc() {
        var maLopHoc = $('#txtMaLopHoc').val();
        iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh='+maLopHoc+'&action=loadlh';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin lớp học từ Dialog danh sách
    function DisplayInforLopHoc(value) {
        var arrValue = value.split(';#');
        $('#hdLopHocID').val(arrValue[0]);
        $('#txtMaLopHoc').val(arrValue[1]);
        $('#txtTenLopHoc').val(arrValue[2]);
        $('#txtTuNgay').val(arrValue[3]);
        $('#txtDenNgay').val(arrValue[4]);
        $('#txtHanDangKy').val(arrValue[5]);
        $('#txtTinhThanh').val(arrValue[6]);
        $('#txtSoGioCNKT').val('');
        $('#txtTongPhi').val('');
        iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&lophocid='+arrValue[0]+'&action=loadcd';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách lớp học (Gọi hàm này từ trang con)
    function CloseFormDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").dialog('close');
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm vẽ dữ liệu chuyên đề từ mảng dữ liệu trả về từ iFrame
    function DrawData_ChuyenDe(arrData, arrDataHocPhi) {
        _arrDataChuyenDe = arrData;
        _arrDataHocPhi = arrDataHocPhi;
        var tbl = document.getElementById('tblDanhSachChuyenDe');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var chuyenDeID = arrData[i][0];
                var maChuyenDe = arrData[i][1];
                var tenChuyenDe = arrData[i][2];
                var loaiChuyenDe = arrData[i][3];
                var tenLoaiChuyenDe = arrData[i][4];
                var tuNgay = arrData[i][5];
                var denNgay = arrData[i][6];
                var soBuoi = arrData[i][7];
                var giangVienID = arrData[i][8];
                var tenGiangVien = arrData[i][9];
                var trinhDoChuyenMon = arrData[i][10];
                var donViCongTac = arrData[i][11];
                var chucVu = arrData[i][12];
                var chungChi = arrData[i][13];
                var soNguoiHocToiDa = arrData[i][20];
                var soNguoiDaDangKy = arrData[i][21];

                var tiLeDaDangKy = soNguoiDaDangKy + "/" + soNguoiHocToiDa;
                if(parseInt(soNguoiDaDangKy) >= parseInt(soNguoiHocToiDa))
                    tiLeDaDangKy = "<span style='color: green;'>" + tiLeDaDangKy + "</span>";

                var tr = document.createElement('TR');
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                var checkbox = document.createElement('input');
                checkbox.type = "checkbox";
                checkbox.value = i + ',' + chuyenDeID;
                checkbox.onclick = function() { GetOneChuyenDe(); };
                tdCheckBox.appendChild(checkbox);
                tr.appendChild(tdCheckBox);

                var ngayHoc = tuNgay + " - " + denNgay;
                if(tuNgay == denNgay)
                    ngayHoc = tuNgay;

                var arrColumn = [(i + 1), maChuyenDe, tenChuyenDe, tenLoaiChuyenDe, ngayHoc, soBuoi, tenGiangVien, trinhDoChuyenMon, donViCongTac, chucVu, chungChi, tiLeDaDangKy];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
        
        // Lấy danh sách các chuyên đề đã đăng ký của học viên với lớp học
        iframeProcess_LoadLopHocHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&idhv='+$('#hdHocVienID').val()+'&action=loadlhhv';
    }
    
    // Created by NGUYEN MANH HUNG 2014/11/27
    // Tick các chuyên đề đã đăng ký học (Dùng khi load dữ liệu các bản đăng ký cũ)
    function TickCheckBox(arrDataChuyenDeDaChon, dangKyHocCaNhanID, coNhanGCN, ngayDangKy, soGioCNKT, tongPhi) {
        $('#tblDanhSachChuyenDe :checkbox').each(function() {
            var chuyenDeId = $(this).val().split(',')[1];
            if(arrDataChuyenDeDaChon.length > 0 && arrDataChuyenDeDaChon.indexOf(chuyenDeId) > -1)
                $(this).prop("checked", true);
            else
                $(this).prop("checked", false);
        });
        
        if(coNhanGCN)
            $('#cboCoGCNGioCNKT').prop('checked', true);
        else
            $('#cboCoGCNGioCNKT').prop('checked', false);

        $('#txtNgayDangKy').val(ngayDangKy);
        $('#hdDangKyHocCaNhanID').val(dangKyHocCaNhanID);
        
        if(_arrDataChuyenDe.length > 0)
            GetListCheckbox(_arrDataChuyenDe);
        
        $('#txtSoGioCNKT').val(soGioCNKT);
        if(tongPhi == '' || isNaN(tongPhi))
            tongPhi = 0;
        $('#txtTongPhi').val(parseFloat(tongPhi).format(0, 3));
    }
    
    // Created by NGUYEN MANH HUNG 2014/11/26
    // Hàm kiểm tra các bản ghi chuyên đề được tick để tính tổng phí, số giờ CNKT, và ghi lại List chuyenDeID được chọn để save vào db
    function GetOneChuyenDe() {
        GetListCheckbox(_arrDataChuyenDe);
    }
    
    function GetListCheckbox(arrData) {
        var tongSoBuoi = 0; // biến chứa tổng số buổi theo các bản ghi được tick
        var tongPhi = 0; // Biến chứa tổng số phí theo các bản ghi được tick
        var lophocid = $('#hdLopHocID').val();
        var timestamp = Number(new Date());
        var listChuyenDeID = '';
        $('#hdListChuyenDeID').val('');
        $('#tblDanhSachChuyenDe :checkbox').each(function () {
            var checked = $(this).prop("checked");
            if ((checked) && ($(this).val() != "all")){
                var rowIndex = $(this).val().split(',')[0];
                if(arrData != undefined) {
                    tongSoBuoi = tongSoBuoi + (parseFloat(arrData[rowIndex][7]));
                }
                listChuyenDeID += $(this).val().split(',')[1] + ',';
            }
        });

        var loaiHocVien = $('#hdLoaiHocVien').val();
        var columnPhi = "";
        if (loaiHocVien == "1")
        {
            if ($('#hdLoaiHoiVienChiTiet').val() == "0" || $('#hdLoaiHoiVienChiTiet').val() == "2" || $('#hdLoaiHoiVienChiTiet').val() == "4" || $('#hdLoaiHoiVienChiTiet').val() == "5" || $('#hdLoaiHoiVienChiTiet').val() == "6")
                columnPhi = "SoTienPhiHVCNCT";
            else if ($('#hdLoaiHoiVienChiTiet').val() == "1")
                columnPhi = "SoTienPhiHVCNLK";
            else
                columnPhi = "SoTienPhiHVCNDD";
        }
     
        if (loaiHocVien == "2")
            tongPhi = "SoTienPhiKTV";
        if (loaiHocVien == "0") {
            var troLyKTV = $('#hdTroLyKTV').val();
            if (troLyKTV == "1")
                columnPhi = "SoTienPhiNQT_TroLyKTV";
            if (troLyKTV == "0")
                columnPhi = "SoTienPhiNTQ";
        }

        $.get('/usercontrols/CNKT/getsongayhoc_ajax.aspx?lophocid=' + lophocid + '&chuyendeid=' + listChuyenDeID + '&time=' + timestamp, function (data_ngayhoc) {

            eval(data_ngayhoc);
            $('#hdSoNgay').val(SoNgay);
            $.get('/usercontrols/CNKT/getphi_ajax.aspx?lophocid=' + lophocid + '&tongsongayhoc=' + SoNgay + '&phi=' + columnPhi + '&time=' + timestamp, function (data) {

                eval(data);

                tongPhi = parseFloat(Phi);

                if (tongPhi == '' || isNaN(tongPhi))
                    tongPhi = 0;

                $('#hdListChuyenDeID').val(listChuyenDeID);
                $('#txtSoGioCNKT').val(tongSoBuoi * 4);
                $('#txtTongPhi').val(parseFloat(tongPhi).format(0, 3));
            });
        });

        //// Dua vao tongSoBuoi hoc -> doi chieu voi bang hoc phi de tinh tongSoPhi phai thanh toan
        //for (var i = 0; i < _arrDataHocPhi.length; i++) {
        //    if(parseFloat(tongSoBuoi) == parseFloat(_arrDataHocPhi[i][0])) {
        //        var loaiHocVien = $('#hdLoaiHocVien').val();
        //        if(loaiHocVien == "1")
        //            tongPhi = parseFloat(_arrDataHocPhi[i][1]);
        //        if(loaiHocVien == "2")
        //            tongPhi = parseFloat(_arrDataHocPhi[i][2]);
        //        if(loaiHocVien == "0") {
        //            var troLyKTV = $('#hdTroLyKTV').val();
        //            if(troLyKTV == "1")
        //                tongPhi = parseFloat(_arrDataHocPhi[i][3]);
        //            if(troLyKTV == "0")
        //                tongPhi = parseFloat(_arrDataHocPhi[i][4]);    
        //        }
        //    }
        //}

        
    }
    
    $('#form_DangKyCaNhan').validate({
        rules: {
            txtNgayDangKy: {
                required: true, dateITA: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }  
    });
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Kiểm tra lỗi và submit form
    // isClose để xác định có đóng Form sau khi thêm hay ko (1: Có đóng Form)
    function SaveDangKy(isClose) {
        $('#hdPostAction').val('Save');
        $('#hdIsCloseForm').val(isClose);
        var hocVienID = $('#hdHocVienID').val();
        var lopHocID = $('#hdLopHocID').val();
        var msgError = "";
        // Kiểm tra bắt buộc phải có giá trị với ListChuyenDeID, hocVienID, lopHocID
        if($('#hdListChuyenDeID').val() == '')
            msgError += "Chưa chọn chuyên đề cần đăng ký học!<br />";
        if(hocVienID == "")
            msgError += "Chưa chọn học viên!<br />";
        if(lopHocID == "")
            msgError += "Chưa chọn lớp đào tạo, CNKT cần đăng ký!<br />";
        
        // Kiểm tra ngày đăng ký < ngày bắt đầu lớp học
        var ngayDangKy = $('#txtNgayDangKy').datepicker('getDate');
        var arrTuNgay = $('#txtTuNgay').val().split('/');
        if(arrTuNgay.length == 3) {
            var tuNgay = new Date(arrTuNgay[2], parseInt(arrTuNgay[1]) - 1, arrTuNgay[0]);
            var minus = (tuNgay - ngayDangKy) / 1000 / 60 / 60 / 24;
            if(minus <= 0)
                msgError += "Ngày đăng ký trước ngày bắt đầu lớp học!<br />";
        }

        if(msgError != "") {
            jQuery("#thongbaoloi_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>" + msgError).show();
            return;
        }
        else {
            jQuery("#thongbaoloi_form").hide();
        }

        if($('#hdDangKyHocCaNhanID').val().length == 0) {
            // Kiem tra chuyen de da vuot qua so nguoi hoc toi da chua
            iframeProcess_LoadLopHocHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&action=checkcd';    
        } else {
            $('#form_DangKyCaNhan').submit();
        }
    }
    
    function SubmitEventSaveForm(arrChuyenDeVuotQuaSoNguoiHoc) {
        if(arrChuyenDeVuotQuaSoNguoiHoc != undefined && arrChuyenDeVuotQuaSoNguoiHoc.length > 0) {
            var chuoiCanhBao = 'Chuyên đề:\n';
            for (var i = 0; i < arrChuyenDeVuotQuaSoNguoiHoc.length; i++) {
                chuoiCanhBao += ' - ' + arrChuyenDeVuotQuaSoNguoiHoc[i] + '\n';
            }
            chuoiCanhBao += 'đã vượt quá số người đăng ký học tối đa. Bạn có muốn tiếp tục đăng ký học cho học viên này không?';
            if(!confirm(chuoiCanhBao))
                return;
        }
        $('#form_DangKyCaNhan').submit();   
    }

    function DeleteDangKy() {
        $('#hdPostAction').val('Delete');
        var hocVienID = $('#hdHocVienID').val();
        var lopHocID = $('#hdLopHocID').val();
        var msgError = "";
        // Kiểm tra bắt buộc phải có giá trị với hocVienID, lopHocID
        if(hocVienID == "")
            msgError += "Chưa chọn học viên!<br />";
        if(lopHocID == "")
            msgError += "Chưa chọn lớp đào tạo, CNKT cần đăng ký!<br />";

        if(msgError != "") {
            jQuery("#thongbaoloi_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>" + msgError).show();
            return;
        }
        else {
            jQuery("#thongbaoloi_form").hide();
        }
        $('#form_DangKyCaNhan').submit();
    }
    
    jQuery(document).ready(function () {
        // dynamic table      

        $("#tblDanhSachChuyenDe .checkall").bind("click", function () {
            var listChuyenDeID = '';
            $('#hdListChuyenDeID').val('');
            var  checked = $(this).prop("checked");
            $('#tblDanhSachChuyenDe :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all"))
                    listChuyenDeID += $(this).val().split(',')[1] + ',';
            });
            $('#hdListChuyenDeID').val(listChuyenDeID);
            GetListCheckbox(_arrDataChuyenDe);
        });
    });
    
    <% FirstLoadPage(); %>
</script>
<div id="DivDanhSachHocVien">
</div>
<div id="DivDanhSachLopHoc">
</div>
