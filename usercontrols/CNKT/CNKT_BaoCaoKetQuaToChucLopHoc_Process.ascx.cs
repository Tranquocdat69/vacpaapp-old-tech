﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_CNKT_BaoCaoKetQuaToChucLopHoc_Process : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string lopHocId = Library.CheckNull(Request.QueryString["idlh"]);
            string action = Library.CheckNull(Request.QueryString["action"]);

            if (action == "loadbc")
                LoadBaoCao(lopHocId);
            if (action == "delete")
                Delete(lopHocId);

            string hdLopHocId = Request.Form["hdLopHocId"];
            string hdNgayCapNhat = Request.Form["hdNgayCapNhat"];
            string hdDanhGiaKhac = Request.Form["hdDanhGiaKhac"];
            string hdKienNghiVoiBTC = Request.Form["hdKienNghiVoiBTC"];
            string hdSoPhieuDanhGia = Request.Form["hdSoPhieuDanhGia"];
            string hdValue = Request.Form["hdValue"];
            if (!string.IsNullOrEmpty(hdLopHocId)) // Update bản ghi số giờ học thực tế
            {
                Save(hdLopHocId, hdNgayCapNhat, hdDanhGiaKhac, hdKienNghiVoiBTC, hdSoPhieuDanhGia, hdValue);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/11
    /// Load danh sách các ý kiến đánh giá và dữ liệu nếu lớp học đã đc đánh giá
    /// </summary>
    /// <param name="lopHocId">ID của lớp học cần đánh giá</param>
    private void LoadBaoCao(string lopHocId)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        js += "var ngayCapNhat = '" + DateTime.Now.ToString("dd/MM/yyyy") + "';" + Environment.NewLine;
        js += "var danhGiaKhac = '';" + Environment.NewLine;
        js += "var kienNghiVoiBtc = '';" + Environment.NewLine;
        js += "var soPhieuDanhGia = '';" + Environment.NewLine;
        // Load bản ghi báo cáo của lớp học
        if (!string.IsNullOrEmpty(lopHocId))
        {
            string lopHocBaoCaoId = "0", ngayCapNhat = "", danhGiaKhac = "", kienNghiVoiBtc = "", soPhieuDanhGia = "";
            string query = "SELECT LopHocBaoCaoID, NgayCapNhat, DanhGiaKhac, KienNghiVoiBTC, SoPhieuDanhGia FROM tblCNKTLopHocBaoCao WHERE LopHocID = " + lopHocId + " ORDER BY LopHocBaoCaoID DESC";
            List<Hashtable> listDataBaoCao = _db.GetListData(query);
            if (listDataBaoCao.Count > 0)
            {
                Hashtable ht = listDataBaoCao[0];
                lopHocBaoCaoId = Library.CheckKeyInHashtable(ht, "LopHocBaoCaoID").ToString();
                ngayCapNhat = Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "NgayCapNhat")).ToString("dd/MM/yyyy");
                danhGiaKhac = Library.CheckKeyInHashtable(ht, "DanhGiaKhac").ToString();
                kienNghiVoiBtc = Library.CheckKeyInHashtable(ht, "KienNghiVoiBTC").ToString();
                soPhieuDanhGia = Library.CheckKeyInHashtable(ht, "SoPhieuDanhGia").ToString();
                js += "ngayCapNhat = '" + ngayCapNhat + "';" + Environment.NewLine;
                js += "danhGiaKhac = '" + danhGiaKhac + "';" + Environment.NewLine;
                js += "kienNghiVoiBtc = '" + kienNghiVoiBtc + "';" + Environment.NewLine;
                js += "soPhieuDanhGia = '" + soPhieuDanhGia + "';" + Environment.NewLine;
            }

            List<Hashtable> listData = _db.GetListData(ListName.Proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc, new List<string> { "@LopHocBaoCaoID", "@SubId" }, new List<object> { lopHocBaoCaoId, DBNull.Value });
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    js += "arrData.push(['" + ht["YKienDanhGiaLopHocID"] + "', '" + ht["TenLoaiDanhGiaLopHoc"] + "', '" + ht["DanhGiaTot"] + "'," +
                          " '" + ht["DanhGiaKha"] + "',  '" + ht["DanhGiaTrungBinh"] + "', '" + ht["SubId"] + "', '', '', '']);" + Environment.NewLine;
                    js += LoadSubYKienDanhGia(lopHocBaoCaoId, ht["YKienDanhGiaLopHocID"].ToString());
                    if(ht["LoaiYKien"].ToString() == "1") // Là loại ý kiến đánh giá từng giảng viên
                    {
                        js += LoadListGiangVien(lopHocId, ht["YKienDanhGiaLopHocID"].ToString());
                    }
                }
            }
        }
        js += "parent.DrawData(arrData, ngayCapNhat, danhGiaKhac, kienNghiVoiBtc, soPhieuDanhGia);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private string LoadSubYKienDanhGia(string idLopHoc, string subId)
    {
        string js = "";
        List<Hashtable> listData = _db.GetListData(ListName.Proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc, new List<string> { "@LopHocBaoCaoID", "@SubId" }, new List<object> { idLopHoc, subId });
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                js += "arrData.push(['" + ht["YKienDanhGiaLopHocID"] + "', '" + ht["TenLoaiDanhGiaLopHoc"] + "', '" + ht["DanhGiaTot"] + "'," +
                      " '" + ht["DanhGiaKha"] + "',  '" + ht["DanhGiaTrungBinh"] + "', '" + ht["SubId"] + "', '', '', '']);" + Environment.NewLine;
            }
        }
        return js;
    }

    private string LoadListGiangVien(string idLopHoc, string yKienDanhGiaLopHocID)
    {
        string js = "";
        List<Hashtable> listData = _db.GetListData(ListName.Proc_CNKT_GetBaoCaoDanhGiaToChucLopHoc_DanhGiaGiangVien, new List<string> { "@LopHocBaoCaoID", "@YKienDanhGiaLopHocChaID" }, new List<object> { idLopHoc, yKienDanhGiaLopHocID });
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                js += "arrData.push(['', '', '" + ht["DanhGiaTot"] + "'," +
                      " '" + ht["DanhGiaKha"] + "',  '" + ht["DanhGiaTrungBinh"] + "', '', '" + ht["GiangVienID"] + "', '" + ht["TenGiangVien"] + "', '" + yKienDanhGiaLopHocID + "']);" + Environment.NewLine;
            }
        }
        return js;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/11
    /// Lưu lại kết quả đánh giá lớp học
    /// </summary>
    /// <param name="lopHocId">ID lớp học cần đánh giá</param>
    /// <param name="ngayCapNhat">Ngày đánh giá</param>
    /// <param name="danhGiaKhac">Đánh giá khác</param>
    /// <param name="kienNghiVoiBtc">Kiến nghị với BTC</param>
    /// <param name="value">Chuỗi giá trị số người đánh giá</param>
    private void Save(string lopHocId, string ngayCapNhat, string danhGiaKhac, string kienNghiVoiBtc, string soPhieuDanhGia, string allValue)
    {
        // Lấy dữ liệu bản ghi báo cáo trg DB của lớp học để Insert hoặc Update
        string query = "SELECT LopHocBaoCaoID FROM tblCNKTLopHocBaoCao WHERE LopHocID = " + lopHocId + " ORDER BY LopHocBaoCaoID DESC";
        List<Hashtable> listData = _db.GetListData(query);
        string lopHocBaoCaoId = "";
        if (listData.Count > 0)
            lopHocBaoCaoId = Library.CheckKeyInHashtable(listData[0], "LopHocBaoCaoID").ToString();
        SqlCnktLopHocBaoCaoProvider lopHocBaoCaoPro = new SqlCnktLopHocBaoCaoProvider(ListName.ConnectionString, false, string.Empty);
        CnktLopHocBaoCao lopHocBaoCao = new CnktLopHocBaoCao();
        if (!string.IsNullOrEmpty(lopHocBaoCaoId)) // Update
            lopHocBaoCao = lopHocBaoCaoPro.GetByLopHocBaoCaoId(Library.Int32Convert(lopHocBaoCaoId));
        else // Insert
            lopHocBaoCao.LopHocId = Library.Int32Convert(lopHocId);
        lopHocBaoCao.NgayCapNhat = Library.DateTimeConvert(ngayCapNhat, "dd/MM/yyyy");
        lopHocBaoCao.DanhGiaKhac = danhGiaKhac;
        lopHocBaoCao.KienNghiVoiBtc = kienNghiVoiBtc;
        lopHocBaoCao.SoPhieuDanhGia = Library.Int32Convert(soPhieuDanhGia);
        if (!string.IsNullOrEmpty(lopHocBaoCaoId)) // Update
            lopHocBaoCaoPro.Update(lopHocBaoCao);
        else
            lopHocBaoCaoPro.Insert(lopHocBaoCao);

        // Insert số người đánh giá theo từng tiêu chí.
        if (lopHocBaoCao.LopHocBaoCaoId > 0)
        {
            // Xóa hết số liệu đánh giá cũ
            string deleteCommand = "DELETE FROM " + ListName.Table_CNKTLopHocBaoCaoChiTiet + " WHERE LopHocBaoCaoID = " + lopHocBaoCao.LopHocBaoCaoId;
            if (_db.ExecuteNonQuery(deleteCommand) > 0)
            {
                // Insert dữ liệu mới vào
                SqlCnktLopHocBaoCaoChiTietProvider baoCaoChiTietPro = new SqlCnktLopHocBaoCaoChiTietProvider(ListName.ConnectionString, false, string.Empty);
                string[] arrValue = allValue.Split(new string[] { ";#" }, StringSplitOptions.None);
                foreach (string value in arrValue)
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        string[] arrProp = value.Split('_');
                        if (arrProp.Length == 5)
                        {
                            CnktLopHocBaoCaoChiTiet baoCaoChiTiet = new CnktLopHocBaoCaoChiTiet();
                            baoCaoChiTiet.LopHocBaoCaoId = lopHocBaoCao.LopHocBaoCaoId;
                            if (!string.IsNullOrEmpty(arrProp[0]))
                                baoCaoChiTiet.YkienDanhGiaLopHocId = Library.Int32Convert(arrProp[0]);
                            if (!string.IsNullOrEmpty(arrProp[1]))
                                baoCaoChiTiet.DoiTuongDuocDanhGiaId = Library.Int32Convert(arrProp[1]);
                            baoCaoChiTiet.DanhGiaTot = Library.DecimalConvert(arrProp[2]);
                            baoCaoChiTiet.DanhGiaKha = Library.DecimalConvert(arrProp[3]);
                            baoCaoChiTiet.DanhGiaTrungBinh = Library.DecimalConvert(arrProp[4]);
                            baoCaoChiTietPro.Insert(baoCaoChiTiet);
                        }
                    }
                }
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.SaveSuccess(1);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void Delete(string lopHocId)
    {
        // Lấy dữ liệu báo cáo theo lớp học ID truyền vào
        string query = "SELECT LopHocBaoCaoID FROM tblCNKTLopHocBaoCao WHERE LopHocID = " + lopHocId + " ORDER BY LopHocBaoCaoID DESC";
        List<Hashtable> listData = _db.GetListData(query);
        string lopHocBaoCaoId = "";
        if (listData.Count > 0)
            lopHocBaoCaoId = Library.CheckKeyInHashtable(listData[0], "LopHocBaoCaoID").ToString();

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        if (!string.IsNullOrEmpty(lopHocBaoCaoId))
        {
            // Xóa tất cả bản ghi trong báo cáo chi tiết dựa theo ID báo cáo
            string deleteCommand = "DELETE FROM " + ListName.Table_CNKTLopHocBaoCaoChiTiet + " WHERE LopHocBaoCaoID = " + lopHocBaoCaoId;
            if (_db.ExecuteNonQuery(deleteCommand) > 0)
            {
                // Tiếp theo xóa bản ghi báo cáo
                deleteCommand = "DELETE FROM " + ListName.Table_CNKTLopHocBaoCao + " WHERE LopHocBaoCaoID = " + lopHocBaoCaoId;
                if (_db.ExecuteNonQuery(deleteCommand) > 0)
                {
                    js += "parent.DeleteSuccess(1);" + Environment.NewLine;
                }
                else
                    js += "parent.DeleteSuccess(0);" + Environment.NewLine;
            }
            else
                js += "parent.DeleteSuccess(0);" + Environment.NewLine;
        }
        js += "</script>";
        Response.Write(js);
    }
}