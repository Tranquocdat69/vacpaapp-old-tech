﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_CNKT_BaoCao_TongHopSoGioCNKTCuaKTV : System.Web.UI.UserControl
{
    protected string tenchucnang = "Tổng hợp số giờ cập nhật kiến thức của kiểm toán viên";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            int nam = Library.Int32Convert(Request.Form["ddlNam"]);
            string tuNgay = Request.Form["txtTuNgay"];
            string denNgay = Request.Form["txtDenNgay"];
            string idCongTy = Library.CheckNull(Request.Form["hdCongTyID"]);
            if (nam > 0)
            {
                DateTime dateTuNgay = new DateTime(Library.Int32Convert(nam), 1, 1);
                if (!string.IsNullOrEmpty(tuNgay))
                    dateTuNgay = Library.DateTimeConvert(tuNgay, "dd/MM/yyyy");

                DateTime dateDenNgay = new DateTime(Library.Int32Convert(nam), 12, 31);
                if (!string.IsNullOrEmpty(denNgay))
                    dateDenNgay = Library.DateTimeConvert(denNgay, "dd/MM/yyyy");

                DataTable dtLopHoc = GetDanhSachLopHoc(dateTuNgay, dateDenNgay);
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/CNKT/TongHopSoGioCNKTCuaKTV.rpt"));
                List<double[]> list = new List<double[]>(); // Chứa tổng số giờ các cột
                DataTable dt = GetData(dtLopHoc, dateTuNgay, dateDenNgay, idCongTy, list);
                rpt.Database.Tables["dtTongHopSoGioCNKTCuaKTV"].SetDataSource(dt);
                double tongSoGioKiKT = list.Sum(doublese => doublese[0]);
                double tongSoGioDD = list.Sum(doublese => doublese[1]);
                double tongSoGioKhac = list.Sum(doublese => doublese[2]);

                int maxRight = 0;
                int width = 840;
                for (int i = 0; i < dtLopHoc.Rows.Count; i++)
                {
                    maxRight = 6000 + (width * i) + (i * 100);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeader" + i, "Lớp " + dtLopHoc.Rows[i]["MaLopHoc"], maxRight, 3000, width, null, null, 12, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "txtControlFooterTongCot" + i, (list[i][0] + list[i][1] + list[i][2]).ToString(), maxRight, 0, width, null, null, 13, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTV.Lop" + i, "fControlSoGioKiKT" + i, maxRight, 0, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlHeader" + i, maxRight, maxRight, 2880, 4200, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lControlDetail" + i, maxRight, maxRight, 0, 360, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lControlFooter" + i, maxRight, maxRight, 0, 360, false);
                }

                // Add cac cot tinh tong
                int rowCount = dtLopHoc.Rows.Count;
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongCong", maxRight, 6000 + ((width + 100) * 3) + (width * rowCount) + (rowCount * 100) - 50, 3600, 3600, false);
                maxRight = 6000 + (width * rowCount) + (rowCount * 100);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongSoGioKiKT", "KT, KiT", maxRight, 3720, width, null, null, 12, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "txtControlFooterTongCot_TongSoGioKiKT", tongSoGioKiKT.ToString(), maxRight, 0, width, null, null, 13, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTV.TongSoGioKiKT", "fControlTongSoGioKiKT", maxRight, 0, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                maxRight += width + 50;
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongSoGioKiKT", maxRight, maxRight, 3600, 4200, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTongSoGioKiKT", maxRight, maxRight, 0, 360, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lControlFooterTongSoGioKiKT", maxRight, maxRight, 0, 360, false);
                maxRight = (6000 + ((width + 100) * 1)) + (width * rowCount) + (rowCount * 100);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongSoGioDD", "ĐĐ", maxRight, 3720, width, null, null, 12, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "txtControlFooterTongCot_TongSoGioDD", tongSoGioDD.ToString(), maxRight, 0, width, null, null, 13, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTV.TongSoGioDD", "fControlTongSoGioDD", maxRight, 0, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                maxRight += width + 50;
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongSoGioDD", maxRight, maxRight, 3600, 4200, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTongSoGioDD", maxRight, maxRight, 0, 360, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lControlFooterTongSoGioDD", maxRight, maxRight, 0, 360, false);
                maxRight = (6000 + ((width + 100) * 2)) + (width * rowCount) + (rowCount * 100);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongSoGioKhac", "Khác", maxRight, 3720, width, null, null, 12, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "txtControlFooterTongCot_TongSoGioKhac", tongSoGioKhac.ToString(), maxRight, 0, width, null, null, 13, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTV.TongSoGioKhac", "fControlTongSoGioKhac", maxRight, 0, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                maxRight += width + 50;
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongSoGioKhac", maxRight, maxRight, 2880, 4200, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTongSoGioKhac", maxRight, maxRight, 0, 360, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lControlFooterTongSoGioKhac", maxRight, maxRight, 0, 360, false);

                // Tạo tiêu đề tổng số giờ chi tiết
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCong", "Tổng số giờ chi tiết " + nam, 6000 + (width * rowCount) + (rowCount * 100), 3000, maxRight - (6000 + (width * rowCount) + (rowCount * 100)) - 50, null, null, 12, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);

                //((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lMiddleHeader"]).Right = maxRight;
                // Add cot "TongCong"
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCongNam", "Tổng cộng giờ " + nam, maxRight + 50, 3360, 1440, null, null, 12, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "txtControlFooterTongCot_TongSoGioCaNam", (tongSoGioKiKT + tongSoGioDD + tongSoGioKhac).ToString(), maxRight, 0, 1440, null, null, 13, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTV.TongSoGioCaNam", "fControlTongCongNam", maxRight + 50, 0, 1440, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                maxRight += 1440 + 50;
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongCongNam", maxRight, maxRight, 2880, 4200, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTongCongNam", maxRight, maxRight, 0, 360, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lControlFooterTongCongNam", maxRight, maxRight, 0, 360, false);

                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_GroupHeader, 0, "lControlGroup1", maxRight, maxRight, 0, 480, false);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_GroupHeader, 1, "lControlGroup2", maxRight, maxRight, 0, 360, false);

                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lTopHeader"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomHeader"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomDetail"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomFooter"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomGroup1"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomGroup2"]).Right = maxRight;

                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "TỔNG HỢP SỐ GIỜ CẬP NHẬT KIẾN THỨC CỦA KIỂM TOÁN VIÊN NĂM " + nam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle2"]).Text = "Từ ngày " + dateTuNgay.ToString("dd/MM/yyyy") + " đến ngày " + dateDenNgay.ToString("dd/MM/yyyy");
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLap"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "TongHopSoGioCNKTCuaKTV");
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, dtLopHoc, nam, dateTuNgay.ToString("dd/MM/yyyy"), dateDenNgay.ToString("dd/MM/yyyy"), list);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "TongHopSoGioCNKTCuaKTV");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "TongHopSoGioCNKTCuaKTV");
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string js = "";
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            js += "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
        js += "$('#txtTuNgay').val('" + Request.Form["txtTuNgay"] + "');" + Environment.NewLine;
        js += "$('#txtDenNgay').val('" + Request.Form["txtDenNgay"] + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["txtMaCongTy"]))
        {
            js += "$('#txtMaCongTy').val('" + Request.Form["txtMaCongTy"] + "');" + Environment.NewLine;
            js += "CallActionGetInforCongTy();" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private DataTable GetData(DataTable dtLopHoc, DateTime dateTuNgay, DateTime dateDenNgay, string idCongTy, List<double[]> list)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "HoTen", "SoChungChiKTV", "NgayCapChungChiKTV", "GhiChu", "TongSoGioKiKT", "TongSoGioDD", "TongSoGioKhac", "TongSoGioCaNam", "LoaiDoiTuong", "CongTy" });
        for (int i = 0; i < dtLopHoc.Rows.Count; i++)
        {
            dt.Columns.Add("Lop" + i);
            list.Add(new double[3]);
        }

        DataTable dtTongHopSoGio = _db.GetDataTable(ListName.Proc_BAOCAO_CNKT_TongHopSoGioCNKTCuaKTV, new List<string> { "@FromDate", "@ToDate", "@HoiVienTapTheID" }, new List<object> { dateTuNgay.ToString("yyyy-MM-dd"), dateDenNgay.ToString("yyyy-MM-dd"), idCongTy });
        if (dtTongHopSoGio.Rows.Count > 0)
        {
            string tempIdHvcn = "0";
            DataRow dr = null;
            for (int x = 0; x < dtTongHopSoGio.Rows.Count; x++)
            {
                if (dtTongHopSoGio.Rows[x]["HoiVienCaNhanID"].ToString() != tempIdHvcn)
                {
                    tempIdHvcn = dtTongHopSoGio.Rows[x]["HoiVienCaNhanID"].ToString();
                    dr = dt.NewRow();
                    dr["STT"] = (dt.Rows.Count + 1);
                    dr["HoTen"] = dtTongHopSoGio.Rows[x]["FullName"];
                    dr["SoChungChiKTV"] = dtTongHopSoGio.Rows[x]["SoChungChiKTV"];
                    dr["NgayCapChungChiKTV"] = !string.IsNullOrEmpty(dtTongHopSoGio.Rows[x]["NgayCapChungChiKTV"].ToString()) ? Library.DateTimeConvert(dtTongHopSoGio.Rows[x]["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy") : "";
                    string loaiHoiVien = dtTongHopSoGio.Rows[x]["LoaiHoiVienCaNhan"].ToString();
                    string troLyKtv = dtTongHopSoGio.Rows[x]["TroLyKTV"].ToString();
                    string idHoiVienTapThe = dtTongHopSoGio.Rows[x]["hoiVienTapTheID"].ToString();
                    if ((loaiHoiVien == "1" || loaiHoiVien == "2") && string.IsNullOrEmpty(troLyKtv) && !string.IsNullOrEmpty(idHoiVienTapThe))
                    {
                        dr["LoaiDoiTuong"] = "PHẦN I: SỐ GIỜ CNKT CỦA KTV ĐĂNG KÝ THEO CÔNG TY KIỂM TOÁN";
                        dr["CongTy"] = dtTongHopSoGio.Rows[x]["SoHieu"] + " - " + dtTongHopSoGio.Rows[x]["TenDoanhNghiep"];
                    }
                    else
                    {
                        dr["LoaiDoiTuong"] = "PHẦN II: SỐ GIỜ CNKT CỦA CÁC ĐỐI TƯỢNG KHÁC (TRỢ LÝ KTV, KTV LÀM Ở ĐƠN VỊ KHÁC ...)";
                    }

                    double tongSoGioKiKT = 0, tongSoGioDD = 0, tongSoKhac = 0;
                    for (int i = 0; i < dtLopHoc.Rows.Count; i++)
                    {
                        DataRow[] arrDataRow = dtTongHopSoGio.Select("HoiVienCaNhanID = " + tempIdHvcn + " AND LopHocID = " + dtLopHoc.Rows[i]["LopHocID"]);
                        foreach (DataRow dataRow in arrDataRow)
                        {
                            dr["Lop" + i] = Library.DoubleConvert(dr["Lop" + i]) + Library.DoubleConvert(dataRow["SoGioThucTe"]);
                            switch (dataRow["LoaiChuyenDe"].ToString())
                            {
                                case "1":
                                    tongSoGioKiKT += Library.DoubleConvert(dataRow["SoGioThucTe"]); // tinh tong dong
                                    list[i][0] += Library.DoubleConvert(dataRow["SoGioThucTe"]); // Tinh tong cot
                                    break;
                                case "2":
                                    tongSoGioDD += Library.DoubleConvert(dataRow["SoGioThucTe"]);// tinh tong dong
                                    list[i][1] += Library.DoubleConvert(dataRow["SoGioThucTe"]);// Tinh tong cot
                                    break;
                                case "3":
                                    tongSoKhac += Library.DoubleConvert(dataRow["SoGioThucTe"]);// tinh tong dong
                                    list[i][2] += Library.DoubleConvert(dataRow["SoGioThucTe"]);// Tinh tong cot
                                    break;
                            }
                        }
                    }
                    dr["TongSoGioKiKT"] = tongSoGioKiKT;
                    dr["TongSoGioDD"] = tongSoGioDD;
                    dr["TongSoGioKhac"] = tongSoKhac;
                    dr["TongSoGioCaNam"] = (tongSoGioKiKT + tongSoGioDD + tongSoKhac);
                    dt.Rows.Add(dr);
                }
            }
        }
        return dt;
    }

    private DataTable GetDanhSachLopHoc(DateTime dateTuNgay, DateTime dateDenNgay)
    {
        DataTable dt = new DataTable();

        //dt = _db.GetDataTable(ListName.Proc_BAOCAO_QLDT_TONGHOPSOGIOCNKT_DYNAMICCOLUMN, new List<string> { "@iddonvicongtac", "@datefrom", "@dateto" }, new List<object> { "", dateTuNgay, dateDenNgay });
        string query = @"SELECT LopHocID, MaLopHoc, TuNgay,DenNgay FROM tblCNKTLopHoc 
                            INNER JOIN tblDMTrangThai DMTT ON tblCNKTLopHoc.TinhTrangID = DMTT.TrangThaiID
                            WHERE (tblCNKTLopHoc.TuNgay >= '" + dateTuNgay.ToString("yyyy-MM-dd") + @"' OR tblCNKTLopHoc.DenNgay >= '" + dateTuNgay.ToString("yyyy-MM-dd") + @"')
	                            AND (tblCNKTLopHoc.TuNgay <= '" + dateDenNgay.ToString("yyyy-MM-dd") + @"' OR tblCNKTLopHoc.DenNgay <= '" + dateDenNgay.ToString("yyyy-MM-dd") + @"') AND DMTT.MaTrangThai = '5' ORDER BY TuNgay ASC";
        dt = _db.GetDataTable(query);
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, DataTable dtLopHoc, int nam, string tuNgay, string denNgay, List<Double[]> list)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:120px;
            vertical-align: middle;
        }

        .HeaderColumn1 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTitle {      
            font-weight: bold;      
            vertical-align: middle;            
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
            height: 40px;
            vertical-align: middle; 
        }

        .GroupTitle2 {
            font-size: 10pt;
            font-weight: bold;
            height: 30px;
            vertical-align: middle; 
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='12' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='12' style='text-align:center;font-size: 14pt; font-weight: bold;'>TỔNG HỢP SỐ GIỜ CẬP NHẬT KIẾN THỨC CỦA KIỂM TOÁN VIÊN NĂM " + nam + @"</td>
            </tr>
            <tr>
                <td colspan='12' style='text-align:center;font-size: 12pt; font-weight: bold;'>TÍNH TỪ NGÀY " + tuNgay + @" ĐẾN NGÀY " + denNgay + @"</td>
            </tr>
            <tr>
                <td colspan='12' style='text-align:center;font-size: 10pt; font-weight: bold;'><i>(Kèm theo Công văn số ...-.../VACPA ngày .../.../20...)</i></td>
            </tr>
            <tr><td><td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td rowspan='2' class='HeaderColumn1'>STT</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 150px;'>Họ và tên</td>
                                <td colspan='2' class='HeaderColumn1'>Chứng chỉ KTV</td>";

        for (int i = 0; i < dtLopHoc.Rows.Count; i++)
        {
            htmlExport += "<td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Lớp " + dtLopHoc.Rows[i]["MaLopHoc"] + "</td>";
        }

       htmlExport += @"         <td colspan='3' class='HeaderColumn1'>Tổng số giờ chi tiết</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Tổng số giờ " + nam + @"</td>";

       htmlExport += "</tr><tr><td class='HeaderColumn1' style='width: 100px;'>Số</td>" +
                     "<td class='HeaderColumn1' style='width: 100px;'>Ngày cấp</td>" +
                     "<td class='HeaderColumn1' style='width: 70px;'>KT, KiT</td>" +
                     "<td class='HeaderColumn1' style='width: 70px;'>ĐĐ</td>" +
                     "<td class='HeaderColumn1' style='width: 70px;'>Khác</td>" +
                     "</tr>";
       string tempLoai = "", tempCongTy = "";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string loai = dt.Rows[i]["LoaiDoiTuong"].ToString();
            string congTy = dt.Rows[i]["CongTy"].ToString();
            if (loai != tempLoai)
            {
                tempLoai = loai;
                htmlExport += "<tr><td colspan='"+(8 + dtLopHoc.Rows.Count)+"' class='GroupTitle'>" + loai + "</td></tr>";
            }

            if (tempCongTy != congTy || loai != tempLoai)
            {
                tempCongTy = congTy;
                htmlExport += "<tr><td colspan='" + (8 + dtLopHoc.Rows.Count) + "' class='GroupTitle2'>" + congTy + "</td></tr>";
            }

            htmlExport += "<tr><td class='ColumnStt'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["HoTen"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoChungChiKTV"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NgayCapChungChiKTV"] + "</td>";
            for (int x = 0; x < dtLopHoc.Rows.Count; x++)
            {
                htmlExport += "<td class='Column'>" + dt.Rows[i]["Lop" + x] + "</td>";
            }
            htmlExport += "<td class='HeaderColumn1'>" + dt.Rows[i]["TongSoGioKiKT"] + "</td>" +
                          "<td class='HeaderColumn1'>" + dt.Rows[i]["TongSoGioDD"] + "</td>" +
                          "<td class='HeaderColumn1'>" + dt.Rows[i]["TongSoGioKhac"] + "</td>" +
                          "<td class='HeaderColumn1'>" + dt.Rows[i]["TongSoGioCaNam"] + "</td>";
            htmlExport += "</tr>";
        }
        htmlExport += "<tr><td class='HeaderColumn1' colspan='4'>TỔNG CỘNG SỐ GIỜ:</td>";
        for (int x = 0; x < dtLopHoc.Rows.Count; x++)
        {
            htmlExport += "<td class='HeaderColumn1'>" + (list[x][0] + list[x][1] + list[x][2]) + "</td>";
        }
        double tongSoGioKiKT = list.Sum(doublese => doublese[0]);
        double tongSoGioDD = list.Sum(doublese => doublese[1]);
        double tongSoGioKhac = list.Sum(doublese => doublese[2]);
        htmlExport += "<td class='HeaderColumn1'>" + tongSoGioKiKT + "</td>" +
                      "<td class='HeaderColumn1'>" + tongSoGioDD + "</td>" +
                      "<td class='HeaderColumn1'>" + tongSoGioKhac + "</td>" +
                      "<td class='HeaderColumn1'>" + (tongSoGioKiKT + tongSoGioDD + tongSoGioKhac) + "</td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='3' style='font-weight: bold; text-align: center;'>Người lập</td><td></td>" +
                      "<td colspan='3' style='font-weight: bold; text-align: center;'>Người soát xét</td><td></td>" +
                      "<td colspan='3' style='font-weight: bold; text-align: center;'>Người duyệt</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("TongHopSoGioCNKTCuaKTV.xsl", "<html>" + css + htmlExport.Replace("<br>", "\n").Replace("<br />", "\n") + "</html>");
    }
}