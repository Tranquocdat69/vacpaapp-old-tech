﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyLopHoc_ViewEmailForAnonymous.ascx.cs"
    Inherits="usercontrols_CNKT_QuanLyLopHoc_ViewEmailForAnonymous" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<form id="Form1" ClientIDMode="Static" runat="server" method="post" enctype="multipart/form-data">
<div style="float: left; width: 100%; margin-top: 10px;">
    <a id="A2" href="javascript:;" class="btn btn-rounded" onclick="Export('word');"><i
        class="iconsweets-word2"></i>Kết xuất Word</a> <a id="A3" href="javascript:;" class="btn btn-rounded"
            onclick="Export('pdf');"><i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
    <input type="hidden" id="hdAction" name="hdAction" />
</div>
<div style="float: left; width: 100%;">
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" HasExportButton="False"
        HasPrintButton="False" />
</div>
</form>
<script type="text/javascript">
    function Export(type) {
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
</script>
