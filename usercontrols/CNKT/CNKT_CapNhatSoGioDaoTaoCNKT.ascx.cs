﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class usercontrols_CNKT_CapNhatSoGioDaoTaoCNKT : System.Web.UI.UserControl
{
    private string tenChucNang = "Cập nhật số giờ đào tạo, CNKT thực tế";
    private string _malh = "";
    protected string _listPermissionOnFunc_CapNhatSoGioCNKT = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenChucNang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenChucNang + @"</h1> </div>"));

        _listPermissionOnFunc_CapNhatSoGioCNKT = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_CapNhatSoGioCNKT, cm.connstr);

        if (_listPermissionOnFunc_CapNhatSoGioCNKT.Contains("XEM|"))
        {          
            cm.ghilog("CapNhatSoGioCNKT", "\"" + Session["Admin_TenDangNhap"] + "\" vào chức năng cập nhật giờ CNKT");
        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem " + tenChucNang + " !</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Chạy một số xử lý khi trang mới load lên
    /// </summary>
    protected void FirstLoadPage()
    {
        string js = "";
        string postAction = Request.Form["hdPostAction"];
        if (string.IsNullOrEmpty(postAction)) // Trường hợp khi mới load trang (Không phải post dữ liệu)
        {
            _malh = Request.QueryString["malh"];
            if (string.IsNullOrEmpty(_malh))
            {
                // Lấy thông tin lớp học mới nhất làm mặc định khi Load trang
                js += "iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh=&action=loadlh';" + Environment.NewLine;
            }
            else
            {
                // Load thông tin theo mã lớp học đc truyền vào
                js += "$('#txtMaLopHoc').val('" + _malh + "');" + Environment.NewLine;
                js += "CallActionGetInforLopHoc();" + Environment.NewLine;
            }
        }
        else // Trường hợp khi POST dữ liệu
        {
            // Load thông tin lớp học đang xử lý
            string maLopHoc = Request.Form["txtMaLopHoc"];
            js += "$('#txtMaLopHoc').val('" + maLopHoc + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
        }
        Response.Write(js);
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_CapNhatSoGioCNKT.Contains("SUA|"))
            Response.Write("$('#btn_save').remove();");

        if (!_listPermissionOnFunc_CapNhatSoGioCNKT.Contains("THEM|"))
            Response.Write("$('#btn_save').remove();");

        if (!_listPermissionOnFunc_CapNhatSoGioCNKT.Contains("XOA|"))
            Response.Write("$('#btn_delete').remove();");

        if (!_listPermissionOnFunc_CapNhatSoGioCNKT.Contains("KETXUAT|"))
            Response.Write("$('#" + lbtExport.ClientID + "').remove();");

        if (!_listPermissionOnFunc_CapNhatSoGioCNKT.Contains("XEM|"))
            Response.Write("$('#form_CapNhat').remove();");
    }
}