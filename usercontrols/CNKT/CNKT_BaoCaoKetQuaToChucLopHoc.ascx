﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_BaoCaoKetQuaToChucLopHoc.ascx.cs"
    Inherits="usercontrols_CNKT_BaoCaoKetQuaToChucLopHoc" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
    
    .tdInput
    {
        max-width: 100px;
    }
</style>
<form id="Form1" name="Form1" runat="server">
<h4 class="widgettitle">
    Báo cáo kết quả tổ chức lớp học</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<div id="DivContent">
    <div class="dataTables_length">
        <fieldset class="fsBlockInfor" style="margin-top: 0px;">
            <legend>Thông tin lớp học</legend>
            <table width="100%" border="0" class="formtblInfor">
                <tr>
                    <td style="width: 100px;">
                        Mã lớp học<span class="starRequired">(*)</span>:
                    </td>
                    <td style="width: 110px;">
                        <input type="text" id="txtMaLopHoc" name="txtMaLopHoc" onchange="CallActionGetInforLopHoc();"
                            class="tdInput" />
                        <input type="hidden" name="hdLopHocID" id="hdLopHocID" />
                    </td>
                    <td>
                        <input type="button" value="---" onclick="OpenDanhSachLopHoc();" style="border: 1px solid gray;" />
                    </td>
                    <td style="width: 100px;">
                        Ngày cập nhật<span class="starRequired">(*)</span>:
                    </td>
                    <td style="width: 110px;">
                        <input type='text' id="txtNgayCapNhat" name="txtNgayCapNhat" class="tdInput" />
                    </td>
                    <td>
                        <img src="/images/icons/calendar.png" id="imgCalendarNgayCapNhat" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Tên lớp học:
                    </td>
                    <th colspan="2">
                        <span id="spanTenLopHoc"></span>
                    </th>
                    <td colspan="3">
                    </td>
                </tr>
                <tr>
                    <td>
                        Thời gian tổ chức:
                    </td>
                    <th colspan="2">
                        <span id="spanThoiGianToChuc"></span>
                    </th>
                    <td>
                        Địa điểm:
                    </td>
                    <th colspan="2">
                        <span id="spanDiaDiemToChuc"></span>
                    </th>
                </tr>
                <tr>
                    <td colspan="6">
                        <h4>
                            Chuyên đề, giảng viên:</h4>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table id="tblDanhSachChuyenDe" border="0" class="formtbl" width="100%;">
                            <tr>
                                <th>
                                    STT
                                </th>
                                <th>
                                    Mã chuyên đề
                                </th>
                                <th>
                                    Tên Chuyên đề
                                </th>
                                <th>
                                    Loại
                                </th>
                                <th>
                                    Giảng viên
                                </th>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <table id="tblDanhSachDanhGia" width="100%" border="0" class="formtbl" style="margin-top: 10px;">
        <tr>
            <th style="width: 35px;">
                STT
            </th>
            <th style="min-width: 150px;">
                Nội dung
            </th>
            <th style="width: 100px;">
                Tốt
            </th>
            <th style="width: 100px;">
                Khá
            </th>
            <th style="width: 100px;">
                Trung bình
            </th>
        </tr>
    </table>
    <table width="100%" id="" border="0" class="formtbl">
        <tr>
            <td style="width: 120px;">
                <b>Số phiếu đánh giá thu được:</b>
            </td>
            <td>
                <input type="text" id="txtSoPhieuDanhGia" name="txtSoPhieuDanhGia" style="max-width: 100px;" />
            </td>
        </tr>
        <tr>
            <td style="width: 120px;">
                <b>Đánh giá khác:</b>
            </td>
            <td>
                <textarea id="txtDanhGiaKhac" name="txtDanhGiaKhac" rows="4"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <b>Kiến nghị với BTC:</b>
            </td>
            <td>
                <textarea id="txtKienNghiVoiBTC" name="txtKienNghiVoiBTC" rows="4"></textarea>
            </td>
        </tr>
    </table>
    <div class="dataTables_length" style="text-align: center;">
        <a id="btnSave" href="javascript:;" class="btn" onclick="Save();"><i class="iconfa-plus-sign">
        </i>Lưu</a> <a id="btnDelete" href="javascript:;" class="btn" onclick="Delete();"><i
            class="iconfa-trash"></i>Xóa</a> <a id="btnExport" href="/admin.aspx?page=BTC_BaoCao_KetQuaToChucLopHocCNKTKTV" class="btn" target="_blank" style="display: none;" ><i class="iconfa-download"></i>Kết xuất</a> <a href="javascript:;" id="DivLoading">
                </a>
    </div>
</div>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadDanhGia" width="0px" height="0px"></iframe>
<script type="text/javascript">

<% CheckPermissionOnPage(); %>

// add open Datepicker event for calendar inputs
    $("#txtNgayCapNhat").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarNgayCapNhat").click(function () {
        $("#txtNgayCapNhat").datepicker("show");
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách lớp học
    function OpenDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").empty();
        $("#DivDanhSachLopHoc").append($("<iframe width='100%' height='100%' id='ifDanhSachLopHoc' name='ifDanhSachLopHoc' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyLopHoc_MiniList"));
        $("#DivDanhSachLopHoc").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách lớp học</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu lớp học theo mã lớp khi NSD nhập trực tiếp mã lớp vào ô textbox
    function CallActionGetInforLopHoc() {
        var maLopHoc = $('#txtMaLopHoc').val();
        $('#btnExport').css('display', 'none'); // An nut ket xuat
        $('#btnExport').prop('href', '');
        iframeProcess.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh='+maLopHoc+'&action=loadlh';
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin lớp học từ Dialog danh sách
    function DisplayInforLopHoc(value) {
        var arrValue = value.split(';#');
        $('#hdLopHocID').val(arrValue[0]);
        $('#txtMaLopHoc').val(arrValue[1]);
        $('#spanTenLopHoc').html(arrValue[2]);
        $('#spanThoiGianToChuc').html(arrValue[3] + ' - ' + arrValue[4]);
        $('#spanDiaDiemToChuc').html(arrValue[7]);
        if(arrValue[0].length > 0){
            $('#btnExport').css('display', '');
            $('#btnExport').prop('href', '/admin.aspx?page=BTC_BaoCao_KetQuaToChucLopHocCNKTKTV&idlh=' + arrValue[0] + '&malh=' + arrValue[1]);
        }
        
        // Gọi phần xử lý load danh sách chuyên đề trong iframe
        iframeProcess.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&lophocid=' + arrValue[0] + '&action=loadcd';
        iframeProcess_LoadDanhGia.location = '/iframe.aspx?page=CNKT_baocaoketquatochuclophoc_Process&idlh=' + arrValue[0] + '&action=loadbc';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách lớp học (Gọi hàm này từ trang con)
    function CloseFormDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").dialog('close');
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm vẽ dữ liệu chuyên đề từ mảng dữ liệu trả về từ iFrame
    function DrawData_ChuyenDe(arrData) {
        var tbl = document.getElementById('tblDanhSachChuyenDe');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var chuyenDeID = arrData[i][0];
                var maChuyenDe = arrData[i][1];
                var tenChuyenDe = arrData[i][2];
                var loaiChuyenDe = arrData[i][3];
                var tenLoaiChuyenDe = arrData[i][4];
                var tenGiangVien = arrData[i][9];
                
                var tr = document.createElement('TR');
                var arrColumn = [(i + 1), maChuyenDe, tenChuyenDe, tenLoaiChuyenDe, tenGiangVien];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
    }

    // Created by NGUYEN MANH HUNG - 2014/12/11
    // Vẽ dữ liệu đánh giá ra bảng
    function DrawData(arrData, ngayCapNhat, danhGiaKhac, kienNghiVoiBtc, soPhieuDanhGia) {
        $('#txtNgayCapNhat').val(ngayCapNhat);
        $('#txtDanhGiaKhac').val(danhGiaKhac);
        $('#txtKienNghiVoiBTC').val(kienNghiVoiBtc);
        $('#txtSoPhieuDanhGia').val(soPhieuDanhGia);
        
        var tbl = document.getElementById('tblDanhSachDanhGia');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        
        if(arrData.length > 0) {
            var stt = 1;
            for(var i = 0; i < arrData.length; i ++) {
                var yKienDanhGiaLopHocId = arrData[i][0];
                var noiDung = arrData[i][1];
                var danhGiaTot = arrData[i][2];
                var danhGiaKha = arrData[i][3];
                var danhGiaTrungBinh = arrData[i][4];
                var subId = arrData[i][5];
                var giangVienId = arrData[i][6];
                var tenGiangVien = arrData[i][7];
                var yKienChaId = arrData[i][8];
                
                var tr = document.createElement('TR');
                var arrColumn = [stt, noiDung, danhGiaTot, danhGiaKha, danhGiaTrungBinh];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    if(j == 0)
                        td.style.textAlign = 'center';
                    if(j < 2) {
                        if(subId != "0" && subId.length > 0) { // Là ý kiến con
                            if(j == 1)
                                td.innerHTML = "<i>" + arrColumn[j] + "</i>";    
                        }else {
                            if(yKienDanhGiaLopHocId.length > 0) {
                                td.innerHTML = arrColumn[j];
                                if(j == 1 ){
                                    stt++;
                                    if(i < arrData.length - 1 && ((arrData[i + 1][5] != "0" && arrData[i + 1][5].length > 0) || arrData[i + 1][0].length == 0))
                                        td.colSpan = "4";
                                    td.style.fontWeight = "bold";
                                }
                            } else { // Trường hợp là đánh giá cho giảng viên hoặc các đối tượng khác
                                if(j == 1 ){
                                    td.innerHTML = "<i> -   " + tenGiangVien + "</i>";
                                }
                            }
                        }
                    }
                    else {
                        // Nếu là loại ý kiến cha hoặc có các tiêu chí đánh giá giảng viên ở trong thì bỏ qua.
                        if(i < arrData.length - 1 && (subId == "0" || subId.length == 0) && ((arrData[i + 1][5] != "0" && arrData[i + 1][5].length > 0) || (arrData[i + 1][0].length == 0 && yKienDanhGiaLopHocId.length > 0))) 
                            continue;
                        var type = '';
                        if(j == 2)
                            type = 'Tot';
                        if (j== 3)
                            type = 'Kha';
                        if(j == 4)
                            type = "TrungBinh";
                        var yKienId = yKienDanhGiaLopHocId;
                        if(yKienId.length == 0)
                            yKienId = yKienChaId;
                        td.innerHTML = "<input type='text' id='txtSoLuongDanhGia_"+yKienId+"_" + giangVienId + "_"+type+"' value='"+arrColumn[j]+"' onchange='CheckIsInteger(this);' />";
                    }
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
    }

    function Save() {
        jQuery("#thongbaothanhcong_form").hide();
        jQuery("#thongbaoloi_form").hide();
        var ngayCapNhat = $('#txtNgayCapNhat').val();
        var danhGiaKhac = $('#txtDanhGiaKhac').val();
        var kienNghiVoiBtc = $('#txtKienNghiVoiBTC').val();
        var soPhieuDanhGia = $('#txtSoPhieuDanhGia').val();
        
        var lopHocId = $('#hdLopHocID').val();
        if(lopHocId == '') {
            alert('Phải chọn lớp học để cập nhật thông tin!');
            return;
        }

        var txtSoPhieuDanhGia = document.getElementById('txtSoPhieuDanhGia');
        if(CheckIsInteger(txtSoPhieuDanhGia)) {
            if(!CheckIsNumberPositive(txtSoPhieuDanhGia)) {
                return;
            }
        } else {
            return;
        }
        var value = '';
        $('#tblDanhSachDanhGia tr').each(function(rowIndex, row){
            var temp = '';
            var tds = $(row).find('td');
            tds.each(function(cellIndex, cell) {
                if (cellIndex == 2 || cellIndex == 3 || cellIndex == 4) {
                    var inputs = $(this).find('input');
                    if (inputs.length > 0) {
                        var id = inputs[0].id;
                        var arrId = id.split('_');
                        if (temp == '')
                            temp += arrId[1] + "_" + arrId[2];
                        temp += "_" + $('#' + inputs[0].id).val();
                    }
                }
            });
            value += temp + ";#";
        });
        $('#DivLoading').html('<img src="/images/loaders/loader11.gif" /> Đang xử lý ...');
        var arrData = [lopHocId, ngayCapNhat, danhGiaKhac, kienNghiVoiBtc, soPhieuDanhGia, value];
        iframeProcess_LoadDanhGia.location = '/iframe.aspx?page=CNKT_baocaoketquatochuclophoc_Process';
        window.iframeProcess_LoadDanhGia.Save(arrData);
    }
    
    function SaveSuccess(type) {
        $('#DivLoading').html('');
        if(type == 1){
            jQuery("#thongbaothanhcong_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Lưu báo cáo thành công!").show();
        } else {
            jQuery("#thongbaoloi_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Không thể lưu báo cáo!").show();
        }
        // Reset dữ liệu trên Form
        iframeProcess_LoadDanhGia.location = '/iframe.aspx?page=CNKT_baocaoketquatochuclophoc_Process&idlh=' + $('#hdLopHocID').val() + '&action=loadbc';
        
        window.scrollTo(0, 0);
    }

    function Delete() {
        jQuery("#thongbaothanhcong_form").hide();
        jQuery("#thongbaoloi_form").hide();
        if(confirm('Bạn chắc chắn với thao tác này chứ?')){
            $('#DivLoading').html('<img src="/images/loaders/loader11.gif" /> Đang xử lý ...');
            iframeProcess_LoadDanhGia.location = '/iframe.aspx?page=CNKT_baocaoketquatochuclophoc_Process&idlh='+$('#hdLopHocID').val()+'&action=delete';
        }
    }
    
    function DeleteSuccess(type) {
        $('#DivLoading').html('');
        if(type == 1){
            jQuery("#thongbaothanhcong_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Xóa số liệu báo cáo thành công!").show();
        } else {
            jQuery("#thongbaoloi_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Không thể xóa số liệu báo cáo của lớp này!").show();
        }
        // Reset dữ liệu trên Form
        iframeProcess_LoadDanhGia.location = '/iframe.aspx?page=CNKT_baocaoketquatochuclophoc_Process&idlh=' + $('#hdLopHocID').val() + '&action=loadbc';
        window.scrollTo(0, 0);
    }
</script>
<div id="DivDanhSachLopHoc">
</div>
