﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_CNKT_QuanLyLopHoc_Details : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý lớp học";
    protected int _rowCount_PhiThanhToan = 0; // Dung de ghi lai so dong  cua bang "Phi thanh toan"
    protected string _listPermissionOnFunc_QuanLyLopHoc = "", _listPermissionOnFunc_ChuyenDe = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected string _IDLopHoc = "";
    Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1><a href='admin.aspx?page=cnkt_quanlylophoc' class='MenuFuncLv1'>" + tenchucnang + @"</a>&nbsp;<img src='/images/next.png' style='margin-top:3px; height: 18px;' />&nbsp;<span class='MenuFuncLv2'>Thông tin lớp đào tạo, cập nhật kiến thức</span></h1> </div>"));
            _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
            _listPermissionOnFunc_ChuyenDe = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyChuyenDe, cm.connstr);

            if (Session["MsgError"] != null)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
                Session.Remove("MsgError");
            }
            if (Session["MsgSuccess"] != null)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + Session["MsgSuccess"] + "</div>"));
                Session.Remove("MsgSuccess");
            }

            if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("XEM|"))
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu lớp đào tạo, cập nhật kiến thức!</div>"));
                form_CreateLopHoc.Visible = false;
                return;
            }
            _IDLopHoc = Request.QueryString["id"];
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(_IDLopHoc))
                {
                    DivChuyenDe.Visible = true;
                    DivPhiThanhToan.Visible = true;
                    lbtExport.Visible = true;
                    LoadOldData();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(_IDLopHoc))
                {
                    if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "SendEmail")
                        SendEmail();
                }
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_QuanLyLopHoc.Contains("KETXUAT|"))
            Response.Write("$('#" + lbtExport.ClientID + "').remove();");

        if (!_listPermissionOnFunc_ChuyenDe.Contains("XEM|"))
            Response.Write("$('#" + DivChuyenDe.ClientID + "').remove();");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/20
    /// Load du lieu cu cua ban ghi
    /// </summary>
    protected void LoadOldData()
    {
        if (!string.IsNullOrEmpty(_IDLopHoc))
        {
            // Lay du lieu theo ID ban ghi
            SqlCnktLopHocProvider cnktLopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc cnktLopHoc = cnktLopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
            if (cnktLopHoc != null)
            {
                lblLoaiLopHoc.Text = cnktLopHoc.Loai == "CNKT" ? "Cập nhật kiến thức" : "Đào tạo";
                lblMaLopHoc.Text = cnktLopHoc.MaLopHoc;
                lbtExport.NavigateUrl = "/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc&idlh=" + this._IDLopHoc + "&malh=" + cnktLopHoc.MaLopHoc;
                lblNgayTao.Text = cnktLopHoc.NgayTao.Value.ToString("dd/MM/yyyy");
                lblTenLopHoc.Text = cnktLopHoc.TenLopHoc;
                lblTuNgay.Text = cnktLopHoc.TuNgay.Value.ToString("dd/MM/yyyy");
                lblDenNgay.Text = cnktLopHoc.DenNgay.Value.ToString("dd/MM/yyyy");
                lblHanDangKy.Text = cnktLopHoc.HanDangKy.Value.ToString("dd/MM/yyyy");
                lblDiaChiLopHoc.Text = cnktLopHoc.DiaChiLopHoc;
                if (Library.CheckIsInt32(cnktLopHoc.TinhThanhId))
                {
                    SqlDmTinhProvider dmTinhPro = new SqlDmTinhProvider(ListName.ConnectionString, false, string.Empty);
                    DmTinh dmTinh = dmTinhPro.GetByTinhId(Library.Int32Convert(cnktLopHoc.TinhThanhId));
                    if (dmTinh != null)
                        lblTinhThanh.Text = dmTinh.TenTinh;
                }
                GetPhiThanhToan();
                LoadListChuyenDe();

                SqlDmTrangThaiProvider trangThaiPro = new SqlDmTrangThaiProvider(ListName.ConnectionString, false, string.Empty);
                DmTrangThai trangThai = trangThaiPro.GetByTrangThaiId(cnktLopHoc.TinhTrangId.Value);
                if (trangThai != null && trangThai.TrangThaiId > 0)
                {
                    if (trangThai.MaTrangThai.Trim() == ListName.Status_DaPheDuyet)
                    {
                        lbtThoaiDuyet.Visible = true;
                        lbtSendEmail.Visible = true;
                    }
                    if (trangThai.MaTrangThai.Trim() == ListName.Status_ChoDuyet)
                    {
                        lbtDuyet.Visible = true;
                        lbtTuChoi.Visible = true;
                    }
                    if (trangThai.MaTrangThai.Trim() == ListName.Status_ChoPheDuyetLai)
                        lbtDuyet.Visible = true;

                }
            }
        }
    }

    private void GetPhiThanhToan()
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "SoNgay", "SoTienPhiHVCNCT", "SoTienPhiHVCNLK", "SoTienPhiHVCNDD", "SoTienPhiKTV", "SoTienPhiNQT_TroLyKTV", "SoTienPhiNTQ" });
        using (SqlConnection con = new SqlConnection(ListName.ConnectionString))
        {
            try
            {
                con.Open();
                string command = "SELECT SoNgay, SoTienPhiHVCNCT, SoTienPhiHVCNLK, SoTienPhiHVCNDD, SoTienPhiKTV, SoTienPhiNQT_TroLyKTV, SoTienPhiNTQ FROM tblCNKTLopHocPhi WHERE LopHocID = @LopHocID ORDER BY SoNgay";
                SqlCommand cmd = new SqlCommand(command, con);
                cmd.Parameters.Add("@LopHocID", SqlDbType.Int).Value = this._IDLopHoc;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    DataRow dataRow = dt.NewRow();
                    while (dr.Read())
                    {
                        dataRow = dt.NewRow();
                        double soNgay = Library.DoubleConvert(dr["SoNgay"]);
                        dataRow["SoNgay"] = soNgay;
                        dataRow["SoTienPhiHVCNCT"] = Library.FormatMoney(dr["SoTienPhiHVCNCT"]);
                        dataRow["SoTienPhiHVCNLK"] = Library.FormatMoney(dr["SoTienPhiHVCNLK"]);
                        dataRow["SoTienPhiHVCNDD"] = Library.FormatMoney(dr["SoTienPhiHVCNDD"]);
                        dataRow["SoTienPhiKTV"] = Library.FormatMoney(dr["SoTienPhiKTV"]);
                        dataRow["SoTienPhiNQT_TroLyKTV"] = Library.FormatMoney(dr["SoTienPhiNQT_TroLyKTV"]);
                        dataRow["SoTienPhiNTQ"] = Library.FormatMoney(dr["SoTienPhiNTQ"]);
                        dt.Rows.Add(dataRow);
                    }
                }

                cmd.Cancel();
                dr.Close();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            finally
            {
                con.Close();
            }
        }
        rpPhiThanhToan.DataSource = dt.DefaultView;
        rpPhiThanhToan.DataBind();
    }

    private void LoadListChuyenDe()
    {
        DataTable dt = new DataTable();

        string command = @"SELECT tblCNKTLopHocChuyenDe.ChuyenDeID as ChuyenDeID, tblCNKTLopHocChuyenDe.MaChuyenDe as MaChuyenDe, tblCNKTLopHocChuyenDe.TenChuyenDe as TenChuyenDe, tblCNKTLopHocChuyenDe.LoaiChuyenDe as LoaiChuyenDe,
                                                tblCNKTLopHocChuyenDe.TuNgay as TuNgay, tblCNKTLopHocChuyenDe.DenNgay as DenNgay,tblCNKTLopHocChuyenDe.SoBuoi as SoBuoi, tblCNKTLopHocChuyenDe.GiangVienID as GiangVienID,
		                                        tblDMGiangVien.TenGiangVien as TenGiangVien, tblDMTrinhDoChuyenMon.TenTrinhDoChuyenMon as TrinhDoChuyenMon, tblDMGiangVien.DonViCongTac as DonViCongTac, tblDMGiangVien.ChungChi as ChungChi, tblDMChucVu.TenChucVu as TenChucVu
	                                        FROM tblCNKTLopHocChuyenDe
	                                         LEFT JOIN tblDMGiangVien on tblCNKTLopHocChuyenDe.GiangVienID = tblDMGiangVien.GiangVienID
	                                         LEFT JOIN tblDMChucVu on tblDMGiangVien.ChucVuID = tblDMChucVu.ChucVuID
	                                         LEFT JOIN tblDMTrinhDoChuyenMon on tblDMGiangVien.TrinhDoChuyenMonID = tblDMTrinhDoChuyenMon.TrinhDoChuyenMonID
	                                         WHERE tblCNKTLopHocChuyenDe.LopHocID = " + this._IDLopHoc + @" ORDER BY tblCNKTLopHocChuyenDe.ChuyenDeID ASC";
        dt = _db.GetDataTable(command);
        rpChuyenDe.DataSource = dt.DefaultView;
        rpChuyenDe.DataBind();
    }

    protected string GetLichHocChuyenDe(string idChuyenDe)
    {
        string fullLichHoc = "";
        string query = "SELECT Ngay, Sang, Chieu FROM " + ListName.Table_CNKTLopHocBuoi + " WHERE ChuyenDeID = " + idChuyenDe;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                string sang = !string.IsNullOrEmpty(ht["Sang"].ToString()) ? "Sáng" : "";
                string chieu = !string.IsNullOrEmpty(ht["Chieu"].ToString()) ? "Chiều" : "";
                if (!string.IsNullOrEmpty(ht["Ngay"].ToString()) && (!string.IsNullOrEmpty(sang.Trim()) || !string.IsNullOrEmpty(chieu.Trim())))
                {
                    if (!string.IsNullOrEmpty(fullLichHoc))
                        fullLichHoc += "<br />";
                    string separa = "";
                    if (!string.IsNullOrEmpty(sang.Trim()) && !string.IsNullOrEmpty(chieu.Trim()))
                        separa = "/";
                    fullLichHoc += Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy") + " (" + sang + separa + chieu + ")";
                }
            }
        }

        return fullLichHoc;
    }

    protected string GetTextLoaiChuyenDe(string value)
    {
        switch (value)
        {
            case ListName.Type_LoaiChuyenDe_KeToanKiemToan:
                return "Kế toán, kiểm toán";
            case ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep:
                return "Đạo đức nghề nghiệp";
            case ListName.Type_LoaiChuyenDe_ChuyenDeKhac:
                return "Chuyên đề khác";
        }
        return "";
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/09
    /// Gửi Email tới tất cả các công ty, hội viên cá nhân về thông tin lớp học.
    /// </summary>
    private void SendEmail()
    {
        try
        {
            _db.OpenConnection();
            SqlCnktLopHocProvider pro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc obj = pro.GetByLopHocId(Library.Int32Convert(_IDLopHoc));

            List<string> listMailAddress = new List<string>(); // List Địa chỉ email hợp lệ sẽ được gửi
            List<string> listSubject = new List<string>();
            List<string> listContent = new List<string>();
            List<string> listHoiVienID = new List<string>();
            List<string> listLoaiHoiVien = new List<string>();
            string query = @"SELECT Email, HoiVienTapTheID AS ID, LoaiHoiVienTapThe AS LoaiHV, 'tt' AS Loai FROM tblHoiVienTapThe
                                        UNION
                                        SELECT Email, HoiVienCaNhanID AS ID, LoaiHoiVienCaNhan AS LoaiHV, 'cn' AS Loai FROM tblHoiVienCaNhan";
            //string query = "SELECT Email FROM tblHoiVienTapThe WHERE HoiVienTapTheID = 1 OR HoiVienTapTheID = 2";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                int index = 0;
                foreach (Hashtable ht in listData)
                {
                    listHoiVienID.Add(ht["ID"].ToString());
                    listLoaiHoiVien.Add("");
                    if(ht["Loai"].ToString() == "cn")
                    {
                            listLoaiHoiVien[index] = ht["LoaiHV"].ToString();
                    }
                    else if(ht["Loai"].ToString() == "tt")
                    {
                        if(ht["LoaiHV"].ToString() == "0" || ht["LoaiHV"].ToString() == "2")
                            listLoaiHoiVien[index]  = "3";
                        if(ht["LoaiHV"].ToString() == "1")
                            listLoaiHoiVien[index] = "4";
                    }
                    string email = ht["Email"].ToString();
                    if (!string.IsNullOrEmpty(email) && Library.CheckIsValidEmail(email))
                    {
                        listMailAddress.Add(email);
                        listSubject.Add("VACPA - Thư mời tham gia lớp học mới");
                        string content = "<b>HỘI KIỂM TOÁN VIÊN HÀNH NGHỀ VIỆT NAM - VACPA xin thông báo:</b><br />";
                        content += "Về việc tổ chức lớp học \"<b>" + obj.TenLopHoc + "</b>\". Từ ngày " +
                                                " " + obj.TuNgay.Value.ToString("dd/MM/yyyy") + " đến " + obj.DenNgay.Value.ToString("dd/MM/yyyy");
                        content += "<br />Kính mời các cá nhân và công ty nếu quan tâm xin hãy đăng ký trước ngày <b>" + obj.HanDangKy.Value.ToString("dd/MM/yyyy") + "</b><br />";
                        content += "Chi tiết nội dung thư mời xem tại đường dẫn sau: <a href='https://" + Request.Url.Authority + "/noframe.aspx?page=CNKT_QuanLyLopHoc_ViewEmailForAnonymous&idlh=" + _IDLopHoc + "' target='_blank'>(Nội dung thư mời tham dự lớp đào tạo, cập nhật kiến thức)</a>";
                        listContent.Add(content);
                    }
                    index++;
                }
            }
            if (listMailAddress.Count > 0)
            {
                if (utility.SendEmail(listMailAddress, listSubject, listContent))
                {
                    // Gui thong bao trong he thong
                    for (int i = 0; i < listHoiVienID.Count; i++)
                    {
                        cm.GuiThongBao(listHoiVienID[i], listLoaiHoiVien.Count > i ? listLoaiHoiVien[i] : "", listSubject[0], listContent[0]);
                    }

                    cm.ghilog(ListName.Func_CNKT_QuanLyLopHoc, "Gửi thư mời tham dự lớp học \"" + obj.TenLopHoc + "\"");
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    string msg = "Đã gửi Email tới các công ty trong danh sách.";
                    js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("SendEmailSuccess", js);
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }
    protected void lbtThoaiDuyet_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            SqlCnktLopHocProvider lopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc lopHoc = lopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
            if (lopHoc != null && lopHoc.LopHocId > 0)
            {
                if (lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_DaPheDuyet, _db))
                {
                    lopHoc.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ThoaiDuyet, _db));
                    if (lopHocPro.Update(lopHoc))
                    {
                        Session["MsgSuccess"] = "Thoái duyệt thông tin lớp học thành công!";
                        Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_details&id=" + lopHoc.LopHocId);
                    }
                }
                else
                {
                    Session["MsgError"] = "Không được thoái duyệt thông tin của lớp học này!";
                    Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_details&id=" + lopHoc.LopHocId);
                }
            }
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }
    protected void lbtDuyet_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            SqlCnktLopHocProvider lopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc lopHoc = lopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
            if (lopHoc != null && lopHoc.LopHocId > 0)
            {
                if (lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_ChoDuyet, _db) || lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_ChoPheDuyetLai, _db))
                {
                    lopHoc.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_DaPheDuyet, _db));
                    if (lopHocPro.Update(lopHoc))
                    {
                        Session["MsgSuccess"] = "Duyệt thông tin lớp học thành công!";
                        Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + lopHoc.LopHocId);
                    }
                }
                else
                {
                    Session["MsgError"] = "Không được duyệt thông tin của lớp học này!";
                    Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + lopHoc.LopHocId);
                }
            }
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }
    protected void lbtTuChoi_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            SqlCnktLopHocProvider lopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, string.Empty);
            CnktLopHoc lopHoc = lopHocPro.GetByLopHocId(Library.Int32Convert(this._IDLopHoc));
            if (lopHoc != null && lopHoc.LopHocId > 0)
            {
                if (lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_ChoDuyet, _db) || lopHoc.TinhTrangId.Value.ToString() == utility.GetStatusID(ListName.Status_ChoPheDuyetLai, _db))
                {
                    lopHoc.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_KhongPheDuyet, _db));
                    if (lopHocPro.Update(lopHoc))
                    {
                        Session["MsgSuccess"] = "Từ chối thông tin lớp học thành công!";
                        Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + lopHoc.LopHocId);
                    }
                }
                else
                {
                    Session["MsgError"] = "Không được từ chối thông tin của lớp học này!";
                    Response.Redirect("/admin.aspx?page=cnkt_quanlylophoc_add&id=" + lopHoc.LopHocId);
                }
            }
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }
}