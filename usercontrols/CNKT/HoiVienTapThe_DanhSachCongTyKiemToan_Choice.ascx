﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HoiVienTapThe_DanhSachCongTyKiemToan_Choice.ascx.cs" Inherits="usercontrols_HoiVienTapThe_DanhSachCongTyKiemToan_Choice" %>

<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<form id="Form1" name="Form1" runat="server">
<fieldset class="fsBlockInfor">
    <legend>Thông tin tìm kiếm</legend>
    <table style="width: 100%;" border="0">
        <tr>
            <td>
                <fieldset class="fsBlockInforMini" style="width: 95%; height: 100px;">
                    <legend>Tiêu chí công ty đủ điều kiện kiểm toán cho đơn vị có lợi ích công chúng</legend>
                    <asp:CheckBox ID="cboDuDieuKienKTCK" runat="server" />Trong lĩnh vực chứng khoán<br />
                    <asp:CheckBox ID="cboDuDieuKienKTKhac" runat="server" />Đơn vị có lợi ích công chúng
                    khác
                </fieldset>
            </td>
            <td>
                <fieldset class="fsBlockInforMini" style="width: 95%; height: 100px;">
                    <legend>Tiêu chí công ty phải nộp báo cáo tự kiểm tra</legend>
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                Xếp loại năm trước:
                            </td>
                            <td>
                                <asp:CheckBox ID="cboXepHang1" runat="server" />Tốt&nbsp;<asp:CheckBox ID="cboXepHang2"
                                    runat="server" />Đạt yêu cầu<br />
                                <asp:CheckBox ID="cboXepHang3" runat="server" />Không đạt yêu cầu&nbsp;<asp:CheckBox
                                    ID="cboXepHang4" runat="server" />Yếu kém<br />
                            </td>
                        </tr>
                    </table>
                    <asp:CheckBox ID="cboTieuChi_Khac" runat="server" />Các tiêu chí khác
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="width: 49%;">
                <table width="95%" border="0" class="formtblInforWithoutBorder">
                    <tr>
                        <td>
                            Mã HVTC/CTKT:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMaCongTy" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Loại hình công ty:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLoaiHinh" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Doanh thu:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDoanhThuTu" runat="server" Width="100px" onchange="CheckIsNumber(this);"></asp:TextBox>
                            -
                            <asp:TextBox ID="txtDoanhThuDen" runat="server" Width="100px" onchange="CheckIsNumber(this);"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 49%;">
                <table width="95%" border="0" class="formtblInforWithoutBorder">
                    <tr>
                        <td>
                            Tên công ty kiểm toán:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTenCongTy" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vùng miền:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlVungMien" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Số năm chưa được kiểm tra:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTuNam" runat="server" Width="60px" onchange="CheckIsInteger(this);"></asp:TextBox>
                            -
                            <asp:TextBox ID="txtDenNam" runat="server" Width="60px" onchange="CheckIsInteger(this);"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</fieldset>
<div style="margin-top: 10px; text-align: center; width: 100%;">
    <asp:LinkButton ID="lbtTruyVan" runat="server" CssClass="btn" OnClick="lbtTruyVan_Click"><i class="iconfa-search"></i>Truy vấn</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách công ty kiểm toán phải nộp báo cáo tự kiểm tra</legend>
    <table id="tblDanhSachCongTy" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th style="width: 30px;">
                </th>
                <th style="width: 30px;">
                    STT
                </th>
                <th style="width: 90px;">
                    Mã HVTC/CTKT
                </th>
                <th style="min-width: 120px;">
                    Tên công ty
                </th>
                <th style="width: 90px;">
                    Tên viết tắt
                </th>
                <th style="width: 70px;">
                    Ngày thành lập
                </th>
                <th style="width: 50px;">
                    Đơn vị đủ ĐK KIT lĩnh vực CK
                </th>
                <th style="width: 50px;">
                    Đơn vị đủ ĐK KIT công chứng khác
                </th>
                <th style="width: 50px;">
                    Lần kiểm tra gần nhất
                </th>
                <th style="width: 80px;">
                    Địa chỉ kiểm tra
                </th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rpDanhSachCongTy" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="vertical-align: middle;">
                            <input type="radio" value='<%# Eval("HoiVienTapTheID") + ";#" + Eval("MaHoiVienTapThe") + ";#" + Eval("TenDoanhNghiep")
                             + ";#" + Eval("TenVietTat") + ";#;#;#;#;#;#;#;#" + Eval("TenVungMien") + ";#" + Eval("LanKiemTraGanNhat") + ";#" + Eval("XepLoai") %>'
                                name="HocVien" />
                        </td>
                        <td>
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td>
                            <%# Eval("MaHoiVienTapThe")%>
                        </td>
                        <td>
                            <%# Eval("TenDoanhNghiep")%>
                        </td>
                        <td>
                            <%# Eval("TenVietTat")%>
                        </td>
                        <td>
                            <%# !string.IsNullOrEmpty(Eval("NgayThanhLap").ToString()) ? Library.DateTimeConvert(Eval("NgayThanhLap")).ToString("dd/MM/yyyy") : ""%>
                        </td>
                        <td>
                            <%# !string.IsNullOrEmpty(Eval("NamDuDKKT_CK").ToString()) ? "x" : "" %>
                        </td>
                        <td>
                            <%# !string.IsNullOrEmpty(Eval("NamDuDKKT_Khac").ToString()) ? "x" : ""%>
                        </td>
                        <td>
                            <%# Eval("LanKiemTraGanNhat")%>
                        </td>
                        <td>
                            <%# Eval("TenTinh")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
</fieldset>
</form>
<iframe name="iframeProcess_Add" width="0px" height="0px"></iframe>
<script type="text/javascript">
    $("#<%= Form1.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtTruyVan.ClientID %>').attr('href'));
        }
    });

    $(document).ready(function () {
        $('#tblDanhSachCongTy').stickyTableHeaders();
    });

    function Choose() {
        var value = '';
        $('#tblDanhSachCongTy :radio').each(function () {
            if ($(this).prop("checked") == true) {
                value += $(this).val();
            }
        });
        if (value.length > 0) {
            parent.DisplayInforCongTy(value);
            parent.CloseDanhSachCongTyKiemToan();
        }
        else {
            alert('Phải chọn ít nhất một công ty!');
        }
    }
</script>