﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_BaoCao_GiayChungNhanGioCNKTChoKTV.ascx.cs"
    Inherits="usercontrols_CNKT_BaoCao_GiayChungNhanGioCNKTChoKTV" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Mẫu đăng ký tham dự cập nhật kiến thức lớp ...</h4>
<div>
    <div class="dataTables_length">
        <a href="javascript:;" id="btn_search" class="btn btn-rounded" onclick="open_dmgiangvien_search();">
            <i class="iconfa-search"></i>Tìm kiếm</a> 
        <asp:LinkButton ID="lbtExportWord" runat="server" CssClass="btn btn-rounded" 
            onclick="lbtExportWord_Click">
            <i class="iconsweets-word2"></i>Kết xuất Word
        </asp:LinkButton> 
        <asp:LinkButton ID="lbtExportPdf" runat="server" CssClass="btn btn-rounded" 
            onclick="lbtExportPdf_Click">
            <i class="iconsweets-pdf2"></i>Kết xuất PDF
        </asp:LinkButton>
    </div>
    
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="500px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="670px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasGotoPageButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None"/>
</div>
</form>
