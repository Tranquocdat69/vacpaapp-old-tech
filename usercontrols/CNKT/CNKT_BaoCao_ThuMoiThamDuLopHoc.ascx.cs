﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_CNKT_BaoCao_ThuMoiThamDuLopHoc : System.Web.UI.UserControl
{
    protected string tenchucnang = "Thư mời tham dự lớp đào tạo, cập nhật kiến thức";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string idLopHoc = Request.Form["hdLopHocID"];
            if (string.IsNullOrEmpty(idLopHoc))
                idLopHoc = Library.CheckNull(Request.QueryString["idlh"]);
            if (!string.IsNullOrEmpty(idLopHoc))
            {
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/CNKT/ThuMoiThamDuLopDT_CNKT.rpt"));
                rpt.Database.Tables["dtNoiDung_GiangVienLopHoc"].SetDataSource(GetNoiDung_GiangVienLopHoc(idLopHoc));
                GetThongTinLopHoc(idLopHoc, rpt);
                GetPhiThamGia(idLopHoc, rpt);
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "ThuMoiThamDuLopDTCNKT");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                {
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "ThuMoiThamDuLopDTCNKT");
                    //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                    //xlsFormatOptions.ShowGridLines = true;
                    //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                    //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                    //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

                }
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string malh = Request.Form["txtMaLopHoc"];
        if (string.IsNullOrEmpty(malh))
            malh = Library.CheckNull(Request.QueryString["malh"]);
        if (!string.IsNullOrEmpty(malh))
        {
            string js = "$('#txtMaLopHoc').val('" + malh + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
            Response.Write(js);
        }
    }

    private DataTable GetNoiDung_GiangVienLopHoc(string idLopHoc)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "Ngay", "Buoi", "TenChuyenDe", "LoaiChuyenDe", "TenGiangVien", "ChucVu", "DonViCongTac" });
        if (string.IsNullOrEmpty(idLopHoc))
            return dt;
        string query = @"SELECT LHB.Ngay, LHB.Sang, LHB.Chieu, LHCD.TenChuyenDe, LHCD.LoaiChuyenDe, DMGV.TenGiangVien, DMCV.TenChucVu, DMGV.DonViCongTac FROM tblCNKTLopHocBuoi LHB
                        LEFT JOIN tblCNKTLopHocChuyenDe LHCD ON LHB.ChuyenDeID = LHCD.ChuyenDeID
                        LEFT JOIN tblDMGiangVien DMGV ON LHCD.GiangVienID = DMGV.GiangVienID
                        LEFT JOIN tblDMChucVu DMCV ON DMGV.ChucVuID = DMCV.ChucVuID
                        WHERE LHB.LopHocID = " + idLopHoc + @"
                        ORDER BY Ngay, Chieu";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            DataRow dr;
            foreach (Hashtable ht in listData)
            {
                if (!string.IsNullOrEmpty(Library.CheckNull(ht["Sang"])))
                {
                    dr = dt.NewRow();
                    dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                    dr["Buoi"] = "Sáng";
                    dr["TenChuyenDe"] = ht["TenChuyenDe"] + "<br><i>(Chuyên đề " + Library.GetTenLoaiChuyenDe(ht["LoaiChuyenDe"].ToString()) + ")</i>";
                    dr["TenGiangVien"] = "<b>" + ht["TenGiangVien"] + "</b> - " + ht["TenChucVu"] + "<br>" + ht["DonViCongTac"];
                    dt.Rows.Add(dr);
                }
                if (!string.IsNullOrEmpty(Library.CheckNull(ht["Chieu"])))
                {
                    dr = dt.NewRow();
                    dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                    dr["Buoi"] = "Chiều";
                    dr["TenChuyenDe"] = ht["TenChuyenDe"] + "<br><i>(Chuyên đề " + Library.GetTenLoaiChuyenDe(ht["LoaiChuyenDe"].ToString()) + ")</i>";
                    dr["TenGiangVien"] = "<b>" + ht["TenGiangVien"] + "</b> - " + ht["TenChucVu"] + " - " + ht["DonViCongTac"];
                    dt.Rows.Add(dr);
                }
            }
        }
        return dt;
    }

    private void GetThongTinLopHoc(string idLopHoc, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idLopHoc))
            return;
        string query = "SELECT MaLopHoc, TenLopHoc, TuNgay, DenNgay, HanDangKy, TenTinh, DiaChiLopHoc FROM tblCNKTLopHoc LH LEFT JOIN tblDMTinh ON LH.TinhThanhID = tblDMTinh.TinhID WHERE LopHocID = " + idLopHoc;
        List<Hashtable> listData = _db.GetListData(query);
        {
            Hashtable ht = listData[0];
            DateTime fromDate = Library.DateTimeConvert(ht["TuNgay"]);
            DateTime toDate = Library.DateTimeConvert(ht["DenNgay"]);
            ((TextObject)rpt.ReportDefinition.ReportObjects["Text2"]).Text = "V/v: Mời tham dự lớp cập nhật kiến thức KTV số " + ht["MaLopHoc"];
            TextObject text3 = (TextObject)rpt.ReportDefinition.ReportObjects["Text3"];
            text3.Text = ht["TenTinh"] + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            ((TextObject)rpt.ReportDefinition.ReportObjects["Text6"]).Text =
                "     Thực hiện Kế hoạch đào tạo cập nhật kiến thức năm " + Library.DateTimeConvert(ht["TuNgay"]).Year +
                " và Quyết định số 2213/QĐ-BTC ngày 28/08/2014 của Bộ Tài chính về việc chấp thuận cho Hội kiểm toán viên hành nghề Việt Nam" +
                " (VACPA) được tổ chức cập nhật kiến thức cho kiểm toán viên đăng ký hành nghề, VACPA sẽ tổ chức lớp học số " + ht["MaLopHoc"] + " tại " + ht["TenTinh"] + ", cụ thể như sau:";
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtSoNgayHoc"]).Text = ((toDate - fromDate).TotalDays + 1).ToString() + " ngày, bắt đầu từ";
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNgayHoc"]).Text = "8h30, ngày " + fromDate.ToString("dd/MM/yyyy") + " đến " + toDate.ToString("dd/MM/yyyy");
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtDiaDiem"]).Text = ht["DiaChiLopHoc"].ToString();
            ReportDocument subRpt = rpt.Subreports[0];
            subRpt.DataDefinition.FormulaFields["UnBoundString1"].Text = "\"<b>5. Tính thời gian cập nhật kiến thức:</b> KTV phải tham gia đủ thời lượng của một chuyên đề học thì mới được tính số giờ CNKT của chuyên đề đó. VACPA sẽ cấp &ldquo;<b>Giấy chứng nhận giờ CNKT năm " + Library.DateTimeConvert(ht["TuNgay"]).Year.ToString() + "</b>&rdquo; cho từng học viên.\"";
            subRpt.DataDefinition.FormulaFields["UnBoundString3"].Text = "\"Thực hiện quy định tại điểm 3 Điều 15 Thông tư số 150/2012/TT-BTC ngày 12/9/2012" +
    "của Bộ Tài chính: Chậm nhất là 03 ngày làm việc trước ngày tổ chức mỗi lớp học, Hội nghề nghiệp phải thông báo với Bộ Tài chính về nội dung chương trình, <u>số lượng kiểm toán viên hành nghề đăng ký tham dự học</u>. Vì vậy, đề nghị các học viên đăng ký sớm <span style='color: blue'>trước 17h00 giờ ngày " + Library.DateTimeConvert(ht["HanDangKy"]).ToString("dd/MM/yyyy") + ".</span>\"";
            subRpt.DataDefinition.FormulaFields["UnBoundString4"].Text = "\"Để lớp CNKT số " + ht["MaLopHoc"] + " thiết thực và hiệu quả, đề nghị Quý Công ty tập hợp các câu hỏi vướng mắc có liên quan đến nội dung lớp học này và gửi kèm bản đăng ký học\"";
        }


    }

    private void GetPhiThamGia(string idLopHoc, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idLopHoc))
            return;
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "Ngay", "HVCNCT", "HVCNLK", "HVCNDD", "KTV", "TroLyKTV", "NQT" });
        string query = "SELECT SoNgay Ngay, SoTienPhiHVCNCT HVCNCT, SoTienPhiHVCNLK HVCNLK, SoTienPhiHVCNDD HVCNDD, SoTienPhiKTV KTV, SoTienPhiNQT_TroLyKTV TroLyKTV, SoTienPhiNTQ NQT FROM tblCNKTLopHocPhi WHERE LopHocID = " + idLopHoc + " ORDER BY Ngay";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                DataRow dr = dt.NewRow();
                dr["Ngay"] = Library.DoubleConvert(ht["Ngay"]);
                dr["HVCNCT"] = Library.FormatMoney(ht["HVCNCT"]);
                dr["HVCNLK"] = Library.FormatMoney(ht["HVCNLK"]);
                dr["HVCNDD"] = Library.FormatMoney(ht["HVCNDD"]);
                dr["KTV"] = Library.FormatMoney(ht["KTV"]);
                dr["TroLyKTV"] = Library.FormatMoney(ht["TroLyKTV"]);
                dr["NQT"] = Library.FormatMoney(ht["NQT"]);
                dt.Rows.Add(dr);
            }
        }
        ReportDocument subRpt = rpt.Subreports[0];
        subRpt.SetDataSource(dt);
    }
}