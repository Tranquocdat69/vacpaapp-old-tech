﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Export_Process.ascx.cs" Inherits="usercontrols_Export_Process" %>

<form id="Form1" name="Form1" method="post" enctype="multipart/form-data">
    <input type="hidden" id="hdContent" name="hdContent"/>
    <input type="hidden" id="hdTypeExport" name="hdTypeExport"/>
    <input type="hidden" id="hdFileName" name="hdFileName"/>
</form>
<script type="text/javascript">
    function AddContent(html, type, fileName) {
        $('#hdContent').val(html);
        $('#hdTypeExport').val(type);
        $('#hdFileName').val(fileName);
        $('#Form1').submit();
    }
</script>