﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_CNKT_DangKyHocCaNhan_Add : System.Web.UI.UserControl
{
    private string tenChucNang = "Đăng ký lớp học";
    private string _mahv = "", _malh = "";
    Commons _cm = new Commons();
    Db _db = new Db(ListName.ConnectionString);
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenChucNang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1><a href='admin.aspx?page=CNKT_QuanLyDangKyHoc' class='MenuFuncLv1'>" + tenChucNang + @"</a>&nbsp;<img src='/images/next.png' style='margin-top:3px; height: 18px;' />&nbsp;<span class='MenuFuncLv2'>Đăng ký học theo cá nhân</span></h1> </div>"));

        try
        {
            _db.OpenConnection();
            string postAction = Request.Form["hdPostAction"];
            string hocVienID = Request.Form["hdHocVienID"];
            string lopHocID = Request.Form["hdLopHocID"];
            string dangKyHocCaNhanID = Request.Form["hdDangKyHocCaNhanID"];
            if (!string.IsNullOrEmpty(postAction) && postAction == "Save")
                Save(lopHocID, hocVienID, dangKyHocCaNhanID);
            if (!string.IsNullOrEmpty(postAction) && postAction == "Delete" && !string.IsNullOrEmpty(dangKyHocCaNhanID))
            {
                int success = Delete(dangKyHocCaNhanID, true);
                if (success > 0)
                {
                    string tenHocVien = Request.Form["txtTenHocVien"];
                    string maHocVien = Request.Form["txtMaHocVien"];
                    string tenLopHoc = Request.Form["txtTenLopHoc"];
                    string maLopHoc = Request.Form["txtMaLopHoc"];
                    _cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Xóa thông tin đăng ký học của \"" + tenHocVien + "(" + maHocVien + ")\" với lớp \"" + tenLopHoc + "\"(" + maLopHoc + ")");
                    Response.Redirect("/admin.aspx?page=CNKT_QuanLyDangKyHoc&lophocid=" + lopHocID);
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' data-dismiss='alert'>×</button>Đã xóa thông tin đăng ký học thành công!</div>"));
                }
            }
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }

    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Chạy một số xử lý khi trang mới load lên
    /// </summary>
    protected void FirstLoadPage()
    {
        string js = "";
        // Gán giá trị "ngày đăng ký" = ngày hiện tại khi mới load trang
        js += "$('#txtNgayDangKy').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;

        string postAction = Request.Form["hdPostAction"];
        if (string.IsNullOrEmpty(postAction)) // Trường hợp khi mới load trang (Không phải post dữ liệu)
        {
            _malh = Request.QueryString["malh"];
            if (string.IsNullOrEmpty(_malh))
            {
                // Lấy thông tin lớp học mới nhất làm mặc định khi Load trang
                js += "iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh=&action=loadlh';" + Environment.NewLine;
            }
            else
            {
                // Load thông tin theo mã lớp học đc truyền vào
                js += "$('#txtMaLopHoc').val('" + _malh + "');" + Environment.NewLine;
                js += "CallActionGetInforLopHoc();" + Environment.NewLine;
            }

            // Load thông tin theo mã học viên đc truyền vào
            _mahv = Request.QueryString["mahv"];
            if (!string.IsNullOrEmpty(_mahv))
            {
                js += "$('#txtMaHocVien').val('" + _mahv + "');" + Environment.NewLine;
                js += "CallActionGetInforHocVien();" + Environment.NewLine;
            }
        }
        else // Trường hợp khi POST dữ liệu
        {
            // Load thông tin lớp học đang xử lý
            string maLopHoc = Request.Form["txtMaLopHoc"];
            js += "$('#txtMaLopHoc').val('" + maLopHoc + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
        }
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/27
    /// Lưu thông tin đăng ký học theo cá nhân
    /// </summary>
    /// <param name="idLopHoc">ID lớp học</param>
    /// <param name="idHocVien">ID học viên</param>
    /// <param name="dangKyHocCaNhanID">ID bản đăng ký học cá nhân (Nếu khác rỗng -> Update)</param>
    private void Save(string idLopHoc, string idHocVien, string dangKyHocCaNhanID)
    {
        try
        {
            _db.OpenConnection();
            string hoiVienTapTheID = Request.Form["hdHoiVienTapTheID"];
            string ngayDangKy = Request.Form["txtNgayDangKy"];
            string layGiayChungNhan = Request.Form["cboCoGCNGioCNKT"];
            string listChuyenDeID = Request.Form["hdListChuyenDeID"];
            string soGioCNKT = Request.Form["txtSoGioCNKT"];
            string soNgay = Request.Form["hdSoNgay"];
            string isClose = Request.Form["hdIsCloseForm"]; // Biến để xác định có đóng Form sau khi đăng ký ko

            if (!string.IsNullOrEmpty(idHocVien))
            {
                // Insert du lieu vao bang tblCNKTDangKyHocCaNhan
                SqlCnktDangKyHocCaNhanProvider dangKyHocCaNhanPro = new SqlCnktDangKyHocCaNhanProvider(ListName.ConnectionString, false, string.Empty);
                SqlTtPhatSinhPhiProvider proPhatSinhPhi = new SqlTtPhatSinhPhiProvider(ListName.ConnectionString, false, string.Empty);
                SqlHoiVienCaNhanProvider proHVCN = new SqlHoiVienCaNhanProvider(ListName.ConnectionString, false, string.Empty);
                CnktDangKyHocCaNhan objDangKyHocCaNhan = new CnktDangKyHocCaNhan(); // Insert
                if (!string.IsNullOrEmpty(dangKyHocCaNhanID) && Library.CheckIsInt32(dangKyHocCaNhanID))// Update
                    objDangKyHocCaNhan = dangKyHocCaNhanPro.GetByDangKyHocCaNhanId(Library.Int32Convert(dangKyHocCaNhanID));
                objDangKyHocCaNhan.LopHocId = Library.Int32Convert(idLopHoc);
                if (!string.IsNullOrEmpty(ngayDangKy))
                    objDangKyHocCaNhan.NgayDangKy = Library.DateTimeConvert(ngayDangKy, '/', 1);
                objDangKyHocCaNhan.LayGiayChungNhan = layGiayChungNhan;
                objDangKyHocCaNhan.HoiVienCaNhanId = Library.Int32Convert(idHocVien);
                objDangKyHocCaNhan.TongSoGioCnkt = Library.DecimalConvert(soGioCNKT);
                //objDangKyHocCaNhan.TongPhi = Library.DecimalConvert(GetTongHocPhi(idLopHoc, idHocVien, (Library.DoubleConvert(soGioCNKT) / 4).ToString()));
                objDangKyHocCaNhan.TongPhi = Library.DecimalConvert(GetTongHocPhi(idLopHoc, idHocVien, soNgay));
                if (!string.IsNullOrEmpty(hoiVienTapTheID))
                    objDangKyHocCaNhan.HoiVienTapTheId = Library.Int32Convert(hoiVienTapTheID);
                if (!string.IsNullOrEmpty(dangKyHocCaNhanID) && Library.CheckIsInt32(dangKyHocCaNhanID))
                {
                    if (dangKyHocCaNhanPro.Update(objDangKyHocCaNhan))
                    {
                        // Lấy ID bản ghi phát sinh phí từ trường hợp đăng ký học này
                        string query = "SELECT PhatSinhPhiID FROM " + ListName.Table_TTPhatSinhPhi + " WHERE DoiTuongPhatSinhPhiID = " + objDangKyHocCaNhan.DangKyHocCaNhanId + " AND LoaiPhi = '4'";
                        List<Hashtable> listData = _db.GetListData(query);
                        if (listData.Count > 0)
                        {
                            TtPhatSinhPhi objPsp = proPhatSinhPhi.GetByPhatSinhPhiId(Library.Int32Convert(listData[0]["PhatSinhPhiID"]));
                            if (objPsp != null && objPsp.PhatSinhPhiId > 0)
                            {
                                objPsp.HoiVienId = Library.Int32Convert(idHocVien);
                                // Lấy loại hội viên cá nhân
                                HoiVienCaNhan objHVCN = proHVCN.GetByHoiVienCaNhanId(Library.Int32Convert(idHocVien));
                                if (objHVCN != null && objHVCN.HoiVienCaNhanId > 0)
                                    objPsp.LoaiHoiVien = objHVCN.LoaiHoiVienCaNhan;
                                objPsp.DoiTuongPhatSinhPhiId = objDangKyHocCaNhan.DangKyHocCaNhanId;
                                objPsp.LoaiPhi = "4";
                                objPsp.NgayPhatSinh = DateTime.Now;
                                objPsp.SoTien = objDangKyHocCaNhan.TongPhi;
                                proPhatSinhPhi.Update(objPsp);
                            }
                        }
                    }
                }
                else
                {
                    if (dangKyHocCaNhanPro.Insert(objDangKyHocCaNhan))
                    {
                        TtPhatSinhPhi objPsp = new TtPhatSinhPhi();
                        objPsp.HoiVienId = Library.Int32Convert(idHocVien);
                        // Lấy loại hội viên cá nhân
                        HoiVienCaNhan objHVCN = proHVCN.GetByHoiVienCaNhanId(Library.Int32Convert(idHocVien));
                        if (objHVCN != null && objHVCN.HoiVienCaNhanId > 0)
                            objPsp.LoaiHoiVien = objHVCN.LoaiHoiVienCaNhan;
                        objPsp.DoiTuongPhatSinhPhiId = objDangKyHocCaNhan.DangKyHocCaNhanId;
                        objPsp.LoaiPhi = "4";
                        objPsp.NgayPhatSinh = DateTime.Now;
                        objPsp.SoTien = objDangKyHocCaNhan.TongPhi;
                        proPhatSinhPhi.Insert(objPsp);
                    }
                }

                SqlCnktLopHocHocVienProvider sqlCnktLopHocHocVienPro = new SqlCnktLopHocHocVienProvider(ListName.ConnectionString, false, string.Empty);

                // Xóa hết dữ liệu đăng ký chuyên đề của học viên trong lớp này
                Delete(objDangKyHocCaNhan.DangKyHocCaNhanId.ToString(), false);

                // Lấy danh sách chuyên đề đã chọn -> mỗi chuyên đề sẽ lưu thành 1 bản ghi trong bảng "tblCNKTLopHocHocVien"
                string status_DaDuyet = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
                // Giá trị ListChuyenDeID = ID1,ID2,ID3,...
                string[] arrChuyenDeID = listChuyenDeID.Split(',');
                foreach (string chuyenDeID in arrChuyenDeID)
                {
                    if (!string.IsNullOrEmpty(chuyenDeID))
                    {
                        CnktLopHocHocVien cnktLopHocHocVien = new CnktLopHocHocVien();
                        cnktLopHocHocVien.DangKyHocCaNhanId = objDangKyHocCaNhan.DangKyHocCaNhanId;
                        cnktLopHocHocVien.HoiVienCaNhanId = Library.Int32Convert(idHocVien);
                        if (!string.IsNullOrEmpty(hoiVienTapTheID))
                            cnktLopHocHocVien.HoiVienTapTheId = Library.Int32Convert(hoiVienTapTheID);
                        cnktLopHocHocVien.LopHocId = Library.Int32Convert(idLopHoc);
                        cnktLopHocHocVien.ChuyenDeId = Library.Int32Convert(chuyenDeID);
                        cnktLopHocHocVien.TinhTrangId = Library.Int32Convert(status_DaDuyet);
                        sqlCnktLopHocHocVienPro.Insert(cnktLopHocHocVien);
                    }
                }
                string tenHocVien = Request.Form["txtTenHocVien"];
                string maHocVien = Request.Form["txtMaHocVien"];
                string tenLopHoc = Request.Form["txtTenLopHoc"];
                string maLopHoc = Request.Form["txtMaLopHoc"];
                _cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Cập nhật thông tin đăng ký học của \"" + tenHocVien + "(" + maHocVien + ")\" với lớp \"" + tenLopHoc + "\"(" + maLopHoc + ")");
                if (isClose == "1")
                    Response.Redirect("/admin.aspx?page=CNKT_QuanLyDangKyHoc");
                else
                {

                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' data-dismiss='alert'>×</button>Cập nhật thông tin đăng ký học thành công!</div>"));
                }
            }
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/27
    /// Xóa thông tin đăng ký học
    /// </summary>
    /// <param name="idLopHoc">ID lớp học</param>
    /// <param name="idHocVien">ID học viên</param>
    private int Delete(string dangKyHocCaNhanID, bool deleteAll)
    {
        if (!string.IsNullOrEmpty(dangKyHocCaNhanID))
        {
            Db db = new Db(ListName.ConnectionString);
            try
            {
                db.OpenConnection();
                string query = "";
                if (deleteAll)
                {
                    query = "DELETE FROM tblCNKTDangKyHocCaNhan WHERE DangKyHocCaNhanID = " + dangKyHocCaNhanID + ";";
                    query += "DELETE FROM tblTTPhatSinhPhi WHERE DoiTuongPhatSinhPhiID = " + dangKyHocCaNhanID;
                    db.ExecuteNonQuery(query);
                }

                query = "DELETE FROM tblCNKTLopHocHocVien WHERE DangKyHocCaNhanID = " + dangKyHocCaNhanID;
                db.ExecuteNonQuery(query);
            }
            catch { }
            finally
            {
                db.CloseConnection();
            }
        }
        return 0;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/04/15
    /// Ham tinh tong so hoc phi phai nop theo so buoi dang ky
    /// </summary>
    /// <param name="idLopHoc">ID lop hoc dang ky</param>
    /// <param name="idHoiVienCaNhan">ID hoi vien ca nhan dang ky</param>
    /// <param name="tongSoBuoi">tong so buoi hoc</param>
    /// <returns></returns>
    private double GetTongHocPhi(string idLopHoc, string idHoiVienCaNhan, string soNgay)
    {
        double tongHocPhi = 0;
        string loaiHvcn = "", troLyKtv = "", loaiHvcnct = "";
        // Lay loai hoi vien ca nhan
        string query = "SELECT LoaiHoiVienCaNhan, LoaiHoiVienCaNhanChiTiet, TroLyKTV FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + idHoiVienCaNhan;
        List<Hashtable> listDataHvcn = _db.GetListData(query);
        if (listDataHvcn.Count > 0)
        {
            loaiHvcn = listDataHvcn[0]["LoaiHoiVienCaNhan"].ToString();
            loaiHvcnct = listDataHvcn[0]["LoaiHoiVienCaNhanChiTiet"].ToString();
            troLyKtv = listDataHvcn[0]["TroLyKTV"].ToString();
        }

        // Lay bang hoc phi
        query = "SELECT SoTienPhiHVCNCT, SoTienPhiHVCNLK, SoTienPhiHVCNDD, SoTienPhiKTV, SoTienPhiNQT_TroLyKTV, SoTienPhiNTQ FROM tblCNKTLopHocPhi WHERE LopHocID = " + idLopHoc + " AND SoNgay = " + soNgay;
        List<Hashtable> listDataHocPhi = _db.GetListData(query);
        if (listDataHocPhi.Count > 0)
        {
            switch (loaiHvcn)
            {
                case "1":
                    if (loaiHvcnct == "0" || loaiHvcnct == "2" || loaiHvcnct == "4" || loaiHvcnct == "5" || loaiHvcnct == "6")
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNCT"]);
                    else if (loaiHvcnct == "1")
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNLK"]);
                    else
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiHVCNDD"]);
                    break;
                case "2":
                    tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiKTV"]);
                    break;
                case "0":
                    if (troLyKtv == "0")
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiNTQ"]);
                    else
                        tongHocPhi = Library.DoubleConvert(listDataHocPhi[0]["SoTienPhiNQT_TroLyKTV"]);
                    break;
            }
        }
        return tongHocPhi;
    }
}