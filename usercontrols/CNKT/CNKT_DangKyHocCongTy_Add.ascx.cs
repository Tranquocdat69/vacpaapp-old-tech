﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_CNKT_DangKyHocCongTy_Add : System.Web.UI.UserControl
{
    private string tenChucNang = "Đăng ký lớp học";
    private string _mact = "", _malh = "";
    Commons _cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenChucNang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1><a href='admin.aspx?page=CNKT_QuanLyDangKyHoc' class='MenuFuncLv1'>" + tenChucNang + @"</a>&nbsp;<img src='/images/next.png' style='margin-top:3px; height: 18px;' />&nbsp;<span class='MenuFuncLv2'>Đăng ký học theo công ty</span></h1> </div>"));

        string postAction = Request.Form["hdPostAction"];
        string hocVienID = Request.Form["hdHocVienID"];
        string lopHocID = Request.Form["hdLopHocID"];
        if (!string.IsNullOrEmpty(postAction) && postAction == "Save")
            Save(lopHocID, hocVienID);
        if (!string.IsNullOrEmpty(postAction) && postAction == "Delete")
        {
            int success = Delete(lopHocID, hocVienID);
            if (success > 0)
            {
                string tenHocVien = Request.Form["txtTenHocVien"];
                string maHocVien = Request.Form["txtMaHocVien"];
                string tenLopHoc = Request.Form["txtTenLopHoc"];
                string maLopHoc = Request.Form["txtMaLopHoc"];
                _cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Xóa thông tin đăng ký học của \"" + tenHocVien + "(" + maHocVien + ")\" với lớp \"" + tenLopHoc + "\"(" + maLopHoc + ")");
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' data-dismiss='alert'>×</button>Đã xóa thông tin đăng ký học thành công!</div>"));
            }
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Chạy một số xử lý khi trang mới load lên
    /// </summary>
    protected void FirstLoadPage()
    {
        string js = "";
        // Gán giá trị "ngày đăng ký" = ngày hiện tại khi mới load trang
        js += "$('#txtNgayDangKy').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;

        string postAction = Request.Form["hdPostAction"];
        if (string.IsNullOrEmpty(postAction)) // Trường hợp khi mới load trang (Không phải post dữ liệu)
        {
            _malh = Request.QueryString["malh"];
            if (string.IsNullOrEmpty(_malh))
            {
                // Lấy thông tin lớp học mới nhất làm mặc định khi Load trang
                js += "iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh=&action=loadlh';" + Environment.NewLine;
            }
            else
            {
                // Load thông tin theo mã lớp học đc truyền vào
                js += "$('#txtMaLopHoc').val('" + _malh + "');" + Environment.NewLine;
                js += "CallActionGetInforLopHoc();" + Environment.NewLine;
            }

            // Load thông tin theo mã công ty đc truyền vào
            _mact = Request.QueryString["mact"];
            if (!string.IsNullOrEmpty(_mact))
            {
                js += "$('#txtMaCongTy').val('" + _mact + "');" + Environment.NewLine;
                js += "CallActionGetInforCongTy();" + Environment.NewLine;
            }
        }
        else // Trường hợp khi POST dữ liệu
        {
            // Load thông tin lớp học đang xử lý
            string maLopHoc = Request.Form["txtMaLopHoc"];
            js += "$('#txtMaLopHoc').val('" + maLopHoc + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
        }
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Lưu thông tin đăng ký học theo cá nhân
    /// </summary>
    /// <param name="idLopHoc">ID lớp học</param>
    /// <param name="idHocVien">ID học viên</param>
    private void Save(string idLopHoc, string idHocVien)
    {
        string ngayDangKy = Request.Form["txtNgayDangKy"];
        string layGiayChungNhan = Request.Form["cboCoGCNGioCNKT"];
        string listChuyenDeID = Request.Form["hdListChuyenDeID"];
        string isClose = Request.Form["hdIsCloseForm"]; // Biến để xác định có đóng Form sau khi đăng ký ko

        if (!string.IsNullOrEmpty(idHocVien))
        {
            SqlCnktLopHocHocVienProvider sqlCnktLopHocHocVienPro = new SqlCnktLopHocHocVienProvider(ListName.ConnectionString, false, string.Empty);

            // Xóa hết dữ liệu đăng ký chuyên đề của học viên trong lớp này
            Delete(idLopHoc, idHocVien);

            // Lấy danh sách chuyên đề đã chọn -> mỗi chuyên đề sẽ lưu thành 1 bản ghi trong bảng "tblCNKTLopHocHocVien"
            // Giá trị ListChuyenDeID = ID1,ID2,ID3,...
            string[] arrChuyenDeID = listChuyenDeID.Split(',');
            foreach (string chuyenDeID in arrChuyenDeID)
            {
                if (!string.IsNullOrEmpty(chuyenDeID))
                {
                    CnktLopHocHocVien cnktLopHocHocVien = new CnktLopHocHocVien();
                    cnktLopHocHocVien.HoiVienCaNhanId = Library.Int32Convert(idHocVien);
                    cnktLopHocHocVien.LopHocId = Library.Int32Convert(idLopHoc);
                    cnktLopHocHocVien.ChuyenDeId = Library.Int32Convert(chuyenDeID);
                    //cnktLopHocHocVien.NgayDangKy = Library.DateTimeConvert(ngayDangKy, '/', 1);
                    //cnktLopHocHocVien.LayGiayChungNhan = layGiayChungNhan;
                    sqlCnktLopHocHocVienPro.Insert(cnktLopHocHocVien);
                }
            }
            string tenHocVien = Request.Form["txtTenHocVien"];
            string maHocVien = Request.Form["txtMaHocVien"];
            string tenLopHoc = Request.Form["txtTenLopHoc"];
            string maLopHoc = Request.Form["txtMaLopHoc"];
            _cm.ghilog(ListName.Func_CNKT_QuanLyDangKyHoc, "Cập nhật thông tin đăng ký học của \"" + tenHocVien + "(" + maHocVien + ")\" với lớp \"" + tenLopHoc + "\"(" + maLopHoc + ")");
            if (isClose == "1")
                Response.Redirect("/admin.aspx?page=CNKT_QuanLyDangKyHoc");
            else
            {

                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' data-dismiss='alert'>×</button>Cập nhật thông tin đăng ký học thành công!</div>"));
            }
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Xóa thông tin đăng ký học
    /// </summary>
    /// <param name="idLopHoc">ID lớp học</param>
    /// <param name="idHocVien">ID học viên</param>
    private int Delete(string idLopHoc, string idHocVien)
    {
        if (!string.IsNullOrEmpty(idLopHoc) && !string.IsNullOrEmpty(idHocVien))
        {
            Db db = new Db(ListName.ConnectionString);
            try
            {
                db.OpenConnection();
                string query = "DELETE FROM tblCNKTLopHocHocVien WHERE HoiVienCaNhanID = " + idHocVien + @" AND LopHocID = " + idLopHoc;
                return db.ExecuteNonQuery(query);
            }
            catch { }
            finally
            {
                db.CloseConnection();
            }
        }
        return 0;
    }
}