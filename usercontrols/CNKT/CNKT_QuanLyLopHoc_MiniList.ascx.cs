﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class usercontrols_CNKT_QuanLyLopHoc_MiniList : System.Web.UI.UserControl
{
    private Db _db = new Db(ListName.ConnectionString);
    private string _mode = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadListLopHoc();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Load danh sach "Lop hoc" theo dieu kien search
    /// </summary>
    private void LoadListLopHoc()
    {
        try
        {
            _db.OpenConnection();
            string query = GetSqlCommandLoadListClass();
            DataTable dt = _db.GetDataTable(query);
            rpLopHoc.DataSource = dt.DefaultView;
            rpLopHoc.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Tạo chuỗi query theo điều kiện search
    /// </summary>
    /// <returns>query string</returns>
    private string GetSqlCommandLoadListClass()
    {
        string maLopHoc = txtMaLopHoc.Text;
        string tenLopHoc = txtTenLop.Text;
        string txtFromDate = txtTuNgay.Text;
        string txtToDate = txtDenNgay.Text;
        string command =
            @"SELECT LopHocID, MaLopHoc, TenLopHoc , TuNgay, DenNgay, HanDangKy, tblDMTinh.TenTinh AS TenTinh, DiaChiLopHoc                                 
                                        FROM tblCNKTLopHoc LEFT JOIN tblDMTinh ON tblCNKTLopHoc.TinhThanhID = tblDMTinh.TinhID
                                        LEFT JOIN tblDMTrangThai ON tblCNKTLopHoc.TinhTrangID = tblDMTrangThai.TrangThaiID
                                         WHERE {ClassID} AND {TenLopHoc} AND {Datetime1} AND {Datetime2} AND tblDMTrangThai.MaTrangThai = '" + ListName.Status_DaPheDuyet + @"' ORDER BY NgayTao DESC";

        if (!string.IsNullOrEmpty(maLopHoc))
            command = command.Replace("{ClassID}", "MaLopHoc like '%" + maLopHoc + "%'");
        else
            command = command.Replace("{ClassID}", "1=1");

        if (!string.IsNullOrEmpty(tenLopHoc))
            command = command.Replace("{TenLopHoc}", "TenLopHoc like N'%" + tenLopHoc + "%'");
        else
            command = command.Replace("{TenLopHoc}", "1=1");

        if (!string.IsNullOrEmpty(txtFromDate))
        {
            txtFromDate = Library.ConvertDatetime(txtFromDate, '/');
            command = command.Replace("{Datetime1}",
                                      "(TuNgay >= '" + txtFromDate + "' OR DenNgay >= '" + txtFromDate + "')");
        }
        else
            command = command.Replace("{Datetime1}", "1=1");
        if (!string.IsNullOrEmpty(txtToDate))
        {
            txtToDate = Library.ConvertDatetime(txtToDate, '/');
            command = command.Replace("{Datetime2}",
                                      "(TuNgay <= '" + txtToDate + "' OR DenNgay <= '" + txtToDate + "')");
        }
        else
            command = command.Replace("{Datetime2}", "1=1");

        return command;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadListLopHoc();
    }

    protected string GetStatusLopHoc(object objTuNgay, object objDenNgay)
    {
        DateTime tuNgay = Library.DateTimeConvert(objTuNgay);
        DateTime denNgay = Library.DateTimeConvert(objDenNgay);
        DateTime ngayHT = DateTime.Now;
        if (ngayHT < tuNgay)
            return "<span style='color: green;'>(Chưa bắt đầu)</span>";
        if (ngayHT >= tuNgay && ngayHT <= denNgay)
            return "<span style='color: red'>(Đang đào tạo)</span>";
        if (ngayHT > denNgay)
            return "(Đã kết thúc)";
        return "";
    }
}