﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyLopHoc_Process.ascx.cs" Inherits="usercontrols_CNKT_QuanLyLopHoc_Process" %>

<form id="formChuyenDe" method="post" enctype="multipart/form-data">
    <input type="hidden" name="hdAction" id="hdAction" />
<input type="hidden" name="hdChuyenDeID" id="hdChuyenDeID" />
<input type="hidden" name="hdMaChuyenDe" id="hdMaChuyenDe" />
<input type="hidden" name="hdLoaiChuyenDe" id="hdLoaiChuyenDe" />
<input type="hidden" name="hdTenChuyenDe" id="hdTenChuyenDe" />
<input type="hidden" name="hdTuNgay" id="hdTuNgay" />
<input type="hidden" name="hdDenNgay" id="hdDenNgay" />
<input type="hidden" name="hdSoBuoi" id="hdSoBuoi" />
<input type="hidden" name="hdGiangVienID" id="hdGiangVienID" />
<input type="hidden" name="hdLopHocID" id="hdLopHocID" />
<input type="hidden" name="hdMaLopHoc" id="hdMaLopHoc" />
</form>
<script type="text/javascript">
    function SaveChuyenDe(arrData) {
        var maChuyenDe = arrData[1];
        var loaiChuyenDe = arrData[2];
        var tenChuyenDe = arrData[3];
        var tuNgay = arrData[4];
        var denNgay = arrData[5];
        var soBuoi = arrData[6];
        var giangVienID = arrData[7];
        var lopHocID = arrData[8];
        var maLopHoc = arrData[9];

        $('#hdAction').val("update");
        $('#hdChuyenDeID').val(arrData[0]);
        $('#hdMaChuyenDe').val(maChuyenDe);
        $('#hdLoaiChuyenDe').val(loaiChuyenDe);
        $('#hdTenChuyenDe').val(tenChuyenDe);
        $('#hdTuNgay').val(tuNgay);
        $('#hdDenNgay').val(denNgay);
        $('#hdSoBuoi').val(soBuoi);
        $('#hdGiangVienID').val(giangVienID);
        $('#hdLopHocID').val(lopHocID);
        $('#hdMaLopHoc').val(maLopHoc);

        $('#formChuyenDe').submit();
    }

    function DeleteChuyenDe(arrChuyenDeID) {
        $('#hdAction').val("delete");
        //        var listChuyenDeID = '';
        //        for (var i = 0; i < arrChuyenDeID.length; i++) {
        //            listChuyenDeID += i + ',';
        //        }
        $('#hdChuyenDeID').val(arrChuyenDeID);
        $('#formChuyenDe').submit();
    }
</script>
