﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dmgiangvien_List.ascx.cs" Inherits="usercontrols_dmgiangvien_List" %>

<style type="text/css">
    .tdTable
    {
        width: 120px;
    }
</style>
<form id="FormDanhSachGiangVien" runat="server" method="post" enctype="multipart/form-data">
<fieldset class="fsBlockInfor">
    <legend>Tiêu chí tìm kiếm</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td>
                Mã giảng viên:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtMaGiangVien" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </td>
            <td>
                Trình độ chuyên môn:
            </td>
            <td class="tdTable">
                <asp:DropDownList ID="ddlTrinhDoChuyenMon" runat="server" name='ddlTrinhDoChuyenMon'>
                </asp:DropDownList>
            </td>
            <td colspan="2">
                
            </td>
        </tr>
        <tr>
            <td>
                Họ và tên:
            </td>
            <td>
                <asp:TextBox ID="txtHoTen" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </td>
            <td>
                Giới tính:
            </td>
            <td>
                <asp:DropDownList ID="ddlGioiTinh" runat="server" name='ddlGioiTinh'>
                    <asp:ListItem Value="">Tất cả</asp:ListItem>
                    <asp:ListItem Value="M">Nam</asp:ListItem>
                    <asp:ListItem Value="F">Nữ</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Chứng chỉ:
            </td>
            <td>
                <asp:TextBox ID="txtChungChi" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Chức vụ:
            </td>
            <td>
                <asp:DropDownList ID="ddlChucVu" runat="server" Width="120px" name='ddlChucVu'>
                </asp:DropDownList>
            </td>
            <td>
                Đơn vị công tác:
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtDonViCongTac" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </td>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearchPopup" runat="server" CssClass="btn" 
        onclick="LinkButton1_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách giảng viên</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <th style="width: 30px;">
            </th>
            <th style="width: 30px;">
                STT
            </th>
            <th style="width: 70px;">
                Mã giảng viên
            </th>
            <th style="min-width: 150px;">
                Họ và tên
            </th>
            <th style="width: 100px;">
                Chức vụ
            </th>
            <th style="width: 150px;">
                Đơn vị công tác
            </th>
            <th style="width: 100px;">
                Trình độ chuyên môn
            </th>
            <th style="width: 100px;">
                Chứng chỉ
            </th>
        </tr>
    </table>
    <div id='DivListGiangVien' style="max-height: 200px;">
        <table id="tblGiangVien" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpGiangVien" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="width: 30px; vertical-align: middle;">
                            <input type="radio" value='<%# Eval("GiangVienID") + ";#" + Eval("TenGiangVien") + ";#" + Eval("ChucVu") + ";#" + Eval("TrinhDoChuyenMon") + ";#" + Eval("DonViCongTac") + ";#" + Eval("ChungChi") %>'
                                name="GiangVien" />
                        </td>
                        <td style="width: 30px;">
                            <%# Eval("STT") %>
                        </td>
                        <td style="width: 70px;">
                            <%# Eval("MaGiangVien") %>
                        </td>
                        <td style="min-width: 150px;">
                            <%# Eval("TenGiangVien") %>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("ChucVu") %>
                        </td>
                        <td style="width: 150px;">
                            <%# Eval("DonViCongTac") %>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("TrinhDoChuyenMon") %>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("ChungChi") %>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</fieldset>
<div style="width: 100%; text-align: right; margin-top: 5px;">
    <a id="btnChoose" href="javascript:;" class="btn" onclick="ChooseGiangVien();"><i
        class="iconfa-ok"></i>Chọn</a> <a id="btnExit" href="javascript:;" class="btn" onclick="parent.CloseFormDMGiangVien();">
            <i class="iconfa-stop"></i>Đóng</a>
</div>
</form>
<script type="text/javascript">
    $("#<%= FormDanhSachGiangVien.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtSearchPopup.ClientID %>').attr('href'));
        }
    });
    
    function ChooseGiangVien() {
        var flag = false;
        $('#tblGiangVien :radio').each(function () {
            checked = $(this).prop("checked");
            if (checked) {
                var chooseItem = $(this).val();
                parent.DisplayInforGiangVien(chooseItem);
                flag = true;
                return;
            }
        });
        if (!flag)
            alert('Phải chọn giảng viên!');
    }

    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivListGiangVien').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivListGiangVien').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivListGiangVien').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }

    myFunction();
</script>