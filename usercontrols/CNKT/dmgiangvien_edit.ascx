﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dmgiangvien_edit.ascx.cs"
    Inherits="usercontrols_dmgiangvien_edit" %>
<style>
    .require
    {
        color: Red;
    }
    .error
    {
        color: Red;
        font-size: 11px;
    }
</style>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("SUA|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");
        return;
    }
    
%>
<form id="form_dmgiangvien_add" clientidmode="Static" runat="server" method="post">
<asp:PlaceHolder ID="PlaceHolder1" runat="server">
    <div id="thongbaoloi_form_themdmgiangvien" name="thongbaoloi_form_themdmgiangvien"
        style="display: none" class="alert alert-error">
    </div>
    <table id="Table1" width="100%" border="0" class="formtbl">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text='ID.HVCN/ID.KTV:'></asp:Label>
            </td>
            <td colspan="6">
                <input type="text" name="txtMaHoiVienCaNhan" id="txtMaHoiVienCaNhan" style="width: 200px;" onchange="CallActionGetInforHocVien();">&nbsp;
                <input type="button" value="---" onclick="OpenDanhSachHocVien();" style="border: 1px solid gray;" />
                <input type="hidden" id="hdHocVienID" name="hdHocVienID"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTenGiangVien" runat="server" Text='Tên:'></asp:Label>
                <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td colspan="6">
                <input type="text" name="Ten" id="Ten">
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblChucVu" runat="server" Text='Chức vụ:'></asp:Label>
                <asp:Label ID="Label7" runat="server"></asp:Label>
            </td>
            <td colspan="6">
                <select name="ChucVuId" id="ChucVuId">
                    <%  
                        try
                        {
                            vLoadChucVu(dt.Rows[0]["ChucVuId"].ToString().Trim());
                        }
                        catch
                        {
                            vLoadChucVu("00");
                        }
              
                    %>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTrinhDoChuyenMon" runat="server" Text='Trình độ chuyên môn:'></asp:Label>
                <asp:Label ID="Label9" runat="server"></asp:Label>
            </td>
            <td colspan="6">
                <select name="TrinhDoChuyenMonId" id="TrinhDoChuyenMonId">
                    <%  
                        try
                        {
                            vLoadTrinhDoChuyenMon(dt.Rows[0]["TrinhDoChuyenMonId"].ToString().Trim());
                        }
                        catch
                        {
                            vLoadTrinhDoChuyenMon("00");
                        }
              
                    %>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblDonViCongTac" runat="server" Text='Đơn vị công tác:'></asp:Label>
                <asp:Label ID="Label4" runat="server"></asp:Label>
            </td>
            <td colspan="6">
                <input type="text" name="DonViCongTac" id="DonViCongTac">
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblChungChi" runat="server" Text='Chứng chỉ:'></asp:Label>
                <asp:Label ID="Label3" runat="server"></asp:Label>
            </td>
            <td colspan="6">
                <input type="text" name="ChungChi" id="ChungChi">
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblGioiTinh" runat="server" Text='Giới tính:'></asp:Label>
                <asp:Label ID="Label5" runat="server"></asp:Label>
            </td>
            <td colspan="6">
                <input type="radio" id="GioiTinhNam" name="GioiTinh" value="M">Nam
                <input type="radio" id="GioiTinhNu" name="GioiTinh" value="F">Nữ
            </td>
        </tr>
        <tr>
            <td>
                Ngày sinh:
            </td>
            <td colspan="6">
                <input type="text" name="txtNgaySinh" id="txtNgaySinh" style="width: 120px;">&nbsp;
                <img src="/images/icons/calendar.png" id="imgNgaySinh" />
            </td>
        </tr>
    </table>
</asp:PlaceHolder>
</form>
<div id="DivDanhSachHocVien"></div>
<iframe name="iframeProcess_LoadHocVien" width="0px" height="0px"></iframe>
<script type="text/javascript">
       $("#txtNgaySinh").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgaySinh").click(function () {
        $("#txtNgaySinh").datepicker("show");
    });

// Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách học viên
    function OpenDanhSachHocVien() {
        $("#DivDanhSachHocVien").empty();
        $("#DivDanhSachHocVien").append($("<iframe width='100%' height='100%' id='ifDanhSachHocVien' name='ifDanhSachHocVien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hoiviencanhan_minilist"));
        $("#DivDanhSachHocVien").dialog({
            resizable: true,
            width: 700,
            height: 500,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách học viên</b>",
            modal: true,
            zIndex: 1000
        });
    }

    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu học viện theo mã học viên khi NSD nhập trực tiếp mã học viên vào ô textbox
    function CallActionGetInforHocVien() {
        var maHocVien = $('#txtMaHoiVienCaNhan').val();
        iframeProcess_LoadHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mahv=' + maHocVien + '&action=loadhv';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin học viên từ Dialog danh sách
    function DisplayInforHocVien(value) {
        var arrValue = value.split(';#');
        $('#hdHocVienID').val(arrValue[0]);
        $('#txtMaHoiVienCaNhan').val(arrValue[1]);
        $('#Ten').val(arrValue[3]);
        $('#ChungChi').val(arrValue[4]);
        $('#DonViCongTac').val(arrValue[6]);
        var gioiTinh = arrValue[7];
        if (gioiTinh == "0")
            $('#GioiTinhNu').prop('checked', true);
        else
            $('#GioiTinhNam').prop('checked', true);
        $('#txtNgaySinh').val(arrValue[12]);
        $('#ChucVuId').val(arrValue[13]);
        iframeProcess_LoadHocVien.location = '/iframe.aspx?page=DanhMuc_Process&idhv=' + arrValue[0] + '&action=loadchungchiquocte';
    }
    
    function SetChungChiQuocTe(value) {
        var chungChi = $('#ChungChi').val();
        if(chungChi.length > 0 && value.length > 0) {
            chungChi += ", " + value;
        } else {
            chungChi += value;
        }
        $('#ChungChi').val(chungChi);
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách học viên (Gọi hàm này từ trang con)
    function CloseFormDanhSachHocVien() {
        $("#DivDanhSachHocVien").dialog('close');
    }

       <% load_giatricu(); %>


        function submitform() {        
                jQuery("#form_dmgiangvien_add").submit();
        }



    jQuery("#form_dmgiangvien_add").validate({
        rules: {
            Ten: {
                required: true
            },
            GioiTinh: {
                required: true
            },
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery("#thongbaoloi_form_suadmgiangvien").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_suadmgiangvien").hide()
            }
        }
    });
     
 $('input:radio[name="GioiTinh"]').change(
    function(){
    var vGioiTinh = document.getElementById('GioiTinh');
        if ($(this).is(':checked') && $(this).val() == 'F') {
            vGioiTinh.value = 'F';
        }
        else if ($(this).is(':checked') && $(this).val() == 'M') {
            vGioiTinh.value = 'M';
        }
    });

</script>
