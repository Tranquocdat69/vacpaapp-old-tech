﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.Drawing;

public partial class usercontrols_CNKT_BaoCao_DanhSachNQTThamDuCNKT : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo danh sách Người quan tâm đăng ký tham dự lớp Cập nhật kiến thức";

    private Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        DataBaseDb db = new DataBaseDb();
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        // _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            //_db.OpenConnection();
            string strIDMaLopHoc = string.Empty;
            string strMaLopHoc = string.Empty;
            string strThoiGianTu = "";
            string strThoiGianDen = "";
            if (!string.IsNullOrEmpty(Request.Form["hdLopHocID"]))
            {
                strIDMaLopHoc = Request.Form["hdLopHocID"];
            }
            if (!string.IsNullOrEmpty(Request.Form["txtMaLopHoc"]))
            {
                strMaLopHoc = Request.Form["txtMaLopHoc"];
            }

            DateTime dtFrom = new DateTime(DateTime.Now.Year, 1, 1);
            DateTime dtTo = new DateTime(DateTime.Now.Year, 12, 31);
            if (!string.IsNullOrEmpty(Request.Form["hdLopHocID"]))
            {
                if (!string.IsNullOrEmpty(Request.Form["txtNgayDaoTaoTu"]))
                {
                    strThoiGianTu = Request.Form["txtNgayDaoTaoTu"];
                    dtFrom = Library.DateTimeConvert(strThoiGianTu, "dd/MM/yyyy");

                }
                if (!string.IsNullOrEmpty(Request.Form["txtNgayDaoTaoDen"]))
                {
                    strThoiGianDen = Request.Form["txtNgayDaoTaoDen"];
                    dtTo = Library.DateTimeConvert(strThoiGianDen, "dd/MM/yyyy");
                }

                string[] paraName = { "@idlophoc", "@datefrom", "@dateto", "@loaihoivien" };
                object[] paraValue = { strIDMaLopHoc, dtFrom, dtTo, 0 };
                //if (string.IsNullOrEmpty(strThoiGianTu) && string.IsNullOrEmpty(strThoiGianDen))
                //{
                //    paraValue = new object[] { strIDMaLopHoc, DBNull.Value, DBNull.Value, 2 };
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(strThoiGianTu) && !string.IsNullOrEmpty(strThoiGianDen))
                //    {
                //        paraValue = new object[] { strIDMaLopHoc, dtFrom, dtTo, 2 };
                //    }
                //    else
                //    {
                //        if (string.IsNullOrEmpty(strThoiGianTu))
                //        {
                //            paraValue = new object[] { strIDMaLopHoc, DBNull.Value, dtTo, 2 };
                //        }
                //        if (string.IsNullOrEmpty(strThoiGianDen))
                //        {
                //            paraValue = new object[] { strIDMaLopHoc, dtFrom, DBNull.Value, 2 };
                //        }
                //    }
                //}



                //dsThanhToan ds = new dsThanhToan();
                DataSet ds = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable dt1 = new DataTable();

                // dt1.TableName = "NopHoiPhiCaNhan";
                dt1 = db.GetDataTable(paraValue, paraName, "proc_BAOCAO_QLDT_KTVCAPNHATKIENTHUC_DINAMICCOLUMN", "KTVCNKT3");
                ds = db.GetData(paraValue, paraName, "proc_BAOCAO_QLDT_KTVCAPNHATKIENTHUC_KTV", "KTVCNKT");
                ds2 = db.GetData(paraValue, paraName, "proc_BAOCAO_QLDT_KTVCAPNHATKIENTHUC_CHUYENDE", "KTVCNKTChuyenDe");
                //ds.Tables.Add(dt1);
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/CNKT/rptBaoCaoDanhSachKTVThamDuCNKT.rpt"));
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "DANH SÁCH NGƯỜI QUAN TÂM THAM DỰ";
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtHaNoi"]).Text = "Hà Nội, Ngày " + dtFrom.Day + "/" + dtFrom.Month + "/" + dtFrom.Year + " - " + dtTo.Day + "/" + dtTo.Month + "/" + dtTo.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtLop"]).Text = "CẬP NHẬT KIẾN THỨC LỚP SỐ  " + strMaLopHoc + "A NĂM " + dtTo.Year;
                rpt.SetDataSource(ds.Tables["KTVCNKT"]);
                // rpt.Subreports[0].DataSourceConnections.Clear();
                rpt.Subreports[0].SetDataSource(ds2.Tables["KTVCNKTChuyenDe"]);
                int maxRight = 0;
                int maxRightb = 0;
                int leftTextBox1 = 7320;
                int leftTextBox2 = 9960;
                for (int i = 0; i < dt1.Columns.Count; i++)
                {
                    int width = 2260;
                    // int widthb = 2280;

                    maxRight = leftTextBox1 + (width * 2) + (480 * 2);
                    maxRightb = leftTextBox2 + (width * 2) + (480 * 2);
                    // maxRight = (7200 + width + 50) + (width * i) + (i * 100);
                    //maxRight = (7200 + width + 50) + (width * i) + (i * 100);
                    //Response.Write("<br/>" + (7320 + (width * i) + (width * i) + (i * 400)));
                    if (i <= 0)
                    {
                        CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtSang" + i, "Sáng ngày " + dt1.Columns[i].ColumnName, leftTextBox1, 520, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtChieu" + i, "Chiều ngày " + dt1.Columns[i].ColumnName, leftTextBox2, 520, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);

                        CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtCDSang" + i, "" + dt1.Rows[0][i].ToString(), leftTextBox1, 880, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtCDChieu" + i, "" + dt1.Rows[0][i].ToString(), leftTextBox2, 880, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);

                        //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtChieu" + i, "Chiều ngày " + dt1.Columns[i].ColumnName, 9780 + (widthb * i) + (widthb * i)+(i * 150), 480, widthb, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtChieu" + i, "nghien ngày " + dt1.Columns[i].ColumnName, 9600 + ((width + 100) * i) + (i * 100), 500, width + 100, null, null, 12, FontStyle.Bold, true, false, "");
                    }
                    else
                    {

                        CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtSang" + i, "Sáng ngày " + dt1.Columns[i].ColumnName, leftTextBox1, 520, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtChieu" + i, "Chiều ngày " + dt1.Columns[i].ColumnName, leftTextBox2, 520, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);

                        CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtCDSang" + i, "" + dt1.Rows[0][i].ToString(), leftTextBox1, 880, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtCDChieu" + i, "" + dt1.Rows[0][i].ToString(), leftTextBox2, 880, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);


                        //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtChieu" + i, "Chiều ngày " + dt1.Columns[i].ColumnName, 9780 + (widthb * i) + (widthb * i)+(i * 150), 480, widthb, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtChieu" + i, "nghien ngày " + dt1.Columns[i].ColumnName, 9600 + ((width + 100) * i) + (i * 100), 500, width + 100, null, null, 12, FontStyle.Bold, true, false, "");
                    }
                    if (i <= dt1.Columns.Count - 1)
                    {
                        if (i == 0)
                        {
                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineSangF" + i, leftTextBox1 + width + 240, leftTextBox1 + width + 240, 480, 1200, true);
                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailF" + i, leftTextBox1 + width + 240, leftTextBox1 + width + 240, 0, 599, true);

                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterFSang" + i, leftTextBox1 + width + 240, leftTextBox1 + width + 240, 0, 599, true);

                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterFSang" + i, leftTextBox1 + width + 240, leftTextBox1 + width + 240, 0, 599, true);

                            //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailFSang" + i, maxRight, maxRight, 0, 599, true);
                            //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailFChieu" + i, maxRightb, maxRightb, 0, 599, true);

                            //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterFSang" + i, maxRight, maxRight, 0, 599, true);
                            //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterFChieu" + i, maxRightb, maxRightb, 0, 599, true);

                            //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterFSang" + i, maxRight, maxRight, 0, 599, true);
                            //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterFChieu" + i, maxRightb, maxRightb, 0, 599, true);
                        }
                        //if (i == dt1.Columns.Count - 1)
                        //{
                        //    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailL" + i, leftTextBox2 + width + 240, leftTextBox2 + width + 240, 0, 599, true);
                        //    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineChieuL" + i, leftTextBox2 + width + 240, leftTextBox2 + width + 240, 480, 1200, true);

                        //    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterLChieu" + i, leftTextBox2 + width + 240, leftTextBox2 + width + 240, 0, 599, true);

                        //    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterLChieu" + i, leftTextBox2 + width + 240, leftTextBox2 + width + 240, 0, 599, true);

                        //    //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailLSang" + i, maxRight, maxRight, 0, 599, true);
                        //    //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailLChieu" + i, maxRightb, maxRightb, 0, 599, true);

                        //    //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterLSang" + i, maxRight, maxRight, 0, 599, true);
                        //    //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterLChieu" + i, maxRightb, maxRightb, 0, 599, true);

                        //    //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterLSang" + i, maxRight, maxRight, 0, 599, true);
                        //    //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterLChieu" + i, maxRightb, maxRightb, 0, 599, true);
                        //}
                        if (dt1.Columns.Count > 1 && i < dt1.Columns.Count - 1)
                        {

                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineSang" + i, maxRight, maxRight, 480, 1200, true);
                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineChieu" + i, maxRightb, maxRightb, 480, 1200, true);

                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailSang" + i, maxRight, maxRight, 0, 599, true);
                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailChieu" + i, maxRightb, maxRightb, 0, 599, true);

                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterSang" + i, maxRight, maxRight, 0, 599, true);
                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterChieu" + i, maxRightb, maxRightb, 0, 599, true);

                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterSang" + i, maxRight, maxRight, 0, 599, true);
                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterChieu" + i, maxRightb, maxRightb, 0, 599, true);
                        }
                        //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineChieu" + i, (int)Math.Pow(maxRight, 2), (int)Math.Pow(maxRight, 2), 480, 1200, true);
                    }
                    leftTextBox1 += (width * 2) + (480 * 2) + 200;
                    leftTextBox2 += (width * 2) + (480 * 2) + 200;


                    //else
                    //{
                    //    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineSang" + i, maxRight, maxRight, 480, 1200, true);
                    //    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineChieu" + i, maxRightb, maxRightb, 480, 1200, true);
                    //    // CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineChieu" + i, (int)Math.Pow(maxRight, 2), (int)Math.Pow(maxRight, 2), 480, 1200, true);
                    //}


                }

                if (dt1.Columns.Count > 0)
                {
                    TextObject txtTongSoGio = rpt.ReportDefinition.ReportObjects["txtTongSoGio"] as TextObject;
                    //((TextObject)rpt.ReportDefinition.ReportObjects["txtTongSoGio"]).Left = maxRight + txtTongSoGio.Width;
                    txtTongSoGio.Left = maxRight;
                    int with = txtTongSoGio.Width;


                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineRight1", maxRight, maxRight, 120, 1199, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineRight2", maxRight + with, maxRight + with, 120, 1199, true);
                    //txtTongSoGio.Text = "Tổng số giờ CNKT";
                    TextObject txtChuKy = rpt.ReportDefinition.ReportObjects["txtChuKy"] as TextObject;
                    //txtChuKy.Width = maxRight;

                    LineObject lineBottomChuKy = rpt.ReportDefinition.ReportObjects["lineBottomChuKy"] as LineObject;
                    lineBottomChuKy.Right = maxRight;

                    LineObject lineTitleNgay = rpt.ReportDefinition.ReportObjects["lineTitleNgay"] as LineObject;
                    lineTitleNgay.Right = maxRight;


                    LineObject linePageHeader = rpt.ReportDefinition.ReportObjects["linePageHeader"] as LineObject;
                    linePageHeader.Right = maxRight + with;

                    LineObject lineBottomHeader = rpt.ReportDefinition.ReportObjects["lineBottomHeader"] as LineObject;
                    lineBottomHeader.Right = maxRight + with;

                    LineObject lineGroupBottom = rpt.ReportDefinition.ReportObjects["lineGroupBottom"] as LineObject;
                    lineGroupBottom.Right = maxRight + with;

                    LineObject lineBottomDetail = rpt.ReportDefinition.ReportObjects["lineBottomDetail"] as LineObject;
                    lineBottomDetail.Right = maxRight + with;

                    LineObject lineBottomFooter1 = rpt.ReportDefinition.ReportObjects["lineBottomFooter1"] as LineObject;
                    lineBottomFooter1.Right = maxRight + with;
                    LineObject lineBottomFooter2 = rpt.ReportDefinition.ReportObjects["lineBottomFooter2"] as LineObject;
                    lineBottomFooter2.Right = maxRight + with;
                    LineObject lineBottomFooter3 = rpt.ReportDefinition.ReportObjects["lineBottomFooter3"] as LineObject;
                    lineBottomFooter3.Right = maxRight + with;

                    LineObject linePageFooter = rpt.ReportDefinition.ReportObjects["linePageFooter"] as LineObject;
                    linePageFooter.Right = maxRight + with;


                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_GroupHeader, 0, "lineGroupRight1", maxRight + with, maxRight + with, 0, 600, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineFooterRight1", maxRight, maxRight, 0, 1800, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineFooterRight2", maxRight + with, maxRight + with, 0, 1800, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetail2", maxRight, maxRight, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetail1", maxRight + with, maxRight + with, 0, 600, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooter2", maxRight, maxRight, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooter1", maxRight + with, maxRight + with, 0, 600, true);

                }
                else
                {
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineRight1", 12480, 12480, 120, 1199, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineRight2", 13680, 13680, 120, 1199, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_GroupHeader, 0, "lineGroupRight1", 13680, 13680, 0, 600, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineFooterRight1", 12480, 12480, 0, 1800, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineFooterRight2", 13680, 13680, 0, 1800, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetail2", 12480, 12480, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetail1", 13680, 13680, 0, 600, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooter2", 12480, 12480, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooter1", 13680, 13680, 0, 600, true);
                }


                CrystalReportViewer1.ReportSource = rpt;
                //CrystalReportViewer1.DataBind();
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoNopPhiHVCaNhan");
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BaoCaoNopPhiHVCaNhan");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoNopPhiHVCaNhan");
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            //  _db.CloseConnection();
        }
        finally
        {
            // _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        if (!string.IsNullOrEmpty(Request.Form["txtMaLopHoc"]))
        {
            string js = "$('#txtMaLopHoc').val('" + Request.Form["txtMaLopHoc"] + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
            js += "$('#txtNgayDaoTaoTu').val('" + Request.Form["txtNgayDaoTaoTu"] + "');" + Environment.NewLine;
            js += "$('#txtNgayDaoTaoDen').val('" + Request.Form["txtNgayDaoTaoDen"] + "');" + Environment.NewLine;
            Response.Write(js);
        }
    }
}