﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodeEngine.Framework.QueryBuilder.Enums;
using HiPT.VACPA.DL;

using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using CodeEngine.Framework.QueryBuilder;


public partial class usercontrols_CNKT_QuanLyDangKyHoc : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý danh sách đăng ký học";
    protected string _listPermissionOnFunc_QuanLyDangKyHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private string _lopHocId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _listPermissionOnFunc_QuanLyDangKyHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyDangKyHoc, cm.connstr);

        this._lopHocId = Request.QueryString["lophocid"];
        if (_listPermissionOnFunc_QuanLyDangKyHoc.Contains("XEM|"))
        {
            try
            {
                _db.OpenConnection();
                LoadInforLopHoc();
                LoadDangKyHoc(_lopHocId);
                LoadDangKyHocTheoChuyenDe(_lopHocId);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _db.CloseConnection();
            }
        }
        else
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem danh sách đăng ký học!</div>"));
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/04
    /// Load thông tin lớp học theo ID truyền trên URL khi mới load trang
    /// </summary>
    private void LoadInforLopHoc()
    {
        string query = "";
        string maLopHoc = "", tenLopHoc = "", tuNgay = "", denNgay = "", hanDangKy = "";
        // Nếu không chọn lớp học nào thì mặc định lấy lớp học có hạn đăng ký mới nhất
        if (string.IsNullOrEmpty(_lopHocId))
        {
            query = "SELECT TOP 1 LopHocID, MaLopHoc, TenLopHoc, TuNgay, DenNgay, HanDangKy FROM tblCNKTLopHoc LEFT JOIN tblDMTrangThai ON tblCNKTLopHoc.TinhTrangID = tblDMTrangThai.TrangThaiID WHERE tblDMTrangThai.MaTrangThai = '" + ListName.Status_DaPheDuyet + @"' ORDER BY HanDangKy DESC";
            List<Hashtable> listData_LopHoc = _db.GetListData(query);
            if (listData_LopHoc.Count > 0)
            {
                _lopHocId = Library.CheckKeyInHashtable(listData_LopHoc[0], "LopHocID").ToString();
                maLopHoc = Library.CheckKeyInHashtable(listData_LopHoc[0], "MaLopHoc").ToString();
                tenLopHoc = Library.CheckKeyInHashtable(listData_LopHoc[0], "TenLopHoc").ToString();
                tuNgay = (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(listData_LopHoc[0], "TuNgay").ToString())
                              ? Library.DateTimeConvert(Library.CheckKeyInHashtable(listData_LopHoc[0], "TuNgay")).ToString(
                                  "dd/MM/yyyy")
                              : "");
                denNgay = (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(listData_LopHoc[0], "DenNgay").ToString())
                              ? Library.DateTimeConvert(Library.CheckKeyInHashtable(listData_LopHoc[0], "DenNgay")).ToString(
                                  "dd/MM/yyyy")
                              : "");
                hanDangKy = (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(listData_LopHoc[0], "HanDangKy").ToString())
                              ? Library.DateTimeConvert(Library.CheckKeyInHashtable(listData_LopHoc[0], "HanDangKy")).ToString(
                                  "dd/MM/yyyy")
                              : "");
            }
        }
        else
        {
            query = "SELECT MaLopHoc, TenLopHoc, TuNgay, DenNgay, HanDangKy FROM tblCNKTLopHoc WHERE LopHocID = " + _lopHocId;
            List<Hashtable> listData_LopHoc = _db.GetListData(query);
            if (listData_LopHoc.Count > 0)
            {
                maLopHoc = Library.CheckKeyInHashtable(listData_LopHoc[0], "MaLopHoc").ToString();
                tenLopHoc = Library.CheckKeyInHashtable(listData_LopHoc[0], "TenLopHoc").ToString();
                tuNgay = (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(listData_LopHoc[0], "TuNgay").ToString())
                              ? Library.DateTimeConvert(Library.CheckKeyInHashtable(listData_LopHoc[0], "TuNgay")).ToString(
                                  "dd/MM/yyyy")
                              : "");
                denNgay = (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(listData_LopHoc[0], "DenNgay").ToString())
                              ? Library.DateTimeConvert(Library.CheckKeyInHashtable(listData_LopHoc[0], "DenNgay")).ToString(
                                  "dd/MM/yyyy")
                              : "");
                hanDangKy = (!string.IsNullOrEmpty(Library.CheckKeyInHashtable(listData_LopHoc[0], "HanDangKy").ToString())
                              ? Library.DateTimeConvert(Library.CheckKeyInHashtable(listData_LopHoc[0], "HanDangKy")).ToString(
                                  "dd/MM/yyyy")
                              : "");
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "$('#txtMaLopHoc').val('" + maLopHoc + "');" + Environment.NewLine;
        js += "$('#spanTenLopHoc').html('" + tenLopHoc + "');" + Environment.NewLine;
        js += "$('#hdLopHocID').val('" + _lopHocId + "');" + Environment.NewLine;
        js += "$('#spanTuNgay').html('" + tuNgay + "');" + Environment.NewLine;
        js += "$('#spanDenNgay').html('" + denNgay + "');" + Environment.NewLine;
        js += "$('#spanHanDangKy').html('" + hanDangKy + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(_lopHocId))
        {
            query = @"SELECT COUNT(LopHocHocVienID) SoDonDKChoDuyet FROM tblCNKTLopHocHocVien
                    INNER JOIN tblDMTrangThai on tblCNKTLopHocHocVien.TinhTrangID = tblDMTrangThai.TrangThaiID
                    WHERE LopHocID = " + _lopHocId + @" AND tblDMTrangThai.MaTrangThai = '" + ListName.Status_ChoDuyet + @"'";
            List<Hashtable> listDataSoDonDKChoDuyet = _db.GetListData(query);
            if (listDataSoDonDKChoDuyet.Count > 0)
            {
                js += "$('#linkOpenFormApprove').html('" + listDataSoDonDKChoDuyet[0]["SoDonDKChoDuyet"] + " đăng ký đang chờ xét duyệt');" + Environment.NewLine;
            }
        }
        js += "</script>";
        Page.RegisterStartupScript("LoadInforLopHoc", js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/02
    /// Load Danh sách đăng ký học từ DB
    /// </summary>
    private void LoadDangKyHoc(string lopHocId)
    {
        if (string.IsNullOrEmpty(lopHocId))
            return;
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        string query = "";
        int soHocVien = 0, NQT = 0, HVCN = 0, KTV = 0, soHocVienCaNhan = 0, soHocVienTapThe = 0;
        double tongSoTien = 0;
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "mhvcn", "FullName", "Email", "LoaiHoiVienCaNhan", "SoChungChiKTV", "NgayDangKy", "layGCN", "soGioCNKT", "TongPhi", "trStyle", "HeaderType", "HiddenRow", "LinkEdit" });
        query = @"SELECT tblCNKTDangKyHocCaNhan.DangKyHocCaNhanID as dkhcnid, tblHoiVienCaNhan.HoiVienCaNhanID HoiVienCaNhanID,
                                 tblHoiVienCaNhan.MaHoiVienCaNhan AS mhvcn, (tblHoiVienCaNhan.HoDem + ' ' + tblHoiVienCaNhan.Ten) AS FullName, tblHoiVienCaNhan.Email,
                                 tblHoiVienCaNhan.LoaiHoiVienCaNhan AS LoaiHoiVienCaNhan, tblHoiVienCaNhan.SoChungChiKTV AS SoChungChiKTV, tblHoiVienCaNhan.NgayCapChungChiKTV AS ngayCapChungChiKTV,
                                 tblCNKTDangKyHocCaNhan.NgayDangKy AS NgayDangKy, tblCNKTDangKyHocCaNhan.LayGiayChungNhan AS layGCN,
                                 tblCNKTDangKyHocCaNhan.TongSoGioCNKT AS soGioCNKT, tblCNKTDangKyHocCaNhan.TongPhi AS TongPhi, tblHoiVienTapThe.HoiVienTapTheID AS hoiVienTapTheID, tblHoiVienTapThe.MaHoiVienTapThe AS maHoiVienTapThe,
                                 tblHoiVienTapThe.TenDoanhNghiep AS TenDoanhNghiep, tblCNKTLopHoc.MaLopHoc AS MaLopHoc, tblCNKTDangKyHocCaNhan.HoiVienTapTheDangKy
                                FROM tblCNKTDangKyHocCaNhan 
                                LEFT JOIN tblHoiVienCaNhan ON tblCNKTDangKyHocCaNhan.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID
                                INNER JOIN tblHoiVienTapThe ON tblHoiVienCaNhan.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
                                LEFT JOIN tblHoiVienTapThe CHA ON tblHoiVienTapThe.HoiVienTapTheChaID = CHA.HoiVienTapTheID
                                LEFT JOIN tblCNKTLopHoc ON tblCNKTDangKyHocCaNhan.LopHocID = tblCNKTLopHoc.LopHocID
                                WHERE tblCNKTDangKyHocCaNhan.LopHocID = " + lopHocId;
        if (Request.QueryString["hvcn"] != "1")
            query += @" AND tblHoiVienCaNhan.LoaiHoiVienCaNhan != '1'";
        if (Request.QueryString["ktv"] != "1")
            query += @" AND tblHoiVienCaNhan.LoaiHoiVienCaNhan != '2'";
        if (Request.QueryString["nqt"] != "1")
            query += @" AND tblHoiVienCaNhan.LoaiHoiVienCaNhan != '0'";
        if (!string.IsNullOrEmpty(Request.QueryString["tk_id"]))
            query += @" AND LTRIM(RTRIM(tblHoiVienCaNhan.MaHoiVienCaNhan)) LIKE N'%" + Request.QueryString["tk_id"].Trim() + "%'";
        if (!string.IsNullOrEmpty(Request.QueryString["tk_name"]))
            query += @" AND LTRIM(RTRIM(tblHoiVienCaNhan.HoDem)) + ' ' + LTRIM(RTRIM(tblHoiVienCaNhan.Ten)) LIKE N'%" + Request.QueryString["tk_name"].Trim() + "%'";
        query += @" ORDER BY CASE ISNULL(tblHoiVienTapThe.HoiVienTapTheChaID,0) WHEN 0 THEN tblHoiVienTapThe.SoHieu ELSE CHA.SoHieu END ASC, tblHoiVienTapThe.HoiVienTapTheID, tblHoiVienCaNhan.SoChungChiKTV ASC, tblHoiVienCaNhan.Ten";
        List<Hashtable> list = _db.GetListData(query);
        AddDataToDataTable(list, ref soHocVien, ref NQT, ref HVCN, ref KTV, ref soHocVienCaNhan, ref soHocVienTapThe, ref tongSoTien, ref js, dt);

        query = @"SELECT tblCNKTDangKyHocCaNhan.DangKyHocCaNhanID as dkhcnid, tblHoiVienCaNhan.HoiVienCaNhanID HoiVienCaNhanID,
                                 tblHoiVienCaNhan.MaHoiVienCaNhan AS mhvcn, (tblHoiVienCaNhan.HoDem + ' ' + tblHoiVienCaNhan.Ten) AS FullName, tblHoiVienCaNhan.Email,
                                 tblHoiVienCaNhan.LoaiHoiVienCaNhan AS LoaiHoiVienCaNhan, tblHoiVienCaNhan.SoChungChiKTV AS SoChungChiKTV, tblHoiVienCaNhan.NgayCapChungChiKTV AS ngayCapChungChiKTV,
                                 tblCNKTDangKyHocCaNhan.NgayDangKy AS NgayDangKy, tblCNKTDangKyHocCaNhan.LayGiayChungNhan AS layGCN,
                                 tblCNKTDangKyHocCaNhan.TongSoGioCNKT AS soGioCNKT, tblCNKTDangKyHocCaNhan.TongPhi AS TongPhi, NULL AS hoiVienTapTheID, NULL AS maHoiVienTapThe,
                                 NULL AS TenDoanhNghiep, tblCNKTLopHoc.MaLopHoc AS MaLopHoc, tblCNKTDangKyHocCaNhan.HoiVienTapTheDangKy
                                FROM tblCNKTDangKyHocCaNhan 
                                LEFT JOIN tblHoiVienCaNhan ON tblCNKTDangKyHocCaNhan.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID
                                
                                LEFT JOIN tblCNKTLopHoc ON tblCNKTDangKyHocCaNhan.LopHocID = tblCNKTLopHoc.LopHocID
                                WHERE tblCNKTDangKyHocCaNhan.LopHocID = " + lopHocId;
        if (Request.QueryString["hvcn"] != "1")
            query += @" AND tblHoiVienCaNhan.LoaiHoiVienCaNhan != '1'";
        if (Request.QueryString["ktv"] != "1")
            query += @" AND tblHoiVienCaNhan.LoaiHoiVienCaNhan != '2'";
        if (Request.QueryString["nqt"] != "1")
            query += @" AND tblHoiVienCaNhan.LoaiHoiVienCaNhan != '0'";
        if (!string.IsNullOrEmpty(Request.QueryString["tk_id"]))
            query += @" AND LTRIM(RTRIM(tblHoiVienCaNhan.MaHoiVienCaNhan)) LIKE N'%" + Request.QueryString["tk_id"].Trim() + "%'";
        if (!string.IsNullOrEmpty(Request.QueryString["tk_name"]))
            query += @" AND LTRIM(RTRIM(tblHoiVienCaNhan.HoDem)) + ' ' + LTRIM(RTRIM(tblHoiVienCaNhan.Ten)) LIKE N'%" + Request.QueryString["tk_name"].Trim() + "%'";
        query += @" AND tblHoiVienCaNhan.HoiVienTapTheID IS NULL
                                ORDER BY tblHoiVienCaNhan.SoChungChiKTV ASC, tblHoiVienCaNhan.Ten";
        list = _db.GetListData(query);
        AddDataToDataTable(list, ref soHocVien, ref NQT, ref HVCN, ref KTV, ref soHocVienCaNhan, ref soHocVienTapThe, ref tongSoTien, ref js, dt);

        rpDanhSachDangKyHoc.DataSource = dt.DefaultView;
        rpDanhSachDangKyHoc.DataBind();

        js += "$('#spanSoHocVienDaDangKy').html('" + soHocVien + "<br>Trong đó: HVCN: " + HVCN + ", KTV: " + KTV + ", NQT: " + NQT + ")');" + Environment.NewLine;
        
        
        js += "$('#spanTongSoTien').html('" + Library.FormatMoney(tongSoTien) + "');" + Environment.NewLine;
        js += "$('#spanSoHocVienCaNhan').html('" + soHocVienCaNhan + "');" + Environment.NewLine;
        js += "$('#spanSoHocVienTapThe').html('" + soHocVienTapThe + "');" + Environment.NewLine;
        js += "</script>";
        Page.RegisterStartupScript("LoadInforLopHoc2", js);
    }

    private void LoadDangKyHocTheoChuyenDe(string lopHocId)
    {
        if (string.IsNullOrEmpty(lopHocId))
            return;

        DataTable dtb = new DataTable();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT COUNT(*) AS SoLuong, B.MaChuyenDe, B.TenChuyenDe FROM tblCNKTLopHocHocVien A INNER JOIN tblCNKTLopHocChuyenDe B ON A.ChuyenDeID = B.ChuyenDeID WHERE A.LopHocID = " + lopHocId + " GROUP BY B.MaChuyenDe, B.TenChuyenDe";
        dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];

        rpDangKyTheoChuyenDe.DataSource = dtb;
        rpDangKyTheoChuyenDe.DataBind();
    }

    private void AddDataToDataTable(List<Hashtable> list, ref int soHocVien, ref int NQT, ref int HVCN, ref int KTV, ref int soHocVienCaNhan, ref int soHocVienTapThe, ref double tongSoTien, ref string js, DataTable dt)
    {
        if (list.Count > 0)
        {
            DataRow dr;

            string temp_HoiVienTapThe = ""; // Biến tạm để xác định những đơn đăng ký cùng công ty
            string space = ""; // Thêm khoảng trống để các ô dữ liệu đăng ký dịch lùi vào
            bool flag_HeaderCongTy = false, flag_HeaderCaNhan = false;
            double temp_TongPhiTheoCongTy = 0;

            foreach (Hashtable ht in list)
            {
                string hoiVienTapTheID = Library.CheckKeyInHashtable(ht, "hoiVienTapTheID").ToString(); // Chỉ những bản ghi đăng ký theo công ty mới có giá trị trường này

                // Đếm số học viên đăng ký theo cá nhân và tổ chức
                if (string.IsNullOrEmpty(hoiVienTapTheID))
                    soHocVienCaNhan++;
                else
                {                  
                    soHocVienTapThe++;
                }

                if(hoiVienTapTheID == temp_HoiVienTapThe || temp_TongPhiTheoCongTy == 0)
                    temp_TongPhiTheoCongTy += Library.DoubleConvert(Library.CheckKeyInHashtable(ht, "TongPhi"));

                if (!string.IsNullOrEmpty(temp_HoiVienTapThe) && (soHocVien == list.Count - 1 || hoiVienTapTheID != temp_HoiVienTapThe))
                {
                    js += "$('#spanTongPhiTheoCongTy_" + temp_HoiVienTapThe + "').html('" + Library.FormatMoney(temp_TongPhiTheoCongTy) + "');" + Environment.NewLine;
                    temp_TongPhiTheoCongTy = Library.DoubleConvert(Library.CheckKeyInHashtable(ht, "TongPhi"));
                }
                // Đoạn code xác định nhưng đơn đăng ký theo công ty và hiển thị 1 dòng tên công ty ở đầu tiên
                if (!string.IsNullOrEmpty(hoiVienTapTheID) && hoiVienTapTheID != temp_HoiVienTapThe)
                {
                    // Thêm đoạn header những đơn đăng ký theo công ty hoặc tổ chức
                    if (!flag_HeaderCongTy)
                    {
                        dr = dt.NewRow();
                        dr["HeaderType"] = "<tr style='background-color: lavender; font-weight: bold;'><td colspan='11'>ĐĂNG KÝ THEO CÔNG TY HOẶC HỘI VIÊN TẬP THỂ (<span name=\"spanSoHocVienTapThe\" id=\"spanSoHocVienTapThe\"></span> học viên)</td></tr>";
                        dr["HiddenRow"] = "style ='display:none;'";
                        dt.Rows.Add(dr);
                        flag_HeaderCongTy = true;
                    }

                    // Đoạn code add các dòng hội viên tổ chức
                    space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    dr = dt.NewRow();
                    dr["mhvcn"] = "<a href='/admin.aspx?page=hosohoivientapthe_edit&prepage=cnkt_quanlydangkyhoc&id=" + hoiVienTapTheID + "&act=view' target='_blank'>" + Library.CheckKeyInHashtable(ht, "maHoiVienTapThe") + "</a>";
                    dr["FullName"] = Library.CheckKeyInHashtable(ht, "TenDoanhNghiep");
                    
                    dr["LoaiHoiVienCaNhan"] = "Công ty/Hội viên tổ chức";
                    dr["TongPhi"] = "<span name=\"spanTongPhiTheoCongTy_" + hoiVienTapTheID + "\" id=\"spanTongPhiTheoCongTy_" + hoiVienTapTheID + "\"></span>";
                    dr["trStyle"] = "style='font-weight: bold;'";
                    string hrefLink = "/admin.aspx?page=CNKT_DangKyHocCongTy_Add&mact=" + Library.CheckKeyInHashtable(ht, "maHoiVienTapThe").ToString().Trim() + "&malh=" + Library.CheckKeyInHashtable(ht, "MaLopHoc");
                    dr["LinkEdit"] = "<a href='" + hrefLink + "' data-placement='top' data-rel='tooltip' class='iconsweets-create' data-original-title='Xem/Sửa' rel='tooltip' class='btn'></a>";
                    dt.Rows.Add(dr);
                    temp_HoiVienTapThe = hoiVienTapTheID;
                }

                if (string.IsNullOrEmpty(hoiVienTapTheID) && !flag_HeaderCaNhan)
                {
                    // Thêm đoạn header những đơn đăng ký theo cá nhân
                    dr = dt.NewRow();
                    dr["HeaderType"] = "<tr style='background-color: lavender; font-weight: bold;'><td colspan='11'>ĐĂNG KÝ THEO CÁ NHÂN (<span name=\"spanSoHocVienCaNhan\" id=\"spanSoHocVienCaNhan\"></span> học viên)</td></tr>";
                    dr["HiddenRow"] = "style ='display:none;'";
                    dt.Rows.Add(dr);
                    flag_HeaderCaNhan = true;
                }

                // Add các đơn đăng ký vào danh sách bình thường
                soHocVien++;
                if (Library.CheckKeyInHashtable(ht, "LoaiHoiVienCaNhan").ToString() == "0")
                    NQT++;
                else if (Library.CheckKeyInHashtable(ht, "LoaiHoiVienCaNhan").ToString() == "1")
                    HVCN++;
                else
                    KTV++;
                dr = dt.NewRow();
                dr["STT"] = soHocVien;
                dr["mhvcn"] = "<a href='/admin.aspx?page=hosohoiviencanhan&id=" + Library.CheckKeyInHashtable(ht, "HoiVienCaNhanID") + "&act=view' target='_blank'>" + Library.CheckKeyInHashtable(ht, "mhvcn") + "</a>";
                dr["FullName"] = space + Library.CheckKeyInHashtable(ht, "FullName");
                dr["Email"] = Library.CheckKeyInHashtable(ht, "Email");
                dr["LoaiHoiVienCaNhan"] = Library.GetTenLoaiHoiVienCaNhan(Library.CheckKeyInHashtable(ht, "LoaiHoiVienCaNhan").ToString());
                dr["SoChungChiKTV"] = Library.CheckKeyInHashtable(ht, "SoChungChiKTV");
                dr["NgayDangKy"] = Library.FormatDateTime(Library.DateTimeConvert(Library.CheckKeyInHashtable(ht, "NgayDangKy")), "dd/MM/yyyy");
                dr["layGCN"] = Library.CheckKeyInHashtable(ht, "layGCN").ToString() == "1" ? "x" : "";
                dr["soGioCNKT"] = Library.CheckKeyInHashtable(ht, "soGioCNKT");
                dr["TongPhi"] = Library.FormatMoney(Library.CheckKeyInHashtable(ht, "TongPhi"));
                if (string.IsNullOrEmpty(hoiVienTapTheID) || Library.CheckKeyInHashtable(ht, "HoiVienTapTheDangKy").ToString() == "")
                {
                    string hrefLink = "/admin.aspx?page=CNKT_DangKyHocCaNhan_Add&mahv=" +
                                      Library.CheckKeyInHashtable(ht, "mhvcn") + "&malh=" +
                                      Library.CheckKeyInHashtable(ht, "MaLopHoc");
                    dr["LinkEdit"] = "<a href='" + hrefLink +
                                     "' data-placement='top' data-rel='tooltip' class='iconsweets-create' data-original-title='Xem/Sửa' rel='tooltip' class='btn'></a>";
                }
                tongSoTien += Library.DoubleConvert(Library.CheckKeyInHashtable(ht, "TongPhi"));
                dt.Rows.Add(dr);
            }
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/19
    /// Process and Create Sql command text
    /// </summary>
    /// <param name="con">SqlConnection already open</param>
    /// <returns>SqlCommand with Sql command text</returns>
    private SqlCommand GetSqlCommandLoadListClass(SqlConnection con)
    {
        string month = Request.Form["ddlThang"];
        string year = Request.Form["ddlNam"];
        string province = Request.Form["ddlProvince"];
        string classID = Request.Form["ddlClassID"];
        string txtFromDate = Request.Form["textFromDate"];
        string txtToDate = Request.Form["textToDate"];
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = con;
        string command = @"SELECT tblCNKTLopHoc.LopHocID AS LopHocID , tblCNKTLopHoc.MaLopHoc , tblCNKTLopHoc.TuNgay, tblCNKTLopHoc.DenNgay,
                                        tblCNKTLopHocChuyenDe.SoBuoi AS SoBuoi, tblDMGiangVien.TenGiangVien AS TenGiangVien
                                        FROM tblCNKTLopHoc
                                         LEFT JOIN tblCNKTLopHocChuyenDe ON tblCNKTLopHoc.LopHocID = tblCNKTLopHocChuyenDe.LopHocID
                                         LEFT JOIN tblDMGiangVien ON tblCNKTLopHocChuyenDe.GiangVienID = tblDMGiangVien.GiangVienID
                                         WHERE (
                                          (tblCNKTLopHoc.TuNgay >= @FromDate AND tblCNKTLopHoc.TuNgay <= @ToDate)
                                          OR
                                          (tblCNKTLopHoc.DenNgay >= @FromDate AND tblCNKTLopHoc.DenNgay <= @ToDate)) AND {ClassID} AND {Province} AND {Datetime1} AND {Datetime2}";

        DateTime fromDate = new DateTime(DateTime.Now.Year, 1, 1);
        DateTime toDate = new DateTime(DateTime.Now.Year, 12, 31);
        if (!string.IsNullOrEmpty(year))
        {
            fromDate = new DateTime(Library.Int32Convert(year), 1, 1);
            toDate = new DateTime(Library.Int32Convert(year), 12, 31);
        }
        if (!string.IsNullOrEmpty(month))
        {
            fromDate = new DateTime(Library.Int32Convert(year), Library.Int32Convert(month), 1);
            toDate = new DateTime(Library.Int32Convert(year), Library.Int32Convert(month), DateTime.DaysInMonth(Library.Int32Convert(year), Library.Int32Convert(month)));
        }

        if (!string.IsNullOrEmpty(classID))
            command = command.Replace("{ClassID}", "tblCNKTLopHoc.MaLopHoc = " + classID);
        else
            command = command.Replace("{ClassID}", "1=1");

        if (!string.IsNullOrEmpty(province))
            command = command.Replace("{Province}", "tblCNKTLopHoc.TinhThanhID = " + province);
        else
            command = command.Replace("{Province}", "1=1");

        if (!string.IsNullOrEmpty(txtFromDate))
        {
            txtFromDate = Library.ConvertDatetime(txtFromDate, '/');
            command = command.Replace("{Datetime1}", "(tblCNKTLopHoc.TuNgay >= '" + txtFromDate + "' OR tblCNKTLopHoc.DenNgay >= '" + txtFromDate + "')");
        }
        else
            command = command.Replace("{Datetime1}", "1=1");
        if (!string.IsNullOrEmpty(txtToDate))
        {
            txtToDate = Library.ConvertDatetime(txtToDate, '/');
            command = command.Replace("{Datetime2}", "(tblCNKTLopHoc.TuNgay <= '" + txtToDate + "' OR tblCNKTLopHoc.DenNgay <= '" + txtToDate + "')");
        }
        else
            command = command.Replace("{Datetime2}", "1=1");

        sqlCommand.CommandText = command;
        sqlCommand.Parameters.Add("FromDate", SqlDbType.Date).Value = fromDate;
        sqlCommand.Parameters.Add("ToDate", SqlDbType.Date).Value = toDate;

        return sqlCommand;
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("XOA|"))
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");

        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("THEM|") && !_listPermissionOnFunc_QuanLyDangKyHoc.Contains("SUA|"))
        {
            Response.Write("$('#Btn_ThemCaNhan').remove();");
            Response.Write("$('#Btn_ThemTapThe').remove();");
            Response.Write("$('a[data-original-title=\"Xem/Sửa\"]').remove();");
        }

        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("XOA|"))
            Response.Write("$('#btn_xoa').remove();");

        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("KETXUAT|"))
            Response.Write("$('#btn_ketxuat').remove();");

        if (!_listPermissionOnFunc_QuanLyDangKyHoc.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
    }

    #region Gen control in "Search Form"

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get List of year from 2000 - 2030 to fill into dropdownlist search by Year.
    /// </summary>
    protected void LoadListYear()
    {
        string output_html = "";
        for (int i = 1940; i <= 2040; i++)
        {
            output_html += i == DateTime.Now.Year
                               ? "<option selected='selected' value='" + i + "'>" + i + "</option>"
                               : "<option value='" + i + "'>" + i + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get list of month to fill into dropdownlist search by Month
    /// </summary>
    protected void LoadListMonth()
    {
        string output_html = "";
        output_html += "<option value=''>Tất cả</option>";
        for (int i = 1; i <= 12; i++)
        {
            output_html += "<option value='" + i + "'>" + i + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get list of Class ID already exists in Database
    /// </summary>
    protected void LoadListClassID()
    {
        string output_html = "";
        output_html += "<option value=''>Tất cả</option>";
        SqlCnktLopHocProvider cnktLopHocPro = new SqlCnktLopHocProvider(ListName.ConnectionString, false, "");
        TList<CnktLopHoc> list = cnktLopHocPro.GetAll();
        foreach (CnktLopHoc item in list)
        {
            output_html += "<option value='" + item.MaLopHoc + "'>" + item.MaLopHoc + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }

    /// <summary>
    /// Create by NGUYEN MANH HUNG - 2014/11/18
    /// Get list of Province 
    /// </summary>
    protected void LoadProvince()
    {
        string output_html = "";
        output_html += "<option value=''>Tất cả</option>";
        SqlDmTinhProvider dmTinhPro = new SqlDmTinhProvider(ListName.ConnectionString, false, "");
        TList<DmTinh> list = dmTinhPro.GetAll();
        foreach (DmTinh item in list)
        {
            output_html += "<option value='" + item.TinhId + "'>" + item.TenTinh + "</option>";
        }
        HttpContext.Current.Response.Write(output_html);
    }

    #endregion
}