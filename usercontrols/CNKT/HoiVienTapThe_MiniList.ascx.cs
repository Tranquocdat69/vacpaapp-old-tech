﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;


public partial class usercontrols_HoiVienTapThe_MiniList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadListCongTy();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Load danh sách "Công ty" theo điều kiện Search
    /// </summary>
    private void LoadListCongTy()
    {
        Db db = new Db(ListName.ConnectionString);
        try
        {
            db.OpenConnection();
            string query = @"SELECT A.HoiVienTapTheID, A.MaHoiVienTapThe, A.TenDoanhNghiep, A.TenVietTat, A.TenTiengAnh, A.DiaChi, A.MaSoThue, 
                                A.SoGiayChungNhanDKKD, A.NgayGiaNhap, A.NgayCapGiayChungNhanDKKD, A.NguoiDaiDienLL_Ten, A.NguoiDaiDienLL_DiDong, A.NguoiDaiDienLL_Email
	                            FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID
	                            WHERE {MaHoiVienTapThe} AND (A.TenDoanhNghiep LIKE N'%" + txtTenCongTy.Text + @"%' OR A.TenTiengAnh LIKE N'%" + txtTenCongTy.Text + @"%')
	                            AND {LoaiHoiVienTapThe} ORDER BY CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END, A.HoiVienTapTheID, A.TenDoanhNghiep";

            query = !string.IsNullOrEmpty(txtMaCongTy.Text)
                        ? query.Replace("{MaHoiVienTapThe}", "A.MaHoiVienTapThe like '%" + txtMaCongTy.Text + "%'")
                        : query.Replace("{MaHoiVienTapThe}", "1=1");
            query = ddlPhanLoai.SelectedValue != ""
                        ? query.Replace("{LoaiHoiVienTapThe}", "A.LoaiHoiVienTapThe = '" + ddlPhanLoai.SelectedValue + "'")
                        : query.Replace("{LoaiHoiVienTapThe}", "1=1");
            DataTable dt = db.GetDataTable(query);
            rpCongTy.DataSource = dt.DefaultView;
            rpCongTy.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadListCongTy();
    }
}