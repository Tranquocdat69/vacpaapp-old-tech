﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.Drawing;

public partial class usercontrols_CNKT_BaoCao_DanhSachKTVThamDuCNKT : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo danh sách Kiểm toán viên tham dự lớp Cập nhật kiến thức";

    private Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        DataBaseDb db = new DataBaseDb();
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        // _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            //_db.OpenConnection();
            string strIDMaLopHoc = string.Empty;
            string strMaLopHoc = string.Empty;
            string strTenLopHoc = string.Empty;
            string strThoiGianTu = "";
            string strThoiGianDen = "";
            if (!string.IsNullOrEmpty(Request.Form["hdLopHocID"]))
            {
                strIDMaLopHoc = Request.Form["hdLopHocID"];
            }
            if (!string.IsNullOrEmpty(Request.Form["txtMaLopHoc"]))
            {
                strMaLopHoc = Request.Form["txtMaLopHoc"];
            }
            if (!string.IsNullOrEmpty(Request.Form["txtTenLopHoc"]))
            {
                strTenLopHoc = Request.Form["txtTenLopHoc"];
            }

            DateTime dtFrom = new DateTime(DateTime.Now.Year, 1, 1);
            DateTime dtTo = new DateTime(DateTime.Now.Year, 12, 31);
            if (!string.IsNullOrEmpty(Request.Form["hdLopHocID"]))
            {
                if (!string.IsNullOrEmpty(Request.Form["txtNgayDaoTaoTu"]))
                {
                    strThoiGianTu = Request.Form["txtNgayDaoTaoTu"];
                    dtFrom = Library.DateTimeConvert(strThoiGianTu, "dd/MM/yyyy");

                }
                if (!string.IsNullOrEmpty(Request.Form["txtNgayDaoTaoDen"]))
                {
                    strThoiGianDen = Request.Form["txtNgayDaoTaoDen"];
                    dtTo = Library.DateTimeConvert(strThoiGianDen, "dd/MM/yyyy");
                }

                string[] paraName = { "@idlophoc", "@datefrom", "@dateto", "@loaihoivien" };
                object[] paraValue = { strIDMaLopHoc, dtFrom, dtTo, 2 };

                DataSet ds = new DataSet();
                DataSet ds2 = new DataSet();

                string sqlTongHV = "select count(*) from"
+ " (select  hvtt.TenDoanhNghiep,hvcn.HoDem+' '+hvcn.Ten as HoVaTen,(case when hvcn.LoaiHoiVienCaNhan =1 then 'X' end) as HoiVienVacpa,hvcn.SoChungChiKTV,hvcn.NgayCapChungChiKTV, '' as TongSoGioCNKT"

+ " from tblHoiVienCaNhan hvcn"
+ " join tblHoiVienTapThe hvtt"
+ " on hvcn.HoiVienTapTheID=hvtt.HoiVienTapTheID"
+ " left join tblCNKTDangKyHocCaNhan dkhcn"
+ " on dkhcn.HoiVienCaNhanID=hvcn.HoiVienCaNhanID"

+ " join tblCNKTLopHoc lh"
+ " on lh.LopHocID=dkhcn.LopHocID"
+ " join tblCNKTLopHocChuyenDe lhcd"
+ " on lh.LopHocID=lhcd.LopHocID"
+ " join tblCNKTLopHocBuoi lhb"
+ " on lhb.LopHocID=lh.LopHocID "
+ " join tblDMGiangVien gv"
+ " on  gv.GiangVienID=lhcd.GiangVienID"
+ " where dkhcn.LopHocID=" + strIDMaLopHoc + " and (hvcn.LoaiHoiVienCaNhan=2 or hvcn.LoaiHoiVienCaNhan=1 or hvcn.LoaiHoiVienCaNhan=0) "
 + " group by hvcn.HoiVienTapTheID,hvtt.HoiVienTapTheID, hvtt.TenDoanhNghiep,hvcn.HoDem,hvcn.Ten,hvcn.SoChungChiKTV,hvcn.NgayCapChungChiKTV, hvcn.LoaiHoiVienCaNhan"
+ "  )as myTable ";

                string sqlTongHVX = "select count(*) from"
+ " (select  hvtt.TenDoanhNghiep,hvcn.HoDem+' '+hvcn.Ten as HoVaTen,(case when hvcn.LoaiHoiVienCaNhan =1 then 'X' end) as HoiVienVacpa,hvcn.SoChungChiKTV,hvcn.NgayCapChungChiKTV, '' as TongSoGioCNKT"

+ " from tblHoiVienCaNhan hvcn"
+ " join tblHoiVienTapThe hvtt"
+ " on hvcn.HoiVienTapTheID=hvtt.HoiVienTapTheID"
+ " left join tblCNKTDangKyHocCaNhan dkhcn"
+ " on dkhcn.HoiVienCaNhanID=hvcn.HoiVienCaNhanID"

+ " join tblCNKTLopHoc lh"
+ " on lh.LopHocID=dkhcn.LopHocID"
+ " join tblCNKTLopHocChuyenDe lhcd"
+ " on lh.LopHocID=lhcd.LopHocID"
+ " join tblCNKTLopHocBuoi lhb"
+ " on lhb.LopHocID=lh.LopHocID "
+ " join tblDMGiangVien gv"
+ " on  gv.GiangVienID=lhcd.GiangVienID"
+ " where dkhcn.LopHocID=" + strIDMaLopHoc + " and (hvcn.LoaiHoiVienCaNhan=2 or hvcn.LoaiHoiVienCaNhan=1 or hvcn.LoaiHoiVienCaNhan=0) "
+ " group by hvcn.HoiVienTapTheID,hvtt.HoiVienTapTheID, hvtt.TenDoanhNghiep,hvcn.HoDem,hvcn.Ten,hvcn.SoChungChiKTV,hvcn.NgayCapChungChiKTV, hvcn.LoaiHoiVienCaNhan"
+ "  )as myTable where HoiVienVacpa='X'";

                string sqlTongGV = "select count(*) from"
+ " (select distinct gv.TenGiangVien,(case when hvcn.LoaiHoiVienCaNhan =1 then 'X' end) as HoiVienVacpa,hvcn.SoChungChiKTV,hvcn.NgayCapChungChiKTV, '' as TongSoGioCNKT "
+ " from  tblCNKTLopHoc lh"

+ " join tblCNKTLopHocBuoi lhb"
+ " on lhb.LopHocID=lh.LopHocID "
 + " join tblCNKTLopHocChuyenDe lhcd"
+ "  on lh.LopHocID=lhcd.LopHocID"
+ "    join tblDMGiangVien gv"
+ " on gv.GiangVienID=lhcd.GiangVienID "
+ " left join tblCNKTDangKyHocCaNhan dkhcn "
+ " on lh.LopHocID=dkhcn.LopHocID"
+ " left join tblHoiVienCaNhan hvcn"
+ " on dkhcn.HoiVienCaNhanID=hvcn.HoiVienCaNhanID and gv.HoiVienCaNhanID=hvcn.HoiVienCaNhanID"
+ " where dkhcn.LopHocID=" + strIDMaLopHoc + " )as myTable";

                object objTongHV = db.GetValue(sqlTongHV);
                object objTongHVX = db.GetValue(sqlTongHVX);
                object objTongGV = db.GetValue(sqlTongGV);
                object objTongHVNotX = (int)objTongHV - (int)objTongHVX;
                // DataTable dt1 = new DataTable();
                DataTable _table = db.GetData("select distinct lhb.Ngay,cd.MaChuyenDe,lhb.Sang,lhb.Chieu from  tblCNKTLopHoc lh join tblCNKTLopHocBuoi lhb on lhb.LopHocID=lh.LopHocID join tblCNKTLopHocChuyenDe cd on cd.ChuyenDeID=lhb.ChuyenDeID where lh.LopHocID=" + strIDMaLopHoc);
                // dt1.TableName = "NopHoiPhiCaNhan";
                // dt1 = db.GetDataTable(paraValue, paraName, "proc_BAOCAO_QLDT_KTVCAPNHATKIENTHUC_DINAMICCOLUMN", "KTVCNKT3");
                ds = db.GetData(paraValue, paraName, "proc_BAOCAO_QLDT_KTVCAPNHATKIENTHUC_KTV", "KTVCNKT");
                ds2 = db.GetData(paraValue, paraName, "proc_BAOCAO_QLDT_KTVCAPNHATKIENTHUC_CHUYENDE", "KTVCNKTChuyenDe");
                //ds.Tables.Add(dt1);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Server.MapPath("Report/CNKT/rptBaoCaoDanhSachKTVThamDuCNKT.rpt"));
                List<object> list = db.GetValueExecuteReader("select TuNgay,DenNgay from tblCNKTLopHoc where LopHocID=" + strIDMaLopHoc);
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtHaNoi"]).Text = "Hà Nội, Ngày " + ((DateTime)list[0]).ToString("dd/MM/yyyy") + " - " + ((DateTime)list[1]).ToString("dd/MM/yyyy");
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtLop"]).Text = "CẬP NHẬT KIẾN THỨC LỚP SỐ  " + strTenLopHoc + " NĂM " + (dtTo.Year + 1).ToString();
                rpt.SetDataSource(ds.Tables["KTVCNKT"]);
                // rpt.Subreports[0].DataSourceConnections.Clear();
                rpt.Subreports[0].SetDataSource(ds2.Tables["KTVCNKTChuyenDe"]);
                int maxRight = 0;
                int maxRightb = 0;
                int leftTextBox1 = 7320;
                int leftTextBox2 = 9960;
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    int width = 2260;
                    // int widthb = 2280;
                    if (_table.Rows[i]["Sang"].ToString().Trim() != "")
                    {
                        maxRight = leftTextBox1 + (width * 2) + (480 * 2);
                    }
                    if (_table.Rows[i]["Chieu"].ToString().Trim() != "")
                    {
                        maxRightb = leftTextBox2 + (width * 2) + (480 * 2);
                    }

                    if (i <= 0)
                    {
                        if (_table.Rows[i]["Sang"].ToString().Trim() != "")
                        {
                            CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtSang" + i, "Sáng ngày " + ((DateTime)_table.Rows[i][0]).ToString("dd/MM/yyyy"), leftTextBox1, 520, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                            CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtCDSang" + i, "" + _table.Rows[i][1], leftTextBox1, 880, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        }
                        if (_table.Rows[i]["Chieu"].ToString().Trim() != "")
                        {
                            CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtChieu" + i, "Chiều ngày " + ((DateTime)_table.Rows[i][0]).ToString("dd/MM/yyyy"), leftTextBox2, 520, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                            CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtCDChieu" + i, "" + _table.Rows[i][1], leftTextBox2, 880, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        }


                    }
                    else
                    {
                        if (_table.Rows[i]["Sang"].ToString().Trim() != "")
                        {
                            CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtSang" + i, "Sáng ngày " + ((DateTime)_table.Rows[i][0]).ToString("dd/MM/yyyy"), leftTextBox1, 520, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                            CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtCDSang" + i, "" + _table.Rows[i][1], leftTextBox1, 880, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        }
                        if (_table.Rows[i]["Chieu"].ToString().Trim() != "")
                        {
                            CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtChieu" + i, "Chiều ngày " + ((DateTime)_table.Rows[i][0]).ToString("dd/MM/yyyy"), leftTextBox2, 520, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);


                            CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "txtCDChieu" + i, "" + _table.Rows[i][1], leftTextBox2, 880, width, null, null, 11, FontStyle.Regular, true, false, CrystalReportControl.HorizontalAlign_Center);
                        }
                    }
                    if (i <= _table.Rows.Count - 1)
                    {
                        if (i == 0)
                        {
                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineSangF" + i, leftTextBox1 + width + 240, leftTextBox1 + width + 240, 480, 1200, true);
                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailF" + i, leftTextBox1 + width + 240, leftTextBox1 + width + 240, 0, 599, true);

                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterFSang" + i, leftTextBox1 + width + 240, leftTextBox1 + width + 240, 0, 1800, false);

                            CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterFSang" + i, leftTextBox1 + width + 240, leftTextBox1 + width + 240, 0, 599, true);

                        }
                        if (_table.Rows.Count > 1 && i < _table.Rows.Count - 1)
                        {
                            if (_table.Rows[i]["Sang"].ToString().Trim() != "")
                            {
                                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineSang" + i, maxRight, maxRight, 480, 1200, true);
                                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailSang" + i, maxRight, maxRight, 0, 599, true);
                                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterSang" + i, maxRight, maxRight, 0, 1800, false);
                                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterSang" + i, maxRight, maxRight, 0, 599, true);

                            }
                            if (_table.Rows[i]["Chieu"].ToString().Trim() != "")
                            {
                                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineChieu" + i, maxRightb, maxRightb, 480, 1200, true);
                                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetailChieu" + i, maxRightb, maxRightb, 0, 599, true);

                                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineReportFooterChieu" + i, maxRightb, maxRightb, 0, 1800, false);
                                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooterChieu" + i, maxRightb, maxRightb, 0, 599, true);
                            }
                        }
                        //CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineChieu" + i, (int)Math.Pow(maxRight, 2), (int)Math.Pow(maxRight, 2), 480, 1200, true);
                    }
                    if (_table.Rows[i]["Sang"].ToString().Trim() != "")
                    {
                        leftTextBox1 += (width * 2) + (480 * 2) + 200;
                    }
                    if (_table.Rows[i]["Chieu"].ToString().Trim() != "")
                    {
                        leftTextBox2 += (width * 2) + (480 * 2) + 200;
                    }




                }

                if (_table.Rows.Count > 0)
                {
                    TextObject txtTongSoGio = rpt.ReportDefinition.ReportObjects["txtTongSoGio"] as TextObject;

                    txtTongSoGio.Left = maxRight;
                    int with = txtTongSoGio.Width;


                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineRight1", maxRight, maxRight, 120, 1199, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineRight2", maxRight + with, maxRight + with, 120, 1199, true);
                    //txtTongSoGio.Text = "Tổng số giờ CNKT";
                    TextObject txtChuKy = rpt.ReportDefinition.ReportObjects["txtChuKy"] as TextObject;
                    //txtChuKy.Width = maxRight;

                    LineObject lineBottomChuKy = rpt.ReportDefinition.ReportObjects["lineBottomChuKy"] as LineObject;
                    lineBottomChuKy.Right = maxRight;

                    LineObject lineTitleNgay = rpt.ReportDefinition.ReportObjects["lineTitleNgay"] as LineObject;
                    lineTitleNgay.Right = maxRight;


                    LineObject linePageHeader = rpt.ReportDefinition.ReportObjects["linePageHeader"] as LineObject;
                    linePageHeader.Right = maxRight + with;

                    LineObject lineBottomHeader = rpt.ReportDefinition.ReportObjects["lineBottomHeader"] as LineObject;
                    lineBottomHeader.Right = maxRight + with;

                    LineObject lineGroupBottom = rpt.ReportDefinition.ReportObjects["lineGroupBottom"] as LineObject;
                    lineGroupBottom.Right = maxRight + with;

                    LineObject lineBottomDetail = rpt.ReportDefinition.ReportObjects["lineBottomDetail"] as LineObject;
                    lineBottomDetail.Right = maxRight + with;

                    LineObject lineBottomFooter1 = rpt.ReportDefinition.ReportObjects["lineBottomFooter1"] as LineObject;
                    lineBottomFooter1.Right = maxRight + with;
                    LineObject lineBottomFooter2 = rpt.ReportDefinition.ReportObjects["lineBottomFooter2"] as LineObject;
                    lineBottomFooter2.Right = maxRight + with;
                    LineObject lineBottomFooter3 = rpt.ReportDefinition.ReportObjects["lineBottomFooter3"] as LineObject;
                    lineBottomFooter3.Right = maxRight + with;

                    LineObject linePageFooter = rpt.ReportDefinition.ReportObjects["linePageFooter"] as LineObject;
                    linePageFooter.Right = maxRight + with;


                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_GroupHeader, 0, "lineGroupRight1", maxRight + with, maxRight + with, 0, 600, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineFooterRight1", maxRight, maxRight, 0, 1800, false);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineFooterRight2", maxRight + with, maxRight + with, 0, 1800, false);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetail2", maxRight, maxRight, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetail1", maxRight + with, maxRight + with, 0, 600, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooter2", maxRight, maxRight, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooter1", maxRight + with, maxRight + with, 0, 600, true);

                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongKet"]).Width = maxRight;
                    int max = maxRight - ((TextObject)rpt.ReportDefinition.ReportObjects["txtThoiGian"]).Width;
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtThoiGian"]).Left = max;
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLapBaoCao"]).Left = max;
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLap"]).Left = max;
                    if (maxRight > 13680)
                        ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiSoatXet"]).Left = (maxRight / 2) - ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiSoatXet"]).Width;
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongKet"]).Text = "Tổng số học viên tham dự là " + objTongHV.ToString() + " người, gồm " + objTongHVX.ToString() + " người là Hội viên,"
+ objTongHVNotX.ToString() + "  người không phải là Hội viên; có " + objTongGV.ToString() + " giảng viên là Kiểm toán viên Đăng ký hành nghề.";
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtThoiGian"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLapBaoCao"]).Text = cm.Admin_HoVaTen;
                }
                else
                {
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineRight1", 12480, 12480, 120, 1199, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageHeader, 0, "lineRight2", 13680, 13680, 120, 1199, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_GroupHeader, 0, "lineGroupRight1", 13680, 13680, 0, 600, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineFooterRight1", 12480, 12480, 0, 1800, false);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportFooter, 0, "lineFooterRight2", 13680, 13680, 0, 1800, false);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetail2", 12480, 12480, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lineDetail1", 13680, 13680, 0, 600, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooter2", 12480, 12480, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_PageFooter, 0, "linePageFooter1", 13680, 13680, 0, 600, true);
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongKet"]).Text = "Tổng số học viên tham dự là " + objTongHV.ToString() + " người, gồm " + objTongHVX.ToString() + " người là Hội viên,"
+ objTongHVNotX.ToString() + "  người không phải là Hội viên; có " + objTongGV.ToString() + " giảng viên là Kiểm toán viên Đăng ký hành nghề.";
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtThoiGian"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLapBaoCao"]).Text = cm.Admin_HoVaTen;
                }


                CrystalReportViewer1.ReportSource = rpt;
                //CrystalReportViewer1.DataBind();
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachKTVThamDuCNKT");
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(ds.Tables[0], ds2.Tables[0], _table, ((DateTime)list[1]).Year.ToString(), strMaLopHoc, (DateTime)list[0], (DateTime)list[1], objTongHV, objTongHVX, objTongHVNotX, objTongGV);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "DanhSachKTVThamDuCNKT");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachKTVThamDuCNKT");
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            //  _db.CloseConnection();
        }
        finally
        {
            // _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string ngayDaoTaoTu = Request.Form["txtNgayDaoTaoTu"];
        string ngayDaoTaoDen = Request.Form["txtNgayDaoTaoDen"];
        string maLopHoc = Request.Form["txtMaLopHoc"];
        string js = "";
        if (!string.IsNullOrEmpty(ngayDaoTaoTu))
            js += "$('#txtNgayDaoTaoTu').val('" + ngayDaoTaoTu + "');" + Environment.NewLine;
        else
        {
            js += "$('#txtNgayDaoTaoTu').val('01/01/"+DateTime.Now.Year+"');" + Environment.NewLine;
        }
        if (!string.IsNullOrEmpty(ngayDaoTaoDen))
            js += "$('#txtNgayDaoTaoDen').val('" + ngayDaoTaoDen + "');" + Environment.NewLine;
        else
        {
            js += "$('#txtNgayDaoTaoDen').val('31/12/" + DateTime.Now.Year + "');" + Environment.NewLine;
        }
        if (!string.IsNullOrEmpty(maLopHoc))
        {
            js += "$('#txtMaLopHoc').val('" + maLopHoc + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, DataTable dtChuyenDe, DataTable dynamicColumn, string nam, string maLopHoc, DateTime fromDate, DateTime toDate, object objTongHV, object objTongHVX, object objTongHVNotX, object objTongGV)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:120px;
            vertical-align: middle;
        }

        .HeaderColumn1 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTitle {      
            font-weight: bold;      
            vertical-align: middle;            
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
            height: 40px;
            vertical-align: middle; 
        }

        .GroupTitle2 {
            font-size: 10pt;
            font-weight: bold;
            height: 30px;
            vertical-align: middle; 
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }

        .ColumnChuyenDe {            
            font-size: 10pt;            
        }
        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='12' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='https://" + Request.Url.Authority + @"/images/HeaderExport.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='12' style='text-align:center;font-size: 14pt; font-weight: bold; height: 50px;'>DANH SÁCH KIỂM TOÁN VIÊN THAM DỰ<br>CẬP NHẬT KIẾN THỨC LỚP SỐ " + maLopHoc + @" NĂM " + nam + @"</td>
            </tr>
            <tr>
                <td colspan='12' style='text-align:center;font-size: 12pt; font-weight: bold;'><i>Hà Nội, ngày " + fromDate.ToString("dd/MM/yyyy") + @" - " + toDate.ToString("dd/MM/yyyy") + @"</i></td>
            </tr>";

        for (int i = 0; i < dtChuyenDe.Rows.Count; i++)
        {
            htmlExport += @"<tr>
                <td class='ColumnChuyenDe' style='font-weight: bold;'>Chuyên đề "+(i + 1)+@":</td>
                <td colspan='3' class='ColumnChuyenDe'>" + dtChuyenDe.Rows[i]["ChuyenDe"] + @"</td>
                <td class='ColumnChuyenDe'>Giảng viên:</td>
                <td colspan='1' class='ColumnChuyenDe' style='font-weight: bold;'>" + dtChuyenDe.Rows[i]["GiangVien"] + @"</td>
                <td class='ColumnChuyenDe'>Chức vụ:</td>
                <td colspan='2' class='ColumnChuyenDe' style='font-weight: bold;'>" + dtChuyenDe.Rows[i]["ChucVu"] + @"</td>
                <td class='ColumnChuyenDe'>Đơn vị công tác:</td>
                <td colspan='3' class='ColumnChuyenDe' style='font-weight: bold;'>" + dtChuyenDe.Rows[i]["DonViCongTac"] + @"</td>
            </tr>";
        }
        
        htmlExport += @"<tr><td><td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td rowspan='3' class='HeaderColumn1'>STT</td>
                                <td rowspan='3' class='HeaderColumn1' style='width: 150px;'>Họ và tên</td>
                                <td rowspan='3' class='HeaderColumn1'>Hội viên</td>
                                <td colspan='2' class='HeaderColumn1'>Chứng chỉ KTV</td>
                                <td colspan='" + (dynamicColumn.Rows.Count * 2) + @"' class='HeaderColumn1'>Chữ ký xác nhận thời gian tham dự CNKT</td>
                                <td rowspan='3' class='HeaderColumn1'>Tổng số giờ CNKT</td></tr>";
        htmlExport += @"<tr><td rowspan='2' class='HeaderColumn1'>Số</td><td rowspan='2' class='HeaderColumn1'>Ngày cấp</td>";
        for (int i = 0; i < dynamicColumn.Rows.Count; i++)
        {
            if (dynamicColumn.Rows[i]["Sang"].ToString().Trim() != "")
                htmlExport += "<td class='HeaderColumn1' style='width: 70px;'>Sáng ngày " + Library.DateTimeConvert(dynamicColumn.Rows[i][0]).ToString("dd/MM/yyyy") + "</td>";
            if (dynamicColumn.Rows[i]["Chieu"].ToString().Trim() != "")
                htmlExport += "<td class='HeaderColumn1' style='width: 70px;'>Chiều ngày " + Library.DateTimeConvert(dynamicColumn.Rows[i][0]).ToString("dd/MM/yyyy") + "</td>";
        }
        htmlExport += "</tr>";
        htmlExport += "<tr>";
        for (int i = 0; i < dynamicColumn.Rows.Count; i++)
        {
            if (dynamicColumn.Rows[i]["Sang"].ToString().Trim() != "")
                htmlExport += "<td class='HeaderColumn1' style='width: 70px;'>" + dynamicColumn.Rows[i][1] + "</td>";
            if (dynamicColumn.Rows[i]["Chieu"].ToString().Trim() != "")
                htmlExport += "<td class='HeaderColumn1' style='width: 70px;'>" + dynamicColumn.Rows[i][1] + "</td>";
        }
        htmlExport += "</tr>";

        string tempCongTy = "";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string congTy = dt.Rows[i]["TenDoanhNghiep"].ToString();
            if (tempCongTy != congTy)
            {
                tempCongTy = congTy;
                htmlExport += "<tr><td colspan='" + (6 + (dynamicColumn.Rows.Count * 2)) + "' class='GroupTitle2'>" + congTy + "</td></tr>";
            }

            htmlExport += "<tr><td class='ColumnStt'>" + (i + 1) + "</td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["HoVaTen"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["HoiVienVacpa"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoChungChiKTV"] + "</td>" +
                          "<td class='Column'>" + (!string.IsNullOrEmpty(dt.Rows[i]["NgayCapChungChiKTV"].ToString()) ? Library.DateTimeConvert(dt.Rows[i]["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy") : "") + "</td>";
            for (int x = 0; x < dynamicColumn.Rows.Count; x++)
            {
                htmlExport += "<td class='Column'></td><td class='Column'></td>";
            }
            htmlExport += "<td class='Column'></td></tr>";
        }
        htmlExport += "<tr><td></td><td colspan='4'>CỘNG TRANG</td>";
        for (int x = 0; x < dynamicColumn.Rows.Count; x++)
        {
            htmlExport += "<td class='Column'></td><td class='Column'></td>";
        }
        htmlExport += "<td class='Column'></td></tr>";
        htmlExport += "<tr><td></td><td colspan='4'>TỔNG CỘNG TRANG</td>";
        for (int x = 0; x < dynamicColumn.Rows.Count; x++)
        {
            htmlExport += "<td class='Column'></td><td class='Column'></td>";
        }
        htmlExport += "<td class='Column'></td></tr>";
        htmlExport += "<tr><td></td><td colspan='4'>Số KTV tham dự từng buổi</td>";
        for (int x = 0; x < dynamicColumn.Rows.Count; x++)
        {
            htmlExport += "<td class='Column'></td><td class='Column'></td>";
        }
        htmlExport += "<td class='Column'></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='10'></td></tr>";
        htmlExport += "<tr><td colspan='10' style='font-weight: bold;'>Tổng số học viên tham dự là " + objTongHV.ToString() + " người, gồm " + objTongHVX.ToString() + " người là Hội viên,"
+ objTongHVNotX.ToString() + "  người không phải là Hội viên; có " + objTongGV.ToString() + " giảng viên là Kiểm toán viên Đăng ký hành nghề.</td></tr>";
        htmlExport += "<tr><td colspan='10'></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td colspan='3' style='text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='3' style='font-weight: bold; text-align: center;'>Chủ tích/Giám đốc trung tâm</td>" +
                      "<td colspan='3' style='font-weight: bold; text-align: center;'>Người soát xét</td>" +
                      "<td colspan='3' style='font-weight: bold; text-align: center;'>Người lập</td></tr>";
        htmlExport += "<tr><td colspan='10'></td></tr>";
        htmlExport += "<tr><td colspan='10'></td></tr>";
        htmlExport += "<tr><td colspan='10'></td></tr>";
        htmlExport += "<tr><td colspan='10'></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td colspan='3' style='text-align: center;'>"+cm.Admin_HoVaTen+"</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("DanhSachKTVThamDuCNKT.xsl", "<html>" + css + htmlExport + "</html>");
    }
}