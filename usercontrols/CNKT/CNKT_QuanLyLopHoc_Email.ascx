﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyLopHoc_Email.ascx.cs" Inherits="usercontrols_CNKT_QuanLyLopHoc_Email" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<form id="FormDanhSachCongTy" runat="server" method="post" enctype="multipart/form-data" clientidmode="Static">
    <fieldset class="fsBlockInfor">
        <legend>Nội dung Email mời tham dự lớp học</legend>
        <div style="float: left; width: 95%; padding-left: 10px;">
            <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
            <br />
            <br />
            <label><b>File ảnh đính kèm: </b></label>&nbsp;
           <input type="file" id="FileDinhKem" name="FileDinhKem" accept=".jpg,.png" />
            <br />
            
            <br />
            <asp:Label ID="lblDes" runat="server" 
                Text="Dưới đây là thông tin chi tiết về lớp học:" Font-Bold="True"></asp:Label>
        </div>
        <div style="float: left; width: 100%;">
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False" HasExportButton="False" 
            HasPrintButton="False"/>
        </div>
    </fieldset>
    <input type="hidden" id="hdAction" name="hdAction" />

</form>
<script type="text/javascript">
    function submitform() {
        $('#hdAction').val('SendEmail');
        jQuery("#FormDanhSachCongTy").submit();
    }
    </script>