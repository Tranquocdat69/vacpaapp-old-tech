﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_QuanLyDangKyHoc.ascx.cs"
    Inherits="usercontrols_CNKT_QuanLyDangKyHoc" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
    
    .tdInput
    {
        max-width: 100px;
    }
</style>
<form id="Form1" name="Form1" runat="server">
<h4 class="widgettitle">
    Danh sách đăng ký học</h4>
<div>
    <div class="dataTables_length">
        <a id="Btn_ThemCaNhan" href="/admin.aspx?page=CNKT_DangKyHocCaNhan_Add" class="btn">
            <i class="iconfa-plus-sign"></i>Thêm đăng ký theo cá nhân</a> <a id="Btn_ThemTapThe"
                href="/admin.aspx?page=CNKT_DangKyHocCongTy_Add" class="btn"><i class="iconfa-plus-sign">
                </i>Thêm đăng ký theo tổ chức</a>
    </div>
    <div class="dataTables_length">
        <fieldset class="fsBlockInfor" style="margin-top: 0px;">
            <legend>Thông tin lớp học</legend>
            <table width="100%" border="0" class="formtblInfor">
                <tr>
                    <td>
                        Mã lớp học:
                    </td>
                    <td>
                        <input type="text" id="txtMaLopHoc" name="txtMaLopHoc" onchange="CallActionGetInforLopHoc();"
                            class="tdInput" />
                        <input type="hidden" name="hdLopHocID" id="hdLopHocID" />
                    </td>
                    <td>
                        <input type="button" value="---" onclick="OpenDanhSachLopHoc();" style="border: 1px solid gray;" />
                    </td>
                    <td>
                        Từ ngày:
                    </td>
                    <th>
                        <span name="spanTuNgay" id="spanTuNgay" />
                    </th>
                    <td>
                        Tổng số học viên đăng ký:
                    </td>
                    <th>
                        <span name="spanSoHocVienDaDangKy" id="spanSoHocVienDaDangKy" />
                    </th>
                </tr>
                <tr>
                    <td>
                        Tên lớp:
                    </td>
                    <th colspan="2">
                        <span id="spanTenLopHoc" name="spanTenLopHoc" />
                    </th>
                    <td>
                        Đến ngày:
                    </td>
                    <th>
                        <span name="spanDenNgay" id="spanDenNgay" />
                    </th>
                    <td>
                        Tổng số tiền:
                    </td>
                    <th>
                        <span name="spanTongSoTien" id="spanTongSoTien"></span>VNĐ
                    </th>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;">
                        <a href='javascript:;' id="linkOpenFormApprove" onclick="OpenFormApprove();" style="color: red; text-decoration: underline;
                            font-size: 11pt;">... đăng ký đang chờ xét duyệt</a>
                    </td>
                    <td>
                        Hạn đăng ký:
                    </td>
                    <th>
                        <span name="spanHanDangKy" id="spanHanDangKy" />
                    </th>
                    <td>
                        Số tiền đã thanh toán:
                    </td>
                    <th>
                        <span name="spanSoTienDaThanhToan" id="spanSoTienDaThanhToan">0</span> VNĐ
                    </th>
                </tr>
                <tr><td colspan="3"><input type="checkbox" id="HVCN" name="HVCN" value="1" checked="checked" />&nbsp;HVCN&nbsp;&nbsp;
  <input type="checkbox" id="KTV" name="KTV" value="1" checked="checked" />&nbsp;K.T Viên&nbsp;&nbsp;
  <input type="checkbox" id="NQT" name="NQT" value="1" checked="checked" />&nbsp;N.Q Tâm&nbsp;&nbsp;</td>
                    <td>ID hội viên:</td><td><input type="text" id="tk_id" name="tk_id" /></td>
                    <td>Tên hội viên:</td><td><input type="text" id="tk_name" name="tk_name" /></td>
                </tr>
            </table>
        </fieldset>
        
        <div style="width: 100%; margin-top: 5px; text-align: center;">
            <a id="btn_guithongbao"  href="#none" class="btn btn-rounded" onclick="confirm_thongbao_user($('#hdLopHocID').val());"><i class="iconfa-envelope"></i> Gửi thông báo thay đổi</a>
            <a href="javascript:;" id="btn_search" class="btn btn-rounded" onclick="SearchByLopHoc();">
                <i class="iconfa-search"></i>Tìm theo lớp này</a>
            <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="fnExcelReport()" ><i class="iconfa-save"></i> Kết xuất</a>
        </div>
    </div>
    <table width="100%" border="0" class="formtbl">
        <tr>
            
            <th style="width: 100px;">
                Mã chuyên đề
            </th>
            <th style="min-width: 150px;">
                Tên chuyên đề
            </th>
            <th style="width: 150px;">
                Số học viên đăng ký
            </th>
            
        </tr>
    </table>
    <div id='DivDanhSachDangKyTheoChuyenDe' style="max-height: 300px;">
        <table id="tblDanhSachDangKyTheoChuyenDe" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpDangKyTheoChuyenDe" runat="server">
                <ItemTemplate>
                    
                    <tbody>
                        <tr>
                            
                            <td style="width: 100px;">
                                <%# Eval("MaChuyenDe")%>
                            </td>
                            <td style="min-width: 150px;">
                                <%# Eval("TenChuyenDe")%>
                            </td>
                            <td style="width: 150px;text-align:center">
                                <%# Eval("SoLuong")%>
                            </td>
                            
                        </tr>
                    </tbody>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <br />
    <table width="100%" border="0" class="formtbl">
        <tr>
            <th style="width: 30px;">
                STT
            </th>
            <th style="width: 100px;">
                Mã
            </th>
            <th style="min-width: 150px;">
                Họ và tên
            </th>
            <th style="min-width: 150px;">
                Email
            </th>
            <th style="width: 150px;">
                Loại
            </th>
            <th style="width: 100px;">
                Số chứng chỉ KTV
            </th>
            <th style="width: 80px;">
                Ngày đăng ký
            </th>
            <th style="width: 50px;">
                Nhận GCN
            </th>
            <th style="width: 50px;">
                Số giờ CNKT
            </th>
            <th style="width: 70px;">
                Tổng phí (VNĐ)
            </th>
            <th style="width: 70px;">
                Thao tác
            </th>
        </tr>
    </table>
    <div id='DivDanhSachDangKyHoc' style="max-height: 300px;">
        <table id="tblDanhSachDangKyHoc" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpDanhSachDangKyHoc" runat="server">
                <ItemTemplate>
                    <%# Eval("HeaderType") %>
                    <tbody <%# Eval("HiddenRow") %>>
                        <tr <%# Eval("trStyle") %>>
                            <td style="text-align: center; width: 30px;">
                                <%# Eval("STT") %>
                            </td>
                            <td style="width: 100px;">
                                <%# Eval("mhvcn")%>
                            </td>
                            <td style="min-width: 150px;">
                                <%# Eval("FullName")%>
                            </td>
                            <td style="min-width: 150px;">
                                <%# Eval("Email")%>
                            </td>
                            <td style="width: 150px;">
                                <%# Eval("LoaiHoiVienCaNhan")%>
                            </td>
                            <td style="width: 100px;">
                                <%# Eval("SoChungChiKTV")%>
                            </td>
                            <td style="width: 80px;">
                                <%# Eval("NgayDangKy")%>
                            </td>
                            <td style="text-align: center; width: 50px;">
                                <%# Eval("layGCN")%>
                            </td>
                            <td style="text-align: center; width: 50px;">
                                <%# Eval("soGioCNKT")%>
                            </td>
                            <td style="text-align: right; width: 70px;">
                                <%# Eval("TongPhi")%>
                            </td>
                            <td style="width: 70px;">
                                <%# Eval("LinkEdit") %>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</div>
    <div id="div_thongbao"></div>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    function confirm_thongbao_user(id) {

        $("#div_thongbao").empty();
        $("#div_thongbao").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=cnkt_thongbao_add&mode=iframe&lophocid=" + id));
        $("#div_thongbao").dialog({
            resizable: true,
            width: 1080,
            height: 740,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Gửi thông báo cho học viên</b>",
            modal: true,

            buttons: {

                "Gửi": function () {
                    window.frames['iframe_user_add'].submitform();
                    // $("#iframe_user_add")[0].contentWindow.submitform();

                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_thongbao').parent().find('button:contains("Gửi")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_thongbao').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

<% CheckPermissionOnPage(); %>

// Hiển thị tooltip
    if (jQuery('#tblDanhSachDangKyHoc').length > 0) jQuery('#tblDanhSachDangKyHoc').tooltip({ selector: "a[rel=tooltip]" });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách lớp học
    function OpenDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").empty();
        $("#DivDanhSachLopHoc").append($("<iframe width='100%' height='100%' id='ifDanhSachLopHoc' name='ifDanhSachLopHoc' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyLopHoc_MiniList"));
        $("#DivDanhSachLopHoc").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách lớp học</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu lớp học theo mã lớp khi NSD nhập trực tiếp mã lớp vào ô textbox
    function CallActionGetInforLopHoc() {
        var maLopHoc = $('#txtMaLopHoc').val();
        iframeProcess.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh='+maLopHoc+'&action=loadlh';
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin lớp học từ Dialog danh sách
    function DisplayInforLopHoc(value) {
        var arrValue = value.split(';#');
        $('#hdLopHocID').val(arrValue[0]);
        $('#txtMaLopHoc').val(arrValue[1]);
        $('#spanTenLopHoc').html(arrValue[2]);
        $('#spanTuNgay').html(arrValue[3]);
        $('#spanDenNgay').html(arrValue[4]);
        $('#spanHanDangKy').html(arrValue[5]);
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách lớp học (Gọi hàm này từ trang con)
    function CloseFormDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").dialog('close');
    }
    
    function OpenFormApprove() {
        $("#DivFormApprove").empty();
        $("#DivFormApprove").append($("<iframe width='100%' height='100%' id='ifFormApprove' name='ifFormApprove' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyDangKyHoc_PheDuyet&idlh=" + $('#hdLopHocID').val()));
        $("#DivFormApprove").dialog({
            resizable: true,
            width: 900,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách đăng ký vượt số học viên tối đa lớp "+$('#txtMaLopHoc').val()+"</b>",
            modal: true,
            zIndex: 1000,
            close: function( event, ui ) {
                SearchByLopHoc();
            }
        });
    }
    
    function SearchByLopHoc() {
        var lopHocID = $('#hdLopHocID').val();
        var HVCN = '1';
        var KTV = '1';
        var NQT = '1';
        checked = $('#HVCN').prop("checked");
        if(!checked)
           HVCN = '0';
        checked = $('#KTV').prop("checked");
        if (!checked)
            KTV = '0';
        checked = $('#NQT').prop("checked");
        if (!checked)
            NQT = '0';
        var tk_id = $('#tk_id').val();
        var tk_name = $('#tk_name').val();
        if(lopHocID == ''){
            alert('Phải chọn thông tin lớp học trước!');
            return;    
        } else {
            window.location = window.location.href.split('&lophocid')[0] + '&lophocid=' + lopHocID + '&hvcn=' + HVCN + '&ktv=' + KTV + '&nqt=' + NQT + '&tk_id=' + tk_id + '&tk_name=' + tk_name;
        }
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm kiểm tra trình duyệt để set thuộc tính overflow cho thẻ DIV hiển thị danh sách bản ghi
    // Chỉ trình duyệt nền Webkit mới có thuộc tính overflow: overlay
    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivDanhSachDangKyHoc').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivDanhSachDangKyHoc').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivDanhSachDangKyHoc').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }

    myFunction();

    function fnExcelReport() {
        $("#tblDanhSachDangKyHoc :input").each(function () {
            this.outerHTML = "<span id=" + this.id + " class=" + this.className + ">" + this.value + "</span>";
        });

        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j = 0;
        tab = document.getElementById('tblDanhSachDangKyHoc'); // id of table

        for (j = 1 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        //tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "CNKT.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);

    }
</script>
<div id="DivDanhSachLopHoc">
</div>
<div id="DivFormApprove">
</div>
