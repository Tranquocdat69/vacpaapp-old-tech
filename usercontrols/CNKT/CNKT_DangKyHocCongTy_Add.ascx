﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_DangKyHocCongTy_Add.ascx.cs"
    Inherits="usercontrols_CNKT_DangKyHocCongTy_Add" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .tdInput
    {
        width: 120px;
    }
</style>
<form id="form_DangKyCaNhan" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Đăng ký học theo công ty</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <table id="tblThongTinChung" width="900px" border="0" class="formtbl">
        <tr>
            <td>
                ID<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtMaCongTy" id="txtMaCongTy" onchange="CallActionGetInforCongTy();" />
                <input type="hidden" name="hdCongTyID" id="hdCongTyID" />
            </td>
            <td>
                <input type="button" value="---" onclick="OpenDanhSachCongTy();" style="border: 1px solid gray;" />
            </td>
            <td>
                Ngày gia nhập HVTC:
            </td>
            <td class="tdInput">
                <input type="text" name="txtNgayGiaNhapHVTT" id="txtNgayGiaNhapHVTT" readonly="readonly" />
            </td>
            <td>
                Người đại diện:
            </td>
            <td class="tdInput">
                <input type="text" name="txtNguoiDaiDien" id="txtNguoiDaiDien" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Tên công ty:
            </td>
            <td colspan="2">
                <input type="text" name="txtTenCongTy" id="txtTenCongTy" readonly="readonly" />
            </td>
            <td>
                Số giấy chứng nhận ĐKKD:
            </td>
            <td>
                <input type="text" name="txtSoGiayChungNhanDKKD" id="txtSoGiayChungNhanDKKD" readonly="readonly" />
            </td>
            <td>
                Mobile:
            </td>
            <td>
                <input type="text" name="txtMobile" id="txtMobile" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Tên viết tắt:
            </td>
            <td colspan="2">
                <input type="text" name="txtTenVietTat" id="txtTenVietTat" readonly="readonly" />
            </td>
            <td>
                Ngày cấp:
            </td>
            <td>
                <input type="text" name="txtNgayCap" id="txtNgayCap" readonly="readonly" />
            </td>
            <td>
                Email:
            </td>
            <td>
                <input type="text" name="txtEmail" id="txtEmail" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Mã số thuế:
            </td>
            <td colspan="2">
                <input type="text" name="txtMaSoThue" id="txtMaSoThue" readonly="readonly" />
            </td>
            <td colspan="4">
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Thông tin lớp học</legend>
    <table width="900px" border="0" class="formtbl">
        <tr>
            <td>
                Tên lớp học<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtMaLopHoc" id="txtMaLopHoc" onchange="CallActionGetInforLopHoc();" />
                <input type="hidden" name="hdLopHocID" id="hdLopHocID" />
            </td>
            <td>
                <input type="button" value="---" onclick="OpenDanhSachLopHoc();" style="border: 1px solid gray;" />
            </td>
            <td>
                Tên lớp học:
            </td>
            <td colspan="3">
                <input type="text" name="txtTenLopHoc" id="txtTenLopHoc" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
            <td>
                Từ ngày:
            </td>
            <td class="tdInput">
                <input type="text" name="txtTuNgay" id="txtTuNgay" readonly="readonly" />
            </td>
            <td>
                Hạn đăng ký:
            </td>
            <td class="tdInput">
                <input type="text" name="txtHanDangKy" id="txtHanDangKy" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Ngày đăng ký<span class="starRequired">(*)</span>:
            </td>
            <td colspan="2">
                <input type="text" name="txtNgayDangKy" id="txtNgayDangKy" />
            </td>
            <td>
                Đến ngày:
            </td>
            <td class="tdInput">
                <input type="text" name="txtDenNgay" id="txtDenNgay" readonly="readonly" />
            </td>
            <td>
                Tỉnh/thành:
            </td>
            <td class="tdInput">
                <input type="text" name="txtTinhThanh" id="txtTinhThanh" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Tổng phí (VNĐ):
            </td>
            <td colspan="2">
                <input type="text" name="txtTongPhi" id="txtTongPhi" readonly="readonly" style="text-align: right;" />
            </td>
            <td>
                Đã nộp phí:
            </td>
            <td>
                <input type="checkbox" name="cboDaNopPhi" id="cboDaNopPhi" readonly="readonly" />
            </td>
            <td colspan="2">
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Danh sách học viên</legend>
    <input type="hidden" id="hdListDangKyHocID" name="hdListDangKyHocID" />
    <input type="hidden" id="hdListMaHocVien" name="hdListMaHocVien" />
    <div style="width: 100%; margin: 10px 0 10px 0;">
        <a id="A2" href="javascript:;" class="btn" onclick="OpenFormThemHocVien(0);"><i class="iconfa-save">
        </i>Thêm</a> <a id="A3" href="javascript:;" class="btn" onclick="OpenFormThemHocVien(1);">
            <i class="iconfa-edit"></i>Sửa</a> <a id="A4" href="javascript:;" class="btn" onclick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?'))DeleteDangKyHoc();">
                <i class="iconfa-trash"></i>Xóa</a>
    </div>
    <table id="tblDanhSachDangKyHoc" width="100%" border="0" class="formtbl">
        <tr>
            <th>
                <input type="checkbox" class="checkall" value="all" />
            </th>
            <th>
                STT
            </th>
            <th>
                ID
            </th>
            <th>
                Họ và tên
            </th>
            <th>
                Đối tượng
            </th>
            <th>
                Số chứng chỉ KTV
            </th>
            <th>
                Ngày cấp chứng chỉ KTV
            </th>
            <th>
                Có lấy GCN
            </th>
            <th>
                Chuyên đề
            </th>
            <th>
                Tổng số giờ
            </th>
            <th>
                Phí phải nộp
            </th>
        </tr>
    </table>
</fieldset>
<div id="FormThemHocVien" style="width: 100%; margin-top: 10px; text-align: center;">
    <a id="btnBack" href="/admin.aspx?page=CNKT_QuanLyDangKyHoc" class="btn"><i class="iconfa-off">
    </i>Trở lại danh sách đăng ký</a>
</div>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadHocVien" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadCongTy" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadLopHoc" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadLopHocCongTy" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadLopHocHocVien" width="0px" height="0px"></iframe>
<div id="DivChonHocVien" style="display: none;">
    <div id="thongbaoloi_form_DangKyHocCaNhan" name="thongbaoloi_form_DangKyHocCaNhan"
        style="display: none; margin-top: 5px;" class="alert alert-error">
    </div>
    <div id="thongbaothanhcong_form_DangKyHocCaNhan" name="thongbaothanhcong_form_DangKyHocCaNhan"
        style="display: none; margin-top: 5px;" class="alert alert-success">
    </div>
    <fieldset class="fsBlockInfor">
        <legend>Thông tin chung</legend>
        <table border="0" class="formtbl">
            <tr>
                <td>
                    Họ và tên<span class="starRequired">(*)</span>:
                </td>
                <td colspan="2">
                    <select id='ddlHocVien' name='ddlHocVien' style="width: 150px;" onchange="CallActionGetInforHocVien();">
                    </select>
                    <%--<input type="text" id="txtMaHocVien" name="txtMaHocVien" onchange="CallActionGetInforHocVien();" />--%>
                    <input type="hidden" id="hdHocVienID" name="hdHocVienID" />
                    <input type="hidden" name="hdLoaiHocVien" id="hdLoaiHocVien" />
                    <input type="hidden" name="hdTroLyKTV" id="hdTroLyKTV" />
                    <input type="hidden" name="hdDangKyHocCaNhanID" id="hdDangKyHocCaNhanID" />
                </td>
                <td>
                    Số chứng chỉ KTV:
                </td>
                <td>
                    <input type="text" readonly="readonly" id="txtSoChungChiKTV" name="txtSoChungChiKTV" />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="checkbox" id="cboCoGCNGioCNKT" name="cboCoGCNGioCNKT" checked="checked"
                        value="1" />
                    Có nhận giấy chứng nhận giờ CNKT
                </td>
                <td>
                    Số giờ học:
                </td>
                <td>
                    <input type="text" readonly="readonly" id="txtSoGioCNKT" name="txtSoGioCNKT" />
                </td>
                <td>
                    Chi phí:
                </td>
                <td>
                    <input type="text" readonly="readonly" id="txtChiPhi" name="txtChiPhi" style="text-align: right;" />
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset class="fsBlockInfor" id="fsChonChuyenDe">
        <legend>Danh sách chuyên đề (*)</legend>
        <input type="hidden" id="hdListChuyenDeID" name="hdListChuyenDeID" />
        <table id="tblDanhSachChuyenDe" width="100%" border="0" class="formtbl">
            <tr>
                <th>
                    <input type="checkbox" class="checkall" value="all" />
                </th>
                <th>
                    STT
                </th>
                <th>
                    Mã CĐ
                </th>
                <th>
                    Tên chuyên đề
                </th>
                <th>
                    Loại CĐ
                </th>
                <th>
                    Ngày học
                </th>
                <th>
                    Thời lượng (buổi)
                </th>
                <th>
                    Số học viên
                </th>
            </tr>
        </table>
    </fieldset>
    <div id="Div1" style="width: 100%; margin-top: 10px; text-align: right;">
        <a id="btnLuuHocVien1" href="javascript:;" class="btn" onclick="SaveDangKyHocVien(0);">
            <i class="iconfa-save"></i>Lưu và thêm mới</a> <a id="btnLuuHocVien2" href="javascript:;"
                class="btn" onclick="SaveDangKyHocVien(1);"><i class="iconfa-save"></i>Lưu và kết
                thúc</a> <a id="A7" href="javascript:;" class="btn" onclick="CloseFormThemHocVien();">
                    <i class="iconfa-off"></i>Đóng</a>
    </div>
</div>
<script type="text/javascript">
    
    $(function () {
        $("#txtNgayDangKy").datepicker({
            dateFormat: 'dd/mm/yy'
        });
    });
    
    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Mở cửa sổ chọn học viên khi bấm nút "Thêm" hoặc "Sửa" đăng ký học của học viên
    // type = 0: Mở form rỗng để Insert; type = 1: Mở form có sẵn dữ liệu để update
    function OpenFormThemHocVien(type) {
        // Kiểm tra thông tin trước khi mở Form
        if($('#hdCongTyID').val() == '' || $('#hdLopHocID').val() == '') {
            alert('Phải chọn thông tin Công ty và Lớp học định đăng ký!');
            return;
        }

        // Ẩn ô thông báo lỗi trên Form khi mở form ra
        jQuery("#thongbaoloi_form_DangKyHocCaNhan").html('');
        jQuery("#thongbaoloi_form_DangKyHocCaNhan").hide();
        
        if(type == 0){ // Mở Form để thêm
            // Reset dữ liệu trên Form
            iframeProcess_LoadHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idhv=&action=loadhvbyid';
            $('#ddlHocVien').val('');
            $('#fsChonChuyenDe').css('display', 'none');
            $('#btnLuuHocVien1').css('display', 'none');
            $('#btnLuuHocVien2').css('display', 'none');
            }
        if(type == 1){ // Mở Form để sửa
            var maHocVien = $('#hdListMaHocVien').val();
            var arrMaHocVien = maHocVien.split(',');
            if(arrMaHocVien.length == 2) {
                iframeProcess_LoadHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mahv=' + arrMaHocVien[0] + '&idct=' + $('#hdCongTyID').val() + '&action=loadhv';
                $('#fsChonChuyenDe').css('display', '');
                $('#btnLuuHocVien1').css('display', '');
                $('#btnLuuHocVien2').css('display', '');
            } else {
                alert('Chỉ được chọn một bản đăng ký để sửa!');
                return;
            }
        }
        $("#DivChonHocVien").dialog({
            resizable: false,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Chọn học viên</b>",
            modal: true,
        });
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Đóng cửa sổ đăng ký khóa học cho học viên, đồng thời load lại dữ liệu danh sách học viên đã đăng ký
    function CloseFormThemHocVien() {
        $("#DivChonHocVien").dialog('close');
        // Gọi trang iframe xử lý lấy danh sách đăng ký học theo công ty
        iframeProcess_LoadLopHocCongTy.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&idct='+$('#hdCongTyID').val()+'&action=loadlhct';
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Gọi sự kiện lấy dữ liệu học viện theo mã học viên khi NSD nhập trực tiếp mã học viên vào ô textbox
    function CallActionGetInforHocVien() {
        var idHocVien = $('#ddlHocVien option:selected').val();
        if(idHocVien.length > 0) {
            $('#fsChonChuyenDe').css('display', '');
            $('#btnLuuHocVien1').css('display', '');
            $('#btnLuuHocVien2').css('display', '');
        }
        else {
            $('#fsChonChuyenDe').css('display', 'none');
            $('#btnLuuHocVien1').css('display', 'none');
            $('#btnLuuHocVien2').css('display', 'none');
        }
        iframeProcess_LoadHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idhv='+idHocVien+'&action=loadhvbyid';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm nhận giá trị thông tin học viên từ Dialog danh sách
    function DisplayInforHocVien(value) {
        var arrValue = value.split(';#');
        
        $('#hdHocVienID').val(arrValue[0]);
        $('#txtMaHocVien').val(arrValue[1]);
        $('#hdLoaiHocVien').val(arrValue[2]);
        $('#txtTenHocVien').val(arrValue[3]);
        $('#txtSoChungChiKTV').val(arrValue[4]);
        $('#hdTroLyKTV').val(arrValue[10]);

        $('#ddlHocVien').val(arrValue[0]);
        // Lấy danh sách các chuyên đề đã đăng ký của học viên với lớp học
        iframeProcess_LoadLopHocHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&idhv='+$('#hdHocVienID').val()+'&action=loadlhhv';
    }
    
    function DisplayListHocVien(arrData) {
        // Remove toàn bộ option cũ
        $('#ddlHocVien').children().remove();
        // Add option mới theo công ty
        if(arrData.length > 0) {
            $('#ddlHocVien').append('<option value=""><< Chọn học viên >></option>');
            for(var i = 0 ; i < arrData.length ; i ++) {
                $('#ddlHocVien').append('<option value="'+arrData[i][0]+'">'+arrData[i][1]+' - '+arrData[i][2]+'</option>');
            }
        } else {
            $('#ddlHocVien').append('<option value=""><< Không có học viên >></option>');
        }
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm mở Dialog danh sách công ty/hội viên tổ chức
    function OpenDanhSachCongTy() {
        $("#DivDanhSachCongTy").empty();
        $("#DivDanhSachCongTy").append($("<iframe width='100%' height='100%' id='ifDanhSachCongTy' name='ifDanhSachCongTy' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hoivientapthe_minilist"));
        $("#DivDanhSachCongTy").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách công ty</b>",
            modal: true,
            zIndex: 1000
        });
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Gọi sự kiện lấy dữ liệu công ty theo mã công ty khi NSD nhập trực tiếp mã công ty vào ô textbox
    function CallActionGetInforCongTy() {
        var maCongTy = $('#txtMaCongTy').val();
        iframeProcess_LoadCongTy.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mact=' + maCongTy + '&action=loadct';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm nhận giá trị thông tin công ty từ Dialog danh sách
    function DisplayInforCongTy(value) {
        var arrValue = value.split(';#');
        $('#hdCongTyID').val(arrValue[0]);
        $('#txtMaCongTy').val(arrValue[1]);
        $('#txtTenCongTy').val(arrValue[2]);
        $('#txtTenVietTat').val(arrValue[3]);
        $('#txtMaSoThue').val(arrValue[4]);
        $('#txtSoGiayChungNhanDKKD').val(arrValue[5]);
        //$('#txtEmail').val(arrValue[6]);
        $('#txtNgayGiaNhapHVTT').val(arrValue[6]);
        $('#txtNgayCap').val(arrValue[7]);
        $('#txtNguoiDaiDien').val(arrValue[8]);
        $('#txtMobile').val(arrValue[9]);
        $('#txtEmail').val(arrValue[10]);
        
        // Load danh sách học viên trong công ty
        iframeProcess_LoadHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idct='+$('#hdCongTyID').val()+'&action=loadlisthv';

        // Gọi trang iframe xử lý lấy danh sách đăng ký học theo công ty
        iframeProcess_LoadLopHocCongTy.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&idct='+arrValue[0]+'&action=loadlhct';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm đóng Dialog danh sách công ty (Gọi hàm này từ trang con)
    function CloseFormDanhSachCongTy() {
        $("#DivDanhSachCongTy").dialog('close');
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm mở Dialog danh sách lớp học
    function OpenDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").empty();
        $("#DivDanhSachLopHoc").append($("<iframe width='100%' height='100%' id='ifDanhSachLopHoc' name='ifDanhSachLopHoc' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=CNKT_QuanLyLopHoc_MiniList"));
        $("#DivDanhSachLopHoc").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách lớp học</b>",
            modal: true,
            zIndex: 1000
        });
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Gọi sự kiện lấy dữ liệu lớp học theo mã lớp khi NSD nhập trực tiếp mã lớp vào ô textbox
    function CallActionGetInforLopHoc() {
        var maLopHoc = $('#txtMaLopHoc').val();
        iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&malh=' + maLopHoc + '&action=loadlh';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm nhận giá trị thông tin lớp học từ Dialog danh sách
    function DisplayInforLopHoc(value) {
        var arrValue = value.split(';#');
        $('#hdLopHocID').val(arrValue[0]);
        $('#txtMaLopHoc').val(arrValue[1]);
        $('#txtTenLopHoc').val(arrValue[2]);
        $('#txtTuNgay').val(arrValue[3]);
        $('#txtDenNgay').val(arrValue[4]);
        $('#txtHanDangKy').val(arrValue[5]);
        $('#txtTinhThanh').val(arrValue[6]);
        $('#txtTongPhi').val('');
        
        // Gọi trang iframe xử lý lấy danh sách chuyên đề của lớp học
        iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&lophocid='+arrValue[0]+'&action=loadcd';
        
        // Gọi trang iframe xử lý lấy danh sách đăng ký học theo công ty
        iframeProcess_LoadLopHocCongTy.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+arrValue[0]+'&idct='+$('#hdCongTyID').val()+'&action=loadlhct';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm đóng Dialog danh sách lớp học (Gọi hàm này từ trang con)
    function CloseFormDanhSachLopHoc() {
        $("#DivDanhSachLopHoc").dialog('close');
    }

    var _arrDataChuyenDe = [];
    var _arrDataHocPhi = [];
    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm vẽ dữ liệu chuyên đề từ mảng dữ liệu trả về từ iFrame
    function DrawData_ChuyenDe(arrData, arrDataHocPhi) {
        _arrDataChuyenDe = arrData;
        _arrDataHocPhi = arrDataHocPhi;
        var tbl = document.getElementById('tblDanhSachChuyenDe');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var chuyenDeID = arrData[i][0];
                var maChuyenDe = arrData[i][1];
                var tenChuyenDe = arrData[i][2];
                var loaiChuyenDe = arrData[i][3];
                var tenLoaiChuyenDe = arrData[i][4];
                var tuNgay = arrData[i][5];
                var denNgay = arrData[i][6];
                var soBuoi = arrData[i][7];
                var soNguoiHocToiDa = arrData[i][20];
                var soNguoiDaDangKy = arrData[i][21];

                var tiLeDaDangKy = soNguoiDaDangKy + "/" + soNguoiHocToiDa;
                if(parseInt(soNguoiDaDangKy) >= parseInt(soNguoiHocToiDa))
                    tiLeDaDangKy = "<span style='color: green;'>" + tiLeDaDangKy + "</span>";
                
                var tr = document.createElement('TR');
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                var checkbox = document.createElement('input');
                checkbox.type = "checkbox";
                checkbox.value = i + ',' + chuyenDeID;
                checkbox.onclick = function() { GetOneChuyenDe(); };
                tdCheckBox.appendChild(checkbox);
                tr.appendChild(tdCheckBox);

                var ngayHoc = tuNgay + " - " + denNgay;
                if(tuNgay == denNgay)
                    ngayHoc = tuNgay;

                var arrColumn = [(i + 1), maChuyenDe, tenChuyenDe, tenLoaiChuyenDe, ngayHoc, soBuoi, tiLeDaDangKy];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
    }
    
    var _arrDataDangKyHoc = [];
    // Created by NGUYEN MANH HUNG - 2014/12/02
    // Hàm vẽ dữ liệu đăng ký học từ mảng dữ liệu trả về từ iFrame
    function DrawData_DangKyHoc(arrData, tongPhi) {
        _arrDataDangKyHoc = arrData;
        $('#txtTongPhi').val(parseFloat(tongPhi).format(0, 3));
        var tbl = document.getElementById('tblDanhSachDangKyHoc');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var dangKyHocCaNhanId = arrData[i][0];
                var maHocVien = arrData[i][1];
                var tenHocVien = arrData[i][2];
                var loaiHoiVienCaNhan = arrData[i][3];
                var tenLoaiHoiVienCaNhan = arrData[i][4];
                var soChungChiKTV = arrData[i][5];
                var ngayCapChungChiKTV = arrData[i][6];
                var layGCN = arrData[i][7];
                var soGioCNKT = arrData[i][8];
                var chiPhi = arrData[i][9];
                var chuyenDe = arrData[i][10];
                if(!isNaN(chiPhi) && chiPhi.length > 0)
                    chiPhi = parseFloat(chiPhi).format(0, 3);

                var tr = document.createElement('TR');
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                var checkbox = document.createElement('input');
                checkbox.type = "checkbox";
                checkbox.value = i + ',' + dangKyHocCaNhanId + ',' + maHocVien;
                checkbox.onclick = function() { GetOneDangKyHoc(); };
                tdCheckBox.appendChild(checkbox);
                tr.appendChild(tdCheckBox);
                
                var arrColumn = [(i + 1), maHocVien, tenHocVien, tenLoaiHoiVienCaNhan, soChungChiKTV, ngayCapChungChiKTV, layGCN, chuyenDe, soGioCNKT, chiPhi];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
    }
    
    // Created by NGUYEN MANH HUNG 2014/11/26
    // Hàm kiểm tra các bản ghi chuyên đề được tick để tính tổng phí, số giờ CNKT, và ghi lại List chuyenDeID được chọn để save vào db
    function GetOneDangKyHoc() {
        GetListCheckbox_DangKyHoc();
    }
    
    function GetListCheckbox_DangKyHoc() {
        var listDangKyHocID = '';
        var listMaHocVien = '';
        $('#hdListDangKyHocID').val('');
        $('#hdListMaHocVien').val('');
        $('#tblDanhSachDangKyHoc :checkbox').each(function () {
            var checked = $(this).prop("checked");
            if ((checked) && ($(this).val() != "all")){
                listDangKyHocID += $(this).val().split(',')[1] + ',';
                listMaHocVien += $(this).val().split(',')[2] + ',';
            }
        });
        $('#hdListDangKyHocID').val(listDangKyHocID);
        $('#hdListMaHocVien').val(listMaHocVien);
    }
    
    // Created by NGUYEN MANH HUNG 2014/11/28
    // Tick các chuyên đề đã đăng ký học (Dùng khi load dữ liệu các bản đăng ký cũ)
    function TickCheckBox(arrDataChuyenDeDaChon, dangKyHocCaNhanID, coNhanGCN, ngayDangKy, soGioCNKT, tongPhi) {
        $('#tblDanhSachChuyenDe :checkbox').each(function() {
            var chuyenDeId = $(this).val().split(',')[1];
            if(arrDataChuyenDeDaChon.indexOf(chuyenDeId) > -1)
                $(this).prop("checked", true);
            else
                 $(this).prop("checked", false);
        });
        
        if(coNhanGCN)
            $('#cboCoGCNGioCNKT').prop('checked', true);
        else
            $('#cboCoGCNGioCNKT').prop('checked', false);

        $('#txtNgayDangKy').val(ngayDangKy);
        $('#hdDangKyHocCaNhanID').val(dangKyHocCaNhanID);
        
        if(_arrDataChuyenDe.length > 0)
            GetListCheckbox(_arrDataChuyenDe);
        
        $('#txtSoGioCNKT').val(soGioCNKT);
        if(!isNaN(tongPhi) && tongPhi != '')
            $('#txtChiPhi').val(parseFloat(tongPhi).format(0, 3));
    }
    
    // Created by NGUYEN MANH HUNG 2014/11/28
    // Hàm kiểm tra các bản ghi chuyên đề được tick để tính tổng phí, số giờ CNKT, và ghi lại List chuyenDeID được chọn để save vào db
    function GetOneChuyenDe() {
        GetListCheckbox(_arrDataChuyenDe);
    }
    
    function GetListCheckbox(arrData) {
        var tongSoBuoi = 0; // biến chứa tổng số buổi theo các bản ghi được tick
        var tongPhi = 0; // Biến chứa tổng số phí theo các bản ghi được tick
        var listChuyenDeID = '';
        $('#hdListChuyenDeID').val('');
        $('#tblDanhSachChuyenDe :checkbox').each(function () {
            var checked = $(this).prop("checked");
            if ((checked) && ($(this).val() != "all")){
                var rowIndex = $(this).val().split(',')[0];
                if(arrData != undefined) {
                    tongSoBuoi = tongSoBuoi + (parseFloat(arrData[rowIndex][7]));
                }
                listChuyenDeID += $(this).val().split(',')[1] + ',';
            }
        });
        
        // Dua vao tongSoBuoi hoc -> doi chieu voi bang hoc phi de tinh tongSoPhi phai thanh toan
        for (var i = 0; i < _arrDataHocPhi.length; i++) {
            if(parseFloat(tongSoBuoi) == parseFloat(_arrDataHocPhi[i][0])) {
                var loaiHocVien = $('#hdLoaiHocVien').val();
                if(loaiHocVien == "1")
                    tongPhi = parseFloat(_arrDataHocPhi[i][1]);
                if(loaiHocVien == "2")
                    tongPhi = parseFloat(_arrDataHocPhi[i][2]);
                if(loaiHocVien == "0") {
                    var troLyKTV = $('#hdTroLyKTV').val();
                    if(troLyKTV == "1")
                        tongPhi = parseFloat(_arrDataHocPhi[i][3]);
                    if(troLyKTV == "0")
                        tongPhi = parseFloat(_arrDataHocPhi[i][4]);    
                }
            }
        }

        $('#hdListChuyenDeID').val(listChuyenDeID);
        $('#txtSoGioCNKT').val(tongSoBuoi * 4);
        $('#txtChiPhi').val(parseFloat(tongPhi).format(0, 3));
    }
    
    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Kiểm tra lỗi và save thông tin đăng ký học của từng học viên
    // isClose để xác định có đóng Form sau khi thêm hay ko (1: Có đóng Form)
    var _isClose = 0;
    function SaveDangKyHocVien(isClose) {
        _isClose = isClose;
        var hocVienID = $('#hdHocVienID').val();
        var msgError = "";
        // Kiểm tra bắt buộc phải có giá trị với ListChuyenDeID, hocVienID, lopHocID
        if($('#hdListChuyenDeID').val() == '')
            msgError += "Chưa chọn chuyên đề cần đăng ký học!<br />";
        if(hocVienID == "")
            msgError += "Chưa chọn học viên!<br />";
        
        // Kiểm tra ngày đăng ký < ngày bắt đầu lớp học
        var ngayDangKy = $('#txtNgayDangKy').datepicker('getDate');
        var arrTuNgay = $('#txtTuNgay').val().split('/');
        if(arrTuNgay.length == 3) {
            var tuNgay = new Date(arrTuNgay[2], parseInt(arrTuNgay[1]) - 1, arrTuNgay[0]);
            var minus = (tuNgay - ngayDangKy) / 1000 / 60 / 60 / 24;
            if(minus <= 0)
                msgError += "Ngày đăng ký trước ngày bắt đầu lớp học!<br />";
        }

        if(msgError != "") {
            jQuery("#thongbaoloi_form_DangKyHocCaNhan").html("<button class='close' type='button' data-dismiss='alert'>×</button>" + msgError).show();
            return;
        }
        else {
            jQuery("#thongbaoloi_form_DangKyHocCaNhan").hide();
        }
        
        if($('#hdDangKyHocCaNhanID').val().length == 0) {
            // Kiem tra chuyen de da vuot qua so nguoi hoc toi da chua
            iframeProcess.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&action=checkcd';   
        } else {
            // Gọi iframe xử lý save thông tin
        var layGcn = '';
        if($('#cboCoGCNGioCNKT').prop('checked'))
            layGcn = $('#cboCoGCNGioCNKT').val();
            iframeProcess.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&idct='+$('#hdCongTyID').val()+'&idhv='+$('#hdHocVienID').val()+'' +
                '&listchuyendeid='+$('#hdListChuyenDeID').val()+'&ngaydangky='+$('#txtNgayDangKy').val()+'&laygcn='+layGcn+'&dangkyhoccanhanid='+$('#hdDangKyHocCaNhanID').val()+'&sogiocnkt='+$('#txtSoGioCNKT').val()+'&tongphi='+$('#txtChiPhi').val()+'&isclose='+_isClose+'&action=savedangkyhocvien';
        }
    }
    
    function SubmitEventSaveForm(arrChuyenDeVuotQuaSoNguoiHoc) {
        if(arrChuyenDeVuotQuaSoNguoiHoc != undefined && arrChuyenDeVuotQuaSoNguoiHoc.length > 0) {
            var chuoiCanhBao = 'Chuyên đề:\n';
            for (var i = 0; i < arrChuyenDeVuotQuaSoNguoiHoc.length; i++) {
                chuoiCanhBao += ' - ' + arrChuyenDeVuotQuaSoNguoiHoc[i] + '\n';
            }
            chuoiCanhBao += 'đã vượt quá số người đăng ký học tối đa. Bạn có muốn tiếp tục đăng ký học cho học viên này không?';
            if(!confirm(chuoiCanhBao))
                return;
        }
        
        // Gọi iframe xử lý save thông tin
        var layGcn = '';
        if($('#cboCoGCNGioCNKT').prop('checked'))
            layGcn = $('#cboCoGCNGioCNKT').val();
        iframeProcess.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&idlh='+$('#hdLopHocID').val()+'&idct='+$('#hdCongTyID').val()+'&idhv='+$('#hdHocVienID').val()+'' +
            '&listchuyendeid='+$('#hdListChuyenDeID').val()+'&ngaydangky='+$('#txtNgayDangKy').val()+'&laygcn='+layGcn+'&dangkyhoccanhanid='+$('#hdDangKyHocCaNhanID').val()+'&sogiocnkt='+$('#txtSoGioCNKT').val()+'&tongphi='+$('#txtChiPhi').val()+'&isclose='+_isClose+'&action=savedangkyhocvien';
    }

    function DeleteDangKyHoc() {
        var listDangKyHocID = $('#hdListDangKyHocID').val();
        iframeProcess.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&iddangkyhoc='+listDangKyHocID+'&idlh='+$('#hdLopHocID').val()+'&idct='+$('#hdCongTyID').val()+'&action=deletedangkyhoc';
        $('#hdListDangKyHocID').val('');
    }

    function DisplayResultAfterSave(isClose) {
        iframeProcess_LoadLopHoc.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&lophocid='+$('#hdLopHocID').val()+'&action=loadcd';
        if(isClose == '1') {
            jQuery("#thongbaothanhcong_form_DangKyHocCaNhan").hide();
            CloseFormThemHocVien();
        }
        else {
            jQuery("#thongbaothanhcong_form_DangKyHocCaNhan").html("<button class='close' type='button' data-dismiss='alert'>×</button>Đăng ký khóa học cho học viên thành công!").show();
            
            // Reset dữ liệu trên Form
            iframeProcess_LoadHocVien.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mahv=&idct='+$('#hdCongTyID').val()+'&action=loadhv';
            $('#ddlHocVien').val('');
        }
    }
    
    jQuery(document).ready(function () {
        // dynamic table      

        $("#tblDanhSachChuyenDe .checkall").bind("click", function () {
            var listChuyenDeID = '';
            $('#hdListChuyenDeID').val('');
            var  checked = $(this).prop("checked");
            $('#tblDanhSachChuyenDe :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all"))
                    listChuyenDeID += $(this).val().split(',')[1] + ',';
            });
            $('#hdListChuyenDeID').val(listChuyenDeID);
            GetListCheckbox(_arrDataChuyenDe);
        });
        
        $("#tblDanhSachDangKyHoc .checkall").bind("click", function () {
            var listDangKyHocID = '';
            $('#hdListDangKyHocID').val('');
            var  checked = $(this).prop("checked");
            $('#tblDanhSachDangKyHoc :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) listDangKyHocID += $(this).val().split(',')[1] + ',';
            });
            $('#hdListDangKyHocID').val(listDangKyHocID);
        });
    });

    <% FirstLoadPage(); %>
</script>
<div id="DivDanhSachCongTy">
</div>
<div id="DivDanhSachLopHoc">
</div>
<div id="DivDanhSachHocVien">
</div>
