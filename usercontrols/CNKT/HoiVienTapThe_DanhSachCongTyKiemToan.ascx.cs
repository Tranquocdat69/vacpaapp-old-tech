﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_HoiVienTapThe_DanhSachCongTyKiemToan : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _idDanhSach = "", _type;
    protected void Page_Load(object sender, EventArgs e)
    {
        _idDanhSach = Library.CheckNull(Request.QueryString["iddanhsach"]);
        _type = Library.CheckNull(Request.QueryString["type"]);
        if (!IsPostBack)
        {
            LoadDanhSachVungMien();
            LoadDanhSachLoaiHinhCongTy();


            LoadData(_idDanhSach, _type);
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách công ty theo điều kiện tìm kiếm
    /// </summary>
    /// <param name="idDanhSach"></param>
    private void LoadData(string idDanhSach, string type)
    {
        try
        {
            _db.OpenConnection();
            // Lấy ngày lập danh sách -> Lấy năm kiểm tra các công ty trong danh sách đã được đưa vào danh sách khác chưa
            string procName = "";
            int year = 0;
            if (type == "bctukiemtra")
            {
                SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                KscldsBaoCaoTuKiemTra obj = pro.GetByDsBaoCaoTuKiemTraId(Library.Int32Convert(idDanhSach));
                year = DateTime.Now.Year;
                if (obj != null && obj.NgayLap != null)
                    year = obj.NgayLap.Value.Year;
                procName = ListName.Proc_KSCL_SearchDanhSachCongTyKiemToan_1;
            }
            if (type == "bckiemtravuviec")
            {
                SqlKscldsKiemTraVuViecProvider pro = new SqlKscldsKiemTraVuViecProvider(ListName.ConnectionString, false, string.Empty);
                KscldsKiemTraVuViec obj = pro.GetByDsYeuCauKiemTraVuViecId(Library.Int32Convert(idDanhSach));
                year = DateTime.Now.Year;
                if (obj != null && obj.NgayLap != null)
                    year = obj.NgayLap.Value.Year;
                procName = ListName.Proc_KSCL_SearchDanhSachCongTyKiemToan_YeuCauKiemTraVuViec;
            }

            List<string> param = new List<string>{"@DSBaoCao", "@DuDKKT_CK", "@DuDKKT_Khac", "@MaHocVienTapThe", "@TenDoanhNghiep", "@LoaiHinhDoanhNghiepID",
                    "@VungMienID", "@DoanhThuTu", "@DoanhThuDen", "@NamHienTai", "@XepHang", "@NgayBatDau", "@NgayKetThuc", "@NamYeuCau"};
            List<object> value = new List<object> { idDanhSach, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DateTime.Now.Year, DBNull.Value, DBNull.Value, DBNull.Value, year };

            if (type == "bckiemtratructiep")
            {
                param.Add("@DSTuKiemTraId");
                param.Add("@DSKiemTraVuViec");
                SqlKscldsKiemTraTrucTiepProvider pro = new SqlKscldsKiemTraTrucTiepProvider(ListName.ConnectionString, false, string.Empty);
                KscldsKiemTraTrucTiep obj = pro.GetByDsKiemTraTrucTiepId(Library.Int32Convert(idDanhSach));
                year = DateTime.Now.Year;
                if (obj != null && obj.NgayLap != null)
                {
                    year = obj.NgayLap.Value.Year;
                    value.Add(obj.DsBaoCaoTuKiemTraId);
                    value.Add(obj.DsYeuCauKiemTraVuViecId);
                }
                value[13] = year;
                procName = ListName.Proc_KSCL_SearchDanhSachCongTyKiemToan_DSKiemTraTrucTiep;
            }

            // Dựa theo các tiêu chí tìm kiếm -> đưa giá trị tham số tương ứng vào store
            if (cboDuDieuKienKTCK.Checked)
                value[1] = 1;
            if (cboDuDieuKienKTKhac.Checked)
                value[2] = 1;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                value[3] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                value[4] = txtTenCongTy.Text;
            if (ddlLoaiHinh.SelectedValue != "")
                value[5] = ddlLoaiHinh.SelectedValue;
            if (ddlVungMien.SelectedValue != "")
                value[6] = ddlVungMien.SelectedValue;
            if (!string.IsNullOrEmpty(txtDoanhThuTu.Text) && Library.CheckIsDecimal(txtDoanhThuTu.Text))
                value[7] = txtDoanhThuTu.Text;
            if (!string.IsNullOrEmpty(txtDoanhThuDen.Text) && Library.CheckIsDecimal(txtDoanhThuDen.Text))
                value[8] = txtDoanhThuDen.Text;
            if (!cboTieuChi_Khac.Checked)
            {
                string xepHang = "";
                if (cboXepHang1.Checked)
                    xepHang += "1,";
                if (cboXepHang2.Checked)
                    xepHang += "2,";
                if (cboXepHang3.Checked)
                    xepHang += "3,";
                if (cboXepHang4.Checked)
                    xepHang += "4,";
                value[10] = xepHang;
            }
            if (!string.IsNullOrEmpty(txtTuNam.Text) && Library.CheckIsInt32(txtTuNam.Text))
                value[11] = txtTuNam.Text + "-1-1";
            if (!string.IsNullOrEmpty(txtDenNam.Text) && Library.CheckIsInt32(txtDenNam.Text))
                value[12] = txtDenNam.Text + "-12-31";

            DataTable dt = _db.GetDataTable(procName, param, value);
            rpDanhSachCongTy.DataSource = dt.DefaultView;
            rpDanhSachCongTy.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách vùng miền đổ vào Dropdownlist
    /// </summary>
    private void LoadDanhSachVungMien()
    {
        ddlVungMien.Items.Clear();
        ddlVungMien.Items.Add(new ListItem("Tất cả", ""));
        SqlDmVungMienProvider vungMienPro = new SqlDmVungMienProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmVungMien> listData = vungMienPro.GetAll();
        foreach (DmVungMien dmVungMien in listData)
        {
            ddlVungMien.Items.Add(new ListItem(dmVungMien.TenVungMien, dmVungMien.VungMienId.ToString()));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách loại hình công ty
    /// </summary>
    private void LoadDanhSachLoaiHinhCongTy()
    {
        ddlLoaiHinh.Items.Clear();
        ddlLoaiHinh.Items.Add(new ListItem("Tất cả", ""));
        SqlDmLoaiHinhDoanhNghiepProvider loaiHinhPro = new SqlDmLoaiHinhDoanhNghiepProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmLoaiHinhDoanhNghiep> listData = loaiHinhPro.GetAll();
        foreach (DmLoaiHinhDoanhNghiep loaiHinh in listData)
        {
            ddlLoaiHinh.Items.Add(new ListItem(loaiHinh.TenLoaiHinhDoanhNghiep, loaiHinh.LoaiHinhDoanhNghiepId.ToString()));
        }
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadData(_idDanhSach, _type);
    }
}