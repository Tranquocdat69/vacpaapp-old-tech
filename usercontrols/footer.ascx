﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="footer.ascx.cs" Inherits="usercontrols_footer" %>

              <div class="footer">               
                  <div class="footer-left">
                        <span>&copy; <% Response.Write(DateTime.Now.Year); %>. Hội kiểm toán viên hành nghề Việt Nam - VACPA.</span>
                        
                  </div>
                  <div class="footer-right">
                        <span>Được xây dựng bởi: <a href="http://www.hipt.vn/">HiPT Group</a></span>
                  </div>
              </div><!--footer-->
