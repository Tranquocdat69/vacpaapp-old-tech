﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dmsothich.ascx.cs" Inherits="usercontrols_dmsothich" %>

 <asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

   

   <form id="Form1" name="Form1" runat="server">
   <h4 class="widgettitle">Danh mục <%=tenchucnang%></h4>
   <div>
        <div  class="dataTables_length">
        
        <a id="btn_them"  href="#none" class="btn btn-rounded" onclick="open_dmsothich_add();"><i class="iconfa-plus-sign"></i> Thêm</a>
        <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="confirm_delete_dmsothich(dmsothich_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
        <a href="#none" class="btn btn-rounded" onclick="open_dmsothich_search();"><i class="iconfa-search"></i> Tìm kiếm</a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
        </div>
        
   </div>

<asp:GridView ClientIDMode="Static" ID="dmsothich_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="dmsothich_grv_PageIndexChanging" onsorting="dmsothich_grv_Sorting"    
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval(truongid)%>" />
                  </ItemTemplate>
              </asp:TemplateField>

             
               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongma)%></span>
                  </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongten)%></span>
                  </ItemTemplate>
              </asp:TemplateField>
                     
              <asp:TemplateField HeaderText='' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                      <a onclick="open_dmsothich_view(<%# Eval(truongid)%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xem"  rel="tooltip" class="btn"><i class="iconsweets-trashcan2" ></i></a>
                      <a onclick="open_dmsothich_edit(<%# Eval(truongid)%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Sửa"  rel="tooltip" class="btn"><i class="iconsweets-create" ></i></a>
                      <a onclick="confirm_delete_dmsothich(<%# Eval(truongid)%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xóa"  rel="tooltip" class="btn" ><i class="iconsweets-trashcan" ></i></a>                                                   
                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>


          </Columns>
</asp:GridView>


<div class="dataTables_info" id="dyntable_info">Chuyển đến trang: 

<asp:DropDownList ID="Pager" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList>

</div>



                                     


</form>





<script type="text/javascript">

<% annut(); %>

    // Array ID được check
    var dmsothich_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#dmsothich_grv .checkall").bind("click", function () {
            dmsothich_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#dmsothich_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) dmsothich_grv_selected.push($(this).val());
            });
        });

        $('#dmsothich_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(dmsothich_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) dmsothich_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa khách hàng
    function confirm_delete_dmsothich(idxoa) {

        $("#div_dmsothich_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                    window.location = 'admin.aspx?page=dmsothich&act=delete&id=' + idxoa;
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_dmsothich_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_dmsothich_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Sửa thông tin khách hàng
    function open_dmsothich_edit(id) {
        var timestamp = Number(new Date());

        $("#div_dmsothich_add").empty();
        $("#div_dmsothich_add").append($("<iframe width='100%' height='100%' id='iframe_dmsothich_add' name='iframe_dmsothich_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmsothich_edit&id=" + id + "&mode=edit&time=" + timestamp));
        $("#div_dmsothich_add").dialog({
            resizable: true,
            width: 850,
            height: 240,
            title: "<img src='images/icons/application_form_edit.png'>&nbsp;<b>Sửa <%=tenchucnang%></b>",
            modal: true,
            close: function (event, ui) { window.location = 'admin.aspx?page=dmsothich'; },
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_dmsothich_add'].submitform();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_dmsothich_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_dmsothich_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Xem thông tin khách hàng
    function open_dmsothich_view(id) {
        var timestamp = Number(new Date());

        $("#div_dmsothich_add").empty();
        $("#div_dmsothich_add").append($("<iframe width='100%' height='100%' id='iframe_dmsothich_add' name='iframe_dmsothich_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmsothich_edit&id=" + id + "&mode=view&time=" + timestamp));
        $("#div_dmsothich_add").dialog({
            resizable: true,
            width: 850,
            height: 240,
            title: "<img src='images/icons/application_form_edit.png'>&nbsp;<b>Xem <%=tenchucnang%></b>",
            modal: true,
            buttons: {

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_dmsothich_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



    // mở form thêm khách hàng
    function open_dmsothich_add() {
        var timestamp = Number(new Date());

        $("#div_dmsothich_add").empty();
        $("#div_dmsothich_add").append($("<iframe width='100%' height='100%' id='iframe_dmsothich_add' name='iframe_dmsothich_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmsothich_add&mode=iframe&time=" + timestamp));
        $("#div_dmsothich_add").dialog({
            resizable: true,
            width: 850,
            height: 240,
            title: "<img src='images/icons/add.png'>&nbsp;<b>Thêm <%=tenchucnang%></b>",
            modal: true,
            close: function (event, ui) { window.location = 'admin.aspx?page=dmsothich'; },
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_dmsothich_add'].submitform();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_dmsothich_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_dmsothich_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    // Tìm kiếm
    function open_dmsothich_search() {
        var timestamp = Number(new Date());

        $("#div_dmsothich_search").dialog({
            resizable: true,
            width: 600,
            height: 210,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm</b>",
            modal: true,
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_dmsothich_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_dmsothich_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('#dmsothich_grv').length > 0) jQuery('#dmsothich_grv').tooltip({ selector: "a[rel=tooltip]" });


   

</script>

<div id="div_dmsothich_add" >
</div>

<div id="div_dmsothich_search" style="display:none" >
<form id="user_search" method="post" enctype="multipart/form-data" >
<table  width="100%" border="0" class="formtbl"  >
    <tr>
        <td style="width:200px"><label><%=tenchucnang%>: </label></td>
        <td><input type="text" name="timkiem_Ten" id="timkiem_Ten"  class="input-xlarge" />
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
        
        </td>
    </tr>
    
</table>
</form>
</div>

<div id="div_dmsothich_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?</p>

</div>

<script type="javascript">

    $('#timkiem_Ten').val('<%=Request.Form["timkiem_Ten"]%>');

 </script>


