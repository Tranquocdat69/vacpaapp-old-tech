﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="boxes.ascx.cs" Inherits="usercontrols_boxes" %>

   <h3>Headers</h3><br />
                
                <div class="row-fluid">
                    <div class="span6">
                        <h4 class="subtitle">This is a subtitle</h4>
                        <h4 class="subtitle2">This is the main title</h4>
                    </div>
                    <div class="span6">
                        <h4 class="subtitle">This is a subtitle</h4>
                        <h4>This Is The Main Title</h4>
                    </div>
                </div><!--row-fluid-->
                
                <br />
                
                <div class="row-fluid">
                    <div class="span6">
                        <h4 class="widgettitle title-primary">Header Primary</h4><br />
                        <h4 class="widgettitle title-danger">Header Danger</h4><br />
                        <h4 class="widgettitle title-warning">Header Warning</h4><br />
                        <h4 class="widgettitle title-success">Header Success</h4><br />
                        <h4 class="widgettitle title-info">Header Info</h4><br />
                        <h4 class="widgettitle title-inverse">Header Inverse</h4><br />
                    </div><!--span6-->
                    
                    <div class="span6">
                        <div class="headtitle">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                              </div>
                            <h4 class="widgettitle title-primary">Header Primary with Dropdown</h4>
                        </div>
                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                              </div>
                            <h4 class="widgettitle title-danger">Header Danger with Dropdown</h4>
                        </div>
                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                              </div>
                            <h4 class="widgettitle title-warning">Header Warning with Dropdown</h4>
                        </div>
                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                              </div>
                            <h4 class="widgettitle title-success">Header Success with Drdopdown</h4>
                        </div>
                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                              </div>
                            <h4 class="widgettitle title-info">Header Info with Dropdown</h4>
                        </div>
                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle title-inverse">Header Inverse with Dropdown</h4>
                        </div>
                    </div>
                </div><!--row-fluid-->
                
                <br />
                <h3>Boxes</h3><br />
                
                <div class="row-fluid">
                    
                    <div class="span6">
                        <div class="widgetbox">
                            <h4 class="widgettitle">Widget Box <a class="close">&times;</a> <a class="minimize">&#8211;</a></h4>
                            <div class="widgetcontent">
                                Content goes here...
                            </div>
                        </div>
                        
                        <div class="widgetbox box-danger">
                            <h4 class="widgettitle">Widget Box <a class="close">&times;</a> <a class="minimize">&#8211;</a></h4>
                            <div class="widgetcontent">
                                Content goes here...
                            </div>
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-warning">
                            <h4 class="widgettitle">Widget Box <a class="close">&times;</a> <a class="minimize">&#8211;</a></h4>
                            <div class="widgetcontent">
                                Content goes here...
                            </div>
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-success">
                            <h4 class="widgettitle">Widget Box <a class="close">&times;</a> <a class="minimize">&#8211;</a></h4>
                            <div class="widgetcontent">
                                Content goes here...
                            </div>
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-info">
                            <h4 class="widgettitle">Widget Box <a class="close">&times;</a> <a class="minimize">&#8211;</a></h4>
                            <div class="widgetcontent">
                                Content goes here...
                            </div>
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-inverse">
                            <h4 class="widgettitle">Widget Box <a class="close">&times;</a> <a class="minimize">&#8211;</a></h4>
                            <div class="widgetcontent">
                                Content goes here...
                            </div>
                        </div><!--widgetbox--> 
                        
                    </div><!--span6-->
                    
                    <div class="span6">
                        
                        <div class="widgetbox">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle">Widget Box with Dropdown</h4>
                        </div>
                        <div class="widgetcontent">
                            Content goes here...
                        </div><!--widgetcontent-->
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-danger">                        
                            <div class="headtitle">
                                <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li><a href="#">Action</a></li>
                                      <li><a href="#">Another action</a></li>
                                      <li><a href="#">Something else here</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <h4 class="widgettitle">Widget Box with Dropdown</h4>
                            </div>
                            <div class="widgetcontent">
                                Content goes here...
                            </div><!--widgetcontent-->
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-warning">                        
                            <div class="headtitle">
                                <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li><a href="#">Action</a></li>
                                      <li><a href="#">Another action</a></li>
                                      <li><a href="#">Something else here</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <h4 class="widgettitle">Widget Box with Dropdown</h4>
                            </div>
                            <div class="widgetcontent">
                                Content goes here...
                            </div><!--widgetcontent-->
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-success">                        
                            <div class="headtitle">
                                <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li><a href="#">Action</a></li>
                                      <li><a href="#">Another action</a></li>
                                      <li><a href="#">Something else here</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <h4 class="widgettitle">Widget Box with Dropdown</h4>
                            </div>
                            <div class="widgetcontent">
                                Content goes here...
                            </div><!--widgetcontent-->
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-info">                        
                            <div class="headtitle">
                                <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li><a href="#">Action</a></li>
                                      <li><a href="#">Another action</a></li>
                                      <li><a href="#">Something else here</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <h4 class="widgettitle">Widget Box with Dropdown</h4>
                            </div>
                            <div class="widgetcontent">
                                Content goes here...
                            </div><!--widgetcontent-->
                        </div><!--widgetbox-->
                        
                        <div class="widgetbox box-inverse">                        
                            <div class="headtitle">
                                <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li><a href="#">Action</a></li>
                                      <li><a href="#">Another action</a></li>
                                      <li><a href="#">Something else here</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <h4 class="widgettitle">Widget Box with Dropdown</h4>
                            </div>
                            <div class="widgetcontent">
                                Content goes here...
                            </div><!--widgetcontent-->
                        </div><!--widgetbox-->
                        
                    </div><!--span6-->
                </div><!--row-fluid-->

