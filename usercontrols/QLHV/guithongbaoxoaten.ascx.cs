﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;
using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;

public partial class usercontrols_QLHV_guithongbaoxoaten : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {



            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            // Tên chức năng
            string tenchucnang = "Thông báo kết quả";
            // Icon CSS Class  
            string IconClass = "iconfa-book";

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

                     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
                     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            // Phân quyền
            if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XEM|"))
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
                Form1.Visible = false;
                return;
            }




            ///////////

            load_users();




        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }

    protected void load_users()
    {
        try
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "SELECT A.XoaTenHoiVienID, B.MaHoiVienCaNhan AS MaHoiVien, B.HoDem + ' ' + B.Ten AS HoVaTen, N'HV cá nhân' AS LoaiHoiVien, A.LyDoXoaTen, B.Email FROM tblXoaTenHoiVien A INNER JOIN tblHoiVienCaNhan B ON A.HoiVienID = B.HoiVienCaNhanID WHERE LoaiHoiVien = 1 AND SoQDXoaTen IS NULL";

            cmd.CommandText += " UNION ALL ";

            cmd.CommandText += "SELECT A.XoaTenHoiVienID, B.MaHoiVienTapThe AS MaHoiVien, B.TenDoanhNghiep AS HoVaTen, N'HV tổ chức' AS LoaiHoiVien, A.LyDoXoaTen, B.Email FROM tblXoaTenHoiVien A INNER JOIN tblHoiVienTapThe B ON A.HoiVienID = B.HoiVienTapTheID WHERE LoaiHoiVien = 2 AND SoQDXoaTen IS NULL";

            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

            Users_grv.DataSource = dtb;
            Users_grv.DataBind();

            foreach (DataRow row in dtb.Rows)
            {
                lsID.Value += row["XoaTenHoiVienID"] + ",";
            }
            if (lsID.Value != "")
                lsID.Value = lsID.Value.Substring(0, lsID.Value.Length - 1);
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }
    protected void Users_grv_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridViewRow row = e.Row;
            row.Attributes["id"] = Users_grv.DataKeys[e.Row.RowIndex].Value.ToString();
        }
    }
    protected void btnLamMoi_Click(object sender, EventArgs e)
    {
        load_users();
    }

    protected void btnGuiThongBao_Click(object sender, EventArgs e)
    {
        SqlXoaTenHoiVienProvider XoaTen_provider = new SqlXoaTenHoiVienProvider(connStr, true, "");

        string[] lstID = lsID.Value.Split(',');
        foreach (string XoaTenHoiVienID in lstID)
        {
            if (XoaTenHoiVienID != "")
            {
                XoaTenHoiVien xoaten = XoaTen_provider.GetByXoaTenHoiVienId(int.Parse(XoaTenHoiVienID));

                xoaten.SoQdXoaTen = txtSoQuyetDinh.Text;
                xoaten.NgayQdXoaTen = DateTime.ParseExact(NgayQuyetDinh.Value, "dd/MM/yyyy", null);
                xoaten.GhiChuXoaTen = txtGhiChu.Text;

                string LoaiHoiVien = "";
                if (xoaten.LoaiHoiVien == "1")
                    LoaiHoiVien = "2";
                else
                    LoaiHoiVien = "3";

                if (fileDinhKem.PostedFile.ContentLength > 0)
                {
                    byte[] datainput1 = new byte[fileDinhKem.PostedFile.ContentLength];
                    fileDinhKem.PostedFile.InputStream.Read(datainput1, 0, fileDinhKem.PostedFile.ContentLength);
                    xoaten.FileDinhKem = datainput1;
                    xoaten.TenFileDinhKem = new System.IO.FileInfo(fileDinhKem.FileName).Name;

                    cm.GuiThongBaoVoiFileDinhKem(xoaten.HoiVienId.ToString(), LoaiHoiVien, "Quyết định xóa tên hội viên", "Bạn đã bị xóa tên hội viên cá nhân, bạn vẫn có thể đăng nhập vào hệ thống và sử dụng phần mềm với người sử dụng là kiểm toán viên.<BR><BR> Số quyết định <b>" + txtSoQuyetDinh.Text + "</b> ngày <b>" + NgayQuyetDinh.Value + "</b>", datainput1, xoaten.TenFileDinhKem);
                }
                else
                    cm.GuiThongBao(xoaten.HoiVienId.ToString(), LoaiHoiVien, "Quyết định xóa tên hội viên", "Bạn đã bị xóa tên hội viên cá nhân, bạn vẫn có thể đăng nhập vào hệ thống và sử dụng phần mềm với người sử dụng là kiểm toán viên.<BR> Số quyết định <b>" + txtSoQuyetDinh.Text + "</b> ngày <b>" + NgayQuyetDinh.Value + "</b>");

                XoaTen_provider.Update(xoaten);

                // Send mail
                string body = "";
                string email = "";
                SqlCommand cmd = new SqlCommand();

                if (xoaten.LoaiHoiVien == "1")
                {
                    cmd.CommandText = "SELECT Email FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + xoaten.HoiVienId;
                    email = DataAccess.DLookup(cmd);
                    body += "Bạn đã bị xóa tên hội viên cá nhân, bạn vẫn có thể đăng nhập vào hệ thống và sử dụng phần mềm với người sử dụng là kiểm toán viên.";
                }
                else
                {
                    cmd.CommandText = "SELECT Email FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + xoaten.HoiVienId;
                    email = DataAccess.DLookup(cmd);
                    body += "Bạn đã bị xóa tên hội viên tổ chức, bạn vẫn có thể đăng nhập vào hệ thống và sử dụng phần mềm với người sử dụng là công ty kiểm toán.";
                }
                string msg = "";
                if (!string.IsNullOrEmpty(email))
                    SmtpMail.Send("BQT WEB VACPA", email, "VACPA - Quyết định xóa tên hội viên tại VACPA", body, ref msg);
            }
        }

        cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Gửi thông báo xóa tên, XoaTenHoiVienID: " + lsID);

        load_users();

        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

        ErrorMessage.Controls.Add(new LiteralControl(thongbao));
    }
}