﻿using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;

using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using CommonLayer;
using VACPA.Data.Bases;

public partial class usercontrols_QLHV_yeucaucnkttaidonvikhac : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();




    string tuchoidon = "tuchoidon";
    string trinhpheduyet = "trinhpheduyet";
    string duyetdon = "duyetdon";
    string tuchoiduyet = "tuchoiduyet";
    string thoaiduyet = "thoaiduyet";


    int trangthaiChoTiepNhanID = 1;
    int trangthaiTuChoiID = 2;
    int trangthaiChoDuyetID = 3;
    int trangthaiTraLaiID = 4;
    int trangthaiDaPheDuyet = 5;
    int trangthaiThoaiDuyetID = 6;


    string tk_TTChoTiepNhan = "1";
    string tk_TTTuChoiTiepNhan = "2";
    string tk_TTChoDuyet = "3";
    string tk_TTTuChoiDuyet = "4";
    string tk_TTDaDuyet = "5";
    string tk_TTThoaiDuyet = "6";

    string tk_LoaiNQT = "0";
    string tk_LoaiHVCN = "1";
    string tk_LoaiKTV = "2";

    string tk_LoaiHVTT = "1";
    string tk_LoaiCTKT = "2";




    string delete = "delete";
    string canhan = "1";
    string tapthe = "2";

    private Db _db = new Db(ListName.ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {



            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            // Tên chức năng
            string tenchucnang = "Quản lý yêu cầu cập nhật thông tin khách hàng";
            // Icon CSS Class  
            string IconClass = "iconfa-book";

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

                     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
                     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            if (Session["MsgError"] != null)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
                Session.Remove("MsgError");
            }
            if (Session["MsgSuccess"] != null)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + Session["MsgSuccess"] + "</div>"));
                Session.Remove("MsgSuccess");
            }

            // Phân quyền
            if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("XEM|"))
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
                Form1.Visible = false;
                return;
            }




            try
            {


                //  action 
                if (!string.IsNullOrEmpty(Request.QueryString["act"]))
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["id"]))
                    {

                        // tu choi duyet 
                        if (Request.QueryString["act"] == tuchoiduyet)
                        {
                            String sLydotuchoi = Request.QueryString["lydo"];
                            string[] lstid = Request.QueryString["id"].Split(',');
                            bool isSendMail = true;
                            foreach (string sid in lstid)
                            {
                                string[] lstIterm = sid.Split('|');
                                String sDonHoiVienID = lstIterm[0];
                                String sDonHoiVienTinhtrang = lstIterm[1];
                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                {
                                    // btnTuChoiDuyet_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                    btnTuChoiDuyet_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                }
                            }
                            if (!isSendMail)
                            {

                            }

                        }


                        //  phe duyet 
                        if (Request.QueryString["act"] == duyetdon)
                        {
                            string[] lstId = Request.QueryString["id"].Split(',');
                            bool isSendMail = true;
                            foreach (string sid in lstId)
                            {
                                string[] lstIterm = sid.Split('|');
                                String sDonHoiVienID = lstIterm[0];
                                String sDonHoiVienTinhtrang = lstIterm[1];
                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                {
                                    // btnPheDuyet_Click(sDonHoiVienID, ref isSendMail);

                                    btnPheDuyet_Click(sDonHoiVienID, ref isSendMail);
                                }
                            }
                            if (!isSendMail)
                            {

                            }

                        }
                    }

                    string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thành công.</div>";
                    Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(thongbao));

                }

                string action = Request.Form["hdAction"];
                if (!string.IsNullOrEmpty(action) && action == "del")
                {
                    string listId = Request.Form["hdListID"];
                    Delete(listId);
                }
            }
            catch (Exception ex)
            {
                Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            }




            ///////////

            load_users();



        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }


    protected void load_users()
    {
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT X.HVYeuCauCNKTID, X.ID, X.IDYeuCau, X.TenKhachHang, X.DonVi, X.TenTrangThai, X.DonViToChuc, SUM(X.SoGioHoc) AS SoGioHoc FROM (SELECT CONVERT(NVARCHAR, A.HVYeuCauCNKTID) + '|' + CONVERT(NVARCHAR, A.TinhTrang) AS HVYeuCauCNKTID, A.HVYeuCauCNKTID AS ID, A.IDYeuCau, D.HoDem + ' ' + D.Ten AS TenKhachHang, E.TenDoanhNghiep AS DonVi, A.DonViToChuc, B.TenTrangThai, C.SoGioHoc FROM tblHVYeuCauCNKT A INNER JOIN tblHoiVienCaNhan D ON A.HoiVienCaNhanID = D.HoiVienCaNhanID LEFT JOIN tblHoiVienTapThe E ON D.HoiVienTapTheID = E.HoiVienTapTheID INNER JOIN tblDMTrangThai B ON A.TinhTrang = B.TrangThaiID  LEFT JOIN tblHVYeuCauCNKTCD C ON A.HVYeuCauCNKTID = C.HVYeuCauCNKTID) AS X
GROUP BY X.HVYeuCauCNKTID, X.IDYeuCau, X.TenKhachHang, X.DonVi, X.ID, X.TenTrangThai, X.DonViToChuc
 ORDER BY X.HVYeuCauCNKTID DESC";

            DataSet ds = DataAccess.RunCMDGetDataSet(cmd);

            DataView dt = ds.Tables[0].DefaultView;

            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;
            int i;
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "CapNhatHoSoHoiVien" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            ds = null;
            dt = null;




        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }
    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }


    protected void btnTuChoiDuyet_Click(String sYeuCauCapNhatID, String sLyDoTuChoi, ref bool isSendMail)
    {
        try
        {
            int yeuCauCapNhatId = 0;
            yeuCauCapNhatId = Convert.ToInt32(sYeuCauCapNhatID);
            if (yeuCauCapNhatId != null && yeuCauCapNhatId != 0)
            {
                string Email = "";
                UpdateStatus(yeuCauCapNhatId, trangthaiTraLaiID, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), sLyDoTuChoi, ref Email);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoDem, Ten FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = (SELECT HoiVienCaNhanID FROM tblHVYeuCauCNKT WHERE HVYeuCauCNKTID = " + yeuCauCapNhatId + ")";
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối phê duyệt yêu cầu cập nhật số giờ đào tạo CNKT tại đơn vị khác của hội viên " + dtb.Rows[0]["HoDem"].ToString() + " " + dtb.Rows[0]["Ten"].ToString());

                // Gửi thông báo
                //cm.GuiThongBao(capnhat.HoiVienCaNhanId.Value.ToString(), "1", "Phê duyệt yêu cầu cập nhật thông tin", "Yêu cầu cập nhật thông tin (Mã yêu cầu : " + capnhat.MaYeuCau + ") của bạn đã được phê duyệt");

                // Send mail
                string body = "";
                body += "Yêu cầu cập nhật số giờ đào tạo CNKT tại đơn vị khác của công ty không được phê duyệt<BR>";
                body += "Lý do: " + sLyDoTuChoi;
                string msg = "";
                if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối phê duyệt yêu cầu cập nhật số giờ đào tạo CNKT tại đơn vị khác", body, ref msg))
                {
                    isSendMail = true;
                }
                else
                {
                    isSendMail = false;

                }


            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnPheDuyet_Click(String sYeuCauCapNhatID, ref bool isSendMail)
    {

        try
        {
            int yeuCauCapNhatId = 0;
            yeuCauCapNhatId = Convert.ToInt32(sYeuCauCapNhatID);
            if (yeuCauCapNhatId != null && yeuCauCapNhatId != 0)
            {


                string Email = "";

                UpdateStatus(yeuCauCapNhatId, trangthaiDaPheDuyet, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), "", ref Email);
                // Gửi thông báo
                //cm.GuiThongBao(capnhat.HoiVienCaNhanId.Value.ToString(), "1", "Phê duyệt yêu cầu cập nhật thông tin", "Yêu cầu cập nhật thông tin (Mã yêu cầu : " + capnhat.MaYeuCau + ") của bạn đã được phê duyệt");
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoDem, Ten FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = (SELECT HoiVienCaNhanID FROM tblHVYeuCauCNKT WHERE HVYeuCauCNKTID = " + yeuCauCapNhatId + ")";
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt yêu cầu cập nhật số giờ đào tạo CNKT tại đơn vị khác của hội viên " + dtb.Rows[0]["HoDem"].ToString() + " " + dtb.Rows[0]["Ten"].ToString());

                // Send mail
                string body = "";
                body += "Yêu cầu cập nhật thông tin của công ty đã được phê duyệt";

                string msg = "";

                if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Phê duyệt yêu cầu cập nhật số giờ đào tạo CNKT tại đơn vị khác", body, ref msg))
                {
                    isSendMail = true;
                }
                else
                {
                    isSendMail = false;
                    //msg = "Lỗi không gửi được mail";
                    //ErrorMessage.Controls.Add(new LiteralControl(msg));
                }


            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }


    public bool UpdateStatus(int HVYeuCauCNKTID, int TinhTrangID, String NguoiDuyet,
                         String NgayDuyet, String LyDoTuChoi, ref string Email)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("HVYeuCauCNKTID", HVYeuCauCNKTID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));

            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));

            DBHelpers.DataParameter outEmail = new DBHelpers.DataParameter("Email", Email);
            outEmail.OutPut = true;
            arrParams.Add(outEmail);

            objDBHelper = ConnectionControllers.Connect("VACPA");


            bool bRedulte = objDBHelper.ExecFunction("sp_yeucaucnkt_xetduyet", arrParams);

            Email = outEmail.Param_Value.ToString();

            return bRedulte;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }

    private void Delete(string listId)
    {
        try
        {
            _db.OpenConnection();
            string listIdDontDel = "";
            var arrListId = listId.Split(',');
            foreach (var id in arrListId)
            {
                string trueId = id.Split('|')[0];
                // Check xem ban ghi da duoc duyet hay chua
                string query = "SELECT MaTrangThai, IDYeuCau FROM tblHVYeuCauCNKT YC INNER JOIN tblDMTrangThai TT ON YC.TinhTrang = TT.TrangThaiID WHERE YC.HVYeuCauCNKTID =" + trueId;
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    if (listData[0]["MaTrangThai"].ToString().Trim() != ListName.Status_DaPheDuyet)
                    {
                        if (_db.ExecuteNonQuery("DELETE FROM tblHVYeuCauCNKTCD WHERE HVYeuCauCNKTID = " + trueId) > 0)
                        {
                            if (_db.ExecuteNonQuery("DELETE FROM tblHVYeuCauCNKT WHERE HVYeuCauCNKTID = " + trueId) > 0)
                            {
                                cm.ghilog("CapNhatHoSoHoiVien", "Đã xóa bản ghi có ID \"" + trueId + "\" của danh mục Cập nhật số giờ CNKT tại đơn vị khác");
                            }
                        }
                    }
                    else
                    {
                        listIdDontDel += "Không thể xóa bản ghi mã <b>" + listData[0]["IDYeuCau"] + "</b> vì đã được phê duyệt<br />";
                    }
                }
            }
            Session["MsgSuccess"] = "Xóa thông tin thành công!<br />" + listIdDontDel;
            Response.Redirect("/admin.aspx?page=yeucaucnkttaidonvikhac");
        }
        catch { _db.CloseConnection(); }
        finally { _db.CloseConnection(); }
    }


}