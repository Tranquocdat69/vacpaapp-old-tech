﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="yeucaucnkttaidonvikhac.ascx.cs"
    Inherits="usercontrols_QLHV_yeucaucnkttaidonvikhac" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style>
    .ui-datepicker
    {
        z-index: 99999999 !important;
    }
</style>
<form id="Form1" runat="server">
    <input type="hidden" id="hdAction" name="hdAction"/>
    <input type="hidden" id="hdListID" name="hdListID"/>
<h4 class="widgettitle">
    Danh sách yêu cầu cập nhật số giờ CNKT tại đơn vị khác</h4>
<div>
    <div class="dataTables_length">
        <a id="btnAdd" href="/admin.aspx?page=yeucaucnkttaidonvikhac_add" class="btn btn-rounded">
            <i class="iconfa-plus-sign"></i>&nbsp;Thêm mới</a> <a id="btnDelete" href="javascript:;" class="btn btn-rounded"
                onclick="Delete();"><i class="iconfa-trash">
                </i>Xóa</a> <a id="btn_duyetdon" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected , 'duyetdon' );">
                    <i class="iconfa-plus-sign"></i>&nbsp;Duyệt</a> <a id="btn_tuchoiduyet" href="#none" class="btn btn-rounded"
                        onclick="xulydon_hv(Users_grv_selected ,'tuchoiduyet' );"><i class="iconfa-plus-sign">
                        </i>Từ chối</a> <a href="#none" class="btn btn-rounded" onclick="open_user_search();">
                            <i class="iconfa-search"></i>Tìm kiếm</a> <a id="btn_ketxuat" href="#none" class="btn btn-rounded"
                                onclick="export_excel()"><i class="iconfa-save"></i>&nbsp;Kết xuất</a>
    </div>
</div>
<asp:GridView ClientIDMode="Static" ID="Users_grv" runat="server" AutoGenerateColumns="False"
    class="table table-bordered responsive dyntable" AllowPaging="True" AllowSorting="True"
    OnPageIndexChanging="Users_grv_PageIndexChanging" OnSorting="Users_grv_Sorting">
    <Columns>
        <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
            ItemStyle-Width="30px">
            <ItemTemplate>
                <input type="checkbox" class="colcheckbox" value="<%# Eval("HVYeuCauCNKTID")%>" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ID yêu cầu" SortExpression="MaYeuCau">
            <ItemTemplate>
                <a target="_blank" href='/admin.aspx?page=yeucaucnkttaidonvikhac_add&id=<%# Eval("HVYeuCauCNKTID").ToString().Split('|')[0] %>'><%# Eval("IDYeuCau") %></a>
                    
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:BoundField DataField="TenKhachHang" HeaderText="Tên khách hàng" SortExpression="TenKhachHang" />
        <asp:BoundField DataField="DonVi" HeaderText="Đơn vị công tác" SortExpression="DonVi" />
        <asp:BoundField DataField="DonViToChuc" HeaderText="Đơn vị tổ chức" SortExpression="DonViToChuc" />
        <asp:BoundField DataField="SoGioHoc" HeaderText="Số giờ học" ItemStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="TenTrangThai" HeaderText="Tình trạng" SortExpression="TinhTrang" />
        <asp:TemplateField HeaderText='Sửa' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <div class="btn-group">
                    <a target="_blank" data-placement="top"
                        data-rel="tooltip" href='/admin.aspx?page=yeucaucnkttaidonvikhac_add&id=<%# Eval("HVYeuCauCNKTID").ToString().Split('|')[0] %>' data-original-title="Sửa" rel="tooltip" class="btn">
                        <i class="iconsweets-create"></i></a>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText='Duyệt' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <div class="btn-group">
                    <a onclick="xulydon_hv('<%# Eval("HVYeuCauCNKTID")%>' , 'duyetdon' );" data-placement="top"
                        data-rel="tooltip" href="#none" data-original-title="Duyệt" rel="tooltip" class="btn">
                        <i class="iconsweets-create"></i></a>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText='Từ chối' ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <div class="btn-group">
                    <a onclick="xulydon_hv('<%# Eval("HVYeuCauCNKTID")%>' ,'tuchoiduyet');" data-placement="top"
                        data-rel="tooltip" href="#none" data-original-title="Từ chối" rel="tooltip" class="btn">
                        <i class="iconsweets-locked2"></i></a>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div class="dataTables_info" id="dyntable_info">
    <div class="pagination pagination-mini">
        Chuyển đến trang:
        <ul>
            <li><a href="#" onclick="$('#tranghientai').val(0); $('#user_search').submit();"><<
                Đầu tiên</a></li>
            <li><a href="#" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager').val()-1); $('#user_search').submit();}">
                < Trước</a></li>
            <li><a style="border: none; background-color: #eeeeee">
                <asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" Style="width: 55px;
                    height: 22px;" onchange="$('#tranghientai').val(this.value); $('#user_search').submit();">
                </asp:DropDownList>
            </a></li>
            <li style="margin-left: 5px;"><a href="#" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) {$('#tranghientai').val(parseInt($('#Pager').val())+1); $('#user_search').submit();} ;"
                style="border-left: 1px solid #ccc;">Sau ></a></li>
            <li><a href="#" onclick="$('#tranghientai').val($('#Pager option').length-1); $('#user_search').submit();">
                Cuối cùng >></a></li>
        </ul>
    </div>
</div>
<%--
<div class="dataTables_info" id="dyntable_info">Chuyển đến trang: 

<asp:DropDownList ID="Pager" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList>

</div>
--%>
</form>
<script type="text/javascript">

<% annut(); %>
    // Array ID được check
    var Users_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#Users_grv .checkall").bind("click", function () {
            Users_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#Users_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });

        $('#Users_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(Users_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa hội viên
    function confirm_delete_user(idxoa) {
     
        $("#div_user_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                 
                 window.location = 'admin.aspx?page=yeucaucnkttaidonvikhac&act=delete&id=' + idxoa;
                
                   
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Sửa thông tin hội viên
    function open_user_edit(id) {
        
//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=capnhathoso_pheduyet&id=" + id + "&mode=edit&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Sửa thông tin đơn hội viên</b>",
//            modal: true,
//            buttons: {

//                "Ghi": function () {
//                    window.frames['iframe_user_add'].submitform();
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    

    // mở form thêm hội viên tap the
    function open_user_add() {

//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=capnhathoso_pheduyet&mode=iframe&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Thêm đơn gia nhập hội viên mới</b>",
//            modal: true,
//            close: function (event, ui) { window.location = 'admin.aspx?page=ketnaphoivientapthe'; },
//            buttons: {

//                "Ghi": function () {
//                    window.frames['iframe_user_add'].submitform();
//                   // $("#iframe_user_add")[0].contentWindow.submitform();
//                   
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
//   
   
    }
      
    function xulydon_hv(lstid , act ) {
            $("#div_"+act).dialog({
            resizable: false,
            width: 400,
            height:200,
            modal: true,
            buttons: {
                "Đồng ý": function () {
                var lydo;
                if(act=='tuchoidon')
                {  
                   lydo= $('#LyDoTuChoiDon').val();
                }else{
                   lydo= $('#LyDoTuChoiDuyet').val();
                }
              
                 window.location = 'admin.aspx?page=yeucaucnkttaidonvikhac&act='+act+'&lydo='+lydo+'&id=' + lstid;
                   
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $("#div_"+act).parent().find('button:contains("Đồng ý")').addClass('btn btn-rounded').prepend('<i class="icon-ok"> </i>&nbsp;');
        $("#div_"+act).parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
  
     

    }
     
    // Tìm kiếm
    function open_user_search() {
        var timestamp = Number(new Date());

        $("#div_user_search").dialog({
            resizable: true,
           width: 765,
            height: 440,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Tìm kiếm</b>",
            modal: true,
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_user_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('#Users_grv').length > 0) jQuery('#Users_grv').tooltip({ selector: "a[rel=tooltip]" });
    
    function Delete() {
        if(confirm('Bạn chắc chắn muốn xóa những bản ghi này chứ?')) {
            $('#hdAction').val('del');
            $('#hdListID').val(Users_grv_selected);
            $('#<%= Form1.ClientID %>').submit();   
        }        
    }


</script>
<div id="div_user_add">
</div>
<div id="div_user_search" style="display: none">
    <form id="user_search" method="post" enctype="multipart/form-data" action="">
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td>
                <label>
                    Trạng thái:
                </label>
            </td>
            <td colspan="3">
                <table width="100%" border="0">
                    <tr>
                        <td style="width: 130px">
                            <input id="tk_TTAll" name="tk_TTAll" type="checkbox" class="input-xlarge checkall"
                                checked="checked" value="true" /><label style="width: 100px">Tất cả</label>
                        </td>
                        <td style="width: 130px">
                            <input id="tk_TTChoDuyet" name="tk_TTChoDuyet" type="checkbox" class="checkbox" value="true" /><label
                                style="width: 100px">Chờ duyệt</label>
                        </td>
                        <td style="width: 130px">
                            <input id="tk_TTTuChoiDuyet" name="tk_TTTuChoiDuyet" type="checkbox" class="checkbox"
                                value="true" /><label style="width: 100px">Từ chối duyệt</label>
                        </td>
                        <td style="width: 130px">
                            <input id="tk_TTDaDuyet" name="tk_TTDaDuyet" type="checkbox" class="checkbox" value="true" /><label
                                style="width: 100px">Đã duyệt</label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <label>
                    Mã khách hàng:</label>
            </td>
            <td colspan="3">
                <input type="text" name="tk_MaHoiVien" id="tk_MaHoiVien" class="input-xlarge">
            </td>
        </tr>
        <tr>
            <td>
                <label>
                    Tên khách hàng:</label>
            </td>
            <td colspan="3">
                <input type="text" name="tk_TenHoiVien" id="tk_TenHoiVien" class="input-xlarge">
            </td>
        </tr>
        <tr>
            <td>
                <label>
                    Đơn vị tổ chức:</label>
            </td>
            <td colspan="3">
                <input type="text" name="tk_DonViToChuc" id="tk_DonViToChuc" class="input-xlarge">
            </td>
        </tr>
        <tr>
            <td>
                <label>
                    Thời gian tổ chức:</label>
            </td>
            <td colspan="3">
                <input style="width: 100px" type="text" name="tk_ThoiGianTu" id="tk_ThoiGianTu" class="input-xlarge">
                -
                <input style="width: 100px" type="text" name="tk_ThoiGianDen" id="tk_ThoiGianDen"
                    class="input-xlarge">
            </td>
        </tr>
        <tr>
            <td>
                <label>
                    Loại chuyên đề:</label>
            </td>
            <td colspan="3">
                <select style="width: 100%" name="tk_LoaiChuyenDe" id="tk_LoaiChuyenDe">
                    <option value="1">Kế toán, kiểm toán</option>
                    <option value="2">Đạo đức nghề nghiệp</option>
                    <option value="3">Khác</option>
                </select>
            </td>
        </tr>
    </table>
    <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
    <input type="hidden" value="0" id="ketxuat" name="ketxuat" />
    </form>
</div>
<script language="javascript">

    //  

    $(function () {
        $("#tk_ThoiGianTu").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            onClose: function (selectedDate) {

                $("#tk_ThoiGianDen").datepicker("option", "minDate", selectedDate);

            }

        });

        $("#tk_ThoiGianDen").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            onClose: function (selectedDate) {

                $("#tk_ThoiGianTu").datepicker("option", "maxDate", selectedDate);

            }

        });

    });

    $(document).ready(function () {
        $('#tk_TTAll').click(function (event) {  //on click 
            if (this.checked) { // check select status
                $('.checkbox').each(function () { //loop through each checkbox

                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });


            } else {

                $('#tk_TTChoDuyet').prop('checked', false);
                $('#tk_TTTuChoiDuyet').prop('checked', false);
                $('#tk_TTDaDuyet').prop('checked', false);

            }
        });

        $('.checkbox').bind("click", function () {
            if (this.checked == false) {

                $('#tk_TTAll').prop('checked', false);
            } else {

                if ($('#tk_TTChoDuyet').is(':checked') == true &&
                           $('#tk_TTTuChoiDuyet').is(':checked') == true &&
                           $('#tk_TTDaDuyet').is(':checked') == true) {

                    $('#tk_TTAll').prop('checked', true);
                }

            }
        });



    });

</script>
<div id="div_user_delete" style="display: none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">
    <p>
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
        Xóa (các) bản ghi được chọn?(Hệ thống chỉ xóa các đơn chưa được phê duyệt)</p>
</div>
<div id="div_duyetdon" style="display: none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận duyệt yêu cầu cập nhật thông tin hội viên</b>">
    <p>
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
        Duyệt (các) yêu cầu được chọn?(Hệ thống chỉ duyệt các đơn chờ phê duyệt)</p>
</div>
<div id="div_tuchoiduyet" style="display: none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận từ chối yêu cầu cập nhật thông tin</b>">
    <p>
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
        Từ chối (các) yêu cầu được chọn?(Hệ thống chỉ từ chối các đơn chờ phê duyệt)</p>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td style="width: 100px">
                <label>
                    Lý do từ chối:
                </label>
            </td>
            <td colspan="3">
                <input type="text" name="LyDoTuChoiDuyet" id="LyDoTuChoiDuyet" class="input-xlarge" />
            </td>
        </tr>
    </table>
</div>
