﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="hoivientapthe_daotao_lophoc.ascx.cs" Inherits="usercontrols_QLHV_hoivientapthe_daotao_lophoc" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style>
    .table th, .table td {text-align:center !important}
</style>
<form id="form1" runat="server">
    <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
    <div class="boxed active">
       

  <section class="content has-sidebar">
   
    <div class="wrapper">	

    	 
        <div class="widget-3 widget">
        <div class="panel">
				
				<div class="shortcode-content">
					<div class="writecomment">
                    <asp:PlaceHolder ID="placeMessage" runat="server"></asp:PlaceHolder>
	   
             <div class="widget2" style="white-space:normal;">
         
                  <div class="menu_body">
           
            <div class="formRow">
            <asp:UpdatePanel ID="UpdatePanelThongTinDN" runat="server" ><ContentTemplate>
                <table width="100%" class="formRowTable" >
                    

                  <tr class="formRowTable">
                     <td   ><label>Tên công ty</label></td>   
                    <td colspan="3" align="left" style=" left:auto"  >
                        
                     <asp:Label ID="txtTenCongTy" runat="server" Font-Bold="true" ></asp:Label> 
                       
                       </td>   
                    
                   
 
                    </tr>

                   
          <tr class="formRowTable" >
                     <td  ><label>Tên lớp học</label></td>   
                    <td  colspan="3">
                     <div class="formRight">
                    <asp:Label ID="txtTenLopHoc" runat="server"  Font-Bold="true" ></asp:Label> 
                     </div>
                     </td>   
   
                        
                      
                           
                    </tr>

              <tr class="formRowTable" >
                     <td  ><label>Số lượng học viên</label></td>   
                    <td>
                     <div class="formRight">
                      <asp:Label ID="txtSoLuongHocVien" runat="server" Font-Bold="true" ></asp:Label> 
                     </div>
                     </td>   
   
                        <td  > 
                        </td>
                        <td  >
                         <div class="formRight">
                        
                         </div>
                        </td>   
                           
                    </tr>
                        

                </table> 
                
            </ContentTemplate></asp:UpdatePanel>  
                
         
            </div>
       
            </div>
                 <br />
            <asp:GridView ID="gvChuyenDe" runat="server" class="table table-bordered responsive dyntable" EmptyDataText="Chưa có dữ liệu" Width="100%"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:BoundField DataField="TenChuyenDe" ItemStyle-Width="400px" HeaderText="Tên chuyên đề" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="GiangVien" HeaderText="Giảng viên" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    
                            </Columns>
                            
                        </asp:GridView>
            </div>
            <div class="widget2">
         <br />
                  <div class="menu_body">
        
         
            
                            
                            <asp:GridView ID="gvLopHoc" runat="server" AutoGenerateColumns="false" 
                                Width="100%" class="table table-bordered responsive dyntable"  >
                                <Columns>
                                    <asp:BoundField DataField="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                      <asp:BoundField DataField="HoTen" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="LoaiHV" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="SoChungChiKTV" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="NgayCapChungChiKTV" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                  <%--<asp:BoundField DataField="TenLopHoc" HeaderText="Tên lớp học" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />--%>
                                 <asp:BoundField DataField="TuNgay" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                

                                  <asp:BoundField DataField="TT_KTKiT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="TT_DaoDuc" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="TT_Khac" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="TT_Tong" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                
                                  
                                </Columns>
                            </asp:GridView>
                            
                        

            </div>
            </div>
 
            
                       <div class="wizButtons" style=" text-align:center; padding: 7px"> 
			    <br>
              
                 
                  <asp:HiddenField ID="hidendHoiVienTapTheID" runat="server"></asp:HiddenField>
                   <asp:HiddenField ID="hidendLopHocID" runat="server"></asp:HiddenField>
                  <asp:HiddenField ID="hidendDonHoiVienTapTheID" runat="server"></asp:HiddenField>
                  <asp:HiddenField ID="hidendChiNhanhID" runat="server"></asp:HiddenField>
                  <asp:HiddenField ID="hidKhachHangID" runat="server" />
                  <asp:HiddenField ID="hidQuanTriID" runat="server" />
                   <asp:HiddenField ID="hidMode" runat="server" />
		    </div>	
					
	   	    </div>

		</div>
			<!-- END .panel -->
			</div>
        </div>
    </div>
       
  </section>

   




       
    </div>    
    </form>