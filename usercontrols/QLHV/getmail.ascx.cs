﻿using HiPT.VACPA.DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_QLHV_getmail : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }
    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Contains(tem.idDoiTuong.ToString()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void GioiTinh(string ma = "00")
    {
        List<objDoiTuong> lst = getGioiTinh();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Contains(tem.idDoiTuong.ToString()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    private List<objDoiTuong> getGioiTinh()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Nam";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 0;
        obj2.TenDoiTuong = "Nữ";
        
        lst.Add(obj1);
        lst.Add(obj2);
        
        return lst;
    }

    protected void btnLayMailHVCN_Click(object sender, EventArgs e)
    {
        
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = @"DECLARE @Result		NVARCHAR(MAX)
SET @Result = ''
	
SELECT		@Result = @Result + Email + '; '
FROM		tblHoiVienCaNhan 
WHERE		LoaiHoiVienCaNhan = '1' AND Email != '' AND Email IS NOT NULL";
        //if(!Request.Form["VungMienId"].Contains("-1"))
        //    cmd.CommandText += @" AND tblDMTinh.VungMienID IN (" + Request.Form["VungMienId"] + @")";
        if (!Request.Form["GioiTinh"].Contains("-1"))
            cmd.CommandText += @" AND GioiTinh IN (" + Request.Form["GioiTinh"] + @")";
cmd.CommandText += @";select @Result";

        txtMailHVCN.Text = DataAccess.DLookup(cmd);
    }

    protected void btnLayMailKTV_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = @"DECLARE @Result		NVARCHAR(MAX)
SET @Result = ''
	
SELECT		@Result = @Result + Email + '; '
FROM		tblHoiVienCaNhan 
WHERE		LoaiHoiVienCaNhan = '2' AND Email != '' AND Email IS NOT NULL";
        //if(!Request.Form["VungMienId"].Contains("-1"))
        //    cmd.CommandText += @" AND tblDMTinh.VungMienID IN (" + Request.Form["VungMienId"] + @")";
        if (!Request.Form["GioiTinh"].Contains("-1"))
            cmd.CommandText += @" AND GioiTinh IN (" + Request.Form["GioiTinh"] + @")";
        cmd.CommandText += @";select @Result";

        txtMailKTV.Text = DataAccess.DLookup(cmd);
    }

    protected void btnLayMailNQT_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = @"DECLARE @Result		NVARCHAR(MAX)
SET @Result = ''
	
SELECT		@Result = @Result + Email + '; '
FROM		tblHoiVienCaNhan 
WHERE		LoaiHoiVienCaNhan = '0' AND Email != '' AND Email IS NOT NULL";
        //if(!Request.Form["VungMienId"].Contains("-1"))
        //    cmd.CommandText += @" AND tblDMTinh.VungMienID IN (" + Request.Form["VungMienId"] + @")";
        if (!Request.Form["GioiTinh"].Contains("-1"))
            cmd.CommandText += @" AND GioiTinh IN (" + Request.Form["GioiTinh"] + @")";
        cmd.CommandText += @";select @Result";

        txtMailNQT.Text = DataAccess.DLookup(cmd);
    }

    protected void btnLayMailHVTT_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select dbo.LayDanhSachMail('2','1')";
        txtMailHVTT.Text = DataAccess.DLookup(cmd);
    }

    protected void btnLayMailCTKT_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select dbo.LayDanhSachMail('2','2')";
        txtMailCTKT.Text = DataAccess.DLookup(cmd);
    }

}