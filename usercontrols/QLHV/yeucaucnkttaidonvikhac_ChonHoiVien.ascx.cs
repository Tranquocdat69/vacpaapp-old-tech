﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class usercontrols_QLHV_yeucaucnkttaidonvikhac_ChonHoiVien : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _congTyId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this._congTyId = Request.QueryString["idct"];
            _db.OpenConnection();
            if (!IsPostBack)
            {
                LoadListHoiVienTapThe();
                LoadListHocVien();
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Load danh sach "Hoi vien" theo dieu kien search
    /// </summary>
    private void LoadListHocVien()
    {
        try
        {
            _db.OpenConnection();
            string query = @"SELECT HoiVienCaNhanID, MaHoiVienCaNhan, LoaiHoiVienCaNhan, HoDem + ' ' + Ten as FullName, SoChungChiKTV, NgayCapChungChiKTV, 
                                GioiTinh, tblHoiVienCaNhan.Mobile AS Mobile, tblHoiVienCaNhan.Email AS Email, tblHoiVienTapThe.TenDoanhNghiep AS DonViCongTac,
                                tblHoiVienCaNhan.TroLyKTV AS TroLyKTV, NgaySinh, ChucVuID
	                            FROM tblHoiVienCaNhan LEFT JOIN tblHoiVienTapThe ON tblHoiVienCaNhan.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
	                            WHERE {MaHoiVienCaNhan} AND (HoDem + ' ' + Ten LIKE N'%" + txtHoTen.Text + @"%')
	                            AND {LoaiHocVien} AND {DonViCongTac} ORDER BY FullName";

            query = !string.IsNullOrEmpty(txtMaHocVien.Text)
                        ? query.Replace("{MaHoiVienCaNhan}", "MaHoiVienCaNhan like N'%" + txtMaHocVien.Text + "%'")
                        : query.Replace("{MaHoiVienCaNhan}", "1=1");
            query = ddlPhanLoai.SelectedValue != ""
                        ? query.Replace("{LoaiHocVien}", "LoaiHoiVienCaNhan = '" + ddlPhanLoai.SelectedValue + "'")
                        : query.Replace("{LoaiHocVien}", "1=1");
            query = ddlDonViCongTac.SelectedValue != ""
                        ? query.Replace("{DonViCongTac}",
                                        "tblHoiVienCaNhan.HoiVienTapTheID = '" + ddlDonViCongTac.SelectedValue + "'")
                        : query.Replace("{DonViCongTac}", "1=1");
            DataTable dt = _db.GetDataTable(query);
            rpHocVien.DataSource = dt.DefaultView;
            rpHocVien.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Load danh sach "Hoi vien tap the", "Cong ty to chuc" do vao Dropdownlist de search
    /// </summary>
    private void LoadListHoiVienTapThe()
    {
        ddlDonViCongTac.Items.Clear();
        string query = "SELECT HoiVienTapTheID, TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        if (!string.IsNullOrEmpty(this._congTyId))
            query = "SELECT HoiVienTapTheID, TenDoanhNghiep FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + this._congTyId;
        else
            ddlDonViCongTac.Items.Add(new ListItem("Tất cả", ""));
        List<Hashtable> listData = _db.GetListData(query);
        foreach (Hashtable ht in listData)
        {
            ddlDonViCongTac.Items.Add(new ListItem(Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]), ht["HoiVienTapTheID"].ToString()));
        }
    }


    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadListHocVien();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Get chuoi ten loai hoc vien dua vao so loai hoc vien lay tu DB
    /// </summary>
    /// <param name="value">So loai hoc vien</param>
    /// <returns>Ten loai hoc vien</returns>
    protected string GetLoaiHocVien(string value)
    {
        switch (value)
        {
            case ListName.Type_LoaiHocVien_NguoiQuanTam:
                return "Người quan tâm";
            case ListName.Type_LoaiHocVien_HoiVien:
                return "Hội viên";
            case ListName.Type_LoaiHocVien_KiemToanVien:
                return "Kiểm toán viên";
        }
        return "";
    }
}