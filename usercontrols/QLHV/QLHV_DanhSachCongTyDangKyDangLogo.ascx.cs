﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CodeEngine.Framework.QueryBuilder.Enums;
using HiPT.VACPA.DL;

using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_QLHV_DanhSachCongTyDangKyDangLogo : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách công ty đăng logo";
    protected string _listPermissionOnFunc_DSCongTyDangLogo = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _listPermissionOnFunc_DSCongTyDangLogo = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_QLHV_DSCongTyDangKyDangLogo, cm.connstr);
        try
        {
            if (!IsPostBack || Request.Form["hdAction"] == "paging")
            {
                if (_listPermissionOnFunc_DSCongTyDangLogo.Contains("XEM|"))
                    LoadData();
                else
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem " + tenchucnang + "!</div>"));
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }

    /// <summary>
    /// create by HUNGNM - 2014/11/18
    /// Load list class from db
    /// </summary>
    private void LoadData()
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "DangKyLoGoID", "DoiTuong", "MaHoiVienTapThe", "TenDayDu", "TenVietTat", "KichThuoc", "ViTri", "NgayDang", "NgayHetHan", "HanNop", "TinhTrang", "MaTrangThai" });

        try
        {
            _db.OpenConnection();
            List<string> param = new List<string> { "@MaCongTy", "@TenGiaoDich", "@TenVietTat", "@KichThuocLogo", "@NgayDangTu", "@NgayDangDen", "@NgayHetHanTu", "@NgayHetHanDen", "@HanNopPhiTu", "@HanNopPhiDen", "@TinhTrang" };
            List<object> value = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value };
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                value[0] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                value[1] = txtTenCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenVietTat.Text))
                value[2] = txtTenVietTat.Text;
            if (!string.IsNullOrEmpty(txtKichThuocLogo.Text))
                value[3] = txtKichThuocLogo.Text;
            if (!string.IsNullOrEmpty(txtNgayDangTu.Text))
                value[4] = Library.DateTimeConvert(txtNgayDangTu.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtNgayDangDen.Text))
                value[5] = Library.DateTimeConvert(txtNgayDangDen.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtNgayHetHanTu.Text))
                value[6] = Library.DateTimeConvert(txtNgayHetHanTu.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtNgayHetHanDen.Text))
                value[7] = Library.DateTimeConvert(txtNgayHetHanDen.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtHanNopPhiTu.Text))
                value[8] = Library.DateTimeConvert(txtHanNopPhiTu.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtHanNopPhiDen.Text))
                value[9] = Library.DateTimeConvert(txtHanNopPhiDen.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            value[10] = rbtTinhTrang.SelectedValue;
            List<Hashtable> listData = _db.GetListData(ListName.Proc_QLHV_SearchDanhSachDangKyDangLogo, param, value);
            if (listData.Count > 0)
            {
                DataRow dataRow;
                foreach (Hashtable ht in listData)
                {
                    if (rbtDoiTuongDangLogo.SelectedValue == "0")
                    {
                        if (ht["DoiTuong"].ToString() != "0")
                            continue;
                    }
                    if (rbtDoiTuongDangLogo.SelectedValue == "3")
                    {
                        if (ht["DoiTuong"].ToString() != "3" && ht["DoiTuong"].ToString() != "4")
                            continue;
                    }
                    dataRow = dt.NewRow();
                    dataRow["DangKyLoGoID"] = ht["DangKyLoGoID"];
                    dataRow["DoiTuong"] = ht["DoiTuong"];
                    dataRow["MaHoiVienTapThe"] = ht["MaHoiVienTapThe"];
                    dataRow["TenDayDu"] = ht["TenDayDu"];
                    dataRow["TenVietTat"] = ht["TenVietTat"];
                    dataRow["KichThuoc"] = ht["KichThuoc"];
                    dataRow["ViTri"] = ht["ViTri"];
                    dataRow["TinhTrang"] = ht["TenTrangThai"];
                    string maTrangThai = ht["MaTrangThai"].ToString().Trim();
                    dataRow["MaTrangThai"] = maTrangThai;
                    dataRow["NgayDang"] = !string.IsNullOrEmpty(ht["NgayDang"].ToString()) ? Library.DateTimeConvert(ht["NgayDang"]).ToString("dd/MM/yyyy") : "";
                    dataRow["NgayHetHan"] = !string.IsNullOrEmpty(ht["NgayHetHan"].ToString()) ? Library.DateTimeConvert(ht["NgayHetHan"]).ToString("dd/MM/yyyy") : "";
                    dataRow["HanNop"] = !string.IsNullOrEmpty(ht["HanNop"].ToString()) ? Library.DateTimeConvert(ht["HanNop"]).ToString("dd/MM/yyyy") : "";
                    dt.Rows.Add(dataRow);
                }
            }
            DataView dv = dt.DefaultView;
            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"] + " " + ViewState["sortdirection"];
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            Pager.Items.Clear();
            for (int i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
                hf_TrangHienTai.Value = Request.Form["tranghientai"];
            if (!string.IsNullOrEmpty(hf_TrangHienTai.Value))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(hf_TrangHienTai.Value);
                Pager.SelectedIndex = Convert.ToInt32(hf_TrangHienTai.Value);
            }
            gv_DanhSachCongTy.DataSource = objPds;
            gv_DanhSachCongTy.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains("XEM|"))
        {
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        }

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains("SUA|"))
            Response.Write("$('a[data-original-title=\"Xem/Sửa\"]').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains("XOA|"))
        {
            Response.Write("$('#" + lbtDelete.ClientID + "').remove();");
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains("THEM|"))
            Response.Write("$('#btn_them').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains(ListName.PERMISSION_Duyet))
            Response.Write("$('#" + btnApprove.ClientID + "').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains(ListName.PERMISSION_TraLai))
            Response.Write("$('#" + btnReject.ClientID + "').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains(ListName.PERMISSION_ThoaiDuyet))
            Response.Write("$('#" + btnDontApprove.ClientID + "').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains(ListName.PERMISSION_KetXuat))
            Response.Write("$('#btnExport').remove();");
    }

    protected void gv_DanhSachCongTy_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_DanhSachCongTy.PageIndex = e.NewPageIndex;
        gv_DanhSachCongTy.DataBind();
    }

    protected void gv_DanhSachCongTy_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hfMaTrangThai = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("hfMaTrangThai") as HiddenField;
            LinkButton lbtDelete = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("btnDeleteInList") as LinkButton;
            if (hfMaTrangThai != null && lbtDelete != null)
                if (hfMaTrangThai.Value == ListName.Status_DaPheDuyet)
                    lbtDelete.Visible = false;
        }
    }

    protected void gv_DanhSachCongTy_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        LoadData();
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadData();
        rbtDoiTuongDangLogo.SelectedIndex = 0;
        rbtTinhTrang.SelectedIndex = 0;
        txtMaCongTy.Text = "";
        txtTenCongTy.Text = "";
        txtTenVietTat.Text = "";
        txtKichThuocLogo.Text = "";
        txtNgayDangTu.Text = "";
        txtNgayDangDen.Text = "";
        txtNgayHetHanTu.Text = "";
        txtNgayHetHanDen.Text = "";
        txtHanNopPhiTu.Text = "";
        txtHanNopPhiDen.Text = "";
    }
    protected void gv_DanhSachCongTy_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            string id = e.CommandArgument.ToString();
            Delete(new List<string> { id });
        }
    }
    protected void lbtDelete_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTy.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTy.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        Delete(listIdDanhSach);
    }

    /// <summary>
    /// Xóa danh sách đoàn kiểm tra
    /// </summary>
    /// <param name="listIdDanhSach">List ID danh sách</param>
    private void Delete(List<string> listIdDanhSach)
    {
        try
        {
            _db.OpenConnection();
            string maDanhSachKoXoaDuoc = "";
            string idTrangThai = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
            foreach (string idDanhSach in listIdDanhSach)
            {
                if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach))
                {
                    SqlTtDangKyLogoProvider pro = new SqlTtDangKyLogoProvider(ListName.ConnectionString, false, string.Empty);
                    // Kiểm tra tình trạng của bản ghi, nếu đã được duyệt thì không cho xóa
                    TtDangKyLogo obj = pro.GetByDangKyLoGoId(Library.Int32Convert(idDanhSach));
                    if (obj != null && obj.TinhTrangId == Library.Int32Convert(idTrangThai))
                    {
                        maDanhSachKoXoaDuoc += obj.TenVietTat + ", ";
                        continue;
                    }

                    if (pro.Delete(Library.Int32Convert(idDanhSach)))
                    {
                        cm.ghilog(ListName.Func_QLHV_DSCongTyDangKyDangLogo, "Xóa bản ghi có ID \"" + idDanhSach + "\" của danh mục " + tenchucnang);
                    }
                }
            }
            maDanhSachKoXoaDuoc = maDanhSachKoXoaDuoc.Trim().TrimEnd(',');
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã xóa những bản ghi dữ liệu hợp lệ.";
            if (!string.IsNullOrEmpty(maDanhSachKoXoaDuoc))
                msg += "<br /><span style=\"color:red;\">Không được phép xóa đăng ký của các công ty: " + maDanhSachKoXoaDuoc + ".</span>";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("DeleteSuccess", js);
            LoadData();
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Xóa những bản ghi được chọn khi bấm nút "Xóa đồng loạt" ở trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTy.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTy.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        Delete(listIdDanhSach);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Duyệt những bản ghi được chọn khi bấm nút "Duyệt đồng loạt" ở phía trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTy.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTy.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        ChangeStatus(listIdDanhSach, ListName.Status_DaPheDuyet, "duyệt");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Từ chối những bản ghi được chọn khi bấm nút "Từ chối đồng loạt ở phía trên"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReject_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTy.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTy.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        ChangeStatus(listIdDanhSach, ListName.Status_KhongPheDuyet, "từ chối");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Từ chối những bản ghi được chọn khi bấm nút "Thoái duyệt đồng loạt ở phía trên"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDontApprove_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTy.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTy.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        ChangeStatus(listIdDanhSach, ListName.Status_ThoaiDuyet, "thoái duyệt");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/31
    /// Cập nhật trạng thái của những bản ghi được chọn
    /// </summary>
    /// <param name="listId">List ID đoàn kiểm tra</param>
    /// <param name="statusCode">Mã trạng thái muốn cập nhật</param>
    /// <param name="action">Tên hành động (VD: duyệt, từ chối)</param>
    private void ChangeStatus(List<string> listId, string statusCode, string action)
    {
        try
        {
            _db.OpenConnection();
            string maDanhSachKoCapNhatDuoc = "";
            string idTrangThai_PheDuyet = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
            foreach (string idDoan in listId)
            {
                if (!string.IsNullOrEmpty(idDoan))
                {
                    SqlTtDangKyLogoProvider pro = new SqlTtDangKyLogoProvider(ListName.ConnectionString, false, string.Empty);
                    SqlTtPhatSinhPhiProvider proPhatSinhPhi = new SqlTtPhatSinhPhiProvider(ListName.ConnectionString, false, string.Empty);
                    TtDangKyLogo obj = new TtDangKyLogo();
                    obj = pro.GetByDangKyLoGoId(Library.Int32Convert(idDoan));
                    // Kiểm tra tình trạng của bản ghi, nếu đã được duyệt thì không cho xử lý nữa
                    if (statusCode == ListName.Status_KhongPheDuyet || statusCode == ListName.Status_ChoDuyet || statusCode == ListName.Status_DaPheDuyet)
                        if (obj != null && obj.TinhTrangId == Library.Int32Convert(idTrangThai_PheDuyet))
                        {
                            maDanhSachKoCapNhatDuoc += obj.TenVietTat + ", ";
                            continue;
                        }
                    if (statusCode == ListName.Status_ThoaiDuyet)
                        if (obj != null && obj.TinhTrangId != Library.Int32Convert(idTrangThai_PheDuyet))
                        {
                            maDanhSachKoCapNhatDuoc += obj.TenVietTat + ", ";
                            continue;
                        }
                    obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(statusCode, _db));
                    if (pro.Update(obj))
                    {
                        // Nếu là duyệt -> insert phát sinh phí
                        if (statusCode == ListName.Status_DaPheDuyet && (obj.DoiTuong == "3" || obj.DoiTuong == "4"))
                        {
                            TtPhatSinhPhi objPsp = new TtPhatSinhPhi();
                            objPsp.HoiVienId = obj.HoiVienTapTheId;
                            objPsp.LoaiHoiVien = obj.DoiTuong;
                            objPsp.DoiTuongPhatSinhPhiId = obj.DangKyLoGoId;
                            objPsp.LoaiPhi = "2";
                            objPsp.NgayPhatSinh = DateTime.Now;
                            objPsp.SoTien = obj.SoTien;
                            objPsp.PhiId = obj.PhiId;
                            proPhatSinhPhi.Insert(objPsp);
                        }
                        // Nếu là thoái duyệt -> delete phát sinh phí
                        if (statusCode == ListName.Status_ThoaiDuyet && (obj.DoiTuong == "3" || obj.DoiTuong == "4"))
                            DeletePhatSinhPhi(obj.DangKyLoGoId.ToString());
                        cm.ghilog(ListName.Func_QLHV_DSCongTyDangKyDangLogo, "Cập nhật trạng thái bản ghi có ID \"" + obj.DangKyLoGoId + "\" thành " + utility.GetStatusNameById(statusCode, _db) + " của danh mục " + tenchucnang);
                    }
                }
            }
            maDanhSachKoCapNhatDuoc = maDanhSachKoCapNhatDuoc.Trim().TrimEnd(',');
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã " + action + " những bản ghi dữ liệu hợp lệ.";
            if (!string.IsNullOrEmpty(maDanhSachKoCapNhatDuoc))
                msg += "<br /><span style=\"color:red;\">Không được phép " + action + " thông tin đăng ký với công ty: " + maDanhSachKoCapNhatDuoc + ".</span>";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("ApproveSuccess", js);
            LoadData();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void DeletePhatSinhPhi(string id)
    {
        string command = "DELETE FROM tblTTPhatSinhPhi WHERE LopHocPhiID = " + id + " AND LoaiPhi = '2'";
        _db.ExecuteNonQuery(command);
    }
}