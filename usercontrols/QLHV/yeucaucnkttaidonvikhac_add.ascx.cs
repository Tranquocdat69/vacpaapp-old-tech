﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Data;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_QLHV_yeucaucnkttaidonvikhac_add : System.Web.UI.UserControl
{

    protected string tenchucnang = "Cập nhật số giờ CNKT tại đơn vị khác";
    protected string _listPermissionOnFunc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    Commons cm = new Commons();
    private string _id = "";
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-book'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
            _listPermissionOnFunc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr);
            if (!_listPermissionOnFunc.Contains("XEM|"))
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
                form_CreateLopHoc.Visible = false;
                return;
            }
            if (Session["MsgError"] != null)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
                Session.Remove("MsgError");
            }
            if (Session["MsgSuccess"] != null)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + Session["MsgSuccess"] + "</div>"));
                Session.Remove("MsgSuccess");
            }
            _db.OpenConnection();
            this._id = Request.QueryString["id"];
            string action = Request.Form["hdAction"];
            if (action == "save")
            {
                Save();
            }
            if (action == "del")
                Delete();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc.Contains("SUA|") && !_listPermissionOnFunc.Contains("THEM|"))
        {
            Response.Write("$('#btn_save1').remove();");
            Response.Write("$('#btn_save2').remove();");
        }

        if (!_listPermissionOnFunc.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");
    }

    protected void LoadOldData()
    {
        try
        {
            _db.OpenConnection();

            string js = "";
            if (!string.IsNullOrEmpty(_id))
            {
                string query = @"SELECT HVYeuCauCNKTID, IDYeuCau, TenLopHoc, TuNgay, DenNgay, DonViToChuc, TenFileBangChung, TinhTrang, MaHoiVienCaNhan, MaTrangThai
                                FROM tblHVYeuCauCNKT YC_CNKT INNER JOIN tblHoiVienCaNhan HVCN ON YC_CNKT.HoiVienCaNhanID = HVCN.HoiVienCaNhanID
                                INNER JOIN tblDMTrangThai TT ON YC_CNKT.TinhTrang = TT.TrangThaiID
                                WHERE HVYeuCauCNKTID = " + this._id;
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    Hashtable ht = listData[0];
                    js += "iframeProcess_ChuyenDe.location = '/iframe.aspx?page=QLHV_Process&idyc=" + _id + "&action=loadcd';" + Environment.NewLine;
                    js += "$('#txtIDYeuCau').val('" + ht["IDYeuCau"] + "');" + Environment.NewLine;
                    js += "$('#txtMaHocVien').val('" + ht["MaHoiVienCaNhan"] + "');" + Environment.NewLine;
                    js += "$('#txtMaHocVien').prop('readonly', true);" + Environment.NewLine;
                    js += "$('#btnOpenFormChonHocVien').remove();" + Environment.NewLine;
                    js += "CallActionGetInforHocVien();" + Environment.NewLine;
                    js += "$('#txtTenLopHoc').val('" + ht["TenLopHoc"] + "');" + Environment.NewLine;
                    js += "$('#txtTuNgay').val('" + Library.DateTimeConvert(ht["TuNgay"]).ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#txtDenNgay').val('" + Library.DateTimeConvert(ht["DenNgay"]).ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#txtDonViToChuc').val('" + ht["DonViToChuc"] + "');" + Environment.NewLine;
                    js += "$('#spanTenFile').html('" + ht["TenFileBangChung"] + "');" + Environment.NewLine;
                    js += "$('#btnDownload').prop('href', '/admin.aspx?page=getfile&id=" + _id + "&type=tblHVYeuCauCNKT');" + Environment.NewLine;
                    js += "$('#btnDownload').html('Tải về');" + Environment.NewLine;

                    string maTrangThai = ht["MaTrangThai"].ToString();
                    if (!string.IsNullOrEmpty(maTrangThai))
                    {
                        if (maTrangThai.Trim() == ListName.Status_KhongPheDuyet || maTrangThai == ListName.Status_ThoaiDuyet)
                        {

                        }
                        if (maTrangThai.Trim() == ListName.Status_TuChoi)
                        {

                        }
                        if (maTrangThai.Trim() == ListName.Status_DaPheDuyet)
                        {
                            js += "$('#btn_save1').remove();" + Environment.NewLine;
                            js += "$('#btn_save2').remove();" + Environment.NewLine;
                            js += "$('#btnDelete').remove();" + Environment.NewLine;
                            js += "$('#btnThemCD').remove();" + Environment.NewLine;
                        }
                    }

                    Response.Write(js);
                }
            }
            else
            {
                js += "$('#spanCacNutChucNang').remove();" + Environment.NewLine;
                Response.Write(js);
            }
        }
        catch { _db.CloseConnection(); }
        finally { _db.CloseConnection(); }
    }

    private void Save()
    {
        string statusDuyet = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
        string isComeBack = Request.Form["hdIsComeBack"];
        string hoiVienId = Request.Form["hdHocVienID"];
        string tenLopHoc = Request.Form["txtTenLopHoc"];
        string tuNgay = Request.Form["txtTuNgay"];
        string denNgay = Request.Form["txtDenNgay"];
        string donViToChuc = Request.Form["txtDonViToChuc"];
        HttpPostedFile file = Request.Files["fileBangChung"];
        SqlHvYeuCauCnktProvider pro = new SqlHvYeuCauCnktProvider(ListName.ConnectionString, false, string.Empty);
        SqlHvYeuCauCnktcdProvider pro_cd = new SqlHvYeuCauCnktcdProvider(ListName.ConnectionString, false, string.Empty);
        HvYeuCauCnkt obj = new HvYeuCauCnkt();
        if (string.IsNullOrEmpty(this._id)) // Insert
        {
            obj.HoiVienCaNhanId = Library.Int32Convert(hoiVienId);
            List<Hashtable> listData = _db.GetListData("SELECT dbo.LaySoChayYeuCauCNKT('" + DateTime.Now.ToString("yy") + "', '" + DateTime.Now.ToString("MM") + "') AS temp");
            if (listData.Count > 0)
                obj.IdYeuCau = listData[0]["temp"].ToString();
            obj.TenLopHoc = tenLopHoc;
            obj.TuNgay = Library.DateTimeConvert(tuNgay, "dd/MM/yyyy");
            obj.DenNgay = Library.DateTimeConvert(denNgay, "dd/MM/yyyy");
            obj.DonViToChuc = donViToChuc;
            obj.TinhTrang = Library.Int32Convert(statusDuyet);
            if (file.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(file.InputStream);
                byte[] fileByte = br.ReadBytes(file.ContentLength);
                obj.FileBangChung = fileByte;
                obj.TenFileBangChung = file.FileName;
            }
            if (pro.Insert(obj))
            {
                SaveListChuyenDe(obj.HvYeuCauCnktid.ToString());
                cm.ghilog("CapNhatHoSoHoiVien", "Cập nhật giá trị \"" + tenLopHoc + "\" vào danh mục " + tenchucnang);
                Session["MsgSuccess"] = "Cập nhật thông tin thành công!";
                if (isComeBack == "1")
                    Response.Redirect("/admin.aspx?page=yeucaucnkttaidonvikhac");
                else
                    Response.Redirect("/admin.aspx?page=yeucaucnkttaidonvikhac_add");
            }
        }
        else // save
        {
            obj = pro.GetByHvYeuCauCnktid(Library.Int32Convert(this._id));
            obj.TenLopHoc = tenLopHoc;
            obj.TuNgay = Library.DateTimeConvert(tuNgay, "dd/MM/yyyy");
            obj.DenNgay = Library.DateTimeConvert(denNgay, "dd/MM/yyyy");
            obj.DonViToChuc = donViToChuc;
            if (file.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(file.InputStream);
                byte[] fileByte = br.ReadBytes(file.ContentLength);
                obj.FileBangChung = fileByte;
                obj.TenFileBangChung = file.FileName;
            }
            if (pro.Update(obj))
            {
                SaveListChuyenDe(obj.HvYeuCauCnktid.ToString());
                cm.ghilog("CapNhatHoSoHoiVien", "Cập nhật giá trị \"" + tenLopHoc + "\" vào danh mục " + tenchucnang);
                Session["MsgSuccess"] = "Cập nhật thông tin thành công!";
                if (isComeBack == "1")
                    Response.Redirect("/admin.aspx?page=yeucaucnkttaidonvikhac");
                else
                    Response.Redirect("/admin.aspx?page=yeucaucnkttaidonvikhac_add");
            }
        }
    }

    private void SaveListChuyenDe(string idYeuCauCnkt)
    {
        // Lấy danh sách ID chuyên đề -> để so sánh với danh sách ID khi update tìm ra những ID bị xóa
        List<string> listIdDangLuu = new List<string>(); // Danh sách những ID chuyên đề sẽ bị xóa sau khi kết thúc tác vụ
        string query = "SELECT HVYeuCauCNKTCDID FROM tblHVYeuCauCNKTCD WHERE HVYeuCauCNKTID = " + idYeuCauCnkt;
        List<Hashtable> listData = _db.GetListData(query);
        foreach (Hashtable ht in listData)
        {
            listIdDangLuu.Add(ht["HVYeuCauCNKTCDID"].ToString());
        }

        Commons cm = new Commons();
        SqlHvYeuCauCnktcdProvider pro_cd = new SqlHvYeuCauCnktcdProvider(ListName.ConnectionString, false, string.Empty);
        HvYeuCauCnktcd obj_cd = new HvYeuCauCnktcd();

        NameValueCollection nvc = Request.Form;
        string[] arrKeys = nvc.AllKeys;
        for (int i = 0; i < arrKeys.Length; i++)
        {
            string key = arrKeys[i];
            if (key.StartsWith("hfValueChuyenDe"))
            {
                string fullValue = Request.Form[key];
                if (string.IsNullOrEmpty(fullValue))
                    continue;
                string[] arrFullValue = fullValue.Split(new string[] { ";#" }, StringSplitOptions.None);

                string chuyenDeId = Library.GetValueFromArray(arrFullValue, 0);
                string loaiChuyenDe = Library.GetValueFromArray(arrFullValue, 1);
                string tenChuyenDe = Library.GetValueFromArray(arrFullValue, 2);
                string soGioHoc = Library.GetValueFromArray(arrFullValue, 3);

                if (!string.IsNullOrEmpty(loaiChuyenDe))
                {
                    if (!string.IsNullOrEmpty(chuyenDeId)) // Update
                    {
                        if (Library.CheckIsInt32(chuyenDeId.Split(',')[0]))
                        {
                            listIdDangLuu.Remove(chuyenDeId.Split(',')[0]); // Loại bỏ những ID chuyên đề không bị xóa khỏi danh sách
                            obj_cd = pro_cd.GetByHvYeuCauCnktcdid(Library.Int32Convert(chuyenDeId.Split(',')[0]));
                        }
                    }
                    else //Insert
                    {
                        obj_cd = new HvYeuCauCnktcd();
                        // Gen Ma Chuyen De
                        // QUY TAC: [Ma lop hoc] + [Loai chuyen de (KT, DD, KH)] + [01 - 99]
                        if (Library.CheckIsInt32(idYeuCauCnkt))
                            obj_cd.HvYeuCauCnktid = Library.Int32Convert(idYeuCauCnkt);
                    }
                    obj_cd.LoaiChuyenDe = loaiChuyenDe;
                    obj_cd.TenChuyenDe = tenChuyenDe;
                    obj_cd.SoGioHoc = Library.DecimalConvert(soGioHoc);
                    if (string.IsNullOrEmpty(chuyenDeId)) //Insert
                    {
                        if (pro_cd.Insert(obj_cd))
                        {
                            cm.ghilog("CapNhatHoSoHoiVien", "Thêm giá trị \"" + tenChuyenDe + "\" vào danh mục hội viên yêu cầu cập nhật kiến thức - chuyên đề");
                        }
                    }
                    else //Update
                    {
                        if (pro_cd.Update(obj_cd))
                        {
                            cm.ghilog("CapNhatHoSoHoiVien", "Cập nhật giá trị \"" + tenChuyenDe + "\" vào danh mục hội viên yêu cầu cập nhật kiến thức - chuyên đề");
                        }
                    }
                }
            }
        }

        // Xóa những chuyên đề theo list ID bị xóa sau khi update
        foreach (string id in listIdDangLuu)
        {
            if (pro_cd.Delete(Library.Int32Convert(id)))
            {
                cm.ghilog("CapNhatHoSoHoiVien", "Xóa bản ghi có ID \"" + id + "\" của danh mục hội viên yêu cầu cập nhật kiến thức - chuyên đề");
            }
        }
    }

    private void Delete()
    {
        if (_db.ExecuteNonQuery("DELETE FROM tblHVYeuCauCNKTCD WHERE HVYeuCauCNKTID = " + _id) > 0)
        {
            if (_db.ExecuteNonQuery("DELETE FROM tblHVYeuCauCNKT WHERE HVYeuCauCNKTID = " + _id) > 0)
            {
                cm.ghilog("CapNhatHoSoHoiVien", "Đã xóa bản ghi có ID \"" + _id + "\" của danh mục " + tenchucnang);
                Session["MsgSuccess"] = "Đã xóa thông tin số giờ CNKT tại đơn vị khác!";
                Response.Redirect("/admin.aspx?page=yeucaucnkttaidonvikhac");
            }
        }
    }
}