﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ketnapchinhanh.ascx.cs" Inherits="usercontrols_QLHV_ketnapchinhanh" %>


<style type="text/css">
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>
 <style type="text/css">
    .Hide
    {
      display:none;
    }
     .style1
     {
         height: 42px;
     }
 </style>

   
   <script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
   <script type="text/javascript" src="js/mask.js"></script>
<script type="text/javascript">


    jQuery(function ($) {

        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });

    function reCalljScript() {



        $("#<%=txtNgayThanhLap.ClientID%> , #<%=txtNgayCapGiayChungNhanDKKD.ClientID%> ,#<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%> ,#<%=txtNguoiDaiDienPL_NgayCapChungChiKTV.ClientID%> ,#<%=txtNguoiDaiDienLL_NgaySinh.ClientID%> ,#<%=txtNguoiDaiDienPL_NgaySinh.ClientID%>").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
        }).datepicker("option", "maxDate", '+0m +0w +0d');

        $("#<%=txtNgayThanhLap.ClientID%> , #<%=txtNgayCapGiayChungNhanDKKD.ClientID%> ,#<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%> ,#<%=txtNguoiDaiDienPL_NgayCapChungChiKTV.ClientID%> ,#<%=txtNguoiDaiDienLL_NgaySinh.ClientID%> ,#<%=txtNguoiDaiDienPL_NgaySinh.ClientID%>").mask("99/99/9999");

    }




</script>



<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<% 
    if (!String.IsNullOrEmpty(cm.Admin_NguoiDungID))
    {
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr).Contains("THEM|"))
        {
            Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");
            return;
        }
    }
    
    %>
    <form id="form_account_add"   runat="server"    >
 <div class="widgetbox">
    <h4 id="lbKetNap" class="widgettitle" runat="server">
       Đơn xin gia nhập chi nhánh
        <a class="close">×</a>
        <a class="minimize">–</a></h4>
    <div class="widgetcontent">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <div id="thongbaoloi_form_themuser" name="thongbaoloi_form_themuser" style="display:none" class="alert alert-error"></div>

     
			 
	 	<div class="writecomment">
            <asp:PlaceHolder ID="placeMessage" runat="server"></asp:PlaceHolder>
	  
            <div class="widget2">
         
                  <div class="menu_body">
            
            <div class="formRow">
            <asp:UpdatePanel ID="UpdatePanelThongTinDN" runat="server" ><ContentTemplate>
            
             <script type="text/javascript" language="javascript">

                 Sys.Application.add_load(reCalljScript);
             </script>
     

                <table width="100%" class="formtbl"  >
                <tr class="trbgr">
                 <td colspan="4">
                  <h6>Thông tin chi nhánh</h6>
                  </td>
                </tr>
                    <tr class="formRowTable">
                     <td width="135"><label>Loại hội viên:</label></td>
                    <td>
                      <div class="formRight">
                    <asp:DropDownList ID="drLoaiHoiVien" runat="server" Width="100%" Height="30px">
                        <asp:ListItem Text="<< Chọn >>" Value="-1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Hội viên chính thức" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Hội viên liên kết" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                       </div>
                    </td>   
                       <td colspan="2"></td>
                    </tr>
                    <tr class="formRowTable" id="trHoiVienCha" runat="server">
                     <td width="135"><label>Tên công ty</label><span class="require">(*)</span></td>
                    <td colspan="3">
                      <div class="formRight">
                    <asp:DropDownList ID="drHoiVienTapThe"  DataTextField="TenDoanhNghiep" 
                              DataValueField="HoiVienTapTheID" runat="server" Width="100%" Height="30px"></asp:DropDownList>
                       </div>
                    </td>   
                                  
                    </tr>
                    
                    <tr class="formRowTable" >
                        <td width="135"><label>Tên chi nhánh</label><span class="require">(*)</span></td>
                    <td colspan="3">
                      <div class="formRight">
                     <asp:TextBox ID="txtTenDoanhNghiep" runat="server" Width="100%"></asp:TextBox>
                     <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="reqTenDoanhNghiep" runat="server" 
                          ControlToValidate="txtTenDoanhNghiep" SetFocusOnError="true" 
                            ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin tên doanh nghiệp.">
                     </asp:RequiredFieldValidator>--%>
                     </div>
                    </td>   
                              
                    </tr>  
                       
                    
                    
                    <tr class="formRowTable">
                     <td width="135"><label>Địa chỉ chi nhánh</label><span class="require">(*)</span></td>
                    <td colspan="3">
                      <div class="formRight">
                    <asp:TextBox ID="txtDiaChi" runat="server" Width="100%"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDiaChi" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin địa chỉ."></asp:RequiredFieldValidator>--%>
                       </div>
                    </td>   
                                  
                    </tr>
           
           
      
                             
                    <tr class="formRowTable">
                        <td width="135"><label>Tỉnh/Thành</label><span class="require">(*)</span></td>
                    <td>
                      <div class="formRight">
                        <asp:DropDownList ID="drTinhThanh"  DataTextField="TenTinh" 
                              DataValueField="MaTinh" runat="server" Width="228px" Height="30px"  AutoPostBack="true" 
                              onselectedindexchanged="drTinhThanh_SelectedIndexChanged"></asp:DropDownList>
                        <%--<asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="00"     ID="RequiredFieldValidatordrTinhThanh" runat="server" ControlToValidate="drTinhThanh" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin tỉnh thành."></asp:RequiredFieldValidator>--%>
                  
               
                       </div>
                    </td>   
                    <td width="135"><label>Quận/Huyện</label></td>
                    <td>
                      <div class="formRight">
                        <asp:DropDownList ID="drQuanHuyen"  DataTextField="TenHuyen" 
                              DataValueField="MaHuyen" runat="server" Width="228px" Height="30px" 
                        AutoPostBack="true" onselectedindexchanged="drQuanHuyen_SelectedIndexChanged"  ></asp:DropDownList>

                 <%--   <select name="HuyenID_DiaChi" id="HuyenID_DiaChi" style="width:230px;height:30px;">
                         <%  
              
                              cm.Load_QuanHuyen();
              
                          %>
                      </select>--%>
                     <%-- <asp:Label ID="Label6" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      </div>
                    </td>              
                    </tr>

                    <tr class="formRowTable">
                          <td width="135"><label>Phường/Xã</label></td>
                    <td style="text-align:left;">
                      <div class="formRight">
                        <asp:DropDownList ID="drPhuongXa"  DataTextField="TenXa" DataValueField="MaXa" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                  <%--  <select name="XaID_DiaChi" id="XaID_DiaChi" style="width:230px;height:30px;">
             <%  
              
                  cm.Load_PhuongXa();
              
              %></select>--%>
                    <%-- <asp:Label ID="Label7" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      </div>
                   </td>   
                     
                     <td width="135"><label>Ngày thành lập</label></td>
                    <td>
                      <div class="formRight">
                      <asp:TextBox ID="txtNgayThanhLap" runat="server" Width="100%"></asp:TextBox>
                      <asp:RangeValidator ID="RangeValidatortxtNgayThanhLap" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="01/01/9999"
                  ControlToValidate ="txtNgayThanhLap"  SetFocusOnError="true" ></asp:RangeValidator>

                   <%--   <asp:Label ID="Label8" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      </div>
                    </td>
                    </tr>

       


                    <tr class="formRowTable" >
                    <td width="135"><label>Điện thoại</label><span class="require">(*)</span></td>
                    <td>
                      <div class="formRight">
                     <asp:TextBox ID="txtDienThoai" runat="server" Width="100%" onkeypress="return NumberOnly()"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtDienThoai" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin điện thoại."></asp:RequiredFieldValidator>--%>
                       </div>
                    </td>   
                    <td width="135"><label>Email</label><span class="require">(*)</span></td>
                    <td>
                       <div class="formRight">
                    <asp:TextBox ID="txtEmail" runat="server" Width="100%"  ></asp:TextBox>
                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator10" runat="server" 
                          ControlToValidate="txtEmail" SetFocusOnError="true" 
                            ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin Email.">
                    </asp:RequiredFieldValidator>--%>
                      </div>
                    </td>              
                    </tr>

                    <tr class="formRowTable">
                        <td width="135"><label>Fax</label><span class="require">(*)</span></td>
                    <td>
                        <div class="formRight">

                    <asp:TextBox ID="txtFax" runat="server" Width="100%" onkeypress="return NumberOnly()"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator11" runat="server" 
                           ControlToValidate="txtFax" SetFocusOnError="true"
                             ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số Fax.">
                    </asp:RequiredFieldValidator>--%>
                     </div>
                    </td>   
                    <td width="135"><label>Website</label><%--<span class="require">(*)</span>--%></td>
                    <td>
                      <div class="formRight">
                    <asp:TextBox ID="txtWebsite" runat="server" Width="100%"  ></asp:TextBox>
                    <%--<asp:Label ID="Label9" runat="server" Text="" Height="13px" > </asp:Label>--%>
                  <%--  <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtWebsite" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin Website."></asp:RequiredFieldValidator>--%>
                     </div>
                    </td>              
                    </tr>
 
                    <tr class="formRowTable">
                    <td width="135"><label>Số giấy chứng nhận ĐKKD</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                      
                     <asp:TextBox ID="txtSoGiayChungNhanDKKD" runat="server" Width="100%"></asp:TextBox>
                     <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator12" runat="server" 
                             ControlToValidate="txtSoGiayChungNhanDKKD" SetFocusOnError="true" 
                               ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số giấy chứng nhận .">
                       </asp:RequiredFieldValidator>--%>
                      </div>
                    </td>  
                    <td width="135"><label>Ngày cấp</label><span class="require">(*)</span></td>
                    <td>
                     
                     <div class="formRight">
                      <asp:TextBox ID="txtNgayCapGiayChungNhanDKKD" runat="server" Width="100%"></asp:TextBox>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator13" runat="server"
                       ControlToValidate="txtNgayCapGiayChungNhanDKKD" SetFocusOnError="true"
                          ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày cấp"></asp:RequiredFieldValidator>--%>
                     
                         <asp:RangeValidator ID="RangeValidatortxtNgayCapGiayChungNhanDKKD" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="01/01/9999"
                  ControlToValidate ="txtNgayCapGiayChungNhanDKKD"  SetFocusOnError="true" ></asp:RangeValidator>


                       </div>
                    
                  </td>              
                    </tr>

                    <tr class="formRowTable">
                    <td width="135"><label>Số giấy chứng nhận đủ điều kiện KDDV Kiểm toán</label></td>
                
                    <td>
                     <div class="formRight">
                      <asp:TextBox ID="txtSoGiayChungNhanKDDVKT" runat="server" Width="100%"></asp:TextBox>
                     
                     
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtNgayCapGiayChungNhanDKKD" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage=""></asp:RequiredFieldValidator>--%>
                      </div>
                  </td>              
                
                       <td width="135"><label>Ngày cấp</label></td>
                    <td> 
                     <div class="formRight">
                    <asp:TextBox ID="txtNgayCapGiayChungNhanKDDVKT" runat="server" Width="100%"></asp:TextBox>
                         <asp:RangeValidator ID="RangeValidatortxtNgayCapGiayChungNhanKDDVKT" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="01/01/9999"
                  ControlToValidate ="txtNgayCapGiayChungNhanKDDVKT"  SetFocusOnError="true" ></asp:RangeValidator>

                   <%-- <asp:Label ID="Label10" runat="server" Text="" Height="13px" > </asp:Label>--%>
                    </div>
                    
                    </td>  

                
                    </tr>

                    <tr class="formRowTable">
                     
                    <td width="135"><label>Mã số thuế</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                    <asp:TextBox ID="txtMaSoThue" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtMaSoThue" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin mã số thuế."></asp:RequiredFieldValidator>--%>
                      </div>
                    </td>  
                


             
                    <td width="135"><label>Lĩnh vực hoạt động</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                      <asp:DropDownList ID="drLinhvucHoatDong"  DataTextField="TenLinhVucHoatDong" DataValueField="LinhVucHoatDongID" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                     <%--<asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="0"     ID="RequiredFieldValidator15" runat="server" ControlToValidate="drLinhvucHoatDong" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin lĩnh vực hoạt động."></asp:RequiredFieldValidator>--%>
                      </div>
                    </td>              
                    </tr>

                    <tr align="left" >
                     <td colspan="4"  style=" text-align:left">
                    <%-- <asp:Label ID="Label21" runat="server" Text="" Height="13px" > </asp:Label>
                     <br />--%>
                     <label   style="  font-weight: bold;  ">Công ty đủ điều kiện kiểm toán đơn vị:  </label>
                     <%--<br /> <asp:Label ID="Label14" runat="server" Text="" Height="13px" > </asp:Label>--%>
                    
                     </td>
                    </tr>
                   
                    <tr class="formRowTable" >
                     <td ><label>Có lợi ích công chúng năm </label></td>
                     <td >
                      
                      <div class="formRight">
                         <asp:TextBox ID="txtNamCoLoiIchCongChung" runat="server" Width="30%" 
                          class="auto" data-a-sep=""  data-a-dec="," data-v-min="0" data-v-max="9999" 
                              MaxLength="4"  ></asp:TextBox>
                        <%--  <asp:Label ID="Label11" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      </div>
                      </td>   
                     <td   ><label>Trong lĩnh vực chứng khoán năm </label></td>   
                     <td>
                        <div class="formRight">
                         <asp:TextBox ID="txtNamTrongLinhVucChungKhoan" runat="server" Width="30%"
                          class="auto" data-a-sep=""  data-a-dec="," data-v-min="0" data-v-max="9999" 
                                MaxLength="4" ></asp:TextBox>
                        <%-- <asp:Label ID="Label12" runat="server" Text="" Height="13px" > </asp:Label>--%>
                        </div>
                     </td>   

                    </tr>



                    <tr class="formRowTable" >
                        <td width="135"><label>Công ty là thành viên hãng kiểm toán quốc tế </label><%--<span class="require">(*)</span>--%></td>
                        <td colspan="3">
                         <div class="formRight">
                         <asp:TextBox ID="txtThanhVienHangKTQT" runat="server" Width="100%"></asp:TextBox>
                         <%--<asp:Label ID="Label13" runat="server" Text="" Height="13px" > </asp:Label>--%>
                        <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtThanhVienHangKTQT" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin địa chỉ."></asp:RequiredFieldValidator>--%>
                        </div>
                        </td>   
                                  
                    </tr>

                    <tr class="formRowTable">
                     <td width="100"><label>Tổng số người làm việc tại doanh nghiệp </label></td>   
                    <td>
                       <div class="formRight">
                       <asp:TextBox ID="txtTongSoNguoiLamViec" runat="server" Width="30%"
                        class="auto" data-a-sep="."  data-a-dec="," data-v-max="9999" ></asp:TextBox>
                     <%--  <asp:Label ID="Label15" runat="server" Text="" Height="13px" > </asp:Label>--%>
                       </div>
                       </td>   
                     <td width="100"><label>Tổng số người có chứng chỉ KTV</label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtTongSoNguoiCoCKKTV" runat="server" Width="30%"
                     class="auto" data-a-sep="."  data-a-dec="," data-v-max="9999"      ></asp:TextBox>
                   <%--  <asp:Label ID="Label16" runat="server" Text="" Height="13px" > </asp:Label>--%>
                     </div>
                     </td>   

                    
                    </tr>

                    <tr class="formRowTable">
                     <td width="100"><label>Tổng số KTV đăng ký hành nghề </label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtTongSoKTVDKHN" runat="server" Width="30%"
                     class="auto" data-a-sep="."  data-a-dec="," data-v-max="9999" ></asp:TextBox>
                    <%-- <asp:Label ID="Label17" runat="server" Text="" Height="13px" > </asp:Label>--%>
                     </div>
                     
                     </td>   
                     <td width="100"><label>Tổng số hội viên cá nhân </label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtTongSoHoiVienCaNhan" runat="server" Width="30%"
                      class="auto" data-a-sep="."  data-a-dec="," data-v-max="9999" ></asp:TextBox>
                   <%--  <asp:Label ID="Label18" runat="server" Text="" Height="13px" > </asp:Label>--%>
                     </div>
                     </td>   

                    
                    </tr>


                </table> 
                
            </ContentTemplate></asp:UpdatePanel>  
                
         
            </div>
       
            </div>
            </div>
            <div class="widget2">
         
                  <div class="menu_body">
           
            <div class="formRow">
                <table width="100%"  class="formtbl" >
                 <tr class="trbgr">
                 <td colspan="4">
                <h6>Thông tin người đại diện pháp luật</h6>
                  </td>
                </tr>
                 <tr  class="formRowTable" >
                         <td width="135"><label>Họ và tên</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                       <asp:TextBox ID="txtNguoiDaiDienPL_Ten" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtNguoiDaiDienPL_Ten" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin họ tên."></asp:RequiredFieldValidator>--%>
                    </div>
                    </td>  

                    <td width="135"><label>Ngày sinh</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                       <asp:TextBox ID="txtNguoiDaiDienPL_NgaySinh" runat="server" Width="100%"></asp:TextBox>
                     
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator155" runat="server" 
                           ControlToValidate="txtNguoiDaiDienPL_NgaySinh" SetFocusOnError="true" 
                               ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày sinh">
                       </asp:RequiredFieldValidator>--%>
                        <asp:RangeValidator ID="RangeValidatortxtNguoiDaiDienPL_NgaySinh" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="01/01/9999"
                  ControlToValidate ="txtNguoiDaiDienPL_NgaySinh"  SetFocusOnError="true" ></asp:RangeValidator>

                       </div>
                  </td>              
                    </tr>



                  <tr  class="formRowTable" >
                   <td width="135"><label>Giới tính</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                      <asp:DropDownList ID="drNguoiDaiDienPL_GioiTinh" DataTextField="TenLoaiHinhDoanhNghiep" DataValueField="LoaiHinhDoanhNghiepID" runat="server" Width="228px" Height="30px">
                                <asp:ListItem Value="1" Text="Nam"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Nữ"></asp:ListItem>
                      </asp:DropDownList>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator24" runat="server" ControlToValidate="drNguoiDaiDienPL_GioiTinh" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin giới tính."></asp:RequiredFieldValidator>--%>
                 
                      </div>
                    </td>  
                    <td width="135"><label>Chức vụ</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                      <asp:DropDownList ID="drNguoiDaiDienPL_ChucVu"    DataTextField="TenChucVu" DataValueField="ChucVuID" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                     
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="0"    ID="RequiredFieldValidator18" runat="server" ControlToValidate="drNguoiDaiDienPL_ChucVu" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin chức vụ"></asp:RequiredFieldValidator>--%>
                      </div>
                  </td>              
                    </tr>


                   <tr  class="formRowTable" >
                   <td width="135"><label>Số chứng chỉ KTV</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtNguoiDaiDienPL_SoChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtNguoiDaiDienPL_SoChungChiKTV" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số chứng chỉ KTV."></asp:RequiredFieldValidator>--%>
                     </div>
                    </td>  
                    <td width="135"><label>Ngày cấp</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight"> 
                      <asp:TextBox ID="txtNguoiDaiDienPL_NgayCapChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator20" runat="server" 
                              ControlToValidate="txtNguoiDaiDienPL_NgayCapChungChiKTV" SetFocusOnError="true" 
                                ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày cấp chứng chỉ KTV">
                        </asp:RequiredFieldValidator>--%>
                          <asp:RangeValidator ID="RangeValidatortxtNguoiDaiDienPL_NgayCapChungChiKTV" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="01/01/9999"
                  ControlToValidate ="txtNguoiDaiDienPL_NgayCapChungChiKTV"  SetFocusOnError="true" ></asp:RangeValidator>

                        </div>
                  </td>              
                    </tr>


                    <tr  class="formRowTable" >
                   <td width="135"><label>Mobie</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtNguoiDaiDienPL_DiDong" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtNguoiDaiDienPL_DiDong" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số di động."></asp:RequiredFieldValidator>--%>
                     </div>
                    </td>  
                   
                       <td width="135"><label>Email</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtNguoiDaiDienPL_Email" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator23" runat="server" ControlToValidate="txtNguoiDaiDienPL_Email" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin địa chỉ Email."></asp:RequiredFieldValidator>--%>
                      </div>
                    </td>  
                    </tr>

                     <tr  class="formRowTable" >

                     <td width="135"><label>Điện thoại cố định</label><%--<span class="require">(*)</span>--%></td>
                    <td>
                     <div class="formRight">
                      <asp:TextBox ID="txtNguoiDaiDienPL_DienThoaiCD" runat="server" Width="100%"></asp:TextBox>
                      <%--<asp:Label ID="Label19" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtNguoiDaiDienPL_DienThoaiCD" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage=""></asp:RequiredFieldValidator>--%>
                     </div>
                    
                       </td>              
                  
              
                    <td width="135"> </td>
                    <td>

                  </td>              
                    </tr>

                </table> 
            </div>

       

            </div>
            </div>

            <div class="widget2">
         
                  <div class="menu_body">
        
            <div class="formRow">
                <table width="100%" class="formtbl"  >
                  <tr class="trbgr">
                 <td colspan="4">
                 <h6>Thông tin người đại diện liên lạc</h6>
                  </td>
                </tr>
               <tr  class="formRowTable" >
                         <td width="135"><label>Họ và tên</label><span class="require">(*)</span></td>
                    <td>
                       <asp:TextBox ID="txtNguoiDaiDienLL_Ten" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNguoiDaiDienLL_Ten" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin họ tên."></asp:RequiredFieldValidator>--%>
                 
                    </td>  
                    <td width="135"><label>Ngày sinh</label><span class="require">(*)</span></td>
                    <td>
                       <asp:TextBox ID="txtNguoiDaiDienLL_NgaySinh" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator2" runat="server"
                               ControlToValidate="txtNguoiDaiDienLL_NgaySinh" SetFocusOnError="true"
                                  ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày sinh">
                       </asp:RequiredFieldValidator>--%>
                        <asp:RangeValidator ID="RangeValidatortxtNguoiDaiDienLL_NgaySinh" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="01/01/9999"
                  ControlToValidate ="txtNguoiDaiDienLL_NgaySinh"  SetFocusOnError="true" ></asp:RangeValidator>

                  </td>              
                    </tr>



                      <tr class="formRowTable">
                   <td width="135"><label>Giới tính</label><span class="require">(*)</span></td>
                    <td>
                      <asp:DropDownList ID="drNguoiDaiDienLL_GioiTinh" DataTextField="TenLoaiHinhDoanhNghiep" DataValueField="LoaiHinhDoanhNghiepID" runat="server" Width="228px" Height="30px">
                                <asp:ListItem Value="1" Text="Nam"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Nữ"></asp:ListItem>
                      </asp:DropDownList>

                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator3" runat="server" ControlToValidate="drNguoiDaiDienLL_GioiTinh" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin giới tính."></asp:RequiredFieldValidator>--%>
                 
                  
                    </td>  
                    <td width="135"><label>Chức vụ</label><span class="require">(*)</span></td>
                    <td>
                      <asp:DropDownList ID="drNguoiDaiDienLL_ChucVu"     DataTextField="TenChucVu" DataValueField="ChucVuID" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                     
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="0"    ID="RequiredFieldValidator4" runat="server" ControlToValidate="drNguoiDaiDienLL_ChucVu" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin chức vụ"></asp:RequiredFieldValidator>--%>
                  </td>              
                    </tr>


                     <%--     <tr>
                   <td width="135"><label>Số chứng chỉ KTV</label><span class="require">(*)</span></td>
                    <td><asp:TextBox ID="txtNguoiDaiDienLL_SoChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                       <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtNguoiDaiDienLL_SoChungChiKTV" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số chứng chỉ KTV."></asp:RequiredFieldValidator>
                 
                    </td>  
                    <td width="135"><label>Ngày cấp</label><span class="require">(*)</span></td>
                    <td>
                     <asp:TextBox ID="txtNguoiDaiDienLL_NgayCapChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                        <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator7" runat="server" 
                                 ControlToValidate="txtNguoiDaiDienLL_NgayCapChungChiKTV" SetFocusOnError="true"
                                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày cấp chứng chỉ KTV">
                       </asp:RequiredFieldValidator>
                  </td>              
                    </tr>--%>


                          <tr class="formRowTable" >
                   <td width="135"><label>Mobie</label><span class="require">(*)</span></td>
                    <td><asp:TextBox ID="txtNguoiDaiDienLL_DiDong" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtNguoiDaiDienLL_DiDong" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số di động."></asp:RequiredFieldValidator>--%>
                 
                    </td>  
                  
                      <td width="135"><label>Email</label><span class="require">(*)</span></td>
                    <td><asp:TextBox ID="txtNguoiDaiDienLL_Email" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtNguoiDaiDienLL_Email" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin địa chỉ Email."></asp:RequiredFieldValidator>--%>
                 
                    </td>  
                  
                  
                  
                              
                    </tr>

                    <tr  class="formRowTable" >
                      <td width="135"><label>Điện thoại cố định</label><%--<span class="require">(*)</span>--%></td>
                    <td>
                     <asp:TextBox ID="txtNguoiDaiDienLL_DienThoaiCD" runat="server" Width="100%"></asp:TextBox>
                    
                 <%--<asp:Label ID="Label20" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtNguoiDaiDienLL_DienThoaiCD" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage=""></asp:RequiredFieldValidator>--%>
                  </td>  

               
                    <td width="135"> </td>
                    <td>

                  </td>              
                    </tr>

                </table> 
            </div>

       

            </div>
            </div>
     
            <div class="widget2">
         
             <div class="menu_body">
                

               <div class="formRow">
                <table width="100%" class="formtbl" >
                   <tr class="trbgr">
                 <td colspan="4">
                  <h6>Tải lên file xác nhận</h6>
                  </td>
                </tr>
                 <tr>
                    <td colspan="3" align="center"><em style="color:#F90; font-size:14px;">(File hồ sơ tải lên phải thuộc một trong các định dạng <em style="color:#Ff0000; font-weight:bold">.DOC, .DOCX, .PDF, .BMP, .GIF, .PNG, .JPG, .RAR, .ZIP</em> . Kích thước file tải lên tối đa <em style="color:#Ff0000; font-weight:bold">10MB</em>, chất lượng phải đảm bảo để người tiếp nhận hồ sơ có thể đọc được. <em style="color:#Ff0000; font-weight:bold">Tất cả các file hồ sơ phải được quét hoặc chụp từ bản gốc (kể cả Đơn, Giấy cam kết, Giấy giới thiệu,….)</em>.  Tổ chức nộp hồ sơ phải chịu trách nhiệm hoàn toàn trước pháp luật về tính chính xác của hồ sơ nộp trực tuyến với hồ sơ gốc.)</em></td>
                  </tr>

                <tr>
                <td>
                               
                   <asp:GridView   ID="FileDinhKem_grv" runat="server"  Width="100%"
                         AutoGenerateColumns="False" 
                         EnableModelValidation="True"    AlternatingRowStyle-BackColor="WhiteSmoke"  >
                  <Columns>
             
          
                <asp:TemplateField  HeaderText="   "  >
                <ItemTemplate>
               
               
                  <asp:Label ID="labTenloaiFile"  runat="server" Text= '<%# Eval("TenBieuMau") %>' />
               
            
                </ItemTemplate><HeaderStyle HorizontalAlign="Center"></HeaderStyle>
              <ItemStyle 
                        HorizontalAlign="Left"   /></asp:TemplateField>

                <asp:TemplateField HeaderText=' '    >
                  <ItemTemplate >
                  <table>
                 <tr  >
                     <td >
                     
                        <asp:Label ID="requred" runat="server" Text="(*)" ForeColor="Red"  ></asp:Label><asp:FileUpload ID="FileUp"   name="FileUp" runat="server" />
                       
                     </td>
                     </tr>
                      <tr  >
                       <td>
                    
                           <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                               ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Jj][Pp][Gg])|.*\.([Bb][Mm][Pp])|.*\.([pP][nN][gG])|.*\.([pO][dD][fF])|.*\.([rR][aA][rR])|.*\.([zZ][iI][pP])|.*\.(?:[dD][oO][cC][xX]?)$)"
                               ControlToValidate="FileUp" Display="Dynamic" 
                                 ErrorMessage="Sai định dạng tập tin đính kèm"  ValidationGroup="CheckFileExt"  SetFocusOnError="true" >
                                 </asp:RegularExpressionValidator>
                           <%--<asp:RequiredFieldValidator id="RequiredFieldFileUpload" runat="server"
                              ControlToValidate="FileUp" Display="Dynamic"
                              ErrorMessage="Nhập tệp đính kèm."  ValidationGroup="CheckFileExt"
                              ForeColor="Red"
                              SetFocusOnError="true">
                            </asp:RequiredFieldValidator>--%>
                         
                            </td>
                            
                            </tr></table></ItemTemplate><HeaderStyle HorizontalAlign="Left" />
                           

                <ItemStyle 
                        HorizontalAlign="Right"  ></ItemStyle>
                       
              </asp:TemplateField>
              
               

            <asp:TemplateField  HeaderText="   " >
              <ItemTemplate>
                  
                <asp:HyperLink ID="linkFileUpload"
                 NavigateUrl='<%# Eval("FileID","Download.ashx?mode=don&AttachFileID={0}") %>'   runat="server">
                    <div style="width: 200px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis">
             
                   <asp:Label ID="Label1"  runat="server" Text= '<%# Eval("TenFileDinhKem") %>' />
                   </div>
                </asp:HyperLink></ItemTemplate><HeaderStyle HorizontalAlign="Center"></HeaderStyle>
              <ItemStyle 
                        HorizontalAlign="Left"     /></asp:TemplateField>

 
             <asp:BoundField DataField="LoaiGiayToID" HeaderText=""  ItemStyle-CssClass="Hide" 
                  HeaderStyle-CssClass="Hide" >
                    <HeaderStyle CssClass="Hide"></HeaderStyle>
                    <ItemStyle CssClass="Hide"></ItemStyle>
              </asp:BoundField>
      
            <asp:BoundField DataField="FileID" HeaderText=""  ItemStyle-CssClass="Hide" 
                  HeaderStyle-CssClass="Hide" >
                  <HeaderStyle CssClass="Hide"></HeaderStyle>
                   <ItemStyle CssClass="Hide"></ItemStyle>
              </asp:BoundField>
    
             <asp:BoundField DataField="BATBUOC" HeaderText=""  ItemStyle-CssClass="Hide" 
                  HeaderStyle-CssClass="Hide" >
                    <HeaderStyle CssClass="Hide"></HeaderStyle>
                 <ItemStyle CssClass="Hide"></ItemStyle>
              </asp:BoundField>
       
            
             <asp:BoundField DataField="TenFileDinhKem" HeaderText=""  ItemStyle-CssClass="Hide" 
                  HeaderStyle-CssClass="Hide" >
                    <HeaderStyle CssClass="Hide"></HeaderStyle>
                 <ItemStyle CssClass="Hide"></ItemStyle>
              </asp:BoundField>
          </Columns>
       </asp:GridView>

    
                </td>
                </tr>
               
                </table> 
            
              </div>
                 
           

            </div>
         
             </div>

            <div class="widget2">
         
                   <div class="menu_body">
         
          <div class="formRow">
                <table width="100%" class="formtbl" >
                   <%--<tr class="trbgr">
                 <td colspan="4">
                   <h6>Câu hỏi bí mật</h6>
                  </td>
                </tr>
                    <tr class="formRowTable" >
                        <td width="135"><label>Câu hỏi bí mật:</label><span class="require">(*)</span></td><td style="text-align:left;"><asp:DropDownList ID="drCauHoi" runat="server" DataTextField="CauHoi" DataValueField="CauHoiBiMatID"  Width="100%" Height="30px"></asp:DropDownList></td>   
                         <td></td>      
                    </tr>
             
                    <tr class="formRowTable" >
                    <td width="135"><label>Câu trả lời bí mật:</label><span class="require">(*)</span></td><td>
                      <asp:TextBox ID="txtCauTraLoi" runat="server"  ></asp:TextBox><asp:RequiredFieldValidator  Display="Dynamic"    ID="reqCauTraLoi" runat="server" ControlToValidate="txtCauTraLoi" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Chưa nhập câu trả lời."></asp:RequiredFieldValidator></td>
                      <td>   </td>
                     
                    </tr>--%>


                 <tr   <% setDisplayLyDoxoa( ); %> > 
                      <td colspan="3"  >
                      <table width="100%">
                      
                           <tr class="trbgr">
                                    <td colspan="2">
                                     <h6>Lý do từ chối(Khi từ chối đơn)</h6>
                                     </td>
                            </tr>
                         <tr class="formRowTable" >
                         <td width="15%"><label>Lý do từ chối:</label><span class="require">(*)</span></td>
                         <td>
                            <asp:TextBox ID="txtLyDoTuChoi" runat="server" TextMode="MultiLine" Rows="4" ></asp:TextBox><%--<asp:RequiredFieldValidator  Display="Dynamic"   
                               ID="RequiredFieldtxtLyDoTuChoi" runat="server" ControlToValidate="txtLyDoTuChoi" SetFocusOnError="true" 
                                 ValidationGroup="CheckLyDoTuChoi"  ErrorMessage="Nhập lý do nếu từ chối đơn."></asp:RequiredFieldValidator>--%>
                          </td>
                     
                     
                    </tr>

                      </table>
                      </td>
                  </tr>
              
              <tr>
          <td class="trbgr" colspan="4" ><h6>Số quyết định/ngày quyết định
              </h6></td>
        </tr>
        <tr><td colspan="4">
            <table>
                <tr>
                    <td>Số quyết định:</td>
                    <td>
                    
                    <asp:TextBox ID="txtSoQuyetDinh" runat="server" Width="100%" ClientIDMode="Static"></asp:TextBox>
                    
                </td>
                <td>
                    Ngày quyết định:
                    
                </td>
                <td >
                    
                    <input name="NgayQD" type="text" id="NgayQD" runat="server" clientidmode="Static" />   
                    
                </td>
                </tr>
            </table>
        </td></tr>

                      <tr>
                      <td colspan="4" align="center">
                      <p>  &nbsp;</p>
                   
                       <asp:HyperLink ID="linkGhi"  runat="server"  class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnGhi" runat="server" CssClass="blueB" Text="Lưu" 
                           onclick="btnGhi_Click"  ValidationGroup="CheckFileExt"          />
                      </asp:HyperLink>

                       <asp:HyperLink ID="linkTuChoiDon"  runat="server"   class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnTuChoiDon" runat="server" CssClass="blueB" Text="Từ chối "
                           onclick="btnTuChoiDon_Click"  ValidationGroup="CheckLyDoTuChoi"          />
                       </asp:HyperLink>
                      <asp:HyperLink ID="linkTrinhPheDuyet"  runat="server"   class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnTrinhPheDuyet" runat="server" CssClass="blueB" Text="Tiếp nhận"
                           onclick="btnTrinhPheDuyet_Click"  ValidationGroup="NotCheck"          />
                      </asp:HyperLink>

                    
                       <asp:HyperLink ID="linkTuChoiDuyet"  runat="server"   class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnTuChoiDuyet" runat="server" CssClass="blueB" Text="Từ chối duyệt"
                           onclick="btnTuChoiDuyet_Click"   ValidationGroup="CheckLyDoTuChoi"         />
                      </asp:HyperLink>
                       <asp:HyperLink ID="linkPheDuyet"  runat="server"   class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnPheDuyet" runat="server" CssClass="blueB" Text="Phê duyệt" 
                           onclick="btnPheDuyet_Click"  ValidationGroup="PheDuyet" OnClientClick="return checkLoaiHoiVien();"         />
                       </asp:HyperLink>

                  <%--   <asp:HyperLink ID="linkThoaiDuyet"  runat="server"  class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnThoaiDuyet" runat="server" CssClass="blueB" Text="Thoái duyệt"
                           onclick="btnThoaiDuyetClick"  ValidationGroup="NotCheck"          />
                      </asp:HyperLink>--%>
                       <asp:HyperLink ID="linkXoaDon"  runat="server"   class="btn btn-rounded"><i class="iconfa-remove-sign"></i>
                          <asp:Button ID="btnXoaDon" runat="server" CssClass="blueB" Text="Xóa đơn"
                           onclick="btnXoaDon_Click"  ValidationGroup="NotCheck"          />
                      </asp:HyperLink>
                      <% if (objHoiVIenTapThe.TinhTrangId == 5)
                           { %>
                        <a class="btn btn-rounded"><i class="iconfa-pencil"></i>
                        <asp:Button ID="btnSuaQuyetDinh" Text="Sửa quyết định" runat="server" 
                  onclick="btnSuaQuyetDinh_Click" /> 
                    </a>
                    <% } %>
                          <a class="btn btn-rounded"><i class="iconsweets-word2"></i>
                        <asp:Button ID="btnKetXuatWord" Text="Kết xuất word" runat="server" 
                  onclick="btnKetXuatWord_Click" /> 
                    </a>
                    <a class="btn btn-rounded"><i class="iconsweets-pdf2"></i>
                        <asp:Button ID="btnKetXuatPDF" Text="Kết xuất pdf" runat="server" 
                  onclick="btnKetXuatPDF_Click" /> 
                    </a>
                          <a class="btn btn-rounded"><i class="iconsweets-word2"></i>
                        <asp:Button ID="btnKetXuat" Text="Kết xuất GCN" runat="server" OnClick="btnKetXuat_Click" />
                    </a>
                          <a class="btn btn-rounded"><i class="iconsweets-word2"></i>
                        <asp:Button ID="btnInQuyetDinh" Text="In quyết định" runat="server" OnClick="btnInQuyetDinh_Click" />
                    </a>
                      <a class="btn btn-rounded"><i class="iconfa-minus-sign"></i>
                        <input type="button" value="Quay lại" onclick="window.location.href = window.location.href.split('?')[0] + '?page=ketnaphoivien'" />
                       </a>
                  
                      <asp:HiddenField ID="hidId" runat="server"    />
                      <asp:HiddenField ID="hidTrangthaiId" runat="server" />
                      <asp:HiddenField ID="hidMode" runat="server" />
                      <asp:HiddenField ID="hidVacpaUser" runat="server" />
                      <asp:HiddenField ID="hidHinhThucNop" runat="server" />
                     </td>
                       </tr>
                    </table>
                    </div>
                    </div>
                    </div>
                     
					
	   	    </div>
           
			 
    </form>
 
    </div>
</div>
           
       

 <script type="text/javascript">


     $.datepicker.setDefaults($.datepicker.regional['vi']);

     $("#<%=txtNgayThanhLap.ClientID%> , #<%=txtNgayCapGiayChungNhanDKKD.ClientID%> ,#<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%> ,#<%=txtNguoiDaiDienPL_NgayCapChungChiKTV.ClientID%> ,#<%=txtNguoiDaiDienLL_NgaySinh.ClientID%> ,#<%=txtNguoiDaiDienPL_NgaySinh.ClientID%>, #NgayQD").datepicker({
         changeMonth: true,
         changeYear: true,
         dateFormat: "dd/mm/yy"
     }).datepicker("option", "maxDate", '+0m +0w +0d');

     $("#<%=txtNgayThanhLap.ClientID%> , #<%=txtNgayCapGiayChungNhanDKKD.ClientID%> ,#<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%> ,#<%=txtNguoiDaiDienPL_NgayCapChungChiKTV.ClientID%> ,#<%=txtNguoiDaiDienLL_NgaySinh.ClientID%> ,#<%=txtNguoiDaiDienPL_NgaySinh.ClientID%>, #NgayQD").mask("99/99/9999");
   
     function checkLoaiHoiVien() {
         <%--if ($('#<%= drLoaiHoiVien.ClientID %>').val() == "-1") {
                alert('Chưa chọn loại hội viên!');
                return false;
         }--%>
         if ($('#<%= drHoiVienTapThe.ClientID %>').val() == "0") {
             alert('Chưa chọn tên công ty!');
             return false;
         }
         //else if ($('#txtSoQuyetDinh').val() == "") {
         //    alert('Chưa nhập số quyết định!');
         //    return false;
         //}
         //else if ($('#NgayQD').val() == "") {
         //    alert('Chưa nhập ngày quyết định!');
         //    return false;
         //}
        }
        </script>