﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="yeucaucnkttaidonvikhac_add.ascx.cs"
    Inherits="usercontrols_QLHV_yeucaucnkttaidonvikhac_add" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<form id="form_CreateLopHoc" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Cập nhật số giờ CNKT tại đơn vị khác</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin khách hàng</legend>
    <table id="tblThongTinChung" width="100%" border="0" class="formtbl">
        <tr>
            <td>
                ID yêu cầu:
            </td>
            <td>
                <input type="text" id="txtIDYeuCau" name="txtIDYeuCau" readonly="readonly" />
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Mã khách hàng<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" name="txtMaHocVien" id="txtMaHocVien" style="width: 120px;" onchange="CallActionGetInforHocVien();" />&nbsp;
                <input type="button" value="---" onclick="OpenDanhSachHocVien();" id="btnOpenFormChonHocVien"
                    style="border: 1px solid gray;" />
                <input type="hidden" id="hdHocVienID" name="hdHocVienID" />
            </td>
            <td>
                Tên khách hàng:
            </td>
            <td>
                <input type="text" name="txtTenHocVien" id="txtTenHocVien" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Đơn vị công tác:
            </td>
            <td>
                <input type="text" name="txtDonViCongTac" id="txtDonViCongTac" readonly="readonly" />
            </td>
            <td>
                Loại khách hàng:
            </td>
            <td>
                <input type="text" name="txtLoaiKhachHang" id="txtLoaiKhachHang" readonly="readonly" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Thông tin số giờ cập nhật kiến thức tại đơn vị khác</legend>
    <input type="hidden" id="hdAction" name="hdAction" />
    <input type="hidden" id="hdIsComeBack" name="hdIsComeBack" />
    <table id="Table1" width="100%" border="0" class="formtbl">
        <tr>
            <td>
                Tên lớp học<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtTenLopHoc" name="txtTenLopHoc" />
            </td>
            <td>
                Thời gian tổ chức<span class="starRequired">(*)</span>:
            </td>
            <td style="min-width: 200px;">
                <input type="text" id="txtTuNgay" name="txtTuNgay" style="width: 80px;" />&nbsp;-&nbsp;
                <input type="text" id="txtDenNgay" name="txtDenNgay" style="width: 80px;" />
            </td>
        </tr>
        <tr>
            <td>
                Đơn vị tổ chức<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtDonViToChuc" name="txtDonViToChuc" />
            </td>
            <td>
                File bằng chứng:
            </td>
            <td>
                <input type="file" value="Chọn file" id="fileBangChung" name="fileBangChung" />&nbsp;
                <span id="spanTenFile" style="font-style: italic;"></span>&nbsp;<a href="javascript:;"
                    id="btnDownload" target="_blank"></a>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Danh sách chuyên đề</legend>
    <div style="width: 100%; margin: 10px 0 10px 0;">
        <a id="btnThemCD" href="javascript:;" class="btn" onclick="OpenFormChuyenDe();"><i
            class="iconfa-plus-sign"></i>Thêm chuyên đề</a>
    </div>
    <table id="tblDanhSachChuyenDe" width="100%" border="0" class="formtbl">
        <tr>
            <th>
                STT
            </th>
            <th>
                Chuyên đề
            </th>
            <th>
                Loại chuyên đề
            </th>
            <th>
                Số giờ học
            </th>
            <th>
                Thao tác
            </th>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; margin: 10px; text-align: center;">
    <a id="btn_save1" href="javascript:;" class="btn btn-rounded" onclick="Save(0);"><i
        class="iconfa-save"></i>Lưu và tiếp tục</a> <a id="btn_save2" href="javascript:;"
            class="btn btn-rounded" onclick="Save(1);"><i class="iconfa-save"></i>Lưu và thoát</a>
    <span id="spanCacNutChucNang"><a href="javascript:;" id="btnDelete" class="btn" onclick="Delete();">
        <i class="iconfa-trash"></i>Xóa</a></span> <a id="A1" href="/admin.aspx?page=yeucaucnkttaidonvikhac"
            class="btn btn-rounded"><i class="iconfa-off"></i>Quay lại</a>
</div>
</form>
<form id="FormCD" clientidmode="Static" method="post" enctype="multipart/form-data">
<div id="DivChuyenDe" style="display: none;">
    <table style="width: 100%;" border="0" class="formtbl">
        <tr>
            <td>
                Tên chuyên đề<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtTenChuyenDe" />
                <input type="hidden" id="hdChuyenDeID" />
                <input type="hidden" id="hdIndexInList" />
            </td>
        </tr>
        <tr>
            <td>
                Loại chuyên đề<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlLoaiChuyenDe">
                    <option value="<%= ListName.Type_LoaiChuyenDe_KeToanKiemToan %>">Kế toán, kiểm toán</option>
                    <option value="<%= ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep %>">Đạo đức nghề nghiệp</option>
                    <option value="<%= ListName.Type_LoaiChuyenDe_ChuyenDeKhac %>">Chuyên đề khác</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                Số giờ học<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtSoGioHoc" />
            </td>
        </tr>
    </table>
</div>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_ChuyenDe" width="0px" height="0px"></iframe>
<script type="text/javascript">
    $("#txtTuNgay").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#txtDenNgay").datepicker({ dateFormat: 'dd/mm/yy' });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm mở Dialog danh sách học viên
    function OpenDanhSachHocVien() {
        $("#DivDanhSachHocVien").empty();
        $("#DivDanhSachHocVien").append($("<iframe width='100%' height='100%' id='ifDanhSachHocVien' name='ifDanhSachHocVien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=yeucaucnkttaidonvikhac_ChonHoiVien"));
        $("#DivDanhSachHocVien").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách học viên</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSachHocVien.ChooseHocVien();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachHocVien').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivDanhSachHocVien').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }

    // Created by NGUYEN MANH HUNG - 2014/11/27
    // Gọi sự kiện lấy dữ liệu học viện theo mã học viên khi NSD nhập trực tiếp mã học viên vào ô textbox
    function CallActionGetInforHocVien() {
        var maHocVien = $('#txtMaHocVien').val();
        iframeProcess.location = '/iframe.aspx?page=QLHV_Process&mahv=' + maHocVien + '&action=loadhv';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm nhận giá trị thông tin học viên từ Dialog danh sách
    function DisplayInforHocVien(value) {
        var arrValue = value.split(';#');
        $('#hdHocVienID').val(arrValue[0]);
        $('#txtMaHocVien').val(arrValue[1]);
        $('#txtTenHocVien').val(arrValue[2]);
        $('#txtDonViCongTac').val(arrValue[3]);
        var loaiHoiVien = arrValue[4];
        if (loaiHoiVien == "0")
            loaiHoiVien = "Người quan tâm";
        if (loaiHoiVien == "1")
            loaiHoiVien = "Hội viên cá nhân";
        if (loaiHoiVien == "2")
            loaiHoiVien = "Kiểm toán viên";
        $('#txtLoaiKhachHang').val(loaiHoiVien);
    }

    // Created by NGUYEN MANH HUNG - 2015/04/20
    // Đóng cửa sổ chọn học viên
    function CloseFormDanhSachHocVien() {
        $("#DivDanhSachHocVien").dialog('close');
    }

    $('#<%=form_CreateLopHoc.ClientID %>').validate({
        rules: {
            txtMaHocVien: {
                required: true
            },
            txtTenLopHoc: {
                required: true
            },
            txtTuNgay: {
                required: true, dateITA: true
            },
            txtDenNgay: {
                required: true, dateITA: true
            },
            txtDonViToChuc: {
                required: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }
    });
    
    $('#txtTuNgay, #txtDenNgay').change(function() {
        ValidateLogic_LopHoc();
    });
    
     function ValidateLogic_LopHoc() {
        var startLopHoc = $('#txtTuNgay').datepicker('getDate');
        var endLopHoc = $('#txtDenNgay').datepicker('getDate');
        
        if(startLopHoc != null && endLopHoc != null) {
            endLopHoc.setDate(endLopHoc.getDate() + 1);
            var minus = (endLopHoc - startLopHoc) / 1000 / 60 / 60 / 24;
            if(minus <= 0){
                $('#txtDenNgay').val('');
                $('#txtDenNgay').focus();
                alert('Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc!');
            }
        }
    }
    
    function Save(isComeBack) {
        $('#hdAction').val('save');
        $('#hdIsComeBack').val(isComeBack);
        $('#form_CreateLopHoc').submit();
    }

    function Delete() {
        if(confirm('Bạn chắc chắn muốn xóa thông tin số giờ CNKT này chứ?')) {
        $('#hdAction').val('del');
        $('#form_CreateLopHoc').submit();
            }
    }

    // --------------- Phần xử lý với bảng chuyên đề
    var _arrDataDanhSachChuyenDe = [];
    function DrawData_ChuyenDe(arrData) {
        ResetControl_ChuyenDe();
        var tbl = document.getElementById('tblDanhSachChuyenDe');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if (arrData.length > 0) {
            _arrDataDanhSachChuyenDe = arrData;
            for (var i = 0; i < arrData.length; i++) {
                var chuyenDeID = arrData[i][0];
                var tenChuyenDe = arrData[i][1];
                var loaiChuyenDe = arrData[i][2];
                var soGioHoc = arrData[i][3];
                var tenLoaiChuyenDe = '';
                if (loaiChuyenDe == "1")
                    tenLoaiChuyenDe = "Kế toán, kiểm toán";
                if (loaiChuyenDe == "2")
                    tenLoaiChuyenDe = "Đạo đức nghề nghiệp";
                if (loaiChuyenDe == "3")
                    tenLoaiChuyenDe = "Chuyên đề khác";

                var tr = document.createElement('TR');
                var arrColumn = [(i + 1), tenChuyenDe, tenLoaiChuyenDe, soGioHoc];
                for (var j = 0; j < arrColumn.length; j++) {
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    if (j == 0) {
                        td.innerHTML += '<input type="hidden" name="hfValueChuyenDe' + i + '" value="' + chuyenDeID + ';#' + loaiChuyenDe + ';#' + tenChuyenDe + ';#' + soGioHoc + '" />';
                    }
                    tr.appendChild(td);
                }
                var tdThaoTac = document.createElement("TD");
                tdThaoTac.innerHTML += "<a href='javascript:;' onclick='OpenFormChuyenDe(); GetOneChuyenDe(\"" + i + "\");' class='btn' tooltip='Sửa'><i class='iconsweets-create'></i></a><a href='javascript:;' onclick='CallMethodDeleteChuyenDe(\"" + i + "\");' class='btn' tooltip='Xóa'><i class='iconsweets-trashcan'></i></a>";
                tr.appendChild(tdThaoTac);
                tbl.appendChild(tr);
            }
        }
    }

    function CallMethodDeleteChuyenDe(index) {
        if (confirm('Bạn chắc chắn muốn xóa chuyên đề này chứ?')) {
            var tempArrDataDanhSachChuyenDe = [];
            for (var i = 0; i < _arrDataDanhSachChuyenDe.length; i++) {
                if (parseInt(i) != parseInt(index))
                    tempArrDataDanhSachChuyenDe.push(_arrDataDanhSachChuyenDe[i]);
            }
            DrawData_ChuyenDe(tempArrDataDanhSachChuyenDe);
        }
    }

    function GetOneChuyenDe(index) {
        $('#hdIndexInList').val(index);
        $('#hdChuyenDeID').val(_arrDataDanhSachChuyenDe[index][0]);
        $('#txtTenChuyenDe').val(_arrDataDanhSachChuyenDe[index][1]);
        $('#ddlLoaiChuyenDe').val(_arrDataDanhSachChuyenDe[index][2]);
        $('#txtSoGioHoc').val(_arrDataDanhSachChuyenDe[index][3]);
    }

    function SaveChuyenDe() {
        var chuyenDeId = $('#hdChuyenDeID').val();
        var loaiChuyenDe = $('#ddlLoaiChuyenDe').val();
        var tenChuyenDe = $('#txtTenChuyenDe').val();
        var soGioHoc = $('#txtSoGioHoc').val();
        if(tenChuyenDe.length == 0 || soGioHoc.length == 0) {
            alert('Phải nhập tên chuyên đề và số giờ học');
            return;
        }
        if(CheckIsNumber(document.getElementById('txtSoGioHoc'))) {
            if(!CheckIsNumberPositive(document.getElementById('txtSoGioHoc'))) {
                return;
            }
        } else {
            return;
        }

        var indexInList = $('#hdIndexInList').val();
        var _tempArrDataDanhSachChuyenDe = [];
        if (indexInList.length > 0) // Update
        {
            var arrDataOneRow = [chuyenDeId, tenChuyenDe, loaiChuyenDe, soGioHoc];
            for (var i = 0; i < _arrDataDanhSachChuyenDe.length; i++) {
                if (i != parseInt(indexInList))
                    _tempArrDataDanhSachChuyenDe.push(_arrDataDanhSachChuyenDe[i]);
                else {
                    _tempArrDataDanhSachChuyenDe.push(arrDataOneRow);
                }
            }
        } else // Add
        {
            var arrDataOneRow = [chuyenDeId, tenChuyenDe, loaiChuyenDe, soGioHoc];
            for (var i = 0; i < _arrDataDanhSachChuyenDe.length; i++) {
                _tempArrDataDanhSachChuyenDe.push(_arrDataDanhSachChuyenDe[i]);
            }
            _tempArrDataDanhSachChuyenDe.push(arrDataOneRow);
        }

        DrawData_ChuyenDe(_tempArrDataDanhSachChuyenDe);
        $("#DivChuyenDe").dialog("close");
    }

    function ResetControl_ChuyenDe() {
        $('#hdIndexInList').val('');
        $('#hdChuyenDeID').val('');
        $('#txtTenChuyenDe').val('');
        $('#ddlLoaiChuyenDe').val($("#ddlLoaiChuyenDe option:first").val());
        $('#txtSoGioHoc').val('');
    }

    function DisplayAlertMessage_ChuyenDe(type, msg) {
        var html = "";
        if (type == 1)
            html = "<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + msg + " thành công!</div>";
        if (type == 0)
            html = "<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + msg + " thất bại!</div>";
        $('#AlertMessageChuyenDe').html(html);
    }

//    $('#FormCD').validate({
//        rules: {
//            txtTenChuyenDe: {
//                required: true
//            },
//            txtSoGioHoc: {
//                required: true, digitsWithOutSeparatorCharacterVN: true
//            }
//        },
//        invalidHandler: function (f, d) {
//            var g = d.numberOfInvalids();

//            if (g) {
//                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
//                jQuery("#thongbaoloi_form").html(e).show();

//            } else {
//                jQuery("#thongbaoloi_form").hide();
//            }
//        }
//    });

    function OpenFormChuyenDe() {
        ResetControl_ChuyenDe();
        $("#DivChuyenDe").dialog({
            resizable: true,
            width: 600,
            height: 250,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Cập nhật thông tin chuyên đề</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Lưu": function () {
                    SaveChuyenDe();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivChuyenDe').parent().find('button:contains("Lưu")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivChuyenDe').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }
    
    <% CheckPermissionOnPage();%>
    <% LoadOldData(); %>
</script>
<div id="DivDanhSachHocVien">
</div>
