﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using System.Drawing;


public partial class usercontrols_hosohoivientapthe : System.Web.UI.UserControl
{
    String id = String.Empty;
    public Commons cm = new Commons();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    clsXacDinhQuyen quyen = new clsXacDinhQuyen();
    protected HoiVienFile FileDinhKem1 = new HoiVienFile();

    public HoiVienCaNhan hoivien;

    public bool QuyenXem { get; set; }
    public bool QuyenThem { get; set; }
    public bool QuyenSua { get; set; }
    public bool QuyenXoa { get; set; }
    public bool QuyenDuyet { get; set; }
    public bool QuyenTraLai { get; set; }
    public bool QuyenHuy { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Tên chức năng
            string tenchucnang = "Cập nhật hồ sơ hội viên cá nhân";
            if (Request.QueryString["ktv"] == "1")
            {
                lbTitle.Text = "Cập nhật hồ sơ kiểm toán viên";
                tenchucnang = "Cập nhật hồ sơ kiểm toán viên";
            }
            // Icon CSS Class  
            string IconClass = "iconfa-credit-card";

            if (!string.IsNullOrEmpty(cm.Admin_NguoiDungID))
            {
                string maquyen = quyen.fcnXDQuyen(int.Parse(cm.Admin_NguoiDungID), "DangKyMaSoCBDT", cm.connstr);

                QuyenXem = (maquyen.Contains("XEM")) ? true : false;
                QuyenThem = (maquyen.Contains("THEM")) ? true : false;
                QuyenSua = (maquyen.Contains("SUA")) ? true : false;
                QuyenXoa = (maquyen.Contains("XOA")) ? true : false;
                QuyenDuyet = (maquyen.Contains("DUYET")) ? true : false;
                QuyenTraLai = (maquyen.Contains("TRALAI")) ? true : false;
                QuyenHuy = (maquyen.Contains("HUY")) ? true : false;
            }

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"
     <li>Biểu mẫu tờ khai <span class=""separator""></span></li> <!-- Nhóm chức năng cấp trên -->
     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            if (!IsPostBack)
            {
                DataTable dtb_tt = new DataTable();
                dtb_tt.Columns.Add("LoaiPhi", typeof(string));
                dtb_tt.Columns.Add("SoTienDaNop", typeof(string));
                dtb_tt.Columns.Add("NgayNop", typeof(string));
                dtb_tt.Columns.Add("SoTienChuaNop", typeof(string));

                dtb_tt.Rows.Add("Phí cập nhật kiến thức chuyên đề 12 năm 2011", "2.000.000", "17/09/2011", "222.222");
                dtb_tt.Rows.Add("Phí cập nhật kiến thức chuyên đề 12 năm 2012", "2.000.000", "17/09/2012", "222.222");
                dtb_tt.Rows.Add("Phí cập nhật kiến thức chuyên đề 12 năm 2013", "2.000.000", "17/09/2013", "222.222");
                dtb_tt.Rows.Add("Phí cập nhật kiến thức chuyên đề 12 năm 2014", "2.000.000", "17/09/2014", "222.222");

                gvThanhToan.DataSource = dtb_tt;
                gvThanhToan.DataBind();

                SetInitialRow();
                SetInitialRow_ChungChi();
                SetInitialRow_KhenThuong();
                SetInitialRow_KyLuat();

                LoadDropDown();

                LoadThongTin();
                LoadHoSoDaoTao(Request.QueryString["id"]);
                LoadDropDownNam(Request.QueryString["id"]);

                LoadThanhVienKiemSoat(Request.QueryString["id"]);
            }



        }
        catch (Exception ex)
        {
            placeMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    private void LoadHoSoDaoTao(string ID)
    {
        string sql = "SELECT C.MaChuyenDe, C.TenChuyenDe, CONVERT(VARCHAR, C.TuNgay, 103) + ' - ' + CONVERT(VARCHAR, C.DenNgay, 103) AS ThoiGian, B.TenLopHoc, dbo.LaySoGioDaoTao(" + ID + ", A.LopHocID, A.ChuyenDeID, '1') AS KT, dbo.LaySoGioDaoTao(" + ID + ", A.LopHocID, A.ChuyenDeID, '2') AS DD, dbo.LaySoGioDaoTao(" + ID + ", A.LopHocID, A.ChuyenDeID, '3') AS Khac";
        sql += " FROM tblCNKTLopHocHocVien A INNER JOIN tblCNKTLopHoc B ON A.LopHocID = B.LopHocID ";
        sql += " INNER JOIN tblCNKTLopHocChuyenDe C ON A.ChuyenDeID = C.ChuyenDeID ";
        sql += " WHERE A.HoiVienCaNhanID = " + ID;
        SqlCommand cmd = new SqlCommand(sql);
        DataTable dtb = new DataTable();
        
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        GridViewHelper helper = new GridViewHelper(this.gvDaoTao);
        helper.RegisterSummary("KT", SummaryOperation.Sum);
        helper.RegisterSummary("DD", SummaryOperation.Sum);
        helper.RegisterSummary("Khac", SummaryOperation.Sum);

        helper.RegisterGroup("TenLopHoc", true, true);
        helper.GroupHeader += new GroupEvent(helper_GroupHeader_DaoTao);
        helper.ApplyGroupSort();

        gvDaoTao.DataSource = dtb;
        gvDaoTao.DataBind();


        for (int i = 0; i < gvDaoTao.Rows.Count; i++)
        {
            if (gvDaoTao.Rows[i].Cells[5].Text == "0")
                gvDaoTao.Rows[i].Cells[5].Text = "";
            if (gvDaoTao.Rows[i].Cells[6].Text == "0")
                gvDaoTao.Rows[i].Cells[6].Text = "";
            if (gvDaoTao.Rows[i].Cells[7].Text == "0")
                gvDaoTao.Rows[i].Cells[7].Text = "";
        }

        string html = "";
        html += "<script type=\"text/javascript\">";

        html += "var gvDaoTao = document.getElementById(\"gvDaoTao\");";
        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 3).ToString() + "].cells[0].innerHTML = 'Tổng số giờ đã cập nhật kiến thức:';";
        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 3).ToString() + "].cells[0].style.fontWeight = 'bold';";
        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 3).ToString() + "].cells[1].style.fontWeight = 'bold';";
        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 3).ToString() + "].cells[2].style.fontWeight = 'bold';";
        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 3).ToString() + "].cells[3].style.fontWeight = 'bold';";

        html += "var row = gvDaoTao.insertRow(" + (gvDaoTao.Rows.Count + 4).ToString() + ");";

        html += "var cell1 = row.insertCell(0);";
        html += "var cell2 = row.insertCell(1);";
        html += "var cell3 = row.insertCell(2);";
        html += "var cell4 = row.insertCell(3);";
        html += "cell1.colSpan = 4;";
        html += "cell1.innerHTML = \"Số giờ cập nhật kiến thức còn thiếu:\";";
        html += "cell2.innerHTML = \"12\";";
        html += "cell2.style.textAlign = \"center\";";
        html += "cell3.innerHTML = \"0\";";
        html += "cell3.style.textAlign = \"center\";";
        html += "cell4.innerHTML = \"4\";";
        html += "cell4.style.textAlign = \"center\";";

        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 4).ToString() + "].cells[0].style.fontWeight = 'bold';";
        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 4).ToString() + "].cells[1].style.fontWeight = 'bold';";
        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 4).ToString() + "].cells[2].style.fontWeight = 'bold';";
        html += "gvDaoTao.rows[" + (gvDaoTao.Rows.Count + 4).ToString() + "].cells[3].style.fontWeight = 'bold';";

        html += "</script>";

        litgvDaoTao.Text = html;
    }

    protected void gvDaoTao_OnDataBound(object sender, EventArgs e)
    {
        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        TableHeaderCell cell = new TableHeaderCell();
        cell.Text = "";
        cell.ColumnSpan = 4;

        row.Controls.Add(cell);

        cell = new TableHeaderCell();
        cell.Text = "Tổng số giờ CNKT đã tham dự";
        cell.ColumnSpan = 4;
        cell.HorizontalAlign = HorizontalAlign.Center;

        row.Controls.Add(cell);
        row.BorderWidth = 0;
        gvDaoTao.HeaderRow.Parent.Controls.AddAt(0, row);
    }

    private void helper_GroupHeader_DaoTao(string groupName, object[] values, GridViewRow row)
    {
        if (groupName == "TenLopHoc")
        {
            row.BackColor = Color.LightGray;
            row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
        }

    }

    protected void gvDaoTao_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    private void LoadDropDownNam(string ID)
    {
        string sql = "SELECT DISTINCT YEAR(B.NgayBaoCaoKT) AS Nam ";
        sql += " FROM tblHoiVienCaNhan A INNER JOIN tblKSCLBaoCaoKTKT B ON A.HoiVienCaNhanID = B.HoiVienCaNhanID";

        sql += " WHERE A.HoiVienCaNhanID = " + ID;
        sql += " ORDER BY Nam DESC";
        SqlCommand cmd = new SqlCommand(sql);
        
        DataTable dtb = new DataTable();
        
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        drNam.DataSource = dtb;
        drNam.DataBind();

        if (!string.IsNullOrEmpty(drNam.SelectedValue))
            LoadKiemSoat(ID, drNam.SelectedValue);
    }

    private void LoadThanhVienKiemSoat(string ID)
    {
        string sql = "SELECT YEAR(A.NgayLap) AS Nam, A.MaDoanKiemTra AS MaDoanKiemTra, CONVERT(VARCHAR, A.NgayLap, 103) AS NgayLapDoan, dbo.LayCongTyKiemTra(A.DoanKiemTraID) AS CTKT FROM tblKSCLDoanKiemTra A";
        sql += " INNER JOIN tblKSCLDoanKiemTraThanhVien B ON A.DoanKiemTraID = B.DoanKiemTraID";
        sql += " WHERE B.HoiVienCaNhanID = " + ID;
        SqlCommand cmd = new SqlCommand(sql);

        DataTable dtb = new DataTable();

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        gvThanhVien.DataSource = dtb;
        gvThanhVien.DataBind();
    }

    protected void drNam_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(drNam.SelectedValue))
            LoadKiemSoat(ID, drNam.SelectedValue);
    }

    private void LoadKiemSoat(string ID, string Nam)
    {
        string sql = "SELECT '" + Nam + "' AS Nam, B.TenKhachHangKT AS HoSo, CASE XepLoai WHEN '1' THEN 'x' ELSE '' END AS Tot, CASE XepLoai WHEN '2' THEN 'x' ELSE '' END AS DatYeuCau, CASE XepLoai WHEN '3' THEN 'x' ELSE '' END AS KhongDat, CASE XepLoai WHEN '4' THEN 'x' ELSE '' END AS YeuKem ";
        sql += " FROM tblHoiVienCaNhan A INNER JOIN tblKSCLBaoCaoKTKT B ON A.HoiVienCaNhanID = B.HoiVienCaNhanID";

        sql += " WHERE A.HoiVienCaNhanID = " + ID + " AND YEAR(B.NgayBaoCaoKT) = " + Nam;
        sql += " ORDER BY Nam DESC";
        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        GridViewHelper helper = new GridViewHelper(this.gvKiemSoat);

        helper.RegisterGroup("Nam", true, true);
        helper.GroupHeader += new GroupEvent(helper_GroupHeader_KiemSoat);
        helper.ApplyGroupSort();

        gvKiemSoat.DataSource = dtb;
        gvKiemSoat.DataBind();

        string html = "";
        html += "<script type=\"text/javascript\">";

        html += "var gvKiemSoat = document.getElementById(\"gvKiemSoat\");";

        html += "var row1 = gvKiemSoat.insertRow(" + (gvKiemSoat.Rows.Count + 3).ToString() + ");";
        html += "var cell1 = row1.insertCell(0);";
        html += "cell1.colSpan = 6;";
        html += "cell1.innerHTML = \"Tổng số báo cáo kiểm toán được kiểm tra: " + dtb.Rows.Count + "\";";

        html += "var row2 = gvKiemSoat.insertRow(" + (gvKiemSoat.Rows.Count + 4).ToString() + ");";
        html += "var cell2 = row2.insertCell(0);";
        html += "cell2.colSpan = 6;";
        html += "cell2.innerHTML = \"Tổng số báo cáo kiểm toán ký trong năm: " + dtb.Rows.Count + "\";";

        html += "var row3 = gvKiemSoat.insertRow(" + (gvKiemSoat.Rows.Count + 5).ToString() + ");";
        html += "var cell3 = row3.insertCell(0);";
        html += "cell3.colSpan = 6;";
        html += "cell3.innerHTML = \"Số lượng báo cáo kiểm toán cho khách hàng là đơn vị có lợi ích công chúng thuộc lĩnh vực chứng khoán: " + dtb.Rows.Count + "\";";

        html += "gvKiemSoat.rows[" + (gvKiemSoat.Rows.Count + 3).ToString() + "].cells[0].style.fontWeight = 'bold';";
        html += "gvKiemSoat.rows[" + (gvKiemSoat.Rows.Count + 4).ToString() + "].cells[0].style.fontWeight = 'bold';";
        html += "gvKiemSoat.rows[" + (gvKiemSoat.Rows.Count + 5).ToString() + "].cells[0].style.fontWeight = 'bold';";

        html += "</script>";

        litgvKiemSoat.Text = html;


    }

    protected void gvKiemSoat_OnDataBound(object sender, EventArgs e)
    {
        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        TableHeaderCell cell = new TableHeaderCell();
        cell.Text = "";
        cell.ColumnSpan = 2;

        row.Controls.Add(cell);

        cell = new TableHeaderCell();
        cell.Text = "Kết quả kiểm tra hồ sơ";
        cell.ColumnSpan = 4;
        cell.HorizontalAlign = HorizontalAlign.Center;

        row.Controls.Add(cell);
        row.BorderWidth = 0;
        gvKiemSoat.HeaderRow.Parent.Controls.AddAt(0, row);
    }

    private void helper_GroupHeader_KiemSoat(string groupName, object[] values, GridViewRow row)
    {
        if (groupName == "Nam")
        {
            row.BackColor = Color.LightGray;
            row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
        }

    }

    protected void gvKiemSoat_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    private void LoadThongTin()
    {
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");

        SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connStr, true, "");
        SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
        SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connStr, true, "");

        hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

        lbHoiVienCaNhanID.Text = hoivien.HoiVienCaNhanId.ToString();

        txtHoDem.Text = hoivien.HoDem;
        txtTen.Text = hoivien.Ten;
        if (hoivien.GioiTinh == "1") rdNam.Checked = true; else rdNu.Checked = true;
        if (hoivien.NgaySinh != null)
            NgaySinh.Value = hoivien.NgaySinh.Value.ToString("dd/MM/yyyy");
        if (hoivien.QuocTichId != null)
            drQuocTich.SelectedValue = hoivien.QuocTichId.Value.ToString();
        txtDiaChi.Text = hoivien.DiaChi;
        txtSoGiayDHKN.Text = hoivien.SoGiayChungNhanDkhn;
        if (hoivien.NgayCapGiayChungNhanDkhn != null)
            NgayCap_DHKN.Value = hoivien.NgayCapGiayChungNhanDkhn.Value.ToString("dd/MM/yyyy");
        if (hoivien.HanCapTu != null)
            HanCapTu.Value = hoivien.HanCapTu.Value.ToString("dd/MM/yyyy");
        if (hoivien.HanCapDen != null)
            HanCapDen.Value = hoivien.HanCapDen.Value.ToString("dd/MM/yyyy");

        txtSoChungChiKTV.Text = hoivien.SoChungChiKtv;
        if (hoivien.NgayCapChungChiKtv != null)
            NgayCap_ChungChiKTV.Value = hoivien.NgayCapChungChiKtv.Value.ToString("dd/MM/yyyy");

        txtEmail.Text = hoivien.Email;
        txtDienThoai.Text = hoivien.DienThoai;
        txtMobile.Text = hoivien.Mobile;
        if (hoivien.ChucVuId != null)
            drChucVu.SelectedValue = hoivien.ChucVuId.Value.ToString();
        if (hoivien.TruongDaiHocId != null)
            drTotNghiep.SelectedValue = hoivien.TruongDaiHocId.Value.ToString();
        if (hoivien.ChuyenNganhDaoTaoId != null)
            drChuyenNganh.SelectedValue = hoivien.ChuyenNganhDaoTaoId.Value.ToString();
        txtNam_ChuyenNganh.Text = hoivien.ChuyenNganhNam;
        if (hoivien.HocViId != null)
            drHocVi.SelectedValue = hoivien.HocViId.Value.ToString();
        txtNam_HocVi.Text = hoivien.HocViNam;
        if (hoivien.HocHamId != null)
            drHocHam.SelectedValue = hoivien.HocHamId.Value.ToString();
        txtNam_HocHam.Text = hoivien.HocHamNam;
        txtSoCMND.Text = hoivien.SoCmnd;
        if (hoivien.CmndNgayCap != null)
            NgayCap_CMND.Value = hoivien.CmndNgayCap.Value.ToString("dd/MM/yyyy");
        if (hoivien.CmndTinhId != null)
            drNoiCap_CMND.SelectedValue = hoivien.CmndTinhId.Value.ToString();
        txtSoTKNganHang.Text = hoivien.SoTaiKhoanNganHang;
        if (hoivien.NganHangId != null)
            drNganHang.SelectedValue = hoivien.NganHangId.Value.ToString();
        if (hoivien.HoiVienTapTheId != null)
            drDonVi.SelectedValue = hoivien.HoiVienTapTheId.Value.ToString();
        if (hoivien.SoThichId != null)
            drSoThich.SelectedValue = hoivien.SoThichId.Value.ToString();

        // chứng chỉ
        LoadChungChi();

        // quá trình làm việc
        LoadQuaTrinh();

        LoadKhenThuongKyLuat(hoivien.HoiVienCaNhanId.ToString());

        // file
        loaduploadFileDinhKem(hoivien.HoiVienCaNhanId);

        if (Request.QueryString["mode"] == "view")
        {
            Response.Write("$('#form_nhaphoso input,select,textarea').attr('disabled', true);");
        }
    }

    private void LoadKhenThuongKyLuat(string ID)
    {
        string sql = "SELECT CONVERT(VARCHAR, A.NgayThang, 103) AS NgayThang, A.HinhThucKhenThuongID, A.LyDo, B.TenHinhThucKhenThuong AS TenHinhThuc FROM tblKhenThuongKyLuat A INNER JOIN tblDMHinhThucKhenThuong B ON A.HinhThucKhenThuongID = B.HinhThucKhenThuongID";

        sql += " WHERE Loai = 1 AND HoiVienCaNhanID = " + ID;
        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        if (dtb.Rows.Count != 0)
        {
            if (ViewState["KhenThuong"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KhenThuong"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = row["NgayThang"].ToString();
                    drCurrentRow["HinhThucID"] = row["HinhThucKhenThuongID"].ToString();
                    drCurrentRow["LyDo"] = row["LyDo"].ToString();
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["KhenThuong"] = dtCurrentTable;

                gvKhenThuong.DataSource = dtCurrentTable;
                gvKhenThuong.DataBind();

                SetPreviousData_KhenThuong();
            }
        }

        ds.Clear();
        dtb.Clear();

        sql = "SELECT CONVERT(VARCHAR, A.NgayThang, 103) AS NgayThang, A.HinhThucKyLuatID, A.LyDo, B.TenHinhThucKyLuat AS TenHinhThuc FROM tblKhenThuongKyLuat A INNER JOIN tblDMHinhThucKyLuat B ON A.HinhThucKyLuatID = B.HinhThucKyLuatID";

        sql += " WHERE Loai = 2 AND HoiVienCaNhanID = " + ID;
        ds = new DataSet();
        dtb = new DataTable();
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        if (dtb.Rows.Count != 0)
        {
            if (ViewState["KyLuat"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KyLuat"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = row["NgayThang"].ToString();
                    drCurrentRow["HinhThucID"] = row["HinhThucKyLuatID"].ToString();
                    drCurrentRow["LyDo"] = row["LyDo"].ToString();
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["KyLuat"] = dtCurrentTable;

                gvKyLuat.DataSource = dtCurrentTable;
                gvKyLuat.DataBind();

                SetPreviousData_KyLuat();
            }
        }
    }

    protected void loaduploadFileDinhKem(int donHoiVienId)
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            string DoiTuong = (Request.QueryString["ktv"] == "1") ? "2" : "1";
            SqlCommand sql = new SqlCommand();

            if (donHoiVienId == 0)
            {
                sql.CommandText = " SELECT  LoaiGiayToID ,TenFile as TenBieuMau ,DoiTuong ,BatBuoc  , 0 as FileID  , ' ' as TenFileDinhKem " +
                                  " FROM tblDMLoaiGiayTo  " +
                                  " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuong + "' order by tblDMLoaiGiayTo.TenFile   ";
            }
            else
            {
                sql.CommandText =
                "    SELECT   LoaiGiayToID ,TenFile as TenBieuMau  ,DoiTuong  ,BatBuoc , " +
                              "   ( SELECT TOP 1 ISNULL ( tblHoiVienFile.FileID,0)  FROM  tblHoiVienFile      WHERE  tblHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblHoiVienFile.HoiVienCaNhanID= " + donHoiVienId + "  ) as FileID ," +
                               " ( SELECT  TOP 1 ISNULL(tblHoiVienFile.TenFile,' ')   FROM  tblHoiVienFile  WHERE  tblHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblHoiVienFile.HoiVienCaNhanID= " + donHoiVienId + " ) as  TenFileDinhKem    " +
                " FROM tblDMLoaiGiayTo  " +
                " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuong + "' order by tblDMLoaiGiayTo.TenFile  ";


            }

            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;


            PagedDataSource objPds1 = new PagedDataSource();
            objPds1.DataSource = ds.Tables[0].DefaultView;

            FileDinhKem_grv.DataSource = objPds1;
            FileDinhKem_grv.DataBind();
            //FileDinhKem_grv.Columns[3].Visible = false; 
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;

            // set validate

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                String sTenLoaiFile = row.Cells[0].Text.Trim();
                String sLoaiFileID = row.Cells[3].Text.Trim();
                String sFileDinhKemID = row.Cells[4].Text.Trim();
                String sBatbuoc = row.Cells[5].Text.Trim();

                String sTenFile = row.Cells[6].Text.Trim();

                // check validate
                if (sBatbuoc == "0" || !String.IsNullOrEmpty(sTenFile))
                {
                    ((RequiredFieldValidator)row.FindControl("RequiredFieldFileUpload")).Visible = false;


                }

                // an hien chu thich bat buoc
                if (sBatbuoc == "0")
                {
                    ((Label)row.FindControl("requred")).Visible = false;
                    //((Label)row.FindControl("requred")).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                }
                else
                {
                    //((Label)row.FindControl("requred")).Text = "(*) ";
                }

                //// an hien link bieu mau

                //if (String.IsNullOrEmpty(sBieuMau))
                //{
                //    //  ((HyperLink)row.FindControl("linkBieuMau")).Visible = false;
                //    HyperLink likBM = ((HyperLink)row.FindControl("linkBieuMau"));// 

                //    likBM.Text = "&nbsp; &nbsp; &nbsp;";
                //    likBM.NavigateUrl = "";

                //}

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }




    }

    //private void LoadTenFile()
    //{
    //    SqlCommand cmd = new SqlCommand();
    //    cmd.CommandText = "SELECT A.TenFile, A.LoaiGiayToID FROM tblDonHoiVienFile A WHERE A.DonHoiVienCaNhanID = " + Request.QueryString["id"];

    //    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

    //    foreach (DataRow row in dtb.Rows)
    //    {
    //        switch (row["LoaiGiayToID"].ToString())
    //        {
    //            case "1":
    //                lbFileAnh.Text = row["TenFile"].ToString();
    //                break;
    //            case "2":
    //                lbFileQuyetDinh.Text = row["TenFile"].ToString();
    //                break;
    //            case "3":
    //                lbFileBanSao.Text = row["TenFile"].ToString();
    //                break;
    //            case "4":
    //                lbFileChuKy.Text = row["TenFile"].ToString();
    //                break;
    //            case "5":
    //                lbFileGiayChungNhan.Text = row["TenFile"].ToString();
    //                break;
    //            case "6":
    //                lbFileGiayToKhac.Text = row["TenFile"].ToString();
    //                break;
    //            case "7":
    //                lbFileDonXinGiaNhap.Text = row["TenFile"].ToString();
    //                break;
    //            default:
    //                break;
    //        }
    //    }
    //}

    private void LoadQuaTrinh()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT A.ThoiGianCongTac, A.ChucVu, A.NoiLamViec FROM tblHoiVienCaNhanQuaTrinhCongTac A WHERE A.HoiVienCaNhanID = " + Request.QueryString["id"];

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb.Rows.Count != 0)
        {
            if (ViewState["QuaTrinh"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["QuaTrinh"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["ThoiGian"] = row["ThoiGianCongTac"].ToString();
                    drCurrentRow["ChucVu"] = row["ChucVu"].ToString();
                    drCurrentRow["NoiLamViec"] = row["NoiLamViec"].ToString();

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["QuaTrinh"] = dtCurrentTable;

                gvQuaTrinh.DataSource = dtCurrentTable;
                gvQuaTrinh.DataBind();

                SetPreviousData();
            }
        }
    }

    private void LoadChungChi()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT A.SoChungChi, A.NgayCap FROM tblHoiVienCaNhanChungChiQuocTe A WHERE A.HoiVienCaNhanID = " + Request.QueryString["id"];

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb.Rows.Count != 0)
        {
            if (ViewState["ChungChi"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["ChungChi"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["SoChungChi"] = row["SoChungChi"].ToString();
                    drCurrentRow["NgayCap"] = DateTime.Parse(row["NgayCap"].ToString()).ToShortDateString();

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["ChungChi"] = dtCurrentTable;

                gvChungChi.DataSource = dtCurrentTable;
                gvChungChi.DataBind();

                SetPreviousData_ChungChi();
            }
        }
    }

    public void LoadDropDown()
    {

        DataTable dtb = new DataTable();

        string sql = "";

        // Đơn vị
        sql = "SELECT HoiVienTapTheID, TenDoanhNghiep FROM tblHoiVienTapThe";
        SqlCommand cmd = new SqlCommand(sql);

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drDonVi.DataSource = dtb;
        drDonVi.DataBind();

        dtb.Clear();
        // Quốc tịch
        sql = "SELECT * FROM tblDMQuocTich";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drQuocTich.DataSource = dtb;
        drQuocTich.DataBind();

        dtb.Clear();
        // Trường ĐH
        sql = "SELECT * FROM tblDMTruongDaiHoc";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drTotNghiep.DataSource = dtb;
        drTotNghiep.DataBind();


        dtb.Clear();
        // Chuyên ngành
        sql = "SELECT * FROM tblDMChuyenNganhDaoTao";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChuyenNganh.DataSource = dtb;
        drChuyenNganh.DataBind();

        dtb.Clear();
        // Học vị
        sql = "SELECT 0 AS HocViID, N'<< Lựa chọn >>' AS TenHocVi UNION ALL (SELECT HocViID, TenHocVi FROM tblDMHocVi)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHocVi.DataSource = dtb;
        drHocVi.DataBind();

        dtb.Clear();
        // Học hàm
        sql = "SELECT 0 AS HocHamID, N'<< Lựa chọn >>' AS TenHocHam UNION ALL (SELECT HocHamID, TenHocHam FROM tblDMHocHam)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHocHam.DataSource = dtb;
        drHocHam.DataBind();

        dtb.Clear();
        // Nơi cấp CMND
        sql = "SELECT * FROM tblDMTinh";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drNoiCap_CMND.DataSource = dtb;
        drNoiCap_CMND.DataBind();

        dtb.Clear();
        // Ngân hàng
        sql = "SELECT 0 AS NganHangID, N'<< Lựa chọn >>' AS TenNganHang UNION ALL (SELECT NganHangID, TenNganHang FROM tblDMNganHang)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drNganHang.DataSource = dtb;
        drNganHang.DataBind();

        dtb.Clear();
        // Load chức vụ
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChucVu.DataSource = dtb;
        drChucVu.DataBind();

        dtb.Clear();
        // Load sở thích
        sql = "SELECT 0 AS SoThichID, N'<< Lựa chọn >>' AS TenSoThich UNION ALL (SELECT SoThichID, TenSoThich FROM tblDMSoThich)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drSoThich.DataSource = dtb;
        drSoThich.DataBind();


    }

    #region GridKhenThuongKyLuat
    protected void ButtonAddKhenThuong_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_KhenThuong();
    }

    protected void ButtonAddKyLuat_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_KyLuat();
    }

    private void SetInitialRow_KhenThuong()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayThang", typeof(string)));
        dt.Columns.Add(new DataColumn("HinhThucID", typeof(string)));
        dt.Columns.Add(new DataColumn("LyDo", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["NgayThang"] = "";
        dr["HinhThucID"] = 0;
        dr["LyDo"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["KhenThuong"] = dt;

        gvKhenThuong.DataSource = dt;
        gvKhenThuong.DataBind();
    }
    private void AddNewRowToGrid_KhenThuong()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["KhenThuong"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KhenThuong"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        dtCurrentTable.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                        dtCurrentTable.Rows[i - 1]["HinhThucID"] = drHinhThuc.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["LyDo"] = lydo.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = "";
                    drCurrentRow["HinhThucID"] = 0;
                    drCurrentRow["LyDo"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["NgayThang"] = "";
                    dr["HinhThucID"] = 0;
                    dr["LyDo"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["KhenThuong"] = dtCurrentTable;

                gvKhenThuong.DataSource = dtCurrentTable;
                gvKhenThuong.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_KhenThuong();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_KhenThuong()
    {
        int rowIndex = 0;
        if (ViewState["KhenThuong"] != null)
        {
            DataTable dt = (DataTable)ViewState["KhenThuong"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    ngaythang.Text = dt.Rows[i]["NgayThang"].ToString();
                    drHinhThuc.SelectedValue = dt.Rows[i]["HinhThucID"].ToString();
                    lydo.Text = dt.Rows[i]["LyDo"].ToString();
                    rowIndex++;
                }
            }
        }
    }


    private void SetInitialRow_KyLuat()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayThang", typeof(string)));
        dt.Columns.Add(new DataColumn("HinhThucID", typeof(string)));
        dt.Columns.Add(new DataColumn("LyDo", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["NgayThang"] = "";
        dr["HinhThucID"] = 0;
        dr["LyDo"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["KyLuat"] = dt;

        gvKyLuat.DataSource = dt;
        gvKyLuat.DataBind();
    }
    private void AddNewRowToGrid_KyLuat()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["KyLuat"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KyLuat"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        dtCurrentTable.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                        dtCurrentTable.Rows[i - 1]["HinhThucID"] = drHinhThuc.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["LyDo"] = lydo.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = "";
                    drCurrentRow["HinhThucID"] = 0;
                    drCurrentRow["LyDo"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["NgayThang"] = "";
                    dr["HinhThucID"] = 0;
                    dr["LyDo"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["KyLuat"] = dtCurrentTable;

                gvKyLuat.DataSource = dtCurrentTable;
                gvKyLuat.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_KyLuat();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_KyLuat()
    {
        int rowIndex = 0;
        if (ViewState["KyLuat"] != null)
        {
            DataTable dt = (DataTable)ViewState["KyLuat"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    ngaythang.Text = dt.Rows[i]["NgayThang"].ToString();
                    drHinhThuc.SelectedValue = dt.Rows[i]["HinhThucID"].ToString();
                    lydo.Text = dt.Rows[i]["LyDo"].ToString();

                    rowIndex++;
                }
            }
        }
    }

    protected void gvKhenThuong_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvKhenThuong_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["KhenThuong"] != null)
        {
            DataTable dt = (DataTable)ViewState["KhenThuong"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    dt.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                    dt.Rows[i - 1]["HinhThucID"] = drHinhThuc.SelectedValue;
                    dt.Rows[i - 1]["LyDo"] = lydo.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["KhenThuong"] = dt;

            gvKhenThuong.DataSource = dt;
            gvKhenThuong.DataBind();

            SetPreviousData_KhenThuong();
        }
    }
    protected void gvKhenThuong_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drHinhThuc = (DropDownList)e.Row.FindControl("drHinhThuc");

                DataSet ds = new DataSet();
                DataTable dtb = new DataTable();

                string sql = "";

                sql = "SELECT 0 AS HinhThucID, N'<< Lựa chọn >>' AS TenHinhThuc UNION ALL (SELECT HinhThucKhenThuongID AS HinhThucID, TenHinhThucKhenThuong AS TenHinhThuc FROM tblDMHinhThucKhenThuong)";

                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                da.Fill(ds);
                dtb = ds.Tables[0];
                drHinhThuc.DataSource = dtb;
                drHinhThuc.DataBind();

            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void gvKyLuat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvKyLuat_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["KyLuat"] != null)
        {
            DataTable dt = (DataTable)ViewState["KyLuat"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    dt.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                    dt.Rows[i - 1]["HinhThucID"] = drHinhThuc.SelectedValue;
                    dt.Rows[i - 1]["LyDo"] = lydo.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["KyLuat"] = dt;

            gvKyLuat.DataSource = dt;
            gvKyLuat.DataBind();

            SetPreviousData_KyLuat();
        }
    }
    protected void gvKyLuat_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drHinhThuc = (DropDownList)e.Row.FindControl("drHinhThuc");

                DataSet ds = new DataSet();
                DataTable dtb = new DataTable();

                string sql = "";

                sql = "SELECT 0 AS HinhThucID, N'<< Lựa chọn >>' AS TenHinhThuc UNION ALL (SELECT HinhThucKyLuatID AS HinhThucID, TenHinhThucKyLuat AS TenHinhThuc FROM tblDMHinhThucKyLuat)";

                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                da.Fill(ds);
                dtb = ds.Tables[0];
                drHinhThuc.DataSource = dtb;
                drHinhThuc.DataBind();

            }
        }
        catch (Exception ex)
        {
        }
    }

    #endregion

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid();
    }

    protected void ButtonAddChungChi_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_ChungChi();
    }

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("ThoiGian", typeof(string)));
        dt.Columns.Add(new DataColumn("ChucVu", typeof(string)));
        dt.Columns.Add(new DataColumn("NoiLamViec", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["ThoiGian"] = "";
        dr["ChucVu"] = "";
        dr["NoiLamViec"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["QuaTrinh"] = dt;

        gvQuaTrinh.DataSource = dt;
        gvQuaTrinh.DataBind();
    }
    private void AddNewRowToGrid()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["QuaTrinh"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["QuaTrinh"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");

                        TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                        TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                        dtCurrentTable.Rows[i - 1]["ThoiGian"] = thoigian.Text;
                        dtCurrentTable.Rows[i - 1]["ChucVu"] = chucvu.Text;
                        dtCurrentTable.Rows[i - 1]["NoiLamViec"] = noilamviec.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["ThoiGian"] = "";
                    drCurrentRow["ChucVu"] = "";
                    drCurrentRow["NoiLamViec"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["ThoiGian"] = "";
                    dr["ChucVu"] = "";
                    dr["NoiLamViec"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["QuaTrinh"] = dtCurrentTable;

                gvQuaTrinh.DataSource = dtCurrentTable;
                gvQuaTrinh.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["QuaTrinh"] != null)
        {
            DataTable dt = (DataTable)ViewState["QuaTrinh"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                    thoigian.Text = dt.Rows[i]["ThoiGian"].ToString();
                    chucvu.Text = dt.Rows[i]["ChucVu"].ToString();
                    noilamviec.Text = dt.Rows[i]["NoiLamViec"].ToString();
                    rowIndex++;
                }
            }
        }
    }


    private void SetInitialRow_ChungChi()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));

        dt.Columns.Add(new DataColumn("SoChungChi", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayCap", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;

        dr["SoChungChi"] = "";
        dr["NgayCap"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["ChungChi"] = dt;

        gvChungChi.DataSource = dt;
        gvChungChi.DataBind();
    }
    private void AddNewRowToGrid_ChungChi()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["ChungChi"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["ChungChi"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values

                        TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                        TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                        dtCurrentTable.Rows[i - 1]["SoChungChi"] = sochungchi.Text;
                        dtCurrentTable.Rows[i - 1]["NgayCap"] = ngaycap.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;

                    drCurrentRow["SoChungChi"] = "";
                    drCurrentRow["NgayCap"] = "";

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;

                    dr["SoChungChi"] = "";
                    dr["NgayCap"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["ChungChi"] = dtCurrentTable;

                gvChungChi.DataSource = dtCurrentTable;
                gvChungChi.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_ChungChi();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_ChungChi()
    {
        int rowIndex = 0;
        if (ViewState["ChungChi"] != null)
        {
            DataTable dt = (DataTable)ViewState["ChungChi"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                    sochungchi.Text = dt.Rows[i]["SoChungChi"].ToString();
                    ngaycap.Text = dt.Rows[i]["NgayCap"].ToString();

                    rowIndex++;
                }
            }
        }
    }

    protected void gvQuaTrinh_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvQuaTrinh_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["QuaTrinh"] != null)
        {
            DataTable dt = (DataTable)ViewState["QuaTrinh"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                    dt.Rows[i - 1]["ThoiGian"] = thoigian.Text;

                    dt.Rows[i - 1]["ChucVu"] = chucvu.Text;
                    dt.Rows[i - 1]["NoiLamViec"] = noilamviec.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["QuaTrinh"] = dt;

            gvQuaTrinh.DataSource = dt;
            gvQuaTrinh.DataBind();

            SetPreviousData();
        }
    }


    protected void gvChungChi_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvChungChi_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["ChungChi"] != null)
        {
            DataTable dt = (DataTable)ViewState["ChungChi"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values

                    TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                    dt.Rows[i - 1]["SoChungChi"] = sochungchi.Text;
                    dt.Rows[i - 1]["NgayCap"] = ngaycap.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["ChungChi"] = dt;

            gvChungChi.DataSource = dt;
            gvChungChi.DataBind();

            SetPreviousData_ChungChi();
        }
    }

    protected void gvChungChi_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void CapnhatFile(int HoiVienId)
    {
        if (HoiVienId != null && HoiVienId != 0)
        {
            SqlHoiVienFileProvider HoiVien_provider = new SqlHoiVienFileProvider(connStr, true, "");

            HttpPostedFile styledfileupload;

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                FileUpload fileUp = (FileUpload)row.FindControl("FileUp");
                FileDinhKem1 = new HoiVienFile();
                if (fileUp != null && fileUp.FileName != "")
                {
                    int fileUploadId = 0;
                    int loaiFieldId = 0;
                    String sTempLoaiFileID = row.Cells[3].Text;
                    String sTempFileUploadID = row.Cells[4].Text;

                    if (!String.IsNullOrEmpty(sTempFileUploadID) && sTempFileUploadID != "0" && sTempFileUploadID != "&nbsp;")
                    {
                        fileUploadId = Convert.ToInt32(row.Cells[4].Text);

                    }
                    if (!String.IsNullOrEmpty(sTempLoaiFileID) && sTempLoaiFileID != "0" && sTempLoaiFileID != "&nbsp;")
                    {
                        loaiFieldId = Convert.ToInt32(row.Cells[3].Text);

                    }
                    string styledfileupload_name = "";
                    styledfileupload = fileUp.PostedFile;
                    if (styledfileupload.FileName != "")
                    {
                        styledfileupload_name = fileUp.FileName;


                        byte[] datainput = new byte[styledfileupload.ContentLength];
                        styledfileupload.InputStream.Read(datainput, 0, styledfileupload.ContentLength);

                        FileDinhKem1.FileId = fileUploadId;
                        FileDinhKem1.HoiVienCaNhanId = HoiVienId;
                        FileDinhKem1.LoaiGiayToId = loaiFieldId;
                        FileDinhKem1.FileDinhKem = datainput;
                        //FileDinhKem1.Tenfile = styledfileupload.FileName;
                        FileDinhKem1.TenFile = new System.IO.FileInfo(styledfileupload.FileName).Name;
                    }
                    if (FileDinhKem1.FileId == 0)
                    {
                        HoiVien_provider.Insert(FileDinhKem1);
                    }
                    else
                    {
                        HoiVien_provider.Update(FileDinhKem1);
                    }

                }
                else
                {

                }

            }


            loaduploadFileDinhKem(HoiVienId);

        }
    }


    protected void btnGhi_Click(object sender, EventArgs e)
    {
        conn.Open();

        string HoiVienCaNhanID = lbHoiVienCaNhanID.Text;

        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connStr, true, "");
        SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
        SqlKhenThuongKyLuatProvider KhenThuongKyLuat_provider = new SqlKhenThuongKyLuatProvider(connStr, true, "");
        SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connStr, true, "");

        string gioitinh = (rdNam.Checked) ? "1" : "0";

        // update bảng tblHoiVienCaNhan
        HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(HoiVienCaNhanID));

        hoivien.HoDem = txtHoDem.Text;
        hoivien.Ten = txtTen.Text;
        hoivien.GioiTinh = gioitinh;
        if (!string.IsNullOrEmpty(NgaySinh.Value))
            hoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
        hoivien.QuocTichId = int.Parse(drQuocTich.SelectedValue);

        if (!string.IsNullOrEmpty(Request.Form["TinhID_QueQuan"]))
            hoivien.QueQuanTinhId = Request.Form["TinhID_QueQuan"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["HuyenID_QueQuan"]))
            hoivien.QueQuanHuyenId = Request.Form["HuyenID_QueQuan"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["XaID_QueQuan"]))
            hoivien.QueQuanXaId = Request.Form["XaID_QueQuan"].ToString();

        hoivien.DiaChi = txtDiaChi.Text;
        if (!string.IsNullOrEmpty(Request.Form["TinhID_DiaChi"]))
            hoivien.DiaChiTinhId = Request.Form["TinhID_DiaChi"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["HuyenID_DiaChi"]))
            hoivien.DiaChiHuyenId = Request.Form["HuyenID_DiaChi"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["XaID_DiaChi"]))
            hoivien.DiaChiXaId = Request.Form["XaID_DiaChi"].ToString();

        hoivien.SoGiayChungNhanDkhn = txtSoGiayDHKN.Text;
        if (!string.IsNullOrEmpty(NgayCap_DHKN.Value))
            hoivien.NgayCapGiayChungNhanDkhn = DateTime.ParseExact(NgayCap_DHKN.Value, "dd/MM/yyyy", null);
        if (!string.IsNullOrEmpty(HanCapTu.Value))
            hoivien.HanCapTu = DateTime.ParseExact(HanCapTu.Value, "dd/MM/yyyy", null);
        if (!string.IsNullOrEmpty(HanCapDen.Value))
            hoivien.HanCapDen = DateTime.ParseExact(HanCapDen.Value, "dd/MM/yyyy", null);

        hoivien.SoChungChiKtv = txtSoChungChiKTV.Text;
        hoivien.NgayCapChungChiKtv = DateTime.ParseExact(NgayCap_ChungChiKTV.Value, "dd/MM/yyyy", null);

        hoivien.Email = txtEmail.Text;
        hoivien.DienThoai = txtDienThoai.Text;
        hoivien.Mobile = txtMobile.Text;
        if (drChucVu.SelectedValue != "0")
            hoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
        hoivien.TruongDaiHocId = int.Parse(drTotNghiep.SelectedValue);
        hoivien.ChuyenNganhDaoTaoId = int.Parse(drChuyenNganh.SelectedValue);
        hoivien.ChuyenNganhNam = txtNam_ChuyenNganh.Text;
        if (drHocVi.SelectedValue != "0")
            hoivien.HocViId = int.Parse(drHocVi.SelectedValue);
        hoivien.HocViNam = txtNam_HocVi.Text;
        if (drHocHam.SelectedValue != "0")
            hoivien.HocHamId = int.Parse(drHocHam.SelectedValue);
        hoivien.HocHamNam = txtNam_HocHam.Text;
        hoivien.SoCmnd = txtSoCMND.Text;
        if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
            hoivien.CmndNgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);
        hoivien.CmndTinhId = int.Parse(drNoiCap_CMND.SelectedValue);
        hoivien.SoTaiKhoanNganHang = txtSoTKNganHang.Text;
        if (drNganHang.SelectedValue != "0")
            hoivien.NganHangId = int.Parse(drNganHang.SelectedValue);
        if (!string.IsNullOrEmpty(drDonVi.SelectedValue))
            hoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
        if (drSoThich.SelectedValue != "0")
            hoivien.SoThichId = int.Parse(drSoThich.SelectedValue);

        HoiVienCaNhan_provider.Update(hoivien);

        // insert chứng chỉ
        string sql = "DELETE tblHoiVienCaNhanChungChiQuocTe WHERE HoiVienCaNhanId = " + HoiVienCaNhanID;
        SqlCommand cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        for (int i = 0; i < gvChungChi.Rows.Count; i++)
        {

            //DropDownList drChungChi = (DropDownList)gvChungChi.Rows[i].Cells[1].FindControl("drChungChi");
            TextBox sochungchi = (TextBox)gvChungChi.Rows[i].Cells[1].FindControl("txtSoChungChi");
            TextBox ngaycap = (TextBox)gvChungChi.Rows[i].Cells[2].FindControl("txtDate");

            //if (drChungChi.SelectedValue != "0")
            if (!string.IsNullOrEmpty(sochungchi.Text))
            {
                HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                chungchi.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);

                chungchi.SoChungChi = sochungchi.Text;
                if (!string.IsNullOrEmpty(ngaycap.Text))
                    chungchi.NgayCap = DateTime.ParseExact(ngaycap.Text, "dd/MM/yyyy", null);

                HoiVienCaNhanChungChi_provider.Insert(chungchi);
            }
        }


        // insert quá trình làm việc
        sql = "DELETE tblHoiVienCaNhanQuaTrinhCongTac WHERE HoiVienCaNhanId = " + HoiVienCaNhanID;
        cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        for (int i = 0; i < gvQuaTrinh.Rows.Count; i++)
        {

            TextBox thoigian = (TextBox)gvQuaTrinh.Rows[i].Cells[1].FindControl("txtThoiGian");
            TextBox chucvu = (TextBox)gvQuaTrinh.Rows[i].Cells[2].FindControl("txtChucVu");
            TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[i].Cells[3].FindControl("txtNoiLamViec");

            HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
            quatrinh.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);
            quatrinh.ThoiGianCongTac = thoigian.Text;
            quatrinh.ChucVu = chucvu.Text;
            quatrinh.NoiLamViec = noilamviec.Text;

            HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
        }

        // Khen thưởng
        sql = "DELETE tblKhenThuongKyLuat WHERE Loai = 1 AND HoiVienCaNhanId = " + HoiVienCaNhanID;
        cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        for (int i = 0; i < gvKhenThuong.Rows.Count; i++)
        {
            TextBox ngaythang = (TextBox)gvKhenThuong.Rows[i].Cells[1].FindControl("txtNgayThang");
            DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[i].Cells[2].FindControl("drHinhThuc");
            TextBox lydo = (TextBox)gvKhenThuong.Rows[i].Cells[3].FindControl("txtLyDo");

            if (drHinhThuc.SelectedValue != "0")
            {
                KhenThuongKyLuat khenthuong = new KhenThuongKyLuat();
                khenthuong.HinhThucKhenThuongId = int.Parse(drHinhThuc.SelectedValue);
                khenthuong.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);
                if (!string.IsNullOrEmpty(ngaythang.Text))
                    khenthuong.NgayThang = DateTime.ParseExact(ngaythang.Text, "dd/MM/yyyy", null);
                khenthuong.Loai = "1";
                khenthuong.LyDo = lydo.Text;

                KhenThuongKyLuat_provider.Insert(khenthuong);
            }
        }

        // Kỷ luật
        sql = "DELETE tblKhenThuongKyLuat WHERE Loai = 2 AND HoiVienCaNhanId = " + HoiVienCaNhanID;
        cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        for (int i = 0; i < gvKyLuat.Rows.Count; i++)
        {
            TextBox ngaythang = (TextBox)gvKyLuat.Rows[i].Cells[1].FindControl("txtNgayThang");
            DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[i].Cells[2].FindControl("drHinhThuc");
            TextBox lydo = (TextBox)gvKyLuat.Rows[i].Cells[3].FindControl("txtLyDo");

            if (drHinhThuc.SelectedValue != "0")
            {
                KhenThuongKyLuat kyluat = new KhenThuongKyLuat();
                kyluat.HinhThucKyLuatId = int.Parse(drHinhThuc.SelectedValue);
                kyluat.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);
                kyluat.Loai = "2";
                if (!string.IsNullOrEmpty(ngaythang.Text))
                    kyluat.NgayThang = DateTime.ParseExact(ngaythang.Text, "dd/MM/yyyy", null);

                kyluat.LyDo = lydo.Text;

                KhenThuongKyLuat_provider.Insert(kyluat);
            }
        }

        // file đính kèm
        CapnhatFile(int.Parse(HoiVienCaNhanID));

        string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;width:60%;margin-left:209px;margin-bottom:-10px;"">
							<p>" + "Bạn đã cập nhật hồ sơ thành công! Xin cảm ơn!" + @"</p>							
						</div>";
        placeMessage.Controls.Add(new LiteralControl(thongbao));

        conn.Close();
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("");
    }

}