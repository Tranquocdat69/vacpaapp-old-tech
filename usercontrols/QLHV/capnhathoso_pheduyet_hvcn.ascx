﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="capnhathoso_pheduyet_hvcn.ascx.cs" Inherits="usercontrols_capnhathoso_pheduyet_hvcn" %>

<asp:PlaceHolder ID="placeMessage" runat="server"></asp:PlaceHolder>
<div class="widgetbox">
    <h4 class="widgettitle">
        <asp:Label ID="lbTitle" runat="server" Text="Yêu cầu thay đổi hồ sơ"></asp:Label><a class="close">×</a>
        <a class="minimize">–</a></h4>
    <div class="widgetcontent">
        <form method="post" enctype="multipart/form-data" name="form_nhaphoso" id="form_nhaphoso"
        runat="server" clientidmode="Static">
        <div id="thongbaoloi_form_nhaphoso" name="thongbaoloi_form_nhaphoso" style="display: none"
            class="alert alert-error">
        </div>
        <asp:ScriptManager ID="s1" runat="server">
        </asp:ScriptManager>
        <table id="Table1" width="100%" border="0" class="formtbl" runat="server">
            
            <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Thông tin chung</h6>
                        <asp:Label ID="lbEmail" runat="server" Visible="false"></asp:Label>
                </td>
                
            </tr>
            <tr>
                <td style="width:25%">
                    <label>Mã yêu cầu:</label>
                   
                </td>
                <td style="width:30%">
                    
                    
                    <asp:TextBox ID="txtMaYeuCau" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                    
                </td>
                <td>
                    <label>Ngày yêu cầu:</label>
                    
                </td>
                <td colspan="2">
                    
                    <asp:TextBox ID="txtNgayYeuCau" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                   
                </td>
            </tr>
            <tr>
                <td>
                    <label>ID:</label>
                </td>
                <td>
                    <asp:TextBox ID="txtMaHoiVienCaNhan" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                </td>
                
                <td colspan="3">
                   
                </td>
            </tr>
            

            <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Thông tin thay đổi</h6>
                </td>
                
            </tr>
         
            <tr>
                <td>
                    <label>Học vị:</label>
                </td>
                <td>
                    <asp:TextBox ID="txtHocVi" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label>Năm:</label>
                </td>
                <td colspan="2">
                    
                    <asp:TextBox ID="txtNam_HocVi" runat="server" Width="30%" ReadOnly="true"></asp:TextBox>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <label>Học hàm:</label>
                </td>
                <td>
                    <asp:TextBox ID="txtHocHam" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label>Năm:</label>
                </td>
                <td colspan="2">
                    
                    <asp:TextBox ID="txtNam_HocHam" runat="server" Width="30%" ReadOnly="true"></asp:TextBox>
                    
                </td>
            </tr>
            
            <tr>
                <td>
                    <label>Số giấy chứng nhận ĐKHNKiT:</label>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtSoGiayDHKN" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                    
                </td>
                <td>
                    <label>Ngày cấp:</label>
                </td>
                <td colspan="2">
                    
                    <input name="NgayCap_DHKN" type="text" id="NgayCap_DHKN" runat="server" clientidmode="Static" readonly="readonly" />  
                    
                </td>
            </tr>
            <tr>
                <td>
                    <label>Hạn cấp:</label>
                    
                </td>
                <td>
                   <input name="HanCapTu" type="text" id="HanCapTu" runat="server" clientidmode="Static" style="width:47%;" readonly="readonly" />&nbsp;-&nbsp;<input name="HanCapDen" type="text" id="HanCapDen" runat="server" clientidmode="Static" style="width:46%;" readonly="readonly" />
                </td>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label>Chức vụ hiện nay:</label>
                </td>
                <td>
                    <asp:TextBox ID="txtChucVu" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                </td>
                
                <td colspan="3">
                    
                </td>
            </tr>
            <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Chứng chi quốc tế</h6>
                </td>
                
            </tr>
            <tr>
                <td colspan="2">
                
                            <asp:GridView ID="gvChungChi" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="display"
                                
                                >
                                <Columns>
                                    <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    
                                    <asp:BoundField DataField="SoChungChi" HeaderText="Số chứng chỉ" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="NgayCap" HeaderText="Ngày cấp" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                                    
                                </Columns>
                            </asp:GridView>
                            
                        
                </td>
                <td colspan="3">
                    
                </td>
            </tr>
            <tr class="trbgr">
                <td colspan="2">
                    <h6>
                        Khen thưởng</h6>
                </td>
                <td colspan="3">
                    <h6>
                        Kỷ luật</h6>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                
                            <asp:GridView ID="gvKhenThuong" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="display"
                                
                                >
                                <Columns>
                                   <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenHinhThuc" HeaderText="Hình thức khen thưởng" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="LyDo" HeaderText="Lý do" ItemStyle-HorizontalAlign="Center" />
                               
                                </Columns>
                            </asp:GridView>
                            
                        
                </td>
                <td colspan="3">
                    <asp:GridView ID="gvKyLuat" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="display"
                                
                                >
                                <Columns>
                                   <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <asp:BoundField DataField="NgayThang" HeaderText="Ngày tháng" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="TenHinhThuc" HeaderText="Hình thức kỷ luật" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="LyDo" HeaderText="Lý do" ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                            </asp:GridView>
                </td>
            </tr>
           
        <tr>
          <td class="trbgr" colspan="5" ><h6>Bằng chứng xác nhận 
              </h6></td>
        </tr>
        <tr><td colspan="5" >
         <a href="/usercontrols/QLHV/Download_CapNhatHoSoHVCN.ashx?id=<%=CapNhatHoSoID %>"><%=TenFileBangChung %></a>
        </td></tr>
       <tr>
          <td class="trbgr" colspan="5" ><h6>Lý do từ chối
              </h6></td>
        </tr>
        <tr><td colspan="5" >
         <asp:TextBox ID="txtLyDoTuChoi" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
         <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtLyDoTuChoi" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập." ValidationGroup="TuChoi"></asp:RequiredFieldValidator>
        </td></tr>
            
            <tr>
                <td colspan="5" align="center">
                    <p>
                        &nbsp;</p>

                        <% if (capnhat.TinhTrangId != 5)
                           { %>
                <a class="btn btn-rounded"><i class="iconfa-plus-sign"></i>
                        <asp:Button ID="btnDuyet" Text="Duyệt" runat="server" ClientIDMode="Static"
                  onclick="btnDuyet_Click" />
                    </a>
            <a  class="btn btn-rounded"><i class="iconfa-ok"></i>
            <asp:Button ID="Btn_CapNhat" Text="Từ chối" runat="server" ValidationGroup="TuChoi" 
                  onclick="btnTuChoi_Click" /> </a>
                  <% } %>
                    <a class="btn btn-rounded"><i class="iconfa-minus-sign"></i>
                        <input type="button" id="xemhoso" value="Xem hồ sơ" onclick="window.location.href = window.location.href.split('?')[0] + '?page=hosohoiviencanhan&id=<%=Request.QueryString["id"] %>'" />
                    </a>
                   <a class="btn btn-rounded"><i class="iconfa-minus-sign"></i>
                        <input type="button" id="quaylai" value="Quay lại" onclick="window.location.href = window.location.href.split('?')[0] + '?page=capnhathoso'" />
                    </a>
                </td>
            </tr>
        </table>
        </form>
    </div>
</div>
