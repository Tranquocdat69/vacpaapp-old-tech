﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="xoatenhoivientapthe.ascx.cs" Inherits="usercontrols_xoatenhoivientapthe" %>


<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 
 <style>
        .ui-datepicker 
        {
            z-index: 99999999 !important;
        }
        
    </style>
   <form id="Form1" runat="server">
   <h4 class="widgettitle">Danh sách xóa tên hội viên</h4>
   <div>
        <div  class="dataTables_length">
        
        <a id="btn_them" href="#none" class="btn btn-rounded"   onclick="OpenDanhSachHoiVien();"  ><i class="iconfa-plus-sign"></i> Thêm</a>
        <a id="btn_tiepnhan" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected ,'tiepnhan'  );"><i class="iconfa-plus-sign"></i> Tiếp nhận</a>
        <a id="btn_tuchoitiepnhan" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected , 'tuchoitiepnhan' );"><i class="iconfa-plus-sign"></i> Từ chối tiếp nhận</a>
        <a id="btn_duyetdon" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected , 'duyetdon' );"><i class="iconfa-plus-sign"></i> Duyệt</a>
        <a id="btn_tuchoiduyet" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected ,'tuchoiduyet' );"><i class="iconfa-plus-sign"></i> Từ chối duyệt</a>
        <%--<a id="btn_thoaiduyet" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected , 'thoaiduyet' );"><i class="iconfa-plus-sign"></i>Thoái duyệt</a>--%>
        
       <%-- <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="confirm_delete_user(Users_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>--%>
        <a href="#none" class="btn btn-rounded" onclick="open_user_search();"><i class="iconfa-search"></i> Tìm kiếm</a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
  
        </div>
        
   </div>
    
<asp:GridView ClientIDMode="Static" ID="Users_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="Users_grv_PageIndexChanging" onsorting="Users_grv_Sorting"    
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval("DinhDanh")%>" />
                  </ItemTemplate>
              </asp:TemplateField>
            <asp:TemplateField  HeaderText="ID"   SortExpression="TenDangNhap" >
              <ItemTemplate>
                 <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:open_user_edit(" + DataBinder.Eval(Container.DataItem, "HoiVienID").ToString() + ",2,"+ DataBinder.Eval(Container.DataItem, "TinhTrangID").ToString()+","+ DataBinder.Eval(Container.DataItem, "XoaTenHoiVienID").ToString()  +"); "%>  Text= '<%# Eval("TenDangNhap") %>'  runat="server">
                    
                </asp:HyperLink>
        
             </ItemTemplate>
             <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
             <ItemStyle   HorizontalAlign="Center"     />
          </asp:TemplateField>

           
              <asp:BoundField DataField="TenKhachHang" HeaderText="Tên Doanh nghiệp" 
                  SortExpression="TenKhachHang" />
                   <asp:BoundField DataField="LyDoXoaTen" HeaderText="Lý do xóa tên" 
                  SortExpression="LyDoXoaTen" />
              

              <asp:BoundField DataField="NgayGiaNhap" HeaderText="Ngày kết nạp" 
                  SortExpression="NgayGiaNhap" DataFormatString="{0:dd/MM/yyyy}" />
              <asp:BoundField DataField="NgayXoaTen" HeaderText="Ngày xóa tên" 
                  SortExpression="NgayXoaTen" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="TinhTrang" HeaderText="Trạng thái" 
                  SortExpression="TinhTrang" />    
              <asp:BoundField DataField="GhiChuXoaTen" HeaderText="Ghi chú" 
                  SortExpression="GhiChuXoaTen" />
                  
              <asp:TemplateField HeaderText='' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                      <a onclick="open_user_edit(<%# Eval("HoiVienID")%>,1,<%# Eval("TinhTrangID")%>,<%# Eval("XoaTenHoiVienID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xem"  rel="tooltip" class="btn"><i class="iconsweets-trashcan2" ></i></a>
                      <%#(Eval("LoaiXoaTen").ToString() != "0" && Eval("TinhTrangID").ToString() == "3") ? "<a onclick='confirm_delete_user(" + Eval("XoaTenHoiVienID") + ");' data-placement='top' data-rel='tooltip' href='#none' data-original-title='Xóa'  rel='tooltip' class='btn'><i class='iconsweets-trashcan' ></i></a>" : ""%>
                  
                  
                  </div>
                  </ItemTemplate>
              </asp:TemplateField>


          </Columns>
</asp:GridView>


<div class="dataTables_info" id="dyntable_info" >
<div class="pagination pagination-mini">Chuyển đến trang:
<ul>
 
<li><a href="#" onclick="$('#tranghientai').val(0); $('#user_search').submit();"><< Đầu tiên</a></li>
<li><a href="#" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager').val()-1); $('#user_search').submit();}" >< Trước</a></li>
<li><a style=" border: none; background-color:#eeeeee">
<asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server"
style=" width:55px; height:22px; "
onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
</asp:DropDownList></a> </li>
<li style="margin-left:5px; "><a href="#" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) {$('#tranghientai').val(parseInt($('#Pager').val())+1); $('#user_search').submit();} ;" style="border-left: 1px solid #ccc;">Sau ></a></li>
<li><a href="#" onclick="$('#tranghientai').val($('#Pager option').length-1); $('#user_search').submit();">Cuối cùng >></a></li>
</ul>
</div>
</div>


<%--
<div class="dataTables_info" id="dyntable_info">Chuyển đến trang: 

<asp:DropDownList ID="Pager" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList>

</div>
--%>

</form>





<script type="text/javascript">

<% annut(); %>
    // Array ID được check
    var Users_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#Users_grv .checkall").bind("click", function () {
            Users_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#Users_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });

        $('#Users_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(Users_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa hội viên
    function confirm_delete_user(idxoa) {
     
        $("#div_user_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                 
                 window.location = 'admin.aspx?page=xoatenhoivientapthe&act=xoa&id=' + idxoa;
                
                   
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Sửa thông tin hội viên
    function open_user_edit(id, loai, idtinhtrang, xoatenhoivienid ) {
          var timestamp = Number(new Date());
        if(loai==1)
        {
            openInNewTab("admin.aspx?page=hosohoiviencanhan&id=" + id + "&act=edit&time=" + timestamp);
        }else{
            openInNewTab("admin.aspx?page=hosohoivientapthe_edit&prepage=xoatenhoivientapthe&id=" + id + "&xoatenhoivienid=" + xoatenhoivienid + "&idtinhtrang=" + idtinhtrang + "&act=edit&time=" + timestamp);
        }
      }


    // Xem thông tin hội viên
    function open_user_view(id,loai) {
        var timestamp = Number(new Date());
       if(loai==1)// ca nhan
        {
           openInNewTab("admin.aspx?page=hoiviencanhan_edit&id=" + id + "&act=view&time=" + timestamp);
        }else{ // tap the
           openInNewTab("admin.aspx?page=ketnaphoivientapthe_edit&id=" + id + "&act=view&time=" + timestamp);
        }
     
//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=ketnaphoivientapthe_edit&id=" + id + "&mode=view&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Xem thông tin đơn hội viên</b>",
//            modal: true,
//            buttons: {

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



    // mở form thêm hội viên tap the
    function open_user_add() {

  
        var timestamp = Number(new Date());
      window.location = "admin.aspx?page=ketnaphoivientapthe_edit&act=add&time=" + timestamp

//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=ketnaphoivientapthe_edit&mode=iframe&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Thêm đơn gia nhập hội viên mới</b>",
//            modal: true,
//            close: function (event, ui) { window.location = 'admin.aspx?page=ketnaphoivientapthe'; },
//            buttons: {

//                "Ghi": function () {
//                    window.frames['iframe_user_add'].submitform();
//                   // $("#iframe_user_add")[0].contentWindow.submitform();
//                   
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
//   
   
    }
      // mở form thêm hội viên ca nhan
    function open_hvcn_add() {

  
        var timestamp = Number(new Date());
      window.location = "admin.aspx?page=hoiviencanhan_add&act=add&time=" + timestamp
       
    }

      //  danh sach cho tiep nhan online
    function tiepnhanonline_hv( ) {
         
        var timestamp = Number(new Date());
      window.location = "admin.aspx?page=ketnaphoivien&act=online&time=" + timestamp
    }
    
   function xulydon_hv(lstid , act ) {
   
            $("#div_"+act).dialog({
            resizable: false,
            width: 400,
            height:200,
            modal: true,
            buttons: {
                "Đồng ý": function () {
                var lydo;
                if(act=='tuchoitiepnhan')
                {  
                   lydo= $('#LyDoTuChoiDon').val();
                }else if(act=='tuchoiduyet'){
                   lydo= $('#LyDoTuChoiDuyet').val();
                }
              
                 window.location = 'admin.aspx?page=xoatenhoivientapthe&act='+act+'&lydo='+lydo+'&id=' + lstid;
                   
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $("#div_"+act).parent().find('button:contains("Đồng ý")').addClass('btn btn-rounded').prepend('<i class="icon-ok"> </i>&nbsp;');
        $("#div_"+act).parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
  
     

    }

     
    // Tìm kiếm
    function open_user_search() {
        var timestamp = Number(new Date());

        $("#div_user_search").dialog({
            resizable: true,
           width: 765,
            height: 440,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Tìm kiếm thông tin đơn xóa tên hội viên</b>",
            modal: true,
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_user_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('#Users_grv').length > 0) jQuery('#Users_grv').tooltip({ selector: "a[rel=tooltip]" });


</script>

<div id="div_user_add" >
</div>

<div id="div_user_search"   style="display:none"    >
<form id="user_search" method="post" enctype="multipart/form-data" action="">
  <table  width="100%" border="0" class="formtbl"  >
    
    <tr>
        <td ><label>Trạng thái: </label></td>
       
          <td  colspan="3" >  
              <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" > <input id="tk_TTAll" name="tk_TTAll" type="checkbox"  class="input-xlarge checkall"  value="true"  /><label  style="width:100px" >Tất cả</label> 
                 </td>
          <td style="width:130px" >  <input id="tk_TTChoTiepNhan" name="tk_TTChoTiepNhan" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Chờ tiếp nhận</label> 
                 </td>
          <td style="width:130px" > <input id="tk_TTChoDuyet" name="tk_TTChoDuyet" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Chờ duyệt</label> 
      </td>
          <td style="width:130px" ></td>
         </tr>
        </table>
                
                   </td>
       
    </tr>
    <tr>
        <td></td>
         <td  colspan="3" >  
           <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" >   <input id="tk_TTTuChoiTiepNhan" name="tk_TTTuChoiTiepNhan" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Từ chối tiếp nhận </label> 
              </td>
          <td style="width:130px" >  <input id="tk_TTTuChoiDuyet" name="tk_TTTuChoiDuyet" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Từ chối duyệt</label> 
                </td>
          <td style="width:130px" >   <input id="tk_TTDaDuyet" name="tk_TTDaDuyet" type="checkbox"  class="checkbox" value="true"   /><label  style="width:100px" >Đã duyệt</label> 
            </td>
          <td style="width:130px" >  
      </td>
         </tr>
        </table>
      
                </td>
    </tr>  
      <tr>
        <td><label>Điều kiện xóa tên:</label></td>
        <td> 
          <select  style="width:100%"  name="tk_LoaiXoaTen" id="tk_LoaiXoaTen">
            <option value="">Tất cả</option>
            <option value="0">HV nộp đơn xin thôi tham gia online</option>
            <option value="1">Hội viên không nộp phí 3 năm</option>
            <option value="2">Lý do khác</option>
         </select>
          </td>
         <td>  </td>
 
         <td>
           
          
             </td>
        
         
    </tr>

    <tr>
        <td><label>Tên doanh nghiệp:</label></td>
        <td> 
         <input type="text" name="tk_TenHoiVien" id="tk_TenHoiVien" class="input-xlarge">  </td>
         <td><label>ID:</label></td>
 
         <td>
           
            <input type="text" name="tk_ID" id="tk_ID" class="input-xlarge">  
             </td>
        
         
    </tr>
    <tr>
        <td><label>Người đại diện:</label></td>
        <td>  
         <input type="text" name="tk_NguoiDaiDienPL" id="tk_NguoiDaiDienPL" class="input-xlarge">   </td>
         <td><label>Loại hình doanh nghiệp:</label></td>
 
         <td>  
          <select id="tk_LoaiHinhDN" style="width:100%"  name="tk_LoaiHinhDN">        
             <%  
                     try
                     {
                         Load_LoaiHinhDoanhNghiep();
                     }
                     catch
                     {

                     }
              
              %>  
    

</select>
           <%--<asp:DropDownList ID="tk_LoaiHinhDN" DataTextField="TenLoaiHinhDoanhNghiep" 
                      DataValueField="LoaiHinhDoanhNghiepID" runat="server" Width="228px" Height="30px">
                      </asp:DropDownList>--%>
         <%-- <select  style="width:100%"  name="tk_LoaiHinhDN" id="tk_LoaiHinhDN">
            <option value="">Tất cả</option>
            <option value="2">Nộp trực tiếp</option>
            <option value="1">Nộp trực tuyến</option>
         </select>--%>
             
         </td>
        
        
         
    </tr>
    <tr>
        <td><label>Tỉnh thành:</label></td>
        <td>  <select  style="width:100%"  name="TinhID_ToChuc" id="TinhID_ToChuc">
              <% cm.Load_ThanhPho("00"); %>
       </select> 
         </td>
         <td><label>Quận huyện:</label></td>
 
         <td>  
              
    <select id="HuyenID_ToChuc" style="width:100%"  name="HuyenID_ToChuc">        
             <%  
                     try
                     {
                         cm.Load_QuanHuyen("000", "00");
                     }
                     catch
                     {

                     }
              
              %>  
    

</select>
    
         </td>
        
         
    </tr>
     <tr>
        <td><label>Ngày kết nạp:</label></td>
        <td>  
         <input  style="width:100px" type="text"  name="tk_NgayNopTu" id="tk_NgayNopTu" class="input-xlarge">  - <input style="width:100px"  type="text" name="tk_NgayNopDen" id="tk_NgayNopDen" class="input-xlarge">    </td>
       
        
         <td><label>Số giấy CN ĐKKD:</label></td>
 
         <td>  
               <input type="text" name="tk_SoDKKD" id="tk_SoDKKD" class="input-xlarge">
         </td>
         
         
    </tr>

      <tr>
        <td><label>Ngày xóa tên:</label></td>
        <td> 
        <input  style="width:100px" type="text" name="tk_NgayPheDuyetTu" id="tk_NgayPheDuyetTu" class="input-xlarge"> - <input  style="width:100px" type="text" name="tk_NgayPheDuyetDen" id="tk_NgayPheDuyetDen" class="input-xlarge">  </td>
       
          <td> </td>
 
         <td>   
          </td>
       
        
       
         
    </tr>
     

    

</table>
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
</form>
</div>


<script language="javascript">
 
// 
//    $('#tk_HVCN').prop('checked',  '<%=Request.Form["tk_HVCN"]%>' );
//    $('#tk_HVTT').prop('checked',   '<%=Request.Form["tk_HVTT"]%>' );
//    $('#tk_TTAll').prop('checked',  '<%=Request.Form["tk_TTAll"]%>' );
//     $('#tk_TTChoTiepNhan').prop('checked',  '<%=Request.Form["tk_TTChoTiepNhan"]%>' );
//    $('#tk_TTChoDuyet').prop('checked',  '<%=Request.Form["tk_TTChoDuyet"]%>' );
//    $('#tk_TTTuChoiTiepNhan').prop('checked',  '<%=Request.Form["tk_TTTuChoiTiepNhan"]%>' );
//     $('#tk_TTTuChoiDuyet').prop('checked',  '<%=Request.Form["tk_TTTuChoiDuyet"]%>' );
//    $('#tk_TTDaDuyet').prop('checked',  '<%=Request.Form["tk_TTDaDuyet"]%>' );
//    $('#tk_TTThoaiDuyet').prop('checked',  '<%=Request.Form["tk_TTThoaiDuyet"]%>' );
//     $('#tk_TenHoiVien').val('<%=Request.Form["tk_TenHoiVien"]%>');
//    $('#tk_ID').val('<%=Request.Form["tk_ID"]%>');
//    $('#tk_SoDon').val('<%=Request.Form["tk_SoDon"]%>');
//     $('#tk_HinhThucNop').val('<%=Request.Form["tk_HinhThucNop"]%>');
//    $('#TinhID_ToChuc').val('<%=Request.Form["TinhID_ToChuc"]%>');
//    $('#HuyenID_ToChuc').val('<%=Request.Form["HuyenID_ToChuc"]%>');
//     $('#tk_NgayNopTu').val('<%=Request.Form["tk_NgayNopTu"]%>');
//      $('#tk_NgayNopDen').val('<%=Request.Form["tk_NgayNopDen"]%>');
//    $('#tk_CTKiemToan').val('<%=Request.Form["tk_CTKiemToan"]%>');
//    $('#tk_NgayPheDuyetTu').val('<%=Request.Form["tk_NgayPheDuyetTu"]%>');
//     $('#tk_NgayPheDuyetDen').val('<%=Request.Form["tk_NgayPheDuyetDen"]%>');
//     $('#tk_SoChungChiKTV').val('<%=Request.Form["tk_SoChungChiKTV"]%>');
//    $('#tk_CNDKHNKT').val('<%=Request.Form["tk_CNDKHNKT"]%>');
//    $('#tk_SoDKKD').val('<%=Request.Form["tk_SoDKKD"]%>');
  
      <% cm.chondiaphuong_script("ToChuc",0); %>

         $.datepicker.setDefaults($.datepicker.regional['vi']);


    $(function () {
        $("#tk_NgayNopTu").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"    ,
             onClose: function (selectedDate) {
               
                 $("#tk_NgayNopDen").datepicker("option", "minDate", selectedDate);

              }
                    
        }) ;

         $("#tk_NgayNopDen").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"     ,
             onClose: function (selectedDate) {
              
                $("#tk_NgayNopTu").datepicker("option", "maxDate", selectedDate);

            }
                 
        }) ;
         $("#tk_NgayPheDuyetTu").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"  ,
             onClose: function (selectedDate) {
               
                 $("#tk_NgayPheDuyetDen").datepicker("option", "minDate", selectedDate);

              }        
        }) ;
         $("#tk_NgayPheDuyetDen").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"       ,
             onClose: function (selectedDate) {
              
                $("#tk_NgayPheDuyetTu").datepicker("option", "maxDate", selectedDate);

            }     
        }) ;

    });
     
    $(document).ready(function() {
            $('#tk_TTAll').click(function(event) {  //on click 
                if(this.checked) { // check select status
                   $('.checkbox').each(function() { //loop through each checkbox
             
                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                    }); 
          
        
                }else{
                      //$('#tk_TTChoTiepNhan').checked = false;
                        $('#tk_TTChoTiepNhan').prop('checked',  false );
                
                        $('#tk_TTChoDuyet').prop('checked',  false );
                        $('#tk_TTTuChoiTiepNhan').prop('checked',  false );
                         $('#tk_TTTuChoiDuyet').prop('checked',  false );
                        $('#tk_TTDaDuyet').prop('checked',  false );
                        $('#tk_TTThoaiDuyet').prop('checked',  false );

        //               $('.checkbox').each(function() { //loop through each checkbox
        //                  this.checkbox = false; //deselect all checkboxes with class "checkbox1"                       
        //               });   
                
                }
            });

            $( '.checkbox' ).bind( "click", function() {
                  if(this.checked==false) {
                 
                       $('#tk_TTAll').prop('checked',  false );
                    }  else{
                        
                       if($('#tk_TTChoTiepNhan').is(':checked')==true && $('#tk_TTChoDuyet').is(':checked')==true &&
                          $('#tk_TTTuChoiTiepNhan').is(':checked')==true &&$('#tk_TTTuChoiDuyet').is(':checked')==true &&
                          $('#tk_TTDaDuyet').is(':checked')==true &&$('#tk_TTThoaiDuyet').is(':checked')==true ){
                       
                          $('#tk_TTAll').prop('checked',  true );
                       }
                    
                    }  
               });
         });


  function OpenDanhSachHoiVien() {
        $("#div_xoaten_add").empty();
        $("#div_xoaten_add").append($("<iframe width='100%' height='100%' id='ifDanhSachLopHoc' name='ifDanhSachHoiVien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=xoatenhoivientapthe_add"));
        $("#div_xoaten_add").dialog({
            resizable: true,
            width:1200,
            height:650,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách hội viên</b>",
            modal: true,
            zIndex: 1000
        });
    }

     function CloseFormDanhSachKhachHang() {
        $("#div_xoaten_add").dialog('close');
         
    }


 </script>
 <div id="div_xoaten_add">
</div>
 
<div id="div_user_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?(Hệ thống chỉ xóa các đơn chưa được phê duyệt)</p>

</div>

<div id="div_tuchoitiepnhan" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận từ chối yêu cầu</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Từ chối (các) yêu cầu được chọn?(Hệ thống chỉ từ chối các đơn đang chờ tiếp nhận)</p>
   <table  width="100%" border="0" class="formtbl"  >
    <tr> 
        <td  style="width:100px" ><label>Lý do từ chối: </label></td>
         <td  colspan="3" >  
           <input type="text" name="LyDoTuChoiDon" id="LyDoTuChoiDon" class="input-xlarge"/> 
                
         </td>
        
      
    </tr>
    
    </table>
</div>

<div id="div_tiepnhan" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận trình lãnh đạo phê duyệt</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Trình phê duyệt (các) yêu cầu được chọn?(Hệ thống chỉ trình phê duyệt các yêu cầu chờ tiếp nhận)</p>

</div>

<div id="div_duyetdon" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận duyệt yêu cầu hội viên</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Duyệt (các) yêu cầu được chọn?(Hệ thống chỉ duyệt các yêu cầu chờ phê duyệt)</p>

</div>

<div id="div_tuchoiduyet" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận từ chối yêu cầu</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Từ chối (các) yêu cầu được chọn?(Hệ thống chỉ từ chối các yêu cầu chờ phê duyệt)</p>
    <table  width="100%" border="0" class="formtbl"  >
    <tr> 
        <td  style="width:100px" ><label>Lý do từ chối: </label></td>
         <td  colspan="3" >  
           <input type="text" name="LyDoTuChoiDuyet" id="LyDoTuChoiDuyet" class="input-xlarge"/> 
                
         </td>
        
      
    </tr>
    
    </table>
</div>


<div id="div_thoaiduyet" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận thoái duyệt yêu cầu</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Thoái duyệt (các) yêu cầu được chọn?(Hệ thống chỉ thoái duyệt các yêu cầu đã phê duyệt)</p>
   <%-- <table  width="100%" border="0" class="formtbl"  >
    <tr> 
        <td  style="width:100px" ><label>Lý do từ chối: </label></td>
         <td  colspan="3" >  
           <input type="text" name="LyDoTuChoiDuyet" id="Text1" class="input-xlarge"/> 
                
         </td>
        
      
    </tr>
    
    </table>--%>
</div>

