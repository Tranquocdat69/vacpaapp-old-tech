﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="nguoiquantam_add.ascx.cs" Inherits="usercontrols_nguoiquantam_add" %>
<style>
    .require
    {
        color: Red;
    }
    .error
    {
        color: Red;
        font-size: 11px;
    }
    .Hide
    {
      display:none;
    }
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript" src="js/chosen.jquery.min.js"></script>


<asp:PlaceHolder ID="placeMessage" runat="server"></asp:PlaceHolder>
<div class="widgetbox">
    <h4 class="widgettitle">
        <asp:Label ID="lbTitle" runat="server" Text="Đăng ký người quan tâm"></asp:Label><a class="close">×</a>
        <a class="minimize">–</a></h4>
    <div class="widgetcontent">
        <form method="post" enctype="multipart/form-data" name="form_nhaphoso" id="form_nhaphoso"
        runat="server" clientidmode="Static">
        <div id="thongbaoloi_form_nhaphoso" name="thongbaoloi_form_nhaphoso" style="display: none"
            class="alert alert-error">
        </div>
        <table id="Table1" width="100%" border="0" class="formtbl" runat="server">
            
            <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Thông tin cá nhân</h6>
                </td>
                
            </tr>
            <tr>
                <td>
                    <label>Họ và tên đệm:</label>
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    
                    
                    <asp:TextBox ID="txtHoDem" runat="server" Width="100%"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="reqHoDem" runat="server" ControlToValidate="txtHoDem" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>
                </td>
                <td>
                    <label>Tên:</label>
                    <asp:Label ID="Label33" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2">
                    
                    <asp:TextBox ID="txtTen" runat="server" Width="100%"></asp:TextBox>
                   <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTen" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Giới tính:</label>
                </td>
                <td>
                    
                    <asp:RadioButton ID="rdNam" runat="server" GroupName="GioiTinh" Text="   Nam" Checked="true" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rdNu" runat="server" GroupName="GioiTinh" Text="   Nữ" />
                   
                </td>
                <td>
                    <label>Ngày sinh:</label>
                </td>
                <td colspan="2">
                   
                    <input name="NgaySinh" type="text" id="NgaySinh" runat="server" clientidmode="Static" />
                   
                </td>
            </tr>
            <tr>
                <td>
                    <label>Địa chỉ email:</label>
                    <asp:Label ID="Label67" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtEmail" runat="server" Width="100%"></asp:TextBox>
                    <asp:CustomValidator Display="Dynamic" ID="CustomValidator1"                         
                          ClientValidationFunction="EmailValidation" 
                          runat="server"
                          ErrorMessage="Địa chỉ email không hợp lệ"
                          >
                    </asp:CustomValidator>
                </td>
                <td>
                    <label>Xác nhận địa chi email:</label>
                    <asp:Label ID="Label68" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2">
                    
                    <asp:TextBox ID="txtXacNhanEmail" runat="server" Width="100%"></asp:TextBox>
                    <asp:CustomValidator Display="Dynamic" ID="CustomValidator2"                         
                          ClientValidationFunction="EmailValidation" 
                          runat="server"
                          ErrorMessage="Địa chỉ email không hợp lệ"
                          >
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Mobile:</label>
                    <asp:Label ID="Label69" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtMobile" runat="server" Width="100%" onkeypress="return NumberOnly()"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtMobile" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>
                </td>
                
                <td colspan="3">
                    <asp:CheckBox ID="chkTroLy" runat="server" Checked="true" Text="Trợ lý kiểm toán viên CTKT" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Chức vụ:</label>
                </td>
                <td>
                    
                    <asp:DropDownList ID="drChucVu" DataTextField="TenChucVu" DataValueField="ChucVuID" runat="server" Width="100%"></asp:DropDownList>
                    
                </td>
                <td>
                    <label>Đơn vị công tác:</label>
                </td>
                <td colspan="2">
                    
                    <asp:DropDownList ID="drDonVi" DataTextField="TenDoanhNghiep" DataValueField="HoiVienTapTheID" runat="server" Width="100%"></asp:DropDownList>
                    
                </td>
            </tr>
            <tr>
            <td>
                    <label>Đơn vị công tác (Khác):</label>
                </td>
                <td colspan="4">
                <asp:TextBox ID="txtDonViCongTac" runat="server" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <p>
                        &nbsp;</p>

                        
                <a class="btn btn-rounded"><i class="iconfa-plus-sign"></i>
                        <input type="button" value="Nhập mới" onclick="window.location.href = window.location.href.split('?')[0] + '?page=nguoiquantam_add'" />
                    </a>
            <a  class="btn btn-rounded"><i class="iconfa-ok"></i>
            <asp:Button ID="Btn_CapNhat" Text="Xác nhận" runat="server" 
                  onclick="btnCapNhat_Click" /> </a>

                    <a class="btn btn-rounded"><i class="iconfa-minus-sign"></i>
                        <input type="button" value="Quay lại" onclick="window.location.href = window.location.href.split('?')[0] + '?page=ketnaphoivien'" />
                    </a>
                   
                </td>
            </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">

    jQuery("#form_nhaphoso").validate({
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form_nhaphoso").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_nhaphoso").hide()
            }
        }
    });

    function NumberOnly() {
        var AsciiValue = event.keyCode
        if ((AsciiValue >= 48 && AsciiValue <= 57) || (AsciiValue == 8 || AsciiValue == 127 || AsciiValue == 44))
            event.returnValue = true;
        else
            event.returnValue = false;
    }
   
    $.datepicker.setDefaults($.datepicker.regional['vi']);
    $(function () {
            $('#NgaySinh').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                yearRange: "-75:+25"
            });

            $('#NgaySinh').mask("99/99/9999");

        });

</script>