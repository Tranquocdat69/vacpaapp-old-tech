﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;

using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using CommonLayer;
using VACPA.Data.Bases;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Globalization;

public partial class usercontrols_danhsachkhachhang : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();

    string tuchoidon = "tuchoidon";
    string trinhpheduyet = "trinhpheduyet";
    string duyetdon = "duyetdon";
    string tuchoiduyet = "tuchoiduyet";
    string thoaiduyet = "thoaiduyet";

    int trangthaiDangHoatDong = 1;
    int trangthaiNgungHoatDong = 0;



    string tk_DangHoatDong = "1";
    string tk_NgungHoatDong = "0";



    string tk_LoaiNQT = "0";
    string tk_LoaiHVCN = "1";
    string tk_LoaiKTV = "2";

    string tk_LoaiHVTT = "1";
    string tk_LoaiCTKT = "2";
    string tk_LoaiChiNhanh = "3";

    string tk_XoaTen = "";

    string tk_HVCT = "0";
    string tk_HVLK = "1";
    string tk_HVCC = "2";
    string tk_HVDD = "3";
    string tk_HVCTrach = "4";
    string tk_HVNH = "5";
    string tk_HVCQQL = "6";

    string delete = "delete";
    string canhan = "1";
    string tapthe = "2";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

        // Tên chức năng
        string tenchucnang = "Quản lý danh sách khách hàng";
        // Icon CSS Class  
        string IconClass = "iconfa-book";

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            Form1.Visible = false;
            return;
        }


        ///////////
        if (!string.IsNullOrEmpty(Request.QueryString["thongbao"]))
        {
            if (Request.QueryString["thongbao"] == "1")
            {
                string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Gửi thành công!</div>";

                ErrorMessage.Controls.Add(new LiteralControl(thongbao));
            }
            else
            {
                string thongbao = @"<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Request.QueryString["thongbao"] + "</div>";

                ErrorMessage.Controls.Add(new LiteralControl(thongbao));
            }
        }

        if (Request.QueryString["act"] == "delete")
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                SqlCommand cmd = new SqlCommand();
                DataTable dtb = new DataTable();
                string[] lstid = Request.QueryString["id"].Split(',');
                foreach (string id in lstid)
                {
                    dtb.Clear();
                    string hoivienid = id.Split(';')[0];
                    if (id.Split(';')[1] == "1") // cá nhân
                    {
                        cmd.CommandText = "exec dbo.sp_XoaHoiVienCaNhan " + hoivienid;
                        DataAccess.RunActionCmd(cmd);
                    }
                    else // tổ chức
                    {
                        cmd.CommandText = "SELECT HoiVienCaNhanID FROM tblHoiVienCaNhan WHERE HoiVienTapTheID = " + hoivienid;
                        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

                        foreach (DataRow row in dtb.Rows)
                        {
                            cmd.CommandText = "exec dbo.sp_XoaHoiVienCaNhan " + row["HoiVienCaNhanID"].ToString();
                            DataAccess.RunActionCmd(cmd);
                        }

                        cmd.CommandText = "exec dbo.sp_XoaHoiVienTapThe " + hoivienid;
                        DataAccess.RunActionCmd(cmd);
                    }
                }
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Xóa thông tin hội viên thành công!</div>"));
            }
        }

        if (Request.QueryString["act"] == "email")
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                SqlCommand sql = new SqlCommand();
                DataTable dtb = new DataTable();
                bool IsSendMail = true;
                string[] lstid = Request.QueryString["id"].Split(',');
                string err = "Lỗi gửi email:<br>";
                foreach (string id in lstid)
                {
                    dtb.Clear();
                    string hoivienid = id.Split(';')[0];
                    if (id.Split(';')[1] == "1") // cá nhân
                    {
                        sql.CommandText = "SELECT TenDangNhap, MatKhau, Email, HoVaTen FROM tblNguoiDung WHERE LoaiHoiVien IN ('0','1','2') AND HoiVienID = " + hoivienid;

                        dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];

                        if (dtb.Rows.Count != 0)
                        {
                            // Gửi mail

                            string body = "";

                            body += "Thân chào Anh/chị <span style=\"color:red\">" + dtb.Rows[0]["HoVaTen"].ToString() + "</span><BR/>";
                            body += "Ban quản trị xin thông báo tài khoản đăng nhập phần mềm quản lý Hội viên VACPA như sau:<BR/>";
                            body += "------ID đăng nhập------<BR/>";
                            body += "Tên đăng nhập: " + dtb.Rows[0]["TenDangNhap"].ToString() + "<BR/>";
                            body += "Mật khẩu: " + dtb.Rows[0]["MatKhau"].ToString() + "<BR/>";
                            body += "<span style=\"text-decoration:underline;font-weight:bold\">Lưu ý:</span><BR/>";
                            body += " - Link đăng nhập phần mềm quản lý hội viên: http://vacpa.org.vn/Page/Login.aspx" + "<BR/>";
                            body += " - Link xin cấp lại mật khẩu: http://vacpa.org.vn/Page/GetPassword.aspx" + "<BR/>";
                            body += " - Ngay sau khi đăng nhập Anh/Chị hãy đổi mật khẩu truy cập." + "<BR/>";
                            body += " - Để đảm bảo được nhận các thông báo thường xuyên với VACPA, Anh/Chị nhớ cập nhật những thông tin về sự thay đổi công việc, đơn vị công tác, số điện thoại di động, email, nơi thường trú, v.v… qua profile cá nhân." + "<BR/>";

                            body += "━━━━━━━━━━━━━━━<BR/>";
                            body += "Trân trọng,<BR/>";
                            body += "Hội kiểm toán viên hành nghề Việt Nam (VACPA)<BR/>";
                            body += "Địa chỉ: Phòng 304, Nhà Dự án, Số 4, Ngõ Hàng Chuối I, Hà Nội<BR/>";
                            body += "Email: quantriweb@vacpa.org.vn";
                            string msg = "";
                            IsSendMail = SmtpMail.Send("BQT WEB VACPA", dtb.Rows[0]["Email"].ToString(), "VACPA - Thông tin tài khoản tại trang tin điện tử VACPA", body, ref msg);
                            if (!IsSendMail)
                            {
                                err += dtb.Rows[0]["TenDangNhap"].ToString() + " - " + dtb.Rows[0]["Email"].ToString() + ": " + msg + "<br>";
                            }

                        }
                    }
                    else // tổ chức
                    {
                        sql.CommandText = "SELECT TenDangNhap, MatKhau, Email, HoVaTen FROM tblNguoiDung WHERE LoaiHoiVien IN ('3','4') AND HoiVienID = " + hoivienid;

                        dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];

                        if (dtb.Rows.Count != 0)
                        {
                            // Gửi mail

                            string body = "";

                            body += "Thân chào công ty <span style=\"color:red\">" + dtb.Rows[0]["HoVaTen"].ToString() + "</span><BR/>";
                            body += "Ban quản trị xin thông báo tài khoản đăng nhập phần mềm quản lý Hội viên VACPA như sau:<BR/>";
                            body += "------ID đăng nhập------<BR/>";
                            body += "Tên đăng nhập: " + dtb.Rows[0]["TenDangNhap"].ToString() + "<BR/>";
                            body += "Mật khẩu: " + dtb.Rows[0]["MatKhau"].ToString() + "<BR/>";
                            body += "<span style=\"text-decoration:underline;font-weight:bold\">Lưu ý:</span><BR/>";
                            body += " - Link đăng nhập phần mềm quản lý hội viên: http://vacpa.org.vn/Page/Login.aspx" + "<BR/>";
                            body += " - Link xin cấp lại mật khẩu: http://vacpa.org.vn/Page/GetPassword.aspx" + "<BR/>";
                            body += " - Ngay sau khi đăng nhập Anh/Chị hãy đổi mật khẩu truy cập." + "<BR/>";
                            body += " - Để đảm bảo được nhận các thông báo thường xuyên với VACPA, Anh/Chị nhớ cập nhật những thông tin về sự thay đổi công việc, đơn vị công tác, số điện thoại di động, email, nơi thường trú, v.v… qua profile cá nhân." + "<BR/>";

                            body += "━━━━━━━━━━━━━━━<BR/>";
                            body += "Trân trọng,<BR/>";
                            body += "Hội kiểm toán viên hành nghề Việt Nam (VACPA)<BR/>";
                            body += "Địa chỉ: Phòng 304, Nhà Dự án, Số 4, Ngõ Hàng Chuối I, Hà Nội<BR/>";
                            body += "Email: quantriweb@vacpa.org.vn";
                            string msg = "";
                            IsSendMail = SmtpMail.Send("BQT WEB VACPA", dtb.Rows[0]["Email"].ToString(), "VACPA - Thông tin tài khoản tại trang tin điện tử VACPA", body, ref msg);
                            if (!IsSendMail)
                            {
                                err += dtb.Rows[0]["TenDangNhap"].ToString() + " - " + dtb.Rows[0]["Email"].ToString() + ": " + msg + "<br>";
                            }

                        }
                    }
                }

                if (err != "Lỗi gửi email:<br>")
                    Response.Redirect("admin.aspx?page=danhsachkhachhang&thongbao=" + err);
                else
                    Response.Redirect("admin.aspx?page=danhsachkhachhang&thongbao=1");
            }
        }

        load_users();

        if (IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.Form["HoiVienID"]))
                setdataCrystalReport();
            else if (!string.IsNullOrEmpty(Request.Form["HoiVienTapTheID"]))
                setdataCrystalReport_HVTT();
        }




    }

    public void setdataCrystalReport()
    {
        List<objGiayChungNhanHoiVienChinhThuc> lst = new List<objGiayChungNhanHoiVienChinhThuc>();


        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();

        //Lấy hoi vien ca nhan
        if (!string.IsNullOrEmpty(Request.Form["HoiVienID"]))
        {
            sql.CommandText = @"select GioiTinh, (HoDem + ' ' + Ten) as HoVaTen, 
SoChungChiKTV, NgayCapChungChiKTV, SoQuyetDinhKetNap, NgayQuyetDinhKetNap
from tblHoiVienCaNhan where hoiviencanhanid = " + Request.Form["HoiVienID"];
            ds = DataAccess.RunCMDGetDataSet(sql);

            DataTable dt = ds.Tables[0];

            //objGiayChungNhanHoiVienChinhThuc obj = new objGiayChungNhanHoiVienChinhThuc();
            //obj.HoVaTen = "¤ng NguyÔn Quèc ¢n";
            //obj.SoCCKTV = "Chøng chØ KTV sè 2772/KTV do Bé Tµi chÝnh";
            //obj.NgayCap = "cÊp ngµy 15/4/2014 Lµ Héi viªn chÝnh thøc cña VACPA";
            //obj.HoVaTenTiengAnh = "Mr. Nguyen Quoc An";
            //obj.SoCCKTVTiengAnh = "Auditor Certificate No.2772/KTV dated April 15, 2014 by";
            //obj.NgayCapTiengAnh = "Ministry of Finance is a full Member of VACPA";
            //obj.NgayThangNamRaQuyetDinh = "Hµ Néi, 10 th¸ng 7 n¨m 2015";
            //obj.SoQuyetDinh = "Sè: 245-2015/Q§-VACPA";
            //lst.Add(obj);
            foreach (DataRow dtr in dt.Rows)
            {
                objGiayChungNhanHoiVienChinhThuc obj = new objGiayChungNhanHoiVienChinhThuc();
                if (dtr["GioiTinh"].ToString() == "1" || dtr["GioiTinh"].ToString() == "1")
                {
                    obj.NgayCapTiengAnh = "¤ng";
                    obj.HoVaTenTiengAnh = "Mr.";
                }
                else
                {
                    obj.NgayCapTiengAnh = "Bµ";
                    obj.HoVaTenTiengAnh = "Ms.";
                }
                obj.HoVaTen += " " + Converter.ConvertToTCVN3(dtr["HoVaTen"].ToString());

                obj.SoCCKTV = "Chøng chØ KTV sè " + Converter.ConvertToTCVN3(dtr["SoChungChiKTV"].ToString()) + " do Bé Tµi chÝnh";
                try
                {
                    obj.NgayCap = "cÊp ngµy " + Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).ToShortDateString() + " Lµ Héi viªn chÝnh thøc cña VACPA";
                }
                catch (Exception)
                {
                    obj.NgayCap = "cÊp ngµy " + " Lµ Héi viªn chÝnh thøc cña VACPA";
                }
                obj.HoVaTenTiengAnh += " " + RemoveSign4VietnameseString(dtr["HoVaTen"].ToString());
                obj.SoCCKTVTiengAnh = "Auditor Certificate No. " + RemoveSign4VietnameseString(dtr["SoChungChiKTV"].ToString()) + " dated " +
                    DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Month) + " " +
                    Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Day + ", " +
                    Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Year +
                    " by";
                obj.SoCCKTVTiengAnh += "\r\nMinistry of Finance is full Member of VACPA";
                try
                {
                    obj.NgayThangNamRaQuyetDinh = "Hµ Néi, ngµy " + Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Day + " th¸ng " +
                   Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Month + " n¨m " + Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Year;
                }
                catch (Exception)
                {
                    obj.NgayThangNamRaQuyetDinh = "Hµ Néi, ngµy " + " th¸ng " +
                                                  " n¨m ";
                }
                try
                {
                    obj.SoQuyetDinh = "Sè: " + dtr["SoQuyetDinhKetNap"].ToString();
                }
                catch (Exception)
                {
                    obj.SoQuyetDinh = "Sè: ";
                }

                lst.Add(obj);
            }

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/GiayChungNhanHoiVienCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objGiayChungNhanHoiVienChinhThuc"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            //CrystalReportViewer1.ReportSource = rpt;
            //CrystalReportViewer1.DataBind();
            //CrystalReportViewer1.SeparatePages = true;

            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true,
                                     "GiayChungNhanHoiVienCaNhan");

        }
    }

    public void setdataCrystalReport_HVTT()
    {
        string strid = Request.Form["HoiVienTapTheID"];
        if (!string.IsNullOrEmpty(strid))
        {
            //int id = Convert.ToInt32(strid);
            List<objThongTinDoanhNghiep_CT> lst = new List<objThongTinDoanhNghiep_CT>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep, TenTiengAnh, SoQuyetDinhKetNap, NgayQuyetDinhKetNap "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE HoiVienTapTheID = '" + strid + "'";
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            // int i = 1;
            List<Anh_Tr> lst_Anh = new List<Anh_Tr>();

            foreach (DataRow tem in dt.Rows)
            {
                objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();

                new1.TenDoanhNghiep = Converter.ConvertToTCVN3(tem["TenDoanhNghiep"].ToString().Replace("\n", ""));

                new1.TenTiengAnh = tem["TenTiengAnh"].ToString() + " is full Member of VACPA";
                new1.SoQD = Converter.ConvertToTCVN3(tem["SoQuyetDinhKetNap"].ToString());
                new1.NgayCN = !string.IsNullOrEmpty(tem["NgayQuyetDinhKetNap"].ToString()) ? "Hà Nội, ngày " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Day + " tháng " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Month + " năm " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Year : "Hà Nội, ngày....tháng....năm......";


                lst.Add(new1);

            }



            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/GiayChungNhanHoiVienTapThe.rpt"));
            // rpt.Load(Server.MapPath("Report/QLHV/ReportTest.rpt"));
            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            rpt.Database.Tables["Anh_Tr"].SetDataSource(lst_Anh);
            //rpt.Database.Tables["lstOBJKhenThuong"].SetDataSource(lstKhenThuong);
            

            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "GiayChungNhanHoiVienTapThe");

        }
    }

    public static string ConvertToTCVN3(string value)
    {
        string text = "";
        int i = 0;
        while (i < value.Length)
        {
            char value2 = char.Parse(value.Substring(i, 1));
            short num = System.Convert.ToInt16(value2);
            if (num <= 259)
            {
                if (num <= 202)
                {
                    if (num != 194)
                    {
                        switch (num)
                        {
                            case 201: text += "Ð"; break;
                            case 202: text += "ü"; break;
                            default: goto IL_7AF;
                        }
                    }
                    else text += "¢";
                }
                else
                {
                    if (num != 212)
                    {
                        switch (num)
                        {
                            case 224: text += "µ"; break;
                            case 225: text += "¸"; break;
                            case 226: text += "©"; break;
                            case 227: text += "·"; break;
                            case 228:
                            case 229:
                            case 230:
                            case 231:
                            case 235:
                            case 238:
                            case 239:
                            case 240:
                            case 241:
                            case 246:
                            case 247:
                            case 248:
                            case 251:
                            case 252: goto IL_7AF;
                            case 232: text += "Ì"; break;
                            case 233: text += "Ð"; break;
                            case 234: text += "ª"; break;
                            case 236: text += "ỡ"; break;
                            case 237: text += "Ý"; break;
                            case 242: text += "ß"; break;
                            case 243: text += "ã"; break;
                            case 244: text += "«"; break;
                            case 245: text += "ừ"; break;
                            case 249: text += "ự"; break;
                            case 250: text += "ư"; break;
                            case 253: text += "ý"; break;
                            default:
                                switch (num)
                                {
                                    case 258: text += "¡"; break;
                                    case 259: text += "¨"; break;
                                    default: goto IL_7AF;
                                }
                                break;
                        }
                    }
                    else text += "¤";
                }
            }
            else
            {
                if (num <= 361)
                {
                    switch (num)
                    {
                        case 272: text += "§"; break;
                        case 273: text += "®"; break;
                        default:
                            if (num != 297)
                            {
                                if (num != 361) goto IL_7AF;
                                text += "ò";
                            }
                            else text += "Ü";
                            break;
                    }
                }
                else
                {
                    switch (num)
                    {
                        case 416: text += "¥"; break;
                        case 417: text += "¬"; break;
                        default:
                            if (num != 431)
                            {
                                switch (num)
                                {
                                    case 7841: text += "¹"; break;
                                    case 7842:
                                    case 7844:
                                    case 7846:
                                    case 7848:
                                    case 7850:
                                    case 7852:
                                    case 7854:
                                    case 7856:
                                    case 7858:
                                    case 7860:
                                    case 7862:
                                    case 7864:
                                    case 7866:
                                    case 7868:
                                    case 7870:
                                    case 7872:
                                    case 7874:
                                    case 7876:
                                    case 7878:
                                    case 7880:
                                    case 7882:
                                    case 7884:
                                    case 7886:
                                    case 7888:
                                    case 7890:
                                    case 7892:
                                    case 7894:
                                    case 7896:
                                    case 7898:
                                    case 7900:
                                    case 7902:
                                    case 7904:
                                    case 7906:
                                    case 7908:
                                    case 7910:
                                    case 7912:
                                    case 7914:
                                    case 7916:
                                    case 7918:
                                    case 7920:
                                    case 7922:
                                    case 7924:
                                    case 7926:
                                    case 7928: goto IL_7AF;
                                    case 7843: text += "¶"; break;
                                    case 7845: text += "Ê"; break;
                                    case 7847: text += "Ç"; break;
                                    case 7849: text += "È"; break;
                                    case 7851: text += "É"; break;
                                    case 7853: text += "Ë"; break;
                                    case 7855: text += "¾"; break;
                                    case 7857: text += "»"; break;
                                    case 7859: text += "¼"; break;
                                    case 7861: text += "½"; break;
                                    case 7863: text += "Æ"; break;
                                    case 7865: text += "Ñ"; break;
                                    case 7867: text += "Î"; break;
                                    case 7869: text += "Ï"; break;
                                    case 7871: text += "Õ"; break;
                                    case 7873: text += "Ò"; break;
                                    case 7875: text += "Ó"; break;
                                    case 7877: text += "Ô"; break;
                                    case 7879: text += "Ö"; break;
                                    case 7881: text += "Ø"; break;
                                    case 7883: text += "Þ"; break;
                                    case 7885: text += "ä"; break;
                                    case 7887: text += "á"; break;
                                    case 7889: text += "è"; break;
                                    case 7891: text += "å"; break;
                                    case 7893: text += "æ"; break;
                                    case 7895: text += "ç"; break;
                                    case 7897: text += "é"; break;
                                    case 7899: text += "í"; break;
                                    case 7901: text += "ê"; break;
                                    case 7903: text += "ë"; break;
                                    case 7905: text += "ì"; break;
                                    case 7907: text += "î"; break;
                                    case 7909: text += "ô"; break;
                                    case 7911: text += "ñ"; break;
                                    case 7913: text += "ø"; break;
                                    case 7915: text += "õ"; break;
                                    case 7917: text += "ö"; break;
                                    case 7919: text += "÷"; break;
                                    case 7921: text += "ù"; break;
                                    case 7923: text += "ú"; break;
                                    case 7925: text += "þ"; break;
                                    case 7927: text += "û"; break;
                                    case 7929: text += "ü"; break;
                                    default: goto IL_7AF;
                                }
                            }
                            else text += "¦";
                            break;
                    }
                }
            }
        IL_7BF:
            i++;
            continue;
        IL_7AF:
            text += value2.ToString();
            goto IL_7BF;
        }
        return text;
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    public static string RemoveSign4VietnameseString(string str)
    {
        if (str == null)
            return "";

        if (str.Trim().Length == 0)
            return "";

        //Tiến hành thay thế , lọc bỏ dấu cho chuỗi
        for (int i = 1; i < VietnameseSigns.Length; i++)
        {
            for (int j = 0; j < VietnameseSigns[i].Length; j++)

                str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);

        }

        str = str.Replace("  ", " ");

        return str.Trim();
    }

    private static readonly string[] VietnameseSigns = new string[]

    {

        "aAeEoOuUiIdDyY",

        "áàạảãâấầậẩẫăắằặẳẵ",

        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

        "éèẹẻẽêếềệểễ",

        "ÉÈẸẺẼÊẾỀỆỂỄ",

        "óòọỏõôốồộổỗơớờợởỡ",

        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

        "úùụủũưứừựửữ",

        "ÚÙỤỦŨƯỨỪỰỬỮ",

        "íìịỉĩ",

        "ÍÌỊỈĨ",

        "đ",

        "Đ",

        "ýỳỵỷỹ",

        "ÝỲỴỶỸ"

    };

    public void DonVi(string ma = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM (SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE '--- ' + ISNULL(A.SoHieu,LTRIM(RTRIM(A.MaHoiVienTapThe))) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID) AS X ORDER BY X.SoHieu ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (ma == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public DataSet sp_KhachHangSearch()
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            bool isCaNhan = false; ;
            bool isKiemToanVien = false;
            bool isNguoiQuanTam = false;
            bool isTapThe = false;
            bool isCTyKiemToan = false;

            bool isDangHoatDong = false;
            bool isNgungHoatDong = false;

            String sLoaiHoiVienTT = "";
            String sLoaiHoiVienCN = "";

            String sLoaiDon = "";
            String sTrangThai = "";
            String sTenHoiVien = "";
            String sHoDemCaNhan = "";
            String sTenCaNhan = "";

            String sIDHoiVien = "";
            String sSoDon = "";
            String sHinhThucNop = "";
            String sMaTinhThanh = "";
            String sMaQuanHuyen = "";

            String sCongtyKiemToan = "";
            String sSoChungChiKTV = "";
            String sSoDKKD = "";
            String sSoCNDuDieuKienHNKT = "";

            String sNgayGiaNhapTu = "";
            String sNgayGiaNhapDen = "";
            String sNamSinhTu = "";
            String sNamSinhDen = "";
            String sNgayXoaTenTu = "";
            String sNgayXoaTenDen = "";
            String sGioiTinh = "";
            String sCoChungChi = "";

            String sLoaiHoiVienChiTiet = "";
            if (Request.Form["tk_HVCT"] != null)
            {
                sLoaiHoiVienChiTiet += tk_HVCT + ",";
            }
            if (Request.Form["tk_HVLK"] != null)
            {
                sLoaiHoiVienChiTiet += tk_HVLK + ",";
            }
            if (Request.Form["tk_HVCC"] != null)
            {
                sLoaiHoiVienChiTiet += tk_HVCC + ",";
            }
            if (Request.Form["tk_HVDD"] != null)
            {
                sLoaiHoiVienChiTiet += tk_HVDD + ",";
            }
            if (Request.Form["tk_HVCTrach"] != null)
            {
                sLoaiHoiVienChiTiet += tk_HVCTrach + ",";
            }
            if (Request.Form["tk_HVNH"] != null)
            {
                sLoaiHoiVienChiTiet += tk_HVNH + ",";
            }
            if (Request.Form["tk_HVCQQL"] != null)
            {
                sLoaiHoiVienChiTiet += tk_HVCQQL + ",";
            }
            sLoaiHoiVienChiTiet = sLoaiHoiVienChiTiet.TrimEnd(',');
            

            //  Loai Don canhan/tap the
            int tempCN = 0;
            int tempTT = 0;


            if (Request.Form["tk_HVCN"] != null)
            {
                if (tempCN == 0)
                {
                    sLoaiHoiVienCN = tk_LoaiHVCN;
                }
                else
                {
                    sLoaiHoiVienCN = sLoaiHoiVienCN + "," + tk_LoaiHVCN;
                }
                tempCN = tempCN + 1;

                isCaNhan = true;
            }
            if (Request.Form["tk_KTV"] != null)
            {
                if (tempCN == 0)
                {
                    sLoaiHoiVienCN = tk_LoaiKTV;
                }
                else
                {
                    sLoaiHoiVienCN = sLoaiHoiVienCN + "," + tk_LoaiKTV;
                }
                tempCN = tempCN + 1;

                isKiemToanVien = true;
            }

            if (Request.Form["tk_NQT"] != null)
            {
                if (tempCN == 0)
                {
                    sLoaiHoiVienCN = tk_LoaiNQT;
                }
                else
                {
                    sLoaiHoiVienCN = sLoaiHoiVienCN + "," + tk_LoaiNQT;
                }
                tempCN = tempCN + 1;

                isNguoiQuanTam = true;
            }


            if (Request.Form["tk_HVTT"] != null)
            {
                if (tempTT == 0)
                {
                    sLoaiHoiVienTT = tk_LoaiHVTT;
                }
                else
                {
                    sLoaiHoiVienTT = sLoaiHoiVienTT + "," + tk_LoaiHVTT;
                }
                tempTT = tempTT + 1;
                isTapThe = true;
            }


            if (Request.Form["tk_CTKT"] != null)
            {
                if (tempTT == 0)
                {
                    sLoaiHoiVienTT = tk_LoaiCTKT;
                }
                else
                {
                    sLoaiHoiVienTT = sLoaiHoiVienTT + "," + tk_LoaiCTKT;
                }
                tempTT = tempTT + 1;
                isCTyKiemToan = true;
            }

            if (Request.Form["tk_ChiNhanh"] != null)
            {
                if (tempTT == 0)
                {
                    sLoaiHoiVienTT = tk_LoaiChiNhanh;
                }
                else
                {
                    sLoaiHoiVienTT = sLoaiHoiVienTT + "," + tk_LoaiChiNhanh;
                }
                tempTT = tempTT + 1;
                isTapThe = true;
            }

            if (Request.Form["tk_XoaTen"] != null)
            {
                tk_XoaTen = Request.Form["tk_XoaTen"];
            }


            if ((isCaNhan && isTapThe && isNguoiQuanTam && isKiemToanVien && isCTyKiemToan) ||
                 (!isCaNhan && !isTapThe && !isNguoiQuanTam && !isKiemToanVien && !isCTyKiemToan)) { sLoaiDon = "3"; }
            else if (isCaNhan || isKiemToanVien || isNguoiQuanTam) { sLoaiDon = "1"; }
            else if (isTapThe || isCTyKiemToan) { sLoaiDon = "2"; }



            // Trang thai
            if (Request.Form["tk_DangHoatDong"] != null)
            {
                isDangHoatDong = true;

            }
            if (Request.Form["tk_NgungHoatDong"] != null)
            {  // check all

                isNgungHoatDong = true;
            }


            if ((isDangHoatDong && isNgungHoatDong) ||
            (!isDangHoatDong && !isNgungHoatDong)) { sTrangThai = ""; }
            else if (isDangHoatDong) { sTrangThai = "1"; }
            else if (isNgungHoatDong) { sTrangThai = "0"; }





            // Ten hoi vien
            if (Request.Form["tk_TenHoiVien"] != null)
            {
                sTenHoiVien = Request.Form["tk_TenHoiVien"];
                string[] stempHoten = Request.Form["tk_TenHoiVien"].Split(' ');
                if (stempHoten.Length > 1)
                {

                    for (int j = 0; j < stempHoten.Length - 1; j++)
                    {
                        sHoDemCaNhan = sHoDemCaNhan + " " + stempHoten[j];

                    }
                    sHoDemCaNhan = sHoDemCaNhan.Trim();
                    sTenCaNhan = stempHoten[stempHoten.Length - 1];
                }
                else
                {
                    sTenCaNhan = Request.Form["tk_TenHoiVien"];
                    sHoDemCaNhan = Request.Form["tk_TenHoiVien"];
                }

            }

            // Ten dang nhap
            if (Request.Form["tk_ID"] != null)
            {
                sIDHoiVien = Request.Form["tk_ID"];
            }

            // So Don
            if (Request.Form["tk_SoDon"] != null)
            {
                sSoDon = Request.Form["tk_SoDon"];
            }

            // Hinh thuc nop
            if (Request.Form["tk_HinhThucNop"] != null)
            {
                sHinhThucNop = Request.Form["tk_HinhThucNop"];
            }

            // Ma Tinh
            if (Request.Form["TinhID_ToChuc"] != null)
            {
                sMaTinhThanh = Request.Form["TinhID_ToChuc"];
            }
            // Ma Huyen
            if (Request.Form["HuyenID_ToChuc"] != null)
            {
                sMaQuanHuyen = Request.Form["HuyenID_ToChuc"];
            }


            // Cong ty kiem toan
            if (Request.Form["tk_CTKiemToan"] != null)
            {
                sCongtyKiemToan = Request.Form["tk_CTKiemToan"];
            }



            // So chung chi KTV
            if (Request.Form["tk_SoChungChiKTV"] != null)
            {
                sSoChungChiKTV = Request.Form["tk_SoChungChiKTV"];
            }

            // Chung nhan dk hanh nghe kt
            if (Request.Form["tk_CNDKHNKT"] != null)
            {
                sSoCNDuDieuKienHNKT = Request.Form["tk_CNDKHNKT"];
            }

            // So DKKD
            if (Request.Form["tk_SoDKKD"] != null)
            {
                sSoDKKD = Request.Form["tk_SoDKKD"];
            }


            // Ngay nop tu
            if (Request.Form["tk_NgayNopTu"] != null)
            {
                sNgayGiaNhapTu = Request.Form["tk_NgayNopTu"].Replace("/", ""); ;
            }

            // Ngay nop den
            if (Request.Form["tk_NgayNopDen"] != null)
            {
                sNgayGiaNhapDen = Request.Form["tk_NgayNopDen"].Replace("/", "");
            }

            // Nam sinh tu
            if (Request.Form["tk_NamSinhTu"] != null)
            {
                sNamSinhTu = Request.Form["tk_NamSinhTu"]; ;
            }

            // Ngay nop den
            if (Request.Form["tk_NamSinhDen"] != null)
            {
                sNamSinhDen = Request.Form["tk_NamSinhDen"];
            }

            // Ngay duyet tu
            if (Request.Form["tk_NgayPheDuyetTu"] != null)
            {
                sNgayXoaTenTu = Request.Form["tk_NgayPheDuyetTu"].Replace("/", ""); ;
            }
            //  Ngay duyet den
            if (Request.Form["tk_NgayPheDuyetDen"] != null)
            {
                sNgayXoaTenDen = Request.Form["tk_NgayPheDuyetDen"].Replace("/", ""); ;
            }

            if (Request.Form["checkNam"] != null)
            {
                if (!string.IsNullOrEmpty(sGioiTinh))
                    sGioiTinh = "";
                else
                    sGioiTinh = "1";
            }

            if (Request.Form["checkNu"] != null)
            {
                if (!string.IsNullOrEmpty(sGioiTinh))
                    sGioiTinh = "";
                else
                    sGioiTinh = "0";
            }

            if (Request.Form["checkCoCC"] != null)
            {
                if (!string.IsNullOrEmpty(sCoChungChi))
                    sCoChungChi = "";
                else
                    sCoChungChi = "1";
            }

            if (Request.Form["checkKoCC"] != null)
            {
                if (!string.IsNullOrEmpty(sCoChungChi))
                    sCoChungChi = "";
                else
                    sCoChungChi = "0";
            }

            if (sCongtyKiemToan == "0")
                sCongtyKiemToan = "";

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            if(sLoaiHoiVienChiTiet != "")
                arrParams.Add(new DBHelpers.DataParameter("LoaiHoiVienChiTiet", sLoaiHoiVienChiTiet));
            arrParams.Add(new DBHelpers.DataParameter("LoaiDon", sLoaiDon));
            arrParams.Add(new DBHelpers.DataParameter("LoaiHoiVienCN", sLoaiHoiVienCN));
            arrParams.Add(new DBHelpers.DataParameter("LoaiHoiVienTT", sLoaiHoiVienTT));
            arrParams.Add(new DBHelpers.DataParameter("TrangThai", sTrangThai));
            arrParams.Add(new DBHelpers.DataParameter("TenHoiVien", sTenHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("HoDemCaNhan", sHoDemCaNhan));
            arrParams.Add(new DBHelpers.DataParameter("TenCaNhan", sTenCaNhan));
            arrParams.Add(new DBHelpers.DataParameter("IDHoiVien", sIDHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("SoDon", sSoDon));
            arrParams.Add(new DBHelpers.DataParameter("HinhThucNop", sHinhThucNop));
            arrParams.Add(new DBHelpers.DataParameter("MaTinhThanh", sMaTinhThanh));
            arrParams.Add(new DBHelpers.DataParameter("MaQuanHuyen", sMaQuanHuyen));
            arrParams.Add(new DBHelpers.DataParameter("CongtyKiemToan", sCongtyKiemToan));
            arrParams.Add(new DBHelpers.DataParameter("SoChungChiKTV", sSoChungChiKTV));
            arrParams.Add(new DBHelpers.DataParameter("SoDKKD", sSoDKKD));
            arrParams.Add(new DBHelpers.DataParameter("SoCNDuDieuKienHNKT", sSoCNDuDieuKienHNKT));
            arrParams.Add(new DBHelpers.DataParameter("NgayGiaNhapTu", sNgayGiaNhapTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayGiaNhapDen", sNgayGiaNhapDen));
            arrParams.Add(new DBHelpers.DataParameter("NamSinhTu", sNamSinhTu));
            arrParams.Add(new DBHelpers.DataParameter("NamSinhDen", sNamSinhDen));
            arrParams.Add(new DBHelpers.DataParameter("NgayXoaTenTu", sNgayXoaTenTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayXoaTenDen", sNgayXoaTenDen));
            arrParams.Add(new DBHelpers.DataParameter("GioiTinh", sGioiTinh));
            arrParams.Add(new DBHelpers.DataParameter("CoChungChi", sCoChungChi));
            arrParams.Add(new DBHelpers.DataParameter("XoaTen", tk_XoaTen));

            objDBHelper = ConnectionControllers.Connect("VACPA");

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_hoivien_search_Truong", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }




    protected void load_users()
    {
        try
        {



            DataSet ds = sp_KhachHangSearch();

            DataView dt = ds.Tables[0].DefaultView;


            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 30;
            int i;
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "DanhSachKhachHang" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            ds = null;
            dt = null;




        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }
    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }
    protected void btnGhi_Click(object sender, EventArgs e)
    {

    }


}