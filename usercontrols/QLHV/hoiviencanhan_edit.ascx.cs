﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using CommonLayer;
using VACPA.Data.Bases;
using System.Text.RegularExpressions;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;

public partial class usercontrols_hoiviencanhan_edit : System.Web.UI.UserControl
{
    String id = String.Empty;
    public Commons cm = new Commons();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    static string login_link = System.Configuration.ConfigurationSettings.AppSettings["vacpaweblogin_link"];
    SqlConnection conn = new SqlConnection(connStr);
    clsXacDinhQuyen quyen = new clsXacDinhQuyen();
    protected DonHoiVienFile FileDinhKem1 = new DonHoiVienFile();
    protected HoiVienFile FileDinhKem2 = new HoiVienFile();
    public DonHoiVienCaNhan hoivien;

    public bool QuyenXem { get; set; }
    public bool QuyenThem { get; set; }
    public bool QuyenSua { get; set; }
    public bool QuyenXoa { get; set; }
    public bool QuyenDuyet { get; set; }
    public bool QuyenTraLai { get; set; }
    public bool QuyenHuy { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Tên chức năng
            string tenchucnang = "Đăng ký kết nạp hội viên cá nhân";
            if (Request.QueryString["ktv"] == "1")
            {
                lbTitle.Text = "Đăng ký kết nạp kiểm toán viên";
                tenchucnang = "Đăng ký kết nạp kiểm toán viên";
                trLoaiHoiVien.Visible = false;
            }
            // Icon CSS Class  
            string IconClass = "iconfa-credit-card";

            if (!string.IsNullOrEmpty(cm.Admin_NguoiDungID))
            {
                string maquyen = quyen.fcnXDQuyen(int.Parse(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr);

                QuyenXem = (maquyen.Contains("XEM")) ? true : false;
                QuyenThem = (maquyen.Contains("THEM")) ? true : false;
                QuyenSua = (maquyen.Contains("SUA")) ? true : false;
                QuyenXoa = (maquyen.Contains("XOA")) ? true : false;
                QuyenDuyet = (maquyen.Contains("DUYET")) ? true : false;
                QuyenTraLai = (maquyen.Contains("TRALAI")) ? true : false;
                QuyenHuy = (maquyen.Contains("HUY")) ? true : false;
            }

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"
     <li>Biểu mẫu tờ khai <span class=""separator""></span></li> <!-- Nhóm chức năng cấp trên -->
     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            SqlDonHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connStr, true, "");

            hoivien = HoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

            if (!IsPostBack)
            {

                SetInitialRow();
                SetInitialRow_ChungChi();

                LoadDropDown();

                LoadThongTin();

                if (Request.QueryString["thongbao"] == "1")
                {
                    string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Lưu thông tin thành công!</div>";

                    placeMessage.Controls.Add(new LiteralControl(thongbao));
                }
            }



        }
        catch (Exception ex)
        {
            placeMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    protected void LoadThongTin()
    {
        if (hoivien.LoaiHoiVienCaNhanChiTiet != null)
        {
            chkHoiVienLienKet.Checked = true;
            drLoaiHoiVien.SelectedValue = "1";
        }
        txtSoDon.Text = hoivien.SoDonHoiVienCaNhan;

        if (hoivien.HoiVienCaNhanId != null)
            lbHoiVienCaNhanID.Text = hoivien.HoiVienCaNhanId.Value.ToString();
        if (hoivien.LoaiHoiVienCaNhanChiTiet != null)
            drLoaiHoiVien.SelectedValue = hoivien.LoaiHoiVienCaNhanChiTiet.Value.ToString();
        txtHoDem.Text = hoivien.HoDem;
        txtTen.Text = hoivien.Ten;
        if (hoivien.GioiTinh == "1") rdNam.Checked = true; else rdNu.Checked = true;
        if (hoivien.NgaySinh != null)
            NgaySinh.Value = hoivien.NgaySinh.Value.ToString("dd/MM/yyyy");
        if (hoivien.QuocTichId != null)
            drQuocTich.SelectedValue = hoivien.QuocTichId.Value.ToString();
        txtDiaChi.Text = hoivien.DiaChi;
        txtSoGiayDHKN.Text = hoivien.SoGiayChungNhanDkhn;
        if (hoivien.NgayCapGiayChungNhanDkhn != null)
            NgayCap_DHKN.Value = hoivien.NgayCapGiayChungNhanDkhn.Value.ToString("dd/MM/yyyy");
        if (hoivien.HanCapTu != null)
            HanCapTu.Value = hoivien.HanCapTu.Value.ToString("dd/MM/yyyy");
        if (hoivien.HanCapDen != null)
            HanCapDen.Value = hoivien.HanCapDen.Value.ToString("dd/MM/yyyy");

        txtSoChungChiKTV.Text = hoivien.SoChungChiKtv;
        if (hoivien.NgayCapChungChiKtv != null)
            NgayCap_ChungChiKTV.Value = hoivien.NgayCapChungChiKtv.Value.ToString("dd/MM/yyyy");

        txtEmail.Text = txtXacNhanEmail.Text = hoivien.Email;
        txtDienThoai.Text = hoivien.DienThoai;
        txtMobile.Text = hoivien.Mobile;
        if (hoivien.ChucVuId != null)
            drChucVu.SelectedValue = hoivien.ChucVuId.Value.ToString();
        if (hoivien.TruongDaiHocId != null)
            drTotNghiep.SelectedValue = hoivien.TruongDaiHocId.Value.ToString();
        if (hoivien.ChuyenNganhDaoTaoId != null)
            drChuyenNganh.SelectedValue = hoivien.ChuyenNganhDaoTaoId.Value.ToString();
        txtNam_ChuyenNganh.Text = hoivien.ChuyenNganhNam;
        if (hoivien.HocViId != null)
            drHocVi.SelectedValue = hoivien.HocViId.Value.ToString();
        txtNam_HocVi.Text = hoivien.HocViNam;
        if (hoivien.HocHamId != null)
            drHocHam.SelectedValue = hoivien.HocHamId.Value.ToString();
        txtNam_HocHam.Text = hoivien.HocHamNam;
        txtSoCMND.Text = hoivien.SoCmnd;
        if (hoivien.CmndNgayCap != null)
            NgayCap_CMND.Value = hoivien.CmndNgayCap.Value.ToString("dd/MM/yyyy");
        if (hoivien.CmndTinhId != null)
            drNoiCap_CMND.SelectedValue = hoivien.CmndTinhId.Value.ToString();
        txtSoTKNganHang.Text = hoivien.SoTaiKhoanNganHang;
        if (hoivien.NganHangId != null)
            drNganHang.SelectedValue = hoivien.NganHangId.Value.ToString();
        if (hoivien.HoiVienTapTheId != null)
            drDonVi.SelectedValue = hoivien.HoiVienTapTheId.Value.ToString();
        else
            txtDonViCongTac.Text = hoivien.DonViCongTac;
        if (hoivien.SoThichId != null)
            drSoThich.SelectedValue = hoivien.SoThichId.Value.ToString();

        txtTuChoi.Text = hoivien.LyDoTuChoi;

        string strSoQD = "";
        DateTime? deNgayQD = null;

        try
        {

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select SoQuyetDinhKetNap, NgayQuyetDinhKetNap from tblHoiVienCaNhan where HoiVienCaNhanID = " + Convert.ToInt32(hoivien.HoiVienCaNhanId.Value);

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];



            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                strSoQD = tem["SoQuyetDinhKetNap"].ToString();
                deNgayQD = Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]);
            }
        }
        catch { }

        txtSoQuyetDinh.Text = strSoQD;
        if (deNgayQD != null)
            NgayQD.Value = deNgayQD.Value.ToString("dd/MM/yyyy");

        // chứng chỉ
        LoadChungChi();

        // quá trình làm việc
        LoadQuaTrinh();

        // LoadSoQuyetDinh(hoivien.HoiVienCaNhanId.Value.ToString());
        // file
        loaduploadFileDinhKem(hoivien.DonHoiVienCaNhanId);

        if (Request.QueryString["act"] == "view")
        {
            string html = "";
            html += "<script>";
            html += "$('#form_nhaphoso input,select,textarea').attr('disabled', true);";
            html += "$('#quaylai').attr('disabled', false);";
            html += "$(\"a\").unbind('click');";
            html += "$('#quaylai').bind(\"click\", function() {window.location = window.location.href.split('?')[0] + '?page=ketnaphoivien';});";
            html += "</script>";

            scriptdisable.Controls.Add(new LiteralControl(html));
        }
    }


    protected void Load_User()
    {
        SqlDonHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connStr, true, "");

        hoivien = HoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));
    }

    protected void loaduploadFileDinhKem(int donHoiVienId)
    {
        try
        {
            conn.Open();

            string DoiTuong = (Request.QueryString["ktv"] == "1") ? "2" : "1";
            SqlCommand sql = new SqlCommand();

            if (donHoiVienId == 0)
            {
                sql.CommandText = " SELECT  LoaiGiayToID ,TenFile as TenBieuMau ,DoiTuong ,BatBuoc  , 0 as FileID  , ' ' as TenFileDinhKem " +
                                  " FROM tblDMLoaiGiayTo  " +
                                  " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuong + "' order by tblDMLoaiGiayTo.TenFile   ";
            }
            else
            {
                sql.CommandText =
                "    SELECT   LoaiGiayToID ,TenFile as TenBieuMau  ,DoiTuong  ,BatBuoc , " +
                              "   ( SELECT TOP 1 ISNULL ( tblDonHoiVienFile.FileID,0)  FROM  tblDonHoiVienFile      WHERE  tblDonHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblDonHoiVienFile.DonHoiVienCaNhanID= " + donHoiVienId + "  ) as FileID ," +
                               " ( SELECT  TOP 1 ISNULL(tblDonHoiVienFile.TenFile,' ')   FROM  tblDonHoiVienFile  WHERE  tblDonHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblDonHoiVienFile.DonHoiVienCaNhanID= " + donHoiVienId + " ) as  TenFileDinhKem    " +
                " FROM tblDMLoaiGiayTo  " +
                " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuong + "' order by tblDMLoaiGiayTo.TenFile  ";


            }

            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;


            PagedDataSource objPds1 = new PagedDataSource();
            objPds1.DataSource = ds.Tables[0].DefaultView;

            FileDinhKem_grv.DataSource = objPds1;
            FileDinhKem_grv.DataBind();
            //FileDinhKem_grv.Columns[3].Visible = false; 
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;

            // set validate

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                String sTenLoaiFile = row.Cells[0].Text.Trim();
                String sLoaiFileID = row.Cells[3].Text.Trim();
                String sFileDinhKemID = row.Cells[4].Text.Trim();
                String sBatbuoc = row.Cells[5].Text.Trim();

                String sTenFile = row.Cells[6].Text.Trim();

                // check validate
                if (sBatbuoc == "0" || !String.IsNullOrEmpty(sTenFile))
                {
                    ((RequiredFieldValidator)row.FindControl("RequiredFieldFileUpload")).Visible = false;


                }

                // an hien chu thich bat buoc
                if (sBatbuoc == "0")
                {
                    ((Label)row.FindControl("requred")).Visible = false;
                    //((Label)row.FindControl("requred")).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                }
                else
                {
                    //((Label)row.FindControl("requred")).Text = "(*) ";
                }

                //// an hien link bieu mau

                //if (String.IsNullOrEmpty(sBieuMau))
                //{
                //    //  ((HyperLink)row.FindControl("linkBieuMau")).Visible = false;
                //    HyperLink likBM = ((HyperLink)row.FindControl("linkBieuMau"));// 

                //    likBM.Text = "&nbsp; &nbsp; &nbsp;";
                //    likBM.NavigateUrl = "";

                //}

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }




    }

    //private void LoadTenFile()
    //{
    //    SqlCommand cmd = new SqlCommand();
    //    cmd.CommandText = "SELECT A.TenFile, A.LoaiGiayToID FROM tblDonHoiVienFile A WHERE A.DonHoiVienCaNhanID = " + Request.QueryString["id"];

    //    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

    //    foreach (DataRow row in dtb.Rows)
    //    {
    //        switch (row["LoaiGiayToID"].ToString())
    //        {
    //            case "1":
    //                lbFileAnh.Text = row["TenFile"].ToString();
    //                break;
    //            case "2":
    //                lbFileQuyetDinh.Text = row["TenFile"].ToString();
    //                break;
    //            case "3":
    //                lbFileBanSao.Text = row["TenFile"].ToString();
    //                break;
    //            case "4":
    //                lbFileChuKy.Text = row["TenFile"].ToString();
    //                break;
    //            case "5":
    //                lbFileGiayChungNhan.Text = row["TenFile"].ToString();
    //                break;
    //            case "6":
    //                lbFileGiayToKhac.Text = row["TenFile"].ToString();
    //                break;
    //            case "7":
    //                lbFileDonXinGiaNhap.Text = row["TenFile"].ToString();
    //                break;
    //            default:
    //                break;
    //        }
    //    }
    //}

    private void LoadQuaTrinh()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT A.ThoiGianCongTac, A.ChucVu, A.NoiLamViec FROM tblDonHoiVienCaNhanQuaTrinhCongTac A WHERE A.DonHoiVienCaNhanID = " + Request.QueryString["id"];

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb.Rows.Count != 0)
        {
            if (ViewState["QuaTrinh"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["QuaTrinh"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["ThoiGian"] = row["ThoiGianCongTac"].ToString();
                    drCurrentRow["ChucVu"] = row["ChucVu"].ToString();
                    drCurrentRow["NoiLamViec"] = row["NoiLamViec"].ToString();

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["QuaTrinh"] = dtCurrentTable;

                gvQuaTrinh.DataSource = dtCurrentTable;
                gvQuaTrinh.DataBind();

                SetPreviousData();
            }
        }
    }

    private void LoadChungChi()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT A.SoChungChi, A.NgayCap FROM tblDonHoiVienCaNhanChungChi A WHERE A.DonHoiVienCaNhanID = " + Request.QueryString["id"];

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb.Rows.Count != 0)
        {
            if (ViewState["ChungChi"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["ChungChi"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["SoChungChi"] = row["SoChungChi"].ToString();
                    if (!string.IsNullOrEmpty(row["NgayCap"].ToString()))
                        drCurrentRow["NgayCap"] = DateTime.Parse(row["NgayCap"].ToString()).ToShortDateString();

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["ChungChi"] = dtCurrentTable;

                gvChungChi.DataSource = dtCurrentTable;
                gvChungChi.DataBind();

                SetPreviousData_ChungChi();
            }
        }
    }

    public void LoadDropDown()
    {

        DataTable dtb = new DataTable();

        string sql = "";

        // Đơn vị
        sql = "SELECT 0 AS HoiVienTapTheID, N'<< Khác >>' AS TenDoanhNghiep, '-' AS SoHieu UNION ALL (SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE '--- ' + ISNULL(A.SoHieu,LTRIM(RTRIM(A.MaHoiVienTapThe))) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID) ORDER BY SoHieu";
        SqlCommand cmd = new SqlCommand(sql);

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drDonVi.DataSource = dtb;
        drDonVi.DataBind();

        dtb.Clear();
        // Quốc tịch
        sql = "SELECT * FROM tblDMQuocTich";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drQuocTich.DataSource = dtb;
        drQuocTich.DataBind();

        dtb.Clear();
        // Trường ĐH
        sql = "SELECT 0 AS TruongDaiHocID, N'<< Lựa chọn >>' AS TenTruongDaiHoc UNION ALL (SELECT TruongDaiHocID,TenTruongDaiHoc FROM tblDMTruongDaiHoc)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drTotNghiep.DataSource = dtb;
        drTotNghiep.DataBind();


        dtb.Clear();
        // Chuyên ngành
        sql = "SELECT 0 AS ChuyenNganhDaoTaoID, N'<< Lựa chọn >>' AS TenChuyenNganhDaoTao UNION ALL (SELECT ChuyenNganhDaoTaoID,TenChuyenNganhDaoTao FROM tblDMChuyenNganhDaoTao)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChuyenNganh.DataSource = dtb;
        drChuyenNganh.DataBind();

        dtb.Clear();
        // Học vị
        sql = "SELECT 0 AS HocViID, N'<< Lựa chọn >>' AS TenHocVi UNION ALL (SELECT HocViID, TenHocVi FROM tblDMHocVi)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHocVi.DataSource = dtb;
        drHocVi.DataBind();

        dtb.Clear();
        // Học hàm
        sql = "SELECT 0 AS HocHamID, N'<< Lựa chọn >>' AS TenHocHam UNION ALL (SELECT HocHamID, TenHocHam FROM tblDMHocHam)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHocHam.DataSource = dtb;
        drHocHam.DataBind();

        dtb.Clear();
        // Nơi cấp CMND
        sql = "SELECT * FROM tblDMTinh";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drNoiCap_CMND.DataSource = dtb;
        drNoiCap_CMND.DataBind();

        dtb.Clear();
        // Ngân hàng
        sql = "SELECT 0 AS NganHangID, N'<< Lựa chọn >>' AS TenNganHang UNION ALL (SELECT NganHangID, TenNganHang FROM tblDMNganHang)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drNganHang.DataSource = dtb;
        drNganHang.DataBind();

        dtb.Clear();
        // Load chức vụ
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChucVu.DataSource = dtb;
        drChucVu.DataBind();

        dtb.Clear();
        // Load sở thích
        sql = "SELECT 0 AS SoThichID, N'<< Lựa chọn >>' AS TenSoThich UNION ALL (SELECT SoThichID, TenSoThich FROM tblDMSoThich)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drSoThich.DataSource = dtb;
        drSoThich.DataBind();


    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid();
    }

    protected void ButtonAddChungChi_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_ChungChi();
    }

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("ThoiGian", typeof(string)));
        dt.Columns.Add(new DataColumn("ChucVu", typeof(string)));
        dt.Columns.Add(new DataColumn("NoiLamViec", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["ThoiGian"] = "";
        dr["ChucVu"] = "";
        dr["NoiLamViec"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["QuaTrinh"] = dt;

        gvQuaTrinh.DataSource = dt;
        gvQuaTrinh.DataBind();
    }
    private void AddNewRowToGrid()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["QuaTrinh"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["QuaTrinh"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");

                        TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                        TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                        dtCurrentTable.Rows[i - 1]["ThoiGian"] = thoigian.Text;
                        dtCurrentTable.Rows[i - 1]["ChucVu"] = chucvu.Text;
                        dtCurrentTable.Rows[i - 1]["NoiLamViec"] = noilamviec.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["ThoiGian"] = "";
                    drCurrentRow["ChucVu"] = "";
                    drCurrentRow["NoiLamViec"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["ThoiGian"] = "";
                    dr["ChucVu"] = "";
                    dr["NoiLamViec"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["QuaTrinh"] = dtCurrentTable;

                gvQuaTrinh.DataSource = dtCurrentTable;
                gvQuaTrinh.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["QuaTrinh"] != null)
        {
            DataTable dt = (DataTable)ViewState["QuaTrinh"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                    thoigian.Text = dt.Rows[i]["ThoiGian"].ToString();
                    chucvu.Text = dt.Rows[i]["ChucVu"].ToString();
                    noilamviec.Text = dt.Rows[i]["NoiLamViec"].ToString();
                    rowIndex++;
                }
            }
        }
    }


    private void SetInitialRow_ChungChi()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));

        dt.Columns.Add(new DataColumn("SoChungChi", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayCap", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;

        dr["SoChungChi"] = "";
        dr["NgayCap"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["ChungChi"] = dt;

        gvChungChi.DataSource = dt;
        gvChungChi.DataBind();
    }
    private void AddNewRowToGrid_ChungChi()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["ChungChi"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["ChungChi"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values

                        TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                        TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                        dtCurrentTable.Rows[i - 1]["SoChungChi"] = sochungchi.Text;
                        dtCurrentTable.Rows[i - 1]["NgayCap"] = ngaycap.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;

                    drCurrentRow["SoChungChi"] = "";
                    drCurrentRow["NgayCap"] = "";

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;

                    dr["SoChungChi"] = "";
                    dr["NgayCap"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["ChungChi"] = dtCurrentTable;

                gvChungChi.DataSource = dtCurrentTable;
                gvChungChi.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_ChungChi();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_ChungChi()
    {
        int rowIndex = 0;
        if (ViewState["ChungChi"] != null)
        {
            DataTable dt = (DataTable)ViewState["ChungChi"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                    sochungchi.Text = dt.Rows[i]["SoChungChi"].ToString();
                    ngaycap.Text = dt.Rows[i]["NgayCap"].ToString();

                    rowIndex++;
                }
            }
        }
    }

    protected void gvQuaTrinh_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvQuaTrinh_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["QuaTrinh"] != null)
        {
            DataTable dt = (DataTable)ViewState["QuaTrinh"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                    dt.Rows[i - 1]["ThoiGian"] = thoigian.Text;

                    dt.Rows[i - 1]["ChucVu"] = chucvu.Text;
                    dt.Rows[i - 1]["NoiLamViec"] = noilamviec.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["QuaTrinh"] = dt;

            gvQuaTrinh.DataSource = dt;
            gvQuaTrinh.DataBind();

            SetPreviousData();
        }
    }


    protected void gvChungChi_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvChungChi_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["ChungChi"] != null)
        {
            DataTable dt = (DataTable)ViewState["ChungChi"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values

                    TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                    dt.Rows[i - 1]["SoChungChi"] = sochungchi.Text;
                    dt.Rows[i - 1]["NgayCap"] = ngaycap.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["ChungChi"] = dt;

            gvChungChi.DataSource = dt;
            gvChungChi.DataBind();

            SetPreviousData_ChungChi();
        }
    }

    protected void gvChungChi_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    public string SinhSoDon()
    {
        string result = "";

        string sql = "SELECT dbo.LaySoChayDonHoiVienCaNhan('" + DateTime.Now.Year.ToString().Substring(2, 2) + "')";
        SqlCommand cmd = new SqlCommand(sql);
        string so = DataAccess.DLookup(cmd);

        result = "DKHVCN" + DateTime.Now.Year.ToString().Substring(2, 2) + so;

        return result;
    }

    public string SinhID(string HoDem, string Ten, string SoChungChiKTV)
    {
        string result = "";

        string sql = "SELECT dbo.BoDau(N'" + Ten + "')";
        SqlCommand cmd = new SqlCommand(sql);
        string ten = DataAccess.DLookup(cmd);

        cmd.CommandText = "SELECT dbo.BoDau(N'" + HoDem + "')";
        string hodem = DataAccess.DLookup(cmd);

        string[] array = hodem.Split(' ');
        hodem = "";
        foreach (string s in array)
        {
            if (!string.IsNullOrEmpty(s))
                hodem += s.Substring(0, 1).ToLower();
        }

        result = ten + hodem;

        for (int i = 0; i < SoChungChiKTV.Length; i++)
        {
            if (Regex.IsMatch(SoChungChiKTV.Substring(i, 1), @"^[0-9]\d*\.?[0]*$"))
                result += SoChungChiKTV.Substring(i, 1);
        }

        return result;
    }

    protected void CapnhatFile(int donHoiVienId)
    {
        if (donHoiVienId != null && donHoiVienId != 0)
        {
            SqlDonHoiVienFileProvider DonHoiVien_provider = new SqlDonHoiVienFileProvider(connStr, true, "");

            HttpPostedFile styledfileupload;

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                FileUpload fileUp = (FileUpload)row.FindControl("FileUp");
                FileDinhKem1 = new DonHoiVienFile();
                if (fileUp != null && fileUp.FileName != "")
                {
                    int fileUploadId = 0;
                    int loaiFieldId = 0;
                    String sTempLoaiFileID = row.Cells[3].Text;
                    String sTempFileUploadID = row.Cells[4].Text;

                    if (!String.IsNullOrEmpty(sTempFileUploadID) && sTempFileUploadID != "0" && sTempFileUploadID != "&nbsp;")
                    {
                        fileUploadId = Convert.ToInt32(row.Cells[4].Text);

                    }
                    if (!String.IsNullOrEmpty(sTempLoaiFileID) && sTempLoaiFileID != "0" && sTempLoaiFileID != "&nbsp;")
                    {
                        loaiFieldId = Convert.ToInt32(row.Cells[3].Text);

                    }
                    string styledfileupload_name = "";
                    styledfileupload = fileUp.PostedFile;
                    if (styledfileupload.FileName != "")
                    {
                        styledfileupload_name = fileUp.FileName;


                        byte[] datainput = new byte[styledfileupload.ContentLength];
                        styledfileupload.InputStream.Read(datainput, 0, styledfileupload.ContentLength);

                        FileDinhKem1.FileId = fileUploadId;
                        FileDinhKem1.DonHoiVienCaNhanId = donHoiVienId;
                        FileDinhKem1.LoaiGiayToId = loaiFieldId;
                        FileDinhKem1.FileDinhKem = datainput;
                        //FileDinhKem1.Tenfile = styledfileupload.FileName;
                        FileDinhKem1.TenFile = new System.IO.FileInfo(styledfileupload.FileName).Name;
                    }
                    if (FileDinhKem1.FileId == 0)
                    {
                        DonHoiVien_provider.Insert(FileDinhKem1);
                    }
                    else
                    {
                        DonHoiVien_provider.Update(FileDinhKem1);
                    }

                }
                else
                {

                }

            }


            loaduploadFileDinhKem(donHoiVienId);

        }
    }

    protected void PheDuyetFile(int HoiVienId)
    {
        if (HoiVienId != null && HoiVienId != 0)
        {
            SqlHoiVienFileProvider HoiVien_provider = new SqlHoiVienFileProvider(connStr, true, "");

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM tblDonHoiVienFile WHERE DonHoiVienCaNhanID = " + Request.QueryString["id"];

            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

            foreach (DataRow row in dtb.Rows)
            {
                HoiVienFile file = new HoiVienFile();
                file.HoiVienCaNhanId = HoiVienId;
                file.LoaiGiayToId = int.Parse(row["LoaiGiayToID"].ToString());
                file.TenFile = row["TenFile"].ToString();
                file.FileDinhKem = (byte[])row["FileDinhKem"];

                HoiVien_provider.Insert(file);
            }
        }
    }

    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        try
        {

            string thongbao = "";

            string sql = "";
            sql = "SELECT * FROM tblNguoiDung WHERE Email = '" + txtEmail.Text + "'";
            SqlCommand cmd = new SqlCommand(sql);

            DataTable dtb = new DataTable();

            dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
            if (dtb.Rows.Count != 0)
            {
                thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>
							<p>" + @"Đã có tài khoản trong chương trình phần mềm quản lý hội viên với tên đăng nhập <b>" + dtb.Rows[0]["TenDangNhap"].ToString() + @"</b></p>							
						</div>";
                placeMessage.Controls.Add(new LiteralControl(thongbao));
                return;
            }
            else
            {

                dtb.Clear();
                sql = "SELECT * FROM tblHoiVienCaNhan A INNER JOIN tblNguoiDung B ON A.MaHoiVienCaNhan = B.TenDangNhap WHERE A.SoChungChiKTV = '" + txtSoChungChiKTV.Text + "'";
                cmd = new SqlCommand(sql);

                dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                if (dtb.Rows.Count != 0)
                {
                    thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>
							<p>" + @"Đã có tài khoản trong chương trình phần mềm quản lý hội viên với tên đăng nhập <b>" + dtb.Rows[0]["TenDangNhap"].ToString() + @"</b></p>							
						</div>";
                    placeMessage.Controls.Add(new LiteralControl(thongbao));
                    return;
                }
            }

            SqlDonHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connStr, true, "");

            SqlDonHoiVienCaNhanChungChiProvider HoiVienCaNhanChungChi_provider = new SqlDonHoiVienCaNhanChungChiProvider(connStr, true, "");
            SqlDonHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlDonHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
            SqlDonHoiVienFileProvider HoiVienFile_provider = new SqlDonHoiVienFileProvider(connStr, true, "");


            string gioitinh = (rdNam.Checked) ? "1" : "0";

            // update vào bảng tblDonHoiVienCaNhan
            DonHoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

            hoivien.HoDem = txtHoDem.Text;
            hoivien.Ten = txtTen.Text;
            hoivien.GioiTinh = gioitinh;
            if (!string.IsNullOrEmpty(NgaySinh.Value))
                hoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
            hoivien.QuocTichId = int.Parse(drQuocTich.SelectedValue);

            if (!string.IsNullOrEmpty(Request.Form["TinhID_QueQuan"]))
                hoivien.QueQuanTinhId = Request.Form["TinhID_QueQuan"].ToString();
            if (!string.IsNullOrEmpty(Request.Form["HuyenID_QueQuan"]))
                hoivien.QueQuanHuyenId = Request.Form["HuyenID_QueQuan"].ToString();
            if (!string.IsNullOrEmpty(Request.Form["XaID_QueQuan"]))
                hoivien.QueQuanXaId = Request.Form["XaID_QueQuan"].ToString();

            hoivien.DiaChi = txtDiaChi.Text;
            if (!string.IsNullOrEmpty(Request.Form["TinhID_DiaChi"]))
                hoivien.DiaChiTinhId = Request.Form["TinhID_DiaChi"].ToString();
            if (!string.IsNullOrEmpty(Request.Form["HuyenID_DiaChi"]))
                hoivien.DiaChiHuyenId = Request.Form["HuyenID_DiaChi"].ToString();
            if (!string.IsNullOrEmpty(Request.Form["XaID_DiaChi"]))
                hoivien.DiaChiXaId = Request.Form["XaID_DiaChi"].ToString();

            hoivien.SoGiayChungNhanDkhn = txtSoGiayDHKN.Text;
            if (!string.IsNullOrEmpty(Request.Form["NgayCap_DHKN"]))
                hoivien.NgayCapGiayChungNhanDkhn = DateTime.ParseExact(Request.Form["NgayCap_DHKN"], "dd/MM/yyyy", null);
            if (!string.IsNullOrEmpty(Request.Form["HanCapTu"]))
                hoivien.HanCapTu = DateTime.ParseExact(Request.Form["HanCapTu"], "dd/MM/yyyy", null);
            if (!string.IsNullOrEmpty(Request.Form["HanCapDen"]))
                hoivien.HanCapDen = DateTime.ParseExact(Request.Form["HanCapDen"], "dd/MM/yyyy", null);

            hoivien.SoChungChiKtv = txtSoChungChiKTV.Text;
            if (!string.IsNullOrEmpty(NgayCap_ChungChiKTV.Value))
                hoivien.NgayCapChungChiKtv = DateTime.ParseExact(NgayCap_ChungChiKTV.Value, "dd/MM/yyyy", null);

            hoivien.Email = txtEmail.Text;
            hoivien.DienThoai = txtDienThoai.Text;
            hoivien.Mobile = txtMobile.Text;
            if (drChucVu.SelectedValue != "0")
                hoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
            if (drTotNghiep.SelectedValue != "0")
                hoivien.TruongDaiHocId = int.Parse(drTotNghiep.SelectedValue);
            if (drChuyenNganh.SelectedValue != "0")
                hoivien.ChuyenNganhDaoTaoId = int.Parse(drChuyenNganh.SelectedValue);
            hoivien.ChuyenNganhNam = txtNam_ChuyenNganh.Text;
            if (drHocVi.SelectedValue != "0")
                hoivien.HocViId = int.Parse(drHocVi.SelectedValue);
            hoivien.HocViNam = txtNam_HocVi.Text;
            if (drHocHam.SelectedValue != "0")
                hoivien.HocHamId = int.Parse(drHocHam.SelectedValue);
            hoivien.HocHamNam = txtNam_HocHam.Text;
            hoivien.SoCmnd = txtSoCMND.Text;
            if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
                hoivien.CmndNgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);
            hoivien.CmndTinhId = int.Parse(drNoiCap_CMND.SelectedValue);
            hoivien.SoTaiKhoanNganHang = txtSoTKNganHang.Text;
            if (drNganHang.SelectedValue != "0")
                hoivien.NganHangId = int.Parse(drNganHang.SelectedValue);
            if (drDonVi.SelectedValue != "0")
                hoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
            else
                hoivien.DonViCongTac = txtDonViCongTac.Text;
            if (drSoThich.SelectedValue != "0")
                hoivien.SoThichId = int.Parse(drSoThich.SelectedValue);

            hoivien.TinhTrangId = 3; // chờ duyệt

            HoiVienCaNhan_provider.Update(hoivien);

            sql = "DELETE tblDonHoiVienCaNhanChungChi WHERE DonHoiVienCaNhanId = " + Request.QueryString["id"];
            cmd = new SqlCommand(sql);
            DataAccess.RunActionCmd(cmd);
            // insert chứng chỉ
            for (int i = 0; i < gvChungChi.Rows.Count; i++)
            {
                //DropDownList drChungChi = (DropDownList)gvChungChi.Rows[i].Cells[1].FindControl("drChungChi");
                TextBox sochungchi = (TextBox)gvChungChi.Rows[i].Cells[1].FindControl("txtSoChungChi");
                TextBox ngaycap = (TextBox)gvChungChi.Rows[i].Cells[2].FindControl("txtDate");

                if (!string.IsNullOrEmpty(sochungchi.Text))
                {
                    DonHoiVienCaNhanChungChi chungchi = new DonHoiVienCaNhanChungChi();
                    chungchi.DonHoiVienCaNhanId = hoivien.DonHoiVienCaNhanId;

                    chungchi.SoChungChi = sochungchi.Text;
                    if (!string.IsNullOrEmpty(ngaycap.Text))
                        chungchi.NgayCap = DateTime.ParseExact(ngaycap.Text, "dd/MM/yyyy", null);

                    HoiVienCaNhanChungChi_provider.Insert(chungchi);
                }
            }

            sql = "DELETE tblDonHoiVienCaNhanQuaTrinhCongTac WHERE DonHoiVienCaNhanId = " + Request.QueryString["id"];
            cmd = new SqlCommand(sql);
            DataAccess.RunActionCmd(cmd);
            // insert quá trình làm việc
            for (int i = 0; i < gvQuaTrinh.Rows.Count; i++)
            {
                TextBox thoigian = (TextBox)gvQuaTrinh.Rows[i].Cells[1].FindControl("txtThoiGian");
                TextBox chucvu = (TextBox)gvQuaTrinh.Rows[i].Cells[3].FindControl("txtChucVu");
                TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[i].Cells[4].FindControl("txtNoiLamViec");

                DonHoiVienCaNhanQuaTrinhCongTac quatrinh = new DonHoiVienCaNhanQuaTrinhCongTac();
                quatrinh.DonHoiVienCaNhanId = hoivien.DonHoiVienCaNhanId;
                quatrinh.ThoiGianCongTac = thoigian.Text;
                quatrinh.ChucVu = chucvu.Text;
                quatrinh.NoiLamViec = noilamviec.Text;

                HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
            }

            // file đính kèm
            CapnhatFile(hoivien.DonHoiVienCaNhanId);

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Cập nhật đơn xin kết nạp hội viên " + hoivien.HoDem + " " + hoivien.Ten);

            thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

            placeMessage.Controls.Add(new LiteralControl(thongbao));

        }
        catch (Exception ex)
        {
            string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + ex.ToString() + @"</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
    }

    protected void btnTiepNhan_Click(object sender, EventArgs e)
    {
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 3, NgayTiepNhan = '" + DateTime.Now.ToString("yyyy-MM-dd") + "', NguoiTiepNhan = N'" + cm.Admin_HoVaTen + "' WHERE DonHoiVienCaNhanID = " + Request.QueryString["id"];

            DataAccess.RunActionCmd(cmd);

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Tiếp nhận đơn xin kết nạp hội viên " + hoivien.HoDem + " " + hoivien.Ten);

            string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Tiếp nhận hồ sơ thành công.</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
        catch (Exception ex)
        {
            string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + ex.ToString() + @"</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
    }

    protected void btnTuChoi_Click(object sender, EventArgs e)
    {
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 2, LyDoTuChoi = N'" + txtTuChoi.Text + "' WHERE DonHoiVienCaNhanID = " + Request.QueryString["id"];

            DataAccess.RunActionCmd(cmd);

            cmd = new SqlCommand();
            cmd.CommandText = "SELECT A.HoiVienCaNhanID, B.LoaiHoiVien FROM tblDonHoiVienCaNhan A INNER JOIN tblNguoiDung B ON A.HoiVienCaNhanID = B.HoiVienID WHERE A.DonHoiVienCaNhanID = " + Request.QueryString["id"];
            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

            // Gửi mail
            string thongbao = "";
            string body = "";
            body += "Đơn xin kết nạp hội viên cá nhân của anh/chị không được tiếp nhận. Lý do: ";
            body += txtTuChoi.Text + "<BR/>";
            string http_Server = (string)System.Configuration.ConfigurationSettings.AppSettings["http_Server"];
            body += "Anh/chị có thể cập nhật lại thông tin đơn theo đường links : " + http_Server + "Page/DangKyHoiVien/DangKyHoiVienCaNhan.aspx?id=" + Request.QueryString["id"] + " <BR/>";
            thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>
							<p>" + "Từ chối tiếp nhận thành công." + @"</p>							
						</div>";
            if (dtb.Rows.Count != 0)
            {
                // Gửi thông báo
                cm.GuiThongBao(dtb.Rows[0]["HoiVienCaNhanID"].ToString(), dtb.Rows[0]["LoaiHoiVien"].ToString(), "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ tiếp nhận đơn xin kết nạp hội viên cá nhân", body);
            }

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối tiếp nhận đơn xin kết nạp hội viên " + hoivien.HoDem + " " + hoivien.Ten);

            string msg = "";
            if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ tiếp nhận đơn xin kết nạp hội viên cá nhân", body, ref msg))
            {
                placeMessage.Controls.Add(new LiteralControl(thongbao));
            }
            else
                placeMessage.Controls.Add(new LiteralControl(msg));
        }
        catch (Exception ex)
        {
            string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + ex.ToString() + @"</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
    }

    protected void btnPheDuyet_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(lbHoiVienCaNhanID.Text))
            {
                PheDuyet();
            }
            else
                PheDuyet_HoiVien();

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin kết nạp hội viên " + hoivien.HoDem + " " + hoivien.Ten);

        }
        catch (Exception ex)
        {
            string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + ex.ToString() + @"</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
    }

    protected void btnTuChoiDuyet_Click(object sender, EventArgs e)
    {
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 4, LyDoTuChoi = N'" + txtTuChoi.Text + "' WHERE DonHoiVienCaNhanID = " + Request.QueryString["id"];

            DataAccess.RunActionCmd(cmd);
            cmd = new SqlCommand();
            cmd.CommandText = "SELECT A.HoiVienCaNhanID, B.LoaiHoiVien FROM tblDonHoiVienCaNhan A INNER JOIN tblNguoiDung B ON A.HoiVienCaNhanID = B.HoiVienID WHERE A.DonHoiVienCaNhanID = " + Request.QueryString["id"];
            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

            // Gửi mail
            string thongbao = "";
            string body = "";
            body += "Đơn xin kết nạp hội viên cá nhân của anh/chị không được phê duyệt. Lý do: ";
            body += txtTuChoi.Text + "<BR/>";
            string http_Server = (string)System.Configuration.ConfigurationSettings.AppSettings["http_Server"];
            body += "Anh/chị có thể cập nhật lại thông tin đơn theo đường links : " + http_Server + "Page/DangKyHoiVien/DangKyHoiVienCaNhan.aspx?id=" + Request.QueryString["id"] + " <BR/>";
            thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>
							<p>" + "Từ chối phê duyệt thành công." + @"</p>							
						</div>";
            if (dtb.Rows.Count != 0)
            {
                // Gửi thông báo
                cm.GuiThongBao(dtb.Rows[0]["HoiVienCaNhanID"].ToString(), dtb.Rows[0]["LoaiHoiVien"].ToString(), "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ phê duyệt đơn xin kết nạp hội viên cá nhân", body);
            }

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối phê duyệt đơn xin kết nạp hội viên " + hoivien.HoDem + " " + hoivien.Ten);

            string msg = "";
            if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ phê duyệt đơn xin kết nạp hội viên cá nhân", body, ref msg))
            {
                placeMessage.Controls.Add(new LiteralControl(thongbao));
            }
            else
                placeMessage.Controls.Add(new LiteralControl(msg));

        }
        catch (Exception ex)
        {
            string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + ex.ToString() + @"</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin.aspx?page=ketnaphoivien");
    }

    protected void btnSuaQuyetDinh_Click(object sender, EventArgs e)
    {
        SqlDonHoiVienCaNhanProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connStr, true, "");
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        DonHoiVienCaNhan donhoivien = DonHoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));
        HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(donhoivien.HoiVienCaNhanId.Value);

        hoivien.SoQuyetDinhKetNap = txtSoQuyetDinh.Text;
        hoivien.NgayQuyetDinhKetNap = DateTime.ParseExact(NgayQD.Value, "dd/MM/yyyy", null);

        HoiVienCaNhan_provider.Update(hoivien);

        cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Cập nhật số quyết định/ngày quyết định của đơn xin kết nạp hội viên " + hoivien.HoDem + " " + hoivien.Ten);
        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

        placeMessage.Controls.Add(new LiteralControl(thongbao));
    }

    // người quan tâm và kiểm toán viên
    protected void PheDuyet()
    {
        SqlDonHoiVienCaNhanProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connStr, true, "");

        DonHoiVienCaNhan donhoivien = DonHoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");

        SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connStr, true, "");
        SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
        SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connStr, true, "");

        string gioitinh = (rdNam.Checked) ? "1" : "0";

        HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(lbHoiVienCaNhanID.Text));
        if (drLoaiHoiVien.SelectedValue != "-1" && drLoaiHoiVien.SelectedValue != "-2")
            hoivien.LoaiHoiVienCaNhanChiTiet = int.Parse(drLoaiHoiVien.SelectedValue);
        else
            hoivien.LoaiHoiVienCaNhanChiTiet = 0;
        hoivien.LoaiHoiVienCaNhan = "1";
        hoivien.HoDem = txtHoDem.Text;
        hoivien.Ten = txtTen.Text;
        hoivien.GioiTinh = gioitinh;
        if (!string.IsNullOrEmpty(NgaySinh.Value))
            hoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
        hoivien.QuocTichId = int.Parse(drQuocTich.SelectedValue);

        if (!string.IsNullOrEmpty(Request.Form["TinhID_QueQuan"]))
            hoivien.QueQuanTinhId = Request.Form["TinhID_QueQuan"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["HuyenID_QueQuan"]))
            hoivien.QueQuanHuyenId = Request.Form["HuyenID_QueQuan"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["XaID_QueQuan"]))
            hoivien.QueQuanXaId = Request.Form["XaID_QueQuan"].ToString();

        hoivien.DiaChi = txtDiaChi.Text;
        if (!string.IsNullOrEmpty(Request.Form["TinhID_DiaChi"]))
            hoivien.DiaChiTinhId = Request.Form["TinhID_DiaChi"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["HuyenID_DiaChi"]))
            hoivien.DiaChiHuyenId = Request.Form["HuyenID_DiaChi"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["XaID_DiaChi"]))
            hoivien.DiaChiXaId = Request.Form["XaID_DiaChi"].ToString();

        hoivien.SoGiayChungNhanDkhn = txtSoGiayDHKN.Text;
        if (!string.IsNullOrEmpty(Request.Form["NgayCap_DHKN"]))
            hoivien.NgayCapGiayChungNhanDkhn = DateTime.ParseExact(Request.Form["NgayCap_DHKN"], "dd/MM/yyyy", null);
        if (!string.IsNullOrEmpty(Request.Form["HanCapTu"]))
            hoivien.HanCapTu = DateTime.ParseExact(Request.Form["HanCapTu"], "dd/MM/yyyy", null);
        if (!string.IsNullOrEmpty(Request.Form["HanCapDen"]))
            hoivien.HanCapDen = DateTime.ParseExact(Request.Form["HanCapDen"], "dd/MM/yyyy", null);

        hoivien.SoChungChiKtv = txtSoChungChiKTV.Text;
        hoivien.NgayCapChungChiKtv = DateTime.ParseExact(NgayCap_ChungChiKTV.Value, "dd/MM/yyyy", null);

        hoivien.Email = txtEmail.Text;
        hoivien.DienThoai = txtDienThoai.Text;
        hoivien.Mobile = txtMobile.Text;
        if (drChucVu.SelectedValue != "0")
            hoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
        if (drTotNghiep.SelectedValue != "0")
            hoivien.TruongDaiHocId = int.Parse(drTotNghiep.SelectedValue);
        if (drChuyenNganh.SelectedValue != "0")
            hoivien.ChuyenNganhDaoTaoId = int.Parse(drChuyenNganh.SelectedValue);
        hoivien.ChuyenNganhNam = txtNam_ChuyenNganh.Text;
        if (drHocVi.SelectedValue != "0")
            hoivien.HocViId = int.Parse(drHocVi.SelectedValue);
        hoivien.HocViNam = txtNam_HocVi.Text;
        if (drHocHam.SelectedValue != "0")
            hoivien.HocHamId = int.Parse(drHocHam.SelectedValue);
        hoivien.HocHamNam = txtNam_HocHam.Text;
        hoivien.SoCmnd = txtSoCMND.Text;
        if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
            hoivien.CmndNgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);
        hoivien.CmndTinhId = int.Parse(drNoiCap_CMND.SelectedValue);
        hoivien.SoTaiKhoanNganHang = txtSoTKNganHang.Text;
        if (drNganHang.SelectedValue != "0")
            hoivien.NganHangId = int.Parse(drNganHang.SelectedValue);
        if (drDonVi.SelectedValue != "0")
            hoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
        else
            hoivien.DonViCongTac = txtDonViCongTac.Text;
        if (drSoThich.SelectedValue != "0")
            hoivien.SoThichId = int.Parse(drSoThich.SelectedValue);

        hoivien.NgayGiaNhap = DateTime.Now;
        hoivien.NgayDuyet = DateTime.Now;
        hoivien.NguoiCapNhat = cm.Admin_HoVaTen;
        hoivien.NguoiDuyet = cm.Admin_HoVaTen;
        hoivien.TinhTrangHoiVienId = 1;
        hoivien.SoQuyetDinhKetNap = txtSoQuyetDinh.Text;
        if (!string.IsNullOrEmpty(NgayQD.Value))
            hoivien.NgayQuyetDinhKetNap = DateTime.ParseExact(NgayQD.Value, "dd/MM/yyyy", null);

        HoiVienCaNhan_provider.Update(hoivien);

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "DELETE tblHoiVienCaNhanChungChiQuocTe WHERE HoiVienCaNhanID = " + lbHoiVienCaNhanID.Text + ";";
        cmd.CommandText += "DELETE tblHoiVienCaNhanQuaTrinhCongTac WHERE HoiVienCaNhanID = " + lbHoiVienCaNhanID.Text + ";";
        cmd.CommandText += "DELETE tblHoiVienFile WHERE HoiVienCaNhanID = " + lbHoiVienCaNhanID.Text + ";";

        DataAccess.RunActionCmd(cmd);

        // insert chứng chỉ        
        for (int i = 0; i < gvChungChi.Rows.Count; i++)
        {

            TextBox sochungchi = (TextBox)gvChungChi.Rows[i].Cells[1].FindControl("txtSoChungChi");
            TextBox ngaycap = (TextBox)gvChungChi.Rows[i].Cells[2].FindControl("txtDate");

            if (!string.IsNullOrEmpty(sochungchi.Text))
            {
                HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                chungchi.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;

                chungchi.SoChungChi = sochungchi.Text;
                if (!string.IsNullOrEmpty(ngaycap.Text))
                    chungchi.NgayCap = DateTime.ParseExact(ngaycap.Text, "dd/MM/yyyy", null);

                HoiVienCaNhanChungChi_provider.Insert(chungchi);
            }
        }

        // insert quá trình làm việc

        for (int i = 0; i < gvQuaTrinh.Rows.Count; i++)
        {
            TextBox thoigian = (TextBox)gvQuaTrinh.Rows[i].Cells[1].FindControl("txtThoiGian");
            TextBox chucvu = (TextBox)gvQuaTrinh.Rows[i].Cells[3].FindControl("txtChucVu");
            TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[i].Cells[4].FindControl("txtNoiLamViec");

            HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
            quatrinh.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
            quatrinh.ThoiGianCongTac = thoigian.Text;
            quatrinh.ChucVu = chucvu.Text;
            quatrinh.NoiLamViec = noilamviec.Text;

            HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
        }

        // file đính kèm
        PheDuyetFile(hoivien.HoiVienCaNhanId);


        // update vào bảng tblNguoiDung
        cmd = new SqlCommand();
        cmd.CommandText = "SELECT NguoiDungID FROM tblNguoiDung WHERE HoiVienId = " + lbHoiVienCaNhanID.Text;
        string NguoiDungID = DataAccess.DLookup(cmd);

        SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connStr, true, "");

        NguoiDung new_user = NguoiDung_provider.GetByNguoiDungId(int.Parse(NguoiDungID));
        new_user.HoVaTen = txtHoDem.Text + " " + txtTen.Text;
        if (!string.IsNullOrEmpty(NgaySinh.Value))
            new_user.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);

        new_user.GioiTinh = gioitinh;
        new_user.Email = txtEmail.Text;
        new_user.DienThoai = txtDienThoai.Text;
        new_user.SoCmnd = txtSoCMND.Text;
        new_user.CauHoiBiMatId = donhoivien.CauHoiBiMatId;
        new_user.CauTraLoiBiMat = donhoivien.CauTraLoiBiMat;
        new_user.LoaiHoiVien = "1";

        NguoiDung_provider.Update(new_user);

        // insert vào bảng tblTTPhatSinhPhi

        //if (hoivien.HoiVienTapTheId != null)
        //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";
        //else
        //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnKhongLamViecTaiCtkt).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";

        try
        {
            // SonNQ sửa lại, update theo phí chi tiết
            cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";

            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
            if (dtb.Rows.Count != 0)
            {

                cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + hoivien.HoiVienCaNhanId + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                DataAccess.RunActionCmd(cmd);
            }
        }
        catch (Exception ex)
        {

        }

        cmd = new SqlCommand();
        cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 5, HoiVienCaNhanID = " + hoivien.HoiVienCaNhanId.ToString() + ", NguoiDuyet = N'" + cm.Admin_TenDangNhap + "', NgayDuyet = '" + DateTime.Now.ToString("yyyy-MM-dd") + "' WHERE DonHoiVienCaNhanID = " + Request.QueryString["id"];

        DataAccess.RunActionCmd(cmd);

        // Send mail
        string body = "";
        body += "Anh/chị đã được phê duyệt kết nạp hội viên cá nhân, có thể sử dụng tài khoản của anh/chị để đăng nhập vào phần mềm với tư cách là hội viên cá nhân<BR/>";
        cm.GuiThongBao(hoivien.HoiVienCaNhanId.ToString(), "1", "VACPA - Phê duyệt kết nạp hội viên cá nhân tại trang tin điện tử VACPA", body);
        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Phê duyệt hồ sơ thành công.</div>";
        string msg = "";
        if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "VACPA - Phê duyệt kết nạp hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg))
        {
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
        else
            placeMessage.Controls.Add(new LiteralControl(msg));
    }

    // hội viên mới
    protected void PheDuyet_HoiVien()
    {
        // tạo tài khoản nếu chưa có
        string HoiVienID = SinhID(txtHoDem.Text, txtTen.Text, txtSoChungChiKTV.Text);

        // insert vào tblHoiVienCaNhan

        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");

        SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connStr, true, "");
        SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
        SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connStr, true, "");

        string gioitinh = (rdNam.Checked) ? "1" : "0";

        HoiVienCaNhan hoivien = new HoiVienCaNhan();
        if (drLoaiHoiVien.SelectedValue != "-1" && drLoaiHoiVien.SelectedValue != "-2")
            hoivien.LoaiHoiVienCaNhanChiTiet = int.Parse(drLoaiHoiVien.SelectedValue);
        else
            hoivien.LoaiHoiVienCaNhanChiTiet = 0;

        hoivien.MaHoiVienCaNhan = HoiVienID;
        hoivien.LoaiHoiVienCaNhan = "1";
        hoivien.HoDem = txtHoDem.Text;
        hoivien.Ten = txtTen.Text;
        hoivien.GioiTinh = gioitinh;
        if (!string.IsNullOrEmpty(NgaySinh.Value))
            hoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
        hoivien.QuocTichId = int.Parse(drQuocTich.SelectedValue);

        if (!string.IsNullOrEmpty(Request.Form["TinhID_QueQuan"]))
            hoivien.QueQuanTinhId = Request.Form["TinhID_QueQuan"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["HuyenID_QueQuan"]))
            hoivien.QueQuanHuyenId = Request.Form["HuyenID_QueQuan"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["XaID_QueQuan"]))
            hoivien.QueQuanXaId = Request.Form["XaID_QueQuan"].ToString();

        hoivien.DiaChi = txtDiaChi.Text;
        if (!string.IsNullOrEmpty(Request.Form["TinhID_DiaChi"]))
            hoivien.DiaChiTinhId = Request.Form["TinhID_DiaChi"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["HuyenID_DiaChi"]))
            hoivien.DiaChiHuyenId = Request.Form["HuyenID_DiaChi"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["XaID_DiaChi"]))
            hoivien.DiaChiXaId = Request.Form["XaID_DiaChi"].ToString();

        hoivien.SoGiayChungNhanDkhn = txtSoGiayDHKN.Text;
        if (!string.IsNullOrEmpty(Request.Form["NgayCap_DHKN"]))
            hoivien.NgayCapGiayChungNhanDkhn = DateTime.ParseExact(Request.Form["NgayCap_DHKN"], "dd/MM/yyyy", null);
        if (!string.IsNullOrEmpty(Request.Form["HanCapTu"]))
            hoivien.HanCapTu = DateTime.ParseExact(Request.Form["HanCapTu"], "dd/MM/yyyy", null);
        if (!string.IsNullOrEmpty(Request.Form["HanCapDen"]))
            hoivien.HanCapDen = DateTime.ParseExact(Request.Form["HanCapDen"], "dd/MM/yyyy", null);

        hoivien.SoChungChiKtv = txtSoChungChiKTV.Text;
        hoivien.NgayCapChungChiKtv = DateTime.ParseExact(NgayCap_ChungChiKTV.Value, "dd/MM/yyyy", null);

        hoivien.Email = txtEmail.Text;
        hoivien.DienThoai = txtDienThoai.Text;
        hoivien.Mobile = txtMobile.Text;
        if (drChucVu.SelectedValue != "0")
            hoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
        if (drTotNghiep.SelectedValue != "0")
            hoivien.TruongDaiHocId = int.Parse(drTotNghiep.SelectedValue);
        if (drChuyenNganh.SelectedValue != "0")
            hoivien.ChuyenNganhDaoTaoId = int.Parse(drChuyenNganh.SelectedValue);
        hoivien.ChuyenNganhNam = txtNam_ChuyenNganh.Text;
        if (drHocVi.SelectedValue != "0")
            hoivien.HocViId = int.Parse(drHocVi.SelectedValue);
        hoivien.HocViNam = txtNam_HocVi.Text;
        if (drHocHam.SelectedValue != "0")
            hoivien.HocHamId = int.Parse(drHocHam.SelectedValue);
        hoivien.HocHamNam = txtNam_HocHam.Text;
        hoivien.SoCmnd = txtSoCMND.Text;
        if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
            hoivien.CmndNgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);
        hoivien.CmndTinhId = int.Parse(drNoiCap_CMND.SelectedValue);
        hoivien.SoTaiKhoanNganHang = txtSoTKNganHang.Text;
        if (drNganHang.SelectedValue != "0")
            hoivien.NganHangId = int.Parse(drNganHang.SelectedValue);
        if (drDonVi.SelectedValue != "0")
            hoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
        else
            hoivien.DonViCongTac = txtDonViCongTac.Text;
        if (drSoThich.SelectedValue != "0")
            hoivien.SoThichId = int.Parse(drSoThich.SelectedValue);

        hoivien.NgayGiaNhap = DateTime.Now;
        hoivien.NgayDuyet = DateTime.Now;
        hoivien.NguoiCapNhat = cm.Admin_HoVaTen;
        hoivien.NguoiDuyet = cm.Admin_HoVaTen;
        hoivien.TinhTrangHoiVienId = 1;
        hoivien.SoQuyetDinhKetNap = txtSoQuyetDinh.Text;
        if (!string.IsNullOrEmpty(NgayQD.Value))
            hoivien.NgayQuyetDinhKetNap = DateTime.ParseExact(NgayQD.Value, "dd/MM/yyyy", null);
        HoiVienCaNhan_provider.Insert(hoivien);

        // insert chứng chỉ

        for (int i = 0; i < gvChungChi.Rows.Count; i++)
        {

            TextBox sochungchi = (TextBox)gvChungChi.Rows[i].Cells[1].FindControl("txtSoChungChi");
            TextBox ngaycap = (TextBox)gvChungChi.Rows[i].Cells[2].FindControl("txtDate");

            if (!string.IsNullOrEmpty(sochungchi.Text))
            {
                HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                chungchi.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;

                chungchi.SoChungChi = sochungchi.Text;
                if (!string.IsNullOrEmpty(ngaycap.Text))
                    chungchi.NgayCap = DateTime.ParseExact(ngaycap.Text, "dd/MM/yyyy", null);

                HoiVienCaNhanChungChi_provider.Insert(chungchi);
            }
        }

        // insert quá trình làm việc

        for (int i = 0; i < gvQuaTrinh.Rows.Count; i++)
        {
            TextBox thoigian = (TextBox)gvQuaTrinh.Rows[i].Cells[1].FindControl("txtThoiGian");
            TextBox chucvu = (TextBox)gvQuaTrinh.Rows[i].Cells[3].FindControl("txtChucVu");
            TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[i].Cells[4].FindControl("txtNoiLamViec");

            HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
            quatrinh.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
            quatrinh.ThoiGianCongTac = thoigian.Text;
            quatrinh.ChucVu = chucvu.Text;
            quatrinh.NoiLamViec = noilamviec.Text;

            HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
        }

        // file đính kèm
        PheDuyetFile(hoivien.HoiVienCaNhanId);

        SqlDonHoiVienCaNhanProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connStr, true, "");

        DonHoiVienCaNhan donhoivien = DonHoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));
        // insert vào bảng tblNguoiDung
        SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connStr, true, "");

        NguoiDung new_user = new NguoiDung();
        new_user.HoVaTen = txtHoDem.Text + " " + txtTen.Text;
        if (!string.IsNullOrEmpty(NgaySinh.Value))
            new_user.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);

        new_user.GioiTinh = gioitinh;
        new_user.Email = txtEmail.Text;
        new_user.DienThoai = txtDienThoai.Text;
        new_user.SoCmnd = txtSoCMND.Text;
        new_user.TenDangNhap = HoiVienID;

        Random random = new Random();
        int randomPassword = random.Next(10000000, 99999999);

        new_user.MatKhau = randomPassword.ToString();
        new_user.LoaiHoiVien = "1";
        new_user.HoiVienId = hoivien.HoiVienCaNhanId;
        new_user.CauHoiBiMatId = donhoivien.CauHoiBiMatId;
        new_user.CauTraLoiBiMat = donhoivien.CauTraLoiBiMat;

        NguoiDung_provider.Insert(new_user);

        // insert vào bảng tblTTPhatSinhPhi
        SqlCommand cmd = new SqlCommand();
        //if (hoivien.HoiVienTapTheId != null)
        //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";
        //else
        //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnKhongLamViecTaiCtkt).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";

        try
        {
            // SonNQ sửa lại, update theo phí chi tiết
            cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";

            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
            if (dtb.Rows.Count != 0)
            {

                cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + hoivien.HoiVienCaNhanId + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                DataAccess.RunActionCmd(cmd);
            }
        }
        catch (Exception ex)
        {

        }

        cmd = new SqlCommand();
        cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 5, HoiVienCaNhanID = " + hoivien.HoiVienCaNhanId.ToString() + ", NguoiDuyet = N'" + cm.Admin_TenDangNhap + "', NgayDuyet = '" + DateTime.Now.ToString("yyyy-MM-dd") + "' WHERE DonHoiVienCaNhanID = " + Request.QueryString["id"];

        DataAccess.RunActionCmd(cmd);

        // Send mail
        string body = "";
        body += "Thân chào Anh/chị <span style=\"color:red\">" + new_user.HoVaTen + "</span><BR/>";
        body += "Ban quản trị xin thông báo tài khoản đăng nhập phần mềm quản lý Hội viên VACPA như sau:<BR/>";
        body += "------ID đăng nhập------<BR/>";
        body += "Tên đăng nhập: " + HoiVienID + "<BR/>";
        body += "Mật khẩu: " + randomPassword + "<BR/>";
        body += "<span style=\"text-decoration:underline;font-weight:bold\">Lưu ý:</span><BR/>";
        body += " - Link đăng nhập phần mềm quản lý hội viên: http://vacpa.org.vn/Page/Login.aspx" + "<BR/>";
        body += " - Link xin cấp lại mật khẩu: http://vacpa.org.vn/Page/GetPassword.aspx" + "<BR/>";
        body += " - Ngay sau khi đăng nhập Anh/Chị hãy đổi mật khẩu truy cập." + "<BR/>";
        body += " - Để đảm bảo được nhận các thông báo thường xuyên với VACPA, Anh/Chị nhớ cập nhật những thông tin về sự thay đổi công việc, đơn vị công tác, số điện thoại di động, email, nơi thường trú, v.v… qua profile cá nhân." + "<BR/>";

        body += "━━━━━━━━━━━━━━━<BR/>";
        body += "Trân trọng,<BR/>";
        body += "Hội kiểm toán viên hành nghề Việt Nam (VACPA)<BR/>";
        body += "Địa chỉ: Phòng 304, Nhà Dự án, Số 4, Ngõ Hàng Chuối I, Hà Nội<BR/>";
        body += "Email: quantriweb@vacpa.org.vn";
        cm.GuiThongBao(hoivien.HoiVienCaNhanId.ToString(), "1", "VACPA - Thông tin tài khoản hội viên cá nhân tại trang tin điện tử VACPA", body);
        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Phê duyệt hồ sơ thành công.</div>";
        string msg = "";
        if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "VACPA - Thông tin tài khoản hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg))
        {
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
        else
            placeMessage.Controls.Add(new LiteralControl(msg));
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    protected void btnKetXuatWord_Click(object sender, EventArgs e)
    {
        string strid = txtSoDon.Text;
        //strid = abcID123.Text;
        //strid = 
        //if (Request.Form["abcID123"] != null)
        //    strid = Request.Form["abcID123"].ToString();
        if (!string.IsNullOrEmpty(strid))
        {

            List<objDonXinGiaNhapHoiVienCaNhan> lst = new List<objDonXinGiaNhapHoiVienCaNhan>();
            List<lstOBJQuaTrinhLamViec> lstQT = new List<lstOBJQuaTrinhLamViec>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoDem + ' ' + Ten as HoTen, NgaySinh, GioiTinh, a.DiaChi + ' - ' + dbo.LayDiaChi_HVCN(a.DonHoiVienCaNhanId) AS DiaChi, dbo.LayQueQuan(a.DonHoiVienCaNhanId) As QueQuan, "
                                + " b.TenTruongDaiHoc, e.TenChuyenNganhDaoTao, a.ChuyenNganhNam, c.TenHocVi, "
                                + " HocViNam, d.TenHocHam, HocHamNam, SoCMND, CMND_NgayCap, f.TenTinh as noiCapCMT, "
                                + " SoTaiKhoanNganHang, TenNganHang, SoChungChiKTV, NgayCapChungChiKTV, "
                                + " SoGiayChungNhanDKHN, convert(varchar, NgayCapGiayChungNhanDKHN, 103) as NgayCapGiayChungNhanDKHN, HanCapTu, HanCapDen, "
                                + " h.TenChucVu, CASE ISNULL(a.DonViCongTac,'') WHEN '' THEN k.TenDoanhNghiep ELSE a.DonViCongTac END AS DonViCongTac, a.DienThoai, Mobile, a.Email, i.TenSoThich, DonHoiVienCaNhanID, NgayNopDon "
                                + " from dbo.tblDonHoiVienCaNhan a "
                                + " left join dbo.tblHoiVienTapThe k on a.HoiVienTapTheID = k.HoiVienTapTheID "
                                + " left join dbo.tblDMTruongDaiHoc b on a.TruongDaiHocID = b.TruongDaiHocID "
                                + " left join dbo.tblDMHocVi c on a.HocViID = c.HocViID "
                                + " left join dbo.tblDMHocHam d on a.HocHamID = d.HocHamID "
                                + " left join dbo.tblDMChuyenNganhDaoTao e on a.ChuyenNganhDaoTaoID = e.ChuyenNganhDaoTaoID "
                                + " left join dbo.tblDMTinh f on a.CMND_TinhID = f.TinhID "
                                + " left join tblDMNganHang g on a.NganHangID = g.NganHangID "
                                + " left join dbo.tblDMChucVu h on a.ChucVuID = h.ChucVuID "
                                + " left join dbo.tblDMSoThich i on a.SoThichID = i.SoThichID "
                                + " WHERE SoDonHoiVienCaNhan = '" + strid + "'";
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            // int i = 1;
            foreach (DataRow tem in dt.Rows)
            {
                objDonXinGiaNhapHoiVienCaNhan new1 = new objDonXinGiaNhapHoiVienCaNhan();
                new1.HoTen = tem["HoTen"].ToString();
                new1.NgaySinh = !string.IsNullOrEmpty(tem["NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgaySinh"])) : "";
                if (tem["GioiTinh"].ToString() == "1")
                {
                    new1.GioiTinh_Nu = ".";
                    new1.GioiTinh_Nam = "X";
                }
                else
                {
                    new1.GioiTinh_Nam = ".";
                    new1.GioiTinh_Nu = "X";
                }
                new1.ChuyenNganh = tem["TenChuyenNganhDaoTao"].ToString();
                new1.Nam_Bang = tem["ChuyenNganhNam"].ToString();
                new1.QueQuan = tem["QueQuan"].ToString();
                new1.DiaChi = tem["DiaChi"].ToString();
                new1.BangCap = tem["TenTruongDaiHoc"].ToString();
                new1.HocHam = tem["TenHocHam"].ToString();
                new1.HocVi = tem["TenHocVi"].ToString();
                new1.Nam_HocHam = tem["HocHamNam"].ToString();
                new1.Nam_HocVi = tem["HocViNam"].ToString();
                new1.CMT = tem["SoCMND"].ToString();
                new1.NgayCap = !string.IsNullOrEmpty(tem["CMND_NgayCap"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["CMND_NgayCap"])) : "";
                new1.NoiCap = tem["noiCapCMT"].ToString();
                new1.SoTK = tem["SoTaiKhoanNganHang"].ToString();
                new1.TaiNganHang = tem["TenNganHang"].ToString();
                new1.SoChungChi = tem["SoChungChiKTV"].ToString();
                new1.NgayCap_CT = !string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCapChungChiKTV"])) : "";
                new1.GiayCN = tem["SoGiayChungNhanDKHN"].ToString();
                new1.NgayCap_GiayCN = tem["NgayCapGiayChungNhanDKHN"].ToString();
                new1.GiayDangKy = !string.IsNullOrEmpty(tem["HanCapTu"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapTu"])) : "";
                new1.NoiCapGiayDangKy = !string.IsNullOrEmpty(tem["HanCapDen"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapDen"])) : "";

                new1.ChucVu = tem["TenChucVu"].ToString();
                new1.DonViCongTac = tem["DonViCongTac"].ToString();
                new1.Email = tem["Email"].ToString();
                new1.DienThoai = tem["DienThoai"].ToString();
                new1.DD = tem["Mobile"].ToString();
                new1.SoThich = tem["TenSoThich"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayNopDon"].ToString()))
                    new1.NgayThangNam = "Ngày " + Convert.ToDateTime(tem["NgayNopDon"]).Day + " tháng " + Convert.ToDateTime(tem["NgayNopDon"]).Month + " năm " + Convert.ToDateTime(tem["NgayNopDon"]).Year;
                else
                    new1.NgayThangNam = "Ngày.....tháng.....năm.....";
                lst.Add(new1);

                DataSet ds1 = new DataSet();
                SqlCommand sql1 = new SqlCommand();
                sql1.CommandText = "select ThoiGianCongTac, ChucVu, NoiLamViec from dbo.tblDonHoiVienCaNhanQuaTrinhCongTac "
                                        + " where DonHoiVienCaNhanID =  " + Convert.ToInt32(tem["DonHoiVienCaNhanID"]);


                ds1 = DataAccess.RunCMDGetDataSet(sql1);
                sql1.Connection.Close();
                sql1.Connection.Dispose();
                sql1 = null;

                DataTable dt1 = ds1.Tables[0];

                //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
                //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
                // int i = 1;
                foreach (DataRow tem1 in dt1.Rows)
                {
                    lstOBJQuaTrinhLamViec obj123 = new lstOBJQuaTrinhLamViec();
                    obj123.ThoiGian = tem1["ThoiGianCongTac"].ToString();
                    obj123.ChucVu = tem1["ChucVu"].ToString();
                    obj123.DonViCongTac = tem1["NoiLamViec"].ToString();

                    lstQT.Add(obj123);
                }
            }



            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/DonXinGiaNhapHoiVienCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objDonXinGiaNhapHoiVienCaNhan"].SetDataSource(lst);
            rpt.Database.Tables["lstOBJQuaTrinhLamViec"].SetDataSource(lstQT);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);


            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "objDonXinGiaNhapHoiVienCaNhan");

        }
    }

    protected void btnKetXuatPDF_Click(object sender, EventArgs e)
    {
        string strid = txtSoDon.Text;
        //strid = abcID123.Text;
        //strid = 
        //if (Request.Form["abcID123"] != null)
        //    strid = Request.Form["abcID123"].ToString();
        if (!string.IsNullOrEmpty(strid))
        {

            List<objDonXinGiaNhapHoiVienCaNhan> lst = new List<objDonXinGiaNhapHoiVienCaNhan>();
            List<lstOBJQuaTrinhLamViec> lstQT = new List<lstOBJQuaTrinhLamViec>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoDem + ' ' + Ten as HoTen, NgaySinh, GioiTinh, a.DiaChi + ' - ' + dbo.LayDiaChi_HVCN(a.DonHoiVienCaNhanId) AS DiaChi, dbo.LayQueQuan(a.DonHoiVienCaNhanId) As QueQuan, "
                                + " b.TenTruongDaiHoc, e.TenChuyenNganhDaoTao, a.ChuyenNganhNam, c.TenHocVi, "
                                + " HocViNam, d.TenHocHam, HocHamNam, SoCMND, CMND_NgayCap, f.TenTinh as noiCapCMT, "
                                + " SoTaiKhoanNganHang, TenNganHang, SoChungChiKTV, NgayCapChungChiKTV, "
                                + " SoGiayChungNhanDKHN, convert(varchar, NgayCapGiayChungNhanDKHN, 103) as NgayCapGiayChungNhanDKHN, HanCapTu, HanCapDen, "
                                + " h.TenChucVu, CASE ISNULL(a.DonViCongTac,'') WHEN '' THEN k.TenDoanhNghiep ELSE a.DonViCongTac END AS DonViCongTac, a.DienThoai, Mobile, a.Email, i.TenSoThich, DonHoiVienCaNhanID, NgayNopDon "
                                + " from dbo.tblDonHoiVienCaNhan a "
                                + " left join dbo.tblHoiVienTapThe k on a.HoiVienTapTheID = k.HoiVienTapTheID "
                                + " left join dbo.tblDMTruongDaiHoc b on a.TruongDaiHocID = b.TruongDaiHocID "
                                + " left join dbo.tblDMHocVi c on a.HocViID = c.HocViID "
                                + " left join dbo.tblDMHocHam d on a.HocHamID = d.HocHamID "
                                + " left join dbo.tblDMChuyenNganhDaoTao e on a.ChuyenNganhDaoTaoID = e.ChuyenNganhDaoTaoID "
                                + " left join dbo.tblDMTinh f on a.CMND_TinhID = f.TinhID "
                                + " left join tblDMNganHang g on a.NganHangID = g.NganHangID "
                                + " left join dbo.tblDMChucVu h on a.ChucVuID = h.ChucVuID "
                                + " left join dbo.tblDMSoThich i on a.SoThichID = i.SoThichID "
                                + " WHERE SoDonHoiVienCaNhan = '" + strid + "'";
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            // int i = 1;
            foreach (DataRow tem in dt.Rows)
            {
                objDonXinGiaNhapHoiVienCaNhan new1 = new objDonXinGiaNhapHoiVienCaNhan();
                new1.HoTen = tem["HoTen"].ToString();
                new1.NgaySinh = !string.IsNullOrEmpty(tem["NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgaySinh"])) : "";
                if (tem["GioiTinh"].ToString() == "1")
                {
                    new1.GioiTinh_Nu = ".";
                    new1.GioiTinh_Nam = "X";
                }
                else
                {
                    new1.GioiTinh_Nam = ".";
                    new1.GioiTinh_Nu = "X";
                }
                new1.ChuyenNganh = tem["TenChuyenNganhDaoTao"].ToString();
                new1.Nam_Bang = tem["ChuyenNganhNam"].ToString();
                new1.QueQuan = tem["QueQuan"].ToString();
                new1.DiaChi = tem["DiaChi"].ToString();
                new1.BangCap = tem["TenTruongDaiHoc"].ToString();
                new1.HocHam = tem["TenHocHam"].ToString();
                new1.HocVi = tem["TenHocVi"].ToString();
                new1.Nam_HocHam = tem["HocHamNam"].ToString();
                new1.Nam_HocVi = tem["HocViNam"].ToString();
                new1.CMT = tem["SoCMND"].ToString();
                new1.NgayCap = !string.IsNullOrEmpty(tem["CMND_NgayCap"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["CMND_NgayCap"])) : "";
                new1.NoiCap = tem["noiCapCMT"].ToString();
                new1.SoTK = tem["SoTaiKhoanNganHang"].ToString();
                new1.TaiNganHang = tem["TenNganHang"].ToString();
                new1.SoChungChi = tem["SoChungChiKTV"].ToString();
                new1.NgayCap_CT = !string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCapChungChiKTV"])) : "";
                new1.GiayCN = tem["SoGiayChungNhanDKHN"].ToString();
                new1.NgayCap_GiayCN = tem["NgayCapGiayChungNhanDKHN"].ToString();
                new1.GiayDangKy = !string.IsNullOrEmpty(tem["HanCapTu"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapTu"])) : "";
                new1.NoiCapGiayDangKy = !string.IsNullOrEmpty(tem["HanCapDen"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapDen"])) : "";

                new1.ChucVu = tem["TenChucVu"].ToString();
                new1.DonViCongTac = tem["DonViCongTac"].ToString();
                new1.Email = tem["Email"].ToString();
                new1.DienThoai = tem["DienThoai"].ToString();
                new1.DD = tem["Mobile"].ToString();
                new1.SoThich = tem["TenSoThich"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayNopDon"].ToString()))
                    new1.NgayThangNam = "Ngày " + Convert.ToDateTime(tem["NgayNopDon"]).Day + " tháng " + Convert.ToDateTime(tem["NgayNopDon"]).Month + " năm " + Convert.ToDateTime(tem["NgayNopDon"]).Year;
                else
                    new1.NgayThangNam = "Ngày.....tháng.....năm.....";
                lst.Add(new1);

                DataSet ds1 = new DataSet();
                SqlCommand sql1 = new SqlCommand();
                sql1.CommandText = "select ThoiGianCongTac, ChucVu, NoiLamViec from dbo.tblDonHoiVienCaNhanQuaTrinhCongTac "
                                        + " where DonHoiVienCaNhanID =  " + Convert.ToInt32(tem["DonHoiVienCaNhanID"]);


                ds1 = DataAccess.RunCMDGetDataSet(sql1);
                sql1.Connection.Close();
                sql1.Connection.Dispose();
                sql1 = null;

                DataTable dt1 = ds1.Tables[0];

                //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
                //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
                // int i = 1;
                foreach (DataRow tem1 in dt1.Rows)
                {
                    lstOBJQuaTrinhLamViec obj123 = new lstOBJQuaTrinhLamViec();
                    obj123.ThoiGian = tem1["ThoiGianCongTac"].ToString();
                    obj123.ChucVu = tem1["ChucVu"].ToString();
                    obj123.DonViCongTac = tem1["NoiLamViec"].ToString();

                    lstQT.Add(obj123);
                }
            }



            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/DonXinGiaNhapHoiVienCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objDonXinGiaNhapHoiVienCaNhan"].SetDataSource(lst);
            rpt.Database.Tables["lstOBJQuaTrinhLamViec"].SetDataSource(lstQT);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "objDonXinGiaNhapHoiVienCaNhan");
        }
    }

    protected void btnKetXuat_Click(object sender, EventArgs e)
    {
        setdataCrystalReport();
    }

    public void setdataCrystalReport()
    {
        List<objGiayChungNhanHoiVienChinhThuc> lst = new List<objGiayChungNhanHoiVienChinhThuc>();


        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();

        //Lấy hoi vien ca nhan
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            sql.CommandText = @"select GioiTinh, (HoDem + ' ' + Ten) as HoVaTen, 
SoChungChiKTV, NgayCapChungChiKTV, SoQuyetDinhKetNap, NgayQuyetDinhKetNap
from tblHoiVienCaNhan where hoiviencanhanid = (select hoiviencanhanid from tbldonhoiviencanhan where donhoiviencanhanid = " + Request.QueryString["id"] + ")";
            ds = DataAccess.RunCMDGetDataSet(sql);

            DataTable dt = ds.Tables[0];

            //objGiayChungNhanHoiVienChinhThuc obj = new objGiayChungNhanHoiVienChinhThuc();
            //obj.HoVaTen = "¤ng NguyÔn Quèc ¢n";
            //obj.SoCCKTV = "Chøng chØ KTV sè 2772/KTV do Bé Tµi chÝnh";
            //obj.NgayCap = "cÊp ngµy 15/4/2014 Lµ Héi viªn chÝnh thøc cña VACPA";
            //obj.HoVaTenTiengAnh = "Mr. Nguyen Quoc An";
            //obj.SoCCKTVTiengAnh = "Auditor Certificate No.2772/KTV dated April 15, 2014 by";
            //obj.NgayCapTiengAnh = "Ministry of Finance is a full Member of VACPA";
            //obj.NgayThangNamRaQuyetDinh = "Hµ Néi, 10 th¸ng 7 n¨m 2015";
            //obj.SoQuyetDinh = "Sè: 245-2015/Q§-VACPA";
            //lst.Add(obj);
            foreach (DataRow dtr in dt.Rows)
            {
                objGiayChungNhanHoiVienChinhThuc obj = new objGiayChungNhanHoiVienChinhThuc();
                if (dtr["GioiTinh"].ToString() == "1" || dtr["GioiTinh"].ToString() == "1")
                {
                    obj.NgayCapTiengAnh = "¤ng";
                    obj.HoVaTenTiengAnh = "Mr.";
                }
                else
                {
                    obj.NgayCapTiengAnh = "Bµ";
                    obj.HoVaTenTiengAnh = "Ms.";
                }
                obj.HoVaTen += " " + Converter.ConvertToTCVN3(dtr["HoVaTen"].ToString());

                obj.SoCCKTV = "Chøng chØ KTV sè " + Converter.ConvertToTCVN3(dtr["SoChungChiKTV"].ToString()) + " do Bé Tµi chÝnh";
                try
                {
                    obj.NgayCap = "cÊp ngµy " + Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).ToShortDateString() + " Lµ Héi viªn chÝnh thøc cña VACPA";
                }
                catch (Exception)
                {
                    obj.NgayCap = "cÊp ngµy " + " Lµ Héi viªn chÝnh thøc cña VACPA";
                }
                obj.HoVaTenTiengAnh += " " + RemoveSign4VietnameseString(dtr["HoVaTen"].ToString());
                obj.SoCCKTVTiengAnh = "Auditor Certificate No. " + RemoveSign4VietnameseString(dtr["SoChungChiKTV"].ToString()) + " dated " +
                    DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Month) + " " +
                    Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Day + ", " +
                    Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Year +
                    " by";
                obj.SoCCKTVTiengAnh += "\r\nMinistry of Finance is full Member of VACPA";
                try
                {
                    obj.NgayThangNamRaQuyetDinh = "Hµ Néi, ngµy " + Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Day + " th¸ng " +
                   Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Month + " n¨m " + Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Year;
                }
                catch (Exception)
                {
                    obj.NgayThangNamRaQuyetDinh = "Hµ Néi, ngµy " + " th¸ng " +
                                                  " n¨m ";
                }
                try
                {
                    obj.SoQuyetDinh = "Sè: " + dtr["SoQuyetDinhKetNap"].ToString();
                }
                catch (Exception)
                {
                    obj.SoQuyetDinh = "Sè: ";
                }

                lst.Add(obj);
            }

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/GiayChungNhanHoiVienCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objGiayChungNhanHoiVienChinhThuc"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            //CrystalReportViewer1.ReportSource = rpt;
            //CrystalReportViewer1.DataBind();
            //CrystalReportViewer1.SeparatePages = true;
            //if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true,
                                     "GiayChungNhanHoiVienCaNhan");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //{
            //    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true,
            //                             "GiayChungNhanHoiVienCaNhan");
            //}
        }
    }

    public static string RemoveSign4VietnameseString(string str)
    {
        if (str == null)
            return "";

        if (str.Trim().Length == 0)
            return "";

        //Tiến hành thay thế , lọc bỏ dấu cho chuỗi
        for (int i = 1; i < VietnameseSigns.Length; i++)
        {
            for (int j = 0; j < VietnameseSigns[i].Length; j++)

                str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);

        }

        str = str.Replace("  ", " ");

        return str.Trim();
    }

    private static readonly string[] VietnameseSigns = new string[]

    {

        "aAeEoOuUiIdDyY",

        "áàạảãâấầậẩẫăắằặẳẵ",

        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

        "éèẹẻẽêếềệểễ",

        "ÉÈẸẺẼÊẾỀỆỂỄ",

        "óòọỏõôốồộổỗơớờợởỡ",

        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

        "úùụủũưứừựửữ",

        "ÚÙỤỦŨƯỨỪỰỬỮ",

        "íìịỉĩ",

        "ÍÌỊỈĨ",

        "đ",

        "Đ",

        "ýỳỵỷỹ",

        "ÝỲỴỶỸ"

    };

    protected void btnInQuyetDinh_Click(object sender, EventArgs e)
    {
        setdataCrystalReport_QD();
    }

    public void setdataCrystalReport_QD()
    {

        DateTime? NgayQuyetDinh = null;
        //if (!string.IsNullOrEmpty(Request.Form["NgayQuyetDinh"]))
        //    NgayQuyetDinh = DateTime.ParseExact(Request.Form["NgayQuyetDinh"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

        string result = "";
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaHoiVienCaNhan FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = (SELECT HoiVienCaNhanID FROM tblDonHoiVienCaNhan WHERE DonHoiVienCaNhanID = " + Request.QueryString["id"] + ")";
        result = DataAccess.DLookup(sql);
        if (!string.IsNullOrEmpty(result))
        {
            List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
            List<DanhSachKetNapHoiVienCaNhan> lst = new List<DanhSachKetNapHoiVienCaNhan>();


            DataSet ds = new DataSet();
            sql = new SqlCommand();
            sql.CommandText = "select a.SoQuyetDinhKetNap, HoDem + ' ' + Ten as HoTen, NgaySinh, GioiTinh, DonViCongTac, "
                             + " SoChungChiKTV, NgayCapChungChiKTV, a.NgayQuyetDinhKetNap "
                             + " from dbo.tblHoiVienCaNhan a  "
                             + " left join dbo.tblHoiVienTapThe b on a.HoiVienTapTheID = b.HoiVienTapTheID "
                             + " where a.mahoiviencanhan = '" + result + "'";

            //if (!string.IsNullOrEmpty(DonViId))
            //    sql.CommandText = sql.CommandText + " AND b.MaHoiVienTapThe = '" + DonViId + "'";
            //  string strNgay = NgayQuyetDinh.Value.Year + "-" + NgayQuyetDinh.Value.Month + "-" + NgayQuyetDinh.Value.Day;
            //if (NgayQuyetDinh != null)
            //    sql.Parameters.AddWithValue("@NgayQuyetDinh", strNgay);
            //else
            //    sql.Parameters.AddWithValue("@NgayQuyetDinh", DBNull.Value);
            //if (NgayKetThuc != null)
            //    sql.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);
            //else
            //    sql.Parameters.AddWithValue("@NgayKetThuc", DBNull.Value);

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            int i = 0;
            string SoQD = "";
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                string gt = "Ông ";
                string NamSinh = "";
                if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                {
                    string strNS = NgayThangVN(Convert.ToDateTime(tem["NgaySinh"]));
                    NamSinh = Convert.ToDateTime(tem["NgaySinh"]).Year.ToString();
                    if (tem["GioiTinh"].ToString() == "0")
                        gt = "Bà ";
                }
                DanhSachKetNapHoiVienCaNhan new1 = new DanhSachKetNapHoiVienCaNhan();
                new1.STT = i.ToString();
                new1.HoTen = gt + tem["HoTen"].ToString();
                new1.MaSo = gt + tem["HoTen"].ToString().Trim() + ";";
                new1.NgaySinh_Nam = ", sinh năm " + NamSinh;
                new1.So_CCKTV = tem["SoChungChiKTV"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()))
                    new1.Ngay_CCKTV = NgayThangVN(Convert.ToDateTime(tem["NgayCapChungChiKTV"]));
                new1.DonViCongTac = tem["DonViCongTac"].ToString();
                SoQD = tem["SoQuyetDinhKetNap"].ToString();
                lst.Add(new1);

                if (!string.IsNullOrEmpty(tem["NgayQuyetDinhKetNap"].ToString()))
                    NgayQuyetDinh = Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]);
            }

            TenBaoCao newabc = new TenBaoCao();
            newabc.SoLuong = i.ToString();
            newabc.TenBC = SoQD;
            newabc.TongCong = "Tổng cộng: " + i.ToString() + " Hội viên cá nhân";
            try
            {
                newabc.ngayBC = NgayThangVN(NgayQuyetDinh.Value);
                newabc.NgayTinh = "Hà Nội, ngày " + (NgayQuyetDinh.Value.Day.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Day : NgayQuyetDinh.Value.Day.ToString()) + " tháng " + (NgayQuyetDinh.Value.Month.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Month : NgayQuyetDinh.Value.Month.ToString()) + " năm " + NgayQuyetDinh.Value.Year;
            }
            catch { }
            lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/QuyetDinhKetNapHoiVienCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["DanhSachKetNapHoiVienCaNhan"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);


            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "QuyetDinhKetNapHoiVienCaNhan");

        }
    }
}