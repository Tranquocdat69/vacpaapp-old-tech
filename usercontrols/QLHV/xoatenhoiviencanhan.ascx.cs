﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using HiPT.VACPA.DL;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Configuration;

using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;

public partial class usercontrols_xoatenhoiviencanhan : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();

    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);


    string tuchoitiepnhan = "tuchoitiepnhan";
    string tiepnhan = "tiepnhan";
    string duyetdon = "duyetdon";
    string tuchoiduyet = "tuchoiduyet";
    string thoaiduyet = "thoaiduyet";

    int trangthaiChoTiepNhanID = 1;
    int trangthaiTuChoiID = 2;
    int trangthaiChoDuyetID = 3;
    int trangthaiTraLaiID = 4;
    int trangthaiDaPheDuyet = 5;
    int trangthaiThoaiDuyetID = 6;


    string tk_TTChoTiepNhan = "1";
    string tk_TTTuChoiTiepNhan = "2";
    string tk_TTChoDuyet = "3";
    string tk_TTTuChoiDuyet = "4";
    string tk_TTDaDuyet = "5";
    string tk_TTThoaiDuyet = "6";





    string delete = "delete";
    string canhan = "1";
    string tapthe = "2";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            // Tên chức năng
            string tenchucnang = "Quản lý xóa tên hội viên cá nhân";
            // Icon CSS Class  
            string IconClass = "iconfa-book";

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            // Phân quyền
            if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XEM|"))
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
                Form1.Visible = false;
                return;
            }

            try
            {

                if (Request.QueryString["act"] != "")
                {
                    String resulte = "";
                    //  action 
                    if (!String.IsNullOrEmpty(Request.QueryString["id"]))
                    {
                        if (Request.QueryString["act"] == "xoa")
                        {
                            SqlCommand cmd = new SqlCommand();
                            cmd.CommandText = "DELETE tblXoaTenHoiVien WHERE XoaTenHoiVienID = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(cmd);
                            resulte = "resxoa=1";

                            cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Xóa đơn xin xóa tên hội viên, XoaTenHoiVienID: " + Request.QueryString["id"]);
                        }
                        else
                        {
                            string[] lstObject = Request.QueryString["id"].Split(',');

                            string lstIdCaNhan = "";
                            string lstIdTinhTrangCaNhan = "";
                            int countCaNhan = 0;
                            foreach (string iterm in lstObject)
                            {
                                string[] lstTemp = iterm.Split('|');
                                if (lstTemp.Length == 2)
                                {

                                    if (countCaNhan == 0)
                                    {
                                        lstIdTinhTrangCaNhan = iterm;
                                        string[] lstIterm = lstTemp[0].Split('|');
                                        lstIdCaNhan = lstIterm[0];
                                    }
                                    else
                                    {
                                        lstIdTinhTrangCaNhan = lstIdTinhTrangCaNhan + "," + iterm;
                                        string[] lstIterm = lstTemp[0].Split('|');
                                        lstIdCaNhan = lstIdCaNhan + "," + lstIterm[0];
                                    }

                                    countCaNhan = countCaNhan + 1;

                                }
                            }

                            if (lstObject.Length > 0)
                            {
                                // delete
                                if (Request.QueryString["act"] == "delete")
                                {
                                    //string[] lstObjectId = lstIdCaNhan.Split(',');
                                    SqlXoaTenHoiVienProvider XoaTenHoiVien_provider = new SqlXoaTenHoiVienProvider(cm.connstr, true, "");
                                    XoaTenHoiVien XoaTenHV = new XoaTenHoiVien();
                                    foreach (string HoiVienID in lstObject)
                                    {
                                        XoaTenHV = new XoaTenHoiVien();
                                        XoaTenHV.HoiVienId = Convert.ToInt32(HoiVienID);
                                        XoaTenHV.LoaiHoiVien = "1";// hoi vien ca nhan
                                        XoaTenHV.LyDoXoaTen = Request.QueryString["dieukienxoaten"];
                                        XoaTenHV.GhiChuXoaTen = Request.QueryString["diengiai"];
                                        XoaTenHV.TinhTrangXoaTenId = trangthaiChoDuyetID;
                                        XoaTenHV.NgayNhap = DateTime.Now;
                                        XoaTenHV.NguoiNhap = cm.Admin_HoVaTen;
                                        XoaTenHV.LoaiXoaTen = Request.QueryString["loaixoaten"];
                                        XoaTenHoiVien_provider.Insert(XoaTenHV);

                                        cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Thêm mới đơn xin xóa tên hội viên cá nhân, HoiVienID: " + HoiVienID);
                                    }
                                    resulte = "resdelete=1";

                                    Response.Redirect("admin.aspx?page=xoatenhoiviencanhan&thongbao=1", false);
                                }
                            }

                            if (lstIdTinhTrangCaNhan.Length > 0)
                            {



                                // trinh phe duyet 
                                if (Request.QueryString["act"] == tiepnhan)
                                {
                                    string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');

                                    foreach (string sDonHoiVien in lstDonHoiVien)
                                    {
                                        string[] lstIterm = sDonHoiVien.Split('|');
                                        String sDonHoiVienID = lstIterm[0];
                                        String sDonHoiVienTinhtrang = lstIterm[1];
                                        if (sDonHoiVienTinhtrang.Trim() == tk_TTChoTiepNhan)
                                        {

                                            btnTrinhPheDuyet_Click(sDonHoiVienID);
                                        }
                                    }
                                    resulte = "res" + tiepnhan + "=1";
                                }
                                //  từ chối tiếp nhận
                                if (Request.QueryString["act"] == tuchoitiepnhan)
                                {
                                    String sLydotuchoi = Request.QueryString["lydo"];
                                    string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                                    bool isSendMail = true;
                                    foreach (string sDonHoiVien in lstDonHoiVien)
                                    {
                                        string[] lstIterm = sDonHoiVien.Split('|');
                                        String sDonHoiVienID = lstIterm[0];
                                        String sDonHoiVienTinhtrang = lstIterm[1];
                                        if (sDonHoiVienTinhtrang.Trim() == tk_TTChoTiepNhan)
                                        {
                                            btnTuChoiDon_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                        }
                                    }
                                    if (!isSendMail)
                                    {
                                        //  resulte = "res" + duyetdon + "=0";
                                    }
                                    else
                                    {
                                        resulte = "res" + tuchoitiepnhan + "=1";
                                    }

                                }

                                //  phe duyet 
                                if (Request.QueryString["act"] == duyetdon)
                                {
                                    string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                                    bool isSendMail = true;
                                    foreach (string sDonHoiVien in lstDonHoiVien)
                                    {
                                        string[] lstIterm = sDonHoiVien.Split('|');
                                        String sDonHoiVienID = lstIterm[0];
                                        String sDonHoiVienTinhtrang = lstIterm[1];
                                        if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                        {
                                            btnPheDuyet_Click(sDonHoiVienID, ref isSendMail);
                                        }
                                    }
                                    if (!isSendMail)
                                    {
                                        //  resulte = "res" + duyetdon + "=0";
                                    }
                                    else
                                    {
                                        resulte = "res" + duyetdon + "=1";
                                    }

                                }
                                // tu choi duyet 
                                if (Request.QueryString["act"] == tuchoiduyet)
                                {
                                    String sLydotuchoi = Request.QueryString["lydo"];
                                    string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                                    bool isSendMail = true;
                                    foreach (string sDonHoiVien in lstDonHoiVien)
                                    {
                                        string[] lstIterm = sDonHoiVien.Split('|');
                                        String sDonHoiVienID = lstIterm[0];
                                        String sDonHoiVienTinhtrang = lstIterm[1];
                                        if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                        {
                                            btnTuChoiDuyet_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                        }
                                    }

                                    if (!isSendMail)
                                    {
                                        //  resulte = "res" + tuchoiduyet + "=0";
                                    }
                                    else
                                    {
                                        resulte = "res" + tuchoiduyet + "=1";
                                    }

                                }
                                // thoai duyet 
                                if (Request.QueryString["act"] == thoaiduyet)
                                {
                                    
                                    string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                                    bool isSendMail = true;
                                    foreach (string sDonHoiVien in lstDonHoiVien)
                                    {
                                        string[] lstIterm = sDonHoiVien.Split('|');
                                        String sDonHoiVienID = lstIterm[0];
                                        String sDonHoiVienTinhtrang = lstIterm[1];
                                        if (sDonHoiVienTinhtrang.Trim() == tk_TTDaDuyet)
                                        {
                                            btnThoaiDuyet_Click(sDonHoiVienID, ref isSendMail);
                                        }
                                    }
                                    if (!isSendMail)
                                    {
                                        //resulte = "res" + thoaiduyet + "=0";
                                    }
                                    else
                                    {
                                        resulte = "res" + thoaiduyet + "=1";
                                    }
                                }
                            }
                        }
                        //Response.Redirect("admin.aspx?page=xoatenhoiviencanhan&" + resulte);
                        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

                        Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(thongbao));
                    }

                }


            }
            catch (Exception ex)
            {
                Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            }




            ///////////

            load_users();


            if (Request.QueryString["thongbao"] == "1")
            {
                string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

                Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(thongbao));
            }


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }





    public DataSet sp_XoaTenHoiVienSearch()
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            bool isCaNhan = false; ;
            bool isTapThe = false;
            String sLoaiDon = "";
            
            String sSoChungChiKTV = "";
            String sLoaiXoaTen = "";
            String sTrangThai = "";
            String sTenHoiVien = "";
            String sHoDemCaNhan = "";
            String sTenCaNhan = "";

            String sIDHoiVien = "";
            String sSoDon = "";
            String sHinhThucNop = "";
            String sMaTinhThanh = "";
            String sMaQuanHuyen = "";

            String sCongtyKiemToan = "";
           
            String sSoCNDuDieuKienHNKT = "";

            String sNgayNopTu = "";
            String sNgayNopDen = "";
            String sNgayDuyetTu = "";
            String sNgayDuyetDen = "";




            // Trang thai
            if (Request.Form["tk_TTAll"] != null && Request.Form["tk_HVCN"] == "true")
            {  // check all

                sTrangThai = "";// all
            }
            else
            {
                int itemp = 0;
                if (Request.Form["tk_TTChoTiepNhan"] != null && Request.Form["tk_TTChoTiepNhan"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTChoTiepNhan;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTChoTiepNhan;
                    }

                    itemp = itemp + 1;
                }
                if (Request.Form["tk_TTChoDuyet"] != null && Request.Form["tk_TTChoDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTChoDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTChoDuyet;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTTuChoiTiepNhan"] != null && Request.Form["tk_TTTuChoiTiepNhan"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTTuChoiTiepNhan;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTTuChoiTiepNhan;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTTuChoiDuyet"] != null && Request.Form["tk_TTTuChoiDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTTuChoiDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTTuChoiDuyet;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTDaDuyet"] != null && Request.Form["tk_TTDaDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTDaDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTDaDuyet;
                    }
                    itemp = itemp + 1;

                }
                //if (Request.Form["tk_TTThoaiDuyet"] != null && Request.Form["tk_TTThoaiDuyet"] == "true")
                //{
                //    if (itemp == 0)
                //    {
                //        sTrangThai = tk_TTThoaiDuyet;
                //    }
                //    else
                //    {
                //        sTrangThai = sTrangThai + "," + tk_TTThoaiDuyet;
                //    }
                //    itemp = itemp + 1;

                //}


            }



            // Ten hoi vien
            if (Request.Form["tk_TenHoiVien"] != null)
            {
                sTenHoiVien = Request.Form["tk_TenHoiVien"];


            }

            // Ten dang nhap
            if (Request.Form["tk_ID"] != null)
            {
                sIDHoiVien = Request.Form["tk_ID"];
            }


            
            // Loai Xoa TEn
            if (Request.Form["tk_LoaiXoaTen"] != null)
            {
                sLoaiXoaTen = Request.Form["tk_LoaiXoaTen"];
            }


            // Ten nguoi dai dien
            if (Request.Form["tk_SoChungChiKTV"] != null)
            {
                sSoChungChiKTV = Request.Form["tk_SoChungChiKTV"];
            }



            // Ma Tinh
            if (Request.Form["TinhID_ToChuc"] != null)
            {
                sMaTinhThanh = Request.Form["TinhID_ToChuc"];
            }
            // Ma Huyen
            if (Request.Form["HuyenID_ToChuc"] != null)
            {
                sMaQuanHuyen = Request.Form["HuyenID_ToChuc"];
            }


            // So CNDKHNKT
            if (Request.Form["tk_SoDKHNKT"] != null)
            {
                sSoCNDuDieuKienHNKT = Request.Form["tk_SoDKHNKT"];
            }


            // Ngay nop tu
            if (Request.Form["tk_NgayNopTu"] != null)
            {
                sNgayNopTu = Request.Form["tk_NgayNopTu"].Replace("/", ""); ;
            }

            // Ngay nop den
            if (Request.Form["tk_NgayNopDen"] != null)
            {
                sNgayNopDen = Request.Form["tk_NgayNopDen"].Replace("/", "");
            }
            // Ngay duyet tu
            if (Request.Form["tk_NgayPheDuyetTu"] != null)
            {
                sNgayDuyetTu = Request.Form["tk_NgayPheDuyetTu"].Replace("/", ""); ;
            }
            //  Ngay duyet den
            if (Request.Form["tk_NgayPheDuyetDen"] != null)
            {
                sNgayDuyetDen = Request.Form["tk_NgayPheDuyetDen"].Replace("/", ""); ;
            }


            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();


            arrParams.Add(new DBHelpers.DataParameter("TrangThai", sTrangThai));
            arrParams.Add(new DBHelpers.DataParameter("TenHoiVien", sTenHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("LoaiXoaTen", sLoaiXoaTen));

            arrParams.Add(new DBHelpers.DataParameter("IDHoiVien", sIDHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("SoChungChiKTV", sSoChungChiKTV));
            
            arrParams.Add(new DBHelpers.DataParameter("MaTinhThanh", sMaTinhThanh));
            arrParams.Add(new DBHelpers.DataParameter("MaQuanHuyen", sMaQuanHuyen));

            arrParams.Add(new DBHelpers.DataParameter("SoCNDKHNKT", sSoCNDuDieuKienHNKT));

            arrParams.Add(new DBHelpers.DataParameter("NgayKetNapTu", sNgayNopTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayKetNapDen", sNgayNopDen));
            arrParams.Add(new DBHelpers.DataParameter("NgayXoaTenTu", sNgayDuyetTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayXoaTenDen", sNgayDuyetDen));


            objDBHelper = ConnectionControllers.Connect("VACPA");

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_xoatenhoiviencn_search", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }


    protected void load_users()
    {
        try
        {



            DataSet ds = sp_XoaTenHoiVienSearch();

            DataView dt = ds.Tables[0].DefaultView;


            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;
            int i;
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "XoaTenHoiVien" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            ds = null;
            dt = null;




        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }
    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }
    protected void btnGhi_Click(object sender, EventArgs e)
    {

    }

    protected void btnTuChoiDon_Click(String sDonHoiVienID, String sLyDoTuChoi, ref bool isSendMail)
    {
        try
        {
            int donHoiVienTtId = 0;
            try
            {
                donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            }
            catch { }
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), sLyDoTuChoi, ref Email);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '1' AND XoaTenHoiVienID = " + donHoiVienTtId;
                string HoiVienID = DataAccess.DLookup(cmd);

                // Send mail
                string body = "";
                body += "VACPA từ chối tiếp nhận đơn xin xóa tên hội viên cá nhân của bạn<BR/>";
                body += "Lý do từ chối: " + sLyDoTuChoi + " <BR/>";
                // Gửi thông báo
                cm.GuiThongBao(HoiVienID, "1", "VACPA - Từ chối tiếp nhận đơn xin xóa tên hội viên cá nhân", body);

                cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoDem, Ten FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + HoiVienID;
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối tiếp nhận đơn xin xóa tên hội viên " + dtb.Rows[0]["HoDem"].ToString() + " " + dtb.Rows[0]["Ten"].ToString());

                string msg = "";
                if (Email != "")
                {
                    if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối tiếp nhận đơn xin xóa tên hội viên cá nhân", body, ref msg))
                    { }
                    else
                    {
                        isSendMail = false;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void btnTrinhPheDuyet_Click(String sDonHoiVienID)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                UpdateStatus(donHoiVienTtId, trangthaiChoDuyetID, "", "", "");

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoDem, Ten FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = (SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '1' AND XoaTenHoiVienID = " + donHoiVienTtId + ")";
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Tiếp nhận đơn xin xóa tên hội viên " + dtb.Rows[0]["HoDem"].ToString() + " " + dtb.Rows[0]["Ten"].ToString());
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnTuChoiDuyet_Click(String sDonHoiVienID, String sLyDoTuChoi, ref bool isSendMail)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiTraLaiID, cm.Admin_TenDangNhap,
                        DateTime.Now.ToString("ddMMyyyy"), sLyDoTuChoi, ref Email);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '1' AND XoaTenHoiVienID = " + donHoiVienTtId;
                string HoiVienID = DataAccess.DLookup(cmd);

                // Send mail
                string body = "";
                body += "VACPA từ chối phê duyệt xóa tên hội viên cá nhân của bạn<BR/>";
                body += "Lý do từ chối: " + sLyDoTuChoi + " <BR/>";
                // Gửi thông báo
                cm.GuiThongBao(HoiVienID, "1", "VACPA - Từ chối duyệt yêu cầu xóa tên hội viên cá nhân", body);

                cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoDem, Ten FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + HoiVienID;
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối duyệt đơn xin xóa tên hội viên " + dtb.Rows[0]["HoDem"].ToString() + " " + dtb.Rows[0]["Ten"].ToString());

                string msg = "";
                if (Email != "")
                {
                    if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối duyệt yêu cầu xóa tên hội viên cá nhân", body, ref msg))
                    { }
                    else
                    {
                        isSendMail = false;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnPheDuyet_Click(String sDonHoiVienID, ref bool isSendMail)
    {

        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiDaPheDuyet, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), "", ref Email);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '1' AND XoaTenHoiVienID = " + donHoiVienTtId;
                string HoiVienID = DataAccess.DLookup(cmd);

                // Send mail
                string body = "";
                body += "Bạn đã được phê duyệt xin thôi gia nhập hội viên cá nhân, bạn vẫn có thể đăng nhập vào hệ thống và sử dụng phần mềm với người sử dụng là Kiểm toán viên<BR/>";
                // Gửi thông báo
                cm.GuiThongBao(HoiVienID, "1", "VACPA - Phê duyệt xin thôi gia nhập hội viên cá nhân tại trang tin điện tử VACPA", body);

                cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoDem, Ten FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + HoiVienID;
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin xóa tên hội viên " + dtb.Rows[0]["HoDem"].ToString() + " " + dtb.Rows[0]["Ten"].ToString());

                string msg = "";

                String thongbao = "";
                if (Email != "")
                {
                    if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Phê duyệt xin thôi gia nhập hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg))
                    { }
                    else
                    {
                        isSendMail = false;
                    }
                }

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void btnThoaiDuyet_Click(String sDonHoiVienID, ref bool isSendMail)
    {

        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiThoaiDuyetID, cm.Admin_TenDangNhap,
                    DateTime.Now.ToString("ddMMyyyy"), "", ref Email);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoDem, Ten FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = (SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '1' AND XoaTenHoiVienID = " + donHoiVienTtId + ")";
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Thoái duyệt đơn xin xóa tên hội viên " + dtb.Rows[0]["HoDem"].ToString() + " " + dtb.Rows[0]["Ten"].ToString());
                // Send mail
                string body = "";
                body += "VACPA đã thoái duyệt đơn xin thôi gia nhập hội viên cá nhân, bạn có thể sử dụng tài khoản để đăng nhập vào phần mềm với tư cách là hội viên cá nhân:<BR/>";
                
                string msg = "";

                if (SmtpMail.Send("BQT WEB VACPA", Email, "Thoái duyệt đơn xin thôi gia nhập hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg))
                { }
                else
                {
                    isSendMail = false;

                }


            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    public bool UpdateStatus(int XoaTenHoiVienID, int TinhTrangID, String NguoiDuyet,
                           String NgayDuyet, String LyDoTuChoi)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("XoaTenHoiVienID", XoaTenHoiVienID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));


            objDBHelper = ConnectionControllers.Connect("VACPA");
            return objDBHelper.ExecFunction("sp_xoatenhoiviencn_xetduyet", arrParams);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }
    public bool UpdateStatus(int XoaTenHoiVienID, int TinhTrangID, String NguoiDuyet,
                               String NgayDuyet, String LyDoTuChoi, ref string Email)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("XoaTenHoiVienID", XoaTenHoiVienID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));



            DBHelpers.DataParameter outEmail = new DBHelpers.DataParameter("Email", Email);
            outEmail.OutPut = true;
            arrParams.Add(outEmail);


            objDBHelper = ConnectionControllers.Connect("VACPA");
            bool bRedulte = objDBHelper.ExecFunction("sp_xoatenhoiviencn_xetduyet", arrParams);

            Email = outEmail.Param_Value.ToString();

            return bRedulte;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }
}