﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_QLHV_Process : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string action = Request.QueryString["action"];
            if (action == "loadct")
            {
                string maCongTy = Request.QueryString["mact"];
                GetInforCongTyKiemToan_HVTT(maCongTy);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void GetInforCongTyKiemToan_HVTT(string maCongTy)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#;#;#';" + Environment.NewLine;
        if (!string.IsNullOrEmpty(maCongTy))
        {
            string query = @"SELECT HoiVienTapTheID, MaHoiVienTapThe, TenDoanhNghiep, TenVietTat, TenTiengAnh, DiaChi, MaSoThue, 
                                SoGiayChungNhanDKKD, NgayGiaNhap, NgayCapGiayChungNhanDKKD, NguoiDaiDienLL_Ten, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email
	                            FROM tblHoiVienTapThe
	                            WHERE MaHoiVienTapThe = '" + maCongTy + @"' ORDER BY TenDoanhNghiep";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "value = '" + ht["HoiVienTapTheID"] + ";#" + ht["MaHoiVienTapThe"].ToString().Trim() + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"].ToString())
                         + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenVietTat"].ToString()) + "';" + Environment.NewLine;
            }
            else
                js += "parent.alert('Không tìm thấy thông tin với mã vừa nhập!');" + Environment.NewLine;
        }
        js += "parent.DisplayInforCongTy(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }
}