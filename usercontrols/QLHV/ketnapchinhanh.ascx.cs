﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using HiPT.VACPA.DL;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Configuration;

using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_ketnapchinhanh : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    String id = String.Empty;

    public DonHoiVienTapThe objHoiVIenTapThe = new DonHoiVienTapThe();
    public HoiVienTapThe objCongTyKiemToan = new HoiVienTapThe();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    protected DonHoiVienFile FileDinhKem1 = new DonHoiVienFile();

    string DoiTuongHoiVienTapThe = "4";
    string DoiTuongCongTyKiemToan = "3";
    int trangthaiChoTiepNhanID = 1;
    int trangthaiTuChoiID = 2;
    int trangthaiChoDuyetID = 3;
    int trangthaiTraLaiID = 4;
    int trangthaiDaPheDuyet = 5;
    int trangthaiThoaiDuyetID = 6;


    string tk_TTChoTiepNhan = "1";
    string tk_TTTuChoiTiepNhan = "2";
    string tk_TTChoDuyet = "3";
    string tk_TTTuChoiDuyet = "4";
    string tk_TTDaDuyet = "5";
    string tk_TTThoaiDuyet = "6";

    public bool QuyenXem { get; set; }
    public bool QuyenThem { get; set; }
    public bool QuyenSua { get; set; }
    public bool QuyenXoa { get; set; }
    public bool QuyenDuyet { get; set; }
    public bool QuyenTraLai { get; set; }
    public bool QuyenHuy { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(cm.Admin_NguoiDungID))
                {
                    string maquyen = kiemtraquyen.fcnXDQuyen(int.Parse(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr);

                    QuyenXem = (maquyen.Contains("XEM")) ? true : false;
                    QuyenThem = (maquyen.Contains("THEM")) ? true : false;
                    QuyenSua = (maquyen.Contains("SUA")) ? true : false;
                    QuyenXoa = (maquyen.Contains("XOA")) ? true : false;
                    QuyenDuyet = (maquyen.Contains("DUYET")) ? true : false;
                    QuyenTraLai = (maquyen.Contains("TRALAI")) ? true : false;
                    QuyenHuy = (maquyen.Contains("HUY")) ? true : false;
                }
                LoadDropDown();
                Load_ThanhPho("00");


                int donHoiVienTtId = 0;
                try
                {
                    donHoiVienTtId = Convert.ToInt32(Request.QueryString["id"]);
                }
                catch
                {

                }
                loadTTHoiVien(donHoiVienTtId);
                loaduploadFileDinhKem(donHoiVienTtId);

                init_button();

                if (Request.QueryString["thongbao"] == "1")
                {
                    string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Lưu thông tin thành công!</div>";

                    placeMessage.Controls.Add(new LiteralControl(thongbao));
                }
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));


        }


    }

    public void init_button()
    {
        try
        {
            String actionMode = Request.QueryString["act"];

            linkGhi.Visible = false;
            linkTuChoiDon.Visible = false;
            linkTrinhPheDuyet.Visible = false;
            linkTuChoiDuyet.Visible = false;
            linkPheDuyet.Visible = false;

            linkXoaDon.Visible = false;
            //linkThoaiDuyet.Visible = false;

            if (actionMode == "view")
            {// vieu
                hidMode.Value = "view";

            }
            if (actionMode == "add")
            {// add


                hidMode.Value = "add";
                linkGhi.Visible = true; ;
                linkTuChoiDon.Visible = false;
                linkTrinhPheDuyet.Visible = false;
                linkTuChoiDuyet.Visible = false;
                linkPheDuyet.Visible = false;

                linkXoaDon.Visible = false;
                // linkThoaiDuyet.Visible = false;

            }

            if (actionMode == "edit")
            { // edit
                hidMode.Value = "edit";
                String tempTrangthai = "";
                try
                {
                    tempTrangthai = (hidTrangthaiId.Value);
                }
                catch
                {

                }
                //int trangthaiChoTiepNhanID = 3;
                //int trangthaiTuChoiID = 4;
                //int trangthaiChoDuyetID = 1;
                //int trangthaiTraLaiID = 2;
                //int trangthaiThoaiDuyetID = 5;
                //int trangthaiDaPheDuyet = 6;

                //  cho tiep nhan
                if (tempTrangthai == trangthaiChoTiepNhanID.ToString())
                {
                    if (hidHinhThucNop.Value == "2")// nop truc tiep   if (!String.IsNullOrEmpty(hidVacpaUser.Value))
                    {
                        linkGhi.Visible = true; ;
                    }

                    linkTuChoiDon.Visible = true;
                    linkTrinhPheDuyet.Visible = true;
                    //
                    linkTuChoiDuyet.Visible = false;
                    linkPheDuyet.Visible = false;

                    //linkThoaiDuyet.Visible = false;
                    linkXoaDon.Visible = true;
                }
                else if (tempTrangthai == trangthaiTuChoiID.ToString())
                {   // tu choi nhan don
                    if (hidHinhThucNop.Value == "2")// nop truc tiep if (!String.IsNullOrEmpty(hidVacpaUser.Value))
                    {
                        linkGhi.Visible = true; ;
                    }

                    linkTuChoiDon.Visible = false;
                    linkTrinhPheDuyet.Visible = true;

                    linkTuChoiDuyet.Visible = false;
                    linkPheDuyet.Visible = false;

                    // linkThoaiDuyet.Visible = false;
                    linkXoaDon.Visible = true;
                }
                else if (tempTrangthai == trangthaiChoDuyetID.ToString())
                {   // da trinh phe duyet( cho duyet)
                    linkGhi.Visible = true;
                    linkTuChoiDon.Visible = false;
                    linkTrinhPheDuyet.Visible = false;
                    linkTuChoiDuyet.Visible = true;
                    linkPheDuyet.Visible = true;

                    // linkThoaiDuyet.Visible = false;
                    linkXoaDon.Visible = true;
                }
                else if (tempTrangthai == trangthaiTraLaiID.ToString())
                { // Chu tich ko phe duyet
                    linkGhi.Visible = true;
                    linkTuChoiDon.Visible = false;
                    linkTrinhPheDuyet.Visible = false;
                    linkTuChoiDuyet.Visible = false;
                    linkPheDuyet.Visible = false;

                    //linkThoaiDuyet.Visible = true;
                    linkXoaDon.Visible = true;
                }
                else if (tempTrangthai == trangthaiDaPheDuyet.ToString())
                {   // Chu tich da phe duyet
                    linkGhi.Visible = false;
                    linkTuChoiDon.Visible = false;
                    linkTrinhPheDuyet.Visible = false;
                    linkTuChoiDuyet.Visible = false;
                    linkPheDuyet.Visible = false;

                    //linkThoaiDuyet.Visible = true;
                    linkXoaDon.Visible = true;
                }

                else
                {
                    linkGhi.Visible = false;
                    linkTuChoiDon.Visible = false;
                    linkTrinhPheDuyet.Visible = false;
                    linkTuChoiDuyet.Visible = false;
                    linkPheDuyet.Visible = false;
                    // linkThoaiDuyet.Visible = false;
                    linkXoaDon.Visible = false;
                }


            }


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));


        }

    }


    public void LoadDropDown()
    {
        conn.Open();

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        // lblErrNhapMa.Visible = false;
        string sql = "";

        // hội sở chính
        sql = "SELECT 0 AS HoiVienTapTheID, '' AS SoHieu, N'<< Lựa chọn >>' AS TenDoanhNghiep UNION ALL (SELECT HoiVienTapTheID, SoHieu, SoHieu + ' - ' + TenDoanhNghiep AS TenDoanhNghiep FROM tblHoiVienTapThe) ORDER BY SoHieu";
        //SqlCommand cmd = new SqlCommand(sql, conn);
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drHoiVienTapThe.DataSource = dtb;
        drHoiVienTapThe.DataBind();

        ds.Clear();
        dtb.Clear();


        // Linh vuc hoat dong
        sql = "SELECT 0 AS LinhVucHoatDongID, N'<< Lựa chọn >>' AS TenLinhVucHoatDong , '' AS MaLinhVucHoatDong  UNION ALL (SELECT LinhVucHoatDongID, TenLinhVucHoatDong , MaLinhVucHoatDong FROM tblDMLinhVucHoatDong)";
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drLinhvucHoatDong.DataSource = dtb;
        drLinhvucHoatDong.DataBind();

        ds.Clear();
        dtb.Clear();


        // Chuc vu nguoi dai dien Phap Luat
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drNguoiDaiDienPL_ChucVu.DataSource = dtb;
        drNguoiDaiDienPL_ChucVu.DataBind();

        ds.Clear();
        dtb.Clear();

        // Chuc vu nguoi dai dien Lien lac
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drNguoiDaiDienLL_ChucVu.DataSource = dtb;
        drNguoiDaiDienLL_ChucVu.DataBind();

        //ds.Clear();
        //dtb.Clear();
        //// Load câu hỏi bí mật
        //sql = "SELECT * FROM tblDMCauHoiBiMat";
        //da = new SqlDataAdapter(sql, conn);
        //da.Fill(ds);
        //dtb = ds.Tables[0];
        //drCauHoi.DataSource = dtb;
        //drCauHoi.DataBind();

        conn.Close();
    }



    public void Load_ThanhPho(string MaTinh = "00")
    {

        //   conn.Open();

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        //lblErrNhapMa.Visible = false;
        string sql = "";

        sql = "SELECT '00' AS MaTinh, N'<< Lựa chọn >>' AS TenTinh UNION ALL (SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ) ORDER BY TenTinh ASC";

        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drTinhThanh.DataSource = dtb;
        drTinhThanh.DataBind();

        ds.Clear();
        dtb.Clear();



    }

    public void Load_QuanHuyen(string MaHuyen = "000", string MaTinh = "00")
    {
        //  conn.Open();

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        //lblErrNhapMa.Visible = false;
        string sql = "";

        sql = "SELECT '000' AS MaHuyen, N'<< Lựa chọn >>' AS TenHuyen UNION ALL (SELECT MaHuyen,TenHuyen FROM tblDMHuyen WHERE (MaTinh='" + MaTinh + "') AND HieuLuc='1' ) ORDER BY TenHuyen ASC";

        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drQuanHuyen.DataSource = dtb;
        drQuanHuyen.DataBind();

        ds.Clear();
        dtb.Clear();


        drPhuongXa.Items.Clear();
        drPhuongXa.DataSource = null;
        drPhuongXa.DataBind();


    }

    public void Load_PhuongXa(string MaXa = "00000", string MaHuyen = "000")
    {
        //   conn.Open();

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        //lblErrNhapMa.Visible = false;
        string sql = "";

        sql = "SELECT '0000' AS MaXa, N'<< Lựa chọn >>' AS TenXa UNION ALL (SELECT MaXa,TenXa FROM tblDMXa WHERE (MaHuyen='" + MaHuyen + "') ) ORDER BY TenXa ASC";

        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drPhuongXa.DataSource = dtb;
        drPhuongXa.DataBind();

        ds.Clear();
        dtb.Clear();



    }

    protected void drTinhThanh_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_QuanHuyen("000", drTinhThanh.SelectedValue.Trim());
    }

    protected void drQuanHuyen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_PhuongXa("00000", drQuanHuyen.SelectedValue.Trim());
    }

    protected void btnGhi_Click(object sender, EventArgs e)
    {

        try
        {

            int donHoiVienTtId = 0;
            try
            {
                donHoiVienTtId = Convert.ToInt32(hidId.Value.ToString());
            }
            catch { }
            CapnhatTTHoiVien(donHoiVienTtId);
            CapnhatFile(donHoiVienTtId);
            try
            {
                donHoiVienTtId = Convert.ToInt32(hidId.Value.ToString());
            }
            catch { }
            loaduploadFileDinhKem(donHoiVienTtId);

            init_button();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));

        }


    }


    protected void btnTuChoiDon_Click(object sender, EventArgs e)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(hidId.Value.ToString());
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                //UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, NguoiDuyet,
                //             NgayNopDon, NgayTiepNhan, NgayDuyet, LyDoTuChoi);

                UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, "",
                              "", "", "", txtLyDoTuChoi.Text);
                hidTrangthaiId.Value = trangthaiTuChoiID.ToString();
                string thongbao = "";

                string http_Server = (string)System.Configuration.ConfigurationSettings.AppSettings["http_Server"];
                // Send mail
                string body = "";
                body += "Công ty không được tiếp nhận đơn kết nạp hội viên tổ chức:<BR/>";
                body += "Lý do từ chối: " + txtLyDoTuChoi.Text + " <BR/>";
                body += "Bạn có thể cập nhật lại thông tin đơn theo đường links : " + http_Server + "Page/DangKyHoiVien/DangKyHoiVienTapThe.aspx?id=" + donHoiVienTtId + " <BR/>";

                string msg = "";
                if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ đơn xin kết nạp hội viên tổ chức", body, ref msg))
                {
                    thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;"">
                							<p>" + "Cập nhật trạng thái thành công." + @"</p>							
                						</div>";

                    placeMessage.Controls.Add(new LiteralControl(thongbao));

                }
                else
                {
                    msg = "Lỗi không gửi được mail";
                    placeMessage.Controls.Add(new LiteralControl(msg));
                }

                init_button();

                SqlDonHoiVienTapTheProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienTapTheProvider(cm.connstr, true, "");
                DonHoiVienTapThe hoivientapthe = new DonHoiVienTapThe();
                hoivientapthe = DonHoiVienCaNhan_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối tiếp nhận đơn xin kết nạp hội viên " + hoivientapthe.TenDoanhNghiep);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void btnTrinhPheDuyet_Click(object sender, EventArgs e)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(hidId.Value.ToString());
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                //UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, NguoiDuyet,
                //             NgayNopDon, NgayTiepNhan, NgayDuyet, LyDoTuChoi);
                UpdateStatus(donHoiVienTtId, trangthaiChoDuyetID, "", "", "", "", "", "");
                hidTrangthaiId.Value = trangthaiChoDuyetID.ToString();
                string thongbao = @"Trình phê duyệt hồ sơ thành công. ";


                placeMessage.Controls.Add(new LiteralControl(thongbao));
                init_button();

                SqlDonHoiVienTapTheProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienTapTheProvider(cm.connstr, true, "");
                DonHoiVienTapThe hoivientapthe = new DonHoiVienTapThe();
                hoivientapthe = DonHoiVienCaNhan_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Tiếp nhận đơn xin kết nạp hội viên " + hoivientapthe.TenDoanhNghiep);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnTuChoiDuyet_Click(object sender, EventArgs e)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(hidId.Value.ToString());
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                //UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, NguoiDuyet,
                //             NgayNopDon, NgayTiepNhan, NgayDuyet, LyDoTuChoi);

                UpdateStatus(donHoiVienTtId, trangthaiTraLaiID, "", cm.Admin_TenDangNhap, "", "", DateTime.Now.ToString("ddMMyyyy"), txtLyDoTuChoi.Text);
                hidTrangthaiId.Value = trangthaiTraLaiID.ToString();
                string thongbao = "";

                string http_Server = (string)System.Configuration.ConfigurationSettings.AppSettings["http_Server"];
                // Send mail
                string body = "";
                body += "Công ty không được phê duyệt kết nạp hội viên tổ chức:<BR/>";
                body += "Lý do từ chối: " + txtLyDoTuChoi.Text + " <BR/>";
                body += "Bạn có thể cập nhật lại thông tin đơn theo đường links : " + http_Server + "Page/DangKyHoiVien/DangKyHoiVienTapThe.aspx?id=" + donHoiVienTtId + " <BR/>";
                string msg = "";
                if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ đơn xin kết nạp hội viên tổ chức", body, ref msg))
                {
                    thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;"">
                							<p>" + "Cập nhật trạng thái thành công." + @"</p>							
                						</div>";
                    placeMessage.Controls.Add(new LiteralControl(thongbao));
                }
                else
                {
                    msg = "Lỗi không gửi được mail";
                    placeMessage.Controls.Add(new LiteralControl(msg));
                }

                init_button();

                SqlDonHoiVienTapTheProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienTapTheProvider(cm.connstr, true, "");
                DonHoiVienTapThe hoivientapthe = new DonHoiVienTapThe();
                hoivientapthe = DonHoiVienCaNhan_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối phê duyệt đơn xin kết nạp hội viên " + hoivientapthe.TenDoanhNghiep);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    private int getHoiVienTapTheID(int DonHoiVienTapTheID)
    {
        int HoiVienTapTheID = 0;

        try
        {

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoiVienTapTheID from tblDonHoiVienTapThe where DonHoiVienTapTheID =" + DonHoiVienTapTheID;

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];



            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                HoiVienTapTheID = Convert.ToInt32(tem["HoiVienTapTheID"]);
            }
        }
        catch { }

        return HoiVienTapTheID;
    }

    protected void btnSuaQuyetDinh_Click(object sender, EventArgs e)
    {
        SqlDonHoiVienTapTheProvider DonHoiVienTapThe_provider = new SqlDonHoiVienTapTheProvider(connStr, true, "");
        SqlHoiVienTapTheProvider HoiVienTapThe_provider = new SqlHoiVienTapTheProvider(connStr, true, "");
        DonHoiVienTapThe donhoivien = DonHoiVienTapThe_provider.GetByDonHoiVienTapTheId(int.Parse(Request.QueryString["id"]));
        HoiVienTapThe hoivien = HoiVienTapThe_provider.GetByHoiVienTapTheId(donhoivien.HoiVienTapTheId.Value);

        hoivien.SoQuyetDinhKetNap = txtSoQuyetDinh.Text;
        hoivien.NgayQuyetDinhKetNap = DateTime.ParseExact(NgayQD.Value, "dd/MM/yyyy", null);

        HoiVienTapThe_provider.Update(hoivien);

        cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Cập nhật số quyết định/ngày quyết định của đơn xin kết nạp hội viên " + hoivien.TenDoanhNghiep);
        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

        placeMessage.Controls.Add(new LiteralControl(thongbao));
    }

    protected void btnPheDuyet_Click(object sender, EventArgs e)
    {

        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(hidId.Value.ToString());
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                //UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, NguoiDuyet,
                //             NgayNopDon, NgayTiepNhan, NgayDuyet, LyDoTuChoi);
                string TenDangNhap = "";
                string MatKhau = "";
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiDaPheDuyet, "", cm.Admin_TenDangNhap, "", "", DateTime.Now.ToString("ddMMyyyy"), "", ref TenDangNhap, ref MatKhau, ref Email);
                hidTrangthaiId.Value = trangthaiDaPheDuyet.ToString();


                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select HoiVienTapTheID from tblDonHoiVienTapThe where DonHoiVienTapTheID =" + donHoiVienTtId;
                string HoiVienTapTheID = DataAccess.DLookup(cmd);

                try
                {
                    if (!string.IsNullOrEmpty(txtSoQuyetDinh.Text) && !string.IsNullOrEmpty(NgayQD.Value))
                    {
                        cmd = new SqlCommand();
                        cmd.CommandText = "UPDATE tblHoiVienTapThe SET LoaiHoiVienTapTheChiTiet = NULL, SoQuyetDinhKetNap = '" + txtSoQuyetDinh.Text + "' , NgayQuyetDinhKetNap = '" + DateTime.ParseExact(NgayQD.Value, "dd/MM/yyyy", null) + "' WHERE HoiVienTapTheID = " + HoiVienTapTheID;

                        DataAccess.RunActionCmd(cmd);
                    }
                }
                catch { }

                //// insert vào bảng tblTTPhatSinhPhi
                //cmd.CommandText = "SELECT NamDuDKKT_CK FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + HoiVienTapTheID;
                //string namdudkkt_ck = DataAccess.DLookup(cmd);

                //if (!string.IsNullOrEmpty(namdudkkt_ck) && namdudkkt_ck != "0")
                //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";
                //else
                //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttKhongDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";

                //DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                //if (dtb.Rows.Count != 0)
                //{

                //    cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + HoiVienTapTheID + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                //    DataAccess.RunActionCmd(cmd);
                //}

                // Send mail
                string body = "";
                body += "Công ty đã được phê duyệt kết nạp hội viên tổ chức, có thể sử dụng tài khoản sau để đăng nhập vào phần mềm:<BR/>";
                body += "Tên đăng nhập: <B>" + TenDangNhap + "</B><BR/>";
                body += "Mật khẩu: <B>" + MatKhau + "</B><BR/>";
                string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;""><p>Phê duyệt hồ sơ thành công</p></div>";
                string msg = "";
                //if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "VACPA - Thông tin tài khoản hội viên tổ chức tại trang tin điện tử VACPA", body, ref msg))
                //{
                placeMessage.Controls.Add(new LiteralControl(thongbao));
                //}
                //else
                //placeMessage.Controls.Add(new LiteralControl(msg));


                init_button();
                SqlDonHoiVienTapTheProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienTapTheProvider(cm.connstr, true, "");
                DonHoiVienTapThe hoivientapthe = new DonHoiVienTapThe();
                hoivientapthe = DonHoiVienCaNhan_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin kết nạp hội viên " + hoivientapthe.TenDoanhNghiep);

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void btnThoaiDuyetClick(object sender, EventArgs e)
    {

        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(hidId.Value.ToString());
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                //UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, NguoiDuyet,
                //             NgayNopDon, NgayTiepNhan, NgayDuyet, LyDoTuChoi);

                UpdateStatus(donHoiVienTtId, trangthaiThoaiDuyetID, "", cm.Admin_TenDangNhap, "", "", "", "");
                hidTrangthaiId.Value = trangthaiThoaiDuyetID.ToString();
                string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;"">
                							<p>" + "Cập nhật tạng thái thành công." + @"</p>							
                						</div>";

                placeMessage.Controls.Add(new LiteralControl(thongbao));
                init_button();
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }
    protected void btnXoaDon_Click(object sender, EventArgs e)
    {

        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(hidId.Value.ToString());
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                sp_DonHoiVienDelete(hidId.Value.ToString());
                string msg = "Xóa thông tin thành công";
                Response.Redirect("admin.aspx?page=KetNapHoiVien&msg=" + msg);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }
    public DataSet sp_DonHoiVienDelete(string sLstDonHoiVienID)
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("LstDonHoiVienID", sLstDonHoiVienID));

            objDBHelper = ConnectionControllers.Connect("VACPA");

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_donhoivien_delete", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }

    protected void loadTTHoiVien(int donHoiVienTtId)
    {
        try
        {

            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                SqlDonHoiVienTapTheProvider DonHoiVienTapThe_provider = new SqlDonHoiVienTapTheProvider(connStr, true, "");
                SqlHoiVienTapTheProvider HoiVienTapThe_provider = new SqlHoiVienTapTheProvider(connStr, true, "");
                objHoiVIenTapThe = DonHoiVienTapThe_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);

                if (objHoiVIenTapThe != null)
                {
                    if (objHoiVIenTapThe.LoaiHoiVienTapTheChiTiet != null)
                        drLoaiHoiVien.SelectedValue = objHoiVIenTapThe.LoaiHoiVienTapTheChiTiet.Value.ToString();
                    if (objHoiVIenTapThe.HoiVienTapTheChaId != null)
                    {
                        drHoiVienTapThe.SelectedValue = objHoiVIenTapThe.HoiVienTapTheChaId.Value.ToString();
                        HoiVienTapThe hvtt = HoiVienTapThe_provider.GetByHoiVienTapTheId(objHoiVIenTapThe.HoiVienTapTheChaId.Value);
                        if (hvtt.LoaiHoiVienTapTheChiTiet != null)
                            drLoaiHoiVien.SelectedValue = hvtt.LoaiHoiVienTapTheChiTiet.Value.ToString();
                    }
                    txtTenDoanhNghiep.Text = objHoiVIenTapThe.TenDoanhNghiep;
                    
                    txtDiaChi.Text = objHoiVIenTapThe.DiaChi;
                    drTinhThanh.SelectedValue = objHoiVIenTapThe.DiaChiTinhId;
                    Load_QuanHuyen("000", drTinhThanh.SelectedValue.Trim());
                    drQuanHuyen.SelectedValue = objHoiVIenTapThe.DiaChiHuyenId;
                    Load_PhuongXa("00000", drQuanHuyen.SelectedValue.Trim());
                    drPhuongXa.SelectedValue = objHoiVIenTapThe.DiaChiXaId;



                    try
                    {
                        txtNgayThanhLap.Text = (objHoiVIenTapThe.NgayThanhLap).Value.ToString("dd/MM/yyyy");
                    }
                    catch
                    {
                    }

                    txtDienThoai.Text = objHoiVIenTapThe.DienThoai;
                    txtEmail.Text = objHoiVIenTapThe.Email;
                    txtFax.Text = objHoiVIenTapThe.Fax;
                    txtWebsite.Text = objHoiVIenTapThe.Website;
                    txtSoGiayChungNhanDKKD.Text = objHoiVIenTapThe.SoGiayChungNhanDkkd;

                    try
                    {
                        txtNgayCapGiayChungNhanDKKD.Text = (objHoiVIenTapThe.NgayCapGiayChungNhanDkkd).Value.ToString("dd/MM/yyyy");
                    }
                    catch
                    {
                    }

                    txtSoGiayChungNhanKDDVKT.Text = objHoiVIenTapThe.SoGiayChungNhanKddvkt;
                    try
                    {
                        txtNgayCapGiayChungNhanKDDVKT.Text = (objHoiVIenTapThe.NgayCapGiayChungNhanKddvkt).Value.ToString("dd/MM/yyyy");
                    }
                    catch
                    {
                    }


                    txtMaSoThue.Text = objHoiVIenTapThe.MaSoThue;
                    try
                    {
                        drLinhvucHoatDong.SelectedValue = objHoiVIenTapThe.LinhVucHoatDongId.ToString();
                    }
                    catch
                    {
                    }
                    txtNamCoLoiIchCongChung.Text = objHoiVIenTapThe.NamDuDkktKhac;
                    txtNamTrongLinhVucChungKhoan.Text = objHoiVIenTapThe.NamDuDkktCk;
                    txtThanhVienHangKTQT.Text = objHoiVIenTapThe.ThanhVienHangKtqt;
                    try
                    {
                        txtTongSoNguoiLamViec.Text = objHoiVIenTapThe.TongSoNguoiLamViec.ToString();
                    }
                    catch
                    {
                    }
                    try
                    {
                        txtTongSoNguoiCoCKKTV.Text = objHoiVIenTapThe.TongSoNguoiCoCkktv.ToString();

                    }
                    catch
                    {
                    }
                    try
                    {
                        txtTongSoKTVDKHN.Text = objHoiVIenTapThe.TongSoKtvdkhn.ToString();

                    }
                    catch
                    {
                    }
                    try
                    {
                        txtTongSoHoiVienCaNhan.Text = objHoiVIenTapThe.TongSoHoiVienCaNhan.ToString();

                    }
                    catch
                    {
                    }


                    // Thong tin nguoi dai dien theo PL
                    txtNguoiDaiDienPL_Ten.Text = objHoiVIenTapThe.NguoiDaiDienPlTen;
                    try
                    {
                        txtNguoiDaiDienPL_NgaySinh.Text = (objHoiVIenTapThe.NguoiDaiDienPlNgaySinh).Value.ToString("dd/MM/yyyy");

                    }
                    catch
                    {
                    }
                    drNguoiDaiDienPL_GioiTinh.SelectedValue = objHoiVIenTapThe.NguoiDaiDienPlGioiTinh;
                    try
                    {
                        drNguoiDaiDienPL_ChucVu.SelectedValue = objHoiVIenTapThe.NguoiDaiDienPlChucVuId.ToString();
                    }
                    catch
                    {
                    }
                    txtNguoiDaiDienPL_SoChungChiKTV.Text = objHoiVIenTapThe.NguoiDaiDienPlSoChungChiKtv;
                    try
                    {
                        txtNguoiDaiDienPL_NgayCapChungChiKTV.Text = (objHoiVIenTapThe.NguoiDaiDienPlNgayCapChungChiKtv).Value.ToString("dd/MM/yyyy");
                    }
                    catch
                    {
                    }

                    txtNguoiDaiDienPL_DiDong.Text = objHoiVIenTapThe.NguoiDaiDienPlDiDong;
                    txtNguoiDaiDienPL_Email.Text = objHoiVIenTapThe.NguoiDaiDienPlEmail;
                    txtNguoiDaiDienPL_DienThoaiCD.Text = objHoiVIenTapThe.NguoiDaiDienPlDienThoaiCd;

                    // Thong tin nguoi lien lac
                    txtNguoiDaiDienLL_Ten.Text = objHoiVIenTapThe.NguoiDaiDienLlTen;
                    try
                    {
                        txtNguoiDaiDienLL_NgaySinh.Text = (objHoiVIenTapThe.NguoiDaiDienLlNgaySinh).Value.ToString("dd/MM/yyyy");
                    }
                    catch
                    {
                    }

                    drNguoiDaiDienLL_GioiTinh.SelectedValue = objHoiVIenTapThe.NguoiDaiDienLlGioiTinh;
                    try
                    {
                        drNguoiDaiDienLL_ChucVu.SelectedValue = objHoiVIenTapThe.NguoiDaiDienLlChucVuId.ToString();
                    }
                    catch
                    {
                    }

                    txtNguoiDaiDienLL_DiDong.Text = objHoiVIenTapThe.NguoiDaiDienLlDiDong;
                    txtNguoiDaiDienLL_Email.Text = objHoiVIenTapThe.NguoiDaiDienLlEmail;
                    txtNguoiDaiDienLL_DienThoaiCD.Text = objHoiVIenTapThe.NguoiDaiDienLlDienThoaiCd;

                    //drCauHoi.SelectedValue = objHoiVIenTapThe.CauHoiBiMatID.ToString();
                    //txtCauTraLoi.Text = objHoiVIenTapThe.CauTraLoiBiMat;

                    hidId.Value = objHoiVIenTapThe.DonHoiVienTapTheId.ToString();
                    hidTrangthaiId.Value = objHoiVIenTapThe.TinhTrangId.ToString();
                    hidHinhThucNop.Value = objHoiVIenTapThe.HinhThucNop.Trim();


                    string strSoQD = "";
                    DateTime? deNgayQD = null;

                    try
                    {

                        DataSet ds = new DataSet();
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "select SoQuyetDinhKetNap, NgayQuyetDinhKetNap from tblHoiVienTapThe where HoiVienTapTheID = " + Convert.ToInt32(objHoiVIenTapThe.HoiVienTapTheId.Value);

                        ds = DataAccess.RunCMDGetDataSet(sql);
                        sql.Connection.Close();
                        sql.Connection.Dispose();
                        sql = null;

                        DataTable dt = ds.Tables[0];



                        //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
                        //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
                        int i = 0;
                        foreach (DataRow tem in dt.Rows)
                        {
                            strSoQD = tem["SoQuyetDinhKetNap"].ToString();
                            deNgayQD = Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]);
                        }
                    }
                    catch { }

                    txtSoQuyetDinh.Text = strSoQD;
                    if (deNgayQD != null)
                        NgayQD.Value = deNgayQD.Value.ToString("dd/MM/yyyy");
                }


            }
        }
        catch (Exception ex)
        {
            string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #000000;"">
            							<p>" + ex.ToString() + @"</p>
            							<i class=""fa fa-plus""></i>
            						</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }


    }
    protected void CapnhatTTHoiVien(int donHoiVienTtId)
    {


        try
        {
            // conn.Open();

            string thongbao = "";
            bool isEdit = (hidMode.Value == "edit" && donHoiVienTtId != null && donHoiVienTtId != 0);



            DataSet ds = new DataSet();

            SqlCommand sql = new SqlCommand();
            sql.Connection = new SqlConnection(connStr); ;





            SqlDonHoiVienTapTheProvider DonHoiVienTapThe_provider = new SqlDonHoiVienTapTheProvider(connStr, true, "");
            if (isEdit)
            {
                objHoiVIenTapThe = DonHoiVienTapThe_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
                objHoiVIenTapThe.TinhTrangId = trangthaiChoDuyetID;
            }
            else
            { //add
                objHoiVIenTapThe.TinhTrangId = trangthaiChoDuyetID;
                hidTrangthaiId.Value = trangthaiChoDuyetID.ToString();
                objHoiVIenTapThe.HinhThucNop = "2";// Nop truc tiep tai VacPa

                objHoiVIenTapThe.NgayNopDon = DateTime.Now;
                objHoiVIenTapThe.SoDonHoiVienTapThe = SinhSoDon();
                hidTrangthaiId.Value = objHoiVIenTapThe.TinhTrangId.ToString();
            }

            if (drHoiVienTapThe.SelectedValue != "0")
                objHoiVIenTapThe.HoiVienTapTheChaId = int.Parse(drHoiVienTapThe.SelectedValue);

            objHoiVIenTapThe.TenDoanhNghiep = txtTenDoanhNghiep.Text.Trim();

            objHoiVIenTapThe.DiaChi = txtDiaChi.Text.Trim();
            objHoiVIenTapThe.DiaChiTinhId = drTinhThanh.SelectedValue;
            objHoiVIenTapThe.DiaChiHuyenId = drQuanHuyen.SelectedValue;
            objHoiVIenTapThe.DiaChiXaId = drPhuongXa.SelectedValue;


            try
            {
                objHoiVIenTapThe.NgayThanhLap = DateTime.ParseExact(txtNgayThanhLap.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }

            objHoiVIenTapThe.DienThoai = txtDienThoai.Text.Trim();
            objHoiVIenTapThe.Email = txtEmail.Text.Trim();
            objHoiVIenTapThe.Fax = txtFax.Text.Trim();
            objHoiVIenTapThe.Website = txtWebsite.Text.Trim();
            objHoiVIenTapThe.SoGiayChungNhanDkkd = txtSoGiayChungNhanDKKD.Text.Trim();

            try
            {
                objHoiVIenTapThe.NgayCapGiayChungNhanDkkd = DateTime.ParseExact(txtNgayCapGiayChungNhanDKKD.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }

            objHoiVIenTapThe.SoGiayChungNhanKddvkt = txtSoGiayChungNhanKDDVKT.Text.Trim();
            try
            {
                objHoiVIenTapThe.NgayCapGiayChungNhanKddvkt = DateTime.ParseExact(txtNgayCapGiayChungNhanKDDVKT.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }


            objHoiVIenTapThe.MaSoThue = txtMaSoThue.Text.Trim();
            objHoiVIenTapThe.LinhVucHoatDongId = Convert.ToInt32(drLinhvucHoatDong.SelectedValue);

            objHoiVIenTapThe.NamDuDkktKhac = txtNamCoLoiIchCongChung.Text.Trim();
            objHoiVIenTapThe.NamDuDkktCk = txtNamTrongLinhVucChungKhoan.Text.Trim();
            objHoiVIenTapThe.ThanhVienHangKtqt = txtThanhVienHangKTQT.Text.Trim();
            try
            {
                objHoiVIenTapThe.TongSoNguoiLamViec = Convert.ToInt32(txtTongSoNguoiLamViec.Text.Trim());
            }
            catch
            {
            }
            try
            {
                objHoiVIenTapThe.TongSoNguoiCoCkktv = Convert.ToInt32(txtTongSoNguoiCoCKKTV.Text.Trim());
            }
            catch
            {
            }
            try
            {
                objHoiVIenTapThe.TongSoKtvdkhn = Convert.ToInt32(txtTongSoKTVDKHN.Text.Trim());
            }
            catch
            {
            }
            try
            {
                objHoiVIenTapThe.TongSoHoiVienCaNhan = Convert.ToInt32(txtTongSoHoiVienCaNhan.Text.Trim());
            }
            catch
            {
            }


            // Thong tin nguoi dai dien theo PL
            objHoiVIenTapThe.NguoiDaiDienPlTen = txtNguoiDaiDienPL_Ten.Text.Trim();
            try
            {
                objHoiVIenTapThe.NguoiDaiDienPlNgaySinh = DateTime.ParseExact(txtNguoiDaiDienPL_NgaySinh.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }
            objHoiVIenTapThe.NguoiDaiDienPlGioiTinh = drNguoiDaiDienPL_GioiTinh.SelectedValue;
            objHoiVIenTapThe.NguoiDaiDienPlChucVuId = Convert.ToInt32(drNguoiDaiDienPL_ChucVu.SelectedValue);
            objHoiVIenTapThe.NguoiDaiDienPlSoChungChiKtv = txtNguoiDaiDienPL_SoChungChiKTV.Text.Trim();
            try
            {
                objHoiVIenTapThe.NguoiDaiDienPlNgayCapChungChiKtv = DateTime.ParseExact(txtNguoiDaiDienPL_NgayCapChungChiKTV.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }

            objHoiVIenTapThe.NguoiDaiDienPlDiDong = txtNguoiDaiDienPL_DiDong.Text.Trim();
            objHoiVIenTapThe.NguoiDaiDienPlEmail = txtNguoiDaiDienPL_Email.Text.Trim();
            objHoiVIenTapThe.NguoiDaiDienPlDienThoaiCd = txtNguoiDaiDienPL_DienThoaiCD.Text.Trim();

            // Thong tin nguoi lien lac
            objHoiVIenTapThe.NguoiDaiDienLlTen = txtNguoiDaiDienLL_Ten.Text.Trim();
            try
            {
                objHoiVIenTapThe.NguoiDaiDienLlNgaySinh = DateTime.ParseExact(txtNguoiDaiDienLL_NgaySinh.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }
            objHoiVIenTapThe.NguoiDaiDienLlGioiTinh = drNguoiDaiDienLL_GioiTinh.SelectedValue;
            objHoiVIenTapThe.NguoiDaiDienLlChucVuId = Convert.ToInt32(drNguoiDaiDienLL_ChucVu.SelectedValue);


            objHoiVIenTapThe.NguoiDaiDienLlDiDong = txtNguoiDaiDienLL_DiDong.Text.Trim();
            objHoiVIenTapThe.NguoiDaiDienLlEmail = txtNguoiDaiDienLL_Email.Text.Trim();
            objHoiVIenTapThe.NguoiDaiDienLlDienThoaiCd = txtNguoiDaiDienLL_DienThoaiCD.Text.Trim();


            if (isEdit)
            {
                DonHoiVienTapThe_provider.Update(objHoiVIenTapThe);

            }
            else
            { //add
                DonHoiVienTapThe_provider.Insert(objHoiVIenTapThe);

            }



            // File dinh kem
            int newHoiVienTapTheId = 0;
            try
            {
                newHoiVienTapTheId = (int)objHoiVIenTapThe.DonHoiVienTapTheId;
            }
            catch { }
            CapnhatFile(newHoiVienTapTheId);
            loaduploadFileDinhKem(newHoiVienTapTheId);
            hidId.Value = newHoiVienTapTheId.ToString();
            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Cập nhật đơn xin kết nạp hội viên " + objHoiVIenTapThe.TenDoanhNghiep);
            if (!isEdit)
                Response.Redirect("admin.aspx?page=ketnaphoivientapthe_edit&id=" + newHoiVienTapTheId + "&act=edit&thongbao=1");
            else
            {
                thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

                placeMessage.Controls.Add(new LiteralControl(thongbao));
            }


        }
        catch (Exception ex)
        {
            string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + ex.ToString() + @"</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }


    }

    protected void CapnhatFile(int donHoiVienTtId)
    {
        if (donHoiVienTtId != null && donHoiVienTtId != 0)
        {
            SqlDonHoiVienFileProvider DonHoiVienTapThe_provider = new SqlDonHoiVienFileProvider(connStr, true, "");
            HttpPostedFile styledfileupload;
            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                FileUpload fileUp = (FileUpload)row.FindControl("FileUp");
                FileDinhKem1 = new DonHoiVienFile();
                if (fileUp != null && fileUp.FileName != "")
                {
                    int fileUploadId = 0;
                    int loaiFieldId = 0;
                    String sTempLoaiFileID = row.Cells[3].Text;
                    String sTempFileUploadID = row.Cells[4].Text;

                    if (!String.IsNullOrEmpty(sTempFileUploadID) && sTempFileUploadID != "0" && sTempFileUploadID != "&nbsp;")
                    {
                        fileUploadId = Convert.ToInt32(row.Cells[4].Text);

                    }
                    if (!String.IsNullOrEmpty(sTempLoaiFileID) && sTempLoaiFileID != "0" && sTempLoaiFileID != "&nbsp;")
                    {
                        loaiFieldId = Convert.ToInt32(row.Cells[3].Text);

                    }
                    string styledfileupload_name = "";
                    styledfileupload = fileUp.PostedFile;
                    if (styledfileupload.FileName != "")
                    {
                        styledfileupload_name = fileUp.FileName;

                        byte[] datainput = new byte[styledfileupload.ContentLength];
                        styledfileupload.InputStream.Read(datainput, 0, styledfileupload.ContentLength);
                        FileDinhKem1.FileId = fileUploadId;
                        FileDinhKem1.DonHoiVienTapTheId = donHoiVienTtId;
                        FileDinhKem1.LoaiGiayToId = loaiFieldId;
                        FileDinhKem1.FileDinhKem = datainput;
                        //FileDinhKem1.Tenfile = styledfileupload.FileName;
                        FileDinhKem1.TenFile = new System.IO.FileInfo(styledfileupload.FileName).Name;

                    }
                    if (FileDinhKem1.FileId == 0)
                    {
                        DonHoiVienTapThe_provider.Insert(FileDinhKem1);
                    }
                    else
                    {
                        DonHoiVienTapThe_provider.Update(FileDinhKem1);
                    }
                    //if (fileUploadId != 0)
                    //{

                    //    SqlCommand sql = new SqlCommand();
                    //    sql.CommandText = "DELETE FROM TBLFILEDINHKEM WHERE FILEDINHKEMID  =   " + fileUploadId + "  ";
                    //    DataSet ds = DataAccess.RunCMDGetDataSet(sql);
                    //    DataAccess.RunActionCmd(sql);

                    //    sql.Connection.Close();
                    //    sql.Connection.Dispose();
                    //    sql = null;

                    //}
                    //DonHoiVienTapThe_provider.Insert(FileDinhKem1);
                }
                else
                {

                }

            }




        }
    }

    protected void loaduploadFileDinhKem(int donHoiVienTtId)
    {
        try
        {


            SqlCommand sql = new SqlCommand();
            sql.Connection = new SqlConnection(connStr);


            if (donHoiVienTtId == 0)
            {
                sql.CommandText = " SELECT  LoaiGiayToID ,TenFile as TenBieuMau ,DoiTuong ,BatBuoc  , 0 as FileID  , ' ' as TenFileDinhKem " +
                                  " FROM tblDMLoaiGiayTo  " +
                                  " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuongHoiVienTapThe + "' order by tblDMLoaiGiayTo.TenFile   ";
            }
            else
            {
                sql.CommandText =
                "    SELECT   LoaiGiayToID ,TenFile as TenBieuMau  ,DoiTuong  ,BatBuoc , " +
                              "   ( SELECT TOP 1 ISNULL ( tblDonHoiVienFile.FileID,0)  FROM  tblDonHoiVienFile      WHERE  tblDonHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblDonHoiVienFile.DonHoiVienTapTheID= " + donHoiVienTtId + "  ) as FileID ," +
                               " ( SELECT  TOP 1 ISNULL(tblDonHoiVienFile.TenFile,' ')   FROM  tblDonHoiVienFile  WHERE  tblDonHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblDonHoiVienFile.DonHoiVienTapTheID= " + donHoiVienTtId + " ) as  TenFileDinhKem    " +
                " FROM tblDMLoaiGiayTo  " +
                " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuongHoiVienTapThe + "' order by tblDMLoaiGiayTo.TenFile  ";


            }



            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;


            PagedDataSource objPds1 = new PagedDataSource();
            objPds1.DataSource = ds.Tables[0].DefaultView;

            FileDinhKem_grv.DataSource = objPds1;
            FileDinhKem_grv.DataBind();
            //FileDinhKem_grv.Columns[3].Visible = false; 
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;

            // set validate

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                String sTenLoaiFile = row.Cells[0].Text.Trim();
                String sLoaiFileID = row.Cells[3].Text.Trim();
                String sFileDinhKemID = row.Cells[4].Text.Trim();
                String sBatbuoc = row.Cells[5].Text.Trim();

                String sTenFile = row.Cells[6].Text.Trim();

                // check validate
                if (sBatbuoc == "0" || !String.IsNullOrEmpty(sTenFile))
                {
                    //((RequiredFieldValidator)row.FindControl("RequiredFieldFileUpload")).Visible = false;


                }

                // an hien chu thich bat buoc
                if (sBatbuoc == "0")
                {
                    ((Label)row.FindControl("requred")).Visible = false;
                    //((Label)row.FindControl("requred")).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                }
                else
                {
                    //((Label)row.FindControl("requred")).Text = "(*) ";
                }


                // set link file
                HyperLink likFileUp = ((HyperLink)row.FindControl("linkFileUpload"));// 

                if (!String.IsNullOrEmpty(sFileDinhKemID) && sFileDinhKemID != "0")
                {
                    likFileUp.NavigateUrl = "Download.ashx?mode=don&AttachFileID=" + sFileDinhKemID;
                }
                else
                {
                    likFileUp.NavigateUrl = "";
                }

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }

    protected void loaduploadFileDinhKem()
    {
        try
        {


            SqlCommand sql = new SqlCommand();
            sql.Connection = new SqlConnection(connStr);



            sql.CommandText = " SELECT  LoaiGiayToID ,TenFile as TenBieuMau ,DoiTuong ,BatBuoc  , 0 as FileID  , ' ' as TenFileDinhKem " +
                              " FROM tblDMLoaiGiayTo  " +
                              " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuongCongTyKiemToan + "' order by tblDMLoaiGiayTo.TenFile   ";


            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;


            PagedDataSource objPds1 = new PagedDataSource();
            objPds1.DataSource = ds.Tables[0].DefaultView;

            FileDinhKem_grv.DataSource = objPds1;
            FileDinhKem_grv.DataBind();
            //FileDinhKem_grv.Columns[3].Visible = false; 
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;

            // set validate

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                String sTenLoaiFile = row.Cells[0].Text.Trim();
                String sLoaiFileID = row.Cells[3].Text.Trim();
                String sFileDinhKemID = row.Cells[4].Text.Trim();
                String sBatbuoc = row.Cells[5].Text.Trim();

                String sTenFile = row.Cells[6].Text.Trim();

                // check validate
                if (sBatbuoc == "0" || !String.IsNullOrEmpty(sTenFile))
                {
                    //((RequiredFieldValidator)row.FindControl("RequiredFieldFileUpload")).Visible = false;


                }

                // an hien chu thich bat buoc
                if (sBatbuoc == "0")
                {
                    ((Label)row.FindControl("requred")).Visible = false;
                    //((Label)row.FindControl("requred")).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                }
                else
                {
                    //((Label)row.FindControl("requred")).Text = "(*) ";
                }


                // set link file
                HyperLink likFileUp = ((HyperLink)row.FindControl("linkFileUpload"));// 

                if (!String.IsNullOrEmpty(sFileDinhKemID) && sFileDinhKemID != "0")
                {
                    likFileUp.NavigateUrl = "Download.ashx?AttachFileID=" + sFileDinhKemID;
                }
                else
                {
                    likFileUp.NavigateUrl = "";
                }

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }


    public string SinhSoDon()
    {
        string result = "";

        string sql = "SELECT dbo.LaySoChayDonHoiVienTapThe('" + DateTime.Now.Year.ToString().Substring(2, 2) + "')";
        SqlCommand cmd = new SqlCommand(sql);
        string so = DataAccess.DLookup(cmd);

        result = "DKHVTT" + DateTime.Now.Year.ToString().Substring(2, 2) + so;

        return result;
    }


    protected void setDisplayLyDoxoa()
    {


        if (hidTrangthaiId.Value == null ||
              hidTrangthaiId.Value == "" ||
              hidTrangthaiId.Value == trangthaiDaPheDuyet.ToString())  // Them moi , da duyet ==> ko hien thi
        {
            Response.Write(@" style=""display: none;"" ");
        }
        else // cac truong hop khac
        {
            Response.Write(@" ");

        }



    }

    public bool UpdateStatus(int DonHoiVienTapTheID, int TinhTrangID, String NguoiTiepNhan, String NguoiDuyet,
                             String NgayNopDon, String NgayTiepNhan, String NgayDuyet, String LyDoTuChoi)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("DonHoiVienTapTheID", DonHoiVienTapTheID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiTiepNhan", NguoiTiepNhan));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayNopDon", NgayNopDon));
            arrParams.Add(new DBHelpers.DataParameter("NgayTiepNhan", NgayTiepNhan));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));


            objDBHelper = ConnectionControllers.Connect("VACPA");
            return objDBHelper.ExecFunction("sp_donhoivien_tt_xetduyet", arrParams);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }
    public bool UpdateStatus(int DonHoiVienTapTheID, int TinhTrangID, String NguoiTiepNhan, String NguoiDuyet,
                                String NgayNopDon, String NgayTiepNhan, String NgayDuyet, String LyDoTuChoi, ref string TenDangNhap, ref  string MatKhau, ref string Email)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("LoaiHoiVienTapTheChiTiet", drLoaiHoiVien.SelectedValue));
            arrParams.Add(new DBHelpers.DataParameter("DonHoiVienTapTheID", DonHoiVienTapTheID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiTiepNhan", NguoiTiepNhan));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayNopDon", NgayNopDon));
            arrParams.Add(new DBHelpers.DataParameter("NgayTiepNhan", NgayTiepNhan));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));

            DBHelpers.DataParameter outTenDangNhap = new DBHelpers.DataParameter("TenDangNhap", TenDangNhap);
            outTenDangNhap.OutPut = true;
            arrParams.Add(outTenDangNhap);

            DBHelpers.DataParameter outMatKhau = new DBHelpers.DataParameter("MatKhau", MatKhau);
            outMatKhau.OutPut = true;
            arrParams.Add(outMatKhau);

            DBHelpers.DataParameter outEmail = new DBHelpers.DataParameter("Email", Email);
            outEmail.OutPut = true;
            arrParams.Add(outEmail);


            objDBHelper = ConnectionControllers.Connect("VACPA");
            bool bRedulte = objDBHelper.ExecFunction("sp_donhoivien_tt_xetduyet", arrParams);
            TenDangNhap = outTenDangNhap.Param_Value.ToString();
            MatKhau = outMatKhau.Param_Value.ToString();
            Email = outEmail.Param_Value.ToString();

            return bRedulte;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    protected void btnKetXuatWord_Click(object sender, EventArgs e)
    {

        string strID = Request.QueryString["id"];

        if (!string.IsNullOrEmpty(strID))
        {


            List<objDonXinGiaNhapHoiVienTapThe> lstBC = new List<objDonXinGiaNhapHoiVienTapThe>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = " select TenDoanhNghiep, TenVietTat, SoHieu, TenTiengAnh, DiaChi, "
                                + " DienThoai, Fax, Email, Website, SoGiayChungNhanDKKD, NgayCapGiayChungNhanDKKD, "
                                + " SoGiayChungNhanKDDVKT, NgayCapGiayChungNhanKDDVKT, LoaiHinhDoanhNghiepID, "
                                + " MaSoThue, TenLinhVucHoatDong, NamDuDKKT_CK,NamDuDKKT_Khac, ThanhVienHangKTQT, "
                                + " TongSoNguoiLamViec, TongSoNguoiCoCKKTV, TongSoKTVDKHN, TongSoHoiVienCaNhan, "
                                + " NguoiDaiDienPL_Ten, c.TenChucVu as cv1, NguoiDaiDienPL_GioiTinh, NguoiDaiDienPL_NgaySinh, "
                                + " NguoiDaiDienPL_SoChungChiKTV, NguoiDaiDienPL_NgayCapChungChiKTV, "
                                + " NguoiDaiDienPL_DienThoaiCD, NguoiDaiDienPL_DiDong, NguoiDaiDienPL_Email, "
                                + " NguoiDaiDienLL_Ten, d.TenChucVu as cv2, NguoiDaiDienLL_NgaySinh, NguoiDaiDienLL_GioiTinh, "
                                + " NguoiDaiDienLL_DienThoaiCD, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email "
                                + " from tblDonHoiVienTapThe a  "
                                + " inner join dbo.tblDMLinhVucHoatDong b on a.LinhVucHoatDongID = b.LinhVucHoatDongID "
                                + " left join dbo.tblDMChucVu c on a.NguoiDaiDienPL_ChucVuID = c.ChucVuID "
                                + " left join dbo.tblDMChucVu d on a.NguoiDaiDienLL_ChucVuID = d.ChucVuID "
                                + " where DonHoiVienTapTheID = " + strID;

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];



            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                objDonXinGiaNhapHoiVienTapThe new1 = new objDonXinGiaNhapHoiVienTapThe();

                new1.SoHieu = tem["SoHieu"].ToString();
                new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
                new1.TenVietTat = tem["TenVietTat"].ToString();
                new1.TenTiengAnh = tem["TenTiengAnh"].ToString();
                new1.DiaChi = tem["DiaChi"].ToString();
                new1.DienThoai = tem["DienThoai"].ToString();
                new1.Fax = tem["Fax"].ToString();
                new1.Email = tem["Email"].ToString();
                new1.Website = tem["Website"].ToString();
                new1.SoDK = tem["SoGiayChungNhanDKKD"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayCapGiayChungNhanDKKD"].ToString()))
                    new1.NgayDK = NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanDKKD"]));
                new1.SoGiayCN = tem["SoGiayChungNhanKDDVKT"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayCapGiayChungNhanKDDVKT"].ToString()))
                    new1.NgayCN = NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanKDDVKT"]));
                if (tem["LoaiHinhDoanhNghiepID"].ToString() == "7")
                {
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";
                    new1.LoaiHinhDoanhNghiep1 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "2")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";

                    new1.LoaiHinhDoanhNghiep2 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "5")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";

                    new1.LoaiHinhDoanhNghiep3 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "9")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";

                    new1.LoaiHinhDoanhNghiep5 = "X";
                }
                else
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";

                    new1.LoaiHinhDoanhNghiep4 = "X";
                }

                new1.MaSoThue = tem["MaSoThue"].ToString();
                new1.LinhVucHoatDong = tem["TenLinhVucHoatDong"].ToString();
                new1.CoLoiIchNam = tem["NamDuDKKT_CK"].ToString();
                new1.LinhVucCKNam = tem["NamDuDKKT_Khac"].ToString();
                new1.LaThanhVienCuaHangKiemToan = tem["ThanhVienHangKTQT"].ToString();
                new1.TongSoNgLamViecTaiCty = tem["TongSoNguoiLamViec"].ToString();
                new1.TongSoNgCCKTV = tem["TongSoNguoiCoCKKTV"].ToString();
                new1.TongSoKTVDKHN = tem["TongSoKTVDKHN"].ToString();
                new1.TongSoNgLaHoiVienCaNhan = tem["TongSoHoiVienCaNhan"].ToString();

                new1.NguoiDaiDien_Ten = tem["NguoiDaiDienPL_Ten"].ToString();
                new1.NguoiDaiDien_ChucVu = tem["cv1"].ToString();
                new1.NgaySinh = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgaySinh"])) : "";
                if (tem["NguoiDaiDienPL_GioiTinh"].ToString() == "1")
                {
                    new1.GioiTInhNu = ".";
                    new1.GioiTInhNam = "X";
                }
                else
                {
                    new1.GioiTInhNam = ".";
                    new1.GioiTInhNu = "X";
                }
                new1.NguoiDaiDien_SoCCKTV = tem["NguoiDaiDienPL_SoChungChiKTV"].ToString();
                new1.NguoiDaiDien_NgayCCKTV = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgayCapChungChiKTV"])) : "";
                new1.NguoiDaiDien_DienThoai = tem["NguoiDaiDienPL_DienThoaiCD"].ToString();
                new1.NguoiDaiDien_mobile = tem["NguoiDaiDienPL_DiDong"].ToString();
                new1.NguoiDaiDien_Email = tem["NguoiDaiDienPL_Email"].ToString();

                new1.NguoiDaiDienLL_Ten = tem["NguoiDaiDienLL_Ten"].ToString();
                new1.NguoiDaiDienLL_ChuVu = tem["cv2"].ToString();
                new1.NguoiDaiDienLL_NGaySinh = !string.IsNullOrEmpty(tem["NguoiDaiDienLL_NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienLL_NgaySinh"])) : "";
                if (tem["NguoiDaiDienLL_GioiTinh"].ToString() == "1")
                {
                    new1.NguoiDaiDienLL_GioiTinhNu = ".";
                    new1.NguoiDaiDienLL_GioiTinhNam = "X";
                }
                else
                {
                    new1.NguoiDaiDienLL_GioiTinhNam = ".";
                    new1.NguoiDaiDienLL_GioiTinhNu = "X";
                }
                new1.NguoiDaiDienLL_DienThoai = tem["NguoiDaiDienLL_DienThoaiCD"].ToString();
                new1.NguoiDaiDien_Email = tem["NguoiDaiDienPL_Email"].ToString();
                new1.NguoiDaiDienLL_mobile = tem["NguoiDaiDienLL_DiDong"].ToString();
                new1.NguoiDaiDienLL_Email = tem["NguoiDaiDienLL_Email"].ToString();
                new1.SoQD = tem["NguoiDaiDienPL_SoChungChiKTV"].ToString();
                new1.NgayKyQD = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgayCapChungChiKTV"])) : "";
                new1.NgayThangNam = "Hà nội, ngày " + (DateTime.Now.Day.ToString().Length == 1 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + " tháng " + (DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + " năm " + DateTime.Now.Year;
                lstBC.Add(new1);


            }

            //TenBaoCao newabc = new TenBaoCao();
            //newabc.TongCong = "Tổng cộng: " + i.ToString();
            //newabc.NgayTinh = "Tính từ " + NgayBatDau.Value.ToShortDateString() + " - " + NgayKetThuc.Value.ToShortDateString();
            //newabc.ngayBC = "Hà nội, ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            //lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/DonXinGiaNhapHoiVienTapThe.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objDonXinGiaNhapHoiVienTapThe"].SetDataSource(lstBC);
            //rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DonXinGiaNhapHoiVienTapThe");

        }
    }

    protected void btnKetXuatPDF_Click(object sender, EventArgs e)
    {
        string strID = Request.QueryString["id"];

        if (!string.IsNullOrEmpty(strID))
        {


            List<objDonXinGiaNhapHoiVienTapThe> lstBC = new List<objDonXinGiaNhapHoiVienTapThe>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = " select TenDoanhNghiep, TenVietTat, SoHieu, TenTiengAnh, DiaChi, "
                                + " DienThoai, Fax, Email, Website, SoGiayChungNhanDKKD, NgayCapGiayChungNhanDKKD, "
                                + " SoGiayChungNhanKDDVKT, NgayCapGiayChungNhanKDDVKT, LoaiHinhDoanhNghiepID, "
                                + " MaSoThue, TenLinhVucHoatDong, NamDuDKKT_CK,NamDuDKKT_Khac, ThanhVienHangKTQT, "
                                + " TongSoNguoiLamViec, TongSoNguoiCoCKKTV, TongSoKTVDKHN, TongSoHoiVienCaNhan, "
                                + " NguoiDaiDienPL_Ten, c.TenChucVu as cv1, NguoiDaiDienPL_GioiTinh, NguoiDaiDienPL_NgaySinh, "
                                + " NguoiDaiDienPL_SoChungChiKTV, NguoiDaiDienPL_NgayCapChungChiKTV, "
                                + " NguoiDaiDienPL_DienThoaiCD, NguoiDaiDienPL_DiDong, NguoiDaiDienPL_Email, "
                                + " NguoiDaiDienLL_Ten, d.TenChucVu as cv2, NguoiDaiDienLL_NgaySinh, NguoiDaiDienLL_GioiTinh, "
                                + " NguoiDaiDienLL_DienThoaiCD, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email "
                                + " from tblDonHoiVienTapThe a  "
                                + " inner join dbo.tblDMLinhVucHoatDong b on a.LinhVucHoatDongID = b.LinhVucHoatDongID "
                                + " left join dbo.tblDMChucVu c on a.NguoiDaiDienPL_ChucVuID = c.ChucVuID "
                                + " left join dbo.tblDMChucVu d on a.NguoiDaiDienLL_ChucVuID = d.ChucVuID "
                                + " where DonHoiVienTapTheID = " + strID;

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];



            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                objDonXinGiaNhapHoiVienTapThe new1 = new objDonXinGiaNhapHoiVienTapThe();

                new1.SoHieu = tem["SoHieu"].ToString();
                new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
                new1.TenVietTat = tem["TenVietTat"].ToString();
                new1.TenTiengAnh = tem["TenTiengAnh"].ToString();
                new1.DiaChi = tem["DiaChi"].ToString();
                new1.DienThoai = tem["DienThoai"].ToString();
                new1.Fax = tem["Fax"].ToString();
                new1.Email = tem["Email"].ToString();
                new1.Website = tem["Website"].ToString();
                new1.SoDK = tem["SoGiayChungNhanDKKD"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayCapGiayChungNhanDKKD"].ToString()))
                    new1.NgayDK = NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanDKKD"]));
                new1.SoGiayCN = tem["SoGiayChungNhanKDDVKT"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayCapGiayChungNhanKDDVKT"].ToString()))
                    new1.NgayCN = NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanKDDVKT"]));
                if (tem["LoaiHinhDoanhNghiepID"].ToString() == "7")
                {
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";
                    new1.LoaiHinhDoanhNghiep1 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "2")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";

                    new1.LoaiHinhDoanhNghiep2 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "5")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";

                    new1.LoaiHinhDoanhNghiep3 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "9")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";

                    new1.LoaiHinhDoanhNghiep5 = "X";
                }
                else
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";

                    new1.LoaiHinhDoanhNghiep4 = "X";
                }

                new1.MaSoThue = tem["MaSoThue"].ToString();
                new1.LinhVucHoatDong = tem["TenLinhVucHoatDong"].ToString();
                new1.CoLoiIchNam = tem["NamDuDKKT_CK"].ToString();
                new1.LinhVucCKNam = tem["NamDuDKKT_Khac"].ToString();
                new1.LaThanhVienCuaHangKiemToan = tem["ThanhVienHangKTQT"].ToString();
                new1.TongSoNgLamViecTaiCty = tem["TongSoNguoiLamViec"].ToString();
                new1.TongSoNgCCKTV = tem["TongSoNguoiCoCKKTV"].ToString();
                new1.TongSoKTVDKHN = tem["TongSoKTVDKHN"].ToString();
                new1.TongSoNgLaHoiVienCaNhan = tem["TongSoHoiVienCaNhan"].ToString();

                new1.NguoiDaiDien_Ten = tem["NguoiDaiDienPL_Ten"].ToString();
                new1.NguoiDaiDien_ChucVu = tem["cv1"].ToString();
                new1.NgaySinh = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgaySinh"])) : "";
                if (tem["NguoiDaiDienPL_GioiTinh"].ToString() == "1")
                {
                    new1.GioiTInhNu = ".";
                    new1.GioiTInhNam = "X";
                }
                else
                {
                    new1.GioiTInhNam = ".";
                    new1.GioiTInhNu = "X";
                }
                new1.NguoiDaiDien_SoCCKTV = tem["NguoiDaiDienPL_SoChungChiKTV"].ToString();
                new1.NguoiDaiDien_NgayCCKTV = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgayCapChungChiKTV"])) : "";
                new1.NguoiDaiDien_DienThoai = tem["NguoiDaiDienPL_DienThoaiCD"].ToString();
                new1.NguoiDaiDien_mobile = tem["NguoiDaiDienPL_DiDong"].ToString();
                new1.NguoiDaiDien_Email = tem["NguoiDaiDienPL_Email"].ToString();

                new1.NguoiDaiDienLL_Ten = tem["NguoiDaiDienLL_Ten"].ToString();
                new1.NguoiDaiDienLL_ChuVu = tem["cv2"].ToString();
                new1.NguoiDaiDienLL_NGaySinh = !string.IsNullOrEmpty(tem["NguoiDaiDienLL_NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienLL_NgaySinh"])) : "";
                if (tem["NguoiDaiDienLL_GioiTinh"].ToString() == "1")
                {
                    new1.NguoiDaiDienLL_GioiTinhNu = ".";
                    new1.NguoiDaiDienLL_GioiTinhNam = "X";
                }
                else
                {
                    new1.NguoiDaiDienLL_GioiTinhNam = ".";
                    new1.NguoiDaiDienLL_GioiTinhNu = "X";
                }
                new1.NguoiDaiDienLL_DienThoai = tem["NguoiDaiDienLL_DienThoaiCD"].ToString();
                new1.NguoiDaiDien_Email = tem["NguoiDaiDienPL_Email"].ToString();
                new1.NguoiDaiDienLL_mobile = tem["NguoiDaiDienLL_DiDong"].ToString();
                new1.NguoiDaiDienLL_Email = tem["NguoiDaiDienLL_Email"].ToString();
                new1.SoQD = tem["NguoiDaiDienPL_SoChungChiKTV"].ToString();
                new1.NgayKyQD = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgayCapChungChiKTV"])) : "";
                new1.NgayThangNam = "Hà nội, ngày " + (DateTime.Now.Day.ToString().Length == 1 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + " tháng " + (DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + " năm " + DateTime.Now.Year;
                lstBC.Add(new1);


            }

            //TenBaoCao newabc = new TenBaoCao();
            //newabc.TongCong = "Tổng cộng: " + i.ToString();
            //newabc.NgayTinh = "Tính từ " + NgayBatDau.Value.ToShortDateString() + " - " + NgayKetThuc.Value.ToShortDateString();
            //newabc.ngayBC = "Hà nội, ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            //lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/DonXinGiaNhapHoiVienTapThe.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objDonXinGiaNhapHoiVienTapThe"].SetDataSource(lstBC);
            //rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DonXinGiaNhapHoiVienTapThe");
        }
    }

    protected void btnKetXuat_Click(object sender, EventArgs e)
    {
        string strid = Request.QueryString["id"];
        if (!string.IsNullOrEmpty(strid))
        {

            List<objThongTinDoanhNghiep_CT> lst = new List<objThongTinDoanhNghiep_CT>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep, TenTiengAnh, SoQuyetDinhKetNap "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE HoiVienTapTheID = '" + strid + "'";
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            List<Anh_Tr> lst_Anh = new List<Anh_Tr>();

            foreach (DataRow tem in dt.Rows)
            {
                objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();

                new1.TenDoanhNghiep = ConvertToTCVN3(tem["TenDoanhNghiep"].ToString().Replace("\n", ""));

                new1.TenTiengAnh = tem["TenTiengAnh"].ToString() + " is a full Member of VACPA";
                new1.SoQD = ConvertToTCVN3(tem["SoQuyetDinhKetNap"].ToString());

                lst.Add(new1);

            }

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/GiayChungNhanHoiVienTapThe.rpt"));

            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            rpt.Database.Tables["Anh_Tr"].SetDataSource(lst_Anh);

            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "GiayChungNhanHoiVienTapThe");

        }
    }

    public static string ConvertToTCVN3(string value)
    {
        string text = "";
        int i = 0;
        while (i < value.Length)
        {
            char value2 = char.Parse(value.Substring(i, 1));
            short num = System.Convert.ToInt16(value2);
            if (num <= 259)
            {
                if (num <= 202)
                {
                    if (num != 194)
                    {
                        switch (num)
                        {
                            case 201: text += "Ð"; break;
                            case 202: text += "ü"; break;
                            default: goto IL_7AF;
                        }
                    }
                    else text += "¢";
                }
                else
                {
                    if (num != 212)
                    {
                        switch (num)
                        {
                            case 224: text += "µ"; break;
                            case 225: text += "¸"; break;
                            case 226: text += "©"; break;
                            case 227: text += "·"; break;
                            case 228:
                            case 229:
                            case 230:
                            case 231:
                            case 235:
                            case 238:
                            case 239:
                            case 240:
                            case 241:
                            case 246:
                            case 247:
                            case 248:
                            case 251:
                            case 252: goto IL_7AF;
                            case 232: text += "Ì"; break;
                            case 233: text += "Ð"; break;
                            case 234: text += "ª"; break;
                            case 236: text += "ỡ"; break;
                            case 237: text += "Ý"; break;
                            case 242: text += "ß"; break;
                            case 243: text += "ã"; break;
                            case 244: text += "«"; break;
                            case 245: text += "ừ"; break;
                            case 249: text += "ự"; break;
                            case 250: text += "ư"; break;
                            case 253: text += "ý"; break;
                            default:
                                switch (num)
                                {
                                    case 258: text += "¡"; break;
                                    case 259: text += "¨"; break;
                                    default: goto IL_7AF;
                                }
                                break;
                        }
                    }
                    else text += "¤";
                }
            }
            else
            {
                if (num <= 361)
                {
                    switch (num)
                    {
                        case 272: text += "§"; break;
                        case 273: text += "®"; break;
                        default:
                            if (num != 297)
                            {
                                if (num != 361) goto IL_7AF;
                                text += "ò";
                            }
                            else text += "Ü";
                            break;
                    }
                }
                else
                {
                    switch (num)
                    {
                        case 416: text += "¥"; break;
                        case 417: text += "¬"; break;
                        default:
                            if (num != 431)
                            {
                                switch (num)
                                {
                                    case 7841: text += "¹"; break;
                                    case 7842:
                                    case 7844:
                                    case 7846:
                                    case 7848:
                                    case 7850:
                                    case 7852:
                                    case 7854:
                                    case 7856:
                                    case 7858:
                                    case 7860:
                                    case 7862:
                                    case 7864:
                                    case 7866:
                                    case 7868:
                                    case 7870:
                                    case 7872:
                                    case 7874:
                                    case 7876:
                                    case 7878:
                                    case 7880:
                                    case 7882:
                                    case 7884:
                                    case 7886:
                                    case 7888:
                                    case 7890:
                                    case 7892:
                                    case 7894:
                                    case 7896:
                                    case 7898:
                                    case 7900:
                                    case 7902:
                                    case 7904:
                                    case 7906:
                                    case 7908:
                                    case 7910:
                                    case 7912:
                                    case 7914:
                                    case 7916:
                                    case 7918:
                                    case 7920:
                                    case 7922:
                                    case 7924:
                                    case 7926:
                                    case 7928: goto IL_7AF;
                                    case 7843: text += "¶"; break;
                                    case 7845: text += "Ê"; break;
                                    case 7847: text += "Ç"; break;
                                    case 7849: text += "È"; break;
                                    case 7851: text += "É"; break;
                                    case 7853: text += "Ë"; break;
                                    case 7855: text += "¾"; break;
                                    case 7857: text += "»"; break;
                                    case 7859: text += "¼"; break;
                                    case 7861: text += "½"; break;
                                    case 7863: text += "Æ"; break;
                                    case 7865: text += "Ñ"; break;
                                    case 7867: text += "Î"; break;
                                    case 7869: text += "Ï"; break;
                                    case 7871: text += "Õ"; break;
                                    case 7873: text += "Ò"; break;
                                    case 7875: text += "Ó"; break;
                                    case 7877: text += "Ô"; break;
                                    case 7879: text += "Ö"; break;
                                    case 7881: text += "Ø"; break;
                                    case 7883: text += "Þ"; break;
                                    case 7885: text += "ä"; break;
                                    case 7887: text += "á"; break;
                                    case 7889: text += "è"; break;
                                    case 7891: text += "å"; break;
                                    case 7893: text += "æ"; break;
                                    case 7895: text += "ç"; break;
                                    case 7897: text += "é"; break;
                                    case 7899: text += "í"; break;
                                    case 7901: text += "ê"; break;
                                    case 7903: text += "ë"; break;
                                    case 7905: text += "ì"; break;
                                    case 7907: text += "î"; break;
                                    case 7909: text += "ô"; break;
                                    case 7911: text += "ñ"; break;
                                    case 7913: text += "ø"; break;
                                    case 7915: text += "õ"; break;
                                    case 7917: text += "ö"; break;
                                    case 7919: text += "÷"; break;
                                    case 7921: text += "ù"; break;
                                    case 7923: text += "ú"; break;
                                    case 7925: text += "þ"; break;
                                    case 7927: text += "û"; break;
                                    case 7929: text += "ü"; break;
                                    default: goto IL_7AF;
                                }
                            }
                            else text += "¦";
                            break;
                    }
                }
            }
        IL_7BF:
            i++;
            continue;
        IL_7AF:
            text += value2.ToString();
            goto IL_7BF;
        }
        return text;
    }

    protected void btnInQuyetDinh_Click(object sender, EventArgs e)
    {
        setdataCrystalReport();
    }

    public void setdataCrystalReport()
    {

        DateTime? NgayQuyetDinh = null;

        string SoQD = "";

        string result = "";
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaHoiVienTapThe FROM tblHoiVienTapThe WHERE HoiVienTapTheID = (SELECT HoiVienTapTheID FROM tblDonHoiVienTapThe WHERE DonHoiVienTapTheID = " + Request.QueryString["id"] + ")";
        result = DataAccess.DLookup(sql);
        if (!string.IsNullOrEmpty(result))
        {
            List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
            List<objThongTinDoanhNghiep_CT> lst = new List<objThongTinDoanhNghiep_CT>();


            DataSet ds = new DataSet();
            sql = new SqlCommand();
            sql.CommandText = "select SoQuyetDinhKetNap, SoHieu, TenDoanhNghiep, TenVietTat, ngayquyetdinhketnap from tblHoiVienTapThe "
                                + " where MaHoiVienTapThe = '" + result + "'";


            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();
                new1.STT = i.ToString();
                new1.SoHieu = tem["SoHieu"].ToString();
                new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
                new1.TenVietTat = tem["TenVietTat"].ToString();
                SoQD = tem["SoQuyetDinhKetNap"].ToString();
                lst.Add(new1);

                if (!string.IsNullOrEmpty(tem["ngayquyetdinhketnap"].ToString()))
                    NgayQuyetDinh = Convert.ToDateTime(tem["ngayquyetdinhketnap"]);
            }

            TenBaoCao newabc = new TenBaoCao();
            newabc.SoLuong = i.ToString();
            newabc.TenBC = SoQD;
            newabc.TongCong = "Tổng cộng: " + i.ToString() + " Hội viên tổ chức";
            try
            {
                newabc.NgayTinh = (NgayQuyetDinh.Value.Day.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Day : NgayQuyetDinh.Value.Day.ToString()) + "/" + (NgayQuyetDinh.Value.Month.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Month : NgayQuyetDinh.Value.Month.ToString()) + "/" + NgayQuyetDinh.Value.Year;
                newabc.ngayBC = "Hà nội, ngày " + (NgayQuyetDinh.Value.Day.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Day : NgayQuyetDinh.Value.Day.ToString()) + " tháng " + (NgayQuyetDinh.Value.Month.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Month : NgayQuyetDinh.Value.Month.ToString()) + " năm " + NgayQuyetDinh.Value.Year;
            }
            catch { }
            lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/QuyetDinhKetNapHoiVienTapThe.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "QuyetDinhKetNapHoiVienTapThe");

        }
    }
}