﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="guithongbaoxoaten.ascx.cs"
    Inherits="usercontrols_QLHV_guithongbaoxoaten" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style>
    .ui-datepicker
    {
        z-index: 99999999 !important;
    }
</style>
<form id="Form1" runat="server">
<asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
<input type="hidden" id="lsID" name="lsID" runat="server" clientidmode="Static" />
<div>
    <div class="dataTables_length">
        <asp:LinkButton ID="btnGuiThongBao" runat="server" CssClass="btn btn-rounded" ValidationGroup="CheckFileExt" 
        ForeColor="Black" onclick="btnGuiThongBao_Click"><i class="iconfa-plus-sign"></i>&nbsp;Gửi thông báo</asp:LinkButton>
    </div>
</div>
<h4 class="widgettitle">
    Thông tin văn bản</h4>
<table id="Table1" width="100%" border="0" class="formtbl" runat="server">
    <tr>
        <td>
            <asp:Label ID="Label6" runat="server" Text='Số quyết định:'></asp:Label>
            <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtSoQuyetDinh" runat="server" Width="100%"></asp:TextBox>
            <asp:RequiredFieldValidator Display="Dynamic" ID="reqSoQuyetDinh" runat="server"
                ControlToValidate="txtSoQuyetDinh" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."
                ValidationGroup="CheckFileExt"></asp:RequiredFieldValidator>
        </td>
        <td>
            <asp:Label ID="Label32" runat="server" Text='Ngày ra quyết định:'></asp:Label>
            <asp:Label ID="Label33" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td colspan="2">
            <input name="NgayQuyetDinh" type="text" id="NgayQuyetDinh" runat="server" clientidmode="Static" />
            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server"
                ControlToValidate="NgayQuyetDinh" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."
                ValidationGroup="CheckFileExt"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text='File đính kèm:'></asp:Label>
        </td>
        <td colspan="4">
            <asp:FileUpload ID="fileDinhKem" runat="server" />
            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server"
                ControlToValidate="fileDinhKem" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."
                ValidationGroup="CheckFileExt"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label8" runat="server" Text='Ghi chú:'></asp:Label>
        </td>
        <td colspan="4">
            <asp:TextBox ID="txtGhiChu" runat="server" Width="100%"></asp:TextBox>
        </td>
    </tr>
</table>
<asp:UpdatePanel ID="up1" runat="server"><ContentTemplate>
<h4 class="widgettitle">
    Danh sách hội viên nhận thông báo</h4>
<div class="dataTables_length">
    <asp:LinkButton ID="btnLamMoi" runat="server" CssClass="btn btn-rounded" CausesValidation="false" 
        ForeColor="Black" onclick="btnLamMoi_Click"><i class="iconfa-refresh"></i>&nbsp;Làm mới</asp:LinkButton> <a id="A2" href="#none" class="btn btn-rounded"
            onclick="delete_user(Users_grv_selected);"><i class="iconfa-plus-sign">
            </i>Xóa</a>
</div>
<asp:GridView ClientIDMode="Static" ID="Users_grv" runat="server" AutoGenerateColumns="False" DataKeyNames="XoaTenHoiVienID"
    class="table table-bordered responsive dyntable" AllowSorting="True" 
    OnSorting="Users_grv_Sorting" onrowdatabound="Users_grv_RowDataBound">
    <Columns>
        <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
            ItemStyle-Width="30px">
            <ItemTemplate>
                <input type="checkbox" class="colcheckbox" value="<%# Eval("XoaTenHoiVienID")%>" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="MaHoiVien" HeaderText="ID hội viên" SortExpression="MaHoiVien" />
        <asp:BoundField DataField="HoVaTen" HeaderText="Tên hội viên" SortExpression="HoVaTen" />
        <asp:BoundField DataField="LoaiHoiVien" HeaderText="Loại hội viên" SortExpression="LoaiHoiVien" />
        <asp:BoundField DataField="LyDoXoaTen" HeaderText="Lý do xóa tên" SortExpression="LyDoXoaTen" />
        <asp:BoundField DataField="Email" HeaderText="Địa chỉ email" SortExpression="Email" />
        <asp:TemplateField>
            <ItemTemplate>
                <a href="#" id='<%# Eval("XoaTenHoiVienID") %>' class="deletable"><i class="iconsweets-trashcan">
                </i></a>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</ContentTemplate></asp:UpdatePanel>
</form>
<script type="text/javascript">

<% annut(); %>
    // Array ID được check
    var Users_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      
        $("a.deletable").click(function (e) {
                //alert($(this).attr('id'));
                $('#lsID').val($('#lsID').val().replace($(this).attr('id'), ""));
                var row = $(this).parent("td").parent('tr'); 
                row.remove();              
            });

        $("#Users_grv .checkall").bind("click", function () {
            Users_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#Users_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });

        $('#Users_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(Users_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa hội viên
    function delete_user(idxoa) {
        
        for (i = 0; i < idxoa.length; i++) {
            $('#' + idxoa[i]).remove();
            $('#lsID').val($('#lsID').val().replace(idxoa[i], ""));
        }
    }


    
    // Hiển thị tooltip
    if (jQuery('#Users_grv').length > 0) jQuery('#Users_grv').tooltip({ selector: "a[rel=tooltip]" });


</script>
<div id="div_user_add">
</div>
<script language="javascript">


    $.datepicker.setDefaults($.datepicker.regional['vi']);


    $(function () {
        $("#NgayQuyetDinh").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            maxDate: '0'

        });


    });


</script>
