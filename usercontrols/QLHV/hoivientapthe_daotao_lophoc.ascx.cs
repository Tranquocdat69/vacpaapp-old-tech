﻿using HiPT.VACPA.DL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_QLHV_hoivientapthe_daotao_lophoc : System.Web.UI.UserControl
{
    String id = String.Empty;
    public Commons cm = new Commons();
    public HoiVienTapTheChiNhanh objHoiVIenTapTheChiNhanh = new HoiVienTapTheChiNhanh();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    int HoiVienTapTheID = 0;
    int LopHocID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        gvChuyenDe.Columns[0].HeaderText = "STT";
        gvChuyenDe.Columns[1].HeaderText = "Tên chuyên đề";
        gvChuyenDe.Columns[2].HeaderText = "Giảng viên";

        gvLopHoc.Columns[0].HeaderText = "STT";
        gvLopHoc.Columns[1].HeaderText = "Họ tên";
        gvLopHoc.Columns[2].HeaderText = "Hội viên";
        gvLopHoc.Columns[3].HeaderText = "Số CCKTV";
        gvLopHoc.Columns[4].HeaderText = "Ngày cấp";
        gvLopHoc.Columns[5].HeaderText = "Ngày học";
        gvLopHoc.Columns[6].HeaderText = "KT, KiT";
        gvLopHoc.Columns[7].HeaderText = "Đạo đức";
        gvLopHoc.Columns[8].HeaderText = "Khác";
        gvLopHoc.Columns[9].HeaderText = "Tổng";

        if (!IsPostBack)
        {
            hidendHoiVienTapTheID.Value = Request.QueryString["id"];
            hidendLopHocID.Value = Request.QueryString["LopHocID"];
            try
            {
                HoiVienTapTheID = Convert.ToInt32(hidendHoiVienTapTheID.Value);
            }
            catch { }
            try
            {
                LopHocID = Convert.ToInt32(hidendLopHocID.Value);
            }
            catch { }
            LoadCongTy(HoiVienTapTheID);
            LoadLopHoc(HoiVienTapTheID, LopHocID);
            LoadLopHoc_HocVien(HoiVienTapTheID, LopHocID);


        }
    }
    public void LoadCongTy(int HoiVienTapTheID)
    {

        HoiVienTapThe HoiVien = new HoiVienTapThe();
        SqlHoiVienTapTheProvider provide_HoiVienTapThe = new SqlHoiVienTapTheProvider(connStr, true, "");

        HoiVien = provide_HoiVienTapThe.GetByHoiVienTapTheId(HoiVienTapTheID);
        txtTenCongTy.Text = HoiVien.TenDoanhNghiep;

    }


    public void LoadLopHoc(int HoiVienTapTheID, int LopHocID)
    {


        SqlCommand sql = new SqlCommand();
        sql.Connection = new SqlConnection(connStr);


        sql.CommandText = " SELECT  ROW_NUMBER() OVER (ORDER BY   tblCNKTLopHocChuyenDe.TuNgay, tblCNKTLopHocChuyenDe.MaChuyenDe   ) AS STT, tblCNKTLopHoc.LopHocID, MaLopHoc ,TenLopHoc ,  tblCNKTLopHocChuyenDe.MaChuyenDe MaChuyenDe ,  " +
                          "   tblCNKTLopHocChuyenDe.TenChuyenDe , tblCNKTLopHocChuyenDe.SoBuoi , tblCNKTLopHocChuyenDe.GiangVienID ,  " +
                          "   tblDMGiangVien.GioiTinh,  tblDMGiangVien.TenGiangVien ,tblDMTrinhDoChuyenMon.TenTrinhDoChuyenMon , tblDMGiangVien.ChungChi, tblDMChucVu.TenChucVu, tblDMGiangVien.DonViCongTac,  " +
                          "   CONVERT(nvarchar(10),tblCNKTLopHocChuyenDe.TuNgay , 103) as TuNgay ,   CONVERT(nvarchar(10),tblCNKTLopHocChuyenDe.DenNgay , 103) as DenNgay      " +
                          "       FROM tblCNKTLopHoc   " +
                          "      LEFT JOIN  tblCNKTLopHocChuyenDe ON tblCNKTLopHocChuyenDe.LopHocID= tblCNKTLopHoc.LopHocID  " +
                          "      LEFT JOIN tblDMGiangVien On tblDMGiangVien.GiangVienID =  tblCNKTLopHocChuyenDe.GiangVienID   " +
                          "      LEFT JOIN  tblDMTrinhDoChuyenMon on tblDMTrinhDoChuyenMon.TrinhDoChuyenMonID= tblDMGiangVien.TrinhDoChuyenMonID  " +
                          "      LEFT JOIN  tblDMChucVu on tblDMChucVu.ChucVuID= tblDMGiangVien.ChucVuID  " +
                          "   WHERE   tblCNKTLopHoc.LopHocID=" + LopHocID;



        String sGiangVien = "";
        String GioiTinh = "";
        String TenGiangVien = "";
        String TenTrinhDoChuyenMon = "";
        String ChungChi = "";
        String TenChucVu = "";
        String DonViCongTac = "";

        DataSet ds = DataAccess.RunCMDGetDataSet(sql);

        DataTable dtb = new DataTable();
        dtb.Columns.Add("TenChuyenDe", typeof(string));
        dtb.Columns.Add("GiangVien", typeof(string));

        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                sGiangVien = ""; GioiTinh = ""; TenGiangVien = ""; TenTrinhDoChuyenMon = "";
                ChungChi = ""; TenChucVu = ""; DonViCongTac = "";

                txtTenLopHoc.Text = ds.Tables[0].Rows[i]["TenLopHoc"] == null ? "" :
                                   ds.Tables[0].Rows[i]["TenLopHoc"].ToString();
                string tenchuyende = ds.Tables[0].Rows[i]["TenChuyenDe"] == null ? "" :
                                  ds.Tables[0].Rows[i]["TenChuyenDe"].ToString();
                GioiTinh = ds.Tables[0].Rows[i]["GioiTinh"] == null ? "" :
                                  ds.Tables[0].Rows[i]["GioiTinh"].ToString();
                TenGiangVien = ds.Tables[0].Rows[i]["TenGiangVien"] == null ? "" :
                                  ds.Tables[0].Rows[i]["TenGiangVien"].ToString();
                TenTrinhDoChuyenMon = ds.Tables[0].Rows[i]["TenTrinhDoChuyenMon"] == null ? "" :
                                  ds.Tables[0].Rows[i]["TenTrinhDoChuyenMon"].ToString();
                ChungChi = ds.Tables[0].Rows[i]["ChungChi"] == null ? "" :
                                  ds.Tables[0].Rows[i]["ChungChi"].ToString();
                TenChucVu = ds.Tables[0].Rows[i]["TenChucVu"] == null ? "" :
                                  ds.Tables[0].Rows[i]["TenChucVu"].ToString();
                DonViCongTac = ds.Tables[0].Rows[i]["DonViCongTac"] == null ? "" :
                                  ds.Tables[0].Rows[i]["DonViCongTac"].ToString();
                sGiangVien = (GioiTinh == "M" ? "Ông" + " " : "Bà" + " ") + TenGiangVien +
                             (TenTrinhDoChuyenMon == "" ? "" : ", " + TenTrinhDoChuyenMon) +
                             (ChungChi == "" ? "" : ", " + ChungChi) +
                             (TenChucVu == "" ? "" : ", " + TenChucVu) +
                             (DonViCongTac == "" ? "" : " - " + DonViCongTac);
                dtb.Rows.Add(tenchuyende, sGiangVien);

            }

        }

        gvChuyenDe.DataSource = dtb;
        gvChuyenDe.DataBind();


        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds = null;


    }

    public void LoadLopHoc_HocVien(int HoiVienTapTheID, int LopHocID)
    {




        DataSet ds = SelectDataSetLopHocCNKT(LopHocID.ToString());
        DataView dt = ds.Tables[0].DefaultView;


        PagedDataSource objPds1 = new PagedDataSource();
        objPds1.DataSource = ds.Tables[0].DefaultView;

        gvLopHoc.DataSource = objPds1;
        gvLopHoc.DataBind();

        txtSoLuongHocVien.Text = gvLopHoc.Rows.Count.ToString();

        ds = null;
        dt = null;

    }

    public DataSet SelectDataSetLopHocCNKT(String LopHocID)
    {

        DBHelpers.DBHelper objDBHelper = null;


        try
        {
            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("LopHocID", LopHocID));
            arrParams.Add(new DBHelpers.DataParameter("HoiVienTapTheID", Request.QueryString["id"]));
            objDBHelper = DBHelpers.ConnectionControllers.ConnectByConnectionString(0, connStr);

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_lophoc_cnkt", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            DBHelpers.ConnectionControllers.DisConnect(objDBHelper);
        }


    }
}