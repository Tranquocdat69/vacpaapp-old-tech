﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="hosohoiviencanhan.ascx.cs"
    Inherits="usercontrols_hosohoiviencanhan" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<script type="text/javascript" src="js/jquery.jgrowl.js"></script>
<script type="text/javascript" src="js/elements.js"></script>
<style>
    .require
    {
        color: Red;
    }
    .error
    {
        color: Red;
        font-size: 11px;
    }
    .Hide
    {
        display: none;
    }
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript" src="js/chosen.jquery.min.js"></script>
<script type="text/javascript">


    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });

    function reCalljScript() {


        $('.date2').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            yearRange: "-75:+25"
        });

        $('.date2').mask("99/99/9999");

        function getme() {
            $("[id$=txtDate]").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                yearRange: "-75:+25"
            });
            $("[id$=txtDate]").mask("99/99/9999");

            $("[id$=txtNgayThang]").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                yearRange: "-75:+25"
            });
            $("[id$=txtNgayThang]").mask("99/99/9999");
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        }
    }

    function EmailValidation(sender, args) {

        var email = document.getElementById('<%=txtEmail.ClientID%>');

        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email.value)) {
            args.IsValid = false;
        }
        else
            args.IsValid = true;


    }

</script>
 <form method="post" enctype="multipart/form-data" name="form_nhaphoso" id="form_nhaphoso"
        runat="server" clientidmode="Static">
 <div class="tabbedwidget tab-primary">
                            <ul>
                                <li id="li1"><a href="#tab1">Profile cá nhân</a></li>
                                <li id="li2"><a href="#tab2">Đào tạo cập nhật kiến thức</a></li>
                                <li id="li3"><a href="#tab3">Kiểm soát chất lượng</a></li>
                                <li id="li4"><a href="#tab4">Thanh toán</a></li>
                            </ul>
                            <div id="tab1">
                                <asp:PlaceHolder ID="placeMessage" runat="server"></asp:PlaceHolder>
<div class="widgetbox">
    <h4 class="widgettitle">
        <asp:Label ID="lbTitle" runat="server" Text="Cập nhật hồ sơ hội viên cá nhân"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4>
    <div class="widgetcontent">
       
        <div id="thongbaoloi_form_nhaphoso" name="thongbaoloi_form_nhaphoso" style="display: none"
            class="alert alert-error">
        </div>
        <asp:ScriptManager ID="s1" runat="server">
        </asp:ScriptManager>
        <table id="Table1" width="100%" border="0" class="formtbl" runat="server">
        <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Thông tin tài khoản</h6>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <label>Tên đăng nhập:</label>
                    
                </td>
                <td>
                    <asp:TextBox ID="txtTenDangNhap" runat="server" Width="100%"></asp:TextBox>
                    
                </td>
                <td colspan="3"><asp:CheckBox ID="chkHVCN" runat="server" Text="Chuyển thành hội viên cá nhân" TextAlign="Right" Visible="false" /><asp:CheckBox ID="chkKTV" runat="server" Text="Chuyển thành kiểm toán viên" TextAlign="Right" Visible="false" /></td>
            </tr>
            <tr id="trLoaiHoiVien" runat="server">
                <td>
                    <label>Đối tượng:</label>
                    
                </td>
                <td colspan="4">
                    <asp:DropDownList ID="drLoaiHoiVien" runat="server" Width="100%" Height="30px">
                        <asp:ListItem Text="<< Chọn >>" Value="-1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="- Hội viên chính thức" Value="-2"></asp:ListItem>           
                        <asp:ListItem Text="++ Hội viên chính thức" Value="0"></asp:ListItem>                        
                        <asp:ListItem Text="++ Hội viên cao cấp" Value="2"></asp:ListItem>                       
                        <asp:ListItem Text="++ Hội viên làm việc chuyên trách tại các Văn phòng VACPA" Value="4"></asp:ListItem>
                        <asp:ListItem Text="++ Hội viên đã nghỉ hưu và không làm việc tại một công ty nào khác" Value="5"></asp:ListItem>
                        <asp:ListItem Text="++ Hội viên đang làm việc tại cơ quan quản lý Nhà nước có liên quan đến nghề nghiệp, hoạt động của VACPA" Value="6"></asp:ListItem>

                        <asp:ListItem Text="- Hội viên liên kết" Value="1"></asp:ListItem>
                        <asp:ListItem Text="- Hội viên danh dự" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                
            </tr>
            <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Thông tin cá nhân</h6>
                    <asp:Label ID="lbHoiVienCaNhanID" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td>
                    <label>Họ và tên đệm:</label>
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtHoDem" runat="server" Width="100%"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="reqHoDem" runat="server" ControlToValidate="txtHoDem"
                        SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    <label>Tên:</label>
                    <asp:Label ID="Label33" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtTen" runat="server" Width="100%"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server"
                        ControlToValidate="txtTen" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Giới tính:</label>
                </td>
                <td>
                    <asp:RadioButton ID="rdNam" runat="server" GroupName="GioiTinh" Text="   Nam" Checked="true" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton
                        ID="rdNu" runat="server" GroupName="GioiTinh" Text="   Nữ" />
                </td>
                <td>
                    <label>Ngày sinh:</label>
                </td>
                <td colspan="2">
                    <input name="NgaySinh" type="text" id="NgaySinh" runat="server" clientidmode="Static" />
                </td>
            </tr>
            <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Quê quán/Quốc tịch</h6>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Quốc tịch:</label>
                </td>
                <td>
                    <asp:DropDownList ID="drQuocTich" DataTextField="TenQuocTich" DataValueField="QuocTichID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td>
                    <label>Tỉnh/thành:</label>
                </td>
                <td colspan="2">
                    <select name="TinhID_QueQuan" id="TinhID_QueQuan" style="width: 100%;">
                        <% try
                           {
                               cm.Load_ThanhPho(hoivien.QueQuanTinhId);
                           }
                           catch
                           {
                               cm.Load_ThanhPho("00");
                           } %>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Quận/huyện:</label>
                </td>
                <td>
                    <select name="HuyenID_QueQuan" id="HuyenID_QueQuan" style="width: 100%;">
                        <%  
                            try
                            {
                                cm.Load_QuanHuyen(hoivien.QueQuanHuyenId, hoivien.QueQuanTinhId);
                            }
                            catch
                            {
                                cm.Load_QuanHuyen();
                            }                           
                        %>
                    </select>
                </td>
                <td>
                    <label>Phường/xã:</label>
                </td>
                <td colspan="2">
                    <select name="XaID_QueQuan" id="XaID_QueQuan" style="width: 100%;">
                        <%  
                            try
                            {
                                cm.Load_PhuongXa(hoivien.QueQuanXaId, hoivien.QueQuanHuyenId);
                            }
                            catch
                            {
                                cm.Load_PhuongXa();
                            }
              
                        %>
                    </select>
                </td>
            </tr>
            <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Nơi ở hiện nay</h6>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Địa chỉ:</label>
                    <asp:Label ID="Label60" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="4">
                    <asp:TextBox ID="txtDiaChi" runat="server" Width="100%"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server"
                        ControlToValidate="txtDiaChi" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Tỉnh/thành:</label>
                </td>
                <td>
                    <select name="TinhID_DiaChi" id="TinhID_DiaChi" style="width: 100%;">
                        <% try
                           {
                               cm.Load_ThanhPho(hoivien.DiaChiTinhId);
                           }
                           catch
                           {
                               cm.Load_ThanhPho("00");
                           } %>
                    </select>
                </td>
                <td>
                    <label>Quận/huyện:</label>
                </td>
                <td colspan="2">
                    <select name="HuyenID_DiaChi" id="HuyenID_DiaChi" style="width: 100%;">
                        <%  
                            try
                            {
                                cm.Load_QuanHuyen(hoivien.DiaChiHuyenId, hoivien.DiaChiTinhId);
                            }
                            catch
                            {
                                cm.Load_QuanHuyen();
                            }                           
                        %>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Phường/xã:</label>
                </td>
                <td>
                    <select name="XaID_DiaChi" id="XaID_DiaChi" style="width: 100%;">
                        <%  
                            try
                            {
                                cm.Load_PhuongXa(hoivien.DiaChiXaId, hoivien.DiaChiHuyenId);
                            }
                            catch
                            {
                                cm.Load_PhuongXa();
                            }
              
                        %>
                    </select>
                </td>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr class="trbgr">
                <td colspan="5">
                    <h6>
                        Trình độ chuyên môn</h6>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <label>Tốt nghiệp đại học:</label>
                    <asp:Label ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="drTotNghiep" DataTextField="TenTruongDaiHoc" DataValueField="TruongDaiHocID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label>Chuyên ngành:</label>
                    <asp:Label ID="Label62" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="drChuyenNganh" DataTextField="TenChuyenNganhDaoTao" DataValueField="ChuyenNganhDaoTaoID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td>
                    <label>Năm:</label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtNam_ChuyenNganh" runat="server" Width="30%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Học vị:</label>
                </td>
                <td>
                    <asp:DropDownList ID="drHocVi" DataTextField="TenHocVi" DataValueField="HocViID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td>
                    <label>Năm:</label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtNam_HocVi" runat="server" Width="30%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Học hàm:</label>
                </td>
                <td>
                    <asp:DropDownList ID="drHocHam" DataTextField="TenHocHam" DataValueField="HocHamID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td>
                    <label>Năm:</label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtNam_HocHam" runat="server" Width="30%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Số CMND/Hộ chiếu:</label>
                    <asp:Label ID="Label63" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSoCMND" runat="server" Width="100%"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator10" runat="server"
                        ControlToValidate="txtSoCMND" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    <label>Ngày cấp:</label>
                    <asp:Label ID="Label64" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2">
                    <input name="NgayCap_CMND" type="text" id="NgayCap_CMND" runat="server" clientidmode="Static" />
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator3" runat="server"
                        ControlToValidate="NgayCap_CMND" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Nơi cấp:</label>
                    <asp:Label ID="Label41" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="drNoiCap_CMND" DataTextField="TenTinh" DataValueField="TinhID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label>Số TK ngân hàng:</label>
                </td>
                <td>
                    <asp:TextBox ID="txtSoTKNganHang" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <label>Tại ngân hàng:</label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="drNganHang" DataTextField="TenNganHang" DataValueField="NganHangID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Số chứng chỉ KTV:</label>
                    <asp:Label ID="Label65" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSoChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator4" runat="server"
                        ControlToValidate="txtSoChungChiKTV" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    <label>Ngày cấp:</label>
                    <asp:Label ID="Label66" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2">
                    <input name="NgayCap_ChungChiKTV" type="text" id="NgayCap_ChungChiKTV" runat="server"
                        clientidmode="Static" />
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator5" runat="server"
                        ControlToValidate="NgayCap_ChungChiKTV" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Số giấy chứng nhận ĐKHNKiT:</label>
                </td>
                <td>
                    <asp:TextBox ID="txtSoGiayDHKN" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <label>Ngày cấp:</label>
                </td>
                <td colspan="2">
                    <input name="NgayCap_DHKN" type="text" id="NgayCap_DHKN" runat="server" clientidmode="Static" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Hạn cấp:</label>
                </td>
                <td>
                    <input name="HanCapTu" type="text" id="HanCapTu" runat="server" clientidmode="Static"
                        style="width: 46%;" />&nbsp;-&nbsp;<input name="HanCapDen" type="text" id="HanCapDen"
                            runat="server" clientidmode="Static" style="width: 46%;" />
                </td>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label>Chức vụ hiện nay:</label>
                </td>
                <td>
                    <asp:DropDownList ID="drChucVu" DataTextField="TenChucVu" DataValueField="ChucVuID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td>
                    <label>Đơn vị công tác:</label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="drDonVi" DataTextField="TenDoanhNghiep" DataValueField="HoiVienTapTheID"
                        runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
            <td>
                    <label>Đơn vị công tác (Khác):</label>
                </td>
                <td colspan="4">
                <asp:TextBox ID="txtDonViCongTac" runat="server" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Địa chỉ email:</label>
                    <asp:Label ID="Label67" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" Width="100%"></asp:TextBox>
                    <%--<asp:CustomValidator Display="Dynamic" ID="CustomValidator1" ClientValidationFunction="EmailValidation" SetFocusOnError="true" ValidateEmptyText="true"
                        runat="server" ErrorMessage="Địa chỉ email không hợp lệ">
                    </asp:CustomValidator>--%>
                </td>
                <td>
                    &nbsp;
                </td>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label>Mobile:</label>
                    <asp:Label ID="Label69" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtMobile" runat="server" Width="100%" onkeypress="return NumberOnly()"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator8" runat="server"
                        ControlToValidate="txtMobile" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    <label>Điện thoại cố định:</label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtDienThoai" runat="server" Width="100%" onkeypress="return NumberOnly()"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Sở thích:</label>
                    
                </td>
                <td>
                    <asp:DropDownList ID="drSoThich" runat="server" DataTextField="TenSoThich" DataValueField="SoThichID"
                        Width="100%">
                    </asp:DropDownList>
                </td>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr class="trbgr">
                <td colspan="2">
                    <h6>
                        Chứng chi quốc tế</h6>
                </td>
                <td colspan="3">
                    <h6>
                        Quá trình làm việc</h6>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <script type="text/javascript">
                        $(function () {
                            $('.date2').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: "dd/mm/yy",
                                yearRange: "-75:+25"
                            });
                            $('.date2').mask("99/99/9999");
                        });

                        function getme() {
                            $("[id$=txtDate]").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: "dd/mm/yy",
                                yearRange: "-75:+25"
                            });
                            $("[id$=txtDate]").mask("99/99/9999");

                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
                        }
                    </script>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <script type="text/javascript" language="javascript">
                                Sys.Application.add_load(reCalljScript);
                            </script>
                            <div style="float: right; margin-top: -30px">
                                <img src="images/icons/color/add.png" style="margin-top: 3px;" /><asp:Button ID="Button2"
                                    runat="server" CssClass="basic" Text="Thêm" CausesValidation="false" OnClick="ButtonAddChungChi_Click" /></div>
                            <asp:Label ID="Label13" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:GridView ID="gvChungChi" runat="server" AutoGenerateColumns="false" Width="100%"
                                CssClass="display" OnRowCommand="gvChungChi_RowCommand" OnRowDeleting="gvChungChi_RowDeleting"
                                OnRowDataBound="gvChungChi_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="STT" HeaderText="STT" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-VerticalAlign="Middle" />
                                    <asp:TemplateField HeaderText="Số chứng chỉ" HeaderStyle-Width="200">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtSoChungChi" runat="server" ClientIDMode="Static" Text='<%#Bind("SoChungChi") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ngày cấp" HeaderStyle-Width="220">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDate" runat="server" class="date2" Text='<%#Bind("NgayCap") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ImageUrl="/images/icons/color/cross.png" ID="btnDelete" runat="server"
                                                CommandArgument='<%#Bind("STT") %>' CommandName="Delete" OnClientClick="return confirm('Xác nhận xóa dòng này?');"
                                                CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="up1" runat="server">
                        <ContentTemplate>
                            <script type="text/javascript" language="javascript">
                                Sys.Application.add_load(reCalljScript);
                            </script>
                            <div style="float: right; margin-top: -30px">
                                <img src="images/icons/color/add.png" style="margin-top: 3px;" /><asp:Button ID="ButtonAdd"
                                    runat="server" CssClass="basic" Text="Thêm" CausesValidation="false" OnClick="ButtonAdd_Click" /></div>
                            <asp:GridView ID="gvQuaTrinh" runat="server" AutoGenerateColumns="false" Width="100%"
                                CssClass="display" OnRowCommand="gvQuaTrinh_RowCommand" OnRowDeleting="gvQuaTrinh_RowDeleting">
                                <Columns>
                                    <asp:BoundField DataField="STT" HeaderText="STT" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-VerticalAlign="Middle" />
                                    <asp:TemplateField HeaderText="Từ...đến..." HeaderStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtThoiGian" runat="server" Text='<%#Bind("ThoiGian") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Chức vụ/Công việc" HeaderStyle-Width="220">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtChucVu" runat="server" Text='<%#Bind("ChucVu") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tên đơn vị công tác" HeaderStyle-Width="220">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNoiLamViec" runat="server" Text='<%#Bind("NoiLamViec") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ImageUrl="/images/icons/color/cross.png" ID="btnDelete" runat="server"
                                                CommandArgument='<%#Bind("STT") %>' CommandName="Delete" OnClientClick="return confirm('Xác nhận xóa dòng này?');"
                                                CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr class="trbgr">
                <td colspan="2">
                    <h6>
                        Khen thưởng</h6>
                </td>
                <td colspan="3">
                    <h6>
                        Kỷ luật</h6>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <script type="text/javascript">
                        $(function () {
                            $('.date2').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: "dd/mm/yy",
                                maxDate: '0'
                            });
                            $('.date2').mask("99/99/9999");
                        });

                        function getme() {
                            $("[id$=txtNgayThang]").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: "dd/mm/yy",
                                maxDate: '0'
                            });
                            $("[id$=txtNgayThang]").mask("99/99/9999");

                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
                        }
                    </script>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <script type="text/javascript" language="javascript">
                                Sys.Application.add_load(reCalljScript);
                            </script>
                            <div style="float: right; margin-top: -30px">
                                <img src="images/icons/color/add.png" style="margin-top: 3px;" /><asp:Button ID="Button1"
                                    runat="server" CssClass="basic" Text="Thêm" CausesValidation="false" OnClick="ButtonAddKhenThuong_Click" /></div>
                            <asp:Label ID="Label12" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:GridView ID="gvKhenThuong" runat="server" CssClass="display" EmptyDataText="Chưa có dữ liệu" Width="100%"
                                ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" OnRowCommand="gvKhenThuong_RowCommand"
                                OnRowDeleting="gvKhenThuong_RowDeleting" OnRowDataBound="gvKhenThuong_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ngày tháng" HeaderStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNgayThang" runat="server" CssClass="date2" Text='<%#Bind("NgayThang") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cấp và hình thức" HeaderStyle-Width="300">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="drHinhThuc" runat="server" DataTextField="TenHinhThuc" DataValueField="HinhThucID"
                                                Width="100%" Height="30px">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lý do" HeaderStyle-Width="300">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLyDo" runat="server" Text='<%#Bind("LyDo") %>' Width="100%"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ImageUrl="/images/icons/color/cross.png" ID="btnDelete" runat="server" CommandArgument='<%#Bind("STT") %>'
                                                CausesValidation="false" CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <script type="text/javascript" language="javascript">
                                Sys.Application.add_load(reCalljScript);
                            </script>
                            <div style="float: right; margin-top: -30px">
                                <img src="images/icons/color/add.png" style="margin-top: 3px;" /><asp:Button ID="Button3"
                                    runat="server" CssClass="basic" Text="Thêm" CausesValidation="false" OnClick="ButtonAddKyLuat_Click" /></div>
                            <asp:GridView ID="gvKyLuat" runat="server" CssClass="display" EmptyDataText="Chưa có dữ liệu" Width="100%"
                                ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" OnRowCommand="gvKyLuat_RowCommand"
                                OnRowDeleting="gvKyLuat_RowDeleting" OnRowDataBound="gvKyLuat_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ngày tháng" HeaderStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNgayThang" runat="server" CssClass="date2" Text='<%#Bind("NgayThang") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cấp và hình thức" HeaderStyle-Width="300">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="drHinhThuc" runat="server" DataTextField="TenHinhThuc" DataValueField="HinhThucID"
                                                Width="100%" Height="30px">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lý do" HeaderStyle-Width="300">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLyDo" runat="server" Text='<%#Bind("LyDo") %>' Width="100%"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ImageUrl="/images/icons/color/cross.png" ID="btnDelete" runat="server" CommandArgument='<%#Bind("STT") %>'
                                                CausesValidation="false" CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="trbgr" colspan="5">
                    <h6>
                        Tải file xác nhận
                    </h6>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <table width="100%">
                        <tr>
                            <td colspan="3" align="center">
                                <em style="color: #F90; font-size: 14px;">(File hồ sơ tải lên phải thuộc một trong các
                                    định dạng <em style="color: #Ff0000; font-weight: bold">.DOC, .DOCX, .PDF, .BMP, .GIF,
                                        .PNG, .JPG, .RAR, .ZIP</em> . Kích thước file tải lên tối đa <em style="color: #Ff0000;
                                            font-weight: bold">10MB</em>, chất lượng phải đảm bảo để người tiếp nhận
                                    hồ sơ có thể đọc được. <em style="color: #Ff0000; font-weight: bold">Tất cả các file
                                        hồ sơ phải được quét hoặc chụp từ bản gốc (kể cả Đơn, Giấy cam kết, Giấy giới thiệu,….)</em>.
                                    Tổ chức nộp hồ sơ phải chịu trách nhiệm hoàn toàn trước pháp luật về tính chính
                                    xác của hồ sơ nộp trực tuyến với hồ sơ gốc.)</em>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="FileDinhKem_grv" runat="server" Width="100%" AutoGenerateColumns="False"
                                    EnableModelValidation="True" AlternatingRowStyle-BackColor="WhiteSmoke">
                                    <Columns>
                                        <asp:TemplateField HeaderText="   ">
                                            <ItemTemplate>
                                                <label><%# Eval("TenBieuMau") %></label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=' '>
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="requred" runat="server" Text="(*)" ForeColor="Red"></asp:Label><asp:FileUpload
                                                                ID="FileUp" name="FileUp" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Jj][Pp][Gg])|.*\.([Bb][Mm][Pp])|.*\.([pP][nN][gG])|.*\.([pO][dD][fF])|.*\.([rR][aA][rR])|.*\.([zZ][iI][pP])|.*\.(?:[dD][oO][cC][xX]?)$)"
                                                                ControlToValidate="FileUp" Display="Dynamic" ErrorMessage="Sai định dạng tập tin đính kèm"
                                                                ValidationGroup="CheckFileExt" SetFocusOnError="true">
                                                            </asp:RegularExpressionValidator>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldFileUpload" runat="server" ControlToValidate="FileUp"
                                                                Display="Dynamic" ErrorMessage="Nhập tệp đính kèm." ValidationGroup="CheckFileExt"
                                                                ForeColor="Red" SetFocusOnError="true">
                                                            </asp:RequiredFieldValidator>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="   ">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="linkFileUpload" NavigateUrl='<%# Eval("FileID","Download.ashx?mode=hv&AttachFileID={0}") %>'
                                                    runat="server">
                                                    <div style="width: 200px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis">
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("TenFileDinhKem") %>' />
                                                    </div>
                                                </asp:HyperLink></ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="LoaiGiayToID" HeaderText="" ItemStyle-CssClass="Hide"
                                            HeaderStyle-CssClass="Hide">
                                            <HeaderStyle CssClass="Hide"></HeaderStyle>
                                            <ItemStyle CssClass="Hide"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FileID" HeaderText="" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide">
                                            <HeaderStyle CssClass="Hide"></HeaderStyle>
                                            <ItemStyle CssClass="Hide"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="BATBUOC" HeaderText="" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide">
                                            <HeaderStyle CssClass="Hide"></HeaderStyle>
                                            <ItemStyle CssClass="Hide"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TenFileDinhKem" HeaderText="" ItemStyle-CssClass="Hide"
                                            HeaderStyle-CssClass="Hide">
                                            <HeaderStyle CssClass="Hide"></HeaderStyle>
                                            <ItemStyle CssClass="Hide"></ItemStyle>
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
          <td class="trbgr" colspan="5" ><h6>Số quyết định/ngày quyết định
              </h6></td>
        </tr>
        <tr><td colspan="5">
            <table>
                <tr>
                    <td>Số quyết định:</td>
                    <td>
                    
                    <asp:TextBox ID="txtSoQuyetDinh" runat="server" Width="100%" Font-Bold="true"></asp:TextBox>
                    
                </td>
                <td>
                    Ngày quyết định:
                    
                </td>
                <td colspan="2">
                    
                    <asp:TextBox ID="txtNgayQuyetDinh" runat="server" Width="100%" ClientIdMode="Static" Font-Bold="true"></asp:TextBox>
                    
                </td>
                </tr>
            </table>
        </td></tr>
            </table>

            <% if(Request.QueryString["ds"] == "xoaten") { %>
            <table id="Table7" width="100%" border="0" class="formtbl" runat="server">
            <tr>
                <td>
                    <label>Lý do từ chối:</label>
                    
                </td>
                <td>
                    <asp:TextBox ID="txtLyDoTuChoi" runat="server" TextMode="MultiLine" Rows="4" Width="100%"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator6" runat="server"
                        ControlToValidate="txtLyDoTuChoi" ValidationGroup="TuChoi" SetFocusOnError="true" ErrorMessage="Trường bắt buộc phải nhập."></asp:RequiredFieldValidator>--%>
                </td>
                
            </tr>
            </table>
            <% } %>
        
    </div>
</div>
                            </div>
                            <div id="tab2">
                                <%--<asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>--%>
                    <div style="margin-bottom: 10px;">Chọn năm <asp:DropDownList ID="drChonNamCNKT" runat="server" DataTextField="Nam" DataValueField="Nam" OnSelectedIndexChanged="drNamCNKT_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
                                <div class="widgetbox">
                                <h4 class="widgettitle">
        <asp:Label ID="Label14" runat="server" Text="Đào tạo cập nhật kiến thức"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4>
                                <div class="widgetcontent">
                                <table id="Table2" width="100%" border="0" class="formtbl" runat="server">
                                <tr>
                                <td>
                                <asp:GridView ID="gvDaoTao" runat="server" ClientIDMode="Static" CssClass="display" EmptyDataText="Chưa có dữ liệu" OnDataBound="gvDaoTao_OnDataBound" OnSorting="gvDaoTao_Sorting"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="MaChuyenDe" SortExpression="MaChuyenDe" HeaderText="Mã chuyên đề" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Width="150px" />
                                <asp:BoundField DataField="TenChuyenDe" SortExpression="TenChuyenDe" HeaderText="Tên chuyên đề" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ThoiGian" SortExpression="ThoiGian" HeaderText="Từ ngày - Đến ngày" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TenLopHoc" SortExpression="TenLopHoc" HeaderText="Tên lớp học" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KT" SortExpression="KT" HeaderText="KT, KiT" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DD" SortExpression="DD" HeaderText="ĐĐ" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Khac" SortExpression="Khac" HeaderText="#" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                        </asp:GridView>
                        </td>
                                </tr>
                                </table>
                                </div></div>
                        <asp:Literal ID="litgvDaoTao" runat="server"></asp:Literal>
                        <%--</ContentTemplate></asp:UpdatePanel>--%>
                            </div>
                            <div id="tab3">
                            <asp:UpdatePanel ID="upKiemSoat" runat="server">
                    <ContentTemplate>
                    <div style="margin-bottom: 10px;">Chọn năm <asp:DropDownList ID="drNam" runat="server" DataTextField="Nam" DataValueField="Nam" OnSelectedIndexChanged="drNam_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
                                <div class="widgetbox">
                                <h4 class="widgettitle">
        <asp:Label ID="Label15" runat="server" Text="Kết quả kiểm soát chất lượng qua các năm"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4>
                                <div class="widgetcontent">
                                <table id="Table3" width="100%" border="0" class="formtbl" runat="server">
                                <tr>
                                <td>

                                 <asp:GridView ID="gvKiemSoat" runat="server" ClientIDMode="Static" CssClass="display" EmptyDataText="Chưa có dữ liệu" OnDataBound="gvKiemSoat_OnDataBound" OnSorting="gvKiemSoat_Sorting"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="HoSo" SortExpression="HoSo" HeaderText="Hồ sơ kiểm toán được kiểm tra" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="500px" />
                                
                                <asp:BoundField DataField="Tot" SortExpression="Tot" HeaderText="Tốt" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DatYeuCau" SortExpression="DatYeuCau" HeaderText="Đạt yêu cầu" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="KhongDat" SortExpression="KhongDat" HeaderText="Không đạt" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="YeuKem" SortExpression="YeuKem" HeaderText="Yếu kém" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                        </asp:GridView>

                                </td>
                                </tr>
                                </table>
                                </div></div>

                                <div class="widgetbox">
                                <h4 class="widgettitle">
        <asp:Label ID="Label16" runat="server" Text="Thông tin tham gia thành viên đoàn kiểm tra qua các năm"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4>
                                <div class="widgetcontent">
                                <table id="Table4" width="100%" border="0" class="formtbl" runat="server">
                                <tr>
                                <td>

                                 <asp:GridView ID="gvThanhVien" runat="server" ClientIDMode="Static" CssClass="display" EmptyDataText="Chưa có dữ liệu"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Nam" SortExpression="HoSo" HeaderText="Năm" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Width="150px" />
                                
                                <asp:BoundField DataField="MaDoanKiemTra" SortExpression="MaDoanKiemTra" HeaderText="Mã đoàn kiểm tra" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NgayLapDoan" SortExpression="NgayLapDoan" HeaderText="Ngày lập đoàn" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="CTKT" HtmlEncode="false" SortExpression="CTKT" HeaderText="Danh sách CTKT kiểm tra trực tiếp" ItemStyle-HorizontalAlign="Left" />
                                
                            </Columns>
                        </asp:GridView>

                                </td>
                                </tr>
                                </table>
                                </div></div>
                                </ContentTemplate>
                    </asp:UpdatePanel>
                            </div>
                            <div id="tab4">
                                <div class="widgetbox">
                                <h4 class="widgettitle">
        <asp:Label ID="Label17" runat="server" Text="Bảng thông tin tình trạng nộp phí"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4>
                                <div class="widgetcontent">
                                <table id="Table5" width="100%" border="0" class="formtbl" runat="server">
                                <tr>
                                <td>

                                 <asp:GridView ID="gvThanhToan" ClientIDMode="Static" runat="server" CssClass="display" EmptyDataText="Chưa có dữ liệu" OnSorting="gvThanhToan_Sorting"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="LoaiPhi" HeaderText="Loại phí" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="400px" />
                                    <asp:BoundField DataField="SoTienPhaiNop" HeaderText="Số tiền phải nộp" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="SoTienDaNop" HeaderText="Số tiền đã nộp" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="NgayNop" HeaderText="Ngày nộp" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="SoTienChuaNop" HeaderText="Số tiền chưa nộp" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                        </asp:GridView>

                                </td>
                                </tr>
                                </table>
                                </div></div>
                            </div>
                            <table id="Table6" width="100%" border="0" class="formtbl" runat="server">
                            <tr>
                <td colspan="5" align="center">
                    <p>
                        &nbsp;</p>
                        <% if (Request.QueryString["act"] != "view")
                           { %>
                    <a class="btn btn-rounded"><i class="iconfa-plus-sign"></i>
                        <asp:Button ID="btnTiepNhan" Text="Lưu" runat="server" OnClick="btnGhi_Click" />
                    </a>
                    <% } %>
                    <% if(Request.QueryString["ds"] == "xoaten") { %>
                    <% if (Request.QueryString["tinhtrangid"] == "1")
                       { %>
                    <a class="btn btn-rounded"><i class="iconfa-plus-sign"></i>
                        <asp:Button ID="Button4" Text="Tiếp nhận" runat="server" OnClick="btnTiepNhan_Click" CausesValidation="false" />
                    </a>                   
                    <a class="btn btn-rounded"><i class="iconfa-plus-sign"></i>
                        <asp:Button ID="Button5" Text="Từ chối tiếp nhận" runat="server" OnClick="btnTuChoiTiepNhan_Click" ValidationGroup="TuChoi" />
                    </a>
                    <% } %>
                    <% if (Request.QueryString["tinhtrangid"] == "3")
                       { %>
                    <a class="btn btn-rounded"><i class="iconfa-plus-sign"></i>
                        <asp:Button ID="Button6" Text="Xóa tên" runat="server" OnClick="btnDuyet_Click" CausesValidation="false" />
                    </a>
                    <a class="btn btn-rounded"><i class="iconfa-plus-sign"></i>
                        <asp:Button ID="Button7" Text="Từ chối xóa tên" runat="server" OnClick="btnTuChoiDuyet_Click" ValidationGroup="TuChoi" />
                    </a>
                    <% } %>
                    <% if (Request.QueryString["tinhtrangid"] == "5")
                       { %>
                       <%--<a class="btn btn-rounded"><i class="iconfa-plus-sign"></i>
                        <asp:Button ID="Button8" Text="Hủy xóa tên" runat="server" OnClick="btnThoaiDuyet_Click" CausesValidation="false" />
                    </a>--%>
                       <% } %>
                    <% } %>
                    <a class="btn btn-rounded"><i class="iconfa-minus-sign"></i>
                        <asp:Button ID="btnTuChoi" Text="Quay lại" runat="server" OnClick="btnQuayLai_Click" CausesValidation="false" />
                    </a>
                    <a class="btn btn-rounded"><i class="iconsweets-word2"></i>
                        <asp:Button ID="btnKetXuat" Text="Kết xuất GCN" runat="server" OnClick="btnKetXuat_Click" />
                    </a>
                     
                     <input type="hidden" id="hdAction" name="hdAction" />
                </td>
            </tr>
                            </table>
                        </div>
     
      <asp:Literal ID="litChonTab" runat="server"></asp:Literal>
                        </form>
<script type="text/javascript">
<% cm.chondiaphuong_script("QueQuan",0); %>
    <% cm.chondiaphuong_script("DiaChi",0); %>

    jQuery("#form_nhaphoso").validate({
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form_nhaphoso").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_nhaphoso").hide()
            }
        }
    });

    function NumberOnly() {
        var AsciiValue = event.keyCode
        if ((AsciiValue >= 48 && AsciiValue <= 57) || (AsciiValue == 8 || AsciiValue == 127 || AsciiValue == 44))
            event.returnValue = true;
        else
            event.returnValue = false;
    }
   
    $.datepicker.setDefaults($.datepicker.regional['vi']);
   $(function () {
            $('#NgaySinh, #NgayCap_DHKN, #NgayCap_ChungChiKTV, #NgayCap_CMND, #txtNgayQuyetDinh').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                yearRange: "-75:+25"
            });            

            $('#HanCapTu').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                yearRange: "-75:+25",
                onClose: function (selectedDate) {              
                $("#HanCapDen").datepicker("option", "minDate", selectedDate);
              }
            });

            $('#HanCapDen').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                yearRange: "-75:+25",
                onClose: function (selectedDate) {              
                $("#HanCapTu").datepicker("option", "maxDate", selectedDate);
              }         
            });

            $('#NgaySinh, #NgayCap_DHKN, #NgayCap_ChungChiKTV, #NgayCap_CMND, #HanCapTu, #HanCapDen, #txtNgayQuyetDinh').mask("99/99/9999");

         });
   
</script>
<asp:PlaceHolder ID="scriptdisable" runat="server"></asp:PlaceHolder>

<asp:Literal ID="litgvThanhToan" runat="server"></asp:Literal>
    <asp:Literal ID="litgvKiemSoat" runat="server"></asp:Literal>
