﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using HiPT.VACPA.DL;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Configuration;

using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;

public partial class usercontrols_xoatenhoivientapthe : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
  
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);


    string tuchoitiepnhan = "tuchoitiepnhan";
    string tiepnhan = "tiepnhan";
    string duyetdon="duyetdon";
    string tuchoiduyet="tuchoiduyet";
    string thoaiduyet="thoaiduyet";

    public int trangthaiChoTiepNhanID = 1;
    public int trangthaiTuChoiID = 2;
    public int trangthaiChoDuyetID = 3;
    public int trangthaiTraLaiID = 4;
    public int trangthaiDaPheDuyet = 5;
    public int trangthaiThoaiDuyetID = 6;


    string tk_TTChoTiepNhan = "1";
    string tk_TTTuChoiTiepNhan = "2";
    string tk_TTChoDuyet = "3";
    string tk_TTTuChoiDuyet = "4";
    string tk_TTDaDuyet = "5";
    string tk_TTThoaiDuyet = "6";

   
     


    string delete = "delete";
    string canhan = "1";
    string tapthe = "2";
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

        // Tên chức năng
        string tenchucnang = "Quản lý xóa tên hội viên tổ chức";
        // Icon CSS Class  
        string IconClass = "iconfa-book";

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            Form1.Visible = false;
            return;
        }
      

        //try
        //{
            String resulte = "";
            if (Request.QueryString["act"] != "")
            {
               
                //  action 
                if (!String.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    if (Request.QueryString["act"] == "xoa")
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = "DELETE tblXoaTenHoiVien WHERE XoaTenHoiVienID = " + Request.QueryString["id"];
                        DataAccess.RunActionCmd(cmd);
                        resulte = "resxoa=1";

                        cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Xóa đơn xin xóa tên hội viên, XoaTenHoiVienID: " + Request.QueryString["id"]);
                    }
                    else
                    {
                        string[] lstObject = Request.QueryString["id"].Split(',');

                        string lstIdTapThe = "";
                        string lstIdTinhTrangTapThe = "";
                        int countTapThe = 0;
                        foreach (string iterm in lstObject)
                        {
                            string[] lstTemp = iterm.Split('|');
                            if (lstTemp.Length == 2)
                            {

                                if (countTapThe == 0)
                                {
                                    lstIdTinhTrangTapThe = iterm;
                                    string[] lstIterm = lstTemp[0].Split('|');
                                    lstIdTapThe = lstIterm[0];
                                }
                                else
                                {
                                    lstIdTinhTrangTapThe = lstIdTinhTrangTapThe + "," + iterm;
                                    string[] lstIterm = lstTemp[0].Split('|');
                                    lstIdTapThe = lstIdTapThe + "," + lstIterm[0];
                                }

                                countTapThe = countTapThe + 1;

                            }
                        }


                        if (lstObject.Length > 0)
                        {
                            // delete
                            if (Request.QueryString["act"] == "delete")
                            {
                                // string[] lstObjectId = lstIdTapThe.Split(',');
                                SqlXoaTenHoiVienProvider XoaTenHoiVien_provider = new SqlXoaTenHoiVienProvider(cm.connstr, true, "");
                                XoaTenHoiVien XoaTenHV = new XoaTenHoiVien();
                                foreach (string HoiVienID in lstObject)
                                {
                                    XoaTenHV = new XoaTenHoiVien();
                                    XoaTenHV.HoiVienId = Convert.ToInt32(HoiVienID);
                                    XoaTenHV.LoaiHoiVien = "2";// hoi vien tap the
                                    XoaTenHV.LyDoXoaTen = Request.QueryString["dieukienxoaten"];
                                    XoaTenHV.GhiChuXoaTen = Request.QueryString["diengiai"];
                                    XoaTenHV.TinhTrangXoaTenId = trangthaiChoDuyetID;
                                    XoaTenHV.NgayNhap = DateTime.Now;
                                    XoaTenHV.NguoiNhap = cm.Admin_HoVaTen;
                                    XoaTenHV.LoaiXoaTen = Request.QueryString["loaixoaten"];//0: tự nguyện; 1: nợ phí 3 năm; 2: lý do khác
                                    XoaTenHoiVien_provider.Insert(XoaTenHV);

                                    cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Thêm mới đơn xin xóa tên hội viên tổ chức, HoiVienID: " + HoiVienID);
                                }
                                resulte = "&resdelete=1";
                                  Response.Redirect("admin.aspx?page=xoatenhoivientapthe&thongbao=1", false);

                            }

                        }


                        if (lstIdTinhTrangTapThe.Length > 0)//    tap the
                        {



                            // tiếp nhận
                            if (Request.QueryString["act"] == tiepnhan)
                            {
                                string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');

                                foreach (string sDonHoiVien in lstDonHoiVien)
                                {
                                    string[] lstIterm = sDonHoiVien.Split('|');
                                    String sDonHoiVienID = lstIterm[0];
                                    String sDonHoiVienTinhtrang = lstIterm[1];
                                    if (sDonHoiVienTinhtrang.Trim() == tk_TTChoTiepNhan)
                                    {

                                        btnTrinhPheDuyet_Click(sDonHoiVienID);
                                    }
                                }
                                resulte = "&res" + tiepnhan + "=1";
                                // Response.Redirect("admin.aspx?page=xoatenhoivientapthe" + resulte);
                            }

                            //  từ chối tiếp nhận
                            if (Request.QueryString["act"] == tuchoitiepnhan)
                            {
                                String sLydotuchoi = Request.QueryString["lydo"];
                                string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                                bool isSendMail = true;
                                foreach (string sDonHoiVien in lstDonHoiVien)
                                {
                                    string[] lstIterm = sDonHoiVien.Split('|');
                                    String sDonHoiVienID = lstIterm[0];
                                    String sDonHoiVienTinhtrang = lstIterm[1];
                                    if (sDonHoiVienTinhtrang.Trim() == tk_TTChoTiepNhan)
                                    {
                                        btnTuChoiDon_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                    }
                                }
                                if (!isSendMail)
                                {
                                    //  resulte = "res" + duyetdon + "=0";
                                }
                                else
                                {
                                    resulte = "res" + tuchoitiepnhan + "=1";
                                }

                            }

                            //  phe duyet 
                            if (Request.QueryString["act"] == duyetdon)
                            {
                                string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                                bool isSendMail = true;
                                foreach (string sDonHoiVien in lstDonHoiVien)
                                {
                                    string[] lstIterm = sDonHoiVien.Split('|');
                                    String sDonHoiVienID = lstIterm[0];
                                    String sDonHoiVienTinhtrang = lstIterm[1];
                                    if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                    {
                                        btnPheDuyet_Click(sDonHoiVienID, ref isSendMail);
                                    }
                                }
                                if (!isSendMail)
                                {
                                    //  resulte = "res" + duyetdon + "=0";
                                }
                                else
                                {
                                    resulte = "&res" + duyetdon + "=1";
                                    // Response.Redirect("admin.aspx?page=xoatenhoivientapthe" + resulte);
                                }

                            }
                            // tu choi duyet 
                            if (Request.QueryString["act"] == tuchoiduyet)
                            {
                                String sLydotuchoi = Request.QueryString["lydo"];
                                string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                                bool isSendMail = true;
                                foreach (string sDonHoiVien in lstDonHoiVien)
                                {
                                    string[] lstIterm = sDonHoiVien.Split('|');
                                    String sDonHoiVienID = lstIterm[0];
                                    String sDonHoiVienTinhtrang = lstIterm[1];
                                    if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                    {
                                        btnTuChoiDuyet_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                    }
                                }

                                if (!isSendMail)
                                {
                                    //  resulte = "res" + tuchoiduyet + "=0";
                                }
                                else
                                {
                                    resulte = "&res" + tuchoiduyet + "=1";
                                    //  Response.Redirect("admin.aspx?page=xoatenhoivientapthe" + resulte);
                                }

                            }
                            // thoai duyet 
                            if (Request.QueryString["act"] == thoaiduyet)
                            {
                                
                                string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                                bool isSendMail = true;
                                foreach (string sDonHoiVien in lstDonHoiVien)
                                {
                                    string[] lstIterm = sDonHoiVien.Split('|');
                                    String sDonHoiVienID = lstIterm[0];
                                    String sDonHoiVienTinhtrang = lstIterm[1];
                                    if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                    {
                                        btnThoaiDuyet_Click(sDonHoiVienID, ref isSendMail);
                                    }
                                }
                                if (!isSendMail)
                                {
                                    //resulte = "res" + thoaiduyet + "=0";
                                }
                                else
                                {
                                    resulte = "&res" + thoaiduyet + "=1";
                                    // Response.Redirect("admin.aspx?page=xoatenhoivientapthe" + resulte);
                                }
                            }
                        }

                    }
                    string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

                    Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(thongbao));
                    //Response.Redirect("admin.aspx?page=xoatenhoivientapthe" + resulte);
                }
              
            }
    
           
        //}
        //catch (Exception ex)
        //{
        //    Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        //}

            ///////////

            load_users();

            if (Request.QueryString["thongbao"] == "1")
            {
                string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

                Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(thongbao));
            }

    }



   

    public DataSet sp_XoaTenHoiVienSearch( )
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            bool isCaNhan = false; ;
            bool isTapThe = false;
            String sLoaiDon = "";
            String sLoaiHinhDoanhNghiep = "";
            String sTenNguoiDaiDien = "";
            String sLoaiXoaTen = "";
            String sTrangThai = "";
            String sTenHoiVien = "";
            String sHoDemCaNhan = "";
            String sTenCaNhan = "";

            String sIDHoiVien = "";
            String sSoDon = "";
            String sHinhThucNop = "";
            String sMaTinhThanh = "";
            String sMaQuanHuyen = "";

            String sCongtyKiemToan = "";
            String sSoChungChiKTV = "";
            String sSoDKKD = "";
            String sSoCNDuDieuKienHNKT = "";

            String sNgayNopTu = "";
            String sNgayNopDen = "";
            String sNgayDuyetTu = "";
            String sNgayDuyetDen = "";

          


            // Trang thai
            if (Request.Form["tk_TTAll"] != null && Request.Form["tk_HVCN"] == "true")
            {  // check all

                sTrangThai = "";// all
            }
            else
            {
                int itemp = 0;
                if (Request.Form["tk_TTChoTiepNhan"] != null && Request.Form["tk_TTChoTiepNhan"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTChoTiepNhan;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTChoTiepNhan;
                    }

                    itemp = itemp + 1;
                }
                if (Request.Form["tk_TTChoDuyet"] != null && Request.Form["tk_TTChoDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTChoDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTChoDuyet;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTTuChoiTiepNhan"] != null && Request.Form["tk_TTTuChoiTiepNhan"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTTuChoiTiepNhan;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTTuChoiTiepNhan;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTTuChoiDuyet"] != null && Request.Form["tk_TTTuChoiDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTTuChoiDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTTuChoiDuyet;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTDaDuyet"] != null && Request.Form["tk_TTDaDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTDaDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTDaDuyet;
                    }
                    itemp = itemp + 1;

                }
                //if (Request.Form["tk_TTThoaiDuyet"] != null && Request.Form["tk_TTThoaiDuyet"] == "true")
                //{
                //    if (itemp == 0)
                //    {
                //        sTrangThai = tk_TTThoaiDuyet;
                //    }
                //    else
                //    {
                //        sTrangThai = sTrangThai + "," + tk_TTThoaiDuyet;
                //    }
                //    itemp = itemp + 1;

                //}


            }



            // Ten hoi vien
            if (Request.Form["tk_TenHoiVien"] != null)
            {
                sTenHoiVien = Request.Form["tk_TenHoiVien"];
                

            }

            // Ten dang nhap
            if (Request.Form["tk_ID"] != null)
            {
                sIDHoiVien = Request.Form["tk_ID"];
            }


            // Loai hinh Dn
            if (Request.Form["tk_LoaiHinhDN"] != null)
            {
                sLoaiHinhDoanhNghiep = Request.Form["tk_LoaiHinhDN"];
            }
            // Loai Xoa TEn
            if (Request.Form["tk_LoaiXoaTen"] != null)
            {
                sLoaiXoaTen = Request.Form["tk_LoaiXoaTen"];
            }

            
            // Ten nguoi dai dien
            if (Request.Form["tk_NguoiDaiDienPL"] != null)
            {
                sTenNguoiDaiDien = Request.Form["tk_NguoiDaiDienPL"];
            }



            // Ma Tinh
            if (Request.Form["TinhID_ToChuc"] != null)
            {
                sMaTinhThanh = Request.Form["TinhID_ToChuc"];
            }
            // Ma Huyen
            if (Request.Form["HuyenID_ToChuc"] != null)
            {
                sMaQuanHuyen = Request.Form["HuyenID_ToChuc"];
            }


            

            // So DKKD
            if (Request.Form["tk_SoDKKD"] != null)
            {
                sSoDKKD = Request.Form["tk_SoDKKD"];
            }


            // Ngay nop tu
            if (Request.Form["tk_NgayNopTu"] != null)
            {
                sNgayNopTu = Request.Form["tk_NgayNopTu"].Replace("/", ""); ;
            }

            // Ngay nop den
            if (Request.Form["tk_NgayNopDen"] != null)
            {
                sNgayNopDen = Request.Form["tk_NgayNopDen"].Replace("/","");
            }
            // Ngay duyet tu
            if (Request.Form["tk_NgayPheDuyetTu"] != null)
            {
                sNgayDuyetTu = Request.Form["tk_NgayPheDuyetTu"].Replace("/", ""); ;
            }
            //  Ngay duyet den
            if (Request.Form["tk_NgayPheDuyetDen"] != null)
            {
                sNgayDuyetDen = Request.Form["tk_NgayPheDuyetDen"].Replace("/", ""); ;
            }


            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

         
            arrParams.Add(new DBHelpers.DataParameter("TrangThai", sTrangThai));
            arrParams.Add(new DBHelpers.DataParameter("TenHoiVien", sTenHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("LoaiXoaTen", sLoaiXoaTen));
         
            arrParams.Add(new DBHelpers.DataParameter("IDHoiVien", sIDHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("TenNguoiDaiDien", sTenNguoiDaiDien));
            arrParams.Add(new DBHelpers.DataParameter("LoaiHinhDoanhNghiep", sLoaiHinhDoanhNghiep));
            arrParams.Add(new DBHelpers.DataParameter("MaTinhThanh", sMaTinhThanh));
            arrParams.Add(new DBHelpers.DataParameter("MaQuanHuyen", sMaQuanHuyen));
           
            arrParams.Add(new DBHelpers.DataParameter("SoDKKD", sSoDKKD));
            
            arrParams.Add(new DBHelpers.DataParameter("NgayKetNapTu", sNgayNopTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayKetNapDen", sNgayNopDen));
            arrParams.Add(new DBHelpers.DataParameter("NgayXoaTenTu", sNgayDuyetTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayXoaTenDen", sNgayDuyetDen));


            objDBHelper = ConnectionControllers.Connect("VACPA");

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_xoatenhoivientt_search", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }

 
    protected void load_users()
    {
        try
        {

         
        
            DataSet ds = sp_XoaTenHoiVienSearch();
 
            DataView dt = ds.Tables[0].DefaultView;
             

            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;
            int i;
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "XoaTenHoiVien" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            ds = null;
            dt = null;


          

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "XoaTenHoiVien", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }
    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }
    protected void btnGhi_Click(object sender, EventArgs e)
    {

    }

    public void Load_LoaiHinhDoanhNghiep( )
    {
        string output_html = "";

        DataSet ds = new DataSet();


        SqlCommand sql = new SqlCommand();
        sql.CommandText =  " SELECT LoaiHinhDoanhNghiepID, TenLoaiHinhDoanhNghiep FROM tblDMLoaiHinhDoanhNghiep";
          ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        output_html += @"<option value=""""><< Tất cả >></option>";
        foreach (DataRow LoaiHinhDN in dt.Rows)
        {
 
                output_html += @"
                    <option  value=""" + LoaiHinhDN["LoaiHinhDoanhNghiepID"].ToString().Trim() + @""">" + LoaiHinhDN["TenLoaiHinhDoanhNghiep"] + @"</option>";
           

        }

        ds = null;
        dt = null;


        System.Web.HttpContext.Current.Response.Write(output_html);
    }


    protected void btnTuChoiDon_Click(String sDonHoiVienID, String sLyDoTuChoi, ref bool isSendMail)
    {
        try
        {
            int donHoiVienTtId = 0;
            try
            {
                donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            }
            catch { }
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy") , sLyDoTuChoi, ref Email);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '2' AND XoaTenHoiVienID = " + donHoiVienTtId;
                string HoiVienID = DataAccess.DLookup(cmd);

                cmd = new SqlCommand();
                cmd.CommandText = "SELECT TenDoanhNghiep FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + HoiVienID;

                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối tiếp nhận đơn xin xóa tên hội viên " + DataAccess.DLookup(cmd));

                // Send mail
                string body = "";
                body += "VACPA từ chối tiếp nhận xóa tên hội viên tổ chức của công ty<BR/>";
                body += "Lý do từ chối: " + sLyDoTuChoi + " <BR/>";
                // Gửi thông báo
                cm.GuiThongBao(HoiVienID, "4", "VACPA - Từ chối tiếp nhận đơn xin xóa tên hội viên tổ chức", body);
                string msg = "";
                if (Email != "")
                {
                    if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối tiếp nhận đơn xin xóa tên hội viên tổ chức", body, ref msg))
                    { }
                    else
                    {
                        isSendMail = false;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void btnTrinhPheDuyet_Click(String sDonHoiVienID)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
               
                UpdateStatus(donHoiVienTtId, trangthaiChoDuyetID, "", "","" );
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT TenDoanhNghiep FROM tblHoiVienTapThe WHERE HoiVienTapTheID = (SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '2' AND XoaTenHoiVienID = " + donHoiVienTtId + ")";
                
                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Tiếp nhận đơn xin xóa tên hội viên " + DataAccess.DLookup(cmd));
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnTuChoiDuyet_Click(String sDonHoiVienID, String sLyDoTuChoi, ref bool isSendMail)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiTraLaiID,  cm.Admin_TenDangNhap,  
                        DateTime.Now.ToString("ddMMyyyy"), sLyDoTuChoi, ref Email);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '2' AND XoaTenHoiVienID = " + donHoiVienTtId;
                string HoiVienID = DataAccess.DLookup(cmd);

                cmd = new SqlCommand();
                cmd.CommandText = "SELECT TenDoanhNghiep FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + HoiVienID;

                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối duyệt đơn xin xóa tên hội viên " + DataAccess.DLookup(cmd));

                // Send mail
                string body = "";
                body += "VACPA từ chối phê duyệt xóa tên hội viên tổ chức của công ty<BR/>";
                body += "Lý do từ chối: " + sLyDoTuChoi + " <BR/>";
                // Gửi thông báo
                cm.GuiThongBao(HoiVienID, "4", "VACPA - Từ chối duyệt yêu cầu xóa tên hội viên tổ chức", body);
                string msg = "";
                if (Email != "")
                {
                    if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối duyệt yêu cầu xóa tên hội viên tổ chức", body, ref msg))
                    { }
                    else
                    {
                        isSendMail = false;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnPheDuyet_Click(String sDonHoiVienID, ref bool isSendMail)
    {

        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiDaPheDuyet,  cm.Admin_TenDangNhap,  DateTime.Now.ToString("ddMMyyyy"), "", ref Email );

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '2' AND XoaTenHoiVienID = " + donHoiVienTtId;
                string HoiVienID = DataAccess.DLookup(cmd);

                cmd = new SqlCommand();
                cmd.CommandText = "SELECT TenDoanhNghiep FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + HoiVienID;

                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin xóa tên hội viên " + DataAccess.DLookup(cmd));

                // Send mail
                string body = "";
                body += "Công ty đã được phê duyệt xin thôi gia nhập hội viên tổ chức, bạn vẫn có thể đăng nhập vào hệ thống và sử dụng phần mềm với người sử dụng là công ty kiểm toán<BR/>";
                // Gửi thông báo
                cm.GuiThongBao(HoiVienID, "4", "VACPA - Phê duyệt xin thôi gia nhập hội viên tổ chức tại trang tin điện tử VACPA", body);
                string msg = "";
                if (Email != "")
                {
                    if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Phê duyệt xin thôi gia nhập hội viên tổ chức tại trang tin điện tử VACPA", body, ref msg))
                    { }
                    else
                    {
                        isSendMail = false;
                    }
                }

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void btnThoaiDuyet_Click(String sDonHoiVienID, ref bool isSendMail)
    {

        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                string TenDangNhap = "";
                string MatKhau = "";
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiDaPheDuyet,   cm.Admin_TenDangNhap, 
                    DateTime.Now.ToString("ddMMyyyy"), "",   ref Email);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT TenDoanhNghiep FROM tblHoiVienTapThe WHERE HoiVienTapTheID = (SELECT HoiVienID FROM tblXoaTenHoiVien WHERE LoaiHoiVien = '2' AND XoaTenHoiVienID = " + donHoiVienTtId + ")";

                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Thoái duyệt đơn xin xóa tên hội viên " + DataAccess.DLookup(cmd));

                // Send mail
                string body = "";
                body += "Công ty đã được phê duyệt kết nạp hội viên tổ chức, có thể sử dụng tài khoản sau để đăng nhập vào phần mềm:<BR/>";
                body += "Tên đăng nhập: <B>" + TenDangNhap + "</B><BR/>";
                body += "Mật khẩu: <B>" + MatKhau + "</B><BR/>";

                string msg = "";

                String thongbao = "";

                //if (SmtpMail.Send("BQT WEB VACPA", Email, "Thông tin tài khoản hội viên tổ chức tại trang tin điện tử VACPA", body, ref msg))
                //{    }
                //else
                //{
                //    isSendMail = false;
                    
                //}


            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    public bool UpdateStatus(int XoaTenHoiVienID, int TinhTrangID,  String NguoiDuyet,
                           String NgayDuyet, String LyDoTuChoi)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("XoaTenHoiVienID", XoaTenHoiVienID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));


            objDBHelper = ConnectionControllers.Connect("VACPA");
            return objDBHelper.ExecFunction("sp_xoatenhoivientt_tt_xetduyet", arrParams);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }
    public bool UpdateStatus(int XoaTenHoiVienID, int TinhTrangID,  String NguoiDuyet,
                               String NgayDuyet, String LyDoTuChoi,  ref string Email)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("XoaTenHoiVienID", XoaTenHoiVienID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));

          

            DBHelpers.DataParameter outEmail = new DBHelpers.DataParameter("Email", Email);
            outEmail.OutPut = true;
            arrParams.Add(outEmail);


            objDBHelper = ConnectionControllers.Connect("VACPA");
            bool bRedulte = objDBHelper.ExecFunction("sp_xoatenhoivientt_tt_xetduyet", arrParams);
          
            Email = outEmail.Param_Value.ToString();

            return bRedulte;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }



    protected void displayButton(object trangthai, String button)
    {


        if (!string.IsNullOrEmpty(button))
            {
                int iTangThai = 0;
                try { iTangThai = Convert.ToInt32(trangthai); }
                catch { }
                if (button== tiepnhan)// co quyen voi chuc nang 
                {
                    if (iTangThai == trangthaiChoTiepNhanID)
                    {
                        Response.Write(@" ");
                    }
                    else {
                        
                        Response.Write(@" style=""display: none;"" ");
                    }
                    
                }
                else if (button == tuchoiduyet)// co quyen voi chuc nang 
                {
                    if (iTangThai == trangthaiChoDuyetID)
                    {
                        Response.Write(@" ");
                    }
                    else
                    {

                        Response.Write(@" style=""display: none;"" ");
                    }


                } 
                else if (button == duyetdon)// co quyen voi chuc nang 
                {
                    if (iTangThai == trangthaiChoDuyetID)
                    {
                        Response.Write(@" ");
                    }
                    else
                    {

                        Response.Write(@" style=""display: none;"" ");
                    }

                }
            }
            else
            {

            }
        

    }

}