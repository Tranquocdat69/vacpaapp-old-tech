﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_QLHV_DanhSachCongTyDangKyDangLogo_Update : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý đăng logo của công ty";
    Db _db = new Db(ListName.ConnectionString);
    protected string _listPermissionOnFunc_DSCongTyDangLogo = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private string _id = "";
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        _id = Library.CheckNull(Request.QueryString["Id"]);
        _listPermissionOnFunc_DSCongTyDangLogo = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_QLHV_DSCongTyDangKyDangLogo, cm.connstr);
        try
        {
            _db.OpenConnection();
            if (Request.Form["hdAction"] != null && Request.Form["hdAction"] == "update")
                Save();
            if (Request.Form["hdAction"] != null && Request.Form["hdAction"] == "delete")
                Delete();
            if (!string.IsNullOrEmpty(_id))
                SpanCacNutChucNang.Visible = true;
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void LoadOneData()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            if (!string.IsNullOrEmpty(_id))
            {
                SqlTtDangKyLogoProvider pro = new SqlTtDangKyLogoProvider(ListName.ConnectionString, false, string.Empty);
                SqlHoiVienTapTheProvider proHVTT = new SqlHoiVienTapTheProvider(ListName.ConnectionString, false, string.Empty);
                TtDangKyLogo obj = pro.GetByDangKyLoGoId(Library.Int32Convert(_id));
                if (obj != null && obj.DangKyLoGoId > 0)
                {
                    string doiTuongDangKyLogo = obj.DoiTuong;
                    if (doiTuongDangKyLogo == "0")
                        js += "$('#rbtKhac').prop('checked', true);" + Environment.NewLine;
                    if (doiTuongDangKyLogo == "3" || doiTuongDangKyLogo == "4")
                    {
                        js += "$('#hdCongTyID').val('" + obj.HoiVienTapTheId + "');" + Environment.NewLine;
                        HoiVienTapThe objHVTT = proHVTT.GetByHoiVienTapTheId(Library.Int32Convert(obj.HoiVienTapTheId));
                        if (objHVTT != null && objHVTT.HoiVienTapTheId > 0)
                            js += "$('#txtMaCongTy').val('" + objHVTT.MaHoiVienTapThe + "');" + Environment.NewLine;
                    }
                    js += "$('#txtTenDayDu').val('" + obj.TenDayDu + "');" + Environment.NewLine;
                    js += "$('#txtTenVietTat').val('" + obj.TenVietTat + "');" + Environment.NewLine;
                    js += "$('#txtKichThuocLogo').val('" + obj.KichThuoc + "');" + Environment.NewLine;
                    js += "$('#txtViTri').val('" + obj.ViTri + "');" + Environment.NewLine;
                    js += "$('#txtLink').val('" + obj.Link + "');" + Environment.NewLine;
                    js += "$('#txtNgayDang').val('" + obj.NgayDang.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#txtNgayHetHan').val('" + obj.NgayHetHan.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#ddlPhiDangLogo').val('" + obj.PhiId + "');" + Environment.NewLine;
                    js += "$('#txtSoTien').val('" + obj.SoTien + "');" + Environment.NewLine;
                    js += "$('#txtHanNopPhi').val('" + obj.HanNop.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "CheckLoaiDoiTuongDangLogo();" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(obj.TenFileLogo))
                    {
                        js += "$('#spanTenFileLogo').html('" + obj.TenFileLogo + "');" + Environment.NewLine;
                        js += "$('#btnDownload').prop('href', '/admin.aspx?page=getfile&id=" + _id + "&type=" + ListName.Table_TTDangKyLogo + "');" + Environment.NewLine;
                        js += "$('#btnDownload').html('Tải về');" + Environment.NewLine;
                    }
                    SpanCacNutChucNang.Visible = true;
                    string statusCode = utility.GetStatusCodeById(obj.TinhTrangId.ToString(), _db).Trim();
                    js += "$('#spanTinhTrangPheDuyet').html('" + utility.GetStatusNameById(obj.TinhTrangId.ToString(), _db) + "');" + Environment.NewLine;
                    if (statusCode == ListName.Status_ChoDuyet || statusCode == ListName.Status_KhongPheDuyet || statusCode == ListName.Status_ThoaiDuyet)
                    {
                        js += "$('#" + btnDontApprove.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#spanTinhTrangPheDuyet').css('color', 'black');" + Environment.NewLine;
                    }
                    if (statusCode == ListName.Status_KhongPheDuyet)
                    {
                        js += "$('#" + btnReject.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + btnDontApprove.ClientID + "').remove();" + Environment.NewLine;
                        //js += "$('#spanTinhTrangPheDuyet').css('color', 'red');" + Environment.NewLine;
                    }
                    if (statusCode == ListName.Status_DaPheDuyet)
                    {
                        js += "$('#btnSave').remove();" + Environment.NewLine;
                        js += "$('#btnDelete').remove();" + Environment.NewLine;
                        js += "$('#" + btnApprove.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + btnReject.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#spanTinhTrangPheDuyet').css('color', 'green');" + Environment.NewLine;
                    }
                }
                else
                {
                    js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Không tìm thấy thông tin đăng ký. Đề nghị thử lại.\").show();" + Environment.NewLine;
                    js += "$('#" + Form1.ClientID + "').remove();" + Environment.NewLine;
                    SpanCacNutChucNang.Visible = false;
                }
            }
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Save()
    {
        bool success = false;

        HttpPostedFile fileLogo = Request.Files["FileLogo"];

        SqlTtDangKyLogoProvider pro = new SqlTtDangKyLogoProvider(ListName.ConnectionString, false, string.Empty);
        TtDangKyLogo obj;
        if (string.IsNullOrEmpty(_id)) // Insert
        {
            obj = new TtDangKyLogo();
        }
        else // Update
        {
            obj = pro.GetByDangKyLoGoId(Library.Int32Convert(_id));
        }
        obj.DoiTuong = Request.Form["DoiTuongDangLogo"];
        if (Request.Form["DoiTuongDangLogo"] == "3")
        {
            obj.HoiVienTapTheId = Library.Int32Convert(Request.Form["hdCongTyID"]);
            SqlHoiVienTapTheProvider proHVTT = new SqlHoiVienTapTheProvider(ListName.ConnectionString, false, string.Empty);
            HoiVienTapThe objHVTT = proHVTT.GetByHoiVienTapTheId(Library.Int32Convert(Request.Form["hdCongTyID"]));
            if(objHVTT != null && objHVTT.HoiVienTapTheId > 0)
            {
                if (objHVTT.LoaiHoiVienTapThe == "0" || objHVTT.LoaiHoiVienTapThe == "2")
                    obj.DoiTuong = "3";
                if (objHVTT.LoaiHoiVienTapThe == "1")
                    obj.DoiTuong = "4";
            }
        }
        obj.TenDayDu = Request.Form["txtTenDayDu"];
        obj.TenVietTat = Request.Form["txtTenVietTat"];
        obj.KichThuoc = Request.Form["txtKichThuocLogo"];
        obj.ViTri = Request.Form["txtViTri"];
        obj.Link = Request.Form["txtLink"];
        string phi = Request.Form["ddlPhiDangLogo"];
        if (!string.IsNullOrEmpty(phi))
            obj.PhiId = Library.Int32Convert(phi.Split(new string[] {";#"}, StringSplitOptions.None)[0]);
        obj.NgayDang = Library.DateTimeConvert(Request.Form["txtNgayDang"], "dd/MM/yyyy");
        obj.NgayHetHan = Library.DateTimeConvert(Request.Form["txtNgayHetHan"], "dd/MM/yyyy");
        obj.HanNop = Library.DateTimeConvert(Request.Form["txtHanNopPhi"], "dd/MM/yyyy");
        obj.SoTien = Library.DecimalConvert(Request.Form["txtSoTien"]);
        obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ChoDuyet, _db));
        if (fileLogo.ContentLength > 0)
        {
            BinaryReader br = new BinaryReader(fileLogo.InputStream);
            byte[] fileByte = br.ReadBytes(fileLogo.ContentLength);
            obj.FileLogo = fileByte;
            obj.TenFileLogo = fileLogo.FileName;
        }
        if (string.IsNullOrEmpty(_id)) // Insert
        {
            if (pro.Insert(obj))
                success = true;
        }
        else // Update
        {
            if (pro.Update(obj))
                success = true;
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        if (success)
        {
            // Đóng form mà refresh số liệu
            js += "parent.CloseFormUpdate();" + Environment.NewLine;
            js += "parent.Search();" + Environment.NewLine;
        }
        else
        {
            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Có lỗi trong qua trình thêm dữ liệu.\").show();" + Environment.NewLine;
        }
        js += "</script>";
        Page.RegisterStartupScript("UpdateSuccess", js);
    }

    protected void Delete()
    {
        bool success = false;
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        SqlTtDangKyLogoProvider pro = new SqlTtDangKyLogoProvider(ListName.ConnectionString, false, string.Empty);
        TtDangKyLogo obj = pro.GetByDangKyLoGoId(Library.Int32Convert(_id));
        if (obj != null && obj.DangKyLoGoId > 0)
        {
            string maTrangThai = utility.GetStatusCodeById(obj.TinhTrangId.ToString(), _db);
            if(maTrangThai == ListName.Status_DaPheDuyet)
            {
                js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Thông tin này đã được duyệt. Không được phép xóa.\").show();" + Environment.NewLine;
                js += "</script>";
                Page.RegisterStartupScript("DeleteSuccess", js);
            }
        }
        if (pro.Delete(Library.Int32Convert(_id)))
        {
            success = true;
        }
        
        if (success)
        {
            // Đóng form mà refresh số liệu
            js += "parent.CloseFormUpdate();" + Environment.NewLine;
            js += "parent.Search();" + Environment.NewLine;
        }
        else
        {
            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Có lỗi trong qua trình xóa dữ liệu.\").show();" + Environment.NewLine;
        }
        js += "</script>";
        Page.RegisterStartupScript("DeleteSuccess", js);
    }

    protected void GetListDMPhi()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            string query = "SELECT PhiID, TenPhi, MucPhi FROM tblTTDMPhiDangLogo ORDER BY TenPhi";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    js += "<option value='" + ht["PhiID"] + "'>" + ht["TenPhi"] + "</option>" + Environment.NewLine;
                }
            }
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void CheckPermissionOnPage()
    {
        LoadOneData();
        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains("XEM|"))
        {
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        }

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains("SUA|"))
            Response.Write("$('#btnSave').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains("XOA|"))
        {
            Response.Write("$('#btnDelete').remove();");
        }

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains("THEM|"))
            Response.Write("$('#btnSave').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains(ListName.PERMISSION_Duyet))
            Response.Write("$('#" + btnApprove.ClientID + "').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains(ListName.PERMISSION_TraLai))
            Response.Write("$('#" + btnReject.ClientID + "').remove();");

        if (!_listPermissionOnFunc_DSCongTyDangLogo.Contains(ListName.PERMISSION_ThoaiDuyet))
            Response.Write("$('#" + btnDontApprove.ClientID + "').remove();");

        string js = "";
        js += "var arrSoTienPhi = [];" + Environment.NewLine;
        try
        {
            _db.OpenConnection();
            string query = "SELECT PhiID, TenPhi, MucPhi FROM tblTTDMPhiDangLogo ORDER BY TenPhi";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                int index = 0;
                foreach (Hashtable ht in listData)
                {
                    js += "arrSoTienPhi[" + index + "] = ['" + ht["PhiID"] + "', '" + Library.FormatMoney(ht["MucPhi"]) + "'];" + Environment.NewLine;
                    index++;
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
        js += "CheckSoTienPhi();" + Environment.NewLine;
        Response.Write(js);
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        ChangeStatus(_id, ListName.Status_DaPheDuyet);
    }
    protected void btnReject_Click(object sender, EventArgs e)
    {
        ChangeStatus(_id, ListName.Status_KhongPheDuyet);
    }
    protected void btnDontApprove_Click(object sender, EventArgs e)
    {
        ChangeStatus(_id, ListName.Status_ThoaiDuyet);
    }

    private void ChangeStatus(string idDoan, string statusCode)
    {
        if (!string.IsNullOrEmpty(idDoan))
        {
            SqlTtDangKyLogoProvider pro = new SqlTtDangKyLogoProvider(ListName.ConnectionString, false, string.Empty);
            SqlTtPhatSinhPhiProvider proPhatSinhPhi = new SqlTtPhatSinhPhiProvider(ListName.ConnectionString, false, string.Empty);
            TtDangKyLogo obj = new TtDangKyLogo();
            obj = pro.GetByDangKyLoGoId(Library.Int32Convert(idDoan));
            try
            {
                _db.OpenConnection();
                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(statusCode, _db));

                if (pro.Update(obj))
                {
                    // Nếu là duyệt -> insert phát sinh phí
                    if (statusCode == ListName.Status_DaPheDuyet && (obj.DoiTuong == "3" || obj.DoiTuong == "4"))
                    {
                        TtPhatSinhPhi objPsp = new TtPhatSinhPhi();
                        objPsp.HoiVienId = obj.HoiVienTapTheId;
                        objPsp.LoaiHoiVien = obj.DoiTuong;
                        objPsp.DoiTuongPhatSinhPhiId = obj.DangKyLoGoId;
                        objPsp.LoaiPhi = "2";
                        objPsp.NgayPhatSinh = DateTime.Now;
                        objPsp.SoTien = obj.SoTien;
                        objPsp.PhiId = obj.PhiId;
                        proPhatSinhPhi.Insert(objPsp);
                    }
                    // Nếu là thoái duyệt -> delete phát sinh phí
                    if (statusCode == ListName.Status_ThoaiDuyet && (obj.DoiTuong == "3" || obj.DoiTuong == "4"))
                        DeletePhatSinhPhi(obj.DangKyLoGoId.ToString());
                    cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Cập nhật trạng thái bản ghi có ID \"" + obj.DangKyLoGoId + "\" thành " + utility.GetStatusNameById(statusCode, _db) + " của danh mục " + tenchucnang);
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    js += "jQuery('#thongbaothanhcong_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Cập nhật trạng thái thành công.\").show();" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("ApproveSuccess", js);
                }
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }
        }
    }

    private void DeletePhatSinhPhi(string id)
    {
        string command = "DELETE FROM tblTTPhatSinhPhi WHERE LopHocPhiID = " + id + " AND LoaiPhi = '2'";
        _db.ExecuteNonQuery(command);
    }
}