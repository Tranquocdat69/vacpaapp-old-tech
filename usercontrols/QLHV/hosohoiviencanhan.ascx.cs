﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using System.Drawing;
using DBHelpers;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
public partial class usercontrols_hosohoiviencanhan : System.Web.UI.UserControl
{
    String id = String.Empty;
    public Commons cm = new Commons();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    clsXacDinhQuyen quyen = new clsXacDinhQuyen();
    protected HoiVienFile FileDinhKem1 = new HoiVienFile();

    public HoiVienCaNhan hoivien;

    public bool QuyenXem { get; set; }
    public bool QuyenThem { get; set; }
    public bool QuyenSua { get; set; }
    public bool QuyenXoa { get; set; }
    public bool QuyenDuyet { get; set; }
    public bool QuyenTraLai { get; set; }
    public bool QuyenHuy { get; set; }

    int trangthaiChoTiepNhanID = 1;
    int trangthaiTuChoiID = 2;
    int trangthaiChoDuyetID = 3;
    int trangthaiTraLaiID = 4;
    int trangthaiDaPheDuyet = 5;
    int trangthaiThoaiDuyetID = 6;

    string tenchucnang = "Cập nhật hồ sơ hội viên cá nhân";
    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        // Tên chức năng
        litChonTab.Text = "";
        if (Request.QueryString["ktv"] == "1")
        {
            lbTitle.Text = "Cập nhật hồ sơ kiểm toán viên";
            tenchucnang = "Cập nhật hồ sơ kiểm toán viên";
            trLoaiHoiVien.Visible = false;
        }
        // Icon CSS Class  
        string IconClass = "iconfa-credit-card";

        if (!string.IsNullOrEmpty(cm.Admin_NguoiDungID))
        {
            string maquyen = quyen.fcnXDQuyen(int.Parse(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr);

            QuyenXem = (maquyen.Contains("XEM")) ? true : false;
            QuyenThem = (maquyen.Contains("THEM")) ? true : false;
            QuyenSua = (maquyen.Contains("SUA")) ? true : false;
            QuyenXoa = (maquyen.Contains("XOA")) ? true : false;
            QuyenDuyet = (maquyen.Contains("DUYET")) ? true : false;
            QuyenTraLai = (maquyen.Contains("TRALAI")) ? true : false;
            QuyenHuy = (maquyen.Contains("HUY")) ? true : false;
        }


        if (!IsPostBack)
        {

            SetInitialRow();
            SetInitialRow_ChungChi();
            SetInitialRow_KhenThuong();
            SetInitialRow_KyLuat();

            LoadDropDown();
            LoadDropDownNam(Request.QueryString["id"]);
            LoadDropDownNamCNKT(Request.QueryString["id"]);
            LoadThongTin();

            LoadThanhToan(Request.QueryString["id"]);

            LoadThanhVienKiemSoat(Request.QueryString["id"]);

            if (Request.QueryString["thongbao"] == "1")
            {
                string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

                placeMessage.Controls.Add(new LiteralControl(thongbao));
            }
            // setdataCrystalReport();
        }

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"
     <li>Biểu mẫu tờ khai <span class=""separator""></span></li> <!-- Nhóm chức năng cấp trên -->
     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));


        //}
        //catch (Exception ex)
        //{
        //    placeMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        //}
    }

    private void LoadDropDownNamCNKT(string ID)
    {
        string sql = @"SELECT MAX(YEAR(B.DenNgay)) + 1 AS Nam
FROM tblCNKTLopHocHocVien A INNER JOIN tblCNKTLopHoc B ON A.LopHocID = B.LopHocID 
WHERE A.HoiVienCaNhanID = " + ID + @"

UNION ALL

SELECT DISTINCT YEAR(B.TuNgay) AS Nam 
        FROM tblCNKTLopHocHocVien A INNER JOIN tblCNKTLopHoc B ON A.LopHocID = B.LopHocID 
        WHERE A.HoiVienCaNhanID = " + ID;

        sql += " ORDER BY Nam DESC";
        SqlCommand cmd = new SqlCommand(sql);

        DataTable dtb = new DataTable();

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        drChonNamCNKT.DataSource = dtb;
        drChonNamCNKT.DataBind();

        if (!string.IsNullOrEmpty(drChonNamCNKT.SelectedValue))
            LoadHoSoDaoTao(ID, drChonNamCNKT.SelectedValue);
    }

    private void LoadHoSoDaoTao(string ID, string Nam)
    {
        string sql = "SELECT C.MaChuyenDe, C.TenChuyenDe, CONVERT(VARCHAR, C.TuNgay, 103) + ' - ' + CONVERT(VARCHAR, C.DenNgay, 103) AS ThoiGian, B.TenLopHoc, dbo.LaySoGioDaoTao(" + ID + ", A.LopHocID, A.ChuyenDeID, '1') AS KT, dbo.LaySoGioDaoTao(" + ID + ", A.LopHocID, A.ChuyenDeID, '2') AS DD, dbo.LaySoGioDaoTao(" + ID + ", A.LopHocID, A.ChuyenDeID, '3') AS Khac";
        sql += " FROM tblCNKTLopHocHocVien A INNER JOIN tblCNKTLopHoc B ON A.LopHocID = B.LopHocID ";
        sql += " INNER JOIN tblCNKTLopHocChuyenDe C ON A.ChuyenDeID = C.ChuyenDeID ";
        sql += " WHERE A.HoiVienCaNhanID = " + ID + " AND B.DenNgay >= '" + (int.Parse(Nam) - 1) + "-08-16' AND B.DenNgay <= '" + Nam + "-08-15'";
        DataSet ds = new DataSet();
        DataTable dtb_dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb_dt = ds.Tables[0];

        GridViewHelper helper = new GridViewHelper(this.gvDaoTao);
        helper.RegisterSummary("KT", SummaryOperation.Sum);
        helper.RegisterSummary("DD", SummaryOperation.Sum);
        helper.RegisterSummary("Khac", SummaryOperation.Sum);

        helper.RegisterGroup("TenLopHoc", true, true);
        helper.GroupHeader += new GroupEvent(helper_GroupHeader_DaoTao);
        helper.ApplyGroupSort();

        gvDaoTao.DataSource = dtb_dt;
        gvDaoTao.DataBind();


        for (int i = 0; i < gvDaoTao.Rows.Count; i++)
        {
            if (gvDaoTao.Rows[i].Cells[5].Text == "0")
                gvDaoTao.Rows[i].Cells[5].Text = "";
            if (gvDaoTao.Rows[i].Cells[6].Text == "0")
                gvDaoTao.Rows[i].Cells[6].Text = "";
            if (gvDaoTao.Rows[i].Cells[7].Text == "0")
                gvDaoTao.Rows[i].Cells[7].Text = "";
        }


        sql = "SELECT * FROM tblCNKTCoCauSoGioHocToiThieu WHERE NamBatDau <= " + DateTime.Now.Year + " AND NamKetThuc >= " + DateTime.Now.Year;
        ds = new DataSet();
        DataTable dtb = new DataTable();
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        string html = "";
        string htmlDT = "";
        html += "<script type=\"text/javascript\">";


        html += "var gvDaoTao = document.getElementById(\"gvDaoTao\");";
        html += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[0].innerHTML = 'Tổng số giờ đã cập nhật kiến thức:';";
        html += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[0].style.fontWeight = 'bold';";
        if (dtb_dt.Rows.Count != 0)
        {
            htmlDT += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[1].style.fontWeight = 'bold';";
            htmlDT += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[2].style.fontWeight = 'bold';";
            htmlDT += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[3].style.fontWeight = 'bold';";

            htmlDT += "var sogioconthieu_kt = 0;var sogioconthieu_dd = 0;var sogioconthieu_khac = 0;";
            if (dtb.Rows.Count != 0)
            {
                if (!string.IsNullOrEmpty(dtb.Rows[0]["KeToanKiemToan"].ToString()))
                    htmlDT += "sogioconthieu_kt = " + int.Parse(dtb.Rows[0]["KeToanKiemToan"].ToString()) + " - parseInt(gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[1].innerHTML);";

                if (!string.IsNullOrEmpty(dtb.Rows[0]["DaoDucNgheNghiep"].ToString()))
                    htmlDT += "sogioconthieu_dd = " + int.Parse(dtb.Rows[0]["DaoDucNgheNghiep"].ToString()) + " - parseInt(gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[2].innerHTML);";

                if (!string.IsNullOrEmpty(dtb.Rows[0]["Khac"].ToString()))
                    htmlDT += "sogioconthieu_khac = " + int.Parse(dtb.Rows[0]["Khac"].ToString()) + " - parseInt(gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[3].innerHTML);";

            }

            htmlDT += "var row = gvDaoTao.insertRow(gvDaoTao.rows.length);";

            htmlDT += "var cell1 = row.insertCell(0);";
            htmlDT += "var cell2 = row.insertCell(1);";
            htmlDT += "var cell3 = row.insertCell(2);";
            htmlDT += "var cell4 = row.insertCell(3);";
            htmlDT += "cell1.colSpan = 4;";
            htmlDT += "cell1.innerHTML = \"Số giờ cập nhật kiến thức còn thiếu:\";";

            htmlDT += "if(sogioconthieu_kt < 0) {";
            htmlDT += " sogioconthieu_khac = sogioconthieu_khac + sogioconthieu_kt;";
            htmlDT += " cell2.innerHTML = 0; }";
            htmlDT += "else";
            htmlDT += " cell2.innerHTML = sogioconthieu_kt.toString();";
            htmlDT += "cell2.style.textAlign = \"center\";";

            htmlDT += "if(sogioconthieu_dd < 0) {";
            htmlDT += " sogioconthieu_khac = sogioconthieu_khac + sogioconthieu_dd;";
            htmlDT += " cell3.innerHTML = 0; }";
            htmlDT += "else";
            htmlDT += " cell3.innerHTML = sogioconthieu_dd.toString();";
            htmlDT += "cell3.style.textAlign = \"center\";";

            htmlDT += "if(sogioconthieu_khac < 0)";
            htmlDT += " cell4.innerHTML = 0;";
            htmlDT += "else";
            htmlDT += " cell4.innerHTML = sogioconthieu_khac.toString();";
            htmlDT += "cell4.style.textAlign = \"center\";";

            htmlDT += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[0].style.fontWeight = 'bold';";
            htmlDT += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[1].style.fontWeight = 'bold';";
            htmlDT += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[2].style.fontWeight = 'bold';";
            htmlDT += "gvDaoTao.rows[gvDaoTao.rows.length - 1].cells[3].style.fontWeight = 'bold';";
        }
        html += htmlDT;
        html += "</script>";

        litgvDaoTao.Text = html;
        
        
    }

    protected void drNamCNKT_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(drChonNamCNKT.SelectedValue))
        {
            LoadHoSoDaoTao(Request.QueryString["id"], drChonNamCNKT.SelectedValue);

            string html = "";          
            html += "<script type=\"text/javascript\">";

            html += "var li1 = document.getElementById(\"li1\");";
            html += "var tab1 = document.getElementById(\"tab1\");";
            html += "var li2 = document.getElementById(\"li2\");";
            html += "var tab2 = document.getElementById(\"tab2\");";
            html += "var li3 = document.getElementById(\"li3\");";
            html += "var tab3 = document.getElementById(\"tab3\");";
            html += "var li4 = document.getElementById(\"li4\");";
            html += "var tab4 = document.getElementById(\"tab4\");";

            html += "li1.removeAttribute(\"class\");";
            html += "tab1.setAttribute(\"aria-hidden\", \"true\");";
            html += "tab1.style.display = 'none';";

            html += "li3.removeAttribute(\"class\");";
            html += "tab3.setAttribute(\"aria-hidden\", \"true\");";
            html += "tab3.style.display = 'none';";

            html += "li4.removeAttribute(\"class\");";
            html += "tab4.setAttribute(\"aria-hidden\", \"true\");";
            html += "tab4.style.display = 'none';";

            html += "li2.setAttribute(\"class\", \"ui-state-default ui-corner-top ui-tabs-active ui-state-active\");";
            html += "tab2.setAttribute(\"aria-hidden\", \"false\");";
            html += "tab2.style.display = 'block';";

            html += "</script>";
            litChonTab.Text = html;

        }
    }

    private void LoadThanhToan(string ID)
    {
        // phí hội viên
        string sql = "SELECT YEAR(A.NgayPhatSinh) AS Nam, B.TenPhi AS LoaiPhi, ISNULL(A.SoTien, 0) AS SoTienPhaiNop, ISNULL(C.SoTienNop, 0) AS SoTienDaNop, CONVERT(VARCHAR, D.NgayNop, 103) AS NgayNop, ISNULL(A.SoTien, 0) - ISNULL(C.SoTienNop,0) AS SoTienChuaNop";
        sql += " FROM tblTTPhatSinhPhi A INNER JOIN tblTTDMPhiHoiVien B ON A.PhiID = B.PhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhanChiTiet C ON A.PhatSinhPhiID = C.PhatSinhPhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhan D ON C.GiaoDichID = D.GiaoDichID ";
        sql += " WHERE A.LoaiPhi = '1' AND A.LoaiHoiVien NOT IN ('3','4') AND A.HoiVienID = " + ID;

        sql += " UNION ALL ";
        // phí đăng logo
        sql += "SELECT YEAR(A.NgayPhatSinh) AS Nam, B.TenPhi AS LoaiPhi, ISNULL(A.SoTien, 0) AS SoTienPhaiNop, ISNULL(C.SoTienNop, 0) AS SoTienDaNop, CONVERT(VARCHAR, D.NgayNop, 103) AS NgayNop, ISNULL(A.SoTien, 0) - ISNULL(C.SoTienNop,0) AS SoTienChuaNop";
        sql += " FROM tblTTPhatSinhPhi A INNER JOIN tblTTDMPhiDangLogo B ON A.PhiID = B.PhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhanChiTiet C ON A.PhatSinhPhiID = C.PhatSinhPhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhan D ON C.GiaoDichID = D.GiaoDichID ";
        sql += " WHERE A.LoaiPhi = '2' AND A.LoaiHoiVien NOT IN ('3','4') AND A.HoiVienID = " + ID;

        sql += " UNION ALL ";
        // phí khác
        sql += "SELECT YEAR(A.NgayPhatSinh) AS Nam, B.TenPhi AS LoaiPhi, ISNULL(A.SoTien, 0) AS SoTienPhaiNop, ISNULL(C.SoTienNop, 0) AS SoTienDaNop, CONVERT(VARCHAR, D.NgayNop, 103) AS NgayNop, ISNULL(A.SoTien, 0) - ISNULL(C.SoTienNop,0) AS SoTienChuaNop";
        sql += " FROM tblTTPhatSinhPhi A INNER JOIN tblTTDMPhiKhac B ON A.PhiID = B.PhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhanChiTiet C ON A.PhatSinhPhiID = C.PhatSinhPhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhan D ON C.GiaoDichID = D.GiaoDichID ";
        sql += " WHERE A.LoaiPhi = '3' AND A.LoaiHoiVien NOT IN ('3','4') AND A.HoiVienID = " + ID;

        sql += " UNION ALL ";
        // phí KSCL
        sql += "SELECT YEAR(A.NgayPhatSinh) AS Nam, N'Phí kiểm soát chất lượng' AS LoaiPhi, ISNULL(A.SoTien, 0) AS SoTienPhaiNop, ISNULL(C.SoTienNop, 0) AS SoTienDaNop, CONVERT(VARCHAR, D.NgayNop, 103) AS NgayNop, ISNULL(A.SoTien, 0) - ISNULL(C.SoTienNop,0) AS SoTienChuaNop";
        sql += " FROM tblTTPhatSinhPhi A INNER JOIN tblTTDMPhiKhac B ON A.PhiID = B.PhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhanChiTiet C ON A.PhatSinhPhiID = C.PhatSinhPhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhan D ON C.GiaoDichID = D.GiaoDichID ";
        sql += " WHERE A.LoaiPhi = '5' AND A.LoaiHoiVien NOT IN ('3','4') AND A.HoiVienID = " + ID;

        sql += " UNION ALL ";
        // phí CNKT
        sql += "SELECT YEAR(A.NgayPhatSinh) AS Nam, N'Phí cập nhật kiến thức (' + C.TenLopHoc + ' - Ngày ' + CONVERT(VARCHAR, C.TuNgay, 103) + ' đến ' + CONVERT(VARCHAR, C.DenNgay, 103) AS LoaiPhi, ISNULL(A.SoTien, 0) AS SoTienPhaiNop, ISNULL(D.SoTienNop, 0) AS SoTienDaNop, CONVERT(VARCHAR, E.NgayNop, 103) AS NgayNop, ISNULL(A.SoTien, 0) - ISNULL(D.SoTienNop,0) AS SoTienChuaNop";
        sql += " FROM tblTTPhatSinhPhi A INNER JOIN tblCNKTDangKyHocCaNhan B ON A.DoiTuongPhatSinhPhiID = B.DangKyHocCaNhanID ";
        sql += " LEFT JOIN tblCNKTLopHoc C ON B.LopHocID = C.LopHocID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhanChiTiet D ON A.PhatSinhPhiID = D.PhatSinhPhiID ";
        sql += " LEFT JOIN tblTTNopPhiCaNhan E ON D.GiaoDichID = E.GiaoDichID ";
        sql += " WHERE A.LoaiPhi = '4' AND A.LoaiHoiVien NOT IN ('3','4') AND A.HoiVienID = " + ID;

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        GridViewHelper helper = new GridViewHelper(this.gvThanhToan);
        helper.RegisterSummary("SoTienPhaiNop", SummaryOperation.Sum);
        helper.RegisterSummary("SoTienDaNop", SummaryOperation.Sum);
        helper.RegisterSummary("SoTienChuaNop", SummaryOperation.Sum);

        helper.RegisterGroup("Nam", true, true);
        helper.GroupHeader += new GroupEvent(helper_GroupHeader_ThanhToan);
        helper.ApplyGroupSort();

        gvThanhToan.DataSource = dtb;
        gvThanhToan.DataBind();

        for (int i = 0; i < gvThanhToan.Rows.Count; i++)
        {
            if (gvThanhToan.Rows[i].Cells[2].Text == "0")
                gvThanhToan.Rows[i].Cells[2].Text = "";
            if (gvThanhToan.Rows[i].Cells[3].Text == "0")
                gvThanhToan.Rows[i].Cells[3].Text = "";
            if (gvThanhToan.Rows[i].Cells[5].Text == "0")
                gvThanhToan.Rows[i].Cells[5].Text = "";
        }

        string html = "";
        html += "<script type=\"text/javascript\">";

        html += "var gvThanhToan = document.getElementById(\"gvThanhToan\");";
        html += "gvThanhToan.rows[gvThanhToan.rows.length - 1].cells[0].innerHTML = 'Tổng cộng:';";
        html += "gvThanhToan.rows[gvThanhToan.rows.length - 1].cells[0].style.fontWeight = 'bold';";
        html += "gvThanhToan.rows[gvThanhToan.rows.length - 1].cells[1].style.fontWeight = 'bold';";
        html += "gvThanhToan.rows[gvThanhToan.rows.length - 1].cells[2].style.fontWeight = 'bold';";
        html += "gvThanhToan.rows[gvThanhToan.rows.length - 1].cells[4].style.fontWeight = 'bold';";

        html += "</script>";

        litgvThanhToan.Text = html;
    }

    protected void gvDaoTao_OnDataBound(object sender, EventArgs e)
    {
        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        TableHeaderCell cell = new TableHeaderCell();
        cell.Text = "";
        cell.ColumnSpan = 4;

        row.Controls.Add(cell);

        cell = new TableHeaderCell();
        cell.Text = "Tổng số giờ CNKT đã tham dự";
        cell.ColumnSpan = 4;
        cell.HorizontalAlign = HorizontalAlign.Center;

        row.Controls.Add(cell);
        row.BorderWidth = 0;
        gvDaoTao.HeaderRow.Parent.Controls.AddAt(0, row);
    }

    private void helper_GroupHeader_DaoTao(string groupName, object[] values, GridViewRow row)
    {
        if (groupName == "TenLopHoc")
        {
            row.BackColor = Color.LightGray;
            row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
        }

    }

    protected void gvDaoTao_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    private void helper_GroupHeader_ThanhToan(string groupName, object[] values, GridViewRow row)
    {
        if (groupName == "Nam")
        {
            row.BackColor = Color.LightGray;
            row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
        }

    }

    protected void gvThanhToan_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    private void LoadDropDownNam(string ID)
    {
        string sql = "SELECT DISTINCT YEAR(B.NgayBaoCaoKT) AS Nam ";
        sql += " FROM tblHoiVienCaNhan A INNER JOIN tblKSCLBaoCaoKTKT B ON A.HoiVienCaNhanID = B.HoiVienCaNhanID";

        sql += " WHERE A.HoiVienCaNhanID = " + ID;
        sql += " ORDER BY Nam DESC";
        SqlCommand cmd = new SqlCommand(sql);

        DataTable dtb = new DataTable();

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        drNam.DataSource = dtb;
        drNam.DataBind();

        if (!string.IsNullOrEmpty(drNam.SelectedValue))
            LoadKiemSoat(ID, drNam.SelectedValue);
    }

    private void LoadThanhVienKiemSoat(string ID)
    {
        string sql = "SELECT YEAR(A.NgayLap) AS Nam, A.MaDoanKiemTra AS MaDoanKiemTra, CONVERT(VARCHAR, A.NgayLap, 103) AS NgayLapDoan, dbo.LayCongTyKiemTra(A.DoanKiemTraID) AS CTKT FROM tblKSCLDoanKiemTra A";
        sql += " INNER JOIN tblKSCLDoanKiemTraThanhVien B ON A.DoanKiemTraID = B.DoanKiemTraID";
        sql += " WHERE B.HoiVienCaNhanID = " + ID;
        SqlCommand cmd = new SqlCommand(sql);

        DataTable dtb = new DataTable();

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        gvThanhVien.DataSource = dtb;
        gvThanhVien.DataBind();
    }

    protected void drNam_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(drNam.SelectedValue))
            LoadKiemSoat(ID, drNam.SelectedValue);
    }

    private void LoadKiemSoat(string ID, string Nam)
    {
        string sql = "SELECT '" + Nam + "' AS Nam, B.TenKhachHangKT AS HoSo, CASE XepLoai WHEN '1' THEN 'x' ELSE '' END AS Tot, CASE XepLoai WHEN '2' THEN 'x' ELSE '' END AS DatYeuCau, CASE XepLoai WHEN '3' THEN 'x' ELSE '' END AS KhongDat, CASE XepLoai WHEN '4' THEN 'x' ELSE '' END AS YeuKem ";
        sql += " FROM tblHoiVienCaNhan A INNER JOIN tblKSCLBaoCaoKTKT B ON A.HoiVienCaNhanID = B.HoiVienCaNhanID";

        sql += " WHERE A.HoiVienCaNhanID = " + ID + " AND YEAR(B.NgayBaoCaoKT) = " + Nam;
        sql += " ORDER BY Nam DESC";
        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        GridViewHelper helper = new GridViewHelper(this.gvKiemSoat);

        helper.RegisterGroup("Nam", true, true);
        helper.GroupHeader += new GroupEvent(helper_GroupHeader_KiemSoat);
        helper.ApplyGroupSort();

        gvKiemSoat.DataSource = dtb;
        gvKiemSoat.DataBind();

        string html = "";
        html += "<script type=\"text/javascript\">";

        html += "var gvKiemSoat = document.getElementById(\"gvKiemSoat\");";

        html += "var row1 = gvKiemSoat.insertRow(" + (gvKiemSoat.Rows.Count + 3).ToString() + ");";
        html += "var cell1 = row1.insertCell(0);";
        html += "cell1.colSpan = 6;";
        html += "cell1.innerHTML = \"Tổng số báo cáo kiểm toán được kiểm tra: " + dtb.Rows.Count + "\";";

        html += "var row2 = gvKiemSoat.insertRow(" + (gvKiemSoat.Rows.Count + 4).ToString() + ");";
        html += "var cell2 = row2.insertCell(0);";
        html += "cell2.colSpan = 6;";
        html += "cell2.innerHTML = \"Tổng số báo cáo kiểm toán ký trong năm: " + dtb.Rows.Count + "\";";

        html += "var row3 = gvKiemSoat.insertRow(" + (gvKiemSoat.Rows.Count + 5).ToString() + ");";
        html += "var cell3 = row3.insertCell(0);";
        html += "cell3.colSpan = 6;";
        html += "cell3.innerHTML = \"Số lượng báo cáo kiểm toán cho khách hàng là đơn vị có lợi ích công chúng thuộc lĩnh vực chứng khoán: " + dtb.Rows.Count + "\";";

        html += "gvKiemSoat.rows[" + (gvKiemSoat.Rows.Count + 3).ToString() + "].cells[0].style.fontWeight = 'bold';";
        html += "gvKiemSoat.rows[" + (gvKiemSoat.Rows.Count + 4).ToString() + "].cells[0].style.fontWeight = 'bold';";
        html += "gvKiemSoat.rows[" + (gvKiemSoat.Rows.Count + 5).ToString() + "].cells[0].style.fontWeight = 'bold';";

        html += "</script>";

        litgvKiemSoat.Text = html;


    }

    protected void gvKiemSoat_OnDataBound(object sender, EventArgs e)
    {
        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        TableHeaderCell cell = new TableHeaderCell();
        cell.Text = "";
        cell.ColumnSpan = 2;

        row.Controls.Add(cell);

        cell = new TableHeaderCell();
        cell.Text = "Kết quả kiểm tra hồ sơ";
        cell.ColumnSpan = 4;
        cell.HorizontalAlign = HorizontalAlign.Center;

        row.Controls.Add(cell);
        row.BorderWidth = 0;
        gvKiemSoat.HeaderRow.Parent.Controls.AddAt(0, row);
    }

    private void helper_GroupHeader_KiemSoat(string groupName, object[] values, GridViewRow row)
    {
        if (groupName == "Nam")
        {
            row.BackColor = Color.LightGray;
            row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
        }

    }

    protected void gvKiemSoat_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    private void LoadThongTin()
    {
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");

        SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connStr, true, "");
        SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
        SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connStr, true, "");

        hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));
        if (hoivien.LoaiHoiVienCaNhanChiTiet != null)
            drLoaiHoiVien.SelectedValue = hoivien.LoaiHoiVienCaNhanChiTiet.Value.ToString();
        txtTenDangNhap.Text = hoivien.MaHoiVienCaNhan;
        //SqlCommand cmd = new SqlCommand();
        //cmd.CommandText = "SELECT MatKhau FROM tblNguoiDung WHERE LoaiHoiVien IN ('0','1','2') AND HoiVienID = " + Request.QueryString["id"];
        ////txtMatKhau.Text = DataAccess.DLookup(cmd);
        //txtMatKhau.Attributes.Add("value", DataAccess.DLookup(cmd));

        lbHoiVienCaNhanID.Text = hoivien.HoiVienCaNhanId.ToString();
        if (hoivien.LoaiHoiVienCaNhan == "0" || hoivien.LoaiHoiVienCaNhan == "1")
            chkKTV.Visible = true;
        if (hoivien.LoaiHoiVienCaNhan == "2")
        {
            chkHVCN.Visible = true;
            lbTitle.Text = "Cập nhật hồ sơ kiểm toán viên";
            tenchucnang = "Cập nhật hồ sơ kiểm toán viên";
        }

        txtHoDem.Text = hoivien.HoDem;
        txtTen.Text = hoivien.Ten;
        if (hoivien.GioiTinh == "1") rdNam.Checked = true; else rdNu.Checked = true;
        if (hoivien.NgaySinh != null)
            NgaySinh.Value = hoivien.NgaySinh.Value.ToString("dd/MM/yyyy");
        if (hoivien.QuocTichId != null)
            drQuocTich.SelectedValue = hoivien.QuocTichId.Value.ToString();
        txtDiaChi.Text = hoivien.DiaChi;
        txtSoGiayDHKN.Text = hoivien.SoGiayChungNhanDkhn;
        if (hoivien.NgayCapGiayChungNhanDkhn != null)
            NgayCap_DHKN.Value = hoivien.NgayCapGiayChungNhanDkhn.Value.ToString("dd/MM/yyyy");
        if (hoivien.HanCapTu != null)
            HanCapTu.Value = hoivien.HanCapTu.Value.ToString("dd/MM/yyyy");
        if (hoivien.HanCapDen != null)
            HanCapDen.Value = hoivien.HanCapDen.Value.ToString("dd/MM/yyyy");

        txtSoChungChiKTV.Text = hoivien.SoChungChiKtv;
        if (hoivien.NgayCapChungChiKtv != null)
            NgayCap_ChungChiKTV.Value = hoivien.NgayCapChungChiKtv.Value.ToString("dd/MM/yyyy");

        txtEmail.Text = hoivien.Email;
        txtDienThoai.Text = hoivien.DienThoai;
        txtMobile.Text = hoivien.Mobile;
        if (hoivien.ChucVuId != null)
            drChucVu.SelectedValue = hoivien.ChucVuId.Value.ToString();
        if (hoivien.TruongDaiHocId != null)
            drTotNghiep.SelectedValue = hoivien.TruongDaiHocId.Value.ToString();
        if (hoivien.ChuyenNganhDaoTaoId != null)
            drChuyenNganh.SelectedValue = hoivien.ChuyenNganhDaoTaoId.Value.ToString();
        txtNam_ChuyenNganh.Text = hoivien.ChuyenNganhNam;
        if (hoivien.HocViId != null)
            drHocVi.SelectedValue = hoivien.HocViId.Value.ToString();
        txtNam_HocVi.Text = hoivien.HocViNam;
        if (hoivien.HocHamId != null)
            drHocHam.SelectedValue = hoivien.HocHamId.Value.ToString();
        txtNam_HocHam.Text = hoivien.HocHamNam;
        txtSoCMND.Text = hoivien.SoCmnd;
        if (hoivien.CmndNgayCap != null)
            NgayCap_CMND.Value = hoivien.CmndNgayCap.Value.ToString("dd/MM/yyyy");
        if (hoivien.CmndTinhId != null)
            drNoiCap_CMND.SelectedValue = hoivien.CmndTinhId.Value.ToString();
        txtSoTKNganHang.Text = hoivien.SoTaiKhoanNganHang;
        if (hoivien.NganHangId != null)
            drNganHang.SelectedValue = hoivien.NganHangId.Value.ToString();
        if (hoivien.HoiVienTapTheId != null)
            drDonVi.SelectedValue = hoivien.HoiVienTapTheId.Value.ToString();
        else
            txtDonViCongTac.Text = hoivien.DonViCongTac;
        if (hoivien.SoThichId != null)
            drSoThich.SelectedValue = hoivien.SoThichId.Value.ToString();

        txtSoQuyetDinh.Text = hoivien.SoQuyetDinhKetNap;
        txtNgayQuyetDinh.Text = (hoivien.NgayQuyetDinhKetNap != null) ? hoivien.NgayQuyetDinhKetNap.Value.ToString("dd/MM/yyyy") : "";

        // chứng chỉ
        LoadChungChi();

        // quá trình làm việc
        LoadQuaTrinh();

        LoadKhenThuongKyLuat(hoivien.HoiVienCaNhanId.ToString());

        // file
        loaduploadFileDinhKem(hoivien.HoiVienCaNhanId);

        if (Request.QueryString["mode"] == "view")
        {
            Response.Write("$('#form_nhaphoso input,select,textarea').attr('disabled', true);");
        }
    }

    private void LoadKhenThuongKyLuat(string ID)
    {
        string sql = "SELECT CONVERT(VARCHAR, A.NgayThang, 103) AS NgayThang, A.HinhThucKhenThuongID, A.LyDo, B.TenHinhThucKhenThuong AS TenHinhThuc FROM tblKhenThuongKyLuat A INNER JOIN tblDMHinhThucKhenThuong B ON A.HinhThucKhenThuongID = B.HinhThucKhenThuongID";

        sql += " WHERE Loai = 1 AND HoiVienCaNhanID = " + ID;
        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        if (dtb.Rows.Count != 0)
        {
            if (ViewState["KhenThuong"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KhenThuong"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = row["NgayThang"].ToString();
                    drCurrentRow["HinhThucID"] = row["HinhThucKhenThuongID"].ToString();
                    drCurrentRow["LyDo"] = row["LyDo"].ToString();
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["KhenThuong"] = dtCurrentTable;

                gvKhenThuong.DataSource = dtCurrentTable;
                gvKhenThuong.DataBind();

                SetPreviousData_KhenThuong();
            }
        }

        ds.Clear();
        dtb.Clear();

        sql = "SELECT CONVERT(VARCHAR, A.NgayThang, 103) AS NgayThang, A.HinhThucKyLuatID, A.LyDo, B.TenHinhThucKyLuat AS TenHinhThuc FROM tblKhenThuongKyLuat A INNER JOIN tblDMHinhThucKyLuat B ON A.HinhThucKyLuatID = B.HinhThucKyLuatID";

        sql += " WHERE Loai = 2 AND HoiVienCaNhanID = " + ID;
        ds = new DataSet();
        dtb = new DataTable();
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        if (dtb.Rows.Count != 0)
        {
            if (ViewState["KyLuat"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KyLuat"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = row["NgayThang"].ToString();
                    drCurrentRow["HinhThucID"] = row["HinhThucKyLuatID"].ToString();
                    drCurrentRow["LyDo"] = row["LyDo"].ToString();
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["KyLuat"] = dtCurrentTable;

                gvKyLuat.DataSource = dtCurrentTable;
                gvKyLuat.DataBind();

                SetPreviousData_KyLuat();
            }
        }
    }

    protected void loaduploadFileDinhKem(int donHoiVienId)
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            string DoiTuong = (Request.QueryString["ktv"] == "1") ? "2" : "1";
            SqlCommand sql = new SqlCommand();

            if (donHoiVienId == 0)
            {
                sql.CommandText = " SELECT  LoaiGiayToID ,TenFile as TenBieuMau ,DoiTuong ,BatBuoc  , 0 as FileID  , ' ' as TenFileDinhKem " +
                                  " FROM tblDMLoaiGiayTo  " +
                                  " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuong + "' order by tblDMLoaiGiayTo.TenFile   ";
            }
            else
            {
                sql.CommandText =
                "    SELECT   LoaiGiayToID ,TenFile as TenBieuMau  ,DoiTuong  ,BatBuoc , " +
                              "   ( SELECT TOP 1 ISNULL ( tblHoiVienFile.FileID,0)  FROM  tblHoiVienFile      WHERE  tblHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblHoiVienFile.HoiVienCaNhanID= " + donHoiVienId + "  ) as FileID ," +
                               " ( SELECT  TOP 1 ISNULL(tblHoiVienFile.TenFile,' ')   FROM  tblHoiVienFile  WHERE  tblHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblHoiVienFile.HoiVienCaNhanID= " + donHoiVienId + " ) as  TenFileDinhKem    " +
                " FROM tblDMLoaiGiayTo  " +
                " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuong + "' order by tblDMLoaiGiayTo.TenFile  ";


            }

            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;


            PagedDataSource objPds1 = new PagedDataSource();
            objPds1.DataSource = ds.Tables[0].DefaultView;

            FileDinhKem_grv.DataSource = objPds1;
            FileDinhKem_grv.DataBind();
            //FileDinhKem_grv.Columns[3].Visible = false; 
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;

            // set validate

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                String sTenLoaiFile = row.Cells[0].Text.Trim();
                String sLoaiFileID = row.Cells[3].Text.Trim();
                String sFileDinhKemID = row.Cells[4].Text.Trim();
                String sBatbuoc = row.Cells[5].Text.Trim();

                String sTenFile = row.Cells[6].Text.Trim();

                // check validate
                if (sBatbuoc == "0" || !String.IsNullOrEmpty(sTenFile))
                {
                    //((RequiredFieldValidator)row.FindControl("RequiredFieldFileUpload")).Visible = false;


                }

                // an hien chu thich bat buoc
                if (sBatbuoc == "0")
                {
                    ((Label)row.FindControl("requred")).Visible = false;
                    //((Label)row.FindControl("requred")).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                }
                else
                {
                    //((Label)row.FindControl("requred")).Text = "(*) ";
                }

                //// an hien link bieu mau

                //if (String.IsNullOrEmpty(sBieuMau))
                //{
                //    //  ((HyperLink)row.FindControl("linkBieuMau")).Visible = false;
                //    HyperLink likBM = ((HyperLink)row.FindControl("linkBieuMau"));// 

                //    likBM.Text = "&nbsp; &nbsp; &nbsp;";
                //    likBM.NavigateUrl = "";

                //}

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }




    }

    //private void LoadTenFile()
    //{
    //    SqlCommand cmd = new SqlCommand();
    //    cmd.CommandText = "SELECT A.TenFile, A.LoaiGiayToID FROM tblDonHoiVienFile A WHERE A.DonHoiVienCaNhanID = " + Request.QueryString["id"];

    //    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

    //    foreach (DataRow row in dtb.Rows)
    //    {
    //        switch (row["LoaiGiayToID"].ToString())
    //        {
    //            case "1":
    //                lbFileAnh.Text = row["TenFile"].ToString();
    //                break;
    //            case "2":
    //                lbFileQuyetDinh.Text = row["TenFile"].ToString();
    //                break;
    //            case "3":
    //                lbFileBanSao.Text = row["TenFile"].ToString();
    //                break;
    //            case "4":
    //                lbFileChuKy.Text = row["TenFile"].ToString();
    //                break;
    //            case "5":
    //                lbFileGiayChungNhan.Text = row["TenFile"].ToString();
    //                break;
    //            case "6":
    //                lbFileGiayToKhac.Text = row["TenFile"].ToString();
    //                break;
    //            case "7":
    //                lbFileDonXinGiaNhap.Text = row["TenFile"].ToString();
    //                break;
    //            default:
    //                break;
    //        }
    //    }
    //}

    private void LoadQuaTrinh()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT A.ThoiGianCongTac, A.ChucVu, A.NoiLamViec FROM tblHoiVienCaNhanQuaTrinhCongTac A WHERE A.HoiVienCaNhanID = " + Request.QueryString["id"];

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb.Rows.Count != 0)
        {
            if (ViewState["QuaTrinh"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["QuaTrinh"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["ThoiGian"] = row["ThoiGianCongTac"].ToString();
                    drCurrentRow["ChucVu"] = row["ChucVu"].ToString();
                    drCurrentRow["NoiLamViec"] = row["NoiLamViec"].ToString();

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["QuaTrinh"] = dtCurrentTable;

                gvQuaTrinh.DataSource = dtCurrentTable;
                gvQuaTrinh.DataBind();

                SetPreviousData();
            }
        }
    }

    private void LoadChungChi()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT A.SoChungChi, A.NgayCap FROM tblHoiVienCaNhanChungChiQuocTe A WHERE A.HoiVienCaNhanID = " + Request.QueryString["id"];

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb.Rows.Count != 0)
        {
            if (ViewState["ChungChi"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["ChungChi"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["SoChungChi"] = row["SoChungChi"].ToString();
                    if (!string.IsNullOrEmpty(row["NgayCap"].ToString()))
                        drCurrentRow["NgayCap"] = DateTime.Parse(row["NgayCap"].ToString()).ToShortDateString();

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["ChungChi"] = dtCurrentTable;

                gvChungChi.DataSource = dtCurrentTable;
                gvChungChi.DataBind();

                SetPreviousData_ChungChi();
            }
        }
    }

    public void LoadDropDown()
    {

        DataTable dtb = new DataTable();

        string sql = "";

        // Đơn vị
        sql = "SELECT 0 AS HoiVienTapTheID, N'<< Khác >>' AS TenDoanhNghiep, '-' AS SoHieu UNION ALL (SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE '--- ' + ISNULL(A.SoHieu,LTRIM(RTRIM(A.MaHoiVienTapThe))) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID) ORDER BY SoHieu";
        SqlCommand cmd = new SqlCommand(sql);

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drDonVi.DataSource = dtb;
        drDonVi.DataBind();

        dtb.Clear();
        // Quốc tịch
        sql = "SELECT * FROM tblDMQuocTich";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drQuocTich.DataSource = dtb;
        drQuocTich.DataBind();

        dtb.Clear();
        // Trường ĐH
        sql = "SELECT 0 AS TruongDaiHocID, N'<< Lựa chọn >>' AS TenTruongDaiHoc UNION ALL (SELECT TruongDaiHocID,TenTruongDaiHoc FROM tblDMTruongDaiHoc)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drTotNghiep.DataSource = dtb;
        drTotNghiep.DataBind();


        dtb.Clear();
        // Chuyên ngành
        sql = "SELECT 0 AS ChuyenNganhDaoTaoID, N'<< Lựa chọn >>' AS TenChuyenNganhDaoTao UNION ALL (SELECT ChuyenNganhDaoTaoID,TenChuyenNganhDaoTao FROM tblDMChuyenNganhDaoTao)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChuyenNganh.DataSource = dtb;
        drChuyenNganh.DataBind();

        dtb.Clear();
        // Học vị
        sql = "SELECT 0 AS HocViID, N'<< Lựa chọn >>' AS TenHocVi UNION ALL (SELECT HocViID, TenHocVi FROM tblDMHocVi)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHocVi.DataSource = dtb;
        drHocVi.DataBind();

        dtb.Clear();
        // Học hàm
        sql = "SELECT 0 AS HocHamID, N'<< Lựa chọn >>' AS TenHocHam UNION ALL (SELECT HocHamID, TenHocHam FROM tblDMHocHam)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHocHam.DataSource = dtb;
        drHocHam.DataBind();

        dtb.Clear();
        // Nơi cấp CMND
        sql = "SELECT * FROM tblDMTinh";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drNoiCap_CMND.DataSource = dtb;
        drNoiCap_CMND.DataBind();

        dtb.Clear();
        // Ngân hàng
        sql = "SELECT 0 AS NganHangID, N'<< Lựa chọn >>' AS TenNganHang UNION ALL (SELECT NganHangID, TenNganHang FROM tblDMNganHang)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drNganHang.DataSource = dtb;
        drNganHang.DataBind();

        dtb.Clear();
        // Load chức vụ
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChucVu.DataSource = dtb;
        drChucVu.DataBind();

        dtb.Clear();
        // Load sở thích
        sql = "SELECT 0 AS SoThichID, N'<< Lựa chọn >>' AS TenSoThich UNION ALL (SELECT SoThichID, TenSoThich FROM tblDMSoThich)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drSoThich.DataSource = dtb;
        drSoThich.DataBind();


    }

    #region GridKhenThuongKyLuat
    protected void ButtonAddKhenThuong_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_KhenThuong();
    }

    protected void ButtonAddKyLuat_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_KyLuat();
    }

    private void SetInitialRow_KhenThuong()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayThang", typeof(string)));
        dt.Columns.Add(new DataColumn("HinhThucID", typeof(string)));
        dt.Columns.Add(new DataColumn("LyDo", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["NgayThang"] = "";
        dr["HinhThucID"] = 0;
        dr["LyDo"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["KhenThuong"] = dt;

        gvKhenThuong.DataSource = dt;
        gvKhenThuong.DataBind();
    }
    private void AddNewRowToGrid_KhenThuong()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["KhenThuong"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KhenThuong"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        dtCurrentTable.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                        dtCurrentTable.Rows[i - 1]["HinhThucID"] = drHinhThuc.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["LyDo"] = lydo.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = "";
                    drCurrentRow["HinhThucID"] = 0;
                    drCurrentRow["LyDo"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["NgayThang"] = "";
                    dr["HinhThucID"] = 0;
                    dr["LyDo"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["KhenThuong"] = dtCurrentTable;

                gvKhenThuong.DataSource = dtCurrentTable;
                gvKhenThuong.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_KhenThuong();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_KhenThuong()
    {
        int rowIndex = 0;
        if (ViewState["KhenThuong"] != null)
        {
            DataTable dt = (DataTable)ViewState["KhenThuong"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    ngaythang.Text = dt.Rows[i]["NgayThang"].ToString();
                    drHinhThuc.SelectedValue = dt.Rows[i]["HinhThucID"].ToString();
                    lydo.Text = dt.Rows[i]["LyDo"].ToString();
                    rowIndex++;
                }
            }
        }
    }


    private void SetInitialRow_KyLuat()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayThang", typeof(string)));
        dt.Columns.Add(new DataColumn("HinhThucID", typeof(string)));
        dt.Columns.Add(new DataColumn("LyDo", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["NgayThang"] = "";
        dr["HinhThucID"] = 0;
        dr["LyDo"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["KyLuat"] = dt;

        gvKyLuat.DataSource = dt;
        gvKyLuat.DataBind();
    }
    private void AddNewRowToGrid_KyLuat()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["KyLuat"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KyLuat"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        dtCurrentTable.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                        dtCurrentTable.Rows[i - 1]["HinhThucID"] = drHinhThuc.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["LyDo"] = lydo.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = "";
                    drCurrentRow["HinhThucID"] = 0;
                    drCurrentRow["LyDo"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["NgayThang"] = "";
                    dr["HinhThucID"] = 0;
                    dr["LyDo"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["KyLuat"] = dtCurrentTable;

                gvKyLuat.DataSource = dtCurrentTable;
                gvKyLuat.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_KyLuat();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_KyLuat()
    {
        int rowIndex = 0;
        if (ViewState["KyLuat"] != null)
        {
            DataTable dt = (DataTable)ViewState["KyLuat"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    ngaythang.Text = dt.Rows[i]["NgayThang"].ToString();
                    drHinhThuc.SelectedValue = dt.Rows[i]["HinhThucID"].ToString();
                    lydo.Text = dt.Rows[i]["LyDo"].ToString();

                    rowIndex++;
                }
            }
        }
    }

    protected void gvKhenThuong_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvKhenThuong_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["KhenThuong"] != null)
        {
            DataTable dt = (DataTable)ViewState["KhenThuong"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    dt.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                    dt.Rows[i - 1]["HinhThucID"] = drHinhThuc.SelectedValue;
                    dt.Rows[i - 1]["LyDo"] = lydo.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["KhenThuong"] = dt;

            gvKhenThuong.DataSource = dt;
            gvKhenThuong.DataBind();

            SetPreviousData_KhenThuong();
        }
    }
    protected void gvKhenThuong_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drHinhThuc = (DropDownList)e.Row.FindControl("drHinhThuc");

                DataSet ds = new DataSet();
                DataTable dtb = new DataTable();

                string sql = "";

                sql = "SELECT 0 AS HinhThucID, N'<< Lựa chọn >>' AS TenHinhThuc UNION ALL (SELECT HinhThucKhenThuongID AS HinhThucID, TenHinhThucKhenThuong AS TenHinhThuc FROM tblDMHinhThucKhenThuong)";

                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                da.Fill(ds);
                dtb = ds.Tables[0];
                drHinhThuc.DataSource = dtb;
                drHinhThuc.DataBind();

            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void gvKyLuat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvKyLuat_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["KyLuat"] != null)
        {
            DataTable dt = (DataTable)ViewState["KyLuat"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    dt.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                    dt.Rows[i - 1]["HinhThucID"] = drHinhThuc.SelectedValue;
                    dt.Rows[i - 1]["LyDo"] = lydo.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["KyLuat"] = dt;

            gvKyLuat.DataSource = dt;
            gvKyLuat.DataBind();

            SetPreviousData_KyLuat();
        }
    }
    protected void gvKyLuat_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drHinhThuc = (DropDownList)e.Row.FindControl("drHinhThuc");

                DataSet ds = new DataSet();
                DataTable dtb = new DataTable();

                string sql = "";

                sql = "SELECT 0 AS HinhThucID, N'<< Lựa chọn >>' AS TenHinhThuc UNION ALL (SELECT HinhThucKyLuatID AS HinhThucID, TenHinhThucKyLuat AS TenHinhThuc FROM tblDMHinhThucKyLuat)";

                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                da.Fill(ds);
                dtb = ds.Tables[0];
                drHinhThuc.DataSource = dtb;
                drHinhThuc.DataBind();

            }
        }
        catch (Exception ex)
        {
        }
    }

    #endregion

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid();
    }

    protected void ButtonAddChungChi_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_ChungChi();
    }

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("ThoiGian", typeof(string)));
        dt.Columns.Add(new DataColumn("ChucVu", typeof(string)));
        dt.Columns.Add(new DataColumn("NoiLamViec", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["ThoiGian"] = "";
        dr["ChucVu"] = "";
        dr["NoiLamViec"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["QuaTrinh"] = dt;

        gvQuaTrinh.DataSource = dt;
        gvQuaTrinh.DataBind();
    }
    private void AddNewRowToGrid()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["QuaTrinh"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["QuaTrinh"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");

                        TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                        TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                        dtCurrentTable.Rows[i - 1]["ThoiGian"] = thoigian.Text;
                        dtCurrentTable.Rows[i - 1]["ChucVu"] = chucvu.Text;
                        dtCurrentTable.Rows[i - 1]["NoiLamViec"] = noilamviec.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["ThoiGian"] = "";
                    drCurrentRow["ChucVu"] = "";
                    drCurrentRow["NoiLamViec"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["ThoiGian"] = "";
                    dr["ChucVu"] = "";
                    dr["NoiLamViec"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["QuaTrinh"] = dtCurrentTable;

                gvQuaTrinh.DataSource = dtCurrentTable;
                gvQuaTrinh.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["QuaTrinh"] != null)
        {
            DataTable dt = (DataTable)ViewState["QuaTrinh"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                    thoigian.Text = dt.Rows[i]["ThoiGian"].ToString();
                    chucvu.Text = dt.Rows[i]["ChucVu"].ToString();
                    noilamviec.Text = dt.Rows[i]["NoiLamViec"].ToString();
                    rowIndex++;
                }
            }
        }
    }


    private void SetInitialRow_ChungChi()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));

        dt.Columns.Add(new DataColumn("SoChungChi", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayCap", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;

        dr["SoChungChi"] = "";
        dr["NgayCap"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["ChungChi"] = dt;

        gvChungChi.DataSource = dt;
        gvChungChi.DataBind();
    }
    private void AddNewRowToGrid_ChungChi()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["ChungChi"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["ChungChi"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values

                        TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                        TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                        dtCurrentTable.Rows[i - 1]["SoChungChi"] = sochungchi.Text;
                        dtCurrentTable.Rows[i - 1]["NgayCap"] = ngaycap.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;

                    drCurrentRow["SoChungChi"] = "";
                    drCurrentRow["NgayCap"] = "";

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;

                    dr["SoChungChi"] = "";
                    dr["NgayCap"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["ChungChi"] = dtCurrentTable;

                gvChungChi.DataSource = dtCurrentTable;
                gvChungChi.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_ChungChi();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_ChungChi()
    {
        int rowIndex = 0;
        if (ViewState["ChungChi"] != null)
        {
            DataTable dt = (DataTable)ViewState["ChungChi"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                    sochungchi.Text = dt.Rows[i]["SoChungChi"].ToString();
                    ngaycap.Text = dt.Rows[i]["NgayCap"].ToString();

                    rowIndex++;
                }
            }
        }
    }

    protected void gvQuaTrinh_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvQuaTrinh_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["QuaTrinh"] != null)
        {
            DataTable dt = (DataTable)ViewState["QuaTrinh"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                    dt.Rows[i - 1]["ThoiGian"] = thoigian.Text;

                    dt.Rows[i - 1]["ChucVu"] = chucvu.Text;
                    dt.Rows[i - 1]["NoiLamViec"] = noilamviec.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["QuaTrinh"] = dt;

            gvQuaTrinh.DataSource = dt;
            gvQuaTrinh.DataBind();

            SetPreviousData();
        }
    }


    protected void gvChungChi_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvChungChi_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["ChungChi"] != null)
        {
            DataTable dt = (DataTable)ViewState["ChungChi"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values

                    TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");


                    dt.Rows[i - 1]["SoChungChi"] = sochungchi.Text;
                    dt.Rows[i - 1]["NgayCap"] = ngaycap.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["ChungChi"] = dt;

            gvChungChi.DataSource = dt;
            gvChungChi.DataBind();

            SetPreviousData_ChungChi();
        }
    }

    protected void gvChungChi_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void CapnhatFile(int HoiVienId)
    {
        if (HoiVienId != null && HoiVienId != 0)
        {
            SqlHoiVienFileProvider HoiVien_provider = new SqlHoiVienFileProvider(connStr, true, "");

            HttpPostedFile styledfileupload;

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                FileUpload fileUp = (FileUpload)row.FindControl("FileUp");
                FileDinhKem1 = new HoiVienFile();
                if (fileUp != null && fileUp.FileName != "")
                {
                    int fileUploadId = 0;
                    int loaiFieldId = 0;
                    String sTempLoaiFileID = row.Cells[3].Text;
                    String sTempFileUploadID = row.Cells[4].Text;

                    if (!String.IsNullOrEmpty(sTempFileUploadID) && sTempFileUploadID != "0" && sTempFileUploadID != "&nbsp;")
                    {
                        fileUploadId = Convert.ToInt32(row.Cells[4].Text);

                    }
                    if (!String.IsNullOrEmpty(sTempLoaiFileID) && sTempLoaiFileID != "0" && sTempLoaiFileID != "&nbsp;")
                    {
                        loaiFieldId = Convert.ToInt32(row.Cells[3].Text);

                    }
                    string styledfileupload_name = "";
                    styledfileupload = fileUp.PostedFile;
                    if (styledfileupload.FileName != "")
                    {
                        styledfileupload_name = fileUp.FileName;


                        byte[] datainput = new byte[styledfileupload.ContentLength];
                        styledfileupload.InputStream.Read(datainput, 0, styledfileupload.ContentLength);

                        FileDinhKem1.FileId = fileUploadId;
                        FileDinhKem1.HoiVienCaNhanId = HoiVienId;
                        FileDinhKem1.LoaiGiayToId = loaiFieldId;
                        FileDinhKem1.FileDinhKem = datainput;
                        //FileDinhKem1.Tenfile = styledfileupload.FileName;
                        FileDinhKem1.TenFile = new System.IO.FileInfo(styledfileupload.FileName).Name;
                    }
                    if (FileDinhKem1.FileId == 0)
                    {
                        HoiVien_provider.Insert(FileDinhKem1);
                    }
                    else
                    {
                        HoiVien_provider.Update(FileDinhKem1);
                    }

                }
                else
                {

                }

            }


            loaduploadFileDinhKem(HoiVienId);

        }
    }


    protected void btnGhi_Click(object sender, EventArgs e)
    {
        conn.Open();

        string HoiVienCaNhanID = lbHoiVienCaNhanID.Text;

        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connStr, true, "");
        SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
        SqlKhenThuongKyLuatProvider KhenThuongKyLuat_provider = new SqlKhenThuongKyLuatProvider(connStr, true, "");
        SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connStr, true, "");

        string gioitinh = (rdNam.Checked) ? "1" : "0";

        string sql_hv = "SELECT NguoiDungID FROM tblNguoiDung WHERE LoaiHoiVien IN ('0','1','2') AND HoiVienID = " + HoiVienCaNhanID;
        SqlCommand cmd_hv = new SqlCommand(sql_hv, conn);
        if (cmd_hv.ExecuteScalar() != null)
        {
            SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connStr, true, "");

            NguoiDung user = NguoiDung_provider.GetByNguoiDungId(int.Parse(cmd_hv.ExecuteScalar().ToString()));
            user.TenDangNhap = txtTenDangNhap.Text;
            user.HoVaTen = txtHoDem.Text + " " + txtTen.Text;
            if (!string.IsNullOrEmpty(NgaySinh.Value))
                user.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);

            user.GioiTinh = gioitinh;
            user.Email = txtEmail.Text;
            user.DienThoai = txtDienThoai.Text;
            user.SoCmnd = txtSoCMND.Text;
            if (chkKTV.Checked)
                user.LoaiHoiVien = "2";
            if (chkHVCN.Checked)
                user.LoaiHoiVien = "1";
            NguoiDung_provider.Update(user);
        }

        // update bảng tblHoiVienCaNhan
        HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(HoiVienCaNhanID));
        string loaihoiviencu = (hoivien.LoaiHoiVienCaNhanChiTiet != null) ? hoivien.LoaiHoiVienCaNhanChiTiet.Value.ToString() : "-1";
        if (drLoaiHoiVien.SelectedValue != "-1" && drLoaiHoiVien.SelectedValue != "-2")
            hoivien.LoaiHoiVienCaNhanChiTiet = int.Parse(drLoaiHoiVien.SelectedValue);
        hoivien.MaHoiVienCaNhan = txtTenDangNhap.Text;
        hoivien.HoDem = txtHoDem.Text;
        hoivien.Ten = txtTen.Text;
        hoivien.GioiTinh = gioitinh;
        if (!string.IsNullOrEmpty(NgaySinh.Value))
            hoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
        hoivien.QuocTichId = int.Parse(drQuocTich.SelectedValue);

        if (!string.IsNullOrEmpty(Request.Form["TinhID_QueQuan"]))
            hoivien.QueQuanTinhId = Request.Form["TinhID_QueQuan"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["HuyenID_QueQuan"]))
            hoivien.QueQuanHuyenId = Request.Form["HuyenID_QueQuan"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["XaID_QueQuan"]))
            hoivien.QueQuanXaId = Request.Form["XaID_QueQuan"].ToString();

        hoivien.DiaChi = txtDiaChi.Text;
        if (!string.IsNullOrEmpty(Request.Form["TinhID_DiaChi"]))
            hoivien.DiaChiTinhId = Request.Form["TinhID_DiaChi"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["HuyenID_DiaChi"]))
            hoivien.DiaChiHuyenId = Request.Form["HuyenID_DiaChi"].ToString();
        if (!string.IsNullOrEmpty(Request.Form["XaID_DiaChi"]))
            hoivien.DiaChiXaId = Request.Form["XaID_DiaChi"].ToString();

        hoivien.SoGiayChungNhanDkhn = txtSoGiayDHKN.Text;
        if (!string.IsNullOrEmpty(NgayCap_DHKN.Value))
            hoivien.NgayCapGiayChungNhanDkhn = DateTime.ParseExact(NgayCap_DHKN.Value, "dd/MM/yyyy", null);
        if (!string.IsNullOrEmpty(HanCapTu.Value))
            hoivien.HanCapTu = DateTime.ParseExact(HanCapTu.Value, "dd/MM/yyyy", null);
        if (!string.IsNullOrEmpty(HanCapDen.Value))
            hoivien.HanCapDen = DateTime.ParseExact(HanCapDen.Value, "dd/MM/yyyy", null);

        hoivien.SoChungChiKtv = txtSoChungChiKTV.Text;
        if (!string.IsNullOrEmpty(NgayCap_ChungChiKTV.Value))
            hoivien.NgayCapChungChiKtv = DateTime.ParseExact(NgayCap_ChungChiKTV.Value, "dd/MM/yyyy", null);

        hoivien.Email = txtEmail.Text;
        hoivien.DienThoai = txtDienThoai.Text;
        hoivien.Mobile = txtMobile.Text;
        if (drChucVu.SelectedValue != "0")
            hoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
        if (drTotNghiep.SelectedValue != "0")
            hoivien.TruongDaiHocId = int.Parse(drTotNghiep.SelectedValue);
        if (drChuyenNganh.SelectedValue != "0")
            hoivien.ChuyenNganhDaoTaoId = int.Parse(drChuyenNganh.SelectedValue);
        hoivien.ChuyenNganhNam = txtNam_ChuyenNganh.Text;
        if (drHocVi.SelectedValue != "0")
            hoivien.HocViId = int.Parse(drHocVi.SelectedValue);
        hoivien.HocViNam = txtNam_HocVi.Text;
        if (drHocHam.SelectedValue != "0")
            hoivien.HocHamId = int.Parse(drHocHam.SelectedValue);
        hoivien.HocHamNam = txtNam_HocHam.Text;
        hoivien.SoCmnd = txtSoCMND.Text;
        if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
            hoivien.CmndNgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);
        hoivien.CmndTinhId = int.Parse(drNoiCap_CMND.SelectedValue);
        hoivien.SoTaiKhoanNganHang = txtSoTKNganHang.Text;
        if (drNganHang.SelectedValue != "0")
            hoivien.NganHangId = int.Parse(drNganHang.SelectedValue);
        if (drDonVi.SelectedValue != "0")
            hoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
        else
        {
            hoivien.HoiVienTapTheId = null;
            hoivien.DonViCongTac = txtDonViCongTac.Text;
        }
        if (drSoThich.SelectedValue != "0")
            hoivien.SoThichId = int.Parse(drSoThich.SelectedValue);

        hoivien.SoQuyetDinhKetNap = txtSoQuyetDinh.Text;
        if (!string.IsNullOrEmpty(txtNgayQuyetDinh.Text))
        {
            try
            {
                hoivien.NgayQuyetDinhKetNap = DateTime.ParseExact(txtNgayQuyetDinh.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }
        }

        hoivien.NgayCapNhat = DateTime.Now;
        hoivien.NguoiCapNhat = cm.Admin_TenDangNhap;
        if (chkKTV.Checked)
            hoivien.LoaiHoiVienCaNhan = "2";
        if (chkHVCN.Checked)
            hoivien.LoaiHoiVienCaNhan = "1";
        HoiVienCaNhan_provider.Update(hoivien);

        // update bảng đơn tblDonHoiVienCaNhan

        string sql = "SELECT DonHoiVienCaNhanId FROM tblDonHoiVienCaNhan WHERE HoiVienCaNhanId = " + hoivien.HoiVienCaNhanId;
        SqlCommand cmd = new SqlCommand(sql, conn);
        if (cmd.ExecuteScalar() != null)
        {
            SqlDonHoiVienCaNhanProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connStr, true, "");
            DonHoiVienCaNhan donhoivien = DonHoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(cmd.ExecuteScalar().ToString()));

            donhoivien.HoDem = txtHoDem.Text;
            donhoivien.Ten = txtTen.Text;
            donhoivien.GioiTinh = gioitinh;
            if (!string.IsNullOrEmpty(NgaySinh.Value))
                donhoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
            donhoivien.QuocTichId = int.Parse(drQuocTich.SelectedValue);

            if (!string.IsNullOrEmpty(Request.Form["TinhID_QueQuan"]))
                donhoivien.QueQuanTinhId = Request.Form["TinhID_QueQuan"].ToString();
            if (!string.IsNullOrEmpty(Request.Form["HuyenID_QueQuan"]))
                donhoivien.QueQuanHuyenId = Request.Form["HuyenID_QueQuan"].ToString();
            if (!string.IsNullOrEmpty(Request.Form["XaID_QueQuan"]))
                donhoivien.QueQuanXaId = Request.Form["XaID_QueQuan"].ToString();

            donhoivien.DiaChi = txtDiaChi.Text;
            if (!string.IsNullOrEmpty(Request.Form["TinhID_DiaChi"]))
                donhoivien.DiaChiTinhId = Request.Form["TinhID_DiaChi"].ToString();
            if (!string.IsNullOrEmpty(Request.Form["HuyenID_DiaChi"]))
                donhoivien.DiaChiHuyenId = Request.Form["HuyenID_DiaChi"].ToString();
            if (!string.IsNullOrEmpty(Request.Form["XaID_DiaChi"]))
                donhoivien.DiaChiXaId = Request.Form["XaID_DiaChi"].ToString();

            donhoivien.SoGiayChungNhanDkhn = txtSoGiayDHKN.Text;
            if (!string.IsNullOrEmpty(NgayCap_DHKN.Value))
                donhoivien.NgayCapGiayChungNhanDkhn = DateTime.ParseExact(NgayCap_DHKN.Value, "dd/MM/yyyy", null);
            if (!string.IsNullOrEmpty(HanCapTu.Value))
                donhoivien.HanCapTu = DateTime.ParseExact(HanCapTu.Value, "dd/MM/yyyy", null);
            if (!string.IsNullOrEmpty(HanCapDen.Value))
                donhoivien.HanCapDen = DateTime.ParseExact(HanCapDen.Value, "dd/MM/yyyy", null);

            donhoivien.SoChungChiKtv = txtSoChungChiKTV.Text;
            if (!string.IsNullOrEmpty(NgayCap_ChungChiKTV.Value))
                donhoivien.NgayCapChungChiKtv = DateTime.ParseExact(NgayCap_ChungChiKTV.Value, "dd/MM/yyyy", null);

            donhoivien.Email = txtEmail.Text;
            donhoivien.DienThoai = txtDienThoai.Text;
            donhoivien.Mobile = txtMobile.Text;
            if (drChucVu.SelectedValue != "0")
                donhoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
            if (drTotNghiep.SelectedValue != "0")
                donhoivien.TruongDaiHocId = int.Parse(drTotNghiep.SelectedValue);
            if (drChuyenNganh.SelectedValue != "0")
                donhoivien.ChuyenNganhDaoTaoId = int.Parse(drChuyenNganh.SelectedValue);
            donhoivien.ChuyenNganhNam = txtNam_ChuyenNganh.Text;
            if (drHocVi.SelectedValue != "0")
                donhoivien.HocViId = int.Parse(drHocVi.SelectedValue);
            donhoivien.HocViNam = txtNam_HocVi.Text;
            if (drHocHam.SelectedValue != "0")
                donhoivien.HocHamId = int.Parse(drHocHam.SelectedValue);
            donhoivien.HocHamNam = txtNam_HocHam.Text;
            donhoivien.SoCmnd = txtSoCMND.Text;
            if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
                donhoivien.CmndNgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);
            donhoivien.CmndTinhId = int.Parse(drNoiCap_CMND.SelectedValue);
            donhoivien.SoTaiKhoanNganHang = txtSoTKNganHang.Text;
            if (drNganHang.SelectedValue != "0")
                donhoivien.NganHangId = int.Parse(drNganHang.SelectedValue);
            if (drDonVi.SelectedValue != "0")
                donhoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
            else
                donhoivien.DonViCongTac = txtDonViCongTac.Text;
            if (drSoThich.SelectedValue != "0")
                donhoivien.SoThichId = int.Parse(drSoThich.SelectedValue);

            DonHoiVienCaNhan_provider.Update(donhoivien);
        }
        // insert chứng chỉ
        sql = "DELETE tblHoiVienCaNhanChungChiQuocTe WHERE HoiVienCaNhanId = " + HoiVienCaNhanID;
        cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        for (int i = 0; i < gvChungChi.Rows.Count; i++)
        {

            //DropDownList drChungChi = (DropDownList)gvChungChi.Rows[i].Cells[1].FindControl("drChungChi");
            TextBox sochungchi = (TextBox)gvChungChi.Rows[i].Cells[1].FindControl("txtSoChungChi");
            TextBox ngaycap = (TextBox)gvChungChi.Rows[i].Cells[2].FindControl("txtDate");

            //if (drChungChi.SelectedValue != "0")
            if (!string.IsNullOrEmpty(sochungchi.Text))
            {
                HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                chungchi.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);

                chungchi.SoChungChi = sochungchi.Text;
                if (!string.IsNullOrEmpty(ngaycap.Text))
                    chungchi.NgayCap = DateTime.ParseExact(ngaycap.Text, "dd/MM/yyyy", null);

                HoiVienCaNhanChungChi_provider.Insert(chungchi);
            }
        }


        // insert quá trình làm việc
        sql = "DELETE tblHoiVienCaNhanQuaTrinhCongTac WHERE HoiVienCaNhanId = " + HoiVienCaNhanID;
        cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        for (int i = 0; i < gvQuaTrinh.Rows.Count; i++)
        {

            TextBox thoigian = (TextBox)gvQuaTrinh.Rows[i].Cells[1].FindControl("txtThoiGian");
            TextBox chucvu = (TextBox)gvQuaTrinh.Rows[i].Cells[2].FindControl("txtChucVu");
            TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[i].Cells[3].FindControl("txtNoiLamViec");

            HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
            quatrinh.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);
            quatrinh.ThoiGianCongTac = thoigian.Text;
            quatrinh.ChucVu = chucvu.Text;
            quatrinh.NoiLamViec = noilamviec.Text;

            HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
        }

        // Khen thưởng
        sql = "DELETE tblKhenThuongKyLuat WHERE Loai = 1 AND HoiVienCaNhanId = " + HoiVienCaNhanID;
        cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        for (int i = 0; i < gvKhenThuong.Rows.Count; i++)
        {
            TextBox ngaythang = (TextBox)gvKhenThuong.Rows[i].Cells[1].FindControl("txtNgayThang");
            DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[i].Cells[2].FindControl("drHinhThuc");
            TextBox lydo = (TextBox)gvKhenThuong.Rows[i].Cells[3].FindControl("txtLyDo");

            if (drHinhThuc.SelectedValue != "0")
            {
                KhenThuongKyLuat khenthuong = new KhenThuongKyLuat();
                khenthuong.HinhThucKhenThuongId = int.Parse(drHinhThuc.SelectedValue);
                khenthuong.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);
                if (!string.IsNullOrEmpty(ngaythang.Text))
                    khenthuong.NgayThang = DateTime.ParseExact(ngaythang.Text, "dd/MM/yyyy", null);
                khenthuong.Loai = "1";
                khenthuong.LyDo = lydo.Text;

                KhenThuongKyLuat_provider.Insert(khenthuong);
            }
        }

        // Kỷ luật
        sql = "DELETE tblKhenThuongKyLuat WHERE Loai = 2 AND HoiVienCaNhanId = " + HoiVienCaNhanID;
        cmd = new SqlCommand(sql, conn);
        cmd.ExecuteNonQuery();
        for (int i = 0; i < gvKyLuat.Rows.Count; i++)
        {
            TextBox ngaythang = (TextBox)gvKyLuat.Rows[i].Cells[1].FindControl("txtNgayThang");
            DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[i].Cells[2].FindControl("drHinhThuc");
            TextBox lydo = (TextBox)gvKyLuat.Rows[i].Cells[3].FindControl("txtLyDo");

            if (drHinhThuc.SelectedValue != "0")
            {
                KhenThuongKyLuat kyluat = new KhenThuongKyLuat();
                kyluat.HinhThucKyLuatId = int.Parse(drHinhThuc.SelectedValue);
                kyluat.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);
                kyluat.Loai = "2";
                if (!string.IsNullOrEmpty(ngaythang.Text))
                    kyluat.NgayThang = DateTime.ParseExact(ngaythang.Text, "dd/MM/yyyy", null);

                kyluat.LyDo = lydo.Text;

                KhenThuongKyLuat_provider.Insert(kyluat);
            }
        }

        // file đính kèm
        CapnhatFile(int.Parse(HoiVienCaNhanID));

        // cập nhật phí nếu thay đổi loại hội viên
        if (loaihoiviencu != drLoaiHoiVien.SelectedValue)
        {
            // kiểm tra xem đã nộp phí hay chưa
            sql = @"SELECT GiaoDichChiTietID FROM tblTTNopPhiCaNhanChiTiet CT inner join tblTTNopPhiCaNhan GD on CT.GiaoDichID = GD.GiaoDichID
inner join tblTTDMPhiHoiVien P on CT.PhiID = P.PhiID  
WHERE CT.HoiVienCaNhanId = " + hoivien.HoiVienCaNhanId + @" AND CT.LoaiPhi = 1 AND P.DoiTuongApDungID = 1 and P.LoaiHoiVien = 0 and P.LoaiHoiVienChiTiet = " + loaihoiviencu + " and YEAR(P.NgayApDung) = YEAR(getDate()) and GD.TinhTrangID = 5";
            cmd = new SqlCommand(sql, conn);
            if (cmd.ExecuteScalar() == null) // nếu chưa nộp phí
            {
                sql = "DELETE tblTTPhatSinhPhi where HoiVienID = " + hoivien.HoiVienCaNhanId + " and LoaiHoiVien = 1 and LoaiPhi = 1 and PhiID IN (select PhiID from tblTTDMPhiHoiVien where DoiTuongApDungID = 1 and LoaiHoiVien = 0 and TinhTrangID = " + ((int)EnumVACPA.TinhTrang.DaPheDuyet).ToString() + " and LoaiHoiVienChiTiet = " + loaihoiviencu + " and YEAR(NgayApDung) = YEAR(getDate()))";
                cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();

                if (hoivien.NgayGiaNhap != null)
                    cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " AND B.TuNgay <= CONVERT(datetime, '" + hoivien.NgayGiaNhap.Value.ToString("dd/MM/yyyy") + "', 103) AND B.DenNgay >= CONVERT(datetime, '" + hoivien.NgayGiaNhap.Value.ToString("dd/MM/yyyy") + "', 103) ORDER BY A.PhiID DESC";
                else
                    cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*100) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                if (dtb.Rows.Count != 0)
                {

                    cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + hoivien.HoiVienCaNhanId + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                    DataAccess.RunActionCmd(cmd);
                }
                else
                {
                    cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi) AS MucPhi FROM tblTTDMPhiHoiVien A WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " ORDER BY A.PhiID DESC";
                    dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                    if (dtb.Rows.Count != 0)
                    {
                        cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + hoivien.HoiVienCaNhanId + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                        DataAccess.RunActionCmd(cmd);
                    }
                }
            }
        }

        cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Cập nhật hồ sơ hội viên " + hoivien.HoDem + " " + hoivien.Ten);

        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

        placeMessage.Controls.Add(new LiteralControl(thongbao));

        conn.Close();
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["ds"] == "xoaten")
            Response.Redirect("admin.aspx?page=xoatenhoiviencanhan");
        else
            Response.Redirect("admin.aspx?page=danhsachkhachhang");
    }

    public bool UpdateStatus(int XoaTenHoiVienID, int TinhTrangID, String NguoiDuyet,
                           String NgayDuyet, String LyDoTuChoi)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("XoaTenHoiVienID", XoaTenHoiVienID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));


            objDBHelper = ConnectionControllers.Connect("VACPA");
            return objDBHelper.ExecFunction("sp_xoatenhoiviencn_xetduyet", arrParams);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }

    public bool UpdateStatus(int XoaTenHoiVienID, int TinhTrangID, String NguoiDuyet,
                               String NgayDuyet, String LyDoTuChoi, ref string Email)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("XoaTenHoiVienID", XoaTenHoiVienID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));



            DBHelpers.DataParameter outEmail = new DBHelpers.DataParameter("Email", Email);
            outEmail.OutPut = true;
            arrParams.Add(outEmail);


            objDBHelper = ConnectionControllers.Connect("VACPA");
            bool bRedulte = objDBHelper.ExecFunction("sp_xoatenhoiviencn_xetduyet", arrParams);

            Email = outEmail.Param_Value.ToString();

            return bRedulte;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }

    protected void btnTiepNhan_Click(object sender, EventArgs e)
    {
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

        UpdateStatus(int.Parse(Request.QueryString["xoatenid"]), trangthaiChoDuyetID, "", "", "");
        cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Tiếp nhận đơn xin xóa tên hội viên " + hoivien.HoDem + " " + hoivien.Ten);
        Response.Redirect("admin.aspx?page=hosohoiviencanhan&ds=xoaten&id=" + Request.QueryString["id"] + "&xoatenid=" + Request.QueryString["xoatenid"] + "&tinhtrangid=3&act=view&thongbao=1");

    }

    protected void btnTuChoiTiepNhan_Click(object sender, EventArgs e)
    {
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

        string Email = "";
        UpdateStatus(int.Parse(Request.QueryString["xoatenid"]), trangthaiTuChoiID, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), txtLyDoTuChoi.Text, ref Email);

        // Send mail
        string body = "";
        body += "VACPA từ chối tiếp nhận xóa tên hội viên cá nhân của bạn<BR/>";
        body += "Lý do từ chối: " + txtLyDoTuChoi.Text + " <BR/>";

        string msg = "";
        if (Email != "")
        {
            if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối tiếp nhận đơn xin xóa tên hội viên cá nhân", body, ref msg))
            { }
            else
            {

            }
        }

        cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối tiếp nhận đơn xin xóa tên hội viên " + hoivien.HoDem + " " + hoivien.Ten);

        Response.Redirect("admin.aspx?page=hosohoiviencanhan&ds=xoaten&id=" + Request.QueryString["id"] + "&xoatenid=" + Request.QueryString["xoatenid"] + "&tinhtrangid=4&act=view&thongbao=1");
    }

    protected void btnDuyet_Click(object sender, EventArgs e)
    {
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

        string Email = "";
        UpdateStatus(int.Parse(Request.QueryString["xoatenid"]), trangthaiDaPheDuyet, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), "", ref Email);

        // Send mail
        string body = "";
        body += "Bạn đã được phê duyệt xin thôi gia nhập hội viên cá nhân, bạn vẫn có thể đăng nhập vào hệ thống và sử dụng phần mềm với người sử dụng là Kiểm toán viên<BR/>";

        string msg = "";
        if (Email != "")
        {
            if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Phê duyệt xin thôi gia nhập hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg))
            { }
        }

        cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin xóa tên hội viên " + hoivien.HoDem + " " + hoivien.Ten);
        Response.Redirect("admin.aspx?page=hosohoiviencanhan&ds=xoaten&id=" + Request.QueryString["id"] + "&xoatenid=" + Request.QueryString["xoatenid"] + "&tinhtrangid=5&act=view&thongbao=1");
    }

    protected void btnTuChoiDuyet_Click(object sender, EventArgs e)
    {
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

        string Email = "";

        UpdateStatus(int.Parse(Request.QueryString["xoatenid"]), trangthaiTraLaiID, cm.Admin_TenDangNhap,
                        DateTime.Now.ToString("ddMMyyyy"), txtLyDoTuChoi.Text, ref Email);

        // Send mail
        string body = "";
        body += "VACPA từ chối phê duyệt xóa tên hội viên cá nhân của bạn<BR/>";
        body += "Lý do từ chối: " + txtLyDoTuChoi.Text + " <BR/>";

        string msg = "";
        if (Email != "")
        {
            if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối duyệt yêu cầu xóa tên hội viên cá nhân", body, ref msg))
            { }
        }
        cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối duyệt đơn xin xóa tên hội viên " + hoivien.HoDem + " " + hoivien.Ten);
        Response.Redirect("admin.aspx?page=hosohoiviencanhan&ds=xoaten&id=" + Request.QueryString["id"] + "&xoatenid=" + Request.QueryString["xoatenid"] + "&tinhtrangid=2&act=view&thongbao=1");
    }

    protected void btnThoaiDuyet_Click(object sender, EventArgs e)
    {
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(int.Parse(Request.QueryString["id"]));

        string Email = "";
        UpdateStatus(int.Parse(Request.QueryString["xoatenid"]), trangthaiThoaiDuyetID, cm.Admin_TenDangNhap,
            DateTime.Now.ToString("ddMMyyyy"), "", ref Email);

        // Send mail
        string body = "";
        body += "VACPA đã thoái duyệt đơn xin thôi gia nhập hội viên cá nhân, bạn có thể sử dụng tài khoản để đăng nhập vào phần mềm với tư cách là hội viên cá nhân:<BR/>";

        string msg = "";

        if (SmtpMail.Send("BQT WEB VACPA", Email, "Thoái duyệt đơn xin thôi gia nhập hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg))
        { }

        cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Thoái duyệt đơn xin xóa tên hội viên " + hoivien.HoDem + " " + hoivien.Ten);
    }

    protected void btnKetXuat_Click(object sender, EventArgs e)
    {
        setdataCrystalReport();
    }

    public void setdataCrystalReport()
    {
        List<objGiayChungNhanHoiVienChinhThuc> lst = new List<objGiayChungNhanHoiVienChinhThuc>();


        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();

        //Lấy hoi vien ca nhan
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            sql.CommandText = @"select GioiTinh, (LTRIM(RTRIM(HoDem)) + ' ' + LTRIM(RTRIM(Ten))) as HoVaTen, 
SoChungChiKTV, NgayCapChungChiKTV, SoQuyetDinhKetNap, NgayQuyetDinhKetNap
from tblHoiVienCaNhan where hoiviencanhanid = " + Request.QueryString["id"];
            ds = DataAccess.RunCMDGetDataSet(sql);

            DataTable dt = ds.Tables[0];

            //objGiayChungNhanHoiVienChinhThuc obj = new objGiayChungNhanHoiVienChinhThuc();
            //obj.HoVaTen = "¤ng NguyÔn Quèc ¢n";
            //obj.SoCCKTV = "Chøng chØ KTV sè 2772/KTV do Bé Tµi chÝnh";
            //obj.NgayCap = "cÊp ngµy 15/4/2014 Lµ Héi viªn chÝnh thøc cña VACPA";
            //obj.HoVaTenTiengAnh = "Mr. Nguyen Quoc An";
            //obj.SoCCKTVTiengAnh = "Auditor Certificate No.2772/KTV dated April 15, 2014 by";
            //obj.NgayCapTiengAnh = "Ministry of Finance is a full Member of VACPA";
            //obj.NgayThangNamRaQuyetDinh = "Hµ Néi, 10 th¸ng 7 n¨m 2015";
            //obj.SoQuyetDinh = "Sè: 245-2015/Q§-VACPA";
            //lst.Add(obj);
            foreach (DataRow dtr in dt.Rows)
            {
                objGiayChungNhanHoiVienChinhThuc obj = new objGiayChungNhanHoiVienChinhThuc();
                if (dtr["GioiTinh"].ToString() == "1" || dtr["GioiTinh"].ToString() == "1")
                {
                    obj.NgayCapTiengAnh = "¤ng";
                    obj.HoVaTenTiengAnh = "Mr.";
                }
                else
                {
                    obj.NgayCapTiengAnh = "Bµ";
                    obj.HoVaTenTiengAnh = "Ms.";
                }
                obj.HoVaTen += " " + Converter.ConvertToTCVN3(dtr["HoVaTen"].ToString());

                obj.SoCCKTV = "Chøng chØ KTV sè " + Converter.ConvertToTCVN3(dtr["SoChungChiKTV"].ToString()) + " do Bé Tµi chÝnh";
                try
                {
                    obj.NgayCap = "cÊp ngµy " + Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).ToShortDateString() + " Lµ Héi viªn chÝnh thøc cña VACPA";
                }
                catch (Exception)
                {
                    obj.NgayCap = "cÊp ngµy " + " Lµ Héi viªn chÝnh thøc cña VACPA";
                }
                obj.HoVaTenTiengAnh += " " + RemoveSign4VietnameseString(dtr["HoVaTen"].ToString());
                obj.SoCCKTVTiengAnh = "Auditor Certificate No. " + RemoveSign4VietnameseString(dtr["SoChungChiKTV"].ToString()) + " dated " +
                    DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Month) + " " +
                    Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Day + ", " +
                    Convert.ToDateTime(dtr["NgayCapChungChiKTV"]).Year +
                    " by";
                obj.SoCCKTVTiengAnh += "\r\nMinistry of Finance is full Member of VACPA";
                try
                {
                    obj.NgayThangNamRaQuyetDinh = "Hµ Néi, ngµy " + Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Day + " th¸ng " +
                   Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Month + " n¨m " + Convert.ToDateTime(dtr["NgayQuyetDinhKetNap"]).Year;
                }
                catch (Exception)
                {
                    obj.NgayThangNamRaQuyetDinh = "Hµ Néi, ngµy " + " th¸ng " +
                                                  " n¨m ";
                }
                try
                {
                    obj.SoQuyetDinh = "Sè: " + dtr["SoQuyetDinhKetNap"].ToString();
                }
                catch (Exception)
                {
                    obj.SoQuyetDinh = "Sè: ";
                }

                lst.Add(obj);
            }

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/GiayChungNhanHoiVienCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objGiayChungNhanHoiVienChinhThuc"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            //CrystalReportViewer1.ReportSource = rpt;
            //CrystalReportViewer1.DataBind();
            //CrystalReportViewer1.SeparatePages = true;
            //if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true,
                                     "GiayChungNhanHoiVienCaNhan");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //{
            //    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true,
            //                             "GiayChungNhanHoiVienCaNhan");
            //}
        }
    }

    public static string RemoveSign4VietnameseString(string str)
    {
        if (str == null)
            return "";

        if (str.Trim().Length == 0)
            return "";

        //Tiến hành thay thế , lọc bỏ dấu cho chuỗi
        for (int i = 1; i < VietnameseSigns.Length; i++)
        {
            for (int j = 0; j < VietnameseSigns[i].Length; j++)

                str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);

        }

        str = str.Replace("  ", " ");

        return str.Trim();
    }

    private static readonly string[] VietnameseSigns = new string[]

    {

        "aAeEoOuUiIdDyY",

        "áàạảãâấầậẩẫăắằặẳẵ",

        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

        "éèẹẻẽêếềệểễ",

        "ÉÈẸẺẼÊẾỀỆỂỄ",

        "óòọỏõôốồộổỗơớờợởỡ",

        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

        "úùụủũưứừựửữ",

        "ÚÙỤỦŨƯỨỪỰỬỮ",

        "íìịỉĩ",

        "ÍÌỊỈĨ",

        "đ",

        "Đ",

        "ýỳỵỷỹ",

        "ÝỲỴỶỸ"

    };

}