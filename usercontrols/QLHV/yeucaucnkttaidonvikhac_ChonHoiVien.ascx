﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="yeucaucnkttaidonvikhac_ChonHoiVien.ascx.cs"
    Inherits="usercontrols_QLHV_yeucaucnkttaidonvikhac_ChonHoiVien" %>
    <script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<style type="text/css">
    .tdTable
    {
        width: 120px;
    }
</style>
<form id="FormDanhSachGiangVien" runat="server" method="post" enctype="multipart/form-data">
<fieldset class="fsBlockInfor">
    <legend>Tiêu chí tìm kiếm</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td>
                ID<span class="starRequired">(*)</span>:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtMaHocVien" runat="server"></asp:TextBox>
            </td>
            <td>
                Họ và tên:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtHoTen" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Phân loại:
            </td>
            <td>
                <asp:DropDownList ID="ddlPhanLoai" runat="server" Width="120px">
                    <asp:ListItem Value="">Tất cả</asp:ListItem>
                    <asp:ListItem Value="1">Hội viên</asp:ListItem>
                    <asp:ListItem Value="2">Kiểm toán viên</asp:ListItem>
                    <asp:ListItem Value="0">Người quan tâm</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Đơn vị công tác:
            </td>
            <td>
                <asp:DropDownList ID="ddlDonViCongTac" runat="server" Width="120px">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearchPopup" runat="server" CssClass="btn" OnClick="lbtSearch_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách nhân viên</legend>
    <table id="tblHocVien" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th style="width: 30px;">
                </th>
                <th style="width: 50px;">
                    STT
                </th>
                <th style="width: 100px;">
                    ID
                </th>
                <th style="min-width: 150px;">
                    Họ và tên
                </th>
                <th style="width: 100px;">
                    Phân loại
                </th>
                <th style="width: 150px;">
                    Số chứng chỉ KTV
                </th>
                <th style="width: 100px;">
                    Ngày cấp chứng chỉ KTV
                </th>
            </tr>
        </thead>
        <asp:Repeater ID="rpHocVien" runat="server">
            <ItemTemplate>
                <tbody>
                    <tr>
                        <td style="width: 30px; vertical-align: middle;">
                            <input type="radio" value='<%# Eval("HoiVienCaNhanID") + ";#" + Eval("MaHoiVienCaNhan") + ";#" + Eval("FullName") + ";#" + Eval("DonViCongTac") + ";#" + Eval("LoaiHoiVienCaNhan") %>'
                                name="HocVien" />
                        </td>
                        <td style="width: 50px; text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("MaHoiVienCaNhan")%>
                        </td>
                        <td style="min-width: 150px;">
                            <%# Eval("FullName")%>
                        </td>
                        <td style="width: 100px;">
                            <%# GetLoaiHocVien(Eval("LoaiHoiVienCaNhan").ToString()) %>
                        </td>
                        <td style="width: 150px;">
                            <%# Eval("SoChungChiKTV")%>
                        </td>
                        <td style="width: 100px;">
                            <%# !string.IsNullOrEmpty(Eval("NgayCapChungChiKTV").ToString()) ? Library.DateTimeConvert(Eval("NgayCapChungChiKTV")).ToString("dd/MM/yyyy") : ""%>
                        </td>
                    </tr>
                </tbody>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</fieldset>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tblHocVien').stickyTableHeaders();
    });
    $("#<%= FormDanhSachGiangVien.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtSearchPopup.ClientID %>').attr('href'));
        }
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm lấy thông tin học viên được chọn từ danh sách
    function ChooseHocVien() {
        var flag = false;
        $('#tblHocVien :radio').each(function () {
            checked = $(this).prop("checked");
            if (checked) {
                // Lấy chuỗi thông tin học viên -> gọi hàm nhận giá trị ở trang cha
                var chooseItem = $(this).val();
                parent.DisplayInforHocVien(chooseItem);
                parent.CloseFormDanhSachHocVien();
                flag = true;
                return;
            }
        });
        // Bắt buộc phải chọn 1 học viên -> nếu ko thì bật cảnh báo
        if (!flag)
            alert('Phải chọn học viên!');
    }
</script>
