﻿
using CommonLayer;
using VACPA.Entities;
using VACPA.Data.SqlClient;



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using System.Drawing;


using DBHelpers;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class usercontrols_hosohoivientapthe_edit : System.Web.UI.UserControl
{



    string DoiTuongHoiVienTapThe = "4";
    public HoiVienTapThe objHoiVIenTapThe = new HoiVienTapThe();
    public string loaihoiviencu = "-1";
    HoiVienTapTheChiNhanh ChiNhanh = new HoiVienTapTheChiNhanh();
    public DataTable dtbPhiTapThe;
    public List<string> PhatSinhPhiTapThe;
    public List<string> PhatSinhPhiCaNhan;
    String id = String.Empty;
    public Commons cm = new Commons();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    clsXacDinhQuyen quyen = new clsXacDinhQuyen();
    protected HoiVienFile FileDinhKem1 = new HoiVienFile();

    string tuchoidon = "tuchoidon";
    string trinhpheduyet = "trinhpheduyet";
    string duyetdon = "duyetdon";
    string tuchoiduyet = "tuchoiduyet";
    string thoaiduyet = "thoaiduyet";

    public int trangthaiChoTiepNhanID = 1;
    public int trangthaiTuChoiID = 2;
    public int trangthaiChoDuyetID = 3;
    public int trangthaiTraLaiID = 4;
    public int trangthaiDaPheDuyet = 5;
    public int trangthaiThoaiDuyetID = 6;


    string tk_TTChoTiepNhan = "1";
    string tk_TTTuChoiTiepNhan = "2";
    string tk_TTChoDuyet = "3";
    string tk_TTTuChoiDuyet = "4";
    string tk_TTDaDuyet = "5";
    string tk_TTThoaiDuyet = "6";





    string delete = "delete";
    string canhan = "1";
    string tapthe = "2";



    public bool QuyenXem { get; set; }
    public bool QuyenThem { get; set; }
    public bool QuyenSua { get; set; }
    public bool QuyenXoa { get; set; }
    public bool QuyenDuyet { get; set; }
    public bool QuyenTraLai { get; set; }
    public bool QuyenHuy { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Tên chức năng
            string tenchucnang = "Cập nhật hồ sơ hội viên tổ chức";
            lbTitle.Text = "Cập nhật hồ sơ hội viên tổ chức";
            if (Request.QueryString["hv"] == "2")
            {
                lbTitle.Text = "Cập nhật hồ sơ công ty kiểm toán";
                tenchucnang = "Cập nhật hồ sơ công ty kiểm toán";
            }
            // Icon CSS Class  
            string IconClass = "iconfa-credit-card";





            if (!IsPostBack)
            {
                hidendHoiVienTapTheID.Value = Request.QueryString["id"];
                hidendXoaTenHoiVienID.Value = Request.QueryString["xoatenhoivienid"];
                int HoiVienTapTheID = 0;
                try
                {
                    HoiVienTapTheID = Convert.ToInt32(hidendHoiVienTapTheID.Value);
                }
                catch
                {

                }

                LoadDropDown();
                Load_ThanhPho("00");
                loadTTHoiVien(HoiVienTapTheID);
                loaduploadFileDinhKem(HoiVienTapTheID);

                // chi nhanh

                LoadChiNhanh(HoiVienTapTheID);
                // thanh vien
                LoadThanhVienVien(HoiVienTapTheID);
                // cap nhat kien thuc

                LoadCongTy(HoiVienTapTheID);
                LoadDaoTao(HoiVienTapTheID);
                // kiem soat chat luong

                LoadHoSo(HoiVienTapTheID);
                LoadDropDownNamThanhToan(HoiVienTapTheID);


                // khen thuong ky luat

                SetInitialRow_KhenThuong();
                SetInitialRow_KyLuat();
                LoadKhenThuongKyLuat(HoiVienTapTheID.ToString());
                if (objHoiVIenTapThe.LoaiHoiVienTapThe == "2")
                {
                    lbTitle.Text = "Cập nhật hồ sơ công ty kiểm toán";
                    tenchucnang = "Cập nhật hồ sơ công ty kiểm toán";
                    trHoiVienCha.Visible = false;
                    trLoaiHoiVien.Visible = false;
                }
                Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

                if (Request.QueryString["thongbao"] == "1")
                {
                    string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

                    placeMessage.Controls.Add(new LiteralControl(thongbao));
                }
            }



        }
        catch (Exception ex)
        {
            placeMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    public void LoadDropDown()
    {
        conn.Open();

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();

        string sql = "";

        // hội sở chính
        sql = "SELECT 0 AS HoiVienTapTheID, N'<< Lựa chọn >>' AS TenDoanhNghiep, '-' AS SoHieu UNION ALL (SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE '--- ' + ISNULL(A.SoHieu,LTRIM(RTRIM(A.MaHoiVienTapThe))) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID) ORDER BY SoHieu";
        //SqlCommand cmd = new SqlCommand(sql, conn);
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drHoiVienTapThe.DataSource = dtb;
        drHoiVienTapThe.DataBind();

        ds.Clear();
        dtb.Clear();

        // Loai hinh Doanh Nnghiep
        sql = "SELECT 0 AS LoaiHinhDoanhNghiepID, N'<< Lựa chọn >>' AS TenLoaiHinhDoanhNghiep UNION ALL (SELECT LoaiHinhDoanhNghiepID, TenLoaiHinhDoanhNghiep FROM tblDMLoaiHinhDoanhNghiep)";
        //SqlCommand cmd = new SqlCommand(sql, conn);
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drLoaiHinhDoanhNghiep.DataSource = dtb;
        drLoaiHinhDoanhNghiep.DataBind();

        ds.Clear();
        dtb.Clear();


        // Linh vuc hoat dong
        sql = "SELECT 0 AS LinhVucHoatDongID, N'<< Lựa chọn >>' AS TenLinhVucHoatDong , '' AS MaLinhVucHoatDong  UNION ALL (SELECT LinhVucHoatDongID, TenLinhVucHoatDong , MaLinhVucHoatDong FROM tblDMLinhVucHoatDong)";
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drLinhvucHoatDong.DataSource = dtb;
        drLinhvucHoatDong.DataBind();

        ds.Clear();
        dtb.Clear();


        // Chuc vu nguoi dai dien Phap Luat
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drNguoiDaiDienPL_ChucVu.DataSource = dtb;
        drNguoiDaiDienPL_ChucVu.DataBind();

        ds.Clear();
        dtb.Clear();

        // Chuc vu nguoi dai dien Lien lac
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drNguoiDaiDienLL_ChucVu.DataSource = dtb;
        drNguoiDaiDienLL_ChucVu.DataBind();

        ds.Clear();
        dtb.Clear();


        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT 0 AS HoiVienTapTheID, N'<< Lựa chọn Chi nhánh/Hội sở >>' AS TenDoanhNghiep, '' AS SoHieu UNION ALL ";
        // lấy ra hội viên cha
        cmd.CommandText += "(SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE ISNULL(A.SoHieu,A.MaHoiVienTapThe) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID WHERE A.HoiVienTapTheID = (SELECT HoiVienTapTheChaID FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + Request.QueryString["id"] + ")) UNION ALL ";
        // lấy ra các hội viên con của cha trừ chính nó
        cmd.CommandText += "(SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE ISNULL(A.SoHieu,A.MaHoiVienTapThe) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID WHERE A.HoiVienTapTheChaID = (SELECT HoiVienTapTheChaID FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + Request.QueryString["id"] + ") AND A.HoiVienTapTheID != " + Request.QueryString["id"] + ") UNION ALL ";
        // lấy ra các hội viên con
        cmd.CommandText += "(SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE ISNULL(A.SoHieu,A.MaHoiVienTapThe) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID WHERE A.HoiVienTapTheChaID = " + Request.QueryString["id"] + ") ";
        cmd.CommandText += "ORDER BY SoHieu";

        drHoiVienTapThe_Chuyen.DataSource = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHoiVienTapThe_Chuyen.DataBind();

        conn.Close();
    }



    public void Load_ThanhPho(string MaTinh = "00")
    {

        //   conn.Open();

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();

        string sql = "";

        sql = "SELECT '00' AS MaTinh, N'<< Lựa chọn >>' AS TenTinh UNION ALL (SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ) ORDER BY TenTinh ASC";

        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drTinhThanh.DataSource = dtb;
        drTinhThanh.DataBind();

        ds.Clear();
        dtb.Clear();



    }

    public void Load_QuanHuyen(string MaHuyen = "000", string MaTinh = "00")
    {
        //  conn.Open();

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();

        string sql = "";

        sql = "SELECT '000' AS MaHuyen, N'<< Lựa chọn >>' AS TenHuyen UNION ALL (SELECT MaHuyen,TenHuyen FROM tblDMHuyen WHERE (MaTinh='" + MaTinh + "') AND HieuLuc='1' ) ORDER BY TenHuyen ASC";

        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drQuanHuyen.DataSource = dtb;
        drQuanHuyen.DataBind();

        ds.Clear();
        dtb.Clear();


        drPhuongXa.Items.Clear();
        drPhuongXa.DataSource = null;
        drPhuongXa.DataBind();


    }

    public void Load_PhuongXa(string MaXa = "00000", string MaHuyen = "000")
    {
        //   conn.Open();

        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();

        string sql = "";

        sql = "SELECT '0000' AS MaXa, N'<< Lựa chọn >>' AS TenXa UNION ALL (SELECT MaXa,TenXa FROM tblDMXa WHERE (MaHuyen='" + MaHuyen + "') ) ORDER BY TenXa ASC";

        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];
        drPhuongXa.DataSource = dtb;
        drPhuongXa.DataBind();

        ds.Clear();
        dtb.Clear();



    }

    protected void drTinhThanh_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_QuanHuyen("000", drTinhThanh.SelectedValue);
    }

    protected void drQuanHuyen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_PhuongXa("00000", drQuanHuyen.SelectedValue);
    }

    protected void CapnhatTTHoiVien(int donHoiVienTtId)
    {


        try
        {
            // conn.Open();

            string thongbao = "";



            DataSet ds = new DataSet();

            SqlCommand sql = new SqlCommand();
            sql.Connection = new SqlConnection(connStr); ;





            SqlHoiVienTapTheProvider HoiVienTapThe_provider = new SqlHoiVienTapTheProvider(connStr, true, "");
            objHoiVIenTapThe = HoiVienTapThe_provider.GetByHoiVienTapTheId(donHoiVienTtId);
            loaihoiviencu = (objHoiVIenTapThe.LoaiHoiVienTapTheChiTiet != null) ? objHoiVIenTapThe.LoaiHoiVienTapTheChiTiet.Value.ToString() : "-1";
            if (chkHVTT.Checked)
            {
                if (objHoiVIenTapThe.HoiVienTapTheChaId != null)
                    objHoiVIenTapThe.HoiVienTapTheChaId = null;
                objHoiVIenTapThe.LoaiHoiVienTapThe = "1";
            }
            if (chkCTKT.Checked)
            {
                if (objHoiVIenTapThe.HoiVienTapTheChaId != null)
                    objHoiVIenTapThe.HoiVienTapTheChaId = null;
                objHoiVIenTapThe.LoaiHoiVienTapThe = "2";
            }

            if (objHoiVIenTapThe.LoaiHoiVienTapThe != "2")
            {
                if (drLoaiHoiVien.SelectedValue != "-1")
                    objHoiVIenTapThe.LoaiHoiVienTapTheChiTiet = int.Parse(drLoaiHoiVien.SelectedValue);
                if (drHoiVienTapThe.SelectedValue != "0")
                    objHoiVIenTapThe.HoiVienTapTheChaId = int.Parse(drHoiVienTapThe.SelectedValue);
                else
                    objHoiVIenTapThe.HoiVienTapTheChaId = null;
            }
            objHoiVIenTapThe.MaHoiVienTapThe = txtTenDangNhap.Text;
            objHoiVIenTapThe.TenDoanhNghiep = txtTenDoanhNghiep.Text.Trim();
            objHoiVIenTapThe.TenTiengAnh = txtTenTiengAnh.Text.Trim();
            objHoiVIenTapThe.TenVietTat = txtTenVietTat.Text.Trim();
            objHoiVIenTapThe.SoHieu = txtSoHieuCongTy.Text.Trim();

            objHoiVIenTapThe.LoaiHinhDoanhNghiepId = Convert.ToInt32(drLoaiHinhDoanhNghiep.SelectedValue);
            objHoiVIenTapThe.DnTrongNuoc = rdDN_TrongNuoc.Checked == true ? "1" : "2";
            objHoiVIenTapThe.DiaChi = txtDiaChi.Text.Trim();
            objHoiVIenTapThe.DiaChiTinhId = drTinhThanh.SelectedValue;
            objHoiVIenTapThe.DiaChiHuyenId = drQuanHuyen.SelectedValue;
            objHoiVIenTapThe.DiaChiXaId = drPhuongXa.SelectedValue;


            try
            {
                objHoiVIenTapThe.NgayThanhLap = DateTime.ParseExact(txtNgayThanhLap.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }

            objHoiVIenTapThe.DienThoai = txtDienThoai.Text.Trim();
            objHoiVIenTapThe.Email = txtEmail.Text.Trim();
            objHoiVIenTapThe.Fax = txtFax.Text.Trim();
            objHoiVIenTapThe.Website = txtWebsite.Text.Trim();
            objHoiVIenTapThe.SoGiayChungNhanDkkd = txtSoGiayChungNhanDKKD.Text.Trim();

            try
            {
                objHoiVIenTapThe.NgayCapGiayChungNhanDkkd = DateTime.ParseExact(txtNgayCapGiayChungNhanDKKD.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }

            objHoiVIenTapThe.SoGiayChungNhanKddvkt = txtSoGiayChungNhanKDDVKT.Text.Trim();
            try
            {
                objHoiVIenTapThe.NgayCapGiayChungNhanKddvkt = DateTime.ParseExact(txtNgayCapGiayChungNhanKDDVKT.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }


            objHoiVIenTapThe.MaSoThue = txtMaSoThue.Text.Trim();
            objHoiVIenTapThe.LinhVucHoatDongId = Convert.ToInt32(drLinhvucHoatDong.SelectedValue);

            objHoiVIenTapThe.NamDuDkktKhac = txtNamCoLoiIchCongChung.Text.Trim();
            objHoiVIenTapThe.NamDuDkktCk = txtNamTrongLinhVucChungKhoan.Text.Trim();
            objHoiVIenTapThe.ThanhVienHangKtqt = txtThanhVienHangKTQT.Text.Trim();
            try
            {
                objHoiVIenTapThe.TongSoNguoiLamViec = Convert.ToInt32(txtTongSoNguoiLamViec.Text.Trim());
            }
            catch
            {
            }
            try
            {
                objHoiVIenTapThe.TongSoNguoiCoCkktv = Convert.ToInt32(txtTongSoNguoiCoCKKTV.Text.Trim());
            }
            catch
            {
            }
            try
            {
                objHoiVIenTapThe.TongSoKtvdkhn = Convert.ToInt32(txtTongSoKTVDKHN.Text.Trim());
            }
            catch
            {
            }
            try
            {
                //  objHoiVIenTapThe.TongSoHoiVienCaNhan = Convert.ToInt32(txtTongSoHoiVienCaNhan.Text.Trim()); 
            }
            catch
            {
            }


            // Thong tin nguoi dai dien theo PL
            objHoiVIenTapThe.NguoiDaiDienPlTen = txtNguoiDaiDienPL_Ten.Text.Trim();
            try
            {
                objHoiVIenTapThe.NguoiDaiDienPlNgaySinh = DateTime.ParseExact(txtNguoiDaiDienPL_NgaySinh.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }
            objHoiVIenTapThe.NguoiDaiDienPlGioiTinh = drNguoiDaiDienPL_GioiTinh.SelectedValue;
            objHoiVIenTapThe.NguoiDaiDienPlChucVuId = Convert.ToInt32(drNguoiDaiDienPL_ChucVu.SelectedValue);
            objHoiVIenTapThe.NguoiDaiDienPlSoChungChiKtv = txtNguoiDaiDienPL_SoChungChiKTV.Text.Trim();
            try
            {
                objHoiVIenTapThe.NguoiDaiDienPlNgayCapChungChiKtv = DateTime.ParseExact(txtNguoiDaiDienPL_NgayCapChungChiKTV.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }

            objHoiVIenTapThe.NguoiDaiDienPlDiDong = txtNguoiDaiDienPL_DiDong.Text.Trim();
            objHoiVIenTapThe.NguoiDaiDienPlEmail = txtNguoiDaiDienPL_Email.Text.Trim();
            objHoiVIenTapThe.NguoiDaiDienPlDienThoaiCd = txtNguoiDaiDienPL_DienThoaiCD.Text.Trim();

            // Thong tin nguoi lien lac
            objHoiVIenTapThe.NguoiDaiDienLlTen = txtNguoiDaiDienLL_Ten.Text.Trim();
            try
            {
                objHoiVIenTapThe.NguoiDaiDienLlNgaySinh = DateTime.ParseExact(txtNguoiDaiDienLL_NgaySinh.Text.Trim(), "dd/MM/yyyy", null);
            }
            catch
            {
            }
            objHoiVIenTapThe.NguoiDaiDienLlGioiTinh = drNguoiDaiDienLL_GioiTinh.SelectedValue;
            objHoiVIenTapThe.NguoiDaiDienLlChucVuId = Convert.ToInt32(drNguoiDaiDienLL_ChucVu.SelectedValue);


            objHoiVIenTapThe.NguoiDaiDienLlDiDong = txtNguoiDaiDienLL_DiDong.Text.Trim();
            objHoiVIenTapThe.NguoiDaiDienLlEmail = txtNguoiDaiDienLL_Email.Text.Trim();
            objHoiVIenTapThe.NguoiDaiDienLlDienThoaiCd = txtNguoiDaiDienLL_DienThoaiCD.Text.Trim();

            objHoiVIenTapThe.SoQuyetDinhKetNap = txtSoQuyetDinh.Text;
            if (!string.IsNullOrEmpty(txtNgayQuyetDinh.Text))
            {
                try
                {
                    objHoiVIenTapThe.NgayQuyetDinhKetNap = DateTime.ParseExact(txtNgayQuyetDinh.Text.Trim(), "dd/MM/yyyy", null);
                }
                catch
                {
                }
            }

            objHoiVIenTapThe.NgayCapNhat = DateTime.Now;
            objHoiVIenTapThe.NguoiCapNhat = cm.Admin_TenDangNhap;

            HoiVienTapThe_provider.Update(objHoiVIenTapThe);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblNguoiDung SET TenDangNhap = N'" + txtTenDangNhap.Text + "', HoVaTen = N'" + txtTenDoanhNghiep.Text + "', Email = '" + txtEmail.Text + "'";
            if (chkHVTT.Checked)
                cmd.CommandText += ", LoaiHoiVien = '4'";
            if (chkCTKT.Checked)
                cmd.CommandText += ", LoaiHoiVien = '3'";
            cmd.CommandText += " WHERE LoaiHoiVien IN ('3','4') AND HoiVienID = " + donHoiVienTtId;
            DataAccess.RunActionCmd(cmd);

            // File dinh kem
            int newHoiVienTapTheId = 0;
            newHoiVienTapTheId = (int)objHoiVIenTapThe.HoiVienTapTheId;
            CapnhatFile(donHoiVienTtId);
            cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Cập nhật thông tin hội viên " + objHoiVIenTapThe.TenDoanhNghiep);
            thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

            placeMessage.Controls.Add(new LiteralControl(thongbao));

        }
        catch (Exception ex)
        {
            string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #000000;"">
            							<p>" + ex.ToString() + @"</p>
            							<i class=""fa fa-plus""></i>
            						</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }


    }


    protected void CapnhatFile(int donHoiVienTtId)
    {
        if (donHoiVienTtId != null && donHoiVienTtId != 0)
        {
            SqlHoiVienFileProvider HoiVienTapThe_provider = new SqlHoiVienFileProvider(connStr, true, "");

            HttpPostedFile styledfileupload;

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                FileUpload fileUp = (FileUpload)row.FindControl("FileUp");
                FileDinhKem1 = new HoiVienFile();
                if (fileUp != null && fileUp.FileName != "")
                {
                    int fileUploadId = 0;
                    int loaiFieldId = 0;
                    String sTempLoaiFileID = row.Cells[3].Text;
                    String sTempFileUploadID = row.Cells[4].Text;

                    if (!String.IsNullOrEmpty(sTempFileUploadID) && sTempFileUploadID != "0" && sTempFileUploadID != "&nbsp;")
                    {
                        fileUploadId = Convert.ToInt32(row.Cells[4].Text);

                    }
                    if (!String.IsNullOrEmpty(sTempLoaiFileID) && sTempLoaiFileID != "0" && sTempLoaiFileID != "&nbsp;")
                    {
                        loaiFieldId = Convert.ToInt32(row.Cells[3].Text);

                    }
                    string styledfileupload_name = "";
                    styledfileupload = fileUp.PostedFile;
                    if (styledfileupload.FileName != "")
                    {
                        styledfileupload_name = fileUp.FileName;


                        byte[] datainput = new byte[styledfileupload.ContentLength];
                        styledfileupload.InputStream.Read(datainput, 0, styledfileupload.ContentLength);

                        FileDinhKem1.FileId = fileUploadId;
                        FileDinhKem1.HoiVienTapTheId = donHoiVienTtId;
                        FileDinhKem1.LoaiGiayToId = loaiFieldId;
                        FileDinhKem1.FileDinhKem = datainput;
                        //FileDinhKem1.Tenfile = styledfileupload.FileName;
                        FileDinhKem1.TenFile = new System.IO.FileInfo(styledfileupload.FileName).Name;
                    }
                    if (FileDinhKem1.FileId == 0)
                    {
                        HoiVienTapThe_provider.Insert(FileDinhKem1);
                    }
                    else
                    {
                        HoiVienTapThe_provider.Update(FileDinhKem1);
                    }
                    //if (fileUploadId != 0)
                    //{

                    //    SqlCommand sql = new SqlCommand();
                    //    sql.CommandText = "DELETE FROM TBLFILEDINHKEM WHERE FILEDINHKEMID  =   " + fileUploadId + "  ";
                    //    DataSet ds = DataAccess.RunCMDGetDataSet(sql);
                    //    DataAccess.RunActionCmd(sql);

                    //    sql.Connection.Close();
                    //    sql.Connection.Dispose();
                    //    sql = null;

                    //}
                    //HoiVienTapThe_provider.Insert(FileDinhKem1);
                }
                else
                {

                }

            }


            loaduploadFileDinhKem(donHoiVienTtId);

        }
    }

    protected void loadTTHoiVien(int donHoiVienTtId)
    {
        try
        {

            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                SqlHoiVienTapTheProvider HoiVienTapThe_provider = new SqlHoiVienTapTheProvider(connStr, true, "");
                objHoiVIenTapThe = HoiVienTapThe_provider.GetByHoiVienTapTheId(donHoiVienTtId);
                if (objHoiVIenTapThe.LoaiHoiVienTapTheChiTiet != null)
                    drLoaiHoiVien.SelectedValue = objHoiVIenTapThe.LoaiHoiVienTapTheChiTiet.Value.ToString();
                if (objHoiVIenTapThe.HoiVienTapTheChaId != null)
                    drHoiVienTapThe.SelectedValue = objHoiVIenTapThe.HoiVienTapTheChaId.Value.ToString();
                if (objHoiVIenTapThe.LoaiHoiVienTapThe == "1")
                    chkHVTT.Visible = false;
                if (objHoiVIenTapThe.LoaiHoiVienTapThe == "2")
                    chkCTKT.Visible = false;
                txtTenDangNhap.Text = objHoiVIenTapThe.MaHoiVienTapThe.Trim();
                txtTenDoanhNghiep.Text = objHoiVIenTapThe.TenDoanhNghiep;
                txtTenTiengAnh.Text = objHoiVIenTapThe.TenTiengAnh;
                txtTenVietTat.Text = objHoiVIenTapThe.TenVietTat;
                txtSoHieuCongTy.Text = objHoiVIenTapThe.SoHieu;
                try
                {
                    drLoaiHinhDoanhNghiep.SelectedValue = objHoiVIenTapThe.LoaiHinhDoanhNghiepId.ToString();
                }
                catch
                {
                }
                rdDN_TrongNuoc.Checked = objHoiVIenTapThe.DnTrongNuoc == "1" ? true : false;
                rdDN_NgoaiNuoc.Checked = !rdDN_TrongNuoc.Checked;

                txtDiaChi.Text = objHoiVIenTapThe.DiaChi;

                drTinhThanh.SelectedValue = objHoiVIenTapThe.DiaChiTinhId;
                drTinhThanh_SelectedIndexChanged(null, null);
                drQuanHuyen.SelectedValue = objHoiVIenTapThe.DiaChiHuyenId;
                drQuanHuyen_SelectedIndexChanged(null, null);
                drPhuongXa.SelectedValue = objHoiVIenTapThe.DiaChiXaId;

                try
                {
                    txtNgayThanhLap.Text = (objHoiVIenTapThe.NgayThanhLap).Value.ToString("dd/MM/yyyy");
                }
                catch
                {
                }

                txtDienThoai.Text = objHoiVIenTapThe.DienThoai;
                txtEmail.Text = objHoiVIenTapThe.Email;
                txtFax.Text = objHoiVIenTapThe.Fax;
                txtWebsite.Text = objHoiVIenTapThe.Website;
                txtSoGiayChungNhanDKKD.Text = objHoiVIenTapThe.SoGiayChungNhanDkkd;

                try
                {
                    txtNgayCapGiayChungNhanDKKD.Text = (objHoiVIenTapThe.NgayCapGiayChungNhanDkkd).Value.ToString("dd/MM/yyyy");
                }
                catch
                {
                }

                txtSoGiayChungNhanKDDVKT.Text = objHoiVIenTapThe.SoGiayChungNhanKddvkt;
                try
                {
                    txtNgayCapGiayChungNhanKDDVKT.Text = (objHoiVIenTapThe.NgayCapGiayChungNhanKddvkt).Value.ToString("dd/MM/yyyy");
                }
                catch
                {
                }


                txtMaSoThue.Text = objHoiVIenTapThe.MaSoThue;
                try
                {
                    drLinhvucHoatDong.SelectedValue = objHoiVIenTapThe.LinhVucHoatDongId.ToString();
                }
                catch
                {
                }
                txtNamCoLoiIchCongChung.Text = objHoiVIenTapThe.NamDuDkktKhac;
                txtNamTrongLinhVucChungKhoan.Text = objHoiVIenTapThe.NamDuDkktCk;
                txtThanhVienHangKTQT.Text = objHoiVIenTapThe.ThanhVienHangKtqt;
                try
                {
                    txtTongSoNguoiLamViec.Text = objHoiVIenTapThe.TongSoNguoiLamViec.ToString();
                }
                catch
                {
                }
                try
                {
                    txtTongSoNguoiCoCKKTV.Text = objHoiVIenTapThe.TongSoNguoiCoCkktv.ToString();

                }
                catch
                {
                }
                try
                {
                    txtTongSoKTVDKHN.Text = objHoiVIenTapThe.TongSoKtvdkhn.ToString();

                }
                catch
                {
                }
                try
                {
                    //    txtTongSoHoiVienCaNhan.Text = objHoiVIenTapThe.TongSoHoiVienCaNhan.ToString();

                }
                catch
                {
                }


                // Thong tin nguoi dai dien theo PL
                txtNguoiDaiDienPL_Ten.Text = objHoiVIenTapThe.NguoiDaiDienPlTen;
                try
                {
                    txtNguoiDaiDienPL_NgaySinh.Text = (objHoiVIenTapThe.NguoiDaiDienPlNgaySinh).Value.ToString("dd/MM/yyyy");

                }
                catch
                {
                }
                drNguoiDaiDienPL_GioiTinh.SelectedValue = objHoiVIenTapThe.NguoiDaiDienPlGioiTinh;
                try
                {
                    drNguoiDaiDienPL_ChucVu.SelectedValue = objHoiVIenTapThe.NguoiDaiDienPlChucVuId.ToString();
                }
                catch
                {
                }
                txtNguoiDaiDienPL_SoChungChiKTV.Text = objHoiVIenTapThe.NguoiDaiDienPlSoChungChiKtv;
                try
                {
                    txtNguoiDaiDienPL_NgayCapChungChiKTV.Text = (objHoiVIenTapThe.NguoiDaiDienPlNgayCapChungChiKtv).Value.ToString("dd/MM/yyyy");
                }
                catch
                {
                }

                txtNguoiDaiDienPL_DiDong.Text = objHoiVIenTapThe.NguoiDaiDienPlDiDong;
                txtNguoiDaiDienPL_Email.Text = objHoiVIenTapThe.NguoiDaiDienPlEmail;
                txtNguoiDaiDienPL_DienThoaiCD.Text = objHoiVIenTapThe.NguoiDaiDienPlDienThoaiCd;

                // Thong tin nguoi lien lac
                txtNguoiDaiDienLL_Ten.Text = objHoiVIenTapThe.NguoiDaiDienLlTen;// = txtNguoiDaiDienLL_Ten.Text.Trim();
                try
                {
                    txtNguoiDaiDienLL_NgaySinh.Text = (objHoiVIenTapThe.NguoiDaiDienLlNgaySinh).Value.ToString("dd/MM/yyyy");
                }
                catch
                {
                }

                drNguoiDaiDienLL_GioiTinh.SelectedValue = objHoiVIenTapThe.NguoiDaiDienLlGioiTinh;
                try
                {
                    drNguoiDaiDienLL_ChucVu.SelectedValue = objHoiVIenTapThe.NguoiDaiDienLlChucVuId.ToString();
                }
                catch
                {
                }

                txtNguoiDaiDienLL_DiDong.Text = objHoiVIenTapThe.NguoiDaiDienLlDiDong;
                txtNguoiDaiDienLL_Email.Text = objHoiVIenTapThe.NguoiDaiDienLlEmail;
                txtNguoiDaiDienLL_DienThoaiCD.Text = objHoiVIenTapThe.NguoiDaiDienLlDienThoaiCd;

                hidendHoiVienTapTheID.Value = objHoiVIenTapThe.HoiVienTapTheId.ToString();

                txtSoQuyetDinh.Text = objHoiVIenTapThe.SoQuyetDinhKetNap;
                txtNgayQuyetDinh.Text = (objHoiVIenTapThe.NgayQuyetDinhKetNap != null) ? objHoiVIenTapThe.NgayQuyetDinhKetNap.Value.ToString("dd/MM/yyyy") : "";
            }
        }
        catch (Exception ex)
        {
            string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #000000;"">
            							<p>" + ex.ToString() + @"</p>
            							<i class=""fa fa-plus""></i>
            						</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }


    }
    protected void loaduploadFileDinhKem(int donHoiVienTtId)
    {
        try
        {


            SqlCommand sql = new SqlCommand();
            sql.Connection = new SqlConnection(connStr); ;


            if (donHoiVienTtId == 0)
            {
                sql.CommandText = " SELECT  LoaiGiayToID ,TenFile as TenBieuMau ,DoiTuong ,BatBuoc  , 0 as FileID  , ' ' as TenFileDinhKem " +
                                  " FROM tblDMLoaiGiayTo  " +
                                  " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuongHoiVienTapThe + "' order by tblDMLoaiGiayTo.TenFile   ";
            }
            else
            {
                sql.CommandText =
                "    SELECT   LoaiGiayToID ,TenFile as TenBieuMau  ,DoiTuong  ,BatBuoc , " +
                              "   ( SELECT TOP 1 ISNULL ( tblHoiVienFile.FileID,0)  FROM  tblHoiVienFile      WHERE  tblHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblHoiVienFile.HoiVienTapTheID= " + donHoiVienTtId + "  ) as FileID ," +
                               " ( SELECT  TOP 1 ISNULL(tblHoiVienFile.TenFile,' ')   FROM  tblHoiVienFile  WHERE  tblHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblHoiVienFile.HoiVienTapTheID= " + donHoiVienTtId + " ) as  TenFileDinhKem    " +
                " FROM tblDMLoaiGiayTo  " +
                " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuongHoiVienTapThe + "' order by tblDMLoaiGiayTo.TenFile  ";


            }



            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;


            PagedDataSource objPds1 = new PagedDataSource();
            objPds1.DataSource = ds.Tables[0].DefaultView;

            FileDinhKem_grv.DataSource = objPds1;
            FileDinhKem_grv.DataBind();
            //FileDinhKem_grv.Columns[3].Visible = false; 
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;

            // set validate

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                String sTenLoaiFile = row.Cells[0].Text.Trim();
                String sLoaiFileID = row.Cells[3].Text.Trim();
                String sFileDinhKemID = row.Cells[4].Text.Trim();
                String sBatbuoc = row.Cells[5].Text.Trim();

                String sTenFile = row.Cells[6].Text.Trim();

                // check validate
                if (sBatbuoc == "0" || !String.IsNullOrEmpty(sTenFile))
                {
                    //((RequiredFieldValidator)row.FindControl("RequiredFieldFileUpload")).Visible = false;


                }

                // an hien chu thich bat buoc
                if (sBatbuoc == "0")
                {
                    ((Label)row.FindControl("requred")).Visible = false;

                }
                else
                {

                }

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }




    }

    protected void btnGhi_Click(object sender, EventArgs e)
    {
        int HoiVienTapTheId = Convert.ToInt32(hidendHoiVienTapTheID.Value.ToString());
        CapnhatTTHoiVien(HoiVienTapTheId);
        CapnhatPhiHoiVien(HoiVienTapTheId);
        CapnhatFile(HoiVienTapTheId);
        loaduploadFileDinhKem(HoiVienTapTheId);
    }

    protected void CapnhatPhiHoiVien(int HoiVienTtId)
    {

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select HoiVienTapTheChaID from tblHoiVienTapThe where HoiVienTapTheID =" + HoiVienTtId;

        if (DataAccess.DLookup(cmd) == "")
        {
            cmd.CommandText = "SELECT CONVERT(varchar, NgayGiaNhap, 103) FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + HoiVienTtId;
            string ngaygianhap = DataAccess.DLookup(cmd);
            if (!string.IsNullOrEmpty(txtNamTrongLinhVucChungKhoan.Text) && txtNamTrongLinhVucChungKhoan.Text != "0") // đủ đk KiT lĩnh vực chứng khoán
            {
                // kiểm tra xem đã nộp phí hay chưa
                cmd.CommandText = @"SELECT GiaoDichChiTietID FROM tblTTNopPhiTapTheChiTiet CT inner join tblTTNopPhiTapThe GD on CT.GiaoDichID = GD.GiaoDichID
inner join tblTTDMPhiHoiVien P on CT.PhiID = P.PhiID  
WHERE CT.HoiVienTapTheId = " + HoiVienTtId + @" AND CT.LoaiPhi = 1 AND P.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttDuDieuKienKiTLinhVucChungKhoan).ToString() + " and P.LoaiHoiVien = 0 and P.LoaiHoiVienChiTiet = " + loaihoiviencu + " and YEAR(P.NgayApDung) = YEAR(getDate()) and GD.TinhTrangID = 5";

                if (DataAccess.DLookup(cmd) == "") // nếu chưa nộp phí
                {
                    if (!string.IsNullOrEmpty(ngaygianhap))
                        cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " AND (CONVERT(DATETIME, '" + ngaygianhap + "', 103) < B.TuNgay OR (B.TuNgay <= CONVERT(DATETIME, '" + ngaygianhap + "', 103) AND B.DenNgay >= CONVERT(DATETIME, '" + ngaygianhap + "', 103))) ORDER BY A.PhiID DESC";
                    else
                        cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*100) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";

                    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                    if (dtb.Rows.Count != 0)
                    {
                        cmd.CommandText = "SELECT * FROM tblTTPhatSinhPhi WHERE LoaiPhi = " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + " AND HoiVienID = " + HoiVienTtId + " AND LoaiHoiVien = " + ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString() + " AND YEAR(NgayPhatSinh) = YEAR(GETDATE())";
                        DataTable dtb_phi = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                        if (dtb_phi.Rows.Count != 0) // đã tồn tại phí của năm đó thì update PhiID và SoTien
                        {
                            cmd.CommandText = "UPDATE tblTTPhatSinhPhi SET PhiID = " + dtb.Rows[0]["PhiID"].ToString() + ", SoTien = " + dtb.Rows[0]["MucPhi"].ToString() + " WHERE LoaiPhi = " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + " AND HoiVienID = " + HoiVienTtId + " AND LoaiHoiVien = " + ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString() + " AND YEAR(NgayPhatSinh) = YEAR(GETDATE())";
                            DataAccess.RunActionCmd(cmd);
                        }
                        else
                        {
                            cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + HoiVienTtId + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                            DataAccess.RunActionCmd(cmd);
                        }
                    }
                }
            }
            else
            {
                // kiểm tra xem đã nộp phí hay chưa
                cmd.CommandText = @"SELECT GiaoDichChiTietID FROM tblTTNopPhiTapTheChiTiet CT inner join tblTTNopPhiTapThe GD on CT.GiaoDichID = GD.GiaoDichID
inner join tblTTDMPhiHoiVien P on CT.PhiID = P.PhiID  
WHERE CT.HoiVienTapTheId = " + HoiVienTtId + @" AND CT.LoaiPhi = 1 AND P.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttKhongDuDieuKienKiTLinhVucChungKhoan).ToString() + " and P.LoaiHoiVien = 0 and P.LoaiHoiVienChiTiet = " + loaihoiviencu + " and YEAR(P.NgayApDung) = YEAR(getDate()) and GD.TinhTrangID = 5";

                if (DataAccess.DLookup(cmd) == "") // nếu chưa nộp phí
                {
                    if (!string.IsNullOrEmpty(ngaygianhap))
                        cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttKhongDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " AND (CONVERT(DATETIME, '" + ngaygianhap + "', 103) < B.TuNgay OR (B.TuNgay <= CONVERT(DATETIME, '" + ngaygianhap + "', 103) AND B.DenNgay >= CONVERT(DATETIME, '" + ngaygianhap + "', 103))) ORDER BY A.PhiID DESC";
                    else
                        cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*100) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttKhongDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + drLoaiHoiVien.SelectedValue + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";

                    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                    if (dtb.Rows.Count != 0)
                    {
                        cmd.CommandText = "SELECT * FROM tblTTPhatSinhPhi WHERE LoaiPhi = " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + " AND HoiVienID = " + HoiVienTtId + " AND LoaiHoiVien = " + ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString() + " AND YEAR(NgayPhatSinh) = YEAR(GETDATE())";
                        DataTable dtb_phi = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                        if (dtb_phi.Rows.Count != 0) // đã tồn tại phí của năm đó thì update PhiID và SoTien
                        {
                            cmd.CommandText = "UPDATE tblTTPhatSinhPhi SET PhiID = " + dtb.Rows[0]["PhiID"].ToString() + ", SoTien = " + dtb.Rows[0]["MucPhi"].ToString() + " WHERE LoaiPhi = " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + " AND HoiVienID = " + HoiVienTtId + " AND LoaiHoiVien = " + ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString() + " AND YEAR(NgayPhatSinh) = YEAR(GETDATE())";
                            DataAccess.RunActionCmd(cmd);
                        }
                        else
                        {
                            cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + HoiVienTtId + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                            DataAccess.RunActionCmd(cmd);
                        }
                    }
                }
            }
        }
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["prepage"]))
        {

            Response.Redirect("admin.aspx?page=" + Request.QueryString["prepage"]);
        }
        else
        {
            Response.Redirect("admin.aspx?page=danhsachkhachhang");
        }
    }


    // chi nhanh

    public void LoadChiNhanh(int HoiVienTapTheID)
    {


        SqlCommand sql = new SqlCommand();
        sql.Connection = new SqlConnection(connStr); ;


        sql.CommandText = " SELECT MaHoiVienTapThe, TenDoanhNghiep, DiaChi, DienThoai, Email  " +
                      "FROM tblHoiVienTapThe WHERE HoiVienTapTheChaID=" + HoiVienTapTheID;



        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        DataView dt = ds.Tables[0].DefaultView;


        PagedDataSource objPds1 = new PagedDataSource();
        objPds1.DataSource = ds.Tables[0].DefaultView;

        gvChiNhanh.DataSource = objPds1;
        gvChiNhanh.DataBind();
        //FileDinhKem_grv.Columns[3].Visible = false; 
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds = null;
        dt = null;

    }
    protected void gvChiNhanh_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Delete")
            {
                id = Convert.ToString(e.CommandArgument);
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void gvChiNhanh_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {


        }
        catch (Exception ex)
        {


        }
    }


    // thanh vien

    public void LoadThanhVienVien(int HoiVienTapTheID)
    {


        SqlCommand sql = new SqlCommand();
        sql.Connection = new SqlConnection(connStr); ;

        String sSqlHoiVien = " SELECT  ROW_NUMBER() OVER (ORDER BY   Ten   ) AS STT, HoiVienCaNhanID, " +
        " MaHoiVienCaNhan ,HoDem +' '+Ten   As Ten  ,DiaChi,    SoChungChiKTV ,NgayCapChungChiKTV ," +
        " CASE GioiTinh WHEN '0' THEN N'Nữ' ELSE N'Nam' END AS GioiTinh     ," +
        " CONVERT(nvarchar(10), NgayGiaNhap , 103) as NgayGiaNhap ,  CONVERT(nvarchar(10),NgaySinh , 103) as NgaySinh ,   " +
        " CASE LoaiHoiVienCaNhan WHEN 1 THEN N'Có' ELSE N'Không' END AS HoiVien   " +
        " FROM tblHoiVienCaNhan where HoiVienTapTheID=" + HoiVienTapTheID + " And LoaiHoiVienCaNhan in (1,2)";


        String sSqlNguoiQuanTam = " SELECT  ROW_NUMBER() OVER (ORDER BY   Ten   ) AS STT, HoiVienCaNhanID, " +
       " MaHoiVienCaNhan ,HoDem +' '+Ten   As Ten  ,DiaChi,    SoChungChiKTV ,NgayCapChungChiKTV ," +
       " CASE GioiTinh WHEN '0' THEN N'Nữ' ELSE N'Nam' END AS GioiTinh     ," +
       " CONVERT(nvarchar(10), NgayGiaNhap , 103) as NgayGiaNhap ,   CONVERT(nvarchar(10),NgaySinh , 103) as NgaySinh ,  " +
       " CASE LoaiHoiVienCaNhan WHEN 1 THEN N'Có' ELSE N'Không' END AS HoiVien   " +
       " FROM tblHoiVienCaNhan where HoiVienTapTheID=" + HoiVienTapTheID + " And LoaiHoiVienCaNhan=0";


        // Hoi Vien 
        sql.CommandText = sSqlHoiVien;
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        DataView dt = ds.Tables[0].DefaultView;


        PagedDataSource objPds1 = new PagedDataSource();
        objPds1.DataSource = ds.Tables[0].DefaultView;

        gvHoiVien.DataSource = objPds1;
        gvHoiVien.DataBind();



        // Nguoi Quan Tam
        sql.CommandText = sSqlNguoiQuanTam;
        ds = DataAccess.RunCMDGetDataSet(sql);
        dt = ds.Tables[0].DefaultView;


        PagedDataSource objPdsNguoiQuanTam = new PagedDataSource();
        objPdsNguoiQuanTam.DataSource = ds.Tables[0].DefaultView;

        gvNguoiQuanTam.DataSource = objPdsNguoiQuanTam;
        gvNguoiQuanTam.DataBind();


        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds = null;
        dt = null;

    }


    // cap nhat kien thuc
    public void LoadCongTy(int HoiVienTapTheID)
    {

        HoiVienTapThe HoiVien = new HoiVienTapThe();
        SqlHoiVienTapTheProvider provide_HoiVienTapThe = new SqlHoiVienTapTheProvider(connStr, true, "");

        HoiVien = provide_HoiVienTapThe.GetByHoiVienTapTheId(HoiVienTapTheID);
        txtTenCongTy.Text = HoiVien.TenDoanhNghiep;

    }

    public void LoadDaoTao(int HoiVienTapTheID)
    {


        SqlCommand sql = new SqlCommand();
        sql.Connection = new SqlConnection(connStr); ;


        sql.CommandText = " SELECT  ROW_NUMBER() OVER (ORDER BY   TuNgay, MaLopHoc   ) AS STT, tblCNKTLopHoc.LopHocID, MaLopHoc ,TenLopHoc ,  " +
        " CONVERT(nvarchar(10),TuNgay , 103) as TuNgay ,   CONVERT(nvarchar(10),DenNgay , 103) as DenNgay     " +
        "     FROM tblCNKTLopHoc  " +
        "     Left join (  select  distinct HoiVienTapTheDangKy , LopHocID from tblCNKTDangKyHocCaNhan where HoiVienTapTheDangKy>0 ) HV " +
        "     ON HV.LopHocID= tblCNKTLopHoc.LopHocID " +
        " WHERE  HV.HoiVienTapTheDangKy= " + HoiVienTapTheID + " ORDER BY tblCNKTLopHoc.TuNgay DESC";

        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        DataView dt = ds.Tables[0].DefaultView;


        PagedDataSource objPds1 = new PagedDataSource();
        objPds1.DataSource = ds.Tables[0].DefaultView;

        gvDaoTao.DataSource = objPds1;
        gvDaoTao.DataBind();

        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds = null;
        dt = null;

    }


    // kiem soat chat luong

    public void LoadHoSo(int HoiVienTapTheID)
    {


        SqlCommand sql = new SqlCommand();
        sql.Connection = new SqlConnection(connStr); ;
        String tempXL = "";
        String tempTotal = "";


        String sqlBC = " SELECT  ROW_NUMBER() OVER (ORDER BY  MaHoSo   ) AS STT, tblKSCLHoSo.MaHoSo, tblKSCLHoSo.MaHoSo as TenHoSo ,  " +
        "  tblKSCLBaoCaoKTKT.BGDTen  As BGDTen ,tblKSCLBaoCaoKTKT.KTVTen  As KTVTen , " +
       "  CASE tblKSCLBaoCaoTongHop.XepLoaiChung  WHEN '1' THEN N'X' ELSE N' ' END AS Loai1 ,    " +
       "  CASE tblKSCLBaoCaoTongHop.XepLoaiChung  WHEN '2' THEN N'X' ELSE N' ' END AS Loai2 , " +
       "  CASE tblKSCLBaoCaoTongHop.XepLoaiChung  WHEN '3' THEN N'X' ELSE N' ' END AS Loai3 , " +
       "  CASE tblKSCLBaoCaoTongHop.XepLoaiChung  WHEN '4' THEN N'X' ELSE N' ' END AS Loai4 " +
         " FROM tblKSCLHoSo  " +
      " LEFT JOIN tblKSCLBaoCaoTongHop ON  tblKSCLBaoCaoTongHop.HoSoID= tblKSCLHoSo.HoSoID " +
      " LEFT JOIN tblKSCLBaoCaoKTKT    ON  tblKSCLBaoCaoKTKT.HoSoID= tblKSCLHoSo.HoSoID " +
      " LEFT JOIN tblKSCLBaoCaoKTHT    ON  tblKSCLBaoCaoKTHT.HoSoID= tblKSCLHoSo.HoSoID " +
      "  WHERE tblKSCLHoSo.HoiVienTapTheID=  " + HoiVienTapTheID;


        // Load BC
        sql.CommandText = sqlBC;

        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        DataView dt = ds.Tables[0].DefaultView;


        PagedDataSource objPds1 = new PagedDataSource();
        objPds1.DataSource = ds.Tables[0].DefaultView;

        gvKSCL.DataSource = objPds1;
        gvKSCL.DataBind();

        txtTongHoSo.Text = ds.Tables[0].DefaultView.Count.ToString();


        // Xep Loai Chung
        String sqlXLChung = " SELECT tblKSCLBaoCaoTongHop.XepLoaiChung as XepLoaiChung  ,Count(  tblKSCLBaoCaoTongHop.XepLoaiChung) as ToTalXlChung  FROM tblKSCLHoSo " +
                             "LEFT JOIN tblKSCLBaoCaoTongHop ON  tblKSCLBaoCaoTongHop.HoSoID= tblKSCLHoSo.HoSoID " +
                            "WHERE tblKSCLHoSo.HoiVienTapTheID=" + HoiVienTapTheID + " GROUP BY tblKSCLBaoCaoTongHop.XepLoaiChung  ";


        sql.CommandText = sqlXLChung;
        ds = DataAccess.RunCMDGetDataSet(sql);
        dt = ds.Tables[0].DefaultView;

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            tempXL = row["XepLoaiChung"] == null ? "" : row["XepLoaiChung"].ToString();
            tempTotal = row["ToTalXlChung"] == null ? "" : row["ToTalXlChung"].ToString();
            if (tempXL == "1")
            {
                txtChungLoai1.Text = tempTotal;
            }
            if (tempXL == "2")
            {
                txtChungLoai2.Text = tempTotal;
            }
            if (tempXL == "3")
            {
                txtChungLoai3.Text = tempTotal;
            }
            if (tempXL == "4")
            {
                txtChungLoai4.Text = tempTotal;
            }

        }


        // Xep Loai HT
        String sqlXLHT = " SELECT  tblKSCLBaoCaoKTHT.XepLoai as XepLoaiHT,Count(  tblKSCLBaoCaoKTHT.XepLoai) as ToTalXlHt  FROM tblKSCLHoSo " +
                             "LEFT JOIN tblKSCLBaoCaoKTHT    ON  tblKSCLBaoCaoKTHT.HoSoID= tblKSCLHoSo.HoSoID " +
                            "WHERE tblKSCLHoSo.HoiVienTapTheID=" + HoiVienTapTheID + " GROUP BY tblKSCLBaoCaoKTHT.XepLoai  ";


        sql.CommandText = sqlXLHT;
        ds = DataAccess.RunCMDGetDataSet(sql);
        dt = ds.Tables[0].DefaultView;

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            tempXL = row["XepLoaiHT"] == null ? "" : row["XepLoaiHT"].ToString();
            tempTotal = row["ToTalXlHt"] == null ? "" : row["ToTalXlHt"].ToString();
            if (tempXL == "1")
            {
                txtHTLoai1.Text = tempTotal;
            }
            if (tempXL == "2")
            {
                txtHTLoai2.Text = tempTotal;
            }
            if (tempXL == "3")
            {
                txtHTLoai3.Text = tempTotal;
            }
            if (tempXL == "4")
            {
                txtHTLoai4.Text = tempTotal;
            }

        }

        // Xep Loai KT
        String sqlXLKT = " SELECT  tblKSCLBaoCaoKTKT.XepLoai as XepLoaiKT,Count(  tblKSCLBaoCaoKTKT.XepLoai) as ToTalXlKt FROM tblKSCLHoSo " +
                             " LEFT JOIN tblKSCLBaoCaoKTKT    ON  tblKSCLBaoCaoKTKT.HoSoID= tblKSCLHoSo.HoSoID " +
                            " WHERE tblKSCLHoSo.HoiVienTapTheID=" + HoiVienTapTheID + " GROUP BY tblKSCLBaoCaoKTKT.XepLoai ";


        sql.CommandText = sqlXLKT;
        ds = DataAccess.RunCMDGetDataSet(sql);
        dt = ds.Tables[0].DefaultView;

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            tempXL = row["XepLoaiKT"] == null ? "" : row["XepLoaiKT"].ToString();
            tempTotal = row["ToTalXlKt"] == null ? "" : row["ToTalXlKt"].ToString();
            if (tempXL == "1")
            {
                txtKTLoai1.Text = tempTotal;
            }
            if (tempXL == "2")
            {
                txtKTLoai2.Text = tempTotal;
            }
            if (tempXL == "3")
            {
                txtKTLoai3.Text = tempTotal;
            }
            if (tempXL == "4")
            {
                txtKTLoai4.Text = tempTotal;
            }

        }



        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds = null;
        dt = null;

    }

    protected string strBoChamPhay(string p)
    {
        if (string.IsNullOrEmpty(p))
            return "0";
        int leng = p.Length;
        if (p.LastIndexOf(',') != -1)
            leng = p.LastIndexOf(',');
        string strOut = p.Substring(0, leng);
        strOut = strOut.Replace(".", string.Empty);
        return strOut;
    }

    protected void grvPhiCongTy_OnDataBound(object sender, EventArgs e)
    {
        double douTongTienNop = 0;
        double douTongTienNo = 0;

        for (int itemp = 0; itemp < grvPhiCongTy.Rows.Count - 1; itemp++)
        {
            GridViewRow row = grvPhiCongTy.Rows[itemp];
            Label lblTienNop = (Label)row.FindControl("TienNop");
            douTongTienNop += Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
            Label lblTienNo = (Label)row.FindControl("TienNo");
            douTongTienNo += Convert.ToDouble(strBoChamPhay(lblTienNo.Text));
        }

        int i = grvPhiCongTy.Rows.Count;
        if (i != 0)
        {
            GridViewRow gvRow = grvPhiCongTy.Rows[i - 1];
            if (gvRow != null)
            {


                Label lblTenLoaiPhi = (Label)gvRow.Cells[0].FindControl("TenLoaiPhi");
                lblTenLoaiPhi.Text = "Tổng";

                Label lblTongTienNop = (Label)gvRow.Cells[2].FindControl("TienNop");
                Label lblTongTienNo = (Label)gvRow.Cells[3].FindControl("TienNo");



                lblTongTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTongTienNop);
                lblTongTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTongTienNo);

                gvRow.Cells[0].ControlStyle.Font.Bold = true;
                gvRow.Cells[1].ControlStyle.Font.Bold = true;
                gvRow.Cells[2].ControlStyle.Font.Bold = true;
                gvRow.Cells[3].ControlStyle.Font.Bold = true;
                //TextBox a = (TextBox)gvRow.Cells[4].FindControl("TienNop");
                //a.ReadOnly = true;
                //TextBox b = (TextBox)gvRow.Cells[5].FindControl("TienNo");
                //b.ReadOnly = true;

                grvPhiCongTy.Columns[2].HeaderStyle.Width = 200;
                grvPhiCongTy.Columns[3].HeaderStyle.Width = 200;

                //string[] Key = new string[] {"PhatSinhPhiID", "PhiID", "LoaiPhi"};
                //grvPhiCongTy.DataKeyNames = Key;
            }



        }
    }

    private void LoadDropDownNamThanhToan(int ID)
    {
        string sql = "SELECT DISTINCT YEAR(NgayPhatSinh) AS Nam ";
        sql += " FROM tblTTPhatSinhPhi";

        //sql += " WHERE LoaiHoiVien = '" + Session["LoaiHoiVien"].ToString() + "' AND HoiVienID = " + ID;
        sql += " ORDER BY Nam DESC";
        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        drNamThanhToan.DataSource = dtb;
        drNamThanhToan.DataBind();

        if (Session["NamThanhToan"] != null)
        {
            RefeshGrvPhiTapThe(ID, Session["NamThanhToan"].ToString());
            drNamThanhToan.SelectedValue = Session["NamThanhToan"].ToString();
        }
        else if (!string.IsNullOrEmpty(drNamThanhToan.SelectedValue))
        {
            Session["NamThanhToan"] = drNamThanhToan.SelectedValue;
            RefeshGrvPhiTapThe(ID, drNamThanhToan.SelectedValue);
        }
    }

    protected void drNamThanhToan_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["NamThanhToan"] = drNamThanhToan.SelectedValue;
        RefeshGrvPhiTapThe(Convert.ToInt32(hidendHoiVienTapTheID.Value), drNamThanhToan.SelectedValue);
    }

    protected void RefeshGrvPhiTapThe(int iHoiVienID, string Nam)
    {
        dtbPhiTapThe = new DataTable();
        dtbPhiTapThe.Columns.Add("PhiID", typeof(int));
        dtbPhiTapThe.Columns.Add("PhatSinhPhiID", typeof(int));
        dtbPhiTapThe.Columns.Add("HoiVienID", typeof(int));
        dtbPhiTapThe.Columns.Add("MaPhi", typeof(string));
        dtbPhiTapThe.Columns.Add("LoaiPhi", typeof(string));
        dtbPhiTapThe.Columns.Add("TenLoaiPhi", typeof(string));
        dtbPhiTapThe.Columns.Add("TongTien", typeof(decimal));
        dtbPhiTapThe.Columns.Add("TienNop", typeof(decimal));
        dtbPhiTapThe.Columns.Add("TienNo", typeof(decimal));

        string LoaiHoiVienTapThe = "";
        SqlCommand sql = new SqlCommand();
        try
        {

            sql.CommandText = "SELECT LoaiHoiVienTapThe FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + iHoiVienID;

            LoaiHoiVienTapThe = DataAccess.DLookup(sql);



        }
        catch
        { }
        if (iHoiVienID == 0)
        {
            //DataTable dtbtemp = new DataTable();
            //dtbtemp.Columns.Add("");
            //dtbtemp.Columns.Add("MaPhi");
            //dtbtemp.Columns.Add("TenLoaiPhi");
            //dtbtemp.Columns.Add("TongTien");
            //dtbtemp.Columns.Add("TienNop");
            //thanhtoanphicanhanchitiet_grv.DataSource = dtbtemp;
            //thanhtoanphicanhanchitiet_grv.DataBind();
            return;
        }
        else
        {
            //Nếu loại = 0: CTKT chưa có ID: Không có phí Hội viên tổ chức => CTKT (LoaiHoiVien = 3)
            //Nếu loại = 1: Có tất => HVTC (LoaiHoiVien = 4)
            //Nếu loại = 2: CTKT có ID: Không có phí Hội viên tổ chức => CTKT (LoaiHoiVien = 3)
            if (LoaiHoiVienTapThe == "1")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiHoiVien] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + ",'" + Nam + "'";
                dtbPhiTapThe = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            }

            if (LoaiHoiVienTapThe == "1")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiKSCL] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }
            else if (LoaiHoiVienTapThe == "0" || LoaiHoiVienTapThe == "2")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiKSCL] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," + (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }

            if (LoaiHoiVienTapThe == "1")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiDangLogo] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiDangLogo + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }
            else if (LoaiHoiVienTapThe == "0" || LoaiHoiVienTapThe == "2")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiDangLogo] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiDangLogo + "," + (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }

            if (LoaiHoiVienTapThe == "1")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiKhac] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }
            else if (LoaiHoiVienTapThe == "0" || LoaiHoiVienTapThe == "2")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiKhac] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + "," + (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }

            if (Session["PhatSinhPhiTapThe"] != null)
                PhatSinhPhiTapThe = (List<string>)Session["PhatSinhPhiTapThe"];
            else
                PhatSinhPhiTapThe = new List<string>();

            foreach (DataRow row in dtbPhiTapThe.Rows)
            {
                PhatSinhPhiTapThe.Add(row["PhatSinhPhiID"].ToString());
            }

            if (PhatSinhPhiTapThe.Count != 0)
                Session["PhatSinhPhiTapThe"] = PhatSinhPhiTapThe;
            else
                Session["PhatSinhPhiTapThe"] = null;

            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiHoiVien_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            DataTable dtbTempHoiVien = DataAccess.RunCMDGetDataSet(sql).Tables[0];

            Double douTongPhiHoiVienCaNhan = 0;
            Double douTongPhiHoiVienCaNhan_TienNop = 0;
            Double douTongPhiHoiVienCaNhan_ConNo = 0;

            if (dtbTempHoiVien.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTempHoiVien.Rows)
                {
                    douTongPhiHoiVienCaNhan += Convert.ToDouble(dtr["TongTien"]);
                    douTongPhiHoiVienCaNhan_TienNop += Convert.ToDouble(dtr["TienNop"]);
                    douTongPhiHoiVienCaNhan_ConNo += Convert.ToDouble(dtr["SoPhiConNo"]);
                }

                DataRow dtrThem1 = dtbPhiTapThe.NewRow();
                dtrThem1["PhiID"] = 0;
                dtrThem1["PhatSinhPhiID"] = 0;
                dtrThem1["HoiVienID"] = iHoiVienID;
                dtrThem1["MaPhi"] = "";
                dtrThem1["LoaiPhi"] = "11";
                dtrThem1["TenLoaiPhi"] = "Phí HVCN";
                dtrThem1["TongTien"] = douTongPhiHoiVienCaNhan;
                dtrThem1["TienNop"] = douTongPhiHoiVienCaNhan_TienNop;
                dtrThem1["TienNo"] = douTongPhiHoiVienCaNhan_ConNo;
                dtbPhiTapThe.Rows.Add(dtrThem1);
            }

            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKSCL_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            DataTable dtbTempKSCL = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKSCL_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTempKSCL.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

            Double douTongPhiKSCLCaNhan = 0;
            Double douTongPhiKSCLCaNhan_TienNop = 0;
            Double douTongPhiKSCLCaNhan_ConNo = 0;

            if (dtbTempKSCL.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTempKSCL.Rows)
                {
                    douTongPhiKSCLCaNhan += Convert.ToDouble(dtr["TongTien"]);
                    douTongPhiKSCLCaNhan_TienNop += Convert.ToDouble(dtr["TienNop"]);
                    douTongPhiKSCLCaNhan_ConNo += Convert.ToDouble(dtr["SoPhiConNo"]);
                }
                DataRow dtrThem2 = dtbPhiTapThe.NewRow();
                dtrThem2["PhiID"] = 0;
                dtrThem2["PhatSinhPhiID"] = 0;
                dtrThem2["HoiVienID"] = iHoiVienID;
                dtrThem2["MaPhi"] = "";
                dtrThem2["LoaiPhi"] = "51";
                dtrThem2["TenLoaiPhi"] = "Phí KSCL theo cá nhân";
                dtrThem2["TongTien"] = douTongPhiKSCLCaNhan;
                dtrThem2["TienNop"] = douTongPhiKSCLCaNhan_TienNop;
                dtrThem2["TienNo"] = douTongPhiKSCLCaNhan_ConNo;
                dtbPhiTapThe.Rows.Add(dtrThem2);
            }


            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            DataTable dtbTempCNKT = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTempCNKT.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTempCNKT.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

            Double douTongPhiCNKTCaNhan = 0;
            Double douTongPhiCNKTCaNhan_TienNop = 0;
            Double douTongPhiCNKTCaNhan_ConNo = 0;

            if (dtbTempCNKT.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTempCNKT.Rows)
                {
                    douTongPhiCNKTCaNhan += Convert.ToDouble(dtr["TongTien"]);
                    douTongPhiCNKTCaNhan_TienNop += Convert.ToDouble(dtr["TienNop"]);
                    douTongPhiCNKTCaNhan_ConNo += Convert.ToDouble(dtr["SoPhiConNo"]);
                }

                DataRow dtrThem3 = dtbPhiTapThe.NewRow();
                dtrThem3["PhiID"] = 0;
                dtrThem3["PhatSinhPhiID"] = 0;
                dtrThem3["HoiVienID"] = iHoiVienID;
                dtrThem3["MaPhi"] = "";
                dtrThem3["LoaiPhi"] = "41";
                dtrThem3["TenLoaiPhi"] = "Phí CNKT";
                dtrThem3["TongTien"] = douTongPhiCNKTCaNhan;
                dtrThem3["TienNop"] = douTongPhiCNKTCaNhan_TienNop;
                dtrThem3["TienNo"] = douTongPhiCNKTCaNhan_ConNo;
                dtbPhiTapThe.Rows.Add(dtrThem3);
            }

            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            DataTable dtbTempKhac = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTempKhac.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

            Double douTongPhiKhacCaNhan = 0;
            Double douTongPhiKhacCaNhan_TienNop = 0;
            Double douTongPhiKhacCaNhan_ConNo = 0;

            if (dtbTempKhac.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTempKhac.Rows)
                {
                    douTongPhiKhacCaNhan += Convert.ToDouble(dtr["TongTien"]);
                    douTongPhiKhacCaNhan_TienNop += Convert.ToDouble(dtr["TienNop"]);
                    douTongPhiKhacCaNhan_ConNo += Convert.ToDouble(dtr["SoPhiConNo"]);
                }
                DataRow dtrThem4 = dtbPhiTapThe.NewRow();
                dtrThem4["PhiID"] = 0;
                dtrThem4["PhatSinhPhiID"] = 0;
                dtrThem4["HoiVienID"] = iHoiVienID;
                dtrThem4["MaPhi"] = "";
                dtrThem4["LoaiPhi"] = "31";
                dtrThem4["TenLoaiPhi"] = "Phí khác theo cá nhân";
                dtrThem4["TongTien"] = douTongPhiKhacCaNhan;
                dtrThem4["TienNop"] = douTongPhiKhacCaNhan_TienNop;
                dtrThem4["TienNo"] = douTongPhiKhacCaNhan_ConNo;
                dtbPhiTapThe.Rows.Add(dtrThem4);
            }

            double douTongTien = 0;
            double douTongTien_TienNop = 0;
            double douTongTien_ConNo = 0;
            foreach (DataRow dtr in dtbPhiTapThe.Rows)
            {
                if (dtr["TongTien"] != null)
                    douTongTien += Convert.ToDouble(dtr["TongTien"]);
                if (dtr["TienNop"] != null)
                    douTongTien_TienNop += Convert.ToDouble(dtr["TienNop"]);
                if (dtr["TienNo"] != null)
                    douTongTien_ConNo += Convert.ToDouble(dtr["TienNo"]);
            }

            DataRow dtrThem = dtbPhiTapThe.NewRow();
            dtrThem["PhiID"] = 0;
            dtrThem["PhatSinhPhiID"] = 0;
            dtrThem["HoiVienID"] = iHoiVienID;
            dtrThem["MaPhi"] = "";
            dtrThem["LoaiPhi"] = "10";
            dtrThem["TenLoaiPhi"] = "Tổng";
            dtrThem["TongTien"] = douTongTien;
            dtrThem["TienNop"] = douTongTien_TienNop;
            dtrThem["TienNo"] = douTongTien_ConNo;
            dtbPhiTapThe.Rows.Add(dtrThem);
            //DataColumn[] columns = new DataColumn[1];
            //columns[0] = dtbPhiTapThe.Columns["LoaiPhi"];
            //dtbPhiTapThe.PrimaryKey = columns;
        }
        grvPhiCongTy.DataSource = dtbPhiTapThe;
        grvPhiCongTy.DataBind();
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        dtbPhiTapThe = null;

        LoadGridCaNhan(iHoiVienID, Nam);
    }

    private void LoadGridCaNhan(int iHoiVienID, string Nam)
    {


        //  string strLoaiPhi = LoaiPhi.SelectedValue;
        DataTable dtbTemp = new DataTable();
        dtbTemp.Columns.Add("HoiVienID");
        dtbTemp.Columns.Add("STT");
        dtbTemp.Columns.Add("MaHoiVien");
        dtbTemp.Columns.Add("HoTen");
        dtbTemp.Columns.Add("SoChungChiKTV");
        dtbTemp.Columns.Add("NgayCapChungChiKTV");
        dtbTemp.Columns.Add("DienGiai");
        dtbTemp.Columns.Add("TongTien", typeof(decimal));
        dtbTemp.Columns.Add("TienNop", typeof(decimal));
        dtbTemp.Columns.Add("SoPhiConNo", typeof(decimal));
        dtbTemp.Columns.Add("PhatSinhPhiId");
        dtbTemp.Columns.Add("PhiId");
        dtbTemp.Columns.Add("LoaiPhi");
        //if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString())
        //{
        //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

        //Session["dtbPhiCaNhan"] = dtbTemp;

        SqlCommand sql = new SqlCommand();
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiHoiVien_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];

        //}
        //else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString())
        //{
        //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", 1000000, 1000000, "0", "1", "1", strLoaiPhi);
        //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", 1000000, 1000000, "0", "1", "1", strLoaiPhi);

        //Session["dtbPhiCaNhan"] = dtbTemp;

        sql = new SqlCommand();
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        //}
        //else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString())
        //{
        //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
        //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
        //dtbTemp.Rows.Add("3", "3", "3", "C", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

        //Session["dtbPhiCaNhan"] = dtbTemp;
        sql = new SqlCommand();
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKSCL_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKSCL_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        //}
        //else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString())
        //{
        //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
        //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
        //dtbTemp.Rows.Add("3", "3", "3", "C", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
        //dtbTemp.Rows.Add("4", "4", "4", "D", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

        //Session["dtbPhiCaNhan"] = dtbTemp;
        sql = new SqlCommand();
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
        dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        //}
        //SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT HoiVienID, '1' as STT, B.MaHoiVien, B.HoDem + ' ' + B.Ten as HoTen, "
        //+ "B.SoChungChiKTV, B.DonViCongTac, FROM tblDMDoiTuongNopPhi";
        //DataSet ds = new DataSet();
        //DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];

        if (Session["PhatSinhPhiCaNhan"] != null)
            PhatSinhPhiCaNhan = (List<string>)Session["PhatSinhPhiCaNhan"];
        else
            PhatSinhPhiCaNhan = new List<string>();

        foreach (DataRow row in dtbTemp.Rows)
        {
            PhatSinhPhiCaNhan.Add(row["PhatSinhPhiID"].ToString());
        }

        if (PhatSinhPhiCaNhan.Count != 0)
            Session["PhatSinhPhiCaNhan"] = PhatSinhPhiCaNhan;
        else
            Session["PhatSinhPhiCaNhan"] = null;

        thanhtoanphicanhan_grv.DataSource = dtbTemp;
        thanhtoanphicanhan_grv.DataBind();

        //vDanhSoGridCaNhan();
    }

    public DataSet rpt_NopPhiTapThe(int HoiVienTapTheID)
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("HoiVienTapTheID", HoiVienTapTheID));

            objDBHelper = ConnectionControllers.Connect("VACPA");

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("rpt_NopPhiTapThe", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }



    #region GridKhenThuongKyLuat
    protected void ButtonAddKhenThuong_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_KhenThuong();
    }

    protected void ButtonAddKyLuat_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_KyLuat();
    }

    private void SetInitialRow_KhenThuong()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayThang", typeof(string)));
        dt.Columns.Add(new DataColumn("Cap", typeof(string)));
        dt.Columns.Add(new DataColumn("LyDo", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["NgayThang"] = "";
        dr["Cap"] = "";
        dr["LyDo"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["KhenThuong"] = dt;

        gvKhenThuong.DataSource = dt;
        gvKhenThuong.DataBind();
    }
    private void AddNewRowToGrid_KhenThuong()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["KhenThuong"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KhenThuong"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        dtCurrentTable.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                        dtCurrentTable.Rows[i - 1]["Cap"] = drHinhThuc.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["LyDo"] = lydo.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = "";
                    drCurrentRow["Cap"] = "";
                    drCurrentRow["LyDo"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["NgayThang"] = "";
                    dr["Cap"] = "";
                    dr["LyDo"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["KhenThuong"] = dtCurrentTable;

                gvKhenThuong.DataSource = dtCurrentTable;
                gvKhenThuong.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_KhenThuong();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_KhenThuong()
    {
        int rowIndex = 0;
        if (ViewState["KhenThuong"] != null)
        {
            DataTable dt = (DataTable)ViewState["KhenThuong"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    ngaythang.Text = dt.Rows[i]["NgayThang"].ToString();
                    drHinhThuc.SelectedValue = dt.Rows[i]["Cap"].ToString();
                    lydo.Text = dt.Rows[i]["LyDo"].ToString();
                    rowIndex++;
                }
            }
        }
    }


    private void SetInitialRow_KyLuat()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayThang", typeof(string)));
        dt.Columns.Add(new DataColumn("Cap", typeof(string)));
        dt.Columns.Add(new DataColumn("LyDo", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["NgayThang"] = "";
        dr["Cap"] = "";
        dr["LyDo"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["KyLuat"] = dt;

        gvKyLuat.DataSource = dt;
        gvKyLuat.DataBind();
    }
    private void AddNewRowToGrid_KyLuat()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["KyLuat"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KyLuat"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        dtCurrentTable.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                        dtCurrentTable.Rows[i - 1]["Cap"] = drHinhThuc.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["LyDo"] = lydo.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = "";
                    drCurrentRow["Cap"] = "";
                    drCurrentRow["LyDo"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["NgayThang"] = "";
                    dr["Cap"] = "";
                    dr["LyDo"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["KyLuat"] = dtCurrentTable;

                gvKyLuat.DataSource = dtCurrentTable;
                gvKyLuat.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_KyLuat();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_KyLuat()
    {
        int rowIndex = 0;
        if (ViewState["KyLuat"] != null)
        {
            DataTable dt = (DataTable)ViewState["KyLuat"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    ngaythang.Text = dt.Rows[i]["NgayThang"].ToString();
                    drHinhThuc.SelectedValue = dt.Rows[i]["Cap"].ToString();
                    lydo.Text = dt.Rows[i]["LyDo"].ToString();

                    rowIndex++;
                }
            }
        }
    }

    private void LoadKhenThuongKyLuat(string ID)
    {
        string sql = "SELECT CONVERT(VARCHAR, A.NgayThang, 103) AS NgayThang, A.HinhThucKhenThuongID, A.LyDo, B.TenHinhThucKhenThuong AS TenHinhThuc FROM tblKhenThuongKyLuat A INNER JOIN tblDMHinhThucKhenThuong B ON A.HinhThucKhenThuongID = B.HinhThucKhenThuongID";

        sql += " WHERE Loai = 1 AND HoiVienTapTheID = " + ID;
        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        if (dtb.Rows.Count != 0)
        {
            if (ViewState["KhenThuong"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KhenThuong"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = row["NgayThang"].ToString();
                    drCurrentRow["Cap"] = row["HinhThucKhenThuongID"].ToString();
                    drCurrentRow["LyDo"] = row["LyDo"].ToString();
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["KhenThuong"] = dtCurrentTable;

                gvKhenThuong.DataSource = dtCurrentTable;
                gvKhenThuong.DataBind();

                SetPreviousData_KhenThuong();
            }
        }

        ds.Clear();
        dtb.Clear();

        sql = "SELECT CONVERT(VARCHAR, A.NgayThang, 103) AS NgayThang, A.HinhThucKyLuatID, A.LyDo, B.TenHinhThucKyLuat AS TenHinhThuc FROM tblKhenThuongKyLuat A INNER JOIN tblDMHinhThucKyLuat B ON A.HinhThucKyLuatID = B.HinhThucKyLuatID";

        sql += " WHERE Loai = 2 AND HoiVienTapTheID = " + ID;
        ds = new DataSet();
        dtb = new DataTable();
        da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        if (dtb.Rows.Count != 0)
        {
            if (ViewState["KyLuat"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["KyLuat"];
                dtCurrentTable.Rows.Clear();
                foreach (DataRow row in dtb.Rows)
                {
                    DataRow drCurrentRow = null;
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["NgayThang"] = row["NgayThang"].ToString();
                    drCurrentRow["Cap"] = row["HinhThucKyLuatID"].ToString();
                    drCurrentRow["LyDo"] = row["LyDo"].ToString();
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }

                ViewState["KyLuat"] = dtCurrentTable;

                gvKyLuat.DataSource = dtCurrentTable;
                gvKyLuat.DataBind();

                SetPreviousData_KyLuat();
            }
        }
    }


    protected void gvKhenThuong_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvKhenThuong_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["KhenThuong"] != null)
        {
            DataTable dt = (DataTable)ViewState["KhenThuong"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    dt.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                    dt.Rows[i - 1]["Cap"] = drHinhThuc.SelectedValue;
                    dt.Rows[i - 1]["LyDo"] = lydo.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["KhenThuong"] = dt;

            gvKhenThuong.DataSource = dt;
            gvKhenThuong.DataBind();

            SetPreviousData_KhenThuong();
        }
    }
    protected void gvKhenThuong_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drHinhThuc = (DropDownList)e.Row.FindControl("drHinhThuc");

                conn.Open();

                DataSet ds = new DataSet();
                DataTable dtb = new DataTable();

                string sql = "";

                sql = "SELECT 0 AS HinhThucKhenThuongID, N'<< Lựa chọn >>' AS TenHinhThucKhenThuong UNION ALL (SELECT HinhThucKhenThuongID, TenHinhThucKhenThuong FROM tblDMHinhThucKhenThuong)";

                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                da.Fill(ds);
                dtb = ds.Tables[0];
                drHinhThuc.DataSource = dtb;
                drHinhThuc.DataBind();

                conn.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void gvKyLuat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvKyLuat_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["KyLuat"] != null)
        {
            DataTable dt = (DataTable)ViewState["KyLuat"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                    DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                    TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                    dt.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                    dt.Rows[i - 1]["Cap"] = drHinhThuc.SelectedValue;
                    dt.Rows[i - 1]["LyDo"] = lydo.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["KyLuat"] = dt;

            gvKyLuat.DataSource = dt;
            gvKyLuat.DataBind();

            SetPreviousData_KyLuat();
        }
    }
    protected void gvKyLuat_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drHinhThuc = (DropDownList)e.Row.FindControl("drHinhThuc");

                conn.Open();

                DataSet ds = new DataSet();
                DataTable dtb = new DataTable();

                string sql = "";

                sql = "SELECT 0 AS HinhThucKyLuatID, N'<< Lựa chọn >>' AS TenHinhThucKyLuat UNION ALL (SELECT HinhThucKyLuatID, TenHinhThucKyLuat FROM tblDMHinhThucKyLuat)";

                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                da.Fill(ds);
                dtb = ds.Tables[0];
                drHinhThuc.DataSource = dtb;
                drHinhThuc.DataBind();

                conn.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }

    #endregion

    protected void btnTiepNhan_Click(object sender, EventArgs e)
    {
        int donHoiVienTtId = 0;
        try
        {
            donHoiVienTtId = Convert.ToInt32(hidendXoaTenHoiVienID.Value);
        }
        catch { }
        if (donHoiVienTtId != null && donHoiVienTtId != 0)
        {
            UpdateStatus(donHoiVienTtId, trangthaiChoDuyetID, "", "", "");
            cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Tiếp nhận đơn xin xóa tên hội viên " + objHoiVIenTapThe.TenDoanhNghiep);
            Response.Redirect("admin.aspx?page=hosohoivientapthe_edit&prepage=xoatenhoivientapthe&id=" + Request.QueryString["id"] + "&xoatenhoivienid=" + Request.QueryString["xoatenhoivienid"] + "&idtinhtrang=3&act=edit&thongbao=1");
        }
    }

    protected void btnTuChoiTiepNhan_Click(object sender, EventArgs e)
    {
        int donHoiVienTtId = 0;
        try
        {
            donHoiVienTtId = Convert.ToInt32(hidendXoaTenHoiVienID.Value);
        }
        catch { }
        if (donHoiVienTtId != null && donHoiVienTtId != 0)
        {
            string Email = "";
            UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), txtLyDoTuChoi.Text, ref Email);

            // Send mail
            string body = "";
            body += "VACPA từ chối tiếp nhận xóa tên hội viên tổ chức của công ty<BR/>";
            body += "Lý do từ chối: " + txtLyDoTuChoi.Text + " <BR/>";

            string msg = "";
            if (Email != "")
            {
                if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối tiếp nhận đơn xin xóa tên hội viên tổ chức", body, ref msg))
                { }
            }
            cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối tiếp nhận đơn xin xóa tên hội viên " + objHoiVIenTapThe.TenDoanhNghiep);
            Response.Redirect("admin.aspx?page=hosohoivientapthe_edit&prepage=xoatenhoivientapthe&id=" + Request.QueryString["id"] + "&xoatenhoivienid=" + Request.QueryString["xoatenhoivienid"] + "&idtinhtrang=4&act=edit&thongbao=1");
        }
    }
    protected void btnXoaTen_Click(object sender, EventArgs e)
    {
        try
        {
            int donHoiVienTtId = 0;
            try
            {
                donHoiVienTtId = Convert.ToInt32(hidendXoaTenHoiVienID.Value);
            }
            catch { }
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {


                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiDaPheDuyet, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), "", ref Email);

                // Send mail
                string body = "";
                body += "Công ty đã được phê duyệt xin thôi gia nhập hội viên tổ chức, bạn vẫn có thể đăng nhập vào hệ thống và sử dụng phần mềm với người sử dụng là công ty kiểm toán<BR/>";

                string msg = "";
                if (Email != "")
                {
                    if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Phê duyệt xin thôi gia nhập hội viên tổ chức tại trang tin điện tử VACPA", body, ref msg))
                    { }
                }
                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin xóa tên hội viên " + objHoiVIenTapThe.TenDoanhNghiep);
                Response.Redirect("admin.aspx?page=hosohoivientapthe_edit&prepage=xoatenhoivientapthe&id=" + Request.QueryString["id"] + "&xoatenhoivienid=" + Request.QueryString["xoatenhoivienid"] + "&idtinhtrang=5&act=edit&thongbao=1");
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }
    protected void btnHuyXoaTen_Click(object sender, EventArgs e)
    {

        try
        {
            int donHoiVienTtId = 0;
            try
            {
                donHoiVienTtId = Convert.ToInt32(hidendXoaTenHoiVienID.Value);
            }
            catch { }
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                string TenDangNhap = "";
                string MatKhau = "";
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiDaPheDuyet, cm.Admin_TenDangNhap,
                    DateTime.Now.ToString("ddMMyyyy"), "", ref Email);

                // Send mail
                string body = "";
                body += "Công ty đã được phê duyệt kết nạp hội viên tổ chức, có thể sử dụng tài khoản sau để đăng nhập vào phần mềm:<BR/>";
                body += "Tên đăng nhập: <B>" + TenDangNhap + "</B><BR/>";
                body += "Mật khẩu: <B>" + MatKhau + "</B><BR/>";

                string msg = "";

                //if (SmtpMail.Send("BQT WEB VACPA", Email, "Thông tin tài khoản hội viên tổ chức tại trang tin điện tử VACPA", body, ref msg))
                //{    }
                //else
                //{
                //    isSendMail = false;

                //}

                string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>";

                placeMessage.Controls.Add(new LiteralControl(thongbao));

                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin xóa tên hội viên " + objHoiVIenTapThe.TenDoanhNghiep);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }
    protected void btnTuChoiXoaTen_Click(object sender, EventArgs e)
    {
        try
        {
            string sLyDoTuChoi = txtLyDoTuChoi.Text;
            int donHoiVienTtId = 0;
            try
            {
                donHoiVienTtId = Convert.ToInt32(hidendXoaTenHoiVienID.Value);
            }
            catch { }
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiTraLaiID, cm.Admin_TenDangNhap,
                        DateTime.Now.ToString("ddMMyyyy"), sLyDoTuChoi, ref Email);

                // Send mail
                string body = "";
                body += "VACPA từ chối phê duyệt xóa tên hội viên tổ chức của công ty<BR/>";
                body += "Lý do từ chối: " + sLyDoTuChoi + " <BR/>";

                string msg = "";
                if (Email != "")
                {
                    if (SmtpMail.Send("BQT WEB VACPA", Email, "VACPA - Từ chối duyệt yêu cầu xóa tên hội viên tổ chức", body, ref msg))
                    { }
                }
                cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối duyệt đơn xin xóa tên hội viên " + objHoiVIenTapThe.TenDoanhNghiep);
                Response.Redirect("admin.aspx?page=hosohoivientapthe_edit&prepage=xoatenhoivientapthe&id=" + Request.QueryString["id"] + "&xoatenhoivienid=" + Request.QueryString["xoatenhoivienid"] + "&idtinhtrang=2&act=edit&thongbao=1");
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }


    public bool UpdateStatus(int XoaTenHoiVienID, int TinhTrangID, String NguoiDuyet,
                          String NgayDuyet, String LyDoTuChoi)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("XoaTenHoiVienID", XoaTenHoiVienID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));


            objDBHelper = ConnectionControllers.Connect("VACPA");
            return objDBHelper.ExecFunction("sp_xoatenhoivientt_tt_xetduyet", arrParams);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }
    public bool UpdateStatus(int XoaTenHoiVienID, int TinhTrangID, String NguoiDuyet,
                               String NgayDuyet, String LyDoTuChoi, ref string Email)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("XoaTenHoiVienID", XoaTenHoiVienID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));



            DBHelpers.DataParameter outEmail = new DBHelpers.DataParameter("Email", Email);
            outEmail.OutPut = true;
            arrParams.Add(outEmail);


            objDBHelper = ConnectionControllers.Connect("VACPA");
            bool bRedulte = objDBHelper.ExecFunction("sp_xoatenhoivientt_tt_xetduyet", arrParams);

            Email = outEmail.Param_Value.ToString();

            return bRedulte;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }
    protected void checkPermit(string button)
    {


        if (!string.IsNullOrEmpty(Request.QueryString["prepage"]))
        {

            if (Request.QueryString["prepage"] == "xoatenhoivientapthe")//goi tu form xoa ten
            {
                if (button == "btnGhi")
                {
                    Response.Write(@" style=""display: none;"" ");
                }
                else if (button == "btnTiepNhan")
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["idtinhtrang"]) && Request.QueryString["idtinhtrang"] == tk_TTChoTiepNhan)
                    {
                        Response.Write(@"   ");
                    }
                    else
                    {
                        Response.Write(@" style=""display: none;"" ");
                    }
                }
                else if (button == "btnTuChoiTiepNhan")
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["idtinhtrang"]) && Request.QueryString["idtinhtrang"] == tk_TTChoTiepNhan)
                    {
                        Response.Write(@"   ");
                    }
                    else
                    {
                        Response.Write(@" style=""display: none;"" ");
                    }
                }
                else if (button == "btnXoaTen")
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["idtinhtrang"]) && Request.QueryString["idtinhtrang"] == tk_TTChoDuyet)
                    {
                        Response.Write(@"   ");
                    }
                    else
                    {
                        Response.Write(@" style=""display: none;"" ");
                    }
                }
                //else if (button == "btnHuyXoaTen")
                //{
                //    if (!String.IsNullOrEmpty(Request.QueryString["idtinhtrang"]) && Request.QueryString["idtinhtrang"] == tk_TTDaDuyet)
                //    {
                //        Response.Write(@"   ");
                //    }
                //    else
                //    {
                //        Response.Write(@" style=""display: none;"" ");
                //    }
                //}
                else if (button == "btnTuChoiXoaTen")
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["idtinhtrang"]) && Request.QueryString["idtinhtrang"] == tk_TTChoDuyet)
                    {
                        Response.Write(@"   ");
                    }
                    else
                    {
                        Response.Write(@" style=""display: none;"" ");
                    }
                }
                else
                {
                    Response.Write(@" style=""display: none;"" ");
                }

            }
            else if (Request.QueryString["prepage"] == "danhsachkhachhang")//goi tu form danh sách khách hàng
            {
                if (button == "btnGhi")
                {
                    Response.Write(@"  ");
                }
                else if (button == "btnTiepNhan")
                {
                    Response.Write(@" style=""display: none;"" ");
                }
                else if (button == "btnTuChoiTiepNhan")
                {
                    Response.Write(@" style=""display: none;"" ");
                }
                else if (button == "btnXoaTen")
                {
                    Response.Write(@" style=""display: none;"" ");
                }
                //else if (button == "btnHuyXoaTen")
                //{
                //    Response.Write(@" style=""display: none;"" ");
                //}
                else if (button == "btnTuChoiXoaTen")
                {
                    Response.Write(@" style=""display: none;"" ");
                }

                else
                {
                    Response.Write(@" style=""display: none;"" ");
                }
            }
            else // ko xac dinh dc danh sach goi den
            {
                Response.Write(@" style=""display: none;"" ");
            }
        }
        else
        { // ko xac dinh dc danh sach goi den
            Response.Write(@" style=""display: none;"" ");
        }



    }

    protected void setDisplayLyDoxoa()
    {


        if (String.IsNullOrEmpty(Request.QueryString["idtinhtrang"]) ||
             Request.QueryString["idtinhtrang"] == trangthaiDaPheDuyet.ToString())  // Them moi , da duyet ==> ko hien thi
        {
            Response.Write(@" style=""display: none;"" ");
        }
        else // cac truong hop khac
        {
            Response.Write(@" ");

        }



    }

    protected void btnKetXuat_Click(object sender, EventArgs e)
    {
        string strid = Request.QueryString["id"];
        if (!string.IsNullOrEmpty(strid))
        {

            List<objThongTinDoanhNghiep_CT> lst = new List<objThongTinDoanhNghiep_CT>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep, TenTiengAnh, SoQuyetDinhKetNap, NgayQuyetDinhKetNap "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE HoiVienTapTheID = '" + strid + "'";
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            List<Anh_Tr> lst_Anh = new List<Anh_Tr>();

            foreach (DataRow tem in dt.Rows)
            {
                objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();

                new1.TenDoanhNghiep = Converter.ConvertToTCVN3(tem["TenDoanhNghiep"].ToString().Replace("\n", ""));

                new1.TenTiengAnh = tem["TenTiengAnh"].ToString() + " is full Member of VACPA";
                new1.SoQD = Converter.ConvertToTCVN3(tem["SoQuyetDinhKetNap"].ToString());
                new1.NgayCN = !string.IsNullOrEmpty(tem["NgayQuyetDinhKetNap"].ToString()) ? "Hà Nội, ngày " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Day + " tháng " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Month + " năm " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Year : "Hà Nội, ngày....tháng....năm......";

                lst.Add(new1);

            }

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/GiayChungNhanHoiVienTapThe.rpt"));

            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            rpt.Database.Tables["Anh_Tr"].SetDataSource(lst_Anh);

            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "GiayChungNhanHoiVienTapThe");

        }
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    public static string ConvertToTCVN3(string value)
    {
        string text = "";
        int i = 0;
        while (i < value.Length)
        {
            char value2 = char.Parse(value.Substring(i, 1));
            short num = System.Convert.ToInt16(value2);
            if (num <= 259)
            {
                if (num <= 202)
                {
                    if (num != 194)
                    {
                        switch (num)
                        {
                            case 201: text += "Ð"; break;
                            case 202: text += "ü"; break;
                            default: goto IL_7AF;
                        }
                    }
                    else text += "¢";
                }
                else
                {
                    if (num != 212)
                    {
                        switch (num)
                        {
                            case 224: text += "µ"; break;
                            case 225: text += "¸"; break;
                            case 226: text += "©"; break;
                            case 227: text += "·"; break;
                            case 228:
                            case 229:
                            case 230:
                            case 231:
                            case 235:
                            case 238:
                            case 239:
                            case 240:
                            case 241:
                            case 246:
                            case 247:
                            case 248:
                            case 251:
                            case 252: goto IL_7AF;
                            case 232: text += "Ì"; break;
                            case 233: text += "Ð"; break;
                            case 234: text += "ª"; break;
                            case 236: text += "ỡ"; break;
                            case 237: text += "Ý"; break;
                            case 242: text += "ß"; break;
                            case 243: text += "ã"; break;
                            case 244: text += "«"; break;
                            case 245: text += "ừ"; break;
                            case 249: text += "ự"; break;
                            case 250: text += "ư"; break;
                            case 253: text += "ý"; break;
                            default:
                                switch (num)
                                {
                                    case 258: text += "¡"; break;
                                    case 259: text += "¨"; break;
                                    default: goto IL_7AF;
                                }
                                break;
                        }
                    }
                    else text += "¤";
                }
            }
            else
            {
                if (num <= 361)
                {
                    switch (num)
                    {
                        case 272: text += "§"; break;
                        case 273: text += "®"; break;
                        default:
                            if (num != 297)
                            {
                                if (num != 361) goto IL_7AF;
                                text += "ò";
                            }
                            else text += "Ü";
                            break;
                    }
                }
                else
                {
                    switch (num)
                    {
                        case 416: text += "¥"; break;
                        case 417: text += "¬"; break;
                        default:
                            if (num != 431)
                            {
                                switch (num)
                                {
                                    case 7841: text += "¹"; break;
                                    case 7842:
                                    case 7844:
                                    case 7846:
                                    case 7848:
                                    case 7850:
                                    case 7852:
                                    case 7854:
                                    case 7856:
                                    case 7858:
                                    case 7860:
                                    case 7862:
                                    case 7864:
                                    case 7866:
                                    case 7868:
                                    case 7870:
                                    case 7872:
                                    case 7874:
                                    case 7876:
                                    case 7878:
                                    case 7880:
                                    case 7882:
                                    case 7884:
                                    case 7886:
                                    case 7888:
                                    case 7890:
                                    case 7892:
                                    case 7894:
                                    case 7896:
                                    case 7898:
                                    case 7900:
                                    case 7902:
                                    case 7904:
                                    case 7906:
                                    case 7908:
                                    case 7910:
                                    case 7912:
                                    case 7914:
                                    case 7916:
                                    case 7918:
                                    case 7920:
                                    case 7922:
                                    case 7924:
                                    case 7926:
                                    case 7928: goto IL_7AF;
                                    case 7843: text += "¶"; break;
                                    case 7845: text += "Ê"; break;
                                    case 7847: text += "Ç"; break;
                                    case 7849: text += "È"; break;
                                    case 7851: text += "É"; break;
                                    case 7853: text += "Ë"; break;
                                    case 7855: text += "¾"; break;
                                    case 7857: text += "»"; break;
                                    case 7859: text += "¼"; break;
                                    case 7861: text += "½"; break;
                                    case 7863: text += "Æ"; break;
                                    case 7865: text += "Ñ"; break;
                                    case 7867: text += "Î"; break;
                                    case 7869: text += "Ï"; break;
                                    case 7871: text += "Õ"; break;
                                    case 7873: text += "Ò"; break;
                                    case 7875: text += "Ó"; break;
                                    case 7877: text += "Ô"; break;
                                    case 7879: text += "Ö"; break;
                                    case 7881: text += "Ø"; break;
                                    case 7883: text += "Þ"; break;
                                    case 7885: text += "ä"; break;
                                    case 7887: text += "á"; break;
                                    case 7889: text += "è"; break;
                                    case 7891: text += "å"; break;
                                    case 7893: text += "æ"; break;
                                    case 7895: text += "ç"; break;
                                    case 7897: text += "é"; break;
                                    case 7899: text += "í"; break;
                                    case 7901: text += "ê"; break;
                                    case 7903: text += "ë"; break;
                                    case 7905: text += "ì"; break;
                                    case 7907: text += "î"; break;
                                    case 7909: text += "ô"; break;
                                    case 7911: text += "ñ"; break;
                                    case 7913: text += "ø"; break;
                                    case 7915: text += "õ"; break;
                                    case 7917: text += "ö"; break;
                                    case 7919: text += "÷"; break;
                                    case 7921: text += "ù"; break;
                                    case 7923: text += "ú"; break;
                                    case 7925: text += "þ"; break;
                                    case 7927: text += "û"; break;
                                    case 7929: text += "ü"; break;
                                    default: goto IL_7AF;
                                }
                            }
                            else text += "¦";
                            break;
                    }
                }
            }
            IL_7BF:
            i++;
            continue;
            IL_7AF:
            text += value2.ToString();
            goto IL_7BF;
        }
        return text;
    }

    protected void btnChuyen_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        if (!string.IsNullOrEmpty(lstIdHVCN.Value))
        {
            cmd.CommandText = "UPDATE tblHoiVienCaNhan SET HoiVienTapTheID = " + drHoiVienTapThe_Chuyen.SelectedValue + " WHERE HoiVienCaNhanID IN (" + lstIdHVCN.Value + ")";
            DataAccess.RunActionCmd(cmd);
        }
        if (!string.IsNullOrEmpty(lstIdNQT.Value))
        {
            cmd.CommandText = "UPDATE tblHoiVienCaNhan SET HoiVienTapTheID = " + drHoiVienTapThe_Chuyen.SelectedValue + " WHERE HoiVienCaNhanID IN (" + lstIdNQT.Value + ")";
            DataAccess.RunActionCmd(cmd);
        }

        string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #1199ed;color:#fff;padding:12px 15px;"">
							<p>Cập nhật thành công</p>							
						</div>";


        LoadThanhVienVien(int.Parse(Request.QueryString["id"]));

        placeMessage.Controls.Add(new LiteralControl(thongbao));
    }
}