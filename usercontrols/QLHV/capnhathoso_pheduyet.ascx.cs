﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using CommonLayer;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using DBHelpers;

public partial class usercontrols_capnhathoso_pheduyet : System.Web.UI.UserControl
 {
        String id = String.Empty;
        public Commons cm = new Commons();
        public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
        public HoiVienTapThe objHoiVIenTapThe = new HoiVienTapThe();
        public HoiVienTapTheChiNhanh objHoiVIenTapTheChiNhanh = new HoiVienTapTheChiNhanh();
        static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
        SqlConnection conn = new SqlConnection(connStr);
        protected DonHoiVienFile FileDinhKem1 = new DonHoiVienFile();

      
        int trangthaiChoDuyetID = 3;
        int trangthaiTraLaiID = 4;
        int trangthaiDaPheDuyet = 5;
  

 
        string tk_TTChoDuyet = "3";
        string tk_TTTuChoiDuyet = "4";
        string tk_TTDaDuyet = "5";
   


        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!IsPostBack)
                {


                    int CapNhatHoSoHVTTID = 0;
                    try
                    {
                        CapNhatHoSoHVTTID = Convert.ToInt32(Request.QueryString["id"]);
                    }
                    catch
                    {

                    }
                    //txtNgayYeuCau.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    LoadDropDown();
                    LoadYeuCauCapNhat(    CapNhatHoSoHVTTID );
                    //SetInitialRow_KhenThuong();
                    //SetInitialRow_KyLuat();
                    //SetInitialRow_ChiNhanh();
                    //LoadLichSu(Session["HoiVienTapTheID"].ToString());

                }
            }
            catch (Exception ex)
            {
                Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));


            }
      


         
           

             


        }
        private void LoadYeuCauCapNhat(int CapNhatHoSoHVTTID)
        {



            if (  CapNhatHoSoHVTTID != 0)
            {
                SqlCapNhatHoSoHvttProvider objCapNhatHoSoHvtt_provider = new SqlCapNhatHoSoHvttProvider(connStr, true, "");
                CapNhatHoSoHvtt objCapNhatHoSoHvtt   = objCapNhatHoSoHvtt_provider.GetByCapNhatHoSoHvttid(CapNhatHoSoHVTTID);



                if (objCapNhatHoSoHvtt != null)
                {

                    txtMaYeucau.Text= objCapNhatHoSoHvtt.MaYeuCau; 

                    try
                    {
                        txtNgayYeuCau.Text = objCapNhatHoSoHvtt.NgayYeuCau.Value.ToString("dd/MM/yyyy"); 
                    }
                    catch
                    {
                    }


                   txtTenDoanhNghiep.Text=  objCapNhatHoSoHvtt.TenDoanhNghiep; 
                   txtTenTiengAnh.Text=   objCapNhatHoSoHvtt.TenTiengAnh ; 
                   txtTenVietTat.Text=  objCapNhatHoSoHvtt.TenVietTat ; 


                    txtSoGiayChungNhanDKKD.Text = objCapNhatHoSoHvtt.SoGiayChungNhanDkkd ; 

                    try
                    {
                        txtNgayCapGiayChungNhanDKKD.Text = objCapNhatHoSoHvtt.NgayCapGiayChungNhanDkkd.Value.ToString("dd/MM/yyyy"); 
                    }
                    catch
                    {
                    }

                   txtSoGiayChungNhanKDDVKT.Text=  objCapNhatHoSoHvtt.SoGiayChungNhanKddvkt; 
                    try
                    {
                        txtNgayCapGiayChungNhanKDDVKT.Text = objCapNhatHoSoHvtt.NgayCapGiayChungNhanKddvkt.Value.ToString("dd/MM/yyyy"); 
                    }
                    catch
                    {
                    }


                    txtMaSoThue.Text= objCapNhatHoSoHvtt.MaSoThue ;// 
                   drLinhvucHoatDong.SelectedValue=  objCapNhatHoSoHvtt.LinhVucHoatDongId.ToString() ; 


                    try
                    {
                       txtVonDieuLe.Text=  objCapNhatHoSoHvtt.VonDieuLe.ToString(); 
                    }
                    catch
                    {
                    }
                    try
                    {
                        txtTongDoanhThu.Text  = objCapNhatHoSoHvtt.TongDoanhThu.ToString();
                    }
                    catch
                    {
                    }
                    try
                    {
                        txtDoanhThuDVKT.Text = objCapNhatHoSoHvtt.DoanhThuDichVuKt.ToString(); 
                    }
                    catch
                    {
                    }
                    try
                    {
                        txtTongSoKHTrongNam.Text= objCapNhatHoSoHvtt.TongSoKhTrongNam.ToString() ; 
                    }
                    catch
                    {
                    }

                    try
                    {
                       TongSoKhChungKhoan.Text=  objCapNhatHoSoHvtt.TongSoKhChungKhoan.ToString(); 
                    }
                    catch
                    {
                    }



                    linkFileUpload.NavigateUrl = "Download_CapNhatHoSoHVTT.ashx?id=" + CapNhatHoSoHVTTID;
                    lalTenFile.Text = objCapNhatHoSoHvtt.TenFileBangChung;
                    // File dinh kem

                    hidTrangthaiId.Value = objCapNhatHoSoHvtt.TinhTrangId.ToString();
                    hidId.Value = CapNhatHoSoHVTTID.ToString();
               

  


                    SqlCommand sql = new SqlCommand();
                    sql.Connection = new SqlConnection(connStr); ;

                    String sqlKhenThuong =  " SELECT ROW_NUMBER() OVER (ORDER BY   tblCapNhatHoSoHVTTKhenThuongKyLuat.NgayThang   ) AS STT ,  " +
                                            " tblCapNhatHoSoHVTTKhenThuongKyLuat.KhenThuongKyLuatID as KhenThuongKyLuatID, " +
                                            " CONVERT(nvarchar(10),  " +
                                            " tblCapNhatHoSoHVTTKhenThuongKyLuat.NgayThang  , 103) as NgayThang , " +
                                            " tblCapNhatHoSoHVTTKhenThuongKyLuat.LyDo as LyDo,    " +
                                            " tblDMHinhThucKhenThuong.TenHinhThucKhenThuong as TenHinhThucKhenThuong, " +
                                            " tblDMHinhThucKyLuat.TenHinhThucKyLuat  as TenHinhThucKyLuat   " +
                                             "  From    tblCapNhatHoSoHVTTKhenThuongKyLuat     " +
                                              "   left join  tblDMHinhThucKhenThuong on tblDMHinhThucKhenThuong.HinhThucKhenThuongID= tblCapNhatHoSoHVTTKhenThuongKyLuat.HinhThucKhenThuongID " +
                                               "  left join  tblDMHinhThucKyLuat on tblDMHinhThucKyLuat.HinhThucKyLuatID= tblCapNhatHoSoHVTTKhenThuongKyLuat.HinhThucKyLuatID  " +
                                              "   where    tblCapNhatHoSoHVTTKhenThuongKyLuat.CapNhatHoSoHVTTID=  " + CapNhatHoSoHVTTID +
                                                "         AND tblCapNhatHoSoHVTTKhenThuongKyLuat.HinhThucKhenThuongID   is not null ";
       



                    sql.CommandText = sqlKhenThuong;

                    DataSet ds = DataAccess.RunCMDGetDataSet(sql);
                    DataView dt = ds.Tables[0].DefaultView;


                    PagedDataSource objPds1 = new PagedDataSource();
                    objPds1.DataSource = ds.Tables[0].DefaultView;

                    gvKhenThuong.DataSource = objPds1;
                    gvKhenThuong.DataBind();



                  
                    String sqlKyLuat = " SELECT ROW_NUMBER() OVER (ORDER BY   tblCapNhatHoSoHVTTKhenThuongKyLuat.NgayThang   ) AS STT ,  " +
                                    " tblCapNhatHoSoHVTTKhenThuongKyLuat.KhenThuongKyLuatID as KhenThuongKyLuatID, " +
                                    " CONVERT(nvarchar(10),  " +
                                    " tblCapNhatHoSoHVTTKhenThuongKyLuat.NgayThang  , 103) as NgayThang , " +
                                    " tblCapNhatHoSoHVTTKhenThuongKyLuat.LyDo as LyDo,    " +
                                    " tblDMHinhThucKhenThuong.TenHinhThucKhenThuong as TenHinhThucKhenThuong, " +
                                    " tblDMHinhThucKyLuat.TenHinhThucKyLuat  as TenHinhThucKyLuat   " +
                                     "  From    tblCapNhatHoSoHVTTKhenThuongKyLuat     " +
                                      "   left join  tblDMHinhThucKhenThuong on tblDMHinhThucKhenThuong.HinhThucKhenThuongID= tblCapNhatHoSoHVTTKhenThuongKyLuat.HinhThucKhenThuongID " +
                                       "  left join  tblDMHinhThucKyLuat on tblDMHinhThucKyLuat.HinhThucKyLuatID= tblCapNhatHoSoHVTTKhenThuongKyLuat.HinhThucKyLuatID  " +
                                      "   where    tblCapNhatHoSoHVTTKhenThuongKyLuat.CapNhatHoSoHVTTID=  " + CapNhatHoSoHVTTID + 
                                      "         AND tblCapNhatHoSoHVTTKhenThuongKyLuat.HinhThucKyLuatID   is not null ";

                    sql.CommandText = sqlKyLuat;

                    ds = DataAccess.RunCMDGetDataSet(sql);
                    dt = ds.Tables[0].DefaultView;


                    PagedDataSource objPds2 = new PagedDataSource();
                    objPds2.DataSource = ds.Tables[0].DefaultView;

                    gvKyLuat.DataSource = objPds2;
                    gvKyLuat.DataBind();



                    sql.CommandText =  "  SELECT  ROW_NUMBER() OVER (ORDER BY   Ten   ) AS STT,   " +
                      "  ChiNhanhID , Ten ,DiaChi,  DienThoai,EmailNguoiLH ,DaiDienPL_Ten as DaiDienPlTen,   " +
                     "   CASE CONVERT(NVARCHAR(MAX), Loai) WHEN '0' THEN N'Chi nhánh' ELSE N'Văn phòng đại diện' END AS TenLoai     " +
                      "  FROM tblCapNhatHoSoHVTTChiNhanh WHERE CapNhatHoSoHVTTID= " + CapNhatHoSoHVTTID;



                    ds = DataAccess.RunCMDGetDataSet(sql);
                    dt = ds.Tables[0].DefaultView;


                    PagedDataSource objPds3 = new PagedDataSource();
                    objPds3.DataSource = ds.Tables[0].DefaultView;

                    gvChiNhanh.DataSource = objPds3;
                    gvChiNhanh.DataBind();






                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    ds = null;
                    dt = null;

                // lich su yc
                 LoadLichSu(objCapNhatHoSoHvtt.HoiVienTapTheId.ToString());

                }
            }


          
            
           
        }


        public void LoadDropDown()
        {
            conn.Open();

            DataSet ds = new DataSet();
            DataTable dtb = new DataTable();

            string sql = "";


            //// Chuc vu nguoi dai dien Phap Luat
            //sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
            //SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            //da.Fill(ds);
            //dtb = ds.Tables[0];
            //drNguoiDaiDienPL_ChucVu.DataSource = dtb;
            //drNguoiDaiDienPL_ChucVu.DataBind();

            //ds.Clear();
            //dtb.Clear();


            // Linh vuc hoat dong
            sql = "SELECT 0 AS LinhVucHoatDongID, N'<< Lựa chọn >>' AS TenLinhVucHoatDong , '' AS MaLinhVucHoatDong  UNION ALL (SELECT LinhVucHoatDongID, TenLinhVucHoatDong , MaLinhVucHoatDong FROM tblDMLinhVucHoatDong)";
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataAdapter  da = new SqlDataAdapter(sql, conn);
            da.Fill(ds);
            dtb = ds.Tables[0];
            drLinhvucHoatDong.DataSource = dtb;
            drLinhvucHoatDong.DataBind();

            ds.Clear();
            dtb.Clear();



            conn.Close();
        }


//        public void Load_ThanhPho(string MaTinh = "00")
//        {

//            //   conn.Open();

//            DataSet ds = new DataSet();
//            DataTable dtb = new DataTable();

//            string sql = "";

//            sql = "SELECT '00' AS MaTinh, N'<< Lựa chọn >>' AS TenTinh UNION ALL (SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ) ORDER BY TenTinh ASC";

//            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
//            da.Fill(ds);
//            dtb = ds.Tables[0];
//            drTinhThanh.DataSource = dtb;
//            drTinhThanh.DataBind();

//            ds.Clear();
//            dtb.Clear();



//        }

//        public void Load_QuanHuyen(string MaHuyen = "000", string MaTinh = "00")
//        {
//            //  conn.Open();

//            DataSet ds = new DataSet();
//            DataTable dtb = new DataTable();

//            string sql = "";

//            sql = "SELECT '000' AS MaHuyen, N'<< Lựa chọn >>' AS TenHuyen UNION ALL (SELECT MaHuyen,TenHuyen FROM tblDMHuyen WHERE (MaTinh='" + MaTinh + "') AND HieuLuc='1' ) ORDER BY TenHuyen ASC";

//            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
//            da.Fill(ds);
//            dtb = ds.Tables[0];
//            drQuanHuyen.DataSource = dtb;
//            drQuanHuyen.DataBind();

//            ds.Clear();
//            dtb.Clear();


//            drPhuongXa.Items.Clear();
//            drPhuongXa.DataSource = null;
//            drPhuongXa.DataBind();


//        }

//        public void Load_PhuongXa(string MaXa = "00000", string MaHuyen = "000")
//        {
//            //   conn.Open();

//            DataSet ds = new DataSet();
//            DataTable dtb = new DataTable();

//            string sql = "";

//            sql = "SELECT '0000' AS MaXa, N'<< Lựa chọn >>' AS TenXa UNION ALL (SELECT MaXa,TenXa FROM tblDMXa WHERE (MaHuyen='" + MaHuyen + "') ) ORDER BY TenXa ASC";

//            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
//            da.Fill(ds);
//            dtb = ds.Tables[0];
//            drPhuongXa.DataSource = dtb;
//            drPhuongXa.DataBind();

//            ds.Clear();
//            dtb.Clear();



//        }

//        protected void drTinhThanh_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            Load_QuanHuyen("000", drTinhThanh.SelectedValue);
//        }

//        protected void drQuanHuyen_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            Load_PhuongXa("00000", drQuanHuyen.SelectedValue);
//        }

//        protected void btnChiNhanhGhi_Click(object sender, EventArgs e)
//        {


//            try
//            {
//                // conn.Open();

//                string thongbao = "";

//                SqlHoiVienTapTheChiNhanhProvider HoiVienTapTheChiNhanh_provider = new SqlHoiVienTapTheChiNhanhProvider(connStr, true, "");

//                try
//                {
//                    objHoiVIenTapTheChiNhanh.HoiVienTapTheId = Convert.ToInt32(Session["HoiVienTapTheID"].ToString());
//                }
//                catch
//                {

//                }


//                try
//                {
//                    objHoiVIenTapTheChiNhanh.ChiNhanhId = Convert.ToInt32(hidendChiNhanhID.Value);
//                }
//                catch
//                {

//                }

//                objHoiVIenTapTheChiNhanh.Loai = rdChiNhah.Checked == true ? "1" : "2";
//                objHoiVIenTapTheChiNhanh.Ten = txtTenDoanhNghiep.Text.Trim();
//                objHoiVIenTapTheChiNhanh.DiaChi = txtDiaChi.Text.Trim();
//                objHoiVIenTapTheChiNhanh.TinhThanhId = Convert.ToInt32(drTinhThanh.SelectedValue);
//                objHoiVIenTapTheChiNhanh.QuanHuyenId = Convert.ToInt32(drQuanHuyen.SelectedValue);
//                objHoiVIenTapTheChiNhanh.PhuongXaId = Convert.ToInt32(drPhuongXa.SelectedValue);

//                objHoiVIenTapTheChiNhanh.DienThoai = txtDienThoai.Text.Trim();
//                objHoiVIenTapTheChiNhanh.EmailNguoiLh = txtEmail.Text.Trim();
//                objHoiVIenTapTheChiNhanh.Fax = txtFax.Text.Trim();



//                // Thong tin nguoi dai dien theo PL
//                objHoiVIenTapTheChiNhanh.DaiDienPlTen = txtNguoiDaiDienPL_Ten.Text.Trim();
//                try
//                {
//                    objHoiVIenTapTheChiNhanh.DaiDienPlNgaySinh = DateTime.ParseExact(txtNguoiDaiDienPL_NgaySinh.Text.Trim(), "dd/MM/yyyy", null);
//                }
//                catch
//                {
//                }
//                objHoiVIenTapTheChiNhanh.DaiDienPlGioiTinh = drNguoiDaiDienPL_GioiTinh.SelectedValue;
//                objHoiVIenTapTheChiNhanh.DaiDienPlChucVuId = Convert.ToInt32(drNguoiDaiDienPL_ChucVu.SelectedValue);
//                objHoiVIenTapTheChiNhanh.DaiDienPlSoChungChiKtv = txtNguoiDaiDienPL_SoChungChiKTV.Text.Trim();
//                try
//                {
//                    objHoiVIenTapTheChiNhanh.DaiDienPlNgayCapCcktv = DateTime.ParseExact(txtNguoiDaiDienPL_NgayCapChungChiKTV.Text.Trim(), "dd/MM/yyyy", null);
//                }
//                catch
//                {
//                }

//                objHoiVIenTapTheChiNhanh.DaiDienPlMobile = txtNguoiDaiDienPL_DiDong.Text.Trim();
//                objHoiVIenTapTheChiNhanh.DaiDienPlEmail = txtNguoiDaiDienPL_Email.Text.Trim();

//                if (hidMode.Value == "add")
//                {
//                    HoiVienTapTheChiNhanh_provider.Insert(objHoiVIenTapTheChiNhanh);

//                }
//                else
//                {
//                    HoiVienTapTheChiNhanh_provider.Update(objHoiVIenTapTheChiNhanh);
//                }


//            }
//            catch (Exception ex)
//            {
//                string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #000000;"">
//            							<p>" + ex.ToString() + @"</p>
//            							<i class=""fa fa-plus""></i>
//            						</div>";
//                placeMessage.Controls.Add(new LiteralControl(thongbao));
//            }


//        }

        private void SetInitialRow_ChiNhanh()
        {

            List<CapNhatHoSoHvttChiNhanh> objListChiNhanh = new List<CapNhatHoSoHvttChiNhanh>();
            if (Session["SessionList"] != null)
            {
                objListChiNhanh = (List<CapNhatHoSoHvttChiNhanh>)Session["SessionList"];
            }



            DataTable dt = new DataTable();
            

            if (Session["SessionDataTable"] != null)
            {
                dt = (DataTable)Session["SessionDataTable"];
            }
            else {
                dt.Columns.Add(new DataColumn("ChiNhanhId", typeof(string)));
                dt.Columns.Add(new DataColumn("Ten", typeof(string)));
                dt.Columns.Add(new DataColumn("Loai", typeof(string)));
                dt.Columns.Add(new DataColumn("TenLoai", typeof(string)));
                dt.Columns.Add(new DataColumn("DiaChi", typeof(string)));

                dt.Columns.Add(new DataColumn("DienThoai", typeof(string)));
                dt.Columns.Add(new DataColumn("EmailNguoiLh", typeof(string)));
                dt.Columns.Add(new DataColumn("DaiDienPlTen", typeof(string)));
                dt.Columns.Add(new DataColumn("TinhThanhId", typeof(string)));
                dt.Columns.Add(new DataColumn("QuanHuyenId", typeof(string)));
                dt.Columns.Add(new DataColumn("PhuongXaId", typeof(string)));

                dt.Columns.Add(new DataColumn("Fax", typeof(string)));
                dt.Columns.Add(new DataColumn("DaiDienPlNgaySinh", typeof(string)));
                dt.Columns.Add(new DataColumn("DaiDienPlGioiTinh", typeof(string)));
                dt.Columns.Add(new DataColumn("DaiDienPlChucVuId", typeof(string)));
                dt.Columns.Add(new DataColumn("DaiDienPlSoChungChiKtv", typeof(string)));
                dt.Columns.Add(new DataColumn("DaiDienPlNgayCapCcktv", typeof(string)));
                dt.Columns.Add(new DataColumn("DaiDienPlMobile", typeof(string)));
                dt.Columns.Add(new DataColumn("DaiDienPlEmail", typeof(string)));


                DataColumn[] columns = new DataColumn[1];
                columns[0] = dt.Columns["ChiNhanhId"];
                dt.PrimaryKey = columns;
            }

 
            Session["SessionDataTable"] = dt;
            gvChiNhanh.DataSource = dt;
            gvChiNhanh.DataBind();
        }


        
     

        #region GridKhenThuongKyLuat
        protected void ButtonAddKhenThuong_Click(object sender, EventArgs e)
        {

            AddNewRowToGrid_KhenThuong();
        }

        protected void ButtonAddKyLuat_Click(object sender, EventArgs e)
        {

            AddNewRowToGrid_KyLuat();
        }

        private void SetInitialRow_KhenThuong()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("STT", typeof(string)));
            dt.Columns.Add(new DataColumn("NgayThang", typeof(string)));
            dt.Columns.Add(new DataColumn("Cap", typeof(string)));
            dt.Columns.Add(new DataColumn("LyDo", typeof(string)));

            DataColumn[] columns = new DataColumn[1];
            columns[0] = dt.Columns["STT"];
            dt.PrimaryKey = columns;



            dr = dt.NewRow();
            dr["STT"] = 1;
            dr["NgayThang"] = "";
            dr["Cap"] = "";
            dr["LyDo"] = "";

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["KhenThuong"] = dt;

            gvKhenThuong.DataSource = dt;
            gvKhenThuong.DataBind();
        }
        private void AddNewRowToGrid_KhenThuong()
        {
            try
            {
                int rowIndex = 0;

                if (ViewState["KhenThuong"] != null)
                {
                    DataTable dtCurrentTable = (DataTable)ViewState["KhenThuong"];
                    DataRow drCurrentRow = null;

                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                        {
                            //extract the TextBox values
                            TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                            DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                            TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                            dtCurrentTable.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                            dtCurrentTable.Rows[i - 1]["Cap"] = drHinhThuc.SelectedValue;
                            dtCurrentTable.Rows[i - 1]["LyDo"] = lydo.Text;

                            rowIndex++;
                        }

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                        drCurrentRow["NgayThang"] = "";
                        drCurrentRow["Cap"] = "";
                        drCurrentRow["LyDo"] = "";
                        dtCurrentTable.Rows.Add(drCurrentRow);
                    }
                    else
                    {
                        DataRow dr = null;
                        dr = dtCurrentTable.NewRow();
                        dr["STT"] = 1;
                        dr["NgayThang"] = "";
                        dr["Cap"] = "";
                        dr["LyDo"] = "";

                        dtCurrentTable.Rows.Add(dr);
                    }

                    ViewState["KhenThuong"] = dtCurrentTable;

                    gvKhenThuong.DataSource = dtCurrentTable;
                    gvKhenThuong.DataBind();
                }
                else
                {
                    Response.Write("ViewState is null");
                }

                //Set Previous Data on Postbacks
                SetPreviousData_KhenThuong();
            }
            catch (Exception ex)
            {
                //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
            }
        }
        private void SetPreviousData_KhenThuong()
        {
            int rowIndex = 0;
            if (ViewState["KhenThuong"] != null)
            {
                DataTable dt = (DataTable)ViewState["KhenThuong"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        ngaythang.Text = dt.Rows[i]["NgayThang"].ToString();
                        drHinhThuc.SelectedValue = dt.Rows[i]["Cap"].ToString();
                        lydo.Text = dt.Rows[i]["LyDo"].ToString();
                        rowIndex++;
                    }
                }
            }
        }


        private void SetInitialRow_KyLuat()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("STT", typeof(string)));
            dt.Columns.Add(new DataColumn("NgayThang", typeof(string)));
            dt.Columns.Add(new DataColumn("Cap", typeof(string)));
            dt.Columns.Add(new DataColumn("LyDo", typeof(string)));

            DataColumn[] columns = new DataColumn[1];
            columns[0] = dt.Columns["STT"];
            dt.PrimaryKey = columns;



            dr = dt.NewRow();
            dr["STT"] = 1;
            dr["NgayThang"] = "";
            dr["Cap"] = "";
            dr["LyDo"] = "";

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["KyLuat"] = dt;

            gvKyLuat.DataSource = dt;
            gvKyLuat.DataBind();
        }
        private void AddNewRowToGrid_KyLuat()
        {
            try
            {
                int rowIndex = 0;

                if (ViewState["KyLuat"] != null)
                {
                    DataTable dtCurrentTable = (DataTable)ViewState["KyLuat"];
                    DataRow drCurrentRow = null;

                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                        {
                            //extract the TextBox values
                            TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                            DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                            TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                            dtCurrentTable.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                            dtCurrentTable.Rows[i - 1]["Cap"] = drHinhThuc.SelectedValue;
                            dtCurrentTable.Rows[i - 1]["LyDo"] = lydo.Text;

                            rowIndex++;
                        }

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                        drCurrentRow["NgayThang"] = "";
                        drCurrentRow["Cap"] = "";
                        drCurrentRow["LyDo"] = "";
                        dtCurrentTable.Rows.Add(drCurrentRow);
                    }
                    else
                    {
                        DataRow dr = null;
                        dr = dtCurrentTable.NewRow();
                        dr["STT"] = 1;
                        dr["NgayThang"] = "";
                        dr["Cap"] = "";
                        dr["LyDo"] = "";

                        dtCurrentTable.Rows.Add(dr);
                    }

                    ViewState["KyLuat"] = dtCurrentTable;

                    gvKyLuat.DataSource = dtCurrentTable;
                    gvKyLuat.DataBind();
                }
                else
                {
                    Response.Write("ViewState is null");
                }

                //Set Previous Data on Postbacks
                SetPreviousData_KyLuat();
            }
            catch (Exception ex)
            {
                //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
            }
        }
        private void SetPreviousData_KyLuat()
        {
            int rowIndex = 0;
            if (ViewState["KyLuat"] != null)
            {
                DataTable dt = (DataTable)ViewState["KyLuat"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        ngaythang.Text = dt.Rows[i]["NgayThang"].ToString();
                        drHinhThuc.SelectedValue = dt.Rows[i]["Cap"].ToString();
                        lydo.Text = dt.Rows[i]["LyDo"].ToString();

                        rowIndex++;
                    }
                }
            }
        }

        protected void gvKhenThuong_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                id = Convert.ToString(e.CommandArgument);
            }
        }
        protected void gvKhenThuong_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int rowIndex = 0;

            if (ViewState["KhenThuong"] != null)
            {
                DataTable dt = (DataTable)ViewState["KhenThuong"];

                if (dt.Rows.Count > 0)
                {
                    for (int i = 1; i <= dt.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox ngaythang = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKhenThuong.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        dt.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                        dt.Rows[i - 1]["Cap"] = drHinhThuc.SelectedValue;
                        dt.Rows[i - 1]["LyDo"] = lydo.Text;

                        rowIndex++;
                    }
                }

                DataRow row_del = dt.Rows.Find(id);

                dt.Rows.Remove(row_del);
                dt.AcceptChanges();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["STT"] = i + 1;
                }

                ViewState["KhenThuong"] = dt;

                gvKhenThuong.DataSource = dt;
                gvKhenThuong.DataBind();

                SetPreviousData_KhenThuong();
            }
        }
        protected void gvKhenThuong_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList drHinhThuc = (DropDownList)e.Row.FindControl("drHinhThuc");

                    conn.Open();

                    DataSet ds = new DataSet();
                    DataTable dtb = new DataTable();

                    string sql = "";

                    sql = "SELECT 0 AS HinhThucKhenThuongID, N'<< Lựa chọn >>' AS TenHinhThucKhenThuong UNION ALL (SELECT HinhThucKhenThuongID, TenHinhThucKhenThuong FROM tblDMHinhThucKhenThuong)";

                    SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                    da.Fill(ds);
                    dtb = ds.Tables[0];
                    drHinhThuc.DataSource = dtb;
                    drHinhThuc.DataBind();

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void gvKyLuat_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                id = Convert.ToString(e.CommandArgument);
            }
        }
        protected void gvKyLuat_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int rowIndex = 0;

            if (ViewState["KyLuat"] != null)
            {
                DataTable dt = (DataTable)ViewState["KyLuat"];

                if (dt.Rows.Count > 0)
                {
                    for (int i = 1; i <= dt.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox ngaythang = (TextBox)gvKyLuat.Rows[rowIndex].Cells[1].FindControl("txtNgayThang");
                        DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[rowIndex].Cells[2].FindControl("drHinhThuc");
                        TextBox lydo = (TextBox)gvKyLuat.Rows[rowIndex].Cells[3].FindControl("txtLyDo");

                        dt.Rows[i - 1]["NgayThang"] = ngaythang.Text;
                        dt.Rows[i - 1]["Cap"] = drHinhThuc.SelectedValue;
                        dt.Rows[i - 1]["LyDo"] = lydo.Text;

                        rowIndex++;
                    }
                }

                DataRow row_del = dt.Rows.Find(id);

                dt.Rows.Remove(row_del);
                dt.AcceptChanges();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["STT"] = i + 1;
                }

                ViewState["KyLuat"] = dt;

                gvKyLuat.DataSource = dt;
                gvKyLuat.DataBind();

                SetPreviousData_KyLuat();
            }
        }
        protected void gvKyLuat_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList drHinhThuc = (DropDownList)e.Row.FindControl("drHinhThuc");

                    conn.Open();

                    DataSet ds = new DataSet();
                    DataTable dtb = new DataTable();

                    string sql = "";

                    sql = "SELECT 0 AS HinhThucKhenThuongID, N'<< Lựa chọn >>' AS TenHinhThucKhenThuong UNION ALL (SELECT HinhThucKhenThuongID, TenHinhThucKhenThuong FROM tblDMHinhThucKhenThuong)";

                    SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                    da.Fill(ds);
                    dtb = ds.Tables[0];
                    drHinhThuc.DataSource = dtb;
                    drHinhThuc.DataBind();

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }

        #endregion


        protected void CapnhatYeuCauThayDoiTTHoiVien(int HoiVienID)
        {


            try
            {
                // conn.Open();

                string thongbao = "";



                DataSet ds = new DataSet();

                SqlCommand sql = new SqlCommand();
                sql.Connection = new SqlConnection(connStr); ;





                SqlCapNhatHoSoHvttProvider objCapNhatHoSoHvtt_provider = new SqlCapNhatHoSoHvttProvider(connStr, true, "");
                CapNhatHoSoHvtt objCapNhatHoSoHvtt = new CapNhatHoSoHvtt();
                objCapNhatHoSoHvtt.HoiVienTapTheId = HoiVienID;
                objCapNhatHoSoHvtt.MaYeuCau = txtMaYeucau.Text.Trim();

                try
                {
                    objCapNhatHoSoHvtt.NgayYeuCau = DateTime.ParseExact(txtNgayYeuCau.Text.Trim(), "dd/MM/yyyy", null);
                }
                catch
                {
                }


                objCapNhatHoSoHvtt.TenDoanhNghiep = txtTenDoanhNghiep.Text.Trim();
                objCapNhatHoSoHvtt.TenTiengAnh = txtTenTiengAnh.Text.Trim();
                objCapNhatHoSoHvtt.TenVietTat = txtTenVietTat.Text.Trim();





                objCapNhatHoSoHvtt.SoGiayChungNhanDkkd = txtSoGiayChungNhanDKKD.Text.Trim();

                try
                {
                    objCapNhatHoSoHvtt.NgayCapGiayChungNhanDkkd = DateTime.ParseExact(txtNgayCapGiayChungNhanDKKD.Text.Trim(), "dd/MM/yyyy", null);
                }
                catch
                {
                }

                objCapNhatHoSoHvtt.SoGiayChungNhanKddvkt = txtSoGiayChungNhanKDDVKT.Text.Trim();
                try
                {
                    objCapNhatHoSoHvtt.NgayCapGiayChungNhanKddvkt = DateTime.ParseExact(txtNgayCapGiayChungNhanKDDVKT.Text.Trim(), "dd/MM/yyyy", null);
                }
                catch
                {
                }


                objCapNhatHoSoHvtt.MaSoThue = txtMaSoThue.Text.Trim();
                objCapNhatHoSoHvtt.LinhVucHoatDongId = Convert.ToInt32(drLinhvucHoatDong.SelectedValue);


                try
                {
                    objCapNhatHoSoHvtt.VonDieuLe = Convert.ToInt32(txtVonDieuLe.Text.Trim());
                }
                catch
                {
                }
                try
                {
                    objCapNhatHoSoHvtt.TongDoanhThu = Convert.ToInt32(txtTongDoanhThu.Text.Trim());
                }
                catch
                {
                }
                try
                {
                    objCapNhatHoSoHvtt.DoanhThuDichVuKt = Convert.ToInt32(txtDoanhThuDVKT.Text.Trim());
                }
                catch
                {
                }
                try
                {
                    objCapNhatHoSoHvtt.TongSoKhTrongNam = Convert.ToInt32(txtTongSoKHTrongNam.Text.Trim());
                }
                catch
                {
                }

                try
                {
                    objCapNhatHoSoHvtt.TongSoKhChungKhoan = Convert.ToInt32(TongSoKhChungKhoan.Text.Trim());
                }
                catch
                {
                }

                objCapNhatHoSoHvtt.TinhTrangId = trangthaiChoDuyetID;


                // File dinh kem

                if (FileUp != null && FileUp.FileName != "")
                {
                    HttpPostedFile styledfileupload;
                    styledfileupload = FileUp.PostedFile;
                    if (styledfileupload.FileName != "")
                    {

                        byte[] datainput = new byte[styledfileupload.ContentLength];
                        styledfileupload.InputStream.Read(datainput, 0, styledfileupload.ContentLength);

                        objCapNhatHoSoHvtt.FileChungMinh = datainput;
                        objCapNhatHoSoHvtt.TenFileBangChung = new System.IO.FileInfo(styledfileupload.FileName).Name;
                    }
                }



                objCapNhatHoSoHvtt_provider.Insert(objCapNhatHoSoHvtt);

                // File dinh kem
                int newCapNhatHoSoHvttid = 0;
                newCapNhatHoSoHvttid = (int)objCapNhatHoSoHvtt.CapNhatHoSoHvttid;




                // Khen thuong ky luat
                UpDateKhenThuongKyLuat(newCapNhatHoSoHvttid);

                //  Chi nhanh 

                UpDateKhenChiNhanht(newCapNhatHoSoHvttid);
            


                thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;"">
                							<p>" + "Cập nhật thông tin thành công." + @"</p>							
                						</div>";

                placeMessage.Controls.Add(new LiteralControl(thongbao));

            }
            catch (Exception ex)
            {
                string thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #000000;"">
            							<p>" + ex.ToString() + @"</p>
            							<i class=""fa fa-plus""></i>
            						</div>";
                placeMessage.Controls.Add(new LiteralControl(thongbao));
            }


        }


        private void UpDateKhenThuongKyLuat(int newobjCapNhatHoSoHvttid)
        {

            SqlCapNhatHoSoHvttKhenThuongKyLuatProvider objCapNhatHoSoHvtt_provider = new SqlCapNhatHoSoHvttKhenThuongKyLuatProvider(connStr, true, "");
            CapNhatHoSoHvttKhenThuongKyLuat objKhenThuongKyLuat = new CapNhatHoSoHvttKhenThuongKyLuat();

            // Khen thuong
            for (int i = 0; i < gvKhenThuong.Rows.Count; i++)
            {
                objKhenThuongKyLuat = new CapNhatHoSoHvttKhenThuongKyLuat();
                //extract the TextBox values
                TextBox Ngay = (TextBox)gvKhenThuong.Rows[i].Cells[1].FindControl("txtNgayThang");
                DropDownList drHinhThuc = (DropDownList)gvKhenThuong.Rows[i].Cells[2].FindControl("drHinhThuc");
                TextBox LyDo = (TextBox)gvKhenThuong.Rows[i].Cells[3].FindControl("txtLyDo");

                if (drHinhThuc.SelectedValue != "0")
                {

                    try
                    {
                        objKhenThuongKyLuat.NgayThang = DateTime.ParseExact(Ngay.Text.Trim(), "dd/MM/yyyy", null);
                    }
                    catch
                    {
                    }

                    // khen thuong
                    try
                    {
                        objKhenThuongKyLuat.HinhThucKhenThuongId = Convert.ToInt32(drHinhThuc.SelectedValue);

                    }
                    catch
                    {
                    }

                    objKhenThuongKyLuat.LyDo = LyDo.Text.Trim();

                    objCapNhatHoSoHvtt_provider.Insert(objKhenThuongKyLuat);

                }

            }


            // Ky Luat
            for (int i = 0; i < gvKyLuat.Rows.Count; i++)
            {
                objKhenThuongKyLuat = new CapNhatHoSoHvttKhenThuongKyLuat();
                //extract the TextBox values
                TextBox Ngay = (TextBox)gvKyLuat.Rows[i].Cells[1].FindControl("txtNgayThang");
                DropDownList drHinhThuc = (DropDownList)gvKyLuat.Rows[i].Cells[2].FindControl("drHinhThuc");
                TextBox LyDo = (TextBox)gvKyLuat.Rows[i].Cells[3].FindControl("txtLyDo");

                if (drHinhThuc.SelectedValue != "0")
                {

                    try
                    {
                        objKhenThuongKyLuat.NgayThang = DateTime.ParseExact(Ngay.Text.Trim(), "dd/MM/yyyy", null);
                    }
                    catch
                    {
                    }

                    // ky luat
                    try
                    {
                        objKhenThuongKyLuat.HinhThucKyLuatId = Convert.ToInt32(drHinhThuc.SelectedValue);

                    }
                    catch
                    {
                    }

                    objKhenThuongKyLuat.LyDo = LyDo.Text.Trim();

                    objCapNhatHoSoHvtt_provider.Insert(objKhenThuongKyLuat);

                }

            }

        }

        private void UpDateKhenChiNhanht(int newobjCapNhatHoSoHvttid)
        {

            SqlCapNhatHoSoHvttChiNhanhProvider objCapNhatHoSoHvtt_provider = new SqlCapNhatHoSoHvttChiNhanhProvider(connStr, true, "");
            CapNhatHoSoHvttChiNhanh objCapNhatHoSoHvttChiNhanh = new CapNhatHoSoHvttChiNhanh();

            DataTable dt = new DataTable();


            if (Session["SessionDataTable"] != null)
            {
                dt = (DataTable)Session["SessionDataTable"];
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    
                    objCapNhatHoSoHvttChiNhanh = new CapNhatHoSoHvttChiNhanh();
                     try
                    {
                        objCapNhatHoSoHvttChiNhanh.CapNhatHoSoHvttid =  newobjCapNhatHoSoHvttid  ;
                    }
                    catch
                    {

                    }
 
                    try
                    {
                        objCapNhatHoSoHvttChiNhanh.ChiNhanhId = Convert.ToInt32(dt.Rows[i]["ChiNhanhId"].ToString());
   
                    }
                    catch
                    {

                    }

                    objCapNhatHoSoHvttChiNhanh.Loai = dt.Rows[i]["Loai"].ToString();
                    objCapNhatHoSoHvttChiNhanh.Ten = dt.Rows[i]["Ten"].ToString();
                    objCapNhatHoSoHvttChiNhanh.DiaChi = dt.Rows[i]["DiaChi"].ToString();
                    objCapNhatHoSoHvttChiNhanh.TinhThanhId = dt.Rows[i]["TinhThanhId"].ToString();// Convert.ToInt32(drTinhThanh.SelectedValue);
                    objCapNhatHoSoHvttChiNhanh.QuanHuyenId = dt.Rows[i]["QuanHuyenId"].ToString();// Convert.ToInt32(drQuanHuyen.SelectedValue);
                    objCapNhatHoSoHvttChiNhanh.PhuongXaId = dt.Rows[i]["PhuongXaId"].ToString();// Convert.ToInt32(drPhuongXa.SelectedValue);

                    objCapNhatHoSoHvttChiNhanh.DienThoai = dt.Rows[i]["DienThoai"].ToString();
                    objCapNhatHoSoHvttChiNhanh.EmailNguoiLh = dt.Rows[i]["EmailNguoiLh"].ToString();
                    objCapNhatHoSoHvttChiNhanh.Fax = dt.Rows[i]["Fax"].ToString();

                   
                    // Thong tin nguoi dai dien theo PL
                    objCapNhatHoSoHvttChiNhanh.DaiDienPlTen = dt.Rows[i]["DaiDienPlTen"].ToString();
                    try
                    {
                        objCapNhatHoSoHvttChiNhanh.DaiDienPlNgaySinh = DateTime.ParseExact(dt.Rows[i]["DaiDienPlNgaySinh"].ToString(), "dd/MM/yyyy", null);
                    }
                    catch
                    {
                    }
                    objCapNhatHoSoHvttChiNhanh.DaiDienPlGioiTinh = dt.Rows[i]["DaiDienPlGioiTinh"].ToString();
                    objCapNhatHoSoHvttChiNhanh.DaiDienPlChucVuId = Convert.ToInt32(dt.Rows[i]["DaiDienPlChucVuId"].ToString());
                    objCapNhatHoSoHvttChiNhanh.DaiDienPlSoChungChiKtv = dt.Rows[i]["DaiDienPlSoChungChiKtv"].ToString();
                    try
                    {
                        objCapNhatHoSoHvttChiNhanh.DaiDienPlNgayCapCcktv = DateTime.ParseExact(dt.Rows[i]["DaiDienPlNgayCapCcktv"].ToString(), "dd/MM/yyyy", null);
                    }
                    catch
                    {
                    }

                    objCapNhatHoSoHvttChiNhanh.DaiDienPlMobile = dt.Rows[i]["DaiDienPlMobile"].ToString();
                    objCapNhatHoSoHvttChiNhanh.DaiDienPlEmail = dt.Rows[i]["DaiDienPlEmail"].ToString();
 
                    objCapNhatHoSoHvtt_provider.Insert(objCapNhatHoSoHvttChiNhanh);

                }
            }
           

            

        }

        protected void btnGhi_Click(object sender, EventArgs e)
        {
            int HoiVienTapTheId = 0;
            try
            {

                HoiVienTapTheId = Convert.ToInt32(Session["HoiVienTapTheID"].ToString());
                CapnhatYeuCauThayDoiTTHoiVien(HoiVienTapTheId);
                LoadLichSu(HoiVienTapTheId.ToString());
            }
            catch
            {

            }





        }


        private void LoadLichSu(string ID)
        {
            string cmd = "SELECT A.CapNhatHoSoHvttID, A.MaYeuCau, A.NgayYeuCau, A.NgayDuyet, B.TenTrangThai FROM tblCapNhatHoSoHvtt A left JOIN tblDMTrangThai B ON A.TinhTrangID = B.TrangThaiID WHERE A.HoiVienTapTheID = " + ID;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd, conn);
            da.Fill(ds);
            DataTable dtb = ds.Tables[0];

            gvLichSu.DataSource = dtb;
            gvLichSu.DataBind();

        }

        protected void gvChiNhanh_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    id = Convert.ToString(e.CommandArgument);
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void gvChiNhanh_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();


                if (Session["SessionDataTable"] != null)
                {
                    dt = (DataTable)Session["SessionDataTable"];
                }
                if(dt.Rows.Count>0){
                    int index= Convert.ToInt32(id);
                    dt.Rows.RemoveAt(index - 1);
                    for (int i = 1; i <= dt.Rows.Count; i++)
                    {

                        dt.Rows[i - 1]["ChiNhanhId"] = i;
                     

                    }
                   
                    Session["SessionDataTable"] = dt;
                    gvChiNhanh.DataSource = dt;
                    gvChiNhanh.DataBind();
                }

            }
            catch (Exception ex)
            {


            }
        }



        protected void btnTuChoiDuyet_Click(object sender, EventArgs e)
        {
            try
            {
                int yeuCauCapNhatId = 0;
                yeuCauCapNhatId = Convert.ToInt32(hidId.Value.ToString());
                if (yeuCauCapNhatId != null && yeuCauCapNhatId != 0)
                {
                    UpdateStatus(yeuCauCapNhatId, trangthaiTraLaiID, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), txtLyDoTuChoi.Text);
               
                    hidTrangthaiId.Value = trangthaiTraLaiID.ToString();
                    
                    string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Từ chối phê duyệt cập nhật hồ sơ thành công.</div>";

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT A.MaYeuCau, B.Email, A.HoiVienTapTheID, B.TenDoanhNghiep FROM tblCapNhatHoSoHVTT A INNER JOIN tblHoiVienTapThe B ON A.HoiVienTapTheID = B.HoiVienTapTheID WHERE A.CapNhatHoSoHVTTID = " + yeuCauCapNhatId;
                    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

                    // Gửi thông báo
                    cm.GuiThongBao(dtb.Rows[0]["HoiVienTapTheID"].ToString(), "4", "Từ chối duyệt yêu cầu cập nhật thông tin", "Yêu cầu cập nhật thông tin (Mã yêu cầu : " + dtb.Rows[0]["MaYeuCau"].ToString() + ") của bạn không được phê duyệt<BR>" + "Lý do: " + txtLyDoTuChoi.Text);
                    cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối phê duyệt yêu cầu cập nhật thông tin hội viên " + dtb.Rows[0]["TenDoanhNghiep"].ToString());
                    // Send mail
                    string body = "";
                    body += "Yêu cầu cập nhật thông tin của công ty không được phê duyệt (Mã yêu cầu : " + dtb.Rows[0]["MaYeuCau"].ToString() + ")<BR>";
                    body += "Lý do: " + txtLyDoTuChoi.Text;
                    string msg = "";
                    if (SmtpMail.Send("BQT WEB VACPA", dtb.Rows[0]["Email"].ToString(), "VACPA - Từ chối phê duyệt yêu cầu cập nhật thông tin", body, ref msg))
                    {
                        placeMessage.Controls.Add(new LiteralControl(thongbao));
                    }
                    else
                        placeMessage.Controls.Add(new LiteralControl(msg));
                    init_button();
                }

            }
            catch (Exception ex)
            {
                Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            }


        }

        protected void btnPheDuyet_Click(object sender, EventArgs e)
        {

            try
            {
                int yeuCauCapNhatId = 0;
                yeuCauCapNhatId = Convert.ToInt32(hidId.Value.ToString());
                if (yeuCauCapNhatId != null && yeuCauCapNhatId != 0)
                {
                    //UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, NguoiDuyet,
                    //             NgayNopDon, NgayTiepNhan, NgayDuyet, LyDoTuChoi);
                    string TenDangNhap = "";
                    string MatKhau = "";
                    string Email = "";
                      UpdateStatus(yeuCauCapNhatId, trangthaiDaPheDuyet, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), "");
           
                    hidTrangthaiId.Value = trangthaiDaPheDuyet.ToString();

                   
                    string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Phê duyệt cập nhật hồ sơ thành công.</div>";

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT A.MaYeuCau, B.Email, A.HoiVienTapTheID, B.TenDoanhNghiep FROM tblCapNhatHoSoHVTT A INNER JOIN tblHoiVienTapThe B ON A.HoiVienTapTheID = B.HoiVienTapTheID WHERE A.CapNhatHoSoHVTTID = " + yeuCauCapNhatId;
                    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

                    // Gửi thông báo
                    cm.GuiThongBao(dtb.Rows[0]["HoiVienTapTheID"].ToString(), "4", "Phê duyệt yêu cầu cập nhật thông tin", "Yêu cầu cập nhật thông tin (Mã yêu cầu : " + dtb.Rows[0]["MaYeuCau"].ToString() + ") của bạn đã được phê duyệt");
                    cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt yêu cầu cập nhật thông tin hội viên " + dtb.Rows[0]["TenDoanhNghiep"].ToString());
                    // Send mail
                    string body = "";
                    body += "Yêu cầu cập nhật thông tin của công ty đã được phê duyệt (Mã yêu cầu : " + dtb.Rows[0]["MaYeuCau"].ToString() + ")";

                    string msg = "";
                    if (SmtpMail.Send("BQT WEB VACPA", dtb.Rows[0]["Email"].ToString(), "VACPA - Phê duyệt yêu cầu cập nhật thông tin", body, ref msg))
                    {
                        placeMessage.Controls.Add(new LiteralControl(thongbao));
                    }
                    else
                        placeMessage.Controls.Add(new LiteralControl(msg));

                    init_button();
                }

            }
            catch (Exception ex)
            {
                Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            }

        }

        public bool UpdateStatus(int CapNhatHoSoHVTTID, int TinhTrangID, String NguoiDuyet,
                            String NgayDuyet, String LyDoTuChoi)
        {
            DBHelpers.DBHelper objDBHelper = null;

            try
            {

                List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

                arrParams.Add(new DBHelpers.DataParameter("CapNhatHoSoHVTTID", CapNhatHoSoHVTTID));
                arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
                arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));

                arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
                arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));


                objDBHelper = ConnectionControllers.Connect("VACPA");
                return objDBHelper.ExecFunction("sp_capnhathshoivien_tt_xetduyet", arrParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectionControllers.DisConnect(objDBHelper);
            }
        }
    


        protected void setDisplayLyDoxoa()
        {


            if (hidTrangthaiId.Value == null ||
                  hidTrangthaiId.Value == "" ||
                  hidTrangthaiId.Value == trangthaiDaPheDuyet.ToString())  // Them moi , da duyet ==> ko hien thi
            {
                Response.Write(@" style=""display: none;"" ");
            }
            else // cac truong hop khac
            {
                Response.Write(@" ");

            }



        }


        public void init_button()
        {
            try
            {
                String actionMode = Request.QueryString["act"];

                //linkGhi.Visible = false;
                //linkTuChoiDon.Visible = false;
                //linkTrinhPheDuyet.Visible = false;
                linkTuChoiDuyet.Visible = false;
                linkPheDuyet.Visible = false;
                //linkThoaiDuyet.Visible = false;

                if (actionMode == "view")
                {// vieu
                    hidMode.Value = "view";

                }
                if (actionMode == "add")
                {// add


                    hidMode.Value = "add";
                   // linkGhi.Visible = true; ;
                   // linkTuChoiDon.Visible = false;
                  //  linkTrinhPheDuyet.Visible = false;
                    linkTuChoiDuyet.Visible = false;
                    linkPheDuyet.Visible = false;
                    // linkThoaiDuyet.Visible = false;

                }

                if (actionMode == "edit")
                { // edit
                    hidMode.Value = "edit";
                    String tempTrangthai = "";
                    try
                    {
                        tempTrangthai = (hidTrangthaiId.Value);
                    }
                    catch
                    {

                    }
                    //int trangthaiChoTiepNhanID = 3;
                    //int trangthaiTuChoiID = 4;
                    //int trangthaiChoDuyetID = 1;
                    //int trangthaiTraLaiID = 2;
                    //int trangthaiThoaiDuyetID = 5;
                    //int trangthaiDaPheDuyet = 6;

                    //  
                   if (tempTrangthai == trangthaiChoDuyetID.ToString())
                    {   // da trinh phe duyet( cho duyet)
                        //linkGhi.Visible = false;
                        //linkTuChoiDon.Visible = false;
                        //linkTrinhPheDuyet.Visible = false;
                        linkTuChoiDuyet.Visible = true;
                        linkPheDuyet.Visible = true;
                        // linkThoaiDuyet.Visible = false;
                    }
                    else if (tempTrangthai == trangthaiTraLaiID.ToString())
                    { // Chu tich ko phe duyet
                        //linkGhi.Visible = false;
                        //linkTuChoiDon.Visible = false;
                        //linkTrinhPheDuyet.Visible = false;
                        linkTuChoiDuyet.Visible = false;
                        linkPheDuyet.Visible = false;
                        //linkThoaiDuyet.Visible = true;
                    }
                    else if (tempTrangthai == trangthaiDaPheDuyet.ToString())
                    {   // Chu tich da phe duyet
                        //linkGhi.Visible = false;
                        //linkTuChoiDon.Visible = false;
                        //linkTrinhPheDuyet.Visible = false;
                        linkTuChoiDuyet.Visible = false;
                        linkPheDuyet.Visible = false;
                        //linkThoaiDuyet.Visible = true;
                    }

                    else
                    {
                        //linkGhi.Visible = false;
                        //linkTuChoiDon.Visible = false;
                        //linkTrinhPheDuyet.Visible = false;
                        linkTuChoiDuyet.Visible = false;
                        linkPheDuyet.Visible = false;
                        // linkThoaiDuyet.Visible = false;
                    }


                }


            }
            catch (Exception ex)
            {
                Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));


            }

        }

    
    }
   