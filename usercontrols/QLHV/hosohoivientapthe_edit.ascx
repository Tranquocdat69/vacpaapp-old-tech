﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="hosohoivientapthe_edit.ascx.cs" Inherits="usercontrols_hosohoivientapthe_edit" %>
  <script type="text/javascript" src="js/jquery.jgrowl.js"></script>
<script type="text/javascript" src="js/elements.js"></script>
 <style type="text/css">
    .Hide
    {
      display:none;
    }
</style>

    <style type="text/css">
    .lbCapcha{float:left;}
     .formRowTable { padding: 5px 4px; clear: both; border-bottom: 1px solid #E2E2E2; border-top: 1px solid white; position: relative; font-size:13px;  top: 0px;  left: 0px; }
    </style>
   <script type="text/javascript" src="../../js/chosen.jquery.min.js"></script>
 
 
 

<style>
    .require
    {
        color: Red;
    }
    .error
    {
        color: Red;
        font-size: 11px;
    }
    .Hide
    {
        display: none;
    }
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript" src="js/chosen.jquery.min.js"></script>
<script type="text/javascript">


    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });

    function reCalljScript() {
        $('.auto').autoNumeric('init');
        // Select with Search
        jQuery(".chzn-select").chosen();
        //   __doPostBack('', '');
        $.datepicker.setDefaults($.datepicker.regional['vi']);

        $('.date2').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            maxDate: '0'
        });

        $(function () {
            $("#<%=txtNgayThanhLap.ClientID%> , #<%=txtNgayCapGiayChungNhanDKKD.ClientID%> ,#<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%> ,#<%=txtNguoiDaiDienPL_NgayCapChungChiKTV.ClientID%> ,#<%=txtNguoiDaiDienLL_NgaySinh.ClientID%> ,#<%=txtNguoiDaiDienPL_NgaySinh.ClientID%>").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy"
            }).datepicker("option", "maxDate", '+0m +0w +0d');

            $("#<%=txtNgayThanhLap.ClientID%> , #<%=txtNgayCapGiayChungNhanDKKD.ClientID%> ,#<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%> ,#<%=txtNguoiDaiDienPL_NgayCapChungChiKTV.ClientID%> ,#<%=txtNguoiDaiDienLL_NgaySinh.ClientID%> ,#<%=txtNguoiDaiDienPL_NgaySinh.ClientID%>").mask("99/99/9999");
        });



    }

     


</script>
 
 <form method="post" enctype="multipart/form-data" name="form_nhaphoso" id="form_nhaphoso"
        runat="server" clientidmode="Static">
 <div class="tabbedwidget tab-primary">
                            <ul>
                                <li><a href="#tab1">Hồ sơ thành viên</a></li>
                                <li><a href="#tab2">Chi nhánh</a></li>
                                <li><a href="#tab3">KTV</a></li>
                                <li><a href="#tab4">Đào tạo, CNKT</a></li>
                                <li><a href="#tab5">KSCL</a></li>
                                <li><a href="#tab6">Thanh toán</a></li>
                                <li><a href="#tab7">Khen thưởng kỷ luật</a></li>
                                
                            </ul>
   <div id="tab1">
         <asp:PlaceHolder ID="placeMessage" runat="server"></asp:PlaceHolder>
<div class="widgetbox">
    <h4 class="widgettitle">
        <asp:Label ID="lbTitle" runat="server" Text="Hồ sơ hội viên tổ chức"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4>
    <div class="widgetcontent">
       
        <div id="thongbaoloi_form_nhaphoso" name="thongbaoloi_form_nhaphoso" style="display: none"
            class="alert alert-error">
        </div>
        <asp:ScriptManager ID="s1" runat="server">
        </asp:ScriptManager>
  
       
       
            <asp:UpdatePanel ID="UpdatePanelThongTinDN" runat="server" ><ContentTemplate>
            
             <script type="text/javascript" language="javascript">
                 Sys.Application.add_load(reCalljScript);
             </script>
     

                <table id="Table2" width="100%" border="0" class="formtbl" runat="server">
                <tr class="trbgr">
                <td colspan="4">
                    <h6>
                       Thông tin tài khoản</h6>
                    
                </td>
                 </tr>
                    <tr class="formRowTable" >
                        <td width="135"><label>Tên đăng nhập</label></td>
                    <td>
                      <div class="formRight">
                     <asp:TextBox ID="txtTenDangNhap" runat="server" Width="100%"></asp:TextBox>
                     
                     </div>
                    </td>   
                        <td colspan="2"><asp:CheckBox ID="chkHVTT" runat="server" Text="Chuyển thành hội viên tổ chức" TextAlign="Right" /><asp:CheckBox ID="chkCTKT" runat="server" Text="Chuyển thành công ty kiểm toán" TextAlign="Right" /></td>      
                    </tr>
                    <tr class="formRowTable" id="trLoaiHoiVien" runat="server">
                     <td width="135"><label>Đối tượng:</label></td>
                    <td>
                      <div class="formRight">
                    <asp:DropDownList ID="drLoaiHoiVien" runat="server" Width="100%" Height="30px">
                        <asp:ListItem Text="<< Chọn >>" Value="-1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Hội viên chính thức" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Hội viên liên kết" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                       </div>
                    </td>   
                       <td></td>           <td></td>
                    </tr>
                    <tr class="trbgr">
                <td colspan="4">
                    <h6>
                       Thông tin doanh nghiệp</h6>
                    
                </td>
                 </tr>
                    
                    <tr class="formRowTable" >
                        <td width="135"><label>Tên doanh nghiệp</label><span class="require">(*)</span></td>
                    <td>
                      <div class="formRight">
                     <asp:TextBox ID="txtTenDoanhNghiep" runat="server" Width="100%"></asp:TextBox>
                     <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="reqTenDoanhNghiep" runat="server" 
                          ControlToValidate="txtTenDoanhNghiep" SetFocusOnError="true" 
                            ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin tên doanh nghiệp.">
                     </asp:RequiredFieldValidator>--%>
                     </div>
                    </td>   
                    <td width="135"><label>Tên viết tắt</label><span class="require">(*)</span></td>
                    <td>
                      <div class="formRight">
                      <asp:TextBox ID="txtTenVietTat" runat="server" Width="100%"></asp:TextBox>
                         
                          <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator7" runat="server" 
                          ControlToValidate="txtTenVietTat" SetFocusOnError="true" 
                            ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin tên viết tắt.">
                     </asp:RequiredFieldValidator>--%>

                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="reqtxtTenVietTat" runat="server" ControlToValidate="txtTenVietTat" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin tên viết tắt"></asp:RequiredFieldValidator>--%>

                     </div>
                    </td>              
                    </tr>
                      
                                 
                    <tr class="formRowTable">
                         <td width="135"><label>Tên tiếng Anh</label></td>
                    <td>
                      <div class="formRight">
                      <asp:TextBox ID="txtTenTiengAnh" runat="server" Width="100%"></asp:TextBox>
                    <%--  <asp:Label ID="Label3" runat="server" Text="" Height="13px" > </asp:Label>--%>
                        </div>
                    </td>  
                    <td width="135"><label>Loại hình doanh nghiệp</label></td>
                       <td style="text-align:left;">
                      <div class="formRight">
                      <asp:DropDownList ID="drLoaiHinhDoanhNghiep" DataTextField="TenLoaiHinhDoanhNghiep" 
                      DataValueField="LoaiHinhDoanhNghiepID" runat="server" Width="228px" Height="30px">
                      </asp:DropDownList>
                 <%--   <asp:Label ID="Label4" runat="server" Text="" Height="13px" > </asp:Label>--%>
                     </div>
                    </td>   
                                 
                    </tr>
                  
                    <tr class="formRowTable">
                          <td width="135"><label>Số hiệu công ty(Do BTC quy định)</label><span class="require">(*)</span></td>
                     <td>
                      <div class="formRight">
                     <asp:TextBox ID="txtSoHieuCongTy" runat="server" Width="100%"></asp:TextBox>
                     <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator5" runat="server" 
                          ControlToValidate="txtSoHieuCongTy" SetFocusOnError="true" 
                            ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số hiệu công ty.">
                     </asp:RequiredFieldValidator>--%>
                     </div>
                    </td>   
                    <td   colspan="2">
                 
                      <asp:Label ID="Label1" runat="server" Text="" Height="13px" > </asp:Label> <br />
                      
                       <asp:RadioButton ID="rdDN_TrongNuoc" runat="server" GroupName="DoanhNghiep" Text="Doanh nghiệp trong nước"  value="1" Checked="true" /> 
                       <asp:RadioButton ID="rdDN_NgoaiNuoc" runat="server" GroupName="DoanhNghiep" Text="Doanh nghiệp nước ngoài"  value="2" />
                      <%-- <br /> <asp:Label ID="Label5" runat="server" Text="" Height="13px" > </asp:Label>--%>
               
                    </td>   
                                 
                    </tr>
                  
                    <tr class="formRowTable" id="trHoiVienCha" runat="server">
                     <td width="135"><label>Là chi nhánh của hội sở chính:</label></td>
                    <td colspan="3">
                      <div class="formRight">
                    <asp:DropDownList ID="drHoiVienTapThe"  DataTextField="TenDoanhNghiep" 
                              DataValueField="HoiVienTapTheID" runat="server" Width="100%" Height="30px"></asp:DropDownList>
                       </div>
                    </td>   
                                  
                    </tr>
                    <tr class="formRowTable">
                     <td width="135"><label>Địa chỉ trụ sở chính</label><span class="require">(*)</span></td>
                    <td colspan="3">
                      <div class="formRight">
                    <asp:TextBox ID="txtDiaChi" runat="server" Width="100%"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDiaChi" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin địa chỉ."></asp:RequiredFieldValidator>--%>
                       </div>
                    </td>   
                                  
                    </tr>
           
           
      
                             
                    <tr class="formRowTable">
                        <td width="135"><label>Tỉnh/Thành</label><span class="require">(*)</span></td>
                       <td style="text-align:left;">
                      <div class="formRight">
                        <asp:DropDownList ID="drTinhThanh"  DataTextField="TenTinh" 
                              DataValueField="MaTinh" runat="server" Width="228px" Height="30px"  AutoPostBack="true" 
                              onselectedindexchanged="drTinhThanh_SelectedIndexChanged"></asp:DropDownList>
                        <%--<asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="00"     ID="RequiredFieldValidatordrTinhThanh" runat="server" ControlToValidate="drTinhThanh" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin tỉnh thành."></asp:RequiredFieldValidator>--%>
                  
               
                       </div>
                    </td>   
                    <td width="135"><label>Quận/Huyện</label></td>
                       <td style="text-align:left;">
                      <div class="formRight">
                        <asp:DropDownList ID="drQuanHuyen"  DataTextField="TenHuyen" 
                              DataValueField="MaHuyen" runat="server" Width="228px" Height="30px" 
                        AutoPostBack="true" onselectedindexchanged="drQuanHuyen_SelectedIndexChanged"  ></asp:DropDownList>

                 <%--   <select name="HuyenID_DiaChi" id="HuyenID_DiaChi" style="width:230px;height:30px;">
                         <%  
              
                              cm.Load_QuanHuyen();
              
                          %>
                      </select>--%>
                     <%-- <asp:Label ID="Label6" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      </div>
                    </td>              
                    </tr>

                    <tr class="formRowTable">
                          <td width="135"><label>Phường/Xã</label></td>
                    <td style="text-align:left;">
                      <div class="formRight">
                        <asp:DropDownList ID="drPhuongXa"  DataTextField="TenXa" DataValueField="MaXa" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                  <%--  <select name="XaID_DiaChi" id="XaID_DiaChi" style="width:230px;height:30px;">
             <%  
              
                  cm.Load_PhuongXa();
              
              %></select>--%>
                    <%-- <asp:Label ID="Label7" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      </div>
                   </td>   
                     
                     <td width="135"><label>Ngày thành lập</label></td>
                    <td>
                      <div class="formRight">
                      <asp:TextBox ID="txtNgayThanhLap" runat="server" Width="100%"></asp:TextBox>
                      <%--<asp:RangeValidator ID="RangeValidatortxtNgayThanhLap" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="31/12/9999"
                  ControlToValidate ="txtNgayThanhLap"  SetFocusOnError="true" ></asp:RangeValidator>--%>

                   <%--   <asp:Label ID="Label8" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      </div>
                    </td>
                    </tr>

       


                    <tr class="formRowTable" >
                    <td width="135"><label>Điện thoại</label><span class="require">(*)</span></td>
                    <td>
                      <div class="formRight">
                     <asp:TextBox ID="txtDienThoai" runat="server" Width="100%" onkeypress="return NumberOnly()"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtDienThoai" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin điện thoại."></asp:RequiredFieldValidator>--%>
                       </div>
                    </td>   
                    <td width="135"><label>Email</label><span class="require">(*)</span></td>
                    <td>
                       <div class="formRight">
                    <asp:TextBox ID="txtEmail" runat="server" Width="100%"  ></asp:TextBox>
                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator10" runat="server" 
                          ControlToValidate="txtEmail" SetFocusOnError="true" 
                            ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin Email.">
                    </asp:RequiredFieldValidator>--%>
                      </div>
                    </td>              
                    </tr>

                    <tr class="formRowTable">
                        <td width="135"><label>Fax</label><span class="require">(*)</span></td>
                    <td>
                        <div class="formRight">

                    <asp:TextBox ID="txtFax" runat="server" Width="100%" onkeypress="return NumberOnly()"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator11" runat="server" 
                           ControlToValidate="txtFax" SetFocusOnError="true"
                             ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số Fax.">
                    </asp:RequiredFieldValidator>--%>
                     </div>
                    </td>   
                    <td width="135"><label>Website</label><%--<span class="require">(*)</span>--%></td>
                    <td>
                      <div class="formRight">
                    <asp:TextBox ID="txtWebsite" runat="server" Width="100%"  ></asp:TextBox>
                    <%--<asp:Label ID="Label9" runat="server" Text="" Height="13px" > </asp:Label>--%>
                  <%--  <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtWebsite" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin Website."></asp:RequiredFieldValidator>--%>
                     </div>
                    </td>              
                    </tr>
 
                    <tr class="formRowTable">
                    <td width="135"><label>Số giấy chứng nhận ĐKKD</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                      
                     <asp:TextBox ID="txtSoGiayChungNhanDKKD" runat="server" Width="100%"></asp:TextBox>
                     <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator12" runat="server" 
                             ControlToValidate="txtSoGiayChungNhanDKKD" SetFocusOnError="true" 
                               ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số giấy chứng nhận .">
                       </asp:RequiredFieldValidator>--%>
                      </div>
                    </td>  
                    <td width="135"><label>Ngày cấp</label><span class="require">(*)</span></td>
                    <td>
                     
                     <div class="formRight">
                      <asp:TextBox ID="txtNgayCapGiayChungNhanDKKD" runat="server" Width="100%"></asp:TextBox>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator13" runat="server"
                       ControlToValidate="txtNgayCapGiayChungNhanDKKD" SetFocusOnError="true"
                          ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày cấp"></asp:RequiredFieldValidator>
                     
                         <asp:RangeValidator ID="RangeValidatortxtNgayCapGiayChungNhanDKKD" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="31/12/9999"
                  ControlToValidate ="txtNgayCapGiayChungNhanDKKD"  SetFocusOnError="true" ></asp:RangeValidator>--%>


                       </div>
                    
                  </td>              
                    </tr>

                    <tr class="formRowTable">
                    <td width="135"><label>Số giấy chứng nhận đủ điều kiện KDDV Kiểm toán</label></td>
                
                    <td>
                     <div class="formRight">
                      <asp:TextBox ID="txtSoGiayChungNhanKDDVKT" runat="server" Width="100%"></asp:TextBox>
                     
                     
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtNgayCapGiayChungNhanDKKD" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage=""></asp:RequiredFieldValidator>--%>
                      </div>
                  </td>              
                
                       <td width="135"><label>Ngày cấp</label></td>
                    <td> 
                     <div class="formRight">
                    <asp:TextBox ID="txtNgayCapGiayChungNhanKDDVKT" runat="server" Width="100%"></asp:TextBox>
                         <%--<asp:RangeValidator ID="RangeValidatortxtNgayCapGiayChungNhanKDDVKT" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="31/12/9999"
                  ControlToValidate ="txtNgayCapGiayChungNhanKDDVKT"  SetFocusOnError="true" ></asp:RangeValidator>--%>

                   <%-- <asp:Label ID="Label10" runat="server" Text="" Height="13px" > </asp:Label>--%>
                    </div>
                    
                    </td>  

                
                    </tr>

                    <tr class="formRowTable">
                     
                    <td width="135"><label>Mã số thuế</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                    <asp:TextBox ID="txtMaSoThue" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtMaSoThue" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin mã số thuế."></asp:RequiredFieldValidator>--%>
                      </div>
                    </td>  
                


             
                    <td width="135"><label>Lĩnh vực hoạt động</label><span class="require">(*)</span></td>
                       <td style="text-align:left;">
                     <div class="formRight">
                      <asp:DropDownList ID="drLinhvucHoatDong"  DataTextField="TenLinhVucHoatDong" DataValueField="LinhVucHoatDongID" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                     <%--<asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="0"     ID="RequiredFieldValidator15" runat="server" ControlToValidate="drLinhvucHoatDong" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin lĩnh vực hoạt động."></asp:RequiredFieldValidator>--%>
                      </div>
                    </td>              
                    </tr>
 

                    <tr align="left" >
                     <td colspan="4"  style=" text-align:left">
                     <asp:Label ID="Label21" runat="server" Text="" Height="13px" > </asp:Label>
                     <br />
                     <label   style="  font-weight: bold;  ">Công ty đủ điều kiện kiểm toán đơn vị:   </label>
                     <%--<br /> <asp:Label ID="Label14" runat="server" Text="" Height="13px" > </asp:Label>--%>
                    
                     </td>
                    </tr>
                   
                    <tr class="formRowTable" >
                     <td ><label>Có lợi ích công chúng năm </label></td>
                     <td >
                      
                      <div class="formRight">
                         <asp:TextBox ID="txtNamCoLoiIchCongChung" runat="server" Width="30%" 
                          class="auto" data-a-sep=""  data-a-dec="," data-v-min="0" data-v-max="9999" 
                              MaxLength="4"  ></asp:TextBox>
                        <%--  <asp:Label ID="Label11" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      </div>
                      </td>   
                     <td   ><label>Trong lĩnh vực chứng khoán năm </label></td>   
                     <td>
                        <div class="formRight">
                         <asp:TextBox ID="txtNamTrongLinhVucChungKhoan" runat="server" Width="30%"
                          class="auto" data-a-sep=""  data-a-dec="," data-v-min="0" data-v-max="9999" 
                                MaxLength="4" ></asp:TextBox>
                        <%-- <asp:Label ID="Label12" runat="server" Text="" Height="13px" > </asp:Label>--%>
                        </div>
                     </td>   

                    </tr>



                    <tr class="formRowTable" >
                        <td width="135"><label>Công ty là thành viên hãng kiểm toán quốc tế </label><%--<span class="require">(*)</span>--%></td>
                        <td colspan="3">
                         <div class="formRight">
                         <asp:TextBox ID="txtThanhVienHangKTQT" runat="server" Width="100%"></asp:TextBox>
                         <%--<asp:Label ID="Label13" runat="server" Text="" Height="13px" > </asp:Label>--%>
                        <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtThanhVienHangKTQT" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin địa chỉ."></asp:RequiredFieldValidator>--%>
                        </div>
                        </td>   
                                  
                    </tr>

                    <tr class="formRowTable">
                     <td width="100"><label>Tổng số người làm việc tại doanh nghiệp </label></td>   
                    <td>
                       <div class="formRight">
                       <asp:TextBox ID="txtTongSoNguoiLamViec" runat="server" Width="30%"
                        class="auto" data-a-sep="."  data-a-dec="," data-v-max="9999" ></asp:TextBox>
                     <%--  <asp:Label ID="Label15" runat="server" Text="" Height="13px" > </asp:Label>--%>
                       </div>
                       </td>   
                     <td width="100"><label>Tổng số người có chứng chỉ KTV</label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtTongSoNguoiCoCKKTV" runat="server" Width="30%"
                     class="auto" data-a-sep="."  data-a-dec="," data-v-max="9999"      ></asp:TextBox>
                   <%--  <asp:Label ID="Label16" runat="server" Text="" Height="13px" > </asp:Label>--%>
                     </div>
                     </td>   

                    
                    </tr>

                    <tr class="formRowTable">
                     <td width="100"><label>Tổng số KTV đăng ký hành nghề </label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtTongSoKTVDKHN" runat="server" Width="30%"
                     class="auto" data-a-sep="."  data-a-dec="," data-v-max="9999" ></asp:TextBox>
                    <%-- <asp:Label ID="Label17" runat="server" Text="" Height="13px" > </asp:Label>--%>
                     </div>
                     
                     </td>   
                     <td width="100"><label>Tổng số hội viên cá nhân </label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtTongSoHoiVienCaNhan" runat="server" Width="30%"
                      class="auto" data-a-sep="."  data-a-dec="," data-v-max="9999" ></asp:TextBox>
                   <%--  <asp:Label ID="Label18" runat="server" Text="" Height="13px" > </asp:Label>--%>
                     </div>
                     </td>   

                    
                    </tr>


                </table> 
                
            </ContentTemplate></asp:UpdatePanel>  
             
            
            <table id="Table1" width="100%" border="0" class="formtbl" runat="server">
                    <tr class="trbgr">
                <td colspan="4">
                    <h6>
                      Thông tin người đại diện pháp luật</h6>
                    
                </td>
                 </tr>
                 <tr  class="formRowTable" >
                         <td width="135"><label>Họ và tên</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                       <asp:TextBox ID="txtNguoiDaiDienPL_Ten" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtNguoiDaiDienPL_Ten" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin họ tên."></asp:RequiredFieldValidator>--%>
                    </div>
                    </td>  

                    <td width="135"><label>Ngày sinh</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                       <asp:TextBox ID="txtNguoiDaiDienPL_NgaySinh" runat="server" Width="100%"></asp:TextBox>
                     
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator155" runat="server" 
                           ControlToValidate="txtNguoiDaiDienPL_NgaySinh" SetFocusOnError="true" 
                               ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày sinh">
                       </asp:RequiredFieldValidator>--%>
                        <%--<asp:RangeValidator ID="RangeValidatortxtNguoiDaiDienPL_NgaySinh" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="31/12/9999"
                  ControlToValidate ="txtNguoiDaiDienPL_NgaySinh"  SetFocusOnError="true" ></asp:RangeValidator>--%>

                       </div>
                  </td>              
                    </tr>



                  <tr  class="formRowTable" >
                   <td width="135"><label>Giới tính</label><span class="require">(*)</span></td>
                       <td style="text-align:left;">
                     <div class="formRight">
                      <asp:DropDownList ID="drNguoiDaiDienPL_GioiTinh" DataTextField="TenLoaiHinhDoanhNghiep" DataValueField="LoaiHinhDoanhNghiepID" runat="server" Width="228px" Height="30px">
                                <asp:ListItem Value="1" Text="Nam"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Nữ"></asp:ListItem>
                      </asp:DropDownList>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator24" runat="server" ControlToValidate="drNguoiDaiDienPL_GioiTinh" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin giới tính."></asp:RequiredFieldValidator>--%>
                 
                      </div>
                    </td>  
                    <td width="135"><label>Chức vụ</label><span class="require">(*)</span></td>
                      <td style="text-align:left;">
                     <div class="formRight">
                      <asp:DropDownList ID="drNguoiDaiDienPL_ChucVu"    DataTextField="TenChucVu" DataValueField="ChucVuID" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                     
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="0"    ID="RequiredFieldValidator18" runat="server" ControlToValidate="drNguoiDaiDienPL_ChucVu" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin chức vụ"></asp:RequiredFieldValidator>--%>
                      </div>
                  </td>              
                    </tr>


                   <tr  class="formRowTable" >
                   <td width="135"><label>Số chứng chỉ KTV</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtNguoiDaiDienPL_SoChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtNguoiDaiDienPL_SoChungChiKTV" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số chứng chỉ KTV."></asp:RequiredFieldValidator>--%>
                     </div>
                    </td>  
                    <td width="135"><label>Ngày cấp</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight"> 
                      <asp:TextBox ID="txtNguoiDaiDienPL_NgayCapChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator20" runat="server" 
                              ControlToValidate="txtNguoiDaiDienPL_NgayCapChungChiKTV" SetFocusOnError="true" 
                                ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày cấp chứng chỉ KTV">
                        </asp:RequiredFieldValidator>
                          <asp:RangeValidator ID="RangeValidatortxtNguoiDaiDienPL_NgayCapChungChiKTV" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="31/12/9999"
                  ControlToValidate ="txtNguoiDaiDienPL_NgayCapChungChiKTV"  SetFocusOnError="true" ></asp:RangeValidator>--%>

                        </div>
                  </td>              
                    </tr>


                    <tr  class="formRowTable" >
                   <td width="135"><label>Mobie</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtNguoiDaiDienPL_DiDong" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtNguoiDaiDienPL_DiDong" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số di động."></asp:RequiredFieldValidator>--%>
                     </div>
                    </td>  
                   
                       <td width="135"><label>Email</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtNguoiDaiDienPL_Email" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator23" runat="server" ControlToValidate="txtNguoiDaiDienPL_Email" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin địa chỉ Email."></asp:RequiredFieldValidator>--%>
                      </div>
                    </td>  
                    </tr>

                     <tr  class="formRowTable" >

                     <td width="135"><label>Điện thoại cố định</label><%--<span class="require">(*)</span>--%></td>
                    <td>
                     <div class="formRight">
                      <asp:TextBox ID="txtNguoiDaiDienPL_DienThoaiCD" runat="server" Width="100%"></asp:TextBox>
                      <%--<asp:Label ID="Label19" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtNguoiDaiDienPL_DienThoaiCD" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage=""></asp:RequiredFieldValidator>--%>
                     </div>
                    
                       </td>              
                  
              
                    <td width="135"> </td>
                    <td>

                  </td>              
                    </tr>

                </table>   


            <table id="Table3" width="100%" border="0" class="formtbl" runat="server">
                    <tr class="trbgr">
                <td colspan="4">
                    <h6>
                      Thông tin người đại diện liên lạc</h6>
                    
                </td>
                 </tr>
               <tr  class="formRowTable" >
                         <td width="135"><label>Họ và tên</label><span class="require">(*)</span></td>
                    <td>
                       <asp:TextBox ID="txtNguoiDaiDienLL_Ten" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNguoiDaiDienLL_Ten" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin họ tên."></asp:RequiredFieldValidator>--%>
                 
                    </td>  
                    <td width="135"><label>Ngày sinh</label><span class="require">(*)</span></td>
                    <td>
                       <asp:TextBox ID="txtNguoiDaiDienLL_NgaySinh" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator2" runat="server"
                               ControlToValidate="txtNguoiDaiDienLL_NgaySinh" SetFocusOnError="true"
                                  ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày sinh">
                       </asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidatortxtNguoiDaiDienLL_NgaySinh" runat="server" 
                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="31/12/9999"
                  ControlToValidate ="txtNguoiDaiDienLL_NgaySinh"  SetFocusOnError="true" ></asp:RangeValidator>--%>

                  </td>              
                    </tr>



                      <tr class="formRowTable">
                   <td width="135"><label>Giới tính</label><span class="require">(*)</span></td>
                       <td style="text-align:left;">
                      <asp:DropDownList ID="drNguoiDaiDienLL_GioiTinh" DataTextField="TenLoaiHinhDoanhNghiep" DataValueField="LoaiHinhDoanhNghiepID" runat="server" Width="228px" Height="30px">
                                <asp:ListItem Value="1" Text="Nam"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Nữ"></asp:ListItem>
                      </asp:DropDownList>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator3" runat="server" ControlToValidate="drNguoiDaiDienLL_GioiTinh" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin giới tính."></asp:RequiredFieldValidator>--%>
                 
                  
                    </td>  
                    <td width="135"><label>Chức vụ</label><span class="require">(*)</span></td>
                      <td style="text-align:left;">
                      <asp:DropDownList ID="drNguoiDaiDienLL_ChucVu"     DataTextField="TenChucVu" DataValueField="ChucVuID" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                     
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="0"    ID="RequiredFieldValidator4" runat="server" ControlToValidate="drNguoiDaiDienLL_ChucVu" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin chức vụ"></asp:RequiredFieldValidator>--%>
                  </td>              
                    </tr>


                     <%--     <tr>
                   <td width="135"><label>Số chứng chỉ KTV</label><span class="require">(*)</span></td>
                    <td><asp:TextBox ID="txtNguoiDaiDienLL_SoChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                       <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtNguoiDaiDienLL_SoChungChiKTV" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số chứng chỉ KTV."></asp:RequiredFieldValidator>
                 
                    </td>  
                    <td width="135"><label>Ngày cấp</label><span class="require">(*)</span></td>
                    <td>
                     <asp:TextBox ID="txtNguoiDaiDienLL_NgayCapChungChiKTV" runat="server" Width="100%"></asp:TextBox>
                        <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator7" runat="server" 
                                 ControlToValidate="txtNguoiDaiDienLL_NgayCapChungChiKTV" SetFocusOnError="true"
                                    ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin ngày cấp chứng chỉ KTV">
                       </asp:RequiredFieldValidator>
                  </td>              
                    </tr>--%>


                          <tr class="formRowTable" >
                   <td width="135"><label>Mobie</label><span class="require">(*)</span></td>
                    <td><asp:TextBox ID="txtNguoiDaiDienLL_DiDong" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtNguoiDaiDienLL_DiDong" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin số di động."></asp:RequiredFieldValidator>--%>
                 
                    </td>  
                  
                      <td width="135"><label>Email</label><span class="require">(*)</span></td>
                    <td><asp:TextBox ID="txtNguoiDaiDienLL_Email" runat="server" Width="100%"></asp:TextBox>
                       <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtNguoiDaiDienLL_Email" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage="Nhập thông tin địa chỉ Email."></asp:RequiredFieldValidator>--%>
                 
                    </td>  
                  
                  
                  
                              
                    </tr>

                    <tr  class="formRowTable" >
                      <td width="135"><label>Điện thoại cố định</label><%--<span class="require">(*)</span>--%></td>
                    <td>
                     <asp:TextBox ID="txtNguoiDaiDienLL_DienThoaiCD" runat="server" Width="100%"></asp:TextBox>
                    
                 <%--<asp:Label ID="Label20" runat="server" Text="" Height="13px" > </asp:Label>--%>
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtNguoiDaiDienLL_DienThoaiCD" SetFocusOnError="true"   ValidationGroup="CheckFileExt"  ErrorMessage=""></asp:RequiredFieldValidator>--%>
                  </td>  

               
                    <td width="135"> </td>
                    <td>

                  </td>              
                    </tr>

                </table> 
            <table id="Table4" width="100%" border="0" class="formtbl" runat="server">
                    <tr class="trbgr">
                <td colspan="4">
                    <h6>
                       Tải file xác nhận</h6>
                    
                </td>
                 </tr>
   
            <tr>
            <td colspan="4">
                    
        <table width="100%">
                        <tr>
                            <td colspan="3" align="center">
                                <em style="color: #F90; font-size: 14px;">(File hồ sơ tải lên phải thuộc một trong các
                                    định dạng <em style="color: #Ff0000; font-weight: bold">.DOC, .DOCX, .PDF, .BMP, .GIF,
                                        .PNG, .JPG, .RAR, .ZIP</em> . Kích thước file tải lên tối đa <em style="color: #Ff0000;
                                            font-weight: bold">10MB</em>, chất lượng phải đảm bảo để người tiếp nhận
                                    hồ sơ có thể đọc được. <em style="color: #Ff0000; font-weight: bold">Tất cả các file
                                        hồ sơ phải được quét hoặc chụp từ bản gốc (kể cả Đơn, Giấy cam kết, Giấy giới thiệu,….)</em>.
                                    Tổ chức nộp hồ sơ phải chịu trách nhiệm hoàn toàn trước pháp luật về tính chính
                                    xác của hồ sơ nộp trực tuyến với hồ sơ gốc.)</em>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:GridView   ID="FileDinhKem_grv" runat="server"  Width="100%"
                         AutoGenerateColumns="False" 
                         EnableModelValidation="True"    AlternatingRowStyle-BackColor="WhiteSmoke"  >
                  <Columns>
             
          
                <asp:TemplateField  HeaderText="   "  >
                <ItemTemplate>
               
               
                  <asp:Label ID="labTenloaiFile"  runat="server" Text= '<%# Eval("TenBieuMau") %>' />
                <asp:Label ID="requred" runat="server" Text="(*)" ForeColor="Red"  ></asp:Label>
            
                </ItemTemplate><HeaderStyle HorizontalAlign="Center"></HeaderStyle>
              <ItemStyle 
                        HorizontalAlign="Left"   /></asp:TemplateField>

                <asp:TemplateField HeaderText=' '    >
                  <ItemTemplate >
                  <table>
                 <tr  >
                     <td >
                     
                       <asp:FileUpload ID="FileUp"   name="FileUp" runat="server" />
                       
                     </td>
                     </tr>
                      <tr  >
                       <td>
                    
                           <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                               ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Jj][Pp][Gg])|.*\.([Bb][Mm][Pp])|.*\.([pP][nN][gG])|.*\.([pO][dD][fF])|.*\.([rR][aA][rR])|.*\.([zZ][iI][pP])|.*\.(?:[dD][oO][cC][xX]?)$)"
                               ControlToValidate="FileUp" Display="Dynamic" 
                                 ErrorMessage="Sai định dạng tập tin đính kèm"  ValidationGroup="CheckFileExt"  SetFocusOnError="true" >
                                 </asp:RegularExpressionValidator>
                           <%--<asp:RequiredFieldValidator id="RequiredFieldFileUpload" runat="server"
                              ControlToValidate="FileUp" Display="Dynamic"
                              ErrorMessage="Nhập tệp đính kèm."  ValidationGroup="CheckFileExt"
                              ForeColor="Red"
                              SetFocusOnError="true">
                            </asp:RequiredFieldValidator>--%>
                         
                            </td>
                            
                            </tr></table></ItemTemplate><HeaderStyle HorizontalAlign="Left" />
                           

                <ItemStyle 
                        HorizontalAlign="Right"  ></ItemStyle>
                       
              </asp:TemplateField>
              
               

            <asp:TemplateField  HeaderText="   " >
              <ItemTemplate>
                  
                <asp:HyperLink ID="linkFileUpload" NavigateUrl='<%# Eval("FileID","Download.ashx?AttachFileID={0}") %>'   runat="server">
                    <div style="width: 200px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis">
             
                   <asp:Label ID="Label1"  runat="server" Text= '<%# Eval("TenFileDinhKem") %>' />
                   </div>
                </asp:HyperLink></ItemTemplate><HeaderStyle HorizontalAlign="Center"></HeaderStyle>
              <ItemStyle 
                        HorizontalAlign="Left"     /></asp:TemplateField>

 
             <asp:BoundField DataField="LoaiGiayToID" HeaderText=""  ItemStyle-CssClass="Hide" 
                  HeaderStyle-CssClass="Hide" >
                    <HeaderStyle CssClass="Hide"></HeaderStyle>
                    <ItemStyle CssClass="Hide"></ItemStyle>
              </asp:BoundField>
      
            <asp:BoundField DataField="FileID" HeaderText=""  ItemStyle-CssClass="Hide" 
                  HeaderStyle-CssClass="Hide" >
                  <HeaderStyle CssClass="Hide"></HeaderStyle>
                   <ItemStyle CssClass="Hide"></ItemStyle>
              </asp:BoundField>
    
             <asp:BoundField DataField="BATBUOC" HeaderText=""  ItemStyle-CssClass="Hide" 
                  HeaderStyle-CssClass="Hide" >
                    <HeaderStyle CssClass="Hide"></HeaderStyle>
                 <ItemStyle CssClass="Hide"></ItemStyle>
              </asp:BoundField>
       
            
             <asp:BoundField DataField="TenFileDinhKem" HeaderText=""  ItemStyle-CssClass="Hide" 
                  HeaderStyle-CssClass="Hide" >
                    <HeaderStyle CssClass="Hide"></HeaderStyle>
                 <ItemStyle CssClass="Hide"></ItemStyle>
              </asp:BoundField>
          </Columns>
       </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
<tr>
          <td class="trbgr" colspan="5" ><h6>Số quyết định/ngày quyết định
              </h6></td>
        </tr>
        <tr><td colspan="5">
            <table>
                <tr>
                    <td>Số quyết định:</td>
                    <td>
                    
                    <asp:TextBox ID="txtSoQuyetDinh" runat="server" Width="100%" Font-Bold="true"></asp:TextBox>
                    
                </td>
                <td>
                    Ngày quyết định:
                    
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtNgayQuyetDinh" runat="server" Width="100%" Font-Bold="true"></asp:TextBox>
                </td>
                </tr>
            </table>
        </td></tr>
            <tr>
              <td colspan="4" ></td>
            </tr>
             <tr>
              <td colspan="4" >
              <div    <% setDisplayLyDoxoa( ); %>  >
                      <table width="100%">
                      
                           <tr class="trbgr">
                                    <td colspan="2"><h6>Lý do từ chối(Khi từ chối đơn)</h6></td></tr><tr >
                           <td>
                          <label>Lý do từ chối:</label><span class="require">(*)</span> </td><td width="100%">
                                 <asp:TextBox ID="txtLyDoTuChoi" runat="server" TextMode="MultiLine" Rows="4" ></asp:TextBox><%--<asp:RequiredFieldValidator  Display="Dynamic"   
                                 ID="RequiredFieldtxtLyDoTuChoi" runat="server" ControlToValidate="txtLyDoTuChoi" SetFocusOnError="true" 
                                 ValidationGroup="CheckLyDoTuChoi"  ErrorMessage="Nhập lý do nếu từ chối đơn."></asp:RequiredFieldValidator>--%></td></tr></table></div></td></tr>
                <tr>
              <td colspan="4" ></td>
            </tr>
                 <tr>
                <td colspan="4" align="center">

                 <a class="btn btn-rounded"  <% checkPermit("btnGhi" ); %> ><i class="iconfa-plus-sign"     ></i><asp:Button ID="btnGhi" Text="Lưu" runat="server" OnClick="btnGhi_Click" ValidationGroup="CheckFileExt"  />
                    </a>
                    <a class="btn btn-rounded"  <% checkPermit( "btnTiepNhan"); %> ><i class="iconfa-plus-sign"     ></i><asp:Button ID="btnTiepNhan" Text="Tiếp nhận" runat="server" OnClick="btnTiepNhan_Click" ValidationGroup="none"  />
                    </a>
                     <a class="btn btn-rounded"  <% checkPermit( "btnTuChoiTiepNhan"); %> ><i class="iconfa-plus-sign"     ></i><asp:Button ID="btnTuChoiTiepNhan" Text="Từ chối tiếp nhận" runat="server" OnClick="btnTuChoiTiepNhan_Click" ValidationGroup="CheckLyDoTuChoi"  />
                    </a>
                    <a class="btn btn-rounded"  <% checkPermit( "btnXoaTen"); %> ><i class="iconfa-plus-sign"     ></i><asp:Button ID="btnXoaTen" Text="Xóa tên" runat="server" OnClick="btnXoaTen_Click" ValidationGroup="none"  />
                    </a>
                     <a class="btn btn-rounded"  <% checkPermit( "btnTuChoiXoaTen"); %> ><i class="iconfa-plus-sign"     ></i><asp:Button ID="btnTuChoiXoaTen" Text="Từ chối xóa tên" runat="server" OnClick="btnTuChoiXoaTen_Click" ValidationGroup="CheckLyDoTuChoi"  />
                    </a>
                    <%--<a class="btn btn-rounded"  <% checkPermit( "btnHuyXoaTen"); %> ><i class="iconfa-plus-sign"     ></i><asp:Button ID="btnHuyXoaTen" Text="Hủy xóa tên" runat="server" OnClick="btnHuyXoaTen_Click" ValidationGroup="none"  />
                    </a>--%>
                    <a class="btn btn-rounded"><i class="iconsweets-word2"></i>
                        <asp:Button ID="btnKetXuat" Text="Kết xuất GCN" runat="server" OnClick="btnKetXuat_Click" />
                    </a>
                    <a class="btn btn-rounded"><i class="iconfa-minus-sign"></i>
                        <asp:Button ID="btnQuayLai" 
                                Text="Quay lại" runat="server" onclick="btnQuayLai_Click"   /></a>

                     <asp:HiddenField ID="hidendHoiVienTapTheID" runat="server"></asp:HiddenField>
                  <asp:HiddenField ID="hidendDonHoiVienTapTheID" runat="server"></asp:HiddenField>
                  <asp:HiddenField ID="hidendXoaTenHoiVienID" runat="server"></asp:HiddenField>
                  <asp:HiddenField ID="hidKhachHangID" runat="server" />
                  <asp:HiddenField ID="hidQuanTriID" runat="server" />
                   <asp:HiddenField ID="hidMode" runat="server" />
               
                </td>
            </tr>
 </table> 

               

    </div>
</div>
   </div>
       
       
       
          <div id="tab2">
                           <div class="widgetbox">
                                <h4 class="widgettitle">
        <asp:Label ID="Label14" runat="server" Text="Chi nhánh, văn phòng đại diện"></asp:Label><a
–</a></h4><div class="widgetcontent"><table id="Table5" width="100%" border="0" class="formtbl" runat="server">
                                <tr>
                                <td>
                            

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                        <script type="text/javascript" language="javascript">
                            Sys.Application.add_load(reCalljScript);
     </script>
     <br /> <br /> 
                 <asp:GridView ID="gvChiNhanh" runat="server" AutoGenerateColumns="false" 
                                Width="100%" CssClass="display" onrowcommand="gvChiNhanh_RowCommand" 
                                onrowdeleting="gvChiNhanh_RowDeleting"   >
                                <Columns>
                                    <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                      <asp:BoundField DataField="MaHoiVienTapThe" HeaderText="Mã chi nhánh" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="TenDoanhNghiep" HeaderText="Tên chi nhánh" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="DiaChi" HeaderText="Địa chỉ" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="DienThoai" HeaderText="Điện thoại" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="Email" HeaderText="Email" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 
                                   <%--<asp:HyperLinkField  HeaderStyle-HorizontalAlign="Center"    DataTextField="ChiNhanhID" 
                                        DataTextFormatString="&lt;a href='/Page/Usercontrols/hoivientapthe_chinhanh_edit.aspx?act=edit&ChiNhanhID={0}' class='floatbox' rev='width:max height:max   enableDragResize:true scrolling:auto controlPos:tr loadPageOnClose:?reload=true'&gt; &lt;img src='/Usercontrols/images/icons/color/pencil.png' border=0 &gt;" HeaderText="Sửa"
                                      >   
                                      <HeaderStyle HorizontalAlign="Center" Width="30px"></HeaderStyle>
                                     <ItemStyle HorizontalAlign="Center" />
                  
                   
                                    </asp:HyperLinkField >--%>
        

                                    <%--<asp:TemplateField HeaderStyle-Width="30"  HeaderText="CongTy_Xoa"  ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            
                                                <asp:ImageButton ImageUrl="/images/cross.png" ID="btnDelete" runat="server" CommandArgument='<%#Bind("ChiNhanhID") %>'
                                                CommandName="Delete" OnClientClick="return confirm('<%=Resources.Language.XacNhanXoaThongTinNay%>');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>

                                  

                                  
                                </Columns>
                            </asp:GridView>
                                                         </ContentTemplate>
                    </asp:UpdatePanel>



                               </td>
                                </tr>
                                </table>
                                </div></div>
                            </div>    
       
       
        <div id="tab3">
                           <div class="widgetbox">
                                <h4 class="widgettitle">
                                <asp:Label ID="Label2" runat="server" Text="Kiểm toán viên"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4><div class="widgetcontent">
                                <table id="Table6" width="100%" border="0" class="formtbl" runat="server">
                                    <tr><td colspan="4">
                                        <asp:HiddenField ID="lstIdHVCN" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="lstIdNQT" runat="server" ClientIDMode="Static" />
                                        <asp:DropDownList ID="drHoiVienTapThe_Chuyen" Width="75%" ClientIDMode="Static" Height="30px" runat="server" DataTextField="TenDoanhNghiep" DataValueField="HoiVienTapTheID"></asp:DropDownList>&nbsp;
                        <a class="btn btn-rounded"><i class="iconfa-plus-sign"></i><asp:Button ID="btnChuyen" runat="server" onclick="btnChuyen_Click" OnClientClick="return checkds();" Text="Chuyển đơn vị công tác" /></a>
                                        </td></tr>
                                   <tr class="trbgr">
                                       <td colspan="4">
                    <h6>
                      Danh sách hội viên cá nhân, kiểm toán viên</h6></td></tr><tr>
                                <td>
                            
  
                            <asp:GridView ID="gvHoiVien" ClientIDMode="Static" runat="server" AutoGenerateColumns="false" 
                                Width="100%" CssClass="display"  >
                                <Columns>
                                    <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
                                            ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <input type="checkbox" class="colcheckbox" value='<%# Eval("HoiVienCaNhanID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    <asp:BoundField DataField="STT" HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                      <asp:BoundField DataField="MaHoiVienCaNhan" HeaderText="Mã" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="Ten" HeaderText="Họ và Tên" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="NgaySinh" HeaderText="Ngày sinh" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="GioiTinh" HeaderText="Giới tính" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="HoiVien" HeaderText="Hội viên" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="NgayGiaNhap" HeaderText="Ngày kết nạp" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                  <asp:BoundField DataField="SoChungChiKTV" HeaderText="Số CC KTV" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                  <asp:BoundField DataField="NgayCapChungChiKTV" HeaderText="Ngày cấp CC KTV" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 
                                </Columns>
                            </asp:GridView>
                        

                               </td>
                                </tr>
                             
                              <tr class="trbgr">
                                       <td colspan="4">
                    <h6>
                    Danh sách người quan tâm </h6></td></tr><tr>
                                <td>
                            
     <asp:GridView ID="gvNguoiQuanTam" runat="server" ClientIDMode="Static" AutoGenerateColumns="false" 
                                Width="100%" CssClass="display"  >
                                <Columns>
                                    <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
                                            ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <input type="checkbox" class="colcheckbox" value='<%# Eval("HoiVienCaNhanID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                  <asp:BoundField DataField="STT" HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                      <asp:BoundField DataField="MaHoiVienCaNhan" HeaderText="Mã" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="Ten" HeaderText="Họ và Tên" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="NgaySinh" HeaderText="Ngày sinh" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="GioiTinh" HeaderText="Giới tính" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="HoiVien" HeaderText="Hội viên" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="NgayGiaNhap" HeaderText="Ngày kết nạp" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                  <asp:BoundField DataField="SoChungChiKTV" HeaderText="Số CC KTV" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                  <asp:BoundField DataField="NgayCapChungChiKTV" HeaderText="Ngày cấp CC KTV" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 
                                </Columns>
                            </asp:GridView>
                        

                               </td>
                                </tr>
                                </table>
                                </div></div>
                            </div>    
       
       <div id="tab4">
                           <div class="widgetbox">
                                <h4 class="widgettitle">
                                <asp:Label ID="Label3" runat="server" Text="Đào tạo, cập nhật kiến thức"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4><div class="widgetcontent">
                  <table id="Table7" width="100%" border="0" class="formtbl" runat="server">
                     
                             
                  
                  <tr class="formRowTable">
                    <td style=" width: 200px; " ><label>Tên công ty</label></td><td>
                       <div class="formRight">
                       <asp:TextBox ID="txtTenCongTy" runat="server"  Width="100%" ></asp:TextBox></div></td></tr><tr class="formRowTable"> 
                    <td><label>Số giờ còn thiếu<a href="#" title="" style="margin: 5px;" onclick="open_sogioconthieu(<%=Request.QueryString["id"] %>)"> <img src="/images/icons/color/add.png" alt="" /> </a></label></td><td  >
                   
                    </td>
                    </tr>
                      
                      <tr>
              <td colspan="4">
                         
                                   <asp:GridView ID="gvDaoTao" runat="server" AutoGenerateColumns="false" 
                                Width="100%" CssClass="display"  >
                                <Columns>
                               <asp:BoundField DataField="STT" HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="MaLopHoc" HeaderText="Mã lớp học" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="TenLopHoc" HeaderText="Tên lớp học" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="TuNgay" HeaderText="Từ ngày" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="DenNgay" HeaderText="Đến ngày" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                              <asp:TemplateField HeaderText='Chi tiết' ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                        <a onclick="open_chitiet(<%=Request.QueryString["id"] %> ,<%# Eval("LopHocID")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Chi tiết"  rel="tooltip" class="btn"><i class="iconsweets-create" ></i></a>
                     
                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>   
                              
                              
                              <asp:BoundField DataField="LopHocID"   Visible="false"  />
                         </Columns>
                        </asp:GridView>
          
                               </td>
                                </tr>
                                </table>
                                </div></div>
                            </div>    
       
           
        <div id="tab5">
                           <div class="widgetbox">
                                <h4 class="widgettitle">
                                <asp:Label ID="Label4" runat="server" Text="Kiểm soát chất lượng"></asp:Label><a   class="close">×</a> <a class="minimize">–</a> </h4><div class="widgetcontent">
                                <table id="Table8" width="100%" border="0" class="formtbl" runat="server">
                                <tr>
                                <td colspan="4">
                                    <asp:GridView ID="gvKSCL" runat="server" AutoGenerateColumns="false" 
                                Width="100%" CssClass="display"  >
                                <Columns>
                               <asp:BoundField DataField="STT" HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="TenHoSo" HeaderText="Hồ sơ kiểm toán" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="BGDTen" HeaderText="Thành viên BGĐ" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="KTVTen" HeaderText="Kiểm toán vien" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="Loai1" HeaderText="Loại 1 Tốt" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" />
                               <asp:BoundField DataField="Loai2" HeaderText="Loại 2 Đạt yêu cầu" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px"  />
                               <asp:BoundField DataField="Loai3" HeaderText="Loại 3 Không đạt" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px"  />
                               <asp:BoundField DataField="Loai4" HeaderText="Loại 4 Yếu kém" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px"  />
                                 
                         </Columns>
                        </asp:GridView>
          
  

                               </td>
                                </tr>
                                <tr>
                                 <td colspan="4">
                                   <table width="100%"   >
                    

                  <tr class="formRowTable">
                    <td  colspan="2"  >
                    
                         <label>Tổng số hồ sơ được kiểm toán</label> </td><td colspan="7">
                        <div class="formRight">
                        <asp:TextBox ID="txtTongHoSo" runat="server"  Width="60px"  ></asp:TextBox></div></td></tr><tr class="formRowTable">
                    <td><label>Xếp loại kiểm tra hệ thống</label> </td><td>
                       <label>Loại 1- Tốt </label></td><td>
                      <div class="formRight">
                        <asp:TextBox ID="txtHTLoai1" runat="server"  Width="60px"  ></asp:TextBox></div></td><td>
                       <label>Loại 2- Đạt yêu cầu </label></td><td>
                      <div class="formRight">
                        <asp:TextBox ID="txtHTLoai2" runat="server"     Width="60px"  ></asp:TextBox></div></td><td>
                       <label>Loại 3- Không đạt </label></td><td>
                      <div class="formRight">
                        <asp:TextBox ID="txtHTLoai3" runat="server"    Width="60px"   ></asp:TextBox></div></td><td>
                       <label>Loại 4- Yếu kém </label></td><td>
                      <div class="formRight">
                        <asp:TextBox ID="txtHTLoai4" runat="server"   Width="60px"    ></asp:TextBox></div></td></tr><tr class="formRowTable">
                    <td  ><label>Xếp loại kiểm tra kỹ thuật</label></td><td>
                       <label>Loại 1- Tốt </label></td><td>
                        <asp:TextBox ID="txtKTLoai1" runat="server"   Width="60px"    ></asp:TextBox></td><td>
                       <label>Loại 2- Đạt yêu cầu </label></td><td>
                        <asp:TextBox ID="txtKTLoai2" runat="server"   Width="60px"    ></asp:TextBox></td><td>
                       <label>Loại 3- Không đạt </label></td><td>
                        <asp:TextBox ID="txtKTLoai3" runat="server"    Width="60px"   ></asp:TextBox></td><td>
                       <label>Loại 4- Yếu kém </label></td><td>
                        <asp:TextBox ID="txtKTLoai4" runat="server"   Width="60px"    ></asp:TextBox></td></tr><tr class="formRowTable">
                    <td><label>Xếp loại chung</label> </td><td>
                       <label>Loại 1- Tốt </label></td><td>
                        <asp:TextBox ID="txtChungLoai1" runat="server"  Width="60px"     ></asp:TextBox></td><td>
                       <label>Loại 2- Đạt yêu cầu </label></td><td>
                        <asp:TextBox ID="txtChungLoai2" runat="server"   Width="60px"    ></asp:TextBox></td><td>
                       <label>Loại 3- Không đạt </label></td><td>
                        <asp:TextBox ID="txtChungLoai3" runat="server"    Width="60px"   ></asp:TextBox></td><td>
                       <label>Loại 4- Yếu kém </label></td><td>
                        <asp:TextBox ID="txtChungLoai4" runat="server"    Width="60px"   ></asp:TextBox></td></tr></table></td></tr></table></div></div></div><div id="tab6">
                           <div class="widgetbox">
                                <div class="widgetcontent">
                                
                <asp:UpdatePanel ID="up1" runat="server" ChildrenAsTriggers="true"><ContentTemplate>
         <script type="text/javascript" language="javascript">
             Sys.Application.add_load(reCalljScript);
     </script>
                  <div>Chọn năm&nbsp; <asp:DropDownList ID="drNamThanhToan" runat="server" DataTextField="Nam" DataValueField="Nam" OnSelectedIndexChanged="drNamThanhToan_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
           <table border="0" width="100%" class="formtbl">
                                <tr class="trbgr">
                                       <td>
                    <h6>
                      Bảng thông tin tình trạng nộp phí</h6></td></tr><tr>
                                <td>
              <asp:GridView ID="grvPhiCongTy" runat="server" AutoGenerateColumns="false" 
                                Width="100%" CssClass="display" OnDataBound = "grvPhiCongTy_OnDataBound"
       DataKeyNames = "PhatSinhPhiID, PhiID, LoaiPhi" >
                                <Columns>
                               
                               <asp:TemplateField HeaderText="Tên loại phí">

                  <ItemTemplate>
                      <asp:Label ID = "TenLoaiPhi" name = "TenLoaiPhi" runat = "server" Text = <%# Eval("TenLoaiPhi")%>></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>
                               <asp:TemplateField HeaderText="Số tiền phải nộp"><ItemTemplate>
                               <asp:Label ID = "TongTien" name = "TongTien" runat = "server" Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TongTien"))%>'></asp:Label>
                               </ItemTemplate></asp:TemplateField>
                               
                               <asp:TemplateField HeaderText="Số tiền đã nộp"><ItemTemplate>
                               <asp:Label ID = "TienNop" name = "TienNop" runat = "server" Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TienNop"))%>'></asp:Label>
                               </ItemTemplate></asp:TemplateField>
                               
                               <asp:TemplateField HeaderText="Số tiền chưa nộp"><ItemTemplate>
                               <asp:Label ID = "TienNo" name = "TienNo" runat = "server" Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TienNo"))%>'></asp:Label>
                               </ItemTemplate></asp:TemplateField>
                               
                                 
                         </Columns>
                        </asp:GridView>
                      
           </td></tr>

            <tr class="trbgr">
                                       <td>
                    <h6>
                      Danh sách hội viên</h6></td></tr><tr>
                                <td>
              <asp:GridView ID="thanhtoanphicanhan_grv" runat="server" AutoGenerateColumns="false" 
                                Width="100%" CssClass="display"
       DataKeyNames = "PhatSinhPhiID, PhiID, LoaiPhi, HoiVienId" >
                                <Columns>
                                
                               <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                HeaderText="ID">
                                <ItemTemplate>
                                    
                                     <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:open_user_edit(" + DataBinder.Eval(Container.DataItem, "HoiVienID").ToString()+"); "%>   Text= '<%# Eval("MaHoiVien") %>'  runat="server">
                    
                </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                               
                               <asp:BoundField DataField="HoTen" HeaderText="Họ tên" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="SoChungChiKTV" HeaderText="Số CC KTV" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="NgayCapChungChiKTV" HeaderText="Ngày cấp CC KTV" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                               <asp:BoundField DataField="DienGiai" HeaderText="Diễn giải" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                              <asp:TemplateField HeaderText="Số tiền phải nộp"><ItemTemplate>
                               <asp:Label ID = "TongTien" name = "TongTien" runat = "server" Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TongTien"))%>'></asp:Label>
                               </ItemTemplate></asp:TemplateField>
                               
                               <asp:TemplateField HeaderText="Số tiền đã nộp"><ItemTemplate>
                               <asp:Label ID = "TienNop" name = "TienNop" runat = "server" Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TienNop"))%>'></asp:Label>
                               </ItemTemplate></asp:TemplateField>
                               
                               <asp:TemplateField HeaderText="Số tiền chưa nộp"><ItemTemplate>
                               <asp:Label ID = "TienNo" name = "TienNo" runat = "server" Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("SoPhiConNo"))%>'></asp:Label>
                               </ItemTemplate></asp:TemplateField>
                         </Columns>
                        </asp:GridView>
                      
           </td></tr></table>
            </ContentTemplate></asp:UpdatePanel>

                                </div>
                            </div>  
    <div id="tab7">
                           <div class="widgetbox">
                                <h4 class="widgettitle">
                                <asp:Label ID="Label6" runat="server" Text="Khen thưởng, kỷ luật"></asp:Label><a
            class="close">×</a> <a class="minimize">–</a></h4><div class="widgetcontent">

                  <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
                        <ContentTemplate>  
                           <script type="text/javascript" language="javascript">
                               Sys.Application.add_load(reCalljScript);
         </script>
                                <table id="Table10" width="100%" border="0" class="formtbl" runat="server">
                                
                                 <tr class="trbgr">
                                       <td colspan="4">
                                          <h6>Khen thưởng </h6></td></tr><tr>
                                 <td colspan="4">

                                  

                                         <div style="float: right; margin-top: -30px"><img src="images/icons/color/add.png" style="margin-top: 3px;" />
                                         <asp:Button ID="btnThem" runat="server" CssClass="basic" Text="Thêm"
                                        OnClick="ButtonAddKhenThuong_Click" />
                                        </div>
                                   <asp:GridView ID="gvKhenThuong" runat="server" CssClass="display" EmptyDataText="Chưa có dữ liệu"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" 
                               Width="100%" 
                                OnRowCommand="gvKhenThuong_RowCommand" 
                                OnRowDeleting="gvKhenThuong_RowDeleting" OnRowDataBound="gvKhenThuong_RowDataBound" >
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Ngày tháng" HeaderStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNgayThang" runat="server"  class="date2" 
                                                Text='<%#Bind("NgayThang") %>'></asp:TextBox></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Cấp và hình thức" HeaderStyle-Width="300">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="drHinhThuc"
                                                runat="server" DataTextField="TenHinhThucKhenThuong" DataValueField="HinhThucKhenThuongID"
                                                >
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lý do" HeaderStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLyDo" runat="server" Text='<%#Bind("LyDo") %>'
                                                ></asp:TextBox></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            
                                                <asp:ImageButton ImageUrl="/images/icons/color/cross.png"  ID="btnDelete" runat="server" CommandArgument='<%#Bind("STT") %>'
                                                CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            </Columns>
                            
                        </asp:GridView>
  

                               </td>
                                </tr>
                             
                                
                                 <tr class="trbgr">
                                       <td colspan="4">
                                          <h6>Kỷ luật</h6></td></tr><tr>
                                 <td colspan="4">
                             

                                          <div style="float: right; margin-top: -30px"><img src="images/icons/color/add.png" style="margin-top: 3px;" />
                                         <asp:Button ID="Button4" runat="server" CssClass="basic" Text="Thêm"
                                        OnClick="ButtonAddKyLuat_Click" />
                                        </div>


         <asp:GridView ID="gvKyLuat" runat="server" CssClass="display" EmptyDataText="Chưa có dữ liệu"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" 
                            Width="100%" 
                            OnRowCommand="gvKyLuat_RowCommand" 
                                OnRowDeleting="gvKyLuat_RowDeleting" OnRowDataBound="gvKyLuat_RowDataBound" >
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Ngày tháng" HeaderStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNgayThang" runat="server"  class="date2" 
                                                Text='<%#Bind("NgayThang") %>'></asp:TextBox></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Cấp và hình thức" HeaderStyle-Width="300">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="drHinhThuc"
                                                runat="server" DataTextField="TenHinhThucKyLuat" DataValueField="HinhThucKyLuatID"
                                                >
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lý do" HeaderStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLyDo" runat="server" Text='<%#Bind("LyDo") %>'
                                                ></asp:TextBox></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            
                                                <asp:ImageButton ImageUrl="/images/icons/color/cross.png"  ID="btnDelete" runat="server" CommandArgument='<%#Bind("STT") %>'
                                                CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                               </td>
                                </tr>
                             
                                </table>
                               
                               
                   </ContentTemplate>
     
                    </asp:UpdatePanel>             
                               
                                </div></div>
                            </div>  
   
   
   
   
   
   
   
   
   </div>
     <div id="div_sogioconthieu"></div>
     <div id="div_chitiet"></div>
    </form>



    <script type="text/javascript">
        function open_user_edit(id) {
            var timestamp = Number(new Date());
            
                openInNewTab("admin.aspx?page=hosohoiviencanhan&id=" + id + "&act=edit&time=" + timestamp);
            
        }

        function checkds() {
            var drop = document.getElementById('drHoiVienTapThe_Chuyen');
            var valSelect = drop.options[drop.selectedIndex].value;
            if (valSelect == "0") {
                alert('Bạn chưa chọn đơn vị cần chuyển!');
                return false;
            }
            if ($('#lstIdHVCN').val() == "" && $('#lstIdNQT').val() == "") {
                alert('Bạn chưa chọn hội viên cần chuyển!');
                return false;
            }
            else
                return true;
        }

        function removeItem(array, item) {
            for (var i in array) {
                if (array[i] == item) {
                    array.splice(i, 1);
                    break;
                }
            }
        }

        var gvHoiVien_selected = new Array();
        var gvNguoiQuanTam_selected = new Array();

        jQuery(document).ready(function () {
            // dynamic table      

            $("#gvHoiVien .checkall").bind("click", function () {
                gvHoiVien_selected = new Array();
                checked = $(this).prop("checked");
                $('#gvHoiVien :checkbox').each(function () {
                    $(this).prop("checked", checked);
                    if ((checked) && ($(this).val() != "all")) gvHoiVien_selected.push($(this).val());
                });
                $('#lstIdHVCN').val(gvHoiVien_selected);

            });

            $('#gvHoiVien :checkbox').each(function () {
                $(this).bind("click", function () {
                    removeItem(gvHoiVien_selected, $(this).val())
                    checked = $(this).prop("checked");
                    if ((checked) && ($(this).val() != "all")) gvHoiVien_selected.push($(this).val());
                    $('#lstIdHVCN').val(gvHoiVien_selected);

                });

            });

            $("#gvNguoiQuanTam .checkall").bind("click", function () {
                gvNguoiQuanTam_selected = new Array();
                checked = $(this).prop("checked");
                $('#gvNguoiQuanTam :checkbox').each(function () {
                    $(this).prop("checked", checked);
                    if ((checked) && ($(this).val() != "all")) gvNguoiQuanTam_selected.push($(this).val());
                });
                $('#lstIdNQT').val(gvNguoiQuanTam_selected);
            });

            $('#gvNguoiQuanTam :checkbox').each(function () {
                $(this).bind("click", function () {
                    removeItem(gvNguoiQuanTam_selected, $(this).val())
                    checked = $(this).prop("checked");
                    if ((checked) && ($(this).val() != "all")) gvNguoiQuanTam_selected.push($(this).val());
                    $('#lstIdNQT').val(gvNguoiQuanTam_selected);
                });

            });


        });

        $.datepicker.setDefaults($.datepicker.regional['vi']);

        $(function () {
            $("#<%=txtNgayThanhLap.ClientID%> , #<%=txtNgayCapGiayChungNhanDKKD.ClientID%> ,#<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%> ,#<%=txtNguoiDaiDienPL_NgayCapChungChiKTV.ClientID%> ,#<%=txtNguoiDaiDienLL_NgaySinh.ClientID%> ,#<%=txtNguoiDaiDienPL_NgaySinh.ClientID%> ,#<%=txtNgayQuyetDinh.ClientID%>").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy"
            }).datepicker("option", "maxDate", '+0m +0w +0d');

            $("#<%=txtNgayThanhLap.ClientID%> , #<%=txtNgayCapGiayChungNhanDKKD.ClientID%> ,#<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%> ,#<%=txtNguoiDaiDienPL_NgayCapChungChiKTV.ClientID%> ,#<%=txtNguoiDaiDienLL_NgaySinh.ClientID%> ,#<%=txtNguoiDaiDienPL_NgaySinh.ClientID%> ,#<%=txtNgayQuyetDinh.ClientID%>").mask("99/99/9999");
        });
        // txtNgayThanhLap txtNgayCapGiayChungNhanDKKD    txtNgayCapGiayChungNhanKDDVKT  txtNguoiDaiDienPL_NgayCapChungChiKTV  txtNguoiDaiDienLL_NgaySinh


        function open_sogioconthieu(id) {

           
            $("#div_sogioconthieu").empty();
            $("#div_sogioconthieu").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hoivientapthe_daotao_giohoc&mode=iframe&id=" + id));
            $("#div_sogioconthieu").dialog({
                resizable: true,
                width: 1024,
                height: 640,
                title: "<img src='images/icons/user_business.png'>&nbsp;<b>Số giờ còn thiếu</b>",
                modal: true,

                buttons: {

                    "Đóng": function () {
                        $(this).dialog("close");
                    }
                }

            });

            $('#div_sogioconthieu').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
        }

        function open_chitiet(id, lophocid) {


            $("#div_chitiet").empty();
            $("#div_chitiet").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hoivientapthe_daotao_lophoc&mode=iframe&LopHocID=" + lophocid + "&id=" + id));
            $("#div_chitiet").dialog({
                resizable: true,
                width: 1024,
                height: 640,
                title: "<img src='images/icons/user_business.png'>&nbsp;<b>Chi tiết lớp học</b>",
                modal: true,

                buttons: {

                    "Đóng": function () {
                        $(this).dialog("close");
                    }
                }

            });

            $('#div_chitiet').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
        }

        </script>