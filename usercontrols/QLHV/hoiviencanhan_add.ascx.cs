﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using System.Text.RegularExpressions;

public partial class usercontrols_hoiviencanhan_add : System.Web.UI.UserControl
{
    String id = String.Empty;
    public Commons cm = new Commons();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    clsXacDinhQuyen quyen = new clsXacDinhQuyen();
    protected DonHoiVienFile FileDinhKem1 = new DonHoiVienFile();
    protected HoiVienFile FileDinhKem2 = new HoiVienFile();
    public bool QuyenXem { get; set; }
    public bool QuyenThem { get; set; }
    public bool QuyenSua { get; set; }
    public bool QuyenXoa { get; set; }
    public bool QuyenDuyet { get; set; }
    public bool QuyenTraLai { get; set; }
    public bool QuyenHuy { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Tên chức năng
            string tenchucnang = "Đăng ký kết nạp hội viên cá nhân";
            if (Request.QueryString["ktv"] == "1")
            {
                lbTitle.Text = "Đăng ký kết nạp kiểm toán viên";
                tenchucnang = "Đăng ký kết nạp kiểm toán viên";
            }

            // Icon CSS Class  
            string IconClass = "iconfa-credit-card";

            if (!string.IsNullOrEmpty(cm.Admin_NguoiDungID))
            {
                string maquyen = quyen.fcnXDQuyen(int.Parse(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr);

                QuyenXem = (maquyen.Contains("XEM")) ? true : false;
                QuyenThem = (maquyen.Contains("THEM")) ? true : false;
                QuyenSua = (maquyen.Contains("SUA")) ? true : false;
                QuyenXoa = (maquyen.Contains("XOA")) ? true : false;
                QuyenDuyet = (maquyen.Contains("DUYET")) ? true : false;
                QuyenTraLai = (maquyen.Contains("TRALAI")) ? true : false;
                QuyenHuy = (maquyen.Contains("HUY")) ? true : false;
            }

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"
     <li>Biểu mẫu tờ khai <span class=""separator""></span></li> <!-- Nhóm chức năng cấp trên -->
     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            if (!IsPostBack)
            {
                
                SetInitialRow();
                SetInitialRow_ChungChi();

                LoadDropDown();
                loaduploadFileDinhKem(0);

                if (Request.QueryString["thongbao"] == "1")
                {
                    string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Lưu thông tin thành công!</div>";

                    placeMessage.Controls.Add(new LiteralControl(thongbao));
                }
            }



        }
        catch (Exception ex)
        {
            placeMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }

    }

    public void LoadDropDown()
    {

        DataTable dtb = new DataTable();

        string sql = "";

        // Đơn vị
        sql = "SELECT 0 AS HoiVienTapTheID, N'<< Khác >>' AS TenDoanhNghiep, '-' AS SoHieu UNION ALL (SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE '--- ' + ISNULL(A.SoHieu,LTRIM(RTRIM(A.MaHoiVienTapThe))) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID) ORDER BY SoHieu";
        SqlCommand cmd = new SqlCommand(sql);

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drDonVi.DataSource = dtb;
        drDonVi.DataBind();

        dtb.Clear();
        // Quốc tịch
        sql = "SELECT * FROM tblDMQuocTich";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drQuocTich.DataSource = dtb;
        drQuocTich.DataBind();

        dtb.Clear();
        // Trường ĐH
        sql = "SELECT 0 AS TruongDaiHocID, N'<< Lựa chọn >>' AS TenTruongDaiHoc UNION ALL (SELECT TruongDaiHocID,TenTruongDaiHoc FROM tblDMTruongDaiHoc)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drTotNghiep.DataSource = dtb;
        drTotNghiep.DataBind();


        dtb.Clear();
        // Chuyên ngành
        sql = "SELECT 0 AS ChuyenNganhDaoTaoID, N'<< Lựa chọn >>' AS TenChuyenNganhDaoTao UNION ALL (SELECT ChuyenNganhDaoTaoID,TenChuyenNganhDaoTao FROM tblDMChuyenNganhDaoTao)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChuyenNganh.DataSource = dtb;
        drChuyenNganh.DataBind();

        dtb.Clear();
        // Học vị
        sql = "SELECT 0 AS HocViID, N'<< Lựa chọn >>' AS TenHocVi UNION ALL (SELECT HocViID, TenHocVi FROM tblDMHocVi)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHocVi.DataSource = dtb;
        drHocVi.DataBind();

        dtb.Clear();
        // Học hàm
        sql = "SELECT 0 AS HocHamID, N'<< Lựa chọn >>' AS TenHocHam UNION ALL (SELECT HocHamID, TenHocHam FROM tblDMHocHam)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drHocHam.DataSource = dtb;
        drHocHam.DataBind();

        dtb.Clear();
        // Nơi cấp CMND
        sql = "SELECT * FROM tblDMTinh";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drNoiCap_CMND.DataSource = dtb;
        drNoiCap_CMND.DataBind();

        dtb.Clear();
        // Ngân hàng
        sql = "SELECT 0 AS NganHangID, N'<< Lựa chọn >>' AS TenNganHang UNION ALL (SELECT NganHangID, TenNganHang FROM tblDMNganHang)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drNganHang.DataSource = dtb;
        drNganHang.DataBind();

        dtb.Clear();
        // Load chức vụ
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChucVu.DataSource = dtb;
        drChucVu.DataBind();

        dtb.Clear();
        // Load sở thích
        sql = "SELECT 0 AS SoThichID, N'<< Lựa chọn >>' AS TenSoThich UNION ALL (SELECT SoThichID, TenSoThich FROM tblDMSoThich)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drSoThich.DataSource = dtb;
        drSoThich.DataBind();

    }

    protected void loaduploadFileDinhKem(int donHoiVienId)
    {
        try
        {

            string DoiTuong = (Request.QueryString["ktv"] == "1") ? "2" : "1";
            SqlCommand sql = new SqlCommand();

            if (donHoiVienId == 0)
            {
                sql.CommandText = " SELECT  LoaiGiayToID ,TenFile as TenBieuMau ,DoiTuong ,BatBuoc  , 0 as FileID  , ' ' as TenFileDinhKem " +
                                  " FROM tblDMLoaiGiayTo  " +
                                  " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuong + "' order by tblDMLoaiGiayTo.TenFile   ";
            }
            else
            {
                sql.CommandText =
                "    SELECT   LoaiGiayToID ,TenFile as TenBieuMau  ,DoiTuong  ,BatBuoc , " +
                              "   ( SELECT TOP 1 ISNULL ( tblDonHoiVienFile.FileID,0)  FROM  tblDonHoiVienFile      WHERE  tblDonHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblDonHoiVienFile.DonHoiVienCaNhanID= " + donHoiVienId + "  ) as FileID ," +
                               " ( SELECT  TOP 1 ISNULL(tblDonHoiVienFile.TenFile,' ')   FROM  tblDonHoiVienFile  WHERE  tblDonHoiVienFile.LoaiGiayToID=  tblDMLoaiGiayTo.LoaiGiayToID AND  tblDonHoiVienFile.DonHoiVienCaNhanID= " + donHoiVienId + " ) as  TenFileDinhKem    " +
                " FROM tblDMLoaiGiayTo  " +
                " where  tblDMLoaiGiayTo.DoiTuong ='" + DoiTuong + "' order by tblDMLoaiGiayTo.TenFile  ";


            }

            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;


            PagedDataSource objPds1 = new PagedDataSource();
            objPds1.DataSource = ds.Tables[0].DefaultView;

            FileDinhKem_grv.DataSource = objPds1;
            FileDinhKem_grv.DataBind();

            sql = null;
            ds = null;
            dt = null;

            // set validate

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                String sTenLoaiFile = row.Cells[0].Text.Trim();
                String sLoaiFileID = row.Cells[3].Text.Trim();
                String sFileDinhKemID = row.Cells[4].Text.Trim();
                String sBatbuoc = row.Cells[5].Text.Trim();

                String sTenFile = row.Cells[6].Text.Trim();

                // check validate
                if (sBatbuoc == "0" || !String.IsNullOrEmpty(sTenFile))
                {
                    //((RequiredFieldValidator)row.FindControl("RequiredFieldFileUpload")).Visible = false;


                }

                // an hien chu thich bat buoc
                if (sBatbuoc == "0")
                {
                    ((Label)row.FindControl("requred")).Visible = false;
                    //((Label)row.FindControl("requred")).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                }
                else
                {
                    //((Label)row.FindControl("requred")).Text = "(*) ";
                }

                //// an hien link bieu mau

                //if (String.IsNullOrEmpty(sBieuMau))
                //{
                //    //  ((HyperLink)row.FindControl("linkBieuMau")).Visible = false;
                //    HyperLink likBM = ((HyperLink)row.FindControl("linkBieuMau"));// 

                //    likBM.Text = "&nbsp; &nbsp; &nbsp;";
                //    likBM.NavigateUrl = "";

                //}

            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }




    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid();
    }

    protected void ButtonAddChungChi_Click(object sender, EventArgs e)
    {

        AddNewRowToGrid_ChungChi();
    }

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        dt.Columns.Add(new DataColumn("ThoiGian", typeof(string)));
        dt.Columns.Add(new DataColumn("ChucVu", typeof(string)));
        dt.Columns.Add(new DataColumn("NoiLamViec", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        dr["ThoiGian"] = "";
        dr["ChucVu"] = "";
        dr["NoiLamViec"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["QuaTrinh"] = dt;

        gvQuaTrinh.DataSource = dt;
        gvQuaTrinh.DataBind();
    }
    private void AddNewRowToGrid()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["QuaTrinh"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["QuaTrinh"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");

                        TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                        TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                        dtCurrentTable.Rows[i - 1]["ThoiGian"] = thoigian.Text;
                        dtCurrentTable.Rows[i - 1]["ChucVu"] = chucvu.Text;
                        dtCurrentTable.Rows[i - 1]["NoiLamViec"] = noilamviec.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    drCurrentRow["ThoiGian"] = "";
                    drCurrentRow["ChucVu"] = "";
                    drCurrentRow["NoiLamViec"] = "";
                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    dr["ThoiGian"] = "";
                    dr["ChucVu"] = "";
                    dr["NoiLamViec"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["QuaTrinh"] = dtCurrentTable;

                gvQuaTrinh.DataSource = dtCurrentTable;
                gvQuaTrinh.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["QuaTrinh"] != null)
        {
            DataTable dt = (DataTable)ViewState["QuaTrinh"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                    thoigian.Text = dt.Rows[i]["ThoiGian"].ToString();
                    chucvu.Text = dt.Rows[i]["ChucVu"].ToString();
                    noilamviec.Text = dt.Rows[i]["NoiLamViec"].ToString();
                    rowIndex++;
                }
            }
        }
    }


    private void SetInitialRow_ChungChi()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("STT", typeof(string)));
        //dt.Columns.Add(new DataColumn("ChungChiID", typeof(string)));
        dt.Columns.Add(new DataColumn("SoChungChi", typeof(string)));
        dt.Columns.Add(new DataColumn("NgayCap", typeof(string)));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dt.Columns["STT"];
        dt.PrimaryKey = columns;



        dr = dt.NewRow();
        dr["STT"] = 1;
        //dr["ChungChiID"] = "0";
        dr["SoChungChi"] = "";
        dr["NgayCap"] = "";

        dt.Rows.Add(dr);

        //Store the DataTable in ViewState
        ViewState["ChungChi"] = dt;

        gvChungChi.DataSource = dt;
        gvChungChi.DataBind();
    }
    private void AddNewRowToGrid_ChungChi()
    {
        try
        {
            int rowIndex = 0;

            if (ViewState["ChungChi"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["ChungChi"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        //DropDownList drChungChi = (DropDownList)gvChungChi.Rows[rowIndex].Cells[1].FindControl("drChungChi");
                        TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                        TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");

                        //dtCurrentTable.Rows[i - 1]["ChungChiID"] = drChungChi.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["SoChungChi"] = sochungchi.Text;
                        dtCurrentTable.Rows[i - 1]["NgayCap"] = ngaycap.Text;

                        rowIndex++;
                    }

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["STT"] = dtCurrentTable.Rows.Count + 1;
                    //drCurrentRow["ChungChiID"] = "0";
                    drCurrentRow["SoChungChi"] = "";
                    drCurrentRow["NgayCap"] = "";

                    dtCurrentTable.Rows.Add(drCurrentRow);
                }
                else
                {
                    DataRow dr = null;
                    dr = dtCurrentTable.NewRow();
                    dr["STT"] = 1;
                    //dr["ChungChiID"] = "0";
                    dr["SoChungChi"] = "";
                    dr["NgayCap"] = "";

                    dtCurrentTable.Rows.Add(dr);
                }

                ViewState["ChungChi"] = dtCurrentTable;

                gvChungChi.DataSource = dtCurrentTable;
                gvChungChi.DataBind();
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData_ChungChi();
        }
        catch (Exception ex)
        {
            //ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
    private void SetPreviousData_ChungChi()
    {
        int rowIndex = 0;
        if (ViewState["ChungChi"] != null)
        {
            DataTable dt = (DataTable)ViewState["ChungChi"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //DropDownList drChungChi = (DropDownList)gvChungChi.Rows[rowIndex].Cells[1].FindControl("drChungChi");
                    TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtDate");

                    //drChungChi.SelectedValue = dt.Rows[i]["ChungChiID"].ToString();
                    sochungchi.Text = dt.Rows[i]["SoChungChi"].ToString();
                    ngaycap.Text = dt.Rows[i]["NgayCap"].ToString();

                    rowIndex++;
                }
            }
        }
    }

    protected void gvQuaTrinh_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvQuaTrinh_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["QuaTrinh"] != null)
        {
            DataTable dt = (DataTable)ViewState["QuaTrinh"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[2].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[rowIndex].Cells[3].FindControl("txtNoiLamViec");

                    dt.Rows[i - 1]["ThoiGian"] = thoigian.Text;

                    dt.Rows[i - 1]["ChucVu"] = chucvu.Text;
                    dt.Rows[i - 1]["NoiLamViec"] = noilamviec.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["QuaTrinh"] = dt;

            gvQuaTrinh.DataSource = dt;
            gvQuaTrinh.DataBind();

            SetPreviousData();
        }
    }


    protected void gvChungChi_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            id = Convert.ToString(e.CommandArgument);
        }
    }
    protected void gvChungChi_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int rowIndex = 0;

        if (ViewState["ChungChi"] != null)
        {
            DataTable dt = (DataTable)ViewState["ChungChi"];

            if (dt.Rows.Count > 0)
            {
                for (int i = 1; i <= dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    DropDownList drChungChi = (DropDownList)gvChungChi.Rows[rowIndex].Cells[1].FindControl("drChungChi");
                    TextBox sochungchi = (TextBox)gvChungChi.Rows[rowIndex].Cells[2].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[rowIndex].Cells[3].FindControl("txtDate");

                    dt.Rows[i - 1]["ChungChiID"] = drChungChi.SelectedValue;
                    dt.Rows[i - 1]["SoChungChi"] = sochungchi.Text;
                    dt.Rows[i - 1]["NgayCap"] = ngaycap.Text;

                    rowIndex++;
                }
            }

            DataRow row_del = dt.Rows.Find(id);

            dt.Rows.Remove(row_del);
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["STT"] = i + 1;
            }

            ViewState["ChungChi"] = dt;

            gvChungChi.DataSource = dt;
            gvChungChi.DataBind();

            SetPreviousData_ChungChi();
        }
    }

    protected void gvChungChi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //try
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        DropDownList drChungChi = (DropDownList)e.Row.FindControl("drChungChi");

        //        DataSet ds = new DataSet();
        //        DataTable dtb = new DataTable();

        //        string sql = "";

        //        sql = "SELECT 0 AS ChungChiID, N'<< Lựa chọn >>' AS TenChungChi UNION ALL (SELECT ChungChiID, TenChungChi FROM tblDMChungChi)";
        //      SqlCommand cmd = new SqlCommand(sql);
        //      dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        //        drChungChi.DataSource = dtb;
        //        drChungChi.DataBind();

        //    }
        //}
        //catch (Exception ex)
        //{
        //}
    }

    public string SinhSoDon()
    {
        string result = "";

        string sql = "SELECT dbo.LaySoChayDonHoiVienCaNhan('" + DateTime.Now.Year.ToString().Substring(2, 2) + "')";
        SqlCommand cmd = new SqlCommand(sql);
        string so = DataAccess.DLookup(cmd);

        result = "DKHVCN" + DateTime.Now.Year.ToString().Substring(2, 2) + so;

        return result;
    }

    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        try
        {

            string thongbao = "";

            string sql = "";
            sql = "SELECT TenDangNhap FROM tblHoiVienCaNhan A INNER JOIN tblNguoiDung B ON A.MaHoiVienCaNhan = B.TenDangNhap WHERE (B.Email = '" + txtEmail.Text + "' OR A.SoChungChiKTV = '" + txtSoChungChiKTV.Text + "')";
            SqlCommand cmd = new SqlCommand(sql);

            DataTable dtb = new DataTable();

            dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
            if (dtb.Rows.Count != 0)
            {
                thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>
							<p>" + @"Đã có tài khoản trong chương trình phần mềm quản lý hội viên với tên đăng nhập <b>" + dtb.Rows[0]["TenDangNhap"].ToString() + @"</b></p>							
						</div>";
                placeMessage.Controls.Add(new LiteralControl(thongbao));
                return;
            }

            if (Request.QueryString["ktv"] == "1")
            {
                SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
                SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connStr, true, "");
                SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connStr, true, "");
                SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
                SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connStr, true, "");


                string gioitinh = (rdNam.Checked) ? "1" : "0";
                string HoiVienID = SinhID(txtHoDem.Text, txtTen.Text, txtSoChungChiKTV.Text);
                // insert vào bảng tblDonHoiVienCaNhan
                HoiVienCaNhan hoivien = new HoiVienCaNhan();
                hoivien.MaHoiVienCaNhan = HoiVienID;
                hoivien.LoaiHoiVienCaNhan = "2";
                if (chkHoiVienLienKet.Checked)
                    hoivien.LoaiHoiVienCaNhanChiTiet = 1;
                else
                    hoivien.LoaiHoiVienCaNhanChiTiet = 0;
                hoivien.HoDem = txtHoDem.Text;
                hoivien.Ten = txtTen.Text;
                hoivien.GioiTinh = gioitinh;
                if (!string.IsNullOrEmpty(NgaySinh.Value))
                    hoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
                hoivien.QuocTichId = int.Parse(drQuocTich.SelectedValue);

                if (!string.IsNullOrEmpty(Request.Form["TinhID_QueQuan"]))
                    hoivien.QueQuanTinhId = Request.Form["TinhID_QueQuan"].ToString();
                if (!string.IsNullOrEmpty(Request.Form["HuyenID_QueQuan"]))
                    hoivien.QueQuanHuyenId = Request.Form["HuyenID_QueQuan"].ToString();
                if (!string.IsNullOrEmpty(Request.Form["XaID_QueQuan"]))
                    hoivien.QueQuanXaId = Request.Form["XaID_QueQuan"].ToString();

                hoivien.DiaChi = txtDiaChi.Text;
                if (!string.IsNullOrEmpty(Request.Form["TinhID_DiaChi"]))
                    hoivien.DiaChiTinhId = Request.Form["TinhID_DiaChi"].ToString();
                if (!string.IsNullOrEmpty(Request.Form["HuyenID_DiaChi"]))
                    hoivien.DiaChiHuyenId = Request.Form["HuyenID_DiaChi"].ToString();
                if (!string.IsNullOrEmpty(Request.Form["XaID_DiaChi"]))
                    hoivien.DiaChiXaId = Request.Form["XaID_DiaChi"].ToString();

                hoivien.SoGiayChungNhanDkhn = txtSoGiayDHKN.Text;
                if (!string.IsNullOrEmpty(Request.Form["NgayCap_DHKN"]))
                    hoivien.NgayCapGiayChungNhanDkhn = DateTime.ParseExact(Request.Form["NgayCap_DHKN"], "dd/MM/yyyy", null);
                if (!string.IsNullOrEmpty(Request.Form["HanCapTu"]))
                    hoivien.HanCapTu = DateTime.ParseExact(Request.Form["HanCapTu"], "dd/MM/yyyy", null);
                if (!string.IsNullOrEmpty(Request.Form["HanCapDen"]))
                    hoivien.HanCapDen = DateTime.ParseExact(Request.Form["HanCapDen"], "dd/MM/yyyy", null);

                hoivien.SoChungChiKtv = txtSoChungChiKTV.Text;
                if (!string.IsNullOrEmpty(NgayCap_ChungChiKTV.Value))
                    hoivien.NgayCapChungChiKtv = DateTime.ParseExact(NgayCap_ChungChiKTV.Value, "dd/MM/yyyy", null);

                hoivien.Email = txtEmail.Text;
                hoivien.DienThoai = txtDienThoai.Text;
                hoivien.Mobile = txtMobile.Text;
                if (drChucVu.SelectedValue != "0")
                    hoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
                if (drTotNghiep.SelectedValue != "0")
                    hoivien.TruongDaiHocId = int.Parse(drTotNghiep.SelectedValue);
                if (drChuyenNganh.SelectedValue != "0")
                    hoivien.ChuyenNganhDaoTaoId = int.Parse(drChuyenNganh.SelectedValue);
                hoivien.ChuyenNganhNam = txtNam_ChuyenNganh.Text;
                if (drHocVi.SelectedValue != "0")
                    hoivien.HocViId = int.Parse(drHocVi.SelectedValue);
                hoivien.HocViNam = txtNam_HocVi.Text;
                if (drHocHam.SelectedValue != "0")
                    hoivien.HocHamId = int.Parse(drHocHam.SelectedValue);
                hoivien.HocHamNam = txtNam_HocHam.Text;
                hoivien.SoCmnd = txtSoCMND.Text;
                if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
                    hoivien.CmndNgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);
                hoivien.CmndTinhId = int.Parse(drNoiCap_CMND.SelectedValue);
                hoivien.SoTaiKhoanNganHang = txtSoTKNganHang.Text;
                if (drNganHang.SelectedValue != "0")
                    hoivien.NganHangId = int.Parse(drNganHang.SelectedValue);
                if (drDonVi.SelectedValue != "0")
                    hoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
                else
                    hoivien.DonViCongTac = txtDonViCongTac.Text;
                if (drSoThich.SelectedValue != "0")
                    hoivien.SoThichId = int.Parse(drSoThich.SelectedValue);

                HoiVienCaNhan_provider.Insert(hoivien);

                // insert chứng chỉ
                for (int i = 0; i < gvChungChi.Rows.Count; i++)
                {
                    //DropDownList drChungChi = (DropDownList)gvChungChi.Rows[i].Cells[1].FindControl("drChungChi");
                    TextBox sochungchi = (TextBox)gvChungChi.Rows[i].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[i].Cells[2].FindControl("txtDate");

                    if (!string.IsNullOrEmpty(sochungchi.Text))
                    {
                        HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                        chungchi.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;

                        chungchi.SoChungChi = sochungchi.Text;
                        if (!string.IsNullOrEmpty(ngaycap.Text))
                            chungchi.NgayCap = DateTime.ParseExact(ngaycap.Text, "dd/MM/yyyy", null);

                        HoiVienCaNhanChungChi_provider.Insert(chungchi);
                    }
                }

                // insert quá trình làm việc
                for (int i = 0; i < gvQuaTrinh.Rows.Count; i++)
                {
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[i].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[i].Cells[3].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[i].Cells[4].FindControl("txtNoiLamViec");

                    HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
                    quatrinh.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
                    quatrinh.ThoiGianCongTac = thoigian.Text;
                    quatrinh.ChucVu = chucvu.Text;
                    quatrinh.NoiLamViec = noilamviec.Text;

                    HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
                }

                // file đính kèm
                CapnhatFile2(hoivien.HoiVienCaNhanId);

                // insert vào bảng tblNguoiDung
                NguoiDung new_user = new NguoiDung();
                new_user.HoVaTen = txtHoDem.Text + " " + txtTen.Text;
                if (!string.IsNullOrEmpty(NgaySinh.Value))
                    new_user.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);

                new_user.GioiTinh = gioitinh;
                new_user.Email = txtEmail.Text;
                new_user.DienThoai = txtDienThoai.Text;
                new_user.SoCmnd = txtSoCMND.Text;
                if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
                    new_user.NgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);

                new_user.TenDangNhap = HoiVienID;
                new_user.LoaiHoiVien = "2";
                new_user.HoiVienId = hoivien.HoiVienCaNhanId;

                new_user.MatKhau = "123456";
                //Random random = new Random();
                //int randomPassword = random.Next(10000000, 99999999);

                //new_user.MatKhau = randomPassword.ToString();

                NguoiDung_provider.Insert(new_user);

                string body = "";
                body += "Thân chào Anh/chị <span style=\"color:red\">" + new_user.HoVaTen + "</span><BR/>";
                body += "Ban quản trị xin thông báo tài khoản đăng nhập phần mềm quản lý Hội viên VACPA như sau:<BR/>";
                body += "------ID đăng nhập------<BR/>";
                body += "Tên đăng nhập: " + HoiVienID + "<BR/>";
                body += "Mật khẩu: 123456<BR/>";
                body += "<span style=\"text-decoration:underline;font-weight:bold\">Lưu ý:</span><BR/>";
                body += " - Link đăng nhập phần mềm quản lý hội viên: http://vacpa.org.vn/Page/Login.aspx" + "<BR/>";
                body += " - Link xin cấp lại mật khẩu: http://vacpa.org.vn/Page/GetPassword.aspx" + "<BR/>";
                body += " - Ngay sau khi đăng nhập Anh/Chị hãy đổi mật khẩu truy cập." + "<BR/>";
                body += " - Để đảm bảo được nhận các thông báo thường xuyên với VACPA, Anh/Chị nhớ cập nhật những thông tin về sự thay đổi công việc, đơn vị công tác, số điện thoại di động, email, nơi thường trú, v.v… qua profile cá nhân." + "<BR/>";
                
                body += "━━━━━━━━━━━━━━━<BR/>";
                body += "Trân trọng,<BR/>";
                body += "Hội kiểm toán viên hành nghề Việt Nam (VACPA)<BR/>";
                body += "Địa chỉ: Phòng 304, Nhà Dự án, Số 4, Ngõ Hàng Chuối I, Hà Nội<BR/>";
                body += "Email: quantriweb@vacpa.org.vn";
                cm.GuiThongBao(hoivien.HoiVienCaNhanId.ToString(), "1", "VACPA - Thông tin tài khoản kiểm toán viên tại trang tin điện tử VACPA", body);

                string msg = "";
                if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "VACPA - Thông tin tài khoản kiểm toán viên tại trang tin điện tử VACPA", body, ref msg))
                {
                    placeMessage.Controls.Add(new LiteralControl(thongbao));
                }
                else
                    placeMessage.Controls.Add(new LiteralControl(msg));

                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Thêm mới kiểm toán viên " + hoivien.HoDem + " " + hoivien.Ten);

                Response.Redirect("admin.aspx?page=hoiviencanhan_add&ktv=1&thongbao=1");
            }
            else
            {
                SqlDonHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connStr, true, "");

                SqlDonHoiVienCaNhanChungChiProvider HoiVienCaNhanChungChi_provider = new SqlDonHoiVienCaNhanChungChiProvider(connStr, true, "");
                SqlDonHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlDonHoiVienCaNhanQuaTrinhCongTacProvider(connStr, true, "");
                SqlDonHoiVienFileProvider HoiVienFile_provider = new SqlDonHoiVienFileProvider(connStr, true, "");


                string gioitinh = (rdNam.Checked) ? "1" : "0";

                // insert vào bảng tblDonHoiVienCaNhan
                DonHoiVienCaNhan hoivien = new DonHoiVienCaNhan();
                if (chkHoiVienLienKet.Checked)
                    hoivien.LoaiHoiVienCaNhanChiTiet = 1;
                hoivien.SoDonHoiVienCaNhan = SinhSoDon();

                hoivien.HoDem = txtHoDem.Text;
                hoivien.Ten = txtTen.Text;
                hoivien.GioiTinh = gioitinh;
                if (!string.IsNullOrEmpty(NgaySinh.Value))
                    hoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
                hoivien.QuocTichId = int.Parse(drQuocTich.SelectedValue);

                if (!string.IsNullOrEmpty(Request.Form["TinhID_QueQuan"]))
                    hoivien.QueQuanTinhId = Request.Form["TinhID_QueQuan"].ToString();
                if (!string.IsNullOrEmpty(Request.Form["HuyenID_QueQuan"]))
                    hoivien.QueQuanHuyenId = Request.Form["HuyenID_QueQuan"].ToString();
                if (!string.IsNullOrEmpty(Request.Form["XaID_QueQuan"]))
                    hoivien.QueQuanXaId = Request.Form["XaID_QueQuan"].ToString();

                hoivien.DiaChi = txtDiaChi.Text;
                if (!string.IsNullOrEmpty(Request.Form["TinhID_DiaChi"]))
                    hoivien.DiaChiTinhId = Request.Form["TinhID_DiaChi"].ToString();
                if (!string.IsNullOrEmpty(Request.Form["HuyenID_DiaChi"]))
                    hoivien.DiaChiHuyenId = Request.Form["HuyenID_DiaChi"].ToString();
                if (!string.IsNullOrEmpty(Request.Form["XaID_DiaChi"]))
                    hoivien.DiaChiXaId = Request.Form["XaID_DiaChi"].ToString();

                hoivien.SoGiayChungNhanDkhn = txtSoGiayDHKN.Text;
                if (!string.IsNullOrEmpty(Request.Form["NgayCap_DHKN"]))
                    hoivien.NgayCapGiayChungNhanDkhn = DateTime.ParseExact(Request.Form["NgayCap_DHKN"], "dd/MM/yyyy", null);
                if (!string.IsNullOrEmpty(Request.Form["HanCapTu"]))
                    hoivien.HanCapTu = DateTime.ParseExact(Request.Form["HanCapTu"], "dd/MM/yyyy", null);
                if (!string.IsNullOrEmpty(Request.Form["HanCapDen"]))
                    hoivien.HanCapDen = DateTime.ParseExact(Request.Form["HanCapDen"], "dd/MM/yyyy", null);

                hoivien.SoChungChiKtv = txtSoChungChiKTV.Text;
                hoivien.NgayCapChungChiKtv = DateTime.ParseExact(NgayCap_ChungChiKTV.Value, "dd/MM/yyyy", null);

                hoivien.Email = txtEmail.Text;
                hoivien.DienThoai = txtDienThoai.Text;
                hoivien.Mobile = txtMobile.Text;
                if (drChucVu.SelectedValue != "0")
                    hoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
                if (drTotNghiep.SelectedValue != "0")
                    hoivien.TruongDaiHocId = int.Parse(drTotNghiep.SelectedValue);
                if (drChuyenNganh.SelectedValue != "0")
                    hoivien.ChuyenNganhDaoTaoId = int.Parse(drChuyenNganh.SelectedValue);
                hoivien.ChuyenNganhNam = txtNam_ChuyenNganh.Text;
                if (drHocVi.SelectedValue != "0")
                    hoivien.HocViId = int.Parse(drHocVi.SelectedValue);
                hoivien.HocViNam = txtNam_HocVi.Text;
                if (drHocHam.SelectedValue != "0")
                    hoivien.HocHamId = int.Parse(drHocHam.SelectedValue);
                hoivien.HocHamNam = txtNam_HocHam.Text;
                hoivien.SoCmnd = txtSoCMND.Text;
                if (!string.IsNullOrEmpty(NgayCap_CMND.Value))
                    hoivien.CmndNgayCap = DateTime.ParseExact(NgayCap_CMND.Value, "dd/MM/yyyy", null);
                hoivien.CmndTinhId = int.Parse(drNoiCap_CMND.SelectedValue);
                hoivien.SoTaiKhoanNganHang = txtSoTKNganHang.Text;
                if (drNganHang.SelectedValue != "0")
                    hoivien.NganHangId = int.Parse(drNganHang.SelectedValue);
                if (drDonVi.SelectedValue != "0")
                    hoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
                else
                    hoivien.DonViCongTac = txtDonViCongTac.Text;
                if (drSoThich.SelectedValue != "0")
                    hoivien.SoThichId = int.Parse(drSoThich.SelectedValue);

                hoivien.HinhThucNop = "2";
                hoivien.NgayNopDon = DateTime.Now;
                hoivien.TinhTrangId = 3; // 3: chờ duyệt
                HoiVienCaNhan_provider.Insert(hoivien);

                // insert chứng chỉ
                for (int i = 0; i < gvChungChi.Rows.Count; i++)
                {
                    //DropDownList drChungChi = (DropDownList)gvChungChi.Rows[i].Cells[1].FindControl("drChungChi");
                    TextBox sochungchi = (TextBox)gvChungChi.Rows[i].Cells[1].FindControl("txtSoChungChi");
                    TextBox ngaycap = (TextBox)gvChungChi.Rows[i].Cells[2].FindControl("txtDate");

                    if (!string.IsNullOrEmpty(sochungchi.Text))
                    {
                        DonHoiVienCaNhanChungChi chungchi = new DonHoiVienCaNhanChungChi();
                        chungchi.DonHoiVienCaNhanId = hoivien.DonHoiVienCaNhanId;

                        chungchi.SoChungChi = sochungchi.Text;
                        if (!string.IsNullOrEmpty(ngaycap.Text))
                            chungchi.NgayCap = DateTime.ParseExact(ngaycap.Text, "dd/MM/yyyy", null);

                        HoiVienCaNhanChungChi_provider.Insert(chungchi);
                    }
                }

                // insert quá trình làm việc
                for (int i = 0; i < gvQuaTrinh.Rows.Count; i++)
                {
                    TextBox thoigian = (TextBox)gvQuaTrinh.Rows[i].Cells[1].FindControl("txtThoiGian");
                    TextBox chucvu = (TextBox)gvQuaTrinh.Rows[i].Cells[3].FindControl("txtChucVu");
                    TextBox noilamviec = (TextBox)gvQuaTrinh.Rows[i].Cells[4].FindControl("txtNoiLamViec");

                    DonHoiVienCaNhanQuaTrinhCongTac quatrinh = new DonHoiVienCaNhanQuaTrinhCongTac();
                    quatrinh.DonHoiVienCaNhanId = hoivien.DonHoiVienCaNhanId;
                    quatrinh.ThoiGianCongTac = thoigian.Text;
                    quatrinh.ChucVu = chucvu.Text;
                    quatrinh.NoiLamViec = noilamviec.Text;

                    HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
                }

                // file đính kèm
                CapnhatFile(hoivien.DonHoiVienCaNhanId);

                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Thêm mới đơn xin kết nạp hội viên " + hoivien.HoDem + " " + hoivien.Ten);

                Response.Redirect("admin.aspx?page=hoiviencanhan_edit&id=" + hoivien.DonHoiVienCaNhanId + "&act=edit&thongbao=1");
            }

        }
        catch (Exception ex)
        {
            string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + ex.ToString() + @"</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
    }

    public string SinhID(string HoDem, string Ten, string SoChungChiKTV)
    {
        string result = "";

        string sql = "SELECT dbo.BoDau(N'" + Ten + "')";
        SqlCommand cmd = new SqlCommand(sql);
        string ten = DataAccess.DLookup(cmd);

        cmd.CommandText = "SELECT dbo.BoDau(N'" + HoDem + "')";
        string hodem = DataAccess.DLookup(cmd);

        string[] array = hodem.Split(' ');
        hodem = "";
        foreach (string s in array)
        {
            if (!string.IsNullOrEmpty(s))
                hodem += s.Substring(0, 1).ToLower();
        }

        result = ten + hodem;

        for (int i = 0; i < SoChungChiKTV.Length; i++)
        {
            if (Regex.IsMatch(SoChungChiKTV.Substring(i, 1), @"^[0-9]\d*\.?[0]*$"))
                result += SoChungChiKTV.Substring(i, 1);
        }

        return result;
    }

    protected void CapnhatFile(int donHoiVienId)
    {
        if (donHoiVienId != null && donHoiVienId != 0)
        {
            SqlDonHoiVienFileProvider DonHoiVien_provider = new SqlDonHoiVienFileProvider(connStr, true, "");

            HttpPostedFile styledfileupload;

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                FileUpload fileUp = (FileUpload)row.FindControl("FileUp");
                FileDinhKem1 = new DonHoiVienFile();
                if (fileUp != null && fileUp.FileName != "")
                {
                    int fileUploadId = 0;
                    int loaiFieldId = 0;
                    String sTempLoaiFileID = row.Cells[3].Text;
                    String sTempFileUploadID = row.Cells[4].Text;

                    if (!String.IsNullOrEmpty(sTempFileUploadID) && sTempFileUploadID != "0" && sTempFileUploadID != "&nbsp;")
                    {
                        fileUploadId = Convert.ToInt32(row.Cells[4].Text);

                    }
                    if (!String.IsNullOrEmpty(sTempLoaiFileID) && sTempLoaiFileID != "0" && sTempLoaiFileID != "&nbsp;")
                    {
                        loaiFieldId = Convert.ToInt32(row.Cells[3].Text);

                    }
                    string styledfileupload_name = "";
                    styledfileupload = fileUp.PostedFile;
                    if (styledfileupload.FileName != "")
                    {
                        styledfileupload_name = fileUp.FileName;


                        byte[] datainput = new byte[styledfileupload.ContentLength];
                        styledfileupload.InputStream.Read(datainput, 0, styledfileupload.ContentLength);

                        FileDinhKem1.FileId = fileUploadId;
                        FileDinhKem1.DonHoiVienCaNhanId = donHoiVienId;
                        FileDinhKem1.LoaiGiayToId = loaiFieldId;
                        FileDinhKem1.FileDinhKem = datainput;
                        //FileDinhKem1.Tenfile = styledfileupload.FileName;
                        FileDinhKem1.TenFile = new System.IO.FileInfo(styledfileupload.FileName).Name;
                    }
                    if (FileDinhKem1.FileId == 0)
                    {
                        DonHoiVien_provider.Insert(FileDinhKem1);
                    }
                    else
                    {
                        DonHoiVien_provider.Update(FileDinhKem1);
                    }

                }
                else
                {

                }

            }


            loaduploadFileDinhKem(donHoiVienId);

        }
    }

    protected void CapnhatFile2(int HoiVienId)
    {
        if (HoiVienId != null && HoiVienId != 0)
        {
            SqlHoiVienFileProvider HoiVien_provider = new SqlHoiVienFileProvider(connStr, true, "");

            HttpPostedFile styledfileupload;

            foreach (GridViewRow row in FileDinhKem_grv.Rows)
            {
                FileUpload fileUp = (FileUpload)row.FindControl("FileUp");
                FileDinhKem2 = new HoiVienFile();
                if (fileUp != null && fileUp.FileName != "")
                {
                    int fileUploadId = 0;
                    int loaiFieldId = 0;
                    String sTempLoaiFileID = row.Cells[3].Text;
                    String sTempFileUploadID = row.Cells[4].Text;

                    if (!String.IsNullOrEmpty(sTempFileUploadID) && sTempFileUploadID != "0" && sTempFileUploadID != "&nbsp;")
                    {
                        fileUploadId = Convert.ToInt32(row.Cells[4].Text);

                    }
                    if (!String.IsNullOrEmpty(sTempLoaiFileID) && sTempLoaiFileID != "0" && sTempLoaiFileID != "&nbsp;")
                    {
                        loaiFieldId = Convert.ToInt32(row.Cells[3].Text);

                    }
                    string styledfileupload_name = "";
                    styledfileupload = fileUp.PostedFile;
                    if (styledfileupload.FileName != "")
                    {
                        styledfileupload_name = fileUp.FileName;


                        byte[] datainput = new byte[styledfileupload.ContentLength];
                        styledfileupload.InputStream.Read(datainput, 0, styledfileupload.ContentLength);

                        FileDinhKem2.FileId = fileUploadId;
                        FileDinhKem2.HoiVienCaNhanId = HoiVienId;
                        FileDinhKem2.LoaiGiayToId = loaiFieldId;
                        FileDinhKem2.FileDinhKem = datainput;
                        //FileDinhKem1.Tenfile = styledfileupload.FileName;
                        FileDinhKem2.TenFile = new System.IO.FileInfo(styledfileupload.FileName).Name;
                    }
                    if (FileDinhKem2.FileId == 0)
                    {
                        HoiVien_provider.Insert(FileDinhKem2);
                    }
                    else
                    {
                        HoiVien_provider.Update(FileDinhKem2);
                    }

                }
                else
                {

                }

            }


            //loaduploadFileDinhKem(donHoiVienId);

        }
    }
}