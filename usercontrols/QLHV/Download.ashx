﻿<%@ WebHandler Language="C#" Class="Download" %>

using System;
using System.Web;

public class Download : IHttpHandler
{

    public Commons cm = new Commons();
    VACPA.Entities.DonHoiVienFile FileDownload = new VACPA.Entities.DonHoiVienFile();
    VACPA.Entities.HoiVienFile FileDownload_HV = new VACPA.Entities.HoiVienFile();
    public void ProcessRequest(HttpContext context)
    {
        try
        {

            byte[] data = null;
            string tenfile = "";
            string sAttachFileID = "";
            int isAttachFileID = 0;

            if (context.Request.QueryString["AttachFileID"] != null && context.Request.QueryString["AttachFileID"] != "")
            {
                sAttachFileID = context.Request.QueryString["AttachFileID"];
                try
                {
                    if (!String.IsNullOrEmpty(sAttachFileID))
                    {
                        isAttachFileID = Convert.ToInt32(sAttachFileID);
                    }
                }
                catch
                {

                }

                if (isAttachFileID != 0)
                {
                    if (context.Request.QueryString["mode"] != null && context.Request.QueryString["mode"] == "don")
                    {
                        VACPA.Data.SqlClient.SqlDonHoiVienFileProvider filedinhkem_provider = new VACPA.Data.SqlClient.SqlDonHoiVienFileProvider(cm.connstr, true, "System.Data.OracleClient");
                        HttpPostedFile styledfileupload;

                        FileDownload = filedinhkem_provider.GetByFileId(isAttachFileID);
                    }
                    else
                    {
                        VACPA.Data.SqlClient.SqlHoiVienFileProvider filedinhkem_provider = new VACPA.Data.SqlClient.SqlHoiVienFileProvider(cm.connstr, true, "System.Data.OracleClient");
                        HttpPostedFile styledfileupload;

                        FileDownload_HV = filedinhkem_provider.GetByFileId(isAttachFileID);
                    }
                }
            }

            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            string TenFile = (context.Request.QueryString["mode"] != null && context.Request.QueryString["mode"] == "don") ? FileDownload.TenFile : FileDownload_HV.TenFile;

            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + TenFile + "\"");
            System.IO.BinaryWriter bw = new System.IO.BinaryWriter(context.Response.OutputStream);
            if (context.Request.QueryString["mode"] != null && context.Request.QueryString["mode"] == "don")
                bw.Write((byte[])FileDownload.FileDinhKem);
            else
                bw.Write((byte[])FileDownload_HV.FileDinhKem);
            bw.Close();
            context.Response.End();


        }
        catch (Exception ex)
        {
            context.Response.ContentType = "text/plain"; context.Response.Write(ex.Message);
        }
        finally
        {
            context.Response.End();
        }


    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}