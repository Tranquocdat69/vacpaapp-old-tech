﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="xoatenhoivien.ascx.cs" Inherits="usercontrols_xoatenhoivien" %>


<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 
 <style>
        .ui-datepicker 
        {
            z-index: 99999999 !important;
        }
        
    </style>
   <form id="Form1" runat="server">
   <h4 class="widgettitle">Danh sách xóa tên hội viên</h4>
   <div>
        <div  class="dataTables_length">
        
        
        <a id="btn_xoaten" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected ,'trinhpheduyet'  );"><i class="iconfa-plus-sign"></i> Xóa tên</a>
        <a id="btn_huyxoaten" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected , 'tuchoidon' );"><i class="iconfa-plus-sign"></i> Hủy xóa tên</a>
        <a id="btn_duyetdon" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected , 'duyetdon' );"><i class="iconfa-plus-sign"></i> Duyệt</a>
        <a id="btn_tuchoiduyet" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected ,'tuchoiduyet' );"><i class="iconfa-plus-sign"></i> Không duyệt</a>
        <a id="btn_thoaiduyet" href="#none" class="btn btn-rounded" onclick="xulydon_hv(Users_grv_selected , 'thoaiduyet' );"><i class="iconfa-plus-sign"></i> Thoái duyệt</a>
       


       
        <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="confirm_delete_user(Users_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
        <a href="#none" class="btn btn-rounded" onclick="open_user_search();"><i class="iconfa-search"></i> Tìm kiếm</a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
  
        </div>
        
   </div>
    
<asp:GridView ClientIDMode="Static" ID="Users_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="Users_grv_PageIndexChanging" onsorting="Users_grv_Sorting"    
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval("DinhDanh")%>" />
                  </ItemTemplate>
              </asp:TemplateField>
            <asp:TemplateField  HeaderText="Mã đơn đăng ký"   SortExpression="SoDonHoiVien" >
              <ItemTemplate>
                 <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:open_user_edit(" + DataBinder.Eval(Container.DataItem, "DonHoiVienID").ToString() + ","+ DataBinder.Eval(Container.DataItem, "Loai").ToString()+ ","+ DataBinder.Eval(Container.DataItem, "TinhTrangID").ToString()+"); "%>   Text= '<%# Eval("SoDonHoiVien") %>'  runat="server">
                    
                </asp:HyperLink>
        
             </ItemTemplate>
             <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
             <ItemStyle   HorizontalAlign="Center"     />
          </asp:TemplateField>

           
              <asp:BoundField DataField="TenKhachHang" HeaderText="Tên khách hàng" 
                  SortExpression="TenKhachHang" />
                   <asp:BoundField DataField="DonVi" HeaderText="Đơn vị công tác" 
                  SortExpression="DonVi" />
                <asp:BoundField DataField="LoaiDon" HeaderText="Loại đơn" 
                  SortExpression="LoaiDon" />

              <asp:BoundField DataField="NgayNopDon" HeaderText="Ngày nộp" 
                  SortExpression="NgayNopDon" DataFormatString="{0:dd/MM/yyyy}" />
              <asp:BoundField DataField="NgayDuyet" HeaderText="Ngày phê duyệt" 
                  SortExpression="NgayDuyet" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="TinhTrang" HeaderText="Trạng thái" 
                  SortExpression="TinhTrang" />    
              <asp:TemplateField HeaderText='' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                      <a onclick="open_user_view(<%# Eval("DonHoiVienID")%>,<%# Eval("Loai")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xóa tên"  rel="tooltip" class="btn"><i class="iconsweets-trashcan2" ></i></a>
                      <a onclick="open_user_edit(<%# Eval("DonHoiVienID")%> ,<%# Eval("Loai")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Sửa"  rel="tooltip" class="btn"><i class="iconsweets-create" ></i></a>
                      <a onclick="confirm_delete_user(<%# Eval("DinhDanh")%> );" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xóa"  rel="tooltip" class="btn" ><i class="iconsweets-trashcan" ></i></a>                                                   

                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>

            

          </Columns>
</asp:GridView>

<div class="dataTables_info" id="dyntable_info">Chuyển đến trang: 

<asp:DropDownList ID="Pager" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList>

</div>


</form>





<script type="text/javascript">

<% annut(); %>
    // Array ID được check
    var Users_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#Users_grv .checkall").bind("click", function () {
            Users_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#Users_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });

        $('#Users_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(Users_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa hội viên
    function confirm_delete_user(idxoa) {
     
        $("#div_user_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                 
                 window.location = 'admin.aspx?page=ketnaphoivien&act=delete&id=' + idxoa;
                
                   
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Sửa thông tin hội viên
    function open_user_edit(id, loai ,idtinhtrang) {
        var timestamp = Number(new Date());
        if(loai==1)
        {
           window.location = "admin.aspx?page=hoiviencanhan_edit&id=" + id + "&act=edit&time=" + timestamp
        }else{
           window.location = "admin.aspx?page=ketnaphoivientapthe_edit&prepage=xoatenhoivien&id=" + id + "&idtinhtrang="+idtinhtrang +"&act=edit&time=" + timestamp
        }
         
//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=ketnaphoivientapthe_edit&id=" + id + "&mode=edit&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Sửa thông tin đơn hội viên</b>",
//            modal: true,
//            buttons: {

//                "Ghi": function () {
//                    window.frames['iframe_user_add'].submitform();
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Xem thông tin hội viên
    function open_user_view(id,loai) {
        var timestamp = Number(new Date());
       if(loai==1)// ca nhan
        {
           window.location = "admin.aspx?page=hoiviencanhan_edit&id=" + id + "&act=view&time=" + timestamp
        }else{ // tap the
           window.location = "admin.aspx?page=ketnaphoivientapthe_edit&prepage=xoatenhoivien&id=" + id + "&act=view&time=" + timestamp
        }
     
//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=ketnaphoivientapthe_edit&id=" + id + "&mode=view&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Xem thông tin đơn hội viên</b>",
//            modal: true,
//            buttons: {

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



    // mở form thêm hội viên tap the
    function open_user_add() {

  
        var timestamp = Number(new Date());
      window.location = "admin.aspx?page=ketnaphoivientapthe_edit&act=add&time=" + timestamp

//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=ketnaphoivientapthe_edit&mode=iframe&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Thêm đơn gia nhập hội viên mới</b>",
//            modal: true,
//            close: function (event, ui) { window.location = 'admin.aspx?page=ketnaphoivientapthe'; },
//            buttons: {

//                "Ghi": function () {
//                    window.frames['iframe_user_add'].submitform();
//                   // $("#iframe_user_add")[0].contentWindow.submitform();
//                   
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
//   
   
    }
      // mở form thêm hội viên ca nhan
    function open_hvcn_add() {

  
        var timestamp = Number(new Date());
      window.location = "admin.aspx?page=hoiviencanhan_add&act=add&time=" + timestamp
       
    }

      //  danh sach cho tiep nhan online
    function tiepnhanonline_hv( ) {
         
        var timestamp = Number(new Date());
      window.location = "admin.aspx?page=ketnaphoivien&act=online&time=" + timestamp
    }
    
    function xulydon_hv(lstid , act ) {
         
        window.location = 'admin.aspx?page=ketnaphoivien&act='+act+'&id=' + lstid;

    }

     
    // Tìm kiếm
    function open_user_search() {
        var timestamp = Number(new Date());

        $("#div_user_search").dialog({
            resizable: true,
           width: 765,
            height: 440,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Tìm kiếm thông tin đơn hội viên</b>",
            modal: true,
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_user_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('#Users_grv').length > 0) jQuery('#Users_grv').tooltip({ selector: "a[rel=tooltip]" });


</script>

<div id="div_user_add" >
</div>

<div id="div_user_search"   style="display:none"   >
<form id="user_search" method="post" enctype="multipart/form-data" action="">
  <table  width="100%" border="0" class="formtbl"  >
    <tr>
        <td  style="width:100px" ><label>Loại đơn: </label></td>
       
        <td  colspan="3" >  
        <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" ><input id="tk_HVCN" name="tk_HVCN" type="checkbox"  class="input-xlarge" value="true"  /> <label  >Hội viên cá nhân</label> </td>
          <td style="width:130px" ><input id="tk_HVTT" name="tk_HVTT" type="checkbox"  class="input-xlarge"  value="true"   /><label  style="width:100px" >Hội viên tập thể</label> </td>
          <td style="width:130px" ></td>
          <td style="width:130px" ></td>
         </tr>
        </table>
                
                
         </td>
        
      
    </tr>
    <tr>
        <td ><label>Trạng thái: </label></td>
       
          <td  colspan="3" >  
              <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" > <input id="tk_TTAll" name="tk_TTAll" type="checkbox"  class="input-xlarge checkall"  value="true"  /><label  style="width:100px" >Tất cả</label> 
                 </td>
          <td style="width:130px" >  <input id="tk_TTChoTiepNhan" name="tk_TTChoTiepNhan" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Chờ tiếp nhận</label> 
                 </td>
          <td style="width:130px" > <input id="tk_TTChoDuyet" name="tk_TTChoDuyet" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Chờ duyệt</label> 
      </td>
          <td style="width:130px" ></td>
         </tr>
        </table>
                
                   </td>
       
    </tr>
    <tr>
        <td></td>
         <td  colspan="3" >  
           <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" >   <input id="tk_TTTuChoiTiepNhan" name="tk_TTTuChoiTiepNhan" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Từ chối tiếp nhận </label> 
              </td>
          <td style="width:130px" >  <input id="tk_TTTuChoiDuyet" name="tk_TTTuChoiDuyet" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Từ chối duyệt</label> 
                </td>
          <td style="width:130px" >   <input id="tk_TTDaDuyet" name="tk_TTDaDuyet" type="checkbox"  class="checkbox" value="true"   /><label  style="width:100px" >Đã duyệt</label> 
            </td>
          <td style="width:130px" > <input id="tk_TTThoaiDuyet" name="tk_TTThoaiDuyet" type="checkbox"  class="checkbox"  value="true"  /><label  style="width:100px" >Thoái duyệt</label> 
      </td>
         </tr>
        </table>
      
                </td>
    </tr>  
    
    <tr>
        <td><label>Tên hội viên:</label></td>
        <td> 
         <input type="text" name="tk_TenHoiVien" id="tk_TenHoiVien" class="input-xlarge">  </td>
         <td><label>ID:</label></td>
 
         <td>
           
            <input type="text" name="tk_ID" id="tk_ID" class="input-xlarge">  
             </td>
        
         
    </tr>
    <tr>
        <td><label>Số đơn:</label></td>
        <td> </asp:TextBox> 
         <input type="text" name="tk_SoDon" id="tk_SoDon" class="input-xlarge">   </td>
         <td><label>Hình thức nộp:</label></td>
 
         <td>  
          <select  style="width:100%"  name="tk_HinhThucNop" id="tk_HinhThucNop">
            <option value="">Tất cả</option>
            <option value="2">Nộp trực tiếp</option>
            <option value="1">Nộp trực tuyến</option>
         </select>
             
         </td>
        
        
         
    </tr>
    <tr>
        <td><label>Tỉnh thành:</label></td>
        <td>  <select  style="width:100%"  name="TinhID_ToChuc" id="TinhID_ToChuc">
              <% cm.Load_ThanhPho("00"); %>
       </select> 
         </td>
         <td><label>Quận huyện:</label></td>
 
         <td>  
              
    <select id="HuyenID_ToChuc" style="width:100%"  name="HuyenID_ToChuc">        
             <%  
                     try
                     {
                         cm.Load_QuanHuyen("000", "00");
                     }
                     catch
                     {

                     }
              
              %>  
    

</select>
    
         </td>
        
         
    </tr>
     <tr>
        <td><label>Ngày nộp:</label></td>
        <td>  
         <input  style="width:100px" type="text"  name="tk_NgayNopTu" id="tk_NgayNopTu" class="input-xlarge">  - <input style="width:100px"  type="text" name="tk_NgayNopDen" id="tk_NgayNopDen" class="input-xlarge">    </td>
       
        
         <td><label>Công ty kiểm toán:</label></td>
 
         <td>  
              <select  style="width:100%"  name="tk_CTKiemToan" id="tk_CTKiemToan">
              <% cm.Load_ThanhPho("00"); %>
       </select> 
         </td>
         
         
    </tr>

      <tr>
        <td><label>Ngày phê duyệt:</label></td>
        <td> 
        <input  style="width:100px" type="text" name="tk_NgayPheDuyetTu" id="tk_NgayPheDuyetTu" class="input-xlarge"> - <input  style="width:100px" type="text" name="tk_NgayPheDuyetDen" id="tk_NgayPheDuyetDen" class="input-xlarge">  </td>
       
          <td><label>Số chứng chỉ KTV:</label></td>
 
         <td>   
         <input type="text" name="tk_SoChungChiKTV" id="tk_SoChungChiKTV" class="input-xlarge"> </td>
       
        
       
         
    </tr>
     <tr>
        <td><label>Số giấy CN ĐKHNKT :</label></td>
        <td> 
          <input type="text" name="tk_CNDKHNKT" id="tk_CNDKHNKT" class="input-xlarge">   </td>
         <td><label>Số giấy CN ĐKKD:</label></td>
 
       <td><input type="text" name="tk_SoDKKD" id="tk_SoDKKD" class="input-xlarge"> 
        </td>
        
         
         
    </tr>

    

</table>
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
</form>
</div>


<script language="javascript">
 
 
    $('#tk_HVCN').prop('checked',  '<%=Request.Form["tk_HVCN"]%>' );
    $('#tk_HVTT').prop('checked',   '<%=Request.Form["tk_HVTT"]%>' );
    $('#tk_TTAll').prop('checked',  '<%=Request.Form["tk_TTAll"]%>' );
     $('#tk_TTChoTiepNhan').prop('checked',  '<%=Request.Form["tk_TTChoTiepNhan"]%>' );
    $('#tk_TTChoDuyet').prop('checked',  '<%=Request.Form["tk_TTChoDuyet"]%>' );
    $('#tk_TTTuChoiTiepNhan').prop('checked',  '<%=Request.Form["tk_TTTuChoiTiepNhan"]%>' );
     $('#tk_TTTuChoiDuyet').prop('checked',  '<%=Request.Form["tk_TTTuChoiDuyet"]%>' );
    $('#tk_TTDaDuyet').prop('checked',  '<%=Request.Form["tk_TTDaDuyet"]%>' );
    $('#tk_TTThoaiDuyet').prop('checked',  '<%=Request.Form["tk_TTThoaiDuyet"]%>' );
     $('#tk_TenHoiVien').val('<%=Request.Form["tk_TenHoiVien"]%>');
    $('#tk_ID').val('<%=Request.Form["tk_ID"]%>');
    $('#tk_SoDon').val('<%=Request.Form["tk_SoDon"]%>');
     $('#tk_HinhThucNop').val('<%=Request.Form["tk_HinhThucNop"]%>');
    $('#TinhID_ToChuc').val('<%=Request.Form["TinhID_ToChuc"]%>');
    $('#HuyenID_ToChuc').val('<%=Request.Form["HuyenID_ToChuc"]%>');
     $('#tk_NgayNopTu').val('<%=Request.Form["tk_NgayNopTu"]%>');
      $('#tk_NgayNopDen').val('<%=Request.Form["tk_NgayNopDen"]%>');
    $('#tk_CTKiemToan').val('<%=Request.Form["tk_CTKiemToan"]%>');
    $('#tk_NgayPheDuyetTu').val('<%=Request.Form["tk_NgayPheDuyetTu"]%>');
     $('#tk_NgayPheDuyetDen').val('<%=Request.Form["tk_NgayPheDuyetDen"]%>');
     $('#tk_SoChungChiKTV').val('<%=Request.Form["tk_SoChungChiKTV"]%>');
    $('#tk_CNDKHNKT').val('<%=Request.Form["tk_CNDKHNKT"]%>');
    $('#tk_SoDKKD').val('<%=Request.Form["tk_SoDKKD"]%>');
  
      <% cm.chondiaphuong_script("ToChuc",0); %>

         $.datepicker.setDefaults($.datepicker.regional['vi']);


    $(function () {
        $("#tk_NgayNopTu").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"    ,
             onClose: function (selectedDate) {
               
                 $("#tk_NgayNopDen").datepicker("option", "minDate", selectedDate);

              }
                    
        }) ;

         $("#tk_NgayNopDen").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"     ,
             onClose: function (selectedDate) {
              
                $("#tk_NgayNopTu").datepicker("option", "maxDate", selectedDate);

            }
                 
        }) ;
         $("#tk_NgayPheDuyetTu").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"  ,
             onClose: function (selectedDate) {
               
                 $("#tk_NgayPheDuyetDen").datepicker("option", "minDate", selectedDate);

              }        
        }) ;
         $("#tk_NgayPheDuyetDen").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"       ,
             onClose: function (selectedDate) {
              
                $("#tk_NgayPheDuyetTu").datepicker("option", "maxDate", selectedDate);

            }     
        }) ;

    });
     
    $(document).ready(function() {
            $('#tk_TTAll').click(function(event) {  //on click 
                if(this.checked) { // check select status
                   $('.checkbox').each(function() { //loop through each checkbox
             
                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                    }); 
          
        
                }else{
                      //$('#tk_TTChoTiepNhan').checked = false;
                        $('#tk_TTChoTiepNhan').prop('checked',  false );
                
                        $('#tk_TTChoDuyet').prop('checked',  false );
                        $('#tk_TTTuChoiTiepNhan').prop('checked',  false );
                         $('#tk_TTTuChoiDuyet').prop('checked',  false );
                        $('#tk_TTDaDuyet').prop('checked',  false );
                        $('#tk_TTThoaiDuyet').prop('checked',  false );

        //               $('.checkbox').each(function() { //loop through each checkbox
        //                  this.checkbox = false; //deselect all checkboxes with class "checkbox1"                       
        //               });   
                
                }
            });

            $( '.checkbox' ).bind( "click", function() {
                  if(this.checked==false) {
                 
                       $('#tk_TTAll').prop('checked',  false );
                    }  else{
                        
                       if($('#tk_TTChoTiepNhan').is(':checked')==true && $('#tk_TTChoDuyet').is(':checked')==true &&
                          $('#tk_TTTuChoiTiepNhan').is(':checked')==true &&$('#tk_TTTuChoiDuyet').is(':checked')==true &&
                          $('#tk_TTDaDuyet').is(':checked')==true &&$('#tk_TTThoaiDuyet').is(':checked')==true ){
                       
                          $('#tk_TTAll').prop('checked',  true );
                       }
                    
                    }  
               });
             

          
});

 </script>

<div id="div_user_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?(Hệ thống chỉ xóa các đơn chưa được phê duyệt)</p>

</div>


