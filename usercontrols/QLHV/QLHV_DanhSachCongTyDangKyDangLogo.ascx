﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QLHV_DanhSachCongTyDangKyDangLogo.ascx.cs"
    Inherits="usercontrols_QLHV_DanhSachCongTyDangKyDangLogo" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" name="Form1" clientidmode="Static" runat="server">
<h4 class="widgettitle">
    Danh sách công ty đăng logo</h4>
<div>
    <div class="dataTables_length">
        <a id="btn_them" href="javascript:;" class="btn btn-rounded" onclick="OpenFormUpdate();">
            <i class="iconfa-plus-sign"></i>Thêm mới</a>&nbsp;
        <asp:LinkButton ID="lbtDelete" runat="server" OnClick="lbtDelete_Click" CssClass="btn"
            OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}">
            <i class="iconfa-remove-sign"></i>Xóa
        </asp:LinkButton>&nbsp;
        <asp:LinkButton ID="btnApprove" runat="server" CssClass="btn" OnClick="btnApprove_Click" OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-remove-sign"></i>Duyệt</asp:LinkButton>&nbsp;
        <asp:LinkButton ID="btnReject" runat="server" CssClass="btn" OnClick="btnReject_Click" OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-remove-sign"></i>Từ chối</asp:LinkButton>&nbsp;
        <asp:LinkButton ID="btnDontApprove" runat="server" CssClass="btn" OnClick="btnDontApprove_Click" OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-thumbs-down"></i>Thoái duyệt</asp:LinkButton>&nbsp;
        <a id="btnExport" href="javascript:;" class="btn btn-rounded">
            <i class="iconfa-save"></i>Kết xuất</a>&nbsp;
        <a href="javascript:;" id="btn_search" class="btn btn-rounded" onclick="OpenFormSearch();">
            <i class="iconfa-search"></i>Tìm kiếm</a>
    </div>
</div>
<asp:GridView ClientIDMode="Static" ID="gv_DanhSachCongTy" runat="server" AutoGenerateColumns="False"
    class="table table-bordered responsive dyntable" AllowPaging="True" AllowSorting="True"
    OnPageIndexChanging="gv_DanhSachCongTy_PageIndexChanging" OnSorting="gv_DanhSachCongTy_Sorting"
    OnRowCommand="gv_DanhSachCongTy_RowCommand" OnRowDataBound="gv_DanhSachCongTy_RowDataBound">
    <Columns>
        <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
            ItemStyle-Width="30px">
            <ItemTemplate>
                <input type="checkbox" id="checkbox" runat="server" class="colcheckbox" value='<%# Eval("DangKyLoGoID")%>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="STT">
            <ItemTemplate>
                <span>
                    <%# Container.DataItemIndex + 1 %></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ID.HVTT/ID.CTKT" SortExpression="MaHoiVienTapThe">
            <ItemTemplate>
                <span>
                    <%# Eval("MaHoiVienTapThe")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên đầy đủ" SortExpression="TenDayDu">
            <ItemTemplate>
                <span>
                    <%# Eval("TenDayDu")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên viết tắt" SortExpression="TenVietTat">
            <ItemTemplate>
                <span>
                    <%# Eval("TenVietTat")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Kích thước logo" SortExpression="KichThuoc">
            <ItemTemplate>
                <span>
                    <%# Eval("KichThuoc")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Vị trí logo" SortExpression="ViTri">
            <ItemTemplate>
                <span>
                    <%# Eval("ViTri")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Ngày đăng logo" SortExpression="NgayDang">
            <ItemTemplate>
                <span>
                    <%# Eval("NgayDang")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Ngày hết hạn" SortExpression="NgayHetHan">
            <ItemTemplate>
                <span>
                    <%# Eval("NgayHetHan")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Hạn nộp phí" SortExpression="HanNop">
            <ItemTemplate>
                <span>
                    <%# Eval("HanNop")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tình trạng" SortExpression="TinhTrang">
            <ItemTemplate>
                <span>
                    <%# Eval("TinhTrang")%></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Thao tác" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <div class="btn-group">
                    <a href='javascript:;' data-placement="top" data-rel="tooltip" onclick="OpenFormUpdate(<%# Eval("DangKyLoGoID") %>);"
                        data-original-title="Xem/Sửa" rel="tooltip" class="btn"><i class="iconsweets-create">
                        </i></a>&nbsp;
                    <asp:LinkButton ID="btnDeleteInList" runat="server" CommandName="del" CommandArgument='<%# Eval("DangKyLoGoID") %>'
                        data-placement="top" data-rel="tooltip" data-original-title="Xóa" rel="tooltip"
                        class="btn" OnClientClick="return confirm('Bạn chắc chắn muốn xóa thông tin này chứ?');"><i class="iconsweets-trashcan"></i></asp:LinkButton>
                        <asp:HiddenField ID="hfMaTrangThai" runat="server" Value='<%# Eval("MaTrangThai") %>' />
                </div>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:HiddenField ID="hf_TrangHienTai" runat="server" />
<div class="dataTables_info" id="dyntable_info">
    <div class="pagination pagination-mini">
        Chuyển đến trang:
        <ul>
            <li><a href="javascript:;" onclick="$('#tranghientai').val(0);$('#hdAction').val('paging'); $('#user_search').submit();"><<
                Đầu tiên</a></li>
            <li><a href="javascript:;" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager option:selected').val()-1); $('#hdAction').val('paging'); $('#user_search').submit();}">
                < Trước</a>
            </li>
            <li><a style="border: none; background-color: #eeeeee">
                <asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" Style="width: 55px;
                    height: 22px;" onchange="$('#tranghientai').val(this.value);$('#hdAction').val('paging'); $('#user_search').submit();">
                </asp:DropDownList>
            </a></li>
            <li style="margin-left: 5px;"><a href="javascript:;" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) { $('#tranghientai').val(parseInt($('#Pager option:selected').val())+1); $('#hdAction').val('paging'); $('#user_search').submit();} ;"
                style="border-left: 1px solid #ccc;">Sau ></a></li>
            <li><a href="javascript:;" onclick="$('#tranghientai').val($('#Pager option').length-1);$('#hdAction').val('paging'); $('#user_search').submit();">
                Cuối cùng >></a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">

    <% CheckPermissionOnPage(); %>

    jQuery(document).ready(function () {
        // dynamic table      

        $("#gv_DanhSachCongTy .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#gv_DanhSachCongTy :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });
    
    function CheckHasChooseItem() {
        var flag = false;
        $('#gv_DanhSachCongTy :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all') {
                flag = true;
                return;
            }
        });
        if (!flag)
            alert('Phải chọn ít nhất một bản ghi để thực hiện thao tác!');
        return flag;
    }
    
    function OpenFormUpdate(id) {
        $("#DivFormUpdate").empty();
        if (id == undefined || id.length == 0)
            $("#DivFormUpdate").append($("<iframe width='100%' height='100%' id='ifNhap' name='ifNhap' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=QLHV_DanhSachCongTyDangKyDangLogo_Update"));
        if (id != undefined && parseInt(id) > 0)
            $("#DivFormUpdate").append($("<iframe width='100%' height='100%' id='ifNhap' name='ifNhap' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=QLHV_DanhSachCongTyDangKyDangLogo_Update&Id=" + id));
        $("#DivFormUpdate").dialog({
            resizable: true,
//            width: 760,
//            height: 550,
            width: 900,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Quản lý đăng logo của công ty</b>",
            modal: true,
            zIndex: 1000,
            close: function( event, ui ) {
                window.location = window.location;
            }
        });
    }

    function CloseFormUpdate() {
        $("#DivFormUpdate").dialog('close');
    }
    
    function OpenFormSearch() {
        $("#DivSearch").dialog({
            resizable: true,
            width: 700,
            height: 500,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm công ty đăng logo</b>",
            modal: true,
            zIndex: 1000
        });
        $("#DivSearch").parent().appendTo($("#Form1"));
    }

    function Search() {
        document.getElementById('<%= lbtTruyVan.ClientID %>').click();
    }

    // Hiển thị tooltip
    if (jQuery('#gv_DanhSachCongTy').length > 0) jQuery('#gv_DanhSachCongTy').tooltip({ selector: "a[rel=tooltip]" });
</script>
<div id="DivSearch" style="display: none;">
    <fieldset class="fsBlockInfor">
        <legend>Tiêu chí tìm kiếm</legend>
        <table style="width: 600px;" border="0" class="formtblInforWithoutBorder">
            <tr>
                <td style="width: 120px;">
                    Đối tượng đăng logo:
                </td>
                <td>
                    <asp:RadioButtonList ID="rbtDoiTuongDangLogo" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="">Tất cả</asp:ListItem>
                        <asp:ListItem Value="3">Công ty kiểm toán</asp:ListItem>
                        <asp:ListItem Value="0">Khác</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    ID.HVTT/ID.CTKT:
                </td>
                <td>
                    <asp:TextBox ID="txtMaCongTy" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Tên giao dịch:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtTenCongTy" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Tên viết tắt:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtTenVietTat" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Kích thước logo:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtKichThuocLogo" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ngày đăng logo:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtNgayDangTu" runat="server" Width="80px" CssClass="dateITA"></asp:TextBox>
                    &nbsp;&nbsp;-&nbsp;&nbsp;
                    <asp:TextBox ID="txtNgayDangDen" runat="server" Width="80px" CssClass="dateITA"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ngày hết hạn:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtNgayHetHanTu" runat="server" Width="80px" CssClass="dateITA"></asp:TextBox>
                    &nbsp;&nbsp;-&nbsp;&nbsp;
                    <asp:TextBox ID="txtNgayHetHanDen" runat="server" Width="80px" CssClass="dateITA"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Hạn nộp phí:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtHanNopPhiTu" runat="server" Width="80px" CssClass="dateITA"></asp:TextBox>
                    &nbsp;&nbsp;-&nbsp;&nbsp;
                    <asp:TextBox ID="txtHanNopPhiDen" runat="server" Width="80px" CssClass="dateITA"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Tình trạng:
                </td>
                <td colspan="3">
                    <asp:RadioButtonList ID="rbtTinhTrang" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="">Tất cả</asp:ListItem>
                        <asp:ListItem Value="3">Chờ duyệt</asp:ListItem>
                        <asp:ListItem Value="5">Đã duyệt</asp:ListItem>
                        <asp:ListItem Value="4">Từ chối</asp:ListItem>
                        <asp:ListItem Value="6">Thoái duyệt</asp:ListItem>
                    </asp:RadioButtonList>
                    <%--<asp:RadioButton ID="rbtTinhTrang_TatCa" runat="server" GroupName="TinhTrang" />Tất cả&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtTinhTrang_ChoDuyet" runat="server" GroupName="TinhTrang" />Chờ duyệt&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtTinhTrang_DaDuyet" runat="server" GroupName="TinhTrang" />Đã duyệt&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtTinhTrang_TuChoi" runat="server" GroupName="TinhTrang" />Từ chối&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtTinhTrang_ThoaiDuyet" runat="server" GroupName="TinhTrang" />Thoái duyệt&nbsp;&nbsp;--%>
                </td>
            </tr>
        </table>
    </fieldset>
    <div style="width: 100%; margin-top: 10px; text-align: right;">
        <asp:LinkButton ID="lbtTruyVan" runat="server" CssClass="btn" OnClientClick="return CheckValidFormSearch();"
            OnClick="lbtTruyVan_Click"><i class="iconfa-search"></i>Tìm</asp:LinkButton>&nbsp;
        <a href="javascript:;" id="A4" class="btn btn-rounded" onclick="$('#DivSearch').dialog('close');">
            <i class="iconfa-search"></i>Bỏ qua</a>
    </div>
</div>
<div id="DivFormUpdate">
</div>
</form>
<form id="user_search" method="post" enctype="multipart/form-data">
    <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
    <input type="hidden" value="0" id="hdAction" name="hdAction" />
</form>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#<%= txtNgayDangTu.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#<%= txtNgayDangDen.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#<%= txtNgayHetHanTu.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#<%= txtNgayHetHanDen.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#<%= txtHanNopPhiTu.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#<%= txtHanNopPhiDen.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

    $(document).ready(function () {
        $("#Form1").validate({
            onsubmit: false
        });
    });

    function CheckValidFormSearch() {
        var isValid = $("#Form1").valid();
        if (isValid) {
            $('#DivSearch').dialog('close');
            return true;
        } else {
            return false;
        }
    }

    $("#DivSearch").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtTruyVan.ClientID %>').attr('href'));
        }
    });
</script>
