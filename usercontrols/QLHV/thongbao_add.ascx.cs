﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DAL;
using System.IO;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using System.Configuration;
using System.Net.Mail;

public partial class usercontrols_QLHV_thongbao_add : System.Web.UI.UserControl
{
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Form["NoiDungThongBao"]))
        {

            SqlCommand sql = new SqlCommand();
            DataTable dtb = new DataTable();

            string[] lstid = Request.QueryString["id"].Split(',');

            string noidung = Request.Form["NoiDungThongBao"];
            string tieude = Request.Form["TieuDeThongBao"];
            HttpPostedFile file = Request.Files["FileDinhKem"];
            string loaihoivien = "";
            string hoivienid = "";
            string email = "";
            string err = "Lỗi gửi email: ";
            SqlThongBaoFileProvider proFile = new SqlThongBaoFileProvider(connStr, false, string.Empty);
            if (Request.Form["sendHVTT"] == "1" || Request.Form["sendCTKT"] == "1" || Request.Form["sendHVCN"] == "1" || Request.Form["sendKTV"] == "1" || Request.Form["sendNQT"] == "1")
            {

                sql.CommandText = "SELECT COUNT(*) FROM tblNguoiDung WHERE LoaiHoiVien IN (";
                if (Request.Form["sendHVTT"] == "1")
                    sql.CommandText += "'4',";
                if (Request.Form["sendCTKT"] == "1")
                    sql.CommandText += "'3',";
                if (Request.Form["sendHVCN"] == "1")
                    sql.CommandText += "'1',";
                if (Request.Form["sendKTV"] == "1")
                    sql.CommandText += "'2',";
                if (Request.Form["sendNQT"] == "1")
                    sql.CommandText += "'0',";
                sql.CommandText = sql.CommandText.Remove(sql.CommandText.Length - 1);
                sql.CommandText += ")";

                int tongso = int.Parse(DataAccess.DLookup(sql));
                int sovonglap = tongso / 350;
                if (tongso % 350 != 0)
                    sovonglap++;

                for (int k = 0; k < sovonglap; k++)
                {

                    sql.CommandText = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY HoiVienID DESC) AS Row, HoiVienID, LoaiHoiVien, Email FROM (SELECT HoiVienID, LoaiHoiVien, Email FROM tblNguoiDung WHERE LoaiHoiVien IN (";
                    if (Request.Form["sendHVTT"] == "1")
                        sql.CommandText += "'4',";
                    if (Request.Form["sendCTKT"] == "1")
                        sql.CommandText += "'3',";
                    if (Request.Form["sendHVCN"] == "1")
                        sql.CommandText += "'1',";
                    if (Request.Form["sendKTV"] == "1")
                        sql.CommandText += "'2',";
                    if (Request.Form["sendNQT"] == "1")
                        sql.CommandText += "'0',";
                    sql.CommandText = sql.CommandText.Remove(sql.CommandText.Length - 1);
                    sql.CommandText += ")) AS X) AS XX";
                    sql.CommandText += " WHERE XX.Row > " + k * 350 + " AND XX.Row <= " + (k + 1) * 350;
                    dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];

                    List<string> lstEmail = new List<string>();

                    foreach (DataRow row in dtb.Rows)
                    {
                        sql.CommandText = "INSERT INTO tblThongBao (HoiVienID_Nhan, LoaiHoiVien, NgayThang, TieuDe, NoiDung) VALUES ('" + row["HoiVienID"].ToString() + ",','" + row["LoaiHoiVien"].ToString() + "','" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "',N'" + tieude + "',N'" + noidung + "')";
                        DataAccess.RunActionCmd(sql);

                        if (file.ContentLength > 0)
                        {
                            sql.CommandText = "SELECT MAX(ThongBaoID) FROM tblThongBao";
                            string thongbaoid = DataAccess.DLookup(sql);

                            ThongBaoFile objFile = new ThongBaoFile();
                            BinaryReader br = new BinaryReader(file.InputStream);
                            byte[] fileByte = br.ReadBytes(file.ContentLength);
                            objFile.File = fileByte;
                            objFile.TenFile = file.FileName;
                            objFile.ThongBaoId = int.Parse(thongbaoid);
                            proFile.Insert(objFile);
                        }

                        if (Request.Form["sendEmail"] == "1" && !string.IsNullOrEmpty(row["Email"].ToString()))
                        {
                            lstEmail.Add(row["Email"].ToString());
                            
                        }
                    }

                    string msg = "";
                    if (file.ContentLength > 0)
                    {
                        BinaryReader br = new BinaryReader(file.InputStream);
                        byte[] fileByte = br.ReadBytes(file.ContentLength);
                        MemoryStream ms = new MemoryStream(fileByte);
                        Attachment at = new Attachment(ms, file.FileName);
                        SmtpMail.SendMultiWithAttach("BQT WEB VACPA", lstEmail, tieude, noidung, at, ref msg);
                    }
                    else
                        SmtpMail.SendMulti("BQT WEB VACPA", lstEmail, tieude, noidung, ref msg);
                    if (msg != "Email đã được gửi thành công!")
                        err += msg + ";";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    foreach (string id in lstid)
                    {

                        hoivienid = id.Split(';')[0];
                        if (id.Split(';')[1] == "1") // cá nhân
                            sql.CommandText = "SELECT LoaiHoiVien, Email FROM tblNguoiDung WHERE LoaiHoiVien IN ('0','1','2') AND HoiVienID = " + hoivienid;
                        else // tổ chức
                            sql.CommandText = "SELECT LoaiHoiVien, Email FROM tblNguoiDung WHERE LoaiHoiVien IN ('3','4') AND HoiVienID = " + hoivienid;

                        dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                        loaihoivien = dtb.Rows[0]["LoaiHoiVien"].ToString();
                        email = dtb.Rows[0]["Email"].ToString();
                        sql.CommandText = "INSERT INTO tblThongBao (HoiVienID_Nhan, LoaiHoiVien, NgayThang, TieuDe, NoiDung) VALUES ('" + hoivienid + ",','" + loaihoivien + "','" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "',N'" + tieude + "',N'" + noidung + "')";
                        DataAccess.RunActionCmd(sql);
                        if (file.ContentLength > 0)
                        {
                            sql.CommandText = "SELECT MAX(ThongBaoID) FROM tblThongBao";
                            string thongbaoid = DataAccess.DLookup(sql);

                            ThongBaoFile objFile = new ThongBaoFile();
                            BinaryReader br = new BinaryReader(file.InputStream);
                            byte[] fileByte = br.ReadBytes(file.ContentLength);
                            objFile.File = fileByte;
                            objFile.TenFile = file.FileName;
                            objFile.ThongBaoId = int.Parse(thongbaoid);
                            proFile.Insert(objFile);
                        }

                        if (Request.Form["sendEmail"] == "1" && !string.IsNullOrEmpty(email))
                        {
                            string msg = "";
                            if (file.ContentLength > 0)
                            {
                                BinaryReader br = new BinaryReader(file.InputStream);
                                byte[] fileByte = br.ReadBytes(file.ContentLength);
                                MemoryStream ms = new MemoryStream(fileByte);
                                Attachment at = new Attachment(ms, file.FileName);
                                SmtpMail.SendWithAttach("BQT WEB VACPA", email, tieude, noidung, at, ref msg);
                            }
                            else
                                SmtpMail.Send("BQT WEB VACPA", email, tieude, noidung, ref msg);
                            if (msg != "Email đã được gửi thành công!")
                                err += email + ":" + msg + ";";
                        }
                    }
                }
            }
            if (err != "Lỗi gửi email: ")
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + err + "</div>"));
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Gửi thông báo thành công</div>"));
        }

    }
}