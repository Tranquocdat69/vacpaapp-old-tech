﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;

public partial class usercontrols_nguoiquantam_add : System.Web.UI.UserControl
{
    String id = String.Empty;
    public Commons cm = new Commons();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    clsXacDinhQuyen quyen = new clsXacDinhQuyen();
    protected DonHoiVienFile FileDinhKem1 = new DonHoiVienFile();
    public bool QuyenXem { get; set; }
    public bool QuyenThem { get; set; }
    public bool QuyenSua { get; set; }
    public bool QuyenXoa { get; set; }
    public bool QuyenDuyet { get; set; }
    public bool QuyenTraLai { get; set; }
    public bool QuyenHuy { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Tên chức năng
            string tenchucnang = "Đăng ký người quan tâm";
            
            // Icon CSS Class  
            string IconClass = "iconfa-credit-card";

            if (!string.IsNullOrEmpty(cm.Admin_NguoiDungID))
            {
                string maquyen = quyen.fcnXDQuyen(int.Parse(cm.Admin_NguoiDungID), "DangKyMaSoCBDT", cm.connstr);

                QuyenXem = (maquyen.Contains("XEM")) ? true : false;
                QuyenThem = (maquyen.Contains("THEM")) ? true : false;
                QuyenSua = (maquyen.Contains("SUA")) ? true : false;
                QuyenXoa = (maquyen.Contains("XOA")) ? true : false;
                QuyenDuyet = (maquyen.Contains("DUYET")) ? true : false;
                QuyenTraLai = (maquyen.Contains("TRALAI")) ? true : false;
                QuyenHuy = (maquyen.Contains("HUY")) ? true : false;
            }

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"
     <li>Biểu mẫu tờ khai <span class=""separator""></span></li> <!-- Nhóm chức năng cấp trên -->
     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            if (!IsPostBack)
            {
                LoadDropDown();
                
            }



        }
        catch (Exception ex)
        {
            placeMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    public void LoadDropDown()
    {

        DataTable dtb = new DataTable();

        string sql = "";

        // Đơn vị
        sql = "SELECT 0 AS HoiVienTapTheID, N'<< Khác >>' AS TenDoanhNghiep, '-' AS SoHieu UNION ALL (SELECT HoiVienTapTheID, SoHieu + ' - ' + TenDoanhNghiep AS TenDoanhNghiep, SoHieu FROM tblHoiVienTapThe) ORDER BY SoHieu";
        SqlCommand cmd = new SqlCommand(sql);

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drDonVi.DataSource = dtb;
        drDonVi.DataBind();
       
        dtb.Clear();
        // Load chức vụ
        sql = "SELECT 0 AS ChucVuID, N'<< Lựa chọn >>' AS TenChucVu UNION ALL (SELECT ChucVuID, TenChucVu FROM tblDMChucVu)";
        cmd = new SqlCommand(sql);
        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        drChucVu.DataSource = dtb;
        drChucVu.DataBind();

        
    }

    public string SinhID(string HoDem, string Ten)
    {
        string result = "";

        string sql = "SELECT dbo.BoDau(N'" + Ten + "')";
        SqlCommand cmd = new SqlCommand(sql, conn);
        string ten = cmd.ExecuteScalar().ToString();

        cmd.CommandText = "SELECT dbo.BoDau(N'" + HoDem + "')";
        string hodem = cmd.ExecuteScalar().ToString();

        string[] array = hodem.Split(' ');
        hodem = "";
        foreach (string s in array)
        {
            if (!string.IsNullOrEmpty(s))
                hodem += s.Substring(0, 1).ToLower();
        }

        result = ten + hodem + DateTime.Now.Year.ToString().Substring(2, 2);

        sql = "SELECT dbo.LaySoChay('" + DateTime.Now.Year.ToString() + "')";
        cmd = new SqlCommand(sql, conn);
        string so = cmd.ExecuteScalar().ToString();

        result += so;

        return result;
    }

    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        try
        {
            conn.Open();

            string thongbao = "";
          
            if (!txtEmail.Text.Equals(txtXacNhanEmail.Text))
            {
                thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #000000;"">
							<p>Địa chỉ email xác nhận không hợp lệ.</p>
							
						</div>";
                placeMessage.Controls.Add(new LiteralControl(thongbao));
                return;
            }

            SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connStr, true, "");
            SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");

            string HoiVienID = SinhID(txtHoDem.Text, txtTen.Text);
            string gioitinh = (rdNam.Checked) ? "1" : "0";
            // insert vào bảng tblHoiVienCaNhan
            HoiVienCaNhan hoivien = new HoiVienCaNhan();

            hoivien.MaHoiVienCaNhan = HoiVienID;
            hoivien.HoDem = txtHoDem.Text;
            hoivien.Ten = txtTen.Text;
            if (!string.IsNullOrEmpty(NgaySinh.Value))
                hoivien.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);
            hoivien.GioiTinh = gioitinh;
            hoivien.TroLyKtv = (chkTroLy.Checked) ? "1" : "0";
            hoivien.Email = txtEmail.Text;
            hoivien.Mobile = txtMobile.Text;
            if (!string.IsNullOrEmpty(drChucVu.SelectedValue))
                hoivien.ChucVuId = int.Parse(drChucVu.SelectedValue);
            if (drDonVi.SelectedValue != "0")
                hoivien.HoiVienTapTheId = int.Parse(drDonVi.SelectedValue);
            else
                hoivien.DonViCongTac = txtDonViCongTac.Text;
            hoivien.LoaiHoiVienCaNhan = "0"; // 0: người quan tâm; 1: hội viên cá nhân; 2: kiểm toán viên

            HoiVienCaNhan_provider.Insert(hoivien);

            // insert vào bảng tblNguoiDung

            NguoiDung new_user = new NguoiDung();
            new_user.HoVaTen = txtHoDem.Text + " " + txtTen.Text;
            if (!string.IsNullOrEmpty(NgaySinh.Value))
                new_user.NgaySinh = DateTime.ParseExact(NgaySinh.Value, "dd/MM/yyyy", null);

            new_user.GioiTinh = gioitinh;
            new_user.Email = txtEmail.Text;
            new_user.DienThoai = txtMobile.Text;
            
            new_user.TenDangNhap = HoiVienID;

            Random random = new Random();
            int randomPassword = random.Next(10000000, 99999999);

            new_user.MatKhau = randomPassword.ToString();
            new_user.HoiVienId = hoivien.HoiVienCaNhanId;
            new_user.LoaiHoiVien = "0";

            NguoiDung_provider.Insert(new_user);


            // Send mail
            string body = "";
            body += "Thân chào Anh/chị <span style=\"color:red\">" + new_user.HoVaTen + "</span><BR/>";
            body += "Ban quản trị xin thông báo tài khoản đăng nhập phần mềm quản lý Hội viên VACPA như sau:<BR/>";
            body += "------ID đăng nhập------<BR/>";
            body += "Tên đăng nhập: " + HoiVienID + "<BR/>";
            body += "Mật khẩu: " + randomPassword + "<BR/>";
            body += "<span style=\"text-decoration:underline;font-weight:bold\">Lưu ý:</span><BR/>";
            body += " - Link đăng nhập phần mềm quản lý hội viên: http://vacpa.org.vn/Page/Login.aspx" + "<BR/>";
            body += " - Link xin cấp lại mật khẩu: http://vacpa.org.vn/Page/GetPassword.aspx" + "<BR/>";
            body += " - Ngay sau khi đăng nhập Anh/Chị hãy đổi mật khẩu truy cập." + "<BR/>";
            body += " - Để đảm bảo được nhận các thông báo thường xuyên với VACPA, Anh/Chị nhớ cập nhật những thông tin về sự thay đổi công việc, đơn vị công tác, số điện thoại di động, email, nơi thường trú, v.v… qua profile cá nhân." + "<BR/>";
            
            body += "━━━━━━━━━━━━━━━<BR/>";
            body += "Trân trọng,<BR/>";
            body += "Hội kiểm toán viên hành nghề Việt Nam (VACPA)<BR/>";
            body += "Địa chỉ: Phòng 304, Nhà Dự án, Số 4, Ngõ Hàng Chuối I, Hà Nội<BR/>";
            body += "Email: quantriweb@vacpa.org.vn";
            thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>
							<p>" + "Đăng ký tài khoản thành công." + @"</p>							
						</div>";
            string msg = "";
            if (SmtpMail.Send("BQT WEB VACPA", txtEmail.Text, "Thông tin đăng ký tài khoản người quan tâm tại trang tin điện tử VACPA", body, ref msg))
            {
                placeMessage.Controls.Add(new LiteralControl(thongbao));
            }
            else
                placeMessage.Controls.Add(new LiteralControl(msg));
        }
        catch (Exception ex)
        {
            string thongbao = @"<div class='alert alert-error' style=''><button class='close' type='button' >×</button>
							<p>" + ex.ToString() + @"</p>
							
						</div>";
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
    }
}