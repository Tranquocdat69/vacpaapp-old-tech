﻿using HiPT.VACPA.DL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;

public partial class usercontrols_QLHV_hoivientapthe_daotao_giohoc : System.Web.UI.UserControl
{
    String id = String.Empty;
    public Commons cm = new Commons();
    public HoiVienTapTheChiNhanh objHoiVIenTapTheChiNhanh = new HoiVienTapTheChiNhanh();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    int HoiVienTapTheID = 0;
    int LopHocID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        gvLopHoc.Columns[0].HeaderText = "STT";
        gvLopHoc.Columns[1].HeaderText = "Họ và tên";
        gvLopHoc.Columns[2].HeaderText = "Hội viên";
        gvLopHoc.Columns[3].HeaderText = "Số chứng chỉ KTV";
        gvLopHoc.Columns[4].HeaderText = "Ngày cấp";

        gvLopHoc.Columns[5].HeaderText = "Tham gia KT, KiT";
        gvLopHoc.Columns[6].HeaderText = "Tham gia Đạo đức";
        gvLopHoc.Columns[7].HeaderText = "Tham gia Khác";

        gvLopHoc.Columns[8].HeaderText = "Còn thiếu KT, KiT";
        gvLopHoc.Columns[9].HeaderText = "Còn thiếu Đạo đức";
        gvLopHoc.Columns[10].HeaderText = "Còn thiếu Khác";

        gvLopHoc.Columns[11].HeaderText = "Tổng";

        if (!IsPostBack)
        {
            labThongTinGioHoc.Text = "";
            hidendHoiVienTapTheID.Value = Request.QueryString["id"];
            
            try
            {
                HoiVienTapTheID = Convert.ToInt32(hidendHoiVienTapTheID.Value);
            }
            catch { }
            


            LoadGioHocToiThieu(HoiVienTapTheID);

        }
    }



    public void LoadGioHocToiThieu(int HoiVienTapTheID)
    {


        SqlCommand sql = new SqlCommand();
        sql.Connection = new SqlConnection(connStr);
        int curYear = (DateTime.Now).Year;

        sql.CommandText = "  select Top 1 CoCauSoGioHocToiThieuID  ,NamBatDau  ,NamKetThuc  ,KeToanKiemToan  ,DaoDucNgheNghiep  ,Khac " +
                          "   FROM  tblCNKTCoCauSoGioHocToiThieu WHERE NamBatDau <= " + curYear + " and NamKetThuc >=" + curYear + "  ";



        String sKtKiT = "";
        String sDD = "";
        String sKhac = "";

        int iKtKiT = 0;
        int iDD = 0;
        int iKhac = 0;
        int iTong = 0;
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
        {
            try
            {
                sKtKiT = ds.Tables[0].Rows[0]["KeToanKiemToan"].ToString();
                iKtKiT = Convert.ToInt32(sKtKiT);
            }
            catch { }

            try
            {
                sDD = ds.Tables[0].Rows[0]["DaoDucNgheNghiep"].ToString();
                iDD = Convert.ToInt32(sDD);

            }
            catch { }
            try
            {
                sKhac = ds.Tables[0].Rows[0]["Khac"].ToString();
                iKhac = Convert.ToInt32(sKhac);
            }
            catch { }
            iTong = iKhac + iDD + iKtKiT;
            labThongTinGioHoc.Text = "Quy định số giờ phải cập nhật kiến thức gồm " + iTong + " giờ, trong đó" + " " + iKtKiT + " giờ Kế toán, kiểm toán" + " " + iDD + " giờ Đạo đức nghề nghiệp" + " " + iKhac + " giờ Khác";

            LoadLopHoc_HocVien_SoGioConThieu(HoiVienTapTheID, iKtKiT, iDD, iKhac);

        }


        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds = null;


    }

    public void LoadLopHoc_HocVien_SoGioConThieu(int HoiVienTapTheID, int iKtKiT, int iDD, int iKhac)
    {





        DataSet ds = SelectDataSetGioHocCNKTConThieu(HoiVienTapTheID.ToString(), iKtKiT, iDD, iKhac);
        DataView dt = ds.Tables[0].DefaultView;


        PagedDataSource objPds1 = new PagedDataSource();
        objPds1.DataSource = ds.Tables[0].DefaultView;

        gvLopHoc.DataSource = objPds1;
        gvLopHoc.DataBind();

        ds = null;
        dt = null;

    }

    public DataSet SelectDataSetGioHocCNKTConThieu(String HoiVienTapTheID, int iKtKiT, int iDD, int iKhac)
    {

        DBHelpers.DBHelper objDBHelper = null;


        try
        {
            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("HoiVienTapTheID", HoiVienTapTheID));
            arrParams.Add(new DBHelpers.DataParameter("YC_KTKiT", iKtKiT));
            arrParams.Add(new DBHelpers.DataParameter("YC_DD", iDD));
            arrParams.Add(new DBHelpers.DataParameter("YC_Khac", iKhac));

            DateTime d = new DateTime(DateTime.Now.Year, 8, 16);
            if (DateTime.Now >= d)
            {
                arrParams.Add(new DBHelpers.DataParameter("TuNgay", DateTime.Now.Year + "-08-16"));
                arrParams.Add(new DBHelpers.DataParameter("DenNgay", (DateTime.Now.Year + 1).ToString() + "-08-15"));
            }              
            else
            {
                arrParams.Add(new DBHelpers.DataParameter("TuNgay", (DateTime.Now.Year - 1).ToString() + "-08-16"));
                arrParams.Add(new DBHelpers.DataParameter("DenNgay", DateTime.Now.Year + "-08-15"));
            }
                

            objDBHelper = DBHelpers.ConnectionControllers.ConnectByConnectionString(0, connStr);

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_lophoc_cnkt_sogiothieu", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            DBHelpers.ConnectionControllers.DisConnect(objDBHelper);
        }


    }
}