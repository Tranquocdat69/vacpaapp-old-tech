﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;

using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using CommonLayer;
using VACPA.Data.Bases;

public partial class usercontrols_capnhathoso : System.Web.UI.UserControl
  {
      public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
      public Commons cm = new Commons();


    

      string tuchoidon = "tuchoidon";
      string trinhpheduyet = "trinhpheduyet";
      string duyetdon = "duyetdon";
      string tuchoiduyet = "tuchoiduyet";
      string thoaiduyet = "thoaiduyet";


      int trangthaiChoTiepNhanID = 1;
      int trangthaiTuChoiID = 2;
      int trangthaiChoDuyetID = 3;
      int trangthaiTraLaiID = 4;
      int trangthaiDaPheDuyet = 5;
      int trangthaiThoaiDuyetID = 6;


      string tk_TTChoTiepNhan = "1";
      string tk_TTTuChoiTiepNhan = "2";
      string tk_TTChoDuyet = "3";
      string tk_TTTuChoiDuyet = "4";
      string tk_TTDaDuyet = "5";
      string tk_TTThoaiDuyet = "6";

      string tk_LoaiNQT= "0";
      string tk_LoaiHVCN  = "1";
      string tk_LoaiKTV = "2";

      string tk_LoaiHVTT = "1";
      string tk_LoaiCTKT = "2";




      string delete = "delete";
      string canhan = "1";
      string tapthe = "2";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
         

      
                        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

                        // Tên chức năng
                        string tenchucnang = "Cập nhật hồ sơ hội viên";
                        // Icon CSS Class  
                        string IconClass = "iconfa-book";

                        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

                     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
                     "));

                        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

                        // Phân quyền
                        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("XEM|"))
                        {
                            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
                            Form1.Visible = false;
                            return;
                        }




                        try
                        {
                             

                            //  action 
                            if (Request.QueryString["act"] != "")
                            {
                                if (!String.IsNullOrEmpty(Request.QueryString["id"]))
                                {
                                    string[] lstObject = Request.QueryString["id"].Split(',');
                                    string lstIdCaNhan = "";
                                    string lstIdTinhTrangCaNhan = "";
                                    int countCanhan = 0;
                                    string lstIdTapThe = "";
                                    string lstIdTinhTrangTapThe = "";
                                    int countTapThe = 0;
                                    bool update = false;
                                    foreach (string iterm in lstObject)
                                    {
                                        string[] lstTemp = iterm.Split('-');
                                        if (lstTemp.Length == 2)
                                        {
                                            if (lstTemp[1] == canhan)
                                            {
                                                if (countCanhan == 0)
                                                {
                                                    lstIdTinhTrangCaNhan = lstTemp[0];
                                                    string[] lstIterm = lstTemp[0].Split('|');
                                                    lstIdCaNhan = lstIterm[0];
                                                }
                                                else
                                                {
                                                    lstIdTinhTrangCaNhan = lstIdTinhTrangCaNhan + "," + lstTemp[0];
                                                    string[] lstIterm = lstTemp[0].Split('|');
                                                    lstIdCaNhan = lstIdCaNhan + "," + lstIterm[0];
                                                }

                                                countCanhan = countCanhan + 1;
                                            }
                                            else
                                            {
                                                if (countTapThe == 0)
                                                {
                                                    lstIdTinhTrangTapThe = lstTemp[0];
                                                    string[] lstIterm = lstTemp[0].Split('|');
                                                    lstIdTapThe = lstIterm[0];
                                                }
                                                else
                                                {
                                                    lstIdTinhTrangTapThe = lstIdTinhTrangTapThe + "," + lstTemp[0];
                                                    string[] lstIterm = lstTemp[0].Split('|');
                                                    lstIdTapThe = lstIdTapThe + "," + lstIterm[0];
                                                }

                                                countTapThe = countTapThe + 1;
                                            }
                                        }
                                    }
                                    if (lstIdTinhTrangCaNhan.Length > 0)//   ca nhan
                                    {
                                        // delete
                                        //if (Request.QueryString["act"] == "delete")
                                        //{
                                             
                                        //}

                                       
                                        // tu choi duyet 
                                        if (Request.QueryString["act"] == tuchoiduyet)
                                        {
                                            String sLydotuchoi = Request.QueryString["lydo"];
                                            string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                                            bool isSendMail = true;
                                            foreach (string sDonHoiVien in lstDonHoiVien)
                                            {
                                                string[] lstIterm = sDonHoiVien.Split('|');
                                                String sDonHoiVienID = lstIterm[0];
                                                String sDonHoiVienTinhtrang = lstIterm[1];
                                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                                {
                                                    // btnTuChoiDuyet_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                                    cm.PheDuyetYeuCauCapNhatHoiVienCaNhan(sDonHoiVienID, tk_TTTuChoiDuyet,  sLydotuchoi);
                                                    update = true;
                                                }
                                            }
                                            if (!isSendMail)
                                            {

                                            }

                                        }

                                         
                                        //  phe duyet 
                                        if (Request.QueryString["act"] == duyetdon)
                                        {
                                            string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                                            bool isSendMail = true;
                                            foreach (string sDonHoiVien in lstDonHoiVien)
                                            {
                                                string[] lstIterm = sDonHoiVien.Split('|');
                                                String sDonHoiVienID = lstIterm[0];
                                                String sDonHoiVienTinhtrang = lstIterm[1];
                                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                                {
                                                    // btnPheDuyet_Click(sDonHoiVienID, ref isSendMail);
                                                
                                                    cm.PheDuyetYeuCauCapNhatHoiVienCaNhan(sDonHoiVienID, tk_TTDaDuyet, "");
                                                    update = true;
                                                }
                                            }
                                            if (!isSendMail)
                                            {

                                            }

                                        }
                                    }

                                    if (lstIdTinhTrangTapThe.Length > 0)//    tap the
                                    {
                                        //// delete
                                        //if (Request.QueryString["act"] == "delete")
                                        //{
                                            
                                        //}

                                       

                                        // tu choi duyet 
                                        if (Request.QueryString["act"] == tuchoiduyet)
                                        {
                                            String sLydotuchoi = Request.QueryString["lydo"];
                                            string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                                            bool isSendMail = true;
                                            foreach (string sDonHoiVien in lstDonHoiVien)
                                            {
                                                string[] lstIterm = sDonHoiVien.Split('|');
                                                String sDonHoiVienID = lstIterm[0];
                                                String sDonHoiVienTinhtrang = lstIterm[1];
                                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                                {
                                                    btnTuChoiDuyet_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                                    update = true;
                                                }
                                            }
                                            if (!isSendMail)
                                            {

                                            }
                                        }

                                       

                                        //  phe duyet 
                                        if (Request.QueryString["act"] == duyetdon)
                                        {
                                            string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                                            bool isSendMail = true;
                                            foreach (string sDonHoiVien in lstDonHoiVien)
                                            {
                                                string[] lstIterm = sDonHoiVien.Split('|');
                                                String sDonHoiVienID = lstIterm[0];
                                                String sDonHoiVienTinhtrang = lstIterm[1];
                                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                                {
                                                    btnPheDuyet_Click(sDonHoiVienID, ref isSendMail);
                                                    update = true;
                                                }
                                            }
                                            if (!isSendMail)
                                            {

                                            }
                                        }

                                    }

                                    if (update)
                                    {
                                        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật hồ sơ thành công.</div>";
                                        Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(thongbao));
                                    }
                                }




                            }




                        }
                        catch (Exception ex)
                        {
                            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
                        }




                        ///////////

                        load_users();


          
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }

    public DataSet sp_YeuCauCapNhatSearch()
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            bool isCaNhan = false; ;
            bool isTapThe = false;
            bool isKiemToanVien = false; ;
            bool isCTyKiemToan = false;


            String sLoaiHoiVienTT = "";
            String sLoaiHoiVienCN = "";

            String sLoaiDon = "";
            String sTrangThai = "";
            String sTenHoiVien = "";
            String sHoDemCaNhan = "";
            String sTenCaNhan = "";

            String sIDHoiVien = "";
            String sSoDon = "";
            String sHinhThucNop = "";
            String sMaTinhThanh = "";
            String sMaQuanHuyen = "";

            String sCongtyKiemToan = "";
            String sSoChungChiKTV = "";
            String sSoDKKD = "";
            String sSoCNDuDieuKienHNKT = "";

            String sNgayNopTu = "";
            String sNgayNopDen = "";
            String sNgayDuyetTu = "";
            String sNgayDuyetDen = "";

            //  Loai Don canhan/tap the
            if (Request.Form["tk_HVCN"] != null && Request.Form["tk_HVCN"] == "true")
            {  
                sLoaiHoiVienCN = tk_LoaiHVCN;
                isCaNhan = true;
            }

            if (Request.Form["tk_HVTT"] != null && Request.Form["tk_HVTT"] == "true")
            {
                sLoaiHoiVienTT = tk_LoaiHVTT;
                isTapThe = true;
            }


            //int tempCN = 0;
            //int tempTT = 0;
            //if (Request.Form["tk_HVCN"] != null && Request.Form["tk_HVCN"] == "true")
            //{
            //    if (tempCN == 0)
            //    {
            //        sLoaiHoiVienCN = tk_LoaiHVCN;
            //    }
            //    else
            //    {
            //        sLoaiHoiVienCN = sLoaiHoiVienCN + "," + tk_LoaiHVCN;
            //    }
            //    tempCN = tempCN + 1;

            //    isCaNhan = true;
            //}
            //if (Request.Form["tk_KTV"] != null && Request.Form["tk_KTV"] == "true")
            //{
            //    if (tempCN == 0)
            //    {
            //        sLoaiHoiVienCN = tk_LoaiKTV;
            //    }
            //    else
            //    {
            //        sLoaiHoiVienCN = sLoaiHoiVienCN + "," + tk_LoaiKTV;
            //    }
            //    tempCN = tempCN + 1;

            //    isKiemToanVien = true;
            //}

            
            //if (Request.Form["tk_HVTT"] != null && Request.Form["tk_HVTT"] == "true")
            //{
            //    if (tempCN == 0)
            //    {
            //        sLoaiHoiVienTT = tk_LoaiHVTT;
            //    }
            //    else
            //    {
            //        sLoaiHoiVienTT = sLoaiHoiVienTT + "," + tk_LoaiHVTT;
            //    }
            //    tempTT = tempTT + 1;
            //    isTapThe = true;
            //}
          

            //if (Request.Form["tk_CTKT"] != null && Request.Form["tk_CTKT"] == "true")
            //{
            //    if (tempCN == 0)
            //    {
            //        sLoaiHoiVienTT = tk_LoaiCTKT;
            //    }
            //    else
            //    {
            //        sLoaiHoiVienTT = sLoaiHoiVienTT + "," + tk_LoaiCTKT;
            //    }
            //    tempTT = tempTT + 1;
            //    isCTyKiemToan = true;
            //}


            //if ((isCaNhan && isTapThe && isKiemToanVien && isCTyKiemToan) || 
            //     (!isCaNhan && !isTapThe && !isKiemToanVien && !isCTyKiemToan))   { sLoaiDon = "3"; }
            //else if (isCaNhan || isKiemToanVien) { sLoaiDon = "1"; }
            //else if (isTapThe || isCTyKiemToan) { sLoaiDon = "2"; }

            if ((isCaNhan && isTapThe  ) ||
                (!isCaNhan && !isTapThe  )) { sLoaiDon = "3"; }
            else if (isCaNhan  ) { sLoaiDon = "1"; }
            else if (isTapThe  ) { sLoaiDon = "2"; }


            // Trang thai
            if (Request.Form["tk_TTAll"] != null && Request.Form["tk_HVCN"] == "true")
            {  // check all

                sTrangThai = "";// all
            }
            else
            {
                int itemp = 0;
                //if (Request.Form["tk_TTChoTiepNhan"] != null && Request.Form["tk_TTChoTiepNhan"] == "true")
                //{
                //    if (itemp == 0)
                //    {
                //        sTrangThai = tk_TTChoTiepNhan;
                //    }
                //    else
                //    {
                //        sTrangThai = sTrangThai + "," + tk_TTChoTiepNhan;
                //    }

                //    itemp = itemp + 1;
                //}
                if (Request.Form["tk_TTChoDuyet"] != null && Request.Form["tk_TTChoDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTChoDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTChoDuyet;
                    }
                    itemp = itemp + 1;

                }
                //if (Request.Form["tk_TTTuChoiTiepNhan"] != null && Request.Form["tk_TTTuChoiTiepNhan"] == "true")
                //{
                //    if (itemp == 0)
                //    {
                //        sTrangThai = tk_TTTuChoiTiepNhan;
                //    }
                //    else
                //    {
                //        sTrangThai = sTrangThai + "," + tk_TTTuChoiTiepNhan;
                //    }
                //    itemp = itemp + 1;

                //}
                if (Request.Form["tk_TTTuChoiDuyet"] != null && Request.Form["tk_TTTuChoiDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTTuChoiDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTTuChoiDuyet;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTDaDuyet"] != null && Request.Form["tk_TTDaDuyet"] == "true")
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTDaDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTDaDuyet;
                    }
                    itemp = itemp + 1;

                }
                //if (Request.Form["tk_TTThoaiDuyet"] != null && Request.Form["tk_TTThoaiDuyet"] == "true")
                //{
                //    if (itemp == 0)
                //    {
                //        sTrangThai = tk_TTThoaiDuyet;
                //    }
                //    else
                //    {
                //        sTrangThai = sTrangThai + "," + tk_TTThoaiDuyet;
                //    }
                //    itemp = itemp + 1;

                //}


            }



            // Ten hoi vien
            if (Request.Form["tk_TenHoiVien"] != null)
            {
                sTenHoiVien = Request.Form["tk_TenHoiVien"];
                string[] stempHoten = Request.Form["tk_TenHoiVien"].Split(' ');
                if (stempHoten.Length > 1)
                {

                    for (int j = 0; j < stempHoten.Length - 1; j++)
                    {
                        sHoDemCaNhan = sHoDemCaNhan + " " + stempHoten[j];

                    }
                    sHoDemCaNhan = sHoDemCaNhan.Trim();
                    sTenCaNhan = stempHoten[stempHoten.Length - 1];
                }
                else
                {
                    sTenCaNhan = Request.Form["tk_TenHoiVien"];
                    sHoDemCaNhan = Request.Form["tk_TenHoiVien"];
                }

            }

            // Ten dang nhap
            if (Request.Form["tk_ID"] != null)
            {
                sIDHoiVien = Request.Form["tk_ID"];
            }

            // So Don
            if (Request.Form["tk_SoDon"] != null)
            {
                sSoDon = Request.Form["tk_SoDon"];
            }

            // Hinh thuc nop
            if (Request.Form["tk_HinhThucNop"] != null)
            {
                sHinhThucNop = Request.Form["tk_HinhThucNop"];
            }

            // Ma Tinh
            if (Request.Form["TinhID_ToChuc"] != null)
            {
                sMaTinhThanh = Request.Form["TinhID_ToChuc"];
            }
            // Ma Huyen
            if (Request.Form["HuyenID_ToChuc"] != null)
            {
                sMaQuanHuyen = Request.Form["HuyenID_ToChuc"];
            }


            // Cong ty kiem toan
            if (Request.Form["tk_CTKiemToan"] != null)
            {
                sCongtyKiemToan = Request.Form["tk_CTKiemToan"];
            }



            // So chung chi KTV
            if (Request.Form["tk_SoChungChiKTV"] != null)
            {
                sSoChungChiKTV = Request.Form["tk_SoChungChiKTV"];
            }

            // Chung nhan dk hanh nghe kt
            if (Request.Form["tk_CNDKHNKT"] != null)
            {
                sSoCNDuDieuKienHNKT = Request.Form["tk_CNDKHNKT"];
            }

            // So DKKD
            if (Request.Form["tk_SoDKKD"] != null)
            {
                sSoDKKD = Request.Form["tk_SoDKKD"];
            }


            // Ngay nop tu
            if (Request.Form["tk_NgayNopTu"] != null)
            {
                sNgayNopTu = Request.Form["tk_NgayNopTu"].Replace("/", ""); ;
            }

            // Ngay nop den
            if (Request.Form["tk_NgayNopDen"] != null)
            {
                sNgayNopDen = Request.Form["tk_NgayNopDen"].Replace("/", "");
            }
            // Ngay duyet tu
            if (Request.Form["tk_NgayPheDuyetTu"] != null)
            {
                sNgayDuyetTu = Request.Form["tk_NgayPheDuyetTu"].Replace("/", ""); ;
            }
            //  Ngay duyet den
            if (Request.Form["tk_NgayPheDuyetDen"] != null)
            {
                sNgayDuyetDen = Request.Form["tk_NgayPheDuyetDen"].Replace("/", ""); ;
            }


            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("LoaiDon", sLoaiDon));
            arrParams.Add(new DBHelpers.DataParameter("LoaiHoiVienCN", sLoaiHoiVienCN));
            arrParams.Add(new DBHelpers.DataParameter("LoaiHoiVienTT", sLoaiHoiVienTT));
            arrParams.Add(new DBHelpers.DataParameter("TrangThai", sTrangThai));
            arrParams.Add(new DBHelpers.DataParameter("TenHoiVien", sTenHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("HoDemCaNhan", sHoDemCaNhan));
            arrParams.Add(new DBHelpers.DataParameter("TenCaNhan", sTenCaNhan));
            arrParams.Add(new DBHelpers.DataParameter("IDHoiVien", sIDHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("SoDon", sSoDon));
            arrParams.Add(new DBHelpers.DataParameter("HinhThucNop", sHinhThucNop));
            arrParams.Add(new DBHelpers.DataParameter("MaTinhThanh", sMaTinhThanh));
            arrParams.Add(new DBHelpers.DataParameter("MaQuanHuyen", sMaQuanHuyen));
            arrParams.Add(new DBHelpers.DataParameter("CongtyKiemToan", sCongtyKiemToan));
            arrParams.Add(new DBHelpers.DataParameter("SoChungChiKTV", sSoChungChiKTV));
            arrParams.Add(new DBHelpers.DataParameter("SoDKKD", sSoDKKD));
            arrParams.Add(new DBHelpers.DataParameter("SoCNDuDieuKienHNKT", sSoCNDuDieuKienHNKT));
            arrParams.Add(new DBHelpers.DataParameter("NgayNopTu", sNgayNopTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayNopDen", sNgayNopDen));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyetTu", sNgayDuyetTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyetDen", sNgayDuyetDen));


            objDBHelper = ConnectionControllers.Connect("VACPA");

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_capnhathshoivien_search", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }

 

    protected void load_users()
    {
        try
        {
            DataSet ds = sp_YeuCauCapNhatSearch();

            DataView dt = ds.Tables[0].DefaultView;
             
            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;
            int i;
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "CapNhatHoSoHoiVien" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            ds = null;
            dt = null;




        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSoHoiVien", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }
    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }


    protected void btnTuChoiDuyet_Click(String sYeuCauCapNhatID, String sLyDoTuChoi, ref bool isSendMail)
    {
        try
        {
            int yeuCauCapNhatId = 0;
            yeuCauCapNhatId = Convert.ToInt32(sYeuCauCapNhatID);
            if (yeuCauCapNhatId != null && yeuCauCapNhatId != 0)
            {

                UpdateStatus(yeuCauCapNhatId,trangthaiTraLaiID,   cm.Admin_TenDangNhap,   DateTime.Now.ToString("ddMMyyyy"), sLyDoTuChoi);

               
                string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Từ chối phê duyệt cập nhật hồ sơ thành công.</div>";

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT A.MaYeuCau, B.Email, A.HoiVienTapTheID, B.TenDoanhNghiep FROM tblCapNhatHoSoHVTT A INNER JOIN tblHoiVienTapThe B ON A.HoiVienTapTheID = B.HoiVienTapTheID WHERE A.CapNhatHoSoHVTTID = " + yeuCauCapNhatId;
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

                // Gửi thông báo
                cm.GuiThongBao(dtb.Rows[0]["HoiVienTapTheID"].ToString(), "1", "Từ chối phê duyệt yêu cầu cập nhật thông tin", "Yêu cầu cập nhật thông tin (Mã yêu cầu : " + dtb.Rows[0]["MaYeuCau"].ToString() + ") của bạn không được phê duyệt<BR>" + "Lý do: " + sLyDoTuChoi);
                cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối phê duyệt yêu cầu cập nhật thông tin hội viên " + dtb.Rows[0]["TenDoanhNghiep"].ToString());
                // Send mail
                string body = "";
                body += "Yêu cầu cập nhật thông tin của công ty không được phê duyệt (Mã yêu cầu : " + dtb.Rows[0]["MaYeuCau"].ToString() + ")<BR>";
                body += "Lý do: " + sLyDoTuChoi;
                string msg = "";
                if (SmtpMail.Send("BQT WEB VACPA", dtb.Rows[0]["Email"].ToString(), "VACPA - Từ chối phê duyệt yêu cầu cập nhật thông tin", body, ref msg))
                {
                    isSendMail = true;
                }
                else
                {
                    isSendMail = false;

                }
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnPheDuyet_Click(String sYeuCauCapNhatID, ref bool isSendMail)
    {

        try
        {
            int yeuCauCapNhatId = 0;
            yeuCauCapNhatId = Convert.ToInt32(sYeuCauCapNhatID);
            if (yeuCauCapNhatId != null && yeuCauCapNhatId != 0)
            {

               
                UpdateStatus(yeuCauCapNhatId, trangthaiDaPheDuyet, cm.Admin_TenDangNhap, DateTime.Now.ToString("ddMMyyyy"), "");
                
                string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Phê duyệt cập nhật hồ sơ thành công.</div>";

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT A.MaYeuCau, B.Email, A.HoiVienTapTheID, B.TenDoanhNghiep FROM tblCapNhatHoSoHVTT A INNER JOIN tblHoiVienTapThe B ON A.HoiVienTapTheID = B.HoiVienTapTheID WHERE A.CapNhatHoSoHVTTID = " + yeuCauCapNhatId;
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

                // Gửi thông báo
                cm.GuiThongBao(dtb.Rows[0]["HoiVienTapTheID"].ToString(), "1", "Phê duyệt yêu cầu cập nhật thông tin", "Yêu cầu cập nhật thông tin (Mã yêu cầu : " + dtb.Rows[0]["MaYeuCau"].ToString() + ") của bạn đã được phê duyệt");
                cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt yêu cầu cập nhật thông tin hội viên " + dtb.Rows[0]["TenDoanhNghiep"].ToString());
                // Send mail
                string body = "";
                body += "Yêu cầu cập nhật thông tin của công ty đã được phê duyệt (Mã yêu cầu : " + dtb.Rows[0]["MaYeuCau"].ToString() + ")";

                string msg = "";

                if (SmtpMail.Send("BQT WEB VACPA", dtb.Rows[0]["Email"].ToString(), "VACPA - Phê duyệt yêu cầu cập nhật thông tin", body, ref msg))
                {
                    isSendMail = true;
                }
                else
                {
                    isSendMail = false;
                    //msg = "Lỗi không gửi được mail";
                    //ErrorMessage.Controls.Add(new LiteralControl(msg));
                }


            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }


    public bool UpdateStatus(int CapNhatHoSoHVTTID, int TinhTrangID,  String NguoiDuyet,
                         String NgayDuyet, String LyDoTuChoi)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("CapNhatHoSoHVTTID", CapNhatHoSoHVTTID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));


            objDBHelper = ConnectionControllers.Connect("VACPA");
            return objDBHelper.ExecFunction("sp_capnhathshoivien_tt_xetduyet", arrParams);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }
    

}