﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;

using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using CommonLayer;
using VACPA.Data.Bases;

public partial class usercontrols_QLHV_xoatenhoiviencanhan_add : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();


    string tuchoidon = "tuchoidon";
    string trinhpheduyet = "trinhpheduyet";
    string duyetdon = "duyetdon";
    string tuchoiduyet = "tuchoiduyet";
    string thoaiduyet = "thoaiduyet";

    public int trangthaiChoTiepNhanID = 1;
    public int trangthaiTuChoiID = 2;
    public int trangthaiChoDuyetID = 3;
    public int trangthaiTraLaiID = 4;
    public int trangthaiDaPheDuyet = 5;
    public int trangthaiThoaiDuyetID = 6;

    int trangthaiDangHoatDong = 1;
    int trangthaiNgungHoatDong = 0;



    string tk_DangHoatDong = "1";
    string tk_NgungHoatDong = "0";



    string tk_LoaiNQT = "0";
    string tk_LoaiHVCN = "1";
    string tk_LoaiKTV = "2";

    string tk_LoaiHVTT = "1";
    string tk_LoaiCTKT = "2";




    string delete = "delete";
    string canhan = "1";
    string tapthe = "2";

    protected void Page_Load(object sender, EventArgs e)
    {
        load_users(1);

        if (Request.QueryString["act"] != "")
        {

            //  action 
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {

                string[] lstObject = Request.QueryString["id"].Split(',');

                string lstIdCaNhan = "";
                string lstIdTinhTrangCaNhan = "";
                int countCaNhan = 0;
                foreach (string iterm in lstObject)
                {
                    string[] lstTemp = iterm.Split('|');
                    if (lstTemp.Length == 2)
                    {

                        if (countCaNhan == 0)
                        {
                            lstIdTinhTrangCaNhan = iterm;
                            string[] lstIterm = lstTemp[0].Split('|');
                            lstIdCaNhan = lstIterm[0];
                        }
                        else
                        {
                            lstIdTinhTrangCaNhan = lstIdTinhTrangCaNhan + "," + iterm;
                            string[] lstIterm = lstTemp[0].Split('|');
                            lstIdCaNhan = lstIdCaNhan + "," + lstIterm[0];
                        }

                        countCaNhan = countCaNhan + 1;

                    }
                }


                if (lstObject.Length > 0)
                {
                    // delete
                    if (Request.QueryString["act"] == "delete")
                    {
                        // string[] lstObjectId = lstIdTapThe.Split(',');
                        SqlXoaTenHoiVienProvider XoaTenHoiVien_provider = new SqlXoaTenHoiVienProvider(cm.connstr, true, "");
                        XoaTenHoiVien XoaTenHV = new XoaTenHoiVien();
                        foreach (string HoiVienID in lstObject)
                        {
                            XoaTenHV = new XoaTenHoiVien();
                            XoaTenHV.HoiVienId = Convert.ToInt32(HoiVienID);
                            XoaTenHV.LoaiHoiVien = "1";// hoi vien ca nhan
                            XoaTenHV.LyDoXoaTen = Request.QueryString["dieukienxoaten"];
                            XoaTenHV.GhiChuXoaTen = Request.QueryString["diengiai"];
                            XoaTenHV.TinhTrangXoaTenId = trangthaiChoDuyetID;
                            XoaTenHV.NgayNhap = DateTime.Now;
                            XoaTenHV.NguoiNhap = cm.Admin_HoVaTen;
                            XoaTenHV.LoaiXoaTen = Request.QueryString["loaixoaten"];//0: tự nguyện; 1: nợ phí 3 năm; 2: lý do khác
                            XoaTenHoiVien_provider.Insert(XoaTenHV);

                            cm.ghilog("XoaTenHoiVien", cm.Admin_TenDangNhap + " " + "Thêm mới đơn xin xóa tên hội viên cá nhân, HoiVienID: " + HoiVienID);
                        }
                        string resulte = "&resdelete=1";
                        Response.Redirect("admin.aspx?page=xoatenhoiviencanhan");

                    }

                }
            }
        }
    }
    public DataSet sp_KhachHangSearch(int i = 0)
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            bool isCaNhan = false; ;
            bool isKiemToanVien = false;
            bool isNguoiQuanTam = false;
            bool isTapThe = false;
            bool isCTyKiemToan = false;

            bool isDangHoatDong = false;
            bool isNgungHoatDong = false;

            String sGioiTinh = "%";
            
            String sChungChi = "%";
            

            String sLoaiHoiVienTT = "";
            String sLoaiHoiVienCN = "";

            String sLoaiDon = "";
            String sTrangThai = "";
            String sTenHoiVien = "";
            String sHoDemCaNhan = "";
            String sTenCaNhan = "";

            String sIDHoiVien = "";
            String sSoDon = "";
            String sHinhThucNop = "";
            String sMaTinhThanh = "";
            String sMaQuanHuyen = "";

            String sCongtyKiemToan = "";
            String sSoChungChiKTV = "";
            String sSoDKHN = "";
            String sSoCNDuDieuKienHNKT = "";

            String sNgayGiaNhapTu = "";
            String sNgayGiaNhapDen = "";
            String sNgayXoaTenTu = "";
            String sNgayXoaTenDen = "";

            String sDonViCongTac = "";

            bool isAll = false;
            if (Request.Form["tk_LyDoKhac"] != null && Request.Form["tk_LyDoKhac"] == "true")
            {
                isAll = true;
            }


            if (isAll || i == 1)
            {
                // Ten hoi vien
                if (Request.Form["tk_TenHoiVien"] != null)
                {
                    sTenHoiVien = Request.Form["tk_TenHoiVien"];
                }

                // Ten dang nhap
                if (Request.Form["tk_ID"] != null)
                {
                    sIDHoiVien = Request.Form["tk_ID"];
                }

                if (Request.Form["tk_Nam"] != null && Request.Form["tk_Nam"] == "true" && Request.Form["tk_Nu"] != null && Request.Form["tk_Nu"] == "false")
                    sGioiTinh = "1";
                if (Request.Form["tk_Nam"] != null && Request.Form["tk_Nam"] == "false" && Request.Form["tk_Nu"] != null && Request.Form["tk_Nu"] == "true")
                    sGioiTinh = "0";
                if (Request.Form["tk_Nam"] != null && Request.Form["tk_Nam"] == "false" && Request.Form["tk_Nu"] != null && Request.Form["tk_Nu"] == "false")
                    sGioiTinh = "x";
                
                //if (Request.Form["tk_ChungChi_Co"] != null && Request.Form["tk_ChungChi_Co"] == "true")
                //{
                //    sChungChi = "1";
                //}               

                // Ma Tinh
                if (Request.Form["TinhID_ToChuc"] != null)
                {
                    sMaTinhThanh = Request.Form["TinhID_ToChuc"];
                }
                // Ma Huyen
                if (Request.Form["HuyenID_ToChuc"] != null)
                {
                    sMaQuanHuyen = Request.Form["HuyenID_ToChuc"];
                }


                // So DKKD
                if (Request.Form["tk_SoDKHN"] != null)
                {
                    sSoDKHN = Request.Form["tk_SoDKHN"];
                }


                // Ngay nop tu
                if (Request.Form["tk_NgayNopTu"] != null)
                {
                    sNgayGiaNhapTu = Request.Form["tk_NgayNopTu"].Replace("/", ""); ;
                }

                // Ngay nop den
                if (Request.Form["tk_NgayNopDen"] != null)
                {
                    sNgayGiaNhapDen = Request.Form["tk_NgayNopDen"].Replace("/", "");
                }

                if (Request.Form["tk_DonViCongTac"] != null)
                {
                    sDonViCongTac = Request.Form["tk_DonViCongTac"];
                }

                List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();


                arrParams.Add(new DBHelpers.DataParameter("TenHoiVien", sTenHoiVien));
                arrParams.Add(new DBHelpers.DataParameter("IDHoiVien", sIDHoiVien));
                arrParams.Add(new DBHelpers.DataParameter("GioiTinh", sGioiTinh));
                
                arrParams.Add(new DBHelpers.DataParameter("ChungChi", sChungChi));
                
                arrParams.Add(new DBHelpers.DataParameter("MaTinhThanh", sMaTinhThanh));
                arrParams.Add(new DBHelpers.DataParameter("MaQuanHuyen", sMaQuanHuyen));

                arrParams.Add(new DBHelpers.DataParameter("SoDKHN", sSoDKHN));

                arrParams.Add(new DBHelpers.DataParameter("NgayGiaNhapTu", sNgayGiaNhapTu));
                arrParams.Add(new DBHelpers.DataParameter("NgayGiaNhapDen", sNgayGiaNhapDen));

                arrParams.Add(new DBHelpers.DataParameter("DonViCongTac", sDonViCongTac));

                objDBHelper = ConnectionControllers.Connect("VACPA");

                DataSet dsTemp = new DataSet();
                objDBHelper.ExecFunction("sp_hoivien_CN_search", arrParams, ref dsTemp);

                return dsTemp;

            }
            else
            {// hoi vien khong nop phi 3 nam
                List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

                objDBHelper = ConnectionControllers.Connect("VACPA");

                DataSet dsTemp = new DataSet();
                objDBHelper.ExecFunction("sp_hoivien_CN_nophi", arrParams, ref dsTemp);


                return dsTemp;
            }


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }





    protected void load_users(int j = 0)
    {
        try
        {



            DataSet ds = sp_KhachHangSearch(j);

            DataView dt = ds.Tables[0].DefaultView;


            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;
            int i;
            // danh sách trang
            Pager.Items.Clear();
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "DanhSachKhachHang" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            ds = null;
            dt = null;




        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DanhSachKhachHang", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }
    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }
    
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        load_users();
    }
    protected void Pager_SelectedIndexChanged(object sender, EventArgs e)
    {
        load_users();
    }
}