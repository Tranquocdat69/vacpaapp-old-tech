﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QLHV_DanhSachCongTyDangKyDangLogo_Update.ascx.cs"
    Inherits="usercontrols_QLHV_DanhSachCongTyDangKyDangLogo_Update" %>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<form id="Form1" name="Form1" clientidmode="Static" runat="server" method="post"
enctype="multipart/form-data">
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<div style="width: 95%; border: 1px gray solid; padding: 5px;">
    Tình trạng bản ghi: <span id="spanTinhTrangPheDuyet"></span>
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin đăng logo</legend>
    <input type="hidden" id="hdAction" name="hdAction" />
    <table style="width: 100%;" border="0" class="formtbl">
        <tr>
            <td style="min-width: 150px;">
                Đối tượng đăng logo<span class="starRequired">(*)</span>:
            </td>
            <td colspan="3">
                <input type="radio" id="rbtCongTyKiemToan" value="3" name="DoiTuongDangLogo" checked="checked"
                    onclick="CheckLoaiDoiTuongDangLogo();" />&nbsp;Công ty kiểm toán&nbsp;&nbsp;&nbsp;
                <input type="radio" id="rbtKhac" name="DoiTuongDangLogo" value="0" onclick="CheckLoaiDoiTuongDangLogo();" />&nbsp;Khác
            </td>
        </tr>
        <tr>
            <td>
                ID.HVTT/ID.CTKT<span class="starRequired">(*)</span>:
            </td>
            <td colspan="3">
                <input type="text" id="txtMaCongTy" name="txtMaCongTy" style="width: 100px;" onchange="CallActionGetInforCongTy();" />&nbsp;
                <input type="button" id="btnOpenFormChonCongTy" value="---" onclick="OpenDanhSachCongTy();"
                    style="border: 1px solid gray;" />
                <input type="hidden" id="hdCongTyID" name="hdCongTyID" />
            </td>
        </tr>
        <tr>
            <td>
                Tên đầy đủ<span class="starRequired">(*)</span>:
            </td>
            <td colspan="3">
                <input type="text" id="txtTenDayDu" name="txtTenDayDu" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Tên viết tắt<span class="starRequired">(*)</span>:
            </td>
            <td colspan="3">
                <input type="text" id="txtTenVietTat" name="txtTenVietTat" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Logo đăng lên website<span class="starRequired">(*)</span>:
            </td>
            <td colspan="3">
                <input type="file" id="FileLogo" name="FileLogo" style="width: 210px;"></input>&nbsp;&nbsp;
                <span id="spanTenFileLogo" style="font-style: italic;"></span>&nbsp;<a href="javascript:;"
                    id="btnDownload" target="_blank"></a>
            </td>
        </tr>
        <tr>
            <td>
                Kích thước logo<span class="starRequired">(*)</span>:
            </td>
            <td style="max-width: 150px;">
                <input type="text" id="txtKichThuocLogo" name="txtKichThuocLogo" />
            </td>
            <td style="min-width: 120px;">
                Vị trí<span class="starRequired">(*)</span>:
            </td>
            <td style="max-width: 150px;">
                <input type="text" id="txtViTri" name="txtViTri" />
            </td>
        </tr>
        <tr>
            <td>
                Ngày đăng<span class="starRequired">(*)</span>:
            </td>
            <td style="max-width: 150px;">
                <input type="text" id="txtNgayDang" name="txtNgayDang" style="width: 100px;" />
            </td>
            <td style="min-width: 120px;">
                Ngày hết hạn<span class="starRequired">(*)</span>:
            </td>
            <td style="max-width: 150px;">
                <input type="text" id="txtNgayHetHan" name="txtNgayHetHan" style="width: 100px;" />
            </td>
        </tr>
        <tr>
            <td>
                Phí đăng logo<span class="starRequired">(*)</span>:
            </td>
            <td style="max-width: 150px;">
                <select id="ddlPhiDangLogo" name="ddlPhiDangLogo" onchange="CheckSoTienPhi();">
                    <% GetListDMPhi();%>
                </select>
            </td>
            <td style="min-width: 120px;">
                Số tiền:
            </td>
            <td style="max-width: 150px;">
                <input type="text" id="txtSoTien" name="txtSoTien" readonly="readonly" style="width: 100px" />&nbsp;VNĐ
            </td>
        </tr>
        <tr>
            <td>
                Hạn nộp phí (trước ngày)<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtHanNopPhi" name="txtHanNopPhi" style="width: 100px;" />
            </td>
            <td>Đường dẫn:</td>
            <td>
                <input type="text" id="txtLink" name="txtLink" />
            </td>
        </tr>
    </table>
</fieldset>
<div style="text-align: center; width: 100%; margin-top: 10px;">
    <a id="btnSave" href="javascript:;" class="btn btn-rounded" onclick="Save();"><i
        class="iconfa-plus-sign"></i>Lưu</a> <span id="SpanCacNutChucNang" runat="server" Visible="False">&nbsp;
            <a id="btnDelete" href="javascript:;" class="btn" onclick="Delete();"><i class="iconfa-remove-sign">
            </i>Xóa</a>&nbsp;
            <asp:LinkButton ID="btnApprove" runat="server" CssClass="btn" 
        OnClientClick="return confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?');" 
        onclick="btnApprove_Click"><i class="iconfa-remove-sign"></i>Duyệt</asp:LinkButton>&nbsp;
            <asp:LinkButton ID="btnReject" runat="server" CssClass="btn" 
        OnClientClick="return confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?');" 
        onclick="btnReject_Click"><i class="iconfa-remove-sign"></i>Từ chối</asp:LinkButton>&nbsp;
            <asp:LinkButton ID="btnDontApprove" runat="server" CssClass="btn" 
        OnClientClick="return confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?');" 
        onclick="btnDontApprove_Click"><i class="iconfa-thumbs-down"></i>Thoái duyệt</asp:LinkButton>&nbsp;
        </span><a id="A1" href="javascript:;" class="btn btn-rounded" onclick="parent.CloseFormUpdate();">
            <i class="iconfa-save"></i>Đóng</a>
</div>
<iframe name="iframeProcess" width="0px" height="0px;"></iframe>
</form>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#txtNgayDang").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#txtNgayHetHan").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#txtHanNopPhi").datepicker({ dateFormat: 'dd/mm/yy' });
    
    $('#<%=Form1.ClientID %>').validate({
        rules: {
            hdCongTyID: {
                required: true
            },
            txtTenDayDu: {
                required: true
            },
            txtTenVietTat: {
                required: true
            },
            FileLogo:{
                required: true
            },
            txtKichThuocLogo: {
                required: true
            },
            txtViTri: {
                required: true
            },
            ddlPhiDangLogo: {
                required: true
            },
            txtNgayDang: {
                required: true, dateITA: true
            },
            txtNgayHetHan: {
                required: true, dateITA: true
            },
            txtHanNopPhi: {
                required: true, dateITA: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }
    });

    function CheckLoaiDoiTuongDangLogo() {
        if($('#rbtKhac').prop('checked')) {
            $('#txtMaCongTy').prop('readonly', true);
            $('#txtTenDayDu').prop('readonly', false);
            $('#txtTenVietTat').prop('readonly', false);
            $('#btnOpenFormChonCongTy').css('display', 'none');
            $('#txtMaCongTy').val('');
            $('#hdCongTyID').val('');
        }
        if($('#rbtCongTyKiemToan').prop('checked')) {
            $('#txtMaCongTy').prop('readonly', false);
            $('#txtTenDayDu').prop('readonly', true);
            $('#txtTenVietTat').prop('readonly', true);
            $('#btnOpenFormChonCongTy').css('display', '');
        }
    }
    
    function CheckSoTienPhi() {
        var phiId = $('#ddlPhiDangLogo option:selected').val();
        if(arrSoTienPhi.length > 0) {
            for (var i = 0; i < arrSoTienPhi.length; i++) {
                if(phiId == arrSoTienPhi[i][0])
                    $('#txtSoTien').val(arrSoTienPhi[i][1]);
            }
        }
    }
    
    function OpenDanhSachCongTy() {
        $("#DivDanhSachCongTyKiemToan").empty();
        $("#DivDanhSachCongTyKiemToan").append($("<iframe width='100%' height='100%' id='ifDanhSachCongTy' name='ifDanhSachCongTy' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hoivientapthe_minilist"));
        $("#DivDanhSachCongTyKiemToan").dialog({
            resizable: true,
            width: 750,
            height: 550,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách công ty</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    function CallActionGetInforCongTy() {
        var maCongTy = $('#txtMaCongTy').val();
        iframeProcess.location = '/iframe.aspx?page=QLHV_Process&mact=' + maCongTy + '&action=loadct';
    }
    
    function DisplayInforCongTy(value) {
        var arrValue = value.split(';#');
        $('#hdCongTyID').val(arrValue[0]);
        $('#txtMaCongTy').val(arrValue[1]);
        $('#txtTenDayDu').val(arrValue[2]);
        $('#txtTenVietTat').val(arrValue[3]);
    }
    
    function CloseFormDanhSachCongTy() {
        $("#DivDanhSachCongTyKiemToan").dialog('close');
    }

    function Save() {
        var ngayDang = $('#txtNgayDang').datepicker('getDate');
        var ngayHetHan = $('#txtNgayHetHan').datepicker('getDate');
        
        if(ngayDang != null && ngayHetHan != null && ngayHetHan < ngayDang) {
            $('#txtNgayHetHan').val('');
            $('#txtNgayHetHan').focus();
            alert('Ngày hết hạn phải lớn hơn hoặc bằng ngày đăng!');
            return;
        }
        $('#hdAction').val('update');
        if($('#rbtKhac').prop('checked'))
            $('#hdCongTyID').rules('remove');
        if($('#spanTenFileLogo').html() != '')
            $('#FileLogo').rules('remove');
        $('#btnSave').attr('onclick', '');
        $('#Form1').submit();
    }
    
    function Delete() {
        if(confirm('Bạn chắc chắn muốn xóa tài liệu này chứ?')) {
            $('#hdAction').val('delete');
            $('#FileLogo').rules('remove');
            $('#Form1').submit();    
        }
    }
    
    <% CheckPermissionOnPage();%>
</script>
<div id="DivDanhSachCongTyKiemToan">
</div>
