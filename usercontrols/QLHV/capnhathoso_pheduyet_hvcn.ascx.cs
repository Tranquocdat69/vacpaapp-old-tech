﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using HiPT.VACPA.DL;

public partial class usercontrols_capnhathoso_pheduyet_hvcn : System.Web.UI.UserControl
{
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;

    public string CapNhatHoSoID;
    public string TenFileBangChung;
    public string HoiVienCaNhanID;
    Commons cm = new Commons();
    public CapNhatHoSoHvcn capnhat;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
            LoadThongTin();
        //}
    }

    private void LoadThongTin()
    {
        SqlCapNhatHoSoHvcnProvider CapNhat_HoiVienCaNhan_provider = new SqlCapNhatHoSoHvcnProvider(connStr, true, "");
        capnhat = CapNhat_HoiVienCaNhan_provider.GetByCapNhatHoSoHvcnid(int.Parse(Request.QueryString["id"]));

        string sql = "SELECT E.Email, A.HoiVienCaNhanID, A.TenFileBangChung, A.CapNhatHoSoHVCNID, A.MaYeuCau, CONVERT(VARCHAR, A.NgayYeuCau, 103) AS NgayYeuCau, E.MaHoiVienCaNhan, A.SoGiayChungNhanDKHN, CONVERT(VARCHAR, A.NgayCapGiayChungNhanDKHN, 103) AS NgayCapGiayChungNhanDKHN, CONVERT(VARCHAR, A.HanCapTu, 103) AS HanCapTu, CONVERT(VARCHAR, A.HanCapDen, 103) AS HanCapDen, A.HocViNam, A.HocHamNam, B.TenChucVu, C.TenHocVi, D.TenHocHam";
        sql += " FROM tblCapNhatHoSoHVCN A LEFT JOIN tblDMChucVu B ON A.ChucVuID = B.ChucVuID";
        sql += " LEFT JOIN tblDMHocVi C ON A.HocViID = C.HocViID";
        sql += " LEFT JOIN tblDMHocHam D ON A.HocHamID = D.HocHamID";
        sql += " INNER JOIN tblHoiVienCaNhan E ON A.HoiVienCaNhanID = E.HoiVienCaNhanID";
        sql += " WHERE A.CapNhatHoSoHVCNID = " + Request.QueryString["id"];
        SqlCommand cmd = new SqlCommand(sql);

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        lbEmail.Text = dtb.Rows[0]["Email"].ToString();
        CapNhatHoSoID = Request.QueryString["id"];
        TenFileBangChung = dtb.Rows[0]["TenFileBangChung"].ToString();
        HoiVienCaNhanID = dtb.Rows[0]["HoiVienCaNhanID"].ToString();

        txtMaYeuCau.Text = dtb.Rows[0]["MaYeuCau"].ToString();
        txtNgayYeuCau.Text = dtb.Rows[0]["NgayYeuCau"].ToString();
        txtMaHoiVienCaNhan.Text = dtb.Rows[0]["MaHoiVienCaNhan"].ToString();
        txtSoGiayDHKN.Text = dtb.Rows[0]["SoGiayChungNhanDKHN"].ToString();
        NgayCap_DHKN.Value = dtb.Rows[0]["NgayCapGiayChungNhanDKHN"].ToString();
        HanCapTu.Value = dtb.Rows[0]["HanCapTu"].ToString();
        HanCapDen.Value = dtb.Rows[0]["HanCapDen"].ToString();
        txtChucVu.Text = dtb.Rows[0]["TenChucVu"].ToString();
        txtHocVi.Text = dtb.Rows[0]["TenHocVi"].ToString();
        txtNam_HocVi.Text = dtb.Rows[0]["HocViNam"].ToString();
        txtHocHam.Text = dtb.Rows[0]["TenHocHam"].ToString();
        txtNam_HocHam.Text = dtb.Rows[0]["HocHamNam"].ToString();

        // chứng chỉ
        LoadChungChi(dtb.Rows[0]["CapNhatHoSoHVCNID"].ToString());
        LoadKhenThuong(dtb.Rows[0]["CapNhatHoSoHVCNID"].ToString());
        LoadKyLuat(dtb.Rows[0]["CapNhatHoSoHVCNID"].ToString());

        if (Request.QueryString["mode"] == "view")
        {
            Response.Write("$('#form_nhaphoso input,select,textarea').attr('disabled', true);");
        }
    }

    private void LoadKyLuat(string ID)
    {
        string sql = "SELECT A.NgayThang, A.LyDo, B.TenHinhThucKyLuat AS TenHinhThuc FROM tblCapNhatHoSoHVCNKhenThuongKyLuat A INNER JOIN tblDMHinhThucKyLuat B ON A.HinhThucKyLuatID = B.HinhThucKyLuatID WHERE Loai = 2 AND A.CapNhatHoSoHVCNID = " + ID;
        SqlCommand cmd = new SqlCommand(sql);

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        gvKyLuat.DataSource = dtb;
        gvKyLuat.DataBind();
    }

    private void LoadKhenThuong(string ID)
    {
        string sql = "SELECT A.NgayThang, A.LyDo, B.TenHinhThucKhenThuong AS TenHinhThuc FROM tblCapNhatHoSoHVCNKhenThuongKyLuat A INNER JOIN tblDMHinhThucKhenThuong B ON A.HinhThucKhenThuongID = B.HinhThucKhenThuongID WHERE Loai = 1 AND A.CapNhatHoSoHVCNID = " + ID;
        SqlCommand cmd = new SqlCommand(sql);

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        gvKhenThuong.DataSource = dtb;
        gvKhenThuong.DataBind();
    }

    private void LoadChungChi(string ID)
    {
        string sql = "SELECT * FROM tblCapNhatHoSoHVCNChungChi WHERE CapNhatHoSoHVCNID = " + ID;
        SqlCommand cmd = new SqlCommand(sql);

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

        gvChungChi.DataSource = dtb;
        gvChungChi.DataBind();
    }

    protected void btnDuyet_Click(object sender, EventArgs e)
    {
        SqlCapNhatHoSoHvcnProvider CapNhat_HoiVienCaNhan_provider = new SqlCapNhatHoSoHvcnProvider(connStr, true, "");
        CapNhatHoSoHvcn capnhat = CapNhat_HoiVienCaNhan_provider.GetByCapNhatHoSoHvcnid(int.Parse(Request.QueryString["id"]));

        SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connStr, true, "");
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(capnhat.HoiVienCaNhanId.Value);
        HoiVienCaNhanID = capnhat.HoiVienCaNhanId.Value.ToString();
        if (!string.IsNullOrEmpty(capnhat.SoGiayChungNhanDkhn))
            hoivien.SoGiayChungNhanDkhn = capnhat.SoGiayChungNhanDkhn;
        if (capnhat.NgayCapGiayChungNhanDkhn != null)
            hoivien.NgayCapGiayChungNhanDkhn = capnhat.NgayCapGiayChungNhanDkhn;
        if (capnhat.HanCapTu != null)
            hoivien.HanCapTu = capnhat.HanCapTu;
        if (capnhat.HanCapDen != null)
            hoivien.HanCapDen = capnhat.HanCapDen;
        if (capnhat.ChucVuId != null)
            hoivien.ChucVuId = capnhat.ChucVuId;
        if (capnhat.HocViId != null)
            hoivien.HocViId = capnhat.HocViId;
        if (!string.IsNullOrEmpty(capnhat.HocViNam))
            hoivien.HocViNam = capnhat.HocViNam;
        if (capnhat.HocHamId != null)
            hoivien.HocHamId = capnhat.HocHamId;
        if (!string.IsNullOrEmpty(capnhat.HocHamNam))
            hoivien.HocHamNam = capnhat.HocHamNam;

        HoiVienCaNhan_provider.Update(hoivien);

        capnhat.NgayDuyet = DateTime.Now;
        capnhat.NguoiDuyet = cm.Admin_HoVaTen;
        capnhat.TinhTrangId = 5;
        CapNhat_HoiVienCaNhan_provider.Update(capnhat);

        string sql = "";
        SqlCommand cmd;
        // insert chứng chỉ
        sql = "SELECT SoChungChi, CONVERT(VARCHAR, NgayCap, 103) AS NgayCap FROM tblCapNhatHoSoHVCNChungChi WHERE CapNhatHoSoHVCNID = " + Request.QueryString["id"];
        cmd = new SqlCommand(sql);

        DataTable dtb_cc = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb_cc.Rows.Count != 0)
        {
            sql = "DELETE tblHoiVienCaNhanChungChiQuocTe WHERE HoiVienCaNhanId = " + HoiVienCaNhanID;
            cmd = new SqlCommand(sql);
            DataAccess.RunActionCmd(cmd);
            for (int i = 0; i < dtb_cc.Rows.Count; i++)
            {
                HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                chungchi.HoiVienCaNhanId = int.Parse(HoiVienCaNhanID);
                chungchi.SoChungChi = dtb_cc.Rows[i]["SoChungChi"].ToString();
                if (!string.IsNullOrEmpty(dtb_cc.Rows[i]["NgayCap"].ToString()))
                    chungchi.NgayCap = DateTime.ParseExact(dtb_cc.Rows[i]["NgayCap"].ToString(), "dd/MM/yyyy", null);
                HoiVienCaNhanChungChi_provider.Insert(chungchi);
            }
        }

        // insert khen thưởng
        
        sql = "SELECT HinhThucKhenThuongID, LyDo, CONVERT(VARCHAR, NgayThang, 103) AS NgayThang FROM tblCapNhatHoSoHVCNKhenThuongKyLuat WHERE Loai = 1 AND CapNhatHoSoHVCNID = " + Request.QueryString["id"];
        cmd = new SqlCommand(sql);

        DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb.Rows.Count != 0)
        {
            sql = "DELETE tblKhenThuongKyLuat WHERE Loai = 1 AND HoiVienCaNhanId = " + HoiVienCaNhanID;
            cmd = new SqlCommand(sql);
            DataAccess.RunActionCmd(cmd);

            sql = "";
            foreach (DataRow row in dtb.Rows)
            {
                sql += "INSERT INTO tblKhenThuongKyLuat (HinhThucKhenThuongID, HoiVienCaNhanID, Loai, NgayThang, LyDo) VALUES (";
                sql += row["HinhThucKhenThuongID"].ToString() + ", ";
                sql += HoiVienCaNhanID + ", '1', ";
                if (!string.IsNullOrEmpty(row["NgayThang"].ToString()))
                    sql += "'" + DateTime.ParseExact(row["NgayThang"].ToString(), "dd/MM/yyyy", null).ToString("yyyy-MM-dd") + "', ";
                sql += "N'" + row["LyDo"].ToString() + "');";
            }
            cmd = new SqlCommand(sql);
            DataAccess.RunActionCmd(cmd);
        }
        dtb.Clear();

        // insert kỷ luật
        
        sql = "SELECT HinhThucKyLuatID, LyDo, CONVERT(VARCHAR, NgayThang, 103) AS NgayThang FROM tblCapNhatHoSoHVCNKhenThuongKyLuat WHERE Loai = 2 AND CapNhatHoSoHVCNID = " + Request.QueryString["id"];
        cmd = new SqlCommand(sql);

        dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
        if (dtb.Rows.Count != 0)
        {
            sql = "DELETE tblKhenThuongKyLuat WHERE Loai = 2 AND HoiVienCaNhanId = " + HoiVienCaNhanID;
            cmd = new SqlCommand(sql);
            DataAccess.RunActionCmd(cmd);

            sql = "";
            foreach (DataRow row in dtb.Rows)
            {
                sql += "INSERT INTO tblKhenThuongKyLuat (HinhThucKyLuatID, HoiVienCaNhanID, Loai, NgayThang, LyDo) VALUES (";
                sql += row["HinhThucKyLuatID"].ToString() + ", ";
                sql += HoiVienCaNhanID + ", '2', ";
                if (!string.IsNullOrEmpty(row["NgayThang"].ToString()))
                    sql += "'" + DateTime.ParseExact(row["NgayThang"].ToString(), "dd/MM/yyyy", null).ToString("yyyy-MM-dd") + "', ";
                sql += "N'" + row["LyDo"].ToString() + "');";
            }
            cmd = new SqlCommand(sql);
            DataAccess.RunActionCmd(cmd);
        }

        // Gửi thông báo
        cm.GuiThongBao(capnhat.HoiVienCaNhanId.Value.ToString(), "1", "Phê duyệt yêu cầu cập nhật thông tin", "Yêu cầu cập nhật thông tin (Mã yêu cầu : " + capnhat.MaYeuCau + ") của bạn đã được phê duyệt");
        cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt yêu cầu cập nhật thông tin hội viên " + hoivien.HoDem + " " + hoivien.Ten);
        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Phê duyệt cập nhật hồ sơ thành công.</div>";

        // Send mail
        string body = "";
        body += "Yêu cầu cập nhật thông tin của anh/chị đã được phê duyệt (Mã yêu cầu : " + capnhat.MaYeuCau + ")";

        string msg = "";
        if (SmtpMail.Send("BQT WEB VACPA", lbEmail.Text, "Phê duyệt yêu cầu cập nhật thông tin", body, ref msg))
        {
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
        else
            placeMessage.Controls.Add(new LiteralControl(msg));
    }

    protected void btnTuChoi_Click(object sender, EventArgs e)
    {
        SqlCapNhatHoSoHvcnProvider CapNhat_HoiVienCaNhan_provider = new SqlCapNhatHoSoHvcnProvider(connStr, true, "");
        CapNhatHoSoHvcn capnhat = CapNhat_HoiVienCaNhan_provider.GetByCapNhatHoSoHvcnid(int.Parse(Request.QueryString["id"]));
        SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connStr, true, "");
        HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(capnhat.HoiVienCaNhanId.Value);

        capnhat.TinhTrangId = 2;
        capnhat.LyDoTuChoi = txtLyDoTuChoi.Text;
        CapNhat_HoiVienCaNhan_provider.Update(capnhat);

        // Gửi thông báo
        cm.GuiThongBao(capnhat.HoiVienCaNhanId.Value.ToString(), "1", "Từ chối phê duyệt yêu cầu cập nhật thông tin", txtLyDoTuChoi.Text);
        cm.ghilog("CapNhatHoSoHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối phê duyệt yêu cầu cập nhật thông tin hội viên " + hoivien.HoDem + " " + hoivien.Ten);
        string thongbao = @"<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Từ chối duyệt cập nhật hồ sơ thành công.</div>";

        // Send mail
        string body = "";
        body += "Yêu cầu cập nhật thông tin của anh/chị không được phê duyệt, lý do: " + txtLyDoTuChoi.Text;

        string msg = "";
        if (SmtpMail.Send("BQT WEB VACPA", lbEmail.Text, "Từ chối phê duyệt yêu cầu cập nhật thông tin", body, ref msg))
        {
            placeMessage.Controls.Add(new LiteralControl(thongbao));
        }
        else
            placeMessage.Controls.Add(new LiteralControl(msg));

    }

    protected void btnXemHoSo_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin.aspx?page=hosohoiviencanhan&id=" + HoiVienCaNhanID);
    }
}