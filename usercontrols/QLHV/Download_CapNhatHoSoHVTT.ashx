﻿<%@ WebHandler Language="C#" Class="Download_CapNhatHoSoHVTT" %>

using System;
using System.Web;

public class Download_CapNhatHoSoHVTT : IHttpHandler {

    public Commons cm = new Commons();
    VACPA.Entities.CapNhatHoSoHvtt FileDownload = new VACPA.Entities.CapNhatHoSoHvtt();
    public void ProcessRequest(HttpContext context)
    {
        try
        {

            byte[] data = null;
            string tenfile = "";
            string sAttachFileID = "";
            int isAttachFileID = 0;

            if (context.Request.QueryString["id"] != null && context.Request.QueryString["id"] != "")
            {
                sAttachFileID = context.Request.QueryString["id"];
                try
                {
                    if (!String.IsNullOrEmpty(sAttachFileID))
                    {
                        isAttachFileID = Convert.ToInt32(sAttachFileID);
                    }
                }
                catch
                {

                }

                if (isAttachFileID != 0)
                {

                    VACPA.Data.SqlClient.SqlCapNhatHoSoHvttProvider filedinhkem_provider = new VACPA.Data.SqlClient.SqlCapNhatHoSoHvttProvider(cm.connstr, true, "System.Data.OracleClient");
                    HttpPostedFile styledfileupload;

                    FileDownload = filedinhkem_provider.GetByCapNhatHoSoHvttid(isAttachFileID);

                }
            }

            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + FileDownload.TenFileBangChung + "\"");
            System.IO.BinaryWriter bw = new System.IO.BinaryWriter(context.Response.OutputStream);
            bw.Write((byte[])FileDownload.FileChungMinh);
            bw.Close();
            context.Response.End();


        }
        catch (Exception ex)
        {
            context.Response.ContentType = "text/plain"; context.Response.Write(ex.Message);
        }
        finally
        {
            context.Response.End();
        }


    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}