﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="danhsachkhachhang.ascx.cs" Inherits="usercontrols_danhsachkhachhang" %>

<script type="text/javascript"  src="/js/editor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/js/editor/ckeditor/adapters/jquery.js"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 
 <style>
        .ui-datepicker 
        {
            z-index: 99999999 !important;
        }
        
    </style>
   <form id="Form1" name="Form1" runat="server" clientidmode="Static">
   <h4 class="widgettitle">Danh sách khách hàng</h4>
   <div>
        <div  class="dataTables_length">
          

       
       <%-- <a id="btn_capnhat"  href="#none" class="btn btn-rounded" onclick="open_user_edit(Users_grv_selected);"><i class="iconsweets-create"></i> Cập nhật</a>--%>
        <a href="#none" class="btn btn-rounded" onclick="open_user_search();"><i class="iconfa-search"></i> Tìm kiếm</a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
        <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="confirm_delete_user(Users_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
  <a id="btn_guiemail"  href="#none" class="btn btn-rounded" onclick="confirm_email_user(Users_grv_selected);"><i class="iconfa-envelope"></i> Gửi email ID</a>
  <a id="btn_guithongbao"  href="#none" class="btn btn-rounded" onclick="confirm_thongbao_user(Users_grv_selected);"><i class="iconfa-envelope"></i> Gửi thông báo</a>
            <a id="btn_exportemail"  href="#none" class="btn btn-rounded" onclick="window.location = 'admin.aspx?page=getmail'"><i class="iconfa-envelope"></i> Export Email</a>
        </div>
        
   </div>
 
<asp:GridView ClientIDMode="Static" ID="Users_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" PageSize="30" 
       onpageindexchanging="Users_grv_PageIndexChanging" onsorting="Users_grv_Sorting"    
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval("HoiVienID")%>;<%# Eval("Loai")%>" />
                  </ItemTemplate>
              </asp:TemplateField>
            <asp:TemplateField  HeaderText="ID"   SortExpression="TenDangNhap" >
              <ItemTemplate>
                 <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:open_user_edit(" + DataBinder.Eval(Container.DataItem, "HoiVienID").ToString() + ","+ DataBinder.Eval(Container.DataItem, "Loai").ToString()+"); "%>   Text= '<%# Eval("TenDangNhap") %>'  runat="server">
                    
                </asp:HyperLink>
        
             </ItemTemplate>
             <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
             <ItemStyle   HorizontalAlign="Center"     />
          </asp:TemplateField>

           
              <asp:BoundField DataField="TenKhachHang" HeaderText="Tên khách hàng" 
                  SortExpression="TenKhachHang" />
                   <asp:BoundField DataField="DonVi" HeaderText="Đơn vị công tác" 
                  SortExpression="DonVi" />
                <asp:BoundField DataField="DoiTuong" HeaderText="Đối tượng" 
                  SortExpression="DoiTuong" />

              <asp:BoundField DataField="NgayGiaNhap" HeaderText="Ngày gia nhập" 
                  SortExpression="NgayGiaNhap" DataFormatString="{0:dd/MM/yyyy}" />
              <asp:BoundField DataField="NgayXoaTen" HeaderText="Ngày thôi gia nhập" 
                  SortExpression="NgayXoaTen" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="TinhTrang" HeaderText="Trạng thái" 
                  SortExpression="TinhTrang" />   
                  <asp:BoundField DataField="NgayCapNhat" HeaderText="Ngày cập nhật" 
                  SortExpression="NgayCapNhat" DataFormatString="{0:dd/MM/yyyy}" />
                  <asp:BoundField DataField="NguoiCapNhat" HeaderText="Người cập nhật" 
                  SortExpression="NguoiCapNhat" /> 
                  
              <asp:TemplateField HeaderText='Sửa' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                        <a onclick="open_user_edit(<%# Eval("HoiVienID")%> ,<%# Eval("Loai")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Sửa"  rel="tooltip" class="btn"><i class="iconsweets-create" ></i></a>
                     
                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText='Xóa' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                      <a onclick="confirm_delete_user(<%# Eval("HoiVienID")%>;<%# Eval("Loai")%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title=""  rel="tooltip" class="btn" ><i class="iconsweets-trashcan" ></i></a>                                                   

                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText='Kết xuất GCN' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      <%#(Eval("Loai").ToString() == "1") ? "<a onclick='open_user_export(" + Eval("HoiVienID") + ");' data-placement='top' data-rel='tooltip' href='#none' data-original-title='Kết xuất'  rel='tooltip' class='btn'><i class='iconsweets-word2' ></i></a>" : "<a onclick='open_user_export_hvtt(" + Eval("HoiVienID") + ");' data-placement='top' data-rel='tooltip' href='#none' data-original-title='Kết xuất'  rel='tooltip' class='btn'><i class='iconsweets-word2' ></i></a>"%>
                        
                     
                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>
            

          </Columns>
</asp:GridView>
<input type="hidden" id="HoiVienID" name="HoiVienID" />
<input type="hidden" id="HoiVienTapTheID" name="HoiVienTapTheID" />
<div class="dataTables_info" id="dyntable_info" >
<div class="pagination pagination-mini">Chuyển đến trang:
<ul>
 
<li><a href="#" onclick="$('#tranghientai').val(0); $('#user_search').submit();"><< Đầu tiên</a></li>
<li><a href="#" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager').val()-1); $('#user_search').submit();}" >< Trước</a></li>
<li><a style=" border: none; background-color:#eeeeee">
<asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server"
style=" width:55px; height:22px; "
onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
</asp:DropDownList></a> </li>
<li style="margin-left:5px; "><a href="#" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) {$('#tranghientai').val(parseInt($('#Pager').val())+1); $('#user_search').submit();} ;" style="border-left: 1px solid #ccc;">Sau ></a></li>
<li><a href="#" onclick="$('#tranghientai').val($('#Pager option').length-1); $('#user_search').submit();">Cuối cùng >></a></li>
</ul>
</div>
</div>

<%--<div class="dataTables_info" id="dyntable_info">Chuyển đến trang: 

<asp:DropDownList ID="Pager" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList>

</div>
--%>

</form>





<script type="text/javascript">

<% annut(); %>
    // Array ID được check
    var Users_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#Users_grv .checkall").bind("click", function () {
            Users_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#Users_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });

        $('#Users_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(Users_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) Users_grv_selected.push($(this).val());
            });
        });
    });

    function confirm_email_user(id) {
    
        $("#div_user_email").dialog({
            resizable: false,
            width: 350,
            height: 160,
            modal: true,
            buttons: {
                "Gửi": function () {
                 
                 window.location = 'admin.aspx?page=danhsachkhachhang&act=email&id=' + id;
                
                   
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_email').parent().find('button:contains("Gửi")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_email').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function confirm_thongbao_user(id) {
    
//        $("#div_thongbao").dialog({
//            resizable: false,
//            width: 950,
//            height: 620,
//            modal: true,
//            buttons: {
//                "Gửi": function () {
//                 var noidung = CKEDITOR.instances.NoiDungThongBao.getData();
//                 alert(noidung);
//                 //window.location = 'admin.aspx?page=danhsachkhachhang&act=thongbao&tieude=' + $('#TieuDeThongBao').val() + '&noidung=' + noidung + '&id=' + id;
//                //$('#id_hv').val(id);
//                //$(this).find("form#form_thongbao").submit();
//                   
//                },
//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });
        $("#div_thongbao").empty();
        $("#div_thongbao").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=thongbao_add&mode=iframe&id=" + id + "&time=" + timestamp));
        $("#div_thongbao").dialog({
            resizable: true,
            width: 850,
            height: 640,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Gửi thông báo cho hội viên</b>",
            modal: true,
            
            buttons: {

                "Gửi": function () {
                    window.frames['iframe_user_add'].submitform();
                   // $("#iframe_user_add")[0].contentWindow.submitform();
                   
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_thongbao').parent().find('button:contains("Gửi")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_thongbao').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    // Xác nhận xóa hội viên
    function confirm_delete_user(idxoa) {
     
        $("#div_user_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                 
                 window.location = 'admin.aspx?page=danhsachkhachhang&act=delete&id=' + idxoa;
                
                   
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_user_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_user_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Sửa thông tin hội viên
    function open_user_edit(id, loai ) {
        var timestamp = Number(new Date());
        if(loai==1)
        {
            openInNewTab("admin.aspx?page=hosohoiviencanhan&id=" + id + "&act=edit&time=" + timestamp);
        }else{
            openInNewTab("admin.aspx?page=hosohoivientapthe_edit&prepage=danhsachkhachhang&id=" + id + "&act=edit&time=" + timestamp);
        }
         
//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hosohoivientapthe_edit&prepage=danhsachkhachhang&id=" + id + "&mode=edit&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Sửa thông tin đơn hội viên</b>",
//            modal: true,
//            buttons: {

//                "Ghi": function () {
//                    window.frames['iframe_user_add'].submitform();
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Xem thông tin hội viên
    function open_user_view(id,loai) {
        var timestamp = Number(new Date());
       if(loai==1)// ca nhan
        {
           openInNewTab("admin.aspx?page=hoiviencanhan_edit&id=" + id + "&act=view&time=" + timestamp);
        }else{ // tap the
           openInNewTab("admin.aspx?page=hosohoivientapthe_edit&prepage=danhsachkhachhang&id=" + id + "&act=view&time=" + timestamp);
        }
     
//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hosohoivientapthe_edit&prepage=danhsachkhachhang&id=" + id + "&mode=view&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Xem thông tin đơn hội viên</b>",
//            modal: true,
//            buttons: {

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



    // mở form thêm hội viên tap the
    function open_user_add() {

  
        var timestamp = Number(new Date());
        openInNewTab("admin.aspx?page=hosohoivientapthe_edit&prepage=danhsachkhachhang&act=add&time=" + timestamp);

//        $("#div_user_add").empty();
//        $("#div_user_add").append($("<iframe width='100%' height='100%' id='iframe_user_add' name='iframe_user_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hosohoivientapthe_edit&prepage=danhsachkhachhang&mode=iframe&time=" + timestamp));
//        $("#div_user_add").dialog({
//            resizable: true,
//            width: 850,
//            height: 640,
//            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Thêm đơn gia nhập hội viên mới</b>",
//            modal: true,
//            close: function (event, ui) { window.location = 'admin.aspx?page=danhsachkhachhangtapthe'; },
//            buttons: {

//                "Ghi": function () {
//                    window.frames['iframe_user_add'].submitform();
//                   // $("#iframe_user_add")[0].contentWindow.submitform();
//                   
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_user_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_user_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
//   
   
    }
    function open_user_export(id) {
        
         $('#HoiVienID').val(id);

         $('#Form1').submit();
    }

    function open_user_export_hvtt(id) {

        $('#HoiVienTapTheID').val(id);

        $('#Form1').submit();
    }

      // mở form thêm hội viên ca nhan
    function open_hvcn_add() {

  
        var timestamp = Number(new Date());
        openInNewTab("admin.aspx?page=hoiviencanhan_add&act=add&time=" + timestamp);
       
    }

      //  danh sach cho tiep nhan online
    function tiepnhanonline_hv( ) {
         
        var timestamp = Number(new Date());
      window.location = "admin.aspx?page=danhsachkhachhang&act=online&time=" + timestamp
    }
    
    function xulydon_hv(lstid , act ) {
         
        window.location = 'admin.aspx?page=danhsachkhachhang&act='+act+'&id=' + lstid;

    }

     
    // Tìm kiếm
    function open_user_search() {
        var timestamp = Number(new Date());

        $("#div_user_search").dialog({
            resizable: true,
           width: 965,
            height: 700,
            title: "<img src='images/icons/user_business.png'>&nbsp;<b>Tìm kiếm thông tin hội viên</b>",
            modal: true,
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_user_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_user_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('#Users_grv').length > 0) jQuery('#Users_grv').tooltip({ selector: "a[rel=tooltip]" });


</script>

<div id="div_user_add" >
</div>

<div id="div_user_search"   style="display:none"    >
<form id="user_search" method="post" enctype="multipart/form-data" action="">
  <table  width="100%" border="0" class="formtbl"  >
    <%--<tr>
        <td  style="width:100px" ><label>Loại đơn: </label></td>
       
        <td  colspan="3" >  
        <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" ><input id="tk_HVCN" name="tk_HVCN" type="checkbox"  class="input-xlarge" value="true"  /> <label  >Hội viên cá nhân</label> </td>
          <td style="width:130px" ><input id="tk_HVTT" name="tk_HVTT" type="checkbox"  class="input-xlarge"  value="true"   /><label  style="width:100px" >Hội viên tổ chức</label> </td>
          <td style="width:130px" ></td>
          <td style="width:130px" ></td>
         </tr>
        </table>
                
                
         </td>
        
      
    </tr>--%>
      <tr>
        <td ><label>Tình trạng: </label></td>
       
          <td  colspan="3" >  
              <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" > <input id="tk_DangHoatDong" name="tk_DangHoatDong" type="checkbox"  class="input-xlarge checkall"  <%=Request.Form["tk_DangHoatDong"] != null ? "checked=\"checked\"" : "" %>  /><label  style="width:100px" >Đang hoạt động</label> 
                 </td>
          <td style="width:130px" >  <input id="tk_NgungHoatDong" name="tk_NgungHoatDong" type="checkbox"  class="checkbox"  <%=Request.Form["tk_NgungHoatDong"] != null ? "checked=\"checked\"" : "" %>  /><label  style="width:100px" >Ngừng hoạt động</label> 
                 </td>
          <td style="width:130px" > 
      </td>
          <td style="width:130px" ></td>
         </tr>
        </table>
                
                   </td>
       
    </tr>
     <tr>
        <td  style="width:100px" ><label>Loại khách hàng:</label></td>
       
        <td  colspan="3" >  
        <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" ><input id="tk_HVCN" name="tk_HVCN" type="checkbox" class="input-xlarge" <%=Request.Form["tk_HVCN"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Hội viên cá nhân</label> </td>
          <td style="width:130px" > <input id="tk_NQT" name="tk_NQT" type="checkbox"  class="checkbox"  <%=Request.Form["tk_NQT"] != null ? "checked=\"checked\"" : "" %>  /><label  style="width:100px" >Người quan tâm </label> </td>
          <td style="width:130px" ><input id="tk_KTV" name="tk_KTV" type="checkbox"  class="input-xlarge" <%=Request.Form["tk_KTV"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Kiểm toán viên</label></td>
          <td style="width:130px" ></td>
         </tr>
        </table>
                
                
         </td>
        
      
    </tr>
   
 
    <tr>
        <td></td>
         <td  colspan="3" >  
           <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" >   <input id="tk_HVTT" name="tk_HVTT" type="checkbox"  class="input-xlarge"  <%=Request.Form["tk_HVTT"] != null ? "checked=\"checked\"" : "" %>   /><label  style="width:100px" >Hội viên tổ chức</label> 
              </td>
          <td style="width:130px" >  <input id="tk_CTKT" name="tk_CTKT" type="checkbox"  class="input-xlarge" <%=Request.Form["tk_CTKT"] != null ? "checked=\"checked\"" : "" %>  /> <label  >C.ty kiểm toán</label>
                </td>
          <td style="width:130px" >   <input id="tk_ChiNhanh" name="tk_ChiNhanh" type="checkbox"  class="input-xlarge" <%=Request.Form["tk_ChiNhanh"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Chi nhánh</label>
            </td>
          <td style="width:130px" >  <input id="tk_XoaTen" name="tk_XoaTen" type="checkbox"  class="input-xlarge" <%=Request.Form["tk_XoaTen"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Hội viên bị xóa tên</label>
      </td>
         </tr>
        </table>
      
                </td>
    </tr>  

       <tr>
        <td  style="width:100px" ><label>Đối tượng:</label></td>
       
        <td  colspan="3" >  
        <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" ><input id="tk_HVCT" name="tk_HVCT" type="checkbox" class="input-xlarge" <%=Request.Form["tk_HVCT"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Hội viên chính thức</label> </td>
          <td style="width:130px" > <input id="tk_HVLK" name="tk_HVLK" type="checkbox"  class="checkbox"  <%=Request.Form["tk_HVLK"] != null ? "checked=\"checked\"" : "" %>  /><label  style="width:100px" >Hội viên liên kết </label> </td>
          <td style="width:130px" ><input id="tk_HVCC" name="tk_HVCC" type="checkbox"  class="input-xlarge" <%=Request.Form["tk_HVCC"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Hội viên cao cấp</label></td>
          <td style="width:130px" ></td>
         </tr>
        </table>
                
                
         </td>
        
      
    </tr>
   
 
    <tr>
        <td></td>
         <td  colspan="3" >  
           <table  width="100%"  border="0" >
         <tr>
          <td  style="width:130px" >   <input id="tk_HVDD" name="tk_HVDD" type="checkbox"  class="input-xlarge"  <%=Request.Form["tk_HVDD"] != null ? "checked=\"checked\"" : "" %>   /><label  style="width:100px" >Hội viên danh dự</label> 
              </td>
          <td style="width:130px" >  <input id="tk_HVCTrach" name="tk_HVCTrach" type="checkbox"  class="input-xlarge" <%=Request.Form["tk_HVCTrach"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Hội viên làm việc chuyên trách tại các Văn phòng VACPA</label>
                </td>
          <td style="width:130px" >   <input id="tk_HVNH" name="tk_HVNH" type="checkbox"  class="input-xlarge" <%=Request.Form["tk_HVNH"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Hội viên đã nghỉ hưu và không làm việc tại một công ty nào khác</label>
            </td>
          <td style="width:130px" >  <input id="tk_HVCQQL" name="tk_HVCQQL" type="checkbox"  class="input-xlarge" <%=Request.Form["tk_HVCQQL"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Hội viên đang làm việc tại cơ quan quản lý Nhà nước có liên quan đến nghề nghiệp, hoạt động của VACPA</label>
      </td>
         </tr>
        </table>
      
                </td>
    </tr> 
    
    <tr>
        <td><label>Tên khách hàng:</label></td>
        <td> 
         <input type="text" name="tk_TenHoiVien" id="tk_TenHoiVien" class="input-xlarge" value="<%=Request.Form["tk_TenHoiVien"] %>" />  </td>
         <td><label>ID khách hàng:</label></td>
 
         <td>
           
            <input type="text" name="tk_ID" id="tk_ID" class="input-xlarge" value="<%=Request.Form["tk_ID"] %>" />  
             </td>
        
         
    </tr>
    <tr>
        <td><label>Giới tính:</label></td>
        <td> <input id="checkNam" name="checkNam" type="checkbox"  class="input-xlarge" <%=Request.Form["checkNam"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Nam</label> 
        <input id="checkNu" name="checkNu" type="checkbox"  class="input-xlarge" <%=Request.Form["checkNu"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Nữ</label> 
           </td>
         <td><label>Có chứng chỉ nước ngoài:</label></td>
 
         <td>  
              <input id="checkCoCC" name="checkCoCC" type="checkbox"  class="input-xlarge" <%=Request.Form["checkCoCC"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Có</label> 
            <input id="checkKoCC" name="checkKoCC" type="checkbox"  class="input-xlarge" <%=Request.Form["checkKoCC"] != null ? "checked=\"checked\"" : "" %>  /> <label  >Không</label>       
         </td>
        
        
         
    </tr>
      <tr>
        <td><label>Năm sinh:</label></td>
        <td style="width:220px;">  
         <input  style="width:100px" type="text"  name="tk_NamSinhTu" id="tk_NamSinhTu" class="input-xlarge" value="<%=Request.Form["tk_NamSinhTu"] %>" />  
         - <input style="width:100px"  type="text" name="tk_NamSinhDen" id="tk_NamSinhDen" class="input-xlarge" value="<%=Request.Form["tk_NamSinhDen"] %>" />    </td>
       
        
         <td></td>
 
         <td>  
              
         </td>
         
         
    </tr>
    <tr>
        <td><label>Tỉnh thành:</label></td>
        <td>  <select  style="width:100%"  name="TinhID_ToChuc" id="TinhID_ToChuc">
              <% 
                  try{
                      cm.Load_ThanhPho(Request.Form["TinhID_ToChuc"]);
                  }
                  catch {
                      cm.Load_ThanhPho("00");
                  }
               %>
       </select> 
         </td>
         <td><label>Quận huyện:</label></td>
 
         <td>  
              
    <select id="HuyenID_ToChuc" style="width:100%"  name="HuyenID_ToChuc">        
             <%  
                     try
                     {
                         cm.Load_QuanHuyen(Request.Form["HuyenID_ToChuc"].ToString(), Request.Form["TinhID_ToChuc"].ToString());
                     }
                     catch
                     {
                         cm.Load_QuanHuyen("000", "00");
                     }
              
              %>  
    

</select>
    
         </td>
        
         
    </tr>
     <tr>
        <td><label>Ngày gia nhập:</label></td>
        <td style="width:220px;">  
         <input  style="width:100px" type="text"  name="tk_NgayNopTu" id="tk_NgayNopTu" class="input-xlarge" value="<%=Request.Form["tk_NgayNopTu"] %>" />  
         - <input style="width:100px"  type="text" name="tk_NgayNopDen" id="tk_NgayNopDen" class="input-xlarge" value="<%=Request.Form["tk_NgayNopDen"] %>" />    </td>
       
        
         <td><label>Đơn vị công tác:</label></td>
 
         <td>  
              <select  style="width:100%"  name="tk_CTKiemToan" id="tk_CTKiemToan">
              <%
                  try{
                      DonVi(Request.Form["tk_CTKiemToan"].ToString());
                  }
                  catch
                  {
                      DonVi("00"); 
                  }
              %>
       </select> 
         </td>
         
         
    </tr>

      <tr>
        <td><label>Ngày thôi gia nhập:</label></td>
        <td> 
        <input  style="width:100px" type="text" name="tk_NgayPheDuyetTu" id="tk_NgayPheDuyetTu" class="input-xlarge" value="<%=Request.Form["tk_NgayPheDuyetTu"] %>" />
         - <input  style="width:100px" type="text" name="tk_NgayPheDuyetDen" id="tk_NgayPheDuyetDen" class="input-xlarge" value="<%=Request.Form["tk_NgayPheDuyetDen"] %>" />  </td>
       
          <td><label>Số chứng chỉ KTV:</label></td>
 
         <td>   
         <input type="text" name="tk_SoChungChiKTV" id="tk_SoChungChiKTV" class="input-xlarge" value="<%=Request.Form["tk_SoChungChiKTV"] %>" /> </td>
       
        
       
         
    </tr>
     <tr>
        <td><label>Số giấy CN ĐKHNKT :</label></td>
        <td> 
          <input type="text" name="tk_CNDKHNKT" id="tk_CNDKHNKT" class="input-xlarge" value="<%=Request.Form["tk_CNDKHNKT"] %>" />   </td>
         <td><label>Số giấy CN ĐKKD:</label></td>
 
       <td><input type="text" name="tk_SoDKKD" id="tk_SoDKKD" class="input-xlarge" value="<%=Request.Form["tk_SoDKKD"] %>" /> 
        </td>
        
         
         
    </tr>

    

</table>
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
</form>
</div>


<script language="javascript">
 

  
      <% cm.chondiaphuong_script("ToChuc",0); %>

         $.datepicker.setDefaults($.datepicker.regional['vi']);


    $(function () {
        $("#tk_NgayNopTu").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"    ,
             onClose: function (selectedDate) {
               
                 $("#tk_NgayNopDen").datepicker("option", "minDate", selectedDate);

              }
                    
        }) ;

         $("#tk_NgayNopDen").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"     ,
             onClose: function (selectedDate) {
              
                $("#tk_NgayNopTu").datepicker("option", "maxDate", selectedDate);

            }
                 
        }) ;
         $("#tk_NgayPheDuyetTu").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"  ,
             onClose: function (selectedDate) {
               
                 $("#tk_NgayPheDuyetDen").datepicker("option", "minDate", selectedDate);

              }        
        }) ;
         $("#tk_NgayPheDuyetDen").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"       ,
             onClose: function (selectedDate) {
              
                $("#tk_NgayPheDuyetTu").datepicker("option", "maxDate", selectedDate);

            }     
        }) ;

    });
     
//    $(document).ready(function() {
//           
//            $('#tk_TTAll').click(function(event) {  //on click 
//                if(this.checked) { // check select status
//                   $('.checkbox').each(function() { //loop through each checkbox
//             
//                        this.checked = true;  //select all checkboxes with class "checkbox1"               
//                    }); 
//          
//        
//                }else{
//                      //$('#tk_TTChoTiepNhan').checked = false;
//                        $('#tk_TTChoTiepNhan').prop('checked',  false );
//                
//                        $('#tk_TTChoDuyet').prop('checked',  false );
//                        $('#tk_TTTuChoiTiepNhan').prop('checked',  false );
//                         $('#tk_TTTuChoiDuyet').prop('checked',  false );
//                        $('#tk_TTDaDuyet').prop('checked',  false );
//                        $('#tk_TTThoaiDuyet').prop('checked',  false );
// 
//                
//                }
//            });

//            $( '.checkbox' ).bind( "click", function() {
//                  if(this.checked==false) {
//                 
//                       $('#tk_TTAll').prop('checked',  false );
//                    }  else{
//                        
//                       if($('#tk_TTChoTiepNhan').is(':checked')==true && $('#tk_TTChoDuyet').is(':checked')==true &&
//                          $('#tk_TTTuChoiTiepNhan').is(':checked')==true &&$('#tk_TTTuChoiDuyet').is(':checked')==true &&
//                          $('#tk_TTDaDuyet').is(':checked')==true &&$('#tk_TTThoaiDuyet').is(':checked')==true ){
//                       
//                          $('#tk_TTAll').prop('checked',  true );
//                       }
//                    
//                    }  
//               });
//             

//          
//   });

 </script>

<div id="div_user_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa hội viên được chọn?</p>

</div>

<div id="div_user_email" style="display:none" title="<img src='images/icons/application_form_edit.png'> <b>Xác nhận gửi email</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Gửi email thông tin tài khoản cho những hội viên được chọn?</p>
    <%--<table  width="100%" border="0" class="formtbl"  >
    <tr> 
        <td  style="width:100px" ><label>Nội dung email: </label></td>
         <td  colspan="3" >  
           <textarea name="NoiDungEmail" id="NoiDungEmail" rows="4">Xin chào anh/chị/công ty. Anh/chị/công ty đã được kết nạp tài khoản hội viên tại trang tin điện tử VACPA. VACPA xin gửi thông tin tài khoản hội viên của anh/chị/công ty:</textarea>
                <label>Tên đăng nhập: </label><label style="font-weight:bold">Username</label>&nbsp;&nbsp;<label>Mật khẩu: </label><label style="font-weight:bold">password</label>
         </td>
        
      
    </tr>
    
    </table>--%>
</div>
<div id="div_thongbao"></div>
<%--<div id="div_thongbao" style="display:none" title="<img src='images/icons/application_form_edit.png'> <b>Gửi thông báo cho hội viên</b>">
<form id="form_thongbao" method="post" enctype="multipart/form-data" action="">
<input type="hidden" id="id_hv" name="id_hv" />
  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Gửi thông báo cho những hội viên được chọn?</p>
    <table  width="100%" border="0" class="formtbl"  >
    <tr> 
        <td  style="width:100px" ><label>Tiêu đề: </label></td>
         <td  colspan="3" >  
           <input type="text" id="TieuDeThongBao" name="TieuDeThongBao" />            
         </td>             
    </tr>
    <tr> 
        <td  style="width:100px" ><label>Nội dung thông báo: </label></td>
         <td  colspan="3" >  
           <textarea name="NoiDungThongBao" id="NoiDungThongBao" rows="8" cols="10" class="ckeditor"></textarea>              
         </td>             
    </tr>
    
    </table>
    </form>
</div>--%>


