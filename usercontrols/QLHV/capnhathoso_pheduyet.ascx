﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="capnhathoso_pheduyet.ascx.cs" Inherits="usercontrols_capnhathoso_pheduyet" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
 
  <div>
  <style type="text/css">
    .Hide
    {
      display:none;
    }
    
  
</style>

  
   <script type="text/javascript" src="../../js/chosen.jquery.min.js"></script>
   
      
 
 <style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>
  <script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
   <script type="text/javascript" src="js/mask.js"></script>
    
<script type="text/javascript">


    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });

    function reCalljScript() {
        $('.auto').autoNumeric('init');
        // Select with Search
        jQuery(".chzn-select").chosen();
        //   __doPostBack('', '');
        $.datepicker.setDefaults($.datepicker.regional['vi']);
  

        $("#<%=txtNgayCapGiayChungNhanDKKD.ClientID%> , #<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%>").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
        }).datepicker("option", "maxDate", '+0m +0w +0d');

        $("#<%=txtNgayCapGiayChungNhanDKKD.ClientID%> , #<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%>").mask("99/99/9999");
    


    }

     


</script>
 
 
 
 

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<% 
    if (!String.IsNullOrEmpty(cm.Admin_NguoiDungID))
    {
        //if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "CapNhatHoSo", cm.connstr).Contains("THEM|"))
        //{
        //    Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");
        //    return;
        //}
    }
    
    %>

 <div class="widgetbox">
    <h4 class="widgettitle">
       Yêu cầu thay đổi thông tin
        <a class="close">×</a>
        <a class="minimize">–</a></h4>
    <div class="widgetcontent">
    <form id="form_account_add"   runat="server"    >
    <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <div id="thongbaoloi_form_themuser" name="thongbaoloi_form_themuser" style="display:none" class="alert alert-error"></div>

     
			 
	 	<div class="writecomment">
            <asp:PlaceHolder ID="placeMessage" runat="server"></asp:PlaceHolder>
	  
            <div class="widget2">
         
                  <div class="menu_body">
            
            <div class="formRow">
          

                <table width="100%" class="formtbl"  >
                <tr class="trbgr">
                 <td colspan="4">
                  <h6>Thông tin yêu cầu</h6>
                  </td>
                </tr>
                   <tr  class="formRowTable" >
                         <td width="135"><label>Mã yêu cầu</label><span class="require">(*)</span></td>
                    <td>
                       <asp:TextBox ID="txtMaYeucau" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                       <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator5" runat="server" 
                       ControlToValidate="txtMaYeucau" SetFocusOnError="true"   ValidationGroup="CheckFileExt_YeuCauCNHS" 
                       ErrorMessage="Nhập thông tin mã yêu cầu."></asp:RequiredFieldValidator>
                 
                    </td>  
                    <td width="135"><label>Ngày yêu cầu</label>  </td>
                    <td>
                    
                       <asp:TextBox ID="txtNgayYeuCau" runat="server" Width="100%" ReadOnly="true" ></asp:TextBox>
                   
                        
                  </td>              
                    </tr>

             

                </table> 
                
            
         
            </div>
       
            </div>
            </div>
            <div class="widget2">
         
                  <div class="menu_body">
           
            <div class="formRow">

              <asp:UpdatePanel ID="UpdatePanelThongTinDN" runat="server" ><ContentTemplate>
            
             <script type="text/javascript" language="javascript">
                 Sys.Application.add_load(reCalljScript);
             </script>

                <table width="100%"  class="formtbl" >
                 <tr class="trbgr">
                 <td colspan="4">
                <h6>Thông tin chỉnh sửa</h6>
                  </td>
                </tr>
                <tr class="formRowTable">
                         <td width="135"><label>Tên doanh nghiệp</label></td>
                    <td colspan="3" >
                      <div class="formRight">
                      <asp:TextBox ID="txtTenDoanhNghiep" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                   
                        </div>
                    </td>  
                  </tr>
                                 
                    <tr class="formRowTable">
                         <td width="135"><label>Tên tiếng Anh</label></td>
                    <td>
                      <div class="formRight">
                      <asp:TextBox ID="txtTenTiengAnh" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                    <%--  <asp:Label ID="Label3" runat="server" Text="" Height="13px" > </asp:Label>--%>
                        </div>
                    </td>  
                    <td width="135"><label>Tên viết tắt</label></td>
                       <td style="text-align:left;">
                      <div class="formRight">
                     <asp:TextBox ID="txtTenVietTat" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                     </div>
                    </td>   
                                 
                    </tr>
                   
 

            
                    <tr class="formRowTable">
                    <td width="135"><label>Số giấy chứng nhận ĐKKD</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                      
                     <asp:TextBox ID="txtSoGiayChungNhanDKKD" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                     <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator12" runat="server" 
                             ControlToValidate="txtSoGiayChungNhanDKKD" SetFocusOnError="true" 
                               ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"  ErrorMessage="Nhập thông tin số giấy chứng nhận .">
                       </asp:RequiredFieldValidator>
                      </div>
                    </td>  
                    <td width="135"><label>Ngày cấp</label><span class="require">(*)</span></td>
                    <td> 
                     
                     <div class="formRight">
                      <asp:TextBox ID="txtNgayCapGiayChungNhanDKKD" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                      <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator13" runat="server"
                       ControlToValidate="txtNgayCapGiayChungNhanDKKD" SetFocusOnError="true"
                          ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"  ErrorMessage="Nhập thông tin ngày cấp"></asp:RequiredFieldValidator>
                     
                         <asp:RangeValidator ID="RangeValidatortxtNgayCapGiayChungNhanDKKD" runat="server" 
                    ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="31/12/9999"
                  ControlToValidate ="txtNgayCapGiayChungNhanDKKD"  SetFocusOnError="true" ></asp:RangeValidator>


                       </div>
                    
                  </td>              
                    </tr>

                    <tr class="formRowTable">
                    <td width="135"><label>Số giấy chứng nhận đủ điều kiện KDDV Kiểm toán</label></td>
                
                    <td>
                     <div class="formRight">
                      <asp:TextBox ID="txtSoGiayChungNhanKDDVKT" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                     
                     
                      <%--<asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtNgayCapGiayChungNhanDKKD" SetFocusOnError="true"   ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"  ErrorMessage=""></asp:RequiredFieldValidator>--%>
                      </div>
                  </td>              
                
                       <td width="135"><label>Ngày cấp</label></td>
                    <td> 
                     <div class="formRight">
                    <asp:TextBox ID="txtNgayCapGiayChungNhanKDDVKT" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                         <asp:RangeValidator ID="RangeValidatortxtNgayCapGiayChungNhanKDDVKT" runat="server" 
                    ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"  ErrorMessage="Nhập đúng ngày có thật và theo định dạng DD/MM/YYYY"
                  Display="Dynamic"
                  ForeColor="Red"
                  Type="Date"   Format="dd/MM/yyyy"
                  MinimumValue="01/01/0001"
                  MaximumValue="31/12/9999"
                  ControlToValidate ="txtNgayCapGiayChungNhanKDDVKT"  SetFocusOnError="true" ></asp:RangeValidator>

                   <%-- <asp:Label ID="Label10" runat="server" Text="" Height="13px" > </asp:Label>--%>
                    </div>
                    
                    </td>  

                
                    </tr>

                    <tr class="formRowTable">
                     
                    <td width="135"><label>Mã số thuế</label><span class="require">(*)</span></td>
                    <td>
                     <div class="formRight">
                    <asp:TextBox ID="txtMaSoThue" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                       <asp:RequiredFieldValidator  Display="Dynamic"    ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtMaSoThue" SetFocusOnError="true"   ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"  ErrorMessage="Nhập thông tin mã số thuế."></asp:RequiredFieldValidator>
                      </div>
                    </td>  
                


             
                    <td width="135"><label>Lĩnh vực hoạt động</label><span class="require">(*)</span></td>
                       <td style="text-align:left;">
                     <div class="formRight">
                      <asp:DropDownList ID="drLinhvucHoatDong"  ReadOnly="true" DataTextField="TenLinhVucHoatDong" DataValueField="LinhVucHoatDongID" runat="server" Width="228px" Height="30px"></asp:DropDownList>
                     <asp:RequiredFieldValidator  Display="Dynamic"   InitialValue="0"     ID="RequiredFieldValidator15" runat="server" ControlToValidate="drLinhvucHoatDong" SetFocusOnError="true"   ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"  ErrorMessage="Nhập thông tin lĩnh vực hoạt động."></asp:RequiredFieldValidator>
                      </div>
                    </td>              
                    </tr>
 

                    <tr align="left" >
                     <td colspan="4"  style=" text-align:left">
                     <asp:Label ID="Label21" runat="server" Text="" Height="13px" > </asp:Label>
                     <br />
                     <label   style="  font-weight: bold;  ">Thông tin tình hình hoạt động của công ty:  </label>
                     
                     </td>
                    </tr>
                   
      <tr class="formRowTable">
                     <td  ><label>Vốn điều lệ (VND)</label></td>   
                    <td>
                       <div class="formRight">
                       <asp:TextBox ID="txtVonDieuLe" runat="server"  ReadOnly="true"
                        class="auto" data-a-sep="."  data-a-dec="," data-v-max="999999999999" ></asp:TextBox> 
                    
                       </div>
                       </td>   
                     <td  ><label>Tổng doanh thu (VND)</label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtTongDoanhThu" runat="server"  ReadOnly="true"
                     class="auto" data-a-sep="."  data-a-dec="," data-v-max="999999999999"      ></asp:TextBox>
                 
                     </div>
                     </td>   

                    </tr>

                   
                                <tr class="formRowTable" >
                                
                      <td  ><label>Doanh thu dịch vụ kiểm toán (VND)</label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtDoanhThuDVKT" runat="server"  ReadOnly="true"
                     class="auto" data-a-sep="."  data-a-dec="," data-v-max="999999999999" ></asp:TextBox>
                  
                     </div>
                     
                     </td>   

                     <td  ><label>Tổng số khách hàng trong năm</label></td>   
                    <td>
                     <div class="formRight">
                     <asp:TextBox ID="txtTongSoKHTrongNam" runat="server"  ReadOnly="true"
                      class="auto" data-a-sep="."  data-a-dec="," data-v-max="999999999999" ></asp:TextBox>
                  
                     </div>
                     </td>   
   
                    
                    </tr>

             <tr>
             
                 <td  colspan="2" ><label>Số lượng kh là đơn vị có lợi ích công chúng thuộc lĩnh vực chứng khoán</label>
                        </td>
                        <td   colspan="2">
                         <div class="formRight">
                         <asp:TextBox ID="TongSoKhChungKhoan" runat="server"  ReadOnly="true"    class="auto" data-a-sep="."  data-a-dec="," data-v-max="999999999999" ></asp:TextBox>
                         </div>
                        </td>   
                        

             </tr>

                </table> 


                   </ContentTemplate></asp:UpdatePanel>  

            </div>

       

            </div>
            </div>

            <div class="widget2">
         
                  <div class="menu_body">
        

            </div>
            </div>
     
            <div class="widget2">
         
             <div class="menu_body">
                

               <div class="formRow">
                <table width="100%" class="formtbl" >
                   <tr class="trbgr">
                 <td colspan="4">
                  <h6>Danh sách chi nhánh văn phòng đại diện</h6>
                  </td>
                </tr>
                

                <tr>
                <td>
                               
                      
                            <asp:GridView ID="gvChiNhanh" runat="server" AutoGenerateColumns="false" 
                                Width="100%" CssClass="display"   onrowcommand="gvChiNhanh_RowCommand" 
                                onrowdeleting="gvChiNhanh_RowDeleting"  >
                                <Columns>
                                    <asp:BoundField DataField="STT" HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                      <asp:BoundField DataField="Ten" HeaderText="Tên chi nhánh/ VPĐD" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="TenLoai" HeaderText="Loại hình" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:BoundField DataField="DiaChi" HeaderText="Địa chỉ" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="DienThoai" HeaderText="Số điện thoại" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="EmailNguoiLh" HeaderText="Email" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                 <asp:BoundField DataField="DaiDienPlTen" HeaderText="Đại diện theo pháp luật" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                
                                   <asp:HyperLinkField  HeaderStyle-HorizontalAlign="Center"    DataTextField="ChiNhanhId"  Visible="false"  
                                        DataTextFormatString="&lt;a href='/Page/Usercontrols/hoivientapthe_yeucaucapnhat_cn.aspx?act=edit&ChiNhanhId={0}' class='floatbox' rev='width:max height:max   enableDragResize:true scrolling:auto controlPos:tr loadPageOnClose:?reload=true'&gt; &lt;img src='/Usercontrols/images/icons/color/pencil.png' border=0 &gt;" HeaderText="Sửa"
                                      >   
                                      <HeaderStyle HorizontalAlign="Center" Width="30px"></HeaderStyle>
                                     <ItemStyle HorizontalAlign="Center" />
                  
                   
                                    </asp:HyperLinkField >
        

                                    <asp:TemplateField HeaderStyle-Width="30"  HeaderText="Xóa"  ItemStyle-HorizontalAlign="Center"   Visible="false"  >
                                        <ItemTemplate>
                                            
                                                <asp:ImageButton ImageUrl="/images/cross.png" ID="btnDelete" runat="server" CommandArgument='<%#Bind("ChiNhanhId") %>'
                                                CommandName="Delete" OnClientClick="return confirm('Xác nhận xóa thông tin này?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                  

                                  <asp:BoundField DataField="ChiNhanhID"   Visible="false"  />
                                </Columns>
                            </asp:GridView>
                         
    
                </td>
                </tr>
               
                </table> 
            
              </div>
                 
           

           
               <div class="formRow">
                <table width="100%" class="formtbl" >
                   <tr class="trbgr">
                 <td colspan="4">
                  <h6>Khen thưởng</h6>
                  </td>
                </tr>
                

                <tr>
                <td>
                               
                           <asp:GridView ID="gvKhenThuong" runat="server" CssClass="display" EmptyDataText="Chưa có dữ liệu"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" 
                               Width="100%" 
                                OnRowCommand="gvKhenThuong_RowCommand" 
                                OnRowDeleting="gvKhenThuong_RowDeleting" OnRowDataBound="gvKhenThuong_RowDataBound" >
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Ngày tháng" HeaderStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNgayThang" runat="server"
                                                Text='<%#Bind("NgayThang") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cấp và hình thức" HeaderStyle-Width="300">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="drHinhThuc"
                                                runat="server" DataTextField="TenHinhThucKhenThuong" DataValueField="HinhThucKhenThuongID"
                                                >
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lý do" HeaderStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLyDo" runat="server" Text='<%#Bind("LyDo") %>'
                                                ></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               <asp:TemplateField HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center"   Visible="false"  >
                                        <ItemTemplate>
                                            
                                                <asp:ImageButton ImageUrl="/images/cross.png" ID="btnDelete" runat="server" CommandArgument='<%#Bind("KhenThuongKyLuatID") %>'
                                                CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            </Columns>
                            
                        </asp:GridView>
                         
    
                </td>
                </tr>
               
                </table> 
            
              </div>
                 
           

           
               <div class="formRow">
                <table width="100%" class="formtbl" >
                   <tr class="trbgr">
                 <td colspan="4">
                  <h6>Kỷ luật</h6>
                  </td>
                </tr>
                

                <tr>
                <td>
                               
                         <asp:GridView ID="gvKyLuat" runat="server" CssClass="display" EmptyDataText="Chưa có dữ liệu"
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" 
                            Width="100%" 
                            OnRowCommand="gvKyLuat_RowCommand" 
                                OnRowDeleting="gvKyLuat_RowDeleting" OnRowDataBound="gvKyLuat_RowDataBound" >
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Ngày tháng" HeaderStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNgayThang" runat="server"
                                                Text='<%#Bind("NgayThang") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cấp và hình thức" HeaderStyle-Width="300" >
                                        <ItemTemplate>
                                            <asp:DropDownList ID="drHinhThuc"
                                                runat="server" DataTextField="TenHinhThucKhenThuong" DataValueField="HinhThucKhenThuongID"
                                                >
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lý do" HeaderStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLyDo" runat="server" Text='<%#Bind("LyDo") %>'
                                                ></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               <asp:TemplateField HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center"   Visible="false"  >
                                        <ItemTemplate>
                                            
                                                <asp:ImageButton ImageUrl="/images/cross.png" ID="btnDelete" runat="server" CommandArgument='<%#Bind("KhenThuongKyLuatID") %>'
                                                CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
    
                </td>
                </tr>
               
                </table> 
            
              </div>
                 
              <div class="formRow">
                <table width="100%" class="formtbl" >
                   <tr class="trbgr">
                 <td colspan="4">
                   <h6>Tập tin đính kèm</h6>
                  </td>
                </tr>
                 <tr  class="formRowTable" >
                         <td width="135"><label>File chứng minh</label><span class="require">(*)</span></td>
                          <td align ="left" style="display:none;" >
                     
                           <div class="formRight">
                         <asp:FileUpload ID="FileUp"   name="FileUp" runat="server"  />
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Visible="false"
                               ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Jj][Pp][Gg])|.*\.([Bb][Mm][Pp])|.*\.([pP][nN][gG])|.*\.([pO][dD][fF])|.*\.([rR][aA][rR])|.*\.([zZ][iI][pP])|.*\.(?:[dD][oO][cC][xX]?)$)"
                               ControlToValidate="FileUp" Display="Dynamic" 
                                 ErrorMessage="Sai định dạng tập tin đính kèm"  ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"  SetFocusOnError="true" >
                                 </asp:RegularExpressionValidator>
                           <asp:RequiredFieldValidator id="RequiredFieldFileUpload" runat="server"
                              ControlToValidate="FileUp" Display="Dynamic"
                              ErrorMessage="Nhập tệp đính kèm."  ValidationGroup="CheckFileExt_YeuCauCNHS_YeuCauCNHS"
                              ForeColor="Red"
                              SetFocusOnError="true">
                            </asp:RequiredFieldValidator>

                  
                     </div>
                         </td>   
                       <td colspan="2" >
                       <asp:HyperLink ID="linkFileUpload"     runat="server">
                    <div style="width: 200px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis">
             
                   <asp:Label ID="lalTenFile"  runat="server" Text= "" />
                   </div>
                     </asp:HyperLink></td></tr>

 

                   
                    </table>
                    </div>

           
               <div class="formRow">
              <table width="100%" class="formtbl" >
                 <tr class="trbgr">
                   <td colspan="4">
                     <h6>Lịch sử thay đổi</h6>
                   </td>
                </tr>
              <tr> 
                <td>
                               
                      <asp:GridView ID="gvLichSu" runat="server" ClientIDMode="Static" CssClass="display"
                 EmptyDataText="Chưa có dữ liệu" 
                 Width="100%" 
                            ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="STT" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="MaYeuCau" SortExpression="MaYeuCau" HeaderText="Mã yêu cầu" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Width="150px" />
                                <asp:BoundField DataField="NgayYeuCau" SortExpression="NgayYeuCau" HeaderText="Ngày yêu cầu" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="NgayDuyet" SortExpression="NgayDuyet" HeaderText="Ngày phê duyệt" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="TenTrangThai" SortExpression="TenTrangThai" HeaderText="Trạng thái" ItemStyle-HorizontalAlign="Center" />
                                
                            </Columns>
                        </asp:GridView>   
    
                </td>
                </tr>
               
                </table> 
            
              </div>
                 
           


            </div>
         
             </div>

            <div class="widget2">
         
                   <div class="menu_body">
         
          <div class="formRow">
                <table width="100%" class="formtbl"   >
                 
                     <tr class="trbgr"    <% setDisplayLyDoxoa( ); %> >
                                    <td colspan="2">
                                     <h6>Lý do từ chối(Khi từ chối đơn)</h6>
                                     </td>
                            </tr>
                      <tr class="formRowTable"   <% setDisplayLyDoxoa( ); %>  >
                         <td width="135"><label>Lý do từ chối:</label><span class="require">(*)</span></td>
                         <td  >
                            <asp:TextBox ID="txtLyDoTuChoi" runat="server"  ></asp:TextBox><asp:RequiredFieldValidator  Display="Dynamic"   
                               ID="RequiredFieldtxtLyDoTuChoi" runat="server" ControlToValidate="txtLyDoTuChoi" SetFocusOnError="true" 
                                 ValidationGroup="CheckLyDoTuChoi"  ErrorMessage="Nhập lý do nếu từ chối đơn."></asp:RequiredFieldValidator>
                          </td>
                     
                     
                    </tr>

 

                      <tr>
                      <td colspan="4" align="center">
                      <p>  &nbsp;</p>
                   
                     <%--  <asp:HyperLink ID="linkGhi"  runat="server"  class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnGhi" runat="server" CssClass="blueB" Text="Lưu"
                           onclick="btnGhi_Click"  ValidationGroup="CheckFileExt"          />
                      </asp:HyperLink>
--%>
                       
                       <asp:HyperLink ID="linkTuChoiDuyet"  runat="server"   class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnTuChoiDuyet" runat="server" CssClass="blueB" Text="Từ chối duyệt"
                           onclick="btnTuChoiDuyet_Click"   ValidationGroup="CheckLyDoTuChoi"         />
                      </asp:HyperLink>
                       <asp:HyperLink ID="linkPheDuyet"  runat="server"   class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnPheDuyet" runat="server" CssClass="blueB" Text="Phê duyệt"
                           onclick="btnPheDuyet_Click"  ValidationGroup="NotCheck"          />
                       </asp:HyperLink>

                  <%--   <asp:HyperLink ID="linkThoaiDuyet"  runat="server"  class="btn btn-rounded"><i class="iconfa-ok"></i>
                          <asp:Button ID="btnThoaiDuyet" runat="server" CssClass="blueB" Text="Thoái duyệt"
                           onclick="btnThoaiDuyetClick"  ValidationGroup="NotCheck"          />
                      </asp:HyperLink>--%>

                      <a class="btn btn-rounded"><i class="iconfa-minus-sign"></i>
                        <input type="button" value="Quay lại" onclick="window.location.href = window.location.href.split('?')[0] + '?page=capnhathoso'" />
                       </a>
                  
                      <asp:HiddenField ID="hidId" runat="server"    />
                      <asp:HiddenField ID="hidTrangthaiId" runat="server" />
                      <asp:HiddenField ID="hidMode" runat="server" />
                      <asp:HiddenField ID="hidVacpaUser" runat="server" />
                     </td>
                       </tr>
                    </table>
                    </div>
                    </div>
                    </div>
                     
					
	   	    </div>
           
			 
    </form>
 
    </div>
</div>
           
      
      
       
 <script type="text/javascript">


        $.datepicker.setDefaults($.datepicker.regional['vi']);


        $("#<%=txtNgayCapGiayChungNhanDKKD.ClientID%> , #<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%>").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
        }).datepicker("option", "maxDate", '+0m +0w +0d');

        $("#<%=txtNgayCapGiayChungNhanDKKD.ClientID%> , #<%=txtNgayCapGiayChungNhanKDDVKT.ClientID%>").mask("99/99/9999");
    


       

        </script>
    </div>