﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using DBHelpers;
using System.Text.RegularExpressions;
using VACPA.Data.SqlClient;
using System.Configuration;
using VACPA.Entities;
using CommonLayer;
using VACPA.Data.Bases;
using System.Drawing;

public partial class usercontrols_ketnaphoivien : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();

    string login_link = System.Configuration.ConfigurationSettings.AppSettings["vacpaweblogin_link"];

    string tuchoidon = "tuchoidon";
    string trinhpheduyet = "trinhpheduyet";
    string duyetdon = "duyetdon";
    string tuchoiduyet = "tuchoiduyet";
    string thoaiduyet = "thoaiduyet";


    int trangthaiChoTiepNhanID = 1;
    int trangthaiTuChoiID = 2;
    int trangthaiChoDuyetID = 3;
    int trangthaiTraLaiID = 4;
    int trangthaiDaPheDuyet = 5;
    int trangthaiThoaiDuyetID = 6;


    string tk_TTChoTiepNhan = "1";
    string tk_TTTuChoiTiepNhan = "2";
    string tk_TTChoDuyet = "3";
    string tk_TTTuChoiDuyet = "4";
    string tk_TTDaDuyet = "5";
    string tk_TTThoaiDuyet = "6";




    string delete = "delete";
    string canhan = "1";
    string tapthe = "2";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

        Page.Master.FindControl("contentmessage").Controls.Clear();
        // Tên chức năng
        string tenchucnang = "Quản lý kết nạp hội viên ";
        // Icon CSS Class  
        string IconClass = "iconfa-book";

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            Form1.Visible = false;
            return;
        }

        if (string.IsNullOrEmpty(Request.Form["timkiem"]) || Request.Form["timkiem"] == "0")
        {
            if (!String.IsNullOrEmpty(Request.QueryString["msg"]))
            {
                Page.Master.FindControl("contentmessage").Controls.Add(
                    new LiteralControl("<div class='coloralert contact-success-block' style='margin-bottom:10px'>" +
                          Request.QueryString["msg"] + "</div>"));

            }

        }




        ///////////

        load_users();



        try
        {
            string msg = "";
            //string abc = Request.QueryString["ngay"];
            //  action 
            if (Request.QueryString["act"] != "")
            {

                if (!String.IsNullOrEmpty(Request.QueryString["id"]))
                {

                    string[] lstObject = Request.QueryString["id"].Split(',');
                    string lstIdCaNhan = "";
                    string lstIdTinhTrangCaNhan = "";
                    int countCanhan = 0;
                    string lstIdTapThe = "";
                    string lstIdTinhTrangTapThe = "";
                    int countTapThe = 0;
                    foreach (string iterm in lstObject)
                    {
                        string[] lstTemp = iterm.Split('-');
                        if (lstTemp.Length == 2)
                        {
                            if (lstTemp[1] == canhan)
                            {
                                if (countCanhan == 0)
                                {
                                    lstIdTinhTrangCaNhan = lstTemp[0];
                                    string[] lstIterm = lstTemp[0].Split('|');
                                    lstIdCaNhan = lstIterm[0];
                                }
                                else
                                {
                                    lstIdTinhTrangCaNhan = lstIdTinhTrangCaNhan + "," + lstTemp[0];
                                    string[] lstIterm = lstTemp[0].Split('|');
                                    lstIdCaNhan = lstIdCaNhan + "," + lstIterm[0];
                                }

                                countCanhan = countCanhan + 1;
                            }
                            else
                            {
                                if (countTapThe == 0)
                                {
                                    lstIdTinhTrangTapThe = lstTemp[0];
                                    string[] lstIterm = lstTemp[0].Split('|');
                                    lstIdTapThe = lstIterm[0];
                                }
                                else
                                {
                                    lstIdTinhTrangTapThe = lstIdTinhTrangTapThe + "," + lstTemp[0];
                                    string[] lstIterm = lstTemp[0].Split('|');
                                    lstIdTapThe = lstIdTapThe + "," + lstIterm[0];
                                }

                                countTapThe = countTapThe + 1;
                            }
                        }
                    }
                    if (lstIdTinhTrangCaNhan.Length > 0)//   ca nhan
                    {
                        // delete
                        if (Request.QueryString["act"] == "delete")
                        {
                            // sp_DonHoiVienDelete(lstIdCaNhan);
                            cm.XoaDonXinKetNapHoiVienCaNhan(lstIdCaNhan);
                            msg = "Xóa thông tin thành công";

                            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Xóa đơn xin kết nạp hội viên, ID:" + lstIdCaNhan);
                        }

                        // tu choi don 
                        if (Request.QueryString["act"] == tuchoidon)
                        {
                            String sLydotuchoi = Request.QueryString["lydo"];
                            string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                            bool isSendMail = true;
                            foreach (string sDonHoiVien in lstDonHoiVien)
                            {
                                string[] lstIterm = sDonHoiVien.Split('|');
                                String sDonHoiVienID = lstIterm[0];
                                String sDonHoiVienTinhtrang = lstIterm[1];
                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoTiepNhan)
                                {
                                    // btnTuChoiDon_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                    PheDuyetHoiVien(sDonHoiVienID, tk_TTTuChoiTiepNhan, ref isSendMail, sLydotuchoi);
                                }
                            }

                            if (!isSendMail)
                            {

                            }
                            msg = "Từ chối đơn thành công";
                        }

                        // tu choi duyet 
                        if (Request.QueryString["act"] == tuchoiduyet)
                        {
                            String sLydotuchoi = Request.QueryString["lydo"];
                            string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                            bool isSendMail = true;
                            foreach (string sDonHoiVien in lstDonHoiVien)
                            {
                                string[] lstIterm = sDonHoiVien.Split('|');
                                String sDonHoiVienID = lstIterm[0];
                                String sDonHoiVienTinhtrang = lstIterm[1];
                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                {
                                    // btnTuChoiDuyet_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                    PheDuyetHoiVien(sDonHoiVienID, tk_TTTuChoiDuyet, ref isSendMail, sLydotuchoi);
                                }
                            }
                            if (!isSendMail)
                            {

                            }
                            msg = "Từ chối duyệt thành công";
                        }

                        // trinh phe duyet 
                        if (Request.QueryString["act"] == trinhpheduyet)
                        {
                            string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                            string sLydotuchoi = "";
                            bool isSendMail = true;
                            foreach (string sDonHoiVien in lstDonHoiVien)
                            {
                                string[] lstIterm = sDonHoiVien.Split('|');
                                String sDonHoiVienID = lstIterm[0];
                                String sDonHoiVienTinhtrang = lstIterm[1];
                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoTiepNhan)
                                {

                                    // btnTrinhPheDuyet_Click(sDonHoiVienID);
                                    PheDuyetHoiVien(sDonHoiVienID, tk_TTChoDuyet, ref isSendMail, sLydotuchoi);

                                }
                            }
                            msg = "Trình phê duyệt thành công";
                        }


                        //  phe duyet 
                        if (Request.QueryString["act"] == duyetdon)
                        {
                            string soQD = Request.QueryString["so"];
                            string ngayQD = Request.QueryString["ngay"];
                            string loaiHVCN = Request.QueryString["loaiHVCN"];
                            //List<string> lsttemp = new List<string>();
                            // string strtem[] = ngayQD.Split('/');
                            if (loaiHVCN != "-1" && loaiHVCN != "-2")
                            {
                                string[] lstDonHoiVien = lstIdTinhTrangCaNhan.Split(',');
                                bool isSendMail = true;
                                foreach (string sDonHoiVien in lstDonHoiVien)
                                {
                                    string[] lstIterm = sDonHoiVien.Split('|');
                                    String sDonHoiVienID = lstIterm[0];
                                    String sDonHoiVienTinhtrang = lstIterm[1];
                                    if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                    {
                                        // btnPheDuyet_Click(sDonHoiVienID, ref isSendMail);
                                        PheDuyetHoiVien(sDonHoiVienID, tk_TTDaDuyet, ref isSendMail, "", soQD, ngayQD, loaiHVCN);
                                    }
                                }
                                if (!isSendMail)
                                {

                                }
                                msg = "Duyệt đơn thành công";
                            }
                            else
                                msg = "Bạn chưa chọn đối tượng HVCN";
                        }
                    }

                    if (lstIdTinhTrangTapThe.Length > 0)//    tap the
                    {
                        // delete
                        if (Request.QueryString["act"] == "delete")
                        {
                            sp_DonHoiVienDelete(lstIdTapThe);
                            msg = "Xóa thông tin thành công";

                            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Xóa đơn xin kết nạp hội viên, ID:" + lstIdTapThe);
                        }

                        // tu choi don 
                        if (Request.QueryString["act"] == tuchoidon)
                        {
                            String sLydotuchoi = Request.QueryString["lydo"];
                            string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                            bool isSendMail = true;
                            foreach (string sDonHoiVien in lstDonHoiVien)
                            {
                                string[] lstIterm = sDonHoiVien.Split('|');
                                String sDonHoiVienID = lstIterm[0];
                                String sDonHoiVienTinhtrang = lstIterm[1];
                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoTiepNhan)
                                {
                                    btnTuChoiDon_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                }
                            }

                            if (!isSendMail)
                            {

                            }

                            msg = "Từ chối đơn thành công";
                        }

                        // tu choi duyet 
                        if (Request.QueryString["act"] == tuchoiduyet)
                        {
                            String sLydotuchoi = Request.QueryString["lydo"];
                            string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                            bool isSendMail = true;
                            foreach (string sDonHoiVien in lstDonHoiVien)
                            {
                                string[] lstIterm = sDonHoiVien.Split('|');
                                String sDonHoiVienID = lstIterm[0];
                                String sDonHoiVienTinhtrang = lstIterm[1];
                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                {
                                    btnTuChoiDuyet_Click(sDonHoiVienID, sLydotuchoi, ref isSendMail);
                                }
                            }
                            if (!isSendMail)
                            {

                            }

                            msg = "Từ chối duyệt thành công";
                        }

                        // trinh phe duyet 
                        if (Request.QueryString["act"] == trinhpheduyet)
                        {
                            string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');

                            foreach (string sDonHoiVien in lstDonHoiVien)
                            {
                                string[] lstIterm = sDonHoiVien.Split('|');
                                String sDonHoiVienID = lstIterm[0];
                                String sDonHoiVienTinhtrang = lstIterm[1];
                                if (sDonHoiVienTinhtrang.Trim() == tk_TTChoTiepNhan)
                                {

                                    btnTrinhPheDuyet_Click(sDonHoiVienID);
                                }
                            }

                            msg = "Trình phê duyệt thành công";
                        }


                        //  phe duyet 
                        if (Request.QueryString["act"] == duyetdon)
                        {
                            string soQD = Request.QueryString["so"];
                            string ngayQD = Request.QueryString["ngay"];
                            string loaiHVTT = Request.QueryString["loaiHVTT"];
                            if (loaiHVTT != "-1")
                            {
                                string[] lstDonHoiVien = lstIdTinhTrangTapThe.Split(',');
                                bool isSendMail = true;
                                foreach (string sDonHoiVien in lstDonHoiVien)
                                {
                                    string[] lstIterm = sDonHoiVien.Split('|');
                                    String sDonHoiVienID = lstIterm[0];
                                    String sDonHoiVienTinhtrang = lstIterm[1];
                                    if (sDonHoiVienTinhtrang.Trim() == tk_TTChoDuyet)
                                    {
                                        btnPheDuyet_Click(sDonHoiVienID, ref isSendMail, soQD, ngayQD, loaiHVTT);
                                    }
                                }
                                if (!isSendMail)
                                {

                                }
                                msg = "Phê duyệt thông tin thành công";
                            }
                            else
                                msg = "Bạn chưa chọn đối tượng HVTC";
                        }

                    }


                    //Response.Redirect("admin.aspx?page=KetNapHoiVien&msg=" + msg);
                }


            }


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }

    public void PheDuyetHoiVien(string DonHoiVienCaNhanID, string TinhTrangID, ref bool IsSendMail, string LyDoTuChoi = "", string soQD = "", string ngayQD = "", string loai = "0") // TinhTrangID: 3-tiếp nhận, 2-từ chối, 4-từ chối duyệt, 5-phê duyệt;
    {
        string connstr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
        SqlDonHoiVienCaNhanProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienCaNhanProvider(connstr, true, "");
        DonHoiVienCaNhan donhoivien = DonHoiVienCaNhan_provider.GetByDonHoiVienCaNhanId(int.Parse(DonHoiVienCaNhanID));

        if (TinhTrangID == "3")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 3, NgayTiepNhan = '" + DateTime.Now.ToString("yyyy-MM-dd") + "', NguoiTiepNhan = N'" + cm.Admin_TenDangNhap + "' WHERE DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;

            DataAccess.RunActionCmd(cmd);

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Tiếp nhận đơn xin kết nạp hội viên " + donhoivien.HoDem + " " + donhoivien.Ten);
        }

        if (TinhTrangID == "2")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 2, LyDoTuChoi = N'" + LyDoTuChoi + "' WHERE DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;

            DataAccess.RunActionCmd(cmd);

            cmd = new SqlCommand();
            cmd.CommandText = "SELECT A.HoiVienCaNhanID, B.LoaiHoiVien FROM tblDonHoiVienCaNhan A INNER JOIN tblNguoiDung B ON A.HoiVienCaNhanID = B.HoiVienID WHERE A.DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;
            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

            // Gửi mail

            string body = "";
            body += "Đơn xin kết nạp hội viên cá nhân của anh/chị không được tiếp nhận. Lý do: ";
            body += LyDoTuChoi + "<BR/>";
            string http_Server = (string)System.Configuration.ConfigurationSettings.AppSettings["http_Server"];
            body += "Anh/chị có thể cập nhật lại thông tin đơn theo đường links : " + http_Server + "Page/DangKyHoiVien/DangKyHoiVienCaNhan.aspx?id=" + DonHoiVienCaNhanID + " <BR/>";
            if (dtb.Rows.Count != 0)
            {
                // Gửi thông báo
                cm.GuiThongBao(dtb.Rows[0]["HoiVienCaNhanID"].ToString(), dtb.Rows[0]["LoaiHoiVien"].ToString(), "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ tiếp nhận đơn xin kết nạp hội viên cá nhân", body);
            }
            string msg = "";
            IsSendMail = SmtpMail.Send("BQT WEB VACPA", donhoivien.Email, "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ tiếp nhận đơn xin kết nạp hội viên cá nhân", body, ref msg);

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối tiếp nhận đơn xin kết nạp hội viên " + donhoivien.HoDem + " " + donhoivien.Ten);
        }

        if (TinhTrangID == "4")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE tblDonHoiVienCaNhan SET TinhTrangID = 4, LyDoTuChoi = N'" + LyDoTuChoi + "', NguoiDuyet = '" + cm.Admin_TenDangNhap + "', NgayDuyet = '" + DateTime.Now.ToString("yyyy-MM-dd") + "' WHERE DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;

            DataAccess.RunActionCmd(cmd);

            cmd = new SqlCommand();
            cmd.CommandText = "SELECT A.HoiVienCaNhanID, B.LoaiHoiVien FROM tblDonHoiVienCaNhan A INNER JOIN tblNguoiDung B ON A.HoiVienCaNhanID = B.HoiVienID WHERE A.DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;
            DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];

            // Gửi mail

            string body = "";
            body += "Đơn xin kết nạp hội viên cá nhân của anh/chị không được phê duyệt. Lý do: ";
            body += LyDoTuChoi + "<BR/>";
            string http_Server = (string)System.Configuration.ConfigurationSettings.AppSettings["http_Server"];
            body += "Anh/chị có thể cập nhật lại thông tin đơn theo đường links : " + http_Server + "Page/DangKyHoiVien/DangKyHoiVienCaNhan.aspx?id=" + DonHoiVienCaNhanID + " <BR/>";
            if (dtb.Rows.Count != 0)
            {
                // Gửi thông báo
                cm.GuiThongBao(dtb.Rows[0]["HoiVienCaNhanID"].ToString(), dtb.Rows[0]["LoaiHoiVien"].ToString(), "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ tiếp nhận đơn xin kết nạp hội viên cá nhân", body);
            }
            string msg = "";
            IsSendMail = SmtpMail.Send("BQT WEB VACPA", donhoivien.Email, "VACPA - Yêu cầu sửa đổi, bổ sung hồ sơ phê duyệt đơn xin kết nạp hội viên cá nhân", body, ref msg);

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối phê duyệt đơn xin kết nạp hội viên " + donhoivien.HoDem + " " + donhoivien.Ten);
        }

        if (TinhTrangID == "5")
        {
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = "UPDATE tblDonHoiVienCaNhan SET LoaiHoiVienCaNhanChiTiet = " + loai + ", TinhTrangID = 5, NguoiDuyet = '" + cm.Admin_TenDangNhap + "', NgayDuyet = '" + DateTime.Now.ToString("yyyy-MM-dd") + "' WHERE DonHoiVienCaNhanID = " + DonHoiVienCaNhanID;

            DataAccess.RunActionCmd(cmd2);

            SqlDonHoiVienCaNhanChungChiProvider DonHoiVienCaNhanChungChi_provider = new SqlDonHoiVienCaNhanChungChiProvider(connstr, true, "");
            SqlDonHoiVienCaNhanQuaTrinhCongTacProvider DonHoiVienCaNhanQuaTrinh_provider = new SqlDonHoiVienCaNhanQuaTrinhCongTacProvider(connstr, true, "");
            SqlDonHoiVienFileProvider DonHoiVienFile_provider = new SqlDonHoiVienFileProvider(connstr, true, "");

            SqlHoiVienCaNhanProvider HoiVienCaNhan_provider = new SqlHoiVienCaNhanProvider(connstr, true, "");
            SqlHoiVienCaNhanChungChiQuocTeProvider HoiVienCaNhanChungChi_provider = new SqlHoiVienCaNhanChungChiQuocTeProvider(connstr, true, "");
            SqlHoiVienCaNhanQuaTrinhCongTacProvider HoiVienCaNhanQuaTrinh_provider = new SqlHoiVienCaNhanQuaTrinhCongTacProvider(connstr, true, "");
            SqlHoiVienFileProvider HoiVienFile_provider = new SqlHoiVienFileProvider(connstr, true, "");

            if (donhoivien.HoiVienCaNhanId != null)
            {
                #region người quan tâm và kiểm toán viên
                HoiVienCaNhan hoivien = HoiVienCaNhan_provider.GetByHoiVienCaNhanId(donhoivien.HoiVienCaNhanId.Value);
                hoivien.LoaiHoiVienCaNhanChiTiet = int.Parse(loai);
                hoivien.LoaiHoiVienCaNhan = "1";
                hoivien.HoDem = donhoivien.HoDem;
                hoivien.Ten = donhoivien.Ten;
                hoivien.GioiTinh = donhoivien.GioiTinh;
                hoivien.NgaySinh = donhoivien.NgaySinh;
                hoivien.QuocTichId = donhoivien.QuocTichId;
                hoivien.QueQuanTinhId = donhoivien.QueQuanTinhId;
                hoivien.QueQuanHuyenId = donhoivien.QueQuanHuyenId;
                hoivien.QueQuanXaId = donhoivien.QueQuanXaId;
                hoivien.DiaChi = donhoivien.DiaChi;
                hoivien.DiaChiTinhId = donhoivien.DiaChiTinhId;
                hoivien.DiaChiHuyenId = donhoivien.DiaChiHuyenId;
                hoivien.DiaChiXaId = donhoivien.DiaChiXaId;
                hoivien.SoGiayChungNhanDkhn = donhoivien.SoGiayChungNhanDkhn;
                hoivien.NgayCapGiayChungNhanDkhn = donhoivien.NgayCapGiayChungNhanDkhn;
                hoivien.HanCapTu = donhoivien.HanCapTu;
                hoivien.HanCapDen = donhoivien.HanCapDen;
                hoivien.SoChungChiKtv = donhoivien.SoChungChiKtv;
                hoivien.NgayCapChungChiKtv = donhoivien.NgayCapChungChiKtv;
                hoivien.Email = donhoivien.Email;
                hoivien.DienThoai = donhoivien.DienThoai;
                hoivien.Mobile = donhoivien.Mobile;
                hoivien.ChucVuId = donhoivien.ChucVuId;
                hoivien.TruongDaiHocId = donhoivien.TruongDaiHocId;
                hoivien.ChuyenNganhDaoTaoId = donhoivien.ChuyenNganhDaoTaoId;
                hoivien.ChuyenNganhNam = donhoivien.ChuyenNganhNam;
                hoivien.HocViId = donhoivien.HocViId;
                hoivien.HocViNam = donhoivien.HocViNam;
                hoivien.HocHamId = donhoivien.HocHamId;
                hoivien.HocHamNam = donhoivien.HocHamNam;
                hoivien.SoCmnd = donhoivien.SoCmnd;
                hoivien.CmndNgayCap = donhoivien.CmndNgayCap;
                hoivien.CmndTinhId = donhoivien.CmndTinhId;
                hoivien.SoTaiKhoanNganHang = donhoivien.SoTaiKhoanNganHang;
                hoivien.NganHangId = donhoivien.NganHangId;
                hoivien.HoiVienTapTheId = donhoivien.HoiVienTapTheId;
                hoivien.DonViCongTac = donhoivien.DonViCongTac;
                hoivien.SoThichId = donhoivien.SoThichId;
                hoivien.SoQuyetDinhKetNap = soQD;
                hoivien.NgayQuyetDinhKetNap = DateTime.ParseExact(ngayQD, "MM/dd/yyyy", null);
                hoivien.NgayGiaNhap = DateTime.Now;
                hoivien.NgayDuyet = DateTime.Now;
                hoivien.NguoiCapNhat = cm.Admin_HoVaTen;
                hoivien.NguoiDuyet = cm.Admin_TenDangNhap;
                hoivien.TinhTrangHoiVienId = 1;
                HoiVienCaNhan_provider.Update(hoivien);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE tblHoiVienCaNhanChungChiQuocTe WHERE HoiVienCaNhanID = " + donhoivien.HoiVienCaNhanId + ";";
                cmd.CommandText += "DELETE tblHoiVienCaNhanQuaTrinhCongTac WHERE HoiVienCaNhanID = " + donhoivien.HoiVienCaNhanId + ";";
                cmd.CommandText += "DELETE tblHoiVienFile WHERE HoiVienCaNhanID = " + donhoivien.HoiVienCaNhanId + ";";

                DataAccess.RunActionCmd(cmd);

                // insert chứng chỉ  
                TList<DonHoiVienCaNhanChungChi> chungchi_data;
                DonHoiVienCaNhanChungChiParameterBuilder filter_chungchi = new DonHoiVienCaNhanChungChiParameterBuilder();
                filter_chungchi.AppendEquals(DonHoiVienCaNhanChungChiColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                chungchi_data = DonHoiVienCaNhanChungChi_provider.Find(filter_chungchi);

                foreach (DonHoiVienCaNhanChungChi donchungchi in chungchi_data)
                {
                    HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                    chungchi.HoiVienCaNhanId = donhoivien.HoiVienCaNhanId.Value;
                    chungchi.SoChungChi = donchungchi.SoChungChi;
                    chungchi.NgayCap = donchungchi.NgayCap;
                    HoiVienCaNhanChungChi_provider.Insert(chungchi);
                }

                // insert quá trình làm việc
                TList<DonHoiVienCaNhanQuaTrinhCongTac> quatrinh_data;
                DonHoiVienCaNhanQuaTrinhCongTacParameterBuilder filter_quatrinh = new DonHoiVienCaNhanQuaTrinhCongTacParameterBuilder();
                filter_quatrinh.AppendEquals(DonHoiVienCaNhanQuaTrinhCongTacColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                quatrinh_data = DonHoiVienCaNhanQuaTrinh_provider.Find(filter_quatrinh);

                foreach (DonHoiVienCaNhanQuaTrinhCongTac donquatrinh in quatrinh_data)
                {
                    HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
                    quatrinh.HoiVienCaNhanId = donhoivien.HoiVienCaNhanId.Value;
                    quatrinh.ThoiGianCongTac = donquatrinh.ThoiGianCongTac;
                    quatrinh.ChucVu = donquatrinh.ChucVu;
                    quatrinh.NoiLamViec = donquatrinh.NoiLamViec;

                    HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
                }

                // file đính kèm
                TList<DonHoiVienFile> file_data;
                DonHoiVienFileParameterBuilder filter_file = new DonHoiVienFileParameterBuilder();
                filter_file.AppendEquals(DonHoiVienFileColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                file_data = DonHoiVienFile_provider.Find(filter_file);

                foreach (DonHoiVienFile donfile in file_data)
                {
                    HoiVienFile file = new HoiVienFile();
                    file.HoiVienCaNhanId = donhoivien.HoiVienCaNhanId.Value;
                    file.LoaiGiayToId = donfile.LoaiGiayToId;
                    file.TenFile = donfile.TenFile;
                    file.FileDinhKem = donfile.FileDinhKem;

                    HoiVienFile_provider.Insert(file);
                }


                // update vào bảng tblNguoiDung
                cmd = new SqlCommand();
                cmd.CommandText = "SELECT NguoiDungID FROM tblNguoiDung WHERE HoiVienId = " + donhoivien.HoiVienCaNhanId.Value;
                string NguoiDungID = DataAccess.DLookup(cmd);

                SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connstr, true, "");

                NguoiDung new_user = NguoiDung_provider.GetByNguoiDungId(int.Parse(NguoiDungID));
                new_user.HoVaTen = donhoivien.HoDem + " " + donhoivien.Ten;
                new_user.NgaySinh = donhoivien.NgaySinh;

                new_user.GioiTinh = donhoivien.GioiTinh;
                new_user.Email = donhoivien.Email;
                new_user.DienThoai = donhoivien.DienThoai;
                new_user.SoCmnd = donhoivien.SoCmnd;
                new_user.CauHoiBiMatId = donhoivien.CauHoiBiMatId;
                new_user.CauTraLoiBiMat = donhoivien.CauTraLoiBiMat;
                new_user.LoaiHoiVien = "1";

                NguoiDung_provider.Update(new_user);

                donhoivien.TinhTrangId = 5;
                donhoivien.NgayDuyet = DateTime.Now;
                donhoivien.NguoiDuyet = cm.Admin_TenDangNhap;
                DonHoiVienCaNhan_provider.Update(donhoivien);

                // insert vào bảng tblTTPhatSinhPhi

                //if (donhoivien.HoiVienTapTheId != null)
                //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";
                //else
                //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnKhongLamViecTaiCtkt).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";

                try
                {
                    // SonNQ sửa lại, update theo phí chi tiết
                    cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + loai + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";

                    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                    if (dtb.Rows.Count != 0)
                    {

                        cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + donhoivien.HoiVienCaNhanId.Value + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                        DataAccess.RunActionCmd(cmd);
                    }
                }
                catch (Exception ex)
                {

                }

                // Send mail
                string body = "";
                body += "Anh/chị đã được phê duyệt kết nạp hội viên cá nhân, có thể sử dụng tài khoản để đăng nhập vào phần mềm với tư cách là hội viên cá nhân";
                cm.GuiThongBao(hoivien.HoiVienCaNhanId.ToString(), "1", "VACPA - Phê duyệt kết nạp hội viên cá nhân tại trang tin điện tử VACPA", body);
                string msg = "";
                IsSendMail = SmtpMail.Send("BQT WEB VACPA", donhoivien.Email, "Thông tin tài khoản hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg);

                #endregion
            }
            else
            {
                #region hội viên mới

                // tạo tài khoản nếu chưa có
                string HoiVienID = SinhID(donhoivien.HoDem, donhoivien.Ten, donhoivien.SoChungChiKtv);

                HoiVienCaNhan hoivien = new HoiVienCaNhan();
                hoivien.LoaiHoiVienCaNhan = "1";
                hoivien.LoaiHoiVienCaNhanChiTiet = int.Parse(loai);
                hoivien.MaHoiVienCaNhan = HoiVienID;
                hoivien.HoDem = donhoivien.HoDem;
                hoivien.Ten = donhoivien.Ten;
                hoivien.GioiTinh = donhoivien.GioiTinh;
                hoivien.NgaySinh = donhoivien.NgaySinh;
                hoivien.QuocTichId = donhoivien.QuocTichId;
                hoivien.QueQuanTinhId = donhoivien.QueQuanTinhId;
                hoivien.QueQuanHuyenId = donhoivien.QueQuanHuyenId;
                hoivien.QueQuanXaId = donhoivien.QueQuanXaId;
                hoivien.DiaChi = donhoivien.DiaChi;
                hoivien.DiaChiTinhId = donhoivien.DiaChiTinhId;
                hoivien.DiaChiHuyenId = donhoivien.DiaChiHuyenId;
                hoivien.DiaChiXaId = donhoivien.DiaChiXaId;
                hoivien.SoGiayChungNhanDkhn = donhoivien.SoGiayChungNhanDkhn;
                hoivien.NgayCapGiayChungNhanDkhn = donhoivien.NgayCapGiayChungNhanDkhn;
                hoivien.HanCapTu = donhoivien.HanCapTu;
                hoivien.HanCapDen = donhoivien.HanCapDen;
                hoivien.SoChungChiKtv = donhoivien.SoChungChiKtv;
                hoivien.NgayCapChungChiKtv = donhoivien.NgayCapChungChiKtv;
                hoivien.Email = donhoivien.Email;
                hoivien.DienThoai = donhoivien.DienThoai;
                hoivien.Mobile = donhoivien.Mobile;
                hoivien.ChucVuId = donhoivien.ChucVuId;
                hoivien.TruongDaiHocId = donhoivien.TruongDaiHocId;
                hoivien.ChuyenNganhDaoTaoId = donhoivien.ChuyenNganhDaoTaoId;
                hoivien.ChuyenNganhNam = donhoivien.ChuyenNganhNam;
                hoivien.HocViId = donhoivien.HocViId;
                hoivien.HocViNam = donhoivien.HocViNam;
                hoivien.HocHamId = donhoivien.HocHamId;
                hoivien.HocHamNam = donhoivien.HocHamNam;
                hoivien.SoCmnd = donhoivien.SoCmnd;
                hoivien.CmndNgayCap = donhoivien.CmndNgayCap;
                hoivien.CmndTinhId = donhoivien.CmndTinhId;
                hoivien.SoTaiKhoanNganHang = donhoivien.SoTaiKhoanNganHang;
                hoivien.NganHangId = donhoivien.NganHangId;
                hoivien.HoiVienTapTheId = donhoivien.HoiVienTapTheId;
                hoivien.DonViCongTac = donhoivien.DonViCongTac;
                hoivien.SoThichId = donhoivien.SoThichId;
                hoivien.SoQuyetDinhKetNap = soQD;
                hoivien.NgayQuyetDinhKetNap = DateTime.ParseExact(ngayQD, "MM/dd/yyyy", null);
                hoivien.NgayGiaNhap = DateTime.Now;
                hoivien.NgayDuyet = DateTime.Now;
                hoivien.NguoiCapNhat = cm.Admin_HoVaTen;
                hoivien.NguoiDuyet = cm.Admin_TenDangNhap;
                hoivien.TinhTrangHoiVienId = 1;
                HoiVienCaNhan_provider.Insert(hoivien);

                // insert chứng chỉ  
                TList<DonHoiVienCaNhanChungChi> chungchi_data;
                DonHoiVienCaNhanChungChiParameterBuilder filter_chungchi = new DonHoiVienCaNhanChungChiParameterBuilder();
                filter_chungchi.AppendEquals(DonHoiVienCaNhanChungChiColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                chungchi_data = DonHoiVienCaNhanChungChi_provider.Find(filter_chungchi);

                foreach (DonHoiVienCaNhanChungChi donchungchi in chungchi_data)
                {
                    HoiVienCaNhanChungChiQuocTe chungchi = new HoiVienCaNhanChungChiQuocTe();
                    chungchi.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
                    chungchi.SoChungChi = donchungchi.SoChungChi;
                    chungchi.NgayCap = donchungchi.NgayCap;
                    HoiVienCaNhanChungChi_provider.Insert(chungchi);
                }

                // insert quá trình làm việc
                TList<DonHoiVienCaNhanQuaTrinhCongTac> quatrinh_data;
                DonHoiVienCaNhanQuaTrinhCongTacParameterBuilder filter_quatrinh = new DonHoiVienCaNhanQuaTrinhCongTacParameterBuilder();
                filter_quatrinh.AppendEquals(DonHoiVienCaNhanQuaTrinhCongTacColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                quatrinh_data = DonHoiVienCaNhanQuaTrinh_provider.Find(filter_quatrinh);

                foreach (DonHoiVienCaNhanQuaTrinhCongTac donquatrinh in quatrinh_data)
                {
                    HoiVienCaNhanQuaTrinhCongTac quatrinh = new HoiVienCaNhanQuaTrinhCongTac();
                    quatrinh.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
                    quatrinh.ThoiGianCongTac = donquatrinh.ThoiGianCongTac;
                    quatrinh.ChucVu = donquatrinh.ChucVu;
                    quatrinh.NoiLamViec = donquatrinh.NoiLamViec;

                    HoiVienCaNhanQuaTrinh_provider.Insert(quatrinh);
                }

                // file đính kèm
                TList<DonHoiVienFile> file_data;
                DonHoiVienFileParameterBuilder filter_file = new DonHoiVienFileParameterBuilder();
                filter_file.AppendEquals(DonHoiVienFileColumn.DonHoiVienCaNhanId, DonHoiVienCaNhanID);
                file_data = DonHoiVienFile_provider.Find(filter_file);

                foreach (DonHoiVienFile donfile in file_data)
                {
                    HoiVienFile file = new HoiVienFile();
                    file.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
                    file.LoaiGiayToId = donfile.LoaiGiayToId;
                    file.TenFile = donfile.TenFile;
                    file.FileDinhKem = donfile.FileDinhKem;

                    HoiVienFile_provider.Insert(file);
                }

                // insert vào bảng tblNguoiDung
                SqlNguoiDungProvider NguoiDung_provider = new SqlNguoiDungProvider(connstr, true, "");

                NguoiDung new_user = new NguoiDung();
                new_user.HoVaTen = donhoivien.HoDem + " " + donhoivien.Ten;
                new_user.NgaySinh = donhoivien.NgaySinh;
                new_user.GioiTinh = donhoivien.GioiTinh;
                new_user.Email = donhoivien.Email;
                new_user.DienThoai = donhoivien.DienThoai;

                new_user.TenDangNhap = HoiVienID;

                Random random = new Random();
                int randomPassword = random.Next(10000000, 99999999);

                new_user.MatKhau = randomPassword.ToString();
                new_user.LoaiHoiVien = "1";
                new_user.HoiVienId = hoivien.HoiVienCaNhanId;

                NguoiDung_provider.Insert(new_user);

                donhoivien.TinhTrangId = 5;
                donhoivien.HoiVienCaNhanId = hoivien.HoiVienCaNhanId;
                donhoivien.NgayDuyet = DateTime.Now;
                donhoivien.NguoiDuyet = cm.Admin_TenDangNhap;
                DonHoiVienCaNhan_provider.Update(donhoivien);

                // insert vào bảng tblTTPhatSinhPhi
                SqlCommand cmd = new SqlCommand();
                //if (donhoivien.HoiVienTapTheId != null)
                //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";
                //else
                //    cmd.CommandText = "SELECT TOP 1 PhiID, MucPhi FROM tblTTDMPhiHoiVien WHERE DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnKhongLamViecTaiCtkt).ToString() + " AND NgayHetHieuLuc >= GETDATE() ORDER BY PhiID DESC";

                try
                {
                    // SonNQ sửa lại, update theo phí chi tiết
                    cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + loai + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";

                    DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                    if (dtb.Rows.Count != 0)
                    {

                        cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + hoivien.HoiVienCaNhanId + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                        DataAccess.RunActionCmd(cmd);
                    }
                }
                catch (Exception ex)
                {

                }

                // Send mail
                string body = "";

                body += "Thân chào Anh/chị <span style=\"color:red\">" + new_user.HoVaTen + "</span><BR/>";
                body += "Ban quản trị xin thông báo tài khoản đăng nhập phần mềm quản lý Hội viên VACPA như sau:<BR/>";
                body += "------ID đăng nhập------<BR/>";
                body += "Tên đăng nhập: " + HoiVienID + "<BR/>";
                body += "Mật khẩu: " + randomPassword + "<BR/>";
                body += "<span style=\"text-decoration:underline;font-weight:bold\">Lưu ý:</span><BR/>";
                body += " - Link đăng nhập phần mềm quản lý hội viên: http://vacpa.org.vn/Page/Login.aspx" + "<BR/>";
                body += " - Link xin cấp lại mật khẩu: http://vacpa.org.vn/Page/GetPassword.aspx" + "<BR/>";
                body += " - Ngay sau khi đăng nhập Anh/Chị hãy đổi mật khẩu truy cập." + "<BR/>";
                body += " - Để đảm bảo được nhận các thông báo thường xuyên với VACPA, Anh/Chị nhớ cập nhật những thông tin về sự thay đổi công việc, đơn vị công tác, số điện thoại di động, email, nơi thường trú, v.v… qua profile cá nhân." + "<BR/>";

                body += "━━━━━━━━━━━━━━━<BR/>";
                body += "Trân trọng,<BR/>";
                body += "Hội kiểm toán viên hành nghề Việt Nam (VACPA)<BR/>";
                body += "Địa chỉ: Phòng 304, Nhà Dự án, Số 4, Ngõ Hàng Chuối I, Hà Nội<BR/>";
                body += "Email: quantriweb@vacpa.org.vn";

                //body += "Anh/chị đã được phê duyệt kết nạp hội viên cá nhân, có thể sử dụng tài khoản sau để đăng nhập vào phần mềm theo đường link " + login_link + " :<BR/>";
                //body += "Tên đăng nhập: <B>" + HoiVienID + "</B><BR/>";
                //body += "Mật khẩu: <B>" + randomPassword + "</B><BR/>";
                //body += "Anh/chị vui lòng đăng nhập trong vòng 60 ngày, sau thời hạn trên anh/chị vui lòng liên hệ với VACPA để cấp lại tài khoản.<BR/>";
                cm.GuiThongBao(hoivien.HoiVienCaNhanId.ToString(), "1", "VACPA - Thông tin tài khoản hội viên cá nhân tại trang tin điện tử VACPA", body);
                string msg = "";
                IsSendMail = SmtpMail.Send("BQT WEB VACPA", donhoivien.Email, "Thông tin tài khoản hội viên cá nhân tại trang tin điện tử VACPA", body, ref msg);

                #endregion
            }

            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin kết nạp hội viên " + donhoivien.HoDem + " " + donhoivien.Ten);
        }

    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    private List<objDoiTuong> getDoiTuong()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 2;
        obj1.TenDoiTuong = "Nộp trực tiếp";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 1;
        obj2.TenDoiTuong = "Nộp trực tuyến";

        lst.Add(obj1);
        lst.Add(obj2);

        return lst;
    }

    public void LoaiNop(string ma = "00")
    {
        string output_html = "";


        output_html += "<option value=\"\">Tất cả</option>";

        List<objDoiTuong> lst = getDoiTuong();

        if (ma != null)
        {
            foreach (var tem in lst)
            {
                if (ma.Contains(tem.idDoiTuong.ToString()))
                {
                    output_html += @"
                         <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
                }
                else
                {
                    output_html += @"
                         <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
                }
            }
        }


        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void DonVi(string ma = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=\"\">Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (ma == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public DataSet sp_DonHoiVienSearch()
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            bool isCaNhan = false; ;
            bool isTapThe = false;
            String sLoaiDon = "";
            String sTrangThai = "";
            String sTenHoiVien = "";
            String sHoDemCaNhan = "";
            String sTenCaNhan = "";

            String sIDHoiVien = "";
            String sSoDon = "";
            String sHinhThucNop = "";
            String sMaTinhThanh = "";
            String sMaQuanHuyen = "";

            String sCongtyKiemToan = "";
            String sSoChungChiKTV = "";
            String sSoDKKD = "";
            String sSoCNDuDieuKienHNKT = "";

            String sNgayNopTu = "";
            String sNgayNopDen = "";
            String sNgayDuyetTu = "";
            String sNgayDuyetDen = "";

            //  Loai Don canhan/tap the
            if (Request.Form["tk_HVCN"] != null)
            {
                isCaNhan = true;
            }

            if (Request.Form["tk_HVTT"] != null)
            {
                isTapThe = true;
            }

            if ((isCaNhan && isTapThe) || (!isCaNhan && !isTapThe)) { sLoaiDon = "3"; }
            else if (isCaNhan) { sLoaiDon = "1"; }
            else if (isTapThe) { sLoaiDon = "2"; }


            // Trang thai
            if (Request.Form["tk_TTAll"] != null)
            {  // check all

                sTrangThai = "";// all
            }
            else
            {
                int itemp = 0;
                if (Request.Form["tk_TTChoTiepNhan"] != null)
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTChoTiepNhan;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTChoTiepNhan;
                    }

                    itemp = itemp + 1;
                }
                if (Request.Form["tk_TTChoDuyet"] != null)
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTChoDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTChoDuyet;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTTuChoiTiepNhan"] != null)
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTTuChoiTiepNhan;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTTuChoiTiepNhan;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTTuChoiDuyet"] != null)
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTTuChoiDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTTuChoiDuyet;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTDaDuyet"] != null)
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTDaDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTDaDuyet;
                    }
                    itemp = itemp + 1;

                }
                if (Request.Form["tk_TTThoaiDuyet"] != null)
                {
                    if (itemp == 0)
                    {
                        sTrangThai = tk_TTThoaiDuyet;
                    }
                    else
                    {
                        sTrangThai = sTrangThai + "," + tk_TTThoaiDuyet;
                    }
                    itemp = itemp + 1;

                }


            }



            // Ten hoi vien
            if (Request.Form["tk_TenHoiVien"] != null)
            {
                sTenHoiVien = Request.Form["tk_TenHoiVien"];
                string[] stempHoten = Request.Form["tk_TenHoiVien"].Split(' ');
                if (stempHoten.Length > 1)
                {

                    for (int j = 0; j < stempHoten.Length - 1; j++)
                    {
                        sHoDemCaNhan = sHoDemCaNhan + " " + stempHoten[j];

                    }
                    sHoDemCaNhan = sHoDemCaNhan.Trim();
                    sTenCaNhan = stempHoten[stempHoten.Length - 1];
                }
                else
                {
                    sTenCaNhan = Request.Form["tk_TenHoiVien"];
                    sHoDemCaNhan = Request.Form["tk_TenHoiVien"];
                }

            }

            // Ten dang nhap
            if (Request.Form["tk_ID"] != null)
            {
                sIDHoiVien = Request.Form["tk_ID"];
            }

            // So Don
            if (Request.Form["tk_SoDon"] != null)
            {
                sSoDon = Request.Form["tk_SoDon"];
            }

            // Hinh thuc nop
            if (Request.Form["tk_HinhThucNop"] != null)
            {
                sHinhThucNop = Request.Form["tk_HinhThucNop"];
            }

            // Ma Tinh
            if (Request.Form["TinhID_ToChuc"] != null)
            {
                sMaTinhThanh = Request.Form["TinhID_ToChuc"];
            }
            // Ma Huyen
            if (Request.Form["HuyenID_ToChuc"] != null)
            {
                sMaQuanHuyen = Request.Form["HuyenID_ToChuc"];
            }


            // Cong ty kiem toan
            if (Request.Form["tk_CTKiemToan"] != null)
            {
                sCongtyKiemToan = Request.Form["tk_CTKiemToan"];
            }



            // So chung chi KTV
            if (Request.Form["tk_SoChungChiKTV"] != null)
            {
                sSoChungChiKTV = Request.Form["tk_SoChungChiKTV"];
            }

            // Chung nhan dk hanh nghe kt
            if (Request.Form["tk_CNDKHNKT"] != null)
            {
                sSoCNDuDieuKienHNKT = Request.Form["tk_CNDKHNKT"];
            }

            // So DKKD
            if (Request.Form["tk_SoDKKD"] != null)
            {
                sSoDKKD = Request.Form["tk_SoDKKD"];
            }


            // Ngay nop tu
            if (Request.Form["tk_NgayNopTu"] != null)
            {
                sNgayNopTu = Request.Form["tk_NgayNopTu"].Replace("/", ""); ;
            }

            // Ngay nop den
            if (Request.Form["tk_NgayNopDen"] != null)
            {
                sNgayNopDen = Request.Form["tk_NgayNopDen"].Replace("/", "");
            }
            // Ngay duyet tu
            if (Request.Form["tk_NgayPheDuyetTu"] != null)
            {
                sNgayDuyetTu = Request.Form["tk_NgayPheDuyetTu"].Replace("/", ""); ;
            }
            //  Ngay duyet den
            if (Request.Form["tk_NgayPheDuyetDen"] != null)
            {
                sNgayDuyetDen = Request.Form["tk_NgayPheDuyetDen"].Replace("/", ""); ;
            }


            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("LoaiDon", sLoaiDon));
            arrParams.Add(new DBHelpers.DataParameter("TrangThai", sTrangThai));
            arrParams.Add(new DBHelpers.DataParameter("TenHoiVien", sTenHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("HoDemCaNhan", sHoDemCaNhan));
            arrParams.Add(new DBHelpers.DataParameter("TenCaNhan", sTenCaNhan));
            arrParams.Add(new DBHelpers.DataParameter("IDHoiVien", sIDHoiVien));
            arrParams.Add(new DBHelpers.DataParameter("SoDon", sSoDon));
            arrParams.Add(new DBHelpers.DataParameter("HinhThucNop", sHinhThucNop));
            arrParams.Add(new DBHelpers.DataParameter("MaTinhThanh", sMaTinhThanh));
            arrParams.Add(new DBHelpers.DataParameter("MaQuanHuyen", sMaQuanHuyen));
            arrParams.Add(new DBHelpers.DataParameter("CongtyKiemToan", sCongtyKiemToan));
            arrParams.Add(new DBHelpers.DataParameter("SoChungChiKTV", sSoChungChiKTV));
            arrParams.Add(new DBHelpers.DataParameter("SoDKKD", sSoDKKD));
            arrParams.Add(new DBHelpers.DataParameter("SoCNDuDieuKienHNKT", sSoCNDuDieuKienHNKT));
            arrParams.Add(new DBHelpers.DataParameter("NgayNopTu", sNgayNopTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayNopDen", sNgayNopDen));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyetTu", sNgayDuyetTu));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyetDen", sNgayDuyetDen));


            objDBHelper = ConnectionControllers.Connect("VACPA");

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_donhoivien_search", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }


    public DataSet sp_DonHoiVienDelete(string sLstDonHoiVienID)
    {

        DBHelpers.DBHelper objDBHelper = null;
        try
        {
            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("LstDonHoiVienID", sLstDonHoiVienID));

            objDBHelper = ConnectionControllers.Connect("VACPA");

            DataSet dsTemp = new DataSet();
            objDBHelper.ExecFunction("sp_donhoivien_delete", arrParams, ref dsTemp);

            return dsTemp;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }


    }


    protected void load_users()
    {
        try
        {



            DataSet ds = sp_DonHoiVienSearch();

            DataView dt = ds.Tables[0].DefaultView;


            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 30;
            int i;
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = "KetNapHoiVien" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }


            Users_grv.DataSource = objPds;
            Users_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            ds = null;
            dt = null;




        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }


    protected void annut()
    {
        Commons cm = new Commons();


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "KetNapHoiVien", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void Users_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Users_grv.PageIndex = e.NewPageIndex;
        Users_grv.DataBind();
    }
    protected void Users_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }
    protected void btnGhi_Click(object sender, EventArgs e)
    {

    }

    protected void btnTuChoiDon_Click(String sDonHoiVienID, String sLyDoTuChoi, ref bool isSendMail)
    {
        try
        {
            int donHoiVienTtId = 0;
            try
            {
                donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            }
            catch { }
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {


                UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, "",
                              "", "", "", sLyDoTuChoi);
                String thongbao = "";
                SqlDonHoiVienTapTheProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienTapTheProvider(cm.connstr, true, "");
                DonHoiVienTapThe hoivientapthe = new DonHoiVienTapThe();
                hoivientapthe = DonHoiVienCaNhan_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
                // Send mail
                string body = "";
                body += "Công ty không được phê duyệt kết nạp hội viên tổ chức:<BR/>";
                body += "Lý do từ chối: " + sLyDoTuChoi + " <BR/>";

                string msg = "";
                if (SmtpMail.Send("BQT WEB VACPA", hoivientapthe.Email, "Từ chối tiếp nhận đơn xin kết nạp hội viên tổ chức", body, ref msg))
                {
                    //                    thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;"">
                    //                							<p>" + "Cập nhật tạng thái thành công." + @"</p>							
                    //                						</div>";

                    //                    ErrorMessage.Controls.Add(new LiteralControl(thongbao));

                }
                else
                {
                    isSendMail = false;
                    //msg = "Lỗi không gửi được mail";
                    //ErrorMessage.Controls.Add(new LiteralControl(msg));
                }
                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối tiếp nhận đơn xin kết nạp hội viên " + hoivientapthe.TenDoanhNghiep);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void btnTrinhPheDuyet_Click(String sDonHoiVienID)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {
                //UpdateStatus(donHoiVienTtId, trangthaiTuChoiID, cm.Admin_TenDangNhap, NguoiDuyet,
                //             NgayNopDon, NgayTiepNhan, NgayDuyet, LyDoTuChoi);
                UpdateStatus(donHoiVienTtId, trangthaiChoDuyetID, "", "", "", "", "", "");

            }
            SqlDonHoiVienTapTheProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienTapTheProvider(cm.connstr, true, "");
            DonHoiVienTapThe hoivientapthe = new DonHoiVienTapThe();
            hoivientapthe = DonHoiVienCaNhan_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
            cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Tiếp nhận đơn xin kết nạp hội viên " + hoivientapthe.TenDoanhNghiep);
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnTuChoiDuyet_Click(String sDonHoiVienID, String sLyDoTuChoi, ref bool isSendMail)
    {
        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                UpdateStatus(donHoiVienTtId, trangthaiTraLaiID, "", cm.Admin_TenDangNhap, "", "",
                        DateTime.Now.ToString("ddMMyyyy"), sLyDoTuChoi);
                String thongbao = "";

                SqlDonHoiVienTapTheProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienTapTheProvider(cm.connstr, true, "");
                DonHoiVienTapThe hoivientapthe = new DonHoiVienTapThe();
                hoivientapthe = DonHoiVienCaNhan_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
                // Send mail
                string body = "";
                body += "Công ty không được phê duyệt kết nạp hội viên tổ chức:<BR/>";
                body += "Lý do từ chối: " + sLyDoTuChoi + " <BR/>";

                string msg = "";
                if (SmtpMail.Send("BQT WEB VACPA", hoivientapthe.Email, "Từ chối tiếp nhận đơn xin kết nạp hội viên tổ chức", body, ref msg))
                {
                    //                    thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;"">
                    //                							<p>" + "Cập nhật tạng thái thành công." + @"</p>							
                    //                						</div>";

                    //                    ErrorMessage.Controls.Add(new LiteralControl(thongbao));

                }
                else
                {
                    isSendMail = false;
                    //msg = "Lỗi không gửi được mail";
                    //ErrorMessage.Controls.Add(new LiteralControl(msg));
                }
                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Từ chối phê duyệt đơn xin kết nạp hội viên " + hoivientapthe.TenDoanhNghiep);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }


    }

    protected void btnPheDuyet_Click(String sDonHoiVienID, ref bool isSendMail, string SoQD = "", string NgayQD = "", string loai = "")
    {

        try
        {
            int donHoiVienTtId = 0;
            donHoiVienTtId = Convert.ToInt32(sDonHoiVienID);
            if (donHoiVienTtId != null && donHoiVienTtId != 0)
            {

                string TenDangNhap = "";
                string MatKhau = "";
                string Email = "";
                UpdateStatus(donHoiVienTtId, trangthaiDaPheDuyet, "", cm.Admin_TenDangNhap, "", "",
                    DateTime.Now.ToString("ddMMyyyy"), "", ref TenDangNhap, ref MatKhau, ref Email, SoQD, NgayQD, loai);

                // Send mail
                string body = "";
                body += "Thân chào Quý công ty<BR/>";
                body += "Ban quản trị xin thông báo tài khoản đăng nhập phần mềm quản lý Hội viên VACPA như sau:<BR/>";
                body += "------ID đăng nhập------<BR/>";
                body += "Tên đăng nhập: " + TenDangNhap + "<BR/>";
                body += "Mật khẩu: " + MatKhau + "<BR/>";

                body += "<span style=\"text-decoration:underline;font-weight:bold\">Lưu ý:</span><BR/>";
                body += " - Link đăng nhập phần mềm quản lý hội viên: http://vacpa.org.vn/Page/Login.aspx" + "<BR/>";
                body += " - Link xin cấp lại mật khẩu: http://vacpa.org.vn/Page/GetPassword.aspx" + "<BR/>";
                body += " - Ngay sau khi đăng nhập Quý công ty hãy đổi mật khẩu truy cập." + "<BR/>";
                body += " - Để đảm bảo được nhận các thông báo thường xuyên với VACPA, Quý công ty nhớ cập nhật những thông tin về sự thay đổi công việc, đơn vị công tác, số điện thoại di động, email, nơi thường trú, v.v… qua profile cá nhân." + "<BR/>";

                body += "━━━━━━━━━━━━━━━<BR/>";
                body += "Trân trọng,<BR/>";
                body += "Hội kiểm toán viên hành nghề Việt Nam (VACPA)<BR/>";
                body += "Địa chỉ: Phòng 304, Nhà Dự án, Số 4, Ngõ Hàng Chuối I, Hà Nội<BR/>";
                body += "Email: quantriweb@vacpa.org.vn";

                string msg = "";

                String thongbao = "";

                if (SmtpMail.Send("BQT WEB VACPA", Email, "Thông tin tài khoản hội viên tổ chức tại trang tin điện tử VACPA", body, ref msg))
                {
                    thongbao = @"<div class=""coloralert contact-success-block"" style=""background: #0D6097;"">
                                    							<p>" + "Cập nhật trạng thái thành công." + @"</p>							
                                    						</div>";

                    ErrorMessage.Controls.Add(new LiteralControl(thongbao));

                }
                else
                {
                    isSendMail = false;
                    msg = "Lỗi không gửi được mail";
                    ErrorMessage.Controls.Add(new LiteralControl(msg));
                }

                SqlDonHoiVienTapTheProvider DonHoiVienCaNhan_provider = new SqlDonHoiVienTapTheProvider(cm.connstr, true, "");
                DonHoiVienTapThe hoivientapthe = new DonHoiVienTapThe();
                hoivientapthe = DonHoiVienCaNhan_provider.GetByDonHoiVienTapTheId(donHoiVienTtId);
                cm.ghilog("KetNapHoiVien", cm.Admin_TenDangNhap + " " + "Phê duyệt đơn xin kết nạp hội viên " + hoivientapthe.TenDoanhNghiep);
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    public bool UpdateStatus(int DonHoiVienTapTheID, int TinhTrangID, String NguoiTiepNhan, String NguoiDuyet,
                            String NgayNopDon, String NgayTiepNhan, String NgayDuyet, String LyDoTuChoi)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("DonHoiVienTapTheID", DonHoiVienTapTheID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiTiepNhan", NguoiTiepNhan));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayNopDon", NgayNopDon));
            arrParams.Add(new DBHelpers.DataParameter("NgayTiepNhan", NgayTiepNhan));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));


            objDBHelper = ConnectionControllers.Connect("VACPA");
            return objDBHelper.ExecFunction("sp_donhoivien_tt_xetduyet", arrParams);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }
    public bool UpdateStatus(int DonHoiVienTapTheID, int TinhTrangID, String NguoiTiepNhan, String NguoiDuyet,
                               String NgayNopDon, String NgayTiepNhan, String NgayDuyet, String LyDoTuChoi, ref string TenDangNhap,
                                ref  string MatKhau, ref string Email, string SoQD, string NgayQD, string loai)
    {
        DBHelpers.DBHelper objDBHelper = null;

        try
        {

            List<DBHelpers.DataParameter> arrParams = new List<DBHelpers.DataParameter>();

            arrParams.Add(new DBHelpers.DataParameter("LoaiHoiVienTapTheChiTiet", loai));
            arrParams.Add(new DBHelpers.DataParameter("DonHoiVienTapTheID", DonHoiVienTapTheID));
            arrParams.Add(new DBHelpers.DataParameter("TinhTrangID", TinhTrangID));
            arrParams.Add(new DBHelpers.DataParameter("NguoiTiepNhan", NguoiTiepNhan));
            arrParams.Add(new DBHelpers.DataParameter("NguoiDuyet", NguoiDuyet));
            arrParams.Add(new DBHelpers.DataParameter("NgayNopDon", NgayNopDon));
            arrParams.Add(new DBHelpers.DataParameter("NgayTiepNhan", NgayTiepNhan));
            arrParams.Add(new DBHelpers.DataParameter("NgayDuyet", NgayDuyet));
            arrParams.Add(new DBHelpers.DataParameter("LyDoTuChoi", LyDoTuChoi));

            DBHelpers.DataParameter outTenDangNhap = new DBHelpers.DataParameter("TenDangNhap", TenDangNhap);
            outTenDangNhap.OutPut = true;
            arrParams.Add(outTenDangNhap);

            DBHelpers.DataParameter outMatKhau = new DBHelpers.DataParameter("MatKhau", MatKhau);
            outMatKhau.OutPut = true;
            arrParams.Add(outMatKhau);

            DBHelpers.DataParameter outEmail = new DBHelpers.DataParameter("Email", Email);
            outEmail.OutPut = true;
            arrParams.Add(outEmail);


            objDBHelper = ConnectionControllers.Connect("VACPA");
            bool bRedulte = objDBHelper.ExecFunction("sp_donhoivien_tt_xetduyet", arrParams);
            TenDangNhap = outTenDangNhap.Param_Value.ToString();
            MatKhau = outMatKhau.Param_Value.ToString();
            Email = outEmail.Param_Value.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select HoiVienTapTheID from tblDonHoiVienTapThe where DonHoiVienTapTheID =" + DonHoiVienTapTheID;
            string HoiVienTapTheID = DataAccess.DLookup(cmd);

            if (!string.IsNullOrEmpty(SoQD) && !string.IsNullOrEmpty(NgayQD))
            {
                cmd.CommandText = "UPDATE tblHoiVienTapThe SET SoQuyetDinhKetNap = '" + SoQD + "' , NgayQuyetDinhKetNap = '" + DateTime.ParseExact(NgayQD, "MM/dd/yyyy", null).ToString("yyyy-MM-dd") + "' WHERE HoiVienTapTheID = " + HoiVienTapTheID;

                DataAccess.RunActionCmd(cmd);
            }

            cmd.CommandText = "select HoiVienTapTheChaID from tblHoiVienTapThe where HoiVienTapTheID =" + HoiVienTapTheID;
            if (DataAccess.DLookup(cmd) == "")
            {
                // insert vào bảng tblTTPhatSinhPhi
                cmd.CommandText = "SELECT NamDuDKKT_CK FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + HoiVienTapTheID;
                string namdudkkt_ck = DataAccess.DLookup(cmd);
                cmd.CommandText = "SELECT CONVERT(varchar, NgayGiaNhap, 103) FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + HoiVienTapTheID;
                string ngaygianhap = DataAccess.DLookup(cmd);
                // SonNQ sửa lại, update theo phí chi tiết
                if (!string.IsNullOrEmpty(ngaygianhap))
                {
                    if (!string.IsNullOrEmpty(namdudkkt_ck) && namdudkkt_ck != "0")
                        cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + loai + " AND B.TuNgay <= CONVERT(DATETIME, '" + ngaygianhap + "', 103) AND B.DenNgay >= CONVERT(DATETIME, '" + ngaygianhap + "', 103) ORDER BY A.PhiID DESC";
                    else
                        cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttKhongDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + loai + " AND B.TuNgay <= CONVERT(DATETIME, '" + ngaygianhap + "', 103) AND B.DenNgay >= CONVERT(DATETIME, '" + ngaygianhap + "', 103) ORDER BY A.PhiID DESC";
                }
                else
                {
                    if (!string.IsNullOrEmpty(namdudkkt_ck) && namdudkkt_ck != "0")
                        cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + loai + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";
                    else
                        cmd.CommandText = "SELECT TOP 1 A.PhiID, CONVERT(INT, A.MucPhi/100*B.MucPhiChiTiet) AS MucPhi FROM tblTTDMPhiHoiVien A LEFT JOIN tblTTDMPhiHoiVienChiTiet B ON A.PhiID = B.PhiID WHERE A.DoiTuongApDungID = " + ((int)EnumVACPA.DoiTuongApDung.HvttKhongDuDieuKienKiTLinhVucChungKhoan).ToString() + " AND A.NgayHetHieuLuc >= GETDATE() AND A.LoaiHoiVienChiTiet = " + loai + " AND B.TuNgay <= GETDATE() AND B.DenNgay >= GETDATE() ORDER BY A.PhiID DESC";
                }
                DataTable dtb = DataAccess.RunCMDGetDataSet(cmd).Tables[0];
                if (dtb.Rows.Count != 0)
                {

                    cmd.CommandText = "INSERT INTO tblTTPhatSinhPhi(HoiVienID, LoaiHoiVien, PhiID, LoaiPhi, NgayPhatSinh, SoTien) VALUES(" + HoiVienTapTheID + ", " + ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString() + ", " + dtb.Rows[0]["PhiID"].ToString() + ", " + ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString() + ", GETDATE(), " + dtb.Rows[0]["MucPhi"].ToString() + ")";
                    DataAccess.RunActionCmd(cmd);
                }
            }
            else
            {
                cmd.CommandText = "UPDATE tblHoiVienTapThe SET LoaiHoiVienTapTheChiTiet = NULL WHERE HoiVienTapTheID = " + HoiVienTapTheID;

                DataAccess.RunActionCmd(cmd);
            }
            return bRedulte;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ConnectionControllers.DisConnect(objDBHelper);
        }
    }

    private int getHoiVienTapTheID(int DonHoiVienTapTheID)
    {
        int HoiVienTapTheID = 0;

        try
        {

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoiVienTapTheID from tblDonHoiVienTapThe where DonHoiVienTapTheID =" + DonHoiVienTapTheID;

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];



            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                HoiVienTapTheID = Convert.ToInt32(tem["HoiVienTapTheID"]);
            }
        }
        catch { }

        return HoiVienTapTheID;
    }


    public string SinhID(string HoDem, string Ten, string SoChungChiKTV)
    {
        string result = "";

        string sql = "SELECT dbo.BoDau(N'" + Ten + "')";
        SqlCommand cmd = new SqlCommand(sql);
        string ten = DataAccess.DLookup(cmd);

        cmd.CommandText = "SELECT dbo.BoDau(N'" + HoDem + "')";
        string hodem = DataAccess.DLookup(cmd);

        string[] array = hodem.Split(' ');
        hodem = "";
        foreach (string s in array)
        {
            if (!string.IsNullOrEmpty(s))
                hodem += s.Substring(0, 1).ToLower();
        }

        result = ten + hodem;

        for (int i = 0; i < SoChungChiKTV.Length; i++)
        {
            if (Regex.IsMatch(SoChungChiKTV.Substring(i, 1), @"^[0-9]\d*\.?[0]*$"))
                result += SoChungChiKTV.Substring(i, 1);
        }

        return result;
    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[4].Text == "Đơn tổ chức" || e.Row.Cells[4].Text == "Đơn chi nhánh")
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    cell.CssClass = "rowtt";
                }
            }
        }
    }
}