﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="tablestatic.ascx.cs" Inherits="usercontrols_tablestatic" %>

<form runat="server">

  <h4 class="widgettitle">Table Bordered</h4>

  <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" class="table table-bordered responsive">
    <Columns>
        <asp:BoundField DataField="KY_HIEU" HeaderText="Ký hiệu" 
            SortExpression="KY_HIEU" />
        <asp:BoundField DataField="TEN_CHI_TIEU_VN" HeaderText="Tên chỉ tiêu" 
            SortExpression="TEN_CHI_TIEU_VN" />
        <asp:BoundField DataField="DON_VI_TINH" HeaderText="Đơn vị tính" 
            SortExpression="DON_VI_TINH" />
    </Columns>
</asp:GridView>
            	<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:TestConnectionString %>" 
    SelectCommand="SELECT TOP 50 [KY_HIEU], [TEN_CHI_TIEU_VN], [DON_VI_TINH] FROM [CG_HE_THONG_CHI_TIEU]">
</asp:SqlDataSource>





            	
                
               
               
                
               
    			
                <div class="divider30"></div>
                
  <h4 class="widgettitle">Deletable Row</h4>

   <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" class="table table-bordered responsive">
    <Columns>
        <asp:BoundField DataField="KY_HIEU" HeaderText="Ký hiệu" 
            SortExpression="KY_HIEU" />
        <asp:BoundField DataField="TEN_CHI_TIEU_VN" HeaderText="Tên chỉ tiêu" 
            SortExpression="TEN_CHI_TIEU_VN" />
        <asp:BoundField DataField="DON_VI_TINH" HeaderText="Đơn vị tính" 
            SortExpression="DON_VI_TINH" />
        <asp:TemplateField >
            <ItemTemplate>
                <a href="" class="deleterow"><span class="icon-trash"></span></a>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
            	<asp:SqlDataSource ID="SqlDataSource2" runat="server" 
    ConnectionString="<%$ ConnectionStrings:TestConnectionString %>" 
    SelectCommand="SELECT TOP 50 [KY_HIEU], [TEN_CHI_TIEU_VN], [DON_VI_TINH] FROM [CG_HE_THONG_CHI_TIEU]">
</asp:SqlDataSource>




            	


<script type="text/javascript">
    jQuery(document).ready(function () {

   


        // delete row in a table
        if (jQuery('.deleterow').length > 0) {
            jQuery('.deleterow').click(function () {
                var conf = confirm('Continue delete?');
                if (conf)
                    jQuery(this).parents('tr').fadeOut(function () {
                        jQuery(this).remove();
                        // do some other stuff here
                    });
                return false;
            });
        }

    });
</script>

</form>

