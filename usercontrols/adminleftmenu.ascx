﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="adminleftmenu.ascx.cs" Inherits="usercontrols_adminleftmenu" %>

<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header" style="cursor:pointer" title="Ẩn menu" onclick="close_leftpanel();">Menu chức năng <span  class="iconsweets-arrowleft"></span></li>
                <li <% checkmenu("|/admin.aspx?|"); %> ><a href="admin.aspx"><span class="iconfa-home"></span> Trang chủ</a></li>
                



                <li <% checkmenu("|/admin.aspx?page=dmchung&dm=dmquytrinh|/admin.aspx?page=nhapthongtinktv|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=timkiemsualoidulieu|/admin.aspx?page=khoiphucdulieu|/admin.aspx?page=saoluudulieu|/admin.aspx?page=nhatkysudung|/admin.aspx?page=role|/admin.aspx?page=account|/admin.aspx?page=cauhinhhethong|/admin.aspx?page=khaibaoquytrinh|/admin.aspx?page=user|/admin.aspx?page=qtht_quanlytailieu|",1); %> ><a href=""><span class="iconfa-wrench"></span> Quản trị hệ thống</a>
                	<ul <% checkmenu("|/admin.aspx?page=dmchung&dm=dmquytrinh|/admin.aspx?page=nhapthongtinktv|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=timkiemsualoidulieu|/admin.aspx?page=khoiphucdulieu|/admin.aspx?page=saoluudulieu|/admin.aspx?page=nhatkysudung|/admin.aspx?page=role|/admin.aspx?page=account|/admin.aspx?page=cauhinhhethong|/admin.aspx?page=khaibaoquytrinh|/admin.aspx?page=user|/admin.aspx?page=qtht_quanlytailieu|",2); %> >
                     
                        <li id="user" <% checkmenu("|/admin.aspx?page=user|"); %> ><a href="admin.aspx?page=user" style="font-weight:bold">Quản lý tài khoản người sử dụng</a></li>
                        <li id="role" <% checkmenu("|/admin.aspx?page=role|"); %> ><a href="admin.aspx?page=role" style="font-weight:bold">Quản lý vai trò</a></li>                        
                        <li id="nhatkysudung" <% checkmenu("|/admin.aspx?page=nhatkysudung|"); %> ><a href="admin.aspx?page=nhatkysudung" style="font-weight:bold">Nhật ký sử dụng phần mềm</a></li>                        
                  
                        <li id="Li1" <% checkmenu("|/admin.aspx?page=qtht_quanlytailieu|"); %> ><a href="admin.aspx?page=qtht_quanlytailieu" style="font-weight:bold">Quản lý tài liệu</a></li>                                                                       
                    </ul>
                </li>


 
 
 <li <% checkmenu("|/admin.aspx?page=dmcocautochuc|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|/admin.aspx?page=dmchung&dm=dmnganhang|/admin.aspx?page=dmchung&dm=dmloaitien|/admin.aspx?page=dmchung&dm=dmloaiphi|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|/admin.aspx?page=dmchung&dm=dmhocvi|/admin.aspx?page=dmchung&dm=dmhocham|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|/admin.aspx?page=dmchung&dm=dmchungchi|/admin.aspx?page=dmchung&dm=dmchucvu|/admin.aspx?page=dmtinh|/admin.aspx?page=dmhuyen|/admin.aspx?page=dmxa|/admin.aspx?page=dmchung&dm=dmcauhoibimat|/admin.aspx?page=dmsothich|/admin.aspx?page=dmdangykienkt|/admin.aspx?page=dmloaitailieu|/admin.aspx?page=dmgiangvien|",1); %> ><a href=""><span class="iconfa-reorder"></span> Quản trị danh mục</a>
                	<ul <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|/admin.aspx?page=dmcocautochuc|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|/admin.aspx?page=dmchung&dm=dmnganhang|/admin.aspx?page=dmchung&dm=dmloaitien|/admin.aspx?page=dmchung&dm=dmloaiphi|/admin.aspx?page=dmchung&dm=dmhocvi|/admin.aspx?page=dmchung&dm=dmhocham|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|/admin.aspx?page=dmchung&dm=dmchungchi|/admin.aspx?page=dmchung&dm=dmchucvu|/admin.aspx?page=dmtinh|/admin.aspx?page=dmhuyen|/admin.aspx?page=dmxa|/admin.aspx?page=dmchung&dm=dmcauhoibimat|/admin.aspx?page=dmsothich|/admin.aspx?page=dmdangykienkt|/admin.aspx?page=dmloaitailieu|/admin.aspx?page=dmgiangvien|",2); %> >
                	
                	 <li class="dropdown"    ><a href="#" style="font-weight:bold">Danh mục chung►</a>
                	   <ul <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|/admin.aspx?page=dmcocautochuc|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|/admin.aspx?page=dmchung&dm=dmnganhang|/admin.aspx?page=dmchung&dm=dmloaitien|/admin.aspx?page=dmchung&dm=dmloaiphi|/admin.aspx?page=dmchung&dm=dmhocvi|/admin.aspx?page=dmchung&dm=dmhocham|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|/admin.aspx?page=dmchung&dm=dmchungchi|/admin.aspx?page=dmchung&dm=dmchucvu|/admin.aspx?page=dmtinh|/admin.aspx?page=dmhuyen|/admin.aspx?page=dmxa|/admin.aspx?page=dmchung&dm=dmcauhoibimat|/admin.aspx?page=dmsothich|/admin.aspx?page=dmdangykienkt|/admin.aspx?page=dmloaitailieu|/admin.aspx?page=dmgiangvien|",2); %> >
                    	<li id="dmtinh" <% checkmenu("|/admin.aspx?page=dmtinh|"); %> ><a href="admin.aspx?page=dmtinh">Tỉnh / Thành phố</a></li>
                        <li id="dmhuyen" <% checkmenu("|/admin.aspx?page=dmhuyen|"); %> ><a href="admin.aspx?page=dmhuyen">Quận / Huyện</a></li>
                        <li id="dmxa" <% checkmenu("|/admin.aspx?page=dmxa|"); %> ><a href="admin.aspx?page=dmxa">Phường / Xã</a></li>                        
                        <li id="dmcocautochuc" <% checkmenu("|/admin.aspx?page=dmcocautochuc|"); %> ><a href="admin.aspx?page=dmcocautochuc">Cơ cấu tổ chức của VACPA</a></li>                        
                        <li id="dmnganhang" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmnganhang|"); %> ><a href="admin.aspx?page=dmchung&dm=dmnganhang">Ngân hàng</a></li>  
                        <li id="dmloaihinhdoanhnghiep" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|"); %> ><a href="admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep">Loại hình doanh nghiệp</a></li>  
                        <li id="dmchucvu" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmchucvu|"); %> ><a href="admin.aspx?page=dmchung&dm=dmchucvu">Chức vụ</a></li>  
                        <li id="dmloaitailieu" <% checkmenu("|/admin.aspx?page=dmloaitailieu|"); %> ><a href="admin.aspx?page=dmloaitailieu">Loại tài liệu</a></li>                                               
                        </ul>
                        </li>
                        <li class="dropdown"    ><a href="#" style="font-weight:bold">Danh mục Quản lý hội viên►</a>
                	   <ul <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|/admin.aspx?page=dmcocautochuc|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|/admin.aspx?page=dmchung&dm=dmnganhang|/admin.aspx?page=dmchung&dm=dmloaitien|/admin.aspx?page=dmchung&dm=dmloaiphi|/admin.aspx?page=dmchung&dm=dmhocvi|/admin.aspx?page=dmchung&dm=dmhocham|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|/admin.aspx?page=dmchung&dm=dmchungchi|/admin.aspx?page=dmchung&dm=dmchucvu|/admin.aspx?page=dmtinh|/admin.aspx?page=dmhuyen|/admin.aspx?page=dmxa|/admin.aspx?page=dmchung&dm=dmcauhoibimat|/admin.aspx?page=dmsothich|/admin.aspx?page=dmdangykienkt|/admin.aspx?page=dmloaitailieu|/admin.aspx?page=dmgiangvien|",2); %> >
                         <li id="dmcauhoibimat" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmcauhoibimat|"); %> ><a href="admin.aspx?page=dmchung&dm=dmcauhoibimat">Câu hỏi bí mật</a></li>                        
                         <li id="dmchuyennganhdaotao" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|"); %> ><a href="admin.aspx?page=dmchung&dm=dmchuyennganhdaotao">Chuyên ngành đào tạo</a></li>  
                         <li id="dmhinhthuckhenthuong" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|"); %> ><a href="admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong">Hình thức khen thưởng</a></li>  
                         <li id="dmhinhthuckyluat" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|"); %> ><a href="admin.aspx?page=dmchung&dm=dmhinhthuckyluat">Hình thức kỷ luật</a></li>  
                         <li id="dmhocham" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmhocham|"); %> ><a href="admin.aspx?page=dmchung&dm=dmhocham">Học hàm</a></li>  
                         <li id="dmhocvi" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmhocvi|"); %> ><a href="admin.aspx?page=dmchung&dm=dmhocvi">Học vị</a></li>  
                         <li id="dmtrinhdochuyenmon" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|"); %> ><a href="admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon">Trình độ chuyên môn</a></li>  
                         <li id="dmtruongdaihoc" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|"); %> ><a href="admin.aspx?page=dmchung&dm=dmtruongdaihoc">Trường đại học</a></li>                                                
                         <li id="dmsothich" <% checkmenu("|/admin.aspx?page=dmsothich|"); %> ><a href="admin.aspx?page=dmsothich">Sở thích của kiểm toán viên</a></li>                                               

                    </ul>
                    </li>
                    
                     <li class="dropdown"    ><a href="#" style="font-weight:bold">Danh mục Quản lý đào tạo, cập nhật kiến thức►</a>
                	   <ul <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|/admin.aspx?page=dmcocautochuc|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|/admin.aspx?page=dmchung&dm=dmnganhang|/admin.aspx?page=dmchung&dm=dmloaitien|/admin.aspx?page=dmchung&dm=dmloaiphi|/admin.aspx?page=dmchung&dm=dmhocvi|/admin.aspx?page=dmchung&dm=dmhocham|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|/admin.aspx?page=dmchung&dm=dmchungchi|/admin.aspx?page=dmchung&dm=dmchucvu|/admin.aspx?page=dmtinh|/admin.aspx?page=dmhuyen|/admin.aspx?page=dmxa|/admin.aspx?page=dmchung&dm=dmcauhoibimat|/admin.aspx?page=dmsothich|/admin.aspx?page=dmdangykienkt|/admin.aspx?page=dmloaitailieu|/admin.aspx?page=dmgiangvien|",2); %> >
                          <li id="dmgiangvien" <% checkmenu("|/admin.aspx?page=dmgiangvien|"); %> ><a href="admin.aspx?page=dmgiangvien">Giảng viên</a></li>                                           
                         
                    </ul>
                    </li>
                    
                     <li class="dropdown"    ><a href="#" style="font-weight:bold">Danh mục kiểm soát chất lượng►</a>
                	   <ul <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|/admin.aspx?page=dmcocautochuc|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|/admin.aspx?page=dmchung&dm=dmnganhang|/admin.aspx?page=dmchung&dm=dmloaitien|/admin.aspx?page=dmchung&dm=dmloaiphi|/admin.aspx?page=dmchung&dm=dmhocvi|/admin.aspx?page=dmchung&dm=dmhocham|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|/admin.aspx?page=dmchung&dm=dmchungchi|/admin.aspx?page=dmchung&dm=dmchucvu|/admin.aspx?page=dmtinh|/admin.aspx?page=dmhuyen|/admin.aspx?page=dmxa|/admin.aspx?page=dmchung&dm=dmcauhoibimat|/admin.aspx?page=dmsothich|/admin.aspx?page=dmdangykienkt|/admin.aspx?page=dmloaitailieu|/admin.aspx?page=dmgiangvien|",2); %> >
                         <li id="dmdangykienkt" <% checkmenu("|/admin.aspx?page=dmdangykienkt|"); %> ><a href="admin.aspx?page=dmdangykienkt">Dạng ý kiến kiểm toán</a></li>    
                         
                    </ul>
                    </li>
                    
                    
                    </ul>
                </li>






           <li    <% checkmenu("|/admin.aspx?page=yeucaucnkttaidonvikhac|/admin.aspx?page=guithongbaoxoaten|/admin.aspx?page=ketnaphoivien|/admin.aspx?page=capnhathoso|/admin.aspx?page=xoatenhoiviencanhan|/admin.aspx?page=xoatenhoivientapthe|/admin.aspx?page=danhsachkhachhang|",1); %> ><a href=""><span class="iconfa-group"></span> Quản lý hội viên</a>
               <ul <% checkmenu("|/admin.aspx?page=yeucaucnkttaidonvikhac|/admin.aspx?page=guithongbaoxoaten|/admin.aspx?page=ketnaphoivien|/admin.aspx?page=capnhathoso|/admin.aspx?page=xoatenhoiviencanhan|/admin.aspx?page=xoatenhoivientapthe|/admin.aspx?page=danhsachkhachhang|",2); %> >
                     	<li id="ketnaphoivien" <% checkmenu("|/admin.aspx?page=ketnaphoivien|"); %> ><a href="admin.aspx?page=ketnaphoivien" style="font-weight:bold">Kết nạp Hội viên</a></li>
                        <li id="capnhathoso"   <% checkmenu("|/admin.aspx?page=capnhathoso|"); %> ><a href="admin.aspx?page=capnhathoso" style="font-weight:bold">Cập nhật hồ sơ hội viên <span style="font-weight:bold;color:Red"><%=SoLuongYeuCauChuaDuyet %></span></a></li>                                           
                       
                         <li class="dropdown"    ><a href="#" style="font-weight:bold">Xóa tên hội viên ►</a>
                         <ul <% checkmenu("|/admin.aspx?page=guithongbaoxoaten|/admin.aspx?page=xoatenhoiviencanhan|/admin.aspx?page=xoatenhoivientapthe|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=xoatenhoiviencanhan|"); %> ><a href="admin.aspx?page=xoatenhoiviencanhan">Xóa tên hội viên cá nhân </a></li>
                            <li <% checkmenu("|/admin.aspx?page=xoatenhoivientapthe|"); %> ><a href="admin.aspx?page=xoatenhoivientapthe">Xóa tên hội viên tổ chức </a></li>                            
			<li <% checkmenu("|/admin.aspx?page=guithongbaoxoaten|"); %> ><a href="admin.aspx?page=guithongbaoxoaten">Gửi thông báo </a></li>                                                      
                         </ul>
                         <li id="yeucaucnkttaidonvikhac" <% checkmenu("|/admin.aspx?page=yeucaucnkttaidonvikhac|"); %> ><a href="admin.aspx?page=yeucaucnkttaidonvikhac" style="font-weight:bold">Quản lý yêu cầu CNKT</a></li>
                        <li id="danhsachkhachhang" <% checkmenu("|/admin.aspx?page=danhsachkhachhang|"); %> ><a href="admin.aspx?page=danhsachkhachhang" style="font-weight:bold">Quản lý danh sách khách hàng</a></li>                                           

                </ul>
         </li>




   <li <% checkmenu("|/admin.aspx?page=cnkt_quanlylophoc|/admin.aspx?page=cnkt_quanlylophoc_add|/admin.aspx?page=cnkt_quanlylophoc_details" +
               "|/admin.aspx?page=cnkt_quanlydangkyhoc|/admin.aspx?page=CNKT_DangKyHocCaNhan_Add|/admin.aspx?page=CNKT_DangKyHocCongTy_Add" +
               "|/admin.aspx?page=cnkt_capnhatsogiodaotaocnkt|/admin.aspx?page=cnkt_baocaoketquatochuclophoc|" +
               "/admin.aspx?page=cnkt_tonghop_giocnkt|/admin.aspx?page=cnkt_tonghop_giocnktconthieu|",1); %>>
                <a href=""><span class="iconfa-book"></span>Quản lý đào tạo CNKT</a>
                <ul <% checkmenu("|/admin.aspx?page=cnkt_quanlylophoc|/admin.aspx?page=cnkt_quanlylophoc_add|/admin.aspx?page=cnkt_quanlylophoc_details" +
               "|/admin.aspx?page=cnkt_quanlydangkyhoc|/admin.aspx?page=CNKT_DangKyHocCaNhan_Add|/admin.aspx?page=CNKT_DangKyHocCongTy_Add" +
               "|/admin.aspx?page=cnkt_capnhatsogiodaotaocnkt|/admin.aspx?page=cnkt_baocaoketquatochuclophoc|" +
               "/admin.aspx?page=cnkt_tonghop_giocnkt|/admin.aspx?page=cnkt_tonghop_giocnktconthieu|",2); %>>
                    <!-- Updated by HUNGNM - 2014/11/25 -->
                    <li id="cnkt_quanlylophoc" <% checkmenu("|/admin.aspx?page=cnkt_quanlylophoc|/admin.aspx?page=cnkt_quanlylophoc_add|/admin.aspx?page=cnkt_quanlylophoc_details|"); %>>
                        <a style="font-weight:bold" href="admin.aspx?page=cnkt_quanlylophoc">Quản lý lớp học</a></li>
                    <li id="cnkt_quanlydangkyhoc" <% checkmenu("|/admin.aspx?page=cnkt_quanlydangkyhoc|"); %>>
                        <a style="font-weight:bold" href="admin.aspx?page=cnkt_quanlydangkyhoc">Quản lý danh sách đăng ký học</a></li>
                    <li id="cnkt_capnhatsogiodaotaocnkt" <% checkmenu("|/admin.aspx?page=cnkt_capnhatsogiodaotaocnkt|"); %>>
                        <a style="font-weight:bold" href="admin.aspx?page=cnkt_capnhatsogiodaotaocnkt">Cập nhật số giờ đào tạo, CNKT</a></li>
                    <li id="cnkt_baocaoketquatochuclophoc" <% checkmenu("|/admin.aspx?page=cnkt_baocaoketquatochuclophoc|"); %>>
                        <a style="font-weight:bold" href="admin.aspx?page=cnkt_baocaoketquatochuclophoc">Báo cáo kết quả tổ chức lớp
                            học</a></li>
                    <li class="dropdown"><a style="font-weight:bold" href="#">Tổng hợp ►</a>
                        <ul <% checkmenu("|/admin.aspx?page=dmgiangvien|/admin.aspx?page=cnkt_quanlylophoc|/admin.aspx?page=cnkt_quanlylophoc_add|/admin.aspx?page=cnkt_quanlylophoc_details" +
               "|/admin.aspx?page=cnkt_quanlydangkyhoc|/admin.aspx?page=CNKT_DangKyHocCaNhan_Add|/admin.aspx?page=CNKT_DangKyHocCongTy_Add" +
               "|/admin.aspx?page=cnkt_capnhatsogiodaotaocnkt|/admin.aspx?page=cnkt_baocaoketquatochuclophoc|" +
               "/admin.aspx?page=cnkt_tonghop_giocnkt|/admin.aspx?page=cnkt_tonghop_giocnktconthieu|",2); %>>
                            <li id="cnkt_tonghop_giocnkt" <% checkmenu("|/admin.aspx?page=cnkt_tonghop_giocnkt|"); %>>
                                <a href="admin.aspx?page=cnkt_tonghop_giocnkt">Tổng hợp số giờ CNKT của học viên</a>
                                <li id="cnkt_tonghop_giocnktconthieu" <% checkmenu("|/admin.aspx?page=cnkt_tonghop_giocnktconthieu|"); %>>
                                    <a href="admin.aspx?page=cnkt_tonghop_giocnktconthieu">Tổng hợp số giờ CNKT còn thiếu của học viên</a>
                        </ul>
                    </li>
                </ul>
            </li>


 <li <% checkmenu("|/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra|/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra_lapdanhsach|/admin.aspx?page=kscl_chuanbi_danhsachbaocaotukiemtra|/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec|/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec_lapdanhsach" +
               "|/admin.aspx?page=kscl_chuanbi_danhsachcongtykiemtratructiep|/admin.aspx?page=kscl_chuanbi_danhsachcongtykiemtratructiep_lapdanhsach|/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra|/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra_lapdoan" +
               "|/admin.aspx?page=kscl_chuanbi_capnhatgiodaotaochothanhviendoankiemtra|/admin.aspx?page=kscl_thuchien_camketdoclapbaomatthongtin|/admin.aspx?page=kscl_thuchien_quanlyhosokiemsoatchatluong|/admin.aspx?page=kscl_thuchien_bocauhoikiemtrahethong|/admin.aspx?page=kscl_thuchien_bocauhoikiemtrakythuat" +
               "|/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrahethong|/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrakythuat|/admin.aspx?page=kscl_thuchien_tonghopketquakiemtra|" +
               "/admin.aspx?page=kscl_ketthuc_xulysaipham|/admin.aspx?page=kscl_ketthuc_xulysaipham_noidung|",1); %> ><a href=""><span class="iconfa-legal"></span> Quản lý kiểm soát chất lượng</a>
                	<ul <% checkmenu("|/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra|/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra_lapdanhsach|/admin.aspx?page=kscl_chuanbi_danhsachbaocaotukiemtra|/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec|/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec_lapdanhsach" +
               "|/admin.aspx?page=kscl_chuanbi_danhsachcongtykiemtratructiep|/admin.aspx?page=kscl_chuanbi_danhsachcongtykiemtratructiep_lapdanhsach|/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra|/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra_lapdoan|" +
               "/admin.aspx?page=kscl_chuanbi_capnhatgiodaotaochothanhviendoankiemtra|/admin.aspx?page=kscl_thuchien_camketdoclapbaomatthongtin|/admin.aspx?page=kscl_thuchien_quanlyhosokiemsoatchatluong|/admin.aspx?page=kscl_thuchien_bocauhoikiemtrahethong|/admin.aspx?page=kscl_thuchien_bocauhoikiemtrakythuat" +
               "|/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrahethong|/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrakythuat|/admin.aspx?page=kscl_thuchien_tonghopketquakiemtra|" +
               "/admin.aspx?page=kscl_ketthuc_xulysaipham|/admin.aspx?page=kscl_ketthuc_xulysaipham_noidung|",2); %> >
                        
                       <li class="dropdown"    ><a href="#" style="font-weight:bold">Giai đoạn chuẩn bị kiểm tra ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra|/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra_lapdanhsach|/admin.aspx?page=kscl_chuanbi_danhsachbaocaotukiemtra|/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec|/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec_lapdanhsach" +
               "|/admin.aspx?page=kscl_chuanbi_danhsachcongtykiemtratructiep|/admin.aspx?page=kscl_chuanbi_danhsachcongtykiemtratructiep_lapdanhsach|/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra|/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra_lapdoan|" +
               "/admin.aspx?page=kscl_chuanbi_capnhatgiodaotaochothanhviendoankiemtra|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra|/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra_lapdanhsach|"); %> ><a href="admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra">Danh sách công ty kiểm toán phải báo cáo tự kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_chuanbi_danhsachbaocaotukiemtra|"); %> ><a href="admin.aspx?page=kscl_chuanbi_danhsachbaocaotukiemtra">Danh sách báo cáo tự kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec|/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec_lapdanhsach|"); %> ><a href="admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec">Danh sách kiểm tra theo yêu cầu</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_chuanbi_danhsachcongtykiemtratructiep|"); %> ><a href="admin.aspx?page=kscl_chuanbi_danhsachcongtykiemtratructiep">Danh sách Công ty kiểm tra trực tiếp</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra|"); %> ><a href="admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra">Danh sách đoàn kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_chuanbi_capnhatgiodaotaochothanhviendoankiemtra|"); %> ><a href="admin.aspx?page=kscl_chuanbi_capnhatgiodaotaochothanhviendoankiemtra">Cập nhật giờ CNKT cho thành viên đoàn kiểm tra</a></li>
                        </ul>

                        
                        <li class="dropdown"    ><a href="#" style="font-weight:bold">Giai đoạn thực hiện kiểm tra tại công ty kiểm toán ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=bienbanketquakiemtra|/admin.aspx?page=baocaoketquakiemtrabaocaotaichinh|/admin.aspx?page=baocaoketquakiemtrahethong|/admin.aspx?page=bocauhoikiemtrabaocaotaichinh|/admin.aspx?page=bocauhoikiemtrahethong|/admin.aspx?page=quanlyhosokiemsoatchatluong|/admin.aspx?page=kscl_thuchien_camketdoclapbaomatthongtin" +
               "|/admin.aspx?page=kscl_thuchien_quanlyhosokiemsoatchatluong|/admin.aspx?page=kscl_thuchien_bocauhoikiemtrahethong|/admin.aspx?page=kscl_thuchien_bocauhoikiemtrakythuat|/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrahethong|/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrakythuat|/admin.aspx?page=kscl_thuchien_tonghopketquakiemtra|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=kscl_thuchien_camketdoclapbaomatthongtin|"); %> ><a href="admin.aspx?page=kscl_thuchien_camketdoclapbaomatthongtin">Cam kết độc lập/bảo mật thông tin</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_thuchien_quanlyhosokiemsoatchatluong|"); %> ><a href="admin.aspx?page=kscl_thuchien_quanlyhosokiemsoatchatluong">Quản lý hồ sơ kiểm soát chất lượng</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=kscl_thuchien_bocauhoikiemtrahethong|"); %> ><a href="admin.aspx?page=kscl_thuchien_bocauhoikiemtrahethong">Bộ câu hỏi kiểm tra hệ thống</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_thuchien_bocauhoikiemtrakythuat|"); %> ><a href="admin.aspx?page=kscl_thuchien_bocauhoikiemtrakythuat">Bộ câu hỏi kiểm tra kỹ thuật</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrahethong|"); %> ><a href="admin.aspx?page=kscl_thuchien_baocaoketquakiemtrahethong">Báo cáo kết quả kiểm tra hệ thống</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrakythuat|"); %> ><a href="admin.aspx?page=kscl_thuchien_baocaoketquakiemtrakythuat">Báo cáo kết quả kiểm tra kỹ thuật</a></li>
                            <li <% checkmenu("|/admin.aspx?page=kscl_thuchien_tonghopketquakiemtra|"); %> ><a href="admin.aspx?page=kscl_thuchien_tonghopketquakiemtra">Tổng hợp kết quả kiểm tra</a></li>
                        </ul>

                        <li class="dropdown"    ><a href="#" style="font-weight:bold">Giai đoạn kết thúc kiểm tra ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=kscl_ketthuc_xulysaipham|/admin.aspx?page=kscl_ketthuc_xulysaipham_noidung|",2); %> >
                            <%--<li <% checkmenu("|/admin.aspx?page=baocaotonghopkiemsoatchatluong|"); %> ><a href="admin.aspx?page=baocaotonghopkiemsoatchatluong">Báo cáo tổng hợp kiểm soát chất lượng và đề xuất xử lý sai phạm</a></li>--%>
                            <li <% checkmenu("|/admin.aspx?page=kscl_ketthuc_xulysaipham|"); %> ><a href="admin.aspx?page=kscl_ketthuc_xulysaipham">Xử lý sai phạm của BTC và VACPA</a></li>                                                        
                        </ul>


                        </li>                                                
                    </ul>
                </li>


                    
  
  
  
<li <% checkmenu("|/admin.aspx?page=quanlyphihoivien|/admin.aspx?page=quanlyphidanglogo|/admin.aspx?page=quanlyphicapnhatkienthuctapthe|/admin.aspx?page=quanlyphikiemsoatchatluong|/admin.aspx?page=quanlyphikhac|/admin.aspx?page=quanlyphidanhsachphi|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicapnhatkienthuc|/admin.aspx?page=thongbaonophoiphi|/admin.aspx?page=quanlyphi|/admin.aspx?page=qlhv_danhsachcongtydangkydanglogo|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicnkt|/admin.aspx?page=thongbaonophoiphi|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thanhtoanphi_quanly|",1); %> ><a href=""><span class="iconfa-money"></span> Thanh toán</a>
                	<ul <%checkmenu("|/admin.aspx?page=quanlyphihoivien|/admin.aspx?page=quanlyphidanglogo|/admin.aspx?page=quanlyphicapnhatkienthuctapthe|/admin.aspx?page=quanlyphikiemsoatchatluong|/admin.aspx?page=quanlyphikhac|/admin.aspx?page=quanlyphidanhsachphi|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicapnhatkienthuc|/admin.aspx?page=thongbaonophoiphi|/admin.aspx?page=quanlyphi|/admin.aspx?page=qlhv_danhsachcongtydangkydanglogo|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicnkt|/admin.aspx?page=thongbaonophoiphi|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thanhtoanphi_quanly|",2); %> >
                         
                        <li class="dropdown"    ><a href="#" style="font-weight:bold">Quản lý phí ►</a>
                        <ul <% checkmenu("|/admin.aspx?page=quanlyphihoivien|/admin.aspx?page=quanlyphidanglogo|/admin.aspx?page=quanlyphicapnhatkienthuctapthe|/admin.aspx?page=quanlyphikiemsoatchatluong|/admin.aspx?page=quanlyphikhac|/admin.aspx?page=quanlyphidanhsachphi|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=quanlyphihoivien|"); %> ><a href="admin.aspx?page=quanlyphihoivien">Phí hội viên</a></li>
                            <li <% checkmenu("|/admin.aspx?page=quanlyphidanglogo|"); %> ><a href="admin.aspx?page=quanlyphidanglogo">Phí đăng logo</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=quanlyphicapnhatkienthuctapthe|"); %> ><a href="admin.aspx?page=quanlyphicapnhatkienthuctapthe">Phí cập nhật kiến thức</a></li>
                            <li <% checkmenu("|/admin.aspx?page=quanlyphikiemsoatchatluong|"); %> ><a href="admin.aspx?page=quanlyphikiemsoatchatluong">Phí kiểm soát chất lượng</a></li>
                            <li <% checkmenu("|/admin.aspx?page=quanlyphikhac|"); %> ><a href="admin.aspx?page=quanlyphikhac">Phí khác</a></li>
                            <li <% checkmenu("|/admin.aspx?page=quanlyphidanhsachphi|"); %> ><a href="admin.aspx?page=quanlyphidanhsachphi">Danh sách phí</a></li>                            
                        </ul>
                        <li id="Li2" <% checkmenu("|/admin.aspx?page=qlhv_danhsachcongtydangkydanglogo|"); %> ><a href="admin.aspx?page=qlhv_danhsachcongtydangkydanglogo" style="font-weight:bold">Danh sách công ty đăng ký đăng logo</a></li>   
                        <li class="dropdown"    ><a href="#" style="font-weight:bold">Thông báo nộp phí ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicapnhatkienthuc|/admin.aspx?page=thongbaonophoiphi|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=thongbaonophoiphi|"); %> ><a href="admin.aspx?page=thongbaonophoiphi">Thông báo nộp hội phí</a></li>
                            <li <% checkmenu("|/admin.aspx?page=thongbaonopphicapnhatkienthuc|"); %> ><a href="admin.aspx?page=thongbaonopphicapnhatkienthuc">Thông báo nộp phí cập nhật kiến thức</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=thongbaonopphikiemsoatchatluong|"); %> ><a href="admin.aspx?page=thongbaonopphikiemsoatchatluong">Thông báo nộp phí kiểm soát chất lượng</a></li>
                            <li <% checkmenu("|/admin.aspx?page=thongbaonopphidanglogo|"); %> ><a href="admin.aspx?page=thongbaonopphidanglogo">Thông báo nộp phí đăng logo</a></li>
                        </ul>

                        
                        <li class="dropdown"    ><a href="#" style="font-weight:bold">Thanh toán phí ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thanhtoanphi_quanly|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=thanhtoanphicanhan|"); %> ><a href="admin.aspx?page=thanhtoanphicanhan">Thanh toán phí cá nhân</a></li>
                            <li <% checkmenu("|/admin.aspx?page=thanhtoanphitapthe|"); %> ><a href="admin.aspx?page=thanhtoanphitapthe">Thanh toán phí tổ chức</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=thanhtoanphi_quanly|"); %> ><a href="admin.aspx?page=thanhtoanphi_quanly">Quản lý giao dịch thanh toán</a></li>                            
                        </ul>

                        

                        </li>                                               
                    </ul>
                </li>  
  


   <li <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc|/admin.aspx?page=CNKT_BaoCao_InGCNGioCNKT|/admin.aspx?page=CNKT_BaoCao_MauDangKyThamDuCNKT|/admin.aspx?page=BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT|/admin.aspx?page=BTC_BaoCao_VeViecToChucLopHocCNKT|/admin.aspx?page=BTC_BaoCao_KetQuaToChucLopHocCNKTKTV|/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet|/admin.aspx?page=CNKT_BaoCao_DanhSachKTVThamDuCNKT|/admin.aspx?page=CNKT_BaoCao_DanhSachNQTThamDuCNKT|/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTV|" +
               "/admin.aspx?page=TT_BaoCao_NopPhiHoiVienCaNhan|/admin.aspx?page=TT_BaoCao_NopPhiHoiVienTapThe|/admin.aspx?page=TT_BaoCao_NopPhiKSCLcuaKTV|/admin.aspx?page=TT_BaoCao_NopPhiKSCLtheoCongTy|/admin.aspx?page=TT_BaoCao_NopPhiCNKTcuaKTV|/admin.aspx?page=TT_BaoCao_NopPhiDangLogo|/admin.aspx?page=TT_BaoCao_NopPhiKhacHoiVienvaNQT|/admin.aspx?page=TT_BaoCao_NopPhiKhacCuaTapTheVaCongTy|" +
               "/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra|/admin.aspx?page=KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra|/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep|/admin.aspx?page=KSCL_BaoCao_DanhSachHoiVienKiemTraChatLuongDichVu|/admin.aspx?page=KSCL_BaoCao_DanhSachThanhVienDoanKiemTraNam|/admin.aspx?page=KSCL_BaoCao_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien|" +
               "/admin.aspx?page=KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien|/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraHeThong|/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraKyThuat|/admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraHeThong|/admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraKyThuat|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamHeThong|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamKyThuat|/admin.aspx?page=KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam|" +
               "/admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienTapThe|/admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienCaNhan|/admin.aspx?page=QLHV_BaoCao_QuyetDinhKetNapHoiVienCaNhan|/admin.aspx?page=QLHV_BC_QuyetDinhKetNapHoiVienTapThe|/admin.aspx?page=QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan|/admin.aspx?page=QLHV_BC_QuyetDinhXoaTenHoiVien|/admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienCaNhan|/admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienTapThe|"+
               "/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienTapThe|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienTapThe|/admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc|/admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet|/admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc|/admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet|/admin.aspx?page=BaoCaoDanhSachNguoiQuanTamTrongCaNuoc|/admin.aspx?page=QLHV_BC_GiayChungNhanHoiVienTapThe|/admin.aspx?page=QLHV_BC_ThongTinHoiVien|/admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienCaNhan|/admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienTapThe|/admin.aspx?page=QLHV_BC_ThongTinHoiVien|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienCaNhan|/admin.aspx?page=QLHV_BC_TheHoiVienCaNhan|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamKyThuat_TungCongTy|",1); %>>
                <a href=""><span class="iconfa-bar-chart"></span>Báo cáo</a>
                <ul <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc|/admin.aspx?page=CNKT_BaoCao_InGCNGioCNKT|/admin.aspx?page=CNKT_BaoCao_MauDangKyThamDuCNKT|/admin.aspx?page=BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT|/admin.aspx?page=BTC_BaoCao_VeViecToChucLopHocCNKT|/admin.aspx?page=BTC_BaoCao_KetQuaToChucLopHocCNKTKTV|/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet|/admin.aspx?page=CNKT_BaoCao_DanhSachKTVThamDuCNKT|/admin.aspx?page=CNKT_BaoCao_DanhSachNQTThamDuCNKT|/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTV|" +
               "/admin.aspx?page=TT_BaoCao_NopPhiHoiVienCaNhan|/admin.aspx?page=TT_BaoCao_NopPhiHoiVienTapThe|/admin.aspx?page=TT_BaoCao_NopPhiKSCLcuaKTV|/admin.aspx?page=TT_BaoCao_NopPhiKSCLtheoCongTy|/admin.aspx?page=TT_BaoCao_NopPhiCNKTcuaKTV|/admin.aspx?page=TT_BaoCao_NopPhiDangLogo|/admin.aspx?page=TT_BaoCao_NopPhiKhacHoiVienvaNQT|/admin.aspx?page=TT_BaoCao_NopPhiKhacCuaTapTheVaCongTy|" +
               "/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra|/admin.aspx?page=KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra|/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep|/admin.aspx?page=KSCL_BaoCao_DanhSachHoiVienKiemTraChatLuongDichVu|/admin.aspx?page=KSCL_BaoCao_DanhSachThanhVienDoanKiemTraNam|/admin.aspx?page=KSCL_BaoCao_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien|" +
               "/admin.aspx?page=KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien|/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraHeThong|/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraKyThuat|/admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraHeThong|/admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraKyThuat|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamHeThong|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamKyThuat|/admin.aspx?page=KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam|" +
               "/admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienTapThe|/admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienCaNhan|/admin.aspx?page=QLHV_BaoCao_QuyetDinhKetNapHoiVienCaNhan|/admin.aspx?page=QLHV_BC_QuyetDinhKetNapHoiVienTapThe|/admin.aspx?page=QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan|/admin.aspx?page=QLHV_BC_QuyetDinhXoaTenHoiVien|/admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienCaNhan|/admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienTapThe|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienTapThe|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienTapThe|/admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc|/admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet|/admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc|/admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet|/admin.aspx?page=BaoCaoDanhSachNguoiQuanTamTrongCaNuoc|/admin.aspx?page=QLHV_BC_GiayChungNhanHoiVienTapThe|/admin.aspx?page=QLHV_BC_ThongTinHoiVien|/admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienCaNhan|/admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienTapThe|/admin.aspx?page=QLHV_BC_ThongTinHoiVien|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienCaNhan|/admin.aspx?page=QLHV_BC_TheHoiVienCaNhan|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamKyThuat_TungCongTy|",2); %>>
                     <li class="dropdown"    ><a href="#" style="font-weight:bold">Quản lý thông tin chung ►</a>  
                        <ul <% checkmenu("|/admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc|/admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet|/admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc|/admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet|/admin.aspx?page=BaoCaoDanhSachNguoiQuanTamTrongCaNuoc|", 2); %> >
                            <li <% checkmenu("|/admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc|"); %> ><a href="admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc">Danh sách công ty kiểm toán trong cả nước</a></li>
                            <li <% checkmenu("|/admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet|"); %> ><a href="admin.aspx?page=BaoCaoDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet">Danh sách công ty kiểm toán trong cả nước (chi tiết)</a></li>
                            <li <% checkmenu("|/admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc|"); %> ><a href="admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc">Danh sách kiểm toán viên trong cả nước</a></li>
                            <li <% checkmenu("|/admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet|"); %> ><a href="admin.aspx?page=BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet">Danh sách kiểm toán viên trong cả nước (chi tiết)</a></li>
                            <li <% checkmenu("|/admin.aspx?page=BaoCaoDanhSachNguoiQuanTamTrongCaNuoc|"); %> ><a href="admin.aspx?page=BaoCaoDanhSachNguoiQuanTamTrongCaNuoc">Danh sách người quan tâm trong cả nước</a></li>
                        </ul> 
                        </li>  

                    <li class="dropdown"    ><a href="#" style="font-weight:bold">Quản lý hội viên ►</a>  
                        <ul <% checkmenu("|/admin.aspx?page=BTC_QLHV_GiayChungNhanHoiVienTapThe|/admin.aspx?page=BTC_QLHV_TheHoiVienCaNhan|/admin.aspx?page=BTC_QLHV_ThongTinHoiVien|/admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienTapThe|/admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienCaNhan|/admin.aspx?page=QLHV_BaoCao_QuyetDinhKetNapHoiVienCaNhan|/admin.aspx?page=QLHV_BC_QuyetDinhKetNapHoiVienTapThe|/admin.aspx?page=QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan|/admin.aspx?page=QLHV_BC_QuyetDinhXoaTenHoiVien|/admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienCaNhan|/admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienTapThe|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienTapThe|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienTapThe|/admin.aspx?page=QLHV_BC_ThongTinHoiVien|" +
               "/admin.aspx?page=QLHV_BC_GiayChungNhanHoiVienTapThe|/admin.aspx?page=BTC_QLHV_ThongTinHoiVien|/admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienCaNhan|/admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienTapThe|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienCaNhan|/admin.aspx?page=QLHV_BC_TheHoiVienCaNhan|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_GiayChungNhanHoiVienTapThe|"); %> ><a href="admin.aspx?page=QLHV_BC_GiayChungNhanHoiVienTapThe">Giấy chứng nhận hội viên tổ chức</a></li> 
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_TheHoiVienCaNhan|"); %> ><a href="admin.aspx?page=QLHV_BC_TheHoiVienCaNhan">Thẻ hội viên cá nhân</a></li>
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_ThongTinHoiVien|"); %> ><a href="admin.aspx?page=QLHV_BC_ThongTinHoiVien">Thông tin cá nhân kiểm toán viên</a></li>                  
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienCaNhan|"); %> ><a href="admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienCaNhan">Đơn xin gia nhập hội viên cá nhân</a></li>          
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienTapThe|"); %> ><a href="admin.aspx?page=QLHV_BC_DonXinGiaNhapHoiVienTapThe">Đơn xin gia nhập hội viên tổ chức</a></li>
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienCaNhan|"); %> ><a href="admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienCaNhan">Danh sách kết nạp hội viên cá nhân</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienTapThe|"); %> ><a href="admin.aspx?page=QLHV_BaoCao_DanhSachKetNapHoiVienTapThe">Danh sách kết nạp hội viên tổ chức</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BaoCao_QuyetDinhKetNapHoiVienCaNhan|"); %> ><a href="admin.aspx?page=QLHV_BaoCao_QuyetDinhKetNapHoiVienCaNhan">Quyết định kết nạp hội viên cá nhân</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_QuyetDinhKetNapHoiVienTapThe|"); %> ><a href="admin.aspx?page=QLHV_BC_QuyetDinhKetNapHoiVienTapThe">Quyết định kết nạp hội viên tổ chức</a></li> 
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan|"); %> ><a href="admin.aspx?page=QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan">Quyết định khen thưởng hội viên tổ chức và cá nhân</a></li>
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_QuyetDinhXoaTenHoiVien|"); %> ><a href="admin.aspx?page=QLHV_BC_QuyetDinhXoaTenHoiVien">Quyết định xóa tên hội viên</a></li>
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienCaNhan|"); %> ><a href="admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienCaNhan">Danh sách khen thưởng hội viên cá nhân</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienTapThe|"); %> ><a href="admin.aspx?page=QLHV_BC_DanhSachKhenThuongHoiVienTapThe">Danh sách khen thưởng hội viên tổ chức</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienCaNhan|"); %> ><a href="admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienCaNhan">Danh sách kỷ luật hội viên cá nhân</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienTapThe|"); %> ><a href="admin.aspx?page=QLHV_BC_DanhSachKyLuatHoiVienTapThe">Danh sách kỷ luật hội viên tổ chức</a></li>                            
                        </ul> 
                        </li>

                    <li class="dropdown"><a href="#" style="font-weight:bold">Cập nhật kiến thức ►</a>
                        <ul <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc|/admin.aspx?page=CNKT_BaoCao_MauDangKyThamDuCNKT|/admin.aspx?page=CNKT_BaoCao_InGCNGioCNKT|/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet|/admin.aspx?page=CNKT_BaoCao_DanhSachKTVThamDuCNKT|/admin.aspx?page=CNKT_BaoCao_DanhSachNQTThamDuCNKT|/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTV|",2); %>>
                            <li id="cnkt_baocao_thumoithamdulophoc" <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc|"); %>>
                                <a href="admin.aspx?page=CNKT_BaoCao_ThuMoiThamDuLopHoc">Thư mời tham dự lớp học</a></li>
                            <li id="cnkt_baocao_maudangkythamducnkt" <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_MauDangKyThamDuCNKT|"); %>>
                                <a href="admin.aspx?page=CNKT_BaoCao_MauDangKyThamDuCNKT">Mẫu đăng ký tham dự cập nhật
                                    kiến thức</a></li>
                            <li id="cnkt_baocao_ingcngiocnkt" <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_InGCNGioCNKT|"); %>>
                                <a href="admin.aspx?page=CNKT_BaoCao_InGCNGioCNKT">Giấy chứng nhận giờ CNKT</a></li>
                                <li id="Li12" <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_DanhSachKTVThamDuCNKT|"); %> ><a href="admin.aspx?page=CNKT_BaoCao_DanhSachKTVThamDuCNKT">Báo cáo danh sách Kiểm toán viên tham dự lớp Cập nhật kiến thức</a></li>
                            <li id="Li13" <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_DanhSachNQTThamDuCNKT|"); %> ><a href="admin.aspx?page=CNKT_BaoCao_DanhSachNQTThamDuCNKT">Báo cáo danh sách Người quan tâm đăng ký tham dự lớp Cập nhật kiến thức</a></li>
                            <li id="Li14" <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTV|"); %>>
                                <a href="admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTV">Tổng hợp số giờ CNKT của KTV</a></li>
                                <li id="Li3" <% checkmenu("|/admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet|"); %>>
                                <a href="admin.aspx?page=CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet">Tổng hợp số giờ CNKT của KTV (Chi tiết)</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" style="font-weight:bold">Kiểm soát chất lượng ►</a>
                        <ul <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra|/admin.aspx?page=KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra|/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep|/admin.aspx?page=KSCL_BaoCao_DanhSachHoiVienKiemTraChatLuongDichVu|/admin.aspx?page=KSCL_BaoCao_DanhSachThanhVienDoanKiemTraNam|" +
               "/admin.aspx?page=KSCL_BaoCao_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien|/admin.aspx?page=KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien|/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraHeThong|/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraKyThuat|/admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraHeThong|/admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraKyThuat|" +
               "/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamHeThong|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamKyThuat|/admin.aspx?page=KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamKyThuat_TungCongTy|",2); %>>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam">Danh sách công ty kiểm toán đã kiểm tra chất lượng qua các năm</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra">Danh sách
                                    công ty kiểm toán phải nộp báo cáo tự kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra">Bảng tình hình nộp báo cáo tự kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep">Danh sách công ty kiểm tra trực tiếp</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_DanhSachHoiVienKiemTraChatLuongDichVu|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_DanhSachHoiVienKiemTraChatLuongDichVu">Danh sách hội viên kiểm tra chất lượng dịch vụ năm</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_DanhSachThanhVienDoanKiemTraNam|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_DanhSachThanhVienDoanKiemTraNam">Danh sách thành viên đoàn kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamHeThong|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamHeThong">Bảng tổng hợp sai phạm hệ thống</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamKyThuat_TungCongTy|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_BangTongHopSaiPhamKyThuat_TungCongTy">Bảng tổng hợp sai phạm kỹ thuật</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien">Tổng hợp sai phạm và hình thức kỷ luật với hội viên</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraHeThong|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraHeThong">Bảng câu hỏi kiểm tra hệ thống</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraKyThuat|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraKyThuat">Bảng câu hỏi kiểm tra kỹ thuật</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraHeThong|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraHeThong">Bảng chấm điểm kiểm tra hệ thống</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraKyThuat|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_BangChamDiemKiemTraKyThuat">Bảng chấm điểm kiểm tra kỹ thuật</a></li>
                            <li <% checkmenu("|/admin.aspx?page=KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien|"); %>>
                                <a href="admin.aspx?page=KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien">Quyết định xử lý kỷ luật Hội viên có sai phạm về chất lượng dịch vụ kiểm toán BCTC</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" style="font-weight:bold">Thanh toán ►</a>
                        <ul <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiHoiVienCaNhan|/admin.aspx?page=TT_BaoCao_NopPhiHoiVienTapThe|/admin.aspx?page=TT_BaoCao_NopPhiKSCLcuaKTV|/admin.aspx?page=TT_BaoCao_NopPhiKSCLtheoCongTy|/admin.aspx?page=TT_BaoCao_NopPhiCNKTcuaKTV|/admin.aspx?page=TT_BaoCao_NopPhiDangLogo|/admin.aspx?page=TT_BaoCao_NopPhiKhacHoiVienvaNQT|/admin.aspx?page=TT_BaoCao_NopPhiKhacCuaTapTheVaCongTy|",2); %>>
                            <li id="Li4" <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiHoiVienCaNhan|"); %>><a
                                href="admin.aspx?page=TT_BaoCao_NopPhiHoiVienCaNhan">Báo cáo nộp hội phí cá nhân</a></li>
                            <li id="Li5" <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiHoiVienTapThe|"); %>><a
                                href="admin.aspx?page=TT_BaoCao_NopPhiHoiVienTapThe">Báo cáo nộp hội phí tổ chức</a></li>
                            <li id="Li6" <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiKSCLcuaKTV|"); %>><a
                                href="admin.aspx?page=TT_BaoCao_NopPhiKSCLcuaKTV">Báo cáo nộp phí kiểm soát chất
                                lượng của kiểm toán viên</a></li>
                            <li id="Li7" <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiKSCLtheoCongTy|"); %>><a
                                href="admin.aspx?page=TT_BaoCao_NopPhiKSCLtheoCongTy">Báo cáo nộp phí kiểm soát
                                chất lượng theo công ty</a></li>
                            <li id="Li8" <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiCNKTcuaKTV|"); %>><a
                                href="admin.aspx?page=TT_BaoCao_NopPhiCNKTcuaKTV">Báo cáo nộp phí cập nhật kiến
                                thức của kiểm toán viên</a></li>
                            <li id="Li9" <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiDangLogo|"); %>><a href="admin.aspx?page=TT_BaoCao_NopPhiDangLogo">
                                Báo cáo nộp phí đăng logo</a></li>
                            <li id="Li10" <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiKhacHoiVienvaNQT|"); %>>
                                <a href="admin.aspx?page=TT_BaoCao_NopPhiKhacHoiVienvaNQT">Báo cáo nộp phí khác của
                                    hội viên cá nhân và người quan tâm</a></li>
                            <li id="Li11" <% checkmenu("|/admin.aspx?page=TT_BaoCao_NopPhiKhacCuaTapTheVaCongTy|"); %>>
                                <a href="admin.aspx?page=TT_BaoCao_NopPhiKhacCuaTapTheVaCongTy">Báo cáo nộp phí khác
                                    của hội viên tổ chức và công ty kiểm toán</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" style="font-weight:bold">Báo cáo Bộ tài chính ►</a>
                        <ul <% checkmenu("|/admin.aspx?page=BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT|/admin.aspx?page=BTC_BaoCao_VeViecToChucLopHocCNKT|/admin.aspx?page=BTC_BaoCao_KetQuaToChucLopHocCNKTKTV|",2); %>>
                            <li <% checkmenu("|/admin.aspx?page=BTC_BaoCao_VeViecToChucLopHocCNKT|"); %>><a href="admin.aspx?page=BTC_BaoCao_VeViecToChucLopHocCNKT">
                                Báo cáo về việc tổ chức lớp học cập nhật kiến thức</a></li>
                            <li <% checkmenu("|/admin.aspx?page=BTC_BaoCao_KetQuaToChucLopHocCNKTKTV|"); %>><a
                                href="admin.aspx?page=BTC_BaoCao_KetQuaToChucLopHocCNKTKTV">Báo cáo kết quả tổ chức
                                lớp học CNKT KTV</a></li>
                            <li <% checkmenu("|/admin.aspx?page=BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT|"); %>>
                                <a href="admin.aspx?page=BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT">Báo cáo tổng hợp
                                    kết quả tổ chức lớp học cập nhật kiến thức kiểm toán viên (năm)</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
  
            
               
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->


    <script type="text/javascript">
<%
 
 

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyTaiKhoanCB", cm.connstr).Contains("XEM|"))
    {
        Response.Write("$('#mnu_tkcb').remove();");        
    }  
    

    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyTaiKhoanKH", cm.connstr).Contains("XEM|"))
    {
        Response.Write("$('#mnu_tkkh').remove();");        
    }  

       if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyNhomQuyen", cm.connstr).Contains("XEM|"))
    {
        Response.Write("$('#mnu_nhomquyen').remove();");        
    }  
 %>
 </script>




