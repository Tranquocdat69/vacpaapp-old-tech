﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="form.ascx.cs" Inherits="usercontrols_form" %>

<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="js/jquery.uniform.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="js/jquery.autogrow-textarea.js"></script>
<script type="text/javascript" src="js/charCount.js"></script>
<script type="text/javascript" src="js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="js/forms.js"></script>

 <div class="widget">
            <h4 class="widgettitle">Form Elements</h4>
            <div class="widgetcontent">
                <form class="stdform" runat="server" method="post">
                    	
                        <p>
                            <label>Small Input</label>
                            <span class="field">
                            <asp:TextBox ID="TextBox1" runat="server" class="input-small" placeholder=".input-small" />
                                </span>
                            <small class="desc">Small description of this field.</small>
                        </p>
                        
                        <p>
                            <label>Medium Input</label>
                            <span class="field">
                            <asp:TextBox ID="TextBox2" runat="server" class="input-medium" placeholder=".input-medium"  />
                            </span>
                        </p>
                        
                        <p>
                            <label>Large Input</label>
                            <span class="field">                            
                                <asp:TextBox ID="TextBox3" runat="server" class="input-large" placeholder=".input-large"  />
                            </span>
                        </p>
                        
                        <p>
                        	<label>X Large Input</label>
                            <span class="field">
                            <asp:TextBox ID="TextBox4" runat="server" class="input-xlarge" placeholder=".input-xlarge"  />
                            
                            </span>
                        </p>
                        
                        <p>
                            <label>XX Large Input</label>
                            <span class="field">
                            <asp:TextBox ID="TextBox5" runat="server" class="input-xxlarge" placeholder=".input-xxlarge"  />
                            </span>
                        </p>
                        
                        <p>
                            <label>Input Block Level</label>
                            <span class="field">
                            <asp:TextBox ID="TextBox7" runat="server" class="input-block-level" placeholder=".input-block-level"  />                            
                            </span>
                        </p>
                        
                        <div class="par control-group warning">
                          <label for="inputWarning" class="control-label">Input with warning</label>
                          <div class="controls">
                          <asp:TextBox ID="TextBox6" runat="server" class="span4" placeholder=".span4"  />                
                            
                            <span class="help-inline">Something may have gone wrong</span>
                          </div>
                        </div>
                        
                        <div class="par control-group error">
                          <label for="inputError" class="control-label">Input with error</label>
                          <div class="controls">                            
                            <asp:TextBox ID="TextBox8" runat="server" class="span4" placeholder=".span4"  /> 
                            <span class="help-inline">Please correct the error</span>
                          </div>
                        </div><!--par-->
                        
                        <div class="par control-group info">
                          <label for="inputInfo" class="control-label">Input with info</label>
                          <div class="controls">                            
                            <asp:TextBox ID="TextBox9" runat="server" class="span4" placeholder=".span4"  />
                            <span class="help-inline">Username is taken</span>
                          </div>
                        </div><!--par-->
                        
                        <div class="par control-group success">
                          <label for="inputSuccess" class="control-label">Input with success</label>
                          <div class="controls">
                          <asp:TextBox ID="TextBox10" runat="server" class="span4" placeholder=".span4"  />                            
                            <span class="help-inline">Woohoo!</span>
                          </div>
                        </div><!--par-->
                        
                        <p>
                            <label>Textarea</label>
                            <asp:TextBox ID="TextBox11" runat="server" Rows="5" TextMode="MultiLine" 
                                Width="500px"></asp:TextBox>
                            </span> 
                        </p>
			
			<p>
                            <label>Textarea with Auto Resize</label>
                            <span class="field">
                            <asp:TextBox ID="autoResizeTA" ClientIDMode="Static"  runat="server" Rows="5" TextMode="MultiLine" 
                                Width="500px" class="span5" style="resize: vertical"></asp:TextBox>
                            
                            </span> 
                        </p>
                        
                        <p>
                            <label>Textarea with Character Count</label>
                            <span class="field">
                            <asp:TextBox ID="textarea2" ClientIDMode="Static"  runat="server" Rows="5" TextMode="MultiLine" 
                                Width="500px" class="span5" ></asp:TextBox>
                               
                            </span> 
                        </p>
                        
                        
                        
                        <p>
                            <label>Select</label>
                            <span class="field">
                                <asp:DropDownList ID="DropDownList1" class="uniformselect" runat="server">
                                    <asp:ListItem>Choose One</asp:ListItem>
                                    <asp:ListItem>Selection One</asp:ListItem>
                                    <asp:ListItem>Selection Two</asp:ListItem>
                                    <asp:ListItem>Selection Three</asp:ListItem>
                                    <asp:ListItem>Selection Four</asp:ListItem>
                                </asp:DropDownList>

                            
                            </span>
                        </p>
                        <br>
                        <p>
                            <label>Disabled Select</label>
                            <span class="field">


                                <asp:DropDownList ID="DropDownList2" disabled="disabled" class="uniformselect" runat="server">
                                    <asp:ListItem>Choose One</asp:ListItem>
                                    <asp:ListItem>Selection One</asp:ListItem>
                                    <asp:ListItem>Selection Two</asp:ListItem>
                                    <asp:ListItem>Selection Three</asp:ListItem>
                                    <asp:ListItem>Selection Four</asp:ListItem>
                                </asp:DropDownList>
			
                            </span>
                        </p>
                        <br>
                        <p>
                            <label>Multiple Select</label>
                            <span class="field">

                            <asp:DropDownList ID="DropDownList3" multiple="multiple" class="uniformselect" runat="server">
                                    <asp:ListItem>Choose One</asp:ListItem>
                                    <asp:ListItem>Selection One</asp:ListItem>
                                    <asp:ListItem>Selection Two</asp:ListItem>
                                    <asp:ListItem>Selection Three</asp:ListItem>
                                    <asp:ListItem>Selection Four</asp:ListItem>
                            </asp:DropDownList>

                           
                            </span>
                        </p>
                        
                        <p>
                            <label>Dual Select</label>
                            <span id="dualselect" class="dualselect">

                            <asp:DropDownList ID="DropDownList4"  multiple="multiple" size="10" class="uniformselect" runat="server">
                                    <asp:ListItem>Choose One</asp:ListItem>
                                    <asp:ListItem>Selection One</asp:ListItem>
                                    <asp:ListItem>Selection Two</asp:ListItem>
                                    <asp:ListItem>Selection Three</asp:ListItem>
                                    <asp:ListItem>Selection Four</asp:ListItem>
                            </asp:DropDownList>


                         
                                <span class="ds_arrow">
                                    <button class="btn ds_prev"><i class="iconfa-chevron-left"></i></button><br />
                                    <button class="btn ds_next"><i class="iconfa-chevron-right"></i></button>
                                </span>
                                <select name="select4" multiple="multiple" size="10">
                                    <option value=""></option>
                                </select>
                            </span>
                        </p>
                        
                        <p>
                            <label>Select with Search</label>
                            <span class="formwrapper">

                              <asp:DropDownList ID="DropDownList5" style="width:350px" class="chzn-select" tabindex="2" runat="server">
                                    <asp:ListItem>Choose One</asp:ListItem>
                                    <asp:ListItem>Selection One</asp:ListItem>
                                    <asp:ListItem>Selection Two</asp:ListItem>
                                    <asp:ListItem>Selection Three</asp:ListItem>
                                    <asp:ListItem>Selection Four</asp:ListItem>
                            </asp:DropDownList>

                            	
                            </span>
                        </p>
                        
                        <p>
                            <label>Enhanced Multiple Select</label>
                            <span class="formwrapper">

                            <asp:DropDownList ID="DropDownList6" style="width:350px" class="chzn-select"  multiple="multiple" tabindex="4" runat="server">
                                    <asp:ListItem>Choose One</asp:ListItem>
                                    <asp:ListItem>Selection One</asp:ListItem>
                                    <asp:ListItem>Selection Two</asp:ListItem>
                                    <asp:ListItem>Selection Three</asp:ListItem>
                                    <asp:ListItem>Selection Four</asp:ListItem>
                            </asp:DropDownList>


                              
                            </span>
                        </p>
                        
                        <p>
                            <label>Radio Buttons</label>
                            <span class="formwrapper">
                                <asp:RadioButton ID="RadioButton1" runat="server" />
                            	 Unchecked Radio &nbsp; &nbsp;
                                <asp:RadioButton ID="RadioButton2" runat="server" Checked="true" /> Checked Radio &nbsp; &nbsp;
                                <asp:RadioButton ID="RadioButton3" runat="server" Enabled="false" />
                                 Disabled Radio 
                            </span>
                        </p>
                                                                        
                        <p>
                            <label>Checkboxes</label>
                            <span class="formwrapper">
                                <asp:CheckBox ID="CheckBox1" runat="server" /> Unchecked Checkbox<br />
                                <asp:CheckBox ID="CheckBox2" runat="server" Checked="true" /> Checked Checkbox <br />
                                <asp:CheckBox ID="CheckBox3" runat="server" Enabled="false" /> Disabled Checkbox <br /> 
                                <asp:CheckBox ID="CheckBox4" runat="server" Enabled="false" Checked="true" /> Disabled Checked Checkbox
                            </span>
                        </p>
			
			<div class="par">
			    <label>File Upload</label>
			    <div class="fileupload fileupload-new" data-provides="fileupload">
				<div class="input-append">
				<div class="uneditable-input span3">
				    <i class="iconfa-file fileupload-exists"></i>
				    <span class="fileupload-preview"></span>
				</div>
				<span class="btn btn-file"><span class="fileupload-new">Chọn file</span>
				<span class="fileupload-exists">Chọn file khác</span>
				
                <asp:FileUpload  runat="server" ID="fu" />
                
                </span>
                    
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Bỏ qua</a>
				</div>
			    </div>
			</div>
            
                      
                        <div class="par">
                            <label>Date Picker</label>
                            <span class="field">
                                <asp:TextBox ID="datepicker" ClientIDMode="Static" runat="server" class="input-small"></asp:TextBox>
                            
                            </span>
                        </div> 
                        
                        <p>
                            <label>Input Tags</label>
                            <span class="field">
                                <asp:TextBox ID="tags" class="input-large" value="foo,bar,baz" ClientIDMode="Static" runat="server"></asp:TextBox>                            	
                            </span>
                        </p>
                        
                      
               
                         
                      
                        
                      
                        
                </form>
            </div><!--widgetcontent-->
            </div><!--widget-->
            
                
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle">With Form Validation</h4>
                <div class="widgetcontent wc1">
                    <form id="form1" class="stdform" method="post" action="forms.html">
                            <div class="par control-group">
                                    <label class="control-label" for="firstname">First Name</label>
                                <div class="controls">
                                <input type="text" name="firstname" id="firstname" class="input-large" />

                                </div>
                            </div>
                            
                            <div class="control-group">
                                    <label class="control-label" for="lastname">Last Name</label>
                                <div class="controls"><input type="text" name="lastname" id="lastname" class="input-large" /></div>
                            </div>
                            
                            <div class="par control-group">
                                    <label class="control-label" for="email">Email</label>
                                <div class="controls"><input type="text" name="email" id="email" class="input-xlarge" /></div>
                            </div>
                            
                            <div class="par control-group">
                                    <label class="control-label" for="location">Location</label>
                                <div class="controls"><textarea cols="80" rows="5" name="location" class="input-xxlarge" id="location"></textarea></div> 
                            </div>
                                                    
                            <p class="stdformbutton">
                                    <button class="btn btn-primary">Submit Button</button>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->
            
            
            <div class="widgetbox box-inverse">
                <h4 class="widgettitle">Form Bordered</h4>
                <div class="widgetcontent nopadding">
                    <form class="stdform stdform2" method="post" action="forms.html">
                            <p>
                                <label>First Name</label>
                                <span class="field"><input type="text" name="firstname" id="firstname2" class="input-xxlarge" /></span>
                            </p>
                            
                            <p>
                                <label>Last Name</label>
                                <span class="field"><input type="text" name="lastname" id="lastname2" class="input-xxlarge" /></span>
                            </p>
                            
                            <p>
                                <label>Email</label>
                                <span class="field"><input type="text" name="email" id="email2" class="input-xxlarge" /></span>
                            </p>
                            
                            <p>
                                <label>Location <small>You can put your own description for this field here.</small></label>
                                <span class="field"><textarea cols="80" rows="5" name="location" id="location2" class="span5"></textarea></span>
                            </p>
                            
                            <p>
                                <label>Select</label>
                                <span class="field"><select name="selection" id="selection2" class="uniformselect">
                                    <option value="">Choose One</option>
                                    <option value="1">Selection One</option>
                                    <option value="2">Selection Two</option>
                                    <option value="3">Selection Three</option>
                                    <option value="4">Selection Four</option>
                                </select></span>
                            </p>
                                                    
                            <p class="stdformbutton">
                                <button class="btn btn-primary">Submit Button</button>
                                <button type="reset" class="btn">Reset Button</button>
                            </p>
                    
                </div><!--widgetcontent-->
            </div><!--widget-->

