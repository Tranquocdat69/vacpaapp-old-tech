﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
public partial class usercontrols_active : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Tên chức năng
        string tenchucnang = "Kích hoạt tài khoản";
        // Icon CSS Class  
        string IconClass = "iconfa-user";

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"
     
     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));



        try
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblDMKhachHang WHERE KichHoat='"+ Request.QueryString["code"] +"' ";            
            
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);

            if (ds.Tables[0].Rows.Count > 0)
            {
                sql.CommandText = "UPDATE tblDMKhachHang SET KichHoat = NULL WHERE KichHoat='" + Request.QueryString["code"] + "' ";
                DataAccess.RunActionCmd(sql);

                if (ds.Tables[0].Rows[0]["LoaiHoSoTemp"].ToString() == "1")
                    sql.CommandText = "UPDATE tblToKhaiDonViSuDungNganSach SET KhachHangID = " + ds.Tables[0].Rows[0]["KhachHangID"].ToString() + " WHERE ToKhaiDonViSuDungNganSachID=" + ds.Tables[0].Rows[0]["HoSoTempID"].ToString();

                if (ds.Tables[0].Rows[0]["LoaiHoSoTemp"].ToString() == "3")
                    sql.CommandText = "UPDATE tblToKhaiXDCBChuanBiDauTu SET KhachHangID = " + ds.Tables[0].Rows[0]["KhachHangID"].ToString() + " WHERE ToKhaiXDCBChuanBiDauTuID=" + ds.Tables[0].Rows[0]["HoSoTempID"].ToString();

                if (ds.Tables[0].Rows[0]["LoaiHoSoTemp"].ToString() == "4")
                    sql.CommandText = "UPDATE tblToKhaiXDCBThucHienDauTu SET KhachHangID = " + ds.Tables[0].Rows[0]["KhachHangID"].ToString() + " WHERE ToKhaiXDCBThucHienDauTuID=" + ds.Tables[0].Rows[0]["HoSoTempID"].ToString();

                DataAccess.RunActionCmd(sql);

                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Kích hoạt tài khoản thành công! Hãy <a href='login.aspx'>đăng nhập hệ thống</a></div>"));
            }
            else
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không tìm thấy tài khoản trong hệ thống!</div>"));
            }
            
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
}