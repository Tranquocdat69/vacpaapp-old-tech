﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BC_DonXinGiaNhapHoiVienCaNhan : System.Web.UI.UserControl
{
    protected string tenchucnang = "Đơn xin gia nhập hội viên cá nhân";
    //protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    //private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    public Commons cm = new Commons();
    //public DataTable dt = new DataTable();
   // public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    //protected void SetValueByJs()
    //{
    //    string js = "";
    //    js += "$('#abcID123').val('123897128973');" + Environment.NewLine;
    //    Response.Write(js);
    //}

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim() == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoaiHinhDoanNghiep(string DonVi = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value= 0>Tất cả</option>";
        foreach (DataRow loaiDN in dt.Rows)
        {
            if (DonVi == loaiDN["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + loaiDN["HoiVienTapTheID"].ToString().Trim() + @""">" + loaiDN["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + loaiDN["HoiVienTapTheID"].ToString().Trim() + @""">" + loaiDN["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }



    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Trim().Contains(tem.idDoiTuong.ToString().Trim()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }


    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    public void setdataCrystalReport()
    {
        string strid = Request.Form["txtID"];
                //strid = abcID123.Text;
                //strid = 
                //if (Request.Form["abcID123"] != null)
                //    strid = Request.Form["abcID123"].ToString();
        if (!string.IsNullOrEmpty(strid))
        {
            
            List<objDonXinGiaNhapHoiVienCaNhan> lst = new List<objDonXinGiaNhapHoiVienCaNhan>();
            List<lstOBJQuaTrinhLamViec> lstQT = new List<lstOBJQuaTrinhLamViec>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoDem + ' ' + Ten as HoTen, NgaySinh, GioiTinh, a.DiaChi + ' - ' + dbo.LayDiaChi_HVCN(a.DonHoiVienCaNhanId) AS DiaChi, dbo.LayQueQuan(a.DonHoiVienCaNhanId) As QueQuan, "
                                + " b.TenTruongDaiHoc, e.TenChuyenNganhDaoTao, a.ChuyenNganhNam, c.TenHocVi, "
                                + " HocViNam, d.TenHocHam, HocHamNam, SoCMND, CMND_NgayCap, f.TenTinh as noiCapCMT, "
                                + " SoTaiKhoanNganHang, TenNganHang, SoChungChiKTV, NgayCapChungChiKTV, "
                                + " SoGiayChungNhanDKHN, convert(varchar, NgayCapGiayChungNhanDKHN, 103) as NgayCapGiayChungNhanDKHN, HanCapTu, HanCapDen, "
                                + " h.TenChucVu, CASE ISNULL(a.DonViCongTac,'') WHEN '' THEN k.TenDoanhNghiep ELSE a.DonViCongTac END AS DonViCongTac, a.DienThoai, Mobile, a.Email, i.TenSoThich, DonHoiVienCaNhanID, NgayNopDon "
                                + " from dbo.tblDonHoiVienCaNhan a "
                                + " left join dbo.tblHoiVienTapThe k on a.HoiVienTapTheID = k.HoiVienTapTheID "
                                + " left join dbo.tblDMTruongDaiHoc b on a.TruongDaiHocID = b.TruongDaiHocID "
                                + " left join dbo.tblDMHocVi c on a.HocViID = c.HocViID "
                                + " left join dbo.tblDMHocHam d on a.HocHamID = d.HocHamID "
                                + " left join dbo.tblDMChuyenNganhDaoTao e on a.ChuyenNganhDaoTaoID = e.ChuyenNganhDaoTaoID "
                                + " left join dbo.tblDMTinh f on a.CMND_TinhID = f.TinhID "
                                + " left join tblDMNganHang g on a.NganHangID = g.NganHangID "
                                + " left join dbo.tblDMChucVu h on a.ChucVuID = h.ChucVuID "
                                + " left join dbo.tblDMSoThich i on a.SoThichID = i.SoThichID "
                                + " WHERE SoDonHoiVienCaNhan = '" + strid + "'";
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            // int i = 1;
            foreach (DataRow tem in dt.Rows)
            {
                objDonXinGiaNhapHoiVienCaNhan new1 = new objDonXinGiaNhapHoiVienCaNhan();
                new1.HoTen = tem["HoTen"].ToString();
                new1.NgaySinh = !string.IsNullOrEmpty(tem["NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgaySinh"])) : "";
                if (tem["GioiTinh"].ToString() == "1")
                {
                    new1.GioiTinh_Nu = ".";
                    new1.GioiTinh_Nam = "X";
                }
                else
                {
                    new1.GioiTinh_Nam = ".";
                    new1.GioiTinh_Nu = "X";
                }
                new1.ChuyenNganh = tem["TenChuyenNganhDaoTao"].ToString();
                new1.Nam_Bang = tem["ChuyenNganhNam"].ToString();
                new1.QueQuan = tem["QueQuan"].ToString();
                new1.DiaChi = tem["DiaChi"].ToString();
                new1.BangCap = tem["TenTruongDaiHoc"].ToString();
                new1.HocHam = tem["TenHocHam"].ToString();
                new1.HocVi = tem["TenHocVi"].ToString();
                new1.Nam_HocHam = tem["HocHamNam"].ToString();
                new1.Nam_HocVi = tem["HocViNam"].ToString();
                new1.CMT = tem["SoCMND"].ToString();
                new1.NgayCap = !string.IsNullOrEmpty(tem["CMND_NgayCap"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["CMND_NgayCap"])) : "";
                new1.NoiCap = tem["noiCapCMT"].ToString();
                new1.SoTK = tem["SoTaiKhoanNganHang"].ToString();
                new1.TaiNganHang = tem["TenNganHang"].ToString();
                new1.SoChungChi = tem["SoChungChiKTV"].ToString();
                new1.NgayCap_CT = !string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCapChungChiKTV"])) : "";
                new1.GiayCN = tem["SoGiayChungNhanDKHN"].ToString();
                new1.NgayCap_GiayCN = tem["NgayCapGiayChungNhanDKHN"].ToString();
                new1.GiayDangKy = !string.IsNullOrEmpty(tem["HanCapTu"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapTu"])) : "";
                new1.NoiCapGiayDangKy = !string.IsNullOrEmpty(tem["HanCapDen"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapDen"])) : "";

                new1.ChucVu = tem["TenChucVu"].ToString();
                new1.DonViCongTac = tem["DonViCongTac"].ToString();
                new1.Email = tem["Email"].ToString();
                new1.DienThoai = tem["DienThoai"].ToString();
                new1.DD = tem["Mobile"].ToString();
                new1.SoThich = tem["TenSoThich"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayNopDon"].ToString()))
                    new1.NgayThangNam = "Ngày " + Convert.ToDateTime(tem["NgayNopDon"]).Day + " tháng " + Convert.ToDateTime(tem["NgayNopDon"]).Month + " năm " + Convert.ToDateTime(tem["NgayNopDon"]).Year;
                else
                    new1.NgayThangNam = "Ngày.....tháng.....năm.....";
                lst.Add(new1);

                DataSet ds1 = new DataSet();
                SqlCommand sql1 = new SqlCommand();
                sql1.CommandText = "select ThoiGianCongTac, ChucVu, NoiLamViec from dbo.tblDonHoiVienCaNhanQuaTrinhCongTac "
                                        + " where DonHoiVienCaNhanID =  " + Convert.ToInt32(tem["DonHoiVienCaNhanID"]);


                ds1 = DataAccess.RunCMDGetDataSet(sql1);
                sql1.Connection.Close();
                sql1.Connection.Dispose();
                sql1 = null;

                DataTable dt1 = ds1.Tables[0];

                //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
                //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
                // int i = 1;
                foreach (DataRow tem1 in dt1.Rows)
                {
                    lstOBJQuaTrinhLamViec obj123 = new lstOBJQuaTrinhLamViec();
                    obj123.ThoiGian = tem1["ThoiGianCongTac"].ToString();
                    obj123.ChucVu = tem1["ChucVu"].ToString();
                    obj123.DonViCongTac = tem1["NoiLamViec"].ToString();

                    lstQT.Add(obj123);
                }
            }
        

            
            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/DonXinGiaNhapHoiVienCaNhan.rpt"));
            
            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objDonXinGiaNhapHoiVienCaNhan"].SetDataSource(lst);
            rpt.Database.Tables["lstOBJQuaTrinhLamViec"].SetDataSource(lstQT);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);
          
            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "objDonXinGiaNhapHoiVienCaNhan");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "objDonXinGiaNhapHoiVienCaNhan");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            //ReportDocument rdPL = new ReportDocument();
            //rdPL.Load(Application.StartupPath + "\\GCN\\312_313\\" + strFilePL);
            //rdPL.SetDataSource(ds);
            //rdPL.SetParameterValue("NoiCapPL", txtNoiCapPL.Text,);
            //crtXemPL.ReportSource = rdPL;
            //crtXemPL.Refresh();

            //rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            //    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //{
            //    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //    //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //    //xlsFormatOptions.ShowGridLines = true;
            //    //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //    //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //    //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            //}
        }
    }
    public void DinhDangFile(string ma = "1")
    {

        List<objDoiTuong> lst = getDinhDangFile();
        string output_html = "";


        output_html += "<option value=0>Tất cả</option>";
        foreach (var tem in lst)
        {
            if (ma == tem.idDoiTuong.ToString())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    private List<objDoiTuong> getDinhDangFile()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Word";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Excel";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "PDF";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }
    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        txtCongTy.Text = "";
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            textID.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
                        + " from dbo.tblHoiVienCaNhan "
                        + " WHERE MaHoiVienCaNhan = '" + lstIdChon[0] + "'";// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtCongTy.Text = tem["HoTen"].ToString();
            }
           
                
            //CrystalReportViewer1.ReportSource = null;
            //CrystalReportViewer1.DataBind();
        }
    }

    protected void btnChay2_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            idDanhSach.Text = result;

            //if (lstIdChon.Count() != 0)
            //{
            //    DataSet ds = new DataSet();
            //    SqlCommand sql = new SqlCommand();
            //    sql.CommandText = "select TenDoanhNghiep "
            //            + " from dbo.tblHoiVienTapThe "
            //            + " WHERE MaHoiVienTapThe = '" + lstIdChon[0] + "'";// + id;
            //    ds = DataAccess.RunCMDGetDataSet(sql);
            //    sql.Connection.Close();
            //    sql.Connection.Dispose();
            //    sql = null;

            //    DataTable dt = ds.Tables[0];
            //    foreach (DataRow tem in dt.Rows)
            //        txtCongTy.Text = tem["TenDoanhNghiep"].ToString();
            //}
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }

    protected void btnChay1_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            // abcID123.Text = lstIdChon[0];

            //if (lstIdChon.Count() != 0)
            //{
            //    DataSet ds = new DataSet();
            //    SqlCommand sql = new SqlCommand();
            //    sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
            //            + " from dbo.tblHoiVienCaNhan "
            //            + " WHERE HoiVienCaNhanID = " + lstIdChon[0];// + id;
            //    ds = DataAccess.RunCMDGetDataSet(sql);
            //    sql.Connection.Close();
            //    sql.Connection.Dispose();
            //    sql = null;

            //    DataTable dt = ds.Tables[0];
            //    foreach (DataRow tem in dt.Rows)
            //        txtCongTy.Text = tem["HoTen"].ToString();
            //}
            //CrystalReportViewer1.ReportSource = null;
            //CrystalReportViewer1.DataBind();
        }
    }

    protected void Text_Change(object sender, EventArgs e)
    {
        txtCongTy.Text = "";
        try{
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
                        + " from dbo.tblHoiVienCaNhan "
                        + " WHERE MaHoiVienCaNhan = '" + textID.Text+ "'";// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtCongTy.Text = tem["HoTen"].ToString();
        }
        catch { }
    }

}