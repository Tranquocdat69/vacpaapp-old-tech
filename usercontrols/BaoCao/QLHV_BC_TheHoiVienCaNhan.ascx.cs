﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BC_TheHoiVienCaNhan : System.Web.UI.UserControl
{
    protected string tenchucnang = "Thẻ hội viên cá nhân";
    //protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    //private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    

    public void LoadDonViCongTac(string DonVi = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (DonVi == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim().Contains(Tinh["MaTinh"].ToString().Trim()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Contains(tem.idDoiTuong.ToString()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void setdataCrystalReport()
    {
        string strid = id.Text;
        if (!string.IsNullOrEmpty(strid))
        {
            //int idHV = Convert.ToInt32(strid);
            List<objThongTinHoiVien_ChiTiet> lst = new List<objThongTinHoiVien_ChiTiet>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoiVienCaNhanID, HODEM, Ten, NgaySinh, GioiTinh, HanCapTu, HanCapDen, b.TenTinh, "
                        + " a.SoChungChiKTV, a.NgayCapChungChiKTV "
                        + " from dbo.tblHoiVienCaNhan a "
                        + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID "
                        + " WHERE MaHoiVienCaNhan = '" + strid + "'";// + id;
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

       
        foreach (DataRow tem in dt.Rows)
        {
            objThongTinHoiVien_ChiTiet new1 = new objThongTinHoiVien_ChiTiet();
            new1.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            new1.NgaySinh = "Sinh: " + (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()) ? Convert.ToDateTime(tem["NgaySinh"]).Year.ToString() : "") + " tại " + tem["TenTinh"].ToString();
            new1.GioiTinh = tem["GioiTinh"].ToString() == "1" ? "Ông: " : "Bà: ";
            new1.SoChungChi = "Số: " + tem["SoChungChiKTV"].ToString() + " ngày " + (!string.IsNullOrEmpty(tem["HanCapTu"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapTu"])) : "");
            new1.NgayCap_CT = "Thẻ có giá trị đến: " + (!string.IsNullOrEmpty(tem["HanCapDen"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapDen"])) : "");
            //new1.lstQuaTrinhLamViec = new List<lstOBJQuaTrinhLamViec>();
            //new1.lstKhenThuong = new List<lstOBJKhenThuong>();
            lst.Add(new1);

           
        }
        

            
            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/TheHoiVienCaNhan.rpt"));
            
            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objThongTinHoiVien_ChiTiet"].SetDataSource(lst);
            
            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "TheHoiVienCaNhan");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "TheHoiVienCaNhan");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            //ReportDocument rdPL = new ReportDocument();
            //rdPL.Load(Application.StartupPath + "\\GCN\\312_313\\" + strFilePL);
            //rdPL.SetDataSource(ds);
            //rdPL.SetParameterValue("NoiCapPL", txtNoiCapPL.Text,);
            //crtXemPL.ReportSource = rdPL;
            //crtXemPL.Refresh();

            //rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            //    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //{
            //    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //    //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //    //xlsFormatOptions.ShowGridLines = true;
            //    //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //    //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //    //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            //}
        }
    }
    

    //private List<objDoiTuong> getDinhDangFile()
    //{
    //    List<objDoiTuong> lst = new List<objDoiTuong>();

    //    objDoiTuong obj1 = new objDoiTuong();
    //    obj1.idDoiTuong = 1;
    //    obj1.TenDoiTuong= "Word";
    //    objDoiTuong obj2 = new objDoiTuong();
    //    obj2.idDoiTuong = 2;
    //    obj2.TenDoiTuong = "Excel";
    //    objDoiTuong obj3 = new objDoiTuong();
    //    obj3.idDoiTuong = 3;
    //    obj3.TenDoiTuong = "PDF";

    //    lst.Add(obj1);
    //    lst.Add(obj2);
    //    lst.Add(obj3);

    //    return lst;
    //}
    protected void Text_Changed(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
                        + " from dbo.tblHoiVienCaNhan "
                        + " WHERE MaHoiVienCaNhan = '" + id.Text + "'";// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtTen.Text = tem["HoTen"].ToString();
        }
        catch { }
    }

    protected void Text_Changed_2(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + idDanhSach.Text + "'";// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtCongTy.Text = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
        }
        catch { }
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            id.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
                        + " from dbo.tblHoiVienCaNhan "
                        + " WHERE MaHoiVienCaNhan = '" + lstIdChon[0] + "'";// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtTen.Text = tem["HoTen"].ToString();
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }

    protected void btnChay2_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            idDanhSach.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + lstIdChon[0] + "'";// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtCongTy.Text = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }
}