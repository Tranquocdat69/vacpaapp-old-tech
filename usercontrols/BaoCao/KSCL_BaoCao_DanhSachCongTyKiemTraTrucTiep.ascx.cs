﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách công ty kiểm tra trực tiếp";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = "";
            string idDanhSachKtttt = Request.Form["hdDanhSachID"];
            if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            {
                nam = Request.Form["ddlNam"];

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/DanhSachCongTyKiemTraTrucTiep.rpt"));
                DataTable dt = GetDanhSachCongTyKiemTraTrucTiep(nam, idDanhSachKtttt);
                rpt.Database.Tables["dtDanhSachCongTyKiemTraTrucTiep"].SetDataSource(dt);
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "DANH SÁCH CÔNG TY KIỂM TRA TRỰC TIẾP NĂM " + nam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["Text10"]).Text = "Doanh thu năm " + nam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachCongTyKiemTraTrucTiepNam_" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, nam);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "DanhSachCongTyKiemTraTrucTiepNam_" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachCongTyKiemTraTrucTiepNam_" + nam);
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string nam = Request.Form["ddlNam"];
        string maDanhSach = Request.Form["txtMaDanhSach"];
        if (string.IsNullOrEmpty(Request.Form["ddlNam"])) // Neu load trang lan dau chua post du lieu thi moi lay gia tri tu querystring
        {
            if (string.IsNullOrEmpty(maDanhSach))
                maDanhSach = Library.CheckNull(Request.QueryString["madskttt"]);
            // Lay nam lap danh sach
            if (!string.IsNullOrEmpty(maDanhSach))
            {
                try
                {
                    _db.OpenConnection();
                    string query = "SELECT YEAR(NgayLap) nam FROM tblKSCLDSKiemTraTrucTiep WHERE MaDanhSach = '" + maDanhSach + "'";
                    List<Hashtable> listData = _db.GetListData(query);
                    if (listData.Count > 0)
                    {
                        nam = listData[0]["nam"].ToString();
                    }
                }
                catch { _db.CloseConnection(); }
                finally
                {
                    _db.CloseConnection();
                }
            }
        }
        string js = "";
        if (!string.IsNullOrEmpty(nam))
        {
            js = "$('#ddlNam').val('" + nam + "');" + Environment.NewLine;
        }
        if (!string.IsNullOrEmpty(maDanhSach))
        {
            js += "$('#txtMaDanhSach').val('" + maDanhSach + "');" + Environment.NewLine;
            js += "$('#hdDanhSachID').val('" + Request.Form["hdDanhSachID"] + "');" + Environment.NewLine;
            js += "CallActionGetInforDanhSach();" + Environment.NewLine;
        }

        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
        {
            if (!string.IsNullOrEmpty(Request.Form["cboCongTyPhaiNopBCTKT"]))
                js += "$('#cboCongTyPhaiNopBCTKT').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboCongTyPhaiNopBCTKT').prop('checked', false);" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboCongTyKiemTraTheoYeuCau"]))
                js += "$('#cboCongTyKiemTraTheoYeuCau').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboCongTyKiemTraTheoYeuCau').prop('checked', false);" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private DataTable GetDanhSachCongTyKiemTraTrucTiep(string nam, string idDanhSachKttt)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "SoHieuCongTy", "TenCongTy", "TenVietTat", "DeNghiKiemTraNamNay", "NamKiemTraGanNhat", "CongTyKT3NamChuaKiemTra", 
            "XepLoaiKem2LanLienKe", "TieuChiKhac", "DoanhThuNam", "SoLuongNhanVien", "SoLuongBCKT", "MaDanhSachKTTT" });
        if (string.IsNullOrEmpty(nam))
            return dt;
        string procName = ListName.Proc_BAOCAO_KSCL_DanhSachCongTyKiemTraTrucTiep;

        if (string.IsNullOrEmpty(Request.Form["cboCongTyPhaiNopBCTKT"]) && string.IsNullOrEmpty(Request.Form["cboCongTyKiemTraTheoYeuCau"]))
            return dt;
        List<Hashtable> listData = _db.GetListData(procName, new List<string> { "@Nam" }, new List<object> { nam });
        if (listData.Count > 0)
        {
            DataRow dr;
            int index = 1;
            foreach (Hashtable ht in listData)
            {
                if ((!string.IsNullOrEmpty(Request.Form["cboCongTyPhaiNopBCTKT"]) && !string.IsNullOrEmpty(ht["DSBaoCaoTuKiemTraChiTietID"].ToString()))
                    || (!string.IsNullOrEmpty(Request.Form["cboCongTyKiemTraTheoYeuCau"]) && !string.IsNullOrEmpty(ht["DSKiemTraVuViecChiTiet"].ToString())))
                {
                    dr = dt.NewRow();
                    dr["STT"] = index;
                    dr["SoHieuCongTy"] = ht["SoHieu"];
                    dr["TenCongTy"] = ht["TenDoanhNghiep"];
                    dr["TenVietTat"] = ht["TenVietTat"];
                    string lanKiemTraGanNhat = ht["LanKiemTraGanNhat"].ToString();
                    dr["NamKiemTraGanNhat"] = lanKiemTraGanNhat;
                    if (Library.Int32Convert(lanKiemTraGanNhat) > 0 && (Library.Int32Convert(nam) - Library.Int32Convert(lanKiemTraGanNhat) >= 3))
                        dr["CongTyKT3NamChuaKiemTra"] = ht["x"];
                    int soLanXepLoaiKem2LanLienKe = Library.Int32Convert(ht["SoLanXepLoaiKem2LanLienKe"]);
                    if (soLanXepLoaiKem2LanLienKe > 0 && soLanXepLoaiKem2LanLienKe >= 6)
                        dr["XepLoaiKem2LanLienKe"] = ht["x"];
                    dr["DoanhThuNam"] = ht["DoanhThuNamGanNhat"];
                    dr["SoLuongNhanVien"] = ht["TongSoNguoiLamViec"];
                    dr["SoLuongBCKT"] = ht["SoBaoCaoKiemToanPH"];
                    dr["MaDanhSachKTTT"] = ht["MaDanhSach"];
                    if (idDanhSachKttt == ht["DSKiemTraTrucTiepID"].ToString() || string.IsNullOrEmpty(idDanhSachKttt))
                    {
                        dt.Rows.Add(dr);
                        index++;
                    }
                }
            }
        }
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, string nam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='12' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='12' style='text-align:center;font-size: 14pt; font-weight: bold;'>DANH SÁCH CÔNG TY KIỂM TRA TRỰC TIẾP NĂM " + nam + @"</td>
            </tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn'>STT</td>
                <td class='HeaderColumn' style='width: 70px;'>Số hiệu công ty</td>
                <td class='HeaderColumn' style='width: 170px;'>Tên công ty</td>
                <td class='HeaderColumn' style='width: 70px;'>Tên viết tắt</td>
                <td class='HeaderColumn' style='width: 100px;'>Đề nghị kiểm tra năm nay</td>
                <td class='HeaderColumn' style='width: 100px;'>Năm kiểm tra gần nhất</td>
                <td class='HeaderColumn' style='width: 100px;'>Công ty kiểm toán trên 3 năm chưa kiểm tra</td>
                <td class='HeaderColumn' style='width: 100px;'>Các công ty kiểm toán mà 2 lần kiểm tra liền kề trước đó kết quả kiểm tra xếp loại 3,4</td>
                <td class='HeaderColumn' style='width: 100px;'>Tiêu chí khác</td>
                <td class='HeaderColumn' style='width: 100px;'>Doanh thu năm " + nam + @"</td>
                <td class='HeaderColumn' style='width: 100px;'>Số lượng nhân viên</td>
                <td class='HeaderColumn' style='width: 100px;'>Số lượng BCKT</td>                
                            </tr>";
        string tempMaDanhSach = "";
        int soCt3NamChuaKiemTra = 0, soCtXepLoaiKem2LanLienKe = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string maDanhSach = dt.Rows[i]["MaDanhSachKTTT"].ToString();

            if (maDanhSach != tempMaDanhSach)
            {
                tempMaDanhSach = maDanhSach;
                htmlExport += "<tr><td colspan='12' class='GroupTitle'>" + maDanhSach + "</td></tr>";
            }
            htmlExport += "<tr><td class='Column'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoHieuCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenVietTat"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DeNghiKiemTraNamNay"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NamKiemTraGanNhat"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["CongTyKT3NamChuaKiemTra"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["XepLoaiKem2LanLienKe"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TieuChiKhac"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DoanhThuNam"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoLuongNhanVien"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoLuongBCKT"] + "</td></tr>";
        }
        htmlExport += "<tr><td style='font-weight: bold; text-align: center;' colspan='6'>Tổng cộng:</td><td style='font-weight: bold; text-align: center;'>" + soCt3NamChuaKiemTra + "</td><td style='font-weight: bold; text-align: center;'>" + soCtXepLoaiKem2LanLienKe + "</td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("DanhSachCongTyKiemTraTrucTiepNam_" + nam + ".xsl", "<html>" + css + htmlExport.Replace("<br>", "\n").Replace("<br />", "\n") + "</html>");
    }
}