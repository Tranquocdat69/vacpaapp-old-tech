﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quyết định khen thưởng hội viên tổ chức và hội viên cá nhân";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID = " + Convert.ToInt32(VM);
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        //output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (var tem in lst)
        {
            if (ma == tem.idDoiTuong.ToString())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }


    
    public void setdataCrystalReport()
    {
       // string VungMien = Request.Form["VungMienId"];
        //string Tinh = Request.Form["TinhId"];
        string SoQuyetDinh = Request.Form["SoQuyetDinh"];
        //DateTime? NgayBatDau = null;
        //if (!string.IsNullOrEmpty(Request.Form["NgayBatDau"]))
        //    NgayBatDau = DateTime.ParseExact(Request.Form["NgayBatDau"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        //DateTime? NgayKetThuc = null;
        //if (!string.IsNullOrEmpty(Request.Form["NgayKetThuc"]))
        //    NgayKetThuc = DateTime.ParseExact(Request.Form["NgayKetThuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime? NgayQuyetDinh = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayQuyetDinh"]))
            NgayQuyetDinh = DateTime.ParseExact(Request.Form["NgayQuyetDinh"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);


        if (SoQuyetDinh != null && NgayQuyetDinh != null)
        {
            List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
            List<objThongTinDoanhNghiep_CT> lstDoanhNghiep = new List<objThongTinDoanhNghiep_CT>();
            List<DanhSachKetNapHoiVienCaNhan> lstCaNhan = new List<DanhSachKetNapHoiVienCaNhan>();
            
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = " select distinct TenDoanhNghiep, TenHinhThucKhenThuong from dbo.tblKhenThuongKyLuat a "
                                + " inner join dbo.tblHoiVienTapThe b on a.HoiVienTapTheID = b.HoiVienTapTheID "
                                + " inner join dbo.tblDMHinhThucKhenThuong c on a.HinhThucKhenThuongID = c.HinhThucKhenThuongID "
                                + " where a.NgayThang = @NgayQuyetDinh";
                                           
            string strNgay = NgayQuyetDinh.Value.Year + "-" + NgayQuyetDinh.Value.Month + "-" + NgayQuyetDinh.Value.Day;
            if (NgayQuyetDinh != null)
                sql.Parameters.AddWithValue("@NgayQuyetDinh", strNgay);
            else
                sql.Parameters.AddWithValue("@NgayQuyetDinh", DBNull.Value);
            //if (NgayKetThuc != null)
            //    sql.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);
            //else
            //    sql.Parameters.AddWithValue("@NgayKetThuc", DBNull.Value);

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            int i = 0;
            //string txtLyDo = "";
            foreach (DataRow tem in dt.Rows)
            {
                i++;

                
                objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();
                new1.STT = i.ToString();
                new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString();
                lstDoanhNghiep.Add(new1);

            }

            DataSet ds1 = new DataSet();
            SqlCommand sql1 = new SqlCommand();
            sql1.CommandText = " select TenDoanhNghiep, HoDem + ' ' + Ten as HoTen, TenChucVu from dbo.tblKhenThuongKyLuat a "
                                + " inner join dbo.tblHoiVienCaNhan b on a.HoiVienCaNhanID = b.HoiVienCaNhanID "
                                + " inner join dbo.tblDMHinhThucKhenThuong c on a.HinhThucKhenThuongID = c.HinhThucKhenThuongID "
                                + " inner join dbo.tblHoiVienTapThe d on b.HoiVienTapTheID = d.HoiVienTapTheID "
                                + " left join dbo.tblDMChucVu e on b.ChucVuID = e.ChucVuID "
                                + " where a.NgayThang = @NgayQuyetDinh"
                                + " order by TenDoanhNghiep";

            string strNgay1 = NgayQuyetDinh.Value.Year + "-" + NgayQuyetDinh.Value.Month + "-" + NgayQuyetDinh.Value.Day;
            if (NgayQuyetDinh != null)
                sql1.Parameters.AddWithValue("@NgayQuyetDinh", strNgay1);
            else
                sql1.Parameters.AddWithValue("@NgayQuyetDinh", DBNull.Value);
            //if (NgayKetThuc != null)
            //    sql.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);
            //else
            //    sql.Parameters.AddWithValue("@NgayKetThuc", DBNull.Value);

            ds1 = DataAccess.RunCMDGetDataSet(sql1);
            sql1.Connection.Close();
            sql1.Connection.Dispose();
            sql1 = null;

            DataTable dt1 = ds1.Tables[0];

            int j = 0;
            //string txtLyDo = "";
            foreach (DataRow tem in dt1.Rows)
            {
                j++;


                DanhSachKetNapHoiVienCaNhan new1 = new DanhSachKetNapHoiVienCaNhan();
                new1.STT = j.ToString();
                new1.TenCT = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
                new1.HoTen = tem["HoTen"].ToString();
                new1.ChucVuHienNay = tem["TenChucVu"].ToString();
                lstCaNhan.Add(new1);
            }

            TenBaoCao newabc = new TenBaoCao();
            newabc.SoLuong = i + " công ty kiểm toán, " + j + " Hội";
            newabc.TenBC = "Số: " + SoQuyetDinh;
            newabc.TongCong = "Tổng cộng: " + i.ToString() + " Hội viên tổ chức và " + j + " hội viên cá nhân";
            newabc.NgayTinh = "(Kèm theo quyết định số " + SoQuyetDinh + " ngày " + (NgayQuyetDinh.Value.Day.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Day : NgayQuyetDinh.Value.Day.ToString()) + "/" + (NgayQuyetDinh.Value.Month.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Month : NgayQuyetDinh.Value.Month.ToString()) + "/" + NgayQuyetDinh.Value.Year + ")";
            newabc.ngayBC = "Hà Nội, ngày " + (NgayQuyetDinh.Value.Day.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Day : NgayQuyetDinh.Value.Day.ToString()) + " tháng " + (NgayQuyetDinh.Value.Month.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Month : NgayQuyetDinh.Value.Month.ToString()) + " năm " + NgayQuyetDinh.Value.Year;
            lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/QuyetDinhKhenThuongTapTheVaCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lstDoanhNghiep);
            rpt.Database.Tables["DanhSachKetNapHoiVienCaNhan"].SetDataSource(lstCaNhan);

            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoDSHoiVienBiXoaTen");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoDSHoiVienBiXoaTen");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            
        }
    }
   
}