﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QLHV_BC_Chon.ascx.cs" Inherits="usercontrols_QLHV_BC_Chon" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript">
</script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

    <%--<form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">
      <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
           <%--<asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  ChildrenAsTriggers = "true"  >
       <ContentTemplate>--%>
            <table id="Table1" border="0" class="formtbl">
     <tr style ="width: 586px">
        <td style="width:180px"><label>Tên: </label></td>
        <td  style="width:380px"><input type="text" name="timkiem_Ten" id="timkiem_Ten" value="<%=Request.Form["timkiem_Ten"]%>"  class="input-xlarge" />
        
        </td>
    </tr>
     <tr style ="width: 586px">
        <td style="width:180px"><label>Mã: </label></td>
        <td  style="width:380px"><input type="text" name="timkiem_Ma" id="timkiem_Ma" value="<%=Request.Form["timkiem_Ma"]%>"  class="input-xlarge" />
        
        </td>
    </tr>
       <td colspan ="2"> <asp:LinkButton ID="btnTimKiem" runat="server" CssClass="btn" 
        onclick="btnTimKiem_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton></td>
      </table>
       <asp:GridView ClientIDMode="Static" ID="grvDanhSach" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="False" AllowSorting="True" 
           Width ="586px"
            DataKeyNames = "HoiVienCaNhanID"
       >
          <Columns>
                    <asp:TemplateField ItemStyle-Width="30px">
                        <%--<HeaderTemplate>
                            <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                        </HeaderTemplate>--%>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <%--<asp:CheckBox ID="chkEmp" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmp_CheckedChanged" ></asp:CheckBox>--%>
                            <input type='checkbox' value='<%# Eval("HoiVienCaNhanID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
               <asp:TemplateField  >
                  <HeaderStyle Width="120px" />
                  <ItemTemplate>
                      <span><%# Eval("HoiVienCaNhanID")%></span>
                  </ItemTemplate>
              </asp:TemplateField>
        
              <asp:TemplateField  >
                  <HeaderStyle Width="280px" />
                  <ItemTemplate>
                      <span><%# Eval("HOTEN")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               

          </Columns>
</asp:GridView>
<%--<input type='button' value='Chon gia tri' onclick='ChooseCongTy();' />--%>
<%--</ContentTemplate>
      </asp:UpdatePanel>     --%>
    </form>

    <script type="text/javascript">
        function ChooseCongTy() {
            var flag = false;
            $('#grvDanhSach :checkbox').each(function () {
                checked = $(this).prop("checked");
                if (checked) {
                    // Lấy chuỗi thông tin công ty -> gọi hàm nhận giá trị ở trang cha
                    var chooseItem = $(this).val();
                    parent.GetValueFromPopup(chooseItem);
                    parent.ClosePopup();
                    flag = true;
                    return;
                }
            });
            // Bắt buộc phải chọn 1 công ty -> nếu ko thì bật cảnh báo
            if (!flag)
                alert('Phải chọn công ty!');
        }

        function close() {
            $(this).closest(".ui-dialog-content").dialog("close");

        }
</script>