﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TT_BaoCao_NopPhiCNKTcuaKTV.ascx.cs" Inherits="css_TT_BaoCao_NopPhiHoiVienCaNhan" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Báo cáo nộp phí cập nhật kiến thức của kiểm toán viên</h4>
<fieldset class="fsBlockInfor">
    <legend>Tiêu chí kết xuất dữ liệu báo cáo</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
    <tr>
            <td>
                Đối tượng học viên:
            </td>
            <td>
                <input type="checkbox" id="cbLamTrongCongTy" name="cbLamTrongCongTy"  /> KTV làm trong công ty kiểm toán
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" id="cbNotKTVLamTrongCongTy" name="cbNotKTVLamTrongCongTy"  />Không KTV và làm trong công ty kiểm toán
            </td>
        </tr>  
         <tr>
            <td>
                
            </td>
            <td>
                <input type="checkbox" id="cbKhongLamTrongCongTy" name="cbKhongLamTrongCongTy"  /> KTV không làm trong công ty kiểm toán
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" id="cbNotKTVKhongLamTrongCongTy" name="cbNotKTVKhongLamTrongCongTy"  />Không KTV và không làm trong công ty kiểm toán
            </td>
        </tr>  
        <tr>
            <td>
                Mã Đơn vị công tác:
            </td>
            <td>
                <input type="text" id="txtMaCongTy" name="txtMaCongTy" style="max-width: 100px;" onchange="CallActionGetInforCongTy();"
                    class="tdInput" />
                <input type="hidden" name="hdCongTyID" id="hdCongTyID" />
                <input type="button" value="---" onclick="OpenDanhSachCongTy();" style="border: 1px solid gray;" />
            </td>
        </tr>
        <tr>
            <td>
                Đơn vị công tác:
            </td>
            <td>
                <input type="text" id="txtTenCongTy" name="txtTenCongTy" readonly="readonly" />
             
            </td>
        </tr>     
        
        
         <tr>
            <td>
                Ngày chốt số liệu<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtNgayLapBC" name="txtNgayLapBC" style="max-width: 100px;" />
                <img src="/images/icons/calendar.png" id="imgCalendarNgayLapBC" />
             
            </td>
        </tr> 
         <tr>
            <td>
                
            </td>
            <td>
                <input type="checkbox" id="cbConNoPhi" name="cbConNoPhi"  /> Còn nợ phí
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" id="cbKhongConNoPhi" name="cbKhongConNoPhi"  />Không còn nợ phí
            </td>
        </tr>        
        <tr>
            <td colspan="2" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');"><i class="iconfa-search"></i>Xem dữ liệu</a> 
                <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="Export('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a> <a id="A1" href="javascript:;" class="btn btn-rounded" onclick="Export('excel');">
                    <i class="iconsweets-excel2"></i>Kết xuất Excel</a> <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="Export('pdf');">
                    <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                    <input type="hidden" id="hdAction" name="hdAction"/>
            </td>
        </tr>
    </table>
</fieldset>
    <fieldset class="fsBlockInfor">
        <legend>Báo cáo nộp phí cập nhật kiến thức của kiểm toán viên</legend>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False" DisplayToolbar="False"/>
    </fieldset>
</form>
<iframe name="iframeProcess_LoadCongTy" width="0px" height="0px"></iframe>

 <iframe name="iframeProcess_LoadHocVien" width="0px" height="0px"></iframe>
<script type="text/javascript">
   
    function OpenDanhSachCongTy() {
        $("#DivDanhSachCongTy").empty();
        $("#DivDanhSachCongTy").append($("<iframe width='100%' height='100%' id='ifDanhSachCongTy' name='ifDanhSachCongTy' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=hoivientapthe_minilist"));
        $("#DivDanhSachCongTy").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách công ty</b>",
            modal: true,
            zIndex: 1000
        });
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Gọi sự kiện lấy dữ liệu công ty theo mã công ty khi NSD nhập trực tiếp mã công ty vào ô textbox
    function CallActionGetInforCongTy() {
        var maCongTy = $('#txtMaCongTy').val();
        iframeProcess_LoadCongTy.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mact=' + maCongTy + '&action=loadct';
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm nhận giá trị thông tin công ty từ Dialog danh sách
    function DisplayInforCongTy(value) {
        var arrValue = value.split(';#');
        $('#hdCongTyID').val(arrValue[0]);
        $('#txtMaCongTy').val(arrValue[1]);
        $('#txtTenCongTy').val(arrValue[2]);        
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm đóng Dialog danh sách công ty (Gọi hàm này từ trang con)
    function CloseFormDanhSachCongTy() {
        $("#DivDanhSachCongTy").dialog('close');
    }
    function Export(type) {
        $('#hdAction').val(type);
        $('#Form1').submit();
    }


    

    
    $("#txtNgayLapBC").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarNgayLapBC").click(function () {
        $("#txtNgayLapBC").datepicker("show");
    });

 $('#Form1').validate({
        rules: {
            txtNgayLapBC: {
                required: true, dateITA: true
            }
            
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form_ingcn").html(e).show();

            } else {
                jQuery("#thongbaoloi_form_ingcn").hide();  
            }
        }
    });

    <% SetDataJavascript(); %>
</script>
<div id="DivDanhSachCongTy">
</div>
<div id="DivDanhSachHocVien">
</div>
