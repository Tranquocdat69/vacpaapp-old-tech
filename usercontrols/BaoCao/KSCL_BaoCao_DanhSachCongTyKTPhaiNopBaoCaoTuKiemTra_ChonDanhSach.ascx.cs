﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra_ChonDanhSach : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _idDanhSach = "", _type, _nam = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        _type = Library.CheckNull(Request.QueryString["type"]);
        this._nam = Library.CheckNull(Request.QueryString["nam"]);
        if (!IsPostBack)
        {
            LoadDanhSachVungMien();
            LoadDanhSachLoaiHinhCongTy();
            LoadData(_type, this._nam);
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách công ty theo điều kiện tìm kiếm
    /// </summary>
    /// <param name="type"></param>
    private void LoadData(string type, string nam)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "ID", "MaDanhSach", "NgayLap", "SoCongTy" });
        try
        {
            _db.OpenConnection();
            List<string> listParam = new List<string>{"@DuDKKT_CK", "@DuDKKT_Khac", "@MaHocVienTapThe", "@TenDoanhNghiep", "@LoaiHinhDoanhNghiepID",
                    "@VungMienID", "@DoanhThuTu", "@DoanhThuDen", "@NamHienTai", "@XepHang", "@NgayBatDau", "@NgayKetThuc", "@MaDanhSach", "@MaTrangThai_ChoDuyet", 
                    "@MaTrangThai_TuChoi", "@MaTrangThai_DaDuyet", "@NgayLapBatDau", "@NgayLapKetThuc"};
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DateTime.Now.Year, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value };
            // Dựa theo các tiêu chí tìm kiếm -> đưa giá trị tham số tương ứng vào store
            if (cboDuDieuKienKTCK.Checked)
                listValue[0] = 1;
            if (cboDuDieuKienKTKhac.Checked)
                listValue[1] = 1;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                listValue[2] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                listValue[3] = txtTenCongTy.Text;
            if (ddlLoaiHinh.SelectedValue != "")
                listValue[4] = ddlLoaiHinh.SelectedValue;
            if (ddlVungMien.SelectedValue != "")
                listValue[5] = ddlVungMien.SelectedValue;
            if (!string.IsNullOrEmpty(txtDoanhThuTu.Text) && Library.CheckIsDecimal(txtDoanhThuTu.Text))
                listValue[6] = txtDoanhThuTu.Text;
            if (!string.IsNullOrEmpty(txtDoanhThuDen.Text) && Library.CheckIsDecimal(txtDoanhThuDen.Text))
                listValue[7] = txtDoanhThuDen.Text;
            if (!cboTieuChi_Khac.Checked)
            {
                string xepHang = "";
                if (cboXepHang1.Checked)
                    xepHang += "1,";
                if (cboXepHang2.Checked)
                    xepHang += "2,";
                if (cboXepHang3.Checked)
                    xepHang += "3,";
                if (cboXepHang4.Checked)
                    xepHang += "4,";
                listValue[9] = xepHang;
            }
            if (!string.IsNullOrEmpty(txtTuNam.Text) && Library.CheckIsInt32(txtTuNam.Text))
                listValue[10] = txtTuNam.Text + "-1-1";
            if (!string.IsNullOrEmpty(txtDenNam.Text) && Library.CheckIsInt32(txtDenNam.Text))
                listValue[11] = txtDenNam.Text + "-12-31";
            if (!string.IsNullOrEmpty(txtMaDanhSach.Text))
                listValue[12] = txtMaDanhSach.Text;
            listValue[15] = ListName.Status_DaPheDuyet;
            if (!string.IsNullOrEmpty(nam))
            {
                listValue[16] = nam + "/1/1";
                listValue[17] = nam + "/12/31";
            }

            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra, listParam, listValue);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["DSBaoCaoTuKiemTraID"].ToString();
                    dr["MaDanhSach"] = ht["MaDanhSach"];
                    dr["NgayLap"] = !string.IsNullOrEmpty(ht["NgayLap"].ToString())
                                        ? Library.DateTimeConvert(ht["NgayLap"]).ToString("dd/MM/yyyy")
                                        : "";
                    dr["SoCongTy"] = ht["SoCongTyPhaiNopBaoCao"];
                    dt.Rows.Add(dr);
                }
            }
            rpDanhSach.DataSource = dt.DefaultView;
            rpDanhSach.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách vùng miền đổ vào Dropdownlist
    /// </summary>
    private void LoadDanhSachVungMien()
    {
        ddlVungMien.Items.Clear();
        ddlVungMien.Items.Add(new ListItem("Tất cả", ""));
        SqlDmVungMienProvider vungMienPro = new SqlDmVungMienProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmVungMien> listData = vungMienPro.GetAll();
        foreach (DmVungMien dmVungMien in listData)
        {
            ddlVungMien.Items.Add(new ListItem(dmVungMien.TenVungMien, dmVungMien.VungMienId.ToString()));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách loại hình công ty
    /// </summary>
    private void LoadDanhSachLoaiHinhCongTy()
    {
        ddlLoaiHinh.Items.Clear();
        ddlLoaiHinh.Items.Add(new ListItem("Tất cả", ""));
        SqlDmLoaiHinhDoanhNghiepProvider loaiHinhPro = new SqlDmLoaiHinhDoanhNghiepProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmLoaiHinhDoanhNghiep> listData = loaiHinhPro.GetAll();
        foreach (DmLoaiHinhDoanhNghiep loaiHinh in listData)
        {
            ddlLoaiHinh.Items.Add(new ListItem(loaiHinh.TenLoaiHinhDoanhNghiep, loaiHinh.LoaiHinhDoanhNghiepId.ToString()));
        }
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadData(_type, this._nam);
    }
}