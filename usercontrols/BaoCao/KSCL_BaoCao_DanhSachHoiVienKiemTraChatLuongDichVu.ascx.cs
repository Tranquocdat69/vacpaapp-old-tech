﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_KSCL_BaoCao_DanhSachHoiVienKiemTraChatLuongDichVu : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách hội viên kiểm tra chất lượng dịch vụ năm";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = "";
            if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            {
                nam = Request.Form["ddlNam"];

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/DanhSachHoiVienKiemTraChatLuongDichVuNam.rpt"));
                DataTable dt = GetDanhSach(nam, rpt);
                rpt.Database.Tables["dtDanhSachHoiVienKiemTraChatLuongDichVuNam"].SetDataSource(dt);
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachHoiVienKiemTraChatLuongDichVuNam" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, nam);
                //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "DanhSachHoiVienKiemTraChatLuongDichVuNam" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachHoiVienKiemTraChatLuongDichVuNam" + nam);
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
        {
            string js = "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboDaNopBaoCao"]))
                js += "$('#cboDaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboDaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboChuaNopBaoCao"]))
                js += "$('#cboChuaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboChuaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            Response.Write(js);
        }
    }

    private DataTable GetDanhSach(string nam, ReportDocument rpt)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "TenHoiVien", "NamThanhLap", "SoLuongNhanVien", "SoLuongNhanVienChuyenNghiep", "SoLuongKTVVietNam", "LanKiemTraGanNhat", "NhomDKCK" });
        if (string.IsNullOrEmpty(nam))
            return dt;

        string procName = ListName.Proc_BAOCAO_KSCL_DanhSachHoiVienKiemTraChatLuongDichVuNam;
        List<Hashtable> listData = _db.GetListData(procName, new List<string> { "@Nam" }, new List<object> { nam });
        if (listData.Count > 0)
        {
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "DANH SÁCH " + listData.Count + " HỘI VIÊN KIỂM TRA CHẤT LƯỢNG DỊCH VỤ NĂM " + nam;
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongSo"]).Text = "Tổng số: " + listData.Count;
            DataRow dr;
            int index = 1;
            foreach (Hashtable ht in listData)
            {
                dr = dt.NewRow();
                dr["STT"] = index;
                dr["TenHoiVien"] = ht["TenDoanhNghiep"];
                dr["NamThanhLap"] = ht["NamThanhLap"];
                dr["SoLuongNhanVien"] = ht["TongSoNguoiLamViec"];
                dr["SoLuongNhanVienChuyenNghiep"] = ht["SoLuongNVChuyenNghiep"];
                dr["SoLuongKTVVietNam"] = ht["SLKTVHanhNghe"];
                int soLanKiemTraTrucTiep = Library.Int32Convert(ht["SoLanKiemTraTrucTiep"]);
                if (soLanKiemTraTrucTiep > 1)
                    dr["LanKiemTraGanNhat"] = ht["LanKiemTraGanNhat"];
                else
                    dr["LanKiemTraGanNhat"] = "Lần đầu tiên";
                string namDuDKCK = ht["NamDuDKKT_CK"].ToString();
                if (!string.IsNullOrEmpty(namDuDKCK))
                    dr["NhomDKCK"] = "Công ty kiểm toán đủ điều kiện kiểm toán cho đơn vị có lợi ích công chúng thuộc lĩnh vực chứng khoán";
                else
                    dr["NhomDKCK"] = "Công ty kiểm toán khác";
                dt.Rows.Add(dr);
                index++;
            }
        }
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, string nam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='6' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='6' style='text-align:center;font-size: 14pt; font-weight: bold;'>DANH SÁCH " + dt.Rows.Count + @" HỘI VIÊN KIỂM TRA CHẤT LƯỢNG DỊCH VỤ NĂM " + nam + @"</td>
            </tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn'>STT</td>                
                <td class='HeaderColumn' style='width: 170px;'>Tên công ty</td>
                <td class='HeaderColumn' style='width: 70px;'>Năm thành lập</td>
                <td class='HeaderColumn' style='width: 100px;'>Số lượng nhân viên</td>
                <td class='HeaderColumn' style='width: 100px;'>KTV Việt Nam</td>
                <td class='HeaderColumn' style='width: 100px;'>Lần kiểm tra gần nhất</td>                
                            </tr>";
        string tempNhom = "";
        DataRow[] arrDataRows = dt.Select("", "NhomDKCK ASC");
        for (int i = 0; i < arrDataRows.Length; i++)
        {
            string nhom = arrDataRows[i]["NhomDKCK"].ToString();

            if (nhom != tempNhom)
            {
                tempNhom = nhom;
                htmlExport += "<tr><td colspan='6' class='GroupTitle'>" + nhom + "</td></tr>";
            }
            htmlExport += "<tr><td class='Column'>" + arrDataRows[i]["STT"] + "</td>" +
                          "<td class='Column'>" + arrDataRows[i]["TenHoiVien"] + "</td>" +
                          "<td class='Column'>" + arrDataRows[i]["NamThanhLap"] + "</td>" +
                          "<td class='Column'>" + arrDataRows[i]["SoLuongNhanVien"] + "</td>" +
                          "<td class='Column'>" + arrDataRows[i]["SoLuongKTVVietNam"] + "</td>" +
                          "<td class='Column'>" + arrDataRows[i]["LanKiemTraGanNhat"] + "</td></tr>";
        }
        htmlExport += "<tr><td colspan='6' style='font-weight: bold;'>Tổng số: " + arrDataRows.Length + "</td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3'></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3'></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3'></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3'></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3'></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3'></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td colspan='3' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("DanhSachHoiVienKiemTraChatLuongDichVuNam" + nam + ".xsl", "<html>" + css + htmlExport.Replace("<br>", "\n").Replace("<br />", "\n") + "</html>");
    }
}