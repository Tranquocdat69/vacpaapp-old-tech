﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_CNKT_BaoCao_InGCNGioCNKT : System.Web.UI.UserControl
{
    protected string tenchucnang = "Giấy chứng nhận giờ cập nhật kiến thức kiểm toán viên";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string idLopHoc = "", idHocVien = "";
            if (Library.CheckNull(Request.Form["hdAction"]) == "wordALL")
            {
                if (!string.IsNullOrEmpty(Request.Form["hdLopHocID"]))
                {
                    string soVaoSo = Request.Form["txtSoVaoSo"];
                    idLopHoc = Request.Form["hdLopHocID"];
                    List<objGiayChungNhanGioCNKT_All> lst = new List<objGiayChungNhanGioCNKT_All>();

                    string txtNgayThangKy = "";
                    string txtNamHoc = "";
                    string txtThongTinLopHoc = "";

                    string query = "SELECT MaLopHoc, TenLopHoc, TuNgay, DenNgay, HanDangKy, TenTinh, DiaChiLopHoc FROM tblCNKTLopHoc LH LEFT JOIN tblDMTinh ON LH.TinhThanhID = tblDMTinh.TinhID WHERE LopHocID = " + idLopHoc;
                    List<Hashtable> listData = _db.GetListData(query);
                    {
                        Hashtable ht = listData[0];
                        string ngayLapBc = Request.Form["txtNgayLapBC"];
                        if (!string.IsNullOrEmpty(ngayLapBc))
                        {
                            DateTime ngayKy = Library.DateTimeConvert(ngayLapBc, "dd/MM/yyyy");
                            txtNgayThangKy = ht["TenTinh"] + ", Ngày " + ngayKy.Day + " tháng " + ngayKy.Month + " năm " + ngayKy.Year;
                        }
                        DateTime ngaybatdau = Library.DateTimeConvert(ht["TuNgay"]);
                        if ((ngaybatdau.Day > 15 && ngaybatdau.Month == 8) || ngaybatdau.Month > 8)
                            txtNamHoc = "Năm " + (ngaybatdau.Year + 1).ToString();
                        else
                            txtNamHoc = "Năm " + ngaybatdau.Year.ToString();
                        txtThongTinLopHoc = "Đã tham gia lớp học cập nhật kiến thức " + ht["MaLopHoc"]
                            + " (" + Library.DateTimeConvert(ht["TuNgay"]).ToString("dd/MM/yyyy")
                            + " - " + Library.DateTimeConvert(ht["DenNgay"]).ToString("dd/MM/yyyy")
                            + ") do VACPA tổ chức, như sau:";
                    }

                    string HoiVienTapTheID = Request.Form["tk_DonVi"];
                    string queryHV = "SELECT distinct b.HoiVienCaNhanID, (HoDem + ' ' + Ten) Fullname, SoChungChiKTV, "
                                       + " NgayCapChungChiKTV, Ten, HoDem "
                                       + " FROM tblHoiVienCaNhan a "
                                       + " inner join tblCNKTLopHocHocVien b on a.HoiVienCaNhanID = b.HoiVienCaNhanID "
                                       + " where b.LopHocID = " + idLopHoc;
                    if (HoiVienTapTheID != "0")
                        queryHV += " and a.HoiVienTapTheID = " + HoiVienTapTheID;
                    queryHV += " order by Ten, HoDem ";
                    List<Hashtable> listDataHV = _db.GetListData(queryHV);

                    for (int i = 0; i < listDataHV.Count; i++)
                    {
                        Hashtable ht = listDataHV[i];
                        string HoiVienId = ht["HoiVienCaNhanID"].ToString();
                        string HoTen = ht["Fullname"].ToString();
                        string SoChungChi = ht["SoChungChiKTV"].ToString();
                        string NgayCapCC = Library.DateTimeConvert(ht["NgayCapChungChiKTV"].ToString()).ToString("dd/MM/yyyy");

                        string Cong_KTKIT = "";
                        string Cong_DD = "";
                        string Cong_Khac = "";
                        string Cong_Tong = "";

                        DataTable dt = GetGioThamGiaHoc_GetChoBCAll(idLopHoc, HoiVienId);

                        int STT = 0;

                        if (dt.Rows.Count != 0)
                        {
                            Cong_KTKIT = dt.Rows[dt.Rows.Count - 1][0].ToString();
                            Cong_DD = dt.Rows[dt.Rows.Count - 1][1].ToString();
                            Cong_Khac = dt.Rows[dt.Rows.Count - 1][2].ToString();
                            Cong_Tong = dt.Rows[dt.Rows.Count - 1][3].ToString();
                        }

                        if (dt.Rows.Count <= 1) // = 1 tuc la chi co TOng, ko co cac chuyen de 
                        {
                            objGiayChungNhanGioCNKT_All obj = new objGiayChungNhanGioCNKT_All();
                            obj.HoiVienId = HoiVienId;
                            obj.HoTen = HoTen;
                            obj.ChungChiSo = SoChungChi;
                            obj.NgayCap = NgayCapCC;

                            obj.TT_LopHoc = txtThongTinLopHoc;
                            obj.TT_Nam = txtNamHoc;
                            obj.TT_NgayKy = txtNgayThangKy;
                            //obj.TT_SoVaoSo = "Số vào sổ: " + soVaoSo + "/VACPA"; ;

                            //obj.STT = STT.ToString();
                            //obj.TenChuyenDe = j.ToString();
                            //obj.Ngay = j.ToString();
                            //obj.Buoi = j.ToString();
                            //obj.Loai_KTKIT = j.ToString();
                            //obj.Loai_DD = j.ToString();
                            //obj.Loai_Khac = j.ToString();
                            //obj.TongSoGio = j.ToString();

                            //obj.Cong_KTKIT = Cong_KTKIT;
                            //obj.Cong_DD = Cong_DD;
                            //obj.Cong_Khac = Cong_Khac;
                            //obj.Cong_Tong = Cong_Tong;

                            lst.Add(obj);
                        }
                        else
                        {
                            for (int j = 0; j < dt.Rows.Count - 1; j++)
                            {
                                STT++;
                                objGiayChungNhanGioCNKT_All obj = new objGiayChungNhanGioCNKT_All();
                                obj.HoiVienId = HoiVienId;
                                obj.HoTen = HoTen;
                                obj.ChungChiSo = SoChungChi;
                                obj.NgayCap = NgayCapCC;

                                obj.TT_LopHoc = txtThongTinLopHoc;
                                obj.TT_Nam = txtNamHoc;
                                obj.TT_NgayKy = txtNgayThangKy;
                                //obj.TT_SoVaoSo = "Số vào sổ: " + soVaoSo + "/VACPA"; ;

                                obj.STT = STT.ToString();
                                obj.TenChuyenDe = dt.Rows[j]["TenChuyenDe"].ToString();
                                obj.Ngay = dt.Rows[j]["Ngay"].ToString();
                                obj.Buoi = dt.Rows[j]["Buoi"].ToString();
                                obj.Loai_KTKIT = dt.Rows[j]["LoaiChuyenDe_KTKIT"].ToString();
                                obj.Loai_DD = dt.Rows[j]["LoaiChuyenDe_DD"].ToString();
                                obj.Loai_Khac = dt.Rows[j]["LoaiChuyenDe_Khac"].ToString();
                                obj.TongSoGio = dt.Rows[j]["TongSoGio"].ToString();

                                obj.Cong_KTKIT = Cong_KTKIT;
                                obj.Cong_DD = Cong_DD;
                                obj.Cong_Khac = Cong_Khac;
                                obj.Cong_Tong = Cong_Tong;

                                lst.Add(obj);
                            }
                        }
                    }
                    ReportDocument rpt = new ReportDocument();
                    rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                    rpt.Load(Server.MapPath("Report/CNKT/GiayChungNhanGioCNKT_All.rpt"));
                    rpt.Database.Tables["objGiayChungNhanGioCNKT_All"].SetDataSource(lst);

                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "GiayChungNhanGioCNKT_" + idLopHoc);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.Form["hdLopHocID"]) && !string.IsNullOrEmpty(Request.Form["hdHocVienID"]))
                {
                    idLopHoc = Request.Form["hdLopHocID"];
                    idHocVien = Request.Form["hdHocVienID"];

                    ReportDocument rpt = new ReportDocument();
                    rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                    rpt.Load(Server.MapPath("Report/CNKT/GiayChungNhanGioCNKT.rpt"));
                    rpt.Database.Tables["dtGiayChungNhan_GioThamGiaLopHoc"].SetDataSource(GetGioThamGiaHoc(idLopHoc, idHocVien, rpt));
                    GetThongTinLopHoc(idLopHoc, rpt);
                    GetThongTinHocVien(idHocVien, rpt);
                    string soVaoSo = Request.Form["txtSoVaoSo"];
                    //((TextObject)rpt.ReportDefinition.ReportObjects["txtSoVaoSo"]).Text = "Số vào sổ: " + soVaoSo + "/VACPA";
                    CrystalReportViewer1.ReportSource = rpt;
                    CrystalReportViewer1.RefreshReport();
                    if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                        rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "GiayChungNhanGioCNKT");
                    if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    {
                        rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "GiayChungNhanGioCNKT");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    private DataTable GetGioThamGiaHoc_GetChoBCAll(string idLopHoc, string idHocVien)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "Ngay", "Buoi", "TenChuyenDe", "LoaiChuyenDe_KTKIT", "LoaiChuyenDe_DD", "LoaiChuyenDe_Khac", "STT", "TongSoGio" });
        if (string.IsNullOrEmpty(idLopHoc))
            return dt;
        string query = @"SELECT DISTINCT LHB.Ngay, LHB.Sang, LHB.Chieu, LHCD.TenChuyenDe, LHCD.LoaiChuyenDe, LHCD.SoBuoi, CASE SoGioThucTe WHEN 0 THEN 0 ELSE 4*(CONVERT(INT,ISNULL(LHB.Sang, '0')) + CONVERT(INT,ISNULL(LHB.Chieu, '0'))) END AS SoGioThucTe
                        FROM tblCNKTLopHocBuoi LHB
                        LEFT JOIN tblCNKTLopHocChuyenDe LHCD ON LHB.ChuyenDeID = LHCD.ChuyenDeID
                        LEFT JOIN tblCNKTLopHocSoGio LHSG ON LHB.ChuyenDeID = LHSG.ChuyenDeID
                        WHERE LHB.LopHocID = " + idLopHoc + @" AND LHSG.HoiVienCaNhanID = " + idHocVien + @"
                        ORDER BY Ngay, Chieu";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            DataRow dr;
            int index = 1;
            int tongKtKiT = 0, tongDD = 0, tongKhac = 0, tongCong = 0;
            foreach (Hashtable ht in listData)
            {
                if (!string.IsNullOrEmpty(Library.CheckNull(ht["Sang"])) && !string.IsNullOrEmpty(Library.CheckNull(ht["Chieu"])))
                {
                    dr = dt.NewRow();
                    dr["Buoi"] = "Ngày";
                    dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                    dr["TenChuyenDe"] = ht["TenChuyenDe"];
                    dr["STT"] = index;
                    dr["LoaiChuyenDe_KTKIT"] = "0";
                    dr["LoaiChuyenDe_DD"] = "0";
                    dr["LoaiChuyenDe_Khac"] = "0";
                    if (!string.IsNullOrEmpty(ht["SoGioThucTe"].ToString()))
                    //if ((Library.Int32Convert(ht["SoBuoi"]) * 4) == Library.Int32Convert(ht["SoGioThucTe"]))// Tham gia đủ số giờ tham gia đào tạo mới hiển thị
                    {
                        string loaiChuyenDe = ht["LoaiChuyenDe"].ToString();
                        if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_KeToanKiemToan)
                        {
                            dr["LoaiChuyenDe_KTKIT"] = ht["SoGioThucTe"].ToString();
                            tongKtKiT += Library.Int32Convert(ht["SoGioThucTe"]);
                        }
                        if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep)
                        {
                            dr["LoaiChuyenDe_DD"] = ht["SoGioThucTe"].ToString();
                            tongDD += Library.Int32Convert(ht["SoGioThucTe"]);
                        }
                        if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_ChuyenDeKhac)
                        {
                            tongKhac += Library.Int32Convert(ht["SoGioThucTe"]);
                            dr["LoaiChuyenDe_Khac"] = ht["SoGioThucTe"].ToString();
                        }
                    }
                    dr["TongSoGio"] = (Library.Int32Convert(dr["LoaiChuyenDe_KTKIT"]) +
                                       Library.Int32Convert(dr["LoaiChuyenDe_DD"]) +
                                       Library.Int32Convert(dr["LoaiChuyenDe_Khac"]));
                    dt.Rows.Add(dr);
                    index++;
                }
                else
                {
                    if (!string.IsNullOrEmpty(Library.CheckNull(ht["Sang"])))
                    {
                        dr = dt.NewRow();
                        dr["Buoi"] = "Sáng";
                        dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                        dr["TenChuyenDe"] = ht["TenChuyenDe"];
                        dr["STT"] = index;
                        dr["LoaiChuyenDe_KTKIT"] = "0";
                        dr["LoaiChuyenDe_DD"] = "0";
                        dr["LoaiChuyenDe_Khac"] = "0";
                        if (!string.IsNullOrEmpty(ht["SoGioThucTe"].ToString()))
                        //if ((Library.Int32Convert(ht["SoBuoi"]) * 4) == Library.Int32Convert(ht["SoGioThucTe"]))// Tham gia đủ số giờ tham gia đào tạo mới hiển thị
                        {
                            string loaiChuyenDe = ht["LoaiChuyenDe"].ToString();
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_KeToanKiemToan)
                            {
                                dr["LoaiChuyenDe_KTKIT"] = ht["SoGioThucTe"].ToString();
                                tongKtKiT += Library.Int32Convert(ht["SoGioThucTe"]);
                            }
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep)
                            {
                                dr["LoaiChuyenDe_DD"] = ht["SoGioThucTe"].ToString();
                                tongDD += Library.Int32Convert(ht["SoGioThucTe"]);
                            }
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_ChuyenDeKhac)
                            {
                                tongKhac += Library.Int32Convert(ht["SoGioThucTe"]);
                                dr["LoaiChuyenDe_Khac"] = ht["SoGioThucTe"].ToString();
                            }
                        }
                        dr["TongSoGio"] = (Library.Int32Convert(dr["LoaiChuyenDe_KTKIT"]) +
                                           Library.Int32Convert(dr["LoaiChuyenDe_DD"]) +
                                           Library.Int32Convert(dr["LoaiChuyenDe_Khac"]));
                        dt.Rows.Add(dr);
                        index++;
                    }

                    if (!string.IsNullOrEmpty(Library.CheckNull(ht["Chieu"])))
                    {
                        dr = dt.NewRow();
                        dr["Buoi"] = "Chiều";
                        dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                        dr["TenChuyenDe"] = ht["TenChuyenDe"];
                        dr["STT"] = index;
                        dr["LoaiChuyenDe_KTKIT"] = "0";
                        dr["LoaiChuyenDe_DD"] = "0";
                        dr["LoaiChuyenDe_Khac"] = "0";
                        if (!string.IsNullOrEmpty(ht["SoGioThucTe"].ToString()))
                        //if ((Library.Int32Convert(ht["SoBuoi"]) * 4) == Library.Int32Convert(ht["SoGioThucTe"]))// Tham gia đủ số giờ tham gia đào tạo mới hiển thị
                        {
                            string loaiChuyenDe = ht["LoaiChuyenDe"].ToString();
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_KeToanKiemToan)
                            {
                                dr["LoaiChuyenDe_KTKIT"] = ht["SoGioThucTe"].ToString();
                                tongKtKiT += Library.Int32Convert(ht["SoGioThucTe"]);
                            }
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep)
                            {
                                dr["LoaiChuyenDe_DD"] = ht["SoGioThucTe"].ToString();
                                tongDD += Library.Int32Convert(ht["SoGioThucTe"]);
                            }
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_ChuyenDeKhac)
                            {
                                tongKhac += Library.Int32Convert(ht["SoGioThucTe"]);
                                dr["LoaiChuyenDe_Khac"] = ht["SoGioThucTe"].ToString();
                            }
                        }
                        dr["TongSoGio"] = (Library.Int32Convert(dr["LoaiChuyenDe_KTKIT"]) +
                                           Library.Int32Convert(dr["LoaiChuyenDe_DD"]) +
                                           Library.Int32Convert(dr["LoaiChuyenDe_Khac"]));
                        dt.Rows.Add(dr);
                        index++;
                    }
                }
            }
            tongCong = tongKtKiT + tongDD + tongKhac;
            dt.Rows.Add(tongKtKiT.ToString(), tongDD.ToString(), tongKhac.ToString(), tongCong.ToString(), "", "", "", ""); // Dong cuoi cung la Tong

            //((TextObject)rpt.ReportDefinition.ReportObjects["txtTongKTKiT"]).Text = tongKtKiT.ToString();
            //((TextObject)rpt.ReportDefinition.ReportObjects["txtTongDD"]).Text = tongDD.ToString();
            //((TextObject)rpt.ReportDefinition.ReportObjects["txtTongKhac"]).Text = tongKhac.ToString();
            //tongCong = tongKtKiT + tongDD + tongKhac;
            //((TextObject)rpt.ReportDefinition.ReportObjects["txtTongCong"]).Text = tongCong.ToString();
        }
        return dt;
    }

    protected void SetDataJavascript()
    {
        string js = "";
        if (!string.IsNullOrEmpty(Request.Form["txtMaLopHoc"]))
        {
            js += "$('#txtMaLopHoc').val('" + Request.Form["txtMaLopHoc"] + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
        }

        if (!string.IsNullOrEmpty(Request.Form["txtMaHocVien"]))
        {
            js += "$('#txtMaHocVien').val('" + Request.Form["txtMaHocVien"] + "');" + Environment.NewLine;
            js += "CallActionGetInforHocVien();" + Environment.NewLine;
        }
        if (string.IsNullOrEmpty(Request.Form["txtNgayLapBC"]))
            js += "$('#txtNgayLapBC').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
        else
            js += "$('#txtNgayLapBC').val('" + Request.Form["txtNgayLapBC"] + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["txtSoVaoSo"]))
            js += "$('#txtSoVaoSo').val('" + Request.Form["txtSoVaoSo"] + "');" + Environment.NewLine;
        Response.Write(js);
    }

    private DataTable GetGioThamGiaHoc(string idLopHoc, string idHocVien, ReportDocument rpt)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "Ngay", "Buoi", "TenChuyenDe", "LoaiChuyenDe_KTKIT", "LoaiChuyenDe_DD", "LoaiChuyenDe_Khac", "STT", "TongSoGio" });
        if (string.IsNullOrEmpty(idLopHoc))
            return dt;
        string query = @"SELECT DISTINCT LHB.Ngay, LHB.Sang, LHB.Chieu, LHCD.TenChuyenDe, LHCD.LoaiChuyenDe, LHCD.SoBuoi, CASE SoGioThucTe WHEN 0 THEN 0 ELSE 4*(CONVERT(INT,ISNULL(LHB.Sang, '0')) + CONVERT(INT,ISNULL(LHB.Chieu, '0'))) END AS SoGioThucTe
                        FROM tblCNKTLopHocBuoi LHB
                        LEFT JOIN tblCNKTLopHocChuyenDe LHCD ON LHB.ChuyenDeID = LHCD.ChuyenDeID
                        LEFT JOIN tblCNKTLopHocSoGio LHSG ON LHB.ChuyenDeID = LHSG.ChuyenDeID
                        WHERE LHB.LopHocID = " + idLopHoc + @" AND LHSG.HoiVienCaNhanID = " + idHocVien + @"
                        ORDER BY Ngay, Chieu";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            DataRow dr;
            int index = 1;
            int tongKtKiT = 0, tongDD = 0, tongKhac = 0, tongCong = 0;
            foreach (Hashtable ht in listData)
            {
                if (!string.IsNullOrEmpty(Library.CheckNull(ht["Sang"])) && !string.IsNullOrEmpty(Library.CheckNull(ht["Chieu"])))
                {
                    dr = dt.NewRow();
                    dr["Buoi"] = "Ngày";
                    dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                    dr["TenChuyenDe"] = ht["TenChuyenDe"];
                    dr["STT"] = index;
                    dr["LoaiChuyenDe_KTKIT"] = "0";
                    dr["LoaiChuyenDe_DD"] = "0";
                    dr["LoaiChuyenDe_Khac"] = "0";
                    if (!string.IsNullOrEmpty(ht["SoGioThucTe"].ToString()))
                    //if ((Library.Int32Convert(ht["SoBuoi"]) * 4) == Library.Int32Convert(ht["SoGioThucTe"]))// Tham gia đủ số giờ tham gia đào tạo mới hiển thị
                    {
                        string loaiChuyenDe = ht["LoaiChuyenDe"].ToString();
                        if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_KeToanKiemToan)
                        {
                            dr["LoaiChuyenDe_KTKIT"] = ht["SoGioThucTe"].ToString();
                            tongKtKiT += Library.Int32Convert(ht["SoGioThucTe"]);
                        }
                        if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep)
                        {
                            dr["LoaiChuyenDe_DD"] = ht["SoGioThucTe"].ToString();
                            tongDD += Library.Int32Convert(ht["SoGioThucTe"]);
                        }
                        if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_ChuyenDeKhac)
                        {
                            tongKhac += Library.Int32Convert(ht["SoGioThucTe"]);
                            dr["LoaiChuyenDe_Khac"] = ht["SoGioThucTe"].ToString();
                        }
                    }
                    dr["TongSoGio"] = (Library.Int32Convert(dr["LoaiChuyenDe_KTKIT"]) +
                                       Library.Int32Convert(dr["LoaiChuyenDe_DD"]) +
                                       Library.Int32Convert(dr["LoaiChuyenDe_Khac"]));
                    dt.Rows.Add(dr);
                    index++;
                }
                else
                {
                    if (!string.IsNullOrEmpty(Library.CheckNull(ht["Sang"])))
                    {
                        dr = dt.NewRow();
                        dr["Buoi"] = "Sáng";
                        dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                        dr["TenChuyenDe"] = ht["TenChuyenDe"];
                        dr["STT"] = index;
                        dr["LoaiChuyenDe_KTKIT"] = "0";
                        dr["LoaiChuyenDe_DD"] = "0";
                        dr["LoaiChuyenDe_Khac"] = "0";
                        if (!string.IsNullOrEmpty(ht["SoGioThucTe"].ToString()))
                        //if ((Library.Int32Convert(ht["SoBuoi"]) * 4) == Library.Int32Convert(ht["SoGioThucTe"]))// Tham gia đủ số giờ tham gia đào tạo mới hiển thị
                        {
                            string loaiChuyenDe = ht["LoaiChuyenDe"].ToString();
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_KeToanKiemToan)
                            {
                                dr["LoaiChuyenDe_KTKIT"] = ht["SoGioThucTe"].ToString();
                                tongKtKiT += Library.Int32Convert(ht["SoGioThucTe"]);
                            }
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep)
                            {
                                dr["LoaiChuyenDe_DD"] = ht["SoGioThucTe"].ToString();
                                tongDD += Library.Int32Convert(ht["SoGioThucTe"]);
                            }
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_ChuyenDeKhac)
                            {
                                tongKhac += Library.Int32Convert(ht["SoGioThucTe"]);
                                dr["LoaiChuyenDe_Khac"] = ht["SoGioThucTe"].ToString();
                            }
                        }
                        dr["TongSoGio"] = (Library.Int32Convert(dr["LoaiChuyenDe_KTKIT"]) +
                                           Library.Int32Convert(dr["LoaiChuyenDe_DD"]) +
                                           Library.Int32Convert(dr["LoaiChuyenDe_Khac"]));
                        dt.Rows.Add(dr);
                        index++;
                    }

                    if (!string.IsNullOrEmpty(Library.CheckNull(ht["Chieu"])))
                    {
                        dr = dt.NewRow();
                        dr["Buoi"] = "Chiều";
                        dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                        dr["TenChuyenDe"] = ht["TenChuyenDe"];
                        dr["STT"] = index;
                        dr["LoaiChuyenDe_KTKIT"] = "0";
                        dr["LoaiChuyenDe_DD"] = "0";
                        dr["LoaiChuyenDe_Khac"] = "0";
                        if (!string.IsNullOrEmpty(ht["SoGioThucTe"].ToString()))
                        //if ((Library.Int32Convert(ht["SoBuoi"]) * 4) == Library.Int32Convert(ht["SoGioThucTe"]))// Tham gia đủ số giờ tham gia đào tạo mới hiển thị
                        {
                            string loaiChuyenDe = ht["LoaiChuyenDe"].ToString();
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_KeToanKiemToan)
                            {
                                dr["LoaiChuyenDe_KTKIT"] = ht["SoGioThucTe"].ToString();
                                tongKtKiT += Library.Int32Convert(ht["SoGioThucTe"]);
                            }
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_DaoDucNgheNghiep)
                            {
                                dr["LoaiChuyenDe_DD"] = ht["SoGioThucTe"].ToString();
                                tongDD += Library.Int32Convert(ht["SoGioThucTe"]);
                            }
                            if (loaiChuyenDe == ListName.Type_LoaiChuyenDe_ChuyenDeKhac)
                            {
                                tongKhac += Library.Int32Convert(ht["SoGioThucTe"]);
                                dr["LoaiChuyenDe_Khac"] = ht["SoGioThucTe"].ToString();
                            }
                        }
                        dr["TongSoGio"] = (Library.Int32Convert(dr["LoaiChuyenDe_KTKIT"]) +
                                           Library.Int32Convert(dr["LoaiChuyenDe_DD"]) +
                                           Library.Int32Convert(dr["LoaiChuyenDe_Khac"]));
                        dt.Rows.Add(dr);
                        index++;
                    }
                }
            }
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongKTKiT"]).Text = tongKtKiT.ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongDD"]).Text = tongDD.ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongKhac"]).Text = tongKhac.ToString();
            tongCong = tongKtKiT + tongDD + tongKhac;
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongCong"]).Text = tongCong.ToString();
        }
        return dt;
    }

    private void GetThongTinLopHoc(string idLopHoc, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idLopHoc))
            return;
        string query = "SELECT MaLopHoc, TenLopHoc, TuNgay, DenNgay, HanDangKy, TenTinh, DiaChiLopHoc FROM tblCNKTLopHoc LH LEFT JOIN tblDMTinh ON LH.TinhThanhID = tblDMTinh.TinhID WHERE LopHocID = " + idLopHoc;
        List<Hashtable> listData = _db.GetListData(query);
        {
            Hashtable ht = listData[0];
            string ngayLapBc = Request.Form["txtNgayLapBC"];
            if (!string.IsNullOrEmpty(ngayLapBc))
            {
                DateTime ngayKy = Library.DateTimeConvert(ngayLapBc, "dd/MM/yyyy");
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtNgayThangKy"]).Text = ht["TenTinh"] + ", Ngày " + ngayKy.Day + " tháng " + ngayKy.Month + " năm " + ngayKy.Year;
            }
            DateTime ngaybatdau = Library.DateTimeConvert(ht["TuNgay"]);
            if ((ngaybatdau.Day > 15 && ngaybatdau.Month == 8) || ngaybatdau.Month > 8)
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtNamLopHoc"]).Text = "Năm " + (ngaybatdau.Year + 1).ToString();
            else
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtNamLopHoc"]).Text = "Năm " + ngaybatdau.Year.ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtThongTinLopHoc"]).Text = "Đã tham gia lớp học cập nhật kiến thức " + ht["MaLopHoc"] + " (" + Library.DateTimeConvert(ht["TuNgay"]).ToString("dd/MM/yyyy") + " - " + Library.DateTimeConvert(ht["DenNgay"]).ToString("dd/MM/yyyy") + ") do VACPA tổ chức, như sau:";
        }
    }

    private void GetThongTinHocVien(string idHocVien, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idHocVien))
            return;
        string query = "SELECT (HoDem + ' ' + Ten) Fullname, SoChungChiKTV, NgayCapChungChiKTV FROM tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + idHocVien;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTenHoiVien"]).Text = ht["Fullname"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtSoChungChiKTV"]).Text = ht["SoChungChiKTV"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNgayCapChungChiKTV"]).Text = Library.DateTimeConvert(ht["NgayCapChungChiKTV"].ToString()).ToString("dd/MM/yyyy");
        }
    }

    public void DonVi(string ma = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT 0 AS HoiVienTapTheID, N'<< Khác >>' AS TenDoanhNghiep, '-' AS SoHieu UNION ALL (SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE '--- ' + ISNULL(A.SoHieu,LTRIM(RTRIM(A.MaHoiVienTapThe))) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID) ORDER BY SoHieu";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (ma == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }
}