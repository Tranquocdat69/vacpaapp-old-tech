﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_BTC_QLHV_GiayChungNhanHoiVienTapThe : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        setdataCrystalReport();
    }



    public void LoaiHinhDoanNghiep(string DonVi = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT LoaiHinhDoanhNghiepID,TenLoaiHinhDoanhNghiep FROM tblDMLoaiHinhDoanhNghiep ORDER BY TenLoaiHinhDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow loaiDN in dt.Rows)
        {
            if (DonVi == loaiDN["LoaiHinhDoanhNghiepID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + loaiDN["LoaiHinhDoanhNghiepID"].ToString().Trim() + @""">" + loaiDN["TenLoaiHinhDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + loaiDN["LoaiHinhDoanhNghiepID"].ToString().Trim() + @""">" + loaiDN["TenLoaiHinhDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }



    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        //output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (var tem in lst)
        {
            if (ma == tem.idDoiTuong.ToString())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }


    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    public void setdataCrystalReport()
    {
        string strid = idDanhSach.Text;
        if (!string.IsNullOrEmpty(strid))
        {
            int id = Convert.ToInt32(strid);
            List<objThongTinHoiVien_ChiTiet> lst = new List<objThongTinHoiVien_ChiTiet>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE HoiVienTapTheID = " + id;
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
        //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
       // int i = 1;
        foreach (DataRow tem in dt.Rows)
        {
            objThongTinHoiVien_ChiTiet new1 = new objThongTinHoiVien_ChiTiet();
            new1.HoTen = tem["TenDoanhNghiep"].ToString();
            //new1.NgaySinh = !string.IsNullOrEmpty(tem["NgaySinh"].ToString()) ? Convert.ToDateTime(tem["NgaySinh"]).ToShortDateString() : "";
            //new1.GioiTinh = tem["GioiTinh"].ToString();
            //new1.QueQuan = tem["TenTinh"].ToString();
            //new1.ChoOHienNay = tem["DiaChi"].ToString();
            //new1.BangCap = tem["TenTruongDaiHoc"].ToString();
            //new1.HocHam = tem["TenHocHam"].ToString();
            //new1.HocVi = tem["TenHocVi"].ToString();
            //new1.Nam_HocHam = tem["HocHamNam"].ToString();
            //new1.Nam_HocVi = tem["HocViNam"].ToString();
            //new1.CMT = tem["SoCMND"].ToString();
            //new1.NgayCap = !string.IsNullOrEmpty(tem["CMND_NgayCap"].ToString()) ? Convert.ToDateTime(tem["CMND_NgayCap"]).ToShortDateString() : "";
            //new1.SoTK = tem["SoTaiKhoanNganHang"].ToString();
            //new1.TaiNganHang = tem["TenNganHang"].ToString();
            //new1.SoChungChi = tem["SoChungChiKTV"].ToString();
            //new1.NgayCap_CT = !string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()) ? Convert.ToDateTime(tem["NgayCapChungChiKTV"]).ToShortDateString() : "";
            //new1.GiayDangKy = tem["SoGiayChungNhanDKHN"].ToString();
            //new1.NoiCapGiayDangKy = "";
            //new1.ChucVu = tem["TenChucVu"].ToString();
            //new1.DonViCongTac = tem["DonViCongTac"].ToString();
            //new1.Email = tem["Email"].ToString();
            //new1.DienThoai = "";
            //new1.DD = tem["DienThoai"].ToString();
            //new1.SoThich = tem["TenSoThich"].ToString();
            //new1.lstQuaTrinhLamViec = new List<lstOBJQuaTrinhLamViec>();
            //new1.lstKhenThuong = new List<lstOBJKhenThuong>();
            lst.Add(new1);

            //lstOBJKhenThuong con = new lstOBJKhenThuong();
            //con.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            //con.HinhThuc = "a" + i;
            //con.LyDo = "b" + i;
            //con.NgayThang = "c" + i;
            //lstOBJKhenThuong con1 = new lstOBJKhenThuong();
            //con1.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            //con1.HinhThuc = "a1" + i;
            //con1.LyDo = "b1" + i;
            //con1.NgayThang = "c1" + i;
            
            ////lstKhenThuong.Add(con);
            ////lstKhenThuong.Add(con1);
          

            //lstOBJKyLuat new1a = new lstOBJKyLuat();
            //new1a.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            //new1a.LyDo = "l1"+i;
            //new1a.HinhThuc = "";
            //new1a.NgayThang = "";
            //lstOBJKyLuat new2a = new lstOBJKyLuat();
            //new2a.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            //new2a.LyDo = "l1"+i;
            //new2a.HinhThuc = "";
            //new2a.NgayThang = "";
            //lstKyLuat.Add(new1a);
            //lstKyLuat.Add(new2a);
            //i++;
        }
        

            
            ReportDocument rpt = new ReportDocument();
            rpt.Load(Server.MapPath("Report/QLHV/GiayChungNhanHoiVienTapThe.rpt"));
            
            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objThongTinHoiVien_ChiTiet"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKhenThuong"].SetDataSource(lstKhenThuong);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);
          
            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "GiayChungNhanHoiVienTapThe");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "GiayChungNhanHoiVienTapThe");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            //ReportDocument rdPL = new ReportDocument();
            //rdPL.Load(Application.StartupPath + "\\GCN\\312_313\\" + strFilePL);
            //rdPL.SetDataSource(ds);
            //rdPL.SetParameterValue("NoiCapPL", txtNoiCapPL.Text,);
            //crtXemPL.ReportSource = rdPL;
            //crtXemPL.Refresh();

            //rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            //    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //{
            //    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //    //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //    //xlsFormatOptions.ShowGridLines = true;
            //    //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //    //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //    //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            //}
        }
    }
    public void DinhDangFile(string ma = "1")
    {

        List<objDoiTuong> lst = getDinhDangFile();
        string output_html = "";
        

        //output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (var tem in lst)
        {
            if (ma == tem.idDoiTuong.ToString())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    private List<objDoiTuong> getDinhDangFile()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Word";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Excel";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "PDF";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }
    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<int> lstIdChon = new List<int>();
            lstIdChon = Session["DSChon"] as List<int>;
            string result = string.Join(",", lstIdChon);
            idDanhSach.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select TenDoanhNghiep "
                            + " from dbo.tblHoiVienTapThe "
                            + " WHERE HoiVienTapTheID = " + lstIdChon[0];// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtCongTy.Text = tem["TenDoanhNghiep"].ToString();
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }

    protected void Text_Change(object sender, EventArgs e)
    {
        try{
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE HoiVienTapTheID = " + idDanhSach.Text;// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtCongTy.Text = tem["TenDoanhNghiep"].ToString();
        }
        catch { }
    }

}