﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
public partial class css_TT_BaoCao_NopPhiHoiVienCaNhan : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo nộp hội phí cá nhân";

    private Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        DataBaseDb db = new DataBaseDb();
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        // _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            //_db.OpenConnection();
            string strIDMaCongTy = string.Empty;
            string strIDHoiVien = string.Empty;
            string strConNo = string.Empty;
            string strKhongConNo = string.Empty;
            string strNgayChotSL = "";
            if (!string.IsNullOrEmpty(Request.Form["hdCongTyID"]))
            {
                strIDMaCongTy = Request.Form["hdCongTyID"];
            }
            if (!string.IsNullOrEmpty(Request.Form["hdHocVienID"]))
            {
                strIDHoiVien = Request.Form["hdHocVienID"];
            }
            if (!string.IsNullOrEmpty(Request.Form["cbConNoPhi"]))
            {
                strConNo = "yes";
            }
            if (!string.IsNullOrEmpty(Request.Form["cbKhongConNoPhi"]))
            {
                strKhongConNo = "yes";
            }
            //if (!string.IsNullOrEmpty(Request.Form["hdHocVienID"]))
            //{
            //    strIDHoiVien = Request.Form["hdHocVienID"];
            //}
            if (!string.IsNullOrEmpty(Request.Form["txtNgayLapBC"]))
            {
                strNgayChotSL = Request.Form["txtNgayLapBC"];
                DateTime dt = Library.DateTimeConvert(strNgayChotSL, "dd/MM/yyyy");
                DataSet ds = new DataSet();
                DataTable dt1 = new DataTable();
                if (!string.IsNullOrEmpty(Request.Form["txtNgayBatDauBC"]))
                {
                    DateTime dt_ngaybc = Library.DateTimeConvert(Request.Form["txtNgayBatDauBC"], "dd/MM/yyyy");
                    string[] paraName = { "@HoiVienTapTheID", "@HoiVienCaNhanID", "@NgayBatDauBaoCao", "@NgayChotSoLieu", "@ConNo", "@KhongConNo" };
                    object[] paraValue = { strIDMaCongTy, strIDHoiVien, dt_ngaybc, dt, strConNo, strKhongConNo };
                    ds = db.GetData(paraValue, paraName, "proc_BAOCAO_THANHTOAN_NOPHOIPHICANHAN", "NopHoiPhiCaNhan");
                }
                else
                {
                    string[] paraName = { "@HoiVienTapTheID", "@HoiVienCaNhanID", "@NgayChotSoLieu", "@ConNo", "@KhongConNo" };
                    object[] paraValue = { strIDMaCongTy, strIDHoiVien, dt, strConNo, strKhongConNo };
                    ds = db.GetData(paraValue, paraName, "proc_BAOCAO_THANHTOAN_NOPHOIPHICANHAN", "NopHoiPhiCaNhan");
                }
                //dsThanhToan ds = new dsThanhToan();
                
                // dt1.TableName = "NopHoiPhiCaNhan";
                
                //ds.Tables.Add(dt1);
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/ThanhToanRpts/rptBaoCaoNopHoiPhiCaNhans.rpt"));
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtThoiGian"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLapBaoCao"]).Text = cm.Admin_HoVaTen;
                rpt.SetDataSource(ds);

                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoNopPhiHVCaNhan");
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(ds.Tables[0]);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BaoCaoNopPhiHVCaNhan");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoNopPhiHVCaNhan");
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            //  _db.CloseConnection();
        }
        finally
        {
            // _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string js = "";
        if (!string.IsNullOrEmpty(Request.Form["txtNgayLapBC"]))
            js += "$('#txtNgayLapBC').val('" + Request.Form["txtNgayLapBC"] + "');" + Environment.NewLine;
        else
            js += "$('#txtNgayLapBC').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["hdCongTyID"]))
        {
            js += "$('#txtMaCongTy').val('" + Request.Form["txtMaCongTy"] + "');" + Environment.NewLine;
            js += "CallActionGetInforCongTy();" + Environment.NewLine;
        }

        if (!string.IsNullOrEmpty(Request.Form["hdHocVienID"]))
        {
            js += "$('#txtMaHocVien').val('" + Request.Form["txtMaHocVien"] + "');" + Environment.NewLine;
            js += "CallActionGetInforHocVien();" + Environment.NewLine;
        }
        if (!string.IsNullOrEmpty(Request.Form["cbConNoPhi"]))
            js += "$('#cbConNoPhi').prop('checked', true);" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["cbKhongConNoPhi"]))
            js += "$('#cbKhongConNoPhi').prop('checked', true);" + Environment.NewLine;
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }
        
        .HeaderColumn1
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='10' style='height: 80px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='10' style='text-align:center;font-size: 14pt; font-weight: bold;'>BÁO CÁO NỘP HỘI PHÍ CÁ NHÂN</td>
            </tr>            
            <tr><td colspan='10'></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td rowspan='2' class='HeaderColumn1'>STT</td>                
                <td rowspan='2' class='HeaderColumn1' style='width: 150px;'>Họ và tên</td>
                <td colspan='2' class='HeaderColumn1'>Ngày sinh</td>
                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Quê quán</td>
                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Số chứng chỉ KTV</td>
                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Ngày cấp chứng chỉ KTV</td>
                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Phí phải nộp năm nay</td>
                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Hội phí còn nợ các năm trước</td>
                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Tổng hội phí phải nộp năm nay</td>
                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Tổng hội phí đã nộp</td>
                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Số hội phí còn nợ</td>                
                            </tr>";
        htmlExport += @"<tr><td class='HeaderColumn1' style='width: 100px;'>Nam</td>
                            <td class='HeaderColumn1' style='width: 100px;'>Nữ</td></tr>";

        DataRow[] arrDataRow = dt.Select("", "TenDoanhNghiep ASC");
        string tempTenDoanhNghiep = "";
        double phiPhaiNopNamNay = 0,
               hoiPhiConNoCacNamTruoc = 0,
               tongHoiPhiPhaiNopNamNay = 0,
               tongHoiPhiDaNop = 0,
               soPhiConNo = 0;
        for (int i = 0; i < arrDataRow.Length; i++)
        {
            string tenDoanhNghiep = arrDataRow[i]["TenDoanhNghiep"].ToString();

            if (tenDoanhNghiep != tempTenDoanhNghiep)
            {
                tempTenDoanhNghiep = tenDoanhNghiep;
                htmlExport += "<tr><td colspan='12' class='GroupTitle'>" + tenDoanhNghiep + "</td></tr>";
            }
            htmlExport += "<tr><td class='Column'>" + (i + 1) + "</td>" +
                          "<td class='Column'>" + arrDataRow[i]["HoVaTen"] + "</td>" +
                          "<td class='Column'>" + (!string.IsNullOrEmpty(arrDataRow[i]["Nam"].ToString()) ? Library.DateTimeConvert(arrDataRow[i]["Nam"]).ToString("dd/MM/yyyy") : "") + "</td>" +
                          "<td class='Column'>" + (!string.IsNullOrEmpty(arrDataRow[i]["Nu"].ToString()) ? Library.DateTimeConvert(arrDataRow[i]["Nu"]).ToString("dd/MM/yyyy") : "") + "</td>" +
                          "<td class='Column'>" + arrDataRow[i]["DiaChi"] + "</td>" +
                          "<td class='Column'>" + arrDataRow[i]["SoChungChiKTV"] + "</td>" +
                          "<td class='Column'>" + (!string.IsNullOrEmpty(arrDataRow[i]["NgayCapChungChiKTV"].ToString()) ? Library.DateTimeConvert(arrDataRow[i]["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy") : "") + "</td>" +
                          "<td class='Column'>" + arrDataRow[i]["PhiPhayNopNamNay"] + "</td>" +
                          "<td class='Column'>" + arrDataRow[i]["HoiPhiConNoCacNamTruoc"] + "</td>" +
                          "<td class='Column'>" + arrDataRow[i]["TongHoiPhiPhaiNopNamNay"] + "</td>" +
                          "<td class='Column'>" + arrDataRow[i]["TongHoiPhiDaNop"] + "</td>" +
                          "<td class='Column'>" + arrDataRow[i]["SoPhiConNo"] + "</td></tr>";
            phiPhaiNopNamNay += Library.DoubleConvert(arrDataRow[i]["PhiPhayNopNamNay"]);
            hoiPhiConNoCacNamTruoc += Library.DoubleConvert(arrDataRow[i]["HoiPhiConNoCacNamTruoc"]);
            tongHoiPhiPhaiNopNamNay += Library.DoubleConvert(arrDataRow[i]["TongHoiPhiPhaiNopNamNay"]);
            tongHoiPhiDaNop += Library.DoubleConvert(arrDataRow[i]["TongHoiPhiDaNop"]);
            soPhiConNo += Library.DoubleConvert(arrDataRow[i]["SoPhiConNo"]);
        }
        htmlExport += "<tr><td></td><td style='font-weight: bold;'>Tổng số</td>" +
                      "<td></td><td></td><td></td><td></td><td></td>" +
                      "<td style='font-weight: bold;'>" + phiPhaiNopNamNay + "</td>" +
                      "<td style='font-weight: bold;'>" + hoiPhiConNoCacNamTruoc + "</td>" +
                      "<td style='font-weight: bold;'>" + tongHoiPhiPhaiNopNamNay + "</td>" +
                      "<td style='font-weight: bold;'>" + tongHoiPhiDaNop + "</td>" +
                      "<td style='font-weight: bold;'>" + soPhiConNo + "</td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='7'></td><td colspan='4' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='7'></td><td colspan='4' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='7'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='7'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='7'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='7'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='7'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='7'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='7'></td><td colspan='4' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("BaoCaoNopPhiHVCaNhan.xsl", "<html>" + css + htmlExport + "</html>");

    }
}