﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_dbChonHoiVienCaNhan_BC : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            
            Session["DSChon"] = null;
            
            grvDanhSach.Columns[1].HeaderText = "Mã";
            grvDanhSach.Columns[2].HeaderText = "Tên";
           // grvDanhSach.Columns[3].HeaderText = "Số CCKTV";

            if (Request.QueryString["type"] == "canhan")
                {
                    string DVId = Request.QueryString["DVID"];
                    string TID = Request.QueryString["TID"];
                    string VMID = Request.QueryString["VMID"];
                    string CCKTV = Request.QueryString["CCKTV"];

                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = @"SELECT MaHoiVienCaNhan as HoiVienCaNhanID, HODEM + ' ' + TEN AS HOTEN FROM dbo.tblHoiVienCaNhan a "
                                          + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID "
                                          + " inner join dbo.tblHoiVienTapThe c on a.HoiVienTapTheID = c.HoiVienTapTheID "
                                          + " WHERE 1=1 ";

                    if (!string.IsNullOrEmpty(DVId))
                        sql.CommandText = sql.CommandText + " AND c.MaHoiVienTapThe = '" + DVId + "'";
                    if (Convert.ToInt32(TID) != 0 && Convert.ToInt32(TID) != -1)
                        sql.CommandText = sql.CommandText + " AND a.DiaChi_TinhID = " + Convert.ToInt32(TID);
                    if (VMID != "null")
                        sql.CommandText = sql.CommandText + " AND b.VungMienID in (" + VMID + ")";

                    if (!string.IsNullOrEmpty(CCKTV))
                        sql.CommandText = sql.CommandText + " AND a.SoChungChiKTV like '%" + CCKTV + "%'";

                    DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                    grvDanhSach.DataSource = dtbTemp;
                    grvDanhSach.DataBind();
                    
                }

            else if (Request.QueryString["type"] == "canhan_don")
            {
                string DVId = Request.QueryString["DVID"];
                string TID = Request.QueryString["TID"];
                string VMID = Request.QueryString["VMID"];
                string CCKTV = Request.QueryString["CCKTV"];

                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"SELECT MaHoiVienCaNhan as HoiVienCaNhanID, HODEM + ' ' + TEN AS HOTEN FROM dbo.tblHoiVienCaNhan a "
                                      + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID "
                                      + " inner join dbo.tblHoiVienTapThe c on a.HoiVienTapTheID = c.HoiVienTapTheID "
                                      + " WHERE 1=1 ";

                if (Convert.ToInt32(DVId) != 0)
                    sql.CommandText = sql.CommandText + " AND c.HoiVienTapTheId = " + Convert.ToInt32(DVId);
                if (Convert.ToInt32(TID) != 0 && Convert.ToInt32(TID) != -1)
                    sql.CommandText = sql.CommandText + " AND a.DiaChi_TinhID = " + Convert.ToInt32(TID);
                if (VMID != "null")
                    sql.CommandText = sql.CommandText + " AND b.VungMienID in (" + VMID + ")";

                if (!string.IsNullOrEmpty(CCKTV))
                    sql.CommandText = sql.CommandText + " AND a.SoChungChiKTV like '%" + CCKTV + "%'";

                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                grvDanhSach.DataSource = dtbTemp;
                grvDanhSach.DataBind();

            }
            else if (Request.QueryString["type"] == "tapthe")
            {

                string DVId = Request.QueryString["DVID"].ToString();
                string TID = Request.QueryString["TID"];
                string VMID = Request.QueryString["VMID"].ToString();
                //if (!string.IsNullOrEmpty(DVId) && !string.IsNullOrEmpty(VMID))
                {
                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = @"select MaHoiVienTapThe AS HoiVienCaNhanID, TenDoanhNghiep AS HOTEN "
                                        + " from tblHoiVienTapThe a left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID "
                                        + " WHERE 1=1 "  ;
                    if (DVId != "null")
                        sql.CommandText = sql.CommandText + " AND LoaiHinhDoanhNghiepID in ( " + DVId + ")";
                    if (Convert.ToInt32(TID) != 0 && Convert.ToInt32(TID) != -1)
                        sql.CommandText = sql.CommandText + " AND a.DiaChi_TinhID = " + Convert.ToInt32(TID);
                    if (VMID != "null")
                        sql.CommandText = sql.CommandText + " AND VungMienID in (" + VMID + ")";
					sql.CommandText += " ORDER BY SoHieu";
                    DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                    grvDanhSach.DataSource = dtbTemp;
                    grvDanhSach.DataBind();
                }
                
            }
            else if (Request.QueryString["type"] == "madon")
            {

                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"select SoDonHoiVienTapThe AS HoiVienCaNhanID, TenDoanhNghiep AS HOTEN from tblDonHoiVienTapThe ";
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                grvDanhSach.DataSource = dtbTemp;
                grvDanhSach.DataBind();

            }
            else if (Request.QueryString["type"] == "doncanhan")
            {

                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"select SoDonHoiVienCaNhan AS HoiVienCaNhanID, HoDem + ' ' + Ten AS HOTEN from tblDonHoiVienCaNhan ";
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                grvDanhSach.DataSource = dtbTemp;
                grvDanhSach.DataBind();

            }
            else if (Request.QueryString["type"] == "canhanKetNap")
            {

                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"select MaHoiVienCaNhan AS HoiVienCaNhanID, HoDem + ' ' + Ten AS HOTEN "
                             + " from dbo.tblHoiVienCaNhan a  ";
                             
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                grvDanhSach.DataSource = dtbTemp;
                grvDanhSach.DataBind();

            }
         }
        //else
        //{
            
        //}
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]) || !string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
        {
            
        if (Request.QueryString["type"] == "canhan")
            {
            //    string id = Request.QueryString["id"];
            //if (!string.IsNullOrEmpty(id))
                {
                    string DVId = Request.QueryString["DVID"];
                    string TID = Request.QueryString["TID"];
                    string VMID = Request.QueryString["VMID"];
                    string CCKTV = Request.QueryString["CCKTV"];

                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = @"SELECT MaHoiVienCaNhan as HoiVienCaNhanID, HODEM + ' ' + TEN AS HOTEN FROM dbo.tblHoiVienCaNhan a "
                                          + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID  "
                                            +" inner join dbo.tblHoiVienTapThe c on a.HoiVienTapTheID = c.HoiVienTapTheID "
                                                + " WHERE 1=1 ";

                    if (!string.IsNullOrEmpty(DVId))
                        sql.CommandText = sql.CommandText + " AND c.MaHoiVienTapThe = '" + DVId + "'";
                    //if (Convert.ToInt32(DVId) != 0)
                    //    sql.CommandText = sql.CommandText + " AND a.HoiVienTapTheID = " + Convert.ToInt32(DVId);
                    if (Convert.ToInt32(TID) != 0 && Convert.ToInt32(TID) != -1)
                        sql.CommandText = sql.CommandText + " AND a.DiaChi_TinhID = " + Convert.ToInt32(TID);
                    if (VMID != "null")
                        sql.CommandText = sql.CommandText + " AND VungMienID in (" + VMID + ")";

                    if (!string.IsNullOrEmpty(CCKTV))
                        sql.CommandText = sql.CommandText + " AND a.SoChungChiKTV like '%" + CCKTV + "%'";

                    if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                    {
                        sql.CommandText += " AND MaHoiVienCaNhan like '%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                    }
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                    {
                        sql.CommandText += " AND LTRIM(RTRIM(HODEM)) + ' ' + LTRIM(RTRIM(TEN)) like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
                    }
                    DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                    grvDanhSach.DataSource = dtbTemp;
                    grvDanhSach.DataBind();
                }
            }

        else if (Request.QueryString["type"] == "canhan_don")
        {
            //    string id = Request.QueryString["id"];
            //if (!string.IsNullOrEmpty(id))
            {
                string DVId = Request.QueryString["DVID"];
                string TID = Request.QueryString["TID"];
                string VMID = Request.QueryString["VMID"];
                // string CCKTV = Request.QueryString["CCKTV"];

                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"SELECT MaHoiVienCaNhan as HoiVienCaNhanID, HODEM + ' ' + TEN AS HOTEN FROM dbo.tblHoiVienCaNhan a "
                                      + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID  "
                                        + " inner join dbo.tblHoiVienTapThe c on a.HoiVienTapTheID = c.HoiVienTapTheID "
                                            + " WHERE 1=1 ";

                if (Convert.ToInt32(DVId) != 0)
                    sql.CommandText = sql.CommandText + " AND c.HoiVienTapTheId = " + Convert.ToInt32(DVId);
                if (Convert.ToInt32(DVId) != 0)
                    sql.CommandText = sql.CommandText + " AND a.HoiVienTapTheID = " + Convert.ToInt32(DVId);
                if (Convert.ToInt32(TID) != 0 && Convert.ToInt32(TID) != -1)
                    sql.CommandText = sql.CommandText + " AND a.DiaChi_TinhID = " + Convert.ToInt32(TID);
                if (VMID != "null")
                    sql.CommandText = sql.CommandText + " AND VungMienID in (" + VMID + ")";

                //if (!string.IsNullOrEmpty(CCKTV))
                //    sql.CommandText = sql.CommandText + " AND a.SoChungChiKTV like '%" + CCKTV + "%'";

                if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                {
                    sql.CommandText += " AND MaHoiVienCaNhan like '%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                }
                if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                {
                    sql.CommandText += " AND LTRIM(RTRIM(HODEM)) + ' ' + LTRIM(RTRIM(TEN)) like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
                }
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                grvDanhSach.DataSource = dtbTemp;
                grvDanhSach.DataBind();
            }
        }
        else if (Request.QueryString["type"] == "tapthe")
            {
                string DVId = Request.QueryString["DVID"];
                string TID = Request.QueryString["TID"];
                string VMID = Request.QueryString["VMID"];

                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"select MaHoiVienTapThe AS HoiVienCaNhanID, TenDoanhNghiep AS HOTEN from tblHoiVienTapThe a left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.MaTinh WHERE 1=1  ";
                if (DVId != "null")
                    sql.CommandText = sql.CommandText + " AND LoaiHinhDoanhNghiepID in ( " + DVId + ")";
                if (Convert.ToInt32(TID) != 0 && Convert.ToInt32(TID) != -1)
                    sql.CommandText = sql.CommandText + " AND a.DiaChi_TinhID = " + Convert.ToInt32(TID);
                if (VMID != "null")
                    sql.CommandText = sql.CommandText + " AND VungMienID IS NULL OR VungMienID in (" + VMID + ")";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                {
                    sql.CommandText += " AND MaHoiVienTapThe like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                }
                if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                {
                    sql.CommandText += " AND TenDoanhNghiep like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
                }
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                grvDanhSach.DataSource = dtbTemp;
                grvDanhSach.DataBind();
            }
        else if (Request.QueryString["type"] == "madon")
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"select SoDonHoiVienTapThe AS HoiVienCaNhanID, TenDoanhNghiep AS HOTEN from tblDonHoiVienTapThe ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
            {
                sql.CommandText += " AND SoDonHoiVienTapThe like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
            }
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            {
                sql.CommandText += " AND TenDoanhNghiep like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
            }
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            grvDanhSach.DataSource = dtbTemp;
            grvDanhSach.DataBind();
        }
        else if (Request.QueryString["type"] == "doncanhan")
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"select SoDonHoiVienCaNhan AS HoiVienCaNhanID, HoDem + ' ' + Ten AS HOTEN from tblDonHoiVienCaNhan ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
            {
                sql.CommandText += " AND SoDonHoiVienCaNhan like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
            }
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            {
                sql.CommandText += " AND LTRIM(RTRIM(HODEM)) + ' ' + LTRIM(RTRIM(TEN)) like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
            }
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            grvDanhSach.DataSource = dtbTemp;
            grvDanhSach.DataBind();
        }

        else if (Request.QueryString["type"] == "canhanKetNap")
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"select MaHoiVienCaNhan AS HoiVienCaNhanID, HoDem + ' ' + Ten AS HOTEN "
                             + " from dbo.tblHoiVienCaNhan a where 1=1 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
            {
                sql.CommandText += " AND MaHoiVienCaNhan like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
            }
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            {
                sql.CommandText += " AND LTRIM(RTRIM(HODEM)) + ' ' + LTRIM(RTRIM(TEN)) like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
            }
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            grvDanhSach.DataSource = dtbTemp;
            grvDanhSach.DataBind();
        }

        }
    }

    //protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
    //{
    //    //CheckBox ChkBoxHeader = (CheckBox)grvDanhSach.HeaderRow.FindControl("chkboxSelectAll");
    //    //for (int i = 0; i < grvDanhSach.Rows.Count; i++)
    //    //{
    //    //    GridViewRow row = grvDanhSach.Rows[i];
    //    //    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkEmp");
    //    //    if (ChkBoxHeader.Checked == true)
    //    //    {
    //    //        ChkBoxRows.Checked = true;
    //    //        chkEmp_CheckedChanged(ChkBoxRows, null);
    //    //    }
    //    //    else
    //    //    {
    //    //        ChkBoxRows.Checked = false;
    //    //        chkEmp_CheckedChanged(ChkBoxRows, null);
    //    //    }
    //    //}
    //}

    protected void chkEmp_CheckedChanged(object sender, EventArgs e)
    {
       // Session["DSChon"] = grvDanhSach.SelectedRow.Cells[0].Text;
       // if (Request.QueryString["type"] == "madon" || Request.QueryString["type"] == "doncanhan")
        {
            List<string> lstIdChon = new List<string>();

            if (Session["DSChon"] != null)
            {
                lstIdChon = (List<string>)Session["DSChon"];
            }

            CheckBox cbkdangcheck = (CheckBox)sender;
            GridViewRow rowdangcheck = (GridViewRow)cbkdangcheck.NamingContainer;
            //rowdangcheck.
            int idongdangcheck = Convert.ToInt32(rowdangcheck.RowIndex);
            if (cbkdangcheck.Checked == true)
            {
                string id = grvDanhSach.DataKeys[idongdangcheck].Value.ToString().Trim();
                if (!lstIdChon.Contains(id))
                    lstIdChon.Add(id);
            }
            else
            {
                string id = grvDanhSach.DataKeys[idongdangcheck].Value.ToString().Trim();
                if (lstIdChon.Contains(id))
                    lstIdChon.Remove(id);
            }

            Session["DSChon"] = lstIdChon;
        }
        //else
        //{
        //    List<int> lstIdChon = new List<int>();

        //    if (Session["DSChon"] != null)
        //    {
        //        lstIdChon = (List<int>)Session["DSChon"];
        //    }
        
        //    CheckBox cbkdangcheck = (CheckBox)sender;
        //    GridViewRow rowdangcheck = (GridViewRow)cbkdangcheck.NamingContainer;
        //    //rowdangcheck.
        //    int idongdangcheck = Convert.ToInt32(rowdangcheck.RowIndex);
        //    if (cbkdangcheck.Checked == true)
        //    {
        //        int id = Convert.ToInt32(grvDanhSach.DataKeys[idongdangcheck].Value);
        //        if (!lstIdChon.Contains(id))
        //            lstIdChon.Add(id);
        //    }
        //    else
        //    {
        //        int id = Convert.ToInt32(grvDanhSach.DataKeys[idongdangcheck].Value);
        //        if (lstIdChon.Contains(id))
        //            lstIdChon.Remove(id);
        //    }

        //    Session["DSChon"] = lstIdChon;
        //}

    }
}