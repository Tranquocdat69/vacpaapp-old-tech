﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_BangTongHopSaiPhamKyThuat_TungCongTy.ascx.cs" Inherits="usercontrols_BaoCao_KSCL_BaoCao_BangTongHopSaiPhamKyThuat_TungCongTy" %>

<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Bảng tổng hợp sai phạm hệ kỹ thuật</h4>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td>
                Năm<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlNam" name="ddlNam" onchange="CallActionGetDanhSachCongTy();">
                    <% GetListYear();%>
                </select>
            </td>
        </tr>
        <tr>
            <td style="width: 150px;">
                Công ty kiểm toán:
            </td>
            <td>
                <select id="ddlCongTy" name="ddlCongTy">
                </select>
                <input type="hidden" id="hdCongTy" name="hdCongTy" />
                <input type="hidden" id="hdTenCongTy" name="hdTenCongTy" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('excel');"><i class="iconsweets-excel2"></i>Kết xuất Excel</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Bảng tổng hợp sai phạm kỹ thuật</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" ShowAllPageIds="True"
        HasExportButton="False" HasPrintButton="False" AutoDataBind="True" />
</fieldset>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    function CallActionGetDanhSachCongTy() {
        iframeProcess.location = '/iframe.aspx?page=KSCL_BaoCao_Proccess&action=loaddscthtsp&nam=' + $('#ddlNam option:selected').val();
    }
    
    <% SetDataJavascript();%>

    function DrawData_DanhSachCongTy(arrData) {
        
        // Remove toàn bộ option cũ
        $('#ddlCongTy').children().remove();
        // Add option mới theo công ty
        if(arrData.length > 0) {
            $('#ddlCongTy').append('<option value="0"><< Chọn công ty >></option>');
            for(var i = 0 ; i < arrData.length ; i ++) {
                if($('#hdCongTy').val().length > 0 && $('#hdCongTy').val() == arrData[i][0])
                    $('#ddlCongTy').append('<option value="'+arrData[i][0]+'" selected="selected">'+arrData[i][1]+'</option>');
                else
                    $('#ddlCongTy').append('<option value="'+arrData[i][0]+'">'+arrData[i][1]+'</option>');
            }
        } else {
            $('#ddlCongTy').append('<option value="0"><< Không có công ty được kiểm tra trong năm này! >></option>');
        }
    }

    function Export(type) {
        // Kiểm tra bắt phải chọn tối thiểu 1 công ty
        var flag = false;
        if($('#ddlCongTy option:selected').val() != "0") {
            $('#hdTenCongTy').val($('#ddlCongTy option:selected').html());
            flag = true;
        }
        if(flag) {
            $('#hdAction').val(type);
            $('#Form1').submit();   
        } else {
            alert('Phải chọn tối thiểu 1 công ty để thực hiện tổng hợp số liệu!');
            return false;
        }
    }

    
</script>
<div id="DivDanhSachDoanKiemTra">
</div>
<div id="DivDanhSachCongTy">
</div>