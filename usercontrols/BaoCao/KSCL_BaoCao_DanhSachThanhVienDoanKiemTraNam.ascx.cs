﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_KSCL_BaoCao_DanhSachThanhVienDoanKiemTraNam : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách thành viên đoàn kiểm tra năm";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = "";
            if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            {
                nam = Request.Form["ddlNam"];

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/DanhSachThanhVienDoanKiemTra.rpt"));
                DataTable dt = GetDanhSachThanhVienDoanKiemTra(nam, rpt);
                rpt.Database.Tables["dtDanhSachThanhVienDoanKiemTra"].SetDataSource(dt);
                string quyetDinhThanhLap = GetQuyetDinhThanhLapDoanKiemTraTrongNam(nam, rpt);
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "DANH SÁCH THÀNH VIÊN ĐOÀN KIỂM TRA NĂM " + nam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachThanhVienDoanKiemTraNam" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, nam, quyetDinhThanhLap);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "DanhSachThanhVienDoanKiemTraNam" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachThanhVienDoanKiemTraNam" + nam);
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
        {
            string js = "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboDaNopBaoCao"]))
                js += "$('#cboDaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboDaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboChuaNopBaoCao"]))
                js += "$('#cboChuaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboChuaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            Response.Write(js);
        }
    }

    private string GetQuyetDinhThanhLapDoanKiemTraTrongNam(string nam, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(nam))
            return "";
        string query = "SELECT QuyetDinhThanhLap FROM tblKSCLDoanKiemTra DKT WHERE YEAR(DKT.NgayLap) = " + nam + " AND DKT.TinhTrangID = 5";
        List<Hashtable> listData = _db.GetListData(query);
        string quyetDinhThanhLap = "";
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                quyetDinhThanhLap += ht["QuyetDinhThanhLap"] + "<br>";
            }
            rpt.DataDefinition.FormulaFields["UnboundString1"].Text = "\"" + quyetDinhThanhLap + "\"";
        }
        return quyetDinhThanhLap;
    }

    private DataTable GetDanhSachThanhVienDoanKiemTra(string nam, ReportDocument rpt)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "HoTen", "ChucVu", "DonViCongTac", "LoaiThanhVien" });
        if (string.IsNullOrEmpty(nam))
            return dt;

        string procName = ListName.Proc_BAOCAO_KSCL_DanhSachThanhVienDoanKiemTra;
        List<Hashtable> listData = _db.GetListData(procName, new List<string> { "@Nam" }, new List<object> { nam });
        if (listData.Count > 0)
        {
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongSo"]).Text = "Tổng số: " + listData.Count;
            DataRow dr;
            int index = 1;
            foreach (Hashtable ht in listData)
            {
                dr = dt.NewRow();
                dr["STT"] = index;
                dr["HoTen"] = ht["Fullname"];
                dr["ChucVu"] = ht["TenChucVu"];
                dr["DonViCongTac"] = ht["DonViCongTac"];
                dr["LoaiThanhVien"] = ht["LoaiThanhVien"];
                dt.Rows.Add(dr);
                index++;
            }
        }
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, string nam, string quyetDinhThanhLap)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='4' style='height: 80px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='4' style='text-align:center;font-size: 14pt; font-weight: bold;'>DANH SÁCH THÀNH VIÊN ĐOÀN KIỂM TRA NĂM " + nam + @"</td>
            </tr>
            <tr>
                <td colspan='4' style='text-align:center;font-size: 12pt;'><i>" + quyetDinhThanhLap + @"</i></td>
            </tr>
<tr><td colspan='4'></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn'>STT</td>                
                <td class='HeaderColumn' style='width: 250px;'>Họ và tên</td>
                <td class='HeaderColumn' style='width: 150px;'>Chức vụ</td>
                <td class='HeaderColumn' style='width: 250px;'>Đơn vị công tác</td>                
                            </tr>";
        string tempLoai = "";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string loai = dt.Rows[i]["LoaiThanhVien"].ToString();

            if (loai != tempLoai)
            {
                tempLoai = loai;
                htmlExport += "<tr><td colspan='4' class='GroupTitle'>" + loai + "</td></tr>";
            }
            htmlExport += "<tr><td class='Column'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["HoTen"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["ChucVu"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DonViCongTac"] + "</td></tr>";
        }
        htmlExport += "<tr><td colspan='4' style='font-weight: bold;'>Tổng số: " + dt.Rows.Count + "</td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='3'></td><td style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='3'></td><td style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("DanhSachThanhVienDoanKiemTraNam" + nam + ".xsl", "<html>" + css + htmlExport + "</html>");
        
    }
}