﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BaoCao_DanhSachKetNapHoiVienTapThe : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách kết nạp hội viên tổ chức";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim() == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Contains(tem.idDoiTuong.ToString()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    public void setdataCrystalReport()
    {
        string VungMien = Request.Form["VungMienId"];
        string Tinh = Request.Form["TinhId"];
        DateTime? NgayBatDau = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayBatDau"]))
            NgayBatDau = DateTime.ParseExact(Request.Form["NgayBatDau"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime? NgayKetThuc = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayKetThuc"]))
            NgayKetThuc = DateTime.ParseExact(Request.Form["NgayKetThuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

        // if (NgayBatDau != null && NgayKetThuc != null)
        {
            List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
            List<objThongTinDoanhNghiep_CT> lst = new List<objThongTinDoanhNghiep_CT>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select a.LoaiHoiVienTapTheChiTiet, MaHoiVienTapThe, SoHieu, TenDoanhNghiep, TenVietTat, TenTiengAnh, DiaChi, "
                                + " DienThoai, Fax, Email, Website, SoGiayChungNhanDKKD, NgayCapGiayChungNhanDKKD, "
                                + " SoGiayChungNhanKDDVKT, NgayCapGiayChungNhanKDDVKT,NguoiDaiDienPL_Ten, "
                                + " NguoiDaiDienPL_DiDong, NguoiDaiDienPL_Email, NguoiDaiDienLL_Ten, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email, "
                                + " SoQuyetDinhKetNap, NgayQuyetDinhKetNap "
                                + " from dbo.tblHoiVienTapThe a "
                                + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID "
                                + " where 1=1";

            if (!string.IsNullOrEmpty(Request.Form["drLoaiHoiVienTT"]) && Request.Form["drLoaiHoiVienTT"] != "-1")
                sql.CommandText = sql.CommandText + " AND a.LoaiHoiVienTapTheChiTiet = " + Request.Form["drLoaiHoiVienTT"];

            if (!string.IsNullOrEmpty(VungMien) && !VungMien.Contains("-1"))
                sql.CommandText = sql.CommandText + " AND b.VungMienID in (" + VungMien + ")";

            if (Tinh != "0" && Tinh != "-1" && !string.IsNullOrEmpty(Tinh))
                sql.CommandText = sql.CommandText + " AND a.DiaChi_TinhID = " + Convert.ToInt32(Tinh);

            if (NgayBatDau != null && NgayKetThuc != null)
                sql.CommandText = sql.CommandText + " AND NgayQuyetDinhKetNap >= @NgayBatDau and NgayQuyetDinhKetNap <= @NgayKetThuc";

            if (Request.Form["chkChiNhanh"] != null)
                sql.CommandText += " AND a.HoiVienTapTheChaID IS NOT NULL";
            else
                sql.CommandText += " AND a.LoaiHoiVienTapThe = 1 AND a.HoiVienTapTheChaID IS NULL";

            if (NgayBatDau != null)
                sql.Parameters.AddWithValue("@NgayBatDau", NgayBatDau);
            else
                sql.Parameters.AddWithValue("@NgayBatDau", DBNull.Value);
            if (NgayKetThuc != null)
                sql.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);
            else
                sql.Parameters.AddWithValue("@NgayKetThuc", DBNull.Value);

            sql.CommandText += " order by ISNULL(CONVERT(INT, SoHieu), 0)";

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];



            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();

                new1.STT = i.ToString();
                new1.MaHoiVienTapThe = tem["MaHoiVienTapThe"].ToString();
                if (!string.IsNullOrEmpty(tem["LoaiHoiVienTapTheChiTiet"].ToString()))
                {
                    switch (tem["LoaiHoiVienTapTheChiTiet"].ToString())
                    {
                        case "0":
                            new1.LoaiHoiVienChiTiet = "Hội viên chính thức";
                            break;
                        case "1":
                            new1.LoaiHoiVienChiTiet = "Hội viên liên kết";
                            break;

                        default:
                            break;
                    }
                }
                new1.SoHieu = tem["SoHieu"].ToString();
                new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
                new1.TenVietTat = tem["TenVietTat"].ToString();
                new1.TenTiengAnh = tem["TenTiengAnh"].ToString();
                new1.DiaChi = tem["DiaChi"].ToString();
                new1.DienThoai = tem["DienThoai"].ToString();
                new1.Fax = tem["Fax"].ToString();
                new1.Email = tem["Email"].ToString();
                new1.Website = tem["Website"].ToString();
                new1.SoDK = tem["SoGiayChungNhanDKKD"].ToString();
                new1.NgayDK = !string.IsNullOrEmpty(tem["NgayCapGiayChungNhanDKKD"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanDKKD"])) : "";
                new1.SoGiayCN = tem["SoGiayChungNhanKDDVKT"].ToString();
                new1.NgayCN = !string.IsNullOrEmpty(tem["NgayCapGiayChungNhanKDDVKT"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanKDDVKT"])) : "";
                new1.NguoiDaiDien_Ten = tem["NguoiDaiDienPL_Ten"].ToString();
                new1.NguoiDaiDien_mobile = tem["NguoiDaiDienPL_DiDong"].ToString();
                new1.NguoiDaiDien_Email = tem["NguoiDaiDienPL_Email"].ToString();
                new1.NguoiDaiDienLL_Ten = tem["NguoiDaiDienLL_Ten"].ToString();
                new1.NguoiDaiDienLL_mobile = tem["NguoiDaiDienLL_DiDong"].ToString();
                new1.NguoiDaiDienLL_Email = tem["NguoiDaiDienLL_Email"].ToString();
                new1.SoQD = tem["SoQuyetDinhKetNap"].ToString();
                new1.NgayKyQD = !string.IsNullOrEmpty(tem["NgayQuyetDinhKetNap"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayQuyetDinhKetNap"])) : "";

                lst.Add(new1);


            }

            TenBaoCao newabc = new TenBaoCao();
            newabc.TongCong = "Tổng cộng: " + i.ToString();

            string NgayTinh = "";
            string ngayBC = "";
            if (NgayBatDau != null && NgayKetThuc != null)
                NgayTinh = "Tính từ " + NgayThangVN(NgayBatDau.Value) + " - " + NgayThangVN(NgayKetThuc.Value);
            else
                NgayTinh = "Tính đến thời điểm hiện tại";

            newabc.NgayTinh = NgayTinh;

            ngayBC = "Hà nội, ngày " + (DateTime.Now.Day.ToString().Length == 1 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + " tháng " + (DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + " năm " + DateTime.Now.Year;
            newabc.ngayBC = ngayBC;
            lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/BaoCaoDanhSachKetNapHoiVienTapThe.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
            {
                //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BaoCaoDanhSachKetNapHoiVienTapThe");
                ExportToExcel("DanhSachKetNapHoiVienTapThe.xls", lst, NgayTinh, ngayBC);
            }
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoDanhSachKetNapHoiVienTapThe");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }


        }
    }

    private void ExportToExcel(string strName, List<objThongTinDoanhNghiep_CT> lst, string NgayTinh, string NgayBC)
    {
        string outHTML = "";

        outHTML += "<table border = '1' cellpadding='5' cellspacing='0' style='font-family:Arial; font-size:8pt; color:#003366'>";
        outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        outHTML += "<td colspan='3' style='border-style:none'></td>";
        //outHTML += "<td colspan='9' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/logo.png' alt='' border='0px'/></td>";
        outHTML += "<td colspan='18' align='right' width='197px' height='81px' style='border-style:none; font-family:.VnHelvetInsH; font-size:14pt; color:Black'><b><u>HỘI KIỂM TOÁN VIÊN HÀNH NGHỀ VIỆT NAM</i></b></td>";
        outHTML += "<td colspan='2' style='border-style:none'></td>";

        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='23' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH KẾT NẠP HỘI VIÊN TẬP THỂ</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='23' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i>" + NgayTinh + "</i></td>";
        outHTML += "</tr>";
        outHTML += "<tr style='border-style:none'></tr>";

        outHTML += "<tr>";
        outHTML += "    <th rowspan='2' width='30px'>STT</th>";
        outHTML += "    <th rowspan='2' width='100px'>ID</th>";
        outHTML += "    <th rowspan='2' width='100px'>Loại hội viên chi tiết</th>";
        outHTML += "    <th rowspan='2' width='100px'>Số hiệu công ty</th>";
        outHTML += "    <th rowspan='2' width='200px'>Tên doanh nghiệp</th>";
        outHTML += "    <th rowspan='2' width='100px'>Tên viết tắt</th>";
        outHTML += "    <th rowspan='2' width='100px'>Tên tiếng anh</th>";
        outHTML += "    <th colspan='5' width='500px'>Thông tin liên lạc</th>";
        outHTML += "    <th rowspan='2' width='100px'>Số giấy CN đăng ký kinh doan/giấy chứng nhận đầu tư</th>";
        outHTML += "    <th rowspan='2' width='50px'>Ngày cấp giấy CN đăng ký kinh doan/giấy chứng nhận đầu tư</th>";
        outHTML += "    <th rowspan='2' width='50px'>Số giấy CN đủ điều kiện kinh doanh dịch vụ kiểm toán</th>";
        outHTML += "    <th rowspan='2' width='50px'>Ngày cấp giấy CN đủ điều kiện kinh doanh dịch vụ kiểm toán</th>";
        outHTML += "    <th rowspan='2' width='100px'>Họ và tên người đại diện pháp luật</th>";
        outHTML += "   <th rowspan='2' width='50px'>Mobile người đại diện pháp luật</th>";
        outHTML += "    <th rowspan='2' width='50px'>Email người đại diện pháp luật</th>";
        outHTML += "    <th rowspan='2' width='50px'>Họ và tên người đại diện liên lạc</th>";
        outHTML += "    <th rowspan='2' width='50px'>Mobile người đại diện liên lạc</th>";
        outHTML += "    <th rowspan='2' width='50px'>Email người đại diện liên lạc</th>";
        outHTML += "    <th rowspan='2' width='50px'>Số QĐ Kết nạp HVTC</th>";
        outHTML += "    <th rowspan='2' width='50px'>Ngày ký QĐ kết nạp HVTC</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th width='100px'>Địa chỉ trụ sở chính</th>";
        outHTML += "    <th width='100px'>ĐTCĐ</th>";
        outHTML += "    <th width='100px'>Fax</th>";
        outHTML += "    <th width='100px'>Email</th>";
        outHTML += "    <th width='100px'>Website</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th width='30px'>1</th>";
        outHTML += "    <th width='100px'>2</th>";
        outHTML += "    <th width='100px'>3</th>";
        outHTML += "    <th width='200px'>4</th>";
        outHTML += "    <th width='100px'>5</th>";
        outHTML += "    <th width='100px'>6</th>";
        outHTML += "    <th width='200px'>7</th>";
        outHTML += "    <th width='100px'>8</th>";
        outHTML += "    <th width='100px'>9</th>";
        outHTML += "    <th width='100px'>10</th>";
        outHTML += "    <th width='100px'>11</th>";
        outHTML += "    <th width='100px'>12</th>";
        outHTML += "    <th width='100px'>13</th>";
        outHTML += "    <th width='100px'>14</th>";
        outHTML += "    <th width='100px'>15</th>";
        outHTML += "    <th width='100px'>16</th>";
        outHTML += "    <th width='100px'>17</th>";
        outHTML += "    <th width='100px'>18</th>";
        outHTML += "    <th width='100px'>19</th>";
        outHTML += "    <th width='100px'>20</th>";
        outHTML += "    <th width='100px'>21</th>";
        outHTML += "    <th width='100px'>22</th>";
        outHTML += "    <th width='100px'>23</th>";
        outHTML += "    <th width='100px'>24</th>";
        outHTML += "</tr>";

        int i = 0;
        foreach (var tem in lst)
        {
            i++;
            outHTML += "<tr>";
            outHTML += "<td>" + i.ToString() + "</td>";
            outHTML += "<td>" + "'" + tem.MaHoiVienTapThe.ToString() + "</td>";
            if (tem.LoaiHoiVienChiTiet != null)
                outHTML += "<td>" + "'" + tem.LoaiHoiVienChiTiet.ToString() + "</td>";
            else
                outHTML += "<td></td>";
            outHTML += "<td>" + "'" + tem.SoHieu.ToString() + "</td>";
            outHTML += "<td>" + tem.TenDoanhNghiep + "</td>";
            outHTML += "<td>" + tem.TenVietTat + "</td>";
            outHTML += "<td>" + tem.TenTiengAnh + "</td>";
            outHTML += "<td>" + tem.DiaChi + "</td>";
            outHTML += "<td>" + "'" + tem.DienThoai + "</td>";
            outHTML += "<td>" + tem.Fax + "</td>";
            outHTML += "<td>" + tem.Email + "</td>";
            outHTML += "<td>" + tem.Website + "</td>";
            outHTML += "<td>" + tem.SoDK + "</td>";
            outHTML += "<td>" + tem.NgayDK + "</td>";
            outHTML += "<td>" + tem.SoGiayCN + "</td>";
            outHTML += "<td>" + tem.NgayCN + "</td>";
            outHTML += "<td>" + tem.NguoiDaiDien_Ten + "</td>";
            outHTML += "<td>" + "'" + tem.NguoiDaiDien_mobile + "</td>";
            outHTML += "<td>" + tem.NguoiDaiDien_Email + "</td>";
            outHTML += "<td>" + tem.NguoiDaiDienLL_Ten + "</td>";
            outHTML += "<td>" + "'" + tem.NguoiDaiDienLL_mobile + "</td>";
            outHTML += "<td>" + tem.NguoiDaiDienLL_Email + "</td>";
            outHTML += "<td>" + tem.SoQD + "</td>";
            outHTML += "<td>" + tem.NgayKyQD + "</td>";
            outHTML += "</tr>";
        }

        outHTML += "<tr>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td align='left'><b> Tổng số: " + i.ToString() + "</b></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "</tr>";

        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='17' style='border-style:none'></td>";
        outHTML += "<td colspan='6' align='center' style='border-style:none'><b>" + NgayBC + "</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='17' style='border-style:none'></td>";
        outHTML += "<td colspan='6' align='center' style='border-style:none'><b>NGƯỜI LẬP BIỂU</b></td>";
        outHTML += "</tr>";

        outHTML += "</table>";

        Library.ExportToExcel(strName, outHTML);
    }
}