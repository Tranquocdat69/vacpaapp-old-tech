﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BC_QuyetDinhKetNapHoiVienTapThe : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quyết định kết nạp hội viên tổ chức";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim() == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Contains(tem.idDoiTuong.ToString()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }


    
    public void setdataCrystalReport()
    {
        string VungMien = Request.Form["VungMienId"];
        //string Tinh = Request.Form["TinhId"];
        string TinhId = Request.Form["TinhId"];
        DateTime? NgayBatDau = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayBatDau"]))
            NgayBatDau = DateTime.ParseExact(Request.Form["NgayBatDau"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime? NgayKetThuc = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayKetThuc"]))
            NgayKetThuc = DateTime.ParseExact(Request.Form["NgayKetThuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime? NgayQuyetDinh = null;
        //if (!string.IsNullOrEmpty(Request.Form["NgayQuyetDinh"]))
        //    NgayQuyetDinh = DateTime.ParseExact(Request.Form["NgayQuyetDinh"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);


        //if (NgayBatDau != null && NgayKetThuc != null && NgayQuyetDinh != null)
        string SoQD = "";

        string result = "";
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            result = string.Join(",", lstIdChon);
        }
        if (!string.IsNullOrEmpty(result))
        {
            List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
            List<objThongTinDoanhNghiep_CT> lst = new List<objThongTinDoanhNghiep_CT>();


            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select SoQuyetDinhKetNap, SoHieu, TenDoanhNghiep, TenVietTat, a.ngayquyetdinhketnap from tblHoiVienTapThe a "
                                + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID "
                                + " where MaHoiVienTapThe = '" + result + "'";

            if (!string.IsNullOrEmpty(VungMien))
                sql.CommandText = sql.CommandText + " AND b.VungMienID in (" + VungMien + ")";

            if (TinhId != "0" && TinhId != "-1")
                sql.CommandText = sql.CommandText + " AND a.DiaChi_TinhID = " + Convert.ToInt32(TinhId);

           // string strNgay = NgayQuyetDinh.Value.Year + "-" + NgayQuyetDinh.Value.Month + "-" + NgayQuyetDinh.Value.Day;
            //if (NgayQuyetDinh != null)
            //    sql.Parameters.AddWithValue("@NgayQuyetDinh", strNgay);
            //else
            //    sql.Parameters.AddWithValue("@NgayQuyetDinh", DBNull.Value);
            //if (NgayKetThuc != null)
            //    sql.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);
            //else
            //    sql.Parameters.AddWithValue("@NgayKetThuc", DBNull.Value);

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();
                new1.STT = i.ToString();
                new1.SoHieu = tem["SoHieu"].ToString();
                new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
                new1.TenVietTat = tem["TenVietTat"].ToString();
                SoQD = tem["SoQuyetDinhKetNap"].ToString();
                lst.Add(new1);

                if (!string.IsNullOrEmpty(tem["ngayquyetdinhketnap"].ToString()))
                    NgayQuyetDinh = Convert.ToDateTime(tem["ngayquyetdinhketnap"]);
            }

            TenBaoCao newabc = new TenBaoCao();
            newabc.SoLuong = i.ToString();
            newabc.TenBC = SoQD;
            newabc.TongCong = "Tổng cộng: " + i.ToString() + " Hội viên tổ chức";
            try{
            newabc.NgayTinh = (NgayQuyetDinh.Value.Day.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Day : NgayQuyetDinh.Value.Day.ToString()) + "/" + (NgayQuyetDinh.Value.Month.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Month : NgayQuyetDinh.Value.Month.ToString()) + "/" + NgayQuyetDinh.Value.Year;
            newabc.ngayBC = "Hà Nội, ngày " + (NgayQuyetDinh.Value.Day.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Day : NgayQuyetDinh.Value.Day.ToString()) + " tháng " + (NgayQuyetDinh.Value.Month.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Month : NgayQuyetDinh.Value.Month.ToString()) + " năm " + NgayQuyetDinh.Value.Year;
            }
            catch {
                newabc.ngayBC = "Hà Nội, ngày....tháng....năm....";
            }
            lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/QuyetDinhKetNapHoiVienTapThe.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "QuyetDinhKetNapHoiVienTapThe");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "QuyetDinhKetNapHoiVienTapThe");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            
        }
    }

    protected void btnChay2_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            idDanhSach.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + lstIdChon[0] + "'";// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtCongTy.Text = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }
}