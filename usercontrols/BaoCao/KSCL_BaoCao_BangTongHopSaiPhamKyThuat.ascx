﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_BangTongHopSaiPhamKyThuat.ascx.cs" Inherits="usercontrols_KSCL_BaoCao_BangTongHopSaiPhamKyThuat" %>

<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Bảng tổng hợp sai phạm hệ kỹ thuật</h4>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td>
                Năm<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlNam" name="ddlNam" onchange="CallActionGetDanhSachCongTy();">
                    <% GetListYear();%>
                </select>
            </td>
        </tr>
        <tr>
            <td style="min-width: 120px;">
                Công ty kiểm toán:
            </td>
            <td>
                <div style="width: 100%; height: 200px; overflow: auto;">
                    <table id="tblDanhSachCongTy" style="width: 100%; min-width: 800px;" border="0" class="formtbl">
                        <tbody id="TBodyChonTatCa">
                            <tr>
                                <td style="width: 30px; text-align: center;">
                                    <input type="checkbox" class="checkall" id="cboCheckAll" name="cboCheckAll" value="all" />
                                </td>
                                <td>
                                    Tất cả
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('excel');"><i class="iconsweets-excel2"></i>Kết xuất Excel</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Bảng tổng hợp sai phạm kỹ thuật</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" ShowAllPageIds="True"
        HasExportButton="False" HasPrintButton="False" AutoDataBind="True" />
</fieldset>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    function CallActionGetDanhSachCongTy() {
        iframeProcess.location = '/iframe.aspx?page=KSCL_BaoCao_Proccess&action=loaddscthtsp&nam=' + $('#ddlNam option:selected').val();
    }
    
    <% SetDataJavascript();%>

    function DrawData_DanhSachCongTy(arrData) {
        var tbl = document.getElementById('tblDanhSachCongTy');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        $('#cboCheckAll').prop('checked', false);
        if(arrData.length > 0) {
            document.getElementById('TBodyChonTatCa').style.display = '';
            if(flagCheckAll)
                $('#cboCheckAll').prop('checked', true);
            for(var i = 0; i < arrData.length; i ++) {
                var idCongTy = arrData[i][0];
                var tenCongTy = arrData[i][1];
                
                var tr = document.createElement('TR');
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                if(arrCongTy.indexOf(idCongTy) > -1)
                    tdCheckBox.innerHTML = "<input type='checkbox' id='cboCongTy_"+idCongTy+"' name='cboCongTy_"+idCongTy+"' value='"+tenCongTy+"' checked='checked' />";
                else
                    tdCheckBox.innerHTML = "<input type='checkbox' id='cboCongTy_"+idCongTy+"' name='cboCongTy_"+idCongTy+"' value='"+tenCongTy+"' />";
                tr.appendChild(tdCheckBox);
               

                var arrColumn = [tenCongTy];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        } else {
            document.getElementById('TBodyChonTatCa').style.display = 'none';
            var tr = document.createElement('TR');
            var td = document.createElement("TD");
            td.colspan = 2;
            td.innerHTML = "<span style='color: red;'>Không có công ty được kiểm tra trong năm này!</span>";
            tr.appendChild(td); 
            tbl.appendChild(tr);
        }
    }
    
    jQuery(document).ready(function () {
        $("#tblDanhSachCongTy .checkall").bind("click", function () {
            var  checked = $(this).prop("checked");
            $('#tblDanhSachCongTy :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });

    function Export(type) {
        // Kiểm tra bắt phải chọn tối thiểu 1 công ty
        var flag = false;
        $('#tblDanhSachCongTy :checkbox').each(function () {
               if($(this).prop("checked"))
                   flag = true;
            });
        if(flag) {
            $('#hdAction').val(type);
            $('#Form1').submit();   
        } else {
            alert('Phải chọn tối thiểu 1 công ty để thực hiện tổng hợp số liệu!');
            return false;
        }
    }

    
</script>
<div id="DivDanhSachDoanKiemTra">
</div>
<div id="DivDanhSachCongTy">
</div>
