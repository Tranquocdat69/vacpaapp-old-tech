﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BC_DanhSachKhenThuongHoiVienTapThe : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách khen thưởng hội viên tổ chức";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim() == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Contains(tem.idDoiTuong.ToString()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void DonVi(string ma = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (ma == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }
    
    public void setdataCrystalReport()
    {
        string VungMien = Request.Form["VungMienId"];
        string Tinh = Request.Form["TinhId"];
        //string DonViId = Request.Form["DonViId"];
        DateTime? NgayBatDau = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayBatDau"]))
            NgayBatDau = DateTime.ParseExact(Request.Form["NgayBatDau"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime? NgayKetThuc = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayKetThuc"]))
            NgayKetThuc = DateTime.ParseExact(Request.Form["NgayKetThuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        //DateTime? NgayQuyetDinh = null;
        //if (!string.IsNullOrEmpty(Request.Form["NgayQuyetDinh"]))
        //    NgayQuyetDinh = DateTime.ParseExact(Request.Form["NgayQuyetDinh"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);


        //if (NgayBatDau != null && NgayKetThuc != null)
        {
            List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
            List<objThongTinDoanhNghiep_CT> lst = new List<objThongTinDoanhNghiep_CT>();


            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep, TenVietTat, SoGiayChungNhanKDDVKT, "
                            + " NgayCapGiayChungNhanKDDVKT, NguoiDaiDienPL_Ten, a.NgayThang, TenHinhThucKhenThuong, a.LyDo "
                            + " from  dbo.tblKhenThuongKyLuat a inner join dbo.tblHoiVienTapThe b "
                            + " on a.HoiVienTapTheID = b.HoiVienTapTheID "
                            + " inner join dbo.tblDMTinh c on b.DiaChi_TinhID = c.MaTinh "
                            + " inner join dbo.tblDMHinhThucKhenThuong d on a.HinhThucKhenThuongID = d.HinhThucKhenThuongID "
                            + " where 1=1 ";

            if (!string.IsNullOrEmpty(VungMien))
                sql.CommandText = sql.CommandText + " AND c.VungMienID in (" + VungMien + ")";

            if (Tinh != "0" && Tinh != "-1")
                sql.CommandText = sql.CommandText + " AND b.DiaChi_TinhID = '" + Tinh + "'";

            if (NgayBatDau != null && NgayKetThuc != null)
                sql.CommandText = sql.CommandText + " AND a.NgayThang >= @NgayBatDau and a.NgayThang <= @NgayKetThuc";
            //string strNgay = NgayQuyetDinh.Value.Year + "-" + NgayQuyetDinh.Value.Month + "-" + NgayQuyetDinh.Value.Day;
            if (NgayBatDau != null)
                sql.Parameters.AddWithValue("@NgayBatDau", NgayBatDau);
            else
                sql.Parameters.AddWithValue("@NgayBatDau", DBNull.Value);
            if (NgayKetThuc != null)
                sql.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);
            else
                sql.Parameters.AddWithValue("@NgayKetThuc", DBNull.Value);

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();
                new1.STT = i.ToString();
                new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
                new1.TenVietTat = tem["TenVietTat"].ToString();
                new1.SoGiayCN = tem["SoGiayChungNhanKDDVKT"].ToString();
                new1.NgayCN = !string.IsNullOrEmpty(tem["NgayCapGiayChungNhanKDDVKT"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanKDDVKT"])) : "";
                new1.NguoiDaiDien_Ten = tem["NguoiDaiDienPL_Ten"].ToString();
                new1.NgayKyQD = !string.IsNullOrEmpty(tem["NgayThang"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayThang"])) : "";
                new1.SoQD = tem["TenHinhThucKhenThuong"].ToString();
                new1.NguoiDaiDien_Email = tem["LyDo"].ToString();
                
                lst.Add(new1);

            }

            TenBaoCao newabc = new TenBaoCao();
            newabc.SoLuong = i.ToString();
            //newabc.TenBC = 
            newabc.TongCong = "Tổng cộng: " + i.ToString() + " Hội viên tổ chức";
            if (NgayBatDau != null && NgayKetThuc != null)
                newabc.NgayTinh = "(Từ ngày " + (NgayBatDau.Value.Day.ToString().Length == 1 ? "0" + NgayBatDau.Value.Day : NgayBatDau.Value.Day.ToString()) + "/" + (NgayBatDau.Value.Month.ToString().Length == 1 ? "0" + NgayBatDau.Value.Month : NgayBatDau.Value.Month.ToString()) + "/" + NgayBatDau.Value.Year + " đến ngày " + (NgayKetThuc.Value.Day.ToString().Length == 1 ? "0" + NgayKetThuc.Value.Day : NgayKetThuc.Value.Day.ToString()) + "/" + (NgayKetThuc.Value.Month.ToString().Length == 1 ? "0" + NgayKetThuc.Value.Month : NgayKetThuc.Value.Month.ToString()) + "/" + NgayKetThuc.Value.Year + ")";
            else
                newabc.NgayTinh = "(Tính đến thời điểm hiện tại)";
            newabc.ngayBC = "Hà Nội, ngày " + (DateTime.Now.Day.ToString().Length == 1 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + " tháng " + (DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + " năm " + DateTime.Now.Year;
            lstTenBC.Add(newabc);

            List<NguoiLapBieu> lstNguoiLapBieu = new List<NguoiLapBieu>();
            NguoiLapBieu nlb = new NguoiLapBieu();
            nlb.nguoiLapBieu = cm.Admin_HoVaTen;
            lstNguoiLapBieu.Add(nlb);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/DanhSachKhenThuongHoiVienTapThe.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            rpt.Database.Tables["NguoiLapBieu"].SetDataSource(lstNguoiLapBieu);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachKhenThuongHoiVienTapThe");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachKhenThuongHoiVienTapThe");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            
        }
    }
   
}