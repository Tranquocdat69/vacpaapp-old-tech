﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra.ascx.cs" Inherits="usercontrols_KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra" %>

<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Bảng tình hình nộp báo cáo tự kiểm tra</h4>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td>
                Năm<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlNam" name="ddlNam">
                    <% GetListYear();%>
                </select>
            </td>
        </tr>
        <tr>
            <td>Tình trạng<span class="starRequired">(*)</span>:</td>
            <td>
                <input type="checkbox" id="cboDaNopBaoCao" name="cboDaNopBaoCao" checked="checked"/>&nbsp;Đã nộp báo cáo&nbsp;&nbsp;&nbsp;
                <input type="checkbox" id="cboChuaNopBaoCao" name="cboChuaNopBaoCao" checked="checked"/>&nbsp;Chưa nộp báo cáo
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('word');"><i class="iconsweets-word2"></i>Kết xuất Word</a> 
                        <a id="A1" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('excel');"><i class="iconsweets-excel2"></i>Kết xuất Excel</a> 
                        <a id="A3" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('pdf');"><i class="iconsweets-pdf2"></i>Kết xuất ODF</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Bảng tình hình nộp báo cáo tự kiểm tra</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" 
        ShowAllPageIds="True" HasExportButton="False" HasPrintButton="False" />
</fieldset>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    function Export(type) {
        if(!$('#cboDaNopBaoCao').prop('checked') && !$('#cboChuaNopBaoCao').prop('checked')) {
            alert('Phải chọn ít nhất 1 tình trạng để lọc dữ liệu báo cáo!');
            return;
        }
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
    
    <% SetDataJavascript();%>
</script>
<div id="DivDanhSach">
</div>