﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT.ascx.cs" Inherits="usercontrols_BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT" %>

<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Báo cáo tổng hợp kết quả tổ chức lớp học cập nhật kiến thức kiểm toán viên</h4>
<div id="thongbaoloi_form_ingcn" name="thongbaoloi_form_ingcn" style="display: none;
    margin-top: 5px;" class="alert alert-error">
</div>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="500px" border="0" class="formtbl" style="margin: 0px auto;">
        <tr>
            <td>
                Năm<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlNam" name="ddlNam">
                    <% GetListYear();%>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('word')"><i class="iconsweets-word2"></i>Kết xuất Word</a>
                <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="Export('pdf')"><i
                    class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Báo cáo tổng hợp kết quả tổ chức lớp học cập nhật kiến thức kiểm toán viên</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" 
        HasExportButton="False" HasPrintButton="False" />
</fieldset>
</form>
<script type="text/javascript">
    function Export(type) {
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
    
    <% SetDataJavascript();%>
</script>
