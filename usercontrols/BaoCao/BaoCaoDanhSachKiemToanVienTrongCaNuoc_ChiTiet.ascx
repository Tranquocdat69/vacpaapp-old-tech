﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet.ascx.cs" Inherits="usercontrols_BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>


<style type="text/css">
    /*.ui-datepicker
    {
        z-index: 1003 !important;
    }*/
    .ui-dropdownchecklist {
          z-index: 99999 !important;
        background: #ffffff !important;
        border: solid 1PX #cccccc!important;
        margin: 0px;
  height: 22px;
  width:270px;
  -webkit-appearance: menulist;
  box-sizing: border-box;
  border: 1px solid;
  border-image-source: initial;
  border-image-slice: initial;
  border-image-width: initial;
  border-image-outset: initial;
  border-image-repeat: initial;
  white-space: pre;
  -webkit-rtl-ordering: logical;
  color: black;
  background-color: white;
  cursor: default;
    }
    .ui-dropdownchecklist span {
        width:100%;
    }
    .ui-dropdownchecklist span span{
        width:100%;
    }
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript" src="js/ui.dropdownchecklist-1.5-min.js"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<%--<script type="text/javascript" src="js/jquery-ui.min.js"></script>--%>

<form id="Form1" runat="server" clientidmode="Static">
<asp:HiddenField ID="hi_LoaiHinh" runat="server" />
<asp:HiddenField ID="hi_TinhThanh" runat="server" />
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<h4 class="widgettitle">
    Danh sách kiểm toán viên trong cả nước (chi tiết)</h4>
<fieldset class="fsBlockInfor" style="margin-top: 0px;">
    <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
    
    <table width="100%" border="0" class="formtbl">
        <tr>
               <td style="width:152px"><asp:Label ID="lblLoaiHinhDoanhnghiep" runat="server" Text ='Tên công ty:' ></asp:Label></td>
          <td>
          <select name="TenCongTy" id="TenCongTy" multiple="multiple">
           <% 
                            try
                            {
                                LoadTenCongTy(Request.Form["TenCongTy"].ToString());
                            }
                            catch
                            {
                                LoadTenCongTy();
                            }
                        %>
          </select>
          </td>
         <td  colspan="2" style="width:152px"><asp:Label ID="Label19" runat="server" Text ='Chứng chỉ quốc tế:' ></asp:Label></td>
          <td colspan="2">
          <select name="ChungChiQuocTe" id="ChungChiQuocTe" multiple="multiple">
          <%  
              try
              {
                  LoadChungChiQuocTe(Request.Form["ChungChiQuocTe"].ToString());
              }
              catch
              {
                  LoadChungChiQuocTe();
              }
              %>
          </select>
          </td>
        </tr>
            <tr>
               <td style="width:152px"><asp:Label ID="Label16" runat="server" Text ='Vùng miền:' ></asp:Label></td>
          <td>
          <select name="VungMien" id="VungMien" multiple="multiple">
          <%  
              try
              {
                  LoadVungMien(Request.Form["VungMien"].ToString());
              }
              catch
              {
                  LoadVungMien();
              }
              %>
          </select>
          </td>
          <td  colspan="2" style="width:152px"><asp:Label ID="Label15" runat="server" Text ='Tỉnh/Thành:' ></asp:Label></td>
          <td colspan="2">
          <select name="TinhThanh" id="TinhThanh" multiple="multiple">
            <%  
                try
                {
                    LoadTinhThanh(Request.Form["VungMien"].ToString(), Request.Form["TinhThanh"].ToString());
                }
                catch
                {
                    LoadTinhThanh("", "");
                }
                        %>
          </select>
          </td>
        </tr>
     
    <tr>
        <td style="width:152px"><asp:Label runat ="server" ID ="Label3" Text ="Ngày sinh:"> </asp:Label></td>
        <td  style ="width:250px"><input type ="text" id ="ngaysinhtu" name="ngaysinhtu" value="<%=Request.Form["ngaysinhtu"]%>"/></td>
        <td style="width:20px"><asp:Label runat ="server" ID ="Label5" Text ="-"> </asp:Label></td>
        <td style ="width:250px"><input type ="text" id ="ngaysinhden" name="ngaysinhden" value="<%=Request.Form["ngaysinhden"]%>"/></td>
        
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
         <td style="width:152px"><asp:Label runat ="server" ID ="Label2" Text ="Độ tuổi:"> </asp:Label></td>
        <td style ="width:250px"><input type ="text" id ="dotuoitu" name="dotuoitu" value="<%=Request.Form["dotuoitu"]%>"class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="200"></td>
        <td style="width:20px"><asp:Label runat ="server" ID ="Label4" Text ="-"> </asp:Label></td>
        <td style ="width:250px"><input type ="text" id ="dotuoiden" name="dotuoiden" value="<%=Request.Form["dotuoiden"]%>"class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="200"></td>
        <td colspan="2">&nbsp;</td>
    </tr>
      <tr>
       <td style="width:152px"><asp:Label ID="Label1" runat="server" Text ='Chức danh:' ></asp:Label></td>
          <td>
          <select name="ChucDanh" id="ChucDanh" multiple="multiple">
          <%
              try
              {
                  LoadChucDanh(Request.Form["ChucDanh"].ToString());
              }
              catch (Exception)
              {

                  LoadChucDanh();
              }
                  
              %>
          </select>
          </td>
         <td colspan="4"><asp:Label ID="Label6" runat="server"></asp:Label>
          <label><input type="checkbox" name="danghanhnghe" id="danghanhnghe" checked="checked"  class="input-xlarge" />Đang hành nghề</label>
             <label><input type="checkbox" name="khonghanhnghe" id="khonghanhnghe" checked="checked"  class="input-xlarge" />Không hành nghề</label>
          </td>
    </tr>  
    </table>

    <div class="dataTables_length" style="text-align: center;">
        <a id="btnSave" href="javascript:;" class="btn" onclick="View();"><i class="iconfa-plus-sign">
             </i>Xem thông tin</a> 
        
        <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="View('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a>
                 <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="View('pdf');">
                    <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                    <a id="A4" href="javascript:;" class="btn btn-rounded" onclick="View('excel');">
                <i class="iconsweets-excel2"></i>Kết xuất Excel</a>
                    <input type="hidden" id="hdAction" name="hdAction"/>
    </div>
</fieldset>

 <fieldset class="fsBlockInfor">
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False" HasExportButton="False" HasPrintButton="False"/>
    </fieldset>

    
</form>

<script type="text/javascript">



    //        $(document).ready(function () {
    //            $("#NgayBatDau").datepicker();
    //            $("#NgayKetThuc").datepicker();

    //            $(".NgayBatDau").datepicker({
    //                dateFormat: 'mm-dd-yy'
    //            }).datepicker('setDate', new Date())

    $(function () {
        $('#ngaysinhtu, #ngaysinhden').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-200:+0',
            onClose: function (dateText, inst) {
                $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
            }
        }).datepicker('option', '', '');

        $('#ngaybaocao').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-10:+10',
            onClose: function (dateText, inst) {
                $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
            }
        }).datepicker('option', '', '');
        $('#ngaybaocao').datepicker('setDate', new Date());
    });


    $(function () { $('#ngaysinhden').datepicker('option', 'minDate', $('#ngaysinhtu').val()) });
    $('#ngaysinhtu').change(function () {

        $('#ngaysinhden').datepicker('option', 'minDate', $('#ngaysinhtu').val());
    });

    //  });

 </script>

 <script type="text/javascript">


     var prm = Sys.WebForms.PageRequestManager.getInstance();

     prm.add_endRequest(function () {
         $('#NgayBatDau, #NgayKetThuc').datepicker({
             changeMonth: true,
             changeYear: true,
             dateFormat: 'dd/mm/yy',
             onClose: function (dateText, inst) {
                 $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
             }
         }).datepicker('option', '', '');

         $('#NgayBatDau').datepicker('setDate', new Date());
         $('#NgayKetThuc').datepicker('setDate', new Date());
     });

        </script>

<%--gọi function in bao cao--%>
<script type="text/javascript">
    function View(type) {
        $('#hdAction').val(type);
        $('#Form1').submit();
    }

    </script>
    <script type="text/javascript">

        $("#TenCongTy").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
        $("#ChungChiQuocTe").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
        $("#VungMien").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
        $("#ChucDanh").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
        $("#TinhThanh").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
    <% LoadCheckBox();%>

        var timestamp = Number(new Date());
        $('#VungMien').change(function () {
            $('#TinhThanh').dropdownchecklist('destroy');
            var VungMien = $('#VungMien').val();
            var IdVungMien = "";
            if (VungMien != null)
                IdVungMien = VungMien.join(',');
            $.ajax({
                url: "noframe.aspx",
                method: "GET",
                data: {
                    'page': 'chondiaphuong',
                    'default': 0,
                    'type': 'tt',
                    'id_vungmien': IdVungMien,
                    'time': timestamp
                },
                dataType: "html",
                success: function (data) {
                    $('#TinhThanh').html(data);
                    $("#TinhThanh").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
                }
            });
        });

        jQuery(function ($) {
            $('.auto').autoNumeric('init');
            // $('#txtTyLe>').autoNumeric('init');
        });
</script>
