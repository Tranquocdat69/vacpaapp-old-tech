﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_BaoCao_getsovaoso_ajax : System.Web.UI.Page
{
    Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            string sql = "SELECT RIGHT('000' + CONVERT(VARCHAR(3), X.RowNum), 3) FROM (SELECT ROW_NUMBER() OVER (ORDER BY tblHoiVienTapThe.SoHieu, B.SoChungChiKTV) AS RowNum, A.HoiVienCaNhanID, B.HoDem + ' ' + B.Ten AS HoTen FROM tblCNKTDangKyHocCaNhan A LEFT JOIN tblHoiVienCaNhan B ON A.HoiVienCaNhanID = B.HoiVienCaNhanID LEFT JOIN tblHoiVienTapThe ON B.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID WHERE A.LopHocID = " + Request.QueryString["lophocid"] + " AND (B.LoaiHoiVienCaNhan = 1 OR B.LoaiHoiVienCaNhan = 2)) AS X WHERE X.HoiVienCaNhanID = " + Request.QueryString["hocvienid"];
            SqlCommand cmd = new SqlCommand(sql);
            cm.write_ajaxvar("SoThuTu", DataAccess.DLookup(cmd));

            cmd.CommandText = "select RIGHT('0000' + CONVERT(VARCHAR(4),X.RowNum), 4) from (select ROW_NUMBER() OVER (ORDER BY LopHocID, tblHoiVienTapThe.SoHieu, B.SoChungChiKTV) AS RowNum, A.LopHocID, A.HoiVienCaNhanID, B.HoDem + ' ' + B.Ten AS HoTen from tblCNKTDangKyHocCaNhan A LEFT JOIN tblHoiVienCaNhan B ON A.HoiVienCaNhanID = B.HoiVienCaNhanID LEFT JOIN tblHoiVienTapThe ON B.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID WHERE (B.LoaiHoiVienCaNhan = 1 OR B.LoaiHoiVienCaNhan = 2)) AS X WHERE X.LopHocID = " + Request.QueryString["lophocid"] + " AND X.HoiVienCaNhanID = " + Request.QueryString["hocvienid"];

            cm.write_ajaxvar("SoLienTuc", DataAccess.DLookup(cmd));
            
        }
        catch (Exception ex)
        {
            cm.write_ajaxvar("SoLienTuc", "");
            cm.write_ajaxvar("SoThuTu", "");
        }
    }
}