﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_BangCauHoiKiemTraHeThong.ascx.cs" Inherits="usercontrols_KSCL_BaoCao_BangCauHoiKiemTraHeThong" %>

<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Bảng câu hỏi kiểm tra hệ thống</h4>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td colspan="2" style="text-align: center;">
                <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('excel');"><i class="iconsweets-excel2"></i>Kết xuất Excel</a> 
                        <a id="A1" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('pdf');"><i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Bảng câu hỏi kiểm tra hệ thống</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" 
        ShowAllPageIds="True" HasExportButton="False" HasPrintButton="False" 
        AutoDataBind="True" />
</fieldset>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">

    function Export(type) {
        $('#hdAction').val(type);
        $('#Form1').submit();
    }

//    function Paging() {
//        alert('123');
//        
//        $('#ctl00_maincontent_ctl00_CrystalReportViewer1 iframe').each(function (index, objIframe) {
//            var id = $(objIframe).prop('id');
//            if(index == 0) {
//                alert('456');
//                //alert($(objIframe).contents().find('.crystalstyle').prop('id'));
//                $(objIframe).contents().find('.crystalstyle').each(function (indexSub, objDiv) {
//                    alert(indexSub);
//                });
//            }
//        });
//    }
//    setTimeout('Paging()', 2000);
    <% SetDataJavascript();%>
</script>
