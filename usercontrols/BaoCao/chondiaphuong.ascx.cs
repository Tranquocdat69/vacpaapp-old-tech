﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using HiPT.VACPA.DL;



   

public partial class usercontrols_chondiaphuong : System.Web.UI.UserControl
{

    Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        

        string type = Request.QueryString["type"];
        switch (type)
        {
            case "tp":
                dmdiaphuong.Controls.Add(new LiteralControl(Load_ThanhPho()));
                break;
            case "qh":
                    string id_thanhpho = Request.QueryString["id_thanhpho"];
                    dmdiaphuong.Controls.Add(new LiteralControl(Load_QuanHuyen("000", id_thanhpho)));                
                break;
            case "px":
                string id_quanhuyen = Request.QueryString["id_quanhuyen"];
                dmdiaphuong.Controls.Add(new LiteralControl(Load_PhuongXa("00000", id_quanhuyen)));
                break;
            case "tt":
                string id_vungmien = Request.QueryString["id_vungmien"];
                dmdiaphuong.Controls.Add(new LiteralControl(Load_TinhThanh("00", id_vungmien)));
                break;
        }

    }


    public string Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";

        DataSet ds = new DataSet();

      
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ORDER BY TenTinh ASC"; ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
        

        DataTable dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }

       
        ds = null;
        dt = null;

        return output_html;
    }

    public string Load_TinhThanh(string MaTinh = "00", string strIdVungMien = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        // sql.CommandText = "SELECT TinhID, TenTinh FROM tblDMTinh where VungMienID in (" + VungMienID + ")";
        sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where 0 = 0 ";
        if (strIdVungMien != null)
        {
            if (strIdVungMien != "000" && strIdVungMien != "" && strIdVungMien != "-1")
                sql.CommandText += " AND VungMIenID in (" + strIdVungMien + ")";
        }

        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = @"<option value=-1><< Tất cả >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            output_html += @"<option value=""" + dtr["MaTinh"] + @""">" + dtr["TenTinh"] + "</option>";

        }

        ds = null;
        dt = null;

        return output_html;
    }

    public string Load_QuanHuyen(string MaHuyen = "000", string MaTinh = "00")
    {
        string output_html = "";

        DataSet ds = new DataSet();

       
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT MaHuyen,TenHuyen FROM tblDMHuyen WHERE (MaTinh='" + MaTinh + "') AND HieuLuc='1' ORDER BY TenHuyen ASC";
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
        
        DataTable dt = ds.Tables[0];

        output_html += @"<option value=""""><< Lựa chọn >></option>";
        foreach (DataRow Huyen in dt.Rows)
        {
           


            if (MaHuyen == Huyen["MaHuyen"].ToString().Trim())
            {
                output_html += @"
                     <option  selected=""selected"" value=""" + Huyen["MaHuyen"].ToString().Trim() + @""">" + Huyen["TenHuyen"] + @"</option>";
            }
            else
            {
                output_html += @"
                    <option  value=""" + Huyen["MaHuyen"].ToString().Trim() + @""">" + Huyen["TenHuyen"] + @"</option>";
            }

            
        }

      
        ds = null;
        dt = null;

        return output_html;
    }


    public string Load_PhuongXa(string MaXa = "00000", string MaHuyen = "000")
    {
        string output_html = "";

        DataSet ds = new DataSet();

       
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT MaXa,TenXa FROM tblDMXa WHERE (MaHuyen='" + MaHuyen + "') ORDER BY TenXa ASC";
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
        
        DataTable dt = ds.Tables[0];

        output_html += @"<option value=""""><< Lựa chọn >></option>";
        foreach (DataRow Xa in dt.Rows)
        {

            if (MaXa == Xa["MaXa"].ToString().Trim())
            {
                output_html += @"
                     <option  selected=""selected"" value=""" + Xa["MaXa"].ToString().Trim() + @""">" + Xa["TenXa"] + @"</option>";
            }
            else
            {
                output_html += @"
                    <option  value=""" + Xa["MaXa"].ToString().Trim() + @""">" + Xa["TenXa"] + @"</option>";
            }

        }

       
        ds = null;
        dt = null;


        return output_html;
    }


}