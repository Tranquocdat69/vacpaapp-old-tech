﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Text.RegularExpressions;

public partial class usercontrols_BaoCaoDanhSachCongTyKiemToanTrongCaNuoc : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách công ty kiểm toán trong cả nước";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            if (this.IsPostBack)
            {
                setdataCrystalReport();
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void LoadLoaiHinhDoanhNghiep(string Id = "")
    {
        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT LoaiHinhDoanhNghiepID, TenLoaiHinhDoanhNghiep FROM tblDMLoaiHinhDoanhNghiep";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["LoaiHinhDoanhNghiepID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["LoaiHinhDoanhNghiepID"] + @""">" + dtr["TenLoaiHinhDoanhNghiep"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["LoaiHinhDoanhNghiepID"] + @""">" + dtr["TenLoaiHinhDoanhNghiep"] + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }
    public void LoadVungMien(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT VungMienID, TenVungMien FROM tblDMVungMien";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["VungMienID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["VungMienID"] + @""">" +
                               dtr["TenVungMien"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["VungMienID"] + @""">" + dtr["TenVungMien"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadTinhThanh(string VungMienId = "", string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where TinhID in (" + Id + ")";
        sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where 0 = 0 ";
        if(VungMienId != "")
            sql.CommandText += " AND VungMienID In ("+ VungMienId +")";
        //if (Id != "")
        //    sql.CommandText += " AND MaTinh In (" + Id + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["MaTinh"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["MaTinh"] + @""">" +
                               dtr["TenTinh"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["MaTinh"] + @""">" + dtr["TenTinh"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);

       }

    public void setdataCrystalReport()
    {
        List<objDanhSachCongTyKiemToanTrongCaNuoc> lst = new List<objDanhSachCongTyKiemToanTrongCaNuoc>();
        string LoaiHinhDoanhNghiep = Request.Form["LoaiHinhDoanhNghiep"];
        string VungMien = Request.Form["VungMien"];
        string TinhThanh = Request.Form["TinhThanh"];
        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();

        //Lấy danh sách hội viên tổ chức, vùng miền, số lượng hội viên cá nhân where theo điều kiện lọc
        sql.CommandText = @"select C.*, CASE ISNULL(C.HoiVienTapTheChaID,0) WHEN 0 THEN C.SoHieu ELSE h.SoHieu END AS SoHieuOrder, E.VungMienID from tblHoiVienTapThe as C left join tblHoiVienTapThe h on C.HoiVienTapTheChaID = h.HoiVienTapTheID 
                                join
                                (
                                select A.HoiVienTapTheID, COUNT(B.HoiVienCaNhanID)as TongSoHoiVien from tblHoiVienTapThe A
                                left join tblHoiVienCaNhan B
                                on A.HoiVienTapTheID = B.HoiVienTapTheID
                                group by A.HoiVienTapTheID ) as D
                                on C.HoiVienTapTheID = D.HoiVienTapTheID
                                left join tblDMTinh E
                                on C.DiaChi_TinhID = E.MaTinh
                                left join tblDMVungMien F
                                on F.VungMienID = E.VungMienID
                                where ( 0=0 ";

        //Lọc theo loại hình được chọn với loại hình khác tất cả
        if (!string.IsNullOrEmpty(LoaiHinhDoanhNghiep) && !LoaiHinhDoanhNghiep.Contains("-1"))
            sql.CommandText += " AND (LoaiHinhDoanhNghiepID in (" + LoaiHinhDoanhNghiep + "))";
        //Lọc theo Tỉnh thành được chọn với loại hình khác tất cả
        if (!string.IsNullOrEmpty(TinhThanh) && !TinhThanh.Contains("-1"))
            sql.CommandText += " AND DiaChi_TinhID in (" + TinhThanh + ")";
        else if (TinhThanh == "-1")
        {
            //Lọc theo Vùng miền được chọn với Tỉnh bằng tất cả và vùng miền khác tất cả
            if (!string.IsNullOrEmpty(VungMien) && !VungMien.Contains("-1"))
                sql.CommandText += " AND E.VungMienID in (" + VungMien + ")";
        }
        //Nếu checkall hoặc uncheckall thì ko lọc
        if ((Request.Form["tinhtrang_danghoatdong"] == null && Request.Form["tinhtrang_dunghoatdong"] == null)
            || (Request.Form["tinhtrang_danghoatdong"] != null && Request.Form["tinhtrang_dunghoatdong"] != null))
        {
        }
        else
        {
            //Nếu không check đang hoạt động => lọc bỏ tình trạng đang hoạt động
            if (Request.Form["tinhtrang_danghoatdong"] == null)
            {
                sql.CommandText += " AND (TinhTrangHoiVienId <> 1) ";
            }
            //Nếu không check dừng hoạt động => lọc bỏ tình trạng dừng hoạt động
            if (Request.Form["tinhtrang_dunghoatdong"] == null)
            {
                sql.CommandText += " AND (TinhTrangHoiVienId <> 0) ";
            }
        }

        //Xử lý đối tượng
        //Nếu checkall hoặc uncheckall thì không lọc
        if ((Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] == null &&
             Request.Form["doituong_dudieukienkitcongchungkhac"] == null &&
             Request.Form["doituong_khongdudieukienkit"] == null)
            ||
            (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null &&
             Request.Form["doituong_dudieukienkitcongchungkhac"] != null &&
             Request.Form["doituong_khongdudieukienkit"] != null))
        {
        }
        else
        {
            //Nếu check một trong ba
            if ((Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null &&
                 Request.Form["doituong_dudieukienkitcongchungkhac"] == null &&
                 Request.Form["doituong_khongdudieukienkit"] == null)
                ||
                (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] == null &&
                 Request.Form["doituong_dudieukienkitcongchungkhac"] != null &&
                 Request.Form["doituong_khongdudieukienkit"] == null)
                ||
                (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] == null &&
                 Request.Form["doituong_dudieukienkitcongchungkhac"] == null &&
                 Request.Form["doituong_khongdudieukienkit"] != null))
            {
                if (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null)
                {
                    sql.CommandText += " AND (NamDuDKKT_CK IS NOT NULL) ";
                }
                else if (Request.Form["doituong_dudieukienkitcongchungkhac"] != null)
                {
                    sql.CommandText += " AND (NamDuDKKT_Khac IS NOT NULL) ";
                }
                else if (Request.Form["doituong_khongdudieukienkit"] != null)
                {
                    sql.CommandText += " AND (NamDuDKKT_CK IS NULL)  ";
                    sql.CommandText += " AND (NamDuDKKT_Khac IS NULL)  ";
                }
            }
            //Nếu check hai trong ba
            if ((Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null &&
                 Request.Form["doituong_dudieukienkitcongchungkhac"] != null &&
                 Request.Form["doituong_khongdudieukienkit"] == null)
                ||
                (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] == null &&
                 Request.Form["doituong_dudieukienkitcongchungkhac"] != null &&
                 Request.Form["doituong_khongdudieukienkit"] != null)
                ||
                (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null &&
                 Request.Form["doituong_dudieukienkitcongchungkhac"] == null &&
                 Request.Form["doituong_khongdudieukienkit"] != null))
            {
                if (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] == null)
                {
                    sql.CommandText +=
                        " AND ((NamDuDKKT_KHAC IS NOT NULL) OR ((NamDuDKKT_Khac IS NULL) AND (NamDuDKKT_CK IS NULL)))  ";
                }
                else if (Request.Form["doituong_dudieukienkitcongchungkhac"] == null)
                {
                    sql.CommandText +=
                        " AND ((NamDuDKKT_CK IS NOT NULL) OR ((NamDuDKKT_Khac IS NULL) AND (NamDuDKKT_CK IS NULL)))  ";
                }
                else if (Request.Form["doituong_khongdudieukienkit"] == null)
                {
                    sql.CommandText += " AND ((NamDuDKKT_CK IS NOT NULL) OR (NamDuDKKT_KHAC IS NOT NULL))  ";
                }
            }
        }

        //Lọc thành viên hãng kiểm toán quốc tế
        if (Request.Form["thanhvienhangkiemtoanquocte"] != null)
        {
            sql.CommandText += " AND (ThanhVienHangKTQT IS NOT NULL) ";
        }

        //Lọc theo vốn điều lệ
        if (Request.Form["vondieuletu"] != null)
        {
            string VonDieuLeTu = strBoChamPhay(Request.Form["vondieuletu"].ToString());
            if (!string.IsNullOrEmpty(VonDieuLeTu))
                sql.CommandText += " AND (VonDieuLe >= " + VonDieuLeTu + ") ";
        }
        if (Request.Form["vondieuleden"] != null)
        {
            string VonDieuLeDen = strBoChamPhay(Request.Form["vondieuleden"].ToString());
            if (!string.IsNullOrEmpty(VonDieuLeDen))
                sql.CommandText += " AND (VonDieuLe <= " + VonDieuLeDen + ") ";
        }
        //Lọc theo số lượng hội viên
        if (Request.Form["soluonghoivientu"] != null)
        {
            string SoLuongHoiVienTu = strBoChamPhay(Request.Form["soluonghoivientu"].ToString());
            if (!string.IsNullOrEmpty(SoLuongHoiVienTu))
                sql.CommandText += " AND (TongSoHoiVien >= " + SoLuongHoiVienTu + ") ";
        }
        if (Request.Form["soluonghoivienden"] != null)
        {
            string SoLuongHoiVienDen = strBoChamPhay(Request.Form["soluonghoivienden"].ToString());
            if (!string.IsNullOrEmpty(SoLuongHoiVienDen))
                sql.CommandText += " AND (TongSoHoiVien <= " + SoLuongHoiVienDen + ") ";
        }
        //Lọc theo doanh thu
        if (Request.Form["doanhthutu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["doanhthutu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (TongDoanhThu >= " + Tu + ") ";
        }
        if (Request.Form["doanhthuden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["doanhthuden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (TongDoanhThu <= " + Den + ") ";
        }
        //Lọc theo số lượng kiểm toán viên
        if (Request.Form["soluongkiemtoanvientu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["soluongkiemtoanvientu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (TongSoKTVDKHN >= " + Tu + ") ";
        }
        if (Request.Form["soluongkiemtoanvienden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["soluongkiemtoanvienden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (TongSoKTVDKHN <= " + Den + ") ";
        }
        //Lọc theo số người lao động
        if (Request.Form["soluongnguoilaodongtu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["soluongnguoilaodongtu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (TongSoNguoiLamViec >= " + Tu + ") ";
        }
        if (Request.Form["soluongnguoilaodongden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["soluongnguoilaodongden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (TongSoNguoiLamViec <= " + Den + ") ";
        }
        //Lọc theo số lượng khách hàng
        if (Request.Form["soluongkhachhangtu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["soluongkhachhangtu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (TongSoKHTrongNam >= " + Tu + ") ";
        }
        if (Request.Form["soluongkhachhangden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["soluongkhachhangden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (TongSoKHTrongNam <= " + Den + ") ";
        }
        //Lọc theo Ngày thành lập
        if (Request.Form["ngaythanhlaptu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["ngaythanhlaptu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (NgayThanhLap >= '" +
                                   (DateTime.ParseExact(Request.Form["ngaythanhlaptu"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }
        if (Request.Form["ngaythanhlapden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["ngaythanhlapden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (NgayThanhLap <= '" +
                                   (DateTime.ParseExact(Request.Form["ngaythanhlapden"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }
        //sql.CommandText += " AND (C.HoiVienTapTheID = 53)";
        //sql.CommandText += "  OR C.HoiVienTapTheID = 54)";
        sql.CommandText += ") order by SoHieuOrder, HoiVienTapTheID";
        ds = DataAccess.RunCMDGetDataSet(sql);


        DataTable dt = ds.Tables[0];

        int i = 0;
        foreach (DataRow tem in dt.Rows)
        {
            i++;
            objDanhSachCongTyKiemToanTrongCaNuoc obj = new objDanhSachCongTyKiemToanTrongCaNuoc();
            obj.NgayThangNam = "(Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            obj.STT = i.ToString();
            obj.SoHieuCongTy = tem["SoHieu"].ToString();
            obj.TenCongTy = tem["TenDoanhNghiep"].ToString();
            obj.TenVietTat = tem["TenVietTat"].ToString();

            try
            {
                obj.NgayQuyetDinhKetNapHoiVienTapThe = Convert.ToDateTime(tem["NgayGiaNhap"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            //var regexItem = new Regex("^[a-zA-Z0-9 ]*$");

            //if(regexItem.IsMatch(tem["DiaChi"].ToString())){
            obj.DiaChiTruSoChinh = tem["DiaChi"].ToString();
            //}
            // string strTempDiaChi = "[^\\p{Ll}\\p{Lu}\\p{Lt}\\p{Lo}\\p{Nd}\\p{Pc}]";
            // string strTempDiaChi ="[^\\w\\d]";
            //obj.DiaChiTruSoChinh = Regex.Replace(tem["DiaChi"].ToString(), strTempDiaChi, "");
            //obj.DiaChiChiNhanh.
            obj.HoVaTenNguoiDaiDien = tem["NguoiDaiDienPL_Ten"].ToString();
            obj.MobileNguoiDaiDien = tem["NguoiDaiDienPL_DiDong"].ToString();
            obj.SoGiayChungNhanDuDKKDDichVuKiT = tem["SoGiayChungNhanDKKD"].ToString();

            try
            {
                obj.NgayCapGiayChungNhanDuDKKDDichVuKiT =
                    Convert.ToDateTime(tem["NgayCapGiayChungNhanDKKD"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            string strTenChiNhanh = "";
            string strDiaChiChiNhanh = "";
            string strHoVaTenGDChiNhanh = "";
            string strMobileGDChiNhanh = "";
            sql.CommandText = "Select * from tblHoiVienTapTheChiNhanh where HoiVienTapTheID = " +
                              tem["HoiVienTapTheID"];
            DataTable dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            if (dtbtemp.Rows.Count > 0)
            {
                int j = 0;
                foreach (DataRow dtr in dtbtemp.Rows)
                {
                    j++;
                    strTenChiNhanh += j + ". " + dtr["Ten"].ToString() + "\r\n";
                    strDiaChiChiNhanh += j + ". " + dtr["DiaChi"].ToString() + "\r\n";
                    strHoVaTenGDChiNhanh += j + ". " + dtr["DaiDienPL_Ten"].ToString() + "\r\n";
                    strMobileGDChiNhanh += j + ". " + dtr["DaiDienPL_Mobile"].ToString() + "\r\n";
                }
            }

            obj.TenChiNhanh = strTenChiNhanh;
            obj.DiaChiChiNhanh = strDiaChiChiNhanh;
            obj.HoVaTenGDChiNhanh = strHoVaTenGDChiNhanh;
            obj.MobileGDChiNhanh = strMobileGDChiNhanh;
            lst.Add(obj);

        }

        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        ReportDocument rpt = new ReportDocument();
        rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
        rpt.Load(Server.MapPath("Report/QuanLyThongTinChung/DanhSachCongTyKiemToanTrongCaNuoc.rpt"));

        //rpt.SetDataSource(lst);
        rpt.Database.Tables["objDanhSachCongTyKiemToanTrongCaNuoc"].SetDataSource(lst);
        //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

        CrystalReportViewer1.ReportSource = rpt;
        CrystalReportViewer1.DataBind();
        //CrystalReportViewer1.SeparatePages = true;
        if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true,
                                     "DanhSachCongTyKiemToanTrongCaNuoc");
        if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
        {
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true,
                                     "DanhSachCongTyKiemToanTrongCaNuoc");


            //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //xlsFormatOptions.ShowGridLines = true;
            //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

        }
        if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
            //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true,
            //                         "DanhSachCongTyKiemToanTrongCaNuoc");
        {
            string strNgayTinh = "(Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            string strNgayBaoCao = DateTime.Now.ToString("dd/MM/yyyy");
            ExportToExcel("DanhSachCongTyKiemToanTrongCaNuoc", lst, strNgayTinh, strNgayBaoCao);
        }
    }

    private void ExportToExcel(string strName, List<objDanhSachCongTyKiemToanTrongCaNuoc> lst, string NgayTinh, string NgayBC)
    {
        string outHTML = "";

        outHTML += "<table border = '1' cellpadding='5' cellspacing='0' style='font-family:Arial; font-size:8pt; color:#003366'>";
//        outHTML += @"<col width='87%' />
//                 <col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />
//<col width='1%' />";
        //outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        //outHTML += "<td colspan='2' style='border-style:none'></td>";
        //outHTML += "<td colspan='2' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/logo.png' alt='' border='0px'/></td>";
        //outHTML += "<td colspan='10' style='border-style:none'></td>";

        //outHTML += "</tr>";
        outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        outHTML += "<td colspan='8' style='border-style:none'></td>";
        outHTML += "<td colspan='6' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/ten.png' alt='' border='0px'/></td>";

        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";

        outHTML += "<tr>";
        outHTML += "<td colspan='14' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH CÔNG TY KIỂM TOÁN TRONG CẢ NƯỚC</b></td>";
        //outHTML += "<td colspan ='5' style='border-style:none;'></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='14' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i>" + NgayTinh + "</i></td>";
       // outHTML += "<td colspan ='5'style='border-style:none;'></td>";
        outHTML += "</tr>";
        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "    <th rowspan = '6' >STT</th>";
        outHTML += "    <th rowspan = '6' ><br>Số<br>hiệu<br>công<br>ty</th>";
        outHTML += "    <th rowspan = '6' >Tên công ty</th>";
        outHTML += "    <th rowspan = '6' ><br><br>Tên<br> viết tắt</th>";
        outHTML += "    <th rowspan = '6' ><br>Ngày quyết<br> định kết nạp<br> hội viên tập<br> thể</th>";
        outHTML += "    <th rowspan = '6' ><br><br>Địa chỉ trụ<br> sở chính</th>";
        outHTML += "    <th rowspan = '6' >Họ và tên<br> người đại diện<br> theo pháp luật<br> (Hoặc <br>TGĐ/GĐ)</th>";
        outHTML += "    <th rowspan = '6' ><br>Mobile người<br> đại diện theo<br> pháp luật<br> (Hoặc TGĐ/GĐ)</th>";
        outHTML += "    <th rowspan = '6' ><br>Số giấy<br> chứng nhận<br> đủ ĐKKD<br> dịch vụ KiT</th>";
        outHTML += "    <th rowspan = '6' ><br>Ngày cấp<br> chứng nhận<br> đủ ĐKKD<br> dịch vụ KiT</th>";
        outHTML += "    <th rowspan = '6' >Tên chi nhánh</th>";
        outHTML += "    <th rowspan = '6' ><br>Địa chỉ <br>chi<br> nhánh</th>";
        outHTML += "    <th rowspan = '6' ><br>Họ và tên<br> GĐ chi<br> nhánh</th>";
        outHTML += "    <th rowspan = '6' ><br><br>Mobile GĐ<br> chi nhánh</th>";
        
        outHTML += "</tr>";

        outHTML += "<tr>";
        outHTML += "</tr>";

        outHTML += "<tr>";
        outHTML += "</tr>";

        outHTML += "<tr>";
        outHTML += "</tr>";

        outHTML += "<tr>"; 
        outHTML += "</tr>";

        outHTML += "<tr>"; 
        outHTML += "</tr>";

        outHTML += "<tr>";
        outHTML += "    <th>1</th>";
        outHTML += "    <th>2</th>";
        outHTML += "    <th>3</th>";
        outHTML += "    <th>4</th>";
        outHTML += "    <th>5</th>";
        outHTML += "    <th>6</th>";
        outHTML += "    <th>7</th>";
        outHTML += "    <th>8</th>";
        outHTML += "    <th>9</th>";
        outHTML += "    <th>10</th>";
        outHTML += "    <th>11</th>";
        outHTML += "    <th>12</th>";
        outHTML += "    <th>13</th>";
        outHTML += "    <th>14</th>";
        outHTML += "</tr>";

        int i = 0;
        foreach (var temp in lst)
        {
            i++;
            outHTML += "<tr>";
            outHTML += "<td>" + i.ToString() + "</td>";
            outHTML += "<td >" + "'" + temp.SoHieuCongTy + "</td>";
            outHTML += "<td style ='WORD-BREAK:BREAK-ALL'>" + temp.TenCongTy + "</td>";
            outHTML += "<td>" + temp.TenVietTat + "</td>";
            outHTML += "<td>" + temp.NgayQuyetDinhKetNapHoiVienTapThe + "</td>";
            outHTML += "<td>" + temp.DiaChiTruSoChinh + "</td>";
            outHTML += "<td>" + temp.HoVaTenNguoiDaiDien + "</td>";
            outHTML += "<td>" + temp.MobileNguoiDaiDien + "</td>";
            outHTML += "<td>" + temp.SoGiayChungNhanDuDKKDDichVuKiT + "</td>";
            outHTML += "<td>" + temp.NgayCapGiayChungNhanDuDKKDDichVuKiT + "</td>";
            outHTML += "<td>" + temp.TenChiNhanh + "</td>";
            outHTML += "<td>" + temp.DiaChiChiNhanh + "</td>";
            outHTML += "<td>" + temp.HoVaTenGDChiNhanh + "</td>";
            outHTML += "<td>" + temp.MobileGDChiNhanh + "</td>";
            outHTML += "</tr>";
        }

        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none'></td>";
        outHTML += "<td colspan='2' align='center' style='border-style:none'><b>" + NgayBC + "</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none'></td>";
        outHTML += "<td colspan='2' align='center' style='border-style:none'><b>NGƯỜI LẬP BIỂU<b></td>";
        outHTML += "</tr>";

        outHTML += "</table>";

        Library.ExportToExcel(strName, outHTML);
    }

    protected string strBoChamPhay(string p)
    {
        int leng = p.Length;
        if (p.LastIndexOf(',') != -1)
            leng = p.LastIndexOf(',');
        string strOut = p.Substring(0, leng);
        strOut = strOut.Replace(".", string.Empty);
        return strOut;
    }

    protected  void LoadCheckBox()
    {
        if (this.IsPostBack)
        {
            if (Request.Form["tinhtrang_danghoatdong"] != null)
            {
                Response.Write("$('#tinhtrang_danghoatdong').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#tinhtrang_danghoatdong').attr('checked', false); ");
            }
            if (Request.Form["tinhtrang_dunghoatdong"] != null)
            {
                Response.Write("$('#tinhtrang_dunghoatdong').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#tinhtrang_dunghoatdong').attr('checked', false); ");
            }
            if (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null)
            {
                Response.Write("$('#doituong_dudieukienkitlinhvucchungkhoan').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#doituong_dudieukienkitlinhvucchungkhoan').attr('checked', false); ");
            }
            if (Request.Form["doituong_khongdudieukienkit"] != null)
            {
                Response.Write("$('#doituong_khongdudieukienkit').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#doituong_khongdudieukienkit').attr('checked', false); ");
            }
            if (Request.Form["doituong_dudieukienkitcongchungkhac"] != null)
            {
                Response.Write("$('#doituong_dudieukienkitcongchungkhac').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#doituong_dudieukienkitcongchungkhac').attr('checked', false); ");
            }
            if (Request.Form["thanhvienhangkiemtoanquocte"] != null)
            {
                Response.Write("$('#thanhvienhangkiemtoanquocte').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#thanhvienhangkiemtoanquocte').attr('checked', false); ");
            }
        }
    }
}