﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan.ascx.cs" Inherits="usercontrols_QLHV_BC_QuyetDinhKhenThuongHoiVienTapTheVaHoiVienCaNhan" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<%--<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>--%>
<script type="text/javascript" src="js/ui.dropdownchecklist-1.5-min.js"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<%--<script type="text/javascript" src="js/jquery-ui.min.js"></script>--%>

<form id="Form1" runat="server" clientidmode="Static">
<asp:HiddenField ID="hi_LoaiHinh" runat="server" />
<asp:HiddenField ID="hi_TinhThanh" runat="server" />
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<h4 class="widgettitle">
    Quyết định khen thưởng hội viên tổ chức và hội viên cá nhân</h4>
<fieldset class="fsBlockInfor" style="margin-top: 0px;">
    <legend>Quyết định khen thưởng hội viên tổ chức và hội viên cá nhân</legend>
    <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
    
    <table width="100%" border="0" class="formtbl">
    <tr>
        <td width="15%"></td>
        <td width="18%">Số quyết định <span style="color:Red">(*)</span>:</td>
        <td width="13%"> <input type="text" name="SoQuyetDinh" id="SoQuyetDinh" value="<%=Request.Form["SoQuyetDinh"] %>" 
                style="width: 149px" /></td>
        <td width = "5%"></td>
        <%--<td width="8%"><a id="a1" onclick="open_ChonID1()" data-placement="top" data-rel="tooltip" href="#none" data-original-title="..."  rel="tooltip" class="btnabc">
            <input type="button" value = "..." id="Button1" name="btChonID" 
                style="width: 49px" /></a></td>--%>
        <td width="18%">Ngày quyết định <span style="color:Red">(*)</span></td>
        <td width="16%"><input type="text" name="NgayQuyetDinh" id="NgayQuyetDinh" value="<%=Request.Form["NgayQuyetDinh"] %>" 
                style="width: 149px" /></td>
        <%--<td width="16%"><input type="text" name="NgayQuyetDinh" id="NgayQuyetDinh" value="" 
                style="width: 149px" /></td>--%>
        <td ></td>
        
    </tr>
    
    
    <%--<tr>
        <td width="20%"></td>
        <td width="13%">Định dạng file: </td>
        <td colspan="4"> <select id="idFile" name="idFile" style="width: 152px">
            <%DinhDangFile(); %>
        </select></td>
        <td width="20%"></td>
    </tr>--%>
    </table>
    

    <div class="dataTables_length" style="text-align: center;">
        <a id="btnSave" href="javascript:;" class="btn" onclick="View('');"><i class="iconfa-plus-sign">
             </i>Xem thông tin</a> 
        
        <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="View('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a>
                 <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="View('pdf');">
                    <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                    <input type="hidden" id="hdAction" name="hdAction"/>
    </div>
</fieldset>

 <fieldset class="fsBlockInfor">
        <legend>Quyết định khen thưởng hội viên tổ chức và hội viên cá nhân</legend>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False" HasExportButton="False" 
            HasPrintButton="False"/>
    </fieldset>

    <div id="div_dmNhanSuHoiVien_add"></div>
</form>

<script type="text/javascript">



//        $(document).ready(function () {
//            $("#NgayBatDau").datepicker();
//            $("#NgayKetThuc").datepicker();

//            $(".NgayBatDau").datepicker({
//                dateFormat: 'mm-dd-yy'
//            }).datepicker('setDate', new Date())

        $(function () {  $('#NgayQuyetDinh').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
        });

$(function () {
    if (document.getElementById("NgayQuyetDinh").value == null)
            $('#NgayQuyetDinh').datepicker('setDate', new Date()) 
     });
          
      //  });

 </script>

 <script type="text/javascript">
     

         var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function() {
        $('#NgayQuyetDinh').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');

//  $('#NgayBatDau').datepicker('setDate', new Date())
  //  $('#NgayKetThuc').datepicker('setDate', new Date())
  if (document.getElementById("NgayQuyetDinh").value == null)
            $('#NgayQuyetDinh').datepicker('setDate', new Date())
});

        </script>

<%--gọi function in bao cao--%>
<script type="text/javascript">
    function View(type) {

        var idHV = document.getElementById('NgayQuyetDinh').value;
        var soHV = document.getElementById('SoQuyetDinh').value;
        if (idHV == "") {
            alert("Bạn chưa nhập thông tin Ngày quyết định!")
            return;
        }
            if (soHV == "") {
            alert("Bạn chưa nhập thông tin Số quyết định!")
            return;
        }
        //        <%setdataCrystalReport(); %>
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
</script>
<%--
<script type="text/javascript">
    function open_ChonID1() {

        
        var NgayQD = $('#NgayQuyetDinh').val();
        

        var timestamp = Number(new Date());
        $("#div_dmNhanSuHoiVien_add").empty();
        $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoivien' name='iframe_hoivien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=QLHV_BC_SoQD&type=SoQD&NgayQuyetDinh=" + NgayQD + "&mode=iframe&time=" + timestamp));
        $("#div_dmNhanSuHoiVien_add").dialog({
            autoOpen: false,
            title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách hội viên</b>",
            modal: true,
            width: "640",
            height: "480",
            buttons: [
                                    {
                                        text: "Chọn",
                                        click: function () {
                                            formChon1();
                                        }
                                    },
                                    {
                                        text: "Đóng",
                                        click: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                            ]
        }).dialog("open");

    }
    
</script>

<script type="text/javascript">
    function formChon1() {
        window.iframe_hoivien.ChooseCongTy();
        
    }

    function GetValueFromPopup(value) {
        $('#SoQuyetDinh').val(value);
    }

    function ClosePopup() {
        $("#div_dmNhanSuHoiVien_add").dialog('close');
    }
</script>--%>

