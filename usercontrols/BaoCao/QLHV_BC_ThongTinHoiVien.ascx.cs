﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BC_ThongTinHoiVien : System.Web.UI.UserControl
{
    protected string tenchucnang = "Thông tin cá nhân kiểm toán viên";
    //protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    //private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    

    public void LoadDonViCongTac(string DonVi = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (DonVi == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim().Contains(Tinh["MaTinh"].ToString().Trim()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Contains(tem.idDoiTuong.ToString()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void setdataCrystalReport()
    {
        string strid = id.Text;
        if (!string.IsNullOrEmpty(strid))
        {
            //int idHV = Convert.ToInt32(strid);
            List<objThongTinHoiVien_ChiTiet> lst = new List<objThongTinHoiVien_ChiTiet>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoiVienCaNhanID, HODEM, Ten, NgaySinh, GioiTinh, b.TenTinh, a.DiaChi, c.TenTruongDaiHoc, "
                        + " d.TenHocHam, a.HocHamNam, e.TenHocVi, a.HocViNam, a.SoCMND, a.CMND_NgayCap, "
                        + " a.SoTaiKhoanNganHang, f.TenNganHang, a.SoChungChiKTV, a.NgayCapChungChiKTV, "
                        + " a.SoGiayChungNhanDKHN, a.NgayCapGiayChungNhanDKHN, g.TenChucVu, a.DonViCongTac, "
                        + " a.Email, a.DienThoai, h.TenSoThich, a.SoQuyetDinhKetNap, a.NgayQuyetDinhKetNap "
                        + " from dbo.tblHoiVienCaNhan a "
                        + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.MaTinh "
                        + " left join dbo.tblDMTruongDaiHoc c on a.TruongDaiHocID = c.TruongDaiHocID "
                        + " left join dbo.tblDMHocHam d on d.HocHamID = a.HocHamID "
                        + " left join dbo.tblDMHocVi e on e.HocViID = a.HocViID "
                        + " left join dbo.tblDMNganHang f on a.NganHangID = f.NganHangID "
                        + " left join dbo.tblDMChucVu g on a.ChucVuID = g.ChucVuID "
                        + " left join dbo.tblDMSoThich h on a.SoThichID = h.SoThichID "
                        + " WHERE MaHoiVienCaNhan = '" + strid + "'";// + id;
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
        List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
        List<lstOBJQuaTrinhLamViec> lstQuaTrinhLamViec = new List<lstOBJQuaTrinhLamViec>();
        List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();

        List<lst_KetQuaKiemSoatChatLuong> lstKiemSoatChatLuong = new List<lst_KetQuaKiemSoatChatLuong>();
        List<KetQuaKiemSoatChatLuong> tongKiemSoat = new List<KetQuaKiemSoatChatLuong>();
        List<ThongTinThamGiaDoanKiemTra> lstDoanKTra = new List<ThongTinThamGiaDoanKiemTra>();
       // int i = 1;
        foreach (DataRow tem in dt.Rows)
        {
            objThongTinHoiVien_ChiTiet new1 = new objThongTinHoiVien_ChiTiet();
            new1.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            new1.NgaySinh = !string.IsNullOrEmpty(tem["NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgaySinh"])) : "";
            new1.GioiTinh = tem["GioiTinh"].ToString() == "1" ? "Nam" : "Nữ";
            new1.QueQuan = tem["TenTinh"].ToString();
            new1.ChoOHienNay = tem["DiaChi"].ToString();
            new1.BangCap = tem["TenTruongDaiHoc"].ToString();
            new1.HocHam = tem["TenHocHam"].ToString();
            new1.HocVi = tem["TenHocVi"].ToString();
            new1.Nam_HocHam = tem["HocHamNam"].ToString();
            new1.Nam_HocVi = tem["HocViNam"].ToString();
            new1.CMT = tem["SoCMND"].ToString();
            new1.NgayCap = !string.IsNullOrEmpty(tem["CMND_NgayCap"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["CMND_NgayCap"])) : "";
            new1.SoTK = tem["SoTaiKhoanNganHang"].ToString();
            new1.TaiNganHang = tem["TenNganHang"].ToString();
            new1.SoChungChi = tem["SoChungChiKTV"].ToString();
            new1.NgayCap_CT = !string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCapChungChiKTV"])) : "";
            new1.GiayDangKy = tem["SoGiayChungNhanDKHN"].ToString();
            new1.NoiCapGiayDangKy = "";
            new1.ChucVu = tem["TenChucVu"].ToString();
            new1.DonViCongTac = tem["DonViCongTac"].ToString();
            new1.Email = tem["Email"].ToString();
            new1.DienThoai = "";
            new1.DD = tem["DienThoai"].ToString();
            new1.SoThich = tem["TenSoThich"].ToString();
            //new1.lstQuaTrinhLamViec = new List<lstOBJQuaTrinhLamViec>();
            //new1.lstKhenThuong = new List<lstOBJKhenThuong>();
            lst.Add(new1);

            TenBaoCao abc123 = new TenBaoCao();
            abc123.TenBC = tem["SoQuyetDinhKetNap"].ToString();
            abc123.ngayBC = !string.IsNullOrEmpty(tem["NgayQuyetDinhKetNap"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayQuyetDinhKetNap"])) : "";
            lstTenBC.Add(abc123);

            int idHV = Convert.ToInt32(tem["HoiVienCaNhanID"]);

            DataSet ds1 = new DataSet();
            SqlCommand sql1 = new SqlCommand();
            sql1.CommandText = " select NgayThang, TenHinhThucKhenThuong, LyDo from tblKhenThuongKyLuat a "
                            + " inner join dbo.tblDMHinhThucKhenThuong b on a.HinhThucKhenThuongID = b.HinhThucKhenThuongID "
                            + " where a.HoiVienCaNhanID = " + idHV;// + id;
            ds1 = DataAccess.RunCMDGetDataSet(sql1);
            sql1.Connection.Close();
            sql1.Connection.Dispose();
            sql1 = null;

            DataTable dt1 = ds1.Tables[0];

            foreach (DataRow tem1 in dt1.Rows)
            {
                lstOBJKhenThuong con = new lstOBJKhenThuong();
                //con.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
                if (!string.IsNullOrEmpty(tem1["NgayThang"].ToString()))
                    con.NgayThang = NgayThangVN(Convert.ToDateTime(tem1["NgayThang"]));
                con.HinhThuc = tem1["TenHinhThucKhenThuong"].ToString();
                con.LyDo = tem1["LyDo"].ToString();
                lstKhenThuong.Add(con);
            }


            DataSet ds2 = new DataSet();
            SqlCommand sql2 = new SqlCommand();
            sql2.CommandText = " select ThoiGianCongTac, ChucVu, NoiLamViec from dbo.tblHoiVienCaNhanQuaTrinhCongTac "
                            + " where HoiVienCaNhanID = " + idHV;// + id;
            ds2 = DataAccess.RunCMDGetDataSet(sql2);
            sql2.Connection.Close();
            sql2.Connection.Dispose();
            sql2 = null;

            DataTable dt2 = ds2.Tables[0];

            foreach (DataRow tem2 in dt2.Rows)
            {
                lstOBJQuaTrinhLamViec con = new lstOBJQuaTrinhLamViec();
              //  con.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
                con.ThoiGian = tem2["ThoiGianCongTac"].ToString();
                con.ChucVu = tem2["ChucVu"].ToString();
                con.DonViCongTac = tem2["NoiLamViec"].ToString();
                lstQuaTrinhLamViec.Add(con);
            }

            DataSet ds3 = new DataSet();
            SqlCommand sql3 = new SqlCommand();
            sql3.CommandText = " select NgayThang, TenHinhThucKyLuat, LyDo from tblKhenThuongKyLuat a "
                                + " inner join dbo.tblDMHinhThucKyLuat b on a.HinhThucKyLuatID = b.HinhThucKyLuatID "
                                + " where a.HoiVienCaNhanID = " + idHV;// + id;
            ds3 = DataAccess.RunCMDGetDataSet(sql3);
            sql3.Connection.Close();
            sql3.Connection.Dispose();
            sql3 = null;

            DataTable dt3 = ds3.Tables[0];

            foreach (DataRow tem3 in dt3.Rows)
            {
                lstOBJKyLuat con = new lstOBJKyLuat();
               // con.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
                if (!string.IsNullOrEmpty(tem3["NgayThang"].ToString()))
                    con.NgayThang = NgayThangVN(Convert.ToDateTime(tem3["NgayThang"]));
                con.HinhThuc = tem3["TenHinhThucKyLuat"].ToString();
                con.LyDo = tem3["LyDo"].ToString();
                lstKyLuat.Add(con);
            }

            DataSet ds4 = new DataSet();
            SqlCommand sql4 = new SqlCommand();
            sql4.CommandText = " select TenKhachHangKT, XepLoai from dbo.tblKSCLBaoCaoKTKT "
                                + " where HoiVienCaNhanID = " + idHV;// + id;
            ds4 = DataAccess.RunCMDGetDataSet(sql4);
            sql4.Connection.Close();
            sql4.Connection.Dispose();
            sql4 = null;

            DataTable dt4 = ds4.Tables[0];

            int iKT = 0;
            foreach (DataRow tem4 in dt4.Rows)
            {
                iKT++;
                lst_KetQuaKiemSoatChatLuong con = new lst_KetQuaKiemSoatChatLuong();
                // con.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
                con.STT = iKT.ToString();
                con.HoSo = tem4["TenKhachHangKT"].ToString();
                con.KetQua = tem4["XepLoai"].ToString();
                lstKiemSoatChatLuong.Add(con);
            }

            KetQuaKiemSoatChatLuong newabc = new KetQuaKiemSoatChatLuong();
            newabc.SoLuongBC = iKT.ToString();
            newabc.TongDaKy = iKT.ToString();
            newabc.TongKiemTra = iKT.ToString();

            tongKiemSoat.Add(newabc);


            DataSet ds5 = new DataSet();
            SqlCommand sql5 = new SqlCommand();
            sql5.CommandText = " select b.NgayLap, b.MaDoanKiemTra, d.TenDoanhNghiep from dbo.tblKSCLDoanKiemTraThanhVien a "
                                + " inner join dbo.tblKSCLDoanKiemTra b on a.DoanKiemTraID = b.DoanKiemTraID "
                                + " left join dbo.tblKSCLDoanKiemTraCongTy c on a.DoanKiemTraID = c.DoanKiemTraID "
                                + " left join dbo.tblDonHoiVienTapThe d on c.HoiVienTapTheID = d.HoiVienTapTheID " 
                                + " where a.HoiVienCaNhanID = " + idHV;// + id;
            ds5 = DataAccess.RunCMDGetDataSet(sql5);
            sql5.Connection.Close();
            sql5.Connection.Dispose();
            sql5 = null;

            DataTable dt5 = ds5.Tables[0];

            //int iKT = 0;
            foreach (DataRow tem5 in dt5.Rows)
            {
                iKT++;
                if (!string.IsNullOrEmpty(tem5["NgayLap"].ToString()))
                {
                    ThongTinThamGiaDoanKiemTra con = new ThongTinThamGiaDoanKiemTra();
                    con.Nam = Convert.ToDateTime(tem5["NgayLap"]).Year.ToString();
                    con.Ngay = NgayThangVN(Convert.ToDateTime(tem5["NgayLap"]));
                    con.Ma = tem5["MaDoanKiemTra"].ToString();
                    con.DanhSach = tem5["TenDoanhNghiep"].ToString();
                    lstDoanKTra.Add(con);
                }
            }
        }
        

            
            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/ThongTinCaNhanKiemToanVien.rpt"));
            
            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objThongTinHoiVien_ChiTiet"].SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["lstOBJQuaTrinhLamViec"].SetDataSource(lstQuaTrinhLamViec);
            rpt.Database.Tables["lstOBJKhenThuong"].SetDataSource(lstKhenThuong);
            rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            rpt.Database.Tables["lst_KetQuaKiemSoatChatLuong"].SetDataSource(lstKiemSoatChatLuong);
            rpt.Database.Tables["KetQuaKiemSoatChatLuong"].SetDataSource(tongKiemSoat);
            rpt.Database.Tables["ThongTinThamGiaDoanKiemTra"].SetDataSource(lstDoanKTra);

            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "ThongTinCaNhanKiemToanVien");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "ThongTinCaNhanKiemToanVien");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            //ReportDocument rdPL = new ReportDocument();
            //rdPL.Load(Application.StartupPath + "\\GCN\\312_313\\" + strFilePL);
            //rdPL.SetDataSource(ds);
            //rdPL.SetParameterValue("NoiCapPL", txtNoiCapPL.Text,);
            //crtXemPL.ReportSource = rdPL;
            //crtXemPL.Refresh();

            //rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            //    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //{
            //    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //    //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //    //xlsFormatOptions.ShowGridLines = true;
            //    //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //    //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //    //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            //}
        }
    }
    

    //private List<objDoiTuong> getDinhDangFile()
    //{
    //    List<objDoiTuong> lst = new List<objDoiTuong>();

    //    objDoiTuong obj1 = new objDoiTuong();
    //    obj1.idDoiTuong = 1;
    //    obj1.TenDoiTuong= "Word";
    //    objDoiTuong obj2 = new objDoiTuong();
    //    obj2.idDoiTuong = 2;
    //    obj2.TenDoiTuong = "Excel";
    //    objDoiTuong obj3 = new objDoiTuong();
    //    obj3.idDoiTuong = 3;
    //    obj3.TenDoiTuong = "PDF";

    //    lst.Add(obj1);
    //    lst.Add(obj2);
    //    lst.Add(obj3);

    //    return lst;
    //}
    protected void Text_Changed(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
                        + " from dbo.tblHoiVienCaNhan "
                        + " WHERE MaHoiVienCaNhan = '" + id.Text + "'";// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtTen.Text = tem["HoTen"].ToString();
        }
        catch { }
    }

    protected void Text_Changed_2(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + idDanhSach.Text + "'";// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtCongTy.Text = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
        }
        catch { }
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            id.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
                        + " from dbo.tblHoiVienCaNhan "
                        + " WHERE MaHoiVienCaNhan = '" + lstIdChon[0] + "'";// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtTen.Text = tem["HoTen"].ToString();
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }

    protected void btnChay2_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            idDanhSach.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + lstIdChon[0] + "'";// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtCongTy.Text = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }
}