﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_BaoCao_BangCauHoiKiemTra_ChonDoanKiemTra : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _nam = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this._nam = Request.QueryString["nam"];
            _db.OpenConnection();
            if (!IsPostBack)
            {
                LoadDanhSach();
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void LoadDanhSach()
    {
        try
        {
            _db.OpenConnection();

            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "ID", "MaDoanKiemTra", "NgayLap", "QuyetDinhThanhLap" });
            if (string.IsNullOrEmpty(_nam))
                return;
            string query = @"SELECT DISTINCT DKT.DoanKiemTraID, DKT.MaDoanKiemTra, DKT.NgayLap, DKT.QuyetDinhThanhLap, DMTT.MaTrangThai, DMTT.TenTrangThai
	                         FROM tblKSCLDoanKiemTra AS DKT
	                         LEFT JOIN tblDMTrangThai AS DMTT ON DKT.TinhTrangID = DMTT.TrangThaiID
	                         LEFT JOIN tblKSCLDoanKiemTraCongTy DKT_CT ON DKT.DoanKiemTraID = DKT_CT.DoanKiemTraID
	                         LEFT JOIN tblHoiVienTapThe HVTC ON DKT_CT.HoiVienTapTheID = HVTC.HoiVienTapTheID
	                         WHERE 
		                        DKT.MaDoanKiemTra like '%" + txtMaDoanKiemTra.Text + @"%'		
		                        AND HVTC.MaHoiVienTapThe like '%" + txtMaCongTy.Text + @"%'
		                        AND (HVTT.TenDoanhNghiep like N'%" + txtTenCongTy.Text + @"%' OR HVTC.TenVietTat like N'%" + txtTenCongTy.Text + @"%' OR HVTC.TenTiengAnh like N'%" + txtTenCongTy.Text + @"%')
		                        AND DMTT.MaTrangThai = 5
		                        AND YEAR(DKT.NgayLap) = " + _nam + @"
	                         ORDER BY DKT.DoanKiemTraID DESC";

            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["DoanKiemTraID"].ToString();
                    dr["MaDoanKiemTra"] = ht["MaDoanKiemTra"].ToString();
                    dr["QuyetDinhThanhLap"] = ht["QuyetDinhThanhLap"].ToString();
                    dr["NgayLap"] = Library.DateTimeConvert(ht["NgayLap"]);
                    dt.Rows.Add(dr);
                }
            }
            rpHocVien.DataSource = dt.DefaultView;
            rpHocVien.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadDanhSach();
    }
}