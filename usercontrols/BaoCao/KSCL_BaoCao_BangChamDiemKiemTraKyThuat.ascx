﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_BangChamDiemKiemTraKyThuat.ascx.cs" Inherits="usercontrols_KSCL_BaoCao_BangChamDiemKiemTraKyThuat" %>

<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Bảng chấm điểm kiểm tra kỹ thuật</h4>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td>
                Năm<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlNam" name="ddlNam">
                    <% GetListYear();%>
                </select>
            </td>
        </tr>
        <tr>
            <td>Mã đoàn kiểm tra<span class="starRequired">(*)</span>:</td>
            <td>
                <input type="text" name="txtMaDoanKiemTra" id="txtMaDoanKiemTra" onchange="CallActionGetInforDoanKiemTra();" style="max-width: 120px;" />
                <input type="hidden" name="hdDoanKiemTraID" id="hdDoanKiemTraID" />
                <input type="button" value="---" onclick="OpenDanhSachDoanKiemTra();" style="border: 1px solid gray;" />
            </td>
        </tr>
        <tr>
            <td>Công ty kiểm toán<span class="starRequired">(*)</span>:</td>
            <td>
                <input type="text" name="txtMaCongTy" id="txtMaCongTy" readonly="readonly" style="max-width: 120px;" />
                <input type="hidden" name="hdCongTyID" id="hdCongTyID" />
                <input type="button" value="---" onclick="OpenDanhSachCongTy();" style="border: 1px solid gray;" />
            </td>
        </tr>
        <tr>
            <td>Báo cáo kiểm tra kỹ thuật<span class="starRequired">(*)</span>:</td>
            <td>
                <select id="ddlDanhSach" name="ddlDanhSach" style="max-width: 200px;"></select>
                <input type="hidden" id="hdBaoCaoKTKT" name="hdBaoCaoKTKT"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('excel');"><i class="iconsweets-excel2"></i>Kết xuất Excel</a> 
                        <a id="A1" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('pdf');"><i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Bảng chấm điểm kiểm tra kỹ thuật</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" 
        ShowAllPageIds="True" HasExportButton="False" HasPrintButton="False" 
        AutoDataBind="True" />
</fieldset>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    // Validate form
    $('#Form1').validate({
        rules: {
            txtMaDoanKiemTra: {
                required: true
            },
            txtMaCongTy: {
                required: true
            },
            ddlDanhSach:{
                required: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }  
    });

    function OpenDanhSachDoanKiemTra() {
        $("#DivDanhSachDoanKiemTra").empty();
        $("#DivDanhSachDoanKiemTra").append($("<iframe width='100%' height='100%' id='ifDanhSach' name='ifDanhSach' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_BaoCao_BangCauHoiKiemTra_ChonDoanKiemTra&nam=" + $('#ddlNam option:selected').val()));
        $("#DivDanhSachDoanKiemTra").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách đoàn kiểm tra</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSach.ChooseDoanKiemTra();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachDoanKiemTra').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivDanhSachDoanKiemTra').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }
    
    function CallActionGetInforDoanKiemTra() {
        var maDkt = $('#txtMaDoanKiemTra').val();
        iframeProcess.location = '/iframe.aspx?page=KSCL_BaoCao_Proccess&action=loadttdkt&madkt=' + maDkt + '&nam=' + $('#ddlNam option:selected').val();
    }
    
    function DisplayInforDoanKiemTra(value) {
        if(value.length == 0) {
            $('#txtMaDoanKiemTra').val('');
            $('#hdDoanKiemTraID').val('');
            $('#hdCongTyID').val('');
            $('#txtMaCongTy').val('');
            alert('Mã đoàn kiểm tra không đúng hoặc không tồn tại!');
        } else {
            $('#hdDoanKiemTraID').val(value.split(';#')[0]);
            $('#txtMaDoanKiemTra').val(value.split(';#')[1]);
            $('#hdCongTyID').val('');
            $('#txtMaCongTy').val('');
        }
    }
    
    function CloseFormDanhSachDoanKiemTra() {
        $("#DivDanhSachDoanKiemTra").dialog('close');
    }
    
    function OpenDanhSachCongTy() {
        if($('#hdDoanKiemTraID').val().length == 0) {
            alert('Phải chọn thông tin đoàn kiểm tra trước!');
            return;
        }
        $("#DivDanhSachCongTy").empty();
        $("#DivDanhSachCongTy").append($("<iframe width='100%' height='100%' id='ifDanhSachCongTy' name='ifDanhSachCongTy' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_BaoCao_BangCauHoiKiemTra_ChonCongTy&iddkt=" + $('#hdDoanKiemTraID').val()));
        $("#DivDanhSachCongTy").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách công ty trong đoàn kiểm tra</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSachCongTy.ChooseCongTy();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachCongTy').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivDanhSachCongTy').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }
    
    function DisplayInforCongTy(value) {
        if(value.length > 0) {
            $('#hdCongTyID').val(value.split(';#')[0]);
            $('#txtMaCongTy').val(value.split(';#')[1]);
            $('#hdBaoCaoKTKT').val('');
            // Load danh sách các báo cáo kiểm tra kỹ thuật của công ty này trong 1 đoàn kiểm tra
            iframeProcess.location = '/iframe.aspx?page=KSCL_BaoCao_Proccess&action=loaddabcktkt&iddkt=' + $('#hdDoanKiemTraID').val() + '&idct=' + $('#hdCongTyID').val();
        }
    }
    
    function CloseFormDanhSachCongTy() {
        $("#DivDanhSachCongTy").dialog('close');
    }
    
    function DisplayListDanhSachBaoCaoKiemTraKyThuat(arrData) {
        // Remove toàn bộ option cũ
        $('#ddlDanhSach').children().remove();
        // Add option mới theo công ty
        if(arrData.length > 0) {
            for(var i = 0 ; i < arrData.length ; i ++) {
                if($('#hdBaoCaoKTKT').val().length > 0 && $('#hdBaoCaoKTKT').val() == arrData[i][0])
                    $('#ddlDanhSach').append('<option value="'+arrData[i][0]+'" selected="selected">'+arrData[i][1]+'</option>');
                else
                    $('#ddlDanhSach').append('<option value="'+arrData[i][0]+'">'+arrData[i][1]+'</option>');
            }
        }
    }

    function Export(type) {
        if($('#ddlDanhSach option:selected').val() == "0") {
            alert('Phải chọn công ty kiểm toán đã được kiểm tra kỹ thuật');
            return;
        }
        $('#hdAction').val(type);
        $('#Form1').submit();
    }

//    function Paging() {
//        alert('123');
//        
//        $('#ctl00_maincontent_ctl00_CrystalReportViewer1 iframe').each(function (index, objIframe) {
//            var id = $(objIframe).prop('id');
//            if(index == 0) {
//                alert('456');
//                //alert($(objIframe).contents().find('.crystalstyle').prop('id'));
//                $(objIframe).contents().find('.crystalstyle').each(function (indexSub, objDiv) {
//                    alert(indexSub);
//                });
//            }
//        });
//    }
//    setTimeout('Paging()', 2000);
    <% SetDataJavascript();%>
</script>
<div id="DivDanhSachDoanKiemTra">
</div>
<div id="DivDanhSachCongTy">
</div>