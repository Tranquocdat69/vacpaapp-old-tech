﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_BangCauHoiKiemTra_ChonDoanKiemTra.ascx.cs" Inherits="usercontrols_KSCL_BaoCao_BangCauHoiKiemTra_ChonDoanKiemTra" %>

<style type="text/css">
    .tdTable
    {
        width: 120px;
    }
</style>
<form id="Form1" runat="server" method="post" enctype="multipart/form-data">
    <fieldset class="fsBlockInfor">
        <legend>Thông tin tìm kiếm</legend>
        <table width="100%" border="0" class="formtblInforWithoutBorder">
            <tr>
                <td style="width: 150px;">
                    Mã đoàn kiểm tra:
                </td>
                <td>
                    <asp:TextBox ID="txtMaDoanKiemTra" runat="server" Width="120px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    ID.HVTT/ID.CTKT:
                </td>
                <td>
                    <asp:TextBox ID="txtMaCongTy" runat="server" Width="100px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Tên công ty kiểm toán:
                </td>
                <td>
                    <asp:TextBox ID="txtTenCongTy" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </fieldset>
<div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearchPopup" runat="server" CssClass="btn" 
        onclick="lbtSearch_Click" OnClientClick="return CheckValidFormSearch();"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách công ty kiểm toán kiểm tra trực tiếp</legend>
    <table id="tblHocVienHeader" width="100%" border="0" class="formtbl">
        <tr>
            <th style="width: 30px;">
                
            </th>
            <th style="width: 50px;">
                STT
            </th>
            <th style="min-width: 150px;">
                Mã đoàn kiểm tra
            </th>
            <th style="width: 100px;">
                Ngày lập
            </th>
            <th style="width: 150px;">
                Quyết định thành lập
            </th>
        </tr>
    </table>
    <div id='DivListHocVien' style="max-height: 200px;">
        <table id="tblHocVien" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpHocVien" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="width: 30px; vertical-align: middle;">
                            <input type="radio" value='<%# Eval("ID") + ";#" + Eval("MaDoanKiemTra") %>' name="DanhSach" />
                        </td>
                        <td style="width: 50px; text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="min-width: 150px;">
                            <%# Eval("MaDoanKiemTra")%>
                        </td>
                        <td style="width: 100px;">
                            <%# Library.DateTimeConvert(Eval("NgayLap")).ToString("dd/MM/yyyy")%>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("QuyetDinhThanhLap")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</fieldset>
</form>
<script type="text/javascript">
    $("#<%= Form1.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtSearchPopup.ClientID %>').attr('href'));
        }
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm lấy thông tin học viên được chọn từ danh sách
    function ChooseDoanKiemTra() {
        var listId = '';
        var flag = false;
        $('#tblHocVien :radio').each(function () {
            var checked = $(this).prop("checked");
            if (checked) {
                listId = $(this).val();
                flag = true;
            }
        });
        if (!flag)
            alert('Phải chọn danh sách!');
        else {
            parent.DisplayInforDoanKiemTra(listId);
            parent.CloseFormDanhSachDoanKiemTra();
        }
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm kiểm tra trình duyệt để set thuộc tính overflow cho thẻ DIV hiển thị danh sách bản ghi
    // Chỉ trình duyệt nền Webkit mới có thuộc tính overflow: overlay
    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivListHocVien').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivListHocVien').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivListHocVien').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }

    function CheckValidFormSearch() {
        var isValid = $("#Form1").valid();
        if (isValid) {
            return true;
        } else {
            return false;
        }
    }

    myFunction();
</script>