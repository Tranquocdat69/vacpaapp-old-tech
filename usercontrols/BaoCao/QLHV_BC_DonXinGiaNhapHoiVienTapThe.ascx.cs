﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BC_DonXinGiaNhapHoiVienTapThe : System.Web.UI.UserControl
{
    protected string tenchucnang = "Đơn xin gia nhập hội viên tổ chức";
    //protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    //private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    //public Commons cm = new Commons();
    //public DataTable dt = new DataTable();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim() == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Trim().Contains(tem.idDoiTuong.ToString().Trim()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }
    
    public void setdataCrystalReport()
    {
        string VungMien = Request.Form["VungMienId"];
        string Tinh = Request.Form["TinhId"];
        string strID = Request.Form["txtID"];

        if (!string.IsNullOrEmpty(strID))
        {


            List<objDonXinGiaNhapHoiVienTapThe> lstBC = new List<objDonXinGiaNhapHoiVienTapThe>();
            
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = " select TenDoanhNghiep, TenVietTat, SoHieu, TenTiengAnh, DiaChi + ' - ' + dbo.LayDiaChi(a.DonHoiVienTapTheId) AS DiaChi, "
                                + " DienThoai, Fax, Email, Website, SoGiayChungNhanDKKD, NgayCapGiayChungNhanDKKD, "
                                + " SoGiayChungNhanKDDVKT, NgayCapGiayChungNhanKDDVKT, LoaiHinhDoanhNghiepID, "
                                + " MaSoThue, TenLinhVucHoatDong, NamDuDKKT_CK,NamDuDKKT_Khac, ThanhVienHangKTQT, "
                                + " TongSoNguoiLamViec, TongSoNguoiCoCKKTV, TongSoKTVDKHN, TongSoHoiVienCaNhan, "
                                + " NguoiDaiDienPL_Ten, c.TenChucVu as cv1, NguoiDaiDienPL_GioiTinh, NguoiDaiDienPL_NgaySinh, "
                                + " NguoiDaiDienPL_SoChungChiKTV, NguoiDaiDienPL_NgayCapChungChiKTV, "
                                + " NguoiDaiDienPL_DienThoaiCD, NguoiDaiDienPL_DiDong, NguoiDaiDienPL_Email, "
                                + " NguoiDaiDienLL_Ten, d.TenChucVu as cv2, NguoiDaiDienLL_NgaySinh, NguoiDaiDienLL_GioiTinh, "
                                + " NguoiDaiDienLL_DienThoaiCD, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email, NgayNopDon "
                                + " from tblDonHoiVienTapThe a  "
                                + " inner join dbo.tblDMLinhVucHoatDong b on a.LinhVucHoatDongID = b.LinhVucHoatDongID "
                                + " left join dbo.tblDMChucVu c on a.NguoiDaiDienPL_ChucVuID = c.ChucVuID "
                                + " left join dbo.tblDMChucVu d on a.NguoiDaiDienLL_ChucVuID = d.ChucVuID "
                                + " where SoDonHoiVienTapThe in ('" + strID + "') ";
            
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            

            //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
            //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
             int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                objDonXinGiaNhapHoiVienTapThe new1 = new objDonXinGiaNhapHoiVienTapThe();
                
                new1.SoHieu = tem["SoHieu"].ToString();
                new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
                new1.TenVietTat = tem["TenVietTat"].ToString();
                new1.TenTiengAnh = tem["TenTiengAnh"].ToString();
                new1.DiaChi = tem["DiaChi"].ToString();
                new1.DienThoai = tem["DienThoai"].ToString();
                new1.Fax = tem["Fax"].ToString();
                new1.Email = tem["Email"].ToString();
                new1.Website = tem["Website"].ToString();
                new1.SoDK = tem["SoGiayChungNhanDKKD"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayCapGiayChungNhanDKKD"].ToString()))
                    new1.NgayDK = NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanDKKD"]));
                new1.SoGiayCN = tem["SoGiayChungNhanKDDVKT"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayCapGiayChungNhanKDDVKT"].ToString()))
                    new1.NgayCN = NgayThangVN(Convert.ToDateTime(tem["NgayCapGiayChungNhanKDDVKT"]));
                if (tem["LoaiHinhDoanhNghiepID"].ToString() == "7")
                {
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";
                    new1.LoaiHinhDoanhNghiep1 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "2")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";
                    
                    new1.LoaiHinhDoanhNghiep2 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "5")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";
                    
                    new1.LoaiHinhDoanhNghiep3 = "X";
                }
                else if (tem["LoaiHinhDoanhNghiepID"].ToString() == "9")
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep4 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";
                    
                    new1.LoaiHinhDoanhNghiep5 = "X";
                }
                else
                {
                    new1.LoaiHinhDoanhNghiep1 = ".";
                    new1.LoaiHinhDoanhNghiep2 = ".";
                    new1.LoaiHinhDoanhNghiep5 = ".";
                    new1.LoaiHinhDoanhNghiep3 = ".";
                    
                    new1.LoaiHinhDoanhNghiep4 = "X";
                }

                new1.MaSoThue = tem["MaSoThue"].ToString();
                new1.LinhVucHoatDong = tem["TenLinhVucHoatDong"].ToString();
                new1.CoLoiIchNam = tem["NamDuDKKT_CK"].ToString();
                new1.LinhVucCKNam = tem["NamDuDKKT_Khac"].ToString();
                new1.LaThanhVienCuaHangKiemToan = tem["ThanhVienHangKTQT"].ToString();
                new1.TongSoNgLamViecTaiCty = tem["TongSoNguoiLamViec"].ToString();
                new1.TongSoNgCCKTV = tem["TongSoNguoiCoCKKTV"].ToString();
                new1.TongSoKTVDKHN = tem["TongSoKTVDKHN"].ToString();
                new1.TongSoNgLaHoiVienCaNhan = tem["TongSoHoiVienCaNhan"].ToString();

                new1.NguoiDaiDien_Ten = tem["NguoiDaiDienPL_Ten"].ToString();
                new1.NguoiDaiDien_ChucVu = tem["cv1"].ToString();
                new1.NgaySinh = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgaySinh"])) : "";
                if (tem["NguoiDaiDienPL_GioiTinh"].ToString() == "1")
                {
                    new1.GioiTInhNu = ".";
                    new1.GioiTInhNam = "X";
                }
                else
                {
                    new1.GioiTInhNam = ".";
                    new1.GioiTInhNu = "X";
                }
                new1.NguoiDaiDien_SoCCKTV = tem["NguoiDaiDienPL_SoChungChiKTV"].ToString();
                new1.NguoiDaiDien_NgayCCKTV = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgayCapChungChiKTV"])) : "";
                new1.NguoiDaiDien_DienThoai = tem["NguoiDaiDienPL_DienThoaiCD"].ToString();
                new1.NguoiDaiDien_mobile = tem["NguoiDaiDienPL_DiDong"].ToString();
                new1.NguoiDaiDien_Email = tem["NguoiDaiDienPL_Email"].ToString();

                new1.NguoiDaiDienLL_Ten = tem["NguoiDaiDienLL_Ten"].ToString();
                new1.NguoiDaiDienLL_ChuVu = tem["cv2"].ToString();
                new1.NguoiDaiDienLL_NGaySinh = !string.IsNullOrEmpty(tem["NguoiDaiDienLL_NgaySinh"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienLL_NgaySinh"])) : "" ;
                if (tem["NguoiDaiDienLL_GioiTinh"].ToString() == "1")
                {
                    new1.NguoiDaiDienLL_GioiTinhNu = ".";
                    new1.NguoiDaiDienLL_GioiTinhNam = "X";
                }
                else
                {
                    new1.NguoiDaiDienLL_GioiTinhNam = ".";
                    new1.NguoiDaiDienLL_GioiTinhNu = "X";
                }
                new1.NguoiDaiDienLL_DienThoai = tem["NguoiDaiDienLL_DienThoaiCD"].ToString();
                new1.NguoiDaiDien_Email = tem["NguoiDaiDienPL_Email"].ToString();
                new1.NguoiDaiDienLL_mobile = tem["NguoiDaiDienLL_DiDong"].ToString();
                new1.NguoiDaiDienLL_Email = tem["NguoiDaiDienLL_Email"].ToString();
                new1.SoQD = tem["NguoiDaiDienPL_SoChungChiKTV"].ToString();
                new1.NgayKyQD = !string.IsNullOrEmpty(tem["NguoiDaiDienPL_NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NguoiDaiDienPL_NgayCapChungChiKTV"])) : "";
                new1.NgayThangNam = "Hà Nội, ngày " + (Convert.ToDateTime(tem["NgayNopDon"]).Day.ToString().Length == 1 ? "0" + Convert.ToDateTime(tem["NgayNopDon"]).Day : Convert.ToDateTime(tem["NgayNopDon"]).Day.ToString()) + " tháng " + (Convert.ToDateTime(tem["NgayNopDon"]).Month.ToString().Length == 1 ? "0" + Convert.ToDateTime(tem["NgayNopDon"]).Month : Convert.ToDateTime(tem["NgayNopDon"]).Month.ToString()) + " năm " + Convert.ToDateTime(tem["NgayNopDon"]).Year;
                lstBC.Add(new1);

                
            }

            //TenBaoCao newabc = new TenBaoCao();
            //newabc.TongCong = "Tổng cộng: " + i.ToString();
            //newabc.NgayTinh = "Tính từ " + NgayBatDau.Value.ToShortDateString() + " - " + NgayKetThuc.Value.ToShortDateString();
            //newabc.ngayBC = "Hà nội, ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            //lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/DonXinGiaNhapHoiVienTapThe.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objDonXinGiaNhapHoiVienTapThe"].SetDataSource(lstBC);
            //rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DonXinGiaNhapHoiVienTapThe");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DonXinGiaNhapHoiVienTapThe");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            
        }
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
          //  id.Text = result;

            //if (lstIdChon.Count() != 0)
            //{
            //    DataSet ds = new DataSet();
            //    SqlCommand sql = new SqlCommand();
            //    sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
            //            + " from dbo.tblHoiVienCaNhan "
            //            + " WHERE HoiVienCaNhanID = " + lstIdChon[0];// + id;
            //    ds = DataAccess.RunCMDGetDataSet(sql);
            //    sql.Connection.Close();
            //    sql.Connection.Dispose();
            //    sql = null;

            //    DataTable dt = ds.Tables[0];
            //    foreach (DataRow tem in dt.Rows)
            //        txtTen.Text = tem["HoTen"].ToString();
            //}
            //CrystalReportViewer1.ReportSource = null;
            //CrystalReportViewer1.DataBind();
        }
    }

    public void DonVi(string ma = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (ma == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    protected void btnChay2_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            idDanhSach.Text = result;

            //if (lstIdChon.Count() != 0)
            //{
            //    DataSet ds = new DataSet();
            //    SqlCommand sql = new SqlCommand();
            //    sql.CommandText = "select TenDoanhNghiep "
            //            + " from dbo.tblHoiVienTapThe "
            //            + " WHERE MaHoiVienTapThe = '" + lstIdChon[0] + "'";// + id;
            //    ds = DataAccess.RunCMDGetDataSet(sql);
            //    sql.Connection.Close();
            //    sql.Connection.Dispose();
            //    sql = null;

            //    DataTable dt = ds.Tables[0];
            //    foreach (DataRow tem in dt.Rows)
            //        txtCongTy.Text = tem["TenDoanhNghiep"].ToString();
            //}
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }
}