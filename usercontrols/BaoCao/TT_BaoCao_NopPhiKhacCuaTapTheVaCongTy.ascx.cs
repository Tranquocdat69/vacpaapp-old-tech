﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
public partial class usercontrols_TT_BaoCao_NopPhiHoiVienTapThe : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo nộp phí khác của hội viên tổ chức và công ty kiểm toán";

    private Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        DataBaseDb db = new DataBaseDb();
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            
            string strIDVungMien = string.Empty;
            string strIDTinhThanh = string.Empty;
            string strConNo = string.Empty;
            string strKhongConNo = string.Empty;
            string strNgayChotSL = "";
            if (!string.IsNullOrEmpty(Request.Form["VungMien"]) && Request.Form["VungMien"] != "-1")
            {
                strIDVungMien = Request.Form["VungMien"];
            }
            if (!string.IsNullOrEmpty(Request.Form["TinhThanh"]) && Request.Form["TinhThanh"] != "-1")
            {
                strIDTinhThanh = Request.Form["TinhThanh"];
            }
            if (!string.IsNullOrEmpty(Request.Form["cbConNoPhi"]))
            {
                strConNo = "yes";
            }
            if (!string.IsNullOrEmpty(Request.Form["cbKhongConNoPhi"]))
            {
                strKhongConNo = "yes";
            }
            //if (!string.IsNullOrEmpty(Request.Form["hdHocVienID"]))
            //{
            //    strIDHoiVien = Request.Form["hdHocVienID"];
            //}
            if (!string.IsNullOrEmpty(Request.Form["txtNgayLapBC"]))
            {
                strNgayChotSL = Request.Form["txtNgayLapBC"];
                DateTime dt = Library.DateTimeConvert(strNgayChotSL, "dd/MM/yyyy");
                string[] paraName = { "@VungMienID", "@TinhThanhID", "@NgayChotSoLieu", "@ConNo", "@KhongConNo" };
                object[] paraValue = { strIDVungMien, strIDTinhThanh, dt, strConNo, strKhongConNo };

                DataSet ds = new DataSet();
                DataTable dt1 = new DataTable();

                ds = db.GetData(paraValue, paraName, "proc_BAOCAO_THANHTOAN_NOPHOIPHIKHACTAPTHECONGTYKIEMTOAN", "NopPhiTapThe");

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/ThanhToanRpts/rptBaoCaoNopPhiTapThe.rpt"));
                ((TextObject)rpt.ReportDefinition.ReportObjects["lblHeader"]).Text = "BÁO CÁO NỘP PHÍ KHÁC CỦA HỘI VIÊN TẬP THỂ VÀ CÔNG TY KIỂM TOÁN";
                ((TextObject)rpt.ReportDefinition.ReportObjects["lblHoiPhiNoCacNamTruoc"]).Text = "Phí khác còn nợ các năm trước";
                ((TextObject)rpt.ReportDefinition.ReportObjects["lblPhiPhaiNopNamNay"]).Text = "Phí khác phải nộp trong năm nay";
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtThoiGian"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLapBaoCao"]).Text = cm.Admin_HoVaTen;
                ((TextObject)rpt.ReportDefinition.ReportObjects["lblTongHoiPhi"]).Text = "Số phí đã nộp";
                ((TextObject)rpt.ReportDefinition.ReportObjects["lblSoHoiPhiConNo"]).Text = "Số phí còn nợ";
                rpt.SetDataSource(ds);

                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoNopPhiKhacCuaHVTTVaCongTyKiemToan");
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(ds.Tables[0]);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BaoCaoNopPhiKhacCuaHVTTVaCongTyKiemToan");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoNopPhiKhacCuaHVTTVaCongTyKiemToan");
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            //  _db.CloseConnection();
        }
        finally
        {
            // _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string js = "";
        if (!string.IsNullOrEmpty(Request.Form["txtNgayLapBC"]))
            js = "$('#txtNgayLapBC').val('" + Request.Form["txtNgayLapBC"] + "');" + Environment.NewLine;
        else
            js = "$('#txtNgayLapBC').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
        js += "$('#VungMien').val('" + Request.Form["VungMien"] + "');" + Environment.NewLine;
        js += "$('#TinhThanh').val('" + Request.Form["TinhThanh"] + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["cbConNoPhi"]))
            js += "$('#cbConNoPhi').prop('checked', true);" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["cbKhongConNoPhi"]))
            js += "$('#cbKhongConNoPhi').prop('checked', true);" + Environment.NewLine;
        Response.Write(js);
    }

    public void LoadVungMien()
    {
        try
        {
            string ma = "";
            DataBaseDb db = new DataBaseDb();
            DataTable dt = db.GetData("select * from tblDMVungMien");
            if (!string.IsNullOrEmpty(Request.Form["VungMien"]))
                ma = Request.Form["VungMien"];
            string _outputhtml = "";
            if (ma != "")
                _outputhtml += "<option value=\"-1\">Tất cả</option>";
            else
                _outputhtml += @"<option selected=""selected"" value=""-1"">Tất cả</option>";

            foreach (DataRow dr in dt.Rows)
            {
                if (ma == dr["VungMienID"].ToString())
                    _outputhtml += "<option selected=\"selected\" value=\"" + dr["VungMienID"] + "\">" + dr["TenVungMien"] + "</option>";
                else
                    _outputhtml += "<option  value=\"" + dr["VungMienID"] + "\">" + dr["TenVungMien"] + "</option>";
            }
            Response.Write(_outputhtml);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void LoadTinhThanh()
    {
        try
        {
            string ma = "";
            string vungmien = "";
            if (!string.IsNullOrEmpty(Request.Form["VungMien"]))
                vungmien = Request.Form["VungMien"];
            DataBaseDb db = new DataBaseDb();
            string sql = "select * from tblDMTinh";
            if (!string.IsNullOrEmpty(vungmien) && vungmien != "-1")
                sql += " where  VungMienID in (" + vungmien + ")";

            DataTable dt = db.GetData(sql);
            if (!string.IsNullOrEmpty(Request.Form["TinhThanh"]))
                ma = Request.Form["TinhThanh"];
            string _outputhtml = "";
            if (ma != "")
                _outputhtml += "<option value=\"-1\"><< Tất cả >></option>";
            else
                _outputhtml += @"<option selected=""selected"" value=""-1""><< Tất cả >></option>";
            foreach (DataRow Tinh in dt.Rows)
            {
                if (ma.Trim() == Tinh["MaTinh"].ToString().Trim())
                {
                    _outputhtml += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
                }
                else
                {
                    _outputhtml += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
                }
            }
            Response.Write(_outputhtml);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void ExportExcel(DataTable dt)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }
        
        .HeaderColumn1
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='10' style='height: 80px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='10' style='text-align:center;font-size: 14pt; font-weight: bold;'>BÁO CÁO NỘP PHÍ KHÁC CỦA HỘI VIÊN TẬP THỂ VÀ CÔNG TY KIỂM TOÁN</td>
            </tr>            
            <tr><td colspan='10'></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn1'>STT</td>                
                <td class='HeaderColumn1' style='width: 100px;'>Số hiệu công ty</td>
                <td class='HeaderColumn1' style='width: 150px;'>Tên công ty</td>
                <td class='HeaderColumn1' style='width: 100px;'>Hội viên VACPA</td>
                <td class='HeaderColumn1' style='width: 100px;'>Ngày thành lập</td>
                <td class='HeaderColumn1' style='width: 100px;'>Người đại diện (giám đốc)</td>
                <td class='HeaderColumn1' style='width: 100px;'>Địa chỉ</td>
                <td class='HeaderColumn1' style='width: 100px;'>Email</td>
                <td class='HeaderColumn1' style='width: 100px;'>Phí khác còn nợ các năm trước</td>
                <td class='HeaderColumn1' style='width: 100px;'>Phí khác phải nộp trong năm nay</td>
                <td class='HeaderColumn1' style='width: 100px;'>Số phí đã nộp</td>
                <td class='HeaderColumn1' style='width: 100px;'>Số phí còn nợ</td>                
                            </tr>";

        double phiPhaiNopNamNay = 0,
               hoiPhiConNoCacNamTruoc = 0,
               tongHoiPhiDaNop = 0,
               soPhiConNo = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            htmlExport += "<tr><td class='Column'>" + (i + 1) + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoHieu"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenDoanhNghiep"] + "</td>" +
                          "<td class='Column'></td>" +
                          "<td class='Column'>" + (!string.IsNullOrEmpty(dt.Rows[i]["NgayThanhLap"].ToString()) ? Library.DateTimeConvert(dt.Rows[i]["NgayThanhLap"]).ToString("dd/MM/yyyy") : "") + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NguoiDaiDien"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DiaChi"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["Email"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["HoiPhiConNoCacNamTruoc"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["PhiPhaiNopNamNay"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TongHoiPhiDaNop"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoPhiConNo"] + "</td></tr>";
            phiPhaiNopNamNay += Library.DoubleConvert(dt.Rows[i]["PhiPhaiNopNamNay"]);
            hoiPhiConNoCacNamTruoc += Library.DoubleConvert(dt.Rows[i]["HoiPhiConNoCacNamTruoc"]);
            tongHoiPhiDaNop += Library.DoubleConvert(dt.Rows[i]["TongHoiPhiDaNop"]);
            soPhiConNo += Library.DoubleConvert(dt.Rows[i]["SoPhiConNo"]);
        }
        htmlExport += "<tr><td></td><td></td><td style='font-weight: bold;'>Tổng số</td>" +
                      "<td></td><td></td><td></td><td></td><td></td>" +
                      "<td style='font-weight: bold;'>" + hoiPhiConNoCacNamTruoc + "</td>" +
                      "<td style='font-weight: bold;'>" + phiPhaiNopNamNay + "</td>" +
                      "<td style='font-weight: bold;'>" + tongHoiPhiDaNop + "</td>" +
                      "<td style='font-weight: bold;'>" + soPhiConNo + "</td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='6'></td><td colspan='4' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td colspan='4' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td colspan='4' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("BaoCaoNopPhiKhacCuaHVTTVaCongTyKiemToan.xsl", "<html>" + css + htmlExport + "</html>");

    }
}