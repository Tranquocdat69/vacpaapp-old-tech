﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_BaoCao_BangCauHoiKiemTra_ChonCongTy : System.Web.UI.UserControl
{
    private string _nam = "", _idDoanKiemTra = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        _nam = Library.CheckNull(Request.QueryString["nam"]);
        _idDoanKiemTra = Library.CheckNull(Request.QueryString["iddkt"]);
        if (!IsPostBack)
            LoadListCongTy();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Load danh sách "Công ty" theo điều kiện Search
    /// </summary>
    private void LoadListCongTy()
    {
        Db db = new Db(ListName.ConnectionString);
        try
        {
            db.OpenConnection();
            string query = @"SELECT HVTC.HoiVienTapTheID, HVTC.MaHoiVienTapThe, HVTC.TenDoanhNghiep, HVTC.TenVietTat, HVTC.DiaChi, HVTC.NguoiDaiDienLL_Ten
	                         FROM tblKSCLDoanKiemTraCongTy AS DKT_CT	                         
	                         LEFT JOIN tblHoiVienTapThe HVTC ON DKT_CT.HoiVienTapTheID = HVTC.HoiVienTapTheID
	                         WHERE 
		                        DKT_CT.DoanKiemTraID = " + _idDoanKiemTra + @"
		                        AND HVTC.MaHoiVienTapThe like '%" + txtMaCongTy.Text + @"%'
		                        AND (HVTT.TenDoanhNghiep like N'%" + txtTenCongTy.Text + @"%' OR HVTC.TenVietTat like N'%" + txtTenCongTy.Text + @"%' OR HVTC.TenTiengAnh like N'%" + txtTenCongTy.Text + @"%')	                        		                        
	                         ORDER BY HVTC.TenDoanhNghiep DESC";
            DataTable dt = db.GetDataTable(query);
            rpCongTy.DataSource = dt.DefaultView;
            rpCongTy.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadListCongTy();
    }
}