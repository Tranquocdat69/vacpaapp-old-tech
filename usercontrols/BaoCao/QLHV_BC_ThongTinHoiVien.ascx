﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QLHV_BC_ThongTinHoiVien.ascx.cs" Inherits="usercontrols_QLHV_BC_ThongTinHoiVien" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<%--<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>--%>

<style type="text/css">
   
    .ui-dropdownchecklist {
        background: #ffffff !important;
        border: solid 1PX #cccccc!important;
        margin: 0px;
  height: 22px;
  width:152px;
  -webkit-appearance: menulist;
  box-sizing: border-box;
  border: 1px solid;
  border-image-source: initial;
  border-image-slice: initial;
  border-image-width: initial;
  border-image-outset: initial;
  border-image-repeat: initial;
  white-space: pre;
  -webkit-rtl-ordering: logical;
  color: black;
  background-color: white;
  cursor: default;
    }
    .ui-dropdownchecklist span {
        width:100%;
    }
    .ui-dropdownchecklist span span{
        width:100%;
    }
</style>

<script type="text/javascript" src="js/ui.dropdownchecklist-1.5-min.js"></script>
<%--<script type="text/javascript" src="js/jquery-ui.min.js"></script>--%>

<form id="Form1" runat="server" clientidmode="Static">
<asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<h4 class="widgettitle">
    Thông tin cá nhân kiểm toán viên</h4>
<fieldset class="fsBlockInfor" style="margin-top: 0px;">
    <legend>Thông tin cá nhân kiểm toán viên</legend>
    <table width="100%" border="0" class="formtbl">
    <tr>
        <td width="20%"></td>
        <td width="13%">Vùng miền:</td>
        <td width="13%"> <select name="VungMienId" id ="VungMienId" style="width: 152px" multiple="multiple" >
            <%  
              try
              {
                  VungMien(Request.Form["VungMienId"].ToString());
              }
              catch 
              {
                  VungMien("00");
              }
              
              %>
        </select>
        </td>
        
        <td width="8%"></td>
        <td width="13%">Tỉnh/thành</td>
        <td width="13%"><select name="TinhId" id ="TinhId" style="width: 152px">
            <%  
              try
              {
                  Load_ThanhPho(Request.Form["TinhId"].ToString());
              }
              catch 
              {
                  Load_ThanhPho("00");
              }
              
              %>
        </select></td>
        <td width="20%"></td>
    </tr>
    <tr>
        <td width="20%"></td>
        <td width="13%">Đơn vị công tác:</td>
        <td width="13%"><asp:TextBox name="idDanhSach" 
                id="idDanhSach" runat ="server"
                AutoPostBack = "true" OnTextChanged = "Text_Changed_2"
                Width="152px" /> </td>
        <td width="8%"><a id="a1" onclick="open_ChonID_2()" data-placement="top" data-rel="tooltip" href="#none" data-original-title="..."  rel="tooltip" class="btnabc">
            <img src="images/icons/search.png" alt="" /></a></td>
        <td width="18%">Tên đơn vị công tác:</td>
        <td width="13%"><asp:TextBox runat="server" ID="txtCongTy" Enabled="false"></asp:TextBox></td>
        <td width="20%"></td>
    </tr>
    <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  >
       <ContentTemplate>
    <tr>
        <td width="20%"></td>
        <td width="13%">ID <span style="color:Red">(*)</span>:</td>
        <td width="13%"><asp:TextBox runat="server" ID="id" OnTextChanged="Text_Changed" AutoPostBack="true"></asp:TextBox> </td>
        <td width="8%"><a id="akickChon" onclick="open_ChonID()" data-placement="top" data-rel="tooltip" href="#none" data-original-title="..."  rel="tooltip" class="btnabc">
            <img src="images/icons/search.png" alt="" /></a></td>
        <td width="13%">Họ và tên:</td>
        <td width="13%"><asp:TextBox runat="server" ID="txtTen" Enabled = "false"></asp:TextBox></td>
        <td width="20%"></td>
    </tr>
        </ContentTemplate></asp:UpdatePanel>
    <%--<tr>
        <td width="20%"></td>
        <td width="13%">Định dạng file: </td>
        <td colspan="4"> <select id="idFile" name="idFile" style="width: 152px">
            <%DinhDangFile(); %>
        </select></td>
        <td width="20%"></td>
    </tr>--%>
    </table>

    <div class="dataTables_length" style="text-align: center;">
        <a id="btnSave" href="javascript:;" class="btn" onclick="View();"><i class="iconfa-plus-sign">
             </i>Xem thông tin</a> 
        
        <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="View('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a>
                 <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="View('pdf');">
                    <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                    <input type="hidden" id="hdAction" name="hdAction"/>
    </div>
</fieldset>

 <fieldset class="fsBlockInfor">
        <legend>Thông tin cá nhân kiểm toán viên</legend>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False" HasExportButton="False" 
            HasPrintButton="False"/>
    </fieldset>

      <div id="div_dmNhanSuHoiVien_add"></div>

    <div style = "display: none">
    <asp:LinkButton ID="btnChay" runat="server" CssClass="btn" 
        onclick="btnChay_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>

    <div style = "display: none">
    <asp:LinkButton ID="btnChay2" runat="server" CssClass="btn" 
        onclick="btnChay2_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>
</form>

<script type="text/javascript">
    function open_ChonID() {
        var dvId = document.getElementById('<%=idDanhSach.ClientID%>').value;
        //alert(dvId);
        var vmId = $('#VungMienId').val();
        var tId = document.getElementById("TinhId").value;
       // var ccKTV = document.getElementById("txtChungChiKTV").value;
        
            var timestamp = Number(new Date());
            $("#div_dmNhanSuHoiVien_add").empty();
            $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoivien' name='iframe_hoivien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dbChonHoiVienCaNhan_BC&type=canhan&VMID=" + vmId + "&TID=" + tId + "&DVID=" + dvId +"&mode=iframe&time=" + timestamp));
            $("#div_dmNhanSuHoiVien_add").dialog({
                autoOpen: false,
                title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách hội viên</b>",
                modal: true,
                width: "640",
                height: "480",
                buttons: [
                             {
                                 text: "Chọn",
                                 click: function () {
                                     formChon();
                                 }
                             },
                                {
                                    text: "Đóng",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                        ]
            }).dialog("open");
        
    }
</script>

<script type="text/javascript">
    function open_ChonID_2() {
        var dvId = "null";
        var vmId = $('#VungMienId').val();

        var tId = document.getElementById("TinhId").value;


        var timestamp = Number(new Date());
        $("#div_dmNhanSuHoiVien_add").empty();
        $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoivien' name='iframe_hoivien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dbChonHoiVienCaNhan_BC&type=tapthe&VMID=" + vmId + "&TID=" + tId + "&DVID=" + dvId + "&mode=iframe&time=" + timestamp));
        $("#div_dmNhanSuHoiVien_add").dialog({
            autoOpen: false,
            title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách hội viên</b>",
            modal: true,
            width: "640",
            height: "480",
            buttons: [
                             {
                                 text: "Chọn",
                                 click: function () {
                                     formChon_2();
                                 }
                             },
                                {
                                    text: "Đóng",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                        ]
        }).dialog("open");

    }
</script>


<script type="text/javascript">
    function formChon() {
        $("#div_dmNhanSuHoiVien_add").dialog('close');
        document.getElementById('<%= btnChay.ClientID %>').click();
    }
</script>

<script type="text/javascript">
    function formChon_2() {
        $("#div_dmNhanSuHoiVien_add").dialog('close');
        document.getElementById('<%= btnChay2.ClientID %>').click();
    }
</script>

<script type="text/javascript">
    function View(type) {
        var idHV = document.getElementById('<%=id.ClientID%>').value;
        if (idHV == "") {
            alert("Bạn chưa chọn ID!")
            return;
        }
        //        <%setdataCrystalReport(); %>
        $('#hdAction').val(type);
        $('#Form1').submit();
    }

    var timestamp = Number(new Date());
    $('#VungMienId').change(function () {
        $('#TinhId').html('');
        $('#TinhId').load('noframe.aspx?page=chondiaphuong&default=0&type=tt&id_vungmien=' + $('#VungMienId').val() + '&time=' + timestamp);
    });

</script>


<script type="text/javascript">
    $("#VungMienId").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
</script>
