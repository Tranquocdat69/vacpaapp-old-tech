﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien_ChonHoiVienCaNhan.ascx.cs" Inherits="usercontrols_KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien_ChonHoiVienCaNhan" %>

<style type="text/css">
    .tdTable
    {
        width: 120px;
    }
</style>
<form id="FormDanhSachGiangVien" runat="server" method="post" enctype="multipart/form-data">
<fieldset class="fsBlockInfor">
    <legend>Tiêu chí tìm kiếm</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td>
                ID<span class="starRequired">(*)</span>:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtMaHocVien" runat="server"></asp:TextBox>
            </td>
            <td>
                Họ và tên:
            </td>
            <td class="tdTable">
                <asp:TextBox ID="txtHoTen" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Phân loại:</td>
            <td>
                <asp:DropDownList ID="ddlPhanLoai" runat="server" Width="120px">
                    <asp:ListItem Value="">Tất cả</asp:ListItem>
                    <asp:ListItem Value="1">Hội viên</asp:ListItem>
                    <asp:ListItem Value="2">Kiểm toán viên</asp:ListItem>
                    <asp:ListItem Value="0">Người quan tâm</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearchPopup" runat="server" CssClass="btn" 
        onclick="lbtSearch_Click" ><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách nhân viên</legend>
    <table id="tblHocVienHeader" width="100%" border="0" class="formtbl">
        <tr>
            <th style="width: 30px;">
                
            </th>
            <th style="width: 50px;">
                STT
            </th>
            <th style="width: 100px;">
                ID
            </th>
            <th style="min-width: 150px;">
                Họ và tên
            </th>
            <th style="width: 100px;">
                Phân loại
            </th>
            <th style="width: 150px;">
                Số chứng chỉ KTV
            </th>
            <th style="width: 100px;">
                Ngày cấp chứng chỉ KTV
            </th>
        </tr>
    </table>
    <div id='DivListHocVien' style="max-height: 200px;">
        <table id="tblHocVien" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpHocVien" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="width: 30px; vertical-align: middle;">
                            <input type="radio" value='<%# Eval("HoiVienCaNhanID") + "," + Eval("MaHoiVienCaNhan") %>' name="rbHoiVienCaNhan" />
                        </td>
                        <td style="width: 50px; text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("MaHoiVienCaNhan")%>
                        </td>
                        <td style="min-width: 150px;">
                            <%# Eval("FullName")%>
                        </td>
                        <td style="width: 100px;">
                            <%# GetLoaiHocVien(Eval("LoaiHoiVienCaNhan").ToString()) %>
                        </td>
                        <td style="width: 150px;">
                            <%# Eval("SoChungChiKTV")%>
                        </td>
                        <td style="width: 100px;">
                            <%# !string.IsNullOrEmpty(Eval("NgayCapChungChiKTV").ToString()) ? Library.DateTimeConvert(Eval("NgayCapChungChiKTV")).ToString("dd/MM/yyyy") : ""%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</fieldset>
<%--<div style="width: 100%; text-align: right; margin-top: 5px;">
    <a id="btnChoose" href="javascript:;" class="btn" onclick="ChooseHocVien();"><i
        class="iconfa-ok"></i>Chọn</a> <a id="btnExit" href="javascript:;" class="btn" onclick="parent.CloseFormDanhCaNhan();">
            <i class="iconfa-stop"></i>Đóng</a>
</div>--%>
</form>
<script type="text/javascript">
    $("#<%= FormDanhSachGiangVien.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtSearchPopup.ClientID %>').attr('href'));
        }
    });

    jQuery(document).ready(function () {
        $("#tblHocVienHeader .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#tblHocVien :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm lấy thông tin học viên được chọn từ danh sách
    function ChooseHocVien() {
        var listId = '';
        var flag = false;
        $('#tblHocVien :radio').each(function () {
            var checked = $(this).prop("checked");
            if (checked) {
                if (checked) {
                    listId = $(this).val();
                    flag = true;
                }
            }
        });
        // Bắt buộc phải chọn 1 học viên -> nếu ko thì bật cảnh báo
        if (!flag)
            alert('Phải chọn học viên!');
        else {
            parent.DisplayInforCaNhan(listId);
            parent.CloseFormDanhCaNhan();
        }
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm kiểm tra trình duyệt để set thuộc tính overflow cho thẻ DIV hiển thị danh sách bản ghi
    // Chỉ trình duyệt nền Webkit mới có thuộc tính overflow: overlay
    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivListHocVien').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivListHocVien').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivListHocVien').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }

    myFunction();
</script>