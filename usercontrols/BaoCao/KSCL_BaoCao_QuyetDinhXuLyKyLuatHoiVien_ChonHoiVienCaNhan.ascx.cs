﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien_ChonHoiVienCaNhan : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _congTyId = "", _idDsKiemTraTrucTiep = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //this._congTyId = Request.QueryString["idct"];
            this._idDsKiemTraTrucTiep = Request.QueryString["idds"];
            _db.OpenConnection();
            if (!IsPostBack)
            {
                LoadListHocVien();
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Load danh sach "Hoi vien" theo dieu kien search
    /// </summary>
    private void LoadListHocVien()
    {
        try
        {
            _db.OpenConnection();
            string query = @"SELECT HVCN.HoiVienCaNhanID, (HVCN.HoDem + ' ' + HVCN.Ten) Fullname,  HVCN.MaHoiVienCaNhan, HVCN.LoaiHoiVienCaNhan, HVCN.SoChungChiKTV, HVCN.NgayCapChungChiKTV
                                FROM tblKSCLXuLySaiPhamChiTiet XLSPCT           
                                INNER JOIN tblKSCLXuLySaiPham XLSP ON XLSPCT.XuLySaiPhamID = XLSP.XuLySaiPhamID
                                INNER JOIN tblHoiVienCaNhan HVCN ON XLSPCT.HoiVienCaNhanID = HVCN.HoiVienCaNhanID          
                                WHERE XLSPCT.HoiVienCaNhanID IS NOT NULL AND XLSP.DSKiemTraTrucTiepID = " + _idDsKiemTraTrucTiep + @"
	                            AND {MaHoiVienCaNhan} AND (HVCN.HoDem + ' ' + HVCN.Ten LIKE N'%" + txtHoTen.Text + @"%')
	                            AND {LoaiHocVien}  ORDER BY FullName";

            query = !string.IsNullOrEmpty(txtMaHocVien.Text)
                        ? query.Replace("{MaHoiVienCaNhan}", "HVCN.MaHoiVienCaNhan like '%" + txtMaHocVien.Text + "%'")
                        : query.Replace("{MaHoiVienCaNhan}", "1=1");
            query = ddlPhanLoai.SelectedValue != ""
                        ? query.Replace("{LoaiHocVien}", "HVCN.LoaiHoiVienCaNhan = '" + ddlPhanLoai.SelectedValue + "'")
                        : query.Replace("{LoaiHocVien}", "1=1");
            DataTable dt = _db.GetDataTable(query);
            rpHocVien.DataSource = dt.DefaultView;
            rpHocVien.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }


    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadListHocVien();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Get chuoi ten loai hoc vien dua vao so loai hoc vien lay tu DB
    /// </summary>
    /// <param name="value">So loai hoc vien</param>
    /// <returns>Ten loai hoc vien</returns>
    protected string GetLoaiHocVien(string value)
    {
        switch (value)
        {
            case ListName.Type_LoaiHocVien_NguoiQuanTam:
                return "Người quan tâm";
            case ListName.Type_LoaiHocVien_HoiVien:
                return "Hội viên";
            case ListName.Type_LoaiHocVien_KiemToanVien:
                return "Kiểm toán viên";
        }
        return "";
    }
}