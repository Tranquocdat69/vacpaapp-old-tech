﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QLHV_BC_QuyetDinhKetNapHoiVienTapThe.ascx.cs" Inherits="usercontrols_QLHV_BC_QuyetDinhKetNapHoiVienTapThe" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<%--<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>--%>

<style type="text/css">
   
    .ui-dropdownchecklist {
        background: #ffffff !important;
        border: solid 1PX #cccccc!important;
        margin: 0px;
  height: 22px;
  width:152px;
  -webkit-appearance: menulist;
  box-sizing: border-box;
  border: 1px solid;
  border-image-source: initial;
  border-image-slice: initial;
  border-image-width: initial;
  border-image-outset: initial;
  border-image-repeat: initial;
  white-space: pre;
  -webkit-rtl-ordering: logical;
  color: black;
  background-color: white;
  cursor: default;
    }
    .ui-dropdownchecklist span {
        width:100%;
    }
    .ui-dropdownchecklist span span{
        width:100%;
    }
</style>

<script type="text/javascript" src="js/ui.dropdownchecklist-1.5-min.js"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<%--<script type="text/javascript" src="js/jquery-ui.min.js"></script>--%>

<form id="Form1" runat="server" clientidmode="Static">
<asp:HiddenField ID="hi_LoaiHinh" runat="server" />
<asp:HiddenField ID="hi_TinhThanh" runat="server" />
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<h4 class="widgettitle">
    Quyết định kết nạp hội viên tổ chức</h4>
<fieldset class="fsBlockInfor" style="margin-top: 0px;">
    <legend>Quyết định kết nạp hội viên tổ chức</legend>
    <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
    
    <table width="100%" border="0" class="formtbl">
    <tr>
        <td width="15%"></td>
        <td width="18%">Vùng miền:</td>
        <td width="13%"> <select name="VungMienId" id ="VungMienId" style="width: 152px" multiple="multiple" >
            <%  
              try
              {
                  VungMien(Request.Form["VungMienId"].ToString());
              }
              catch 
              {
                  VungMien("00");
              }
              
              %>
        </select></td>
        <td width="5%"></td>
        <td width="18%">Tỉnh/Thành:</td>
        <td width="13%"> <select name="TinhId" id ="TinhId" style="width: 152px" >
            <%  
              try
              {
                  Load_ThanhPho(Request.Form["TinhId"].ToString());
              }
              catch 
              {
                  Load_ThanhPho("00");
              }
              
              %>
        </select></td>
        <%--<td width="16%"><input type="text" name="NgayQuyetDinh" id="NgayQuyetDinh" value="" 
                style="width: 149px" /></td>--%>
        <td ></td>
        
    </tr>
    <tr>
        <td width="15%"></td>
        <td width="18%">Đơn vị:</td>
        <td width="13%"><asp:TextBox name="idDanhSach" 
                id="idDanhSach" runat ="server"
                AutoPostBack = "true"
                Width="152px" /> </td>
        <td width="5%"><a id="a1" onclick="open_ChonID_2()" data-placement="top" data-rel="tooltip" href="#none" data-original-title="..."  rel="tooltip" class="btnabc">
            <input type="button" value = "..." id="Button1" name="btChonID" 
                style="width: 49px" /></a></td>

        <td width="18%">Tên đơn vị công tác:</td>
        <td width="13%"><asp:TextBox runat="server" ID="txtCongTy" Enabled="false"></asp:TextBox></td>
        <td></td>
    </tr>
   <%-- <tr>
        <td width="15%"></td>
        <td width="18%">Ngày kết nạp:</td>
        <td width="13%"><input type="text" name="NgayBatDau" id="NgayBatDau" value="<%=Request.Form["NgayBatDau"] %>" 
                style="width: 149px" />
        
        </td>
        <td width="5%">-</td>
        <td width="13%"><input type="text" name="NgayKetThuc" id="NgayKetThuc" value="<%=Request.Form["NgayKetThuc"] %>"  /></td>
        <td colspan="2"></td>
        
    </tr>--%>
    <%--<tr>
        <td width="15%"></td>
        <td width="18%">Ngày quyết định <span style="color:Red">(*)</span></td>
        <td width="16%"><input type="text" name="NgayQuyetDinh" id="NgayQuyetDinh" value="<%=Request.Form["NgayQuyetDinh"] %>" 
                style="width: 149px" /></td>
        <td colspan="4"></td>
    </tr>--%>
    
    <%--<tr>
        <td width="20%"></td>
        <td width="13%">Định dạng file: </td>
        <td colspan="4"> <select id="idFile" name="idFile" style="width: 152px">
            <%DinhDangFile(); %>
        </select></td>
        <td width="20%"></td>
    </tr>--%>
    </table>
    

    <div class="dataTables_length" style="text-align: center;">
        <a id="btnSave" href="javascript:;" class="btn" onclick="View();"><i class="iconfa-plus-sign">
             </i>Xem thông tin</a> 
        
        <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="View('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a>
                 <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="View('pdf');">
                    <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                    <input type="hidden" id="hdAction" name="hdAction"/>
    </div>
</fieldset>

 <fieldset class="fsBlockInfor">
        <legend>Quyết định kết nạp hội viên tổ chức</legend>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False"/>
    </fieldset>

      <div id="div_dmNhanSuHoiVien_add"></div>

      <div style = "display: none">
    <asp:LinkButton ID="btnChay2" runat="server" CssClass="btn" 
        onclick="btnChay2_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>
</form>

<script type="text/javascript">



//        $(document).ready(function () {
//            $("#NgayBatDau").datepicker();
//            $("#NgayKetThuc").datepicker();

//            $(".NgayBatDau").datepicker({
//                dateFormat: 'mm-dd-yy'
//            }).datepicker('setDate', new Date())

        $(function () {  $('#NgayBatDau, #NgayKetThuc, #NgayQuyetDinh').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
        });

$(function () {
    if (document.getElementById("NgayBatDau").value == null)
        $('#NgayBatDau, #NgayKetThuc, #NgayQuyetDinh').datepicker('setDate', new Date()) 
    });
          
      //  });

 </script>

 <script type="text/javascript">
     

         var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function() {
        $('#NgayBatDau, #NgayKetThuc, #NgayQuyetDinh').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');

          if (document.getElementById("NgayBatDau").value == null)
          {
            $('#NgayBatDau').datepicker('setDate', new Date())
            $('#NgayKetThuc').datepicker('setDate', new Date())
            $('#NgayQuyetDinh').datepicker('setDate', new Date())
        }
});

        </script>

<%--gọi function in bao cao--%>
<script type="text/javascript">
    function View(type) {
        
        $('#hdAction').val(type);
        $('#Form1').submit();
    }

    var timestamp = Number(new Date());
    $('#VungMienId').change(function () {
        $('#TinhId').html('');
        $('#TinhId').load('noframe.aspx?page=chondiaphuong&default=0&type=tt&id_vungmien=' + $('#VungMienId').val() + '&time=' + timestamp);
    });
</script>

<script type="text/javascript">
    $("#VungMienId").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
</script>

<script type="text/javascript">
    function open_ChonID_2() {
        var dvId = "null";
        var vmId = $('#VungMienId').val();

        var tId = document.getElementById("TinhId").value;


        var timestamp = Number(new Date());
        $("#div_dmNhanSuHoiVien_add").empty();
        $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoivien' name='iframe_hoivien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dbChonHoiVienCaNhan_BC&type=tapthe&VMID=" + vmId + "&TID=" + tId + "&DVID=" + dvId + "&mode=iframe&time=" + timestamp));
        $("#div_dmNhanSuHoiVien_add").dialog({
            autoOpen: false,
            title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách hội viên</b>",
            modal: true,
            width: "640",
            height: "480",
            buttons: [
                             {
                                 text: "Chọn",
                                 click: function () {
                                     formChon_2();
                                 }
                             },
                                {
                                    text: "Đóng",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                        ]
        }).dialog("open");

    }
</script>

<script type="text/javascript">
    function formChon_2() {
        $("#div_dmNhanSuHoiVien_add").dialog('close');
        document.getElementById('<%= btnChay2.ClientID %>').click();
    }
</script>

