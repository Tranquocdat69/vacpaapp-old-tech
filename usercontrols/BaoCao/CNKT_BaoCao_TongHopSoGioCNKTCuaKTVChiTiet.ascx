﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet.ascx.cs" Inherits="usercontrols_CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet" %>

<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Tổng hợp số giờ cập nhật kiến thức của kiểm toán viên (Chi tiết)</h4>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="800px" border="0" class="formtbl">
        <tr>
            <td style="min-width: 150px;">
                Mã đơn vị công tác:
            </td>
            <td>
                <input type="text" name="txtMaCongTy" id="txtMaCongTy" onchange="CallActionGetInforCongTy();" style="max-width: 120px;" />
                <input type="hidden" name="hdCongTyID" id="hdCongTyID" />
                <input type="button" value="---" onclick="OpenDanhSachCongTy();" style="border: 1px solid gray;" />
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                Tên đơn vị công tác:
            </td>
            <td colspan="3">
                <input type="text" readonly="readonly" id="txtTenCongTy"/>
            </td>
        </tr>
        <tr>
            <td>
                Năm<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlNam" name="ddlNam" onchange="ValidateDkNgay();">
                    <% GetListYear();%>
                </select>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Từ ngày:</td>
            <td><input type="text" name="txtTuNgay" id="txtTuNgay" style="max-width: 100px;" onchange="ValidateDkNgay();" />
            <img id="imgCalendarTuNgay" src="/images/icons/calendar.png" /></td>
            <td>Đến ngày:</td>
            <td><input type="text" name="txtDenNgay" id="txtDenNgay" style="max-width: 100px;" onchange="ValidateDkNgay();" />
            <img id="imgCalendarDenNgay" src="/images/icons/calendar.png" /></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('excel');"><i class="iconsweets-excel2"></i>Kết xuất Excel</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Tổng hợp số giờ cập nhật kiến thức của kiểm toán viên (Chi tiết)</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" ShowAllPageIds="True"
        HasExportButton="False" HasPrintButton="False" AutoDataBind="True" />
</fieldset>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    $("#txtDenNgay").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarDenNgay").click(function () {
        $("#txtDenNgay").datepicker("show");
    });

    $("#txtTuNgay").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarTuNgay").click(function () {
        $("#txtTuNgay").datepicker("show");
    });

    $('#Form1').validate({
        rules: {
            txtTuNgay: {
                dateITA: true
            },
            txtDenNgay: {
                dateITA: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }  
    });

    <% SetDataJavascript();%>
    
    function OpenDanhSachCongTy() {
        $("#DivDanhSachCongTy").empty();
        $("#DivDanhSachCongTy").append($("<iframe width='100%' height='100%' id='ifDanhSach' name='ifDanhSach' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=HoiVienTapThe_MiniList"));
        $("#DivDanhSachCongTy").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách đơn vị công tác</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    function CallActionGetInforCongTy() {
        var maCongTy = $('#txtMaCongTy').val();
        iframeProcess.location = '/iframe.aspx?page=CNKT_DangKyHocCaNhan_Process&mact=' + maCongTy + '&action=loadct';
    }
    
    function DisplayInforCongTy(value) {
        if(value.length > 0) {
            $('#hdCongTyID').val(value.split(';#')[0]);
            $('#txtMaCongTy').val(value.split(';#')[1]);
            $('#txtTenCongTy').val(value.split(';#')[2]);
        }
    }
    
    function CloseFormDanhSachCongTy() {
        $("#DivDanhSachCongTy").dialog('close');
    }
    
    function ValidateDkNgay() {
        var startLopHoc = $('#txtTuNgay').datepicker('getDate');
        var endLopHoc = $('#txtDenNgay').datepicker('getDate');
        
        //// Kiem tra 2 ngay phai nam trong nam da chon tu dropdownlist nam
        //if(startLopHoc != null) {
        //    if(parseInt(startLopHoc.getFullYear()) != parseInt($('#ddlNam option:selected').val())) {
        //        $('#txtTuNgay').val('');
        //        $('#txtTuNgay').focus();
        //        alert('Ngày bắt đầu thuộc năm đã chọn!');
        //    }
        //}
        
        //if(endLopHoc != null) {
        //    if(parseInt(endLopHoc.getFullYear()) != parseInt($('#ddlNam option:selected').val())) {
        //        $('#txtDenNgay').val('');
        //        $('#txtDenNgay').focus();
        //        alert('Ngày kết thúc thuộc năm đã chọn!');
        //    }
        //}
        
        if(startLopHoc != null && endLopHoc != null) {
            endLopHoc.setDate(endLopHoc.getDate() + 1);
            var minus = (endLopHoc - startLopHoc) / 1000 / 60 / 60 / 24;
            if(minus <= 0){
                $('#txtDenNgay').val('');
                $('#txtDenNgay').focus();
                alert('Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc!');
            }
        }
    }

    function Export(type) {
        $('#hdAction').val(type);
        $('#Form1').submit();  
    }
</script>
<div id="DivDanhSachCongTy"></div>