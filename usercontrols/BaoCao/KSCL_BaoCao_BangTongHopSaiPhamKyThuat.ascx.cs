﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class usercontrols_KSCL_BaoCao_BangTongHopSaiPhamKyThuat : System.Web.UI.UserControl
{
    protected string tenchucnang = "Bảng tổng hợp sai phạm kỹ thuật";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = Request.Form["ddlNam"];
            // Lấy danh sách idCongTy đã tick trong checkbox
            NameValueCollection nvc = Request.Form;
            List<string> listIdCongTy = (from string controlNames in nvc where controlNames.StartsWith("cboCongTy") where !string.IsNullOrEmpty(Request.Form[controlNames]) select controlNames.Replace("cboCongTy_", "")).ToList();
            List<string> listTenCongTy = new List<string>();
            foreach (string s in listIdCongTy)
            {
                listTenCongTy.Add(Request.Form["cboCongTy_" + s]);
            }
            if (listIdCongTy.Count > 0)
            {
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/BangTongHopSaiPhamKyThuat.rpt"));
                DataTable dt = GetNhomCauHoi("2", "1", nam, listIdCongTy);
                rpt.Database.Tables["dtBangTongHopSaiSot"].SetDataSource(dt);
                int maxRight = 0;
                for (int i = 0; i < listIdCongTy.Count; i++)
                {
                    int width = 1440;
                    maxRight = (7560 + width + 50) + (width * i) + (i * 100);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControl" + i, listTenCongTy[i], 7560 + (width * i) + (i * 100), 3360, width, null, null, 11, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    if (i < listIdCongTy.Count - 1)
                        CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControl" + i, maxRight, maxRight, 3240, 3720, true);
                    else
                        CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControl" + i, maxRight, maxRight, 2760, 3720, true);

                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControl" + i, maxRight, maxRight, 0, 360, true);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtBangTongHopSaiSot.CongTy" + i, "fControl" + i, 7560 + (width * i) + (i * 100), 0, width, null, null, 12, null, true, false, "", CrystalReportControl.TextFormat_HTML);
                }

                // Add Cột tính tổng ở cuối
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlTinhTong", "Tổng cộng", maxRight + 50, 2880, 1440, null, null, 12, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTinhTong", maxRight + 1540, maxRight + 1540, 2760, 3720, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTinhTong", maxRight + 1540, maxRight + 1540, 0, 360, true);
                CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtBangTongHopSaiSot.TongCong", "fControlTongCong", maxRight + 50, 0, 1440, null, null, 12, null, true, false, "", CrystalReportControl.TextFormat_HTML);

                ((TextObject)rpt.ReportDefinition.ReportObjects["Text5"]).Width = (maxRight - ((TextObject)rpt.ReportDefinition.ReportObjects["Text5"]).Left);
                ((TextObject)rpt.ReportDefinition.ReportObjects["Text5"]).ObjectFormat.HorizontalAlignment = Alignment.HorizontalCenterAlign;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lTopHeader"]).Right = maxRight + 1540;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lMiddleHeader"]).Right = maxRight + 1540;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomHeader"]).Right = maxRight + 1540;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomDetail"]).Right = maxRight + 1540;

                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle2"]).Text = "(Kết quả kiểm tra năm " + nam + ")";
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, listTenCongTy, nam);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BangTongHopSaiPhamKyThuatNam" + nam);
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string js = "";
        js += "var flagCheckAll = false;" + Environment.NewLine;
        js += "var arrCongTy = [];" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
        {
            js += "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
            NameValueCollection nvc = Request.Form;
            List<string> listIdCongTy = (from string controlNames in nvc where controlNames.StartsWith("cboCongTy") where !string.IsNullOrEmpty(Request.Form[controlNames]) select controlNames.Replace("cboCongTy_", "")).ToList();
            foreach (string s in listIdCongTy)
            {
                js += "arrCongTy.push('" + s + "');" + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(Request.Form["cboCheckAll"]))
                js += "flagCheckAll = true;" + Environment.NewLine;

        }
        js += "CallActionGetDanhSachCongTy();" + Environment.NewLine;
        Response.Write(js);
    }

    private void SetDataRow(DataTable dt, string stt, string noiDung, string diemChuan, DataRow[] arrDataRowBaoCaoKT, List<string> listIdCongTy, List<int> listSumTungDonVi)
    {
        DataRow dr = dt.NewRow();
        dr["STT"] = stt;
        dr["NoiDung"] = noiDung;
        dr["DiemChuan"] = "<center>" + diemChuan + "</center>";
        int sum = 0;
        for (int i = 0; i < listIdCongTy.Count; i++)
        {
            if (listSumTungDonVi.Count < listIdCongTy.Count)
                listSumTungDonVi.Add(0);
            if (arrDataRowBaoCaoKT != null && arrDataRowBaoCaoKT.Length > 0)
            {
                foreach (DataRow dataRow in arrDataRowBaoCaoKT)
                {
                    if (Equals(dataRow["HoiVienTapTheID"].ToString(), listIdCongTy[i]))
                    {
                        if (Equals(dataRow["TraLoi"].ToString(), "3"))
                        {
                            dr["CongTy" + i] = "<center>x</center>";
                            sum++;
                            listSumTungDonVi[i]++;
                        }
                        if (Equals(dataRow["TraLoi"].ToString(), "2"))
                        {
                            dr["CongTy" + i] = "<center>N/A</center>";
                        }
                    }
                }
            }
        }
        dr["TongCong"] = "<center><b>" + sum + "</b></center>";
        dt.Rows.Add(dr);
        //return listSumTungDonVi;
    }

    private double _tongSoDiem = 0;
    protected DataTable GetNhomCauHoi(string loaiCauHoi, string tinhTrangHieuLuc, string nam, List<string> listIdCongTy)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "NoiDung", "DiemChuan", "TongCong" });
        for (int i = 0; i < listIdCongTy.Count; i++)
        {
            dt.Columns.Add("CongTy" + i);
        }
        DataTable data = GetDanhSachCauHoi(loaiCauHoi, tinhTrangHieuLuc); // Get danh sách câu hỏi đổ tất cả vào DataTable
        // Lấy dữ liệu chi tiết báo cáo kiểm tra nếu có id Hồ sơ kiểm tra kiểm soát chất lượng
        DataTable dataBaoCaoKT = new DataTable();

        dataBaoCaoKT = GetDanhSachBaoCaoKiemTraChiTiet(nam); // Get dữ liệu bảng điểm 


        // Get Danh sách nhóm câu hỏi
        string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE Loai = " + loaiCauHoi + " AND (NhomCauHoiChaID IS NULL OR NhomCauHoiChaID = '') ORDER BY NhomCauHoiID";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            int index = 1;
            int currentIndexInDataTable = 0;
            foreach (Hashtable ht in listData)
            {
                DataRow[] arrDataRow = data.Select("NCHID = " + ht["NhomCauHoiID"]); // Lấy danh sách câu hỏi theo ID nhóm câu hỏi
                List<int> listSumTungDonVi = new List<int>();
                double sumDiemToiDa = arrDataRow.Sum(dataRow => Library.DoubleConvert(dataRow["DiemToiDa"]));
                _tongSoDiem += sumDiemToiDa;
                currentIndexInDataTable = dt.Rows.Count;
                SetDataRow(dt, index.ToString(), "<b>" + ht["TenNhomCauHoi"] + "</b>", (sumDiemToiDa > 0 ? Library.ChangeFormatNumber(sumDiemToiDa, "us") : ""), null, listIdCongTy, listSumTungDonVi);

                query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + ht["NhomCauHoiID"] + " ORDER BY NhomCauHoiID";
                List<Hashtable> listDataChild = _db.GetListData(query);
                string tienToXacDinhLaCauHoi = ""; // Tiền tố xác định là câu hỏi trong chuỗi STT (Thêm .0.)
                if (listDataChild.Count > 0)
                    tienToXacDinhLaCauHoi = "0.";
                int cauHoiIndex = 1;
                foreach (DataRow dataRow in arrDataRow)
                {
                    DataRow[] arrDataRowBaoCaoKT = null;
                    if (dataBaoCaoKT != null && dataBaoCaoKT.Rows.Count > 0)
                    {
                        string queryDataTable = "";
                        if (loaiCauHoi == "2")
                        {
                            queryDataTable = "CauHoiKTKTID = " + dataRow["CauHoiKTKTID"];
                            arrDataRowBaoCaoKT = dataBaoCaoKT.Select(queryDataTable); // Lấy kết quả báo cáo theo câu hỏi (Nếu có)
                        }
                    }
                    SetDataRow(dt, index + "." + tienToXacDinhLaCauHoi + cauHoiIndex, dataRow["CauHoi"].ToString(), (Library.DoubleConvert(dataRow["DiemToiDa"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(dataRow["DiemToiDa"]), "us") : ""), arrDataRowBaoCaoKT, listIdCongTy, listSumTungDonVi);
                    cauHoiIndex++;
                }
                GetNhomCauHoiSub(dt, listDataChild, data, dataBaoCaoKT, index, loaiCauHoi, ref sumDiemToiDa, listIdCongTy, listSumTungDonVi); // Gọi hàm đệ quy
                // Set lại điểm tối đa ở đây
                dt.Rows[currentIndexInDataTable]["DiemChuan"] = "<center><b>" + sumDiemToiDa + "</b></center>";
                int tongCongSoLanSaiPham_TungNhomLon = 0;
                for (int i = 0; i < listSumTungDonVi.Count; i++)
                {
                    if (listSumTungDonVi[i] > 0)
                    {
                        tongCongSoLanSaiPham_TungNhomLon++;
                        dt.Rows[currentIndexInDataTable]["CongTy" + i] = "<center><b>x</center></b>";
                    }
                }
                dt.Rows[currentIndexInDataTable]["TongCong"] = "<center><b>" + tongCongSoLanSaiPham_TungNhomLon + "</center></b>";

                index++;
            }
        }
        return dt;
    }

    protected void GetNhomCauHoiSub(DataTable dt, List<Hashtable> listData, DataTable data, DataTable dataBaoCaoKT, int index, string loaiCauHoi, ref double sumDiemToiDa_NhomCha, List<string> listIdCongTy, List<int> listSumTungDonVi_NhomCha)
    {
        if (listData.Count > 0)
        {
            int subIndex = 1;
            int currentIndexInDataTable = 0;
            foreach (Hashtable ht in listData)
            {
                DataRow[] arrDataRow = data.Select("NCHID = " + ht["NhomCauHoiID"]);
                List<int> listSumTungDonVi = new List<int>();
                double sumDiemToiDa = arrDataRow.Sum(dataRow => Library.DoubleConvert(dataRow["DiemToiDa"]));
                _tongSoDiem += sumDiemToiDa;
                currentIndexInDataTable = dt.Rows.Count;
                SetDataRow(dt, index + "." + subIndex, "<b>" + ht["TenNhomCauHoi"] + "</b>", (sumDiemToiDa > 0 ? Library.ChangeFormatNumber(sumDiemToiDa, "us") : ""), null, listIdCongTy, listSumTungDonVi);
                string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + ht["NhomCauHoiID"] + " ORDER BY NhomCauHoiID";
                List<Hashtable> listDataChild = _db.GetListData(query);
                string tienToXacDinhLaCauHoi = ""; // Tiền tố xác định là câu hỏi trong chuỗi STT (Thêm .0.)
                if (listDataChild.Count > 0)
                    tienToXacDinhLaCauHoi = "0.";
                int cauHoiIndex = 1;
                foreach (DataRow dataRow in arrDataRow)
                {
                    DataRow[] arrDataRowBaoCaoKT = null;
                    if (dataBaoCaoKT != null && dataBaoCaoKT.Rows.Count > 0)
                    {
                        string queryDataTable = "";
                        if (loaiCauHoi == "2")
                        {
                            queryDataTable = "CauHoiKTKTID = " + dataRow["CauHoiKTKTID"];
                            arrDataRowBaoCaoKT = dataBaoCaoKT.Select(queryDataTable); // Lấy kết quả báo cáo theo câu hỏi (Nếu có)
                        }
                    }
                    SetDataRow(dt, index + "." + subIndex + "." + tienToXacDinhLaCauHoi + cauHoiIndex, dataRow["CauHoi"].ToString(), (Library.DoubleConvert(dataRow["DiemToiDa"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(dataRow["DiemToiDa"]), "us") : ""), arrDataRowBaoCaoKT, listIdCongTy, listSumTungDonVi);
                    cauHoiIndex++;
                }
                GetNhomCauHoiSub(dt, listDataChild, data, dataBaoCaoKT, subIndex, loaiCauHoi, ref sumDiemToiDa_NhomCha, listIdCongTy, listSumTungDonVi);
                subIndex++;
                sumDiemToiDa_NhomCha += sumDiemToiDa;
                for (int i = 0; i < listSumTungDonVi_NhomCha.Count; i++)
                {
                    listSumTungDonVi_NhomCha[i] += listSumTungDonVi[i];
                }
                // Set lại điểm tối đa ở đây
                dt.Rows[currentIndexInDataTable]["DiemChuan"] = "<center><b>" + sumDiemToiDa + "</b></center>";
                int tongCongSoLanSaiPham_TungNhomLon = 0;
                for (int i = 0; i < listSumTungDonVi.Count; i++)
                {
                    if (listSumTungDonVi[i] > 0)
                    {
                        tongCongSoLanSaiPham_TungNhomLon++;
                        dt.Rows[currentIndexInDataTable]["CongTy" + i] = "<center><b>x</center></b>";
                    }
                }
                dt.Rows[currentIndexInDataTable]["TongCong"] = "<center><b>" + tongCongSoLanSaiPham_TungNhomLon + "</center></b>";
            }
        }
    }

    private DataTable GetDanhSachCauHoi(string loaiCauHoi, string tinhTrangHieuLuc)
    {
        DataTable dt = new DataTable();
        string query = @"SELECT CauHoiKTKTID, MaCauHoi, NgayLap, a.NhomCauHoiID NCHID, CauHoi, DiemToiDa, CanCu, HuongDanChamDiem, TinhTrangHieuLuc FROM tblKSCLCauHoiKT a
                            INNER JOIN tblDMNhomCauHoiKSCL b ON a.NhomCauHoiID = b.NhomCauHoiID
                            WHERE b.Loai = " + loaiCauHoi + " AND a.TinhTrangHieuLuc = '" + tinhTrangHieuLuc + "'";
        dt = _db.GetDataTable(query);
        return dt;
    }

    private DataTable GetDanhSachBaoCaoKiemTraChiTiet(string nam)
    {
        string query = @"SELECT BCKTKTChiTiet.BaoCaoKTKTChiTietID, BCKTKTChiTiet.CauHoiKTKTID, BCKTKTChiTiet.TraLoi, HS.HoiVienTapTheID
                         FROM tblKSCLBaoCaoKTKTChiTiet BCKTKTChiTiet
                        INNER JOIN tblKSCLBaoCaoKTKT BCKTKT ON BCKTKTChiTiet.BaoCaoKTKTID = BCKTKT.BaoCaoKTKTID
                        INNER JOIN tblKSCLHoSo HS ON BCKTKT.HoSoID = HS.HoSoID
                        INNER JOIN tblKSCLDoanKiemTra DKT ON HS.DoanKiemTraID = DKT.DoanKiemTraID
                        INNER JOIN tblDMTrangThai AS DMTT ON DKT.TinhTrangID = DMTT.TrangThaiID   
                        WHERE YEAR(DKT.NgayLap) = " + nam + @" AND DMTT.MaTrangThai = 5
                        ORDER BY CauHoiKTKTID";
        DataTable dt = _db.GetDataTable(query);
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, List<string> listTenCongTy, string nam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:120px;
            vertical-align: middle;
        }

        .HeaderColumn1 
        {
            font-size: 11pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTitle {                    
            vertical-align: middle;            
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
            height: 40px;
            vertical-align: middle; 
        }

        .GroupTitle2 {
            font-size: 10pt;
            font-weight: bold;
            height: 30px;
            vertical-align: middle; 
        }
        
        .Column {
            font-size: 12pt;
            text-align: center;
            vertical-align: middle;
        }

        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='8' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='8' style='text-align:center;font-size: 13pt; font-weight: bold;'>TỔNG HỢP CÁC SAI SÓT VỀ KỸ THUẬT</td>
            </tr>
            <tr>
                <td colspan='8' style='text-align:center;font-size: 13pt; font-weight: bold;'>(Kết quả kiểm tra năm " + nam + @")</td>
            </tr>           
            <tr><td><td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td rowspan='2' class='HeaderColumn1'>STT</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 200px;'>Nội dung sai sót</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Điểm chuẩn</td>
                                <td colspan='" + listTenCongTy.Count + @"' class='HeaderColumn1'>Công ty</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Tổng cộng</td>";

        htmlExport += "</tr><tr>";
        for (int i = 0; i < listTenCongTy.Count; i++)
        {
            htmlExport += "<td class='HeaderColumn1' style='width: 70px;'>" + listTenCongTy[i] + "</td>";
        }
        htmlExport += "</tr>";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            htmlExport += "<tr><td class='ColumnStt'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["NoiDung"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DiemChuan"] + "</td>";
            for (int x = 0; x < listTenCongTy.Count; x++)
            {
                htmlExport += "<td class='Column'>" + dt.Rows[i]["CongTy" + x] + "</td>";
            }
            htmlExport += "<td class='HeaderColumn1'>" + dt.Rows[i]["TongCong"] + "</td>";
            htmlExport += "</tr>";
        }
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("BangTongHopSaiPhamKyThuatNam" + nam + ".xsl", "<html>" + css + htmlExport + "</html>");
    }
}