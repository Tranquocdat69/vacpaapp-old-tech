﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_BTC_BaoCao_VeViecToChucLopHocCNKT : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo về việc tổ chức lớp học CNKT";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string idLopHoc = "";
            if (!string.IsNullOrEmpty(Request.Form["hdLopHocID"]))
            {
                idLopHoc = Request.Form["hdLopHocID"];

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/BTC/BaoCaoVeViecToChucLopHocCNKT.rpt"));
                rpt.Database.Tables["dtNoiDung_GiangVienLopHoc"].SetDataSource(GetNoiDung_GiangVienLopHoc(idLopHoc));
                GetThongTinLopHoc(idLopHoc, rpt);
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoVeViecToChucLopHocCNKT");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                {
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoVeViecToChucLopHocCNKT");
                    //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                    //xlsFormatOptions.ShowGridLines = true;
                    //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                    //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                    //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

                }
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        if (!string.IsNullOrEmpty(Request.Form["txtMaLopHoc"]))
        {
            string js = "$('#txtMaLopHoc').val('" + Request.Form["txtMaLopHoc"] + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
            Response.Write(js);
        }
    }

    private DataTable GetNoiDung_GiangVienLopHoc(string idLopHoc)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "Ngay", "Buoi", "TenChuyenDe", "LoaiChuyenDe", "TenGiangVien", "ChucVu", "DonViCongTac" });
        if (string.IsNullOrEmpty(idLopHoc))
            return dt;
        string query = @"SELECT LHB.Ngay, LHB.Sang, LHB.Chieu, LHCD.TenChuyenDe, LHCD.LoaiChuyenDe, DMGV.TenGiangVien, DMCV.TenChucVu, DMGV.DonViCongTac FROM tblCNKTLopHocBuoi LHB
                        LEFT JOIN tblCNKTLopHocChuyenDe LHCD ON LHB.ChuyenDeID = LHCD.ChuyenDeID
                        LEFT JOIN tblDMGiangVien DMGV ON LHCD.GiangVienID = DMGV.GiangVienID
                        LEFT JOIN tblDMChucVu DMCV ON DMGV.ChucVuID = DMCV.ChucVuID
                        WHERE LHB.LopHocID = " + idLopHoc + @"
                        ORDER BY Ngay, Chieu";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            DataRow dr;
            foreach (Hashtable ht in listData)
            {
                if (!string.IsNullOrEmpty(Library.CheckNull(ht["Sang"])))
                {
                    dr = dt.NewRow();
                    dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                    dr["Buoi"] = "Sáng";
                    dr["TenChuyenDe"] = ht["TenChuyenDe"] + "<br><i>(Chuyên đề " + Library.GetTenLoaiChuyenDe(ht["LoaiChuyenDe"].ToString()) + ")</i>";
                    dr["TenGiangVien"] = "<b>" + ht["TenGiangVien"] + "</b> - " + ht["TenChucVu"] + "<br>" + ht["DonViCongTac"];
                    dt.Rows.Add(dr);
                }
                if (!string.IsNullOrEmpty(Library.CheckNull(ht["Chieu"])))
                {
                    dr = dt.NewRow();
                    dr["Ngay"] = Library.DateTimeConvert(ht["Ngay"]).ToString("dd/MM/yyyy");
                    dr["Buoi"] = "Chiều";
                    dr["TenChuyenDe"] = ht["TenChuyenDe"] + "<br><i>(Chuyên đề " + Library.GetTenLoaiChuyenDe(ht["LoaiChuyenDe"].ToString()) + ")</i>";
                    dr["TenGiangVien"] = "<b>" + ht["TenGiangVien"] + "</b> - " + ht["TenChucVu"] + "<br>" + ht["DonViCongTac"];
                    dt.Rows.Add(dr);
                }
            }
        }
        return dt;
    }

    private void GetThongTinLopHoc(string idLopHoc, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idLopHoc))
            return;

        ((TextObject)rpt.ReportDefinition.ReportObjects["txtDiaDiemNgayThang"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;

        string query = "SELECT MaLopHoc, TenLopHoc, TuNgay, DenNgay, HanDangKy, TenTinh, DiaChiLopHoc FROM tblCNKTLopHoc LH LEFT JOIN tblDMTinh ON LH.TinhThanhID = tblDMTinh.TinhID WHERE LopHocID = " + idLopHoc;
        List<Hashtable> listData = _db.GetListData(query);
        {
            Hashtable ht = listData[0];
            DateTime fromDate = Library.DateTimeConvert(ht["TuNgay"]);
            DateTime toDate = Library.DateTimeConvert(ht["DenNgay"]);

            ((TextObject)rpt.ReportDefinition.ReportObjects["txtHeaderMaLopHoc"]).Text = "V/v: Thông báo về lớp CNKT số " + ht["MaLopHoc"];
            rpt.DataDefinition.FormulaFields["UnBoundString1"].Text = "\"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hội Kiểm toán viên hành nghề Việt Nam (VACPA) đã được Bộ Tài chính chấp thuận tổ chức các lớp học cập nhật kiến thức (CNKT) cho kiểm toán viên năm " + fromDate.Year + " <i>(Theo Quyết định số ........./QĐ-BTC ngày ................ của Bộ Tài chính)</i>." +
                                                                      " Thực hiện Kế hoạch đào tạo CNKT " + fromDate.Year + ", Hội kiểm toán viên hành nghề Việt Nam sẽ tổ chức lớp học cập nhật kiến thức cho kiểm toán viên (Lớp số "+ht["MaLopHoc"]+"), như sau:\"";
            rpt.DataDefinition.FormulaFields["UnboundString2"].Text = "\"<b>2. Thời gian:</b> " + ((toDate - fromDate).TotalDays + 1).ToString() + " ngày, bắt đầu từ <b>8h00, ngày " + fromDate.ToString("dd/MM/yyyy") + " đến " + toDate.ToString("dd/MM/yyyy") + ".</b>\"";
            rpt.DataDefinition.FormulaFields["UnboundString3"].Text = "\"<b>3. Địa điểm tổ chức:</b> " + ht["DiaChiLopHoc"] + ".\"";

            query = @"SELECT COUNT(DangKyHocCaNhanID) SoLuotDangKyHoc
                FROM tblCNKTDangKyHocCaNhan WHERE LopHocID = " + idLopHoc;
            List<Hashtable> listData2 = _db.GetListData(query);
            {
                rpt.DataDefinition.FormulaFields["UnboundString4"].Text = "\"<b>5. Số lượng KTV đăng ký dự học tính đến thời điểm thông báo:</b> bình quân là " + listData2[0]["SoLuotDangKyHoc"] + " học viên.\"";
            }
        }
    }
}