﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TT_BaoCao_NopPhiHoiVienTapThe.ascx.cs" Inherits="usercontrols_TT_BaoCao_NopPhiHoiVienTapThe" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<style type="text/css">
   
    .ui-dropdownchecklist {
        background: #ffffff !important;
        border: solid 1PX #cccccc!important;
        margin: 0px;
  height: 22px;
  width:152px;
  -webkit-appearance: menulist;
  box-sizing: border-box;
  border: 1px solid;
  border-image-source: initial;
  border-image-slice: initial;
  border-image-width: initial;
  border-image-outset: initial;
  border-image-repeat: initial;
  white-space: pre;
  -webkit-rtl-ordering: logical;
  color: black;
  background-color: white;
  cursor: default;
    }
    .ui-dropdownchecklist span {
        width:100%;
    }
    .ui-dropdownchecklist span span{
        width:100%;
    }
</style>
<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Báo cáo nộp hội phí tổ chức</h4>
<fieldset class="fsBlockInfor">
    <legend>Tiêu chí kết xuất dữ liệu báo cáo</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td>
                Vùng miền:
            </td>
            <td >
                <select name="VungMien" id="VungMien" multiple="multiple">
                    <%  
                        
                        try
                        {
                           
                            LoadVungMien();
                        }
                        catch (Exception ex)
                        {

                        }
                    %>
                </select>
            </td>
        </tr>
         <tr>
            <td>
                Tỉnh thành:
            </td>
            <td>
                <select name="TinhThanh" id="TinhThanh">
                    <%  
                        
                        try
                        {
                           
                            LoadTinhThanh();
                        }
                        catch (Exception ex)
                        {

                        }
                    %>
                </select>
            </td>
        </tr>
       
        
       
         <tr>
            <td>
                Ngày chốt số liệu<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtNgayLapBC" name="txtNgayLapBC" style="max-width: 100px;" />
                <img src="/images/icons/calendar.png" id="imgCalendarNgayLapBC" />&nbsp;-&nbsp;
             <input type="text" id="txtNgayLapBCDen" name="txtNgayLapBCDen" style="max-width: 100px;" />
                <img src="/images/icons/calendar.png" id="imgCalendarNgayLapBCDen" />
            </td>
        </tr> 
        <tr>
            <td>
                Hội viên tổ chức:
            </td>
            <td>
                <select name="BoPhanCongTac" id="BoPhanCongTac"  style="width:430px" >
                                    <asp:PlaceHolder ID="BoPhanCongTac" runat="server"></asp:PlaceHolder>
                                </select>
            </td>
        </tr> 
         <tr>
            <td>
                
            </td>
            <td>
                <input type="checkbox" id="cbConNoPhi" name="cbConNoPhi" onchange="if($('#cbConNoPhi').is(':checked')) $('#hdConNoPhi').val(1); else $('#hdConNoPhi').val(0);" /> Còn nợ phí
              &nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" id="hdConNoPhi" name="hdConNoPhi" value="0" />
              <input type="checkbox" id="cbKhongConNoPhi" name="cbKhongConNoPhi" onchange="if($('#cbKhongConNoPhi').is(':checked')) $('#hdKhongConNoPhi').val(1); else $('#hdKhongConNoPhi').val(0);" />Không còn nợ phí
                <input type="hidden" id="hdKhongConNoPhi" name="hdKhongConNoPhi" value="0" />
            </td>
        </tr>        
        <tr>
            <td colspan="2" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');"><i class="iconfa-search"></i>Xem dữ liệu</a> 
                <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="Export('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a> <a id="A1" href="javascript:;" class="btn btn-rounded" onclick="Export('excel');">
                    <i class="iconsweets-excel2"></i>Kết xuất Excel</a> <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="Export('pdf');">
                    <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                    <input type="hidden" id="hdAction" name="hdAction"/>
            </td>
        </tr>
    </table>
</fieldset>
    <fieldset class="fsBlockInfor">
        <legend>Báo cáo nộp hội phí tổ chức</legend>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False" DisplayToolbar="False"/>
    </fieldset>
</form>
<iframe name="iframeProcess_LoadCongTy" width="0px" height="0px"></iframe>

 <iframe name="iframeProcess_LoadHocVien" width="0px" height="0px"></iframe>
<script type="text/javascript">
   
    $("#txtNgayLapBC").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarNgayLapBC").click(function () {
        $("#txtNgayLapBC").datepicker("show");
    });

    $("#txtNgayLapBCDen").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarNgayLapBCDen").click(function () {
        $("#txtNgayLapBCDen").datepicker("show");
    });

 $('#Form1').validate({
        rules: {
            txtNgayLapBCDen: {
                required: true, dateITA: true
            },
            txtNgayLapBC: {
                dateITA: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form_ingcn").html(e).show();

            } else {
                jQuery("#thongbaoloi_form_ingcn").hide();  
            }
        }
    });

    function Export(type) {
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
    
    <% SetDataJavascript();%>
</script>
<script type="text/javascript">
    $("#VungMien").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
    var timestamp = Number(new Date());
    $('#VungMien').change(function () {
        $('#TinhThanh').html('');
        //alert($('#VungMien').val());
        $('#TinhThanh').load('noframe.aspx?page=chondiaphuong&default=0&type=tt&id_vungmien=' + $('#VungMien').val() + '&time=' + timestamp);
    });
</script>
<div id="DivDanhSachCongTy">
</div>
<div id="DivDanhSachHocVien">
</div>
