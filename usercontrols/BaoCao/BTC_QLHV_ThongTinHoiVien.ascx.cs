﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_BTC_QLHV_ThongTinHoiVien : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        setdataCrystalReport();
    }

    

    public void LoadDonViCongTac(string DonVi = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (DonVi == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";


        //output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (var tem in lst)
        {
            if (ma == tem.idDoiTuong.ToString())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void setdataCrystalReport()
    {
        string strid = id.Text;
        if (!string.IsNullOrEmpty(strid))
        {
            int idHV = Convert.ToInt32(strid);
            List<objThongTinHoiVien_ChiTiet> lst = new List<objThongTinHoiVien_ChiTiet>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HODEM, Ten, NgaySinh, GioiTinh, b.TenTinh, a.DiaChi, c.TenTruongDaiHoc, "
                        + " d.TenHocHam, a.HocHamNam, e.TenHocVi, a.HocViNam, a.SoCMND, a.CMND_NgayCap, "
                        + " a.SoTaiKhoanNganHang, f.TenNganHang, a.SoChungChiKTV, a.NgayCapChungChiKTV, "
                        + " a.SoGiayChungNhanDKHN, a.NgayCapGiayChungNhanDKHN, g.TenChucVu, a.DonViCongTac, "
                        + " a.Email, a.DienThoai, h.TenSoThich "
                        + " from dbo.tblHoiVienCaNhan a "
                        + " left join dbo.tblDMTinh b on a.DiaChi_TinhID = b.TinhID "
                        + " left join dbo.tblDMTruongDaiHoc c on a.TruongDaiHocID = c.TruongDaiHocID "
                        + " left join dbo.tblDMHocHam d on d.HocHamID = a.HocHamID "
                        + " left join dbo.tblDMHocVi e on e.HocViID = a.HocViID "
                        + " left join dbo.tblDMNganHang f on a.NganHangID = f.NganHangID "
                        + " left join dbo.tblDMChucVu g on a.ChucVuID = g.ChucVuID "
                        + " left join dbo.tblDMSoThich h on a.SoThichID = h.SoThichID "
                        + " WHERE HoiVienCaNhanID = " + idHV;// + id;
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
        List<lstOBJQuaTrinhLamViec> lstQuaTrinhLamViec = new List<lstOBJQuaTrinhLamViec>();
       // int i = 1;
        foreach (DataRow tem in dt.Rows)
        {
            objThongTinHoiVien_ChiTiet new1 = new objThongTinHoiVien_ChiTiet();
            new1.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            new1.NgaySinh = !string.IsNullOrEmpty(tem["NgaySinh"].ToString()) ? Convert.ToDateTime(tem["NgaySinh"]).ToShortDateString() : "";
            new1.GioiTinh = tem["GioiTinh"].ToString();
            new1.QueQuan = tem["TenTinh"].ToString();
            new1.ChoOHienNay = tem["DiaChi"].ToString();
            new1.BangCap = tem["TenTruongDaiHoc"].ToString();
            new1.HocHam = tem["TenHocHam"].ToString();
            new1.HocVi = tem["TenHocVi"].ToString();
            new1.Nam_HocHam = tem["HocHamNam"].ToString();
            new1.Nam_HocVi = tem["HocViNam"].ToString();
            new1.CMT = tem["SoCMND"].ToString();
            new1.NgayCap = !string.IsNullOrEmpty(tem["CMND_NgayCap"].ToString()) ? Convert.ToDateTime(tem["CMND_NgayCap"]).ToShortDateString() : "";
            new1.SoTK = tem["SoTaiKhoanNganHang"].ToString();
            new1.TaiNganHang = tem["TenNganHang"].ToString();
            new1.SoChungChi = tem["SoChungChiKTV"].ToString();
            new1.NgayCap_CT = !string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()) ? Convert.ToDateTime(tem["NgayCapChungChiKTV"]).ToShortDateString() : "";
            new1.GiayDangKy = tem["SoGiayChungNhanDKHN"].ToString();
            new1.NoiCapGiayDangKy = "";
            new1.ChucVu = tem["TenChucVu"].ToString();
            new1.DonViCongTac = tem["DonViCongTac"].ToString();
            new1.Email = tem["Email"].ToString();
            new1.DienThoai = "";
            new1.DD = tem["DienThoai"].ToString();
            new1.SoThich = tem["TenSoThich"].ToString();
            //new1.lstQuaTrinhLamViec = new List<lstOBJQuaTrinhLamViec>();
            //new1.lstKhenThuong = new List<lstOBJKhenThuong>();
            lst.Add(new1);

            DataSet ds1 = new DataSet();
            SqlCommand sql1 = new SqlCommand();
            sql1.CommandText = " select NgayThang, TenHinhThucKhenThuong, LyDo from tblKhenThuongKyLuat a "
                            + " inner join dbo.tblDMHinhThucKhenThuong b on a.HinhThucKhenThuongID = b.HinhThucKhenThuongID "
                            + " where a.HoiVienCaNhanID = " + idHV;// + id;
            ds1 = DataAccess.RunCMDGetDataSet(sql1);
            sql1.Connection.Close();
            sql1.Connection.Dispose();
            sql1 = null;

            DataTable dt1 = ds1.Tables[0];

            foreach (DataRow tem1 in dt1.Rows)
            {
                lstOBJKhenThuong con = new lstOBJKhenThuong();
                con.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
                con.NgayThang = tem1["NgayThang"].ToString();
                con.HinhThuc = tem1["TenHinhThucKhenThuong"].ToString();
                con.LyDo = tem1["LyDo"].ToString();
                lstKhenThuong.Add(con);
            }


            DataSet ds2 = new DataSet();
            SqlCommand sql2 = new SqlCommand();
            sql2.CommandText = " select ThoiGianCongTac, ChucVu, NoiLamViec from dbo.tblHoiVienCaNhanQuaTrinhCongTac "
                            + " where HoiVienCaNhanID = " + idHV;// + id;
            ds2 = DataAccess.RunCMDGetDataSet(sql2);
            sql2.Connection.Close();
            sql2.Connection.Dispose();
            sql2 = null;

            DataTable dt2 = ds2.Tables[0];

            foreach (DataRow tem2 in dt2.Rows)
            {
                lstOBJQuaTrinhLamViec con = new lstOBJQuaTrinhLamViec();
                con.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
                con.ThoiGian = tem2["ThoiGianCongTac"].ToString();
                con.ChucVu = tem2["ChucVu"].ToString();
                con.DonViCongTac = tem2["NoiLamViec"].ToString();
                lstQuaTrinhLamViec.Add(con);
            }

        }
        

            
            ReportDocument rpt = new ReportDocument();
            rpt.Load(Server.MapPath("Report/QLHV/BaoCaoTongHopThongTinHoiVien.rpt"));
            
            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objThongTinHoiVien_ChiTiet"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJQuaTrinhLamViec"].SetDataSource(lstQuaTrinhLamViec);
         //   rpt.Database.Tables["lstOBJKhenThuong"].SetDataSource(lstKhenThuong);
          
            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoTongHopThongTinHoiVien");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoTongHopThongTinHoiVien");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            //ReportDocument rdPL = new ReportDocument();
            //rdPL.Load(Application.StartupPath + "\\GCN\\312_313\\" + strFilePL);
            //rdPL.SetDataSource(ds);
            //rdPL.SetParameterValue("NoiCapPL", txtNoiCapPL.Text,);
            //crtXemPL.ReportSource = rdPL;
            //crtXemPL.Refresh();

            //rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            //    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //{
            //    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //    //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //    //xlsFormatOptions.ShowGridLines = true;
            //    //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //    //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //    //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            //}
        }
    }
    

    //private List<objDoiTuong> getDinhDangFile()
    //{
    //    List<objDoiTuong> lst = new List<objDoiTuong>();

    //    objDoiTuong obj1 = new objDoiTuong();
    //    obj1.idDoiTuong = 1;
    //    obj1.TenDoiTuong= "Word";
    //    objDoiTuong obj2 = new objDoiTuong();
    //    obj2.idDoiTuong = 2;
    //    obj2.TenDoiTuong = "Excel";
    //    objDoiTuong obj3 = new objDoiTuong();
    //    obj3.idDoiTuong = 3;
    //    obj3.TenDoiTuong = "PDF";

    //    lst.Add(obj1);
    //    lst.Add(obj2);
    //    lst.Add(obj3);

    //    return lst;
    //}
    protected void Text_Changed(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
                        + " from dbo.tblHoiVienCaNhan "
                        + " WHERE HoiVienCaNhanID = " + id.Text;// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtTen.Text = tem["HoTen"].ToString();
        }
        catch { }
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<int> lstIdChon = new List<int>();
            lstIdChon = Session["DSChon"] as List<int>;
            string result = string.Join(",", lstIdChon);
            id.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select HoDem + ' ' + Ten as HoTen "
                        + " from dbo.tblHoiVienCaNhan "
                        + " WHERE HoiVienCaNhanID = " + lstIdChon[0];// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtTen.Text = tem["HoTen"].ToString();
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }
}