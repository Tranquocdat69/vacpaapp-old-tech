﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class usercontrols_KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách công ty kiểm toán đã kiểm tra chất lượng qua các năm";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            NameValueCollection nvc = Request.Form;
            List<string> listIdTinhThanh = (from string controlNames in nvc where controlNames.StartsWith("cboTinh") where !string.IsNullOrEmpty(Request.Form[controlNames]) select controlNames.Replace("cboTinh_", "")).ToList();
            int tuNam = Library.Int32Convert(Request.Form["ddlTuNam"]);
            int denNam = Library.Int32Convert(Request.Form["ddlDenNam"]);
            // Lấy danh sách idCongTy đã tick trong checkbox
            if (denNam >= tuNam && tuNam > 0 && listIdTinhThanh.Count > 0)
            {
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam.rpt"));
                DataTable dt = GetDanhSachCongTyDaKiemTraChatLuong(tuNam, denNam, listIdTinhThanh);
                rpt.Database.Tables["dtDanhSachCongTyKiemTraChatLuongQuaCacNam"].SetDataSource(dt);
                int maxRight = 0;
                int index = 0;
                for (int i = tuNam; i <= denNam; i++)
                {
                    int width = 1080;
                    // Tạo các tiêu đề con trong 1 năm trước
                    maxRight = 8760 + (width * index * 5) + (index * 100 * 5);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderXepLoai" + i, "Xếp loại", maxRight, 3360, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtDanhSachCongTyKiemTraChatLuongQuaCacNam.XepLoai" + index, "fControlXepLoai" + i, maxRight, 120, width, null, null, 10, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlXepLoai" + i, maxRight, maxRight, 3240, 3720, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlXepLoai" + i, maxRight, maxRight, 0, 600, true);
                    maxRight = (8760 + ((width + 100) * 1)) + (width * index * 5) + (index * 100 * 5);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderDuDkChungKhoan" + i, "Được chấp nhận kiểm toán cho đơn vị có lợi ích công chúng thuộc lĩnh vực chứng khoán\n", maxRight, 3360, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtDanhSachCongTyKiemTraChatLuongQuaCacNam.DuDKChungKhoan" + index, "fControlDuDKChungKhoan" + i, maxRight, 120, width, null, null, 10, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlDuDkChungKhoan" + i, maxRight, maxRight, 3240, 3720, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlDuDkChungKhoan" + i, maxRight, maxRight, 0, 600, true);
                    maxRight = (8760 + ((width + 100) * 2)) + (width * index * 5) + (index * 100 * 5);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderDuDkKhac" + i, "Được chấp nhận kiểm toán cho đơn vị có lợi ích công chúng khác\n", maxRight, 3360, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtDanhSachCongTyKiemTraChatLuongQuaCacNam.DuDKKhac" + index, "fControlDuDKKhac" + i, maxRight, 120, width, null, null, 10, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlDuDkKhac" + i, maxRight, maxRight, 3240, 3720, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlDuDkKhac" + i, maxRight, maxRight, 0, 600, true);
                    maxRight = (8760 + ((width + 100) * 3)) + (width * index * 5) + (index * 100 * 5);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderDDKT" + i, "Địa điểm kiểm tra", maxRight, 3360, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtDanhSachCongTyKiemTraChatLuongQuaCacNam.DiaDiemKiemTra" + index, "fControlDiaDiemKiemTra" + i, maxRight, 120, width, null, null, 10, null, true, false, "", "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlDDKT" + i, maxRight, maxRight, 3240, 3720, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlDDKT" + i, maxRight, maxRight, 0, 600, true);
                    maxRight = (8760 + ((width + 100) * 4)) + (width * index * 5) + (index * 100 * 5);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderGhiChu" + i, "Ghi chú", maxRight, 3360, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtDanhSachCongTyKiemTraChatLuongQuaCacNam.GhiChu" + index, "fControlGhiChu" + i, maxRight, 120, width, null, null, 10, null, true, false, "", "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlGhiChu" + i, maxRight, maxRight, 0, 600, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControl" + i, maxRight, maxRight, 2760, 3720, true);

                    // Tạo tiêu đề năm
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderNam" + i, "Năm " + i, 8760 + (width * index * 5) + (index * 100 * 5), 2880, maxRight - (8760 + (width * index * 5) + (index * 100 * 5)) - 50, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    index++;
                }
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lTopHeader"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lMiddleHeader"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomHeader"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomDetail"]).Right = maxRight;

                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle2"]).Text = "Từ năm " + tuNam + " đến năm " + denNam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, tuNam, denNam);
                //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam");

                string strIDVungMien = "", strIDTinhThanh = "";
                if (!string.IsNullOrEmpty(Request.Form["VungMien"]) && Request.Form["VungMien"] != "-1")
                {
                    strIDVungMien = Request.Form["VungMien"];
                }
                if (!string.IsNullOrEmpty(Request.Form["TinhThanh"]) && Request.Form["TinhThanh"] != "-1")
                {
                    strIDTinhThanh = Request.Form["TinhThanh"];
                }
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string js = "";
        if (!string.IsNullOrEmpty(Request.Form["cboCheckAllVungMien"]))
            js += "$('#cboCheckAllVungMien').prop('checked', true);" + Environment.NewLine;
        else
            js += "$('#cboCheckAllVungMien').prop('checked', false);" + Environment.NewLine;
        js += "var flagCheckAll = false;" + Environment.NewLine;
        js += "var arrTinhThanh = [];" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["ddlTuNam"]))
            js += "$('#ddlTuNam').val('" + Request.Form["ddlTuNam"] + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["ddlDenNam"]))
            js += "$('#ddlDenNam').val('" + Request.Form["ddlDenNam"] + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["ddlTuNam"]))
        {
            NameValueCollection nvc = Request.Form;
            List<string> listIdTinhThanh = (from string controlNames in nvc where controlNames.StartsWith("cboTinh") where !string.IsNullOrEmpty(Request.Form[controlNames]) select controlNames.Replace("cboTinh_", "")).ToList();
            foreach (string s in listIdTinhThanh)
            {
                js += "arrTinhThanh.push('" + s + "');" + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(Request.Form["cboCheckAll"]))
                js += "flagCheckAll = true;" + Environment.NewLine;
        }
        js += "CallActionGetDanhSachTinhThanh();" + Environment.NewLine;
        Response.Write(js);
    }

    private DataTable GetDanhSachCongTyDaKiemTraChatLuong(int tuNam, int denNam, List<string> listIdTinh)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "SoHieu", "TenCongTy", "TenVietTat", "HoiVien", "NamThanhLap", "TinhThanhPho" });
        int index = 0;
        for (int i = tuNam; i <= denNam; i++)
        {
            dt.Columns.Add("Nam" + index);
            dt.Columns.Add("XepLoai" + index);
            dt.Columns.Add("DuDKChungKhoan" + index);
            dt.Columns.Add("DuDKKhac" + index);
            dt.Columns.Add("DiaDiemKiemTra" + index);
            dt.Columns.Add("GhiChu" + index);
            index++;
        }
        string query = @"SELECT HVTC.HoiVienTapTheID, HVTC.SoHieu, HVTC.TenDoanhNghiep, HVTC.TenVietTat, HVTC.LoaiHoiVienTapThe, YEAR(HVTT.NgayThanhLap) NamThanhLap, Tinh.TinhID, Tinh.TenTinh,
                         YEAR(DKT.NgayLap) NamKiemTra, BCTH.XepLoaiChung, BCTH.KetLuan
                        FROM tblKSCLBaoCaoTongHop BCTH
                        INNER JOIN tblKSCLHoSo HS ON BCTH.HoSoID = HS.HoSoID
                        INNER JOIN tblKSCLDoanKiemTra DKT ON HS.DoanKiemTraID = DKT.DoanKiemTraID
                        LEFT JOIN tblHoiVienTapThe HVTC ON HS.HoiVienTapTheID = HVTC.HoiVienTapTheID
                        LEFT JOIN tblDMTinh Tinh ON HVTC.DiaChi_TinhID = Tinh.MaTinh
                        WHERE YEAR(DKT.NgayLap) >= " + tuNam + @" AND YEAR(DKT.NgayLap) <= " + denNam + @"
                        ORDER BY HVTC.TenDoanhNghiep";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            string tempIdHVTT = "0";
            DataRow dr = null;
            foreach (Hashtable ht in listData)
            {
                if (listIdTinh.Contains(ht["TinhID"].ToString()))
                {
                    if (ht["HoiVienTapTheID"].ToString() != tempIdHVTT)
                    {
                        tempIdHVTT = ht["HoiVienTapTheID"].ToString();
                        dr = dt.NewRow();
                        dr["STT"] = (dt.Rows.Count + 1);
                        dr["SoHieu"] = ht["SoHieu"];
                        dr["TenCongTy"] = ht["TenDoanhNghiep"];
                        dr["TenVietTat"] = ht["TenVietTat"];
                        if (ht["LoaiHoiVienTapThe"].ToString() == "1")
                            dr["HoiVien"] = "x";
                        dr["NamThanhLap"] = ht["NamThanhLap"];
                        dr["TinhThanhPho"] = ht["TenTinh"];
                        index = 0;
                        for (int i = tuNam; i <= denNam; i++)
                        {
                            if (ht["NamKiemTra"].ToString() == i.ToString())
                            {
                                dr["XepLoai" + index] = Library.GetTenXepLoai(ht["XepLoaiChung"].ToString());
                                dr["DuDKChungKhoan" + index] = "";
                                dr["DuDKKhac" + index] = "";
                                dr["DiaDiemKiemTra" + index] = "";
                                dr["GhiChu" + index] = ht["KetLuan"];
                            }
                            index++;
                        }
                        dt.Rows.Add(dr);
                    }
                    else
                    {
                        index = 0;
                        for (int i = tuNam; i <= denNam; i++)
                        {
                            if (ht["NamKiemTra"].ToString() == i.ToString())
                            {
                                dt.Rows[dt.Rows.Count - 1]["XepLoai" + index] = Library.GetTenXepLoai(ht["XepLoaiChung"].ToString());
                                dt.Rows[dt.Rows.Count - 1]["DuDKChungKhoan" + index] = "";
                                dt.Rows[dt.Rows.Count - 1]["DuDKKhac" + index] = "";
                                dt.Rows[dt.Rows.Count - 1]["DiaDiemKiemTra" + index] = "";
                                dt.Rows[dt.Rows.Count - 1]["GhiChu" + index] = ht["KetLuan"];
                            }
                            index++;
                        }
                    }
                }
            }
        }
        return dt;
    }

    protected void GetListVungMien()
    {
        string js = "";
        try
        {
            _db.OpenConnection();
            string query = "SELECT VungMienID, TenVungMien FROM tblDMVungMien";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    string check = "";
                    if (string.IsNullOrEmpty(Request.Form["ddlTuNam"]) || !string.IsNullOrEmpty(Request.Form["cboVungMien_" + ht["VungMienID"]]))
                        check = "checked='checked'";
                    js += @"<tr>
                            <td style='width: 30px; text-align: center;'><input type='checkbox' id='cboVungMien_" + ht["VungMienID"] + @"' name='cboVungMien_" + ht["VungMienID"] + @"' value='" + ht["VungMienID"] + @"' " + check + @" onclick='CallActionGetDanhSachTinhThanh();' /></td>
                            <td>" + ht["TenVungMien"] + @"</td>
                        </tr>";
                }
            }
        }
        catch { _db.OpenConnection(); }
        finally
        {
            _db.CloseConnection();
        }
        Response.Write(js);
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, int tuNam, int denNam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:120px;
            vertical-align: middle;
        }

        .HeaderColumn1 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTitle {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='12' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='12' style='text-align:center;font-size: 14pt; font-weight: bold;'>DANH SÁCH CÔNG TY KIỂM TOÁN ĐÃ KIỂM TRA CHẤT LƯỢNG</td>
            </tr>
            <tr>
                <td colspan='12' style='text-align:center;font-size: 12pt; font-weight: bold;'><i>Từ năm " + tuNam + @" đến năm " + denNam + @"</i></td>
            </tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td rowspan='2' class='HeaderColumn1'>STT</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Số hiệu công ty</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Tên công ty</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 100px;'>Tên viết tắt</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Hội viên</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Năm thành lập</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Tỉnh/thành phố</td>";
        for (int i = tuNam; i <= denNam; i++)
        {
            htmlExport += "<td class='HeaderColumn1' colspan='5'>Năm " + i + "</td>";
        }

        htmlExport += "</tr><tr>";
        for (int i = tuNam; i <= denNam; i++)
        {
            htmlExport += @"<td class='HeaderColumn' style='width: 70px;'>Xếp loại</td>
                                <td class='HeaderColumn' style='width: 70px;'>Được chấp nhận kiểm toán cho đơn vị có lợi ích công chúng thuộc lĩnh vực chứng khoán</td>
                                <td class='HeaderColumn' style='width: 70px;'>Được chấp nhận kiểm toán cho đơn vị có lợi ích công chúng khác</td>
                                <td class='HeaderColumn' style='width: 70px;'>Địa điểm kiểm tra</td>
                                <td class='HeaderColumn' style='width: 70px;'>Ghi chú</td>";
        }
        htmlExport += "</tr>";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string stt = dt.Rows[i]["STT"].ToString();
            string soHieu = dt.Rows[i]["SoHieu"].ToString();
            string tenCongTy = dt.Rows[i]["TenCongTy"].ToString();
            string tenVietTat = dt.Rows[i]["TenVietTat"].ToString();
            string hoiVien = dt.Rows[i]["HoiVien"].ToString();
            string namThanhLap = dt.Rows[i]["NamThanhLap"].ToString();
            string tinhThanhPho = dt.Rows[i]["TinhThanhPho"].ToString();

            htmlExport += "<tr><td class='ColumnStt'>" + stt + "</td><td class='Column'>" + soHieu + "</td><td class='ColumnTitle'>" + tenCongTy + "</td>" +
                          "<td class='Column'>" + tenVietTat + "</td><td class='Column'>" + hoiVien + "</td><td class='Column'>" + namThanhLap + "</td><td class='Column'>" + tinhThanhPho + "</td>";
            int index = 0;
            for (int x = tuNam; x <= denNam; x++)
            {
                htmlExport += "<td class='Column'>" + dt.Rows[i]["XepLoai" + index] + "</td>";
                htmlExport += "<td class='Column'>" + dt.Rows[i]["DuDKChungKhoan" + index] + "</td>";
                htmlExport += "<td class='Column'>" + dt.Rows[i]["DuDKKhac" + index] + "</td>";
                htmlExport += "<td class='Column'>" + dt.Rows[i]["DiaDiemKiemTra" + index] + "</td>";
                htmlExport += "<td class='Column'>" + dt.Rows[i]["GhiChu" + index] + "</td>";
                index++;
            }
            htmlExport += "</tr>";
        }
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam.xsl", "<html>" + css + htmlExport.Replace("<br>", "\n").Replace("<br />", "\n") + "</html>");
    }

    public void LoadVungMien()
    {
        try
        {
            string ma = "";
            DataBaseDb db = new DataBaseDb();
            DataTable dt = db.GetData("select * from tblDMVungMien");
            if (!string.IsNullOrEmpty(Request.Form["VungMien"]))
                ma = Request.Form["VungMien"];
            string _outputhtml = "";
            if (ma != "")
                _outputhtml += "<option value=\"-1\">Tất cả</option>";
            else
                _outputhtml += @"<option selected=""selected"" value=""-1"">Tất cả</option>";

            foreach (DataRow dr in dt.Rows)
            {
                if (ma == dr["VungMienID"].ToString())
                    _outputhtml += "<option selected=\"selected\" value=\"" + dr["VungMienID"] + "\">" + dr["TenVungMien"] + "</option>";
                else
                    _outputhtml += "<option  value=\"" + dr["VungMienID"] + "\">" + dr["TenVungMien"] + "</option>";
            }
            Response.Write(_outputhtml);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void LoadTinhThanh()
    {
        try
        {
            string ma = "";
            string vungmien = "";
            if (!string.IsNullOrEmpty(Request.Form["VungMien"]))
                vungmien = Request.Form["VungMien"];
            DataBaseDb db = new DataBaseDb();
            string sql = "select * from tblDMTinh";
            if (!string.IsNullOrEmpty(vungmien) && vungmien != "-1")
                sql += " where  VungMienID in (" + vungmien + ")";

            DataTable dt = db.GetData(sql);
            if (!string.IsNullOrEmpty(Request.Form["TinhThanh"]))
                ma = Request.Form["TinhThanh"];
            string _outputhtml = "";
            if (ma != "")
                _outputhtml += "<option value=\"-1\"><< Tất cả >></option>";
            else
                _outputhtml += @"<option selected=""selected"" value=""-1""><< Tất cả >></option>";
            foreach (DataRow Tinh in dt.Rows)
            {
                if (ma.Trim() == Tinh["MaTinh"].ToString().Trim())
                {
                    _outputhtml += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
                }
                else
                {
                    _outputhtml += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
                }
            }
            Response.Write(_outputhtml);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}