﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_KSCL_BaoCao_BangTinhHinhNopBaoCaoTuKiemTra : System.Web.UI.UserControl
{
    protected string tenchucnang = "Bảng tình hình nộp báo cáo tự kiểm tra";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = "";
            if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            {
                nam = Request.Form["ddlNam"];

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/BangTinhHinhNopBaoCaoTuKiemTra.rpt"));
                DataTable dt = GetDanhSachNopBaoCao(nam);
                rpt.Database.Tables["dtBangTinhHinhNopBaoCaoTuKiemTra"].SetDataSource(dt);
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "BẢNG TÌNH HÌNH NỘP BÁO CÁO TỰ KIỂM TRA NĂM " + nam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BangTinhHinhNopBaoCaoTuKiemTraNam_" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, nam);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BangTinhHinhNopBaoCaoTuKiemTraNam_" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BangTinhHinhNopBaoCaoTuKiemTraNam_" + nam);
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
        {
            string js = "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboDaNopBaoCao"]))
                js += "$('#cboDaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboDaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboChuaNopBaoCao"]))
                js += "$('#cboChuaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboChuaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            Response.Write(js);
        }
    }

    private DataTable GetDanhSachNopBaoCao(string nam)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "SoHieuCongTy", "TenCongTy", "TenVietTat", "Email", "NgayCapGCNDuDKKDKT", "ThanhVienHangQT", "HVTT", "SoHoiVienVACPA",
            "DuDK_CK", "DuDK_Khac", "TinhTrangNopBC", "NgayNopBC", "MaDanhSach" });
        if (string.IsNullOrEmpty(nam))
            return dt;
        if (string.IsNullOrEmpty(Request.Form["cboDaNopBaoCao"]) && string.IsNullOrEmpty(Request.Form["cboChuaNopBaoCao"]))
            return dt;

        string procName = ListName.Proc_BAOCAO_KSCL_BangTinhHinhNopBaoCaoTuKiemTra;
        List<Hashtable> listData = _db.GetListData(procName, new List<string> { "@Nam" }, new List<object> { nam });
        if (listData.Count > 0)
        {
            DataRow dr;
            int index = 1;
            foreach (Hashtable ht in listData)
            {
                if ((!string.IsNullOrEmpty(Request.Form["cboDaNopBaoCao"]) && !string.IsNullOrEmpty(ht["NgayNop"].ToString()))
                    || (!string.IsNullOrEmpty(Request.Form["cboChuaNopBaoCao"]) && string.IsNullOrEmpty(ht["NgayNop"].ToString())))
                {
                    dr = dt.NewRow();
                    dr["STT"] = index;
                    dr["SoHieuCongTy"] = ht["SoHieu"];
                    dr["TenCongTy"] = ht["TenDoanhNghiep"];
                    dr["TenVietTat"] = ht["TenVietTat"];
                    dr["Email"] = ht["Email"];
                    if (!string.IsNullOrEmpty(ht["NgayCapGiayChungNhanKDDVKT"].ToString()))
                        dr["NgayCapGCNDuDKKDKT"] =
                            Library.DateTimeConvert(ht["NgayCapGiayChungNhanKDDVKT"]).ToString("dd/MM/yyyy");
                    dr["ThanhVienHangQT"] = ht["ThanhVienHangKTQT"];
                    string loaiHoiVien = ht["LoaiHoiVienTapThe"].ToString();
                    if (loaiHoiVien == "1")
                        dr["HVTT"] = ht["x"];
                    dr["SoHoiVienVACPA"] = ht["MaHoiVienTapThe"];
                    dr["DuDK_CK"] = !string.IsNullOrEmpty(ht["NamDuDKKT_CK"].ToString()) ? "x" : "";
                    dr["DuDK_Khac"] = !string.IsNullOrEmpty(ht["NamDuDKKT_Khac"].ToString()) ? "x" : "";
                    dr["TinhTrangNopBC"] = !string.IsNullOrEmpty(ht["NgayNop"].ToString()) ? "Đã nộp" : "Chưa nộp";
                    if (!string.IsNullOrEmpty(ht["NgayNop"].ToString()))
                        dr["NgayNopBC"] = Library.DateTimeConvert(ht["NgayNop"]).ToString("dd/MM/yyyy");
                    dr["MaDanhSach"] = ht["MaDanhSach"];
                    dt.Rows.Add(dr);
                    index++;
                }
            }
        }
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if(DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, string nam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='12' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='12' style='text-align:center;font-size: 14pt; font-weight: bold;'>BẢNG TÌNH HÌNH NỘP BÁO CÁO TỰ KIỂM TRA NĂM " + nam + @"</td>
            </tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn'>STT</td>
                <td class='HeaderColumn' style='width: 70px;'>Số hiệu công ty</td>
                <td class='HeaderColumn' style='width: 170px;'>Tên công ty</td>
                <td class='HeaderColumn' style='width: 70px;'>Tên viết tắt</td>
                <td class='HeaderColumn' style='width: 100px;'>Email</td>                          
                <td class='HeaderColumn' style='width: 100px;'>Ngày cấp GCN đủ ĐKKDKT</td>
                <td class='HeaderColumn' style='width: 100px;'>Thành viên hãng quốc tế</td>
                <td class='HeaderColumn' style='width: 100px;'>HVTT</td>
                <td class='HeaderColumn' style='width: 100px;'>Số hội viên VACPA</td>
                <td class='HeaderColumn' style='width: 100px;'>Được chấp thuận KT cho đơn vị có lợi ích công chúng trong lĩnh vực chứng khoán</td>
                <td class='HeaderColumn' style='width: 100px;'>Được chấp nhận KT cho đơn vị có lợi ích công chúng khác</td>
                <td class='HeaderColumn' style='width: 100px;'>Tình trạng nộp báo cáo</td>
                <td class='HeaderColumn' style='width: 100px;'>Ngày nộp báo cáo</td>                
                            </tr>";
        string tempMaDanhSach = "";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string maDanhSach = dt.Rows[i]["MaDanhSach"].ToString();

            if (maDanhSach != tempMaDanhSach)
            {
                tempMaDanhSach = maDanhSach;
                htmlExport += "<tr><td colspan='13' class='GroupTitle'>" + maDanhSach + "</td></tr>";
            }
            htmlExport += "<tr><td class='Column'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoHieuCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenVietTat"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["Email"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NgayCapGCNDuDKKDKT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["ThanhVienHangQT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["HVTT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoHoiVienVACPA"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DuDK_CK"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DuDK_Khac"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TinhTrangNopBC"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NgayNopBC"] + "</td></tr>";
        }
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("BangTinhHinhNopBaoCaoTuKiemTraNam_" + nam + ".xsl", "<html>" + css + htmlExport.Replace("<br>", "\n").Replace("<br />", "\n") + "</html>");
    }
}