﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer.ClientDoc;
using ReportDocument = CrystalDecisions.CrystalReports.Engine.ReportDocument;
using TextObject = CrystalDecisions.CrystalReports.Engine.TextObject;
using System.Drawing;

public partial class usercontrols_CNKT_BaoCao_MauDangKyThamDuCNKT : System.Web.UI.UserControl
{
    protected string tenchucnang = "Mẫu đăng ký tham dự cập nhật kiến thức";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string idLopHoc = Request.Form["hdLopHocID"];
            string idCongTy = "1";//Request.Form["hdCongTyID"];
            if (!string.IsNullOrEmpty(idLopHoc))
            {
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/CNKT/MauDangKyThamDuCapNhatKienThuc.rpt"));
                rpt.Database.Tables["dtDanhSachDangKyHoc"].SetDataSource(GetBangDangKyHoc(idLopHoc, idCongTy));
                GetThongTinLopHoc(idLopHoc, rpt);
                //GetThongTinCongTy(idCongTy, rpt);
                
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "MauDangKyThamDuCNKT");
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "MauDangKyThamDuCNKT");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "MauDangKyThamDuCNKT");
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string js = "";
        if (!string.IsNullOrEmpty(Request.Form["txtMaLopHoc"]))
        {
            js += "$('#txtMaLopHoc').val('" + Request.Form["txtMaLopHoc"] + "');" + Environment.NewLine;
            js += "CallActionGetInforLopHoc();" + Environment.NewLine;
        }

        //if (!string.IsNullOrEmpty(Request.Form["txtMaHocVien"]))
        //{
        //    js += "$('#txtMaHocVien').val('" + Request.Form["txtMaHocVien"] + "');" + Environment.NewLine;
        //    js += "CallActionGetInforHocVien();" + Environment.NewLine;
        //}
        Response.Write(js);
    }

    private void GetThongTinLopHoc(string idLopHoc, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idLopHoc))
            return;
        string query = "SELECT MaLopHoc, TenLopHoc, TuNgay, DenNgay, TenTinh, DiaChiLopHoc FROM tblCNKTLopHoc LH LEFT JOIN tblDMTinh ON LH.TinhThanhID = tblDMTinh.TinhID WHERE LopHocID = " + idLopHoc;
        List<Hashtable> listData = _db.GetListData(query);
        {
            Hashtable ht = listData[0];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtDiaDiemNgayKy"]).Text = ht["TenTinh"] + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "MẪU ĐĂNG KÝ THAM DỰ CẬP NHẬT KIẾN THỨC LỚP  " + ht["MaLopHoc"];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNgayHoc"]).Text = "Ngày " + Library.DateTimeConvert(ht["TuNgay"]).ToString("dd/MM/yyyy") + " - " + Library.DateTimeConvert(ht["DenNgay"]).ToString("dd/MM/yyyy");

            DateTime tuNgay = Library.DateTimeConvert(ht["TuNgay"]);
            DateTime denNgay = Library.DateTimeConvert(ht["DenNgay"]);

            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "STT", "HoVaTenKTV", "LaHoiVienVacpa", "SoChungChiKTV", "NgayCapChungChiKTV" });
            int maxRight = 0;
            double countDays = (denNgay - tuNgay).TotalDays;
            for (int i = 0; i <= countDays; i ++)
            {
                DateTime day = tuNgay.AddDays(i);
                int width = 1200;
                maxRight = (7320 + width + 50) + (width * i) + (i * 100);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControl" + i, day.ToString("dd/MM/yyyy"), 7320 + (width * i) + (i * 100), 4800, width, null, null, 12, FontStyle.Bold, false, false, "");
                if (i < countDays)
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControl" + i, maxRight, maxRight, 4680, 5280, true);
                else
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControl" + i, maxRight, maxRight, 4200, 5280, true);

                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControl" + i, maxRight, maxRight, 0, 479, true);
                //CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "Ngay" + i, "fControl" + i, 7320 + (width * i) + (i * 100), 120, width, null, null, 12, null, true, false, "");
            }
            for (int i = 1; i <= 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["STT"] = i;
                dt.Rows.Add(dr);
            }
            ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lTopHeader"]).Right = maxRight;
            ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lMiddleHeader"]).Right = maxRight;
            ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomHeader"]).Right = maxRight;
            ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomDetail"]).Right = maxRight;

            rpt.Database.Tables["dtDanhSachDangKyHoc"].SetDataSource(dt);
        }
    }

    private void GetThongTinCongTy(string idCongTy, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(idCongTy))
            return;
        string query = "SELECT TenDoanhNghiep, DiaChi, MaSoThue, NguoiDaiDienLL_Ten, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + idCongTy;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTenCongTy"]).Text = ht["TenDoanhNghiep"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtDiaChi"]).Text = ht["DiaChi"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtMaSoThue"]).Text = ht["MaSoThue"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLL"]).Text = ht["NguoiDaiDienLL_Ten"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLL_DT"]).Text = ht["NguoiDaiDienLL_DiDong"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLL_Email"]).Text = ht["NguoiDaiDienLL_Email"].ToString();
        }
    }

    private DataTable GetBangDangKyHoc(string idLopHoc, string idCongTy)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "HoVaTenKTV", "LaHoiVienVacpa", "SoChungChiKTV", "NgayCapChungChiKTV", "NgayHoc", "CoDangKy" });
        string query = @"SELECT tblCNKTDangKyHocCaNhan.DangKyHocCaNhanID as dkhcnid, tblHoiVienCaNhan.HoiVienCaNhanID HoiVienCaNhanID,
	tblHoiVienCaNhan.MaHoiVienCaNhan AS mhvcn, (tblHoiVienCaNhan.HoDem + ' ' + tblHoiVienCaNhan.Ten) AS FullName,
	tblHoiVienCaNhan.LoaiHoiVienCaNhan AS LoaiHoiVienCaNhan, tblHoiVienCaNhan.SoChungChiKTV AS SoChungChiKTV, tblHoiVienCaNhan.NgayCapChungChiKTV AS NgayCapChungChiKTV,
	tblCNKTDangKyHocCaNhan.NgayDangKy AS NgayDangKy, tblCNKTDangKyHocCaNhan.LayGiayChungNhan AS layGCN,
	tblCNKTDangKyHocCaNhan.TongSoGioCNKT AS soGioCNKT, tblCNKTDangKyHocCaNhan.TongPhi AS TongPhi, tblHoiVienTapThe.HoiVienTapTheID AS hoiVienTapTheID, tblHoiVienTapThe.MaHoiVienTapThe AS maHoiVienTapThe,
	tblHoiVienTapThe.TenDoanhNghiep AS TenDoanhNghiep, tblCNKTLopHoc.MaLopHoc AS MaLopHoc
FROM tblCNKTDangKyHocCaNhan 
LEFT JOIN tblHoiVienCaNhan ON tblCNKTDangKyHocCaNhan.HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID
LEFT JOIN tblHoiVienTapThe ON tblCNKTDangKyHocCaNhan.HoiVienTapTheDangKy = tblHoiVienTapThe.HoiVienTapTheID
LEFT JOIN tblCNKTLopHoc ON tblCNKTDangKyHocCaNhan.LopHocID = tblCNKTLopHoc.LopHocID
WHERE tblCNKTDangKyHocCaNhan.LopHocID = " + idLopHoc + @" AND tblCNKTDangKyHocCaNhan.HoiVienTapTheID = " + idCongTy + @" AND (tblCNKTDangKyHocCaNhan.HoiVienTapTheDangKy IS NOT NULL AND tblCNKTDangKyHocCaNhan.HoiVienTapTheDangKy != '')
ORDER BY tblHoiVienTapThe.SoHieu ASC, tblHoiVienCaNhan.SoChungChiKTV ASC";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            int index = 1;
            for (int i = 1; i <= 4; i++)
            {
                foreach (Hashtable ht in listData)
                {
                    DataRow dr = dt.NewRow();
                    dr["STT"] = index;
                    dr["HoVaTenKTV"] = ht["FullName"];
                    string loaiHoiVien = ht["LoaiHoiVienCaNhan"].ToString();
                    if (loaiHoiVien == "1")
                        dr["LaHoiVienVacpa"] = "x";
                    dr["SoChungChiKTV"] = ht["SoChungChiKTV"];
                    //dr["NgayCapChungChiKTV"] = Library.DateTimeConvert(ht["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy");
                    dr["NgayCapChungChiKTV"] = Library.DateTimeConvert(ht["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy");
                    dr["NgayHoc"] = "Ngày " + i;
                    if(i%2 == 0)
                        dr["CoDangKy"] = "x";
                    dr["CoDangKy"] = "v";
                    dt.Rows.Add(dr);
                    index++;
                }
            }
        }
        return dt;
    }
}