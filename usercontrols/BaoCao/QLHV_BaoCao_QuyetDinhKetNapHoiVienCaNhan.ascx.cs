﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BaoCao_QuyetDinhKetNapHoiVienCaNhan : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quyết định kết nạp hội viên cá nhân";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID = " + Convert.ToInt32(VM);
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }



    public void DonVi(string ma = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (ma == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    public void setdataCrystalReport()
    {
        string VungMien = Request.Form["VungMienId"];
        string Tinh = Request.Form["TinhId"];
        //string DonViId = Request.Form["txtID"];

        //DonViId = "Deloitte001";
        DateTime? NgayBatDau = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayBatDau"]))
            NgayBatDau = DateTime.ParseExact(Request.Form["NgayBatDau"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime? NgayKetThuc = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayKetThuc"]))
            NgayKetThuc = DateTime.ParseExact(Request.Form["NgayKetThuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime? NgayQuyetDinh = null;
        //if (!string.IsNullOrEmpty(Request.Form["NgayQuyetDinh"]))
        //    NgayQuyetDinh = DateTime.ParseExact(Request.Form["NgayQuyetDinh"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

        string result = "";
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            result = string.Join(",", lstIdChon);
        }
        if (!string.IsNullOrEmpty(result))
        {
            List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
            List<DanhSachKetNapHoiVienCaNhan> lst = new List<DanhSachKetNapHoiVienCaNhan>();


            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select a.SoQuyetDinhKetNap, HoDem + ' ' + Ten as HoTen, NgaySinh, GioiTinh, DonViCongTac, "
                             + " SoChungChiKTV, NgayCapChungChiKTV, a.NgayQuyetDinhKetNap "
                             + " from dbo.tblHoiVienCaNhan a  "
                             + " left join dbo.tblHoiVienTapThe b on a.HoiVienTapTheID = b.HoiVienTapTheID "
                             + " where a.mahoiviencanhan = '" + result + "'";

            //if (!string.IsNullOrEmpty(DonViId))
            //    sql.CommandText = sql.CommandText + " AND b.MaHoiVienTapThe = '" + DonViId + "'";
          //  string strNgay = NgayQuyetDinh.Value.Year + "-" + NgayQuyetDinh.Value.Month + "-" + NgayQuyetDinh.Value.Day;
            //if (NgayQuyetDinh != null)
            //    sql.Parameters.AddWithValue("@NgayQuyetDinh", strNgay);
            //else
            //    sql.Parameters.AddWithValue("@NgayQuyetDinh", DBNull.Value);
            //if (NgayKetThuc != null)
            //    sql.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);
            //else
            //    sql.Parameters.AddWithValue("@NgayKetThuc", DBNull.Value);

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            int i = 0;
            string SoQD = "";
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                string gt = "Ông ";
                string NamSinh = "";
                if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                {
                    string strNS = NgayThangVN(Convert.ToDateTime(tem["NgaySinh"]));
                    NamSinh = Convert.ToDateTime(tem["NgaySinh"]).Year.ToString();
                    if (tem["GioiTinh"].ToString() != "1")
                        gt = "Bà ";
                }
                DanhSachKetNapHoiVienCaNhan new1 = new DanhSachKetNapHoiVienCaNhan();
                new1.STT = i.ToString();
                new1.HoTen = gt + tem["HoTen"].ToString().Trim();
                new1.MaSo = gt + tem["HoTen"].ToString().Trim() + ";";
                new1.NgaySinh_Nam = ", sinh năm " + NamSinh;
                new1.So_CCKTV = tem["SoChungChiKTV"].ToString();
                if (!string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()))
                    new1.Ngay_CCKTV = NgayThangVN(Convert.ToDateTime(tem["NgayCapChungChiKTV"]));
                new1.DonViCongTac = tem["DonViCongTac"].ToString();
                SoQD = tem["SoQuyetDinhKetNap"].ToString();
                lst.Add(new1);

                if (!string.IsNullOrEmpty(tem["NgayQuyetDinhKetNap"].ToString()))
                    NgayQuyetDinh = Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]);
            }

            TenBaoCao newabc = new TenBaoCao();
            newabc.SoLuong = i.ToString();
            newabc.TenBC = SoQD;
            newabc.TongCong = "Tổng cộng: " + i.ToString() + " Hội viên cá nhân";
            try{
            newabc.ngayBC = NgayThangVN(NgayQuyetDinh.Value);
            newabc.NgayTinh = "Hà Nội, ngày " + (NgayQuyetDinh.Value.Day.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Day : NgayQuyetDinh.Value.Day.ToString()) + " tháng " + (NgayQuyetDinh.Value.Month.ToString().Length == 1 ? "0" + NgayQuyetDinh.Value.Month : NgayQuyetDinh.Value.Month.ToString()) + " năm " + NgayQuyetDinh.Value.Year;
            }
            catch { }
            lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/QuyetDinhKetNapHoiVienCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["DanhSachKetNapHoiVienCaNhan"].SetDataSource(lst);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "QuyetDinhKetNapHoiVienCaNhan");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "QuyetDinhKetNapHoiVienCaNhan");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }


        }
    }

    //protected void Text_Changed_2(object sender, EventArgs e)
    //{
    //    //txtCongTy.Text = "";
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        SqlCommand sql = new SqlCommand();
    //        sql.CommandText = "select TenDoanhNghiep "
    //                    + " from dbo.tblHoiVienTapThe "
    //                    + " WHERE MaHoiVienTapThe = '" + idDanhSach.Text + "'";// + id;
    //        ds = DataAccess.RunCMDGetDataSet(sql);
    //        sql.Connection.Close();
    //        sql.Connection.Dispose();
    //        sql = null;

    //        DataTable dt = ds.Tables[0];
    //        foreach (DataRow tem in dt.Rows)
    //            txtCongTy.Text = tem["TenDoanhNghiep"].ToString();
    //    }
    //    catch { }
    //}

    protected void btnChay2_Click(object sender, EventArgs e)
    {
      //  txtCongTy.Text = "";
        //if (Session["DSChon"] != null)
        //{
        //    List<string> lstIdChon = new List<string>();
        //    lstIdChon = Session["DSChon"] as List<string>;
        //    string result = string.Join(",", lstIdChon);
        //    idDanhSach.Text = result;

        //    //if (lstIdChon.Count() != 0)
        //    //{
        //    //    DataSet ds = new DataSet();
        //    //    SqlCommand sql = new SqlCommand();
        //    //    sql.CommandText = "select TenDoanhNghiep "
        //    //            + " from dbo.tblHoiVienTapThe "
        //    //            + " WHERE MaHoiVienTapThe = '" + lstIdChon[0] + "'";// + id;
        //    //    ds = DataAccess.RunCMDGetDataSet(sql);
        //    //    sql.Connection.Close();
        //    //    sql.Connection.Dispose();
        //    //    sql = null;

        //    //    DataTable dt = ds.Tables[0];
        //    //    foreach (DataRow tem in dt.Rows)
        //    //        txtCongTy.Text = tem["TenDoanhNghiep"].ToString();
        //    //}
        //    CrystalReportViewer1.ReportSource = null;
        //    CrystalReportViewer1.DataBind();
        //}
    }

     protected void btnChay_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            txtID.Text = result;
        }
    }
}