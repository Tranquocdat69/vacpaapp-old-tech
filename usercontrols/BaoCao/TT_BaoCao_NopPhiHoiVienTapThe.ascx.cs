﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.Collections;
public partial class usercontrols_TT_BaoCao_NopPhiHoiVienTapThe : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo nộp hội phí tổ chức";
    Db _db = new Db(ListName.ConnectionString);
    private Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        DataBaseDb db = new DataBaseDb();
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));


        LoadListHoiVienTapThe();

        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {

            string strIDVungMien = string.Empty;
            string strIDTinhThanh = string.Empty;
            string strConNo = string.Empty;
            string strKhongConNo = string.Empty;
            string strNgayChotSL = "";
            string strNgayChotSLDen = "";
            string HoiVienTapTheID = Request.Form["BoPhanCongTac"];
            if (!string.IsNullOrEmpty(Request.Form["VungMien"]) && Request.Form["VungMien"] != "-1")
            {
                strIDVungMien = Request.Form["VungMien"];
            }
            if (!string.IsNullOrEmpty(Request.Form["TinhThanh"]) && Request.Form["TinhThanh"] != "-1")
            {
                strIDTinhThanh = Request.Form["TinhThanh"];
            }
            if (!string.IsNullOrEmpty(Request.Form["hdConNoPhi"]) && Request.Form["hdConNoPhi"] == "1")
            {
                strConNo = "yes";
            }
            if (!string.IsNullOrEmpty(Request.Form["hdKhongConNoPhi"]) && Request.Form["hdKhongConNoPhi"] == "1")
            {
                strKhongConNo = "yes";
            }

            if (!string.IsNullOrEmpty(Request.Form["txtNgayLapBCDen"]))
            {
                
                strNgayChotSLDen = Request.Form["txtNgayLapBCDen"];
                DateTime dtDen = Library.DateTimeConvert(strNgayChotSLDen, "dd/MM/yyyy");
                DateTime dt = new DateTime();

                DataSet ds = new DataSet();
                DataTable dt1 = new DataTable();
                if (!string.IsNullOrEmpty(Request.Form["txtNgayLapBC"]))
                {
                    dt = Library.DateTimeConvert(Request.Form["txtNgayLapBC"], "dd/MM/yyyy");
                    string[] paraName = { "@VungMienID", "@TinhThanhID", "@NgayChotSoLieu", "@ConNo", "@KhongConNo", "@HVTTID", "@NgayChotSoLieuDen" };
                    object[] paraValue = { strIDVungMien, strIDTinhThanh, dt, strConNo, strKhongConNo, HoiVienTapTheID, dtDen };
                    ds = db.GetData(paraValue, paraName, "proc_BAOCAO_THANHTOAN_NOPHOIPHITAPTHE", "NopHoiPhiTapThe");
                }
                else
                {
                    string[] paraName = { "@VungMienID", "@TinhThanhID", "@ConNo", "@KhongConNo", "@HVTTID", "@NgayChotSoLieuDen" };
                    object[] paraValue = { strIDVungMien, strIDTinhThanh, strConNo, strKhongConNo, HoiVienTapTheID, dtDen };
                    ds = db.GetData(paraValue, paraName, "proc_BAOCAO_THANHTOAN_NOPHOIPHITAPTHE", "NopHoiPhiTapThe");
                }
                //dsThanhToan ds = new dsThanhToan();
                
                // dt1.TableName = "NopHoiPhiCaNhan";
                
                //ds.Tables.Add(dt1);
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/ThanhToanRpts/rptBaoCaoNopHoiPhiTapThe.rpt"));
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtThoiGian"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiLapBaoCao"]).Text = cm.Admin_HoVaTen;
                rpt.SetDataSource(ds);

                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoNopHoiPhiTapThe");
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(ds.Tables[0]);
                //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BaoCaoNopHoiPhiTapThe");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoNopHoiPhiTapThe");
            }

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            //  _db.CloseConnection();
        }
        finally
        {
            // _db.CloseConnection();
        }
    }

    private void LoadListHoiVienTapThe()
    {
        _db.OpenConnection();

        string query = "SELECT * FROM (SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE '--- ' + ISNULL(A.SoHieu,LTRIM(RTRIM(A.MaHoiVienTapThe))) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID WHERE A.LoaiHoiVienTapThe = 1) AS X ORDER BY X.SoHieu ASC";
        BoPhanCongTac.Controls.Add(new LiteralControl("<option value=''>Tất cả</option>"));
        List<Hashtable> listData = _db.GetListData(query);
        foreach (Hashtable ht in listData)
        {
            BoPhanCongTac.Controls.Add(new LiteralControl("<option value='" + ht["HoiVienTapTheID"].ToString() + "'>" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"].ToString()) + "</option>"));

        }
        _db.CloseConnection();
    }

    protected void SetDataJavascript()
    {
        string js = "";
        //if (!string.IsNullOrEmpty(Request.Form["txtNgayLapBC"]))
        //    js += "$('#txtNgayLapBC').val('" + Request.Form["txtNgayLapBC"] + "');" + Environment.NewLine;
        //else
        //    js += "$('#txtNgayLapBC').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["txtNgayLapBCDen"]))
            js += "$('#txtNgayLapBCDen').val('" + Request.Form["txtNgayLapBCDen"] + "');" + Environment.NewLine;
        else
            js += "$('#txtNgayLapBCDen').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["VungMien"]))
        {
            js += "$('#VungMien').val('" + Request.Form["VungMien"] + "');" + Environment.NewLine;
            js += "$('#TinhThanh').val('" + Request.Form["TinhThanh"] + "');" + Environment.NewLine;
        }
        if (!string.IsNullOrEmpty(Request.Form["cbConNoPhi"]))
            js += "$('#cbConNoPhi').prop('checked', true);" + Environment.NewLine;
        if (!string.IsNullOrEmpty(Request.Form["cbKhongConNoPhi"]))
            js += "$('#cbKhongConNoPhi').prop('checked', true);" + Environment.NewLine;
        Response.Write(js);
    }

    public void LoadVungMien()
    {
        try
        {
            string ma = "";
            DataBaseDb db = new DataBaseDb();
            DataTable dt = db.GetData("select * from tblDMVungMien");
            if (!string.IsNullOrEmpty(Request.Form["VungMien"]))
                ma = Request.Form["VungMien"];
            string _outputhtml = "";
            if (ma != "")
                _outputhtml += "<option value=\"-1\">Tất cả</option>";
            else
                _outputhtml += @"<option selected=""selected"" value=""-1"">Tất cả</option>";

            foreach (DataRow dr in dt.Rows)
            {
                if (ma == dr["VungMienID"].ToString())
                    _outputhtml += "<option selected=\"selected\" value=\"" + dr["VungMienID"] + "\">" + dr["TenVungMien"] + "</option>";
                else
                    _outputhtml += "<option  value=\"" + dr["VungMienID"] + "\">" + dr["TenVungMien"] + "</option>";
            }
            Response.Write(_outputhtml);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void LoadTinhThanh()
    {
        try
        {
            string ma = "";
            string vungmien = "";
            if (!string.IsNullOrEmpty(Request.Form["VungMien"]))
                vungmien = Request.Form["VungMien"];
            DataBaseDb db = new DataBaseDb();
            string sql = "select * from tblDMTinh";
            if (!string.IsNullOrEmpty(vungmien) && vungmien != "-1")
                sql += " where  VungMienID in (" + vungmien + ")";

            DataTable dt = db.GetData(sql);
            if (!string.IsNullOrEmpty(Request.Form["TinhThanh"]))
                ma = Request.Form["TinhThanh"];
            string _outputhtml = "";
            if (ma != "")
                _outputhtml += "<option value=\"-1\"><< Tất cả >></option>";
            else
                _outputhtml += @"<option selected=""selected"" value=""-1""><< Tất cả >></option>";
            foreach (DataRow Tinh in dt.Rows)
            {
                if (ma.Trim() == Tinh["MaTinh"].ToString().Trim())
                {
                    _outputhtml += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
                }
                else
                {
                    _outputhtml += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
                }
            }
            Response.Write(_outputhtml);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void ExportExcel(DataTable dt)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }
        
        .HeaderColumn1
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='10' style='height: 80px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='10' style='text-align:center;font-size: 14pt; font-weight: bold;'>BÁO CÁO NỘP HỘI PHÍ TẬP THỂ</td>
            </tr>            
            <tr><td colspan='10'></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn1'>STT</td>                
                <td class='HeaderColumn1' style='width: 100px;'>Số hiệu công ty</td>
                <td class='HeaderColumn1' style='width: 150px;'>Tên công ty</td>
                <td class='HeaderColumn1' style='width: 100px;'>Ngày thành lập</td>
                <td class='HeaderColumn1' style='width: 100px;'>Người đại diện (giám đốc)</td>
                <td class='HeaderColumn1' style='width: 100px;'>Địa chỉ</td>
                <td class='HeaderColumn1' style='width: 100px;'>Email</td>
                <td class='HeaderColumn1' style='width: 100px;'>Hội phí còn nợ các năm trước</td>
                <td class='HeaderColumn1' style='width: 100px;'>Hội phí phải nộp năm nay</td>
                <td class='HeaderColumn1' style='width: 100px;'>Tổng hội phí đã nộp</td>
                <td class='HeaderColumn1' style='width: 100px;'>Số hội phí còn nợ</td>                
                            </tr>";

        double phiPhaiNopNamNay = 0,
               hoiPhiConNoCacNamTruoc = 0,
               tongHoiPhiDaNop = 0,
               soPhiConNo = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string tenDoanhNghiep = dt.Rows[i]["TenDoanhNghiep"].ToString();

            htmlExport += "<tr><td class='Column'>" + (i + 1) + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoHieu"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenDoanhNghiep"] + "</td>" +
                          "<td class='Column'>" + (!string.IsNullOrEmpty(dt.Rows[i]["NgayThanhLap"].ToString()) ? Library.DateTimeConvert(dt.Rows[i]["NgayThanhLap"]).ToString("dd/MM/yyyy") : "") + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NguoiDaiDien"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DiaChi"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["Email"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["HoiPhiConNoCacNamTruoc"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["PhiPhaiNopNamNay"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TongHoiPhiDaNop"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoPhiConNo"] + "</td></tr>";
            phiPhaiNopNamNay += Library.DoubleConvert(dt.Rows[i]["PhiPhaiNopNamNay"]);
            hoiPhiConNoCacNamTruoc += Library.DoubleConvert(dt.Rows[i]["HoiPhiConNoCacNamTruoc"]);
            tongHoiPhiDaNop += Library.DoubleConvert(dt.Rows[i]["TongHoiPhiDaNop"]);
            soPhiConNo += Library.DoubleConvert(dt.Rows[i]["SoPhiConNo"]);
        }
        htmlExport += "<tr><td></td><td></td><td style='font-weight: bold;'>Tổng số</td>" +
                      "<td></td><td></td><td></td><td></td>" +
                      "<td style='font-weight: bold;'>" + phiPhaiNopNamNay + "</td>" +
                      "<td style='font-weight: bold;'>" + hoiPhiConNoCacNamTruoc + "</td>" +
                      "<td style='font-weight: bold;'>" + tongHoiPhiDaNop + "</td>" +
                      "<td style='font-weight: bold;'>" + soPhiConNo + "</td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='6'></td><td colspan='4' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td colspan='4' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td></td></tr>";
        htmlExport += "<tr><td colspan='6'></td><td colspan='4' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("BaoCaoNopHoiPhiTapThe.xsl", "<html>" + css + htmlExport + "</html>");

    }
}