﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_QLHV_BaoCao_DanhSachKetNapHoiVienCaNhan : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách kết nạp hội viên cá nhân";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim() == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }



    public void DonVi(string ma = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheID,TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (ma == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    public void setdataCrystalReport()
    {
        string VungMien = Request.Form["VungMienId"];
        string Tinh = Request.Form["TinhId"];
        string DonViId = idDanhSach.Text;
        DateTime? NgayBatDau = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayBatDau"]))
            NgayBatDau = DateTime.ParseExact(Request.Form["NgayBatDau"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime? NgayKetThuc = null;
        if (!string.IsNullOrEmpty(Request.Form["NgayKetThuc"]))
            NgayKetThuc = DateTime.ParseExact(Request.Form["NgayKetThuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

        //if (NgayBatDau != null && NgayKetThuc != null)
        {
            List<TenBaoCao> lstTenBC = new List<TenBaoCao>();
            List<DanhSachKetNapHoiVienCaNhan> lst = new List<DanhSachKetNapHoiVienCaNhan>();


            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"select * from
                                (
                                select g.hoivientaptheid, a.LoaiHoiVienCaNhanChiTiet, MaHoiVienCaNhan, HoDem + ' ' + Ten as HoTen, Ten, NgaySinh, GioiTinh, b.TenTinh, HocViID, 
                                HocHamID, SoCMND, CMND_NgayCap, SoTaiKhoanNganHang, c.TenNganHang, SoChungChiKTV, NgayCapChungChiKTV, SoGiayChungNhanDKHN, 
                                HanCapTu, HanCapDen, d.SoChungChi, d.NgayCap, e.TenChucVu, a.DonViCongTac, a.DiaChi, a.DienThoai, a.Mobile, a.Email, 
                                CASE ISNULL(g.HoiVienTapTheChaID,0) WHEN 0 THEN g.SoHieu ELSE h.SoHieu END AS SoHieuOrder, CASE ISNULL(g.HoiVienTapTheChaID,0) WHEN 0 THEN g.SoHieu ELSE g.MaHoiVienTapThe END AS SoHieu, g.TenDoanhNghiep, a.SoQuyetDinhKetNap, a.NgayQuyetDinhKetNap  
                                from dbo.tblHoiVienCaNhan a  
                                left join dbo.tblDMTinh b on a.QueQuan_TinhID = b.MaTinh 
                                left join tblDMNganHang c on a.NganHangID = c.NganHangID 
                                left join tblDonHoiVienCaNhanChungChi d on a.HoiVienCaNhanID = d.HoiVienCaNhanID 
                                left join tblDMChucVu e on a.ChucVuID = e.ChucVuID 
                                left join tblDonHoiVienTapThe f on a.HoiVienTapTheID = f.HoiVienTapTheID 
                                inner join dbo.tblHoiVienTapThe g on a.HoiVienTapTheID = g.HoiVienTapTheID
                                LEFT JOIN tblHoiVienTapThe h ON g.HoiVienTapTheChaID = h.HoiVienTapTheID
                                where a.LoaiHoiVienCaNhan = 1

                                union all

                                select 9999 as hoivientaptheid, a.LoaiHoiVienCaNhanChiTiet, MaHoiVienCaNhan, HoDem + ' ' + Ten as HoTen, Ten, NgaySinh, GioiTinh, b.TenTinh, HocViID, 
                                HocHamID, SoCMND, CMND_NgayCap, SoTaiKhoanNganHang, c.TenNganHang, SoChungChiKTV, NgayCapChungChiKTV, SoGiayChungNhanDKHN, 
                                HanCapTu, HanCapDen, d.SoChungChi, d.NgayCap, e.TenChucVu, a.DonViCongTac, a.DiaChi, a.DienThoai, a.Mobile, a.Email, 
                                '9999' as SoHieuOrder, '9999' as SoHieu, N'Đơn vị khác' as TenDoanhNghiep, a.SoQuyetDinhKetNap, a.NgayQuyetDinhKetNap  
                                from dbo.tblHoiVienCaNhan a  
                                left join dbo.tblDMTinh b on a.QueQuan_TinhID = b.MaTinh 
                                left join tblDMNganHang c on a.NganHangID = c.NganHangID 
                                left join tblDonHoiVienCaNhanChungChi d on a.HoiVienCaNhanID = d.HoiVienCaNhanID 
                                left join tblDMChucVu e on a.ChucVuID = e.ChucVuID 
                                left join tblDonHoiVienTapThe f on a.HoiVienTapTheID = f.HoiVienTapTheID
                                where a.HoiVienTapTheID is null and a.LoaiHoiVienCaNhan = 1
                                ) X where 1=1";

            if (!string.IsNullOrEmpty(Request.Form["drLoaiHoiVienCN"]) && Request.Form["drLoaiHoiVienCN"] != "-1")
                sql.CommandText = sql.CommandText + " AND X.LoaiHoiVienCaNhanChiTiet = " + Request.Form["drLoaiHoiVienCN"];

            if (!string.IsNullOrEmpty(DonViId))
                sql.CommandText = sql.CommandText + " AND X.MaHoiVienTapThe = '" + DonViId + "'";
            if (NgayBatDau != null && NgayKetThuc != null)
                sql.CommandText = sql.CommandText + " AND X.NgayQuyetDinhKetNap >= @NgayBatDau and X.NgayQuyetDinhKetNap <= @NgayKetThuc";

            sql.CommandText = sql.CommandText + " order by X.SoHieu, X.TenDoanhNghiep, X.Ten";

            if (NgayBatDau != null)
                sql.Parameters.AddWithValue("@NgayBatDau", NgayBatDau);
            else
                sql.Parameters.AddWithValue("@NgayBatDau", DBNull.Value);
            if (NgayKetThuc != null)
                sql.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);
            else
                sql.Parameters.AddWithValue("@NgayKetThuc", DBNull.Value);

            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];

            int i = 0;
            foreach (DataRow tem in dt.Rows)
            {
                i++;
                DanhSachKetNapHoiVienCaNhan new1 = new DanhSachKetNapHoiVienCaNhan();
                new1.STT = i.ToString();
                new1.MaSo = tem["MaHoiVienCaNhan"].ToString();
                if (!string.IsNullOrEmpty(tem["LoaiHoiVienCaNhanChiTiet"].ToString()))
                {
                    switch (tem["LoaiHoiVienCaNhanChiTiet"].ToString())
                    {
                        case "0":
                            new1.LoaiHoiVien = "Hội viên chính thức";
                            break;
                        case "1":
                            new1.LoaiHoiVien = "Hội viên liên kết";
                            break;
                        case "2":
                            new1.LoaiHoiVien = "Hội viên cao cấp";
                            break;
                        case "3":
                            new1.LoaiHoiVien = "Hội viên danh dự";
                            break;
                        case "4":
                            new1.LoaiHoiVien = "Hội viên làm việc chuyên trách tại các Văn phòng VACPA";
                            break;
                        case "5":
                            new1.LoaiHoiVien = "Hội viên đã nghỉ hưu và không làm việc tại một công ty nào khác";
                            break;
                        case "6":
                            new1.LoaiHoiVien = "Hội viên đang làm việc tại cơ quan quản lý Nhà nước có liên quan đến nghề nghiệp, hoạt động của VACPA";
                            break;
                        default:
                            break;
                    }
                }
                new1.HoTen = tem["HoTen"].ToString();
                new1.TenCT = "<" + tem["SoHieu"].ToString() + "><" + tem["TenDoanhNghiep"].ToString().Replace("\n", "") + ">";
                if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                {
                    string NS = NgayThangVN(Convert.ToDateTime(tem["NgaySinh"]));
                    if (tem["GioiTinh"].ToString() == "1")
                        new1.NgaySinh_Nam = NS;
                    else
                        new1.NgaySinh_Nu = NS;
                }
                new1.QueQuan = tem["TenTinh"].ToString();
                if (tem["HocViID"].ToString() == "1")
                    new1.CuNhan = "X";
                else if (tem["HocViID"].ToString() == "3")
                    new1.ThacSi = "X";
                else if (tem["HocViID"].ToString() == "4")
                    new1.TienSi = "X";

                if (!string.IsNullOrEmpty(tem["HocHamID"].ToString()))
                    new1.GS = "X";
                new1.CMT = tem["SoCMND"].ToString();
                new1.NgayCap = !string.IsNullOrEmpty(tem["CMND_NgayCap"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["CMND_NgayCap"])) : "";
                new1.SoTK = tem["SoTaiKhoanNganHang"].ToString();
                new1.TaiNH = tem["TenNganHang"].ToString();
                new1.So_CCKTV = tem["SoChungChiKTV"].ToString();
                new1.Ngay_CCKTV = !string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCapChungChiKTV"])) : "";
                new1.SoGiayCN = tem["SoGiayChungNhanDKHN"].ToString();
                new1.GiayCN_Tu = !string.IsNullOrEmpty(tem["HanCapTu"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapTu"])) : "";
                new1.GiayCN_Den = !string.IsNullOrEmpty(tem["HanCapDen"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["HanCapDen"])) : "";
                new1.ChungChiQT_So = tem["SoChungChi"].ToString();
                new1.ChungChiQT_Ngay = !string.IsNullOrEmpty(tem["NgayCap"].ToString()) ? NgayThangVN(Convert.ToDateTime(tem["NgayCap"])) : "";
                new1.ChucVuHienNay = tem["TenChucVu"].ToString();
                new1.DonViCongTac = tem["DonViCongTac"].ToString();
                new1.ChoOHienNay = tem["DiaChi"].ToString();
                new1.DTCD = tem["DienThoai"].ToString();
                new1.Mobile = tem["Mobile"].ToString();
                new1.Email = tem["Email"].ToString();
                new1.SoQDKetNap = tem["SoQuyetDinhKetNap"].ToString();
                new1.NgayQDKetNap = tem["NgayQuyetDinhKetNap"].ToString();

                lst.Add(new1);

            }

            List<NguoiLapBieu> lstNguoiLapBieu = new List<NguoiLapBieu>();
            NguoiLapBieu nlb = new NguoiLapBieu();
            nlb.nguoiLapBieu = cm.Admin_HoVaTen;
            lstNguoiLapBieu.Add(nlb);

            TenBaoCao newabc = new TenBaoCao();
            newabc.TongCong = "Tổng cộng: " + i.ToString();
            string NgayTinh = "";
            string ngayBC = "";
            if (NgayBatDau != null && NgayKetThuc != null)
                NgayTinh = "Tính từ " + NgayThangVN(NgayBatDau.Value) + " - " + NgayThangVN(NgayKetThuc.Value);
            else
                NgayTinh = "Tính đến thời điểm hiện tại";

            newabc.NgayTinh = NgayTinh;

            ngayBC = "Hà nội, ngày " + (DateTime.Now.Day.ToString().Length == 1 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + " tháng " + (DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + " năm " + DateTime.Now.Year;
            newabc.ngayBC = ngayBC;
            lstTenBC.Add(newabc);

            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/DanhSachKetNapHoiVienCaNhan.rpt"));

            //rpt.SetDataSource(lst);
            rpt.Database.Tables["TenBaoCao"].SetDataSource(lstTenBC);
            rpt.Database.Tables["DanhSachKetNapHoiVienCaNhan"].SetDataSource(lst);
            rpt.Database.Tables["NguoiLapBieu"].SetDataSource(lstNguoiLapBieu);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
            {
                //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "DanhSachKetNapHoiVienCaNhan");
                ExportToExcel("DanhSachKetNapHoiVienCaNhan.xls", lst, NgayTinh, ngayBC);
            }
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachKetNapHoiVienCaNhan");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }


        }
    }

    private void ExportToExcel(string strName, List<DanhSachKetNapHoiVienCaNhan> lst, string NgayTinh, string NgayBC)
    {
        string outHTML = "";

        outHTML += "<table border = '1' cellpadding='5' cellspacing='0' style='font-family:Arial; font-size:8pt; color:#003366'>";
        outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        outHTML += "<td colspan='2' style='border-style:none'></td>";
        //outHTML += "<td colspan='13' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/logo.png' alt='' border='0px'/></td>";
        outHTML += "<td colspan='26' align='right' width='197px' height='81px' style='border-style:none; font-family:.VnHelvetInsH; font-size:14pt; color:Black'><b><u>HỘI KIỂM TOÁN VIÊN HÀNH NGHỀ VIỆT NAM</i></b></td>";
        outHTML += "<td colspan='2' style='border-style:none'></td>";

        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='30' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH KẾT NẠP HỘI VIÊN CÁ NHÂN</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='30' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i>" + NgayTinh + "</i></td>";
        outHTML += "</tr>";
        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "    <th rowspan='2' width='30px'>STT</th>";
        outHTML += "    <th rowspan='2' width='100px'>Mã số</th>";
        outHTML += "    <th rowspan='2' width='100px'>Loại hội viên chi tiết</th>";
        outHTML += "    <th rowspan='2' width='200px'>Họ và Tên</th>";
        outHTML += "    <th colspan='2' width='200px'>Ngày sinh</th>";
        outHTML += "    <th rowspan='2' width='200px'>Quê quán/Quốc tịch</th>";
        outHTML += "    <th colspan='4' width='200px'>Trình độ chuyên môn</th>";
        outHTML += "    <th colspan='2' width='200px'>CMT/Hộ chiếu</th>";
        outHTML += "    <th colspan='2' width='200px'>TK ngân hàng</th>";
        outHTML += "    <th colspan='2' width='200px'>Chứng chỉ KTV</th>";
        outHTML += "    <th colspan='3' width='300px'>Giấy CN ĐKHNKiT</th>";
        outHTML += "    <th colspan='3' width='300px'>Chứng chỉ quốc tế</th>";
        outHTML += "    <th rowspan='2' width='100px'>Chức vụ hiện nay</th>";
        outHTML += "    <th rowspan='2' width='100px'>Đơn vị công tác</th>";
        outHTML += "    <th colspan='4' width='400px'>Thông tin liên lạc</th>";
        outHTML += "    <th rowspan='2' width='100px'>Số QĐ kết nạp HVCN</th>";
        outHTML += "    <th rowspan='2' width='100px'>Ngày ký QĐ kết nạp HVCN</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th width='100px'>Nam</th>";
        outHTML += "    <th width='100px'>Nữ</th>";
        outHTML += "    <th width='50px'>Cử nhân</th>";
        outHTML += "    <th width='50px'>Thạc sĩ</th>";
        outHTML += "    <th width='50px'>TS</th>";
        outHTML += "    <th width='50px'>PGS/GS</th>";
        outHTML += "    <th width='100px'>Số</th>";
        outHTML += "    <th width='100px'>Ngày cấp</th>";
        outHTML += "    <th width='100px'>Số</th>";
        outHTML += "    <th width='100px'>Tại</th>";
        outHTML += "    <th width='100px'>Số</th>";
        outHTML += "    <th width='100px'>Ngày cấp</th>";
        outHTML += "    <th width='100px'>Số</th>";
        outHTML += "    <th width='100px'>Thời hạn cấp từ ngày</th>";
        outHTML += "    <th width='100px'>Thời hạn cấp đến ngày</th>";
        outHTML += "    <th width='100px'>Số</th>";
        outHTML += "    <th width='100px'>Ngày cấp</th>";
        outHTML += "    <th width='100px'>Nơi cấp</th>";
        outHTML += "    <th width='100px'>Nơi ở hiện nay</th>";
        outHTML += "    <th width='100px'>ĐTCĐ</th>";
        outHTML += "    <th width='100px'>Mobile</th>";
        outHTML += "    <th width='100px'>Email</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th width='30px'>1</th>";
        outHTML += "    <th width='100px'>2</th>";
        outHTML += "    <th width='100px'>3</th>";
        outHTML += "    <th width='200px'>4</th>";
        outHTML += "    <th width='100px'>5</th>";
        outHTML += "    <th width='100px'>6</th>";
        outHTML += "    <th width='200px'>7</th>";
        outHTML += "    <th width='100px'>8</th>";
        outHTML += "    <th width='100px'>9</th>";
        outHTML += "    <th width='100px'>10</th>";
        outHTML += "    <th width='100px'>11</th>";
        outHTML += "    <th width='100px'>12</th>";
        outHTML += "    <th width='100px'>13</th>";
        outHTML += "    <th width='100px'>14</th>";
        outHTML += "    <th width='100px'>15</th>";
        outHTML += "    <th width='100px'>16</th>";
        outHTML += "    <th width='100px'>17</th>";
        outHTML += "    <th width='100px'>18</th>";
        outHTML += "    <th width='100px'>19</th>";
        outHTML += "    <th width='100px'>20</th>";
        outHTML += "    <th width='100px'>21</th>";
        outHTML += "    <th width='100px'>22</th>";
        outHTML += "    <th width='100px'>23</th>";
        outHTML += "    <th width='100px'>24</th>";
        outHTML += "    <th width='100px'>25</th>";
        outHTML += "    <th width='100px'>26</th>";
        outHTML += "     <th width='100px'>27</th>";
        outHTML += "    <th width='100px'>28</th>";
        outHTML += "    <th width='100px'>29</th>";
        outHTML += "    <th width='100px'>30</th>";
        outHTML += "    <th width='100px'>31</th>";
        outHTML += "</tr>";

        var q = lst.Select(t => new { t.TenCT }).Distinct();

        int i = 0;
        foreach (var temp in q)
        {
            outHTML += "<tr>";
            outHTML += "<td colspan='31' align='left'>" + temp.TenCT.Replace("<", "_").Replace(">", "") + "</td>";
            outHTML += "</tr>";

            var qChiTiet = lst.Where(t => t.TenCT == temp.TenCT);
            foreach (var tChiTiet in qChiTiet)
            {
                i++;
                outHTML += "<tr>";
                outHTML += "<td>" + i.ToString() + "</td>";
                outHTML += "<td>" + tChiTiet.MaSo + "</td>";
                if (tChiTiet.LoaiHoiVien != null)
                    outHTML += "<td>" + tChiTiet.LoaiHoiVien + "</td>";
                outHTML += "<td align='left'>" + tChiTiet.HoTen + "</td>";
                outHTML += "<td>" + tChiTiet.NgaySinh_Nam + "</td>";
                outHTML += "<td>" + tChiTiet.NgaySinh_Nu + "</td>";
                outHTML += "<td align='left'>" + tChiTiet.QueQuan + "</td>";
                outHTML += "<td>" + tChiTiet.CuNhan + "</td>";
                outHTML += "<td>" + tChiTiet.ThacSi + "</td>";
                outHTML += "<td>" + tChiTiet.TienSi + "</td>";
                outHTML += "<td>" + tChiTiet.GS + "</td>";
                outHTML += "<td>" + tChiTiet.CMT + "</td>";
                outHTML += "<td>" + tChiTiet.NgayCap + "</td>";
                outHTML += "<td>" + tChiTiet.SoTK + "</td>";
                outHTML += "<td align='left'>" + tChiTiet.TaiNH + "</td>";
                outHTML += "<td>" + tChiTiet.So_CCKTV + "</td>";
                outHTML += "<td>" + tChiTiet.Ngay_CCKTV + "</td>";
                outHTML += "<td>" + tChiTiet.SoGiayCN + "</td>";
                outHTML += "<td>" + tChiTiet.GiayCN_Tu + "</td>";
                outHTML += "<td>" + tChiTiet.GiayCN_Den + "</td>";
                outHTML += "<td>" + tChiTiet.ChungChiQT_So + "</td>";
                outHTML += "<td>" + tChiTiet.ChungChiQT_Ngay + "</td>";
                outHTML += "<td></td>";
                outHTML += "<td align='left'>" + tChiTiet.ChucVuHienNay + "</td>";
                outHTML += "<td align='left'>" + tChiTiet.DonViCongTac + "</td>";
                outHTML += "<td>" + tChiTiet.ChoOHienNay + "</td>";
                outHTML += "<td>" + tChiTiet.DTCD + "</td>";
                outHTML += "<td>" + "'" + tChiTiet.Mobile + "</td>";
                outHTML += "<td>" + tChiTiet.Email + "</td>";
                outHTML += "<td>" + tChiTiet.SoQDKetNap + "</td>";
                outHTML += "<td>" + tChiTiet.NgayQDKetNap + "</td>";

                outHTML += "</tr>";
            }
        }

        outHTML += "<tr>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td align='left'><b> Tổng số: " + i.ToString() + "</b></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td ></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "<td></td>";
        outHTML += "</tr>";

        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='24' style='border-style:none'></td>";
        outHTML += "<td colspan='6' align='center' style='border-style:none'><b>" + NgayBC + "</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='24' style='border-style:none'></td>";
        outHTML += "<td colspan='6' align='center' style='border-style:none'><b>NGƯỜI LẬP BIỂU<b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='24' style='border-style:none'></td>";
        outHTML += "<td colspan='6' align='center' style='border-style:none'><b>" + cm.Admin_HoVaTen + "<b></td>";
        outHTML += "</tr>";

        outHTML += "</table>";

        Library.ExportToExcel(strName, outHTML);
    }

    protected void Text_Changed_2(object sender, EventArgs e)
    {
        txtCongTy.Text = "";
        try
        {
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + idDanhSach.Text + "'";// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtCongTy.Text = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
        }
        catch { }
    }

    protected void btnChay2_Click(object sender, EventArgs e)
    {
        txtCongTy.Text = "";
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            idDanhSach.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + lstIdChon[0] + "'";// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtCongTy.Text = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }
}