﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_KSCL_BaoCao_BangChamDiemKiemTraHeThong : System.Web.UI.UserControl
{
    protected string tenchucnang = "Bảng chấm điểm kiểm tra hệ thống";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = Library.CheckNull(Request.Form["ddlNam"]);
            string idDoanKiemTra = Library.CheckNull(Request.Form["hdDoanKiemTraID"]);
            string idCongTy = Library.CheckNull(Request.Form["hdCongTyID"]);
            if (!string.IsNullOrEmpty(idDoanKiemTra) && !string.IsNullOrEmpty(idCongTy))
            {
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/BangChamDiemKiemTraHeThong.rpt"));
                List<Hashtable> listDataCongTyKiemToan = GetThongTinCongTyKiemToan(idCongTy, rpt);
                List<Hashtable> listDataNoiDungBangChamDiem = new List<Hashtable>();
                string idBcktht = GetNoiDungBangChamDiem(idDoanKiemTra, idCongTy, rpt, ref listDataNoiDungBangChamDiem);
                if (!string.IsNullOrEmpty(idBcktht))
                {
                    DataTable dt = GetNhomCauHoi("1", "1", idBcktht, rpt);
                    rpt.Database.Tables["dtBangCauHoiKiemTraHeThong"].SetDataSource(dt);
                    DataTable dtThanhVienDoanKiemTra = GetDanhSachThanhVienDoanKiemTra(idDoanKiemTra);
                    rpt.Subreports[0].Database.Tables["dtDanhSachThanhVienDoanKiemTra"].SetDataSource(dtThanhVienDoanKiemTra);

                    ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "BẢNG CÂU HỎI KIỂM TRA HỆ THỐNG NĂM " + nam;
                    //((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    //((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                    CrystalReportViewer1.ReportSource = rpt;
                    CrystalReportViewer1.RefreshReport();
                    if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                        ExportExcel(dt, dtThanhVienDoanKiemTra, listDataCongTyKiemToan, listDataNoiDungBangChamDiem, nam);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BangCauHoiKiemTraHeThong");
                    if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                        rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BangCauHoiKiemTraHeThong");
                }
                else
                {
                    CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có dữ liệu báo cáo tương ứng với điều kiện đầu vào!</div>"));
                }
            }
            else
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
        {
            string js = "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
            js += "$('#txtMaDoanKiemTra').val('" + Request.Form["txtMaDoanKiemTra"] + "');" + Environment.NewLine;
            js += "$('#hdDoanKiemTraID').val('" + Request.Form["hdDoanKiemTraID"] + "');" + Environment.NewLine;
            js += "$('#txtMaCongTy').val('" + Request.Form["txtMaCongTy"] + "');" + Environment.NewLine;
            js += "$('#hdCongTyID').val('" + Request.Form["hdCongTyID"] + "');" + Environment.NewLine;
            Response.Write(js);
        }
    }

    private void SetDataRow(DataTable dt, string stt, string noiDung, string canCu, string diemChuan, string diemThucTe, string huongDanChamDiem, string traLoi, string ghiChu)
    {
        DataRow dr = dt.NewRow();
        dr["STT"] = "<center>" + stt + "</center>";
        dr["NoiDung"] = noiDung;
        dr["CanCu"] = canCu;
        dr["DiemChuan"] = "<center>" + diemChuan + "</center>";
        dr["DiemThucTe"] = "<center>" + diemThucTe + "</center>";
        dr["HuongDanChamDiem"] = huongDanChamDiem;
        dr["TraLoi"] = "<center>" + traLoi + "</center>";
        dr["GhiChu"] = ghiChu;
        dt.Rows.Add(dr);
    }

    private double _tongSoDiemToiDa = 0, _tongSoDiemThucTe = 0, _tongSoDiemKhongDuocTinh = 0;
    protected DataTable GetNhomCauHoi(string loaiCauHoi, string tinhTrangHieuLuc, string idBaoCaoKtht, ReportDocument rpt)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "NoiDung", "CanCu", "DiemChuan", "DiemThucTe", "HuongDanChamDiem", "TraLoi", "GhiChu" });

        DataTable data = GetDanhSachCauHoi(loaiCauHoi, tinhTrangHieuLuc); // Get danh sách câu hỏi đổ tất cả vào DataTable
        // Lấy dữ liệu chi tiết báo cáo kiểm tra nếu có id Hồ sơ kiểm tra kiểm soát chất lượng
        DataTable dataBaoCaoKT = new DataTable();
        if (!string.IsNullOrEmpty(idBaoCaoKtht))
        {
            dataBaoCaoKT = GetDanhSachBaoCaoKiemTraChiTiet(idBaoCaoKtht); // Get dữ liệu bảng điểm 
        }

        // Get Danh sách nhóm câu hỏi
        string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE Loai = " + loaiCauHoi + " AND (NhomCauHoiChaID IS NULL OR NhomCauHoiChaID = '') ORDER BY NhomCauHoiID";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            int index = 1;
            int currentIndexInDataTable = 0;
            foreach (Hashtable ht in listData)
            {
                DataRow[] arrDataRow = data.Select("NCHID = " + ht["NhomCauHoiID"]); // Lấy danh sách câu hỏi theo ID nhóm câu hỏi
                double sumDiemToiDa = arrDataRow.Sum(dataRow => Library.DoubleConvert(dataRow["DiemToiDa"]));
                double sumDiemThucTe = 0;
                currentIndexInDataTable = dt.Rows.Count;
                SetDataRow(dt, "<b>" + index.ToString() + "</b>", "<b>" + ht["TenNhomCauHoi"] + "</b>", "", (sumDiemToiDa > 0 ? "<b>" + Library.ChangeFormatNumber(sumDiemToiDa, "us") + "</b>" : ""), "", "", "", "");

                query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + ht["NhomCauHoiID"] + " ORDER BY NhomCauHoiID";
                List<Hashtable> listDataChild = _db.GetListData(query);
                string tienToXacDinhLaCauHoi = ""; // Tiền tố xác định là câu hỏi trong chuỗi STT (Thêm .0.)
                if (listDataChild.Count > 0)
                    tienToXacDinhLaCauHoi = "0.";
                int cauHoiIndex = 1;
                foreach (DataRow dataRow in arrDataRow)
                {
                    string traLoi = "", ghiChu = "", diemThucTe = "";
                    if (dataBaoCaoKT != null && dataBaoCaoKT.Rows.Count > 0)
                    {
                        string queryDataTable = "";
                        if (loaiCauHoi == "1")
                            queryDataTable = "CauHoiKTHTID = " + dataRow["CauHoiKTKTID"];
                        DataRow[] arrDataRowBaoCaoKT = dataBaoCaoKT.Select(queryDataTable); // Lấy kết quả báo cáo theo câu hỏi (Nếu có)
                        if (arrDataRowBaoCaoKT.Length > 0)
                        {
                            traLoi = GetStrTraLoi(arrDataRowBaoCaoKT[0]["TraLoi"].ToString());
                            diemThucTe = (Library.DoubleConvert(arrDataRowBaoCaoKT[0]["DiemThucTe"]) > 0
                                              ? Library.ChangeFormatNumber(
                                                  Library.DoubleConvert(arrDataRowBaoCaoKT[0]["DiemThucTe"]), "us")
                                              : "");
                            ghiChu = arrDataRowBaoCaoKT[0]["GhiChu"].ToString();
                            sumDiemThucTe += Library.DoubleConvert(diemThucTe);
                            if (arrDataRowBaoCaoKT[0]["TraLoi"].ToString() == "2")
                                _tongSoDiemKhongDuocTinh += Library.DoubleConvert(dataRow["DiemToiDa"]);
                        }
                    }
                    SetDataRow(dt, index + "." + tienToXacDinhLaCauHoi + cauHoiIndex, dataRow["CauHoi"].ToString(), dataRow["CanCu"].ToString(), (Library.DoubleConvert(dataRow["DiemToiDa"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(dataRow["DiemToiDa"]), "us") : ""), diemThucTe, dataRow["HuongDanChamDiem"].ToString(), traLoi, ghiChu);
                    cauHoiIndex++;
                }
                GetNhomCauHoiSub(dt, listDataChild, data, dataBaoCaoKT, index, loaiCauHoi, ref sumDiemToiDa, ref sumDiemThucTe); // Gọi hàm đệ quy
                // Set lại điểm tối đa ở đây
                dt.Rows[currentIndexInDataTable]["DiemChuan"] = "<center><b>" + sumDiemToiDa + "</center></b>";
                dt.Rows[currentIndexInDataTable]["DiemThucTe"] = "<center><b>" + sumDiemThucTe + "</center></b>";
                _tongSoDiemToiDa += sumDiemToiDa;
                _tongSoDiemThucTe += sumDiemThucTe;
                index++;
            }
        }

        ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongDiemToiDa"]).Text = _tongSoDiemToiDa.ToString();
        ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongDiemThucTe"]).Text = _tongSoDiemThucTe.ToString();
        // Tính điểm quy đổi
        double temp = _tongSoDiemThucTe * _tongSoDiemToiDa;
        double soDiemQuyDoi = temp / (_tongSoDiemToiDa - _tongSoDiemKhongDuocTinh);
        ((TextObject)rpt.ReportDefinition.ReportObjects["txtTongDiemQuyDoi"]).Text = Library.FormatMoney(soDiemQuyDoi);
        return dt;
    }

    protected void GetNhomCauHoiSub(DataTable dt, List<Hashtable> listData, DataTable data, DataTable dataBaoCaoKT, int index, string loaiCauHoi, ref double sumDiemToiDa_NhomCha, ref double sumDiemThucTe_NhomCha)
    {
        if (listData.Count > 0)
        {
            int subIndex = 1;
            int currentIndexInDataTable = 0;
            foreach (Hashtable ht in listData)
            {
                DataRow[] arrDataRow = data.Select("NCHID = " + ht["NhomCauHoiID"]);
                double sumDiemToiDa = arrDataRow.Sum(dataRow => Library.DoubleConvert(dataRow["DiemToiDa"]));
                double sumDiemThucTe = 0;
                currentIndexInDataTable = dt.Rows.Count;
                SetDataRow(dt, "<b>" + index + "." + subIndex + "</b>", "<b>" + ht["TenNhomCauHoi"] + "</b>", "", (sumDiemToiDa > 0 ? "<b>" + Library.ChangeFormatNumber(sumDiemToiDa, "us") + "</b>" : ""), "", "", "", "");
                string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + ht["NhomCauHoiID"] + " ORDER BY NhomCauHoiID";
                List<Hashtable> listDataChild = _db.GetListData(query);
                string tienToXacDinhLaCauHoi = ""; // Tiền tố xác định là câu hỏi trong chuỗi STT (Thêm .0.)
                if (listDataChild.Count > 0)
                    tienToXacDinhLaCauHoi = "0.";
                int cauHoiIndex = 1;
                foreach (DataRow dataRow in arrDataRow)
                {
                    string traLoi = "", ghiChu = "", diemThucTe = "";
                    if (dataBaoCaoKT != null && dataBaoCaoKT.Rows.Count > 0)
                    {
                        string queryDataTable = "";
                        if (loaiCauHoi == "1")
                            queryDataTable = "CauHoiKTHTID = " + dataRow["CauHoiKTKTID"];
                        DataRow[] arrDataRowBaoCaoKT = dataBaoCaoKT.Select(queryDataTable); // Lấy kết quả báo cáo theo câu hỏi (Nếu có)
                        if (arrDataRowBaoCaoKT.Length > 0)
                        {
                            traLoi = GetStrTraLoi(arrDataRowBaoCaoKT[0]["TraLoi"].ToString());
                            diemThucTe = (Library.DoubleConvert(arrDataRowBaoCaoKT[0]["DiemThucTe"]) > 0
                                              ? Library.ChangeFormatNumber(
                                                  Library.DoubleConvert(arrDataRowBaoCaoKT[0]["DiemThucTe"]), "us")
                                              : "");
                            ghiChu = arrDataRowBaoCaoKT[0]["GhiChu"].ToString();
                            sumDiemThucTe += Library.DoubleConvert(diemThucTe);
                            if (arrDataRowBaoCaoKT[0]["TraLoi"].ToString() == "2")
                                _tongSoDiemKhongDuocTinh += Library.DoubleConvert(dataRow["DiemToiDa"]);
                        }
                    }
                    SetDataRow(dt, index + "." + subIndex + "." + tienToXacDinhLaCauHoi + cauHoiIndex, dataRow["CauHoi"].ToString(), dataRow["CanCu"].ToString(), (Library.DoubleConvert(dataRow["DiemToiDa"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(dataRow["DiemToiDa"]), "us") : ""), diemThucTe, dataRow["HuongDanChamDiem"].ToString(), traLoi, ghiChu);
                    cauHoiIndex++;
                }
                GetNhomCauHoiSub(dt, listDataChild, data, dataBaoCaoKT, subIndex, loaiCauHoi, ref sumDiemToiDa_NhomCha, ref sumDiemThucTe_NhomCha);
                subIndex++;
                sumDiemToiDa_NhomCha += sumDiemToiDa;
                sumDiemThucTe_NhomCha += sumDiemThucTe;
                // Set lại điểm tối đa ở đây
                dt.Rows[currentIndexInDataTable]["DiemChuan"] = "<center><b>" + sumDiemToiDa + "</center></b>";
                dt.Rows[currentIndexInDataTable]["DiemThucTe"] = "<center><b>" + sumDiemThucTe + "</center></b>";
            }
        }
    }

    private DataTable GetDanhSachCauHoi(string loaiCauHoi, string tinhTrangHieuLuc)
    {
        DataTable dt = new DataTable();
        string query = @"SELECT CauHoiKTKTID, MaCauHoi, NgayLap, a.NhomCauHoiID NCHID, CauHoi, DiemToiDa, CanCu, HuongDanChamDiem, TinhTrangHieuLuc FROM tblKSCLCauHoiKT a
                            INNER JOIN tblDMNhomCauHoiKSCL b ON a.NhomCauHoiID = b.NhomCauHoiID
                            WHERE b.Loai = " + loaiCauHoi + " AND a.TinhTrangHieuLuc = '" + tinhTrangHieuLuc + "'";
        dt = _db.GetDataTable(query);
        return dt;
    }

    private DataTable GetDanhSachBaoCaoKiemTraChiTiet(string idBaoCaoKT)
    {
        string query = "SELECT BaoCaoKTHTChiTietID, CauHoiKTHTID, TraLoi, DiemThucTe, GhiChu FROM " + ListName.Table_KSCLBaoCaoKTHTChiTiet + " WHERE BaoCaoKTHTID = " + idBaoCaoKT;
        DataTable dt = _db.GetDataTable(query);
        return dt;
    }

    private DataTable GetDanhSachThanhVienDoanKiemTra(string idDoanKiemTra)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "HoTen", "ChucVu", "DonViCongTac", "LoaiThanhVien", "SoCCKTV" });
        if (string.IsNullOrEmpty(idDoanKiemTra))
            return dt;
        string query = @"SELECT Fullname, SoCCKTV FROM (SELECT TVDKT.TenThanhVien Fullname, TVDKT.SoChungChiKTV SoCCKTV, DKTTV.VaiTro  FROM tblKSCLDoanKiemTraThanhVien DKTTV	
	                        INNER JOIN tblDMThanhVienDoanKT TVDKT ON DKTTV.ThanhVienDoanKiemTraID = TVDKT.ThanhVienDoanKiemTraID	                            
	                        WHERE DKTTV.DoanKiemTraID = " + idDoanKiemTra + @" AND DKTTV.ThanhVienDoanKiemTraID IS NOT NULL			
	                        UNION ALL
	                        SELECT (HVCN.HoDem + ' ' + HVCN.Ten) Fullname, HVCN.SoChungChiKTV SoCCKTV, DKTTV.VaiTro  FROM tblKSCLDoanKiemTraThanhVien DKTTV	
	                        INNER JOIN tblHoiVienCaNhan HVCN ON DKTTV.HoiVienCaNhanID = HVCN.HoiVienCaNhanID	
	                        WHERE DKTTV.DoanKiemTraID = " + idDoanKiemTra + @" AND DKTTV.HoiVienCaNhanID IS NOT NULL) AS tblResult
	                        ORDER BY tblResult.VaiTro";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            DataRow dr;
            int index = 1;
            foreach (Hashtable ht in listData)
            {
                dr = dt.NewRow();
                dr["HoTen"] = index + ". Ông (Bà): " + ht["Fullname"];
                dr["SoCCKTV"] = " Số CC KTV: " + ht["SoCCKTV"];
                dt.Rows.Add(dr);
                index++;
            }
        }
        return dt;
    }

    private List<Hashtable> GetThongTinCongTyKiemToan(string idCongTy, ReportDocument rpt)
    {
        string query = @"SELECT SoHieu, TenDoanhNghiep, NguoiDaiDienPL_Ten, CV.TenChucVu FROM tblHoiVienTapThe HVTC
                        LEFT JOIN tblDMChucVu CV ON HVTC.NguoiDaiDienPL_ChucVuID = CV.ChucVuID WHERE HVTC.HoiVienTapTheID = " + idCongTy;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtSoHieuCongTy"]).Text = "Số hiệu Công ty: " + ht["SoHieu"];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtTenCongTy"]).Text = "Tên Công ty: " + ht["TenDoanhNghiep"].ToString().Replace("\n", "");
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiDaiDienPL"]).Text = "Ông (Bà): " + ht["NguoiDaiDienPL_Ten"].ToString().Replace("\n", "") + "   Chức vụ: " + ht["TenChucVu"];
        }
        return listData;
    }

    private string GetNoiDungBangChamDiem(string idDoanKiemTra, string idCongTy, ReportDocument rpt, ref List<Hashtable> listData)
    {
        string idBcktht = "";
        string query = @"SELECT BCKTHT.BaoCaoKTHTID, BCKTHT.SLThanhVienBGD, BCKTHT.SLKTVHanhNghe, BCKTHT.SoLuongNVChuyenNghiep, BCKTHT.SLBCKTPhatHanh, BCKTHT.HinhThucLuuTaiLieu, BCKTHT.PhanMemKT
                        FROM tblKSCLBaoCaoKTHT BCKTHT
                        INNER JOIN tblKSCLHoSo HS ON BCKTHT.HoSoID = HS.HoSoID
                        WHERE HS.DoanKiemTraID = " + idDoanKiemTra + @" AND HS.HoiVienTapTheID = " + idCongTy;
        listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            idBcktht = ht["BaoCaoKTHTID"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtSLThanhVienBGD"]).Text = "Số lượng thành viên BGĐ: " + ht["SLThanhVienBGD"];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtSLKTVHanhNghe"]).Text = "Số lượng KTV hành nghề: " + ht["SLKTVHanhNghe"];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtSLNVCN"]).Text = "Số lượng nhân viên chuyên nghiệp/Tổng số nhân viên: " + ht["SoLuongNVChuyenNghiep"];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtSLBCKTPhatHanh"]).Text = "Số lượng BCKT đã phát hành trung bình một năm: " + ht["SLBCKTPhatHanh"];

            string hinhThucLuuTaiLieu = ht["HinhThucLuuTaiLieu"].ToString();
            if (hinhThucLuuTaiLieu == "1" || hinhThucLuuTaiLieu == "3")
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtLuuFileCung"]).Text = "x";
            if (hinhThucLuuTaiLieu == "2" || hinhThucLuuTaiLieu == "3")
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtLuuFileMem"]).Text = "x";
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtPhamMemKiemToan"]).Text = ht["PhanMemKT"].ToString();
        }
        return idBcktht;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private string GetStrTraLoi(string traLoi)
    {
        switch (traLoi)
        {
            case "1":
                return "Có";
            case "2":
                return "N/A";
            case "3":
                return "Không";
        }
        return "";
    }

    private void ExportExcel(DataTable dt, DataTable dtThanhVienDoanKiemTra, List<Hashtable> listDataCongTyKiemToan, List<Hashtable> listDataNoiDungBangChamDiem, string nam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:120px;
            vertical-align: middle;
        }

        .HeaderColumn1 
        {
            font-size: 11pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTitle {                    
            vertical-align: middle;            
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
            height: 40px;
            vertical-align: middle; 
        }

        .GroupTitle2 {
            font-size: 10pt;
            font-weight: bold;
            height: 30px;
            vertical-align: middle; 
        }
        
        .Column {
            font-size: 12pt;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnSum {
            font-size: 12pt;
            font-weight: bold;
            vertical-align: middle;
        }

        br { mso-data-placement:same-cell; }
    </style></head>";
        string hinhThucLuuTaiLieu = listDataNoiDungBangChamDiem[0]["HinhThucLuuTaiLieu"].ToString();
        string luuFileCung = "<input type='checkbox' />", luuFileMem = "<input type='checkbox' />";
        if (hinhThucLuuTaiLieu == "1" || hinhThucLuuTaiLieu == "3")
            luuFileCung = "<input type='checkbox' checked='checked' />";
        if (hinhThucLuuTaiLieu == "2" || hinhThucLuuTaiLieu == "3")
            luuFileMem = "<input type='checkbox' checked='checked' />";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='8' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='6' style='text-align:center;font-size: 14pt; font-weight: bold;'>BẢNG CÂU HỎI KIỂM TRA HỆ THỐNG NĂM " + nam + @"</td>
            </tr>
            <tr><td></td></tr>
            <tr><td colspan='8' style='font-size: 12pt; font-weight: bold;'>Họ tên thành viên Đoàn kiểm tra:</td></tr>";
        for (int i = 0; i < dtThanhVienDoanKiemTra.Rows.Count; i++)
        {
            htmlExport += "<tr><td></td><td colspan='7' style='font-size: 12pt;'>" + (i + 1) + ". Ông (Bà): " + dtThanhVienDoanKiemTra.Rows[i]["HoTen"].ToString().Replace((i + 1) + ". Ông (Bà): ", "") + " Số CC KTV: " + dtThanhVienDoanKiemTra.Rows[i]["SoCCKTV"].ToString().Replace(" Số CC KTV: ", "") + "</td></tr>";
        }
        htmlExport += @"<tr><td colspan='8'><span style='font-size: 12pt; font-weight: bold;'>Công ty kiểm toán được kiểm tra:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style='font-size: 12pt;'>Số hiệu công ty: " + listDataCongTyKiemToan[0]["SoHieu"] + @"</span></td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>Tên Công ty: " + listDataCongTyKiemToan[0]["TenDoanhNghiep"].ToString().Replace("\n", "") + @"</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>Họ và tên người đại diện Công ty:</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>Ông (Bà): " + listDataCongTyKiemToan[0]["NguoiDaiDienPL_Ten"].ToString().Replace("\n", "") + @" Chức vụ: " + listDataCongTyKiemToan[0]["TenChucVu"] + @"</td></tr>
            <tr><td colspan='8' style='font-size: 12pt; font-weight: bold;'>Thông tin chung về Công ty được kiểm tra:</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>Số lượng thành viên BGĐ: " + listDataNoiDungBangChamDiem[0]["SLThanhVienBGD"] + @"</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>Số lượng KTV hành nghề: " + listDataNoiDungBangChamDiem[0]["SLKTVHanhNghe"] + @"</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>Số lượng nhân viên chuyên nghiệp/Tổng số nhân viên: " + listDataNoiDungBangChamDiem[0]["SoLuongNVChuyenNghiep"] + @"</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>Số lượng BCKT đã phát hành trung bình một năm: " + listDataNoiDungBangChamDiem[0]["SLBCKTPhatHanh"] + @"</td></tr>
            <tr><td colspan='8' style='font-size: 12pt; font-weight: bold;'>Hình thức lưu tài liệu, hồ sơ về hợp đồng dịch vụ:</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>" + luuFileCung + @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trên file cứng:</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>" + luuFileMem + @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trên file mềm: " + listDataNoiDungBangChamDiem[0]["PhanMemKT"] + @"</td></tr>
            <tr><td></td></tr>
            <tr><td colspan='8' style='font-size: 12pt; font-weight: bold;'>Kết quả kiểm tra:</td></tr>
            <tr><td></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn1'>STT</td>
                                <td class='HeaderColumn1' style='width: 200px;'>Câu hỏi</td>
                                <td class='HeaderColumn1' style='width: 70px;'>Căn cứ</td>
                                <td class='HeaderColumn1' style='width: 100px;'>Trả lời</td>
                                <td class='HeaderColumn1' style='width: 70px;'>Điểm chuẩn</td>
                                <td class='HeaderColumn1' style='width: 70px;'>Điểm thực tế</td>
                                <td class='HeaderColumn1' style='width: 120px;'>Ghi chú của người kiểm tra</td>
                                <td class='HeaderColumn1' style='width: 200px;'>Hướng dẫn chấm điểm</td>";

        htmlExport += "</tr>";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            htmlExport += "<tr><td class='ColumnStt'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["NoiDung"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["CanCu"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TraLoi"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DiemChuan"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DiemThucTe"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["GhiChu"] + "</td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["HuongDanChamDiem"] + "</td>";
            htmlExport += "</tr>";
        }
        // Tính điểm quy đổi
        double temp = _tongSoDiemThucTe * _tongSoDiemToiDa;
        double soDiemQuyDoi = temp / (_tongSoDiemToiDa - _tongSoDiemKhongDuocTinh);
        htmlExport += "<tr><td></td><td class='ColumnSum'>Cộng điểm</td><td></td><td></td><td class='ColumnSum' style='text-align: right;'>" + _tongSoDiemToiDa.ToString() + "</td><td class='ColumnSum' style='text-align: right;'>" + _tongSoDiemThucTe + "</td><td></td><td></td></tr>";
        htmlExport += "<tr><td></td><td class='ColumnSum'>Điểm quy đổi</td><td></td><td></td><td class='ColumnSum' style='text-align: right;'></td><td class='ColumnSum' style='text-align: right;'>" + Library.FormatMoney(soDiemQuyDoi) + "</td><td></td><td></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: center;'>1.</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Ý kiến nhận xét tổng quát của TV đoàn kiểm tra</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: center;'>2.</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Ý kiến giải trình của Công ty</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: center;'>3.</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Xếp loại</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: left;'>(1)</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Từ 80 điểm trở lên</td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: left;'>(2)</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Từ 60 điểm đến dưới 80 điểm</td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: left;'>(3)</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Từ 40 điểm đến dưới 60 điểm</td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: left;'>(4)</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Dưới 40 điểm</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td colspan='8' style='font-size: 12pt; font-weight: bold;'>TV Đoàn kiểm tra 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TV Đoàn kiểm tra 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Người đại diện công ty</td></tr>";
        htmlExport += "<tr><td colspan='8' style='font-size: 12pt;'><i>&nbsp;&nbsp;(Chữ ký, họ tên)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Chữ ký, họ tên)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Chữ ký, họ tên)</i></td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("BangCauHoiKiemTraHeThong.xsl", "<html>" + css + htmlExport + "</html>");
    }
}