﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QLHV_BC_DanhSachKyLuatHoiVienCaNhan.ascx.cs" Inherits="usercontrols_QLHV_QLHV_BC_DanhSachKyLuatHoiVienCaNhan" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<%--<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>--%>
<script type="text/javascript" src="js/ui.dropdownchecklist-1.5-min.js"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<%--<script type="text/javascript" src="js/jquery-ui.min.js"></script>--%>

<form id="Form1" runat="server" clientidmode="Static">
<asp:HiddenField ID="hi_LoaiHinh" runat="server" />
<asp:HiddenField ID="hi_TinhThanh" runat="server" />
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<h4 class="widgettitle">
    Danh sách kỷ luật hội viên cá nhân</h4>
<fieldset class="fsBlockInfor" style="margin-top: 0px;">
    <legend>Danh sách kỷ luật hội viên cá nhân</legend>
    <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
    
    <table width="100%" border="0" class="formtbl">
    <tr>
        <td width="20%"></td>
        <td width="13%">Đơn vị công tác:</td>
        <td width="13%"><asp:TextBox name="idDanhSach" 
                id="idDanhSach" runat ="server"
                AutoPostBack = "true" OnTextChanged = "Text_Changed_2"
                Width="152px" /> </td>
        <td width="8%"><a id="a1" onclick="open_ChonID_2()" data-placement="top" data-rel="tooltip" href="#none" data-original-title="..."  rel="tooltip" class="btnabc">
            <img src="images/icons/search.png" alt="" /></a></td>
        <td width="18%">Tên công ty kiểm toán:</td>
        <td width="13%"><asp:TextBox runat="server" ID="txtCongTy" Enabled="false"></asp:TextBox></td>
        <td width="20%"></td>
                
    </tr>
    <tr>
        <td width="15%"></td>
        <td width="18%">Thời gian kỷ luật:</td>
        <td width="13%"><input type="text" name="NgayBatDau" id="NgayBatDau" value="<%=Request.Form["NgayBatDau"] %>"  
                style="width: 149px" />
        
        </td>
        <td width="5%">-</td>
        <td width="13%"><input type="text" name="NgayKetThuc" id="NgayKetThuc" value="<%=Request.Form["NgayKetThuc"] %>"  /></td>
        <td colspan="2"></td>
        
    </tr>
    <%--<tr>
        <td width="15%"></td>
        <td width="18%">Ngày quyết định <span style="color:Red">(*)</span></td>
        <td width="16%"><input type="text" name="NgayQuyetDinh" id="NgayQuyetDinh" value="" 
                style="width: 149px" /></td>
        <td colspan="4"></td>
    </tr>--%>
    
    <%--<tr>
        <td width="20%"></td>
        <td width="13%">Định dạng file: </td>
        <td colspan="4"> <select id="idFile" name="idFile" style="width: 152px">
            <%DinhDangFile(); %>
        </select></td>
        <td width="20%"></td>
    </tr>--%>
    </table>
    

    <div class="dataTables_length" style="text-align: center;">
        <a id="btnSave" href="javascript:;" class="btn" onclick="View();"><i class="iconfa-plus-sign">
             </i>Xem thông tin</a> 
        
        <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="View('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a>
                 <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="View('pdf');">
                    <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                    <input type="hidden" id="hdAction" name="hdAction"/>
    </div>
</fieldset>

 <fieldset class="fsBlockInfor">
        <legend>Danh sách kỷ luật hội viên cá nhân</legend>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False" HasExportButton="False" 
            HasPrintButton="False"/>
    </fieldset>

    <div id="div_dmNhanSuHoiVien_add"></div>

   
    <div style = "display: none">
    <asp:LinkButton ID="btnChay2" runat="server" CssClass="btn" 
        onclick="btnChay2_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>
</form>

<script type="text/javascript">



//        $(document).ready(function () {
//            $("#NgayBatDau").datepicker();
//            $("#NgayKetThuc").datepicker();

//            $(".NgayBatDau").datepicker({
//                dateFormat: 'mm-dd-yy'
//            }).datepicker('setDate', new Date())

        $(function () {  $('#NgayBatDau, #NgayKetThuc').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
        });

$(function () {
        if (document.getElementById("NgayBatDau").value == null)
            $('#NgayBatDau, #NgayKetThuc').datepicker('setDate', new Date())
          });
          
      //  });

 </script>

 <script type="text/javascript">
     

         var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function() {
        $('#NgayBatDau, #NgayKetThuc').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');

        if (document.getElementById("NgayBatDau").value == null)
        {
            $('#NgayBatDau').datepicker('setDate', new Date())
            $('#NgayKetThuc').datepicker('setDate', new Date())
        }
  //$('#NgayQuyetDinh').datepicker('setDate', new Date())
});

        </script>

        <script type="text/javascript">
            function open_ChonID_2() {
                var dvId = "null";
                var vmId = "null";

                var tId = 0;


                var timestamp = Number(new Date());
                $("#div_dmNhanSuHoiVien_add").empty();
                $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoivien' name='iframe_hoivien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dbChonHoiVienCaNhan_BC&type=tapthe&VMID=" + vmId + "&TID=" + tId + "&DVID=" + dvId + "&mode=iframe&time=" + timestamp));
                $("#div_dmNhanSuHoiVien_add").dialog({
                    autoOpen: false,
                    title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách hội viên</b>",
                    modal: true,
                    width: "640",
                    height: "480",
                    buttons: [
                             {
                                 text: "Chọn",
                                 click: function () {
                                     formChon_2();
                                 }
                             },
                                {
                                    text: "Đóng",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                        ]
                }).dialog("open");

            }
</script>


<script type="text/javascript">
    function formChon_2() {
        $("#div_dmNhanSuHoiVien_add").dialog('close');
        document.getElementById('<%= btnChay2.ClientID %>').click();
    }
</script>

<%--gọi function in bao cao--%>
<script type="text/javascript">
    function View(type) {
        //        <%setdataCrystalReport(); %>
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
</script>


