﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_BaoCaoDanhSachKiemToanVienTrongCaNuoc : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách Kiểm toán viên trong cả nước";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            if (this.IsPostBack)
                setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void LoadVungMien(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT VungMienID, TenVungMien FROM tblDMVungMien";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["VungMienID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["VungMienID"] + @""">" +
                               dtr["TenVungMien"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["VungMienID"] + @""">" + dtr["TenVungMien"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadTinhThanh(string VungMienId = "", string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where TinhID in (" + Id + ")";
        sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where 0 = 0 AND MATINH IS NOT NULL";
        if (VungMienId != "")
            sql.CommandText += " AND VungMienID In (" + VungMienId + ")";
        //if (Id != "")
        //    sql.CommandText += " AND MaTinh In (" + Id + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["MaTinh"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["MaTinh"] + @""">" +
                               dtr["TenTinh"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["MaTinh"] + @""">" + dtr["TenTinh"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);

    }

    public void LoadTenCongTy(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheId, TenDoanhNghiep FROM tblHoiVienTapThe";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["HoiVienTapTheId"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["HoiVienTapTheId"] + @""">" +
                               dtr["TenDoanhNghiep"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["HoiVienTapTheId"] + @""">" + dtr["TenDoanhNghiep"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadChungChiQuocTe(string Id = "")
    {
        string output_html = "";
        //-1: tất. 0: không có. 1: Có
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        if (Id.Contains("1"))
            output_html += @"<option selected=""selected"" value=1>Có chứng chỉ Quốc tế</option>";
        else
            output_html += @"<option value=1>Có chứng chỉ Quốc tế</option>";
        if (Id.Contains("0"))
            output_html += @"<option selected=""selected"" value=0>Không có chứng chỉ Quốc tế</option>";
        else
            output_html += @"<option value=0>Không có chứng chỉ Quốc tế</option>";
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadChucDanh(string Id ="")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT ChucVuID, TenChucVu FROM tblDMChucVu";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["ChucVuID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["ChucVuID"] + @""">" +
                               dtr["TenChucVu"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["ChucVuID"] + @""">" + dtr["TenChucVu"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void setdataCrystalReport()
    {

        List<objDanhSachKiemToanVienTrongCaNuoc> lst = new List<objDanhSachKiemToanVienTrongCaNuoc>();
        string strTenCongTy = Request.Form["TenCongTy"];
        string strChungChiQuocTe = Request.Form["ChungChiQuocTe"];
        string strChucDanh = Request.Form["ChucDanh"];
        string strVungMien = Request.Form["VungMien"];
        string strTinhThanh = Request.Form["TinhThanh"];

        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = @"select *, CASE WHEN SoHieu IS NULL THEN 'zzzzzzzzzz' when SoHieu IS NOT NULL THEN SoHieu END as ABC from (select A.*, TenChucVu, TenTinh, TenQuocTich, CASE ISNULL(E.HoiVienTapTheChaID,0) WHEN 0 THEN E.SoHieu ELSE G.SoHieu END AS SoHieu, E.TenDoanhNghiep
, C.VungMienID, (datepart(yyyy,GETDATE())  - YEAR(NgaySinh)) as DoTuoi, (select Count(ChungChiQuocTeID) from tblHoiVienCaNhanChungChiQuocTe where HoiVienCaNhanID = A.HoiVienCaNhanID 
group by HoiVienCaNhanID) as SoChungChiQuocTe from tblHoiVienCaNhan A 
left join tblDMChucVu B on A.ChucVuID = B.ChucVuID 
left join tblDMTinh C on A.QueQuan_TinhID = C.TinhID
left join tblDMQuocTich D on A.QuocTichID = D.QuocTichID
left join tblHoiVienTapThe E on A.HoiVienTapTheID = E.HoiVienTapTheID
left join tblHoiVienTapThe G on E.HoiVienTapTheChaID = G.HoiVienTapTheID
where LoaiHoiVienCaNhan <> 0) as Temp
where (0 = 0
";

        //Lọc theo Tên công ty được chọn khác tất cả
        if (!string.IsNullOrEmpty(strTenCongTy) && !strTenCongTy.Contains("-1"))
            sql.CommandText += " AND (HoiVienTapTheID in (" + strTenCongTy + "))";
        //Lọc theo Chứng chỉ được chọn khác tất cả
        if (!string.IsNullOrEmpty(strChungChiQuocTe) && !strChungChiQuocTe.Contains("-1"))
        {
            if (strChungChiQuocTe == "1")
                sql.CommandText += " AND (SoChungChiQuocTe > 0)";
            else if (strChungChiQuocTe == "0")
                sql.CommandText += " AND (SoChungChiQuocTe IS NULL OR SoChungChiQuocTe = 0)";
        }
        //Lọc theo Tỉnh thành được chọn khác tất cả
        if (!string.IsNullOrEmpty(strTinhThanh) && !strTinhThanh.Contains("-1"))
            sql.CommandText += " AND DiaChi_TinhID in (" + strTinhThanh + ")";
        else if (strTinhThanh == "-1")
        {
            //Lọc theo Vùng miền được chọn với Tỉnh bằng tất cả và vùng miền khác tất cả
            if (!string.IsNullOrEmpty(strVungMien) && !strVungMien.Contains("-1"))
                sql.CommandText += " AND VungMienID in (" + strVungMien + ")";
        }

        //Lọc theo Chức danh được chọn khác tất cả
        if (!string.IsNullOrEmpty(strChucDanh) && !strChucDanh.Contains("-1"))
            sql.CommandText += " AND (ChucVuID in (" + strChucDanh + "))";

        //Lọc theo Ngày sinh
        if (Request.Form["ngaysinhtu"] != null)
        {
            string Tu = Request.Form["ngaysinhtu"].ToString();
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (NgaySinh >= '" +
                                   (DateTime.ParseExact(Request.Form["ngaysinhtu"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }
        if (Request.Form["ngaysinhden"] != null)
        {
            string Den = Request.Form["ngaysinhden"].ToString();
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (NgaySinh <= '" +
                                   (DateTime.ParseExact(Request.Form["ngaysinhden"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }

        //Lọc theo độ tuổi
        if (Request.Form["dotuoitu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["dotuoitu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (DoTuoi >= " + Tu + ") ";
        }
        if (Request.Form["dotuoiden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["dotuoiden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (DoTuoi <= " + Den + ") ";
        }

        //Nếu không check đang hành nghề => lọc bỏ hành nghề
        if (Request.Form["danghanhnghe"] != null)
        {
            sql.CommandText += " AND TinhTrangHoiVienID = 1 ";
        }
        else
            sql.CommandText += " AND TinhTrangHoiVienID = 0 ";
        //Nếu không check hội viên => bỏ lọc hội viên
        if (Request.Form["hoivien"] != null)
        {
            sql.CommandText += " AND (LoaiHoiVienCaNhan = 1) ";
        }
        else
            sql.CommandText += " AND (LoaiHoiVienCaNhan = 2) ";
        sql.CommandText += ") order by ABC, HoiVienCaNhanID";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        int i = 0;
        foreach (DataRow tem in dt.Rows)
        {
            i++;
            objDanhSachKiemToanVienTrongCaNuoc obj = new objDanhSachKiemToanVienTrongCaNuoc();
            obj.NgayThangNam = "(Tính đến ngày " + Request.Form["ngaybaocao"].ToString() + ")";
            obj.SoHieuCongTy = tem["SoHieu"].ToString();
            obj.TenCongTy = tem["TenDoanhNghiep"].ToString();
            obj.STT = i.ToString();
            obj.HoVaTen = tem["HoDem"].ToString() + " " + tem["Ten"].ToString();

            string NgaySinhNam = "";
            string NgaySinhNu = "";
            try
            {
                if (tem["GioiTinh"].ToString() == "0")
                    if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                        NgaySinhNu = Convert.ToDateTime(tem["NgaySinh"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }
            try
            {
                if (tem["GioiTinh"].ToString() == "1")
                    if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                        NgaySinhNam = Convert.ToDateTime(tem["NgaySinh"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            obj.NgaySinhNam = NgaySinhNam;
            obj.NgaySinhNu = NgaySinhNu;

            string HoiVienVACPA = "";
            if (!string.IsNullOrEmpty(tem["LoaiHoiVienCaNhan"].ToString()) && tem["LoaiHoiVienCaNhan"].ToString() == ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString())
                HoiVienVACPA = "x";
            obj.HoiVienVACPA = HoiVienVACPA;
            string QueQuanQuocTich = "";
            if (!string.IsNullOrEmpty(tem["TenTinh"].ToString()))
                QueQuanQuocTich = tem["TenTinh"].ToString();
            if (!string.IsNullOrEmpty(tem["TenQuocTich"].ToString()))
                if (!string.IsNullOrEmpty(tem["TenTinh"].ToString()))
                    QueQuanQuocTich += "/" + tem["TenQuocTich"].ToString();
                else
                    QueQuanQuocTich = tem["TenQuocTich"].ToString();
            obj.QueQuanQuocTich = QueQuanQuocTich;
            obj.ChucVu = tem["TenChucVu"].ToString();
            obj.SoChungChiKTV = tem["SoChungChiKTV"].ToString();
            try
            {
                if (!string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()))
                    obj.NgayCapChungChiKTV = Convert.ToDateTime(tem["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            obj.SoGiayChungNhanDKHNKiT = tem["SoGiayChungNhanDKHN"].ToString();

            try
            {
                obj.ThoiHanGiayChungNhan_BatDau = Convert.ToDateTime(tem["HanCapTu"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }
            try
            {
                obj.ThoiHanGiayChungNhan_KetThuc = Convert.ToDateTime(tem["HanCapDen"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            string DienThoaiCoDinhDiDong = "";
            if (!string.IsNullOrEmpty(tem["DienThoai"].ToString()))
                DienThoaiCoDinhDiDong = tem["DienThoai"].ToString();
            if (!string.IsNullOrEmpty(tem["Mobile"].ToString()))
                if (!string.IsNullOrEmpty(tem["DienThoai"].ToString()))
                    DienThoaiCoDinhDiDong += "/" + tem["Mobile"].ToString();
                else
                    DienThoaiCoDinhDiDong = tem["Mobile"].ToString();
            obj.DienThoaiCoDinhDiDong = DienThoaiCoDinhDiDong;
            obj.Email = tem["Email"].ToString();
            lst.Add(obj);
        }

        ReportDocument rpt = new ReportDocument();
        rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
        rpt.Load(Server.MapPath("Report/QuanLyThongTinChung/DanhSachKiemToanVienTrongCaNuoc.rpt"));

        //rpt.SetDataSource(lst);
        rpt.Database.Tables["objDanhSachKiemToanVienTrongCaNuoc"].SetDataSource(lst);
        //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

        CrystalReportViewer1.ReportSource = rpt;
        CrystalReportViewer1.DataBind();

        if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachKiemToanVienTrongCaNuoc");
        if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
        {
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachKiemToanVienTrongCaNuoc");
            //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //xlsFormatOptions.ShowGridLines = true;
            //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

        }
        if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
        //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true,
        //                         "DanhSachCongTyKiemToanTrongCaNuoc");
        {
            string strNgayTinh = "(Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            string strNgayBaoCao = DateTime.Now.ToString("dd/MM/yyyy");
            ExportToExcel("DanhSachKiemToanVienTrongCaNuoc", lst, strNgayTinh, strNgayBaoCao);
        }
    }

    private void ExportToExcel(string strName, List<objDanhSachKiemToanVienTrongCaNuoc> lst, string NgayTinh, string NgayBC)
    {
        string outHTML = "";

        outHTML += "<table border = '1' cellpadding='5' cellspacing='0' style='font-family:Arial; font-size:8pt; color:#003366'>";
        //outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        //outHTML += "<td colspan='2' style='border-style:none'></td>";
        //outHTML += "<td colspan='2' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/logo.png' alt='' border='0px'/></td>";
        //outHTML += "<td colspan='10' style='border-style:none'></td>";

        //outHTML += "</tr>";
        outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        outHTML += "<td colspan='8' style='border-style:none'></td>";
        outHTML += "<td colspan='6' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/ten.png' alt='' border='0px'/></td>";

        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";

        outHTML += "<td colspan='9' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH KIỂM TOÁN VIÊN TRONG CẢ NƯỚC</b></td>";
        outHTML += "<td colspan='5'</td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='9' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i>" + NgayTinh + "</i></td>";
        outHTML += "<td colspan='5'</td>";
        outHTML += "</tr>";
        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "    <th rowspan = '2' width='30px' height ='150'>STT</th>";
        outHTML += "    <th rowspan = '2'>Họ và tên</th>";
        outHTML += "    <th colspan = '2'>Ngày sinh</th>";
        outHTML += "    <th rowspan = '2'>Hội viên VACPA</th>";
        outHTML += "    <th rowspan = '2'>Quê quán/Quốc tịch</th>";
        outHTML += "    <th rowspan = '2'>Chức vụ</th>";
        outHTML += "    <th rowspan = '2'>Số Chứng chỉ KTV</th>";
        outHTML += "    <th rowspan = '2'>Ngày cấp Chứng chỉ KTV</th>";
        outHTML += "    <th rowspan = '2'>Số giấy chứng nhận ĐKHN KiT</th>";
        outHTML += "    <th colspan = '2'>Thời hạn giấy chứng nhận ĐKHN KiT</th>";
        outHTML += "    <th rowspan = '2'>Điện thoại cố định/ di động</th>";
        outHTML += "    <th rowspan = '2'>Email</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th>Nam</th>";
        outHTML += "    <th>Nữ</th>";
        outHTML += "    <th>Ngày bắt đầu</th>";
        outHTML += "    <th>Ngày kết thúc</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th width='30px'>1</th>";
        outHTML += "    <th width='300px'>2</th>";
        outHTML += "    <th width='80px'>3</th>";
        outHTML += "    <th width='80px'>4</th>";
        outHTML += "    <th width='150px'>5</th>";
        outHTML += "    <th width='200px'>6</th>";
        outHTML += "    <th width='200px'>7</th>";
        outHTML += "    <th width='200px'>9</th>";
        outHTML += "    <th width='200px'>10</th>";
        outHTML += "    <th width='200px'>11</th>";
        outHTML += "    <th width='200px'>12</th>";
        outHTML += "    <th width='200px'>13</th>";
        outHTML += "    <th width='200px'>14</th>";
        outHTML += "    <th width='200px'>15</th>";
        outHTML += "</tr>";

        var q = lst.Select(m => new { m.SoHieuCongTy, m.TenCongTy }).Distinct();

        int i = 0;
        foreach (var temp in q)
        {
            outHTML += "<tr>";
            outHTML += "<td colspan='14' align='left'>" + temp.SoHieuCongTy.Replace("<", "_").Replace(">", "") + " " + temp.TenCongTy.Replace("<", "_").Replace(">", "") + "</td>";
            outHTML += "</tr>";

            var qChiTiet = lst.Where(t => t.SoHieuCongTy == temp.SoHieuCongTy && t.TenCongTy == temp.TenCongTy);
            foreach (var tChiTiet in qChiTiet)
            {
                i++;
                outHTML += "<tr>";
                outHTML += "<td align='center'>" + i.ToString() + "</td>";
                outHTML += "<td>" + tChiTiet.HoVaTen + "</td>";
                outHTML += "<td align='center'>" + tChiTiet.NgaySinhNam + "</td>";
                outHTML += "<td align='center'>" + tChiTiet.NgaySinhNu + "</td>";
                outHTML += "<td>" + tChiTiet.HoiVienVACPA + "</td>";
                outHTML += "<td>" + tChiTiet.QueQuanQuocTich + "</td>";
                outHTML += "<td>" + tChiTiet.ChucVu + "</td>";
                outHTML += "<td>" + tChiTiet.SoChungChiKTV + "</td>";
                outHTML += "<td align='center'>" + tChiTiet.NgayCapChungChiKTV + "</td>";
                outHTML += "<td>" + tChiTiet.SoGiayChungNhanDKHNKiT + "</td>";
                outHTML += "<td align='center'>" + tChiTiet.ThoiHanGiayChungNhan_BatDau + "</td>";
                outHTML += "<td align='center'>" + tChiTiet.ThoiHanGiayChungNhan_KetThuc + "</td>";
                outHTML += "<td>" + tChiTiet.DienThoaiCoDinhDiDong + "</td>";
                outHTML += "<td>" + tChiTiet.Email + "</td>";
                outHTML += "</tr>";
            }
        }

        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none'></td>";
        outHTML += "<td colspan='2' align='center' style='border-style:none'><b>" + NgayBC + "</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none'></td>";
        outHTML += "<td colspan='2' align='center' style='border-style:none'><b>NGƯỜI LẬP BIỂU<b></td>";
        outHTML += "</tr>";

        outHTML += "</table>";

        Library.ExportToExcel(strName, outHTML);
    }

    protected string strBoChamPhay(string p)
    {
        int leng = p.Length;
        if (p.LastIndexOf(',') != -1)
            leng = p.LastIndexOf(',');
        string strOut = p.Substring(0, leng);
        strOut = strOut.Replace(".", string.Empty);
        return strOut;
    }

    protected void LoadCheckBox()
    {
        if (this.IsPostBack)
        {
            if (Request.Form["danghanhnghe"] != null)
            {
                Response.Write("$('#danghanhnghe').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#danghanhnghe').attr('checked', false); ");
            }
            if (Request.Form["hoivien"] != null)
            {
                Response.Write("$('#hoivien').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#hoivien').attr('checked', false); ");
            }
        }
    }
}