﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Drawing;
using System.IO;

public partial class usercontrols_QLHV_BC_GiayChungNhanHoiVienTapThe : System.Web.UI.UserControl
{
    protected string tenchucnang = "Giấy chứng nhận hội viên tổ chức";
    //protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    //private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    public static Bitmap Convert_Text_to_Image(string txt, string fontname, int fontsize)
    {
        //creating bitmap image
        Bitmap bmp = new Bitmap(1, 1);

        //FromImage method creates a new Graphics from the specified Image.
        Graphics graphics = Graphics.FromImage(bmp);
        // Create the Font object for the image text drawing.
        Font font = new Font(fontname, fontsize, FontStyle.Italic);
        // Instantiating object of Bitmap image again with the correct size for the text and font.
        SizeF stringSize = graphics.MeasureString(txt, font);
        bmp = new Bitmap(bmp, (int)stringSize.Width, (int)stringSize.Height);
        graphics = Graphics.FromImage(bmp);


        // Set Background color
        graphics.Clear(Color.White);
        //graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        //graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
        /* It can also be a way
       bmp = new Bitmap(bmp, new Size((int)graphics.MeasureString(txt, font).Width, (int)graphics.MeasureString(txt, font).Height));*/

        //Draw Specified text with specified format 
        graphics.DrawString(txt, font, Brushes.Navy, 0, 0);
        font.Dispose();
        graphics.Flush();
        graphics.Dispose();
        return bmp;     //return Bitmap Image 
    }

    public void LoaiHinhDoanNghiep(string DonVi = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT LoaiHinhDoanhNghiepID,TenLoaiHinhDoanhNghiep FROM tblDMLoaiHinhDoanhNghiep ORDER BY TenLoaiHinhDoanhNghiep ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        if (DonVi != "00")
            output_html += @"<option value= 0>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value= 0>Tất cả</option>";
        
        foreach (DataRow loaiDN in dt.Rows)
        {
            if (DonVi.Contains(loaiDN["LoaiHinhDoanhNghiepID"].ToString().Trim()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + loaiDN["LoaiHinhDoanhNghiepID"].ToString().Trim() + @""">" + loaiDN["TenLoaiHinhDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + loaiDN["LoaiHinhDoanhNghiepID"].ToString().Trim() + @""">" + loaiDN["TenLoaiHinhDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public static string ConvertToTCVN3(string value)
    {
        string text = "";
        int i = 0;
        while (i < value.Length)
        {
            char value2 = char.Parse(value.Substring(i, 1));
            short num = System.Convert.ToInt16(value2);
            if (num <= 259)
            {
                if (num <= 202)
                {
                    if (num != 194)
                    {
                        switch (num)
                        {
                            case 201: text += "Ð"; break;
                            case 202: text += "ü"; break;
                            default: goto IL_7AF;
                        }
                    }
                    else text += "¢";
                }
                else
                {
                    if (num != 212)
                    {
                        switch (num)
                        {
                            case 224: text += "µ"; break;
                            case 225: text += "¸"; break;
                            case 226: text += "©"; break;
                            case 227: text += "·"; break;
                            case 228:
                            case 229:
                            case 230:
                            case 231:
                            case 235:
                            case 238:
                            case 239:
                            case 240:
                            case 241:
                            case 246:
                            case 247:
                            case 248:
                            case 251:
                            case 252: goto IL_7AF;
                            case 232: text += "Ì"; break;
                            case 233: text += "Ð"; break;
                            case 234: text += "ª"; break;
                            case 236: text += "ỡ"; break;
                            case 237: text += "Ý"; break;
                            case 242: text += "ß"; break;
                            case 243: text += "ã"; break;
                            case 244: text += "«"; break;
                            case 245: text += "ừ"; break;
                            case 249: text += "ự"; break;
                            case 250: text += "ư"; break;
                            case 253: text += "ý"; break;
                            default:
                                switch (num)
                                {
                                    case 258: text += "¡"; break;
                                    case 259: text += "¨"; break;
                                    default: goto IL_7AF;
                                }
                                break;
                        }
                    }
                    else text += "¤";
                }
            }
            else
            {
                if (num <= 361)
                {
                    switch (num)
                    {
                        case 272: text += "§"; break;
                        case 273: text += "®"; break;
                        default:
                            if (num != 297)
                            {
                                if (num != 361) goto IL_7AF;
                                text += "ò";
                            }
                            else text += "Ü";
                            break;
                    }
                }
                else
                {
                    switch (num)
                    {
                        case 416: text += "¥"; break;
                        case 417: text += "¬"; break;
                        default:
                            if (num != 431)
                            {
                                switch (num)
                                {
                                    case 7841: text += "¹"; break;
                                    case 7842:
                                    case 7844:
                                    case 7846:
                                    case 7848:
                                    case 7850:
                                    case 7852:
                                    case 7854:
                                    case 7856:
                                    case 7858:
                                    case 7860:
                                    case 7862:
                                    case 7864:
                                    case 7866:
                                    case 7868:
                                    case 7870:
                                    case 7872:
                                    case 7874:
                                    case 7876:
                                    case 7878:
                                    case 7880:
                                    case 7882:
                                    case 7884:
                                    case 7886:
                                    case 7888:
                                    case 7890:
                                    case 7892:
                                    case 7894:
                                    case 7896:
                                    case 7898:
                                    case 7900:
                                    case 7902:
                                    case 7904:
                                    case 7906:
                                    case 7908:
                                    case 7910:
                                    case 7912:
                                    case 7914:
                                    case 7916:
                                    case 7918:
                                    case 7920:
                                    case 7922:
                                    case 7924:
                                    case 7926:
                                    case 7928: goto IL_7AF;
                                    case 7843: text += "¶"; break;
                                    case 7845: text += "Ê"; break;
                                    case 7847: text += "Ç"; break;
                                    case 7849: text += "È"; break;
                                    case 7851: text += "É"; break;
                                    case 7853: text += "Ë"; break;
                                    case 7855: text += "¾"; break;
                                    case 7857: text += "»"; break;
                                    case 7859: text += "¼"; break;
                                    case 7861: text += "½"; break;
                                    case 7863: text += "Æ"; break;
                                    case 7865: text += "Ñ"; break;
                                    case 7867: text += "Î"; break;
                                    case 7869: text += "Ï"; break;
                                    case 7871: text += "Õ"; break;
                                    case 7873: text += "Ò"; break;
                                    case 7875: text += "Ó"; break;
                                    case 7877: text += "Ô"; break;
                                    case 7879: text += "Ö"; break;
                                    case 7881: text += "Ø"; break;
                                    case 7883: text += "Þ"; break;
                                    case 7885: text += "ä"; break;
                                    case 7887: text += "á"; break;
                                    case 7889: text += "è"; break;
                                    case 7891: text += "å"; break;
                                    case 7893: text += "æ"; break;
                                    case 7895: text += "ç"; break;
                                    case 7897: text += "é"; break;
                                    case 7899: text += "í"; break;
                                    case 7901: text += "ê"; break;
                                    case 7903: text += "ë"; break;
                                    case 7905: text += "ì"; break;
                                    case 7907: text += "î"; break;
                                    case 7909: text += "ô"; break;
                                    case 7911: text += "ñ"; break;
                                    case 7913: text += "ø"; break;
                                    case 7915: text += "õ"; break;
                                    case 7917: text += "ö"; break;
                                    case 7919: text += "÷"; break;
                                    case 7921: text += "ù"; break;
                                    case 7923: text += "ú"; break;
                                    case 7925: text += "þ"; break;
                                    case 7927: text += "û"; break;
                                    case 7929: text += "ü"; break;
                                    default: goto IL_7AF;
                                }
                            }
                            else text += "¦";
                            break;
                    }
                }
            }
        IL_7BF:
            i++;
            continue;
        IL_7AF:
            text += value2.ToString();
            goto IL_7BF;
        }
        return text;
    }

    public void Load_ThanhPho(string MaTinh = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        string VM = Request.Form["VungMienId"];
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT MaTinh,TenTinh FROM tblDMTinh WHERE HieuLuc='1' ";
        if (!string.IsNullOrEmpty(VM) && VM != "0" && VM != "-1")
            sql.CommandText = sql.CommandText + " and VungMienID in (" + VM + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (MaTinh.Trim() == Tinh["MaTinh"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["MaTinh"].ToString().Trim() + @""">" + Tinh["TenTinh"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void VungMien(string ma = "00")
    {
        List<objDoiTuong> lst = getVungMien();
        string output_html = "";

        if (ma != "00")
            output_html += @"<option value=-1>Tất cả</option>";
        else
            output_html += @"<option selected=""selected"" value=-1>Tất cả</option>";

        foreach (var tem in lst)
        {
            if (ma.Contains(tem.idDoiTuong.ToString()))
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }


    public class objDoiTuong
    {
        public int idDoiTuong { set; get; }
        public string TenDoiTuong { set; get; }
    }

    public static byte[] ImageToByte2(System.Drawing.Image img)
    {
        byte[] byteArray = new byte[0];
        using (MemoryStream stream = new MemoryStream())
        {
            img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            stream.Close();

            byteArray = stream.ToArray();
        }
        return byteArray;
    }

    public void setdataCrystalReport()
    {
        string strid = idDanhSach.Text;
        if (!string.IsNullOrEmpty(strid))
        {
            //int id = Convert.ToInt32(strid);
            List<objThongTinDoanhNghiep_CT> lst = new List<objThongTinDoanhNghiep_CT>();

            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep, TenTiengAnh, SoQuyetDinhKetNap, NgayQuyetDinhKetNap "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + strid + "'";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        //List<lstOBJKhenThuong> lstKhenThuong = new List<lstOBJKhenThuong>();
        //List<lstOBJKyLuat> lstKyLuat = new List<lstOBJKyLuat>();
       // int i = 1;
        List<Anh_Tr> lst_Anh = new List<Anh_Tr>();

        foreach (DataRow tem in dt.Rows)
        {
            objThongTinDoanhNghiep_CT new1 = new objThongTinDoanhNghiep_CT();


            // if view and export PDF to image view, if file word to text (Company Name)
            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                new1.TenDoanhNghiep = Converter.ConvertToTCVN3( tem["TenDoanhNghiep"].ToString().Replace("\n",""));
            else
            {
                Bitmap data = Convert_Text_to_Image(tem["TenDoanhNghiep"].ToString().Replace("\n", ""), "Monotype Corsiva", 22); // Passing appropriate value to Convert_Text_to_Image method 

                byte[] arr = ImageToByte2(data);
                //MemoryStream ms = new MemoryStream(arr);

                Anh_Tr aabb = new Anh_Tr();
                aabb.arr = arr;
                // aabb.arr = data;
                lst_Anh.Add(aabb);
            }
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //    new1.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString().Replace("\n", "");
            new1.TenTiengAnh = tem["TenTiengAnh"].ToString() + " is full Member of VACPA";
            new1.SoQD = Converter.ConvertToTCVN3(tem["SoQuyetDinhKetNap"].ToString());
            new1.NgayCN = !string.IsNullOrEmpty(tem["NgayQuyetDinhKetNap"].ToString()) ? "Hà Nội, ngày " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Day + " tháng " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Month + " năm " + Convert.ToDateTime(tem["NgayQuyetDinhKetNap"]).Year : "Hà Nội, ngày....tháng....năm......";
            
           
            lst.Add(new1);

            //lstOBJKhenThuong con = new lstOBJKhenThuong();
            //con.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            //con.HinhThuc = "a" + i;
            //con.LyDo = "b" + i;
            //con.NgayThang = "c" + i;
            //lstOBJKhenThuong con1 = new lstOBJKhenThuong();
            //con1.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            //con1.HinhThuc = "a1" + i;
            //con1.LyDo = "b1" + i;
            //con1.NgayThang = "c1" + i;
            
            ////lstKhenThuong.Add(con);
            ////lstKhenThuong.Add(con1);
          

            //lstOBJKyLuat new1a = new lstOBJKyLuat();
            //new1a.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            //new1a.LyDo = "l1"+i;
            //new1a.HinhThuc = "";
            //new1a.NgayThang = "";
            //lstOBJKyLuat new2a = new lstOBJKyLuat();
            //new2a.HoTen = tem["HODEM"].ToString() + " " + tem["TEN"].ToString();
            //new2a.LyDo = "l1"+i;
            //new2a.HinhThuc = "";
            //new2a.NgayThang = "";
            //lstKyLuat.Add(new1a);
            //lstKyLuat.Add(new2a);
            //i++;
        }
        

            
            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/QLHV/GiayChungNhanHoiVienTapThe.rpt"));
           // rpt.Load(Server.MapPath("Report/QLHV/ReportTest.rpt"));
            //rpt.SetDataSource(lst);
            rpt.Database.Tables["objThongTinDoanhNghiep_CT"].SetDataSource(lst);
            rpt.Database.Tables["Anh_Tr"].SetDataSource(lst_Anh);
            //rpt.Database.Tables["lstOBJKhenThuong"].SetDataSource(lstKhenThuong);
            //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);
          
            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.DataBind();

            if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "GiayChungNhanHoiVienTapThe");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "GiayChungNhanHoiVienTapThe");
                //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
                //xlsFormatOptions.ShowGridLines = true;
                //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
                //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            }

            //ReportDocument rdPL = new ReportDocument();
            //rdPL.Load(Application.StartupPath + "\\GCN\\312_313\\" + strFilePL);
            //rdPL.SetDataSource(ds);
            //rdPL.SetParameterValue("NoiCapPL", txtNoiCapPL.Text,);
            //crtXemPL.ReportSource = rdPL;
            //crtXemPL.Refresh();

            //rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            //    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
            //{
            //    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoKetQuaToChucLopHocCNKTKTV");
            //    //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //    //xlsFormatOptions.ShowGridLines = true;
            //    //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //    //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //    //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

            //}
        }
    }

    public string NgayThangVN(DateTime dtime)
    {

        string Ngay = dtime.Day.ToString().Length == 1 ? "0" + dtime.Day : dtime.Day.ToString();
        string Thang = dtime.Month.ToString().Length == 1 ? "0" + dtime.Month : dtime.Month.ToString();
        string Nam = dtime.Year.ToString();


        string NT = Ngay + "/" + Thang + "/" + Nam;
        return NT;
    }

    public void DinhDangFile(string ma = "1")
    {

        List<objDoiTuong> lst = getDinhDangFile();
        string output_html = "";
        

        //output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (var tem in lst)
        {
            if (ma == tem.idDoiTuong.ToString())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + tem.idDoiTuong + @""">" + tem.TenDoiTuong + @"</option>";
            }
        }

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    private List<objDoiTuong> getDinhDangFile()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Word";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Excel";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "PDF";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }
    private List<objDoiTuong> getVungMien()
    {
        List<objDoiTuong> lst = new List<objDoiTuong>();

        objDoiTuong obj1 = new objDoiTuong();
        obj1.idDoiTuong = 1;
        obj1.TenDoiTuong = "Miền Bắc";
        objDoiTuong obj2 = new objDoiTuong();
        obj2.idDoiTuong = 2;
        obj2.TenDoiTuong = "Miền Trung";
        objDoiTuong obj3 = new objDoiTuong();
        obj3.idDoiTuong = 3;
        obj3.TenDoiTuong = "Miền Nam";

        lst.Add(obj1);
        lst.Add(obj2);
        lst.Add(obj3);

        return lst;
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        if (Session["DSChon"] != null)
        {
            List<string> lstIdChon = new List<string>();
            lstIdChon = Session["DSChon"] as List<string>;
            string result = string.Join(",", lstIdChon);
            idDanhSach.Text = result;

            if (lstIdChon.Count() != 0)
            {
                DataSet ds = new DataSet();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select TenDoanhNghiep "
                            + " from dbo.tblHoiVienTapThe "
                            + " WHERE MaHoiVienTapThe = '" + lstIdChon[0] + "'";// + id;
                ds = DataAccess.RunCMDGetDataSet(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                DataTable dt = ds.Tables[0];
                foreach (DataRow tem in dt.Rows)
                    txtCongTy.Text = tem["TenDoanhNghiep"].ToString();
            }
            CrystalReportViewer1.ReportSource = null;
            CrystalReportViewer1.DataBind();
        }
    }

    protected void Text_Change(object sender, EventArgs e)
    {
        try{
            DataSet ds = new DataSet();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select TenDoanhNghiep "
                        + " from dbo.tblHoiVienTapThe "
                        + " WHERE MaHoiVienTapThe = '" + idDanhSach.Text + "'";// + id;
            ds = DataAccess.RunCMDGetDataSet(sql);
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;

            DataTable dt = ds.Tables[0];
            foreach (DataRow tem in dt.Rows)
                txtCongTy.Text = tem["TenDoanhNghiep"].ToString();
        }
        catch { }
    }

}