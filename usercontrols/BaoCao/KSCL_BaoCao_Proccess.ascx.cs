﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_BaoCao_Proccess : System.Web.UI.UserControl
{
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string action = Library.CheckNull(Request.QueryString["action"]);
            if (action == "loadttdanhsach")
            {
                string maDanhSachKiemTraTrucTiep = Library.CheckNull(Request.QueryString["madanhsach"]);
                string nam = Library.CheckNull(Request.QueryString["nam"]);
                LoadThongTinDanhSachKiemTraTrucTiep(maDanhSachKiemTraTrucTiep, nam);
            }
            if (action == "loadttdkt")
            {
                string maDoanKiemTra = Library.CheckNull(Request.QueryString["madkt"]);
                string nam = Library.CheckNull(Request.QueryString["nam"]);
                LoadThongTinDoanKiemTra(maDoanKiemTra, nam);
            }

            if (action == "loaddabcktkt")
            {
                string idDkt = Library.CheckNull(Request.QueryString["iddkt"]);
                string idCongTy = Library.CheckNull(Request.QueryString["idct"]);
                LoadDanhSachBaoCaoKiemTraKyThuat(idDkt, idCongTy);
            }
            if (action == "loaddscthtsp")
            {
                string nam = Library.CheckNull(Request.QueryString["nam"]);
                LoadDanhSachCongTyKiemTraSaiPhamTheoNam(nam);
            }
            if (action == "loaddstinhthanh")
            {
                string idVung = Library.CheckNull(Request.QueryString["idvung"]);
                LoadDanhSachTinhThanhTheoVungMien(idVung);
            }
            if (action == "loaddstukiemtra")
            {
                string maDanhSach = Library.CheckNull(Request.QueryString["madanhsach"]);
                string nam = Library.CheckNull(Request.QueryString["nam"]);
                GetInforDanhSachPhaiNopBCTuKiemTra(maDanhSach, nam);
            }
        }
        catch
        {
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void LoadThongTinDanhSachKiemTraTrucTiep(string maDanhSachKiemTraTrucTiep, string nam)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var dataDS = '';" + Environment.NewLine;
        string query = @"SELECT DSKiemTraTrucTiepID FROM tblKSCLDSKiemTraTrucTiep WHERE MaDanhSach = '" + maDanhSachKiemTraTrucTiep + @"' AND YEAR(NgayLap) = " + nam;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            js += "dataDS = '" + ht["DSKiemTraTrucTiepID"] + "';" + Environment.NewLine;
        }
        js += "parent.DisplayInforDanhSach(dataDS);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void LoadThongTinDoanKiemTra(string maDoanKiemTra, string nam)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var dataDS = '';" + Environment.NewLine;
        string query = @"SELECT DISTINCT DKT.DoanKiemTraID, DKT.MaDoanKiemTra, DKT.NgayLap, DKT.QuyetDinhThanhLap, DMTT.MaTrangThai, DMTT.TenTrangThai
	                         FROM tblKSCLDoanKiemTra AS DKT
	                         LEFT JOIN tblDMTrangThai AS DMTT ON DKT.TinhTrangID = DMTT.TrangThaiID
	                         WHERE 
		                        DKT.MaDoanKiemTra = '" + maDoanKiemTra + @"'				                        
		                        AND DMTT.MaTrangThai = 5
		                        AND YEAR(DKT.NgayLap) = " + nam + @"
	                         ORDER BY DKT.DoanKiemTraID DESC";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            js += "dataDS = '" + ht["DoanKiemTraID"] + ";#" + ht["MaDoanKiemTra"] + "';" + Environment.NewLine;
        }
        js += "parent.DisplayInforDoanKiemTra(dataDS);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void LoadDanhSachBaoCaoKiemTraKyThuat(string idDkt, string idCongTy)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        string query = @"SELECT BCKTKT.BaoCaoKTKTID, BCKTKT.SoBaoCaoKT, BCKTKT.NgayBaoCaoKT
                            FROM tblKSCLBaoCaoKTKT BCKTKT
                            INNER JOIN tblKSCLHoSo HS ON BCKTKT.HoSoID = HS.HoSoID
                            WHERE HS.DoanKiemTraID = " + idDkt + @" AND HS.HoiVienTapTheID = " + idCongTy;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                js += "arrData.push(['" + ht["BaoCaoKTKTID"] + "', '" + ht["SoBaoCaoKT"] + " - " + Library.DateTimeConvert(ht["NgayBaoCaoKT"]).ToString("dd/MM/yyyy") + "']);" + Environment.NewLine;
            }
        }
        else
        {
            js += "arrData.push(['0', 'Không tìm thấy báo cáo kiểm tra kỹ thuật của công ty này!']);" + Environment.NewLine;
        }
        js += "parent.DisplayListDanhSachBaoCaoKiemTraKyThuat(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void LoadDanhSachCongTyKiemTraSaiPhamTheoNam(string nam)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        string query = @"SELECT DISTINCT HVTC.HoiVienTapTheID, HVTC.TenDoanhNghiep, HVTC.TenVietTat
                            FROM tblKSCLDoanKiemTraCongTy AS DKT_CT	
                            INNER JOIN tblKSCLDoanKiemTra DKT ON DKT_CT.DoanKiemTraID = DKT.DoanKiemTraID   
                            INNER JOIN tblDMTrangThai AS DMTT ON DKT.TinhTrangID = DMTT.TrangThaiID                     
                            LEFT JOIN tblHoiVienTapThe HVTC ON DKT_CT.HoiVienTapTheID = HVTC.HoiVienTapTheID
                            WHERE 
                            YEAR(DKT.NgayLap) = " + nam + @" AND DMTT.MaTrangThai = 5
                            ORDER BY HVTC.TenDoanhNghiep DESC";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                string tenHienThi = ht["TenVietTat"].ToString();
                if (string.IsNullOrEmpty(tenHienThi))
                    tenHienThi = ht["TenDoanhNghiep"].ToString();
                js += "arrData.push(['" + ht["HoiVienTapTheID"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(tenHienThi) + "']);" + Environment.NewLine;
            }
        }
        js += "parent.DrawData_DanhSachCongTy(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/03/27
    /// Load danh sach tinh thanh theo id Vung mien
    /// </summary>
    /// <param name="idVungMien">ID vung mien</param>
    private void LoadDanhSachTinhThanhTheoVungMien(string idVungMien)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        if (!string.IsNullOrEmpty(idVungMien))
        {
            string query = "SELECT TinhID, TenTinh FROM tblDMTinh WHERE HieuLuc = '1' AND ([VungMien])";
            if (idVungMien.Contains("all"))
                query = "SELECT TinhID, TenTinh FROM tblDMTinh WHERE HieuLuc = '1'";
            else
            {
                string clause = "";
                string[] arrListIdVungMien = idVungMien.Split(',');
                foreach (string s in arrListIdVungMien)
                {
                    if (!string.IsNullOrEmpty(s))
                        clause += " VungMienID = " + s + " OR";
                }
                clause = clause.TrimEnd('R').TrimEnd('O');
                query = query.Replace("[VungMien]", clause);
            }
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    js += "arrData.push(['" + ht["TinhID"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenTinh"]) + "']);" + Environment.NewLine;
                }
            }
        }
        js += "parent.DrawData_DanhSachTinhThanh(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/04/06
    /// Get thông tin chi tiết của danh sách phải nộp báo cáo tự kiểm tra theo mã danh sách và năm lập danh sách
    /// </summary>
    /// <param name="maDanhSach">Mã danh sách</param>
    /// <param name="nam">Năm lập danh sách</param>
    private void GetInforDanhSachPhaiNopBCTuKiemTra(string maDanhSach, string nam)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#';" + Environment.NewLine;
        string query = @"SELECT DSBaoCaoTuKiemTraID, MaDanhSach, tblDMTrangThai.MaTrangThai, NgayLap,
                                 (SELECT COUNT(DSKiemTraTrucTiepID) FROM tblKSCLDSKiemTraTrucTiep WHERE DSBaoCaoTuKiemTraID = a.DSBaoCaoTuKiemTraID) AS DaDuocAddVaoDSKiemTraTrucTiep
                                  FROM tblKSCLDSBaoCaoTuKiemTra AS a LEFT JOIN tblDMTrangThai ON a.TinhTrangID = tblDMTrangThai.TrangThaiID
                                  WHERE a.MaDanhSach = '" + maDanhSach + @"' AND YEAR(a.NgayLap) = " + nam;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            bool flag = true;
            if (ht["MaTrangThai"].ToString().Trim() != ListName.Status_DaPheDuyet)
            {
                js += "parent.alert('Danh sách với mã vừa nhập chưa được phê duyệt!');" + Environment.NewLine;
                flag = false;
            }
            if (flag)
                js += "value = '" + ht["DSBaoCaoTuKiemTraID"] + ";#" + ht["MaDanhSach"] + ";#" + Library.DateTimeConvert(ht["NgayLap"]).ToString("dd/MM/yyyy") + "';" + Environment.NewLine;
        }
        else
            js += "parent.alert('Không tìm thấy danh sách với mã vừa nhập!');" + Environment.NewLine;
        js += "parent.DisplayInforDsTuKiemTra(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }
}