﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam.ascx.cs"
    Inherits="usercontrols_KSCL_BaoCao_DanhSachCacCongTyKiemToanDaKiemTraChatLuongQuaCacNam" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
    .ui-dropdownchecklist {
        background: #ffffff !important;
        border: solid 1PX #cccccc!important;
        margin: 0px;
  height: 22px;
  width:152px;
  -webkit-appearance: menulist;
  box-sizing: border-box;
  border: 1px solid;
  border-image-source: initial;
  border-image-slice: initial;
  border-image-width: initial;
  border-image-outset: initial;
  border-image-repeat: initial;
  white-space: pre;
  -webkit-rtl-ordering: logical;
  color: black;
  background-color: white;
  cursor: default;
    }
    .ui-dropdownchecklist span {
        width:100%;
    }
    .ui-dropdownchecklist span span{
        width:100%;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Danh sách công ty kiểm toán đã kiểm tra chất lượng qua các năm</h4>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="800px" border="0" class="formtbl">
        <tr>
            <td style="min-width: 120px;">
                Vùng miền:
            </td>
            <td>
                <div style="height: 150px; overflow: auto;">
                    <table id="tblDanhSachVungMien" style="min-width: 250px;" border="0" class="formtbl">
                        <tr>
                            <td style="width: 30px; text-align: center;">
                                <input type="checkbox" class="checkallVungMien" id="cboCheckAllVungMien" name="cboCheckAllVungMien" value="all" checked="checked" />
                            </td>
                            <td>
                                Tất cả
                            </td>
                        </tr>
                        <% GetListVungMien(); %>
                    </table>
                </div>
            </td>
            <td>
                Tỉnh/thành:
            </td>
            <td>
                <div style="height: 150px; overflow: auto;">
                    <table id="tblDanhSachTinhThanh" style="min-width: 250px;" border="0" class="formtbl">
                        <tbody id="TBodyChonTatCa">
                            <tr>
                                <td style="width: 30px; text-align: center;">
                                    <input type="checkbox" class="checkall" id="cboCheckAll" name="cboCheckAll" value="all" />
                                </td>
                                <td>
                                    Tất cả
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>Vùng miền:</td>
            <td><select name="VungMien" id="VungMien" multiple="multiple">
                    <%  
                        
                        try
                        {
                           
                            LoadVungMien();
                        }
                        catch (Exception ex)
                        {

                        }
                    %>
                </select></td>
            <td>Tỉnh/thành:</td>
            <td><select name="TinhThanh" id="TinhThanh">
                    <%  
                        
                        try
                        {
                           
                            LoadTinhThanh();
                        }
                        catch (Exception ex)
                        {

                        }
                    %>
                </select></td>
        </tr>
        <tr>
            <td>
                Từ năm:
            </td>
            <td>
                <select id="ddlTuNam" name="ddlTuNam">
                    <% GetListYear();%>
                </select>
            </td>
            <td>
                Đến năm:
            </td>
            <td>
                <select id="ddlDenNam" name="ddlDenNam">
                    <% GetListYear();%>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('excel');"><i class="iconsweets-excel2"></i>Kết xuất Excel</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Danh sách công ty kiểm toán đã kiểm tra chất lượng qua các năm</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" ShowAllPageIds="True"
        HasExportButton="False" HasPrintButton="False" AutoDataBind="True" />
</fieldset>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    function CallActionGetDanhSachTinhThanh() {
        var listId = '';
        $('#tblDanhSachVungMien :checkbox').each(function () {
                if($(this).prop("checked"))
                    listId += $(this).val() + ',';
            });
        iframeProcess.location = '/iframe.aspx?page=KSCL_BaoCao_Proccess&action=loaddstinhthanh&idvung=' + listId;
    }
    
    <% SetDataJavascript();%>

    function DrawData_DanhSachTinhThanh(arrData) {
        var tbl = document.getElementById('tblDanhSachTinhThanh');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        $('#cboCheckAll').prop('checked', false);
        if(arrData.length > 0) {
            document.getElementById('TBodyChonTatCa').style.display = '';
            for(var i = 0; i < arrData.length; i ++) {
                var idCongTy = arrData[i][0];
                var tenCongTy = arrData[i][1];
                
                var tr = document.createElement('TR');
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                if(arrTinhThanh.indexOf(idCongTy) > -1)
                    tdCheckBox.innerHTML = "<input type='checkbox' id='cboTinh_"+idCongTy+"' name='cboTinh_"+idCongTy+"' value='"+tenCongTy+"' checked='checked' />";
                else
                    tdCheckBox.innerHTML = "<input type='checkbox' id='cboTinh_"+idCongTy+"' name='cboTinh_"+idCongTy+"' value='"+tenCongTy+"' />";
                tr.appendChild(tdCheckBox);
               

                var arrColumn = [tenCongTy];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
            if(flagCheckAll) {
                $('#cboCheckAll').prop('checked', true);
                $('#tblDanhSachTinhThanh :checkbox').each(function () {
                    $(this).prop("checked", true);
                });
            }
            else {
                if(arrTinhThanh.length == 0) {
                    $('#tblDanhSachTinhThanh :checkbox').each(function () {
                    $(this).prop("checked", true);
                });
                }
            }
        } else {
            document.getElementById('TBodyChonTatCa').style.display = 'none';
            var tr = document.createElement('TR');
            var td = document.createElement("TD");
            td.colspan = 2;
            td.innerHTML = "<span style='color: red;'>Không tìm thấy thông tin tỉnh/thành phố!</span>";
            tr.appendChild(td); 
            tbl.appendChild(tr);
        }
        arrTinhThanh = []; // reset mang ve rong -> lan chon tiep theo ko bi phu thuoc vao du lieu cu
    }
    
    jQuery(document).ready(function () {
        $("#tblDanhSachVungMien .checkallVungMien").bind("click", function () {
            var  checked = $(this).prop("checked");
            $('#tblDanhSachVungMien :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
            CallActionGetDanhSachTinhThanh();
        });
        
        $("#tblDanhSachTinhThanh .checkall").bind("click", function () {
            var  checked = $(this).prop("checked");
            $('#tblDanhSachTinhThanh :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });

    function Export(type) {
        // Kiểm tra bắt phải chọn tối thiểu 1 tỉnh thành
        var flag = false;
        $('#tblDanhSachTinhThanh :checkbox').each(function () {
               if($(this).prop("checked"))
                   flag = true;
            });
        if(!flag) {
            alert('Phải chọn tối thiểu 1 tỉnh/thành phố để thực hiện tổng hợp số liệu!');
            return false;
        }
        if(parseInt($('#ddlTuNam option:selected').val()) > parseInt($('#ddlDenNam option:selected').val())) {
            alert('Năm bắt đầu phải nhỏ hơn hoặc bằng năm kết thúc!');
            return false;
        }
        if(flag) {
            $('#hdAction').val(type);
            $('#Form1').submit();   
        }
    }

    $("#VungMien").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 150, emptyText: "Tất cả" });
    var timestamp = Number(new Date());
    $('#VungMien').change(function () {
        $('#TinhThanh').html('');
        //alert($('#VungMien').val());
        $('#TinhThanh').load('noframe.aspx?page=chondiaphuong&default=0&type=tt&id_vungmien=' + $('#VungMien').val() + '&time=' + timestamp);
    });
    
</script>
<div id="DivDanhSachDoanKiemTra">
</div>
<div id="DivDanhSachCongTy">
</div>
