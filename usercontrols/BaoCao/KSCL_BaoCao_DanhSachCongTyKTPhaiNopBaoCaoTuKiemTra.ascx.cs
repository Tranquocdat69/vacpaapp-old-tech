﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách công ty kiểm toán phải nộp báo cáo tự kiểm tra";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = "";
            if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            {
                nam = Request.Form["ddlNam"];
                int idDanhSach = Library.Int32Convert(Request.Form["hdDanhSachID"]);
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/DanhSachCongTyKiemToanPhaiNopBaoCaoTuKiemTra.rpt"));
                DataTable dt = GetDanhSachCongTy(nam, idDanhSach);
                rpt.Database.Tables["dtDanhSachCongTyKiemToanPhaiNopBaoCaoTuKiemTra"].SetDataSource(dt);
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "DANH SÁCH CÔNG TY KIỂM TOÁN PHẢI NỘP BÁO CÁO TỰ KIỂM TRA NĂM " + nam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    ExportWord(dt, nam);
                    //rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachCongTyPhaiNopBaoCaoTuKiemTraNam_" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, nam);
                //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "DanhSachCongTyPhaiNopBaoCaoTuKiemTraNam_" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachCongTyPhaiNopBaoCaoTuKiemTraNam_" + nam);
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string nam = Request.Form["ddlNam"];
        string maDanhSach = Request.Form["txtMaDanhSach"];
        if (string.IsNullOrEmpty(Request.Form["ddlNam"])) // Neu load trang lan dau chua post du lieu thi moi lay gia tri tu querystring
        {
            if (string.IsNullOrEmpty(maDanhSach))
                maDanhSach = Library.CheckNull(Request.QueryString["mads"]);
            // Lay nam lap danh sach
            if (!string.IsNullOrEmpty(maDanhSach))
            {
                try
                {
                    _db.OpenConnection();
                    string query = "SELECT YEAR(NgayLap) nam FROM tblKSCLDSBaoCaoTuKiemTra WHERE MaDanhSach = '" + maDanhSach + "'";
                    List<Hashtable> listData = _db.GetListData(query);
                    if (listData.Count > 0)
                    {
                        nam = listData[0]["nam"].ToString();
                    }
                }
                catch { _db.CloseConnection(); }
                finally
                {
                    _db.CloseConnection();
                }
            }
        }
        string js = "";
        if (!string.IsNullOrEmpty(nam))
        {
            js = "$('#ddlNam').val('" + nam + "');" + Environment.NewLine;
        }
        if (!string.IsNullOrEmpty(maDanhSach))
        {
            js += "$('#txtMaDanhSach').val('" + maDanhSach + "');" + Environment.NewLine;
            js += "CallActionGetInforDanhSach();" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private DataTable GetDanhSachCongTy(string nam, int idDanhSach)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "SoHieuCongTy", "TenCongTy", "TenVietTat", "Email", "SDT", "NguoiDDPL_Ten", "NguoiDDPL_Mobile", "HVTT", "Tren3NamChuaKiemTra", "XepLoaiKem2LanLienKe", "TieuChiKhac", "MaDanhSach", "NgayLapDanhSach" });
        if (string.IsNullOrEmpty(nam))
            return dt;
        string procName = ListName.Proc_BAOCAO_KSCL_DanhSachCongTyKiemToanPhaiNopBaoCaoTuKiemTra;
        object tempIdDanhSach = DBNull.Value;
        if (idDanhSach > 0)
            tempIdDanhSach = idDanhSach;
        List<Hashtable> listData = _db.GetListData(procName, new List<string> { "@Nam", "@IdDanhSach" }, new List<object> { nam, tempIdDanhSach });
        if (listData.Count > 0)
        {
            DataRow dr;
            int index = 1;
            foreach (Hashtable ht in listData)
            {
                dr = dt.NewRow();
                dr["STT"] = index;
                dr["SoHieuCongTy"] = ht["SoHieu"];
                dr["TenCongTy"] = ht["TenDoanhNghiep"];
                dr["TenVietTat"] = ht["TenVietTat"];
                dr["Email"] = ht["Email"];
                dr["SDT"] = ht["DienThoai"];
                dr["NguoiDDPL_Ten"] = ht["NguoiDaiDienPL_Ten"];
                dr["NguoiDDPL_Mobile"] = ht["NguoiDaiDienPL_DiDong"];
                string loaiHoiVien = ht["LoaiHoiVienTapThe"].ToString();
                if (loaiHoiVien == "1")
                    dr["HVTT"] = ht["x"];
                string lanKiemTraGanNhat = ht["LanKiemTraGanNhat"].ToString();
                if (Library.Int32Convert(lanKiemTraGanNhat) > 0 && (Library.Int32Convert(nam) - Library.Int32Convert(lanKiemTraGanNhat) >= 3))
                    dr["Tren3NamChuaKiemTra"] = ht["x"];
                int soLanXepLoaiKem2LanLienKe = Library.Int32Convert(ht["SoLanXepLoaiKem2LanLienKe"]);
                if (soLanXepLoaiKem2LanLienKe > 0 && soLanXepLoaiKem2LanLienKe >= 6)
                    dr["XepLoaiKem2LanLienKe"] = ht["x"];
                dr["MaDanhSach"] = "Mã: " + ht["MaDanhSach"] + " <i>(Ngày lập: " + Library.DateTimeConvert(ht["NgayLap"]).ToString("dd/MM/yyyy") + ")</i>";
                dt.Rows.Add(dr);
                index++;
            }
        }
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, string nam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='12' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='12' style='text-align:center;font-size: 14pt; font-weight: bold;'>DANH SÁCH CÔNG TY KIỂM TOÁN PHẢI NỘP BÁO CÁO TỰ KIỂM TRA NĂM " + nam + @"</td>
            </tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn'>STT</td>
                <td class='HeaderColumn' style='width: 70px;'>Số hiệu công ty</td>
                <td class='HeaderColumn' style='width: 170px;'>Tên công ty</td>
                <td class='HeaderColumn' style='width: 70px;'>Tên viết tắt</td>
                <td class='HeaderColumn' style='width: 100px;'>Email</td>
                <td class='HeaderColumn' style='width: 100px;'>SĐT</td>                
                <td class='HeaderColumn' style='width: 100px;'>Họ và tên người đại diện theo pháp luật (hoặc TGĐ, GĐ)</td>
                <td class='HeaderColumn' style='width: 100px;'>Mobile người đại diện theo pháp luật (hoặc TGĐ/GĐ)</td>
                <td class='HeaderColumn' style='width: 100px;'>HVTT</td>
                <td class='HeaderColumn' style='width: 100px;'>Công ty kiểm toán trên 3 năm chưa kiểm tra</td>
                <td class='HeaderColumn' style='width: 100px;'>Các công ty kiểm toán mà 2 lần kiểm tra liền kề trước đó xếp loại 3, 4</td>
                <td class='HeaderColumn' style='width: 100px;'>Tiêu chí khác</td>
                            </tr>";
        string tempMaDanhSach = "";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string maDanhSach = dt.Rows[i]["MaDanhSach"].ToString();

            if (maDanhSach != tempMaDanhSach)
            {
                tempMaDanhSach = maDanhSach;
                htmlExport += "<tr><td colspan='12' class='GroupTitle'>" + maDanhSach + "</td></tr>";
            }
            htmlExport += "<tr><td class='Column'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoHieuCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenVietTat"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["Email"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SDT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NguoiDDPL_Ten"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NguoiDDPL_Mobile"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["HVTT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["Tren3NamChuaKiemTra"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["XepLoaiKem2LanLienKe"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TieuChiKhac"] + "</td></tr>";
        }
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4'></td></tr>";
        htmlExport += "<tr><td colspan='8'></td><td colspan='4' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("DanhSachCongTyPhaiNopBaoCaoTuKiemTraNam_" + nam + ".xsl", "<html>" + css + htmlExport.Replace("<br>", "\n").Replace("<br />", "\n") + "</html>");
    }

    private void ExportWord(DataTable dt, string nam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }       
        
    </style></head>";
        string htmlExport = @"<p><img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' /></p><br />              
                <p style='font-family: Times New Roman;text-align:center;font-size: 14pt; font-weight: bold;'>DANH SÁCH CÔNG TY KIỂM TOÁN PHẢI NỘP BÁO CÁO TỰ KIỂM TRA NĂM " + nam + @"</p><br />";
        htmlExport += "<table border='1' style='font-family: Times New Roman;' cellpadding='0' cellspacing='0'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn'>STT</td>
                <td class='HeaderColumn' style='width: 70px;'>Số hiệu công ty</td>
                <td class='HeaderColumn' style='width: 170px;'>Tên công ty</td>
                <td class='HeaderColumn' style='width: 70px;'>Tên viết tắt</td>
                <td class='HeaderColumn' style='width: 100px;'>Email</td>
                <td class='HeaderColumn' style='width: 100px;'>SĐT</td>                
                <td class='HeaderColumn' style='width: 100px;'>Họ và tên người đại diện theo pháp luật (hoặc TGĐ, GĐ)</td>
                <td class='HeaderColumn' style='width: 100px;'>Mobile người đại diện theo pháp luật (hoặc TGĐ/GĐ)</td>
                <td class='HeaderColumn' style='width: 100px;'>HVTT</td>
                <td class='HeaderColumn' style='width: 100px;'>Công ty kiểm toán trên 3 năm chưa kiểm tra</td>
                <td class='HeaderColumn' style='width: 100px;'>Các công ty kiểm toán mà 2 lần kiểm tra liền kề trước đó xếp loại 3, 4</td>
                <td class='HeaderColumn' style='width: 100px;'>Tiêu chí khác</td>
                            </tr>";
        string tempMaDanhSach = "";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string maDanhSach = dt.Rows[i]["MaDanhSach"].ToString();

            if (maDanhSach != tempMaDanhSach)
            {
                tempMaDanhSach = maDanhSach;
                htmlExport += "<tr><td colspan='12' class='GroupTitle'>" + maDanhSach + "</td></tr>";
            }
            htmlExport += "<tr><td class='Column'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoHieuCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TenVietTat"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["Email"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SDT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NguoiDDPL_Ten"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NguoiDDPL_Mobile"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["HVTT"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["Tren3NamChuaKiemTra"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["XepLoaiKem2LanLienKe"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["TieuChiKhac"] + "</td></tr>";
        }
        htmlExport += "</table>";
        htmlExport += "<table style='width: 100%;'><tr><td style='width: 50%;'></td><td style='width: 50%;'>";
        htmlExport += "<div style='font-family: Times New Roman; width: 100%;'>";
        htmlExport += "<p style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</p>";
        htmlExport += "<p style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></p>";
        htmlExport += "<br /><br /><br /><br /><br />";
        htmlExport += "<p style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</p>";
        htmlExport += "</div></td></tr></table>";
        Library.ExportToWord("DanhSachCongTyPhaiNopBaoCaoTuKiemTraNam_" + nam + ".xsl", "<html>" + css + htmlExport.Replace("<br>", "\n").Replace("<br />", "\n") + "</html>");
    }
}