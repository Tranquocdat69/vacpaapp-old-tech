﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class usercontrols_BaoCaoDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách công ty kiểm toán trong cả nước (chi tiết)";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            if (this.IsPostBack)
                setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void LoadLoaiHinhDoanhNghiep(string Id = "")
    {
        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT LoaiHinhDoanhNghiepID, TenLoaiHinhDoanhNghiep FROM tblDMLoaiHinhDoanhNghiep";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["LoaiHinhDoanhNghiepID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["LoaiHinhDoanhNghiepID"] + @""">" + dtr["TenLoaiHinhDoanhNghiep"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["LoaiHinhDoanhNghiepID"] + @""">" + dtr["TenLoaiHinhDoanhNghiep"] + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }
    public void LoadVungMien(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT VungMienID, TenVungMien FROM tblDMVungMien";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["VungMienID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["VungMienID"] + @""">" +
                               dtr["TenVungMien"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["VungMienID"] + @""">" + dtr["TenVungMien"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadTinhThanh(string VungMienId = "", string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where TinhID in (" + Id + ")";
        sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where 0 = 0 AND MATINH IS NOT NULL";
        if (VungMienId != "")
            sql.CommandText += " AND VungMienID In (" + VungMienId + ")";
        //if (Id != "")
        //    sql.CommandText += " AND MaTinh In (" + Id + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["MaTinh"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["MaTinh"] + @""">" +
                               dtr["TenTinh"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["MaTinh"] + @""">" + dtr["TenTinh"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);

    }

    public void setdataCrystalReport()
    {
        List<objDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet> lst = new List<objDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet>();
        string LoaiHinhDoanhNghiep = Request.Form["LoaiHinhDoanhNghiep"];
        string VungMien = Request.Form["VungMien"];
        string TinhThanh = Request.Form["TinhThanh"];
        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();

        //Lấy danh sách hội viên tổ chức, vùng miền, số lượng hội viên cá nhân where theo điều kiện lọc
        sql.CommandText = @"select C.*, D.TongSoHoiVien, CASE ISNULL(C.HoiVienTapTheChaID,0) WHEN 0 THEN C.SoHieu ELSE h.SoHieu END AS SoHieuOrder, E.VungMienID from tblHoiVienTapThe as C left join tblHoiVienTapThe h on C.HoiVienTapTheChaID = h.HoiVienTapTheID 
                                join
                                (
                                select A.HoiVienTapTheID, COUNT(B.HoiVienCaNhanID)as TongSoHoiVien from tblHoiVienTapThe A
                                left join tblHoiVienCaNhan B
                                on A.HoiVienTapTheID = B.HoiVienTapTheID
                                group by A.HoiVienTapTheID ) as D
                                on C.HoiVienTapTheID = D.HoiVienTapTheID
                                left join tblDMTinh E
                                on C.DiaChi_TinhID = E.MaTinh
                                left join tblDMVungMien F
                                on F.VungMienID = E.VungMienID
                                where ( 0=0 ";

        //Lọc theo loại hình được chọn với loại hình khác tất cả
        if (!string.IsNullOrEmpty(LoaiHinhDoanhNghiep) && !LoaiHinhDoanhNghiep.Contains("-1"))
            sql.CommandText += " AND (LoaiHinhDoanhNghiepID in (" + LoaiHinhDoanhNghiep + "))";
        //Lọc theo Tỉnh thành được chọn với loại hình khác tất cả
        if (!string.IsNullOrEmpty(TinhThanh) && !TinhThanh.Contains("-1"))
            sql.CommandText += " AND DiaChi_TinhID in (" + TinhThanh + ")";
        else if (TinhThanh == "-1")
        {
            //Lọc theo Vùng miền được chọn với Tỉnh bằng tất cả và vùng miền khác tất cả
            if (!string.IsNullOrEmpty(VungMien) && !VungMien.Contains("-1"))
                sql.CommandText += " AND E.VungMienID in (" + VungMien + ")";
        }
        //Nếu checkall hoặc uncheckall thì ko lọc
        if ((Request.Form["tinhtrang_danghoatdong"] == null && Request.Form["tinhtrang_dunghoatdong"] == null)
            || (Request.Form["tinhtrang_danghoatdong"] != null && Request.Form["tinhtrang_dunghoatdong"] != null))
        {
        }
        else
        {
            //Nếu không check đang hoạt động => lọc bỏ tình trạng đang hoạt động
            if (Request.Form["tinhtrang_danghoatdong"] == null)
            {
                sql.CommandText += " AND (TinhTrangHoiVienId <> 1) ";
            }
            //Nếu không check dừng hoạt động => lọc bỏ tình trạng dừng hoạt động
            if (Request.Form["tinhtrang_dunghoatdong"] == null)
            {
                sql.CommandText += " AND (TinhTrangHoiVienId <> 0) ";
            }
        }

        //Xử lý đối tượng
        //Nếu checkall hoặc uncheckall thì không lọc
        if ((Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] == null &&
             Request.Form["doituong_dudieukienkitcongchungkhac"] == null)
            ||
            (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null &&
             Request.Form["doituong_dudieukienkitcongchungkhac"] != null))
        {
        }
        else
        {
            if (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null)
            {
                sql.CommandText += " AND (NamDuDKKT_CK IS NOT NULL) ";
            }
            else if (Request.Form["doituong_dudieukienkitcongchungkhac"] != null)
            {
                sql.CommandText += " AND (NamDuDKKT_Khac IS NOT NULL) ";
            }
        }

        //Lọc thành viên hãng kiểm toán quốc tế
        if (Request.Form["thanhvienhangkiemtoanquocte"] != null)
        {
            sql.CommandText += " AND (ThanhVienHangKTQT IS NOT NULL) ";
        }

        //Lọc theo vốn điều lệ
        if (Request.Form["vondieuletu"] != null)
        {
            string VonDieuLeTu = strBoChamPhay(Request.Form["vondieuletu"].ToString());
            if (!string.IsNullOrEmpty(VonDieuLeTu))
                sql.CommandText += " AND (VonDieuLe >= " + VonDieuLeTu + ") ";
        }
        if (Request.Form["vondieuleden"] != null)
        {
            string VonDieuLeDen = strBoChamPhay(Request.Form["vondieuleden"].ToString());
            if (!string.IsNullOrEmpty(VonDieuLeDen))
                sql.CommandText += " AND (VonDieuLe <= " + VonDieuLeDen + ") ";
        }
        //Lọc theo số lượng hội viên
        if (Request.Form["soluonghoivientu"] != null)
        {
            string SoLuongHoiVienTu = strBoChamPhay(Request.Form["soluonghoivientu"].ToString());
            if (!string.IsNullOrEmpty(SoLuongHoiVienTu))
                sql.CommandText += " AND (TongSoHoiVien >= " + SoLuongHoiVienTu + ") ";
        }
        if (Request.Form["soluonghoivienden"] != null)
        {
            string SoLuongHoiVienDen = strBoChamPhay(Request.Form["soluonghoivienden"].ToString());
            if (!string.IsNullOrEmpty(SoLuongHoiVienDen))
                sql.CommandText += " AND (TongSoHoiVien <= " + SoLuongHoiVienDen + ") ";
        }
        //Lọc theo doanh thu
        if (Request.Form["doanhthutu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["doanhthutu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (TongDoanhThu >= " + Tu + ") ";
        }
        if (Request.Form["doanhthuden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["doanhthuden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (TongDoanhThu <= " + Den + ") ";
        }
        //Lọc theo số lượng kiểm toán viên
        if (Request.Form["soluongkiemtoanvientu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["soluongkiemtoanvientu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (TongSoKTVDKHN >= " + Tu + ") ";
        }
        if (Request.Form["soluongkiemtoanvienden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["soluongkiemtoanvienden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (TongSoKTVDKHN <= " + Den + ") ";
        }
        //Lọc theo số người lao động
        if (Request.Form["soluongnguoilaodongtu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["soluongnguoilaodongtu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (TongSoNguoiLamViec >= " + Tu + ") ";
        }
        if (Request.Form["soluongnguoilaodongden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["soluongnguoilaodongden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (TongSoNguoiLamViec <= " + Den + ") ";
        }
        //Lọc theo số lượng khách hàng
        if (Request.Form["soluongkhachhangtu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["soluongkhachhangtu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (TongSoKHTrongNam >= " + Tu + ") ";
        }
        if (Request.Form["soluongkhachhangden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["soluongkhachhangden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (TongSoKHTrongNam <= " + Den + ") ";
        }
        //Lọc theo Ngày thành lập
        if (Request.Form["ngaythanhlaptu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["ngaythanhlaptu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (NgayThanhLap >= '" +
                                   (DateTime.ParseExact(Request.Form["ngaythanhlaptu"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }
        if (Request.Form["ngaythanhlapden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["ngaythanhlapden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (NgayThanhLap <= '" +
                                   (DateTime.ParseExact(Request.Form["ngaythanhlapden"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }

        sql.CommandText += ") order by SoHieuOrder, HoiVienTapTheID";
        ds = DataAccess.RunCMDGetDataSet(sql);


        DataTable dt = ds.Tables[0];
        int i = 0;
        foreach (DataRow tem in dt.Rows)
        {
            i++;
            objDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet obj = new objDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet();
            obj.STT = i.ToString();
            obj.TenIDHVTT_IDCTKT = tem["MaHoiVienTapThe"].ToString();
            obj.SoHieuCongTy = tem["SoHieu"].ToString();
            obj.TenDoanhNghiep = tem["TenDoanhNghiep"].ToString();
            obj.TenVietTat = tem["TenVietTat"].ToString();
            obj.TenTiengAnh = tem["TenTiengAnh"].ToString();

            //Chưa có trong DB
            //string strSoQuyetDinhKetNapHVTT = "";
            //string strNgayQuyetDinhKetNapHVTT = "";
            //if (tem["HoiVienTapTheID"] != null)
            //{
            //    sql.CommandText = "Select * from tblDonHoiVienTapThe where HoiVienTapTheID =" +
            //                      tem["HoiVienTapTheID"].ToString();
            //    ds = DataAccess.RunCMDGetDataSet(sql);
            //    if (ds.Tables[0].Rows.Count > 0)
            //    {
            //        strSoQuyetDinhKetNapHVTT =
            //            ds.Tables[0].Rows[0]["SoDonHoiVienTapThe"].ToString();
            //        try
            //        {
            //            strNgayQuyetDinhKetNapHVTT =
            //                Convert.ToDateTime(ds.Tables[0].Rows[0]["NgayDuyet"]).ToShortDateString();
            //        }
            //        catch (Exception)
            //        {
            //        }
            //    }
            //}
            //obj.SoQuyetDinhKetNapHVTT = strSoQuyetDinhKetNapHVTT;
            //obj.NgayKyQuyetDinhKetNapHVTT = strNgayQuyetDinhKetNapHVTT;

            string strLoaiHinhDoanhNghiep = "";
            if (!string.IsNullOrEmpty(tem["LoaiHinhDoanhNghiepID"].ToString()))
            {
                sql.CommandText = "Select * from tblDMLoaiHinhDoanhNghiep where LoaiHinhDoanhNghiepID = " +
                                  tem["LoaiHinhDoanhNghiepID"].ToString();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (ds.Tables[0].Rows.Count > 0)
                    strLoaiHinhDoanhNghiep =
                        ds.Tables[0].Rows[0]["TenLoaiHinhDoanhNghiep"].ToString();
            }
            obj.LoaiHinhDoanhNghiep_THHH_HD_DNTN = strLoaiHinhDoanhNghiep;

            if (tem["DNTrongNuoc"] != null)
                if (tem["DNTrongNuoc"].ToString() == "1")
                    obj.LoaiHinhDoanhNghiepTrongNuoc = "x";
                else if (tem["DNTrongNuoc"].ToString() == "2")
                    obj.LoaiHinhDoanhNghiepCoVonDTNN = "x";

            obj.VonDieuLe = tem["VonDieuLe"].ToString();
            obj.TongDoanhThu = tem["TongDoanhThu"].ToString();
            obj.DoanhThuDichVuKiemToan = tem["DoanhThuDichVuKT"].ToString();
            obj.TongSoKhachHangTrongNam = tem["TongSoKHTrongNam"].ToString();
            obj.SoLuongKHLaDonViCoLoiIchCongChungThuocLinhVucChungKhoan = tem["TongSoKHChungKhoan"].ToString();
            obj.DiaChiTruSoChinh = tem["DiaChi"].ToString();
            try
            {
                obj.NgayThanhLap = Convert.ToDateTime(tem["NgayThanhLap"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }
            
            obj.DTCD = tem["DienThoai"].ToString();
            obj.Fax = tem["Fax"].ToString();
            obj.Email = tem["Email"].ToString();
            obj.Website = tem["Website"].ToString();
            obj.SoGiayCNDangKyKD_GiayCNDauTu = tem["SoGiayChungNhanDKKD"].ToString();
            try
            {
                obj.NgayCapGiayCNDangKyKD_GiayCNDauTu = Convert.ToDateTime(tem["ngayCapGiayChungNhanDKKD"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }
            
            //obj.SoGiayChungNhanDuDKKDDichVuKiT = tem[""].ToString();
            //obj.NgayCapGiayChungNhanDuDKKDDichVuKiT = tem[""].ToString();
            obj.MaSoThue = tem["MaSoThue"].ToString();

            string strLinhVucHoatDong = "";
            if (!string.IsNullOrEmpty(tem["LinhVucHoatDongID"].ToString()))
            {
                sql.CommandText = "Select * from tblDMLinhVucHoatDong where LinhVucHoatDongID =" +
                                  tem["LinhVucHoatDongID"].ToString();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (ds.Tables[0].Rows.Count > 0)
                    strLinhVucHoatDong =
                        ds.Tables[0].Rows[0]["LinhVucHoatDongID"].ToString();
            }

            obj.LinhVucHoatDongChinh = strLinhVucHoatDong;
            try
            {
                if (tem["NamDuDKKT_CK"] != null)
                    if (Convert.ToDateTime(tem["NamDuDKKT_CK"]) <= DateTime.Now)
                        obj.DonViDuDKKitTrongLinhVucChungKhoan = "x";
            }
            catch (Exception)
            {
            }
            try
            {
                if (tem["NamDuDKKT_Khac"] != null)
                    if (Convert.ToDateTime(tem["NamDuDKKT_Khac"]) <= DateTime.Now)
                        obj.DonViDuDKKiTCongChungKhacNam = "x";
            }
            catch (Exception)
            {
            }
            if (tem["NamDuDKKT_CK"] == null && tem["NamDuDKKT_Khac"] == null)
                obj.CongTyDuDKKiemToanDonvi = "x";

            obj.CongTyLaThanhVienHangKiemToanQuocTe = tem["ThanhVienHangKTQT"].ToString();
            obj.TongSoNguoiLamVieccTaiDN = tem["TongSoNguoiLamViec"].ToString();
            obj.TongSoNguoiCoChungChiKTV = tem["TongSoNguoiCoCKKTV"].ToString();
            obj.TongSoKTVDangKyHanhNgheKiemToan = tem["TongSoKTVDKHN"].ToString();
            obj.TongSoHoiVienCaNhan = tem["TongSoHoiVien"].ToString();
            obj.NDDPL_HoVaTen = tem["NguoiDaiDienPL_Ten"].ToString();

            try
            {
                obj.NDDPL_NgaySinh = Convert.ToDateTime(tem["NguoiDaiDienPL_NgaySinh"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {}
            string strChucVuDDPL = "";
            if (!string.IsNullOrEmpty(tem["NguoiDaiDienPL_ChucVuID"].ToString()))
            {
                sql.CommandText = "Select * from tblDMChucVu where ChucVuID =" +
                                  tem["NguoiDaiDienPL_ChucVuID"].ToString();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (ds.Tables[0].Rows.Count > 0)
                    strChucVuDDPL =
                        ds.Tables[0].Rows[0]["TenChucVu"].ToString();
            }
            obj.NDDPL_ChucVu = strChucVuDDPL;
            obj.NDDPL_GioiTinh = tem["NguoiDaiDienPL_GioiTinh"].ToString();
            obj.NDDPL_CCKTV = tem["NguoiDaiDienPL_SoChungChiKTV"].ToString();

            try
            {
                obj.NDDPL_NgayCapCCKTV = Convert.ToDateTime(tem["NguoiDaiDienPL_NgayCapChungChiKTV"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            //Luôn luôn là BTC
            obj.NDDPL_NoiCap = "BTC";

            string strNDDPL_DienThoaiCoDinhDiDong = "";
            if (!string.IsNullOrEmpty(tem["NguoiDaiDienPL_DienThoaiCD"].ToString()))
                strNDDPL_DienThoaiCoDinhDiDong = tem["NguoiDaiDienPL_DienThoaiCD"].ToString();
            if (!string.IsNullOrEmpty(tem["NguoiDaiDienPL_DiDong"].ToString()))
                if (!string.IsNullOrEmpty(tem["NguoiDaiDienPL_DienThoaiCD"].ToString()))
                    strNDDPL_DienThoaiCoDinhDiDong += "/" + tem["NguoiDaiDienPL_DiDong"].ToString();
                else
                    strNDDPL_DienThoaiCoDinhDiDong = tem["NguoiDaiDienPL_DiDong"].ToString();


            obj.NDDPL_DienThoaiCoDinh_DiDong = strNDDPL_DienThoaiCoDinhDiDong;
            obj.NDDPL_Email = tem["NguoiDaiDienPL_Email"].ToString();
            obj.NDDLL_HoVaTen = tem["NguoiDaiDienLL_Ten"].ToString();

            try
            {
                obj.NDDLL_NgaySinh = Convert.ToDateTime(tem["NguoiDaiDienLL_NgaySinh"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            string strChucVuDDLL = "";
            if (!string.IsNullOrEmpty(tem["NguoiDaiDienLL_ChucVuID"].ToString()))
            {
                sql.CommandText = "Select * from tblDMChucVu where ChucVuID =" +
                                  tem["NguoiDaiDienLL_ChucVuID"].ToString();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (ds.Tables[0].Rows.Count > 0)
                    strChucVuDDLL =
                        ds.Tables[0].Rows[0]["TenChucVu"].ToString();
            }
            obj.NDDLL_ChucVu = strChucVuDDLL;
            obj.NDDLL_GioiTinh = tem["NguoiDaiDienLL_GioiTinh"].ToString();


            string strNDDLL_DienThoaiCoDinhDiDong = "";
            if (!string.IsNullOrEmpty(tem["NguoiDaiDienLL_DienThoaiCD"].ToString()))
                strNDDLL_DienThoaiCoDinhDiDong = tem["NguoiDaiDienLL_DienThoaiCD"].ToString();
            if (!string.IsNullOrEmpty(tem["NguoiDaiDienLL_DiDong"].ToString()))
                if (!string.IsNullOrEmpty(tem["NguoiDaiDienLL_DienThoaiCD"].ToString()))
                    strNDDLL_DienThoaiCoDinhDiDong += "/" + tem["NguoiDaiDienLL_DiDong"].ToString();
                else
                    strNDDLL_DienThoaiCoDinhDiDong = tem["NguoiDaiDienLL_DiDong"].ToString();
            obj.NDDLL_DienThoaiCoDinh_DiDong = strNDDLL_DienThoaiCoDinhDiDong;
            obj.NDDLL_Email = tem["NguoiDaiDienLL_Email"].ToString();

            string strTenChiNhanh = "";
            string strDiaChiChiNhanh = "";
            string strHoVaTenGDChiNhanh = "";
            string strMobileGDChiNhanh = "";
            string strDienThoaiCoDinh_DiDongChiNhanh = "";
            string strEmailChiNhanh = "";
            string strEmailGiamDocChiNhanh = "";
            DataTable dtbtemp = new DataTable();
            if (!string.IsNullOrEmpty(tem["HoiVienTapTheID"].ToString()))
            {
                sql.CommandText = "Select * from tblHoiVienTapTheChiNhanh where HoiVienTapTheID = " +
                                  tem["HoiVienTapTheID"];
                dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtbtemp.Rows.Count > 0)
                {
                    int j = 0;
                    foreach (DataRow dtr in dtbtemp.Rows)
                    {
                        j++;
                        strTenChiNhanh += j + ". " + dtr["Ten"].ToString() + "\r\n";
                        strDiaChiChiNhanh += j + ". " + dtr["DiaChi"].ToString() + "\r\n";
                        strHoVaTenGDChiNhanh += j + ". " + dtr["DaiDienPL_Ten"].ToString() + "\r\n";
                        strMobileGDChiNhanh += j + ". " + dtr["DaiDienPL_Mobile"].ToString() + "\r\n";
                        strDienThoaiCoDinh_DiDongChiNhanh += j + ". " + dtr["DienThoai"].ToString() + "\r\n";
                        strEmailChiNhanh += j + ". " + dtr["EmailNguoiLH"].ToString() + "\r\n";
                        strEmailGiamDocChiNhanh += j + ". " + dtr["DaiDienPL_Email"].ToString() + "\r\n";
                    }
                }

                obj.TTLLCN_TenChiNhanh = strTenChiNhanh;
                obj.TTLLCN_DiaChiChiNhanh = strDiaChiChiNhanh;
                obj.TTLLCN_HoVaTenGiamDocChiNhanh = strHoVaTenGDChiNhanh;
                obj.TTLLCN_MobileGD = strMobileGDChiNhanh;
                obj.TTLLCN_DienThoaiCoDinh_DiDong = strDienThoaiCoDinh_DiDongChiNhanh;
                obj.TTLLCN_EmailChiNhanh = strEmailChiNhanh;
                obj.TTLLCN_EmailGiamDocChiNhanh = strEmailGiamDocChiNhanh;


                //Chưa có trong DB
                //string strTenVPDD= "";
                //string strDiaChiVPDD = "";
                //string strHoVaTenGDVPDD = "";
                //string strMobileGDVPDD = "";
                //string strDienThoaiCoDinh_DiDongVPDD = "";
                //string strEmailVPDD = "";
                //string strEmailGiamDocVPDD = "";
                //sql.CommandText = "Select * from tblHoiVienTapTheVPDD where HoiVienTapTheID = " +
                //                  tem["HoiVienTapTheID"];
                //dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                //if (dtbtemp.Rows.Count > 0)
                //{
                //    int j = 0;
                //    foreach (DataRow dtr in dtbtemp.Rows)
                //    {
                //        j++;
                //        strTenVPDD += j + ". " + dtr["Ten"].ToString() + "\r\n";
                //        strDiaChiVPDD += j + ". " + dtr["DiaChi"].ToString() + "\r\n";
                //        strHoVaTenGDVPDD += j + ". " + dtr["DaiDienPL_Ten"].ToString() + "\r\n";
                //        strMobileGDVPDD += j + ". " + dtr["DaiDienPL_Mobile"].ToString() + "\r\n";
                //        strDienThoaiCoDinh_DiDongVPDD += j + ". " + dtr["DienThoai"].ToString() + "\r\n";
                //        strEmailVPDD += j + ". " + dtr["EmailNguoiLH"].ToString() + "\r\n";
                //        strEmailGiamDocVPDD += j + ". " + dtr["DaiDienPL_Email"].ToString() + "\r\n";
                //    }
                //}

                ////obj.TTLLVPDD_TenVPDD = tem[""].ToString();
                ////obj.TTLLVPDD_DiaChiVPDD = tem[""].ToString();
                ////obj.TTLLVPDD_DienThoaiCoDinh_DiDong = tem[""].ToString();
                ////obj.TTLLVPDD_EmailGDVPDD = tem[""].ToString();
                ////obj.TTLLVPDD_HoVaTenGiamDocVPDD = tem[""].ToString();
                ////obj.TTLLVPDD_MobileGD = tem[""].ToString();
                ////obj.TTLLVPDD_EmailGDVPDD = tem[""].ToString();

                string strTongSoBaoCao1 = "";

                sql.CommandText = "Select * from tblKSCLDSBaoCaoTuKiemTraChiTiet where HoiVienTapTheID = " +
                                  tem["HoiVienTapTheID"] + " AND datepart(yyyy,GETDATE()) = " + DateTime.Now.Year;
                dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtbtemp.Rows.Count > 0)
                {
                    strTongSoBaoCao1 = dtbtemp.Rows[0]["SoBaoCaoKiemToanPH"].ToString();
                }

                obj.KQKSCL_TongSobaoCaoKiemToanDaKyTrongNam = strTongSoBaoCao1;

                //string strTongSoBaoCao2 = "";

                //sql.CommandText =
                //    "select count(HoSoID) as SoLuong from tblKSCLBaoCaoKTKT where HoSoID in( Select HoSoID from tblKSCLHoSo where HoiVienTapTheID = " +
                //    tem["HoiVienTapTheID"] + " and (DATEPART(YYYY, DenNgayThucTe) = " + DateTime.Now.Year + ")) AND DonViChungKhoan = 1";
                //dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                //if (dtbtemp.Rows.Count > 0)
                //{
                //    strTongSoBaoCao2 = dtbtemp.Rows[0]["SoLuong"].ToString();

                //}
                //obj.KQKSCL_SoLuongBaoCaoKiemToanChoDonViCoLoiIchCongChungTrongLinhVucChungKhoan = strTongSoBaoCao2;

                string strTongSoBaoCao3 = "";
                sql.CommandText =
                    "select count(HoSoID) as SoLuong from tblKSCLBaoCaoKTKT where HoSoID in( Select HoSoID from tblKSCLHoSo where HoiVienTapTheID = " +
                    tem["HoiVienTapTheID"] + " and (DATEPART(YYYY, DenNgayThucTe) = " + DateTime.Now.Year + "))";
                dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtbtemp.Rows.Count > 0)
                {
                    strTongSoBaoCao3 = dtbtemp.Rows[0]["SoLuong"].ToString();

                }
                obj.KQKSCL_SoLuongBaoCaoKiemToanDuocKiemTra = strTongSoBaoCao3;

                string strKhenThuongNgayThang = "";
                string strHinhThucKhenThuong = "";
                string strLyDoKhenThuong = "";
                sql.CommandText = @"select A.*, B.TenHinhThucKhenThuong from tblKhenThuongKyLuat A
left join tblDMHinhThucKhenThuong B on A.HinhThucKhenThuongID = B.HinhThucKhenThuongID
 where Loai = 1 and HoiVienTapTheID = " + tem["HoiVienTapTheID"].ToString();

                dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtbtemp.Rows.Count > 0)
                {
                    int j = 0;
                    foreach (DataRow dtr in dtbtemp.Rows)
                    {
                        j++;
                        try
                        {
                            strKhenThuongNgayThang += j + ". " +
                                                      Convert.ToDateTime(dtbtemp.Rows[0]["NgayThang"]).ToString(
                                                          "dd/MM/yyyy") + "\r\n";
                        }
                        catch (Exception)
                        {
                        }
                        strHinhThucKhenThuong += j + ". " + dtbtemp.Rows[0]["TenHinhThucKhenThuong"].ToString() + "\r\n";
                        strLyDoKhenThuong += j + ". " + dtbtemp.Rows[0]["LyDo"].ToString() + "\r\n";
                    }
                }

                obj.KhenThuong_NgayThang = strKhenThuongNgayThang;
                obj.KhenThuong_CapVaHinhThucKhenThuongKyLuat = strHinhThucKhenThuong;
                obj.KhenThuong_LyDo = strLyDoKhenThuong;

                string strKyLuatNgayThang = "";
                string strHinhThucKyLuat = "";
                string strLyDoKyLuat = "";
                sql.CommandText = @"select A.*, B.TenHinhThucKyLuat from tblKhenThuongKyLuat A
left join tblDMHinhThucKyLuat B on A.HinhThucKyLuatID = B.HinhThucKyLuatID
 where Loai = 2 and HoiVienTapTheID = " + tem["HoiVienTapTheID"].ToString();

                dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtbtemp.Rows.Count > 0)
                {
                    int j = 0;
                    foreach (DataRow dtr in dtbtemp.Rows)
                    {
                        j++;
                        try
                        {
                            strKyLuatNgayThang += j + ". " +
                                                  Convert.ToDateTime(dtbtemp.Rows[0]["NgayThang"]).ToString("dd/MM/yyyy") +
                                                  "\r\n";
                        }
                        catch (Exception)
                        {
                        }
                        strHinhThucKyLuat += j + ". " + dtbtemp.Rows[0]["TenHinhThucKyLuat"].ToString() + "\r\n";
                        strLyDoKyLuat += j + ". " + dtbtemp.Rows[0]["LyDo"].ToString() + "\r\n";
                    }
                }


                obj.KyLuat_NgayThang = strKyLuatNgayThang;
                obj.KyLuat_CapVaHinhThucKhenThuongKyLuat = strHinhThucKyLuat;
                obj.KyLuat_LyDo = strLyDoKyLuat;
            }
            lst.Add(obj);

        }

        ReportDocument rpt = new ReportDocument();
        rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
        rpt.Load(Server.MapPath("Report/QuanLyThongTinChung/DanhSachCongTyKiemToanTrongCaNuoc_ChiTiet.rpt"));

        //rpt.SetDataSource(lst);
        rpt.Database.Tables["objDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet"].SetDataSource(lst);
        //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

        CrystalReportViewer1.ReportSource = rpt;
        CrystalReportViewer1.DataBind();

        if (Library.CheckNull(Request.Form["hdAction"]) == "word")
           // rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachCongTyKiemToanTrongCaNuoc_ChiTiet");
            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachCongTyKiemToanTrongCaNuoc_ChiTiet");
        
        if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
        {
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachCongTyKiemToanTrongCaNuoc_ChiTiet");
            //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //xlsFormatOptions.ShowGridLines = true;
            //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

        }
        if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
        {
            string strNgayTinh = "(Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            string strNgayBaoCao = DateTime.Now.ToString("dd/MM/yyyy");
            ExportToExcel("DanhSachCongTyKiemToanTrongCaNuoc_ChiTiet", lst, strNgayTinh, strNgayBaoCao);
        }
    }

    private void ExportToExcel(string strName, List<objDanhSachCongTyKiemToanTrongCaNuoc_ChiTiet> lst, string NgayTinh, string NgayBC)
    {
        string outHTML = "";

        outHTML += "<table border = '1' cellpadding='5' cellspacing='0' style='font-family:Arial; font-size:8pt; color:#003366'>";
        //outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        //outHTML += "<td colspan='2' style='border-style:none'></td>";
        //outHTML += "<td colspan='2' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/logo.png' alt='' border='0px'/></td>";
        //outHTML += "<td colspan='70' style='border-style:none'></td>";

        //outHTML += "</tr>";
        outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        outHTML += "<td colspan='8' style='border-style:none'></td>";
        outHTML += "<td colspan='66' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/ten.png' alt='' border='0px'/></td>";

        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "</tr>";

        outHTML += "<td colspan='9' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH CÔNG TY KIỂM TOÁN TRONG CẢ NƯỚC</b></td>";
        outHTML += "<td colspan='65'></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='9' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i>" + NgayTinh + "</i></td>";
        outHTML += "<td colspan='65'></td>";
        outHTML += "</tr>";
        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "    <th rowspan = '2' width='30px' height ='150'>STT</th>";
        outHTML += "    <th rowspan = '2'>Tên ID.HVTT, ID.CTKT</th>";
        outHTML += "    <th rowspan = '2'>Số hiệu công ty</th>";
        outHTML += "    <th rowspan = '2'>Tên Doanh nghiệp</th>";
        outHTML += "    <th rowspan = '2'>Tên viết tắt</th>";
        outHTML += "    <th rowspan = '2'>Tên tiếng Anh</th>";
        outHTML += "    <th rowspan = '2'>Số quyết định kết nạp HVTC</th>";
        outHTML += "    <th rowspan = '2'>Ngày ký Quyết định kết nạp HVTC</th>";
        outHTML += "    <th rowspan = '2'>Loại hình doanh nghiệp (TNHH, HD, DNTN) </th>";
        outHTML += "    <th rowspan = '2'>Loại hình doanh nghiệp trong nước</th>";
        outHTML += "    <th rowspan = '2'>Loại hình doanh nghiệp có vốn ĐTNN</th>";
        outHTML += "    <th colspan = '5'>Thông tin về tình hình hoạt động công ty</th>";
        outHTML += "    <th colspan = '6'>Thông tin liên lạc công ty</th>";
        outHTML += "    <th rowspan = '2'>Số giấy CN đăng ký kinh doanh/Giấy chứng nhận đầu tư</th>";
        outHTML += "    <th rowspan = '2'>Ngày cấp Giấy CN đăng ký kinh doanh/Giấy chứng nhận đầu tư</th>";
        outHTML += "    <th rowspan = '2'>Số Giấy CN đủ đk kinh doanh dịch vụ kiểm toán</th>";
        outHTML += "    <th rowspan = '2'>Ngày cấp Giấy CN đủ đk kinh doanh dịch vụ kiểm toán</th>";
        outHTML += "    <th rowspan = '2'>Mã số thuế</th>";
        outHTML += "    <th rowspan = '2'>Lĩnh vực hoạt động chính</th>";
        outHTML += "    <th rowspan = '2'>Đơn vị đủ ĐK KiT Trong lĩnh vực chứng khoán năm</th>";
        outHTML += "    <th rowspan = '2'>Đơn vị đủ ĐK KiT công chúng khác năm</th>";
        outHTML += "    <th rowspan = '2'>Công ty đủ điều kiện kiểm toán đơn vị</th>";
        outHTML += "    <th rowspan = '2'>Công ty là thành viên hãng kiểm toán quốc tế</th>";
        outHTML += "    <th rowspan = '2'>Tổng số người làm việc tại doanh nghiệp</th>";
        outHTML += "    <th rowspan = '2'>Tổng số người có chứng chỉ KTV</th>";
        outHTML += "    <th rowspan = '2'>Tổng số KTV đăng ký hành nghề kiểm toán</th>";
        outHTML += "    <th rowspan = '2'>Tổng số hội viên cá nhân</th>";
        outHTML += "    <th colspan = '9'>Người đại diện pháp luật</th>";
        outHTML += "    <th colspan = '6'>Người đại diện liên lạc</th>";
        outHTML += "    <th colspan = '7'>Thông tin liên lạc Chi nhánh</th>";
        outHTML += "    <th colspan = '7'>Thông tin liên lạc Văn phòng đại diện</th>";
        outHTML += "    <th colspan = '3'>Kết quả kiểm soát chất lượng</th>";
        outHTML += "    <th colspan = '3'>Khen thưởng</th>";
        outHTML += "    <th colspan = '3'>Kỷ luật</th>";

        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th>Vốn điều lệ</th>";
        outHTML += "    <th>Tổng doanh thu</th>";
        outHTML += "    <th>Doanh thu dịch vụ kiểm toán</th>";
        outHTML += "    <th>Tổng số khách hàng trong năm</th>";
        outHTML += "    <th>Số lượng KH là đơn vị có lợi ích công chúng thuộc lĩnh vực chứng khoán</th>";
        outHTML += "    <th>Địa chỉ trụ sở chính</th>";
        outHTML += "    <th>Ngày thành lập</th>";
        outHTML += "    <th>ĐTCĐ</th>";
        outHTML += "    <th>Fax</th>";
        outHTML += "    <th>Email</th>";
        outHTML += "    <th>Website</th>";
        outHTML += "    <th>Họ và tên</th>";
        outHTML += "    <th>Ngày sinh</th>";
        outHTML += "    <th>Chức vụ</th>";
        outHTML += "    <th>Giới tính</th>";
        outHTML += "    <th>CCKTV</th>";
        outHTML += "    <th>Ngày cấp CCKTV</th>";
        outHTML += "    <th>Nơi cấp</th>";
        outHTML += "    <th>Điện thoại cố định/ Di động</th>";
        outHTML += "    <th>Email</th>";
        outHTML += "    <th>Họ và tên</th>";
        outHTML += "    <th>Ngày sinh</th>";
        outHTML += "    <th>Chức vụ</th>";
        outHTML += "    <th>Giới tính</th>";
        outHTML += "    <th>Điện thoại cố định/Di động</th>";
        outHTML += "    <th>Email</th>";
        outHTML += "    <th>Tên chi nhánh</th>";
        outHTML += "    <th>Địa chỉ chi nhánh</th>";
        outHTML += "    <th>Điện thoại cố định/ Di động</th>";
        outHTML += "    <th>Email chi nhánh</th>";
        outHTML += "    <th>Họ và tên giám đốc chi nhánh</th>";
        outHTML += "    <th>Mobile GĐ</th>";
        outHTML += "    <th>Email giám đốc chi nhánh</th>";
        outHTML += "    <th>Tên văn phòng đại diện</th>";
        outHTML += "    <th>Địa chỉ văn phòng đại diện</th>";
        outHTML += "    <th>Điện thoại cố định/ Di động</th>";
        outHTML += "    <th>Email văn phòng đại diện</th>";
        outHTML += "    <th>Họ và tên giám đốc văn phòng đại diện</th>";
        outHTML += "    <th>Mobile GĐ</th>";
        outHTML += "    <th>Email giám đốc văn phòng đại diện</th>";
        outHTML += "    <th>Tổng số báo cáo kiểm toán đã ký trong năm</th>";
        outHTML += "    <th>Số lượng báo cáo kiểm toán cho đơn vị có lợi ích công chúng trong lĩnh vực chứng khoán</th>";
        outHTML += "    <th>Số lượng báo cáo kiểm toán được kiểm tra</th>";
        outHTML += "    <th>Ngày tháng</th>";
        outHTML += "    <th>Cấp và hình thức khen thưởng, kỷ luật</th>";
        outHTML += "    <th>Lý do</th>";
        outHTML += "    <th>Ngày tháng</th>";
        outHTML += "    <th>Cấp và hình thức khen thưởng, kỷ luật</th>";
        outHTML += "    <th>Lý do</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th width='30px'>1</th>";
        outHTML += "    <th width='300px'>2</th>";
        outHTML += "    <th width='80px'>3</th>";
        outHTML += "    <th width='80px'>4</th>";
        outHTML += "    <th width='150px'>5</th>";
        outHTML += "    <th width='200px'>6</th>";
        outHTML += "    <th width='200px'>7</th>";
        outHTML += "    <th width='200px'>8</th>";
        outHTML += "    <th width='200px'>9</th>";
        outHTML += "    <th width='200px'>10</th>";
        outHTML += "    <th width='200px'>11</th>";
        outHTML += "    <th width='200px'>12</th>";
        outHTML += "    <th width='200px'>13</th>";
        outHTML += "    <th width='200px'>14</th>";
        outHTML += "    <th width='200px'>15</th>";
        outHTML += "    <th width='200px'>16</th>";
        outHTML += "    <th width='200px'>17</th>";
        outHTML += "    <th width='200px'>18</th>";
        outHTML += "    <th width='200px'>19</th>";
        outHTML += "    <th width='200px'>20</th>";
        outHTML += "    <th width='200px'>21</th>";
        outHTML += "    <th width='200px'>22</th>";
        outHTML += "    <th width='200px'>23</th>";
        outHTML += "    <th width='200px'>24</th>";
        outHTML += "    <th width='200px'>25</th>";
        outHTML += "    <th width='200px'>26</th>";
        outHTML += "    <th width='200px'>27</th>";
        outHTML += "    <th width='200px'>28</th>";
        outHTML += "    <th width='200px'>29</th>";
        outHTML += "    <th width='200px'>30</th>";
        outHTML += "    <th width='200px'>31</th>";
        outHTML += "    <th width='200px'>32</th>";
        outHTML += "    <th width='200px'>33</th>";
        outHTML += "    <th width='200px'>34</th>";
        outHTML += "    <th width='200px'>35</th>";
        outHTML += "    <th width='200px'>36</th>";
        outHTML += "    <th width='200px'>37</th>";
        outHTML += "    <th width='200px'>38</th>";
        outHTML += "    <th width='200px'>39</th>";
        outHTML += "    <th width='200px'>40</th>";
        outHTML += "    <th width='200px'>41</th>";
        outHTML += "    <th width='200px'>42</th>";
        outHTML += "    <th width='200px'>43</th>";
        outHTML += "    <th width='200px'>44</th>";
        outHTML += "    <th width='200px'>45</th>";
        outHTML += "    <th width='200px'>46</th>";
        outHTML += "    <th width='200px'>47</th>";
        outHTML += "    <th width='200px'>48</th>";
        outHTML += "    <th width='200px'>49</th>";
        outHTML += "    <th width='200px'>50</th>";
        outHTML += "    <th width='200px'>51</th>";
        outHTML += "    <th width='200px'>52</th>";
        outHTML += "    <th width='200px'>53</th>";
        outHTML += "    <th width='200px'>54</th>";
        outHTML += "    <th width='200px'>55</th>";
        outHTML += "    <th width='200px'>56</th>";
        outHTML += "    <th width='200px'>57</th>";
        outHTML += "    <th width='200px'>58</th>";
        outHTML += "    <th width='200px'>59</th>";
        outHTML += "    <th width='200px'>60</th>";
        outHTML += "    <th width='200px'>61</th>";
        outHTML += "    <th width='200px'>62</th>";
        outHTML += "    <th width='200px'>63</th>";
        outHTML += "    <th width='200px'>64</th>";
        outHTML += "    <th width='200px'>65</th>";
        outHTML += "    <th width='200px'>66</th>";
        outHTML += "    <th width='200px'>67</th>";
        outHTML += "    <th width='200px'>68</th>";
        outHTML += "    <th width='200px'>69</th>";
        outHTML += "    <th width='200px'>70</th>";
        outHTML += "    <th width='200px'>71</th>";
        outHTML += "    <th width='200px'>72</th>";
        outHTML += "    <th width='200px'>73</th>";
        outHTML += "    <th width='200px'>74</th>";
        outHTML += "</tr>";

        int i = 0;
        foreach (var temp in lst)
        {
            i++;
            outHTML += "<tr>";
            outHTML += "<td>" + i.ToString() + "</td>";
            outHTML += "<td>" + temp.TenIDHVTT_IDCTKT + "</td>";
            outHTML += "<td>" + temp.SoHieuCongTy + "</td>";
            outHTML += "<td>" + temp.TenDoanhNghiep + "</td>";
            outHTML += "<td>" + temp.TenVietTat + "</td>";
            outHTML += "<td>" + temp.TenTiengAnh + "</td>";
            outHTML += "<td>" + temp.SoQuyetDinhKetNapHVTT + "</td>";
            outHTML += "<td>" + temp.NgayKyQuyetDinhKetNapHVTT + "</td>";
            outHTML += "<td>" + temp.LoaiHinhDoanhNghiep_THHH_HD_DNTN + "</td>";
            outHTML += "<td>" + temp.LoaiHinhDoanhNghiepTrongNuoc + "</td>";
            outHTML += "<td>" + temp.LoaiHinhDoanhNghiepCoVonDTNN + "</td>";
            outHTML += "<td>" + temp.VonDieuLe + "</td>";
            outHTML += "<td>" + temp.TongDoanhThu + "</td>";
            outHTML += "<td>" + temp.DoanhThuDichVuKiemToan + "</td>";
            outHTML += "<td>" + temp.TongSoKhachHangTrongNam + "</td>";
            outHTML += "<td>" + temp.SoLuongKHLaDonViCoLoiIchCongChungThuocLinhVucChungKhoan + "</td>";
            outHTML += "<td>" + temp.DiaChiTruSoChinh + "</td>";
            outHTML += "<td>" + temp.NgayThanhLap + "</td>";
            outHTML += "<td>" + temp.DTCD + "</td>";
            outHTML += "<td>" + temp.Fax + "</td>";
            outHTML += "<td>" + temp.Email + "</td>";
            outHTML += "<td>" + temp.Website + "</td>";
            outHTML += "<td>" + temp.SoGiayCNDangKyKD_GiayCNDauTu + "</td>";
            outHTML += "<td>" + temp.NgayCapGiayCNDangKyKD_GiayCNDauTu + "</td>";
            outHTML += "<td>" + temp.SoGiayChungNhanDuDKKDDichVuKiT + "</td>";
            outHTML += "<td>" + temp.NgayCapGiayChungNhanDuDKKDDichVuKiT + "</td>";
            outHTML += "<td>" + temp.MaSoThue + "</td>";
            outHTML += "<td>" + temp.LinhVucHoatDongChinh + "</td>";
            outHTML += "<td>" + temp.DonViDuDKKitTrongLinhVucChungKhoan + "</td>";
            outHTML += "<td>" + temp.DonViDuDKKiTCongChungKhacNam + "</td>";
            outHTML += "<td>" + temp.CongTyDuDKKiemToanDonvi + "</td>";
            outHTML += "<td>" + temp.CongTyLaThanhVienHangKiemToanQuocTe + "</td>";
            outHTML += "<td>" + temp.TongSoNguoiLamVieccTaiDN + "</td>";
            outHTML += "<td>" + temp.TongSoNguoiCoChungChiKTV + "</td>";
            outHTML += "<td>" + temp.TongSoKTVDangKyHanhNgheKiemToan + "</td>";
            outHTML += "<td>" + temp.TongSoHoiVienCaNhan + "</td>";
            outHTML += "<td>" + temp.NDDPL_HoVaTen + "</td>";
            outHTML += "<td>" + temp.NDDPL_NgaySinh + "</td>";
            outHTML += "<td>" + temp.NDDPL_ChucVu + "</td>";
            outHTML += "<td>" + temp.NDDPL_GioiTinh + "</td>";
            outHTML += "<td>" + temp.NDDPL_CCKTV + "</td>";
            outHTML += "<td>" + temp.NDDPL_NgayCapCCKTV + "</td>";
            outHTML += "<td>" + temp.NDDPL_NoiCap + "</td>";
            outHTML += "<td>" + temp.NDDPL_DienThoaiCoDinh_DiDong + "</td>";
            outHTML += "<td>" + temp.NDDPL_Email + "</td>";
            outHTML += "<td>" + temp.NDDLL_HoVaTen + "</td>";
            outHTML += "<td>" + temp.NDDLL_NgaySinh + "</td>";
            outHTML += "<td>" + temp.NDDLL_ChucVu + "</td>";
            outHTML += "<td>" + temp.NDDLL_GioiTinh + "</td>";
            outHTML += "<td>" + temp.NDDLL_DienThoaiCoDinh_DiDong + "</td>";
            outHTML += "<td>" + temp.NDDLL_Email + "</td>";
            outHTML += "<td>" + temp.TTLLCN_TenChiNhanh + "</td>";
            outHTML += "<td>" + temp.TTLLCN_DiaChiChiNhanh + "</td>";
            outHTML += "<td>" + temp.TTLLCN_DienThoaiCoDinh_DiDong + "</td>";
            outHTML += "<td>" + temp.TTLLCN_EmailChiNhanh + "</td>";
            outHTML += "<td>" + temp.TTLLCN_HoVaTenGiamDocChiNhanh + "</td>";
            outHTML += "<td>" + temp.TTLLCN_MobileGD + "</td>";
            outHTML += "<td>" + temp.TTLLCN_EmailGiamDocChiNhanh + "</td>";
            outHTML += "<td>" + temp.TTLLVPDD_TenVPDD + "</td>";
            outHTML += "<td>" + temp.TTLLVPDD_DiaChiVPDD + "</td>";
            outHTML += "<td>" + temp.TTLLVPDD_DienThoaiCoDinh_DiDong + "</td>";
            outHTML += "<td>" + temp.TTLLVPDD_EmailVPDD + "</td>";
            outHTML += "<td>" + temp.TTLLVPDD_HoVaTenGiamDocVPDD + "</td>";
            outHTML += "<td>" + temp.TTLLVPDD_MobileGD + "</td>";
            outHTML += "<td>" + temp.TTLLVPDD_EmailGDVPDD + "</td>";
            outHTML += "<td>" + temp.KQKSCL_TongSobaoCaoKiemToanDaKyTrongNam + "</td>";
            outHTML += "<td>" + temp.KQKSCL_SoLuongBaoCaoKiemToanChoDonViCoLoiIchCongChungTrongLinhVucChungKhoan + "</td>";
            outHTML += "<td>" + temp.KQKSCL_SoLuongBaoCaoKiemToanDuocKiemTra + "</td>";
            outHTML += "<td>" + temp.KhenThuong_NgayThang + "</td>";
            outHTML += "<td>" + temp.KhenThuong_CapVaHinhThucKhenThuongKyLuat + "</td>";
            outHTML += "<td>" + temp.KhenThuong_LyDo + "</td>";
            outHTML += "<td>" + temp.KyLuat_NgayThang + "</td>";
            outHTML += "<td>" + temp.KyLuat_CapVaHinhThucKhenThuongKyLuat + "</td>";
            outHTML += "<td>" + temp.KyLuat_LyDo + "</td>";
            outHTML += "</tr>";
        }

        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none'></td>";
        outHTML += "<td colspan='62' align='center' style='border-style:none'><b>" + NgayBC + "</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none'></td>";
        outHTML += "<td colspan='62' align='center' style='border-style:none'><b>NGƯỜI LẬP BIỂU<b></td>";
        outHTML += "</tr>";

        outHTML += "</table>";

        Library.ExportToExcel(strName, outHTML);
    }

    protected string strBoChamPhay(string p)
    {
        int leng = p.Length;
        if (p.LastIndexOf(',') != -1)
            leng = p.LastIndexOf(',');
        string strOut = p.Substring(0, leng);
        strOut = strOut.Replace(".", string.Empty);
        return strOut;
    }

    protected void LoadCheckBox()
    {
        if (this.IsPostBack)
        {
            if (Request.Form["tinhtrang_danghoatdong"] != null)
            {
                Response.Write("$('#tinhtrang_danghoatdong').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#tinhtrang_danghoatdong').attr('checked', false); ");
            }
            if (Request.Form["tinhtrang_dunghoatdong"] != null)
            {
                Response.Write("$('#tinhtrang_dunghoatdong').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#tinhtrang_dunghoatdong').attr('checked', false); ");
            }
            if (Request.Form["doituong_dudieukienkitlinhvucchungkhoan"] != null)
            {
                Response.Write("$('#doituong_dudieukienkitlinhvucchungkhoan').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#doituong_dudieukienkitlinhvucchungkhoan').attr('checked', false); ");
            }
            if (Request.Form["doituong_dudieukienkitcongchungkhac"] != null)
            {
                Response.Write("$('#doituong_dudieukienkitcongchungkhac').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#doituong_dudieukienkitcongchungkhac').attr('checked', false); ");
            }
            if (Request.Form["thanhvienhangkiemtoanquocte"] != null)
            {
                Response.Write("$('#thanhvienhangkiemtoanquocte').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#thanhvienhangkiemtoanquocte').attr('checked', false); ");
            }
        }
    }
}