﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_BTC_BaoCao_TongHopKetQuaToChucLopHocCNKT : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo tổng hợp kết quả tổ chức lớp học cập nhật kiến thức kiểm toán viên";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        _listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = Request.Form["ddlNam"];
            if (!string.IsNullOrEmpty(nam))
            {
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/BTC/BaoCaoTongHopKetQuaToChucLopHocCNKT.rpt"));
                GetThongTinBaoCao(nam, rpt);
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "BaoCaoTongHopKetQuaToChucLopHocCNKTKTV");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BaoCaoTongHopKetQuaToChucLopHocCNKTKTV");
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string js = "";
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            js += "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
        Response.Write(js);
    }

    private void GetThongTinBaoCao(string nam, ReportDocument rpt)
    {
        if (string.IsNullOrEmpty(nam))
            return;
        ((TextObject)rpt.ReportDefinition.ReportObjects["txtDiaDiemNgayThang"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
        ((TextObject)rpt.ReportDefinition.ReportObjects["txtNamBaoCao"]).Text = "Năm " + nam;
        ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle2"]).Text = "		Hội Kiểm toán viên hành nghề Việt Nam (VACPA) đã tổ chức các lớp học cập nhật kiến thức cho kiểm toán viên năm " + nam + " theo Quyết định số 2115/QĐ-BTC ngày 27/8/2013 của Bộ Tài chính.";
        ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle3"]).Text = "VACPA xin báo cáo tổng hợp kết quả tổ chức cập nhật kiến thức năm " + nam + " như sau:";

        string idStatusApprove = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
        if(string.IsNullOrEmpty(idStatusApprove))
            return;
        string query = "SELECT COUNT(LopHocID) SoLopDaToChuc FROM tblCNKTLopHoc WHERE YEAR(TuNgay) = " + nam + " AND TinhTrangID = " + idStatusApprove;
        List<Hashtable> listData = _db.GetListData(query);
        {
            Hashtable ht = listData[0];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtContent1"]).Text = "1. Số lượng lớp đã tổ chức: " + ht["SoLopDaToChuc"] + " lớp";
        }

        query = @"SELECT COUNT(DKHCN.DangKyHocCaNhanID) SoLuotDangKyHoc
                FROM tblCNKTLopHoc LH
                LEFT JOIN tblCNKTDangKyHocCaNhan DKHCN ON LH.LopHocID = DKHCN.LopHocID WHERE YEAR(LH.TuNgay) = " + nam + " AND LH.TinhTrangID = " + idStatusApprove;
        List<Hashtable> listData2 = _db.GetListData(query);
        {
            Hashtable ht = listData2[0];
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtContent2"]).Text = "2. Số lượt kiểm toán viên tham dự: " + ht["SoLuotDangKyHoc"] + " KTV";
        }
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if(DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }
}