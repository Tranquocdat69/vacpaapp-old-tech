﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_CNKT_BaoCao_TongHopSoGioCNKTCuaKTVChiTiet : System.Web.UI.UserControl
{
    protected string tenchucnang = "Tổng hợp số giờ cập nhật kiến thức của kiểm toán viên (Chi tiết)";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            int nam = Library.Int32Convert(Request.Form["ddlNam"]);
            string tuNgay = Request.Form["txtTuNgay"];
            string denNgay = Request.Form["txtDenNgay"];
            string idCongTy = Library.CheckNull(Request.Form["hdCongTyID"]);
            if (string.IsNullOrEmpty(Request.Form["ddlNam"])) // Khi chưa post form lần nào thì mới lấy tham số querystring
            {
                if (nam == 0)
                    nam = Library.Int32Convert(Library.CheckNull(Request.QueryString["nam"]));
                if (string.IsNullOrEmpty(tuNgay))
                    tuNgay = Library.CheckNull(Request.QueryString["tungay"]);
                if (string.IsNullOrEmpty(denNgay))
                    denNgay = Library.CheckNull(Request.QueryString["denngay"]);
                if (string.IsNullOrEmpty(idCongTy))
                    idCongTy = Library.CheckNull(Request.QueryString["idct"]);
            }
            if (nam > 0)
            {
                DateTime dateTuNgay = new DateTime(Library.Int32Convert(nam), 1, 1);
                if (!string.IsNullOrEmpty(tuNgay))
                    dateTuNgay = Library.DateTimeConvert(tuNgay, "dd/MM/yyyy");

                DateTime dateDenNgay = new DateTime(Library.Int32Convert(nam), 12, 31);
                if (!string.IsNullOrEmpty(denNgay))
                    dateDenNgay = Library.DateTimeConvert(denNgay, "dd/MM/yyyy");

                DataTable dtLopHoc = GetDanhSachLopHoc(dateTuNgay, dateDenNgay);
                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/CNKT/TongHopSoGioCNKTCuaKTVChiTiet.rpt"));
                List<double[]> list = new List<double[]>(); // Chứa tổng số giờ các cột
                DataTable dt = GetData(dtLopHoc, dateTuNgay, dateDenNgay, idCongTy, list);
                rpt.Database.Tables["dtTongHopSoGioCNKTCuaKTVChiTiet"].SetDataSource(dt);
                double tongSoGioKiKT = list.Sum(doublese => doublese[0]);
                double tongSoGioDD = list.Sum(doublese => doublese[1]);
                double tongSoGioKhac = list.Sum(doublese => doublese[2]);

                int maxRight = 0;
                int width = 840;
                for (int i = 0; i < dtLopHoc.Rows.Count; i++)
                {
                    // Tạo các tiêu đề con trong 1 năm trước
                    maxRight = 10200 + (width * i * 3) + (i * 100 * 3);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderSoGioKiKT" + i, "KT, KiT", maxRight, 3720, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCot_SoGioKiKT" + i, list[i][0].ToString(), maxRight, 4320, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTVChiTiet.SoGioKiKT" + i, "fControlSoGioKiKT" + i, maxRight, 120, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlSoGioKiKT" + i, maxRight, maxRight, 3600, 4200, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlSoGioKiKT" + i, maxRight, maxRight, 0, 600, true);
                    maxRight = (10200 + ((width + 100) * 1)) + (width * i * 3) + (i * 100 * 3);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderSoGioDD" + i, "ĐĐ", maxRight, 3720, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCot_SoGioDD" + i, list[i][1].ToString(), maxRight, 4320, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTVChiTiet.SoGioDD" + i, "fControlSoGioDD" + i, maxRight, 120, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlSoGioDD" + i, maxRight, maxRight, 3600, 4200, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlSoGioDD" + i, maxRight, maxRight, 0, 600, true);
                    maxRight = (10200 + ((width + 100) * 2)) + (width * i * 3) + (i * 100 * 3);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderSoGioKhac" + i, "Khác", maxRight, 3720, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCot_SoGioKhac" + i, list[i][2].ToString(), maxRight, 4320, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTVChiTiet.SoGioKhac" + i, "fControlSoGioKhac" + i, maxRight, 120, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                    maxRight += width + 50;
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlSoGioKhac" + i, maxRight, maxRight, 2880, 4200, true);
                    CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlSoGioKhac" + i, maxRight, maxRight, 0, 600, true);

                    // Tạo tiêu đề
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderLopHoc" + i, "Lớp " + dtLopHoc.Rows[i]["MaLopHoc"], 10200 + (width * i * 3) + (i * 100 * 3), 3000, maxRight - (10200 + (width * i * 3) + (i * 100 * 3)) - 50, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                    CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderLopHocThoiGian" + i, Library.DateTimeConvert(dtLopHoc.Rows[i]["TuNgay"]).ToString("dd/MM/yyyy") + " - " + Library.DateTimeConvert(dtLopHoc.Rows[i]["DenNgay"]).ToString("dd/MM/yyyy"), 10200 + (width * i * 3) + (i * 100 * 3), 3240, maxRight - (10200 + (width * i * 3) + (i * 100 * 3)) - 50, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                }

                // Add cac cot tinh tong
                int rowCount = dtLopHoc.Rows.Count;
                maxRight = 10200 + (width * rowCount * 3) + (rowCount * 100 * 3);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongSoGioKiKT", "KT, KiT", maxRight, 3720, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCot_TongSoGioKiKT", tongSoGioKiKT.ToString(), maxRight, 4320, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTVChiTiet.TongSoGioKiKT", "fControlTongSoGioKiKT", maxRight, 120, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                maxRight += width + 50;
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongSoGioKiKT", maxRight, maxRight, 3600, 4200, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTongSoGioKiKT", maxRight, maxRight, 0, 600, true);
                maxRight = (10200 + ((width + 100) * 1)) + (width * rowCount * 3) + (rowCount * 100 * 3);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongSoGioDD", "ĐĐ", maxRight, 3720, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCot_TongSoGioDD", tongSoGioDD.ToString(), maxRight, 4320, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTVChiTiet.TongSoGioDD", "fControlTongSoGioDD", maxRight, 120, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                maxRight += width + 50;
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongSoGioDD", maxRight, maxRight, 3600, 4200, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTongSoGioDD", maxRight, maxRight, 0, 600, true);
                maxRight = (10200 + ((width + 100) * 2)) + (width * rowCount * 3) + (rowCount * 100 * 3);
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongSoGioKhac", "Khác", maxRight, 3720, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                //CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCot_TongSoGioKhac", tongSoGioKhac.ToString(), maxRight, 4320, width, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawFieldObjectCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "", "dtTongHopSoGioCNKTCuaKTVChiTiet.TongSoGioKhac", "fControlTongSoGioKhac", maxRight, 120, width, null, null, 11, null, true, false, CrystalReportControl.HorizontalAlign_Center, "");
                maxRight += width + 50;
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongSoGioKhac", maxRight, maxRight, 2880, 4200, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTongSoGioKhac", maxRight, maxRight, 0, 600, true);

                // Tạo tiêu đề năm
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderTongCong", "Tổng cộng giờ " + nam, 10200 + (width * rowCount * 3) + (rowCount * 100 * 3), 3000, maxRight - (10200 + (width * rowCount * 3) + (rowCount * 100 * 3)) - 50, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);

                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lMiddleHeader"]).Right = maxRight;
                // Add cot "ghi chu"
                maxRight += 1440 + 50;
                CrystalReportControl.DrawTextObjectCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "txtControlHeaderGhiChu", "Ghi chú", 10200 + (width * (rowCount + 1) * 3) + ((rowCount + 1) * 100 * 3), 3360, 1440, null, null, 10, FontStyle.Bold, true, false, CrystalReportControl.HorizontalAlign_Center);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_ReportHeader, 0, "lControlTongGhiChu", maxRight, maxRight, 2880, 4200, true);
                CrystalReportControl.DrawLineCrystalReport(rpt, CrystalReportControl.Section_Detail, 0, "lDetailControlTongGhiChu", maxRight, maxRight, 0, 600, true);

                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lTopHeader"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomHeader"]).Right = maxRight;
                //((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomHeader2"]).Right = maxRight;
                ((CrystalDecisions.CrystalReports.Engine.LineObject)rpt.ReportDefinition.ReportObjects["lBottomDetail"]).Right = maxRight;

                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "TỔNG HỢP SỐ GIỜ CẬP NHẬT KIẾN THỨC CỦA KIỂM TOÁN VIÊN NĂM " + nam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle2"]).Text = "Từ ngày " + dateTuNgay.ToString("dd/MM/yyyy") + " đến ngày " + dateDenNgay.ToString("dd/MM/yyyy");
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                    ExportExcel(dt, dtLopHoc, nam, dateTuNgay.ToString("dd/MM/yyyy"), dateDenNgay.ToString("dd/MM/yyyy"), list);
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "TongHopSoGioCNKTCuaKTVChiTiet");
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string js = "";
        string nam = Request.Form["ddlNam"];
        string tuNgay = Request.Form["txtTuNgay"];
        string denNgay = Request.Form["txtDenNgay"];
        string idCongTy = Library.CheckNull(Request.Form["hdCongTyID"]);
        string maCongTy = Library.CheckNull(Request.Form["txtMaCongTy"]);
        if (string.IsNullOrEmpty(Request.Form["ddlNam"])) // Khi chưa post form lần nào thì mới lấy tham số querystring
        {
            if (string.IsNullOrEmpty(nam))
                nam = Library.CheckNull(Request.QueryString["nam"]);
            if (string.IsNullOrEmpty(tuNgay))
                tuNgay = Library.CheckNull(Request.QueryString["tungay"]);
            if (string.IsNullOrEmpty(denNgay))
                denNgay = Library.CheckNull(Request.QueryString["denngay"]);
            if (string.IsNullOrEmpty(idCongTy))
                idCongTy = Library.CheckNull(Request.QueryString["idct"]);
            if (string.IsNullOrEmpty(maCongTy))
                maCongTy = Library.CheckNull(Request.QueryString["mact"]);
        }
        if (!string.IsNullOrEmpty(nam))
            js += "$('#ddlNam').val('" + nam + "');" + Environment.NewLine;
        js += "$('#txtTuNgay').val('" + tuNgay + "');" + Environment.NewLine;
        js += "$('#txtDenNgay').val('" + denNgay + "');" + Environment.NewLine;
        if (!string.IsNullOrEmpty(maCongTy))
        {
            js += "$('#txtMaCongTy').val('" + maCongTy + "');" + Environment.NewLine;
            js += "CallActionGetInforCongTy();" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private DataTable GetData(DataTable dtLopHoc, DateTime dateTuNgay, DateTime dateDenNgay, string idCongTy, List<double[]> list)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "HoTen", "TenCongTy", "NgaySinhNam", "NgaySinhNu", "SoChungChiKTV", "NgayCapChungChiKTV", "GhiChu", "TongSoGioKiKT", "TongSoGioDD", "TongSoGioKhac" });
        for (int i = 0; i < dtLopHoc.Rows.Count; i++)
        {
            dt.Columns.Add("SoGioKiKT" + i);
            dt.Columns.Add("SoGioDD" + i);
            dt.Columns.Add("SoGioKhac" + i);
            list.Add(new double[3]);
        }

        DataTable dtTongHopSoGio = _db.GetDataTable(ListName.Proc_BAOCAO_CNKT_TongHopSoGioCNKTCuaKTVChiTiet, new List<string> { "@FromDate", "@ToDate", "@HoiVienTapTheID" }, new List<object> { dateTuNgay.ToString("yyyy-MM-dd"), dateDenNgay.ToString("yyyy-MM-dd"), idCongTy });
        if (dtTongHopSoGio.Rows.Count > 0)
        {
            string tempIdHvcn = "0";
            DataRow dr = null;
            for (int x = 0; x < dtTongHopSoGio.Rows.Count; x++)
            {
                if (dtTongHopSoGio.Rows[x]["HoiVienCaNhanID"].ToString() != tempIdHvcn)
                {
                    tempIdHvcn = dtTongHopSoGio.Rows[x]["HoiVienCaNhanID"].ToString();
                    dr = dt.NewRow();
                    dr["STT"] = (dt.Rows.Count + 1);
                    dr["HoTen"] = dtTongHopSoGio.Rows[x]["FullName"];
                    dr["TenCongTy"] = dtTongHopSoGio.Rows[x]["TenDoanhNghiep"];
                    string gioiTinh = dtTongHopSoGio.Rows[x]["GioiTinh"].ToString();
                    if (gioiTinh.ToLower() == "0")
                        dr["NgaySinhNu"] = !string.IsNullOrEmpty(dtTongHopSoGio.Rows[x]["NgaySinh"].ToString()) ? Library.DateTimeConvert(dtTongHopSoGio.Rows[x]["NgaySinh"]).ToString("dd/MM/yyyy") : "";
                    else if (gioiTinh.ToLower() == "1")
                        dr["NgaySinhNam"] = !string.IsNullOrEmpty(dtTongHopSoGio.Rows[x]["NgaySinh"].ToString()) ? Library.DateTimeConvert(dtTongHopSoGio.Rows[x]["NgaySinh"]).ToString("dd/MM/yyyy") : "";
                    dr["SoChungChiKTV"] = dtTongHopSoGio.Rows[x]["SoChungChiKTV"];
                    dr["NgayCapChungChiKTV"] = !string.IsNullOrEmpty(dtTongHopSoGio.Rows[x]["NgayCapChungChiKTV"].ToString()) ? Library.DateTimeConvert(dtTongHopSoGio.Rows[x]["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy") : "";

                    double tongSoGioKiKT = 0, tongSoGioDD = 0, tongSoKhac = 0;
                    for (int i = 0; i < dtLopHoc.Rows.Count; i++)
                    {
                        DataRow[] arrDataRow = dtTongHopSoGio.Select("HoiVienCaNhanID = " + tempIdHvcn + " AND LopHocID = " + dtLopHoc.Rows[i]["LopHocID"]);
                        foreach (DataRow dataRow in arrDataRow)
                        {
                            if (dataRow["LoaiChuyenDe"].ToString() == "1")
                            {
                                dr["SoGioKiKT" + i] = Library.DoubleConvert(dr["SoGioKiKT" + i]) + Library.DoubleConvert(dataRow["SoGioThucTe"]);
                                list[i][0] += Library.DoubleConvert(dataRow["SoGioThucTe"]); // Tinh tong cot
                                tongSoGioKiKT += Library.DoubleConvert(dataRow["SoGioThucTe"]); // tinh tong dong
                            }
                            if (dataRow["LoaiChuyenDe"].ToString() == "2")
                            {
                                dr["SoGioDD" + i] = Library.DoubleConvert(dr["SoGioDD" + i]) + Library.DoubleConvert(dataRow["SoGioThucTe"]);
                                list[i][1] += Library.DoubleConvert(dataRow["SoGioThucTe"]);// Tinh tong cot
                                tongSoGioDD += Library.DoubleConvert(dataRow["SoGioThucTe"]);// tinh tong dong
                            }
                            if (dataRow["LoaiChuyenDe"].ToString() == "3")
                            {
                                dr["SoGioKhac" + i] = Library.DoubleConvert(dr["SoGioKhac" + i]) + Library.DoubleConvert(dataRow["SoGioThucTe"]);
                                list[i][2] += Library.DoubleConvert(dataRow["SoGioThucTe"]);// Tinh tong cot
                                tongSoKhac += Library.DoubleConvert(dataRow["SoGioThucTe"]);// tinh tong dong
                            }
                        }
                    }
                    dr["TongSoGioKiKT"] = tongSoGioKiKT;
                    dr["TongSoGioDD"] = tongSoGioDD;
                    dr["TongSoGioKhac"] = tongSoKhac;
                    dt.Rows.Add(dr);
                }
            }
        }
        return dt;
    }

    private DataTable GetDanhSachLopHoc(DateTime dateTuNgay, DateTime dateDenNgay)
    {
        DataTable dt = new DataTable();

        string query = @"SELECT LopHocID, MaLopHoc, TuNgay,DenNgay FROM tblCNKTLopHoc 
                            INNER JOIN tblDMTrangThai DMTT ON tblCNKTLopHoc.TinhTrangID = DMTT.TrangThaiID
                            WHERE (tblCNKTLopHoc.TuNgay >= '" + dateTuNgay.ToString("yyyy-MM-dd") + @"' OR tblCNKTLopHoc.DenNgay >= '" + dateTuNgay.ToString("yyyy-MM-dd") + @"')
	                            AND (tblCNKTLopHoc.TuNgay <= '" + dateDenNgay.ToString("yyyy-MM-dd") + @"' OR tblCNKTLopHoc.DenNgay <= '" + dateDenNgay.ToString("yyyy-MM-dd") + @"') AND DMTT.MaTrangThai = '5' ORDER BY TuNgay ASC";
        dt = _db.GetDataTable(query);
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, DataTable dtLopHoc, int nam, string tuNgay, string denNgay, List<Double[]> list)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:120px;
            vertical-align: middle;
        }

        .HeaderColumn1 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTitle {      
            font-weight: bold;      
            vertical-align: middle;            
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
            height: 40px;
            vertical-align: middle; 
        }

        .GroupTitle2 {
            font-size: 10pt;
            font-weight: bold;
            height: 30px;
            vertical-align: middle; 
        }
        
        .Column {
            text-align: center;
            vertical-align: middle;
        }

        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='12' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='https://" + Request.Url.Authority + @"/images/HeaderExport.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='12' style='text-align:center;font-size: 14pt; font-weight: bold;'>TỔNG HỢP SỐ GIỜ CẬP NHẬT KIẾN THỨC CỦA KIỂM TOÁN VIÊN NĂM " + nam + @"</td>
            </tr>
            <tr>
                <td colspan='12' style='text-align:center;font-size: 12pt; font-weight: bold;'>TÍNH TỪ NGÀY " + tuNgay + @" ĐẾN NGÀY " + denNgay + @"</td>
            </tr>
            <tr>
                <td colspan='12' style='text-align:center;font-size: 10pt; font-weight: bold;'><i>(Kèm theo Công văn số ...-.../VACPA ngày .../.../20...)</i></td>
            </tr>
            <tr><td><td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td rowspan='2' class='HeaderColumn1'>STT</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 150px;'>Họ và tên</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 200px;'>Tên công ty</td
                                <td colspan='2' class='HeaderColumn1'>Ngày sinh</td>
                                <td colspan='2' class='HeaderColumn1'>Chứng chỉ KTV</td>";

        for (int i = 0; i < dtLopHoc.Rows.Count; i++)
        {
            htmlExport += "<td colspan='3' class='HeaderColumn1' style='width: 70px; height:40px;'>Lớp " + dtLopHoc.Rows[i]["MaLopHoc"] + " <br>" + Library.DateTimeConvert(dtLopHoc.Rows[i]["TuNgay"]).ToString("dd/MM/yyyy") + " - " + Library.DateTimeConvert(dtLopHoc.Rows[i]["DenNgay"]).ToString("dd/MM/yyyy") + "</td>";
        }

        htmlExport += @"        <td colspan='3' class='HeaderColumn1'>Tổng số giờ " + nam + @"</td>
                                <td rowspan='2' class='HeaderColumn1' style='width: 70px;'>Ghi chú</td>";

        htmlExport += "</tr><tr><td class='HeaderColumn1' style='width: 100px;'>Nam</td>" +
                      "<td class='HeaderColumn1' style='width: 100px;'>Nữ</td>" +
                      "<td class='HeaderColumn1' style='width: 100px;'>Số</td>" +
                      "<td class='HeaderColumn1' style='width: 100px;'>Ngày cấp</td>";
        for (int i = 0; i < dtLopHoc.Rows.Count; i++)
        {
            htmlExport += "<td  class='HeaderColumn1' style='width: 70px;'>KT, KiT</td>" +
                          "<td  class='HeaderColumn1' style='width: 70px;'>ĐĐ</td>" +
                          "<td  class='HeaderColumn1' style='width: 70px;'>Khác</td>";
        }
        htmlExport += "<td class='HeaderColumn1' style='width: 70px;'>KT, KiT</td>" +
                      "<td class='HeaderColumn1' style='width: 70px;'>ĐĐ</td>" +
                      "<td class='HeaderColumn1' style='width: 70px;'>Khác</td>" +
                      "</tr>";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            htmlExport += "<tr><td class='ColumnStt'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["HoTen"] + "</td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["TenCongTy"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NgaySinhNam"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NgaySinhNu"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["SoChungChiKTV"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["NgayCapChungChiKTV"] + "</td>";
            for (int x = 0; x < dtLopHoc.Rows.Count; x++)
            {
                htmlExport += "<td class='Column'>" + dt.Rows[i]["SoGioKiKT" + x] + "</td>";
                htmlExport += "<td class='Column'>" + dt.Rows[i]["SoGioDD" + x] + "</td>";
                htmlExport += "<td class='Column'>" + dt.Rows[i]["SoGioKhac" + x] + "</td>";
            }
            htmlExport += "<td class='HeaderColumn1'>" + dt.Rows[i]["TongSoGioKiKT"] + "</td>" +
                          "<td class='HeaderColumn1'>" + dt.Rows[i]["TongSoGioDD"] + "</td>" +
                          "<td class='HeaderColumn1'>" + dt.Rows[i]["TongSoGioKhac"] + "</td>" +
                          "<td class='HeaderColumn1'>" + dt.Rows[i]["GhiChu"] + "</td>";
            htmlExport += "</tr>";
        }
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td colspan='3' style='font-weight: bold; text-align: center;'>Người lập</td><td></td>" +
                      "<td colspan='3' style='font-weight: bold; text-align: center;'>Người soát xét</td><td></td>" +
                      "<td colspan='3' style='font-weight: bold; text-align: center;'>Người duyệt</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("TongHopSoGioCNKTCuaKTVChiTiet.xsl", "<html>" + css + htmlExport + "</html>");
    }
}