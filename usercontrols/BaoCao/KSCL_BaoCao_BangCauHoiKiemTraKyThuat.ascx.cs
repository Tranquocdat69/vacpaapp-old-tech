﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_KSCL_BaoCao_BangCauHoiKiemTraKyThuat : System.Web.UI.UserControl
{
    protected string tenchucnang = "Bảng câu hỏi kiểm tra kỹ thuật";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            ReportDocument rpt = new ReportDocument();
            rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
            rpt.Load(Server.MapPath("Report/KSCL/BangCauHoiKiemTraKyThuat.rpt"));
            DataTable dt = GetNhomCauHoi("2", "1");
            rpt.Database.Tables["dtBangCauHoiKiemTraHeThong"].SetDataSource(dt);
            //((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "BẢNG TÌNH HÌNH NỘP BÁO CÁO TỰ KIỂM TRA NĂM " + nam;
            //((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            //((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
            CrystalReportViewer1.ReportSource = rpt;
            CrystalReportViewer1.RefreshReport();
            if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                ExportExcel(dt);
                //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "BangCauHoiKiemTraKyThuat");
            if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "BangCauHoiKiemTraKyThuat");
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
        {
            string js = "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboDaNopBaoCao"]))
                js += "$('#cboDaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboDaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboChuaNopBaoCao"]))
                js += "$('#cboChuaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboChuaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            Response.Write(js);
        }
    }

    private void SetDataRow(DataTable dt, string stt, string noiDung, string canCu, string diemChuan, string diemThucTe, string huongDanChamDiem)
    {
        DataRow dr = dt.NewRow();
        dr["STT"] = stt;
        dr["NoiDung"] = noiDung;
        dr["CanCu"] = canCu;
        dr["DiemChuan"] = "<center>" + diemChuan + "</center>";
        dr["DiemThucTe"] = diemThucTe;
        dr["HuongDanChamDiem"] = huongDanChamDiem;
        dt.Rows.Add(dr);
    }

    private double _tongSoDiem = 0;
    protected DataTable GetNhomCauHoi(string loaiCauHoi, string tinhTrangHieuLuc)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "NoiDung", "CanCu", "DiemChuan", "DiemThucTe", "HuongDanChamDiem" });

        DataTable data = GetDanhSachCauHoi(loaiCauHoi, tinhTrangHieuLuc); // Get danh sách câu hỏi đổ tất cả vào DataTable

        // Get Danh sách nhóm câu hỏi
        string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE Loai = " + loaiCauHoi + " AND (NhomCauHoiChaID IS NULL OR NhomCauHoiChaID = '') ORDER BY NhomCauHoiID";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            int index = 1;
            int currentIndexInDataTable = 0;
            foreach (Hashtable ht in listData)
            {
                DataRow[] arrDataRow = data.Select("NCHID = " + ht["NhomCauHoiID"]); // Lấy danh sách câu hỏi theo ID nhóm câu hỏi
                double sumDiemToiDa = arrDataRow.Sum(dataRow => Library.DoubleConvert(dataRow["DiemToiDa"]));
                _tongSoDiem += sumDiemToiDa;
                currentIndexInDataTable = dt.Rows.Count;
                SetDataRow(dt, "<b>" + index.ToString() + "</b>", "<b>" + ht["TenNhomCauHoi"] + "</b>", "", (sumDiemToiDa > 0 ? Library.ChangeFormatNumber(sumDiemToiDa, "us") : ""), "", "");

                query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + ht["NhomCauHoiID"] + " ORDER BY NhomCauHoiID";
                List<Hashtable> listDataChild = _db.GetListData(query);
                string tienToXacDinhLaCauHoi = ""; // Tiền tố xác định là câu hỏi trong chuỗi STT (Thêm .0.)
                if (listDataChild.Count > 0)
                    tienToXacDinhLaCauHoi = "0.";
                int cauHoiIndex = 1;
                foreach (DataRow dataRow in arrDataRow)
                {
                    SetDataRow(dt, index + "." + tienToXacDinhLaCauHoi + cauHoiIndex, dataRow["CauHoi"].ToString(), dataRow["CanCu"].ToString(), (Library.DoubleConvert(dataRow["DiemToiDa"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(dataRow["DiemToiDa"]), "us") : ""), "", dataRow["HuongDanChamDiem"].ToString());
                    cauHoiIndex++;
                }
                GetNhomCauHoiSub(dt, listDataChild, data, index, loaiCauHoi, ref sumDiemToiDa); // Gọi hàm đệ quy
                // Set lại điểm tối đa ở đây
                dt.Rows[currentIndexInDataTable]["DiemChuan"] = "<center><b>" + sumDiemToiDa + "</center></b>";

                index++;
            }
        }
        return dt;
    }

    protected void GetNhomCauHoiSub(DataTable dt, List<Hashtable> listData, DataTable data, int index, string loaiCauHoi, ref double sumDiemToiDa_NhomCha)
    {
        if (listData.Count > 0)
        {
            int subIndex = 1;
            int currentIndexInDataTable = 0;
            foreach (Hashtable ht in listData)
            {
                DataRow[] arrDataRow = data.Select("NCHID = " + ht["NhomCauHoiID"]);
                double sumDiemToiDa = arrDataRow.Sum(dataRow => Library.DoubleConvert(dataRow["DiemToiDa"]));
                _tongSoDiem += sumDiemToiDa;
                currentIndexInDataTable = dt.Rows.Count;
                SetDataRow(dt, "<b>" + index + "." + subIndex + "</b>", "<b>" + ht["TenNhomCauHoi"] + "</b>", "", (sumDiemToiDa > 0 ? Library.ChangeFormatNumber(sumDiemToiDa, "us") : ""), "", "");
                string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + ht["NhomCauHoiID"] + " ORDER BY NhomCauHoiID";
                List<Hashtable> listDataChild = _db.GetListData(query);
                string tienToXacDinhLaCauHoi = ""; // Tiền tố xác định là câu hỏi trong chuỗi STT (Thêm .0.)
                if (listDataChild.Count > 0)
                    tienToXacDinhLaCauHoi = "0.";
                int cauHoiIndex = 1;
                foreach (DataRow dataRow in arrDataRow)
                {
                    SetDataRow(dt, index + "." + subIndex + "." + tienToXacDinhLaCauHoi + cauHoiIndex, dataRow["CauHoi"].ToString(), dataRow["CanCu"].ToString(), (Library.DoubleConvert(dataRow["DiemToiDa"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(dataRow["DiemToiDa"]), "us") : ""), "", dataRow["HuongDanChamDiem"].ToString());
                    cauHoiIndex++;
                }
                GetNhomCauHoiSub(dt, listDataChild, data, subIndex, loaiCauHoi, ref sumDiemToiDa_NhomCha);
                subIndex++;
                sumDiemToiDa_NhomCha += sumDiemToiDa;
                // Set lại điểm tối đa ở đây
                dt.Rows[currentIndexInDataTable]["DiemChuan"] = "<center><b>" + sumDiemToiDa + "</center></b>";
            }
        }
    }

    private DataTable GetDanhSachCauHoi(string loaiCauHoi, string tinhTrangHieuLuc)
    {
        DataTable dt = new DataTable();
        string query = @"SELECT CauHoiKTKTID, MaCauHoi, NgayLap, a.NhomCauHoiID NCHID, CauHoi, DiemToiDa, CanCu, HuongDanChamDiem, TinhTrangHieuLuc FROM tblKSCLCauHoiKT a
                            INNER JOIN tblDMNhomCauHoiKSCL b ON a.NhomCauHoiID = b.NhomCauHoiID
                            WHERE b.Loai = " + loaiCauHoi + " AND a.TinhTrangHieuLuc = '" + tinhTrangHieuLuc + "'";
        dt = _db.GetDataTable(query);
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:120px;
            vertical-align: middle;
        }

        .HeaderColumn1 
        {
            font-size: 11pt;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTitle {                    
            vertical-align: middle;            
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
            height: 40px;
            vertical-align: middle; 
        }

        .GroupTitle2 {
            font-size: 10pt;
            font-weight: bold;
            height: 30px;
            vertical-align: middle; 
        }
        
        .Column {
            font-size: 12pt;
            text-align: center;
            vertical-align: middle;
        }

        .ColumnSum {
            font-size: 12pt;
            font-weight: bold;
            vertical-align: middle;
        }

        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='8' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='6' style='text-align:center;font-size: 14pt; font-weight: bold;'>BẢNG CÂU HỎI KIỂM TRA KỸ THUẬT NĂM ...</td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td><td colspan='4' style='font-size: 12pt; font-weight: bold;'>Họ và tên TV Đoàn kiểm tra:</td><td style='font-size: 12pt; font-weight: bold;' colspan='3'>Ngày kiểm tra: ....................</td></tr>           
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>1. .......................................................</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt;'>2. .......................................................</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Họ và tên người đại diện công ty:..........................................</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Tên Công ty kiểm toán:..........................................</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Tên khách hàng:..........................................</td></tr>
            <tr><td></td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Báo cáo tài chính năm:.................... Phí kiểm toán:................</td></tr>            
            <tr><td></td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Báo cáo kiểm toán số:.................... ngày:................</td></tr> 
            <tr><td></td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Thành viên BGĐ ký BCKT:.................... Giấy CNĐKHN:................</td></tr> 
            <tr><td></td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Kiểm toán viên ký BCKT:.................... Giấy CNĐKHN:................</td></tr> 
            <tr><td></td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Dạng ý kiến kiểm toán:..........................................</td></tr>
            <tr><td></td></tr>
            <tr><td colspan='8' style='font-size: 12pt; font-weight: bold;'>Kết quả kiểm tra:</td></tr>
            <tr><td></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn1'>STT</td>
                                <td class='HeaderColumn1' style='width: 200px;'>Câu hỏi</td>
                                <td class='HeaderColumn1' style='width: 70px;'>Tham chiếu văn bản</td>
                                <td class='HeaderColumn1' style='width: 100px;'>Trả lời</td>
                                <td class='HeaderColumn1' style='width: 70px;'>Điểm tối đa</td>
                                <td class='HeaderColumn1' style='width: 70px;'>Điểm thực tế</td>
                                <td class='HeaderColumn1' style='width: 120px;'>Ghi chú của người kiểm tra</td>
                                <td class='HeaderColumn1' style='width: 200px;'>Hướng dẫn chấm điểm</td>";

        htmlExport += "</tr>";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            htmlExport += "<tr><td class='ColumnStt'>" + dt.Rows[i]["STT"] + "</td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["NoiDung"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["CanCu"] + "</td>" +
                          "<td class='Column'></td>" +
                          "<td class='Column'>" + dt.Rows[i]["DiemChuan"] + "</td>" +
                          "<td class='Column'>" + dt.Rows[i]["DiemThucTe"] + "</td>" +
                          "<td class='Column'></td>" +
                          "<td class='ColumnTitle'>" + dt.Rows[i]["HuongDanChamDiem"] + "</td>";
            htmlExport += "</tr>";
        }
        htmlExport += "<tr><td></td><td class='ColumnSum'>Cộng điểm</td><td></td><td></td><td class='ColumnSum' style='text-align: right;'></td><td class='ColumnSum' style='text-align: right;'></td><td></td><td></td></tr>";
        htmlExport += "<tr><td></td><td class='ColumnSum'>Điểm quy đổi</td><td></td><td></td><td class='ColumnSum' style='text-align: right;'></td><td class='ColumnSum' style='text-align: right;'></td><td></td><td></td></tr>";
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: center;'>1.</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Ý kiến nhận xét tổng quát của TV đoàn kiểm tra(Tính đầy đủ, tính tin cậy)</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: center;'>2.</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Ý kiến giải trình của KTV phụ trách file</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: center;'>3.</td><td colspan='7' style='font-size: 12pt; font-weight: bold;'>Xếp loại</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: left;'>(1)</td><td colspan='3' style='font-size: 12pt; font-weight: bold;'>Từ 80 điểm trở lên</td><td></td><td colspan='3' style='font-size: 12pt; font-weight: bold;'>Tốt <i>(Không có sai phạm hoặc lưu ý đáng kể)</i></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: left;'>(2)</td><td colspan='3' style='font-size: 12pt; font-weight: bold;'>Từ 60 điểm đến dưới 80 điểm</td><td></td><td colspan='3' style='font-size: 12pt; font-weight: bold;'>Đạt yêu cầu <i>(Không có sai phạm đáng kể, có một số sai phạm kèm theo các ý kiến lưu ý)</i></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: left;'>(3)</td><td colspan='3' style='font-size: 12pt; font-weight: bold;'>Từ 40 điểm đến dưới 60 điểm</td><td></td><td colspan='3' style='font-size: 12pt; font-weight: bold;'>Chưa đạt yêu cầu <i>(Còn hạn chế)</i></td></tr>";
        htmlExport += "<tr><td style='font-size: 12pt; font-weight: bold; text-align: left;'>(4)</td><td colspan='3' style='font-size: 12pt; font-weight: bold;'>Dưới 40 điểm</td><td></td><td colspan='3' style='font-size: 12pt; font-weight: bold;'>Yếu kém <i>(Hạn chế nghiêm trọng, có nhiều sai phạm)</i></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        htmlExport += "<tr><td colspan='8' style='font-size: 12pt; font-weight: bold;'>TV Đoàn kiểm tra 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TV Đoàn kiểm tra 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Người đại diện công ty</td></tr>";
        htmlExport += "<tr><td colspan='8' style='font-size: 12pt;'><i>&nbsp;&nbsp;(Chữ ký, họ tên)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Chữ ký, họ tên)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Chữ ký, họ tên)</i></td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("BangCauHoiKiemTraKyThuat.xsl", "<html>" + css + htmlExport + "</html>");
    }
}