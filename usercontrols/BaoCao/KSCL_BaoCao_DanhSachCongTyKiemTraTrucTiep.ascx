﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep.ascx.cs" Inherits="usercontrols_KSCL_BaoCao_DanhSachCongTyKiemTraTrucTiep" %>

<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<style type="text/css">
    .DivReport
    {
        margin: 0 auto;
        width: 700px;
        font-family: Time New Roman;
    }
    
    .tblBorder
    {
        border: 0px;
    }
    
    .tblBorder th
    {
        border: 1px solid gray;
    }
    
    .tblBorder td
    {
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        border-bottom: 1px solid gray;
    }
</style>
<form id="Form1" runat="server" clientidmode="Static">
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<h4 class="widgettitle">
    Danh sách công ty kiểm tra trực tiếp</h4>
<fieldset class="fsBlockInfor">
    <legend>Tham số kết xuất dữ liệu</legend>
    <table id="tblThongTinChung" width="700px" border="0" class="formtbl">
        <tr>
            <td>
                Năm<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlNam" name="ddlNam">
                    <% GetListYear();%>
                </select>
            </td>
        </tr>
        <tr>
            <td>Mã danh sách KTTT:</td>
            <td>
                <input type="text" name="txtMaDanhSach" id="txtMaDanhSach" onchange="CallActionGetInforDanhSach();" style="max-width: 120px;" />
                <input type="hidden" name="hdDanhSachID" id="hdDanhSachID" />
                <input type="button" value="---" onclick="OpenDanhSachKiemTraTrucTiep();" style="border: 1px solid gray;" />
            </td>
        </tr>
        <tr>
            <td>Tính chất<span class="starRequired">(*)</span>:</td>
            <td>
                <input type="checkbox" id="cboCongTyPhaiNopBCTKT" name="cboCongTyPhaiNopBCTKT" checked="checked"/>&nbsp;Công ty phải nộp báo cáo tự kiểm tra<br />
                <input type="checkbox" id="cboCongTyKiemTraTheoYeuCau" name="cboCongTyKiemTraTheoYeuCau" checked="checked"/>&nbsp;Công ty kiểm tra theo yêu cầu
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <a href="javascript:;" id="btnSearch" class="btn btn-rounded" onclick="Export('');">
                    <i class="iconfa-search"></i>Xem dữ liệu</a> <a id="A2" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('word');"><i class="iconsweets-word2"></i>Kết xuất Word</a> 
                        <a id="A1" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('excel');"><i class="iconsweets-excel2"></i>Kết xuất Excel</a> 
                        <a id="A3" href="javascript:;" class="btn btn-rounded"
                        onclick="Export('pdf');"><i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Danh sách công ty kiểm tra trực tiếp</legend>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" GroupTreeImagesFolderUrl=""
        Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px" Width="350px"
        EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
        HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False"
        HasZoomFactorList="False" ToolPanelView="None" SeparatePages="False" 
        ShowAllPageIds="True" HasExportButton="False" HasPrintButton="False" />
</fieldset>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    function OpenDanhSachKiemTraTrucTiep() {
        $("#DivDanhSach").empty();
        $("#DivDanhSach").append($("<iframe width='100%' height='100%' id='ifDanhSach' name='ifDanhSach' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien_ChonDanhSachKTTT&nam=" + $('#ddlNam option:selected').val()));
        $("#DivDanhSach").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách kiểm tra trực tiếp</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSach.ChooseHocVien();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSach').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivDanhSach').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }
    
    function GetMaDanhSachFromPopup(value) {
        $('#txtMaDanhSach').val(value);
        CallActionGetInforDanhSach();
    }
    
    function CallActionGetInforDanhSach() {
        var maDanhSach = $('#txtMaDanhSach').val();
        iframeProcess.location = '/iframe.aspx?page=KSCL_BaoCao_Proccess&action=loadttdanhsach&madanhsach=' + maDanhSach + '&nam=' + $('#ddlNam option:selected').val();
    }
    
    function DisplayInforDanhSach(value) {
        if(value.length == 0) {
            $('#txtMaDanhSach').val('');
            $('#hdDanhSachID').val('');
            $('#hdCaNhanID').val('');
            $('#txtMaCaNhan').val('');
            alert('Mã danh sách không đúng hoặc không tồn tại!');
        } else {
            $('#hdDanhSachID').val(value);
            $('#hdCaNhanID').val('');
            $('#txtMaCaNhan').val('');
        }
    }
    
    function CloseFormDanhSachKiemTraTrucTiep() {
        $("#DivDanhSach").dialog('close');
    }

    function Export(type) {
        if(!$('#cboCongTyPhaiNopBCTKT').prop('checked') && !$('#cboCongTyKiemTraTheoYeuCau').prop('checked')) {
            alert('Phải chọn ít nhất 1 loại tính chất để lọc dữ liệu báo cáo!');
            return;
        }
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
    
    <% SetDataJavascript();%>
</script>
<div id="DivDanhSach">
</div>