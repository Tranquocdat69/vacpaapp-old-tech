﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quyết định xử lý kỷ luật Hội viên có sai phạm về chất lượng dịch vụ kiểm toán BCTC năm";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = "";
            string idDanhSach = Request.Form["hdDanhSachID"];
            string idCaNhan = Request.Form["hdCaNhanID"];
            if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            {
                nam = Request.Form["ddlNam"];

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/QuyetDinhXuLyKyLuatHoiVien.rpt"));
                if (!string.IsNullOrEmpty(idDanhSach) && !string.IsNullOrEmpty(idCaNhan))
                    rpt.Database.Tables["dtNoiDungQuyetDinhXuLySaiPham"].SetDataSource(GetNoiDungXuLySaiPham(idDanhSach, idCaNhan, rpt));
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitle"]).Text = "V/v Xử lý kỷ luật Hội viên có sai phạm về chất lượng dịch vụ kiểm toán BCTC năm " + nam;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "QuyetDinhXuLyKyLuatHoiVien");
                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "QuyetDinhXuLyKyLuatHoiVien");
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        string nam = Request.Form["ddlNam"];
        string maDanhSach = Request.Form["txtMaDanhSach"];
        string maCaNhan = Request.Form["txtMaCaNhan"];
        if (string.IsNullOrEmpty(Request.Form["ddlNam"])) // Neu load trang lan dau chua post du lieu thi moi lay gia tri tu querystring
        {
            if (string.IsNullOrEmpty(maDanhSach))
                maDanhSach = Library.CheckNull(Request.QueryString["madskttt"]);
            // Lay nam lap danh sach
            if (!string.IsNullOrEmpty(maDanhSach))
            {
                try
                {
                    _db.OpenConnection();
                    string query = "SELECT YEAR(NgayLap) nam FROM tblKSCLDSKiemTraTrucTiep WHERE MaDanhSach = '" + maDanhSach + "'";
                    List<Hashtable> listData = _db.GetListData(query);
                    if(listData.Count > 0)
                    {
                        nam = listData[0]["nam"].ToString();
                    }
                }
                catch { _db.CloseConnection(); }
                finally
                {
                    _db.CloseConnection();
                }
            }
        }
        string js = "";
        if (!string.IsNullOrEmpty(nam))
        {
            js = "$('#ddlNam').val('" + nam + "');" + Environment.NewLine;
        }
        if(!string.IsNullOrEmpty(maDanhSach))
        {
            js += "$('#txtMaDanhSach').val('" + maDanhSach + "');" + Environment.NewLine;
            js += "$('#hdDanhSachID').val('" + Request.Form["hdDanhSachID"] + "');" + Environment.NewLine;
            //js += "CallActionGetInforDanhSach();" + Environment.NewLine;
        }
        if(!string.IsNullOrEmpty(maCaNhan))
        {
            js += "$('#txtMaCaNhan').val('" + Request.Form["txtMaCaNhan"] + "');" + Environment.NewLine;
            js += "$('#hdCaNhanID').val('" + Request.Form["hdCaNhanID"] + "');" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private DataTable GetNoiDungXuLySaiPham(string idDanhSach, string idCaNhan, ReportDocument rpt)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "NoiDung" });
        string query = @"SELECT (HVCN.HoDem + ' ' + HVCN.Ten) Fullname,  HVCN.HoiVienCaNhanID, HVCN.SoChungChiKTV, HVCN.NgayCapChungChiKTV, CV.TenChucVu,
                            XLSPCT.NoiDungSaiPham, XLSP.SoQuyetDinh, XLSP.NgayQuyetDinh
                            FROM tblKSCLXuLySaiPhamChiTiet XLSPCT           
                            INNER JOIN tblKSCLXuLySaiPham XLSP ON XLSPCT.XuLySaiPhamID = XLSP.XuLySaiPhamID
                            INNER JOIN tblHoiVienCaNhan HVCN ON XLSPCT.HoiVienCaNhanID = HVCN.HoiVienCaNhanID    
                            LEFT JOIN tblDMChucVu CV ON HVCN.ChucVuID = CV.ChucVuID      
                            WHERE XLSPCT.HoiVienCaNhanID = " + idCaNhan + @" AND XLSP.DSKiemTraTrucTiepID = " + idDanhSach;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            string ngayCapGcnKtv = "";
            if (!string.IsNullOrEmpty(ht["NgayCapChungChiKTV"].ToString()))
                ngayCapGcnKtv = Library.DateTimeConvert(ht["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy");
            DataRow dr;
            dr = dt.NewRow();
            dr["NoiDung"] = "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Điều 1: </b>Cảnh cáo Hội viên " + ht["Fullname"] + ", Chứng chỉ KTV số: " + ht["SoChungChiKTV"] + ", cấp ngày  " + ngayCapGcnKtv + ", nguyên " + ht["TenChucVu"] + ", do: " + ht["NoiDungSaiPham"];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["NoiDung"] = "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Điều 2: </b>Hội viên " + ht["Fullname"] + " cần nghiêm túc rút kinh nghiệm, tích cực trau dồi chuyên môn nghề nghiệp, đảm bảo tuân thủ đầy đủ các quy định của chuẩn mực kiểm toán, quy định đạo đức nghề nghiệp, các quy định pháp lý có liên quan và Điều lệ Hội Kiểm toán viên hành nghề Việt Nam, tránh lặp lại các sai phạm tương tự trong tương lại.";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["NoiDung"] = "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Điều 3: </b>Hội viên là Giám đốc Công ty TNHH Kiểm toán Immanuel, Tổng thư ký VACPA, Trưởng Văn phòng VACPA Hà Nội, Trưởng Văn phòng VACPA Hồ Chí Minh cần tăng cường các biện pháp quản lý kiểm toán viên, quản lý hội viên, kiểm soát chất lượng hoạt động kiểm toán, đảm bảo thực hiện đúng các quy định của Nhà nước và hướng dẫn của Hội.";
            dt.Rows.Add(dr);

            DateTime ngayQuyetDinh = DateTime.Now;
            if (!string.IsNullOrEmpty(ht["NgayQuyetDinh"].ToString()))
                ngayQuyetDinh = Library.DateTimeConvert(ht["NgayQuyetDinh"]);
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtHeader2"]).Text = "Hà Nội, Ngày " + ngayQuyetDinh.Day + " tháng " + ngayQuyetDinh.Month + " năm " + ngayQuyetDinh.Year;
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtHeader1"]).Text = "Số: " + ht["SoQuyetDinh"].ToString();
            ((TextObject)rpt.ReportDefinition.ReportObjects["txtNguoiNhan"]).Text = ht["Fullname"].ToString();
        }
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }
}