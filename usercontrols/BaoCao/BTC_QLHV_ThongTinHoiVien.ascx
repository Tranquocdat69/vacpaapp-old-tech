﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BTC_QLHV_ThongTinHoiVien.ascx.cs" Inherits="usercontrols_BTC_QLHV_ThongTinHoiVien" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<%--<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>--%>
<script type="text/javascript" src="js/ui.dropdownchecklist-1.5-min.js"></script>
<%--<script type="text/javascript" src="js/jquery-ui.min.js"></script>--%>

<form id="Form1" runat="server" clientidmode="Static">
<asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<h4 class="widgettitle">
    Thông tin cá nhân kiểm toán viên</h4>
<fieldset class="fsBlockInfor" style="margin-top: 0px;">
    <legend>Thông tin kiểm toán viên</legend>
    <table width="100%" border="0" class="formtbl">
    <tr>
        <td width="20%"></td>
        <td width="13%">Đơn vị công tác:</td>
        <td width="13%"> <select name="DonViId" id ="DonViId" style="width: 152px" >
            <%  
              try
              {
                  LoadDonViCongTac(dt.Rows[0]["DBHC_TinhID_ToChuc"].ToString().Trim());
              }
              catch 
              {
                  LoadDonViCongTac("00");
              }
              
              %>
        </select></td>
        <td width="8%"></td>
        <td width="13%">Vùng miền:</td>
        <td width="13%"> <select name="VungMienId" id ="VungMienId" style="width: 152px" >
            <%  
              try
              {
                  VungMien(dt.Rows[0]["DBHC_TinhID_ToChuc"].ToString().Trim());
              }
              catch 
              {
                  VungMien("00");
              }
              
              %>
        </select>
        </td>
        <td width="20%"></td>
    </tr>
    <tr>
        <td width="20%"></td>
        <td width="13%">Tỉnh/thành</td>
        <td width="13%"><select name="TinhId" id ="TinhId" style="width: 152px">
            <%  
              try
              {
                  cm.Load_ThanhPho(dt.Rows[0]["DBHC_TinhID_ToChuc"].ToString().Trim());
              }
              catch 
              {
                  cm.Load_ThanhPho("00");
              }
              
              %>
        </select></td>
        <td width="8%"></td>
        <td width="13%">Số chứng chỉ KTV:</td>
        <td width="13%"> <input type="text" id="txtChungChiKTV" name = "txtChungChiKTV" 
                style="width: 152px" /></td>
        <td width="20%"></td>
    </tr>
    <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  >
       <ContentTemplate>
    <tr>
        <td width="20%"></td>
        <td width="13%">ID <span style="color:Red">(*)</span>:</td>
        <td width="13%"><asp:TextBox runat="server" ID="id" OnTextChanged="Text_Changed" AutoPostBack="true"></asp:TextBox> </td>
        <td width="8%"><a id="akickChon" onclick="open_ChonID()" data-placement="top" data-rel="tooltip" href="#none" data-original-title="..."  rel="tooltip" class="btnabc">
            <input type="button" value = "..." id="btChonID" name="btChonID" 
                style="width: 49px" /></a></td>
        <td width="13%">Họ và tên:</td>
        <td width="13%"><asp:TextBox runat="server" ID="txtTen" Enabled = "false"></asp:TextBox></td>
        <td width="20%"></td>
    </tr>
        </ContentTemplate></asp:UpdatePanel>
    <%--<tr>
        <td width="20%"></td>
        <td width="13%">Định dạng file: </td>
        <td colspan="4"> <select id="idFile" name="idFile" style="width: 152px">
            <%DinhDangFile(); %>
        </select></td>
        <td width="20%"></td>
    </tr>--%>
    </table>

    <div class="dataTables_length" style="text-align: center;">
        <a id="btnSave" href="javascript:;" class="btn" onclick="View();"><i class="iconfa-plus-sign">
             </i>Xem thông tin</a> 
        
        <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="View('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a>
                 <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="View('pdf');">
                    <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
                    <input type="hidden" id="hdAction" name="hdAction"/>
    </div>
</fieldset>

 <fieldset class="fsBlockInfor">
        <legend>THÔNG TIN HỘI VIÊN</legend>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
        Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
        HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
        HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
        ToolPanelView="None" SeparatePages="False"/>
    </fieldset>

      <div id="div_dmNhanSuHoiVien_add"></div>

    <div style = "display: none">
    <asp:LinkButton ID="btnChay" runat="server" CssClass="btn" 
        onclick="btnChay_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>
</form>

<script type="text/javascript">
    function open_ChonID() {
        var dvId = document.getElementById("DonViId");
        var vmId = document.getElementById("VungMienId");
        var tId = document.getElementById("TinhId");

        if (dvId.options[dvId.selectedIndex].value == "" || vmId.options[vmId.selectedIndex].value == "" || tId.options[tId.selectedIndex].value == "")
            alert("Bạn cần chọn đủ thông tin trước khi chọn ID!");
        else {
            var timestamp = Number(new Date());
            $("#div_dmNhanSuHoiVien_add").empty();
            $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoivien' name='iframe_hoivien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dbChonHoiVienCaNhan_BC&type=canhan&mode=iframe&time=" + timestamp));
            $("#div_dmNhanSuHoiVien_add").dialog({
                autoOpen: false,
                title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách hội viên</b>",
                modal: true,
                width: "640",
                height: "480",
                buttons: [
                             {
                                 text: "Chọn",
                                 click: function () {
                                     formChon();
                                 }
                             },
                                {
                                    text: "Đóng",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                        ]
            }).dialog("open");
        }
    }
</script>


<script type="text/javascript">
    function formChon() {
        $("#div_dmNhanSuHoiVien_add").dialog('close');
        document.getElementById('<%= btnChay.ClientID %>').click();
    }
</script>

<script type="text/javascript">
    function View(type) {
        //        <%setdataCrystalReport(); %>
        $('#hdAction').val(type);
        $('#Form1').submit();
    }
</script>
<%--<script type="text/javascript">
    $("#DonViId").dropdownchecklist(); 
</script>--%>
