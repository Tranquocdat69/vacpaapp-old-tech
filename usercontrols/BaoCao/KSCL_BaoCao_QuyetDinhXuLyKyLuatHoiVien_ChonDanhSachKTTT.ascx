﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien_ChonDanhSachKTTT.ascx.cs" Inherits="usercontrols_KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien_ChonDanhSachKTTT" %>

<style type="text/css">
    .tdTable
    {
        width: 120px;
    }
</style>
<form id="Form1" runat="server" method="post" enctype="multipart/form-data">

    <fieldset class="fsBlockInfor">
        <legend>Thông tin tìm kiếm</legend>
        <table width="100%" border="0">
            <tr>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Mã danh sách:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMaDanhSach" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Tình trạng:
                            </td>
                            <td>
                                <asp:CheckBox ID="cboChoDuyet" runat="server" Checked="True" />Chờ duyệt
                                <asp:CheckBox ID="cboTuChoi" runat="server" Checked="True" />Từ chối
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:CheckBox ID="cboDaDuyet" runat="server" Checked="True" />Đã duyệt
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset class="fsBlockInforMini" style="width: 95%; height: 100px;">
                        <legend>Tiêu chí công ty đủ điều kiện kiểm toán cho đơn vị có lợi ích công chúng</legend>
                        <asp:CheckBox ID="cboDuDieuKienKTCK" runat="server" />Trong lĩnh vực chứng khoán<br />
                        <asp:CheckBox ID="cboDuDieuKienKTKhac" runat="server" />Đơn vị có lợi ích công chúng
                        khác
                    </fieldset>
                </td>
                <td>
                    <fieldset class="fsBlockInforMini" style="width: 95%; height: 100px;">
                        <legend>Tiêu chí công ty phải nộp báo cáo tự kiểm tra</legend>
                        <table style="width: 100%;">
                        <tr>
                            <td>
                                Xếp loại năm trước:
                            </td>
                            <td>
                                <asp:CheckBox ID="cboXepHang1" runat="server" />Tốt&nbsp;<asp:CheckBox ID="cboXepHang2"
                                    runat="server" />Đạt yêu cầu<br />
                                <asp:CheckBox ID="cboXepHang3" runat="server" />Không đạt yêu cầu&nbsp;<asp:CheckBox
                                    ID="cboXepHang4" runat="server" />Yếu kém<br />
                            </td>
                        </tr>
                    </table>
                        <asp:CheckBox ID="cboTieuChi_Khac" runat="server" />Các tiêu chí khác
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Mã HVTC/CTKT:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMaCongTy" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Loại hình công ty:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlLoaiHinh" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Doanh thu:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDoanhThuTu" runat="server" Width="100px" onchange="CheckIsNumber(this);"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtDoanhThuDen" runat="server" Width="100px" onchange="CheckIsNumber(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Tên công ty kiểm toán:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTenCongTy" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Vùng miền:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlVungMien" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Số năm chưa được kiểm tra:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTuNam" runat="server" Width="60px" onchange="CheckIsInteger(this);"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtDenNam" runat="server" Width="60px" onchange="CheckIsInteger(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </fieldset>
<div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearchPopup" runat="server" CssClass="btn" 
        onclick="lbtSearch_Click" OnClientClick="return CheckValidFormSearch();"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách công ty kiểm toán kiểm tra trực tiếp</legend>
    <table id="tblHocVienHeader" width="100%" border="0" class="formtbl">
        <tr>
            <th style="width: 30px;">
                
            </th>
            <th style="width: 50px;">
                STT
            </th>
            <th style="min-width: 150px;">
                Mã danh sách
            </th>
            <th style="width: 100px;">
                Ngày lập danh sách
            </th>
            <th style="width: 100px;">
                Số lượng công ty
            </th>
        </tr>
    </table>
    <div id='DivListHocVien' style="max-height: 200px;">
        <table id="tblHocVien" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpHocVien" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="width: 30px; vertical-align: middle;">
                            <input type="radio" value='<%# Eval("MaDanhSach") %>' name="DanhSach" />
                        </td>
                        <td style="width: 50px; text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="min-width: 150px;">
                            <%# Eval("MaDanhSach")%>
                        </td>
                        <td style="width: 100px;">
                            <%# Library.DateTimeConvert(Eval("NgayLap")).ToString("dd/MM/yyyy")%>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("SoCongTy")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</fieldset>
</form>
<script type="text/javascript">
    $("#<%= Form1.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtSearchPopup.ClientID %>').attr('href'));
        }
    });

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm lấy thông tin học viên được chọn từ danh sách
    function ChooseHocVien() {
        var listId = '';
        var flag = false;
        $('#tblHocVien :radio').each(function () {
            var checked = $(this).prop("checked");
            if (checked) {
                listId = $(this).val();
                flag = true;
            }
        });
        if (!flag)
            alert('Phải chọn danh sách!');
        else {
            parent.GetMaDanhSachFromPopup(listId);
            parent.CloseFormDanhSachKiemTraTrucTiep();
        }
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm kiểm tra trình duyệt để set thuộc tính overflow cho thẻ DIV hiển thị danh sách bản ghi
    // Chỉ trình duyệt nền Webkit mới có thuộc tính overflow: overlay
    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivListHocVien').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivListHocVien').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivListHocVien').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }
    
    //    $(document).ready(function () {
    //        $("#Form1").validate({
    //            onsubmit: false
    //        });
    //    });

    function CheckValidFormSearch() {
        var isValid = $("#Form1").valid();
        if (isValid) {
            return true;
        } else {
            return false;
        }
    }

    myFunction();
</script>