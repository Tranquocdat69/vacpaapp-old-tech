﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class usercontrols_KSCL_BaoCao_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien : System.Web.UI.UserControl
{
    protected string tenchucnang = "Tổng hợp sai phạm và hình thức kỷ luật đối với hội viên";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            _db.OpenConnection();
            string nam = "";
            if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
            {
                nam = Request.Form["ddlNam"];

                ReportDocument rpt = new ReportDocument();
                rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
                rpt.Load(Server.MapPath("Report/KSCL/TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien.rpt"));
                DataTable dt = GetDanhSachSaiPham(nam, rpt);
                rpt.Database.Tables["dtTongHopSaiPhamVaHinhThucKyLuatVoiHoiVien"].SetDataSource(dt);
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtTitleNam"]).Text = "NĂM " + nam;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter1"]).Text = "Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                ((TextObject)rpt.ReportDefinition.ReportObjects["txtFooter3"]).Text = cm.Admin_HoVaTen;
                CrystalReportViewer1.ReportSource = rpt;
                CrystalReportViewer1.RefreshReport();
                if (Library.CheckNull(Request.Form["hdAction"]) == "word")
                    rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "TongHopSaiPhamVaHinhThucKyLuatVoiHoiVienNam" + nam);
                if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
                {
                    //string fileName = Server.MapPath("~") + "/TongHopSaiPhamVaHinhThucKyLuatVoiHoiVienNam.xls";
                    //rpt.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    //rpt.ExportOptions.ExportFormatType = ExportFormatType.Excel;
                    //ExcelFormatOptions objFormatOptions = new ExcelFormatOptions();
                    //objFormatOptions.ExcelUseConstantColumnWidth = false;
                    //objFormatOptions.ShowGridLines = true;
                    //rpt.ExportOptions.FormatOptions = objFormatOptions;
                    //DiskFileDestinationOptions objOption = new DiskFileDestinationOptions();
                    //objOption.DiskFileName = fileName;
                    //rpt.ExportOptions.DestinationOptions = objOption;
                    //rpt.Export();
                    //Response.Redirect("/TongHopSaiPhamVaHinhThucKyLuatVoiHoiVienNam.xls");
                    //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "TongHopSaiPhamVaHinhThucKyLuatVoiHoiVienNam" + nam);
                    ExportExcel(dt, nam);
                }

                if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
                    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "TongHopSaiPhamVaHinhThucKyLuatVoiHoiVienNam" + nam);
            }
            else
            {
                CrystalReportControl.ResetReportToNull(CrystalReportViewer1);
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void SetDataJavascript()
    {
        if (!string.IsNullOrEmpty(Request.Form["ddlNam"]))
        {
            string js = "$('#ddlNam').val('" + Request.Form["ddlNam"] + "');" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboDaNopBaoCao"]))
                js += "$('#cboDaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboDaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            if (!string.IsNullOrEmpty(Request.Form["cboChuaNopBaoCao"]))
                js += "$('#cboChuaNopBaoCao').prop('checked', true);" + Environment.NewLine;
            else
                js += "$('#cboChuaNopBaoCao').prop('checked', false);" + Environment.NewLine;
            Response.Write(js);
        }
    }

    private DataTable GetDanhSachSaiPham(string nam, ReportDocument rpt)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "STT", "TenCongTy_HocVien", "TomTatThongTin", "NoiDungSaiPham", "QuyetDinhBTC", "QuyetDinhVACPA", "NhomCongTy" });
        if (string.IsNullOrEmpty(nam))
            return dt;

        string procName = ListName.Proc_BAOCAO_KSCL_TongHopSaiPhamVaHinhThucKyLuatVoiHoiVien;
        List<Hashtable> listData = _db.GetListData(procName, new List<string> { "@Nam" }, new List<object> { nam });
        if (listData.Count > 0)
        {
            DataRow dr;
            int index = 1;
            foreach (Hashtable ht in listData)
            {
                dr = dt.NewRow();
                dr["STT"] = index;
                string tomTatThongTin = "";
                if (!string.IsNullOrEmpty(ht["HoiVienCaNhanID"].ToString()))
                {
                    dr["TenCongTy_HocVien"] = ht["Fullname"];
                    tomTatThongTin += "Chứng chỉ KTV: " + ht["SoChungChiKTV"] + "<br>";
                    tomTatThongTin += "Ngày cấp: " + Library.DateTimeConvert(ht["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy") + "<br>";
                    tomTatThongTin += "Chức vụ: " + ht["TenChucVu"];
                }
                else
                {
                    dr["TenCongTy_HocVien"] = ht["TenDoanhNghiep"];
                    tomTatThongTin += "Năm thành lập: " + ht["NamThanhLap"] + "<br>";
                    tomTatThongTin += "Năm kiểm tra: " + nam;
                }
                dr["TomTatThongTin"] = tomTatThongTin;
                dr["NoiDungSaiPham"] = ht["NoiDungSaiPham"];
                dr["QuyetDinhBTC"] = ht["QuyetDinhBTC"];
                dr["QuyetDinhVACPA"] = ht["QuyetDinhVACPA"];
                dr["NhomCongTy"] = ht["TenDoanhNghiep"];
                dt.Rows.Add(dr);
                index++;
            }
        }
        return dt;
    }

    protected void GetListYear()
    {
        string js = "";
        for (int i = 1940; i <= 2040; i++)
        {
            if (DateTime.Now.Year == i)
                js += "<option selected='selected'>" + i + "</option>" + Environment.NewLine;
            else
                js += "<option>" + i + "</option>" + Environment.NewLine;
        }
        Response.Write(js);
    }

    private void ExportExcel(DataTable dt, string nam)
    {
        string css = @"<head><style type='text/css'>
        .HeaderColumn 
        {
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
            height:70px;
            vertical-align: middle;
        }

        .GroupTitle {
            font-size: 10pt;
            font-weight: bold;
        }

        .ColumnStt {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTitle {
            text-align: center;
            vertical-align: middle;
        }
        
        .ColumnTomTat {
            text-align: center;
            vertical-align: middle;
        }

        .ColumnNdsp {
            text-align: center;
            vertical-align: middle;
        }

        .ColumnHtxlBTC {
            text-align: center;
            vertical-align: middle;
        }

        .ColumnHtxlVacpa {
            text-align: center;
            vertical-align: middle;
        }

        br { mso-data-placement:same-cell; }
    </style></head>";
        string htmlExport = @"<table style='font-family: Times New Roman;'>
            <tr>
                <td colspan='6' style='height: 100px; text-align: center; vertical-align: middle; padding: 10px;'>
                    <img src='http://" + Request.Url.Authority + @"/images/HeaderExport_3.png' />
                </td>
            </tr>        
            <tr>
                <td colspan='6' style='text-align:center;font-size: 14pt; font-weight: bold;'>TỔNG HỢP SAI PHẠM VÀ HÌNH THỨC KỶ LUẬT VỚI HỘI VIÊN</td>
            </tr>
            <tr>
                <td colspan='6' style='text-align:center;font-size: 12pt; font-weight: bold;'><i>NĂM " + nam + @"</i></td>
            </tr>";
        htmlExport += "</table>";
        htmlExport += "<table border='1' style='font-family: Times New Roman;'>";
        htmlExport += @"
                            <tr>
                                <td class='HeaderColumn'>STT</td>
                <td class='HeaderColumn' style='width: 170px;'>Công ty/Kiểm toán viên</td>
                <td class='HeaderColumn' style='width: 170px;'>Tóm tắt thông tin</td>
                <td class='HeaderColumn' style='width: 170px;'>Nội dung sai phạm</td>
                <td class='HeaderColumn' style='width: 170px;'>Quyết định hình thức xử lý của BTC</td>
                <td class='HeaderColumn' style='width: 170px;'>Đề xuất hình thức xử lý của VACPA</td>
                            </tr>";
        string tempNhom = "";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string nhom = dt.Rows[i]["NhomCongTy"].ToString();
            string stt = dt.Rows[i]["STT"].ToString();
            string title = dt.Rows[i]["TenCongTy_HocVien"].ToString();
            string tomTatThongTin = dt.Rows[i]["TomTatThongTin"].ToString();
            string noiDungSaiPham = dt.Rows[i]["NoiDungSaiPham"].ToString();
            string quyetDinhBtc = dt.Rows[i]["QuyetDinhBTC"].ToString();
            string quyetDinhVacpa = dt.Rows[i]["QuyetDinhVACPA"].ToString();
            if (nhom != tempNhom)
            {
                tempNhom = nhom;
                htmlExport += "<tr><td colspan='6' class='GroupTitle'>" + nhom + "</td></tr>";
            }
            htmlExport += "<tr><td class='ColumnStt'>" + stt + "</td><td class='ColumnTitle'>" + title + "</td><td class='ColumnTomTat'>" + tomTatThongTin + "</td>" +
                          "<td class='ColumnNdsp'>" + noiDungSaiPham + "</td><td class='ColumnHtxlBTC'>" + quyetDinhBtc + "</td><td class='ColumnHtxlVacpa'>" + quyetDinhVacpa + "</td></tr>";
        }
        htmlExport += "</table>";
        htmlExport += "<table style='font-family: Times New Roman;'>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2' style='font-weight: bold; text-align: center;'>Hà Nội, Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + "</td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2' style='font-weight: bold; text-align: center;'><i>Người lập biểu</i></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2'></td></tr>";
        htmlExport += "<tr><td></td><td></td><td></td><td></td><td colspan='2' style='font-weight: bold; text-align: center;'>" + cm.Admin_HoVaTen + "</td></tr>";
        htmlExport += "</table>";
        Library.ExportToExcel("TongHopSaiPhamVaHinhThucKyLuatVoiHoiVienNam.xsl", "<html>" + css + htmlExport.Replace("<br>", "\n").Replace("<br />", "\n") + "</html>");
    }
}