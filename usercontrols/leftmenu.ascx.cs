﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_leftmenu : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    protected void checkmenu(string pages, int HaveChild = 0)
    {
        string url = Request.Path + "?" + Request.QueryString.ToString();

        if (HaveChild == 0)
        {
            if (pages.Contains("|" + url + "|")) Response.Write(" class='active' ");
        }
        if (HaveChild == 1)
        {
            if (pages.Contains("|" + url + "|")) Response.Write(" class='dropdown active' ");
            if (!pages.Contains("|" + url + "|")) Response.Write(" class='dropdown' ");
        }
        if (HaveChild == 2)
        {
            if (pages.Contains("|" + url + "|")) Response.Write("  style='display: block;' ");
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public string get_sohoso(string trangthai)
    {
        SqlCommand sql = new SqlCommand();
        string idtrangthai = "0";
        sql.CommandText = "select count(TrangThaiHoSoID) from tblTrangThaiHoSo where ToKhaiDangKyThiKiemToanVienID IN (SELECT ToKhaiDangKyThiKiemToanVienID From tblToKhaiDangKyThiKiemToanVien WHERE KhachHangID="+cm.Khachhang_KhachHangID+") AND TrangThaiID="+trangthai;

        if (trangthai=="ALL")
            sql.CommandText = "select count(TrangThaiHoSoID) from tblTrangThaiHoSo where ToKhaiDangKyThiKiemToanVienID IN (SELECT ToKhaiDangKyThiKiemToanVienID From tblToKhaiDangKyThiKiemToanVien WHERE KhachHangID=" + cm.Khachhang_KhachHangID + ") ";

        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        if (ds.Tables[0].Rows.Count > 0)
            if (!DBNull.Value.Equals(ds.Tables[0].Rows[0][0])) idtrangthai = ds.Tables[0].Rows[0][0].ToString().Trim();

        string sohoso = "";

        sohoso += "" + ds.Tables[0].Rows[0][0].ToString() + "";

        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds = null;

        return sohoso;
    }
}