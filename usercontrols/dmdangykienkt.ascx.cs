﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;

public partial class usercontrols_dmdangykienkt : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();


    // Tên chức năng
    public string tenchucnang = "Dạng ý kiến Kiểm toán";
    // Icon CSS Class  
    public string IconClass = "";
    public string bangdm = "tblDMDangYKienKT";
    public string quyen = "DMDangYKienKT";
    public string truongid = "DangYKienKTID";
    public string truongten = "TenDangYKien";
    public string truongma = "MaDangYKien";

    protected void Page_Load(object sender, EventArgs e)
    {
        Commons cm = new Commons();
        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

        dmdangykienkt_grv.Columns[1].HeaderText = "Mã dạng ý kiến";
        dmdangykienkt_grv.Columns[1].SortExpression = truongma;
        dmdangykienkt_grv.Columns[2].HeaderText = tenchucnang;
        dmdangykienkt_grv.Columns[2].SortExpression = truongten;


        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>Danh mục " + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>Danh mục " + tenchucnang + @"</h1> </div>"));


        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            Form1.Visible = false;
            return;
        }




        ///////////


        load_data();


        try
        {
            if (Request.QueryString["act"] == "delete")
            {

                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
                {

                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "DELETE FROM " + bangdm + " WHERE " + truongid + " IN (" + Request.QueryString["id"] + ")";
                    DataAccess.RunActionCmd(sql);

                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog(quyen, "Xóa bản ghi có ID \"" + Request.QueryString["id"] + "\" của danh mục " + tenchucnang);

                    Response.Redirect("admin.aspx?page=dmdangykienkt");
                }
            }



        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }




    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void load_data()
    {
        try
        {
            int i;
            Commons cm = new Commons();
            // Tìm kiếm
            SelectQueryBuilder query = new SelectQueryBuilder();
            query.SelectFromTable(bangdm);
            query.SelectAllColumns();


            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                query.AddWhere(truongten, Comparison.Like, "%" + Request.Form["timkiem_Ten"] + "%");

            SqlCommand sql = new SqlCommand();
            sql.CommandText = query.BuildQuery();
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;

            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = quyen + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }

            dmdangykienkt_grv.DataSource = objPds;

            dmdangykienkt_grv.DataBind();
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void dmdangykienkt_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        dmdangykienkt_grv.PageIndex = e.NewPageIndex;
        dmdangykienkt_grv.DataBind();
    }

    protected void dmdangykienkt_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }





}