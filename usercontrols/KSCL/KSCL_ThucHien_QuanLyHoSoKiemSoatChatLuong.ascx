﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong.ascx.cs"
    Inherits="usercontrols_KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" clientidmode="Static" name="Form1" runat="server">
<h4 class="widgettitle">
    Danh sách hồ sơ kiểm soát chất lượng</h4>
<div class="dataTables_length">
    <a id="btnOpenFormUpdate" href="javascript:;" class="btn btn-rounded"
        onclick="OpenFormUpdate();"><i class="iconfa-edit"></i>Sửa</a>&nbsp; <a href="javascript:;"
            id="btn_search" class="btn btn-rounded" onclick="OpenFormSearch();"><i class="iconfa-search">
            </i>Tìm kiếm</a>
</div>
<div>
    <fieldset class="fsBlockInfor">
        <legend>Danh sách hồ sơ kiểm soát chất lượng</legend>
        <asp:GridView ClientIDMode="Static" ID="gv_DanhSach" runat="server" AutoGenerateColumns="False"
            class="table table-bordered responsive dyntable" AllowPaging="False" AllowSorting="True"
            OnPageIndexChanging="gv_DanhSach_PageIndexChanging" OnRowCommand="gv_DanhSach_RowCommand"
            OnRowDataBound="gv_DanhSach_RowDataBound" onsorting="gv_DanhSach_Sorting">
            <Columns>
                <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
                    ItemStyle-Width="30px">
                    <ItemTemplate>
                        <input type="checkbox" id="checkbox" runat="server" class="colcheckbox" value='<%# Eval("ID")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mã hồ sơ" SortExpression="MaHoSo">
                    <ItemTemplate>
                        <%# Eval("MaHoSo")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tên công ty kiểm toán" SortExpression="TenCongTy">
                    <ItemTemplate>
                        <%# Eval("TenCongTy")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tên viết tắt" SortExpression="TenVietTat">
                    <ItemTemplate>
                        <%# Eval("TenVietTat")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Thời gian kiểm tra" SortExpression="TuNgay">
                    <ItemTemplate>
                        <%# Eval("TuNgay") + " - " + Eval("DenNgay")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tình trạng" SortExpression="TrangThai">
                    <ItemTemplate>
                        <%# Eval("TrangThai") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Thao tác" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <div class="btn-group">
                            <a href='javascript:;' data-placement="top"
                                    onclick="OpenFormNoiDungHoSo(<%# Eval("ID") %>);" data-rel="tooltip" data-original-title="Xem/Sửa"
                                    rel="tooltip" class="btn"><i class="iconsweets-create"></i></a>
                            <%--<asp:LinkButton ID="btnDeleteInList" runat="server" CommandArgument='<%# Eval("ID") %>'
                                CommandName="deleteHoSo" data-placement="top" data-rel="tooltip" data-original-title="Xóa"
                                rel="tooltip" class="btn" OnClientClick="return confirm('Bạn chắc chắn muốn thực hiện thao tác này không?');"><i class="iconsweets-trashcan"></i></asp:LinkButton>--%>
                        </div>
                        <asp:HiddenField ID="hf_SoFileTaiLieu" runat="server" Value='<%# Eval("SoFileTaiLieu") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hf_TrangHienTai" runat="server" />
        <div class="dataTables_info" id="dyntable_info">
    <div class="pagination pagination-mini">
        Chuyển đến trang:
        <ul>
            <li><a href="javascript:;" onclick="$('#tranghientai').val(0);$('#hdAction').val('paging'); $('#user_search').submit();"><<
                Đầu tiên</a></li>
            <li><a href="javascript:;" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager option:selected').val()-1); $('#hdAction').val('paging'); $('#user_search').submit();}">
                < Trước</a>
            </li>
            <li><a style="border: none; background-color: #eeeeee">
                <asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" Style="width: 55px;
                    height: 22px;" onchange="$('#tranghientai').val(this.value);$('#hdAction').val('paging'); $('#user_search').submit();">
                </asp:DropDownList>
            </a></li>
            <li style="margin-left: 5px;"><a href="javascript:;" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) { $('#tranghientai').val(parseInt($('#Pager option:selected').val())+1); $('#hdAction').val('paging'); $('#user_search').submit();} ;"
                style="border-left: 1px solid #ccc;">Sau ></a></li>
            <li><a href="javascript:;" onclick="$('#tranghientai').val($('#Pager option').length-1);$('#hdAction').val('paging'); $('#user_search').submit();">
                Cuối cùng >></a></li>
        </ul>
    </div>
</div>
    </fieldset>
</div>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    // Hiển thị tooltip
    if (jQuery('#gv_DanhSach').length > 0) jQuery('#gv_DanhSach').tooltip({ selector: "a[rel=tooltip]" });

    jQuery(document).ready(function () {
        // dynamic table      

        $("#gv_DanhSach .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#gv_DanhSach :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });

    });
    // Created by NGUYEN MANH HUNG - 2014/12/12
    // Hàm mở Dialog tìm kiếm
    function OpenFormSearch() {
        $("#DivSearch").dialog({
            resizable: true,
            width: 800,
            height: 400,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm hồ sơ kiểm soát chất lượng</b>",
            modal: true,
            zIndex: 1000
        });
        $("#DivSearch").parent().appendTo($("#Form1"));
    }

    function OpenFormNoiDungHoSo(id) {
        $("#DivNoiDungHoSo").empty();
        if (id != undefined && parseInt(id) > 0)
            $("#DivNoiDungHoSo").append($("<iframe width='100%' height='100%' id='ifNoiDungHoSoKiemSoatChatLuong' name='ifNoiDungHoSoKiemSoatChatLuong' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_CapNhat&Id=" + id));
        $("#DivNoiDungHoSo").dialog({
            resizable: true,
            width: 850,
            height: 550,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Hồ sơ kiểm soát chất lượng</b>",
            modal: true,
            zIndex: 1000
        });
    }

    function CloseFormNoiDungHoSo() {
        $("#DivNoiDungHoSo").dialog('close');
    }

    function OpenFormUpdate() {
        var count = 0;
        var id = '';
        $('#gv_DanhSach :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all') {
                count++;
                id = $(this).val();
            }
        });
        if (count == 0) {
            alert('Phải chọn một bản ghi để thực hiện thao tác!');
            return false;
        }
        if (count > 1) {
            alert('Chỉ được chọn một bản ghi để thực hiện cập nhật!');
            return false;
        }
        OpenFormNoiDungHoSo(id);
    }
</script>
<div id="DivSearch" style="display: none;">
    <fieldset class="fsBlockInfor">
        <legend>Tiêu chí tìm kiếm</legend>
        <table width="100%" border="0" class="formtblInforWithoutBorder">
            <tr>
                <td>
                    Thời gian kiểm tra:
                </td>
                <td>
                    <asp:TextBox ID="txtThoiGianKiemTraTu" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>&nbsp;<img
                        src="/images/icons/calendar.png" id="imgThoiGianKiemTraTu" />
                </td>
                <td>
                    -
                </td>
                <td>
                    <asp:TextBox ID="txtThoiGianKiemTraDen" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>&nbsp;<img
                        src="/images/icons/calendar.png" id="imgThoiGianKiemTraDen" />
                </td>
            </tr>
            <tr>
                <td style="width: 150px;">
                    Mã đoàn kiểm tra:
                </td>
                <td>
                    <asp:TextBox ID="txtMaDoanKiemTra" runat="server" Width="120px"></asp:TextBox>
                </td>
                <td>
                    ID CTKT/HVTT:
                </td>
                <td>
                    <asp:TextBox ID="txtMaCongTy" runat="server" Width="120px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Tên công ty kiểm toán:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtTenCongTy" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    ID HVCN/KTV:
                </td>
                <td>
                    <asp:TextBox ID="txtMaHVCN" runat="server" Width="120px"></asp:TextBox>
                </td>
                <td>
                    Tên kiểm toán viên:
                </td>
                <td>
                    <asp:TextBox ID="txtTenKiemToanVien" runat="server" Width="120px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <fieldset class="fsBlockInforMini">
            <legend>Thành viên đoàn kiểm tra</legend>
            <table width="100%" border="0" class="formtblInforWithoutBorder">
                <tr>
                    <td>
                        Mã thành viên:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMaThanhVienDKT" runat="server" Width="120px"></asp:TextBox>
                    </td>
                    <td>
                        Tên thành viên:
                    </td>
                    <td>
                        <asp:TextBox ID="txtTenThanhVienDKT" runat="server" Width="120px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Số chứng chỉ KTV:
                    </td>
                    <td>
                        <asp:TextBox ID="txtSoChungChiKTV" runat="server" Width="120px"></asp:TextBox>
                    </td>
                    <td>
                        Ngày cấp:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNgayCap" runat="server" Width="120px" CssClass="dateITA"></asp:TextBox>&nbsp;<img
                            src="/images/icons/calendar.png" id="imgNgayCap" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </fieldset>
    <div style="width: 100%; margin-top: 10px; text-align: right;">
        <asp:LinkButton ID="lbtTruyVan" runat="server" CssClass="btn" OnClick="lbtTruyVan_Click"
            OnClientClick="return CheckValidFormSearch();"><i class="iconfa-search"></i>Tìm</asp:LinkButton>
        <a href="javascript:;" id="A4" class="btn btn-rounded" onclick="$('#DivSearch').dialog('close');">
            <i class="iconfa-off"></i>Bỏ qua</a>
    </div>
</div>
<div id="DivNoiDungHoSo">
</div>
<script type="text/javascript">
    $('#<%= txtThoiGianKiemTraTu.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgThoiGianKiemTraTu").click(function () {
        $("#<%= txtThoiGianKiemTraTu.ClientID %>").datepicker("show");
    });
    $('#<%= txtThoiGianKiemTraDen.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgThoiGianKiemTraDen").click(function () {
        $("#<%= txtThoiGianKiemTraDen.ClientID %>").datepicker("show");
    });
    
    $('#<%= txtNgayCap.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgayCap").click(function () {
        $("#<%= txtNgayCap.ClientID %>").datepicker("show");
    });
    
    $(document).ready(function() {
        $("#Form1").validate({
            onsubmit: false
        });
    });

    function CheckValidFormSearch() {
        var isValid = $("#Form1").valid();
        if(isValid) {
            $('#DivSearch').dialog('close');
            return true;
        } else {
            return false;
        }
    }
    
    $("#DivSearch").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtTruyVan.ClientID %>').attr('href'));
        }
    });

    <% CheckPermissionOnPage();%>
</script>
</form>
<form id="user_search" method="post" enctype="multipart/form-data">
    <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
    <input type="hidden" value="0" id="hdAction" name="hdAction" />
</form>