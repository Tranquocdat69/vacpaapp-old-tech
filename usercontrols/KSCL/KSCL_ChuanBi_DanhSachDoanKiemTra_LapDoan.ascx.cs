﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_DanhSachDoanKiemTra_LapDoan : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý danh sách đoàn kiểm tra";
    protected string _perOnFunc_DsDoanKiemTra = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    protected string _idDoanKiemTra = "";
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1><a href='/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra' class='MenuFuncLv1'>" + tenchucnang + @"</a>&nbsp;<img src='/images/next.png' style='margin-top:3px; height: 18px;' />&nbsp;<span class='MenuFuncLv2'>Lập đoàn kiểm tra</span></h1> </div>"));

        _perOnFunc_DsDoanKiemTra = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, cm.connstr);

        this._idDoanKiemTra = Library.CheckNull(Request.QueryString["iddoan"]);
        if (_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Xem))
        {
            if (!string.IsNullOrEmpty(_idDoanKiemTra) && Library.CheckIsInt32(_idDoanKiemTra))
            {
                DivDanhSachCongTyDaThem.Visible = true;
                DivDanhSachThanhVienDoanKT.Visible = true;
                SpanCacNutChucNang.Visible = true;
            }
            else
            {
                DivDanhSachCongTyDaThem.Visible = false;
                DivDanhSachThanhVienDoanKT.Visible = false;
                SpanCacNutChucNang.Visible = false;
            }
            try
            {
                _db.OpenConnection();
                if (Request.Form["hdAction"] != null && Request.Form["hdAction"] == "save")
                {
                    SaveThongTinDoanKiemTra(this._idDoanKiemTra);
                }
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }
        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Lấy thông tin danh sách theo ID
    /// </summary>
    /// <param name="idDanhSach">ID danh sách cần lấy dữ liệu</param>
    protected void LoadDanhSachById(string idDoan)
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(idDoan) && Library.CheckIsInt32(idDoan))
            {
                SqlKsclDoanKiemTraProvider pro = new SqlKsclDoanKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                KsclDoanKiemTra obj = pro.GetByDoanKiemTraId(Library.Int32Convert(idDoan));
                if (obj != null && obj.DoanKiemTraId > 0)
                {
                    string js = "$('#txtNgayLapDoan').val('" + obj.NgayLap.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#spanTrangThai').html('" + utility.GetStatusNameById(obj.TinhTrangId.ToString(), _db) + "');" + Environment.NewLine;
                    js += "$('#txtMaDoanKiemTra').val('" + obj.MaDoanKiemTra + "');" + Environment.NewLine;
                    js += "$('#hdDoanKiemTraID').val('" + idDoan + "');" + Environment.NewLine;
                    js += "$('#txtQuyetDinhThanhLap').val('" + obj.QuyetDinhThanhLap + "');" + Environment.NewLine;
                    js += "$('#txtDienGiai').val('" + obj.DienGiai + "');" + Environment.NewLine;
                    js += "$('#ddlDsKiemTraTrucTiep').children().remove();" + Environment.NewLine;
                    SqlKscldsKiemTraTrucTiepProvider pro2 = new SqlKscldsKiemTraTrucTiepProvider(ListName.ConnectionString, false, string.Empty);
                    KscldsKiemTraTrucTiep obj2 = pro2.GetByDsKiemTraTrucTiepId(obj.DsKiemTraTrucTiepId.Value);
                    if (obj2 != null && obj2.DsKiemTraTrucTiepId > 0)
                        js += "$('#ddlDsKiemTraTrucTiep').append('<option value=\"" + obj.DsKiemTraTrucTiepId + "\">" + obj2.MaDanhSach + "</option>');" + Environment.NewLine;
                    js += "$('#ddlDsKiemTraTrucTiep').prop('disabled', 'disabled');" + Environment.NewLine;
                    js += "GetDanhSachThanhVienTrongDoanKiemTra();" + Environment.NewLine;
                    js += "GetDanhSachCongTyDaAdd();" + Environment.NewLine;
                    js += "close_leftpanel();" + Environment.NewLine;
                    //js += "GetDanhSachCongTyDaAdd();" + Environment.NewLine; // Gọi js load danh sách các công ty đã add vào Danh Sách báo cáo
                    string statusCode = utility.GetStatusCodeById(obj.TinhTrangId.ToString(), _db).Trim();
                    if (statusCode == ListName.Status_ChoDuyet || statusCode == ListName.Status_KhongPheDuyet || statusCode == ListName.Status_ThoaiDuyet)
                    {
                        js += "$('#" + btnSendEmail.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + btnDontApprove.ClientID + "').remove();" + Environment.NewLine;
                    }
                    if (statusCode == ListName.Status_KhongPheDuyet)
                    {
                        js += "$('#" + btnReject.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + btnDontApprove.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#spanTinhTrang').css('color', 'red');" + Environment.NewLine;
                    }
                    if (statusCode == ListName.Status_DaPheDuyet)
                    {
                        js += "$('#btnSave').remove();" + Environment.NewLine;
                        js += "$('#" + btnDelete.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + btnApprove.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + btnReject.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#spanTinhTrang').css('color', 'green');" + Environment.NewLine;
                        js += "$('#btnAddCongTy').remove();" + Environment.NewLine;
                        js += "$('#btnRemoveCongTy').remove();" + Environment.NewLine;
                        js += "$('#btnAddThanhVien').remove();" + Environment.NewLine;
                        js += "$('#btnRemoveThanhVien').remove();" + Environment.NewLine;
                    }
                    Response.Write(js);
                }
            }
            else
            {
                string js = "$('#txtNgayLapDoan').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                Response.Write(js);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Lưu thông tin đoàn kiểm tra
    /// </summary>
    /// <param name="idDoan">ID đoàn kiểm tra (Có: Edit; Không: Insert)</param>
    private void SaveThongTinDoanKiemTra(string idDoan)
    {
        string strNgayLap = Request.Form["txtNgayLapDoan"];
        string quyetDinhThanhLap = Request.Form["txtQuyetDinhThanhLap"];
        string dienGiai = Request.Form["txtDienGiai"];
        string dsKiemTraTrucTiep = Request.Form["ddlDsKiemTraTrucTiep"];
        DateTime ngayLap = DateTime.Now;

        if (!string.IsNullOrEmpty(strNgayLap))
            ngayLap = Library.DateTimeConvert(strNgayLap, "dd/MM/yyyy");

        SqlKsclDoanKiemTraProvider pro = new SqlKsclDoanKiemTraProvider(ListName.ConnectionString, false, string.Empty);
        KsclDoanKiemTra obj = new KsclDoanKiemTra();
        if (!string.IsNullOrEmpty(idDoan) && Library.CheckIsInt32(idDoan)) // Update
        {
            obj = pro.GetByDoanKiemTraId(Library.Int32Convert(idDoan));
            obj.NgayLap = ngayLap;
            obj.QuyetDinhThanhLap = quyetDinhThanhLap;
            obj.DienGiai = dienGiai;
            try
            {
                _db.OpenConnection();
                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ChoDuyet, _db));
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }

            if (pro.Update(obj))
            {
                // Cập nhật vai trò của thành viên, ngày kiểm tra cho các công ty
                SaveThongTinKhac();

                cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Cập nhật bản ghi có ID \"" + obj.DoanKiemTraId + "\" của danh mục " + tenchucnang);
                Response.Redirect(Request.RawUrl);
            }
        }
        else // Insert
        {
            string tinhTrangId = utility.GetStatusID(ListName.Status_ChoDuyet, _db);
            if (Library.CheckIsInt32(tinhTrangId))
                obj.TinhTrangId = Library.Int32Convert(tinhTrangId);
            SqlKscldsKiemTraTrucTiepProvider pro2 = new SqlKscldsKiemTraTrucTiepProvider(ListName.ConnectionString, false, string.Empty);
            KscldsKiemTraTrucTiep obj2 = pro2.GetByDsKiemTraTrucTiepId(Library.Int32Convert(dsKiemTraTrucTiep));
            if (obj2 != null && obj2.DsKiemTraTrucTiepId > 0)
                obj.MaDoanKiemTra = GenMaDanhSach(obj2.MaDanhSach);
            obj.DsKiemTraTrucTiepId = Library.Int32Convert(dsKiemTraTrucTiep);
            obj.NgayLap = ngayLap;
            obj.QuyetDinhThanhLap = quyetDinhThanhLap;
            obj.DienGiai = dienGiai;
            try
            {
                _db.OpenConnection();
                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ChoDuyet, _db));
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }

            if (pro.Insert(obj))
            {
                cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Thêm bản ghi có ID \"" + obj.DoanKiemTraId + "\" của danh mục " + tenchucnang);
                Response.Redirect(Request.RawUrl + "&iddoan=" + obj.DoanKiemTraId);
            }
        }
    }

    private void SaveThongTinKhac()
    {
        SqlKsclDoanKiemTraThanhVienProvider proDoanKiemTraThanhVien = new SqlKsclDoanKiemTraThanhVienProvider(ListName.ConnectionString, false, string.Empty);
        SqlKsclDoanKiemTraCongTyProvider proDoanKiemTraCongTy = new SqlKsclDoanKiemTraCongTyProvider(ListName.ConnectionString, false, string.Empty);
        NameValueCollection nvc = Request.Form;
        string[] arrKeys = nvc.AllKeys;
        for (int i = 0; i < arrKeys.Length; i++)
        {
            string key = arrKeys[i];
            if (key.StartsWith("ddlVaiTro_")) // Update vai trò
            {
                string vaiTro = nvc[key];
                string doanKiemTraThanhVienId = key.Split('_')[1];
                if (!string.IsNullOrEmpty(doanKiemTraThanhVienId) && Library.CheckIsInt32(doanKiemTraThanhVienId))
                {
                    KsclDoanKiemTraThanhVien obj = proDoanKiemTraThanhVien.GetByDoanKiemTraThanhVienId(Library.Int32Convert(doanKiemTraThanhVienId));
                    obj.VaiTro = vaiTro;
                    proDoanKiemTraThanhVien.Update(obj);
                }
            }
            if (key.StartsWith("txtNgayBatDauKT_")) // Update ngày kiểm tra
            {
                string doanKiemTraCongTyId = key.Split('_')[1];
                if (!string.IsNullOrEmpty(doanKiemTraCongTyId) && Library.CheckIsInt32(doanKiemTraCongTyId))
                {
                    KsclDoanKiemTraCongTy obj = proDoanKiemTraCongTy.GetByDoanKiemTraCongTyId(Library.Int32Convert(doanKiemTraCongTyId));
                    if (!string.IsNullOrEmpty(nvc[key]))
                        obj.TuNgay = Library.DateTimeConvert(nvc[key], "dd/MM/yyyy");
                    if (!string.IsNullOrEmpty(nvc["txtNgayKetThucKT_" + doanKiemTraCongTyId]))
                        obj.DenNgay = Library.DateTimeConvert(nvc["txtNgayKetThucKT_" + doanKiemTraCongTyId], "dd/MM/yyyy");
                    proDoanKiemTraCongTy.Update(obj);
                }
            }
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Tạo mã đoàn kiểm tra mới
    /// </summary>
    /// <param name="maDsKiemTraTrucTiep">Mã danh sách kiểm tra trực tiếp</param>
    /// <returns></returns>
    private string GenMaDanhSach(string maDsKiemTraTrucTiep)
    {
        string maDoan = maDsKiemTraTrucTiep;
        string query = "SELECT TOP 1 MaDoanKiemTra FROM " + ListName.Table_KSCLDoanKiemTra + " WHERE MaDoanKiemTra like '" + maDoan + "%' ORDER BY DoanKiemTraID DESC";
        List<Hashtable> listData = _db.GetListData(query);
        int stt = 1;
        if (listData.Count > 0)
        {
            string temp_maDoan = listData[0]["MaDoanKiemTra"].ToString();
            if (temp_maDoan.Length == 10)
            {
                stt = Library.Int32Convert(temp_maDoan.Substring(8, 2)) + 1;
            }
        }
        if (stt > 9)
            maDoan += stt;
        else
            maDoan += "0" + stt;
        return maDoan;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        else
            LoadDanhSachById(this._idDoanKiemTra);
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Them))
        {
            if (string.IsNullOrEmpty(this._idDoanKiemTra))
                Response.Write("$('#btnSave').remove();");
        }
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Sua))
        {
            if (!string.IsNullOrEmpty(this._idDoanKiemTra))
            {
                Response.Write("$('#btnSave').remove();");
                Response.Write("$('#btnAddThanhVien').remove();");
                Response.Write("$('#btnRemoveThanhVien').remove();");
                Response.Write("$('#btnAddCongTy').remove();");
                Response.Write("$('#btnRemoveCongTy').remove();");
            }
        }
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Xoa))
            Response.Write("$('#" + btnDelete.ClientID + "').remove();");

        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Duyet))
        {
            Response.Write("$('#" + btnApprove.ClientID + "').remove();");
            Response.Write("$('#" + btnReject.ClientID + "').remove();");
            Response.Write("$('#" + btnDontApprove.ClientID + "').remove();");
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        ChangeStatus(this._idDoanKiemTra, ListName.Status_DaPheDuyet);
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        ChangeStatus(this._idDoanKiemTra, ListName.Status_KhongPheDuyet);
    }

    protected void btnDontApprove_Click(object sender, EventArgs e)
    {
        ChangeStatus(this._idDoanKiemTra, ListName.Status_ThoaiDuyet);
    }

    private void ChangeStatus(string idDoan, string statusCode)
    {
        if (!string.IsNullOrEmpty(idDoan))
        {
            SqlKsclDoanKiemTraProvider pro = new SqlKsclDoanKiemTraProvider(ListName.ConnectionString, false, string.Empty);
            KsclDoanKiemTra obj = new KsclDoanKiemTra();
            obj = pro.GetByDoanKiemTraId(Library.Int32Convert(idDoan));
            try
            {
                _db.OpenConnection();
                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(statusCode, _db));

                if (pro.Update(obj))
                {
                    cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Cập nhật trạng thái bản ghi có ID \"" + obj.DoanKiemTraId + "\" thành " + utility.GetStatusNameById(statusCode, _db) + " của danh mục " + tenchucnang);
                    if (statusCode == ListName.Status_DaPheDuyet) // Nếu là phê duyệt -> tạo hồ sơ cho các công ty trong danh sách đoàn kiểm tra
                        SaveHoSo(idDoan, obj.NgayLap.Value);

                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    js += "jQuery('#thongbaothanhcong_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Cập nhật trạng thái thành công.\").show();" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("ApproveSuccess", js);
                }
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }
        }
    }

    private void SaveHoSo(string idDoan, DateTime ngayLap)
    {
        SqlKsclHoSoProvider pro = new SqlKsclHoSoProvider(ListName.ConnectionString, false, string.Empty);
        // Lấy danh sách các công ty trong đoàn kiểm tra
        string query = @"SELECT tblKSCLDoanKiemTraCongTy.HoiVienTapTheID, MaHoiVienTapThe FROM tblKSCLDoanKiemTraCongTy 
                                INNER JOIN tblHoiVienTapThe ON tblKSCLDoanKiemTraCongTy.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
                                WHERE tblKSCLDoanKiemTraCongTy.DoanKiemTraID = " + idDoan;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                KsclHoSo obj = new KsclHoSo();
                obj.HoiVienTapTheId = Library.Int32Convert(ht["HoiVienTapTheID"]);
                obj.MaHoSo = GenMaHoSo(ht["MaHoiVienTapThe"].ToString().Trim(), ngayLap.ToString("yy"));
                obj.DoanKiemTraId = Library.Int32Convert(idDoan);
                pro.Insert(obj);
            }
        }
    }

    private string GenMaHoSo(string maCongTy, string year)
    {
        string maHoSo = "KSCL" + maCongTy + year;
        string query = "SELECT TOP 1 MaHoSo FROM " + ListName.Table_KSCLHoSo + " WHERE MaHoSo like '" + maHoSo + "%' ORDER BY HoSoID DESC";
        List<Hashtable> listData = _db.GetListData(query);
        int stt = 1;
        if (listData.Count > 0)
        {
            string temp_maHoSo = listData[0]["MaHoSo"].ToString();
            if (temp_maHoSo.Length > 0)
            {
                stt = Library.Int32Convert(temp_maHoSo.Substring(temp_maHoSo.Length - 2, 2)) + 1;
            }
        }
        if (stt > 9)
            maHoSo += stt;
        else
            maHoSo += "0" + stt;
        return maHoSo;
    }

    /// <summary>
    /// Xóa danh sách
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(_idDoanKiemTra) && Library.CheckIsInt32(_idDoanKiemTra))
            {
                SqlKsclDoanKiemTraProvider pro = new SqlKsclDoanKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                // Xóa các bản ghi chi tiết danh sách trước
                if (_db.ExecuteNonQuery(ListName.Proc_KSCL_DeleteDanhSachDoanKiemTra, new List<string> { "@DoanKiemTraID" }, new List<object> { _idDoanKiemTra }) > 0)
                {
                    if (pro.Delete(Library.Int32Convert(_idDoanKiemTra)))
                    {
                        cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Xóa bản ghi có ID \"" + _idDoanKiemTra + "\" của danh mục " + tenchucnang);
                        Response.Redirect("/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra");
                    }
                }
            }
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/31
    /// Load danh sách kiểm tra trực tiếp
    /// </summary>
    protected void LoadListDsKiemTraTrucTiep()
    {
        try
        {
            _db.OpenConnection();
            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SelectDsKiemTraTrucTiepDuDKDeLapDoanKiemTra, new List<string>(), new List<object>());
            if (listData.Count > 0)
            {
                string js = "";
                foreach (Hashtable ht in listData)
                {
                    js += "<option value='" + ht["DSKiemTraTrucTiepID"] + "'>" + ht["MaDanhSach"] + "</option>";
                }
                Response.Write(js);
            }
            else
            {
                Response.Write("<option value=''>Không có danh sách kiểm tra trực tiếp được phê duyệt!</option>");
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }


    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        SendEmail();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/06
    /// Gửi Email thông báo về kế hoạch tới kiểm tra trực tiếp.
    /// </summary>
    private void SendEmail()
    {
        try
        {
            _db.OpenConnection();

            List<string> listMailAddress = new List<string>(); // List Địa chỉ email hợp lệ sẽ được gửi
            List<string> listSubject = new List<string>();
            List<string> listContent = new List<string>();
            List<string> listHoiVienTapTheID = new List<string>();
            List<string> listLoaiHvtt = new List<string>();
            string query = @"SELECT DKTCT.HoiVienTapTheID, DKTCT.TuNgay, DKTCT.DenNgay, HVTC.MaHoiVienTapThe, HVTC.Email,LoaiHoiVienTapThe FROM tblKSCLDoanKiemTraCongTy DKTCT
                                INNER JOIN tblHoiVienTapThe HVTC ON DKTCT.HoiVienTapTheID = HVTC.HoiVienTapTheID
                                WHERE DKTCT.DoanKiemTraID = " + _idDoanKiemTra;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    string email = ht["Email"].ToString();
                    if (!string.IsNullOrEmpty(email) && Library.CheckIsValidEmail(email))
                    {
                        listHoiVienTapTheID.Add(ht["HoiVienTapTheID"].ToString());
                        if (ht["LoaiHoiVienTapThe"].ToString() == "0" || ht["LoaiHoiVienTapThe"].ToString() == "2")
                            listLoaiHvtt.Add("3");
                        else if (ht["LoaiHoiVienTapThe"].ToString() == "1")
                            listLoaiHvtt.Add("4");
                        else
                            listLoaiHvtt.Add("");
                        listMailAddress.Add(email);
                        listSubject.Add("VACPA - Thông báo kế hoạch kiểm tra trực tiếp");
                        string content = "VACPA xin thông báo kế hoạch kiểm tra trực tiếp tại công ty quý vị trong khoảng thời gian" +
                                                " " + (!string.IsNullOrEmpty(Library.CheckNull(ht["TuNgay"])) ? Library.DateTimeConvert(ht["TuNgay"]).ToString("dd/MM/yyyy") : "") + " - " + (!string.IsNullOrEmpty(Library.CheckNull(ht["DenNgay"])) ? Library.DateTimeConvert(ht["DenNgay"]).ToString("dd/MM/yyyy") : "");
                        content += "<br /><br />Có vấn đề gì thắc mắc, quý công ty xin liên hệ với VACPA để được giải đáp.";
                        listContent.Add(content);
                    }
                }
            }
            if (listMailAddress.Count > 0)
            {
                if (utility.SendEmail(listMailAddress, listSubject, listContent))
                {
                    for (int i = 0; i < listMailAddress.Count; i++)
                    {
                        cm.GuiThongBao(listHoiVienTapTheID[i], listLoaiHvtt.Count > i ? listLoaiHvtt[i] : "", listSubject[0], listContent[0]);
                    }
                    cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Gửi thư thông báo kế hoạch kiểm tra trực tiếp tới các công ty");
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    string msg = "Đã gửi Email tới các công ty trong danh sách.";
                    js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("SendEmailSuccess", js);
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }
}