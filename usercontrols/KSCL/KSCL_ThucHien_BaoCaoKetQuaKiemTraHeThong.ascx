﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ThucHien_BaoCaoKetQuaKiemTraHeThong.ascx.cs"
    Inherits="usercontrols_KSCL_ThucHien_BaoCaoKetQuaKiemTraHeThong" %>
<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .tdInput
    {
        width: 120px;
    }
</style>
<form id="Form1" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Báo cáo kết quả kiểm tra hệ thống</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none; margin-top: 5px;"
    class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <input type="hidden" name="hdBaoCaoKTHTID" id="hdBaoCaoKTHTID" />
    <input type="hidden" name="hdAction" id="hdAction" />
    <table id="tblThongTinChung" width="900px" border="0" class="formtbl">
        <tr>
            <td style="width: 180px;">
                Mã hồ sơ KSCL<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" name="txtMaHoSo" id="txtMaHoSo" onchange="CallActionGetInforHoSo();" />
                <input type="hidden" name="hdHoSoID" id="hdHoSoID" />
            </td>
            <td>
                <input type="button" value="---" onclick="OpenDanhSachHoSo();" style="border: 1px solid gray;" />
            </td>
            <td style="width: 180px;">
                Tên công ty kiểm toán:
            </td>
            <td>
                <input type="text" name="txtTenCongTyKiemToan" id="txtTenCongTyKiemToan" readonly="readonly" />
                <input type="hidden" name="hdMaCongTy" id="hdMaCongTy" />
            </td>
        </tr>
        <tr>
            <td>
                Họ và tên người đại diện:
            </td>
            <td colspan="2">
                <input type="text" name="txtNguoiDaiDien" id="txtNguoiDaiDien" readonly="readonly" />
            </td>
            <td>
                Chức vụ:
            </td>
            <td>
                <input type="text" name="txtChucVu" id="txtChucVu" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Tên thành viên chấm điểm 1<span class="starRequired">(*)</span>:
            </td>
            <td colspan="2">
                <select id="ddlThanhVienChamDiem1" name="ddlThanhVienChamDiem1">
                </select>
            </td>
            <td>
                Tên thành viên chấm điểm 2<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlThanhVienChamDiem2" name="ddlThanhVienChamDiem2">
                </select>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung về công ty được kiểm tra</legend>
    <table width="900px" border="0" class="formtbl">
        <tr>
            <td>
                Số lượng thành viên BGĐ<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtSoLuongThanhVienBGD" id="txtSoLuongThanhVienBGD" />
            </td>
            <td>
                Số lượng KTV hành nghề<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtSoLuongKTVHanhNghe" id="txtSoLuongKTVHanhNghe" />
            </td>
        </tr>
        <tr>
            <td>
                Số lượng nhân viên chuyên nghiệp/Tổng số nhân viên<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtSoLuongNhanVienChuyenNghiep" id="txtSoLuongNhanVienChuyenNghiep" />
            </td>
            <td>
                Số lượng BCKT đã phát hành trung bình một năm<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtSoLuongBCKT" id="txtSoLuongBCKT" />
            </td>
        </tr>
        <tr>
            <td>
                Hình thức lưu tài liệu, hồ sơ về hợp đồng dịch vụ<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="checkbox" id="cboFileCung" name="cboFileCung" />&nbsp;Trên file cứng
                &nbsp;&nbsp;<input type="checkbox" id="cboFileMem" name="cboFileMem" />&nbsp;Trên
                file mềm
            </td>
            <td>
                Phần mềm kiểm toán sử dụng (Nếu có)<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtPhanMemKeToan" id="txtPhanMemKeToan" />
            </td>
        </tr>
    </table>
</fieldset>
<div id="DivControlAction" style="width: 100%; margin: 10px 0px 5px 0px; text-align: center; display: none;">
    <a id="btnSave" href="javascript:;" class="btn" onclick="SubmitForm();"><i class="iconfa-save">
    </i>Lưu</a> <a id="btnNopBaoCao" href="javascript:;" class="btn" onclick="NopBaoCao();"><i
        class="iconfa-ok"></i>Nộp báo cáo</a> <a id="A3" href=""
            class="btn"><i class="iconfa-download"></i>Kết xuất</a> <a id="btnBack" href="javascript:;" 
                class="btn" onclick="OpenWindowQuanLyHoSo();"><i class="iconfa-question-sign"></i>Truy vấn lịch sử hồ sơ KSCL</a>
</div>
<fieldset class="fsBlockInfor">
    <legend>Bảng câu hỏi kiểm tra hệ thống</legend>
    <input type="hidden" id="hdListDangKyHocID" name="hdListDangKyHocID" />
    <input type="hidden" id="hdListMaHocVien" name="hdListMaHocVien" />
    <div style="text-align: center; width: 100%; margin: 10px;">
        <input type="hidden" id="hdTongDiem" name="hdTongDiem" />
        <input type="hidden" id="hdTongDiemThucTe" name="hdTongDiemThucTe" />
        <span style="font-weight: bold; margin-right: 10px;">Tổng điểm thực tế chưa quy đổi:&nbsp;&nbsp;&nbsp;
            <input type="text" readonly="readonly" id="txtTongDiemThucTeChuaQuyDoi" name="txtTongDiemThucTeChuaQuyDoi"
                style="width: 70px; font-weight: bold;" /></span> <span style="font-weight: bold;
                    margin-right: 10px;">Tổng điểm thực tế đã quy đổi:&nbsp;&nbsp;&nbsp;
                    <input type="text" readonly="readonly" id="txtTongDiemThucTeDaQuyDoi" name="txtTongDiemThucTeDaQuyDoi"
                        style="width: 70px; font-weight: bold;" /></span> <span style="font-weight: bold;
                            margin-right: 10px;">Xếp loại<span class="starRequired">(*)</span>:&nbsp;&nbsp;&nbsp;
                            <select id="ddlXepHang" name="ddlXepHang" style="width: 100px;">
                                <option value="1">Tốt</option>
                                <option value="2">Đạt yêu cầu</option>
                                <option value="3">Không đạt</option>
                                <option value="4">Yếu kém</option>
                            </select></span>
    </div>
    <table id="tblDanhSachCauHoi" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th style="width: 20px;">
                </th>
                <th>
                    STT
                </th>
                <th style="min-width: 200px;">
                    Câu hỏi
                </th>
                <th style="width: 70px;">
                    Căn cứ
                </th>
                <th style="width: 50px;">
                    Điểm tối đa
                </th>
                <th style="min-width: 250px;">
                    Hướng dẫn chấm điểm
                </th>
                <th style="width: 50px;">
                    Trả lời
                </th>
                <th style="width: 50px;">
                    Điểm thực tế
                </th>
                <th style="min-width: 100px;">
                    Ghi chú của người kiểm tra
                </th>
            </tr>
        </thead>
        <tbody id="TBodyDanhSachCauHoi">
        </tbody>
    </table>
</fieldset>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadBangCauHoi" width="0px" height="0px"></iframe>
</form>

<script type="text/javascript">
    // Validate form
    $('#Form1').validate({
        rules: {
            hdHoSoID: {
                required: true
            },
            ddlThanhVienChamDiem1: {
                required:true
            },
            ddlThanhVienChamDiem2: {
                required:true
            },
            txtSoLuongThanhVienBGD: {
                required:true, digitsWithOutSeparatorCharacterVN: true
            },
            txtSoLuongKTVHanhNghe: {
                required:true, digitsWithOutSeparatorCharacterVN: true
            },
            txtSoLuongNhanVienChuyenNghiep: {
                required:true, digitsWithOutSeparatorCharacterVN: true
            },
            txtSoLuongBCKT: {
                required:true, digitsWithOutSeparatorCharacterVN: true
            },
            txtPhanMemKeToan: {
                required:true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }  
    });

    function OpenDanhSachHoSo() {
        $("#DivDanhSachHoSo").empty();
        $("#DivDanhSachHoSo").append($("<iframe width='100%' height='100%' id='ifDanhSachHoSo' name='ifDanhSachHoSo' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_MiniList"));
        $("#DivDanhSachHoSo").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách hồ sơ kiểm soát chất lượng</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSachHoSo.ChooseHoSo();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachHoSo').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivDanhSachHoSo').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }
    
    function GetMaHoSoFromPopup(value) {
        var arrValue = value.split(';#');
        $('#txtMaHoSo').val(arrValue[1]);
        iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso='+arrValue[1]+'&action=loadinforhoso';
    }
    
    function CloseFormDanhSachHoSo() {
        $("#DivDanhSachHoSo").dialog('close');
    }
    
    function CallActionGetInforHoSo() {
        var maHoSo = $('#txtMaHoSo').val();
        iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso='+maHoSo+'&action=loadinforhoso';
    }

    function DisplayInforHoSo(value) {
        var arrValue = value.split(';#');
        if(arrValue[0].length == 0) {
            $('#txtMaHoSo').val('');
            var tbodyMain = document.getElementById('TBodyDanhSachCauHoi');
            var trs = tbodyMain.getElementsByTagName("tr");
            for (var i = 0; i < trs.length; i++) {
                trs[i].parentNode.removeChild(trs[i]);
                i--;
            }
            $('#hdBaoCaoKTHTID').val('');
            $('#ddlXepHang').val('1');
            $('#txtSoLuongThanhVienBGD').val('');
            $('#txtSoLuongKTVHanhNghe').val('');
            $('#txtSoLuongNhanVienChuyenNghiep').val('');
            $('#txtSoLuongBCKT').val('');
            
            $('#cboFileCung').prop('checked', false);
            $('#cboFileMem').prop('checked', false);
            $('#txtPhanMemKeToan').val('');
            $('#txtTongDiemThucTeChuaQuyDoi').val('');
            $('#txtTongDiemThucTeDaQuyDoi').val('');
            $('#DivControlAction').css('display', 'none');
            alert('Mã hồ sơ không đúng hoặc không tồn tại!');
        } else {
            $('#DivControlAction').css('display', '');
        }
        $('#hdHoSoID').val(arrValue[0]);
        $('#hdMaCongTy').val(arrValue[1]);
        $('#txtTenCongTyKiemToan').val(arrValue[2]);
        $('#txtNguoiDaiDien').val(arrValue[3]);
        $('#txtChucVu').val(arrValue[4]);
        var doanKiemTraId = arrValue[5];
        iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&dktid='+doanKiemTraId+'&action=loadlistthanhviendkt';
        
        // Nếu mã hồ sơ hợp lệ -> Get danh sách câu hỏi và dữ liệu đã chấm điểm nếu có
        if(arrValue[0].length > 0)
            GetDataBaoCao();
    }

    function DisplayListThanhVienDoanKiemTra(arrData) {
        // Remove toàn bộ option cũ
        $('#ddlThanhVienChamDiem1').children().remove();
        $('#ddlThanhVienChamDiem2').children().remove();
        // Add option mới
        if(arrData.length > 0) {
            for(var i = 0 ; i < arrData.length ; i ++) {
                $('#ddlThanhVienChamDiem1').append('<option value="'+arrData[i][0]+'">'+arrData[i][1]+'</option>');
                $('#ddlThanhVienChamDiem2').append('<option value="'+arrData[i][0]+'">'+arrData[i][1]+'</option>');
            }
        }
    }
    
    function SubmitForm() {
        $('#hdAction').val('update');
        $('#Form1').submit();
    }
    
    function NopBaoCao() {
        if(confirm('Bạn chắc chắn muốn nộp báo cáo này chứ?')){
            $('#hdAction').val('submit');
            $('#Form1').submit();
        }
    }
    
    function OpenWindowQuanLyHoSo() {
        window.open('/admin.aspx?page=kscl_thuchien_quanlyhosokiemsoatchatluong&macongty=' + $('#hdMaCongTy').val(), '_blank');
    }
    
    function GetDataBaoCao() {
        iframeProcess_LoadBangCauHoi.location = '/iframe.aspx?page=KSCL_ThucHien_Process&action=getlistcauhoi&loai=1&hieuluc=1&hosoid=' + $('#hdHoSoID').val();
    }
    
    function DrawDataBaoCao(dataBaoCao) {
        var arrValue = dataBaoCao.split(';#');
        $('#hdBaoCaoKTHTID').val(arrValue[0]);
        $('#ddlXepHang').val(arrValue[1]);
        $('#ddlThanhVienChamDiem1').val(arrValue[2]);
        $('#ddlThanhVienChamDiem2').val(arrValue[3]);
        $('#txtSoLuongThanhVienBGD').val(arrValue[4]);
        $('#txtSoLuongKTVHanhNghe').val(arrValue[5]);
        $('#txtSoLuongNhanVienChuyenNghiep').val(arrValue[6]);
        $('#txtSoLuongBCKT').val(arrValue[7]);
        var hinhThucLuuTaiLieu = arrValue[8];
        $('#cboFileCung').prop('checked', false);
        $('#cboFileMem').prop('checked', false);
        if(hinhThucLuuTaiLieu == "1")
            $('#cboFileCung').prop('checked', true);
        if(hinhThucLuuTaiLieu == "2")
            $('#cboFileMem').prop('checked', true);
        if(hinhThucLuuTaiLieu == "3") {
            $('#cboFileCung').prop('checked', true);
            $('#cboFileMem').prop('checked', true);
        }
        $('#txtPhanMemKeToan').val(arrValue[9]);
        var maTrangThai = arrValue[10];
        if(maTrangThai == <%= ListName.Status_DaPheDuyet %>){
            $('#btnSave').css('display', 'none');
            $('#btnNopBaoCao').css('display', 'none');
        }
        else {
            $('#btnSave').css('display', '');
            $('#btnNopBaoCao').css('display', '');
        }
    }
    
    function Draw_DanhSachCauHoi(arrData, arrDataDiemToiDa, tongSoDiem) {
        if(tongSoDiem != undefined){
            $('#hdTongDiem').val(tongSoDiem);
            $('#hdTongDiemThucTe').val(tongSoDiem);
        }
        var tbodyMain = document.getElementById('TBodyDanhSachCauHoi');
        var trs = tbodyMain.getElementsByTagName("tr");
        for (var i = 0; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            var tempNhomCauHoiCha = '';
            var parentIndex = 0; // Để lưu index của nhóm cha
            for(var i = 0; i < arrData.length; i ++) {
                var cauHoiId = arrData[i][0];
                var stt = arrData[i][1];
                var cauHoi = arrData[i][3];
                var canCu = arrData[i][4];
                var diemToiDa = arrData[i][5];
                var huongDanChamDiem = arrData[i][6];

                var traLoi = arrData[i][10];
                var diemThucTe = arrData[i][11];
                var ghiChu = arrData[i][12];
                if(cauHoiId == '') {
                    for (var j = 0; j < arrDataDiemToiDa.length; j++) {
                        if(arrDataDiemToiDa[j][0] == arrData[i][10])
                            diemToiDa = arrDataDiemToiDa[j][1];
                    }
                }
                var tr = document.createElement('TR');
                if(tempNhomCauHoiCha != '' && cauHoiId != ''){
                    tr.className = 'class_' + tempNhomCauHoiCha;
                    //tr.style.display = 'none';
                }
                if(cauHoiId == ''){
                    parentIndex = i;
                    tr.style.fontWeight = 'bold';
                    tempNhomCauHoiCha = (i + 1);
                }
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                if(cauHoiId == ''){
                    var link = document.createElement('a');
                    link.href = 'javascript:;';
                    link.innerHTML = '[-]';
                    link.id = 'Display_HiddenData_' + (i + 1);
                    link.onclick = function() { AnHienDanhSachCauHoi(this); };
                    tdCheckBox.appendChild(link);
                }
                tr.appendChild(tdCheckBox); 

                var arrColumn = [stt, cauHoi, canCu, diemToiDa, huongDanChamDiem];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    if(j == 0 || j == 3)
                        td.style.textAlign = 'center';
                    if(j == 3) {
                        td.innerHTML = "<span id='spanDiemToiDa_"+i+"'>"+arrColumn[j]+"</span><input type='hidden' id='hdDiemToiDa_"+i+"' value='"+arrColumn[j]+"'>";
                    } else {
                        td.innerHTML = arrColumn[j];
                    }
                    tr.appendChild(td); 
                }
                
                var tdTraLoi = document.createElement("TD");
                if(cauHoiId != '')
                    tdTraLoi.innerHTML = "<input type='radio' id='rbtCo_"+i+"' name='rbtTraLoi_"+i+"' value='1' onchange='CheckRadioTraLoi("+i+", "+parentIndex+");' />&nbsp; C<br /><input type='radio' id='rbtNA_"+i+"' name='rbtTraLoi_"+i+"' value='2' onchange='CheckRadioTraLoi("+i+", "+parentIndex+");' />&nbsp; N/A<br /><input type='radio' id='rbtKo_"+i+"' name='rbtTraLoi_"+i+"' value='3' checked='checked' onchange='CheckRadioTraLoi("+i+",  "+parentIndex+");' />&nbsp; K";
                tr.appendChild(tdTraLoi);
                
                var tdDiemThucTe = document.createElement("TD");
                if(cauHoiId != ''){
                    tdDiemThucTe.innerHTML = "<input type='text' id='txtDiemThucTe_"+i+"' name='txtDiemThucTe_"+i+"' readonly='readonly' value='0' class='DiemThucTe' onchange='SumTongDiemThucTeChuaQuyDoi("+i+");' />";
                    tdDiemThucTe.innerHTML += "<input type='hidden' id='hdCauHoiID_"+i+"' name='hdCauHoiID_"+i+"' value='"+cauHoiId+"'>";
                }
                tr.appendChild(tdDiemThucTe);
                
                var tdGhiChu = document.createElement("TD");
                if(cauHoiId != '')
                    tdGhiChu.innerHTML = "<textarea  id='txtGhiChu_"+i+"' name='txtGhiChu_"+i+"' style='min-width: 95px;' rows='5'></textarea>";
                tr.appendChild(tdGhiChu);
                
                tbodyMain.appendChild(tr);
                
                // Đổ dữ liệu báo cáo đã có
                if(cauHoiId != '') {
                    if(traLoi == "1"){
                        $('#rbtCo_' + i).prop('checked', true);
                        $('#txtDiemThucTe_' + i).prop('readonly', false);
                        if($('#spanDiemToiDa_' + i).html() == '0'){
                            $('#spanDiemToiDa_' + parentIndex).html((parseFloat($('#spanDiemToiDa_' + parentIndex).html()) + parseFloat($('#hdDiemToiDa_' + i).val())));
                            $('#hdTongDiemThucTe').val((parseFloat($('#hdTongDiemThucTe').val()) + parseFloat($('#hdDiemToiDa_' + i).val())));
                        }
                        $('#spanDiemToiDa_' + i).html($('#hdDiemToiDa_' + i).val());
                    }
                    if(traLoi == "2") {
                        $('#rbtNA_' + i).prop('checked', true);
                        $('#txtDiemThucTe_' + i).prop('readonly', true);
                        $('#spanDiemToiDa_' + i).html('0');
                        $('#spanDiemToiDa_' + parentIndex).html((parseFloat($('#spanDiemToiDa_' + parentIndex).html()) - parseFloat($('#hdDiemToiDa_' + i).val())));
                        $('#hdTongDiemThucTe').val((parseFloat($('#hdTongDiemThucTe').val()) - parseFloat($('#hdDiemToiDa_' + i).val())));
                    }
                    if(traLoi == "3") {
                        $('#rbtKo_' + i).prop('checked', true);
                        $('#txtDiemThucTe_' + i).prop('readonly', true);
                        if($('#spanDiemToiDa_' + i).html() == '0') {
                            $('#spanDiemToiDa_' + parentIndex).html((parseFloat($('#spanDiemToiDa_' + parentIndex).html()) + parseFloat($('#hdDiemToiDa_' + i).val())));
                            $('#hdTongDiemThucTe').val((parseFloat($('#hdTongDiemThucTe').val()) + parseFloat($('#hdDiemToiDa_' + i).val())));
                        }
                        $('#spanDiemToiDa_' + i).html($('#hdDiemToiDa_' + i).val());
                    }
                    $('#txtDiemThucTe_' + i).val(diemThucTe);
                    $('#txtGhiChu_' + i).val(ghiChu);
                }
            }
            var tongDiem = 0;
            $('.DiemThucTe').each(function () {
                var diem = $(this).val();
                if(!isNaN(diem) && diem != '') {
                    tongDiem += parseFloat(diem);
                }
            });
            $('#txtTongDiemThucTeChuaQuyDoi').val(tongDiem);
        
            // Sum tổng điểm đã quy đổi
            var a = (parseFloat(tongDiem) * parseFloat($('#hdTongDiem').val()));
            var tongDiemThucTeDaQuyDoi = (a / parseFloat($('#hdTongDiemThucTe').val()));
            $('#txtTongDiemThucTeDaQuyDoi').val(tongDiemThucTeDaQuyDoi);
        }
    }
    
    function AnHienDanhSachCauHoi(obj) {
        var id = obj.id;
        var index = id.split('Display_HiddenData_')[1];
        if($('.class_' + index).css('display') != undefined){
            if($('.class_' + index).css('display') == 'none'){
                $('#Display_HiddenData_' + index).html('[-]');
                $('.class_' + index).css('display', 'table-row');
            }
            else{
                $('#Display_HiddenData_' + index).html('[+]');
                $('.class_' + index).css('display', 'none');    
            }
        } else {
            $('#Display_HiddenData_' + index).html('[+]');
            $('.class_' + index).css('display', 'none');
        }
    }

    function CheckRadioTraLoi(index, parentIndex) {
        if($('#rbtCo_' + index).prop('checked')) {
            $('#txtDiemThucTe_' + index).prop('readonly', false);
            if($('#spanDiemToiDa_' + index).html() == '0'){
                $('#spanDiemToiDa_' + parentIndex).html((parseFloat($('#spanDiemToiDa_' + parentIndex).html()) + parseFloat($('#hdDiemToiDa_' + index).val())));
                $('#hdTongDiemThucTe').val((parseFloat($('#hdTongDiemThucTe').val()) + parseFloat($('#hdDiemToiDa_' + index).val())));
            }
            $('#spanDiemToiDa_' + index).html($('#hdDiemToiDa_' + index).val());
        } 
        if($('#rbtNA_' + index).prop('checked')){
            $('#txtDiemThucTe_' + index).val('0');
            $('#txtDiemThucTe_' + index).prop('readonly', true);
            $('#spanDiemToiDa_' + index).html('0');
            $('#spanDiemToiDa_' + parentIndex).html((parseFloat($('#spanDiemToiDa_' + parentIndex).html()) - parseFloat($('#hdDiemToiDa_' + index).val())));
            $('#hdTongDiemThucTe').val((parseFloat($('#hdTongDiemThucTe').val()) - parseFloat($('#hdDiemToiDa_' + index).val())));
        }
        if($('#rbtKo_' + index).prop('checked')) {
            $('#txtDiemThucTe_' + index).val('0');
            $('#txtDiemThucTe_' + index).prop('readonly', true);
            if($('#spanDiemToiDa_' + index).html() == '0') {
                $('#spanDiemToiDa_' + parentIndex).html((parseFloat($('#spanDiemToiDa_' + parentIndex).html()) + parseFloat($('#hdDiemToiDa_' + index).val())));
                $('#hdTongDiemThucTe').val((parseFloat($('#hdTongDiemThucTe').val()) + parseFloat($('#hdDiemToiDa_' + index).val())));
            }
            $('#spanDiemToiDa_' + index).html($('#hdDiemToiDa_' + index).val());
        }
        SumTongDiemThucTeChuaQuyDoi(index);
    }

    // Sự kiện xảy ra khi thay đổi giá trị trong ô điểm thực tế (Bắt lỗi, tính tổng)
    function SumTongDiemThucTeChuaQuyDoi(index) {
        if(index != undefined) {
            if(CheckIsNumber(document.getElementById('txtDiemThucTe_' + index))) {
                var diemToiDa = $('#hdDiemToiDa_' + index).val();
                if(CheckIsNumberPositive(document.getElementById('txtDiemThucTe_' + index))) {
                    if(!isNaN(diemToiDa)) {
                        if(parseFloat(diemToiDa) < parseFloat($('#txtDiemThucTe_' + index).val())) {
                            $('#txtDiemThucTe_' + index).val('');
                            alert('Số điểm không được lớn hơn điểm tối đa.');
                            var t = setTimeout("$('#txtDiemThucTe_' + index).focus()", 1);
                        }
                    }   
                }
            }
        }
        var tongDiem = 0;
        $('.DiemThucTe').each(function () {
            var diem = $(this).val();
            if(!isNaN(diem) && diem != '') {
                tongDiem += parseFloat(diem);
            }
        });
        $('#txtTongDiemThucTeChuaQuyDoi').val(tongDiem);
        
        // Sum tổng điểm đã quy đổi
        var a = (parseFloat(tongDiem) * parseFloat($('#hdTongDiem').val()));
        var tongDiemThucTeDaQuyDoi = (a / parseFloat($('#hdTongDiemThucTe').val()));
        $('#txtTongDiemThucTeDaQuyDoi').val(tongDiemThucTeDaQuyDoi);
        if(parseFloat(tongDiemThucTeDaQuyDoi) < 40)
            $('#ddlXepHang').val('4');
        if(parseFloat(tongDiemThucTeDaQuyDoi) >= 40 && parseFloat(tongDiemThucTeDaQuyDoi) < 60)
            $('#ddlXepHang').val('3');
        if(parseFloat(tongDiemThucTeDaQuyDoi) >= 60 && parseFloat(tongDiemThucTeDaQuyDoi) < 80)
            $('#ddlXepHang').val('2');
        if(parseFloat(tongDiemThucTeDaQuyDoi) >= 80)
            $('#ddlXepHang').val('1');
    }   
    
    
    jQuery(document).ready(function () {
        $('#tblDanhSachCauHoi').stickyTableHeaders();
    });
    
    <% CheckPermissionOnPage(); %>
</script>
<div id="DivDanhSachHoSo">
</div>
