﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_KetThuc_XuLySaiPham_UpdateHoiVienCaNhan : System.Web.UI.UserControl
{
    private string _idXuLySaiPhamChiTiet = "";
    private Db _db = new Db(ListName.ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        this._idXuLySaiPhamChiTiet = Library.CheckNull(Request.QueryString["id"]);
        if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "update")
            Save();
    }

    protected void GetOneData()
    {
        try
        {
            _db.OpenConnection();
            string query = @"SELECT XuLySaiPhamChiTietID, NoiDungSaiPham, QuyetDinhBTC, QuyetDinhVACPA, XLSPCT.HoiVienTapTheID, HVTC.TenDoanhNghiep,  XLSPCT.HoiVienCaNhanID, (HVCN.HoDem + ' ' + HVCN.Ten) TenHoiVienCaNhan FROM tblKSCLXuLySaiPhamChiTiet XLSPCT
                            LEFT JOIN tblHoiVienCaNhan HVCN ON XLSPCT.HoiVienCaNhanID = HVCN.HoiVienCaNhanID
                            LEFT JOIN tblHoiVienTapThe HVTC ON XLSPCT.HoiVienTapTheID = HVTC.HoiVienTapTheID
                            WHERE XuLySaiPhamChiTietID = " + _idXuLySaiPhamChiTiet;
            List<Hashtable> listData = _db.GetListData(query);
            if(listData.Count > 0)
            {
                Hashtable ht = listData[0];
                string js = "";
                js += "$('#spanTenHoiVien').html('" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenHoiVienCaNhan"].ToString()) + "');" + Environment.NewLine;
                js += "$('#spanDonViCongTac').html('" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"].ToString()) + "');" + Environment.NewLine;
                js += "$('#txtNoiDungSaiPham').val('" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NoiDungSaiPham"].ToString()) + "');" + Environment.NewLine;
                js += "$('#txtQuyetDinhXuLyCuaBTC').val('" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["QuyetDinhBTC"].ToString()) + "');" + Environment.NewLine;
                js += "$('#txtQuyetDinhXuLyCuaVACPA').val('" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["QuyetDinhVACPA"].ToString()) + "');" + Environment.NewLine;
                Response.Write(js);
            }
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Save()
    {
        string js = "";
        SqlKsclXuLySaiPhamChiTietProvider pro = new SqlKsclXuLySaiPhamChiTietProvider(ListName.ConnectionString, false, string.Empty);
        KsclXuLySaiPhamChiTiet obj = pro.GetByXuLySaiPhamChiTietId(Library.Int32Convert(_idXuLySaiPhamChiTiet));
        obj.NoiDungSaiPham = Request.Form["txtNoiDungSaiPham"];
        obj.QuyetDinhBtc = Request.Form["txtQuyetDinhXuLyCuaBTC"];
        obj.QuyetDinhVacpa = Request.Form["txtQuyetDinhXuLyCuaVACPA"];
        pro.Update(obj);
        js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CallAlertSuccessFloatRightBottom('Lưu thông tin xử lý sai phạm của kiểm toán viên thành công.');" + Environment.NewLine;
        js += "parent.CallActionGetInforXuLySaiPham();" + Environment.NewLine;
        js += "parent.CloseFormUpdateChoHVCN();" + Environment.NewLine;
        js += "</script>";
        Page.RegisterStartupScript("SaveSucess", js);
    }
}