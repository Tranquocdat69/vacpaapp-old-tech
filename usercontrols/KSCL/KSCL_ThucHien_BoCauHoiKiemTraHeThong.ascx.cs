﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_ThucHien_BoCauHoiKiemTraHeThong : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý bộ câu hỏi kiểm tra hệ thống";
    protected string _listPermissionOnFunc_BoCauHoiKiemTraHeThong = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected string _IDLopHoc = "";
    Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
            _listPermissionOnFunc_BoCauHoiKiemTraHeThong = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ThucHien_CauHoiKTHT, cm.connstr);
            if (_listPermissionOnFunc_BoCauHoiKiemTraHeThong.Contains("XEM|"))
            {
                hdNgayHienTai.Value = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + " !</div>"));
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }

    protected void CheckPermissionOnPage()
    {
        // Set giá trị mặc định là ngày hiện tại cho ô "Ngày lập câu hỏi"
        Response.Write("$('#txtNgayLapCauHoi').val('"+DateTime.Now.ToString("dd/MM/yyyy")+"');");

        // Gọi sự kiện load danh sách câu hỏi kiểm tra hệ thống
        Response.Write("GetDanhSachCauHoi();");

        if (!_listPermissionOnFunc_BoCauHoiKiemTraHeThong.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");

        if (!_listPermissionOnFunc_BoCauHoiKiemTraHeThong.Contains("KETXUAT|"))
            Response.Write("$('#btnExport').remove();");

        if (!_listPermissionOnFunc_BoCauHoiKiemTraHeThong.Contains("THEM|"))
            Response.Write("$('#btnAdd').remove();");

        if (!_listPermissionOnFunc_BoCauHoiKiemTraHeThong.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/15
    /// Get danh sách nhóm câu hỏi cha
    /// </summary>
    protected void GetNhomCauHoi()
    {
        try
        {
            string js = "";
            _db.OpenConnection();
            string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE Loai = 1 AND (NhomCauHoiChaID IS NULL OR NhomCauHoiChaID = '') ORDER BY NhomCauHoiID";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    js += "<option value=\"" + ht["NhomCauHoiID"] + "\">" + ht["TenNhomCauHoi"] + "</option>";
                    js = GetNhomCauHoiSub(js, ht["NhomCauHoiID"].ToString(), "&nbsp;&nbsp;&nbsp;"); // Gọi hàm đệ quy
                }
            }
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/15
    /// Get danh sách các nhóm câu hỏi con (Hàm đệ quy)
    /// </summary>
    /// <param name="js">Chuỗi html chưa dữ liệu</param>
    /// <param name="nhomCauHoiChaId">ID câu hỏi cha</param>
    /// <param name="space">Khoảng trống để lùi vào ở đầu mỗi bản ghi</param>
    protected string GetNhomCauHoiSub(string js, string nhomCauHoiChaId, string space)
    {
        string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + nhomCauHoiChaId + " ORDER BY NhomCauHoiID";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                js += "<option value=\"" + ht["NhomCauHoiID"] + "\">" + space + ht["TenNhomCauHoi"] + "</option>";
                js = GetNhomCauHoiSub(js, ht["NhomCauHoiID"].ToString(), space + "&nbsp;&nbsp;&nbsp;");
            }
        }
        return js;
    }
}