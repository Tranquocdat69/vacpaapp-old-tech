﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_DanhSachDoanKiemTra : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách đoàn kiểm tra";
    protected string _perOnFunc_DsDoanKiemTra = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _perOnFunc_DsDoanKiemTra = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, cm.connstr);
        if (_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Xem))
        {

            if (!IsPostBack || Request.Form["hdAction"] == "paging")
            {
                LoadDanhSachDoanKiemTra();
            }

        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/31
    /// Load List danh sách đoàn kiểm tra kiểm tra theo điều kiện tìm kiếm
    /// </summary>
    private void LoadDanhSachDoanKiemTra()
    {
        try
        {
            _db.OpenConnection();
            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "ID", "MaDoanKiemTra", "NgayLap", "QuyetDinhThanhLap", "MaTrangThai", "TenTrangThai" });

            List<string> listParam = new List<string> { "@MaDoanKiemTra", "@NgayLapDoanTu", "@NgayLapDoanDen", "@MaTrangThai_ChoDuyet", "@MaTrangThai_TuChoi", "@MaTrangThai_DaDuyet", 
                    "@MaTrangThai_ThoaiDuyet", "@MaCongTy", "@TenCongTy" };
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, };
            if (!string.IsNullOrEmpty(txtMaDoanKiemTra.Text))
                listValue[0] = txtMaDoanKiemTra.Text;
            if (!string.IsNullOrEmpty(txtNgayLapDoanTu.Text))
                listValue[1] = Library.ConvertDatetime(txtNgayLapDoanTu.Text, '/');
            if (!string.IsNullOrEmpty(txtNgayLapDoanDen.Text))
                listValue[2] = Library.ConvertDatetime(txtNgayLapDoanDen.Text, '/');
            if (cboChoDuyet.Checked)
                listValue[3] = ListName.Status_ChoDuyet;
            if (cboTuChoi.Checked)
                listValue[4] = ListName.Status_KhongPheDuyet;
            if (cboDaDuyet.Checked)
                listValue[5] = ListName.Status_DaPheDuyet;
            if (cboThoaiDuyet.Checked)
                listValue[6] = ListName.Status_ThoaiDuyet;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                listValue[7] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                listValue[8] = txtTenCongTy.Text;
            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SearchDanhSachDoanKiemTra, listParam, listValue);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["DoanKiemTraID"].ToString();
                    dr["MaDoanKiemTra"] = ht["MaDoanKiemTra"].ToString();
                    dr["QuyetDinhThanhLap"] = ht["QuyetDinhThanhLap"].ToString();
                    dr["NgayLap"] = Library.DateTimeConvert(ht["NgayLap"]);
                    string maTrangThai = ht["MaTrangThai"].ToString().Trim();
                    dr["MaTrangThai"] = maTrangThai;
                    dr["TenTrangThai"] = ht["TenTrangThai"].ToString();
                    if (maTrangThai == ListName.Status_DaPheDuyet)
                        dr["TenTrangThai"] = "<span style='color:green'>" + ht["TenTrangThai"] + "</span>";
                    if (maTrangThai == ListName.Status_KhongPheDuyet)
                        dr["TenTrangThai"] = "<span style='color:red'>" + ht["TenTrangThai"] + "</span>";
                    dt.Rows.Add(dr);
                }
            }
            DataView dv = dt.DefaultView;
            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"] + " " + ViewState["sortdirection"];
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            Pager.Items.Clear();
            for (int i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
                hf_TrangHienTai.Value = Request.Form["tranghientai"];
            if (!string.IsNullOrEmpty(hf_TrangHienTai.Value))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(hf_TrangHienTai.Value);
                Pager.SelectedIndex = Convert.ToInt32(hf_TrangHienTai.Value);
            }
            gv_DanhSachDoanKiemTra.DataSource = objPds;
            gv_DanhSachDoanKiemTra.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void gv_DanhSachDoanKiemTra_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_DanhSachDoanKiemTra.PageIndex = e.NewPageIndex;
        gv_DanhSachDoanKiemTra.DataBind();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Them))
            Response.Write("$('#btnOpenFormInsert').remove();");
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Sua))
            Response.Write("$('a[data-original-title=\"Xem/Sửa\"]').remove();");
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Xoa))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
            Response.Write("$('#" + btnDelete.ClientID + "').remove();");
        }
        if (!_perOnFunc_DsDoanKiemTra.Contains(ListName.PERMISSION_Duyet))
        {
            Response.Write("$('#" + btnApprove.ClientID + "').remove();");
            Response.Write("$('#" + btnReject.ClientID + "').remove();");
            Response.Write("$('#" + btnDontApprove.ClientID + "').remove();");
        }
    }

    protected void gv_DanhSachDoanKiemTra_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hfMaTrangThai = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("hfMaTrangThai") as HiddenField;
            LinkButton lbtDelete = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("btnDeleteInList") as LinkButton;
            if (hfMaTrangThai != null && lbtDelete != null)
                if (hfMaTrangThai.Value == ListName.Status_DaPheDuyet)
                    lbtDelete.Visible = false;
        }
    }

    protected void gv_DanhSachDoanKiemTra_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // Xóa từng bản ghi khi bấm nút "Xóa" trên danh sách
        if (e.CommandName == "delete_danhsach")
        {
            string idDanhSach = e.CommandArgument.ToString();
            DeleteDanhSach(new List<string> { idDanhSach });
        }
    }

    /// <summary>
    /// Xóa danh sách đoàn kiểm tra
    /// </summary>
    /// <param name="listIdDanhSach">List ID danh sách</param>
    private void DeleteDanhSach(List<string> listIdDanhSach)
    {
        try
        {
            _db.OpenConnection();
            string maDanhSachKoXoaDuoc = "";
            string idTrangThai = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
            foreach (string idDanhSach in listIdDanhSach)
            {
                if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach))
                {
                    SqlKsclDoanKiemTraProvider pro = new SqlKsclDoanKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                    // Kiểm tra tình trạng của bản ghi, nếu đã được duyệt thì không cho xóa
                    KsclDoanKiemTra obj = pro.GetByDoanKiemTraId(Library.Int32Convert(idDanhSach));
                    if (obj != null && obj.TinhTrangId == Library.Int32Convert(idTrangThai))
                    {
                        maDanhSachKoXoaDuoc += obj.MaDoanKiemTra + ", ";
                        continue;
                    }

                    // Xóa các bản ghi chi tiết danh sách trước
                    if (_db.ExecuteNonQuery(ListName.Proc_KSCL_DeleteDanhSachDoanKiemTra, new List<string> { "@DoanKiemTraID" }, new List<object> { idDanhSach }) > 0)
                    {
                        if (pro.Delete(Library.Int32Convert(idDanhSach)))
                        {
                            cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Xóa bản ghi có ID \"" + idDanhSach + "\" của danh mục " + tenchucnang);
                        }
                    }
                }
            }
            maDanhSachKoXoaDuoc = maDanhSachKoXoaDuoc.Trim().TrimEnd(',');
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã xóa những bản ghi dữ liệu hợp lệ.";
            if (!string.IsNullOrEmpty(maDanhSachKoXoaDuoc))
                msg += "<br /><span style=\"color:red;\">Không được phép xóa danh sách mã " + maDanhSachKoXoaDuoc + ".</span>";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("DeleteSuccess", js);
            LoadDanhSachDoanKiemTra();
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Xóa những bản ghi được chọn khi bấm nút "Xóa đồng loạt" ở trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachDoanKiemTra.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachDoanKiemTra.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        DeleteDanhSach(listIdDanhSach);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Duyệt những bản ghi được chọn khi bấm nút "Duyệt đồng loạt" ở phía trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachDoanKiemTra.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachDoanKiemTra.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        ChangeStatus(listIdDanhSach, ListName.Status_DaPheDuyet, "duyệt");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Từ chối những bản ghi được chọn khi bấm nút "Từ chối đồng loạt ở phía trên"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReject_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachDoanKiemTra.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachDoanKiemTra.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        ChangeStatus(listIdDanhSach, ListName.Status_KhongPheDuyet, "từ chối");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Từ chối những bản ghi được chọn khi bấm nút "Thoái duyệt đồng loạt ở phía trên"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDontApprove_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachDoanKiemTra.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachDoanKiemTra.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        ChangeStatus(listIdDanhSach, ListName.Status_ThoaiDuyet, "thoái duyệt");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/31
    /// Cập nhật trạng thái của những bản ghi được chọn
    /// </summary>
    /// <param name="listId">List ID đoàn kiểm tra</param>
    /// <param name="statusCode">Mã trạng thái muốn cập nhật</param>
    /// <param name="action">Tên hành động (VD: duyệt, từ chối)</param>
    private void ChangeStatus(List<string> listId, string statusCode, string action)
    {
        try
        {
            _db.OpenConnection();
            string maDanhSachKoCapNhatDuoc = "";
            string idTrangThai_PheDuyet = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
            foreach (string idDoan in listId)
            {
                if (!string.IsNullOrEmpty(idDoan))
                {
                    SqlKsclDoanKiemTraProvider pro = new SqlKsclDoanKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                    KsclDoanKiemTra obj = new KsclDoanKiemTra();
                    obj = pro.GetByDoanKiemTraId(Library.Int32Convert(idDoan));
                    // Kiểm tra tình trạng của bản ghi, nếu đã được duyệt thì không cho xử lý nữa
                    if (statusCode == ListName.Status_KhongPheDuyet || statusCode == ListName.Status_ChoDuyet || statusCode == ListName.Status_DaPheDuyet)
                        if (obj != null && obj.TinhTrangId == Library.Int32Convert(idTrangThai_PheDuyet))
                        {
                            maDanhSachKoCapNhatDuoc += obj.MaDoanKiemTra + ", ";
                            continue;
                        }
                    if (statusCode == ListName.Status_ThoaiDuyet)
                        if (obj != null && obj.TinhTrangId != Library.Int32Convert(idTrangThai_PheDuyet))
                        {
                            maDanhSachKoCapNhatDuoc += obj.MaDoanKiemTra + ", ";
                            continue;
                        }
                    obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(statusCode, _db));
                    if (pro.Update(obj))
                    {
                        if (statusCode == ListName.Status_DaPheDuyet)// Nếu là phê duyệt -> tạo hồ sơ cho các công ty trong danh sách đoàn kiểm tra
                            SaveHoSo(idDoan, obj.NgayLap.Value);
                        cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Cập nhật trạng thái bản ghi có ID \"" + obj.DoanKiemTraId + "\" thành " + utility.GetStatusNameById(statusCode, _db) + " của danh mục " + tenchucnang);
                    }
                }
            }
            maDanhSachKoCapNhatDuoc = maDanhSachKoCapNhatDuoc.Trim().TrimEnd(',');
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã " + action + " những bản ghi dữ liệu hợp lệ.";
            if (!string.IsNullOrEmpty(maDanhSachKoCapNhatDuoc))
                msg += "<br /><span style=\"color:red;\">Không được phép " + action + " đoàn kiểm tra với mã " + maDanhSachKoCapNhatDuoc + ".</span>";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("ApproveSuccess", js);
            LoadDanhSachDoanKiemTra();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void SaveHoSo(string idDoan, DateTime ngayLap)
    {
        SqlKsclHoSoProvider pro = new SqlKsclHoSoProvider(ListName.ConnectionString, false, string.Empty);
        // Lấy danh sách các công ty trong đoàn kiểm tra
        string query = @"SELECT tblKSCLDoanKiemTraCongTy.HoiVienTapTheID, MaHoiVienTapThe FROM tblKSCLDoanKiemTraCongTy 
                                INNER JOIN tblHoiVienTapThe ON tblKSCLDoanKiemTraCongTy.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
                                WHERE tblKSCLDoanKiemTraCongTy.DoanKiemTraID = " + idDoan;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                KsclHoSo obj = new KsclHoSo();
                obj.HoiVienTapTheId = Library.Int32Convert(ht["HoiVienTapTheID"]);
                obj.MaHoSo = GenMaHoSo(ht["MaHoiVienTapThe"].ToString().Trim(), ngayLap.ToString("yy"));
                obj.DoanKiemTraId = Library.Int32Convert(idDoan);
                pro.Insert(obj);
            }
        }
    }

    private string GenMaHoSo(string maCongTy, string year)
    {
        string maHoSo = "KSCL" + maCongTy + year;
        string query = "SELECT TOP 1 MaHoSo FROM " + ListName.Table_KSCLHoSo + " WHERE MaHoSo like '" + maHoSo + "%' ORDER BY HoSoID DESC";
        List<Hashtable> listData = _db.GetListData(query);
        int stt = 1;
        if (listData.Count > 0)
        {
            string temp_maHoSo = listData[0]["MaHoSo"].ToString();
            if (temp_maHoSo.Length > 0)
            {
                stt = Library.Int32Convert(temp_maHoSo.Substring(temp_maHoSo.Length - 2, 2)) + 1;
            }
        }
        if (stt > 9)
            maHoSo += stt;
        else
            maHoSo += "0" + stt;
        return maHoSo;
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadDanhSachDoanKiemTra();

        txtMaDoanKiemTra.Text = "";
        txtNgayLapDoanTu.Text = "";
        txtNgayLapDoanDen.Text = "";
        cboChoDuyet.Checked = true;
        cboDaDuyet.Checked = true;
        cboTuChoi.Checked = true;
        cboThoaiDuyet.Checked = true;
        txtMaCongTy.Text = "";
        txtTenCongTy.Text = "";
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDoanKiemTra = new List<string>();
        for (int i = 0; i < gv_DanhSachDoanKiemTra.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachDoanKiemTra.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDoanKiemTra.Add(checkbox.Value);
        }
        SendEmail(listIdDoanKiemTra);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/06
    /// Gửi Email thông báo về kế hoạch tới kiểm tra trực tiếp.
    /// </summary>
    private void SendEmail(List<string> listIdDoanKiemTra)
    {
        try
        {
            _db.OpenConnection();

            List<string> listMailAddress = new List<string>(); // List Địa chỉ email hợp lệ sẽ được gửi
            List<string> listSubject = new List<string>();
            List<string> listContent = new List<string>();
            List<string> listHoiVienTapTheID = new List<string>();
            List<string> listLoaiHvtt = new List<string>();
            string listMaDoanKiemTraKoHopLe = ""; // List chứa những mã đoàn kiểm tra chưa được phê duyệt để gửi Email
            foreach (string idDoanKiemTra in listIdDoanKiemTra)
            {
                string query = @"SELECT DKTCT.HoiVienTapTheID, DKTCT.TuNgay, DKTCT.DenNgay, HVTC.MaHoiVienTapThe, HVTC.Email,LoaiHoiVienTapThe, tblDMTrangThai.MaTrangThai, DKT.MaDoanKiemTra FROM tblKSCLDoanKiemTraCongTy DKTCT
                                INNER JOIN tblHoiVienTapThe HVTC ON DKTCT.HoiVienTapTheID = HVTC.HoiVienTapTheID
                                INNER JOIN tblKSCLDoanKiemTra DKT ON DKTCT.DoanKiemTraID = DKT.DoanKiemTraID
                                LEFT JOIN tblDMTrangThai ON DKT.TinhTrangID = tblDMTrangThai.TrangThaiID
                                WHERE DKTCT.DoanKiemTraID = " + idDoanKiemTra;
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    if (listData[0]["MaTrangThai"].ToString().Trim() == ListName.Status_DaPheDuyet)
                    {
                        foreach (Hashtable ht in listData)
                        {
                            string email = ht["Email"].ToString();
                            if (!string.IsNullOrEmpty(email) && Library.CheckIsValidEmail(email))
                            {
                                listHoiVienTapTheID.Add(ht["HoiVienTapTheID"].ToString());
                                if (ht["LoaiHoiVienTapThe"].ToString() == "0" || ht["LoaiHoiVienTapThe"].ToString() == "2")
                                    listLoaiHvtt.Add("3");
                                else if (ht["LoaiHoiVienTapThe"].ToString() == "1")
                                    listLoaiHvtt.Add("4");
                                else
                                    listLoaiHvtt.Add("");
                                listMailAddress.Add(email);
                                listSubject.Add("VACPA - Thông báo kế hoạch kiểm tra trực tiếp");
                                string content = "VACPA xin thông báo kế hoạch kiểm tra trực tiếp tại công ty quý vị trong khoảng thời gian" +
                                                " " + (!string.IsNullOrEmpty(Library.CheckNull(ht["TuNgay"])) ? Library.DateTimeConvert(ht["TuNgay"]).ToString("dd/MM/yyyy") : "") + " - " + (!string.IsNullOrEmpty(Library.CheckNull(ht["DenNgay"])) ? Library.DateTimeConvert(ht["DenNgay"]).ToString("dd/MM/yyyy") : "");
                                content += "<br /><br />Có vấn đề gì thắc mắc, quý công ty xin liên hệ với VACPA để được giải đáp.";
                                listContent.Add(content);
                            }
                        }
                    }
                    else
                    {
                        listMaDoanKiemTraKoHopLe += listData[0]["MaDoanKiemTra"] + ", ";
                    }
                }
            }
            if (listMailAddress.Count > 0)
            {
                if (utility.SendEmail(listMailAddress, listSubject, listContent))
                {
                    for (int i = 0; i < listMailAddress.Count; i++)
                    {
                        cm.GuiThongBao(listHoiVienTapTheID[i], listLoaiHvtt.Count > i ? listLoaiHvtt[i] : "", listSubject[0], listContent[0]);
                    }
                    cm.ghilog(ListName.Func_KSCL_ChuanBi_DSDoanKiemTra, "Gửi thư thông báo kế hoạch kiểm tra trực tiếp tới các công ty");
                    listMaDoanKiemTraKoHopLe = listMaDoanKiemTraKoHopLe.Trim().TrimEnd(',');
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    string msg = "Đã gửi Email tới các công ty trong danh sách.";
                    if (!string.IsNullOrEmpty(listMaDoanKiemTraKoHopLe))
                        msg += "<br /><span style=\"color:red;\">Không thể gửi email với đoàn kiểm tra chưa được phê duyệt với mã " + listMaDoanKiemTraKoHopLe + ".</span>";
                    js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("SendEmailSuccess", js);
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }
    protected void gv_DanhSachDoanKiemTra_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        LoadDanhSachDoanKiemTra();
    }
}