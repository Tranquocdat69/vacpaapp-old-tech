﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_DanhSachCongTyKTTheoYeuCauKiemTraVuViec.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_DanhSachCongTyKTTheoYeuCauKiemTraVuViec" %>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" ClientIDMode="Static" name="Form1" runat="server">
<h4 class="widgettitle">
    Danh sách công ty kiểm toán kiểm tra theo yêu cầu</h4>
<div class="dataTables_length">
    <a id="btnOpenFormInsert" href="/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec_lapdanhsach"
        class="btn btn-rounded"><i class="iconfa-plus-sign"></i>Thêm mới</a>
    <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn" OnClick="btnDelete_Click"
        OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-trash"></i>Xóa</asp:LinkButton>
    <a href="javascript:;" id="btn_search" class="btn btn-rounded"
                onclick="OpenFormSearch();"><i class="iconfa-search"></i>Tìm kiếm</a>
</div>
<div>
    <fieldset class="fsBlockInfor">
        <legend>Danh sách công ty kiểm toán kiểm tra theo yêu cầu</legend>
        <asp:GridView ClientIDMode="Static" ID="gv_DanhSachCongTyTheoYCKTVuViec" runat="server"
            AutoGenerateColumns="False" class="table table-bordered responsive dyntable"
            AllowPaging="False" AllowSorting="True" OnPageIndexChanging="gv_DanhSachCongTyTheoYCKTVuViec_PageIndexChanging"
            OnRowCommand="gv_DanhSachCongTyTheoYCKTVuViec_RowCommand" 
            onrowdatabound="gv_DanhSachCongTyTheoYCKTVuViec_RowDataBound" 
            onsorting="gv_DanhSachCongTyTheoYCKTVuViec_Sorting">
            <Columns>
                <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
                    ItemStyle-Width="30px">
                    <ItemTemplate>
                        <input type="checkbox" id="checkbox" runat="server" class="colcheckbox" value='<%# Eval("ID")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mã danh sách" SortExpression="MaDanhSach">
                    <ItemTemplate>
                        <%# Eval("MaDanhSach")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày lập danh sách" SortExpression="NgayLap">
                    <ItemTemplate>
                        <%# Library.DateTimeConvert(Eval("NgayLap")).ToString("dd/MM/yyyy")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số lượng công ty" SortExpression="SoCongTy">
                    <ItemTemplate>
                        <%# Eval("SoCongTy")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Thao tác" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <div class="btn-group">
                            <a href='/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec_lapdanhsach&iddanhsach=<%# Eval("ID") %>'
                                data-placement="top" data-rel="tooltip" data-original-title="Xem/Sửa" rel="tooltip"
                                class="btn"><i class="iconsweets-create"></i></a>
                            <asp:LinkButton ID="btnDeleteInList" runat="server" CommandArgument='<%# Eval("ID") %>'
                                CommandName="delete_danhsach" data-placement="top" data-rel="tooltip" data-original-title="Xóa"
                                rel="tooltip" class="btn" OnClientClick="return confirm('Bạn chắc chắn muốn thực hiện thao tác này không?');"><i class="iconsweets-trashcan"></i></asp:LinkButton>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hf_TrangHienTai" runat="server" />
        <div class="dataTables_info" id="dyntable_info">
    <div class="pagination pagination-mini">
        Chuyển đến trang:
        <ul>
            <li><a href="javascript:;" onclick="$('#tranghientai').val(0);$('#hdAction').val('paging'); $('#user_search').submit();"><<
                Đầu tiên</a></li>
            <li><a href="javascript:;" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager option:selected').val()-1); $('#hdAction').val('paging'); $('#user_search').submit();}">
                < Trước</a>
            </li>
            <li><a style="border: none; background-color: #eeeeee">
                <asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" Style="width: 55px;
                    height: 22px;" onchange="$('#tranghientai').val(this.value);$('#hdAction').val('paging'); $('#user_search').submit();">
                </asp:DropDownList>
            </a></li>
            <li style="margin-left: 5px;"><a href="javascript:;" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) { $('#tranghientai').val(parseInt($('#Pager option:selected').val())+1); $('#hdAction').val('paging'); $('#user_search').submit();} ;"
                style="border-left: 1px solid #ccc;">Sau ></a></li>
            <li><a href="javascript:;" onclick="$('#tranghientai').val($('#Pager option').length-1);$('#hdAction').val('paging'); $('#user_search').submit();">
                Cuối cùng >></a></li>
        </ul>
    </div>
</div>
    </fieldset>
</div>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    // Hiển thị tooltip
    if (jQuery('#gv_DanhSachCongTyTheoYCKTVuViec').length > 0) jQuery('#gv_DanhSachCongTyTheoYCKTVuViec').tooltip({ selector: "a[rel=tooltip]" });

    jQuery(document).ready(function () {
        // dynamic table      

        $("#gv_DanhSachCongTyTheoYCKTVuViec .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#gv_DanhSachCongTyTheoYCKTVuViec :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });

    });
    // Created by NGUYEN MANH HUNG - 2014/12/12
    // Hàm mở Dialog tìm kiếm
    function OpenFormSearch() {
        $("#DivSearch").dialog({
            resizable: true,
            width: 800,
            height: 420,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm danh sách công ty kiểm toán kiểm tra theo yêu cầu</b>",
            modal: true,
            zIndex: 1000
        });
        $("#DivSearch").parent().appendTo($("form:first"));
    }

    function CheckHasChooseItem() {
        var flag = false;
        $('#gv_DanhSachCongTyTheoYCKTVuViec :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all') {
                flag = true;
                return;
            }
        });
        if (!flag)
            alert('Phải chọn ít nhất một bản ghi để thực hiện thao tác!');
        return flag;
    }
</script>
<div id="DivSearch" style="display: none;">
    <fieldset class="fsBlockInfor">
        <legend>Thông tin tìm kiếm</legend>
        <table width="100%" border="0">
            <tr>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Mã danh sách:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMaDanhSach" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ngày lập danh sách:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNgayLapBatDau" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtNgayLapKetThuc" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 49%;">
                    
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset class="fsBlockInforMini" style="width: 95%; height: 100px;">
                        <legend>Tiêu chí công ty đủ điều kiện kiểm toán cho đơn vị có lợi ích công chúng</legend>
                        <asp:CheckBox ID="cboDuDieuKienKTCK" runat="server" />Trong lĩnh vực chứng khoán<br />
                        <asp:CheckBox ID="cboDuDieuKienKTKhac" runat="server" />Đơn vị có lợi ích công chúng
                        khác
                    </fieldset>
                </td>
                <td>
                    <fieldset class="fsBlockInforMini" style="width: 95%; height: 100px;">
                        <legend>Tiêu chí công ty phải nộp báo cáo tự kiểm tra</legend>
                        <table style="width: 100%;">
                        <tr>
                            <td>
                                Xếp loại năm trước:
                            </td>
                            <td>
                                <asp:CheckBox ID="cboXepHang1" runat="server" />Tốt&nbsp;<asp:CheckBox ID="cboXepHang2"
                                    runat="server" />Đạt yêu cầu<br />
                                <asp:CheckBox ID="cboXepHang3" runat="server" />Không đạt yêu cầu&nbsp;<asp:CheckBox
                                    ID="cboXepHang4" runat="server" />Yếu kém<br />
                            </td>
                        </tr>
                    </table>
                        <asp:CheckBox ID="cboTieuChi_Khac" runat="server" />Các tiêu chí khác
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Mã HVTC/CTKT:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMaCongTy" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Loại hình công ty:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlLoaiHinh" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Doanh thu:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDoanhThuTu" runat="server" Width="100px" onchange="CheckIsNumber(this);"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtDoanhThuDen" runat="server" Width="100px" onchange="CheckIsNumber(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Tên công ty kiểm toán:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTenCongTy" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Vùng miền:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlVungMien" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Số năm chưa được kiểm tra:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTuNam" runat="server" Width="60px" onchange="CheckIsInteger(this);"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtDenNam" runat="server" Width="60px" onchange="CheckIsInteger(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </fieldset>
    <div style="width: 100%; margin-top: 10px; text-align: right;">
        <asp:LinkButton ID="lbtTruyVan" runat="server" CssClass="btn" OnClick="lbtTruyVan_Click"
            OnClientClick="return CheckValidFormSearch();"><i class="iconfa-search"></i>Tìm</asp:LinkButton>
        <a href="javascript:;" id="A4" class="btn btn-rounded" onclick="$('#DivSearch').dialog('close');">
            <i class="iconfa-off"></i>Bỏ qua</a>
    </div>
</div>
<script type="text/javascript">
    $('#<%= txtNgayLapBatDau.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $('#<%= txtNgayLapKetThuc.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $(document).ready(function() {
        $("#Form1").validate({
            onsubmit: false
        });
    });

    function CheckValidFormSearch() {
        var isValid = $("#Form1").valid();
        if(isValid) {
            $('#DivSearch').dialog('close');
            return true;
        } else {
            return false;
        }
    }
    
    $("#DivSearch").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtTruyVan.ClientID %>').attr('href'));
        }
    });

    <% CheckPermissionOnPage();%>
</script>
</form>
<form id="user_search" method="post" enctype="multipart/form-data">
    <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
    <input type="hidden" value="0" id="hdAction" name="hdAction" />
</form>