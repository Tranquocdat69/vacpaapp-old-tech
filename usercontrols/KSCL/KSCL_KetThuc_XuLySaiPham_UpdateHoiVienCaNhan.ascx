﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_KetThuc_XuLySaiPham_UpdateHoiVienCaNhan.ascx.cs"
    Inherits="usercontrols_KSCL_KetThuc_XuLySaiPham_UpdateHoiVienCaNhan" %>
<style type="text/css">
    .tdInput
    {
        width: 120px;
    }
</style>
<form id="Form1" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
                                                                                                                                                 margin-top: 5px;" class="alert alert-success">
</div>
<div id="Div1" style="width: 100%; margin: 5px 0px;">
    <a id="btnAdd" href="javascript:;" class="btn" onclick="SubmitForm();"><i class="iconfa-save">
    </i>Lưu</a> <a id="A4" href="javascript:;" class="btn" onclick="parent.CloseFormUpdateChoHVCN();">
        <i class="iconfa-off"></i>Đóng</a>
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <input type="hidden" name="hdAction" id="hdAction" />
    <input type="hidden" name="hdCongTyID" id="hdCongTyID" />
    <input type="hidden" name="hdXuLySaiPhamCongTyID" id="hdXuLySaiPhamCongTyID" />
    <table id="tblThongTinChung" width="600px" border="0" class="formtbl">
        <tr>
            <td style="width: 180px;">Tên:</td>
            <td><span id="spanTenHoiVien"></span></td>
        </tr>
        <tr>
            <td>Đơn vị công tác hiện tại:</td>
            <td><span id="spanDonViCongTac"></span></td>
        </tr>
        <tr>
            <td>
                Nội dung sai phạm:
            </td>
            <td>
                <textarea rows="5" name="txtNoiDungSaiPham" id="txtNoiDungSaiPham"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                Quyết định xử lý của BTC:
            </td>
            <td>
                <textarea rows="5" name="txtQuyetDinhXuLyCuaBTC" id="txtQuyetDinhXuLyCuaBTC"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                Quyết định xử lý của VACPA:
            </td>
            <td>
                <textarea rows="5" name="txtQuyetDinhXuLyCuaVACPA" id="txtQuyetDinhXuLyCuaVACPA"></textarea>
            </td>
        </tr>
    </table>
</fieldset>
</form>
<script type="text/javascript">
    function SubmitForm() {
        $('#hdAction').val('update');
        $('#Form1').submit();
    }

    function Delete() {
        if (confirm('Bạn chắc chắn muốn xóa báo cáo này chứ?')) {
            $('#hdAction').val('delete');
            $('#Form1').submit();
        }
    }
    <% GetOneData();%>
</script>