﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_ThucHien_TongHopKetQuaKiemTra : System.Web.UI.UserControl
{
    protected string tenchucnang = "Tổng hợp kết quả kiểm tra";
    protected string _listPermissionOnFunc_TongHopKetQuaKiemTra = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected string _IDLopHoc = "";
    Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
            _listPermissionOnFunc_TongHopKetQuaKiemTra = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ThucHien_TongHopKetQuaKT, cm.connstr);
            if (_listPermissionOnFunc_TongHopKetQuaKiemTra.Contains("XEM|"))
            {
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "update")
                    Save();
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "delete")
                    Delete();
            }
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + " !</div>"));
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_TongHopKetQuaKiemTra.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");

        if (!_listPermissionOnFunc_TongHopKetQuaKiemTra.Contains("KETXUAT|"))
            Response.Write("$('#btnExport').remove();");

        if (!_listPermissionOnFunc_TongHopKetQuaKiemTra.Contains("THEM|"))
            Response.Write("$('#btnSave').remove();");

        if (!_listPermissionOnFunc_TongHopKetQuaKiemTra.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");
    }

    private void Save()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            SqlKsclBaoCaoTongHopProvider pro = new SqlKsclBaoCaoTongHopProvider(ListName.ConnectionString, false, string.Empty);
            NameValueCollection nvc = Request.Form;

            if (!string.IsNullOrEmpty(nvc["hdHoSoID"]))
            {
                KsclBaoCaoTongHop obj;
                string baoCaoTongHopId = "";
                string query = "SELECT BaoCaoTongHopID FROM " + ListName.Table_KSCLBaoCaoTongHop + " WHERE HoSoID = " + nvc["hdHoSoID"];
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                    baoCaoTongHopId = listData[0]["BaoCaoTongHopID"].ToString();
                if (string.IsNullOrEmpty(baoCaoTongHopId)) // insert
                {
                    obj = new KsclBaoCaoTongHop();
                    obj.HoSoId = Library.Int32Convert(nvc["hdHoSoID"]);
                }
                else
                {
                    obj = pro.GetByBaoCaoTongHopId(Library.Int32Convert(baoCaoTongHopId));
                }
                obj.XepLoaiKt = nvc["ddlXepLoaiKyThuat"];
                obj.XepLoaiChung = nvc["ddlXepLoaiChung"];
                obj.KetLuan = nvc["txtKetLuanChung"];
                HttpPostedFile file = Request.Files["FileBienBan"];
                if (file.ContentLength > 0)
                {
                    BinaryReader br = new BinaryReader(file.InputStream);
                    byte[] fileByte = br.ReadBytes(file.ContentLength);
                    obj.BienBanKscl = fileByte;
                    obj.TenFileBienBanKscl = file.FileName;
                }
                if (string.IsNullOrEmpty(baoCaoTongHopId)) // insert
                    pro.Insert(obj);
                else
                    pro.Update(obj);

                js = "<script type='text/javascript'>" + Environment.NewLine;
                js += "CallAlertSuccessFloatRightBottom('Lưu báo cáo tổng hợp kết quả kiểm tra thành công.');" + Environment.NewLine;
                js += "$('#txtMaHoSo').val('" + nvc["txtMaHoSo"] + "');" + Environment.NewLine;
                js += "iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso=" + nvc["txtMaHoSo"] + "&action=loadinforhoso';" + Environment.NewLine;
                js += "</script>";
                Page.RegisterStartupScript("SaveSucess", js);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Delete()
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(Request.Form["hdHoSoID"]))
            {
                string query = "SELECT BaoCaoTongHopID FROM " + ListName.Table_KSCLBaoCaoTongHop + " WHERE HoSoID = " + Request.Form["hdHoSoID"];
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    string baoCaoTongHopId = listData[0]["BaoCaoTongHopID"].ToString();
                    SqlKsclBaoCaoTongHopProvider pro = new SqlKsclBaoCaoTongHopProvider(ListName.ConnectionString, false, string.Empty);
                    // Kiểm tra ngày xóa < ngày kết thúc kiểm tra
                    bool flag = false; // Biến để xác định có được xóa dữ liệu ko
                    SqlKsclHoSoProvider proHoSo = new SqlKsclHoSoProvider(ListName.ConnectionString, false, string.Empty);
                    KsclHoSo objHoSo = proHoSo.GetByHoSoId(Library.Int32Convert(Request.Form["hdHoSoID"]));
                    if(objHoSo != null && objHoSo.HoSoId > 0)
                    {
                        if (objHoSo.DenNgayThucTe.HasValue && DateTime.Now > objHoSo.DenNgayThucTe.Value.AddDays(1))
                        {
                            string js = "<script type='text/javascript'>" + Environment.NewLine;
                            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Hồ sơ đã hoàn thành. Không được phép xóa dữ liệu tổng hợp kết quả kiểm tra.\").show();" + Environment.NewLine;
                            js += "$('#txtMaHoSo').val('" + Request.Form["txtMaHoSo"] + "');" + Environment.NewLine;
                            js += "iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso=" + Request.Form["txtMaHoSo"] + "&action=loadinforhoso';" + Environment.NewLine;
                            js += "</script>";
                            Page.RegisterStartupScript("DeleteFail", js);
                            return;
                        }
                    }

                    if(pro.Delete(Library.Int32Convert(baoCaoTongHopId)))
                    {
                        string js = "<script type='text/javascript'>" + Environment.NewLine;
                        js += "jQuery('#thongbaothanhcong_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Đã xóa báo cáo tổng hợp kết quả kiểm tra.\").show();" + Environment.NewLine;
                        js += "</script>";
                        Page.RegisterStartupScript("DeleteSuccess", js);
                    }
                    else
                    {
                        string js = "<script type='text/javascript'>" + Environment.NewLine;
                        js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Không thể xóa báo cáo tổng hợp kết quả kiểm tra.\").show();" + Environment.NewLine;
                        js += "</script>";
                        Page.RegisterStartupScript("DeleteFail", js);
                    }
                }
                else
                {
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Hồ sơ này chưa được tổng hợp báo cáo hoặc không tìm thấy báo cáo tổng hợp.\").show();" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("DeleteFail", js);
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }
}