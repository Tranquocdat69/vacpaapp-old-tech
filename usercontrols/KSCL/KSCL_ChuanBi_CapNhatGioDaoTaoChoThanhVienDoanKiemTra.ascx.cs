﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_CapNhatGioDaoTaoChoThanhVienDoanKiemTra : System.Web.UI.UserControl
{
    protected string tenchucnang = "Cập nhật giờ CNKT cho thành viên đoàn kiểm tra";
    protected string _perOnFunc_GioCNKTThanhVienDoanKT = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    protected string _idDoanKiemTra = "";
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _perOnFunc_GioCNKTThanhVienDoanKT = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ChuanBi_GioDTThanhVienDoanKT, cm.connstr);

        this._idDoanKiemTra = Library.CheckNull(Request.QueryString["iddoan"]);
        if (_perOnFunc_GioCNKTThanhVienDoanKT.Contains(ListName.PERMISSION_Xem))
        {
            try
            {
                _db.OpenConnection();
                if (!string.IsNullOrEmpty(Request.Form["ddlDoanKiemTra"]))
                {
                    SaveSoGioCNKT();
                }
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }
        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Lấy thông tin danh sách theo ID
    /// </summary>
    /// <param name="idDanhSach">ID danh sách cần lấy dữ liệu</param>
    protected void FirstLoadPage()
    {
        string js = "$('#txtNgayCapNhat').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
        js += "GetDanhSachThanhVienTrongDoanKiemTra();" + Environment.NewLine;
        //js += "close_leftpanel();" + Environment.NewLine;
        Response.Write(js);
    }

    private void SaveSoGioCNKT()
    {
        SqlKsclDoanKiemTraThanhVienProvider proDoanKiemTraThanhVien = new SqlKsclDoanKiemTraThanhVienProvider(ListName.ConnectionString, false, string.Empty);
        NameValueCollection nvc = Request.Form;
        string[] arrKeys = nvc.AllKeys;
        for (int i = 0; i < arrKeys.Length; i++)
        {
            string key = arrKeys[i];
            if (key.StartsWith("txtSoGioKT_"))
            {
                string doanKiemTraThanhVienId = key.Split('_')[1];
                string soGioKT = nvc["txtSoGioKT_" + doanKiemTraThanhVienId];
                string soGioDD = nvc["txtSoGioDD_" + doanKiemTraThanhVienId];
                string soGioKhac = nvc["txtSoGioKhac_" + doanKiemTraThanhVienId];
                if (!string.IsNullOrEmpty(doanKiemTraThanhVienId) && Library.CheckIsInt32(doanKiemTraThanhVienId))
                {
                    KsclDoanKiemTraThanhVien obj = proDoanKiemTraThanhVien.GetByDoanKiemTraThanhVienId(Library.Int32Convert(doanKiemTraThanhVienId));
                    obj.SoGioKt = Library.DecimalConvert(soGioKT);
                    obj.SoGioDd = Library.DecimalConvert(soGioDD);
                    obj.SoGioKhac = Library.DecimalConvert(soGioKhac);
                    obj.NgayCapNhat = Library.DateTimeConvert(Request.Form["txtNgayCapNhat"], "dd/MM/yyyy");
                    proDoanKiemTraThanhVien.Update(obj);
                }
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        string msg = "Đã cập nhật số giờ CNKT của các thành viên trong đoàn kiểm tra.";
        js += "$('#ddlDoanKiemTra option').prop('selected', false).filter('[value=\"" + Request.Form["ddlDoanKiemTra"] + "\"]').prop('selected', true);" + Environment.NewLine;
        js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
        js += "</script>";
        Page.RegisterStartupScript("SendEmailSuccess", js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_GioCNKTThanhVienDoanKT.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        else
            FirstLoadPage();
        if (!_perOnFunc_GioCNKTThanhVienDoanKT.Contains(ListName.PERMISSION_Them))
        {
            if (string.IsNullOrEmpty(this._idDoanKiemTra))
                Response.Write("$('#btnSave').remove();");
        }
        if (!_perOnFunc_GioCNKTThanhVienDoanKT.Contains(ListName.PERMISSION_Sua))
        {
            if (!string.IsNullOrEmpty(this._idDoanKiemTra))
            {
                Response.Write("$('#btnSave').remove();");
            }
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/31
    /// Load danh sách kiểm tra trực tiếp
    /// </summary>
    protected void LoadListDoanKiemTra()
    {
        try
        {
            _db.OpenConnection();
            string query = @"SELECT DoanKiemTraID, MaDoanKiemTra FROM tblKSCLDoanKiemTra DKT
                                    LEFT JOIN tblDMTrangThai ON DKT.TinhTrangID = tblDMTrangThai.TrangThaiID
                                    WHERE tblDMTrangThai.MaTrangThai = " + ListName.Status_DaPheDuyet + @"
                                    ORDER BY DoanKiemTraID DESC";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                string js = "";
                foreach (Hashtable ht in listData)
                {
                    js += "<option value='" + ht["DoanKiemTraID"] + "'>" + ht["MaDoanKiemTra"] + "</option>";
                }
                Response.Write(js);
            }
            else
            {
                Response.Write("<option value=''>Không có đoàn kiểm tra được phê duyệt!</option>");
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }
}