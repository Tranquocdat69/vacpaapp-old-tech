﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_DanhSachCongTyKTTheoYeuCauKiemTraVuViec : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách công ty kiểm toán kiểm tra theo yêu cầu";
    protected string _perOnFunc_DSCongTyKTTheoVuViec = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _perOnFunc_DSCongTyKTTheoVuViec = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ChuanBi_DSCongTyKTTheoVuViec, cm.connstr);
        if (_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Xem))
        {

            if (!IsPostBack || Request.Form["hdAction"] == "paging")
            {
                LoadDanhSachVungMien();
                LoadDanhSachLoaiHinhCongTy();
                LoadDanhSachCongTyKiemToanTheoYeuCauKiemTraVuViec();
            }

        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/19
    /// Load List danh sách công ty kiểm toán kiểm tra theo yêu cầu theo điều kiện tìm kiếm
    /// </summary>
    private void LoadDanhSachCongTyKiemToanTheoYeuCauKiemTraVuViec()
    {
        try
        {
            _db.OpenConnection();
            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "ID", "MaDanhSach", "NgayLap", "SoCongTy" });

            List<string> listParam = new List<string>{"@DuDKKT_CK", "@DuDKKT_Khac", "@MaHocVienTapThe", "@TenDoanhNghiep", "@LoaiHinhDoanhNghiepID",
                    "@VungMienID", "@DoanhThuTu", "@DoanhThuDen", "@NamHienTai", "@XepHang", "@NgayBatDau", "@NgayKetThuc", "@MaDanhSach", "@NgayLapBatDau", "@NgayLapKetThuc"};
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DateTime.Now.Year, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value };
            // Dựa theo các tiêu chí tìm kiếm -> đưa giá trị tham số tương ứng vào store
            if (cboDuDieuKienKTCK.Checked)
                listValue[0] = 1;
            if (cboDuDieuKienKTKhac.Checked)
                listValue[1] = 1;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                listValue[2] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                listValue[3] = txtTenCongTy.Text;
            if (ddlLoaiHinh.SelectedValue != "")
                listValue[4] = ddlLoaiHinh.SelectedValue;
            if (ddlVungMien.SelectedValue != "")
                listValue[5] = ddlVungMien.SelectedValue;
            if (!string.IsNullOrEmpty(txtDoanhThuTu.Text) && Library.CheckIsDecimal(txtDoanhThuTu.Text))
                listValue[6] = txtDoanhThuTu.Text;
            if (!string.IsNullOrEmpty(txtDoanhThuDen.Text) && Library.CheckIsDecimal(txtDoanhThuDen.Text))
                listValue[7] = txtDoanhThuDen.Text;
            if (!cboTieuChi_Khac.Checked)
            {
                string xepHang = "";
                if (cboXepHang1.Checked)
                    xepHang += "1,";
                if (cboXepHang2.Checked)
                    xepHang += "2,";
                if (cboXepHang3.Checked)
                    xepHang += "3,";
                if (cboXepHang4.Checked)
                    xepHang += "4,";
                listValue[9] = xepHang;
            }
            if (!string.IsNullOrEmpty(txtTuNam.Text) && Library.CheckIsInt32(txtTuNam.Text))
                listValue[10] = txtTuNam.Text + "-1-1";
            if (!string.IsNullOrEmpty(txtDenNam.Text) && Library.CheckIsInt32(txtDenNam.Text))
                listValue[11] = txtDenNam.Text + "-12-31";
            if (!string.IsNullOrEmpty(txtMaDanhSach.Text))
                listValue[12] = txtMaDanhSach.Text;
            if (!string.IsNullOrEmpty(txtNgayLapBatDau.Text) && Library.CheckDateTime(txtNgayLapBatDau.Text, "dd/MM/yyyy"))
                listValue[13] = Library.ConvertDatetime(txtNgayLapBatDau.Text, '/');
            if (!string.IsNullOrEmpty(txtNgayLapKetThuc.Text) && Library.CheckDateTime(txtNgayLapKetThuc.Text, "dd/MM/yyyy"))
                listValue[14] = Library.ConvertDatetime(txtNgayLapKetThuc.Text, '/');

            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec, listParam, listValue);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["DSYeuCauKiemTraVuViecID"].ToString();
                    dr["MaDanhSach"] = ht["MaDanhSach"].ToString();
                    dr["NgayLap"] = Library.DateTimeConvert(ht["NgayLap"]);
                    dr["SoCongTy"] = ht["SoCongTy"].ToString();
                    dt.Rows.Add(dr);
                }
            }
            DataView dv = dt.DefaultView;
            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"] + " " + ViewState["sortdirection"];
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            Pager.Items.Clear();
            for (int i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
                hf_TrangHienTai.Value = Request.Form["tranghientai"];
            if (!string.IsNullOrEmpty(hf_TrangHienTai.Value))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(hf_TrangHienTai.Value);
                Pager.SelectedIndex = Convert.ToInt32(hf_TrangHienTai.Value);
            }
            gv_DanhSachCongTyTheoYCKTVuViec.DataSource = objPds;
            gv_DanhSachCongTyTheoYCKTVuViec.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void gv_DanhSachCongTyTheoYCKTVuViec_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_DanhSachCongTyTheoYCKTVuViec.PageIndex = e.NewPageIndex;
        gv_DanhSachCongTyTheoYCKTVuViec.DataBind();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        if (!_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Them))
            Response.Write("$('#btnOpenFormInsert').remove();");
        if (!_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Sua))
            Response.Write("$('a[data-original-title=\"Xem/Sửa\"]').remove();");
        if (!_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Xoa))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
            Response.Write("$('#" + btnDelete.ClientID + "').remove();");
        }
    }

    protected void gv_DanhSachCongTyTheoYCKTVuViec_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // Xóa từng bản ghi khi bấm nút "Xóa" trên danh sách
        if (e.CommandName == "delete_danhsach")
        {
            string idDanhSach = e.CommandArgument.ToString();
            DeleteDanhSach(new List<string> { idDanhSach });
        }
    }

    /// <summary>
    /// Xóa danh sách công ty kiểm toán kiểm tra theo yêu cầu
    /// </summary>
    /// <param name="listIdDanhSach">List ID danh sách</param>
    private void DeleteDanhSach(List<string> listIdDanhSach)
    {
        try
        {
            _db.OpenConnection();
            foreach (string idDanhSach in listIdDanhSach)
            {
                if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach))
                {
                    SqlKscldsKiemTraVuViecProvider pro = new SqlKscldsKiemTraVuViecProvider(ListName.ConnectionString, false, string.Empty);
                    // Xóa các bản ghi chi tiết danh sách trước
                    string deleteCommand = "DELETE FROM " + ListName.Table_KSCLDSKiemTraVuViecChiTiet + " WHERE DSYeuCauKiemTraVuViecID = " + idDanhSach;
                    if (_db.ExecuteNonQuery(deleteCommand) > 0)
                    {
                        if (pro.Delete(Library.Int32Convert(idDanhSach)))
                        {
                            cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTTheoVuViec, "Xóa bản ghi có ID \"" + idDanhSach + "\" của danh mục " + tenchucnang);
                        }
                    }
                }
            }
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã xóa những bản ghi dữ liệu hợp lệ.";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("DeleteSuccess", js);
            LoadDanhSachCongTyKiemToanTheoYeuCauKiemTraVuViec();
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Xóa những bản ghi được chọn khi bấm nút "Xóa đồng loạt" ở trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTyTheoYCKTVuViec.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTyTheoYCKTVuViec.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        DeleteDanhSach(listIdDanhSach);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách vùng miền đổ vào Dropdownlist
    /// </summary>
    private void LoadDanhSachVungMien()
    {
        ddlVungMien.Items.Clear();
        ddlVungMien.Items.Add(new ListItem("Tất cả", ""));
        SqlDmVungMienProvider vungMienPro = new SqlDmVungMienProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmVungMien> listData = vungMienPro.GetAll();
        foreach (DmVungMien dmVungMien in listData)
        {
            ddlVungMien.Items.Add(new ListItem(dmVungMien.TenVungMien, dmVungMien.VungMienId.ToString()));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách loại hình công ty
    /// </summary>
    private void LoadDanhSachLoaiHinhCongTy()
    {
        ddlLoaiHinh.Items.Clear();
        ddlLoaiHinh.Items.Add(new ListItem("Tất cả", ""));
        SqlDmLoaiHinhDoanhNghiepProvider loaiHinhPro = new SqlDmLoaiHinhDoanhNghiepProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmLoaiHinhDoanhNghiep> listData = loaiHinhPro.GetAll();
        foreach (DmLoaiHinhDoanhNghiep loaiHinh in listData)
        {
            ddlLoaiHinh.Items.Add(new ListItem(loaiHinh.TenLoaiHinhDoanhNghiep, loaiHinh.LoaiHinhDoanhNghiepId.ToString()));
        }
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadDanhSachCongTyKiemToanTheoYeuCauKiemTraVuViec();

        txtMaDanhSach.Text = "";
        txtNgayLapBatDau.Text = "";
        txtNgayLapKetThuc.Text = "";
        cboDuDieuKienKTCK.Checked = false;
        cboDuDieuKienKTKhac.Checked = false;
        cboXepHang1.Checked = false;
        cboXepHang2.Checked = false;
        cboXepHang3.Checked = false;
        cboXepHang4.Checked = false;
        cboTieuChi_Khac.Checked = false;
        txtMaCongTy.Text = "";
        if (ddlLoaiHinh.Items.Count > 0)
            ddlLoaiHinh.SelectedIndex = 0;
        txtDoanhThuTu.Text = "";
        txtDoanhThuDen.Text = "";
        txtTenCongTy.Text = "";
        if (ddlVungMien.Items.Count > 0)
            ddlVungMien.SelectedIndex = 0;
        txtTuNam.Text = "";
        txtDenNam.Text = "";
    }

    protected void gv_DanhSachCongTyTheoYCKTVuViec_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        // Code đoạn kiểm tra xem công ty đã lập danh sách kiểm tra trực tiếp hay chưa -> Đã lập thì ẩn nút xóa đi
    }
    protected void gv_DanhSachCongTyTheoYCKTVuViec_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        LoadDanhSachCongTyKiemToanTheoYeuCauKiemTraVuViec();
    }
}