﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_ChuanBi_DanhSachCongTyTuKiemTra_Process : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    Utility utility = new Utility();
    Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string idDanhSach = Library.CheckNull(Request.QueryString["iddanhsach"]);
            string action = Library.CheckNull(Request.QueryString["action"]);

            if (action == "loaddsctdaadd")
                LoadDanhSachCongTyDaAdd(idDanhSach);
            if (action == "addcompany")
            {
                string listIdCongTy = Library.CheckNull(Request.QueryString["listid"]);
                AddCompanyToList(idDanhSach, listIdCongTy);
            }
            if (action == "removecompany")
            {
                string listIdChiTietDanhSach = Library.CheckNull(Request.QueryString["listidchitietdanhsach"]);
                RemoveCompanyFromList(listIdChiTietDanhSach);
            }
            if (action == "sendmail")
            {
                string listCompanyNeedAdditional = Library.CheckNull(Request.QueryString["listcompany"]);
                SendEmail(idDanhSach, listCompanyNeedAdditional);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Load danh sách nhưng công ty đã được thêm vào danh sách theo ID danh sách
    /// </summary>
    /// <param name="idDanhSach">ID danh sách</param>
    private void LoadDanhSachCongTyDaAdd(string idDanhSach)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        string query = @"SELECT a.DSBaoCaoTuKiemTraChiTietID, b.MaHoiVienTapThe, b.TenDoanhNghiep, b.TenVietTat, b.DiaChi, b.Email, b.DienThoai, b.NguoiDaiDienPL_Ten, b.NguoiDaiDienPL_DiDong  FROM tblKSCLDSBaoCaoTuKiemTraChiTiet AS a
                             LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID
                             WHERE DSBaoCaoTuKiemTraID = " + idDanhSach;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in listData)
            {
                js += "arrData[" + index + "] = ['" + ht["DSBaoCaoTuKiemTraChiTietID"] + "', '" + ht["MaHoiVienTapThe"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + "'," +
                      " '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenVietTat"]) + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["DiaChi"]) + "', '" + ht["Email"] + "', '" + ht["DienThoai"] + "', '" + ht["NguoiDaiDienPL_Ten"] + "'," +
                      " '" + ht["NguoiDaiDienPL_DiDong"] + "'];" + Environment.NewLine;
                index++;
            }

        }
        js += "parent.DrawData_DangKyCongTyDaAdd(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Add công ty vào danh sách tự kiểm tra
    /// </summary>
    /// <param name="idDanhSach">ID danh sách tự kiểm tra</param>
    /// <param name="listIdCongTy">Danh sách ID công ty</param>
    private void AddCompanyToList(string idDanhSach, string listIdCongTy)
    {
        SqlKscldsBaoCaoTuKiemTraChiTietProvider pro = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(listIdCongTy) && !string.IsNullOrEmpty(idDanhSach))
        {
            string[] arrIdCongTy = listIdCongTy.Split(',');
            for (int i = 0; i < arrIdCongTy.Length; i++)
            {
                if (!string.IsNullOrEmpty(arrIdCongTy[i]))
                {
                    KscldsBaoCaoTuKiemTraChiTiet obj = new KscldsBaoCaoTuKiemTraChiTiet();
                    obj.HoiVienTapTheId = Library.Int32Convert(arrIdCongTy[i]);
                    obj.DsBaoCaoTuKiemTraId = Library.Int32Convert(idDanhSach);
                    obj.TinhTrangGuiEmailThongBao = "2";
                    pro.Insert(obj);
                }
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CloseDanhSachCongTyKiemSoat();" + Environment.NewLine;
        js += "parent.GetDanhSachCongTyDaAdd();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Xóa công ty khỏi danh sách tự kiểm tra
    /// </summary>
    /// <param name="listIdChiTietDanhSach">Danh sách ID của các bản ghi chi tiết báo cáo tự kiểm tra</param>
    private void RemoveCompanyFromList(string listIdChiTietDanhSach)
    {
        SqlKscldsBaoCaoTuKiemTraChiTietProvider pro = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(listIdChiTietDanhSach))
        {
            string[] arrIdCongTy = listIdChiTietDanhSach.Split(',');
            for (int i = 0; i < arrIdCongTy.Length; i++)
            {
                if (!string.IsNullOrEmpty(arrIdCongTy[i]))
                {
                    pro.Delete(Library.Int32Convert(arrIdCongTy[i]));
                }
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.GetDanhSachCongTyDaAdd();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Gửi Email tới các công ty trong danh sách yêu cầu nộp báo cáo tự kiểm tra
    /// </summary>
    private void SendEmail(string listIdDanhSach, string strCompanyNeedAdditional)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        string listCompanyFail = "";
        strCompanyNeedAdditional = strCompanyNeedAdditional.TrimEnd(',');
        List<string> listCompanyNeedAdditional = strCompanyNeedAdditional.Split(',').ToList<string>(); // Danh sách ID các bản ghi báo cáo chi tiết đã đc gửi Email những sẽ đc gửi tiếp lần nữa
        try
        {
            if (!string.IsNullOrEmpty(listIdDanhSach))
            {
                SqlKscldsBaoCaoTuKiemTraChiTietProvider proChiTiet = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
                SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                //KscldsBaoCaoTuKiemTra obj = pro.GetByDsBaoCaoTuKiemTraId(Library.Int32Convert(idDanhSach));

                string clause = "";
                string[] arrIdDanhSach = listIdDanhSach.Split(',');
                foreach (string idDanhSach in arrIdDanhSach)
                {
                    if (!string.IsNullOrEmpty(idDanhSach.Trim()))
                    {
                        if (!string.IsNullOrEmpty(clause))
                            clause += " OR ";
                        clause += "a.DSBaoCaoTuKiemTraID = " + idDanhSach;
                    }
                }
                if (!string.IsNullOrEmpty(clause))
                    clause = " (" + clause + ")";

                string query = "SELECT a.DSBaoCaoTuKiemTraChiTietID, a.TinhTrangGuiEmailThongBao, Email, LoaiHoiVienTapThe, b.HoiVienTapTheID, TenDoanhNghiep, c.HanNop FROM tblKSCLDSBaoCaoTuKiemTraChiTiet AS a LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID" +
                               " LEFT JOIN tblKSCLDSBaoCaoTuKiemTra AS c ON a.DSBaoCaoTuKiemTraID = c.DSBaoCaoTuKiemTraID WHERE " + clause;
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    List<string> listMailAddress = new List<string>(); // List Địa chỉ email hợp lệ sẽ được gửi
                    List<string> listIDSentSuccess = new List<string>(); // List ID bản ghi chi tiết danh sách tự kiểm tra đc gưi Email -> dùng để Cập nhật lại trạng thái gửi Email
                    List<string> listSubject = new List<string>();
                    List<string> listContent = new List<string>();
                    List<string> listHoiVienTapTheID = new List<string>();
                    List<string> listLoaiHvtt = new List<string>();
                    foreach (Hashtable ht in listData)
                    {
                        string emailAddress = ht["Email"].ToString();
                        string dSBaoCaoTuKiemTraChiTietID = ht["DSBaoCaoTuKiemTraChiTietID"].ToString();
                        string tinhTrangGuiEmailThongBao = ht["TinhTrangGuiEmailThongBao"].ToString();
                        if (string.IsNullOrEmpty(tinhTrangGuiEmailThongBao) || tinhTrangGuiEmailThongBao == "2" || listCompanyNeedAdditional.Contains(dSBaoCaoTuKiemTraChiTietID))
                        {
                            if (!string.IsNullOrEmpty(emailAddress) && Library.CheckIsValidEmail(emailAddress))
                            {
                                listHoiVienTapTheID.Add(ht["HoiVienTapTheID"].ToString());
                                if (ht["LoaiHoiVienTapThe"].ToString() == "0" || ht["LoaiHoiVienTapThe"].ToString() == "2")
                                    listLoaiHvtt.Add("3");
                                else if (ht["LoaiHoiVienTapThe"].ToString() == "1")
                                    listLoaiHvtt.Add("4");
                                else
                                    listLoaiHvtt.Add("");
                                listIDSentSuccess.Add(ht["DSBaoCaoTuKiemTraChiTietID"].ToString());
                                listMailAddress.Add(emailAddress);
                                listSubject.Add("VACPA: Thông báo nộp báo cáo tự kiểm tra");
                                listContent.Add("Yêu cầu đơn vị gửi thông báo tự kiểm tra. Hạn nộp " + Library.DateTimeConvert(ht["HanNop"]).ToString("dd/MM/yyyy"));
                            }
                            else
                            {
                                listCompanyFail += Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + ";#";
                            }
                        }
                    }
                    if (listMailAddress.Count > 0)
                    {
                        if (utility.SendEmail(listMailAddress, listSubject, listContent))
                        {
                            for (int i = 0; i < listMailAddress.Count; i++)
                            {
                                cm.GuiThongBao(listHoiVienTapTheID[i], listLoaiHvtt.Count > i ? listLoaiHvtt[i] : "", listSubject[0], listContent[0]);
                            }
                            
                            foreach (string id in listIDSentSuccess)
                            {
                                KscldsBaoCaoTuKiemTraChiTiet objChiTiet = proChiTiet.GetByDsBaoCaoTuKiemTraChiTietId(Library.Int32Convert(id));
                                if (objChiTiet != null && objChiTiet.DsBaoCaoTuKiemTraChiTietId > 0)
                                {
                                    objChiTiet.TinhTrangGuiEmailThongBao = "1";
                                    proChiTiet.Update(objChiTiet);
                                }
                            }
                            js += "parent.SendEmail_Success(1, '" + listCompanyFail + "');"; // Thành công
                        }
                        else
                            js += "parent.SendEmail_Success(0, '" + listCompanyFail + "');"; // Thất bại
                    }
                }
            }
        }
        catch
        {
            js += "parent.SendEmail_Success(0, '" + listCompanyFail + "');";
        }
        js += "</script>";
        Response.Write(js);
    }
}