﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_CapNhat.ascx.cs"
    Inherits="usercontrols_KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_CapNhat" %>
    <script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<style type="text/css">
    .tdNoiDungSoLieu
    {
        font-weight: bold;
    }
</style>
<form id="Form1" name="Form1" clientidmode="Static" runat="server" method="post"
enctype="multipart/form-data">
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<div style="width: 100%; margin-top: 10px;">
    <a id="btnSave" href="javascript:;" class="btn btn-rounded" onclick="Save();"><i
        class="iconfa-save"></i>Lưu</a>&nbsp;<a id="A1" href="javascript:;" class="btn btn-rounded"
                    onclick="parent.CloseFormNoiDungHoSo();"><i class="iconfa-off"></i>Đóng</a>
</div>
<table style="width: 100%;">
    <tr>
        <td style="width: 49%;">
            <fieldset class="fsBlockInfor">
                <legend>Thông tin chung</legend>
                <table style="width: 100%;" border="0" class="formtblInforWithoutBorder">
                    <tr>
                        <td>
                            Mã hồ sơ:
                        </td>
                        <td class="tdNoiDungSoLieu">
                            <span id="spanMaHoSo"></span>
                            <input type="hidden" id="hdHoSoID" name="hdHoSoID" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tình trạng:
                        </td>
                        <td class="tdNoiDungSoLieu">
                            <span id="spanTinhTrang"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tên công ty kiểm toán:
                        </td>
                        <td class="tdNoiDungSoLieu">
                            <span id="spanTenCongTyKiemToan"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mã đoàn kiểm tra:
                        </td>
                        <td class="tdNoiDungSoLieu">
                            <span id="spanMaDoanKiemTra"></span>&nbsp;<input type="button" id="Button2" value="Chi tiết"
                                onclick="OpenFormThongTinDoanKiemTra();" style="border: 1px solid gray;" />
                                <input type="hidden" id="hdDoanKiemTraID"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Thời gian kiểm tra kế hoạch:
                        </td>
                        <td class="tdNoiDungSoLieu">
                            <span id="spanThoiGianKeHoach"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Thời gian kiểm tra thực tế:
                        </td>
                        <td>
                            <input type="text" id="txtTuNgayThucTe" name="txtTuNgayThucTe" style="width: 70px;"
                                onchange="CheckNgayKiemTraThucTe();" />
                            <img src="/images/icons/calendar.png" id="imgTuNgayThucTe" />
                            &nbsp;-&nbsp;
                            <input type="text" id="txtDenNgayThucTe" name="txtDenNgayThucTe" style="width: 70px;"
                                onchange="CheckNgayKiemTraThucTe();" />
                            <img src="/images/icons/calendar.png" id="imgDenNgayThucTe" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ghi chú:
                        </td>
                        <td>
                            <textarea id="txtGhiChu" name="txtGhiChu" rows="5"></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
        <td style="width: 49%; padding-left: 5px;">
            <fieldset class="fsBlockInfor">
                <legend>Tài liệu</legend>
                <table style="width: 100%;" border="0" class="formtblInforWithoutBorder">
                    <tr>
                        <td>
                            <input type="file" id="FileTaiLieu" name="FileTaiLieu" style="width: 220px;" />&nbsp;
                            <a id="A2" href="javascript:;" class="btn" onclick="UploadFile();"><i class="iconfa-plus-sign">
                            </i>Thêm tài liệu</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a id="A3" href="javascript:;" class="btn" onclick="DeleteCauHoi();"><i class="iconfa-trash">
                            </i>Xóa tài liệu</a>
                            <table id="tblDanhSachFile" style="width: 100%; margin-top: 5px;" border="0" class="formtbl">
                                <thead>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                            STT
                                        </th>
                                        <th>
                                            Tên tài liệu
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="TBodyDanhSachFile"></tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>
</table>
<iframe name="iframeProcess" width="0px" height="0px;"></iframe>
<iframe name="iframeProcess_ThanhVien" width="0px" height="0px;"></iframe>
<iframe name="iframeProcess_UploadFile" width="0px" height="0px;" src="/iframe.aspx?page=KSCL_ThucHien_Process">
</iframe>
<div id="divThongTinDoanKiemTra" style="display: none;">
    <table id="tblDanhSachThanhVienDaAdd" width="100%" border="0" class="formtbl">
            <thead>
                <tr>
                    
                    <th>
                        STT
                    </th>
                    <th>
                        Tên thành viên
                    </th>
                    <th>
                        Năm sinh
                    </th>
                    <th>
                        Chức vụ
                    </th>
                    <th>
                        Đơn vị công tác
                    </th>
                    <th>
                        Số chứng chỉ kiểm toán viên
                    </th>
                    <th>
                        Ngày cấp
                    </th>
                    <th>
                        Số lần đã tham gia đoàn kiểm tra trực tiếp
                    </th>
                    <th>
                        Vai trò
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
</div>
</form>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#txtTuNgayThucTe").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgTuNgayThucTe").click(function () {
        $("#txtTuNgayThucTe").datepicker("show");
    });
    
    $("#txtDenNgayThucTe").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgDenNgayThucTe").click(function () {
        $("#txtDenNgayThucTe").datepicker("show");
    });
    
    function CheckNgayKiemTraThucTe() {
        var start;
        var end;
            start = $('#txtTuNgayThucTe').datepicker('getDate');
            end = $('#txtDenNgayThucTe').datepicker('getDate');
        
        if(start != null && end != null) {
            end.setDate(end.getDate() + 1);
            var minus = (end - start) / 1000 / 60 / 60 / 24;
            if(minus <= 0){
                $('#txtDenNgayThucTe').val('');
                alert('Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc!');
            }
        }
    }
    
    function UploadFile() {
        var uploadFile = document.getElementById('FileTaiLieu');
        window.iframeProcess_UploadFile.UpdateFile(uploadFile,  $('#hdHoSoID').val());
    }

    function GetListUpload() {
        iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&hosoid=' + $('#hdHoSoID').val() + '&action=loadlistfile';
    }

    function DrawData_DanhSachFile(arrData) {
        var tbodyMain = document.getElementById('TBodyDanhSachFile');
        var trs = tbodyMain.getElementsByTagName("tr");
        for (var i = 0; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var stt = (i + 1);
                var hoSoFileId = arrData[i][0];
                var maTaiLieu = arrData[i][1];
                var tenFile = arrData[i][2];
                var maHoSo_IDBaoCao = arrData[i][3];
                var typeBaoCao = arrData[i][4];
                
                var tr = document.createElement('TR');
                var tdCheckBox = document.createElement("TD");
                if(hoSoFileId.length > 0){
                    tdCheckBox.style.textAlign = 'center';
                    var checkbox = document.createElement('input');
                    checkbox.type = "checkbox";
                    checkbox.value = i + ',' + hoSoFileId;
                    tdCheckBox.appendChild(checkbox);
                }
                tr.appendChild(tdCheckBox); 

                var arrColumn = [stt, tenFile];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    if(j == 1) {
                        if(hoSoFileId.length > 0)
                            td.innerHTML = "<a href='/admin.aspx?page=getfile&id="+hoSoFileId+"&type=<%= ListName.Table_KSCLHoSoFile %>'>" + arrColumn[j] + "</a>";
                        else {
                            if(typeBaoCao == "KTHT") {
                                td.innerHTML = "<a href='/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrahethong&mahoso="+maHoSo_IDBaoCao+"' target='_Blank'>" + arrColumn[j] + "</a>";    
                            }
                            if(typeBaoCao == "KTKT") {
                                td.innerHTML = "<a href='/admin.aspx?page=kscl_thuchien_baocaoketquakiemtrakythuat&idbaocao="+maHoSo_IDBaoCao+"' target='_Blank'>" + arrColumn[j] + "</a>";    
                            }
                        }
                    }
                    else {
                        td.style.textAlign = 'center';
                        td.innerHTML = arrColumn[j];
                    }
                    tr.appendChild(td); 
                }
                tbodyMain.appendChild(tr);
            }
        }
    }
    
    function DeleteCauHoi() {
        if(!confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?'))
            return;
        var listId = '';
        $('#tblDanhSachFile :checkbox').each(function () {
            var checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")){
                    listId += $(this).val().split(',')[1] + ',';
                }
            });
        if(listId.length > 0)
            iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&action=deletehosofile&listid=' + listId;
        else
            alert('Phải chọn ít nhất một bản ghi để xóa!');
    }
    
    $('#<%=Form1.ClientID %>').validate({
        rules: {
            txtTuNgayThucTe: {
                required: true, dateITA: true
            },
            txtDenNgayThucTe: {
                required: true, dateITA: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }
    });

    function Save() {
        $('#<%=Form1.ClientID %>').submit();
    }
    
    function OpenFormThongTinDoanKiemTra() {
        $("#divThongTinDoanKiemTra").dialog({
            resizable: true,
            width: 800,
            height: 400,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Đoàn kiểm tra</b>",
            modal: true,
            zIndex: 1000
        });
        $("#divThongTinDoanKiemTra").parent().appendTo($("#Form1"));
    }
    
    function GetDanhSachThanhVienTrongDoanKiemTra() {
        iframeProcess_ThanhVien.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&iddoan='+$('#hdDoanKiemTraID').val()+'&action=dsthanhvien';
    }
    
    function DrawData_DanhSachThanhVienTrongDoanKiemTra(arrData) {
        var tbl = document.getElementById('tblDanhSachThanhVienDaAdd');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var doanKiemTraThanhVienId = arrData[i][0];
                var tenThanhVien = arrData[i][1];
                var namSinh = arrData[i][2];
                var tenChucVu = arrData[i][3];
                var donViCongTac = arrData[i][4];
                var soChungChiKTV = arrData[i][5];
                var ngayCapChungChiKTV = arrData[i][6];
                var soLanThamGia = arrData[i][7];
                var vaiTro = arrData[i][8];
                var vaiTroText = '';
                if(vaiTro == '1')
                    vaiTroText = 'Trưởng đoàn 1';
                if(vaiTro == '2')
                    vaiTroText = 'Trưởng đoàn 2';
                if(vaiTro == '3')
                    vaiTroText = 'Thành viên';

                var tr = document.createElement('TR');
                
                var arrColumn = [(i + 1), tenThanhVien, namSinh, tenChucVu, donViCongTac, soChungChiKTV, ngayCapChungChiKTV, soLanThamGia, vaiTroText];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
    }
    
    jQuery(document).ready(function () {
        $('#tblDanhSachFile').stickyTableHeaders();    
        $('#tblDanhSachThanhVienDaAdd').stickyTableHeaders();
    });
    
    <% LoadOneData(); %>
</script>
<div id="DivDanhSachCongTyKiemToan">
</div>