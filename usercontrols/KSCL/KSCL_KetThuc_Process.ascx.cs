﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_KetThuc_Process : System.Web.UI.UserControl
{
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string action = Library.CheckNull(Request.QueryString["action"]);
            if (action == "loadttdanhsach")
            {
                string maDanhSachKiemTraTrucTiep = Library.CheckNull(Request.QueryString["madanhsach"]);
                LoadThongTinDanhSachKiemTraTrucTiep(maDanhSachKiemTraTrucTiep);
            }
            if (action == "loadxulysaipham")
            {
                string idDanhSachKiemTraTrucTiep = Library.CheckNull(Request.QueryString["iddanhsach"]);
                LoadThongTinXuLySaiPham(idDanhSachKiemTraTrucTiep);
            }
            if (action == "loadxulysaiphamcongty")
            {
                string idCongTy = Library.CheckNull(Request.QueryString["idcongty"]);
                string idXuLySaiPham = Library.CheckNull(Request.QueryString["idxulysaipham"]);
                LoadThongTinXuLySaiPhamCongTy(idCongTy, idXuLySaiPham);
            }
            if (action == "insertxulysaiphamcongty")
            {
                string listId = Library.CheckNull(Request.QueryString["idxulysaiphamchitiet"]);
                string idCongTy = Library.CheckNull(Request.QueryString["idcongty"]);
                string idXuLySaiPham = Library.CheckNull(Request.QueryString["idxulysaipham"]);
                InsertThongTinXuLySaiPhamCongTy(listId, idCongTy, idXuLySaiPham);
            }
            if (action == "deletexulysaiphamcongty")
            {
                string listId = Library.CheckNull(Request.QueryString["idxulysaiphamchitiet"]);
                DeleteThongTinXuLySaiPhamCongTy(listId);
            }
        }
        catch
        {
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void LoadThongTinDanhSachKiemTraTrucTiep(string maDanhSachKiemTraTrucTiep)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var dataDS = '';" + Environment.NewLine;
        string query = @"SELECT DSKiemTraTrucTiepID FROM tblKSCLDSKiemTraTrucTiep WHERE MaDanhSach = '" + maDanhSachKiemTraTrucTiep + @"'";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            js += "dataDS = '" + ht["DSKiemTraTrucTiepID"] + "';" + Environment.NewLine;
        }
        js += "parent.DisplayInforDanhSach(dataDS);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void LoadThongTinXuLySaiPham(string idDanhSachKiemTraTrucTiep)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var dataSP = ';#;#;#';" + Environment.NewLine;
        js += "var arrDataSP = [];" + Environment.NewLine;
        string query = @"SELECT XLSP.XuLySaiPhamID, XLSP.SoQuyetDinh, XLSP.NgayQuyetDinh, TT.MaTrangThai FROM tblKSCLXuLySaiPham XLSP                            
                            LEFT JOIN tblDMTrangThai TT ON XLSP.TinhTrangID = TT.TrangThaiID
                            WHERE XLSP.DSKiemTraTrucTiepID = " + idDanhSachKiemTraTrucTiep;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            js += "dataSP = '" + listData[0]["XuLySaiPhamID"] + ";#" + listData[0]["SoQuyetDinh"] + ";#" + (!string.IsNullOrEmpty(listData[0]["NgayQuyetDinh"].ToString()) ? Library.DateTimeConvert(listData[0]["NgayQuyetDinh"]).ToString("dd/MM/yyyy") : "") + ";#" + listData[0]["MaTrangThai"].ToString().Trim() + "';" + Environment.NewLine;

            // Load danh sách xử lý sai phạm theo danh sách
            query = @"SELECT XuLySaiPhamChiTietID, NoiDungSaiPham, QuyetDinhBTC, QuyetDinhVACPA, XLSPCT.HoiVienTapTheID, HVTC.TenDoanhNghiep,  XLSPCT.HoiVienCaNhanID, (HVCN.HoDem + ' ' + HVCN.Ten) TenHoiVienCaNhan, HVCN.SoGiayChungNhanDKHN FROM tblKSCLXuLySaiPhamChiTiet XLSPCT
                        LEFT JOIN tblHoiVienCaNhan HVCN ON XLSPCT.HoiVienCaNhanID = HVCN.HoiVienCaNhanID
                        LEFT JOIN tblHoiVienTapThe HVTC ON XLSPCT.HoiVienTapTheID = HVTC.HoiVienTapTheID
                        WHERE XuLySaiPhamID = " + listData[0]["XuLySaiPhamID"] + @" ORDER BY XLSPCT.HoiVienTapTheID, XLSPCT.HoiVienCaNhanID";
            List<Hashtable> listDataChiTiet = _db.GetListData(query);
            if (listDataChiTiet.Count > 0)
            {
                int index = 0;
                foreach (Hashtable ht in listDataChiTiet)
                {
                    js += "arrDataSP[" + index + "] = ['" + ht["XuLySaiPhamChiTietID"] + "', '" + ht["HoiVienCaNhanID"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenHoiVienCaNhan"].ToString()) + "', '" + ht["HoiVienTapTheID"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"].ToString()) + "', '" + Library.CompactText(Library.RemoveSpecCharaterWhenSendByJavascript(ht["NoiDungSaiPham"].ToString()), 50) + "', '" + Library.CompactText(Library.RemoveSpecCharaterWhenSendByJavascript(ht["QuyetDinhBTC"].ToString()), 50) + "', '" + Library.CompactText(Library.RemoveSpecCharaterWhenSendByJavascript(ht["QuyetDinhVACPA"].ToString()), 50) + "', '" + ht["SoGiayChungNhanDKHN"] + "'];" + Environment.NewLine;
                    index++;
                }
            }
        }

        js += "parent.DisplayInforXuLySaiPham(dataSP, arrDataSP);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void LoadThongTinXuLySaiPhamCongTy(string idCongTy, string idXuLySaiPham)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var dataXLSPCongTy = ';#;#;#';" + Environment.NewLine;
        js += "var tenCongTy = '';" + Environment.NewLine;
        js += "var arrDataXLSPCaNhan = [];" + Environment.NewLine;
        if (!string.IsNullOrEmpty(idCongTy))
        {
            string query = @"SELECT XuLySaiPhamChiTietID, NoiDungSaiPham, QuyetDinhBTC, QuyetDinhVACPA, HoiVienCaNhanID FROM tblKSCLXuLySaiPhamChiTiet 
                            WHERE HoiVienCaNhanID IS NULL AND HoiVienTapTheID = " + idCongTy + @" AND XuLySaiPhamID = " + idXuLySaiPham;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "dataXLSPCongTy = '" + ht["XuLySaiPhamChiTietID"] + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NoiDungSaiPham"].ToString()) + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["QuyetDinhBTC"].ToString()) + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["QuyetDinhVACPA"].ToString()) + "';" + Environment.NewLine;
            }
            query = "SELECT TenDoanhNghiep FROM tblHoiVienTapThe WHERE HoiVienTapTheID = " + idCongTy;
            listData = _db.GetListData(query);
            if (listData.Count > 0)
                js += "tenCongTy = '" + Library.RemoveSpecCharaterWhenSendByJavascript(listData[0]["TenDoanhNghiep"].ToString()) + "';" + Environment.NewLine;
            query = @"SELECT XuLySaiPhamChiTietID, NoiDungSaiPham, QuyetDinhBTC, QuyetDinhVACPA, XLSPCT.HoiVienCaNhanID, (HVCN.HoDem + ' ' + HVCN.Ten) TenHoiVienCaNhan, HVCN.LoaiHoiVienCaNhan FROM tblKSCLXuLySaiPhamChiTiet XLSPCT
                        LEFT JOIN tblHoiVienCaNhan HVCN ON XLSPCT.HoiVienCaNhanID = HVCN.HoiVienCaNhanID
                        WHERE XLSPCT.HoiVienCaNhanID IS NOT NULL AND XLSPCT.HoiVienTapTheID = " + idCongTy + @" AND XuLySaiPhamID = " + idXuLySaiPham;
            listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                int index = 0;
                foreach (Hashtable ht in listData)
                {
                    js += "arrDataXLSPCaNhan[" + index + "] = ['" + ht["XuLySaiPhamChiTietID"] + "', '" + ht["HoiVienCaNhanID"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenHoiVienCaNhan"].ToString()) + "', '" + Library.CompactText(Library.RemoveSpecCharaterWhenSendByJavascript(ht["NoiDungSaiPham"].ToString()), 50) + "', '" + Library.CompactText(Library.RemoveSpecCharaterWhenSendByJavascript(ht["QuyetDinhBTC"].ToString()), 50) + "', '" + Library.CompactText(Library.RemoveSpecCharaterWhenSendByJavascript(ht["QuyetDinhVACPA"].ToString()), 50) + "', '" + ht["LoaiHoiVienCaNhan"] + "'];" + Environment.NewLine;
                    index++;
                }
            }
        }
        js += "parent.DisplayInforXuLySaiPhamCongTy(dataXLSPCongTy, tenCongTy, arrDataXLSPCaNhan);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void InsertThongTinXuLySaiPhamCongTy(string listId, string idCongTy, string idXuLySaiPham)
    {
        SqlKsclXuLySaiPhamChiTietProvider pro = new SqlKsclXuLySaiPhamChiTietProvider(ListName.ConnectionString, false, string.Empty);
        listId = listId.TrimEnd(',');
        string[] arrListId = listId.Split(',');
        for (int i = 0; i < arrListId.Length; i++)
        {
            if (!string.IsNullOrEmpty(arrListId[i]))
            {
                KsclXuLySaiPhamChiTiet obj = new KsclXuLySaiPhamChiTiet();
                obj.HoiVienCaNhanId = Library.Int32Convert(arrListId[i]);
                obj.XuLySaiPhamId = Library.Int32Convert(idXuLySaiPham);
                obj.HoiVienTapTheId = Library.Int32Convert(idCongTy);
                pro.Insert(obj);
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CallAlertSuccessFloatRightBottom('Đã thêm danh sách kiểm toán viên thành công.');" + Environment.NewLine;
        js += "parent.CallActionGetInforXuLySaiPham();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void DeleteThongTinXuLySaiPhamCongTy(string listId)
    {
        SqlKsclXuLySaiPhamChiTietProvider pro = new SqlKsclXuLySaiPhamChiTietProvider(ListName.ConnectionString, false, string.Empty);
        listId = listId.TrimEnd(',');
        string[] arrListId = listId.Split(',');
        for (int i = 0; i < arrListId.Length; i++)
        {
            if (!string.IsNullOrEmpty(arrListId[i]))
            {
                KsclXuLySaiPhamChiTiet obj = pro.GetByXuLySaiPhamChiTietId(Library.Int32Convert(arrListId[i]));
                if (obj != null && obj.XuLySaiPhamChiTietId > 0)
                {
                    // Trường hợp xóa thông tin sai phạm của công ty -> Xóa tất cả các thông tin sai phạm của các kiểm toán viên trong công ty đấy
                    if (obj.HoiVienCaNhanId == null || string.IsNullOrEmpty(obj.HoiVienCaNhanId.Value.ToString()))
                    {
                        string command = "DELETE FROM tblKSCLXuLySaiPhamChiTiet WHERE XuLySaiPhamID = " + obj.XuLySaiPhamId + @" AND HoiVienTapTheID = " + obj.HoiVienTapTheId;
                        _db.ExecuteNonQuery(command);
                    }
                    else
                        pro.Delete(Library.Int32Convert(arrListId[i]));
                }
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CallAlertSuccessFloatRightBottom('Xóa danh sách kiểm toán viên thành công.');" + Environment.NewLine;
        js += "parent.CallActionGetInforXuLySaiPham();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }
}