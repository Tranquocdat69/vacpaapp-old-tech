﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ThucHien_CamKetDocLapBaoMatThongTin : System.Web.UI.UserControl
{
    protected string tenchucnang = "Cam kết độc lập/bảo mật thông tin";
    protected string _perOnFunc_CamKetBaoMat = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    protected string _idDoanKiemTra = "";
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _perOnFunc_CamKetBaoMat = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ThucHien_CamKetBaoMat, cm.connstr);

        this._idDoanKiemTra = Library.CheckNull(Request.QueryString["iddoan"]);
        if (_perOnFunc_CamKetBaoMat.Contains(ListName.PERMISSION_Xem))
        {
            try
            {
                _db.OpenConnection();
                string action = Request.Form["hdAction"];
                if (action == "save")
                    Save();
                if (action == "delete")
                    Delete();
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }
        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/01/08
    /// Load một số thông tin khi mới load trang
    /// </summary>
    protected void FirstLoadPage()
    {
        string js = "$('#txtNgayKyCamKet').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
        js += "itemCongTySelected = '" + Request.Form["ddlCongTy"] + "';" + Environment.NewLine;
        js += "GetDanhSachThanhVienTrongDoanKiemTra();" + Environment.NewLine;
        //js += "close_leftpanel();" + Environment.NewLine;
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/01/08
    /// Lưu dữ liệu
    /// </summary>
    private void Save()
    {
        string camketId = Request.Form["hdCamKetID"];
        string doanKiemTraId = Request.Form["ddlDoanKiemTra"];
        string congTyId = Request.Form["ddlCongTy"];
        string ngayKy = Request.Form["txtNgayKyCamKet"];
        HttpPostedFile fileCamKetBaoMatThongTin = Request.Files["FileCamKetBaoMatThongTin"];
        HttpPostedFile fileCamKetDocLap = Request.Files["FileCamKetDocLap"];
        SqlKsclCamKetProvider pro = new SqlKsclCamKetProvider(ListName.ConnectionString, false, string.Empty);
        if (string.IsNullOrEmpty(camketId)) // Insert
        {
            KsclCamKet obj = new KsclCamKet();
            obj.DoanKiemTraId = Library.Int32Convert(doanKiemTraId);
            obj.HoiVienTapTheId = Library.Int32Convert(congTyId);
            obj.NgayKy = Library.DateTimeConvert(ngayKy, "dd/MM/yyyy");
            if (fileCamKetBaoMatThongTin.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(fileCamKetBaoMatThongTin.InputStream);
                byte[] fileByte = br.ReadBytes(fileCamKetBaoMatThongTin.ContentLength);
                obj.CamKetBaoMat = fileByte;
                obj.TenFileCamKetBaoMat = fileCamKetBaoMatThongTin.FileName;
            }
            if (fileCamKetDocLap.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(fileCamKetDocLap.InputStream);
                byte[] fileByte = br.ReadBytes(fileCamKetDocLap.ContentLength);
                obj.CamKetDocLap = fileByte;
                obj.TenFileCamKetDocLap = fileCamKetDocLap.FileName;
            }
            pro.Insert(obj);
        }
        else // Update
        {
            KsclCamKet obj = pro.GetByCamKetId(Library.Int32Convert(camketId));
            obj.NgayKy = Library.DateTimeConvert(ngayKy, "dd/MM/yyyy");
            if (fileCamKetBaoMatThongTin.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(fileCamKetBaoMatThongTin.InputStream);
                byte[] fileByte = br.ReadBytes(fileCamKetBaoMatThongTin.ContentLength);
                obj.CamKetBaoMat = fileByte;
                obj.TenFileCamKetBaoMat = fileCamKetBaoMatThongTin.FileName;
            }
            if (fileCamKetDocLap.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(fileCamKetDocLap.InputStream);
                byte[] fileByte = br.ReadBytes(fileCamKetDocLap.ContentLength);
                obj.CamKetDocLap = fileByte;
                obj.TenFileCamKetDocLap = fileCamKetDocLap.FileName;
            }
            pro.Update(obj);
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        string msg = "Đã lưu nội dung cam kết.";
        js += "$('#ddlDoanKiemTra option').prop('selected', false).filter('[value=\"" + Request.Form["ddlDoanKiemTra"] + "\"]').prop('selected', true);" + Environment.NewLine;
        js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
        js += "</script>";
        Page.RegisterStartupScript("SendEmailSuccess", js);
    }

    private void Delete()
    {
        string camKetId = Request.Form["hdCamKetID"];
        SqlKsclCamKetProvider pro = new SqlKsclCamKetProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(camKetId) && Library.CheckIsInt32(camKetId))
        {
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "";
            KsclCamKet obj = pro.GetByCamKetId(Library.Int32Convert(camKetId));
            if (obj != null && obj.CamKetId > 0)
            {
                if (pro.Delete(obj))
                    js += "CallAlertErrorFloatRightBottom('Đã xóa nội dung cam kết.');" + Environment.NewLine;
                else
                    js += "CallAlertErrorFloatRightBottom('Không thể xóa nội dung cam kết.');" + Environment.NewLine;
            }
            else
                js += "CallAlertErrorFloatRightBottom('Bản cam kết không tồn tại hoặc đã bị xóa.');" + Environment.NewLine;
            js += "$('#ddlDoanKiemTra option').prop('selected', false).filter('[value=\"" + Request.Form["ddlDoanKiemTra"] + "\"]').prop('selected', true);" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("SendEmailSuccess", js);
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/01/08
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_CamKetBaoMat.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        else
            FirstLoadPage();
        if (!_perOnFunc_CamKetBaoMat.Contains(ListName.PERMISSION_Them))
        {
            if (string.IsNullOrEmpty(this._idDoanKiemTra))
                Response.Write("$('#btnSave').remove();");
        }
        if (!_perOnFunc_CamKetBaoMat.Contains(ListName.PERMISSION_Sua))
        {
            if (!string.IsNullOrEmpty(this._idDoanKiemTra))
            {
                Response.Write("$('#btnSave').remove();");
            }
        }
        if (!_perOnFunc_CamKetBaoMat.Contains(ListName.PERMISSION_Xoa))
        {
            Response.Write("$('#btnDelete').remove();");
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/01/08
    /// Load danh sách kiểm tra trực tiếp
    /// </summary>
    protected void LoadListDoanKiemTra()
    {
        try
        {
            _db.OpenConnection();
            string query = @"SELECT DoanKiemTraID, MaDoanKiemTra FROM tblKSCLDoanKiemTra DKT
                                    LEFT JOIN tblDMTrangThai ON DKT.TinhTrangID = tblDMTrangThai.TrangThaiID
                                    WHERE tblDMTrangThai.MaTrangThai = " + ListName.Status_DaPheDuyet + @"
                                    ORDER BY DoanKiemTraID DESC";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                string js = "";
                foreach (Hashtable ht in listData)
                {
                    js += "<option value='" + ht["DoanKiemTraID"] + "'>" + ht["MaDoanKiemTra"] + "</option>";
                }
                Response.Write(js);
            }
            else
            {
                Response.Write("<option value=''>Không có đoàn kiểm tra được phê duyệt!</option>");
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }
}