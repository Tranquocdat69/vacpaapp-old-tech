﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ThucHien_Process : System.Web.UI.UserControl
{
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string idDoanKiemTra = Library.CheckNull(Request.QueryString["iddoan"]);
            string action = Library.CheckNull(Request.QueryString["action"]);
            if (action == "loaddscongty")
                GetDanhSachCongTyTheoDoanKiemTra(idDoanKiemTra);
            if (action == "getcamket")
            {
                string idCongTy = Library.CheckNull(Request.QueryString["idcongty"]);
                GetCamKetTheoDoanKiemTraCongTy(idDoanKiemTra, idCongTy);
            }
            if (action == "getlistcauhoi")
            {
                string loaiCauHoi = Library.CheckNull(Request.QueryString["loai"]);
                string tinhTrangHieuLuc = Library.CheckNull(Request.QueryString["hieuluc"]);
                string hoSoId = Library.CheckNull(Request.QueryString["hosoid"]);
                string idBaoCao = Library.CheckNull(Request.QueryString["idbaocao"]);
                GetNhomCauHoi(loaiCauHoi, tinhTrangHieuLuc, hoSoId, idBaoCao);
            }
            if (action == "deletecauhoi")
            {
                string listId = Library.CheckNull(Request.QueryString["listid"]);
                DeleteCauHoi(listId);
            }

            // Kiểm tra nếu có giá trị POST từ hdCauHoi -> Save thông tin câu hỏi
            string cauHoi = Request.Form["hdCauHoi"];
            if (!string.IsNullOrEmpty(cauHoi))
            {
                UpdateCauHoi();
            }

            HttpFileCollection fc = Request.Files;
            if (fc["FileTaiLieu"] != null)
            {
                UploadFile(fc["FileTaiLieu"]);
            }
            if (action == "loadlistfile")
            {
                string idHoSo = Library.CheckNull(Request.QueryString["hosoid"]);
                GetDanhSachFile(idHoSo);
            }
            if (action == "deletehosofile")
            {
                string listId = Library.CheckNull(Request.QueryString["listid"]);
                DeleteHoSoFile(listId);
            }

            if (action == "loadinforhoso")
            {
                string maHoSo = Library.CheckNull(Request.QueryString["mahoso"]);
                LoadInforHoSo(maHoSo);
            }
            if (action == "loadlistthanhviendkt")
            {
                string doanKiemTraId = Library.CheckNull(Request.QueryString["dktid"]);
                GetListThanhVienDoanKiemTra(doanKiemTraId);
            }
            if (action == "getthkqkt")
            {
                string idHoSo = Library.CheckNull(Request.QueryString["idhoso"]);
                GetTongHopKetQuaKiemTra(idHoSo);
            }
        }
        catch
        {
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created NGUYEN MANH HUNG - 2015/01/08
    /// Get danh sách công ty theo đoàn kiểm tra -> đổ vào ddl bên trang "Bảo mật thông tin"
    /// </summary>
    /// <param name="idDoanKiemTra">ID đoàn kiểm tra</param>
    private void GetDanhSachCongTyTheoDoanKiemTra(string idDoanKiemTra)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrDanhSachCongTy = [];" + Environment.NewLine;
        if (!string.IsNullOrEmpty(idDoanKiemTra))
        {
            string query = @"SELECT HVTC.HoiVienTapTheID, HVTC.TenDoanhNghiep FROM tblKSCLDoanKiemTraCongTy DKTCT
                                LEFT JOIN tblHoiVienTapThe HVTC ON DKTCT.HoiVienTapTheID = HVTC.HoiVienTapTheID
                                WHERE DKTCT.DoanKiemTraID = " + idDoanKiemTra;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                int index = 0;
                foreach (Hashtable ht in listData)
                {
                    js += "arrDanhSachCongTy[" + index + "] = ['" + ht["HoiVienTapTheID"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + "'];" + Environment.NewLine;
                    index++;
                }
            }
        }
        js += "parent.DisplayListCongTy(arrDanhSachCongTy);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void GetCamKetTheoDoanKiemTraCongTy(string idDoanKiemTra, string idCongTy)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrDataCamKet = ['','" + DateTime.Now.ToString("dd/MM/yyyy") + "','',''];" + Environment.NewLine;
        if (!string.IsNullOrEmpty(idDoanKiemTra) && !string.IsNullOrEmpty(idCongTy))
        {
            string query = @"SELECT TOP 1 CamKetID, NgayKy, TenFileCamKetBaoMat, TenFileCamKetDocLap FROM tblKSCLCamKet
                            WHERE HoiVienTapTheID = " + idCongTy + @" AND DoanKiemTraID = " +
                           idDoanKiemTra;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "arrDataCamKet = ['" + ht["CamKetID"] + "','" +
                      (!string.IsNullOrEmpty(ht["NgayKy"].ToString())
                           ? Library.DateTimeConvert(ht["NgayKy"]).ToString("dd/MM/yyyy")
                           : "") + "'," +
                      " '" + ht["TenFileCamKetBaoMat"] + "', '" + ht["TenFileCamKetDocLap"] + "']" + Environment.NewLine;
            }
        }
        js += "parent.DisplayDataCamKet(arrDataCamKet);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/12
    /// Save nội dung câu hỏi
    /// </summary>
    private void UpdateCauHoi()
    {
        string msg = "";
        string cauHoiId = Request.Form["hdCauHoiID"];
        string ngayLapCauHoi = Request.Form["hdNgayLapCauHoi"];
        string nhomCauHoi = Request.Form["hdNhomCauHoi"];
        string coHieuLuc = Request.Form["hdCoHieuLuc"];
        string cauHoi = Request.Form["hdCauHoi"];
        string diemToiDa = Request.Form["hdDiemToiDa"];
        string canCu = Request.Form["hdCanCu"];
        string huongDanChamDiem = Request.Form["hdHuongDanChamDiem"];
        string loaiCauHoi = Request.Form["hdLoaiCauHoi"];

        SqlKsclCauHoiKtProvider pro = new SqlKsclCauHoiKtProvider(ListName.ConnectionString, false, string.Empty);
        if (string.IsNullOrEmpty(cauHoiId)) // Insert
        {
            KsclCauHoiKt obj = new KsclCauHoiKt();
            obj.MaCauHoi = GenMaCauHoi(nhomCauHoi);
            obj.NgayLap = Library.DateTimeConvert(ngayLapCauHoi, "dd/MM/yyyy");
            obj.NhomCauHoiId = Library.Int32Convert(nhomCauHoi);
            obj.CauHoi = cauHoi;
            obj.DiemToiDa = Library.DecimalConvert(diemToiDa);
            obj.CanCu = canCu;
            obj.HuongDanChamDiem = huongDanChamDiem;
            obj.TinhTrangHieuLuc = coHieuLuc;
            obj.Loai = loaiCauHoi;
            if (pro.Insert(obj))
                msg = "Tạo câu hỏi thành công.";
        }
        else // Update
        {
            KsclCauHoiKt obj = pro.GetByCauHoiKtktid(Library.Int32Convert(cauHoiId));
            obj.NgayLap = Library.DateTimeConvert(ngayLapCauHoi, "dd/MM/yyyy");
            obj.CauHoi = cauHoi;
            obj.DiemToiDa = Library.DecimalConvert(diemToiDa);
            obj.CanCu = canCu;
            obj.HuongDanChamDiem = huongDanChamDiem;
            obj.TinhTrangHieuLuc = coHieuLuc;
            if (obj.NhomCauHoiId != Library.Int32Convert(nhomCauHoi))
            {
                obj.MaCauHoi = GenMaCauHoi(nhomCauHoi);
                obj.NhomCauHoiId = Library.Int32Convert(nhomCauHoi);
            }
            if (pro.Update(obj))
                msg = "Cập nhật nội dung câu hỏi thành công.";
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
        js += "parent.ResetControl();" + Environment.NewLine;
        js += "parent.GetDanhSachCauHoi();" + Environment.NewLine;
        js += "</script>" + Environment.NewLine;
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/15
    /// Hàm sinh mã câu hỏi kiểm tra
    /// </summary>
    /// <param name="idNhomCauHoi">ID nhóm câu hỏi</param>
    /// <returns></returns>
    private string GenMaCauHoi(string idNhomCauHoi)
    {
        string maCauHoi = "";
        SqlDmNhomCauHoiKsclProvider proNhomCauHoi = new SqlDmNhomCauHoiKsclProvider(ListName.ConnectionString, false, string.Empty);
        DmNhomCauHoiKscl objNhomCauHoi = proNhomCauHoi.GetByNhomCauHoiId(Library.Int32Convert(idNhomCauHoi));
        if (objNhomCauHoi != null && objNhomCauHoi.NhomCauHoiId > 0)
        {
            maCauHoi = objNhomCauHoi.MaNhom;
        }
        int stt = 1;
        string query = "SELECT TOP 1 MaCauHoi FROM " + ListName.Table_KSCLCauHoiKT + " WHERE MaCauHoi like '" + maCauHoi + "%' ORDER BY MaCauHoi DESC";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            string temp_MaCauHoi = listData[0]["MaCauHoi"].ToString();
            if (temp_MaCauHoi.Length > 0)
            {
                stt = Library.Int32Convert(temp_MaCauHoi.Substring(temp_MaCauHoi.Length - 2, 2)) + 1;
            }
        }
        if (stt > 9)
            maCauHoi += stt;
        else
            maCauHoi += "0" + stt;

        return maCauHoi;
    }

    private void DeleteCauHoi(string listId)
    {
        SqlKsclCauHoiKtProvider pro = new SqlKsclCauHoiKtProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(listId))
        {
            string[] arrId = listId.Split(',');
            foreach (string id in arrId)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    pro.Delete(Library.Int32Convert(id));
                }
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CallAlertSuccessFloatRightBottom('Đã xóa những câu hỏi được chọn.');" + Environment.NewLine;
        js += "parent.ResetControl();" + Environment.NewLine;
        js += "parent.GetDanhSachCauHoi();" + Environment.NewLine;
        js += "</script>" + Environment.NewLine;
        Response.Write(js);
    }

    private double _tongSoDiem = 0; // Lưu tổng số điểm theo bảng điểm được thiết lập
    /// <summary>
    /// Created by NGUYEN MANH HUNG
    /// Get danh sách nhóm câu hỏi, câu hỏi theo điều kiện truyền vào. Đồng thời lấy dữ liệu bảng chấm điểm nếu có
    /// </summary>
    /// <param name="loaiCauHoi">Loại câu hỏi (1: câu hỏi kiểm tra hệ thống;  2: câu hỏi kiểm tra kỹ thuật)</param>
    /// <param name="tinhTrangHieuLuc">Tình trạng hiệu lực (1: Có hiệu lực; 2: Hết hiệu lực)</param>
    /// <param name="maHoSo">Mã hồ sơ kiểm soát chất lượng</param>
    protected void GetNhomCauHoi(string loaiCauHoi, string tinhTrangHieuLuc, string hoSoId, string idBaoCaoKTKT)
    {
        try
        {
            DataTable data = GetDanhSachCauHoi(loaiCauHoi, tinhTrangHieuLuc); // Get danh sách câu hỏi đổ tất cả vào DataTable
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            // Lấy dữ liệu chi tiết báo cáo kiểm tra nếu có id Hồ sơ kiểm tra kiểm soát chất lượng
            DataTable dataBaoCaoKT = new DataTable();
            if (!string.IsNullOrEmpty(hoSoId) || !string.IsNullOrEmpty(idBaoCaoKTKT))
            {
                string idBaoCaoKT = idBaoCaoKTKT; // Gán giá trị = idBaoCaoKTKT truyền vào với trường hợp get số liệu cho báo cáo kiểm tra kỹ thuật
                js += GetInforBaoCaoKiemTra(loaiCauHoi, hoSoId, ref idBaoCaoKT); // Get thông tin chung của bảng báo cáo kết quả kiểm tra
                if (!string.IsNullOrEmpty(idBaoCaoKT))
                    dataBaoCaoKT = GetDanhSachBaoCaoKiemTraChiTiet(loaiCauHoi, idBaoCaoKT); // Get dữ liệu bảng điểm 
            }
            js += "var arrData = [];" + Environment.NewLine;
            js += "var arrDataDiemToiDa = [];" + Environment.NewLine;
            _db.OpenConnection();
            // Get Danh sách nhóm câu hỏi
            string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE Loai = " + loaiCauHoi + " AND (NhomCauHoiChaID IS NULL OR NhomCauHoiChaID = '') ORDER BY NhomCauHoiID";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                int index = 1;
                foreach (Hashtable ht in listData)
                {
                    DataRow[] arrDataRow = data.Select("NCHID = " + ht["NhomCauHoiID"]); // Lấy danh sách câu hỏi theo ID nhóm câu hỏi
                    double sumDiemToiDa = arrDataRow.Sum(dataRow => Library.DoubleConvert(dataRow["DiemToiDa"]));
                    _tongSoDiem += sumDiemToiDa;
                    js += "arrData.push(['', '" + index + "', '', '<b>" + ht["TenNhomCauHoi"] + "</b>', '', '" + (sumDiemToiDa > 0 ? Library.ChangeFormatNumber(sumDiemToiDa, "us") : "") + "', '', '','','','" + ht["NhomCauHoiID"] + "']);" + Environment.NewLine;

                    query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + ht["NhomCauHoiID"] + " ORDER BY NhomCauHoiID";
                    List<Hashtable> listDataChild = _db.GetListData(query);
                    string tienToXacDinhLaCauHoi = ""; // Tiền tố xác định là câu hỏi trong chuỗi STT (Thêm .0.)
                    if (listDataChild.Count > 0)
                        tienToXacDinhLaCauHoi = "0.";
                    int cauHoiIndex = 1;
                    foreach (DataRow dataRow in arrDataRow)
                    {
                        js += "arrData.push(['" + dataRow["CauHoiKTKTID"] + "', '" + index + "." + tienToXacDinhLaCauHoi + cauHoiIndex + "', '" + dataRow["MaCauHoi"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(dataRow["CauHoi"].ToString()) + "', '" + dataRow["CanCu"] + "', '" + (Library.DoubleConvert(dataRow["DiemToiDa"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(dataRow["DiemToiDa"]), "us") : "") + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(dataRow["HuongDanChamDiem"].ToString()) + "'," +
                            " '" + (!string.IsNullOrEmpty(dataRow["NgayLap"].ToString()) ? Library.DateTimeConvert(dataRow["NgayLap"]).ToString("dd/MM/yyyy") : "") + "', '" + dataRow["NCHID"] + "', '" + dataRow["TinhTrangHieuLuc"] + "'";
                        if (dataBaoCaoKT != null && dataBaoCaoKT.Rows.Count > 0)
                        {
                            string queryDataTable = "";
                            if (loaiCauHoi == "1")
                                queryDataTable = "CauHoiKTHTID = " + dataRow["CauHoiKTKTID"];
                            if (loaiCauHoi == "2")
                                queryDataTable = "CauHoiKTKTID = " + dataRow["CauHoiKTKTID"];
                            DataRow[] arrDataRowBaoCaoKT = dataBaoCaoKT.Select(queryDataTable); // Lấy kết quả báo cáo theo câu hỏi (Nếu có)
                            if (arrDataRowBaoCaoKT.Length > 0)
                            {
                                js += ",'" + arrDataRowBaoCaoKT[0]["TraLoi"] + "','" + (Library.DoubleConvert(arrDataRowBaoCaoKT[0]["DiemThucTe"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(arrDataRowBaoCaoKT[0]["DiemThucTe"]), "us") : "") + "','" + arrDataRowBaoCaoKT[0]["GhiChu"] + "'";
                            }
                        }
                        js += "]);" + Environment.NewLine;


                        cauHoiIndex++;
                    }
                    js = GetNhomCauHoiSub(js, listDataChild, data, dataBaoCaoKT, index, loaiCauHoi, ref sumDiemToiDa); // Gọi hàm đệ quy
                    index++;
                    js += "arrDataDiemToiDa.push(['" + ht["NhomCauHoiID"] + "','" + sumDiemToiDa + "']);" + Environment.NewLine;
                }
            }
            js += "parent.Draw_DanhSachCauHoi(arrData, arrDataDiemToiDa, '" + Library.ChangeFormatNumber(_tongSoDiem, "us") + "');" + Environment.NewLine;
            js += "</script>";
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/15
    /// Get danh sách các nhóm câu hỏi con (Hàm đệ quy)
    /// </summary>
    /// <param name="js">Chuỗi html chưa dữ liệu</param>
    /// <param name="nhomCauHoiChaId">ID câu hỏi cha</param>
    /// <param name="data">DataTable chứa danh sách câu hỏi</param>
    protected string GetNhomCauHoiSub(string js, List<Hashtable> listData, DataTable data, DataTable dataBaoCaoKT, int index, string loaiCauHoi, ref double sumDiemToiDa_NhomCha)
    {
        if (listData.Count > 0)
        {
            int subIndex = 1;
            foreach (Hashtable ht in listData)
            {
                DataRow[] arrDataRow = data.Select("NCHID = " + ht["NhomCauHoiID"]);
                double sumDiemToiDa = arrDataRow.Sum(dataRow => Library.DoubleConvert(dataRow["DiemToiDa"]));
                _tongSoDiem += sumDiemToiDa;
                js += "arrData.push(['', '" + index + "." + subIndex + "', '', '<b>" + ht["TenNhomCauHoi"] + "</b>', '', '" + (sumDiemToiDa > 0 ? Library.ChangeFormatNumber(sumDiemToiDa, "us") : "") + "', '', '','','','" + ht["NhomCauHoiID"] + "']);" + Environment.NewLine;
                string query = "SELECT NhomCauHoiID, TenNhomCauHoi FROM " + ListName.Table_DMNhomCauHoiKSCL + " WHERE NhomCauHoiChaID = " + ht["NhomCauHoiID"] + " ORDER BY NhomCauHoiID";
                List<Hashtable> listDataChild = _db.GetListData(query);
                string tienToXacDinhLaCauHoi = ""; // Tiền tố xác định là câu hỏi trong chuỗi STT (Thêm .0.)
                if (listDataChild.Count > 0)
                    tienToXacDinhLaCauHoi = "0.";
                int cauHoiIndex = 1;
                foreach (DataRow dataRow in arrDataRow)
                {
                    js += "arrData.push(['" + dataRow["CauHoiKTKTID"] + "', '" + index + "." + subIndex + "." + tienToXacDinhLaCauHoi + cauHoiIndex + "', '" + dataRow["MaCauHoi"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(dataRow["CauHoi"].ToString()) + "', '" + dataRow["CanCu"] + "', '" + (Library.DoubleConvert(dataRow["DiemToiDa"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(dataRow["DiemToiDa"]), "us") : "") + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(dataRow["HuongDanChamDiem"].ToString()) + "'," +
                              " '" + (!string.IsNullOrEmpty(dataRow["NgayLap"].ToString()) ? Library.DateTimeConvert(dataRow["NgayLap"]).ToString("dd/MM/yyyy") : "") + "', '" + dataRow["NCHID"] + "', '" + dataRow["TinhTrangHieuLuc"] + "'";
                    if (dataBaoCaoKT != null && dataBaoCaoKT.Rows.Count > 0)
                    {
                        string queryDataTable = "";
                        if (loaiCauHoi == "1")
                            queryDataTable = "CauHoiKTHTID = " + dataRow["CauHoiKTKTID"];
                        if (loaiCauHoi == "2")
                            queryDataTable = "CauHoiKTKTID = " + dataRow["CauHoiKTKTID"];
                        DataRow[] arrDataRowBaoCaoKT = dataBaoCaoKT.Select(queryDataTable); // Lấy kết quả báo cáo theo câu hỏi (Nếu có)
                        if (arrDataRowBaoCaoKT.Length > 0)
                        {
                            js += ",'" + arrDataRowBaoCaoKT[0]["TraLoi"] + "','" + (Library.DoubleConvert(arrDataRowBaoCaoKT[0]["DiemThucTe"]) > 0 ? Library.ChangeFormatNumber(Library.DoubleConvert(arrDataRowBaoCaoKT[0]["DiemThucTe"]), "us") : "") + "','" + arrDataRowBaoCaoKT[0]["GhiChu"] + "'";
                        }
                    }
                    js += "]);" + Environment.NewLine;
                    cauHoiIndex++;
                }
                js = GetNhomCauHoiSub(js, listDataChild, data, dataBaoCaoKT, subIndex, loaiCauHoi, ref sumDiemToiDa_NhomCha);
                subIndex++;
                sumDiemToiDa_NhomCha += sumDiemToiDa;
                js += "arrDataDiemToiDa.push(['" + ht["NhomCauHoiID"] + "','" + sumDiemToiDa + "']);" + Environment.NewLine;
            }
        }
        return js;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG
    /// Get danh sách câu hỏi theo loại câu hỏi và tình trạng hiệu lực
    /// </summary>
    /// <param name="loaiCauHoi">Loại câu hỏi (1: câu hỏi kiểm tra hệ thống;  2: câu hỏi kiểm tra kỹ thuật)</param>
    /// <param name="tinhTrangHieuLuc">Tình trạng hiệu lực (1: Có hiệu lực; 2: Hết hiệu lực)</param>
    /// <returns>DataTable chứa tất cả câu hỏi phù hợp điều kiện</returns>
    private DataTable GetDanhSachCauHoi(string loaiCauHoi, string tinhTrangHieuLuc)
    {
        DataTable dt = new DataTable();
        string query = @"SELECT CauHoiKTKTID, MaCauHoi, NgayLap, a.NhomCauHoiID NCHID, CauHoi, DiemToiDa, CanCu, HuongDanChamDiem, TinhTrangHieuLuc FROM tblKSCLCauHoiKT a
                            INNER JOIN tblDMNhomCauHoiKSCL b ON a.NhomCauHoiID = b.NhomCauHoiID
                            WHERE b.Loai = " + loaiCauHoi + " AND a.TinhTrangHieuLuc = '" + tinhTrangHieuLuc + "'";
        dt = _db.GetDataTable(query);
        return dt;
    }

    private void UploadFile(HttpPostedFile hpf)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        SqlKsclHoSoFileProvider pro = new SqlKsclHoSoFileProvider(ListName.ConnectionString, false, string.Empty);
        KsclHoSoFile obj = new KsclHoSoFile();
        if (hpf.ContentLength > 0)
        {
            BinaryReader br = new BinaryReader(hpf.InputStream);
            byte[] fileByte = br.ReadBytes(hpf.ContentLength);
            obj.Loai = "4";
            obj.FileTaiLieu = fileByte;
            obj.TenFile = hpf.FileName;
            obj.HoSoId = Library.Int32Convert(Request.Form["hdHoSoID"]);
            if (pro.Insert(obj))
            {
                //js += "$('#FormUploadFile').remove('#FileTaiLieu');" + Environment.NewLine;
                //js += "parent.CallAlertSuccessFloatRightBottom('Tải lên thành công.');" + Environment.NewLine;
            }
        }
        js += "parent.GetListUpload();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void GetDanhSachFile(string idHoSo)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        int index = 0;

        // Lấy danh sách kết quả báo cáo kiểm tra hệ thống, kỹ thuật
        string query = "SELECT BCKTHT.BaoCaoKTHTID, HS.MaHoSo FROM " + ListName.Table_KSCLBaoCaoKTHT + " BCKTHT LEFT JOIN " + ListName.Table_KSCLHoSo + " HS ON BCKTHT.HoSoID = HS.HoSoID WHERE BCKTHT.HoSoID = " + idHoSo;
        List<Hashtable> listData_BaoCaoKTHT = _db.GetListData(query);
        if (listData_BaoCaoKTHT.Count > 0)
        {
            js += "arrData[" + index + "] = ['', '', 'Báo cáo kết quả kiểm tra hệ thống', '" + listData_BaoCaoKTHT[0]["MaHoSo"] + "', 'KTHT'];" + Environment.NewLine;
            index++;
        }

        query = "SELECT BCKTKT.BaoCaoKTKTID, SoBaoCaoKT, HS.MaHoSo FROM " + ListName.Table_KSCLBaoCaoKTKT + " BCKTKT LEFT JOIN " + ListName.Table_KSCLHoSo + " HS ON BCKTKT.HoSoID = HS.HoSoID WHERE BCKTKT.HoSoID = " + idHoSo;
        List<Hashtable> listData_BaoCaoKTKT = _db.GetListData(query);
        if (listData_BaoCaoKTKT.Count > 0)
        {
            foreach (Hashtable ht in listData_BaoCaoKTKT)
            {
                js += "arrData[" + index + "] = ['', '', 'Báo cáo kết quả kiểm tra kỹ thuật - " + ht["SoBaoCaoKT"] + "', '" + ht["BaoCaoKTKTID"] + "', 'KTKT'];" + Environment.NewLine;
                index++;
            }
        }

        query = @"SELECT HoSoFileID, MaTaiLieu, TenFile FROM tblKSCLHoSoFile WHERE HoSoID = " + idHoSo + @" AND Loai = '4'";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            foreach (Hashtable ht in listData)
            {
                js += "arrData[" + index + "] = ['" + ht["HoSoFileID"] + "', '" + ht["MaTaiLieu"] + "', '" + ht["TenFile"] + "'];" + Environment.NewLine;
                index++;
            }
        }
        js += "parent.DrawData_DanhSachFile(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void DeleteHoSoFile(string listId)
    {
        SqlKsclHoSoFileProvider pro = new SqlKsclHoSoFileProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(listId))
        {
            string[] arrId = listId.Split(',');
            foreach (string id in arrId)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    pro.Delete(Library.Int32Convert(id));
                }
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.GetListUpload();" + Environment.NewLine;
        js += "</script>" + Environment.NewLine;
        Response.Write(js);
    }

    private void LoadInforHoSo(string maHoSo)
    {
        string js = "<script>" + Environment.NewLine;
        js += "var data = ';#;#;#;#;#';" + Environment.NewLine;
        string query = @"SELECT HS.HoSoID, HVTC.MaHoiVienTapThe, HVTC.TenVietTat, HVTC.NguoiDaiDienPL_Ten, CV.TenChucVu, HS.DoanKiemTraID FROM " + ListName.Table_KSCLHoSo + @" HS
                            LEFT JOIN tblHoiVienTapThe HVTC ON HS.HoiVienTapTheID = HVTC.HoiVienTapTheID
                            LEFT JOIN tblDMChucVu CV ON HVTC.NguoiDaiDienPL_ChucVuID = CV.ChucVuID
                            WHERE HS.MaHoSo = '" + maHoSo + @"'";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            js += "data = '" + ht["HoSoID"] + ";#" + ht["MaHoiVienTapThe"] + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenVietTat"]) + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienPL_Ten"].ToString()) + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenChucVu"].ToString()) + ";#" + ht["DoanKiemTraID"] + "';";
        }
        js += "parent.DisplayInforHoSo(data);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void GetListThanhVienDoanKiemTra(string doanKiemTraId)
    {
        string js = "<script>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        string query = @"SELECT DoanKiemTraThanhVienID, (HVCN.HoDem + ' ' + HVCN.Ten) HoTen, DMTVDKT.TenThanhVien, DKTTV.HoiVienCaNhanID, DKTTV.ThanhVienDoanKiemTraID  FROM tblKSCLDoanKiemTraThanhVien DKTTV
                                LEFT JOIN tblHoiVienCaNhan HVCN ON DKTTV.HoiVienCaNhanID = HVCN.HoiVienCaNhanID
                                LEFT JOIN tblDMThanhVienDoanKT DMTVDKT ON DKTTV.ThanhVienDoanKiemTraID = DMTVDKT.ThanhVienDoanKiemTraID
                                WHERE DKTTV.DoanKiemTraID = '" + doanKiemTraId + @"'";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in listData)
            {
                string tenThanhVien = !string.IsNullOrEmpty(ht["HoiVienCaNhanID"].ToString())
                                          ? Library.RemoveSpecCharaterWhenSendByJavascript(ht["HoTen"].ToString())
                                          : Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenThanhVien"].ToString());
                js += "arrData[" + index + "] = ['" + ht["DoanKiemTraThanhVienID"] + "', '" + tenThanhVien + "'];" + Environment.NewLine;
                index++;
            }
        }
        js += "parent.DisplayListThanhVienDoanKiemTra(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG
    /// Get thông tin báo cáo kiểm tra
    /// </summary>
    /// <param name="loai">Loại báo cáo (1: báo cáo kiểm tra hệ thống;  2: báo cáo kiểm tra kỹ thuật)</param>
    /// <param name="hoSoId">ID hồ sơ kiểm soát chất lượng</param>
    /// <returns>Chuỗi js chứa nội dung báo cáo</returns>
    private string GetInforBaoCaoKiemTra(string loai, string hoSoId, ref string idBaoCaoKT)
    {
        if (loai == "1") // Kiểm tra hệ thống
        {
            string js = "var dataBaoCaoKT = ';#;#;#;#;#;#;#;#;#';" + Environment.NewLine;
            string query = "SELECT BaoCaoKTHTID, XepLoai, ThanhVienChamDiem1, ThanhVienChamDiem2, SLThanhVienBGD, SLKTVHanhNghe, SoLuongNVChuyenNghiep, SLBCKTPhatHanh, " +
                           "HinhThucLuuTaiLieu, PhanMemKT, TT.MaTrangThai FROM " + ListName.Table_KSCLBaoCaoKTHT + " BCKTHT LEFT JOIN " + ListName.Table_DMTrangThai + " TT ON " +
                           "BCKTHT.TinhTrangID = TT.TrangThaiID WHERE HoSoID = " + hoSoId;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                idBaoCaoKT = ht["BaoCaoKTHTID"].ToString();
                js += "dataBaoCaoKT = '" + ht["BaoCaoKTHTID"] + ";#" + ht["XepLoai"] + ";#" + ht["ThanhVienChamDiem1"] + ";#" + ht["ThanhVienChamDiem2"] + ";#" +
                      "" + ht["SLThanhVienBGD"] + ";#" + ht["SLKTVHanhNghe"] + ";#" + ht["SoLuongNVChuyenNghiep"] + ";#" + ht["SLBCKTPhatHanh"] + ";#" +
                      "" + ht["HinhThucLuuTaiLieu"] + ";#" + ht["PhanMemKT"] + ";#" + ht["MaTrangThai"] + "';" + Environment.NewLine;
            }
            js += "parent.DrawDataBaoCao(dataBaoCaoKT);" + Environment.NewLine;
            return js;
        }
        if (loai == "2") // Kiểm tra kỹ thuật
        {
            // Trường hợp này ko dùng ID của hồ sơ, mà phải dùng id của bản báo cáo kỹ thuật
            string js = "var dataBaoCaoKT = ';#;#;#;#;#;#;#;#;#;#;#;#;#;#;#;#;#;#;#';" + Environment.NewLine;
            string query = "SELECT BaoCaoKTKTID, XepLoai, ThanhVienChamDiem1, ThanhVienChamDiem2, TenKhachHangKT, BCTCNam, DonViChungKhoan, PhiKiemToan, SoBaoCaoKT," +
                           "NgayBaoCaoKT, BGDTen, BGDSoGiayCNDKHN, BGDNgayCNDKHN, KTVTen, KTVSoGiayCNDKHN, KTVNgayCNDKHN, HoiVienCaNhanID, DangYKienKTID, DangYKienKTKhac," +
                           "TT.MaTrangThai FROM " + ListName.Table_KSCLBaoCaoKTKT + " BCKTKT LEFT JOIN " + ListName.Table_DMTrangThai + " TT ON " +
                           "BCKTKT.TinhTrangID = TT.TrangThaiID WHERE BaoCaoKTKTID = " + idBaoCaoKT;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                idBaoCaoKT = ht["BaoCaoKTKTID"].ToString();
                js += "dataBaoCaoKT = '" + ht["BaoCaoKTKTID"] + ";#" + ht["XepLoai"] + ";#" + ht["ThanhVienChamDiem1"] + ";#" + ht["ThanhVienChamDiem2"] + ";#" +
                      "" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenKhachHangKT"]) + ";#" + ht["BCTCNam"] + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["DonViChungKhoan"]) + ";#" + Library.FormatMoney(ht["PhiKiemToan"]) + ";#" +
                      "" + ht["SoBaoCaoKT"] + ";#" + (!string.IsNullOrEmpty(ht["NgayBaoCaoKT"].ToString()) ? Library.DateTimeConvert(ht["NgayBaoCaoKT"]).ToString("dd/MM/yyyy") : "") + ";#" +
                      "" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["BGDTen"]) + ";#" + ht["BGDSoGiayCNDKHN"] + ";#" + (!string.IsNullOrEmpty(ht["BGDNgayCNDKHN"].ToString()) ? Library.DateTimeConvert(ht["BGDNgayCNDKHN"]).ToString("dd/MM/yyyy") : "") + ";#" +
                      "" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["KTVTen"]) + ";#" + ht["KTVSoGiayCNDKHN"] + ";#" + (!string.IsNullOrEmpty(ht["KTVNgayCNDKHN"].ToString()) ? Library.DateTimeConvert(ht["KTVNgayCNDKHN"]).ToString("dd/MM/yyyy") : "") + ";#" +
                      "" + ht["HoiVienCaNhanID"] + ";#" + ht["DangYKienKTID"] + ";#" +
                      "" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["DangYKienKTKhac"]) + ";#" + ht["MaTrangThai"] + "';" + Environment.NewLine;
            }
            js += "parent.DrawDataBaoCao(dataBaoCaoKT);" + Environment.NewLine;
            return js;
        }
        return "";
    }

    private DataTable GetDanhSachBaoCaoKiemTraChiTiet(string loai, string idBaoCaoKT)
    {
        if (loai == "1")
        {
            string query = "SELECT BaoCaoKTHTChiTietID, CauHoiKTHTID, TraLoi, DiemThucTe, GhiChu FROM " + ListName.Table_KSCLBaoCaoKTHTChiTiet + " WHERE BaoCaoKTHTID = " + idBaoCaoKT;
            DataTable dt = _db.GetDataTable(query);
            return dt;
        }
        if (loai == "2")
        {
            string query = "SELECT BaoCaoKTKTChiTietID, CauHoiKTKTID, TraLoi, DiemThucTe, GhiChu FROM " + ListName.Table_KSCLBaoCaoKTKTChiTiet + " WHERE BaoCaoKTKTID = " + idBaoCaoKT;
            DataTable dt = _db.GetDataTable(query);
            return dt;
        }
        return new DataTable();
    }

    private void GetTongHopKetQuaKiemTra(string idHoSo)
    {
        string js = "<script>" + Environment.NewLine;
        js += "var data = ';#;#;#;#;#;#;#;#';" + Environment.NewLine;
        js += "var arrDataKTKT = [];" + Environment.NewLine;
        string xepHangKTHT = "", tongDiemKTHT = "", xepHangKTKT = "", diemTrungBinhKTKT = "", diemTrungBinhKSCL = "", xepHangChung = "", ketLuanChung = "", tenFile = "", baoCaoTongHopId = "";
        string query = "SELECT XepLoai, BaoCaoKTHTID FROM tblKSCLBaoCaoKTHT WHERE HoSoID = " + idHoSo;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            xepHangKTHT = listData[0]["XepLoai"].ToString();
            List<Hashtable> listData1 = _db.GetListData(ListName.Proc_KSCL_TinhTongDiemDaQuyDoiKTHT, new List<string> { "@BaoCaoID" }, new List<object> { listData[0]["BaoCaoKTHTID"] });
            if (listData1.Count > 0)
            {
                double tempTongDiemKTHT = Library.DoubleConvert(listData1[0]["TongDiemKTHT"]);
                tempTongDiemKTHT = Math.Truncate(tempTongDiemKTHT * 100) / 100;
                tongDiemKTHT = (tempTongDiemKTHT > 0 ? Library.ChangeFormatNumber(tempTongDiemKTHT, "us") : "");
            }
        }

        // Lấy thông tin từ phần bảng báo cáo tổng hợp trước
        query = "SELECT XepLoaiKT, BaoCaoTongHopID, XepLoaiChung, BienBanKSCL, TenFileBienBanKSCL, KetLuan FROM tblKSCLBaoCaoTongHop WHERE HoSoID = " + idHoSo;
        List<Hashtable> listDataBaoCaoTongHop = _db.GetListData(query);
        if (listDataBaoCaoTongHop.Count > 0)
        {
            xepHangKTKT = listDataBaoCaoTongHop[0]["XepLoaiKT"].ToString();
            xepHangChung = listDataBaoCaoTongHop[0]["XepLoaiChung"].ToString();
            ketLuanChung = Library.RemoveSpecCharaterWhenSendByJavascript(listDataBaoCaoTongHop[0]["KetLuan"].ToString());
            tenFile = listDataBaoCaoTongHop[0]["TenFileBienBanKSCL"].ToString();
            baoCaoTongHopId = listDataBaoCaoTongHop[0]["BaoCaoTongHopID"].ToString();
        }

        // Lấy thông tin từ báo cáo kiểm tra kỹ thuật
        query = "SELECT XepLoai, BaoCaoKTKTID, TenKhachHangKT, BGDTen, KTVTen, BCTCNam FROM tblKSCLBaoCaoKTKT WHERE HoSoID = " + idHoSo;
        listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            double tongDiemKTKT = 0;
            int indexKTKT = 0;
            foreach (Hashtable ht in listData)
            {
                List<Hashtable> listData1 = _db.GetListData(ListName.Proc_KSCL_TinhTongDiemDaQuyDoiKTKT, new List<string> { "@BaoCaoID" }, new List<object> { listData[0]["BaoCaoKTKTID"] });
                if (listData1.Count > 0)
                {
                    tongDiemKTKT += Library.DoubleConvert(listData1[0]["TongDiemKTKT"]);
                }
                js += "arrDataKTKT[" + indexKTKT + "] = ['" + ht["BaoCaoKTKTID"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenKhachHangKT"]) + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["BGDTen"]) + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["KTVTen"]) + "', '" + ht["BCTCNam"] + "', " +
                      "'" + Library.DoubleConvert(listData1[0]["TongDiemKTKT"]) + "', '" + TinhXepLoai(Library.DoubleConvert(listData1[0]["TongDiemKTKT"])) + "'];" + Environment.NewLine;
                indexKTKT++;
            }
            double tempDiemTrungBinhKTKT = (tongDiemKTKT / Library.DoubleConvert(listData.Count));
            tempDiemTrungBinhKTKT = Math.Truncate(tempDiemTrungBinhKTKT * 100) / 100;
            diemTrungBinhKTKT = tempDiemTrungBinhKTKT > 0 ? Library.ChangeFormatNumber(tempDiemTrungBinhKTKT, "us") : "";
            if (string.IsNullOrEmpty(xepHangKTKT)) // Nếu ko có xếp hạng KTKT -> chưa có tổng hợp báo cáo thì lấy điểm trung bình để xếp hạng
            {
                xepHangKTKT = TinhXepLoai(tempDiemTrungBinhKTKT);
            }
        }
        double tempDiemTrungBinhKSCL = (Library.DoubleConvert(tongDiemKTHT) + Library.DoubleConvert(diemTrungBinhKTKT)) / 2;
        tempDiemTrungBinhKSCL = Math.Truncate(tempDiemTrungBinhKSCL * 100) / 100;
        diemTrungBinhKSCL = tempDiemTrungBinhKSCL > 0 ? Library.ChangeFormatNumber(tempDiemTrungBinhKSCL, "us") : "";
        if (string.IsNullOrEmpty(xepHangChung))
            xepHangChung = TinhXepLoai(Library.DoubleConvert(diemTrungBinhKSCL));
        js += "data = '" + baoCaoTongHopId + ";#" + tongDiemKTHT + ";#" + GetXepLoai(xepHangKTHT) + ";#" + diemTrungBinhKTKT + ";#" + xepHangKTKT + ";#" + diemTrungBinhKSCL + ";#" + xepHangChung + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ketLuanChung) + ";#" + tenFile + "';" + Environment.NewLine;
        js += "parent.DrawDataKetQuaKiemTra(data, arrDataKTKT);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private string TinhXepLoai(double diem)
    {
        if (diem < 40)
            return "4";
        if (diem >= 40 && diem < 60)
            return "3";
        if (diem >= 60 && diem < 80)
            return "2";
        if (diem >= 80)
            return "1";
        return "";
    }

    private string GetXepLoai(string xepLoai)
    {
        switch (xepLoai)
        {
            case "1":
                return "Loại 1 - Tốt";
            case "2":
                return "Loại 2 - Đạt";
            case "3":
                return "Loại 3 - Không đạt";
            case "4":
                return "Loại 4 - Yếu kém";
        }
        return "";
    }
}
