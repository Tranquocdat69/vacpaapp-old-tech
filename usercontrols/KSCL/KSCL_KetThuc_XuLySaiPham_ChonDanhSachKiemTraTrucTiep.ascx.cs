﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_KetThuc_XuLySaiPham_ChonDanhSachKiemTraTrucTiep : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _congTyId = "", _idXuLySaiPham = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this._congTyId = Request.QueryString["idct"];
            this._idXuLySaiPham = Request.QueryString["idxulysaipham"];
            _db.OpenConnection();
            if (!IsPostBack)
            {
                LoadDanhSach();
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Load danh sach "Hoi vien" theo dieu kien search
    /// </summary>
    private void LoadDanhSach()
    {
        try
        {
            _db.OpenConnection();
            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "ID", "MaDanhSach", "NgayLap", "MaTrangThai", "TenTrangThai", "SoCongTy" });

            List<string> listParam = new List<string>{"@DuDKKT_CK", "@DuDKKT_Khac", "@MaHocVienTapThe", "@TenDoanhNghiep", "@LoaiHinhDoanhNghiepID",
                    "@VungMienID", "@DoanhThuTu", "@DoanhThuDen", "@NamHienTai", "@XepHang", "@NgayBatDau", "@NgayKetThuc", "@MaDanhSach", "@MaTrangThai_ChoDuyet", 
                    "@MaTrangThai_TuChoi", "@MaTrangThai_DaDuyet", "@NgayLapBatDau", "@NgayLapKetThuc"};
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DateTime.Now.Year, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value };
            // Dựa theo các tiêu chí tìm kiếm -> đưa giá trị tham số tương ứng vào store
            if (cboDuDieuKienKTCK.Checked)
                listValue[0] = 1;
            if (cboDuDieuKienKTKhac.Checked)
                listValue[1] = 1;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                listValue[2] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                listValue[3] = txtTenCongTy.Text;
            if (ddlLoaiHinh.SelectedValue != "")
                listValue[4] = ddlLoaiHinh.SelectedValue;
            if (ddlVungMien.SelectedValue != "")
                listValue[5] = ddlVungMien.SelectedValue;
            if (!string.IsNullOrEmpty(txtDoanhThuTu.Text) && Library.CheckIsDecimal(txtDoanhThuTu.Text))
                listValue[6] = txtDoanhThuTu.Text;
            if (!string.IsNullOrEmpty(txtDoanhThuDen.Text) && Library.CheckIsDecimal(txtDoanhThuDen.Text))
                listValue[7] = txtDoanhThuDen.Text;
            if (!cboTieuChi_Khac.Checked)
            {
                string xepHang = "";
                if (cboXepHang1.Checked)
                    xepHang += "1,";
                if (cboXepHang2.Checked)
                    xepHang += "2,";
                if (cboXepHang3.Checked)
                    xepHang += "3,";
                if (cboXepHang4.Checked)
                    xepHang += "4,";
                listValue[9] = xepHang;
            }
            if (!string.IsNullOrEmpty(txtTuNam.Text) && Library.CheckIsInt32(txtTuNam.Text))
                listValue[10] = txtTuNam.Text + "-1-1";
            if (!string.IsNullOrEmpty(txtDenNam.Text) && Library.CheckIsInt32(txtDenNam.Text))
                listValue[11] = txtDenNam.Text + "-12-31";
            if (!string.IsNullOrEmpty(txtMaDanhSach.Text))
                listValue[12] = txtMaDanhSach.Text;
            if (cboChoDuyet.Checked)
                listValue[13] = ListName.Status_ChoDuyet;
            if (cboTuChoi.Checked)
                listValue[14] = ListName.Status_KhongPheDuyet;
            if (cboDaDuyet.Checked)
                listValue[15] = ListName.Status_DaPheDuyet;
            if (!string.IsNullOrEmpty(txtNgayLapBatDau.Text) && Library.CheckDateTime(txtNgayLapBatDau.Text, "dd/MM/yyyy"))
                listValue[16] = Library.ConvertDatetime(txtNgayLapBatDau.Text, '/');
            if (!string.IsNullOrEmpty(txtNgayLapKetThuc.Text) && Library.CheckDateTime(txtNgayLapKetThuc.Text, "dd/MM/yyyy"))
                listValue[17] = Library.ConvertDatetime(txtNgayLapKetThuc.Text, '/');

            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SearchDanhSachCongTyKTKiemTraTrucTiep, listParam, listValue);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["DSKiemTraTrucTiepID"].ToString();
                    dr["MaDanhSach"] = ht["MaDanhSach"].ToString();
                    dr["NgayLap"] = Library.DateTimeConvert(ht["NgayLap"]);
                    string maTrangThai = ht["MaTrangThai"].ToString().Trim();
                    dr["MaTrangThai"] = maTrangThai;
                    dr["TenTrangThai"] = ht["TenTrangThai"].ToString();
                    if (maTrangThai == ListName.Status_DaPheDuyet)
                        dr["TenTrangThai"] = "<span style='color:green'>" + ht["TenTrangThai"] + "</span>";
                    if (maTrangThai == ListName.Status_KhongPheDuyet)
                        dr["TenTrangThai"] = "<span style='color:red'>" + ht["TenTrangThai"] + "</span>";
                    dr["SoCongTy"] = ht["SoCongTy"].ToString();
                    dt.Rows.Add(dr);
                }
            }
            rpHocVien.DataSource = dt.DefaultView;
            rpHocVien.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadDanhSach();
    }
}