﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_DanhSachCongTyTuKiemTra_LapDanhSach : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý danh sách các công ty kiểm toán phải nộp báo cáo tự kiểm tra";
    protected string _perOnFunc_DsCongTyNopBcTuKiemTra = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    protected string _idDanhSach = "";
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1><a href='/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra' class='MenuFuncLv1'>" + tenchucnang + @"</a>&nbsp;<img src='/images/next.png' style='margin-top:3px; height: 18px;' />&nbsp;<span class='MenuFuncLv2'>Lập danh sách</span></h1> </div>"));

        _perOnFunc_DsCongTyNopBcTuKiemTra = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, cm.connstr);

        this._idDanhSach = Library.CheckNull(Request.QueryString["iddanhsach"]);
        if (_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Xem))
        {
            if (!string.IsNullOrEmpty(_idDanhSach) && Library.CheckIsInt32(_idDanhSach))
            {
                DivDanhSachCongTyDaThem.Visible = true;
                SpanCacNutChucNang.Visible = true;
            }
            else
            {
                DivDanhSachCongTyDaThem.Visible = false;
                SpanCacNutChucNang.Visible = false;
            }
            try
            {
                _db.OpenConnection();
                if (Request.Form["hdAction"] != null && Request.Form["hdAction"] == "save")
                {
                    SaveThongTinDanhSach(this._idDanhSach);
                }
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }
        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Lấy thông tin danh sách theo ID
    /// </summary>
    /// <param name="idDanhSach">ID danh sách cần lấy dữ liệu</param>
    protected void LoadDanhSachById(string idDanhSach)
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach))
            {
                SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                KscldsBaoCaoTuKiemTra obj = pro.GetByDsBaoCaoTuKiemTraId(Library.Int32Convert(idDanhSach));
                if (obj != null && obj.DsBaoCaoTuKiemTraId > 0)
                {
                    string js = "$('#txtNgayLapDanhSach').val('" + obj.NgayLap.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#txtHanNop').val('" + obj.HanNop.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#spanTinhTrang').html('" + utility.GetStatusNameById(obj.TinhTrangId.ToString(), _db) + "');" + Environment.NewLine;
                    js += "$('#txtMaDanhSach').val('" + obj.MaDanhSach + "');" + Environment.NewLine;
                    js += "$('#hdDanhSachID').val('" + idDanhSach + "');" + Environment.NewLine;
                    js += "GetDanhSachCongTyDaAdd();" + Environment.NewLine; // Gọi js load danh sách các công ty đã add vào Danh Sách báo cáo
                    string statusCode = utility.GetStatusCodeById(obj.TinhTrangId.ToString(), _db).Trim();
                    if (statusCode == ListName.Status_ChoDuyet || statusCode == ListName.Status_KhongPheDuyet || statusCode == ListName.Status_ThoaiDuyet)
                        js += "$('#btnSendEmail').remove();" + Environment.NewLine;
                    if (statusCode == ListName.Status_KhongPheDuyet)
                    {
                        js += "$('#" + btnReject.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#spanTinhTrang').css('color', 'red');" + Environment.NewLine;
                    }
                    if (statusCode == ListName.Status_DaPheDuyet)
                    {
                        js += "$('#btnSave').remove();" + Environment.NewLine;
                        js += "$('#" + btnDelete.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + btnApprove.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#" + btnReject.ClientID + "').remove();" + Environment.NewLine;
                        js += "$('#spanTinhTrang').css('color', 'green');" + Environment.NewLine;
                        js += "$('#btnAddCongTy').remove();" + Environment.NewLine;
                        js += "$('#btnRemoveCongTy').remove();" + Environment.NewLine;
                    }
                    Response.Write(js);
                }
            }
            else
            {
                string js = "$('#txtNgayLapDanhSach').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                Response.Write(js);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Lưu thông tin danh sách tự kiểm tra
    /// </summary>
    /// <param name="idDanhSach">ID danh sách (Có: Edit; Không: Insert)</param>
    private void SaveThongTinDanhSach(string idDanhSach)
    {
        string strNgayLap = Request.Form["txtNgayLapDanhSach"];
        string strHanNop = Request.Form["txtHanNop"];
        DateTime ngayLap = DateTime.Now;
        DateTime hanNop = new DateTime();

        if (!string.IsNullOrEmpty(strNgayLap))
            ngayLap = Library.DateTimeConvert(strNgayLap, "dd/MM/yyyy");
        if (!string.IsNullOrEmpty(strHanNop))
            hanNop = Library.DateTimeConvert(strHanNop, "dd/MM/yyyy");

        SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);
        KscldsBaoCaoTuKiemTra obj = new KscldsBaoCaoTuKiemTra();
        if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach)) // Update
        {
            obj = pro.GetByDsBaoCaoTuKiemTraId(Library.Int32Convert(idDanhSach));
            obj.NgayLap = ngayLap;
            obj.HanNop = hanNop;
            try
            {
                _db.OpenConnection();
                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ChoDuyet, _db));
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }

            if (pro.Update(obj))
            {
                cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, "Cập nhật bản ghi có ID \"" + obj.DsBaoCaoTuKiemTraId + "\" của danh mục " + tenchucnang);
                Response.Redirect(Request.RawUrl);
            }
        }
        else // Insert
        {
            string tinhTrangId = utility.GetStatusID(ListName.Status_ChoDuyet, _db);
            if (Library.CheckIsInt32(tinhTrangId))
                obj.TinhTrangId = Library.Int32Convert(tinhTrangId);
            obj.MaDanhSach = GenMaDanhSach(ngayLap);
            obj.NgayLap = ngayLap;
            obj.HanNop = hanNop;
            try
            {
                _db.OpenConnection();
                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ChoDuyet, _db));
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }

            if (pro.Insert(obj))
            {
                cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, "Thêm bản ghi có ID \"" + obj.DsBaoCaoTuKiemTraId + "\" của danh mục " + tenchucnang);
                Response.Redirect(Request.RawUrl + "&iddanhsach=" + obj.DsBaoCaoTuKiemTraId);
            }
        }

    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Tạo mã danh sách mới
    /// </summary>
    /// <param name="ngayLap">Ngày lập danh sách</param>
    /// <returns></returns>
    private string GenMaDanhSach(DateTime ngayLap)
    {
        string maDanhSach = "BCTKT" + ngayLap.ToString("yy");
        string query = "SELECT TOP 1 MaDanhSach FROM " + ListName.Table_KSCLDSBaoCaoTuKiemTra + " WHERE MaDanhSach like '" + maDanhSach + "%' ORDER BY DSBaoCaoTuKiemTraID DESC";
        List<Hashtable> listData = _db.GetListData(query);
        int stt = 1;
        if (listData.Count > 0)
        {
            string temp_maDanhSach = listData[0]["MaDanhSach"].ToString();
            if (temp_maDanhSach.Length == 9)
            {
                stt = Library.Int32Convert(temp_maDanhSach.Substring(7, 2)) + 1;
            }
        }
        if (stt > 9)
            maDanhSach += stt;
        else
            maDanhSach += "0" + stt;
        return maDanhSach;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        else
            LoadDanhSachById(this._idDanhSach);
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Them))
        {
            if (string.IsNullOrEmpty(this._idDanhSach))
                Response.Write("$('#btnSave').remove();");
        }
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Sua))
        {
            if (!string.IsNullOrEmpty(this._idDanhSach))
            {
                Response.Write("$('#btnSave').remove();");
                Response.Write("$('#btnAddCongTy').remove();");
                Response.Write("$('#btnRemoveCongTy').remove();");
            }
        }
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Xoa))
            Response.Write("$('#" + btnDelete.ClientID + "').remove();");

        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Duyet))
        {
            Response.Write("$('#btnApprove').remove();");
            Response.Write("$('#btnReject').remove();");
        }

        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_KetXuat))
            Response.Write("$('#btExport').remove();");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        ChangeStatus(this._idDanhSach, ListName.Status_DaPheDuyet);
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        ChangeStatus(this._idDanhSach, ListName.Status_KhongPheDuyet);
    }

    private void ChangeStatus(string idDanhSach, string statusCode)
    {
        if (!string.IsNullOrEmpty(idDanhSach))
        {
            SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);
            KscldsBaoCaoTuKiemTra obj = new KscldsBaoCaoTuKiemTra();
            obj = pro.GetByDsBaoCaoTuKiemTraId(Library.Int32Convert(idDanhSach));
            try
            {
                _db.OpenConnection();
                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(statusCode, _db));

                if (pro.Update(obj))
                {
                    cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, "Cập nhật trạng thái bản ghi có ID \"" + obj.DsBaoCaoTuKiemTraId + "\" thành " + utility.GetStatusNameById(statusCode, _db) + " của danh mục " + tenchucnang);
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    js += "jQuery('#thongbaothanhcong_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Cập nhật trạng thái thành công.\").show();" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("ApproveSuccess", js);
                }
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }
        }
    }

    /// <summary>
    /// Xóa danh sách
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(this._idDanhSach) && Library.CheckIsInt32(this._idDanhSach))
            {
                SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);

                // Xóa các bản ghi chi tiết danh sách trước
                string deleteCommand = "DELETE FROM " + ListName.Table_KSCLDSBaoCaoTuKiemTraChiTiet + " WHERE DSBaoCaoTuKiemTraID = " + this._idDanhSach;
                if (_db.ExecuteNonQuery(deleteCommand) > 0)
                {
                    if (pro.Delete(Library.Int32Convert(this._idDanhSach)))
                    {
                        cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, "Xóa bản ghi có ID \"" + this._idDanhSach + "\" của danh mục " + tenchucnang);
                        Response.Redirect("/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra");
                    }
                }
            }
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }
}