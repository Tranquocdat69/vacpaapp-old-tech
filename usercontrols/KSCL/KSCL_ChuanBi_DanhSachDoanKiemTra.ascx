﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_DanhSachDoanKiemTra.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_DanhSachDoanKiemTra" %>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" clientidmode="Static" name="Form1" runat="server">
<h4 class="widgettitle">
    Danh sách đoàn kiểm tra</h4>
<div class="dataTables_length">
    <a id="btnOpenFormInsert" href="/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra_lapdoan"
        class="btn btn-rounded"><i class="iconfa-plus-sign"></i>Thêm mới</a> <a id="btnOpenFormUpdate"
            href="javascript:;" class="btn btn-rounded" onclick="OpenFormUpdate();">
            <i class="iconfa-edit"></i>Sửa</a>
    <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn" OnClick="btnDelete_Click"
        OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-trash"></i>Xóa</asp:LinkButton>
    <asp:LinkButton ID="btnApprove" runat="server" CssClass="btn" OnClick="btnApprove_Click"
        OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-thumbs-up"></i>Duyệt</asp:LinkButton>
    <asp:LinkButton ID="btnReject" runat="server" CssClass="btn" OnClick="btnReject_Click" OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-remove-sign"></i>Từ chối</asp:LinkButton>
    <asp:LinkButton ID="btnDontApprove" runat="server" CssClass="btn" OnClick="btnDontApprove_Click" OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-thumbs-down"></i>Thoái duyệt</asp:LinkButton>
    <asp:LinkButton ID="btnSendEmail" runat="server" CssClass="btn" OnClientClick="if(confirm('Bạn chắc chắn muốn gửi email thông báo tới các công ty trong danh sách này chứ?')){ return CheckHasChooseItem();} else {return false;}" onclick="btnSendEmail_Click"><i class="iconfa-inbox"></i>Gửi email</asp:LinkButton>
    <a href="javascript:;" id="btn_search" class="btn btn-rounded"
                onclick="OpenFormSearch();"><i class="iconfa-search"></i>Tìm kiếm</a>
</div>
<div>
    <fieldset class="fsBlockInfor">
        <legend>Danh sách đoàn kiểm tra</legend>
        <asp:GridView ClientIDMode="Static" ID="gv_DanhSachDoanKiemTra" runat="server" AutoGenerateColumns="False"
            class="table table-bordered responsive dyntable" AllowPaging="False" AllowSorting="True"
            OnPageIndexChanging="gv_DanhSachDoanKiemTra_PageIndexChanging" OnRowCommand="gv_DanhSachDoanKiemTra_RowCommand"
            OnRowDataBound="gv_DanhSachDoanKiemTra_RowDataBound" 
            onsorting="gv_DanhSachDoanKiemTra_Sorting">
            <Columns>
                <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
                    ItemStyle-Width="30px">
                    <ItemTemplate>
                        <input type="checkbox" id="checkbox" runat="server" class="colcheckbox" value='<%# Eval("ID")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mã đoàn kiểm tra" SortExpression="MaDoanKiemTra">
                    <ItemTemplate>
                        <%# Eval("MaDoanKiemTra")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày thành lập" SortExpression="NgayLap">
                    <ItemTemplate>
                        <%# Library.DateTimeConvert(Eval("NgayLap")).ToString("dd/MM/yyyy")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quyết định thành lập" SortExpression="QuyetDinhThanhLap">
                    <ItemTemplate>
                        <%# Eval("QuyetDinhThanhLap")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tình trạng" SortExpression="TenTrangThai">
                    <ItemTemplate>
                        <%# Eval("TenTrangThai") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Thao tác" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <div class="btn-group">
                            <a href='/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra_lapdoan&iddoan=<%# Eval("ID") %>'
                                data-placement="top" data-rel="tooltip" data-original-title="Xem/Sửa" rel="tooltip"
                                class="btn"><i class="iconsweets-create"></i></a>
                            <asp:LinkButton ID="btnDeleteInList" runat="server" CommandArgument='<%# Eval("ID") %>'
                                CommandName="delete_danhsach" data-placement="top" data-rel="tooltip" data-original-title="Xóa"
                                rel="tooltip" class="btn" OnClientClick="return confirm('Bạn chắc chắn muốn thực hiện thao tác này không?');"><i class="iconsweets-trashcan"></i></asp:LinkButton>
                            <asp:HiddenField ID="hfMaTrangThai" runat="server" Value='<%# Eval("MaTrangThai") %>' />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hf_TrangHienTai" runat="server" />
        <div class="dataTables_info" id="dyntable_info">
    <div class="pagination pagination-mini">
        Chuyển đến trang:
        <ul>
            <li><a href="javascript:;" onclick="$('#tranghientai').val(0);$('#hdAction').val('paging'); $('#user_search').submit();"><<
                Đầu tiên</a></li>
            <li><a href="javascript:;" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager option:selected').val()-1); $('#hdAction').val('paging'); $('#user_search').submit();}">
                < Trước</a>
            </li>
            <li><a style="border: none; background-color: #eeeeee">
                <asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" Style="width: 55px;
                    height: 22px;" onchange="$('#tranghientai').val(this.value);$('#hdAction').val('paging'); $('#user_search').submit();">
                </asp:DropDownList>
            </a></li>
            <li style="margin-left: 5px;"><a href="javascript:;" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) { $('#tranghientai').val(parseInt($('#Pager option:selected').val())+1); $('#hdAction').val('paging'); $('#user_search').submit();} ;"
                style="border-left: 1px solid #ccc;">Sau ></a></li>
            <li><a href="javascript:;" onclick="$('#tranghientai').val($('#Pager option').length-1);$('#hdAction').val('paging'); $('#user_search').submit();">
                Cuối cùng >></a></li>
        </ul>
    </div>
</div>
    </fieldset>
</div>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    // Hiển thị tooltip
    if (jQuery('#gv_DanhSachDoanKiemTra').length > 0) jQuery('#gv_DanhSachDoanKiemTra').tooltip({ selector: "a[rel=tooltip]" });

    jQuery(document).ready(function () {
        // dynamic table      

        $("#gv_DanhSachDoanKiemTra .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#gv_DanhSachDoanKiemTra :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });

    });
    // Created by NGUYEN MANH HUNG - 2014/12/12
    // Hàm mở Dialog tìm kiếm
    function OpenFormSearch() {
        $("#DivSearch").dialog({
            resizable: true,
            width: 600,
            height: 300,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm đoàn kiểm tra</b>",
            modal: true,
            zIndex: 1000
        });
        $("#DivSearch").parent().appendTo($("#Form1"));
    }

    function CheckHasChooseItem() {
        var flag = false;
        $('#gv_DanhSachDoanKiemTra :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all') {
                flag = true;
                return;
            }
        });
        if (!flag)
            alert('Phải chọn ít nhất một bản ghi để thực hiện thao tác!');
        return flag;
    }

    function OpenFormUpdate() {
        var count = 0;
        var id = '';
        $('#gv_DanhSachDoanKiemTra :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all') {
                count++;
                id = $(this).val();
            }
        });
        if (count == 0) {
            alert('Phải chọn một bản ghi để thực hiện thao tác!');
            return false;
        }
        if (count > 1) {
            alert('Chỉ được chọn một bản ghi để thực hiện cập nhật!');
            return false;
        }
        window.location.href = '/admin.aspx?page=kscl_chuanbi_danhsachdoankiemtra_lapdoan&iddoan=' + id;
    }

    // Created by NGUYEN MANH HUNG - 2014/12/18
    // Mở Form xem danh sách các công ty cần gửi Email
    function OpenDanhSachGuiEmail() {
        var listIdDanhSach = '';
        // Lấy List ID danh sách cần gửi Email
        $('#gv_DanhSachDoanKiemTra :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all')
                listIdDanhSach += $(this).val() + ',';
        });

        $("#DivDanhSachGuiEmail").empty();
        $("#DivDanhSachGuiEmail").append($("<iframe width='100%' height='100%' id='ifDanhSachGuiEmail' name='ifDanhSachGuiEmail' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_ChuanBi_XacNhanDanhSachGuiEmail&iddanhsach=" + listIdDanhSach));
        $("#DivDanhSachGuiEmail").dialog({
            resizable: true,
            width: 900,
            height: 700,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Gửi email đến công ty nộp báo cáo tự kiểm tra</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Gửi Email": function () {
                    window.ifDanhSachGuiEmail.ChooseAndSendEmail();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachGuiEmail').parent().find('button:contains("Gửi Email")').addClass('btn btn-rounded').prepend('<i class="iconfa-share"> </i>&nbsp;');
        $('#DivDanhSachGuiEmail').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function CloseDanhSachGuiEmail() {
        $("#DivDanhSachGuiEmail").dialog('close');
    }

    // Created by NGUYEN MANH HUNG - 2014/12/17
    // Gửi email
    function SendEmail(listIdCompanyNeedAdditional) {
        var listIdDanhSach = '';
        // Lấy List ID danh sách cần gửi Email
        $('#gv_DanhSachDoanKiemTra :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all')
                listIdDanhSach += $(this).val() + ',';
        });

        CloseDanhSachGuiEmail();
        $('#btnSendEmail').html('<img src="/images/loaders/loader1.gif" />Hệ thống đang gửi Email. Hãy chờ trong giây lát.');
        iframeProcess.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&iddanhsach=' + listIdDanhSach + '&listcompany=' + listIdCompanyNeedAdditional + '&action=sendmail&type=bctukiemtra';
    }

    function SendEmail_Success(success, listCompanyFail) {
        $('#btnSendEmail').html('<i class="iconfa-inbox"></i>Gửi email');
        var arrCompanyFail = listCompanyFail.split(';#');
        var html = "";
        if (listCompanyFail.length > 0)
            html = "<br />Những công ty dưới đây chưa đăng ký địa chỉ Email hoặc có sự cố khi gửi: <br />";
        for (var i = 0; i < arrCompanyFail.length; i++) {
            if (arrCompanyFail[i].length > 0)
                html += "- " + arrCompanyFail[i] + "<br />";
        }
        if (success == 1) {
            CallAlertSuccessFloatRightBottom('Đã gửi Email tới các đơn vị trong danh sách.' + html);
        } else {
            CallAlertErrorFloatRightBottom('Có sự cố trong quá trình gửi Email.' + html);
        }
    }
</script>
<div id="DivSearch" style="display: none;">
    <fieldset class="fsBlockInfor">
        <legend>Thông tin tìm kiếm</legend>
        <table width="100%" border="0" class="formtblInforWithoutBorder">
            <tr>
                <td style="width: 150px;">
                    Mã đoàn kiểm tra:
                </td>
                <td>
                    <asp:TextBox ID="txtMaDoanKiemTra" runat="server" Width="120px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ngày lập đoàn:
                </td>
                <td>
                    <asp:TextBox ID="txtNgayLapDoanTu" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>&nbsp;
                    <img src="/images/icons/calendar.png" id="imgNgayLapDoanTu" />
                    &nbsp;-&nbsp;
                    <asp:TextBox ID="txtNgayLapDoanDen" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>&nbsp;
                    <img src="/images/icons/calendar.png" id="imgNgayLapDoanDen" />
                </td>
            </tr>
            <tr>
                <td>
                    Tình trạng:
                </td>
                <td>
                    <asp:CheckBox ID="cboChoDuyet" runat="server" Checked="True" />Chờ duyệt&nbsp;
                    <asp:CheckBox ID="cboDaDuyet" runat="server" Checked="True" />Đã duyệt&nbsp;
                    <asp:CheckBox ID="cboTuChoi" runat="server" Checked="True" />Từ chối&nbsp;
                    <asp:CheckBox ID="cboThoaiDuyet" runat="server" Checked="True" />Thoái duyệt
                </td>
            </tr>
            <tr>
                <td>
                    ID.HVTT/ID.CTKT:
                </td>
                <td>
                    <asp:TextBox ID="txtMaCongTy" runat="server" Width="100px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Tên công ty kiểm toán:
                </td>
                <td>
                    <asp:TextBox ID="txtTenCongTy" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </fieldset>
    <div style="width: 100%; margin-top: 10px; text-align: right;">
        <asp:LinkButton ID="lbtTruyVan" runat="server" CssClass="btn" OnClick="lbtTruyVan_Click"
            OnClientClick="return CheckValidFormSearch();"><i class="iconfa-search"></i>Tìm</asp:LinkButton>
        <a href="javascript:;" id="A4" class="btn btn-rounded" onclick="$('#DivSearch').dialog('close');">
            <i class="iconfa-off"></i>Bỏ qua</a>
    </div>
</div>
<div id="DivDanhSachGuiEmail">
</div>
<script type="text/javascript">
    $('#<%= txtNgayLapDoanTu.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgayLapDoanTu").click(function () {
        $("#<%= txtNgayLapDoanTu.ClientID %>").datepicker("show");
    });
    $('#<%= txtNgayLapDoanDen.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgayLapDoanDen").click(function () {
        $("#<%= txtNgayLapDoanDen.ClientID %>").datepicker("show");
    });
    $(document).ready(function() {
        $("#Form1").validate({
            onsubmit: false
        });
    });

    function CheckValidFormSearch() {
        var isValid = $("#Form1").valid();
        if(isValid) {
            $('#DivSearch').dialog('close');
            return true;
        } else {
            return false;
        }
    }

    $("#DivSearch").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtTruyVan.ClientID %>').attr('href'));
        }
    });

    <% CheckPermissionOnPage();%>
</script>
</form>
<form id="user_search" method="post" enctype="multipart/form-data">
    <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
    <input type="hidden" value="0" id="hdAction" name="hdAction" />
</form>
