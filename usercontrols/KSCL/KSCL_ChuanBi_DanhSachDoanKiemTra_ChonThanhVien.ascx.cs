﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_ChuanBi_DanhSachDoanKiemTra_ChonThanhVien : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _idDoanKiemTra = "", _type;
    protected void Page_Load(object sender, EventArgs e)
    {
        _idDoanKiemTra = Library.CheckNull(Request.QueryString["iddoan"]);
        _type = Library.CheckNull(Request.QueryString["type"]);
        if (!IsPostBack)
        {
            LoadData(_idDoanKiemTra);
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/31
    /// Load danh sách thành viên theo điều kiện tìm kiếm
    /// </summary>
    /// <param name="idDoanKiemTra">ID đoàn kiểm tra</param>
    private void LoadData(string idDoanKiemTra)
    {
        try
        {
            _db.OpenConnection();
            List<int> listHoiVienCaNhanIdDaAdd = new List<int>(); // Chứa list ID hội viên cá nhân đã add vào đoàn kiểm tra
            List<int> listThanhVienDoanKiemTraIdDaAdd = new List<int>(); // Chứa list ID thành viên đoàn kiểm tra đã add vào đoàn kiểm tra
            string query = "SELECT HoiVienCaNhanID, ThanhVienDoanKiemTraID FROM " + ListName.Table_KSCLDoanKiemTraThanhVien + " WHERE DoanKiemTraID = " + idDoanKiemTra;
            List<Hashtable> listData = _db.GetListData(query);
            foreach (Hashtable ht in listData)
            {
                if (!string.IsNullOrEmpty(ht["HoiVienCaNhanID"].ToString()))
                    listHoiVienCaNhanIdDaAdd.Add(Library.Int32Convert(ht["HoiVienCaNhanID"]));
                if (!string.IsNullOrEmpty(ht["ThanhVienDoanKiemTraID"].ToString()))
                    listThanhVienDoanKiemTraIdDaAdd.Add(Library.Int32Convert(ht["ThanhVienDoanKiemTraID"]));
            }


            List<string> listParam = new List<string> { "@DonViCongTac", "@MaThanhVien", "@TenThanhVien" };
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value};

            // Dựa theo các tiêu chí tìm kiếm -> đưa giá trị tham số tương ứng vào store
            if (!string.IsNullOrEmpty(txtDonViCongTac.Text))
                listValue[0] = txtDonViCongTac.Text;
            if (!string.IsNullOrEmpty(txtMaThanhVien.Text))
                listValue[1] = txtMaThanhVien.Text;
            if (!string.IsNullOrEmpty(txtTenThanhVien.Text))
                listValue[2] = txtTenThanhVien.Text;
            //if (!string.IsNullOrEmpty(txtMaHoiVienCaNhan.Text))
            //    listValue[3] = txtMaHoiVienCaNhan.Text;
            //if (!string.IsNullOrEmpty(txtTenHoiVienCaNhan.Text))
            //    listValue[4] = txtTenHoiVienCaNhan.Text;

            DataTable dt = _db.GetDataTable(ListName.Proc_KSCL_SearchDanhSachThanhVienDoanKiemTra_1, listParam, listValue);
            List<DataRow> listDataRowBiXoa = new List<DataRow>(); // List chứa những DataRow sẽ bị xóa
            // Kiểm tra nếu những thành viên đã được add thì sẽ ko hiển thị
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (string.IsNullOrEmpty(dr["HVCN"].ToString())) // Thành viên đoàn kiểm tra
                {
                    if (listThanhVienDoanKiemTraIdDaAdd.Contains(Library.Int32Convert(dr["ID"])))
                        listDataRowBiXoa.Add(dr);
                }
                else // Hội viên cá nhân
                {
                    if (listHoiVienCaNhanIdDaAdd.Contains(Library.Int32Convert(dr["ID"])))
                        listDataRowBiXoa.Add(dr);
                }
            }
            foreach (DataRow dataRow in listDataRowBiXoa)
            {
                dt.Rows.Remove(dataRow);
            }
            rpDanhSachThanhVien.DataSource = dt.DefaultView;
            rpDanhSachThanhVien.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadData(_idDoanKiemTra);
    }
}