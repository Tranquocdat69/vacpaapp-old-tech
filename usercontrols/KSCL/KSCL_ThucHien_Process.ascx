﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ThucHien_Process.ascx.cs" Inherits="usercontrols_KSCL_ThucHien_Process" %>

<form id="FormUpdateCauHoi" method="POST"  enctype="multipart/form-data">
    <input type="hidden" id="hdLoaiCauHoi" name="hdLoaiCauHoi"/>
    <input type="hidden" id="hdCauHoiID" name="hdCauHoiID"/>
    <input type="hidden" id="hdNgayLapCauHoi" name="hdNgayLapCauHoi"/>
    <input type="hidden" id="hdNhomCauHoi" name="hdNhomCauHoi"/>
    <input type="hidden" id="hdCoHieuLuc" name="hdCoHieuLuc"/>
    <input type="hidden" id="hdCauHoi" name="hdCauHoi"/>
    <input type="hidden" id="hdDiemToiDa" name="hdDiemToiDa"/>
    <input type="hidden" id="hdCanCu" name="hdCanCu"/>
    <input type="hidden" id="hdHuongDanChamDiem" name="hdHuongDanChamDiem"/>
</form>
<form id="FormUploadFile" method="POST"  enctype="multipart/form-data">
    <input type="hidden" id="hdHoSoID" name="hdHoSoID"/>
</form>
<script type="text/javascript">
    function SaveCauHoi(arrData) {
        $('#hdCauHoiID').val(arrData[0]);
        $('#hdNgayLapCauHoi').val(arrData[1]);
        $('#hdNhomCauHoi').val(arrData[2]);
        $('#hdCoHieuLuc').val(arrData[3]);
        $('#hdCauHoi').val(arrData[4]);
        $('#hdDiemToiDa').val(arrData[5]);
        $('#hdCanCu').val(arrData[6]);
        $('#hdHuongDanChamDiem').val(arrData[7]);
        $('#hdLoaiCauHoi').val(arrData[8]);
        $('#FormUpdateCauHoi').submit();
    }

    function UpdateFile(obj, hoSoId) {
        $('#hdHoSoID').val(hoSoId);
        var $this = $(obj), $clone = $this.clone();
        //alert($this.val());
        $this.after($clone).appendTo($('#FormUploadFile'));
        $('#FormUploadFile').submit();
    }
</script>