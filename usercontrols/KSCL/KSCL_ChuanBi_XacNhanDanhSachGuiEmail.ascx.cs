﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_ChuanBi_XacNhanDanhSachGuiEmail : System.Web.UI.UserControl
{
    protected string _idDanhSach = "";
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        this._idDanhSach = Library.CheckNull(Request.QueryString["iddanhsach"]);
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(this._idDanhSach))
            {
                //LoadDanhSachCongTyChuaGuiEmail(this._idDanhSach);
                LoadDanhSachCongTyDaGuiEmail(this._idDanhSach);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Load danh sách nhưng công ty đã được gửi Email trong danh sách yêu cầu nộp báo cáo tự kiểm tra
    /// </summary>
    private void LoadDanhSachCongTyDaGuiEmail(string listIdDanhSach)
    {
        string clause = "";
        string[] arrIdDanhSach = listIdDanhSach.TrimEnd(',').Split(',');
        foreach (string idDanhSach in arrIdDanhSach)
        {
            if (!string.IsNullOrEmpty(idDanhSach.Trim()))
            {
                if (!string.IsNullOrEmpty(clause))
                    clause += " OR ";
                clause += "DSBaoCaoTuKiemTraID = " + idDanhSach;
            }
        }
        if (!string.IsNullOrEmpty(clause))
            clause = " AND (" + clause + ")";
        string query = @"SELECT a.DSBaoCaoTuKiemTraChiTietID, b.MaHoiVienTapThe, b.TenDoanhNghiep, b.TenVietTat, b.DiaChi, b.Email, b.DienThoai, b.NguoiDaiDienPL_Ten, b.NguoiDaiDienPL_DiDong  FROM tblKSCLDSBaoCaoTuKiemTraChiTiet AS a
                             LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID
                             WHERE a.TinhTrangGuiEmailThongBao = '2' " + clause;
        DataTable dt = _db.GetDataTable(query);
        rpDanhSach.DataSource = dt.DefaultView;
        rpDanhSach.DataBind();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Load danh sách những công ty đã được gửi Email thông báo yêu cầu nộp báo cáo
    /// </summary>
    /// <param name="listIdDanhSach">List ID danh sách </param>
//    private void LoadDanhSachCongTyChuaGuiEmail(string listIdDanhSach)
//    {
//        string clause = "";
//        string[] arrIdDanhSach = listIdDanhSach.Split(',');
//        foreach (string idDanhSach in arrIdDanhSach)
//        {
//            if (!string.IsNullOrEmpty(idDanhSach.Trim()))
//            {
//                if (!string.IsNullOrEmpty(clause))
//                    clause += " OR ";
//                clause += "DSBaoCaoTuKiemTraID = " + idDanhSach;
//            }
//        }
//        if (!string.IsNullOrEmpty(clause))
//            clause = " AND (" + clause + ")";
//        string query = @"SELECT COUNT(DSBaoCaoTuKiemTraChiTietID) AS SoCongTyChuaGuiEmail FROM tblKSCLDSBaoCaoTuKiemTraChiTiet                            
//                             WHERE TinhTrangGuiEmailThongBao = '2' " + clause;
//        List<Hashtable> listData = _db.GetListData(query);
//        if (listData.Count > 0)
//        {
//            lblSoCongTyChuaGuiEmail.Text = listData[0]["SoCongTyChuaGuiEmail"].ToString();
//        }
//    }
}