﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_ThucHien_BaoCaoKetQuaKiemTraKyThuat : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo kết quả kiểm tra kỹ thuật";
    protected string _listPermissionOnFunc_BCKetQuaKiemTraKyThuat = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected string _IDLopHoc = "";
    Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
            _listPermissionOnFunc_BCKetQuaKiemTraKyThuat = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ThucHien_BCKetQuaKTKT, cm.connstr);
            if (_listPermissionOnFunc_BCKetQuaKiemTraKyThuat.Contains("XEM|"))
            {
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "update")
                    Save();
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "submit")
                    Submit();
            }
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + " !</div>"));
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }

    protected void CheckPermissionOnPage()
    {
        // Load dữ liệu theo ID báo cáo truyền vào
        try
        {
            _db.OpenConnection();

            string idBaoCao = Library.CheckNull(Request.QueryString["idbaocao"]);
            if (!string.IsNullOrEmpty(idBaoCao))
            {
                string query = @"SELECT HS.MaHoSo FROM tblKSCLBaoCaoKTKT BCKTKT LEFT JOIN tblKSCLHoSo HS ON BCKTKT.HoSoID = HS.HoSoID
                                WHERE BCKTKT.BaoCaoKTKTID = " + idBaoCao;
                List<Hashtable> listData = _db.GetListData(query);
                if(listData.Count > 0)
                {
                    Response.Write("$('#txtMaHoSo').val('" + listData[0]["MaHoSo"] + "');");
                    Response.Write("iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso=" + listData[0]["MaHoSo"] + "&action=loadinforhoso';");
                }
            }
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }

        if (!_listPermissionOnFunc_BCKetQuaKiemTraKyThuat.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");

        if (!_listPermissionOnFunc_BCKetQuaKiemTraKyThuat.Contains("KETXUAT|"))
            Response.Write("$('#btnExport').remove();");

        if (!_listPermissionOnFunc_BCKetQuaKiemTraKyThuat.Contains("THEM|"))
            Response.Write("$('#btnAdd').remove();");

        if (!_listPermissionOnFunc_BCKetQuaKiemTraKyThuat.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");
    }

    private void Save()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            SqlKsclBaoCaoKtktProvider pro = new SqlKsclBaoCaoKtktProvider(ListName.ConnectionString, false, string.Empty);
            SqlKsclBaoCaoKtktChiTietProvider proChiTiet = new SqlKsclBaoCaoKtktChiTietProvider(ListName.ConnectionString, false, string.Empty);
            NameValueCollection nvc = Request.Form;

            if (!string.IsNullOrEmpty(nvc["hdHoSoID"]))
            {
                // Xử lý dữ liệu ở bảng KSCLBaoCaoKTKT trước
                KsclBaoCaoKtkt obj;
                string baoCaoKTKTId = Library.CheckNull(Request.QueryString["idbaocao"]);
                if (string.IsNullOrEmpty(baoCaoKTKTId)) // insert
                {
                    obj = new KsclBaoCaoKtkt();
                    if (CheckSoBaoCaoKTIsExist(nvc["txtBaoCaoKiemToanSo"], "insert", string.Empty))
                    {
                        js = "<script type='text/javascript'>" + Environment.NewLine;
                        js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Số báo cáo kiểm toán đã được báo cáo kết quả kiểm tra kỹ thuật.\").show();" + Environment.NewLine;
                        js += "</script>";
                        Page.RegisterStartupScript("SaveFail1", js);
                        return;
                    }
                }
                else
                {
                    obj = pro.GetByBaoCaoKtktid(Library.Int32Convert(baoCaoKTKTId));
                    if (obj.TinhTrangId == Library.Int32Convert(utility.GetStatusID(ListName.Status_DaPheDuyet, _db)))
                    {
                        js = "<script type='text/javascript'>" + Environment.NewLine;
                        js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Báo cáo này đã được nộp. Bạn không được phép sửa nội dung của báo cáo.\").show();" + Environment.NewLine;
                        js += "</script>";
                        Page.RegisterStartupScript("SaveFail", js);
                        return;
                    }
                    if (CheckSoBaoCaoKTIsExist(nvc["txtBaoCaoKiemToanSo"], "update", baoCaoKTKTId))
                    {
                        js = "<script type='text/javascript'>" + Environment.NewLine;
                        js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Số báo cáo kiểm toán đã được báo cáo kết quả kiểm tra kỹ thuật.\").show();" + Environment.NewLine;
                        js += "</script>";
                        Page.RegisterStartupScript("SaveFail2", js);
                        return;
                    }
                }
                obj.HoSoId = Library.Int32Convert(nvc["hdHoSoID"]);
                obj.XepLoai = nvc["ddlXepHang"];
                obj.ThanhVienChamDiem1 = Library.Int32Convert(nvc["ddlThanhVienChamDiem1"]);
                obj.ThanhVienChamDiem2 = Library.Int32Convert(nvc["ddlThanhVienChamDiem2"]);
                obj.TenKhachHangKt = nvc["txtTenKhachHangKiemToan"];
                obj.BctcNam = nvc["txtBaoCaoTaiChinhNam"];
                obj.DonViChungKhoan = nvc["cboDonViChungKhoan"] == "on" ? "1" : "0";
                obj.PhiKiemToan = Library.DecimalConvert(nvc["txtPhiKiemToan"]);
                obj.SoBaoCaoKt = nvc["txtBaoCaoKiemToanSo"];
                obj.NgayBaoCaoKt = Library.DateTimeConvert(nvc["txtNgayBaoCaoKiemToan"], "dd/MM/yyyy");
                obj.BgdTen = nvc["txtThanhVienBGD"];
                obj.BgdSoGiayCndkhn = nvc["txtGiayChungNhanDKHN_ThanhVienBGD"];
                obj.BgdNgayCndkhn = Library.DateTimeConvert(nvc["txtNgayCapGiayChungNhanDKHN_ThanhVienBGD"], "dd/MM/yyyy");
                obj.KtvTen = nvc["txtKiemToanVien"];
                obj.KtvSoGiayCndkhn = nvc["txtGiayChungNhanDKHN_KiemToanVien"];
                obj.KtvNgayCndkhn = Library.DateTimeConvert(nvc["txtNgayCapGiayChungNhanDKHN_KiemToanVien"], "dd/MM/yyyy");
                obj.DangYkienKtid = Library.Int32Convert(nvc["ddlDangYKienKiemToan"]);
                obj.DangYkienKtKhac = nvc["txtDangYKienKiemToanKhac"];
                if (string.IsNullOrEmpty(baoCaoKTKTId)) // insert
                    pro.Insert(obj);
                else
                    pro.Update(obj);

                // Xóa hết dữ liệu ở bảng KSCLBaoCaoKTKTChiTiet theo ID của báo cáo KTKT
                string command = @"DELETE FROM " + ListName.Table_KSCLBaoCaoKTKTChiTiet + " WHERE BaoCaoKTKTID = " + obj.BaoCaoKtktid;
                _db.ExecuteNonQuery(command);

                // Insert dữ liệu KSCLBaoCaoKTKTChiTiet
                string[] arrKeys = nvc.AllKeys;
                for (int i = 0; i < arrKeys.Length; i++)
                {
                    string key = arrKeys[i];
                    if (key.StartsWith("rbtTraLoi_")) // Update vai trò
                    {
                        KsclBaoCaoKtktChiTiet objChiTiet = new KsclBaoCaoKtktChiTiet();
                        string traLoi = nvc[key];
                        string diem = nvc["txtDiemThucTe_" + key.Split('_')[1]];
                        string ghiChu = nvc["txtGhiChu_" + key.Split('_')[1]];
                        objChiTiet.CauHoiKtktid = Library.Int32Convert(nvc["hdCauHoiID_" + key.Split('_')[1]]);
                        objChiTiet.BaoCaoKtktid = obj.BaoCaoKtktid;
                        objChiTiet.TraLoi = traLoi;
                        objChiTiet.DiemThucTe = Library.DecimalConvert(diem.Replace('.',','));
                        objChiTiet.GhiChu = ghiChu;
                        proChiTiet.Insert(objChiTiet);
                    }
                }

                js = "<script type='text/javascript'>" + Environment.NewLine;
                js += "CallAlertSuccessFloatRightBottom('Lưu báo cáo kết quả kiểm tra thành công.');" + Environment.NewLine;
                js += "$('#txtMaHoSo').val('" + nvc["txtMaHoSo"] + "');" + Environment.NewLine;
                js += "iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso=" + nvc["txtMaHoSo"] + "&action=loadinforhoso';" + Environment.NewLine;
                js += "</script>";
                Page.RegisterStartupScript("SaveSucess", js);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG
    /// </summary>
    /// <param name="soBaoCaoKT">Số báo cáo kiểm toán cần kiểm tra</param>
    /// <returns>True: Đã tồn tại; False: Chưa tồn tại</returns>
    private bool CheckSoBaoCaoKTIsExist(string soBaoCaoKT, string type, string idBaoCao)
    {
        string query = "SELECT BaoCaoKTKTID FROM " + ListName.Table_KSCLBaoCaoKTKT + " WHERE SoBaoCaoKT = '" + soBaoCaoKT + "'";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            if (type == "insert")
                return true;
            else
            {
                if (listData[0]["BaoCaoKTKTID"].ToString() != idBaoCao)
                    return true;
            }
        }
        return false;
    }

    private void Submit()
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(Request.Form["hdHoSoID"]))
            {
                string query = "SELECT BaoCaoKTKTID FROM " + ListName.Table_KSCLBaoCaoKTKT + " WHERE HoSoID = " + Request.Form["hdHoSoID"];
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    string baoCaoKTKTId = listData[0]["BaoCaoKTKTID"].ToString();
                    SqlKsclBaoCaoKtktProvider pro = new SqlKsclBaoCaoKtktProvider(ListName.ConnectionString, false, string.Empty);
                    KsclBaoCaoKtkt obj = pro.GetByBaoCaoKtktid(Library.Int32Convert(baoCaoKTKTId));
                    if (obj != null && obj.BaoCaoKtktid > 0)
                    {
                        if (obj.TinhTrangId == Library.Int32Convert(utility.GetStatusID(ListName.Status_DaPheDuyet, _db)))
                        {
                            string js = "<script type='text/javascript'>" + Environment.NewLine;
                            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Báo cáo này đã được nộp. Bạn không được nộp thêm lần nữa.\").show();" + Environment.NewLine;
                            js += "</script>";
                            Page.RegisterStartupScript("SubmitFail", js);
                        }
                        obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_DaPheDuyet, _db));
                        if (pro.Update(obj))
                        {
                            string js = "<script type='text/javascript'>" + Environment.NewLine;
                            js += "jQuery('#thongbaothanhcong_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Đã nộp báo cáo kết quả kiểm tra kỹ thuật.\").show();" + Environment.NewLine;
                            js += "</script>";
                            Page.RegisterStartupScript("SubmitSuccess", js);
                        }
                    }
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void LoadDangYKienKiemToan()
    {
        try
        {
            _db.OpenConnection();

            string query = "SELECT DangYKienKTID, TenDangYKien FROM tblDMDangYKienKT";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                string js = "";
                foreach (Hashtable ht in listData)
                {
                    js += "<option value='" + ht["DangYKienKTID"] + "'>" + ht["TenDangYKien"] + "</option>" + Environment.NewLine;
                }
                Response.Write(js);
            }
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }
}