﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_DanhSachCongTyTuKiemTra_LapDanhSach.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_DanhSachCongTyTuKiemTra_LapDanhSach" %>

<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" name="Form1" clientidmode="Static" runat="server" method="post"
enctype="multipart/form-data">
<h4 class="widgettitle">
    Lập danh sách công ty kiểm toán phải nộp báo cáo tự kiểm tra</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <table style="max-width: 700px;" border="0" class="formtbl">
        <tr>
            <td>
                Mã danh sách:
            </td>
            <td>
                <input type="text" id="txtMaDanhSach" name="txtMaDanhSach" readonly="readonly" />
                <input type="hidden" id="hdDanhSachID" name="hdDanhSachID" />
                <input type="hidden" id="hdAction" name="hdAction" />
            </td>
            <td>
                Ngày lập danh sách<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtNgayLapDanhSach" name="txtNgayLapDanhSach" style="width: 80px;" /><img
                    src="/images/icons/calendar.png" id="imgNgayLapDanhSach" />
            </td>
        </tr>
        <tr>
            <td>
                Tình trạng:
            </td>
            <td>
                <span id="spanTinhTrang"></span>
            </td>
            <td>
                Hạn nộp<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtHanNop" name="txtHanNop" style="width: 80px;" /><img src="/images/icons/calendar.png"
                    id="imgHanNop" />
            </td>
        </tr>
    </table>
</fieldset>
<div style="text-align: center; width: 100%; margin-top: 10px;">
    <a id="btnSave" href="javascript:;" class="btn btn-rounded" onclick="SubmitForm();">
        <i class="iconfa-plus-sign"></i>Lưu</a> <span id="SpanCacNutChucNang" runat="server"
            visible="False">
            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn" OnClick="btnDelete_Click"
                OnClientClick="return confirm('Bạn chắc chắn muốn xóa danh sách này chứ?');"><i class="iconfa-trash"></i>Xóa</asp:LinkButton>
            <asp:LinkButton ID="btnApprove" runat="server" CssClass="btn" OnClick="btnApprove_Click"
                OnClientClick="$('#hdAction').val('approve'); return confirm('Bạn chắc chắn muốn duyệt danh sách này chứ?');"><i class="iconfa-thumbs-up">
            </i>Duyệt</asp:LinkButton>
            <asp:LinkButton ID="btnReject" runat="server" CssClass="btn" OnClick="btnReject_Click"
                OnClientClick="$('#hdAction').val('reject'); return confirm('Bạn chắc chắn muốn từ chối danh sách này chứ?');"><i class="iconfa-remove-sign"></i>Từ chối</asp:LinkButton>
            <a id="btnSendEmail" href="javascript:;" class="btn btn-rounded" onclick="OpenDanhSachGuiEmail();">
                <i class="iconfa-inbox"></i>Gửi email</a> <a id="btExport" href="javascript:;" class="btn btn-rounded"
                    onclick="CallReportPage();"><i class="iconfa-download"></i>Kết xuất</a></span>
</div>
<div id="DivDanhSachCongTyDaThem" runat="server" visible="False">
    <fieldset class="fsBlockInfor">
        <input type='hidden' id="hdListBaoCaoChiTietID" />
        <legend>Danh sách công ty kiểm toán phải nộp báo cáo tự kiểm tra</legend>
        <div style="margin-bottom: 10px;">
            <a id="btnAddCongTy" href="javascript:;" class="btn btn-rounded" onclick="OpenDanhSachCongTyKiemToan();">
                <i class="iconfa-plus-sign"></i>Thêm</a> <a id="btnRemoveCongTy" href="javascript:;"
                    class="btn btn-rounded" onclick="RemoveCompanyFromList();"><i class="iconfa-trash">
                    </i>Xóa</a>
        </div>
        <table id="tblDanhSachCongTyDaAdd" width="100%" border="0" class="formtbl">
            <thead>
                <tr>
                    <th rowspan="2" class="firstColumn">
                        <input type="checkbox" class="checkall" value="all" />
                    </th>
                    <th rowspan="2">
                        STT
                    </th>
                    <th rowspan="2">
                        Mã HVTC/CTKT
                    </th>
                    <th rowspan="2">
                        Tên công ty
                    </th>
                    <th rowspan="2">
                        Tên viết tắt
                    </th>
                    <th rowspan="2">
                        Địa chỉ
                    </th>
                    <th rowspan="2">
                        Email
                    </th>
                    <th rowspan="2">
                        Số điện thoại
                    </th>
                    <th colspan="2">
                        Người đại diện theo pháp luật
                    </th>
                </tr>
                <tr>
                    <th>
                        Họ và tên
                    </th>
                    <th>
                        Mobile
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </fieldset>
</div>
</form>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_Export" width="0px" height="0px" src="/iframe.aspx?page=Export_Process">
</iframe>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#txtNgayLapDanhSach").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgayLapDanhSach").click(function () {
        $("#txtNgayLapDanhSach").datepicker("show");
    });

    $("#txtHanNop").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgHanNop").click(function () {
        $("#txtHanNop").datepicker("show");
    });

    function OpenDanhSachCongTyKiemToan() {
        $("#DivDanhSachCongTyKiemToan").empty();
        $("#DivDanhSachCongTyKiemToan").append($("<iframe width='100%' height='100%' id='ifDanhSachCongTyKiemToan' name='ifDanhSachCongTyKiemToan' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=HoiVienTapThe_DanhSachCongTyKiemToan&type=bctukiemtra&iddanhsach=" + $('#hdDanhSachID').val()));
        $("#DivDanhSachCongTyKiemToan").dialog({
            resizable: true,
            width: 900,
            height: 700,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách công ty kiểm toán</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSachCongTyKiemToan.Choose();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachCongTyKiemToan').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivDanhSachCongTyKiemToan').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }

    function CloseDanhSachCongTyKiemToan() {
        $("#DivDanhSachCongTyKiemToan").dialog('close');
    }

    function AddCompanyToList(listIdCongTy) {
        iframeProcess.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&iddanhsach='+$('#hdDanhSachID').val()+'&listid='+listIdCongTy+'&action=addcompany&type=bctukiemtra';
    }
    
    function RemoveCompanyFromList() {
        if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')) {
            var listId = '';
            $('#tblDanhSachCongTyDaAdd :checkbox').each(function() {
                if ($(this).prop("checked") == true && $(this).val() != 'all') {
                    listId += $(this).val().split(',')[1] + ',';
                }
                    
            });
            if(listId.length > 0)
                iframeProcess.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&listidchitietdanhsach=' + listId + '&action=removecompany&type=bctukiemtra';
            else {
                alert('Phải chọn ít nhất một công ty để tiếp xóa khỏi danh sách!');
            }
        }
    }

    $('#<%=Form1.ClientID %>').validate({
        rules: {
            txtNgayLapDanhSach: {
                required: true, dateITA: true
            },
            txtHanNop: {
                required: true, dateITA: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }
    });
    
    function SubmitForm() {
        var ngayLap = $('#txtNgayLapDanhSach').datepicker('getDate');
        var hanNop = $('#txtHanNop').datepicker('getDate');
        
        if(ngayLap != null && hanNop != null) {
            hanNop.setDate(hanNop.getDate() + 1);
            var minus = (hanNop - ngayLap) / 1000 / 60 / 60 / 24;
            if(minus <= 0){
                $('#txtHanNop').val('');
                $('#txtHanNop').focus();
                alert('Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc!');
                return;
            }
        }
        $('#hdAction').val('save');
        $('#<%=Form1.ClientID %>').submit();
    }
    
    function GetDanhSachCongTyDaAdd() {
        var idDanhSach = $('#hdDanhSachID').val();
        if(idDanhSach != '') {
            iframeProcess.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&iddanhsach='+idDanhSach+'&action=loaddsctdaadd&type=bctukiemtra';
        }
    }
    
    var _arrDataDanhSachCongTyDaAdd = [];
    // Created by NGUYEN MANH HUNG - 2014/12/15
    // Hàm vẽ danh sách các công ty đã add vào Danh sách từ Iframe
    function DrawData_DangKyCongTyDaAdd(arrData) {
        _arrDataDanhSachCongTyDaAdd = arrData;
        var tbl = document.getElementById('tblDanhSachCongTyDaAdd');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 2; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var baoCaoChiTietId = arrData[i][0];
                var maCongTy = arrData[i][1];
                var tenCongTy = arrData[i][2];
                var tenVietTat = arrData[i][3];
                var diaChi = arrData[i][4];
                var email = arrData[i][5];
                var soDt = arrData[i][6];
                var tenNguoiDaiDien = arrData[i][7];
                var soDtNguoiDaiDien = arrData[i][8];
                

                var tr = document.createElement('TR');
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                tdCheckBox.className = 'firstColumn';
                var checkbox = document.createElement('input');
                checkbox.type = "checkbox";
                checkbox.value = i + ',' + baoCaoChiTietId;
                checkbox.onclick = function() { GetOneCheckBox(); };
                tdCheckBox.appendChild(checkbox);
                tr.appendChild(tdCheckBox);
                
                var arrColumn = [(i + 1), maCongTy, tenCongTy, tenVietTat, diaChi, email, soDt, tenNguoiDaiDien, soDtNguoiDaiDien];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
    }
    
    function GetOneCheckBox() {
        var listID = '';
        $('#hdListBaoCaoChiTietID').val('');
        $('#tblDanhSachCongTyDaAdd :checkbox').each(function () {
            var checked = $(this).prop("checked");
            if ((checked) && ($(this).val() != "all")){
                listID += $(this).val().split(',')[1] + ',';
            }
        });
        $('#hdListBaoCaoChiTietID').val(listID);
    }
    
    // Created by NGUYEN MANH HUNG - 2014/12/17
    // Kết xuất danh sách ra Excel
    function ExportExcel() {
        var temp_html = $('#tblDanhSachCongTyDaAdd').html(); // Chứa nội dung gốc của Table trước khi loại bỏ cột đầu tiên
        
        // Bỏ cột đầu tiên chứa checkbox ra khỏi Table
        $('#tblDanhSachCongTyDaAdd .firstColumn').each(function(index, cell) {
            $(cell).remove();
        });
        var html = $('#tblDanhSachCongTyDaAdd').html();
        $('#tblDanhSachCongTyDaAdd').html(temp_html); // Gán lại nội dung gốc cho Table sau khi đã lấy chuỗi giá trị để kết xuất
        var exportContent = "<table><tr><td colspan='9'>Mã danh sách: "+$('#txtMaDanhSach').val()+"</td></tr>" +
            "<tr><td colspan='9'>Ngày lập: "+$('#txtNgayLapDanhSach').val()+"</td></tr>" +
            "<tr><td colspan='9'>Hạn nộp: "+$('#txtHanNop').val()+"</td></tr>" +
            "<tr><td colspan='9'><b>Danh sách công ty kiểm toán phải nộp báo cáo tự kiểm tra</b></td></tr></table>";
        exportContent += "<table border='1'>" + html + "</table>";
        window.iframeProcess_Export.AddContent(exportContent, 'Excel', 'Danh sách công ty phải nộp báo cáo tự kiểm tra'); // Gọi trang xử lý kết xuất dữ liệu
    }
    
    function OpenDanhSachGuiEmail() {
        $("#DivDanhSachGuiEmail").empty();
        $("#DivDanhSachGuiEmail").append($("<iframe width='100%' height='100%' id='ifDanhSachGuiEmail' name='ifDanhSachGuiEmail' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_ChuanBi_XacNhanDanhSachGuiEmail&iddanhsach=" + $('#hdDanhSachID').val()));
        $("#DivDanhSachGuiEmail").dialog({
            resizable: true,
            width: 900,
            height: 700,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Gửi email đến công ty nộp báo cáo tự kiểm tra</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Gửi Email": function () {
                    window.ifDanhSachGuiEmail.ChooseAndSendEmail();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachGuiEmail').parent().find('button:contains("Gửi Email")').addClass('btn btn-rounded').prepend('<i class="iconfa-share"> </i>&nbsp;');
        $('#DivDanhSachGuiEmail').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function CloseDanhSachGuiEmail() {
        $("#DivDanhSachGuiEmail").dialog('close');
    }
    
    // Created by NGUYEN MANH HUNG - 2014/12/17
    // Gửi email
    function SendEmail(listIdCompanyNeedAdditional) {
        CloseDanhSachGuiEmail();
        $('#btnSendEmail').html('<img src="/images/loaders/loader1.gif" />Hệ thống đang gửi Email. Hãy chờ trong giây lát.');
        iframeProcess.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&iddanhsach='+$('#hdDanhSachID').val()+'&listcompany='+listIdCompanyNeedAdditional+'&action=sendmail&type=bctukiemtra';
    }
    
    function SendEmail_Success(success, listCompanyFail) {
        $('#btnSendEmail').html('<i class="iconfa-inbox"></i>Gửi email');
        var arrCompanyFail = listCompanyFail.split(';#');
        var html = "";
        if(listCompanyFail.length > 0)
            html = "<br />Những công ty dưới đây chưa đăng ký địa chỉ Email hoặc có sự cố khi gửi: <br />";
        for(var i = 0; i< arrCompanyFail.length; i++) {
            if(arrCompanyFail[i].length > 0)
                html += "- " + arrCompanyFail[i] + "<br />";
        }
        if (success == 1) {
                jQuery("#thongbaothanhcong_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Đã gửi Email tới các đơn vị trong danh sách." + html).show();
            } else {
                jQuery("#thongbaoloi_form").html("<button class='close' type='button' data-dismiss='alert'>×</button>Có sự cố trong quá trình gửi Email." + html).show();
            }
    }
    
    jQuery(document).ready(function () {
        // dynamic table      
        $('#tblDanhSachCongTyDaAdd').stickyTableHeaders();

        $("#tblDanhSachCongTyDaAdd .checkall").bind("click", function () {
            var listId = '';
            $('#hdListBaoCaoChiTietID').val('');
            var  checked = $(this).prop("checked");
            $('#tblDanhSachCongTyDaAdd :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all"))
                    listId += $(this).val().split(',')[1] + ',';
            });
            $('#hdListBaoCaoChiTietID').val(listId);
        });
    });
    
    function CallReportPage() {
        var url = "/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra&mads=" + $('#txtMaDanhSach').val();
        window.open(url, '_blank');
    }
    
    <% CheckPermissionOnPage();%>
</script>
<div id="DivDanhSachCongTyKiemToan">
</div>
<div id="DivDanhSachGuiEmail">
</div>