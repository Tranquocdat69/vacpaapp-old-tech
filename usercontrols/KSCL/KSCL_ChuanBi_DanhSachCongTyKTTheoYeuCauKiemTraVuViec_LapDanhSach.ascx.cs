﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_DanhSachCongTyKTTheoYeuCauKiemTraVuViec_LapDanhSach : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý danh sách các công ty kiểm toán kiểm tra theo yêu cầu";
    protected string _perOnFunc_DSCongTyKTTheoVuViec = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    protected string _idDanhSach = "";
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1><a href='/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec' class='MenuFuncLv1'>" + tenchucnang + @"</a></a>&nbsp;<img src='/images/next.png' style='margin-top:3px; height: 18px;' />&nbsp;<span class='MenuFuncLv2'>Lập danh sách</span></h1> </div>"));

        _perOnFunc_DSCongTyKTTheoVuViec = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ChuanBi_DSCongTyKTTheoVuViec, cm.connstr);

        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        if (Session["MsgSuccess"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + Session["MsgSuccess"] + "</div>"));
            Session.Remove("MsgSuccess");
        }

        this._idDanhSach = Library.CheckNull(Request.QueryString["iddanhsach"]);
        if (_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Xem))
        {
            if (!string.IsNullOrEmpty(_idDanhSach) && Library.CheckIsInt32(_idDanhSach))
            {
                DivDanhSachCongTyDaThem.Visible = true;
                SpanCacNutChucNang.Visible = true;
            }
            else
            {
                DivDanhSachCongTyDaThem.Visible = false;
                SpanCacNutChucNang.Visible = false;
            }
            try
            {
                _db.OpenConnection();
                if (Request.Form["hdAction"] != null && Request.Form["hdAction"] == "save")
                {
                    SaveThongTinDanhSach(this._idDanhSach);
                }
            }
            catch { }
            finally
            {
                _db.CloseConnection();
            }
        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Lấy thông tin danh sách theo ID
    /// </summary>
    /// <param name="idDanhSach">ID danh sách cần lấy dữ liệu</param>
    protected void LoadDanhSachById(string idDanhSach)
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach))
            {
                SqlKscldsKiemTraVuViecProvider pro = new SqlKscldsKiemTraVuViecProvider(ListName.ConnectionString, false, string.Empty);
                KscldsKiemTraVuViec obj = pro.GetByDsYeuCauKiemTraVuViecId(Library.Int32Convert(idDanhSach));
                if (obj != null && obj.DsYeuCauKiemTraVuViecId > 0)
                {
                    string js = "$('#txtNgayLapDanhSach').val('" + obj.NgayLap.Value.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#txtMaDanhSach').val('" + obj.MaDanhSach + "');" + Environment.NewLine;
                    js += "$('#hdDanhSachID').val('" + idDanhSach + "');" + Environment.NewLine;
                    if (obj.FileCanCu != null && obj.FileCanCu.Length > 0)
                    {
                        js += "$('#spanTenFileCanCu').html('" + obj.TenFile + "');" + Environment.NewLine;
                        js += "$('#btnDownload').prop('href', '/admin.aspx?page=getfile&id=" + obj.DsYeuCauKiemTraVuViecId + "&type=" + ListName.Table_KSCLDSKiemTraVuViec + "');" + Environment.NewLine;
                    }
                    else
                    {
                        js += "$('#spanTenFileCanCu').html('Chưa có file được chọn');" + Environment.NewLine;
                        js += "$('#btnDownload').remove();" + Environment.NewLine;
                    }
                    js += "GetDanhSachCongTyDaAdd();" + Environment.NewLine; // Gọi js load danh sách các công ty đã add vào Danh Sách báo cáo
                    Response.Write(js);
                }
            }
            else
            {
                string js = "$('#txtNgayLapDanhSach').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                js += "$('#spanTenFileCanCu').html('Chưa có file được chọn');" + Environment.NewLine;
                js += "$('#btnDownload').remove();" + Environment.NewLine;
                Response.Write(js);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Lưu thông tin danh sách công ty kiểm toán kiểm tra theo yêu cầu
    /// </summary>
    /// <param name="idDanhSach">ID danh sách (Có: Edit; Không: Insert)</param>
    private void SaveThongTinDanhSach(string idDanhSach)
    {
        string strNgayLap = Request.Form["txtNgayLapDanhSach"];
        HttpPostedFile fileCanCu = Request.Files["FileCanCu"];
        DateTime ngayLap = DateTime.Now;

        if (!string.IsNullOrEmpty(strNgayLap))
            ngayLap = Library.DateTimeConvert(strNgayLap, "dd/MM/yyyy");

        SqlKscldsKiemTraVuViecProvider pro = new SqlKscldsKiemTraVuViecProvider(ListName.ConnectionString, false, string.Empty);
        KscldsKiemTraVuViec obj = new KscldsKiemTraVuViec();
        if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach)) // Update
        {
            obj = pro.GetByDsYeuCauKiemTraVuViecId(Library.Int32Convert(idDanhSach));
            obj.NgayLap = ngayLap;
            if (fileCanCu.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(fileCanCu.InputStream);
                byte[] fileByte = br.ReadBytes(fileCanCu.ContentLength);
                obj.FileCanCu = fileByte;
                obj.TenFile = fileCanCu.FileName;
            }
            if (pro.Update(obj))
            {
                cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTTheoVuViec, "Cập nhật bản ghi có ID \"" + obj.DsYeuCauKiemTraVuViecId + "\" của danh mục " + tenchucnang);
                Session["MsgSuccess"] = "Lưu thông tin thành công!";
                Response.Redirect(Request.RawUrl);
            }
        }
        else // Insert
        {
            obj.MaDanhSach = GenMaDanhSach(ngayLap);
            obj.NgayLap = ngayLap;
            if (fileCanCu.ContentLength > 0)
            {
                BinaryReader br = new BinaryReader(fileCanCu.InputStream);
                byte[] fileByte = br.ReadBytes(fileCanCu.ContentLength);
                obj.FileCanCu = fileByte;
                obj.TenFile = fileCanCu.FileName;
            }
            if (pro.Insert(obj))
            {
                cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTTheoVuViec, "Thêm bản ghi có ID \"" + obj.DsYeuCauKiemTraVuViecId + "\" của danh mục " + tenchucnang);
                Session["MsgSuccess"] = "Lưu thông tin thành công!";
                Response.Redirect(Request.RawUrl + "&iddanhsach=" + obj.DsYeuCauKiemTraVuViecId);
            }
        }

    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Tạo mã danh sách mới
    /// </summary>
    /// <param name="ngayLap">Ngày lập danh sách</param>
    /// <returns></returns>
    private string GenMaDanhSach(DateTime ngayLap)
    {
        string maDanhSach = "KTVV" + ngayLap.ToString("yy");
        string query = "SELECT TOP 1 MaDanhSach FROM " + ListName.Table_KSCLDSKiemTraVuViec + " WHERE MaDanhSach like '" + maDanhSach + "%' ORDER BY DSYeuCauKiemTraVuViecID DESC";
        List<Hashtable> listData = _db.GetListData(query);
        int stt = 1;
        if (listData.Count > 0)
        {
            string temp_maDanhSach = listData[0]["MaDanhSach"].ToString();
            if (temp_maDanhSach.Length == 8)
            {
                stt = Library.Int32Convert(temp_maDanhSach.Substring(6, 2)) + 1;
            }
        }
        if (stt > 9)
            maDanhSach += stt;
        else
            maDanhSach += "0" + stt;
        return maDanhSach;
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        else
            LoadDanhSachById(this._idDanhSach);
        if (!_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Them))
        {
            if (string.IsNullOrEmpty(this._idDanhSach))
                Response.Write("$('#btnSave').remove();");
        }
        if (!_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Sua))
        {
            if (!string.IsNullOrEmpty(this._idDanhSach))
            {
                Response.Write("$('#btnSave').remove();");
                Response.Write("$('#btnAddCongTy').remove();");
                Response.Write("$('#btnRemoveCongTy').remove();");
            }
        }
        if (!_perOnFunc_DSCongTyKTTheoVuViec.Contains(ListName.PERMISSION_Xoa))
            Response.Write("$('#" + btnDelete.ClientID + "').remove();");

    }

    /// <summary>
    /// Xóa danh sách
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(this._idDanhSach) && Library.CheckIsInt32(this._idDanhSach))
            {
                SqlKscldsKiemTraVuViecProvider pro = new SqlKscldsKiemTraVuViecProvider(ListName.ConnectionString, false, string.Empty);

                // Xóa các bản ghi chi tiết danh sách trước
                string deleteCommand = "DELETE FROM " + ListName.Table_KSCLDSKiemTraVuViecChiTiet + " WHERE DSYeuCauKiemTraVuViecID = " + this._idDanhSach;
                if (_db.ExecuteNonQuery(deleteCommand) > 0)
                {
                    if (pro.Delete(Library.Int32Convert(this._idDanhSach)))
                    {
                        cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTTheoVuViec, "Xóa bản ghi có ID \"" + this._idDanhSach + "\" của danh mục " + tenchucnang);
                        Response.Redirect("/admin.aspx?page=kscl_chuanbi_danhsachcongtykttheoyeucaukiemtravuviec");
                    }
                }
            }
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }
}