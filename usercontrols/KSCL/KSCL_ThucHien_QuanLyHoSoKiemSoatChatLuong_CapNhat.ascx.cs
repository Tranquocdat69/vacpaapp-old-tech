﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_CapNhat : System.Web.UI.UserControl
{
    protected string tenchucnang = "Hồ sơ kiểm soát chất lượng";
    Db _db = new Db(ListName.ConnectionString);
    protected string _perOnFunc_DSHoSoKiemSoatChatLuong = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private string _id = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        _id = Library.CheckNull(Request.QueryString["Id"]);
        try
        {
            _db.OpenConnection();
            if (Request.Form["txtTuNgayThucTe"] != null)
                Save();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void LoadOneData()
    {
        try
        {
            _db.OpenConnection();
            string js = "$('#hdHoSoID').val('" + _id + "');" + Environment.NewLine;
            js += "GetListUpload();" + Environment.NewLine;
            if (!string.IsNullOrEmpty(_id))
            {
                string query = @"SELECT MaHoSo, TuNgayThucTe, DenNgayThucTe, HVTC.TenDoanhNghiep, DKT.MaDoanKiemTra, HS.HoiVienTapTheID, HS.DoanKiemTraID, GhiChu
                                    FROM " + ListName.Table_KSCLHoSo + @" HS
                                    LEFT JOIN tblHoiVienTapThe HVTC ON HS.HoiVienTapTheID = HVTC.HoiVienTapTheID
                                    LEFT JOIN tblKSCLDoanKiemTra DKT ON HS.DoanKiemTraID = DKT.DoanKiemTraID
                                    WHERE HoSoID = " + _id;
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    Hashtable ht = listData[0];
                    js += "$('#spanMaHoSo').html('" + ht["MaHoSo"] + "');" + Environment.NewLine;
                    DateTime tuNgay = Library.DateTimeConvert(ht["TuNgayThucTe"]);
                    DateTime denNgay = Library.DateTimeConvert(ht["DenNgayThucTe"]);
                    if (string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()) || denNgay.AddDays(1) > DateTime.Now)
                        js += "$('#spanTinhTrang').html('Đang thực hiện');" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()) && denNgay.AddDays(1) < DateTime.Now)
                        js += "$('#spanTinhTrang').html('Đã hoàn thành');" + Environment.NewLine;
                    js += "$('#spanTenCongTyKiemToan').html('" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + "');" + Environment.NewLine;
                    js += "$('#spanMaDoanKiemTra').html('" + ht["MaDoanKiemTra"] + "');" + Environment.NewLine;
                    js += "$('#hdDoanKiemTraID').val('" + ht["DoanKiemTraID"] + "');" + Environment.NewLine;
                    js += "GetDanhSachThanhVienTrongDoanKiemTra();" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(ht["TuNgayThucTe"].ToString()))
                        js += "$('#txtTuNgayThucTe').val('" + tuNgay.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()))
                        js += "$('#txtDenNgayThucTe').val('" + denNgay.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#txtGhiChu').val('" + ht["GhiChu"] + "');" + Environment.NewLine;

                    // Lấy thời gian kiểm tra theo kế hoạch
                    query = @"SELECT TuNgay, DenNgay FROM " + ListName.Table_KSCLDoanKiemTraCongTy + @" WHERE DoanKiemTraID = " + ht["DoanKiemTraID"] + @" AND HoiVienTapTheID = " + ht["HoiVienTapTheID"];
                    List<Hashtable> listData2 = _db.GetListData(query);
                    if (listData2.Count > 0)
                    {
                        string tuNgayKeHoach = !string.IsNullOrEmpty(listData2[0]["TuNgay"].ToString()) ? Library.DateTimeConvert(listData2[0]["TuNgay"]).ToString("dd/MM/yyyy") : "";
                        string denNgayKeHoach = !string.IsNullOrEmpty(listData2[0]["DenNgay"].ToString()) ? Library.DateTimeConvert(listData2[0]["DenNgay"]).ToString("dd/MM/yyyy") : "";
                        js += "$('#spanThoiGianKeHoach').html('" + tuNgayKeHoach + " - " + denNgayKeHoach + "');" + Environment.NewLine;
                    }
                }
                else
                {
                    js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Không tìm thấy thông tin hồ sơ. Đề nghị thử lại.\").show();" + Environment.NewLine;
                    js += "$('#" + Form1.ClientID + "').remove();" + Environment.NewLine;
                }
            }
            else
                js += "$('#txtNgayNop').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Save()
    {
        bool success = false;

        string idHoSo = Request.Form["hdHoSoID"];
        string ghiChu = Request.Form["txtGhiChu"];
        string tuNgayThucTe = Request.Form["txtTuNgayThucTe"];
        string denNgayThucTe = Request.Form["txtDenNgayThucTe"];
        
        SqlKsclHoSoProvider pro = new SqlKsclHoSoProvider(ListName.ConnectionString, false, string.Empty);
        KsclHoSo obj = pro.GetByHoSoId(Library.Int32Convert(idHoSo));
        if(obj != null && obj.HoSoId > 0)
        {
            obj.GhiChu = ghiChu;
            if (!string.IsNullOrEmpty(tuNgayThucTe))
                obj.TuNgayThucTe = Library.DateTimeConvert(tuNgayThucTe, "dd/MM/yyyy");
            if (!string.IsNullOrEmpty(denNgayThucTe))
                obj.DenNgayThucTe = Library.DateTimeConvert(denNgayThucTe, "dd/MM/yyyy");
            success = pro.Update(obj);
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        if (success)
        {
            js += "jQuery('#thongbaothanhcong_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Cập nhật nội dung hồ sơ thành công.\").show();" + Environment.NewLine;
        }
        else
        {
            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Có lỗi trong qua trình thêm dữ liệu.\").show();" + Environment.NewLine;
        }
        js += "</script>";
        Page.RegisterStartupScript("UpdateSuccess", js);
    }
}