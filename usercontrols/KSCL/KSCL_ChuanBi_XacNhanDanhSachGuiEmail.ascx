﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_XacNhanDanhSachGuiEmail.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_XacNhanDanhSachGuiEmail" %>

<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<form id="Form1" name="Form1" runat="server">
<%--<span style="font-weight: bold;">Số công ty chưa được gửi Email thông báo: 
<asp:Label ID="lblSoCongTyChuaGuiEmail" runat="server" Text=""></asp:Label></span><br />--%>
<fieldset class="fsBlockInfor">
    <input type='hidden' id="hdListBaoCaoChiTietID" />
    <legend>Các công ty chưa được gửi Email thông báo nộp báo cáo tự kiểm tra, Chọn các công ty muốn gửi Email thông báo.</legend>
    <table id="tblDanhSachCongTyChuaGuiEmail" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th rowspan="2" class="firstColumn">
                    <input type="checkbox" class="checkall" value="all" />
                </th>
                <th rowspan="2">
                    STT
                </th>
                <th rowspan="2">
                    Mã HVTC/CTKT
                </th>
                <th rowspan="2">
                    Tên công ty
                </th>
                <th rowspan="2">
                    Tên viết tắt
                </th>
                <th rowspan="2">
                    Địa chỉ
                </th>
                <th rowspan="2">
                    Email
                </th>
                <th rowspan="2">
                    Số điện thoại
                </th>
                <th colspan="2">
                    Người đại diện theo pháp luật
                </th>
            </tr>
            <tr>
                <th>
                    Họ và tên
                </th>
                <th>
                    Mobile
                </th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rpDanhSach" runat="server">
                <ItemTemplate>
                    <tr style="text-align: center;">
                        <td style="vertical-align: middle;">
                            <input type="checkbox" value='<%# Eval("DSBaoCaoTuKiemTraChiTietID") %>' />
                        </td>
                        <td>
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td>
                            <%# Eval("MaHoiVienTapThe")%>
                        </td>
                        <td>
                            <%# Eval("TenDoanhNghiep")%>
                        </td>
                        <td>
                            <%# Eval("TenVietTat")%>
                        </td>
                        <td>
                            <%# Eval("DiaChi")%>
                        </td>
                        <td>
                            <%# Eval("Email")%>
                        </td>
                        <td>
                            <%# Eval("DienThoai")%>
                        </td>
                        <td>
                            <%# Eval("NguoiDaiDienPL_Ten")%>
                        </td>
                        <td>
                            <%# Eval("NguoiDaiDienPL_DiDong")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
</fieldset>
<%--<div style="text-align: right; width: 100%; margin-top: 10px;">
    <a id="btn_them" href="javascript:;" class="btn btn-rounded" onclick="ChooseAndSendEmail();"><i
        class="iconfa-inbox"></i>Gửi Email</a> <a id="btn_xoa" href="javascript:;" class="btn btn-rounded"
            onclick="parent.CloseDanhSachGuiEmail();"><i class="iconfa-remove-sign"></i>
            Đóng</a>
</div>--%>
</form>
<script type="text/javascript">
    jQuery(document).ready(function () {
        // dynamic table      
        $('#tblDanhSachCongTyChuaGuiEmail').stickyTableHeaders();

        $("#tblDanhSachCongTyChuaGuiEmail .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#tblDanhSachCongTyChuaGuiEmail :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });

    function ChooseAndSendEmail() {
        var listID = '';
        var flag = false;
        $('#tblDanhSachCongTyChuaGuiEmail :checkbox').each(function () {
            if ($(this).prop("checked") == true && $(this).val() != 'all') {
                listID += $(this).val() + ',';
                flag = true;
            }
        });
        if (flag)
            parent.SendEmail(listID);
        else
            alert('Phải chọn ít nhất 1 công ty để gửi thông báo!');
    }
</script>