﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_ChuanBi_DanhSachBaoCaoTuKiemTra_NhapBaoCao : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý báo cáo tự kiểm tra";
    Db _db = new Db(ListName.ConnectionString);
    protected string _perOnFunc_DSCongTyKTTheoVuViec = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private string _id = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        _id = Library.CheckNull(Request.QueryString["Id"]);
        try
        {
            _db.OpenConnection();
            if (Request.Form["hdCongTyID"] != null)
                Save();
            if (!string.IsNullOrEmpty(_id))
                SpanCacNutChucNang.Visible = true;
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void LoadOneData()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            if (!string.IsNullOrEmpty(_id))
            {
                string query = @"SELECT b.MaHoiVienTapThe, c.DSBaoCaoTuKiemTraID, c.MaDanhSach, a.NgayNop, a.TenFile, a.SoBaoCaoKiemToanPH, a.DoanhThuNamGanNhat, a.LinhVucDuDKKIT FROM tblKSCLDSBaoCaoTuKiemTraChiTiet AS a
                                    LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID
                                    LEFT JOIN tblKSCLDSBaoCaoTuKiemTra AS c ON a.DSBaoCaoTuKiemTraID = c.DSBaoCaoTuKiemTraID
                                    WHERE a.DSBaoCaoTuKiemTraChiTietID = " + _id;
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    Hashtable ht = listData[0];
                    js += "$('#txtNgayNop').val('" + Library.DateTimeConvert(ht["NgayNop"]).ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
                    js += "$('#txtMaCongTy').val('" + ht["MaHoiVienTapThe"].ToString().Trim() + "');" + Environment.NewLine;
                    js += "$('#txtMaCongTy').prop('readonly', 'readonly');" + Environment.NewLine;
                    js += "$('#btnOpenFormCongTy').remove();" + Environment.NewLine;
                    js += "CallActionGetInforCongTy();" + Environment.NewLine;
                    js += "$('#ddlDanhSach').children().remove();" + Environment.NewLine;
                    js += "$('#hdDanhSachID').val('" + ht["DSBaoCaoTuKiemTraID"] + "');" + Environment.NewLine; // Dùng riêng cho update
                    js += "$('#ddlDanhSach').append('<option value=\"" + ht["DSBaoCaoTuKiemTraID"] + "\">" + ht["MaDanhSach"] + "</option>');" + Environment.NewLine;
                    js += "$('#ddlDanhSach').prop('disabled', 'disabled');" + Environment.NewLine;
                    js += "$('#txtDoanhThuNamGanNhat').val('" + Library.FormatMoney(ht["DoanhThuNamGanNhat"]) + "');" + Environment.NewLine;
                    string linhVucDuDKKit = ht["LinhVucDuDKKIT"].ToString();
                    if (linhVucDuDKKit == "1")
                        js += "$('#rdDuDKCK').prop('checked', true);" + Environment.NewLine;
                    if (linhVucDuDKKit == "2")
                        js += "$('#rdDuDKCongChungKhac').prop('checked', true);" + Environment.NewLine;
                    js += "$('#txtSoBaoCaoKiemToanPH').val('" + Library.FormatMoney(ht["SoBaoCaoKiemToanPH"]) + "');" + Environment.NewLine;
                    if (!string.IsNullOrEmpty(ht["TenFile"].ToString()))
                    {
                        js += "$('#spanTenFileCanCu').html('" + ht["TenFile"] + "');" + Environment.NewLine;
                        js += "$('#btnDownload').prop('href', '/admin.aspx?page=getfile&id=" + _id + "&type=" + ListName.Table_KSCLDSBaoCaoTuKiemTraChiTiet + "');" + Environment.NewLine;
                        js += "$('#btnDownload').html('Tải về');" + Environment.NewLine;
                    }
                }
                else
                {
                    js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Không tìm thấy thông tin báo cáo. Đề nghị thử lại.\").show();" + Environment.NewLine;
                    js += "$('#" + Form1.ClientID + "').remove();" + Environment.NewLine;
                }
            }
            else
                js += "$('#txtNgayNop').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');" + Environment.NewLine;
            Response.Write(js);
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Save()
    {
        bool success = false;

        string idCongTy = Request.Form["hdCongTyID"];
        string idDanhSach = Request.Form["ddlDanhSach"];
        if (string.IsNullOrEmpty(idDanhSach))
            idDanhSach = Request.Form["hdDanhSachID"];
        string ngayNop = Request.Form["txtNgayNop"];
        string doanhThuNamGanNhat = Request.Form["txtDoanhThuNamGanNhat"];
        string duDk = Request.Form["tieuchidkkt"];
        string soBaoCaoKiemToanPh = Request.Form["txtSoBaoCaoKiemToanPH"];
        HttpPostedFile fileCanCu = Request.Files["FileCanCu"];

        SqlHoiVienTapTheProvider proHoiVienTapThe = new SqlHoiVienTapTheProvider(ListName.ConnectionString, false, string.Empty);
        HoiVienTapThe objHVTT = proHoiVienTapThe.GetByHoiVienTapTheId(Library.Int32Convert(idCongTy));

        // Lấy ID bản ghi tblKSCLDSBaoCaoTuKiemTraChiTiet theo ID hội viên tổ chức và ID danh sách
        string query = "SELECT DSBaoCaoTuKiemTraChiTietID FROM " + ListName.Table_KSCLDSBaoCaoTuKiemTraChiTiet + " WHERE HoiVienTapTheID =" + idCongTy + " AND DSBaoCaoTuKiemTraID = " + idDanhSach;
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            string idChiTietDanhSach = listData[0]["DSBaoCaoTuKiemTraChiTietID"].ToString();
            // Nếu idChiTietDanhSach hợp lệ thì update thông tin báo cáo mới nộp vào bảng.
            if (!string.IsNullOrEmpty(idChiTietDanhSach) && Library.CheckIsInt32(idChiTietDanhSach))
            {
                SqlKscldsBaoCaoTuKiemTraChiTietProvider pro = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
                KscldsBaoCaoTuKiemTraChiTiet obj = pro.GetByDsBaoCaoTuKiemTraChiTietId(Library.Int32Convert(idChiTietDanhSach));
                obj.NgayNop = Library.DateTimeConvert(ngayNop, "dd/MM/yyyy");
                obj.MaBaoCao = GenMaBaoCao(objHVTT, obj.NgayNop.Value);
                obj.SoBaoCaoKiemToanPh = Library.DecimalConvert(soBaoCaoKiemToanPh);
                obj.DoanhThuNamGanNhat = Library.DecimalConvert(doanhThuNamGanNhat);
                if (duDk == "0")
                    obj.LinhVucDuDkkit = 1;
                if (duDk == "1")
                    obj.LinhVucDuDkkit = 2;
                if (duDk == "2")
                    obj.LinhVucDuDkkit = null;
                obj.HinhThucNop = "2";
                if (fileCanCu.ContentLength > 0)
                {
                    BinaryReader br = new BinaryReader(fileCanCu.InputStream);
                    byte[] fileByte = br.ReadBytes(fileCanCu.ContentLength);
                    obj.FileBaoCao = fileByte;
                    obj.TenFile = fileCanCu.FileName;
                }
                if (pro.Update(obj))
                {
                    // sau khi update thành công thông tin vào bảng 
                    // -> Update tiếp thông tin "doanh thu năm gần nhất" và "Điều kiện kiểm toán" vào thông tin công ty hoặc hội viên tổ chức.
                    if (objHVTT != null && objHVTT.HoiVienTapTheId > 0)
                    {
                        objHVTT.TongDoanhThu = Library.DecimalConvert(doanhThuNamGanNhat);
                        if (duDk == "0")
                        {
                            objHVTT.NamDuDkktCk = obj.NgayNop.Value.Year.ToString();
                            objHVTT.NamDuDkktKhac = null;
                        }
                        if (duDk == "1")
                        {
                            objHVTT.NamDuDkktKhac = obj.NgayNop.Value.Year.ToString();
                            objHVTT.NamDuDkktCk = null;
                        }
                        if (proHoiVienTapThe.Update(objHVTT))
                            success = true;
                    }
                }
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        if (success)
        {
            // Đóng form mà refresh số liệu
            js += "parent.CloseFormNhapBaoCao();" + Environment.NewLine;
            js += "parent.Search();" + Environment.NewLine;
            Session["MsgSuccess"] = "Lưu báo cáo thành công!";
        }
        else
        {
            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Có lỗi trong qua trình thêm dữ liệu.\").show();" + Environment.NewLine;
        }
        js += "</script>";
        Page.RegisterStartupScript("UpdateSuccess", js);
    }

    private string GenMaBaoCao(HoiVienTapThe objHVTT, DateTime ngayNop)
    {
        string maBaoCao = "";
        if (objHVTT != null && objHVTT.HoiVienTapTheId > 0)
        {
            maBaoCao += objHVTT.MaHoiVienTapThe.Trim();
            maBaoCao += ngayNop.ToString("yy");
            // Lấy số thứ tự mới nhất của mã báo cáo theo hội viên tổ chức và năm nộp báo cáo
            string query = "SELECT TOP 1 MaBaoCao FROM tblKSCLDSBaoCaoTuKiemTraChiTiet WHERE HoiVienTapTheID = " + objHVTT.HoiVienTapTheId + " AND YEAR(NgayNop) = " + ngayNop.Year + " ORDER BY MaBaoCao DESC";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                string tempMaBaoCao = listData[0]["MaBaoCao"].ToString();
                string index = tempMaBaoCao.Substring(tempMaBaoCao.Length - 2, 2);
                if (Library.CheckIsInt32(index))
                    maBaoCao += (Library.Int32Convert(index) + 1) > 9 ? (Library.Int32Convert(index) + 1).ToString() : ("0" + (Library.Int32Convert(index) + 1));
            }
            else
            {
                maBaoCao += "01";
            }
        }
        return maBaoCao;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        bool success = false;
        SqlKscldsBaoCaoTuKiemTraChiTietProvider pro = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
        KscldsBaoCaoTuKiemTraChiTiet obj = pro.GetByDsBaoCaoTuKiemTraChiTietId(Library.Int32Convert(_id));
        if (obj != null && obj.DsBaoCaoTuKiemTraChiTietId > 0)
        {
            obj.MaBaoCao = null;
            obj.NgayNop = null;
            obj.FileBaoCao = null;
            obj.SoBaoCaoKiemToanPh = null;
            obj.HinhThucNop = null;
            obj.TenFile = null;
            if (pro.Update(obj))
            {
                success = true;
                cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, "Gỡ bản báo cáo của bản ghi có ID \"" + _id + "\" của danh mục " + tenchucnang);
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        if (success)
        {
            // Đóng form mà refresh số liệu
            js += "parent.CloseFormNhapBaoCao();" + Environment.NewLine;
            js += "parent.Search();" + Environment.NewLine;
        }
        else
        {
            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Có lỗi trong qua trình xóa dữ liệu.\").show();" + Environment.NewLine;
        }
        js += "</script>";
        Page.RegisterStartupScript("DeleteSuccess", js);
    }
}