﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_MiniList.ascx.cs"
    Inherits="usercontrols_KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_MiniList" %>
<style type="text/css">
    .tdTable
    {
        width: 120px;
    }
</style>
<form id="FormDanhSachCongTy" runat="server" method="post" enctype="multipart/form-data">
<fieldset class="fsBlockInfor">
    <legend>Tiêu chí tìm kiếm</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <td>
                Thời gian kiểm tra:
            </td>
            <td>
                <asp:TextBox ID="txtThoiGianKiemTraTu" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>&nbsp;<img
                    src="/images/icons/calendar.png" id="imgThoiGianKiemTraTu" />
            </td>
            <td>
                -
            </td>
            <td>
                <asp:TextBox ID="txtThoiGianKiemTraDen" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>&nbsp;<img
                    src="/images/icons/calendar.png" id="imgThoiGianKiemTraDen" />
            </td>
        </tr>
        <tr>
            <td style="width: 150px;">
                Mã đoàn kiểm tra:
            </td>
            <td>
                <asp:TextBox ID="txtMaDoanKiemTra" runat="server" Width="120px"></asp:TextBox>
            </td>
            <td>
                ID CTKT/HVTT:
            </td>
            <td>
                <asp:TextBox ID="txtMaCongTy" runat="server" Width="120px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Tên công ty kiểm toán:
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtTenCongTy" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                ID HVCN/KTV:
            </td>
            <td>
                <asp:TextBox ID="txtMaHVCN" runat="server" Width="120px"></asp:TextBox>
            </td>
            <td>
                Tên kiểm toán viên:
            </td>
            <td>
                <asp:TextBox ID="txtTenKiemToanVien" runat="server" Width="120px"></asp:TextBox>
            </td>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearchPopup" runat="server" CssClass="btn" OnClick="lbtSearch_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách hồ sơ kiểm soát chất lượng</legend>
    <table width="100%" border="0" class="formtbl">
        <tr>
            <th style="width: 30px;">
            </th>
            <th style="width: 35px;">
                STT
            </th>
            <th style="width: 150px;">
                Mã hồ sơ
            </th>
            <th style="min-width: 150px;">
                Tên công ty kiểm toán
            </th>
            <th style="width: 140px;">
                Thời gian kiểm tra
            </th>
            <th style="width: 100px;">
                Tình trạng
            </th>
        </tr>
    </table>
    <div id='DivListHoSo' style="max-height: 200px;">
        <table id="tblHoSo" width="100%" border="0" class="formtbl">
            <asp:Repeater ID="rpHoSo" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="width: 30px; vertical-align: middle;">
                            <input type="radio" value='<%# Eval("ID") + ";#" + Eval("MaHoSo")  %>'
                                name="HocVien" />
                        </td>
                        <td style="width: 35px; text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="width: 150px;">
                            <%# Eval("MaHoSo")%>
                        </td>
                        <td style="min-width: 150px;">
                            <%# Eval("TenCongTy") + " <i>(" + Eval("TenVietTat") + ")</i>"%>
                        </td>
                        <td style="width: 140px;">
                            <%# Eval("TuNgay") + " - " + Eval("DenNgay")%>
                        </td>
                        <td style="width: 100px;">
                            <%# Eval("TrangThai") %>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</fieldset>
</form>
<script type="text/javascript">
    $("#<%= FormDanhSachCongTy.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtSearchPopup.ClientID %>').attr('href'));
        }
    });

    $('#<%= txtThoiGianKiemTraTu.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgThoiGianKiemTraTu").click(function () {
        $("#<%= txtThoiGianKiemTraTu.ClientID %>").datepicker("show");
    });
    $('#<%= txtThoiGianKiemTraDen.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgThoiGianKiemTraDen").click(function () {
        $("#<%= txtThoiGianKiemTraDen.ClientID %>").datepicker("show");
    });

    // Created by NGUYEN MANH HUNG - 2015/01/26
    // Hàm lấy thông tin hồ sơ được chọn từ danh sách
    function ChooseHoSo() {
        var flag = false;
        $('#tblHoSo :radio').each(function () {
            checked = $(this).prop("checked");
            if (checked) {
                var chooseItem = $(this).val();
                parent.GetMaHoSoFromPopup(chooseItem);
                parent.CloseFormDanhSachHoSo();
                flag = true;
                return;
            }
        });
        if (!flag)
            alert('Phải chọn hồ sơ kiểm soát chất lượng!');
    }

    // Created by NGUYEN MANH HUNG - 2014/11/28
    // Hàm kiểm tra trình duyệt để set thuộc tính overflow cho thẻ DIV hiển thị danh sách bản ghi
    // Chỉ trình duyệt nền Webkit mới có thuộc tính overflow: overlay
    function myFunction() {
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            $('#DivListHoSo').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Opera") != -1) {
            $('#DivListHoSo').css('overflow', 'overlay');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            $('#DivListHoSo').css('overflow', 'auto');
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {

        }
        else {

        }
    }

    myFunction();
</script>
