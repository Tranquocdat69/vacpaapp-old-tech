﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_DanhSachBaoCaoTuKiemTra : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách báo cáo tự kiểm tra";
    protected string _perOnFunc_DsBcTuKiemTra = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _perOnFunc_DsBcTuKiemTra = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ChuanBi_DSBaoCaoTuKiemTra, cm.connstr);

        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        if (Session["MsgSuccess"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>" + Session["MsgSuccess"] + "</div>"));
            Session.Remove("MsgSuccess");
        }

        if (_perOnFunc_DsBcTuKiemTra.Contains(ListName.PERMISSION_Xem))
        {

            if (!IsPostBack || Request.Form["hdAction"] == "paging")
            {
                LoadDanhSachVungMien();
                LoadDanhSachLoaiHinhCongTy();
                LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();
            }

        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/19
    /// Load List danh sách công ty phải nộp báo cáo tự kiểm tra theo điều kiện tìm kiếm
    /// </summary>
    private void LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra()
    {
        try
        {
            _db.OpenConnection();
            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "ID", "MaBaoCao", "HoiVienTapTheID", "MaHoiVienTapThe", "TenDoanhNghiep", "TenVietTat", "MaDanhSach", "NgayLap", "HinhThucNop", "NgayNop", "Url" });

            List<string> listParam = new List<string>{"@DuDKKT_CK", "@DuDKKT_Khac", "@MaHocVienTapThe", "@TenDoanhNghiep", "@LoaiHinhDoanhNghiepID",
                    "@VungMienID", "@DoanhThuTu", "@DoanhThuDen", "@NamHienTai", "@XepHang", "@NgayBatDau", "@NgayKetThuc", "@MaDanhSach",
                    "@NgayLapBatDau", "@NgayLapKetThuc", "@HinhThucNopBaoCao", "@MaBaoCao", "@NgayNopBatDau", "@NgayNopKetThuc"};
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DateTime.Now.Year, DBNull.Value,
                    DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value };
            // Dựa theo các tiêu chí tìm kiếm -> đưa giá trị tham số tương ứng vào store
            if (cboDuDieuKienKTCK.Checked)
                listValue[0] = 1;
            if (cboDuDieuKienKTKhac.Checked)
                listValue[1] = 1;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                listValue[2] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                listValue[3] = txtTenCongTy.Text;
            if (ddlLoaiHinh.SelectedValue != "")
                listValue[4] = ddlLoaiHinh.SelectedValue;
            if (ddlVungMien.SelectedValue != "")
                listValue[5] = ddlVungMien.SelectedValue;
            if (!string.IsNullOrEmpty(txtDoanhThuTu.Text) && Library.CheckIsDecimal(txtDoanhThuTu.Text))
                listValue[6] = txtDoanhThuTu.Text;
            if (!string.IsNullOrEmpty(txtDoanhThuDen.Text) && Library.CheckIsDecimal(txtDoanhThuDen.Text))
                listValue[7] = txtDoanhThuDen.Text;
            if (!cboTieuChi_Khac.Checked)
            {
                string xepHang = "";
                if (cboXepHang1.Checked)
                    xepHang += "1,";
                if (cboXepHang2.Checked)
                    xepHang += "2,";
                if (cboXepHang3.Checked)
                    xepHang += "3,";
                if (cboXepHang4.Checked)
                    xepHang += "4,";
                listValue[9] = xepHang;
            }
            if (!string.IsNullOrEmpty(txtTuNam.Text) && Library.CheckIsInt32(txtTuNam.Text))
                listValue[10] = txtTuNam.Text + "-1-1";
            if (!string.IsNullOrEmpty(txtDenNam.Text) && Library.CheckIsInt32(txtDenNam.Text))
                listValue[11] = txtDenNam.Text + "-12-31";
            if (!string.IsNullOrEmpty(txtMaDanhSach.Text))
                listValue[12] = txtMaDanhSach.Text;
            if (!string.IsNullOrEmpty(txtNgayLapBatDau.Text) && Library.CheckDateTime(txtNgayLapBatDau.Text, "dd/MM/yyyy"))
                listValue[13] = Library.ConvertDatetime(txtNgayLapBatDau.Text, '/');
            if (!string.IsNullOrEmpty(txtNgayLapKetThuc.Text) && Library.CheckDateTime(txtNgayLapKetThuc.Text, "dd/MM/yyyy"))
                listValue[14] = Library.ConvertDatetime(txtNgayLapKetThuc.Text, '/');
            if (ddlHinhThucNopBaoCao.SelectedValue != "")
                listValue[15] = ddlHinhThucNopBaoCao.SelectedValue;
            if (!string.IsNullOrEmpty(txtMaBaoCao.Text))
                listValue[16] = txtMaBaoCao.Text;
            if (!string.IsNullOrEmpty(txtNgayNopBaoCaoBatDau.Text) && Library.CheckDateTime(txtNgayNopBaoCaoBatDau.Text, "dd/MM/yyyy"))
                listValue[17] = Library.ConvertDatetime(txtNgayNopBaoCaoBatDau.Text, '/');
            if (!string.IsNullOrEmpty(txtNgayNopBaoCaoKetThuc.Text) && Library.CheckDateTime(txtNgayNopBaoCaoKetThuc.Text, "dd/MM/yyyy"))
                listValue[18] = Library.ConvertDatetime(txtNgayNopBaoCaoKetThuc.Text, '/');


            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SearchDanhSachBaoCaoTuKiemTra, listParam, listValue);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["DSBaoCaoTuKiemTraChiTietID"].ToString();
                    dr["MaBaoCao"] = ht["MaBaoCao"].ToString().Trim();
                    dr["HoiVienTapTheID"] = ht["HoiVienTapTheID"];
                    dr["MaHoiVienTapThe"] = ht["MaHoiVienTapThe"].ToString().Trim();
                    dr["TenDoanhNghiep"] = ht["TenDoanhNghiep"].ToString();
                    dr["TenVietTat"] = ht["TenVietTat"].ToString();
                    dr["MaDanhSach"] = ht["MaDanhSach"].ToString().Trim();
                    dr["NgayLap"] = Library.DateTimeConvert(ht["NgayLap"]);
                    dr["NgayNop"] = Library.DateTimeConvert(ht["NgayNop"]);
                    string hinhThucNop = ht["HinhThucNop"].ToString();
                    dr["HinhThucNop"] = hinhThucNop == "1" ? "Online" : "Offline";
                    dr["Url"] = "/admin.aspx?page=getfile&id=" + dr["ID"] + "&type=" + ListName.Table_KSCLDSBaoCaoTuKiemTraChiTiet;
                    dt.Rows.Add(dr);
                }
            }
            DataView dv = dt.DefaultView;
            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"] + " " + ViewState["sortdirection"];
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            Pager.Items.Clear();
            for (int i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
                hf_TrangHienTai.Value = Request.Form["tranghientai"];
            if (!string.IsNullOrEmpty(hf_TrangHienTai.Value))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(hf_TrangHienTai.Value);
                Pager.SelectedIndex = Convert.ToInt32(hf_TrangHienTai.Value);
            }
            gv_DanhSachBaoCaoTuKiemTra.DataSource = objPds;
            gv_DanhSachBaoCaoTuKiemTra.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void gv_DanhSachBaoCaoTuKiemTra_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_DanhSachBaoCaoTuKiemTra.PageIndex = e.NewPageIndex;
        gv_DanhSachBaoCaoTuKiemTra.DataBind();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_DsBcTuKiemTra.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        if (!_perOnFunc_DsBcTuKiemTra.Contains(ListName.PERMISSION_Them))
            Response.Write("$('#btnOpenFormInsert').remove();");
        if (!_perOnFunc_DsBcTuKiemTra.Contains(ListName.PERMISSION_Sua))
            Response.Write("$('#btnOpenFormUpdate').remove();");
        if (!_perOnFunc_DsBcTuKiemTra.Contains(ListName.PERMISSION_Xoa))
        {
            Response.Write("$('#" + btnDelete.ClientID + "').remove();");
        }

        if (!_perOnFunc_DsBcTuKiemTra.Contains(ListName.PERMISSION_KetXuat))
            Response.Write("$('#btnExport').remove();");
    }

    /// <summary>
    /// Xóa danh sách công ty phải nộp báo cáo tự kiểm tra
    /// </summary>
    /// <param name="listIdDanhSach">List ID danh sách</param>
    private void DeleteDanhSach(List<string> listIdDanhSach)
    {
        try
        {
            _db.OpenConnection();
            SqlKscldsBaoCaoTuKiemTraChiTietProvider pro = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
            foreach (string idDanhSach in listIdDanhSach)
            {
                if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach))
                {
                    KscldsBaoCaoTuKiemTraChiTiet obj = pro.GetByDsBaoCaoTuKiemTraChiTietId(Library.Int32Convert(idDanhSach));
                    if (obj != null && obj.DsBaoCaoTuKiemTraChiTietId > 0)
                    {
                        obj.MaBaoCao = null;
                        obj.NgayNop = null;
                        obj.FileBaoCao = null;
                        obj.SoBaoCaoKiemToanPh = null;
                        obj.HinhThucNop = null;
                        obj.TenFile = null;
                        if (pro.Update(obj))
                            cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, "Gỡ bản báo cáo của bản ghi có ID \"" + idDanhSach + "\" của danh mục " + tenchucnang);
                    }
                }
            }
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã xóa những bản ghi dữ liệu hợp lệ.";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("DeleteSuccess", js);
            LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Xóa những bản ghi được chọn khi bấm nút "Xóa đồng loạt" ở trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachBaoCaoTuKiemTra.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachBaoCaoTuKiemTra.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        DeleteDanhSach(listIdDanhSach);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách vùng miền đổ vào Dropdownlist
    /// </summary>
    private void LoadDanhSachVungMien()
    {
        ddlVungMien.Items.Clear();
        ddlVungMien.Items.Add(new ListItem("Tất cả", ""));
        SqlDmVungMienProvider vungMienPro = new SqlDmVungMienProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmVungMien> listData = vungMienPro.GetAll();
        foreach (DmVungMien dmVungMien in listData)
        {
            ddlVungMien.Items.Add(new ListItem(dmVungMien.TenVungMien, dmVungMien.VungMienId.ToString()));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách loại hình công ty
    /// </summary>
    private void LoadDanhSachLoaiHinhCongTy()
    {
        ddlLoaiHinh.Items.Clear();
        ddlLoaiHinh.Items.Add(new ListItem("Tất cả", ""));
        SqlDmLoaiHinhDoanhNghiepProvider loaiHinhPro = new SqlDmLoaiHinhDoanhNghiepProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmLoaiHinhDoanhNghiep> listData = loaiHinhPro.GetAll();
        foreach (DmLoaiHinhDoanhNghiep loaiHinh in listData)
        {
            ddlLoaiHinh.Items.Add(new ListItem(loaiHinh.TenLoaiHinhDoanhNghiep, loaiHinh.LoaiHinhDoanhNghiepId.ToString()));
        }
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();
        txtMaDanhSach.Text = "";
        txtNgayLapBatDau.Text = "";
        txtNgayLapKetThuc.Text = "";
        ddlHinhThucNopBaoCao.SelectedIndex = 0;
        txtMaBaoCao.Text = "";
        txtNgayNopBaoCaoBatDau.Text = "";
        txtNgayNopBaoCaoKetThuc.Text = "";
        cboDuDieuKienKTCK.Checked = false;
        cboDuDieuKienKTKhac.Checked = false;
        cboXepHang1.Checked = false;
        cboXepHang2.Checked = false;
        cboXepHang3.Checked = false;
        cboXepHang4.Checked = false;
        cboTieuChi_Khac.Checked = false;
        txtMaCongTy.Text = "";
        if(ddlLoaiHinh.Items.Count > 0)
            ddlLoaiHinh.SelectedIndex = 0;
        txtDoanhThuTu.Text = "";
        txtDoanhThuDen.Text = "";
        txtTenCongTy.Text = "";
        if (ddlVungMien.Items.Count > 0)
            ddlVungMien.SelectedIndex = 0;
        txtTuNam.Text = "";
        txtDenNam.Text = "";
    }
    protected void gv_DanhSachBaoCaoTuKiemTra_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();
    }
}