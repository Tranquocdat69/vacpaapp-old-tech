﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_KetThuc_XuLySaiPham_NoiDung : System.Web.UI.UserControl
{
    private string _idDanhSachKiemTraTrucTiep = "";
    protected string _listPermissionOnFunc_XuLySaiPham = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        this._idDanhSachKiemTraTrucTiep = Library.CheckNull(Request.QueryString["iddanhsach"]);
        try
        {
            _db.OpenConnection();
            _listPermissionOnFunc_XuLySaiPham = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_KetThuc_XuLySaiPham, cm.connstr);
            if (_listPermissionOnFunc_XuLySaiPham.Contains("XEM|"))
            {
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "update")
                    Save();
            }
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu xử lý sai phạm theo quyết định của BTC và VACPA !</div>"));
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Save()
    {
        string js = "";
        SqlKsclXuLySaiPhamChiTietProvider pro = new SqlKsclXuLySaiPhamChiTietProvider(ListName.ConnectionString, false, string.Empty);
        string idXuLySaiPham = Library.CheckNull(Request.QueryString["idxlsp"]);
        if (!string.IsNullOrEmpty(Request.Form["ddlCongTy"]))
        {
            string idCongTy = Request.Form["ddlCongTy"];
            string idXuLySaiPhamCongTy = Request.Form["hdXuLySaiPhamCongTyID"];
            KsclXuLySaiPhamChiTiet obj;
            if (string.IsNullOrEmpty(idXuLySaiPhamCongTy)) // Insert
            {
                obj = new KsclXuLySaiPhamChiTiet();
                obj.HoiVienTapTheId = Library.Int32Convert(idCongTy);
                obj.XuLySaiPhamId = Library.Int32Convert(idXuLySaiPham);
            }
            else
                obj = pro.GetByXuLySaiPhamChiTietId(Library.Int32Convert(idXuLySaiPhamCongTy));
            obj.NoiDungSaiPham = Request.Form["txtNoiDungSaiPham"];
            obj.QuyetDinhBtc = Request.Form["txtQuyetDinhXuLyCuaBTC"];
            obj.QuyetDinhVacpa = Request.Form["txtQuyetDinhXuLyCuaVACPA"];
            if(string.IsNullOrEmpty(idXuLySaiPhamCongTy))
            {
                pro.Insert(obj);
            }
            else
            {
                pro.Update(obj);
            }
            js = "<script type='text/javascript'>" + Environment.NewLine;
            js += "CallAlertSuccessFloatRightBottom('Lưu thông tin xử lý sai phạm của công ty thành công.');" + Environment.NewLine;
            js += "$('#hdCongTyID').val('" + Request.Form["hdCongTyID"] + "');" + Environment.NewLine;
            js += "iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=loadxulysaiphamcongty&idcongty=" + idCongTy + "&idxulysaipham=" + idXuLySaiPham + "';" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("SaveSucess", js);
        }
    }

    protected void CheckPermissionOnPage()
    {
        Response.Write("if($('#hdCongTyID').val().length == 0) $('#hdCongTyID').val('" + Library.CheckNull(Request.QueryString["idcongty"]) + "');");
        Response.Write("if($('#hdCongTyID').val().length > 0) $('#ddlCongTy').val($('#hdCongTyID').val());");
        Response.Write("CallActionGetInforXuLySaiPham();");

        if (!_listPermissionOnFunc_XuLySaiPham.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");

        if (!_listPermissionOnFunc_XuLySaiPham.Contains("KETXUAT|"))
            Response.Write("$('#btnExport').remove();");

        if (!_listPermissionOnFunc_XuLySaiPham.Contains("THEM|"))
            Response.Write("$('#btnAdd').remove();");

        if (!_listPermissionOnFunc_XuLySaiPham.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");
    }

    protected void GetDanhSachCongTy()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            string query = @"SELECT HVTC.HoiVienTapTheID, HVTC.MaHoiVienTapThe, HVTC.TenDoanhNghiep FROM tblKSCLDSKiemTraTrucTiepChiTiet DSKTTT
                            LEFT JOIN tblHoiVienTapThe HVTC ON DSKTTT.HoiVienTapTheID = HVTC.HoiVienTapTheID
                             WHERE DSKTTT.DSKiemTraTrucTiepID = " + this._idDanhSachKiemTraTrucTiep;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                js += "<option value=''>-- Chọn một công ty để cập nhật thông tin --</option>" + Environment.NewLine;
                foreach (Hashtable ht in listData)
                {
                    js += "<option value='" + ht["HoiVienTapTheID"] + "'>" + ht["MaHoiVienTapThe"] + "</option>" + Environment.NewLine;
                }
            }
            else
                js += "<option value=''>-- Không có công ty trong danh sách --</option>" + Environment.NewLine;
            Response.Write(js);
        }
        catch { _db.CloseConnection(); }
        finally
        {
            _db.CloseConnection();
        }
    }
}