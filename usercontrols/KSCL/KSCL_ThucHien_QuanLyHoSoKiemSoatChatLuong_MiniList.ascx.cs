﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_MiniList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadListCongTy();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Load danh sách "Công ty" theo điều kiện Search
    /// </summary>
    private void LoadListCongTy()
    {
        Db _db = new Db(ListName.ConnectionString);
        try
        {
            _db.OpenConnection();
            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "ID", "MaHoSo", "TenCongTy", "TenVietTat", "TuNgay", "DenNgay", "TrangThai" });

            List<string> listParam = new List<string> { "@NgayBatDauKiemTraTu", "@NgayBatDauKiemTraDen", "@MaDoanKiemTra", "@MaCongTy", "@TenCongTy", "@MaHoiVienCaNhan", 
                "@TenHoiVienCaNhan", "@MaThanhVien", "@TenThanhVien", "@SoChungChiKTV", "@NgayCapChungChiKTV" };
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value,
                DBNull.Value, DBNull.Value, DBNull.Value };

            if (!string.IsNullOrEmpty(txtThoiGianKiemTraTu.Text))
                listValue[0] = Library.DateTimeConvert(txtThoiGianKiemTraTu.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtThoiGianKiemTraDen.Text))
                listValue[1] = Library.DateTimeConvert(txtThoiGianKiemTraDen.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtMaDoanKiemTra.Text))
                listValue[2] = txtMaDoanKiemTra.Text;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                listValue[3] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                listValue[4] = txtTenCongTy.Text;
            if (!string.IsNullOrEmpty(txtMaHVCN.Text))
                listValue[5] = txtMaHVCN.Text;
            if (!string.IsNullOrEmpty(txtTenKiemToanVien.Text))
                listValue[6] = txtTenKiemToanVien.Text;

            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SearchDanhSachHoSoKiemSoatChatLuong, listParam, listValue);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["HoSoID"].ToString();
                    dr["MaHoSo"] = ht["MaHoSo"].ToString();
                    dr["TenCongTy"] = ht["TenDoanhNghiep"].ToString();
                    dr["TenVietTat"] = ht["TenVietTat"].ToString();
                    DateTime tuNgay = Library.DateTimeConvert(ht["TuNgayThucTe"]);
                    DateTime denNgay = Library.DateTimeConvert(ht["DenNgayThucTe"]);
                    dr["TuNgay"] = !string.IsNullOrEmpty(ht["TuNgayThucTe"].ToString()) ? tuNgay.ToString("dd/MM/yyyy") : "";
                    dr["DenNgay"] = !string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()) ? denNgay.ToString("dd/MM/yyyy") : "";
                    if (string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()) || denNgay.AddDays(1) > DateTime.Now)
                        dr["TrangThai"] = "Đang thực hiện";
                    if (!string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()) && denNgay.AddDays(1) < DateTime.Now)
                        dr["TrangThai"] = "Đã hoàn thành";
                    dt.Rows.Add(dr);
                }
            }
            rpHoSo.DataSource = dt.DefaultView;
            rpHoSo.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/28
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadListCongTy();
    }
}