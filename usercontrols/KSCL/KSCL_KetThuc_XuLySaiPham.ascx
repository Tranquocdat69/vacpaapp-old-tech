﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_KetThuc_XuLySaiPham.ascx.cs"
    Inherits="usercontrols_KSCL_KetThuc_XuLySaiPham" %>
<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .tdInput
    {
        width: 120px;
    }
</style>
<form id="Form1" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Xử lý sai phạm theo quyết định của BTC và VACPA</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <input type="hidden" name="hdXuLySaiPhamID" id="hdXuLySaiPhamID" />
    <input type="hidden" name="hdAction" id="hdAction" />
    <table id="tblThongTinChung" width="800px" border="0" class="formtbl">
        <tr>
            <td style="width: 200px;">
                Mã danh sách kiểm tra trực tiếp<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtMaDanhSach" id="txtMaDanhSach" onchange="CallActionGetInforDanhSach();" />
                <input type="hidden" name="hdDanhSachID" id="hdDanhSachID" />
            </td>
            <td style="width: 25px;">
                <input type="button" value="---" onclick="OpenDanhSachKiemTraTrucTiep();" style="border: 1px solid gray;" />
            </td>
            <td>
                Trạng thái:
            </td>
            <td>
                <span id="spanTrangThai"></span>
            </td>
        </tr>
        <tr>
            <td>
                Số quyết định
            </td>
            <td colspan="2">
                <input type="text" name="txtSoQuyetDinh" id="txtSoQuyetDinh" />
            </td>
            <td>
                Ngày ra quyết định:
            </td>
            <td>
                <input type="text" name="txtNgayRaQuyetDinh" id="txtNgayRaQuyetDinh" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset id="fsDanhSachXuLySaiPham" style="display: none;" class="fsBlockInfor">
    <legend>Danh sách xử lý sai phạm</legend>
    <div style="margin: 5px 0;" id="DivControlActionDanhSachSaiPham">
        <a id="A1" href="javascript:;" class="btn" onclick="OpenFormNoiDung();"><i class="iconfa-plus">
        </i>Thêm</a> <a id="A2" href="javascript:;" class="btn" onclick="OpenFormEditNoiDung();"><i class="iconfa-edit">
        </i>Sửa</a> <a id="A3" href="javascript:;" class="btn" onclick="DeleteFormNoiDung();"><i class="iconfa-trash">
        </i>Xóa</a>
    </div>
    <table id="tblDanhSachXuLySaiPham" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th style="width: 30px;">
                    <input type="checkbox" value="all" class="checkall" />
                </th>
                <th style="width: 35px;">
                    STT
                </th>
                <th style="min-width: 200px;">
                    Tên công ty kiểm toán/Kiểm toán viên
                </th>
                <th style="width: 120px;">
                    Số giấy chứng nhận ĐKHN
                </th>
                <th style="width: 200px;">
                    Nội dung sai phạm
                </th>
                <th style="width: 150px;">
                    Quyết định xử lý của BTC
                </th>
                <th style="width: 150px;">
                    Quyết định xử lý của VACPA
                </th>
            </tr>
        </thead>
        <tbody id="TBodyDanhSachXuLySaiPham">
        </tbody>
    </table>
</fieldset>
<div id="DivControlAction" style="width: 100%; margin: 10px 0px 5px 0px; text-align: center;
    display: none;">
    <a id="btnSave" href="javascript:;" class="btn" onclick="SubmitForm();"><i class="iconfa-save">
    </i>Lưu</a>&nbsp; <span id="spanButtonActionEdit" style="display: none;"><a id="btnDelete"
        href="javascript:;" class="btn" onclick="Delete();"><i class="iconfa-trash"></i>
        Xóa</a>&nbsp;
        <a id="btnApprove" href="javascript:;" class="btn" onclick="Approve();"><i class="iconfa-thumbs-up"></i>Duyệt</a>&nbsp;
        <a id="btnReject" href="javascript:;" class="btn" onclick="Reject();"><i class="iconfa-remove-sign"></i>Từ chối</a>&nbsp;
        <a id="btnBackApprove" href="javascript:;" class="btn" onclick="BackApprove();"><i class="iconfa-thumbs-down"></i>Thoái duyệt</a>&nbsp;
        <a id="btnExport" href="javascript:;" class="btn btn-rounded" onclick="CallReportPage();"><i class="iconfa-download"></i>Kết xuất</a> </span>
</div>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadKetQuaKiemTra" width="0px" height="0px"></iframe>
</form>
<script type="text/javascript">
    $("#txtNgayRaQuyetDinh").datepicker({ dateFormat: 'dd/mm/yy' });
    // Validate form
    $('#Form1').validate({
        rules: {
            hdDanhSachID: {
                required: true
            },
            txtNgayRaQuyetDinh: {
                dateITA: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }  
    });
    
    function OpenFormNoiDung(idCongTy) {
        $("#DivNoiDung").empty();
        if(idCongTy == undefined)
            $("#DivNoiDung").append($("<iframe width='100%' height='100%' id='ifNoiDung' name='ifNoiDung' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_KetThuc_XuLySaiPham_NoiDung&iddanhsach=" + $('#hdDanhSachID').val() + "&idxlsp=" + $('#hdXuLySaiPhamID').val()));
        else {
            $("#DivNoiDung").append($("<iframe width='100%' height='100%' id='ifNoiDung' name='ifNoiDung' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_KetThuc_XuLySaiPham_NoiDung&iddanhsach=" + $('#hdDanhSachID').val() + "&idxlsp=" + $('#hdXuLySaiPhamID').val() + "&idcongty=" +idCongTy));
        }
        $("#DivNoiDung").dialog({
            resizable: true,
            width: 900,
            height: 750,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Nội dung sai phạm và biện pháp xử lý</b>",
            modal: true,
            zIndex: 1000,
            close: function( event, ui ) {
                CallActionGetInforXuLySaiPham();
            }
        });
    }
    
    function CloseFormNoiDung() {
        $("#DivNoiDung").dialog('close');
    }

    function OpenFormEditNoiDung() {
        var id = '';
        var hoiVienTapTheId = '';
        var hoiVienCaNhanId = '';
        var count = 0;
        $('#tblDanhSachXuLySaiPham :checkbox').each(function () {
            var checked = $(this).prop("checked");
            if (checked && $(this).val() != 'all') {
                var arr = $(this).val().split(';#');
                id = arr[0];
                hoiVienTapTheId = arr[1];
                hoiVienCaNhanId = arr[2];
                count++;
            }
        });
        if (count == 0)
            alert('Phải chọn ít nhất một bản ghi để thực hiện thao tác!');
        if(count > 1)
            alert('Chỉ được chọn một bản ghi để thực hiện cập nhật!');
        if(count == 1) {
            if(hoiVienCaNhanId.length > 0)
                OpenFormUpdateChoHVCN(id);
            else {
                OpenFormNoiDung(hoiVienTapTheId);
            }
        }
    }

    function DeleteFormNoiDung() {
        if(confirm('Bạn chắc chắn muốn xóa các bản ghi này chứ?')) {
            var listId = '';
            var flag = false;
            $('#tblDanhSachXuLySaiPham :checkbox').each(function () {
                var checked = $(this).prop("checked");
                if (checked && $(this).val() != 'all') {
                    var arr = $(this).val().split(';#');
                    listId += arr[0] + ',';
                    flag = true;
                }
            });
            if (!flag)
                alert('Phải chọn ít nhất một bản ghi để thực hiện thao tác!');
            else
                iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=deletexulysaiphamcongty&idxulysaiphamchitiet=' + listId;
        } 
    }

    function OpenDanhSachKiemTraTrucTiep() {
        $("#DivDanhSachKiemTraTrucTiep").empty();
        $("#DivDanhSachKiemTraTrucTiep").append($("<iframe width='100%' height='100%' id='ifDanhSach' name='ifDanhSach' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_KetThuc_XuLySaiPham_ChonDanhSachKiemTraTrucTiep"));
        $("#DivDanhSachKiemTraTrucTiep").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách kiểm tra trực tiếp</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSach.ChooseHocVien();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachKiemTraTrucTiep').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivDanhSachKiemTraTrucTiep').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }
    
    function GetMaDanhSachFromPopup(value) {
        $('#txtMaDanhSach').val(value);
        CallActionGetInforDanhSach();
    }
    
    function CloseFormDanhSachKiemTraTrucTiep() {
        $("#DivDanhSachKiemTraTrucTiep").dialog('close');
    }
    
    function CallActionGetInforDanhSach() {
        var maDanhSach = $('#txtMaDanhSach').val();
        iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=loadttdanhsach&madanhsach=' + maDanhSach;
    }
    
    function DisplayInforDanhSach(value) {
        if(value.length == 0) {
            $('#txtMaDanhSach').val('');
            $('#txtSoQuyetDinh').val('');
            $('#txtNgayRaQuyetDinh').val('');
            $('#spanTrangThai').html('');
            $('#DivControlAction').css('display', 'none');
            $('#fsDanhSachXuLySaiPham').css('display', 'none');
            $('#spanButtonActionEdit').css('display', 'none');
            alert('Mã danh sách không đúng hoặc không tồn tại!');
        } else {
            $('#DivControlAction').css('display', '');
        }
        $('#hdDanhSachID').val(value);
  
        if(value.length > 0)
            CallActionGetInforXuLySaiPham();
    }
    
    function CallActionGetInforXuLySaiPham() {
        var idDanhSach = $('#hdDanhSachID').val();
        iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=loadxulysaipham&iddanhsach=' + idDanhSach;
    }

    function DisplayInforXuLySaiPham(value, arrData) {
        var arrValue = value.split(';#');
        var tbodyMain = document.getElementById('TBodyDanhSachXuLySaiPham');
        var trs = tbodyMain.getElementsByTagName("tr");
        for (var i = 0; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrValue[0].length == 0) {
            $('#txtSoQuyetDinh').val('');
            $('#txtNgayRaQuyetDinh').val('');
            $('#spanTrangThai').html('');
            $('#fsDanhSachXuLySaiPham').css('display', 'none');
            $('#spanButtonActionEdit').css('display', 'none');
            
        }
        else {
            $('#fsDanhSachXuLySaiPham').css('display', '');
            $('#spanButtonActionEdit').css('display', '');
            if(arrData.length > 0) {
                var parentIndex = 0;
                var childIndex = 0;
                for(var i = 0; i < arrData.length; i ++) {
                    var xuLySaiPhamChiTietId = arrData[i][0];
                    var hoiVienCaNhanId = arrData[i][1];
                    var tenHoiVienCaNhan = arrData[i][2];
                    var hoiVienTapTheId = arrData[i][3];
                    var tenDoanhNghiep = arrData[i][4];
                    var noiDungSaiPham = arrData[i][5];
                    var quyetDinhBtc = arrData[i][6];
                    var quyetDinhVacpa = arrData[i][7];
                    var soGiayChungNhanHN = arrData[i][8];
                    var index = 0;

                    var ten = '';
                    if(hoiVienTapTheId.length > 0 && hoiVienCaNhanId.length == 0) {
                        ten = "<b>" + tenDoanhNghiep + "</b>";
                        parentIndex++;
                        index = "<b>" + parentIndex + "</b>";
                    }
                    else {
                        ten = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + tenHoiVienCaNhan;
                        childIndex++;
                        index = parentIndex + "." + childIndex;
                    }
                    
                    var tr = document.createElement('TR');
                    var tdCheckBox = document.createElement("TD");
                    tdCheckBox.style.textAlign = 'center';
                    tdCheckBox.className = 'firstColumn';
                    var checkbox = document.createElement('input');
                    checkbox.type = "checkbox";
                    checkbox.value = xuLySaiPhamChiTietId + ";#" + hoiVienTapTheId + ";#" + hoiVienCaNhanId;
                    tdCheckBox.appendChild(checkbox);
                    tr.appendChild(tdCheckBox);
                
                    var arrColumn = [index, ten, soGiayChungNhanHN, noiDungSaiPham, quyetDinhBtc, quyetDinhVacpa];
                    for(var j = 0; j < arrColumn.length; j ++){
                        var td = document.createElement("TD");
                        if(j == 0)
                            td.style.textAlign = 'center';
                        td.innerHTML = arrColumn[j];
                        tr.appendChild(td); 
                    }
                
                    tbodyMain.appendChild(tr);
                }
            }
        }
        $('#hdXuLySaiPhamID').val(arrValue[0]);
        $('#txtSoQuyetDinh').val(arrValue[1]);
        $('#txtNgayRaQuyetDinh').val(arrValue[2]);
        var maTrangThai = arrValue[3];
        if(maTrangThai == "5") {
            $('#btnApprove').css('display', 'none');
            $('#btnReject').css('display', 'none');
            $('#btnBackApprove').css('display', '');
            $('#btnSave').css('display', 'none');
            $('#btnDelete').css('display', 'none');
            $('#DivControlActionDanhSachSaiPham').css('display', 'none');
            $('#spanTrangThai').html('Đã phê duyệt');
            $('#spanTrangThai').css('color', 'green');
        }
        if(maTrangThai == "3") {
            $('#btnApprove').css('display', '');
            $('#btnReject').css('display', '');
            $('#btnBackApprove').css('display', 'none');
            $('#spanTrangThai').html('Chờ phê duyệt');
            $('#spanTrangThai').css('color', 'black');
        }
        if(maTrangThai == "6" || maTrangThai == "4") {
            $('#btnApprove').css('display', '');
            $('#btnReject').css('display', 'none');
            $('#btnBackApprove').css('display', 'none');
            if(maTrangThai == "2"){
                $('#spanTrangThai').html('Đã từ chối');
                $('#spanTrangThai').css('color', 'red');
            }
            if(maTrangThai == "6"){
                $('#spanTrangThai').html('Thoái duyệt');
                $('#spanTrangThai').css('color', 'black');
            }
        }
    }
    
    function OpenFormUpdateChoHVCN(id) {
        $("#DivUpdateChoHVCN").empty();
        $("#DivUpdateChoHVCN").append($("<iframe width='100%' height='100%' id='ifUpdateChoHVCN' name='ifUpdateChoHVCN' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_KetThuc_XuLySaiPham_UpdateHoiVienCaNhan&id=" + id));
        $("#DivUpdateChoHVCN").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Cập nhật cho kiểm toán viên</b>",
            modal: true,
            zIndex: 1000,
            close: function( event, ui ) {
                CallActionGetInforXuLySaiPham();
            }
        });
    }
    
    function CloseFormUpdateChoHVCN() {
        $("#DivUpdateChoHVCN").dialog('close');
    }
    
    function SubmitForm() {
        $('#hdAction').val('update');
        $('#Form1').submit();
    }
    
    function Delete() {
        if(confirm('Bạn chắc chắn muốn xóa thông tin xử lý sai phạm này chứ?')){
            $('#hdAction').val('delete');
            $('#Form1').submit();
        }
    }
    function Approve() {
        if(confirm('Bạn chắc chắn muốn duyệt thông tin xử lý sai phạm này chứ?')){
            $('#hdAction').val('approve');
            $('#Form1').submit();
        }
    }
    function Reject() {
        if(confirm('Bạn chắc chắn muốn từ chối thông tin xử lý sai phạm này chứ?')){
            $('#hdAction').val('reject');
            $('#Form1').submit();
        }
    }
    function BackApprove() {
        if(confirm('Bạn chắc chắn muốn thoái duyệt thông tin xử lý sai phạm này chứ?')){
            $('#hdAction').val('backapprove');
            $('#Form1').submit();
        }
    }
    
    jQuery(document).ready(function () {
        $('#tblDanhSachXuLySaiPham').stickyTableHeaders();
        
        $("#tblDanhSachXuLySaiPham .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#tblDanhSachXuLySaiPham :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });
    
    function CallReportPage() {
        var url = "/admin.aspx?page=KSCL_BaoCao_QuyetDinhXuLyKyLuatHoiVien&madskttt=" + $('#txtMaDanhSach').val();
        window.open(url, '_blank');
    }
    
    <% CheckPermissionOnPage(); %>
</script>
<div id="DivDanhSachKiemTraTrucTiep">
</div>
<div id="DivNoiDung">
</div>
<div id="DivUpdateChoHVCN">
</div>
