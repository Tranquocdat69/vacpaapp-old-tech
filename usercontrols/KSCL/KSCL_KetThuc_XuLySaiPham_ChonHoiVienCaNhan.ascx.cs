﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class usercontrols_KSCL_KetThuc_XuLySaiPham_ChonHoiVienCaNhan : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _congTyId = "", _idXuLySaiPham = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this._congTyId = Request.QueryString["idct"];
            this._idXuLySaiPham = Request.QueryString["idxulysaipham"];
            _db.OpenConnection();
            if (!IsPostBack)
            {
                LoadListHoiVienTapThe();
                LoadListHocVien();
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Load danh sach "Hoi vien" theo dieu kien search
    /// </summary>
    private void LoadListHocVien()
    {
        try
        {
            _db.OpenConnection();
            string query = @"SELECT HoiVienCaNhanID, MaHoiVienCaNhan, LoaiHoiVienCaNhan, HoDem + ' ' + Ten as FullName, SoChungChiKTV, NgayCapChungChiKTV, 
                                GioiTinh, tblHoiVienCaNhan.Mobile AS Mobile, tblHoiVienCaNhan.Email AS Email, tblHoiVienTapThe.TenDoanhNghiep AS DonViCongTac,
                                tblHoiVienCaNhan.TroLyKTV AS TroLyKTV
	                            FROM tblHoiVienCaNhan LEFT JOIN tblHoiVienTapThe ON tblHoiVienCaNhan.HoiVienTapTheID = tblHoiVienTapThe.HoiVienTapTheID
	                            WHERE {MaHoiVienCaNhan} AND (HoDem + ' ' + Ten LIKE N'%" + txtHoTen.Text + @"%')
	                            AND {LoaiHocVien} AND {DonViCongTac} ORDER BY FullName";

            query = !string.IsNullOrEmpty(txtMaHocVien.Text)
                        ? query.Replace("{MaHoiVienCaNhan}", "MaHoiVienCaNhan like '%" + txtMaHocVien.Text + "%'")
                        : query.Replace("{MaHoiVienCaNhan}", "1=1");
            query = ddlPhanLoai.SelectedValue != ""
                        ? query.Replace("{LoaiHocVien}", "LoaiHoiVienCaNhan = '" + ddlPhanLoai.SelectedValue + "'")
                        : query.Replace("{LoaiHocVien}", "1=1");
            query = ddlDonViCongTac.SelectedValue != ""
                        ? query.Replace("{DonViCongTac}",
                                        "tblHoiVienCaNhan.HoiVienTapTheID = '" + ddlDonViCongTac.SelectedValue + "'")
                        : query.Replace("{DonViCongTac}", "1=1");
            DataTable dt = _db.GetDataTable(query);

            query = @"SELECT HoiVienCaNhanID FROM tblKSCLXuLySaiPhamChiTiet                        
                        WHERE HoiVienCaNhanID IS NOT NULL AND HoiVienTapTheID = " + this._congTyId + @" AND XuLySaiPhamID = " + this._idXuLySaiPham;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                foreach (Hashtable ht in listData)
                {
                    DataRow[] arrDataRow = dt.Select("HoiVienCaNhanID = " + ht["HoiVienCaNhanID"]);
                    foreach (DataRow dataRow in arrDataRow)
                    {
                        dt.Rows.Remove(dataRow);
                    }
                }
            }

            rpHocVien.DataSource = dt.DefaultView;
            rpHocVien.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Load danh sach "Hoi vien tap the", "Cong ty to chuc" do vao Dropdownlist de search
    /// </summary>
    private void LoadListHoiVienTapThe()
    {
        ddlDonViCongTac.Items.Clear();
        string query = "SELECT HoiVienTapTheID, TenDoanhNghiep FROM tblHoiVienTapThe ORDER BY TenDoanhNghiep ASC";
        ddlDonViCongTac.Items.Add(new ListItem("Tất cả", ""));
        List<Hashtable> listData = _db.GetListData(query);
        foreach (Hashtable ht in listData)
        {
            ddlDonViCongTac.Items.Add(new ListItem(ht["TenDoanhNghiep"].ToString(), ht["HoiVienTapTheID"].ToString()));
        }
        if (!string.IsNullOrEmpty(_congTyId))
            ddlDonViCongTac.SelectedValue = _congTyId;
    }


    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Su kien khi bam nut "Tim kiem"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        LoadListHocVien();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/11/26
    /// Get chuoi ten loai hoc vien dua vao so loai hoc vien lay tu DB
    /// </summary>
    /// <param name="value">So loai hoc vien</param>
    /// <returns>Ten loai hoc vien</returns>
    protected string GetLoaiHocVien(string value)
    {
        switch (value)
        {
            case ListName.Type_LoaiHocVien_NguoiQuanTam:
                return "Người quan tâm";
            case ListName.Type_LoaiHocVien_HoiVien:
                return "Hội viên";
            case ListName.Type_LoaiHocVien_KiemToanVien:
                return "Kiểm toán viên";
        }
        return "";
    }
}