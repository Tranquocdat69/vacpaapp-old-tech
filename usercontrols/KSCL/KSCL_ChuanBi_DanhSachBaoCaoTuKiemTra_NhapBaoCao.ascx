﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_DanhSachBaoCaoTuKiemTra_NhapBaoCao.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_DanhSachBaoCaoTuKiemTra_NhapBaoCao" %>

<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<form id="Form1" name="Form1" clientidmode="Static" runat="server" method="post"
enctype="multipart/form-data">
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <table style="max-width: 750px;" border="0" class="formtbl">
        <tr>
            <td style="min-width: 120px;">
                ID.HVTT/ID.CTKT<span class="starRequired">(*)</span>:
            </td>
            <td style="width: 150px;">
                <input type="text" id="txtMaCongTy" name="txtMaCongTy" onchange="CallActionGetInforCongTy();" />
                <input type="hidden" id="hdCongTyID" name="hdCongTyID" />
            </td>
            <td style="width: 25px;">
                <input type="button" id="btnOpenFormCongTy" value="---" onclick="OpenDanhSachCongTyKiemToan();"
                    style="border: 1px solid gray;" />
            </td>
            <td style="min-width: 120px;">
                Danh sách CTKT phải nộp báo cáo tự kiểm tra:
            </td>
            <td style="width: 200px;">
                <select id="ddlDanhSach" name="ddlDanhSach">
                </select>
                <input type="hidden" id="hdDanhSachID" name="hdDanhSachID" />
            </td>
        </tr>
        <tr>
            <td>
                Tên HVTC/CTKT:
            </td>
            <td colspan="4">
                <input type="text" id="txtTenCongTy" name="txtTenCongTy" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Ngày nộp<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtNgayNop" name="txtNgayNop" style="width: 80px;" /><img
                    src="/images/icons/calendar.png" id="imgNgayNop" />
            </td>
            <td>
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td>
                File căn cứ<span class="starRequired">(*)</span>:
            </td>
            <td colspan="2">
                <input type="file" id="FileCanCu" name="FileCanCu" style="width: 210px;"></input>
            </td>
            <td colspan="2">
                <span id="spanTenFileCanCu" style="font-style: italic;"></span>&nbsp;<a href="javascript:;"
                    id="btnDownload" target="_blank"></a>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <span style="font-style: italic; color: red;">File báo cáo tự kiểm tra phải thuộc một
                    trong các định dạng .PDF, .BMP, .GiF, .PNG, .JPG. Kích thước file tải lên tối đa
                    10MB, chất lượng phải đảm bảo có thể đọc được và phải được quét hoặc chụp từ bản
                    gốc.</span>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Thông tin công ty</legend>
    <table style="max-width: 750px;" border="0" class="formtbl">
        <tr>
            <td style="min-width: 200px;">
                Doanh thu năm gần nhất<span class="starRequired">(*)</span>:
            </td>
            <td style="width: 150px;">
                <input type="text" id="txtDoanhThuNamGanNhat" name="txtDoanhThuNamGanNhat" onblur='DisplayTypeNumber(this);'
                    onfocus='UnDisplayTypeNumber(this);' style='text-align: right;' />
            </td>
            <td style="width: 25px;">
                VNĐ
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <input type="radio" value="0" id="rdDuDKCK" name="tieuchidkkt" />
                Đơn vị đủ ĐK KiT lĩnh vực CK&nbsp;
                <input type="radio" value="1" id="rdDuDKCongChungKhac" name="tieuchidkkt" />
                Đơn vị đủ ĐK KiT công chúng khác&nbsp;
                <input type="radio" value="2" checked="checked" name="tieuchidkkt" />
                Đơn vị khác
            </td>
        </tr>
        <tr>
            <td colspan="5">
                Số lượng báo cáo kiểm toán báo cáo tài chính đã phát hành của năm gần nhất<span class="starRequired">(*)</span>:
                <input type="text" id="txtSoBaoCaoKiemToanPH" name="txtSoBaoCaoKiemToanPH" style="width: 100px;
                    text-align: right;" onblur='DisplayTypeNumber(this);' onfocus='UnDisplayTypeNumber(this);' />
            </td>
        </tr>
        <tr>
            <td>
                Năm được kiểm tra gần nhất:
            </td>
            <td>
                <input type="text" id="txtNamDuocKiemTraGanNhat" name="txtNamDuocKiemTraGanNhat"
                    readonly="readonly" />
            </td>
            <td>
            </td>
            <td style="min-width: 120px;">
                Kết quả:
            </td>
            <td style="width: 150px;">
                <input type="text" id="txtKetQua" name="txtKetQua" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td>
                Vùng miền:
            </td>
            <td>
                <input type="text" id="txtVungMien" name="txtVungMien" readonly="readonly" />
            </td>
            <td colspan="3">
            </td>
        </tr>
    </table>
</fieldset>
<div style="text-align: center; width: 100%; margin-top: 10px;">
    <a id="btnSave" href="javascript:;" class="btn btn-rounded" onclick="Save();"><i
        class="iconfa-plus-sign"></i>Lưu</a> <span id="SpanCacNutChucNang" runat="server"
            visible="False">
            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn" OnClientClick="return confirm('Bạn chắc chắn muốn xóa danh sách này chứ?');"
                OnClick="btnDelete_Click"><i class="iconfa-trash"></i>Xóa</asp:LinkButton></span> <a id="A1" href="javascript:;" class="btn btn-rounded"
                    onclick="parent.CloseFormNhapBaoCao();"><i class="iconfa-off"></i>Đóng</a>
</div>
<iframe name="iframeProcess" width="0px" height="0px;"></iframe>
</form>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#txtNgayNop").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgayNop").click(function () {
        $("#txtNgayNop").datepicker("show");
    });

    function OpenDanhSachCongTyKiemToan() {
        $("#DivDanhSachCongTyKiemToan").empty();
        $("#DivDanhSachCongTyKiemToan").append($("<iframe width='100%' height='100%' id='ifDanhSachCongTyKiemToan' name='ifDanhSachCongTyKiemToan' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=HoiVienTapThe_DanhSachCongTyKiemToan_Choice"));
        $("#DivDanhSachCongTyKiemToan").dialog({
            resizable: true,
            width: 900,
            height: 700,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách công ty kiểm toán</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSachCongTyKiemToan.Choose();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachCongTyKiemToan').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="icon-ok"> </i>&nbsp;');
        $('#DivDanhSachCongTyKiemToan').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="icon-off"> </i>&nbsp;');
    }

    function CloseDanhSachCongTyKiemToan() {
        $("#DivDanhSachCongTyKiemToan").dialog('close');
    }
    
    function CallActionGetInforCongTy() {
        var maCongTy = $('#txtMaCongTy').val();
        
        // Gọi trang xử lý Load thông tin công ty theo mã công ty
        iframeProcess.location = '/iframe.aspx?page=kscl_chuanbi_process&mact=' + maCongTy + '&action=loadct';
    }

    function DisplayInforCongTy(value) {
        var arrValue = value.split(';#');
        $('#hdCongTyID').val(arrValue[0]);
        $('#txtMaCongTy').val(arrValue[1]);
        $('#txtTenCongTy').val(arrValue[2]);
        $('#txtVungMien').val(arrValue[11]);
        $('#txtNamDuocKiemTraGanNhat').val(arrValue[12]);
        $('#txtKetQua').val(arrValue[13]);

        if(!$('#txtMaCongTy').prop('readonly')){
            
        // Gọi trang xử lý Load những danh sách yêu cầu nộp báo cáo tự kiểm tra mà công ty chưa nộp
        iframeProcess.location = '/iframe.aspx?page=kscl_chuanbi_process&idct=' + arrValue[0] + '&action=loaddsycnopbcchuanop';
            }
    }
    
    function DisplayListDanhSachYcNopBCTuKiemTra(arrData) {
        // Remove toàn bộ option cũ
        $('#ddlDanhSach').children().remove();
        // Add option mới theo công ty
        if(arrData.length > 0) {
            for(var i = 0 ; i < arrData.length ; i ++) {
                $('#ddlDanhSach').append('<option value="'+arrData[i][0]+'">'+arrData[i][1]+'</option>');
            }
        }
    }
    
    $('#<%=Form1.ClientID %>').validate({
        rules: {
            txtNgayNop: {
                required: true, dateITA: true
            },
            txtDoanhThuNamGanNhat: {
                required: true,digitsWithOutSeparatorCharacterVN: true
            },
            txtSoBaoCaoKiemToanPH: {
                required: true,digitsWithOutSeparatorCharacterVN: true
            },
            FileCanCu:{
                required: true,extensionImage: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }
    });

    function Save() {
        if($('#hdCongTyID').val().length == 0) {
            alert('Phải chọn công ty hoặc hội viên tổ chức!');
            return;
        }
        if($('#ddlDanhSach option:selected').length == 0 || $('#ddlDanhSach option:selected').val().length == 0) {
            alert('Phải chọn danh sách yêu cầu nộp báo cáo tự kiểm tra mà công ty này chưa nộp!');
            return;
        }
        if($('#spanTenFileCanCu').html() != '')
            $('#FileCanCu').rules('remove');
        $('#Form1').submit();
    }
    
    <% LoadOneData();%>
</script>
<div id="DivDanhSachCongTyKiemToan">
</div>