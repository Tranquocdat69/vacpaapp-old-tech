﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_DanhSachCongTyKiemTraTrucTiep_FormChonDSTuKiemTra.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_DanhSachCongTyKiemTraTrucTiep_FormChonDSTuKiemTra" %>

<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<form id="Form1" name="Form1" ClientIDMode="Static" runat="server">
<fieldset class="fsBlockInfor">
    <legend>Thông tin tìm kiếm</legend>
    <table style="width: 100%;" border="0">
        <tr>
            <td style="width: 49%;">
                Mã danh sách:
                <asp:TextBox ID="txtMaDanhSach" runat="server" Width="100px"></asp:TextBox>
            </td>
            <td style="width: 49%;">
                Ngày lập:
                <asp:TextBox ID="txtNgayLap" runat="server" Width="100px" CssClass="dateITA"></asp:TextBox>
            </td>
        </tr>
    </table>
</fieldset>
<div style="margin-top: 10px; text-align: center; width: 100%;">
    <asp:LinkButton ID="lbtTruyVan" runat="server" CssClass="btn" OnClick="lbtTruyVan_Click" OnClientClick="return CheckValidFormSearch();"><i class="iconfa-plus-sign"></i>Truy vấn</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách công ty kiểm toán phải nộp báo cáo tự kiểm tra</legend>
    <table id="tblDanhSach" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th style="width: 30px;">
                    
                </th>
                <th style="width: 30px;">
                    STT
                </th>
                <th>
                    Mã danh sách
                </th>
                <th>
                    Ngày lập
                </th>
                <th>
                    Số công ty
                </th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rpDanhSach" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="vertical-align: middle;">
                            <input type="radio" value='<%# Eval("ID") + ";#" + Eval("MaDanhSach") %>' name="ChonDanhSach" />
                        </td>
                        <td>
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td>
                            <%# Eval("MaDanhSach")%>
                        </td>
                        <td>
                            <%# Eval("NgayLap") %>
                        </td>
                        <td>
                            <%# Eval("SoCongTy")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
</fieldset>
</form>
<iframe name="iframeProcess_Add" width="0px" height="0px"></iframe>
<script type="text/javascript">
    $('#<%= txtNgayLap.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });

    $(document).ready(function () {
        $("#Form1").validate({
            onsubmit: false
        });

        $('#tblDanhSach').stickyTableHeaders();
    });

    function Choose() {
        var value = '';
        $('#tblDanhSach :radio').each(function () {
            if ($(this).prop("checked") == true) {
                value += $(this).val();
            }
        });
        if (value.length > 0) {
            var type = getParameterByName('type');
            if (type == 'dstukiemtra')
                parent.DisplayInforDsTuKiemTra(value);
            if (type == 'dskiemtravuviec')
                parent.DisplayInforDsKiemTraVuViec(value);
            parent.CloseDanhSach();
        }
        else {
            alert('Phải chọn ít nhất một danh sách để thực hiện thao tác!');
        }
    }
</script>