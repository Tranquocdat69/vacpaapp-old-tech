﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_CapNhatGioDaoTaoChoThanhVienDoanKiemTra.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_CapNhatGioDaoTaoChoThanhVienDoanKiemTra" %>

<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" name="Form1" clientidmode="Static" runat="server" method="post"
enctype="multipart/form-data">
<h4 class="widgettitle">
    Cập nhật giờ CNKT cho thành viên đoàn kiểm tra</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <table style="max-width: 700px;" border="0" class="formtbl">
        <tr>
            <td>
                Mã đoàn kiểm tra<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlDoanKiemTra" name="ddlDoanKiemTra" style="width: 200px;" onchange="GetDanhSachThanhVienTrongDoanKiemTra();">
                    <% LoadListDoanKiemTra();%>
                </select>
            </td>
            <td>
                Ngày cập nhật<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtNgayCapNhat" name="txtNgayCapNhat" style="width: 80px;" />
                <img src="/images/icons/calendar.png" id="imgNgayCapNhat" />
            </td>
        </tr>
    </table>
</fieldset>
<div style="text-align: center; width: 100%; margin-top: 10px;">
    <a id="btnSave" href="javascript:;" class="btn btn-rounded" onclick="SubmitForm();">
        <i class="iconfa-plus-sign"></i>Lưu</a>
</div>
<div id="DivDanhSachThanhVienDoanKT">
    <fieldset class="fsBlockInfor">
        <legend>Danh sách thành viên đoàn kiểm tra</legend>
        <table id="tblDanhSachThanhVienDaAdd" width="100%" border="0" class="formtbl">
            <thead>
                <tr>
                    <th rowspan="2">
                        STT
                    </th>
                    <th rowspan="2">
                        Tên thành viên
                    </th>
                    <th rowspan="2">
                        Năm sinh
                    </th>
                    <th rowspan="2">
                        Chức vụ
                    </th>
                    <th rowspan="2">
                        Đơn vị công tác
                    </th>
                    <th rowspan="2" style="width: 80px;">
                        Số chứng chỉ kiểm toán viên
                    </th>
                    <th rowspan="2" style="width: 80px;">
                        Ngày cấp chứng chỉ KTV
                    </th>
                    <th rowspan="2">
                        Vai trò
                    </th>
                    <th colspan="3">
                        Số giờ đào tạo, CNKT
                    </th>
                </tr>
                <tr>
                    <th style="width: 50px;">
                        KT,KiT
                    </th>
                    <th style="width: 50px;">
                        ĐĐ
                    </th>
                    <th style="width: 50px;">
                        #
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </fieldset>
</div>
</form>
<iframe name="iframeProcess_ThanhVien" width="0px" height="0px"></iframe>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#txtNgayCapNhat").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgayCapNhat").click(function () {
        $("#txtNgayCapNhat").datepicker("show");
    });
    
    $(document).ready(function () {
        $('#tblDanhSachThanhVienDaAdd').stickyTableHeaders();
    });

    function GetDanhSachThanhVienTrongDoanKiemTra() {
        iframeProcess_ThanhVien.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&iddoan='+$('#ddlDoanKiemTra option:selected').val()+'&action=dsthanhvien';
    }
    
    function DrawData_DanhSachThanhVienTrongDoanKiemTra(arrData) {
        var tbl = document.getElementById('tblDanhSachThanhVienDaAdd');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 2; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            var ngayCapNhat = '';
            for(var i = 0; i < arrData.length; i ++) {
                var doanKiemTraThanhVienId = arrData[i][0];
                var tenThanhVien = arrData[i][1];
                var namSinh = arrData[i][2];
                var tenChucVu = arrData[i][3];
                var donViCongTac = arrData[i][4];
                var soChungChiKTV = arrData[i][5];
                var ngayCapChungChiKTV = arrData[i][6];
                var vaiTro = arrData[i][8];
                var soGioKT = arrData[i][9];
                var soGioDD = arrData[i][10];
                var soGioKhac = arrData[i][11];
                ngayCapNhat = arrData[i][12];

                var tr = document.createElement('TR');
                
                var arrColumn = [(i + 1), tenThanhVien, namSinh, tenChucVu, donViCongTac, soChungChiKTV, ngayCapChungChiKTV, GetTenVaiTro(vaiTro)];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }

                var controlName = ['txtSoGioKT', 'txtSoGioDD', 'txtSoGioKhac'];
                var arrValue = [soGioKT, soGioDD, soGioKhac];
                for(var j = 0; j < 3; j ++){
                    var tdSoGioDaoTao = document.createElement("TD");
                    var input = document.createElement('input');
                    input.type = 'text';
                    input.id = controlName[j] + '_' + doanKiemTraThanhVienId;
                    input.name = controlName[j] + '_' + doanKiemTraThanhVienId;
                    input.value = arrValue[j];
                    tdSoGioDaoTao.appendChild(input);  
                    tr.appendChild(tdSoGioDaoTao);
                }
                
                tbl.appendChild(tr);
            }
            if(ngayCapNhat != '')
                $('#txtNgayCapNhat').val(ngayCapNhat);
        }
    }
    
    function GetTenVaiTro(vaiTro) {
        if(vaiTro == '1')
            return 'Trưởng đoàn 1';
        if(vaiTro == '2')
            return 'Trưởng đoàn 2';
        if(vaiTro == '3')
            return 'Thành viên';
        return "";
    }
    
    $('#<%=Form1.ClientID %>').validate({
        rules: {
            txtNgayCapNhat: {
                required: true, dateITA: true
            },
            
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }
    });
    
    function SubmitForm() {
        if($('#ddlDoanKiemTra option:selected').length == 0 || $('#ddlDoanKiemTra option:selected').val().length == 0) {
            alert('Phải chọn đoàn kiểm tra!');
            return;
        }
        
        $('#<%=Form1.ClientID %>').submit();
    }
    
    <% CheckPermissionOnPage();%>
</script>