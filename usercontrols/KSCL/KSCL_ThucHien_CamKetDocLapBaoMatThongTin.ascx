﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ThucHien_CamKetDocLapBaoMatThongTin.ascx.cs"
    Inherits="usercontrols_KSCL_ThucHien_CamKetDocLapBaoMatThongTin" %>
<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" name="Form1" clientidmode="Static" runat="server" method="post"
enctype="multipart/form-data">
<h4 class="widgettitle">
    Cam kết độc lập/bảo mật thông tin</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <input type="hidden" id="hdCamKetID" name="hdCamKetID" />
    <input type="hidden" id="hdAction" name="hdAction" />
    <table style="width: 100%;" border="0" class="formtbl">
        <tr>
            <td style="width: 180px;">
                Mã đoàn kiểm tra<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlDoanKiemTra" name="ddlDoanKiemTra" style="width: 200px;" onchange="GetDanhSachThanhVienTrongDoanKiemTra();">
                    <% LoadListDoanKiemTra();%>
                </select>
            </td>
            <td style="width: 180px;">
                Ngày ký cam kết đối tượng được kiểm tra<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtNgayKyCamKet" name="txtNgayKyCamKet" style="width: 80px;" />
                <img src="/images/icons/calendar.png" id="imgNgayKyCamKet" />
            </td>
        </tr>
        <tr>
            <td>
                Công ty kiểm toán<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlCongTy" name="ddlCongTy" style="width: 200px;" onchange="GetCamKet();">
                </select>
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td>
                Cam kết với đối tượng được kiểm tra<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="file" id="FileCamKetBaoMatThongTin" name="FileCamKetBaoMatThongTin"
                    style="width: 210px;"></input>&nbsp; <span id="spanTenFileCamKetBaoMatThongTin" style="font-style: italic;">
                    </span>&nbsp; <a href="javascript:;" id="btnDownloadCamKetBaoMatThongTin" target="_blank">
                    </a>
            </td>
            <td>
                Cam kết thành viên đoàn<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="file" id="FileCamKetDocLap" name="FileCamKetDocLap" style="width: 210px;"></input>&nbsp;
                <span id="spanTenFileCamKetDocLap" style="font-style: italic;"></span>&nbsp;<a href="javascript:;"
                    id="btnDownloadCamKetDocLap" target="_blank"></a>
            </td>
        </tr>
    </table>
</fieldset>
<div id="DivDanhSachThanhVienDoanKT">
    <fieldset class="fsBlockInfor">
        <input type='hidden' id="Hidden1" />
        <legend>Danh sách thành viên trong đoàn kiểm tra</legend>
        <table id="tblDanhSachThanhVienDaAdd" width="100%" border="0" class="formtbl">
            <thead>
                <tr>
                    <th>
                        STT
                    </th>
                    <th>
                        Tên thành viên
                    </th>
                    <th>
                        Năm sinh
                    </th>
                    <th>
                        Chức vụ
                    </th>
                    <th>
                        Đơn vị công tác
                    </th>
                    <th>
                        Số chứng chỉ KTV
                    </th>
                    <th>
                        Ngày cấp chứng chỉ KTV
                    </th>
                    <th>
                        Vai trò
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </fieldset>
</div>
<div style="text-align: center; width: 100%; margin-top: 10px;">
    <a id="btnSave" href="javascript:;" class="btn btn-rounded" onclick="$('#hdAction').val('save'); SubmitForm();">
        <i class="iconfa-plus-sign"></i>Lưu</a> <span id="SpanCacNutChucNang">
            <a href="javascript:;" id="btnDelete" class="btn" onclick="SubmitForm_Delete();"><i class="iconfa-remove-sign"></i>Xóa</a>
        </span>
</div>
</form>
<iframe name="iframeProcess_CongTy" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_ThanhVien" width="0px" height="0px"></iframe>
<script type="text/javascript">
    // add open Datepicker event for calendar inputs
    $("#txtNgayKyCamKet").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgNgayKyCamKet").click(function () {
        $("#txtNgayKyCamKet").datepicker("show");
    });
    
    $(document).ready(function () {
        $('#tblDanhSachThanhVienDaAdd').stickyTableHeaders();
    });


    
    function GetDanhSachThanhVienTrongDoanKiemTra() {
        // Load danh sách thành viên trong đoàn kiểm tra
        iframeProcess_ThanhVien.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&iddoan='+$('#ddlDoanKiemTra option:selected').val()+'&action=dsthanhvien';
        
        // Load danh sách công ty theo đoàn kiểm tra
        iframeProcess_CongTy.location = '/iframe.aspx?page=KSCL_ThucHien_Process&iddoan='+$('#ddlDoanKiemTra option:selected').val()+'&action=loaddscongty';
    }
    
    function GetCamKet() {
        iframeProcess_CongTy.location = '/iframe.aspx?page=KSCL_ThucHien_Process&iddoan='+$('#ddlDoanKiemTra option:selected').val()+'&idcongty='+$('#ddlCongTy option:selected').val()+'&action=getcamket';
    }
    
    function DisplayDataCamKet(arrDataCamKet) {
        if(arrDataCamKet.length > 0) {
            $('#txtNgayKyCamKet').val(arrDataCamKet[1]);
            $('#spanTenFileCamKetBaoMatThongTin').html(arrDataCamKet[2]);
            $('#spanTenFileCamKetDocLap').html(arrDataCamKet[3]);
            if(parseInt(arrDataCamKet[0]) > 0){
                $('#hdCamKetID').val(arrDataCamKet[0]);
                $('#SpanCacNutChucNang').css('display', '');
                $('#btnDownloadCamKetBaoMatThongTin').prop('href', '/admin.aspx?page=getfile&id='+arrDataCamKet[0]+'&typecamket=bmtt&type=<%= ListName.Table_KSCLCamKet %>');
                $('#btnDownloadCamKetBaoMatThongTin').html('Tải về');
                $('#btnDownloadCamKetDocLap').prop('href', '/admin.aspx?page=getfile&id='+arrDataCamKet[0]+'&typecamket=dl&type=<%= ListName.Table_KSCLCamKet %>');
                $('#btnDownloadCamKetDocLap').html('Tải về');
            } else {
                $('#SpanCacNutChucNang').css('display', 'none');
                $('#btnDownloadCamKetBaoMatThongTin').html('');
                $('#btnDownloadCamKetDocLap').html('');
                $('#hdCamKetID').val('');
            }
        }
    }

    var itemCongTySelected = '';
    function DisplayListCongTy(arrData) {
        // Remove toàn bộ option cũ
        $('#ddlCongTy').children().remove();
        // Add option mới theo công ty
        if(arrData.length > 0) {
            for(var i = 0 ; i < arrData.length ; i ++) {
                if(itemCongTySelected.length > 0 && itemCongTySelected == arrData[i][0])
                    $('#ddlCongTy').append('<option value="'+arrData[i][0]+'" selected="selected">'+arrData[i][1]+'</option>');
                else
                    $('#ddlCongTy').append('<option value="'+arrData[i][0]+'">'+arrData[i][1]+'</option>');
            }
        }
        itemCongTySelected = '';
        GetCamKet();
    }
    
    function DrawData_DanhSachThanhVienTrongDoanKiemTra(arrData) {
        var tbl = document.getElementById('tblDanhSachThanhVienDaAdd');
        var trs = tbl.getElementsByTagName("tr");
        for (var i = 1; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            for(var i = 0; i < arrData.length; i ++) {
                var doanKiemTraThanhVienId = arrData[i][0];
                var tenThanhVien = arrData[i][1];
                var namSinh = arrData[i][2];
                var tenChucVu = arrData[i][3];
                var donViCongTac = arrData[i][4];
                var soChungChiKTV = arrData[i][5];
                var ngayCapChungChiKTV = arrData[i][6];
                var vaiTro = arrData[i][8];

                var tr = document.createElement('TR');
                
                var arrColumn = [(i + 1), tenThanhVien, namSinh, tenChucVu, donViCongTac, soChungChiKTV, ngayCapChungChiKTV, GetTenVaiTro(vaiTro)];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbl.appendChild(tr);
            }
        }
    }
    
    function GetTenVaiTro(vaiTro) {
        if(vaiTro == '1')
            return 'Trưởng đoàn 1';
        if(vaiTro == '2')
            return 'Trưởng đoàn 2';
        if(vaiTro == '3')
            return 'Thành viên';
        return "";
    }

    $('#<%=Form1.ClientID %>').validate({
        rules: {
            txtNgayKyCamKet: {
                required: true, dateITA: true
            },
            ddlDoanKiemTra:{
                required: true
            },
            ddlCongTy:{
                required: true
            },
            FileCamKetBaoMatThongTin:{
                required: true
            },
            FileCamKetDocLap:{
                required: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }
    });
    
    function SubmitForm() {
        if($('#spanTenFileCamKetBaoMatThongTin').html() != '')
            $('#FileCamKetBaoMatThongTin').rules('remove');
        if($('#spanTenFileCamKetDocLap').html() != '')
            $('#FileCamKetDocLap').rules('remove');
        $('#<%=Form1.ClientID %>').submit();
    }
    
    function SubmitForm_Delete() {
        if(confirm('Bạn chắc chắn muốn xóa danh sách này chứ?')){
            if(parseInt($('#hdCamKetID').val()) > 0){
                $('#hdAction').val('delete');
                $('#FileCamKetBaoMatThongTin').rules('remove');
                $('#FileCamKetDocLap').rules('remove');
                $('#txtNgayKyCamKet').rules('remove');
                $('#<%=Form1.ClientID %>').submit();        
            } else {
                alert('Bản ghi không tồn tại hoặc đã bị xóa.');
            }
        }
    }
    
    <% CheckPermissionOnPage();%>
</script>
