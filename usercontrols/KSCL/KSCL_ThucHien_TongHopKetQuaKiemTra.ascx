﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ThucHien_TongHopKetQuaKiemTra.ascx.cs"
    Inherits="usercontrols_KSCL_ThucHien_TongHopKetQuaKiemTra" %>
<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .tdInput
    {
        width: 120px;
    }
</style>
<form id="Form1" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Tổng hợp kết quả kiểm tra</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <input type="hidden" name="hdBaoCaoKTHTID" id="hdBaoCaoKTHTID" />
    <input type="hidden" name="hdAction" id="hdAction" />
    <table id="tblThongTinChung" width="800px" border="0" class="formtbl">
        <tr>
            <td style="width: 180px;">
                Mã hồ sơ KSCL<span class="starRequired">(*)</span>:
            </td>
            <td class="tdInput">
                <input type="text" name="txtMaHoSo" id="txtMaHoSo" onchange="CallActionGetInforHoSo();" />
                <input type="hidden" name="hdHoSoID" id="hdHoSoID" />
            </td>
            <td style="width: 25px;">
                <input type="button" value="---" onclick="OpenDanhSachHoSo();" style="border: 1px solid gray;" />
            </td>
            <td style="width: 180px;">
                Tên công ty kiểm toán:
            </td>
            <td>
                <span id="spanTenCongTy"></span>
                <input type="hidden" name="hdMaCongTy" id="hdMaCongTy" />
            </td>
        </tr>
        <tr>
            <td>
                Tổng điểm KTHT:
            </td>
            <td colspan="2">
                <input type="text" name="txtTongDiemKTHT" id="txtTongDiemKTHT" readonly="readonly" />
            </td>
            <td>
                Xếp loại hệ thống:
            </td>
            <td>
                <span id="spanXepLoaiKTHT"></span>
            </td>
        </tr>
        <tr>
            <td>
                Điểm trung bình KTKT:
            </td>
            <td colspan="2">
                <input type="text" name="txtDiemTrungBinhKTKT" id="txtDiemTrungBinhKTKT" readonly="readonly" />
            </td>
            <td>
                Xếp loại kỹ thuật<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlXepLoaiKyThuat" name="ddlXepLoaiKyThuat">
                    <option value="1">Tốt</option>
                    <option value="2">Đạt yêu cầu</option>
                    <option value="3">Không đạt</option>
                    <option value="4">Yếu kém</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                Điểm trung bình KSCL:
            </td>
            <td colspan="2">
                <input type="text" name="txtDiemTrungBinhKSCL" id="txtDiemTrungBinhKSCL" readonly="readonly" />
            </td>
            <td>
                Xếp loại chung<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlXepLoaiChung" name="ddlXepLoaiChung">
                    <option value="1">Tốt</option>
                    <option value="2">Đạt yêu cầu</option>
                    <option value="3">Không đạt</option>
                    <option value="4">Yếu kém</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                Biên bản KSCL<span class="starRequired">(*)</span>:
            </td>
            <td colspan="4">
                <input type="file" id="FileBienBan" name="FileBienBan" />&nbsp;File biên bản: <span
                    id="spanTenFileBienBan" style="font-style: italic;"></span>&nbsp;<a href="javascript:;"
                        id="btnDownload" target="_blank"></a>
            </td>
        </tr>
        <tr>
            <td>
                Kết luận chung:
            </td>
            <td colspan="4">
                <textarea rows="5" id="txtKetLuanChung" name="txtKetLuanChung"></textarea>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor">
    <legend>Tổng hợp kết quả kiểm tra kỹ thuật</legend>
    <table id="tblTongHopketQuaKiemTraKyThuat" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th rowspan="2">
                    STT
                </th>
                <th rowspan="2" style="min-width: 200px;">
                    Tên khách hàng kiểm toán
                </th>
                <th colspan="2">
                    Người ký báo cáo kiểm toán
                </th>
                <th rowspan="2" style="width: 70px;">
                    BCTC năm
                </th>
                <th rowspan="2" style="width: 100px;">
                    Cộng điểm<br />
                    (Đã quy đổi)
                </th>
                <th colspan="4">
                    Kết quả kiểm tra hồ sơ
                </th>
            </tr>
            <tr>
                <th style="width: 150px;">
                    Thành viên BGK
                </th>
                <th style="width: 150px;">
                    KTV
                </th>
                <th style="width: 50px;">
                    Loại 1<br />
                    Tốt
                </th>
                <th style="width: 50px;">
                    Loại 2<br />
                    Đạt
                </th>
                <th style="width: 50px;">
                    Loại 3<br />
                    Không đạt
                </th>
                <th style="width: 50px;">
                    Loại 4<br />
                    Yếu kém
                </th>
            </tr>
        </thead>
        <tbody id="TBodyDanhSachCauHoi">
        </tbody>
    </table>
</fieldset>
<div id="DivControlAction" style="width: 100%; margin: 10px 0px 5px 0px; text-align: center;
    display: none;">
    <a id="btnSave" href="javascript:;" class="btn" onclick="SubmitForm();"><i class="iconfa-save">
    </i>Lưu</a> <a id="btnDelete" href="javascript:;" class="btn" onclick="Delete();"><i
        class="iconfa-trash"></i>Xóa</a>
</div>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadKetQuaKiemTra" width="0px" height="0px"></iframe>
</form>
<script type="text/javascript">
    // Validate form
    $('#Form1').validate({
        rules: {
            hdHoSoID: {
                required: true
            },
            ddlXepLoaiKyThuat: {
                required:true
            },
            FileBienBan:{
                required: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }  
    });

    function OpenDanhSachHoSo() {
        $("#DivDanhSachHoSo").empty();
        $("#DivDanhSachHoSo").append($("<iframe width='100%' height='100%' id='ifDanhSachHoSo' name='ifDanhSachHoSo' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong_MiniList"));
        $("#DivDanhSachHoSo").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách hồ sơ kiểm soát chất lượng</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSachHoSo.ChooseHoSo();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachHoSo').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivDanhSachHoSo').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }
    
    function GetMaHoSoFromPopup(value) {
        var arrValue = value.split(';#');
        $('#txtMaHoSo').val(arrValue[1]);
        iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso='+arrValue[1]+'&action=loadinforhoso';
    }
    
    function CloseFormDanhSachHoSo() {
        $("#DivDanhSachHoSo").dialog('close');
    }
    
    function CallActionGetInforHoSo() {
        var maHoSo = $('#txtMaHoSo').val();
        iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso='+maHoSo+'&action=loadinforhoso';
    }

    function DisplayInforHoSo(value) {
        var arrValue = value.split(';#');
        if(arrValue[0].length == 0) {
            $('#txtMaHoSo').val('');
            var tbodyMain = document.getElementById('TBodyDanhSachCauHoi');
            var trs = tbodyMain.getElementsByTagName("tr");
            for (var i = 0; i < trs.length; i++) {
                trs[i].parentNode.removeChild(trs[i]);
                i--;
            }
            
            $('#DivControlAction').css('display', 'none');
            alert('Mã hồ sơ không đúng hoặc không tồn tại!');
        } else {
            $('#DivControlAction').css('display', '');
        }
        $('#hdHoSoID').val(arrValue[0]);
        $('#hdMaCongTy').val(arrValue[1]);
        $('#spanTenCongTy').html(arrValue[2]);
        
        // Nếu mã hồ sơ hợp lệ -> Get danh sách câu hỏi và dữ liệu đã chấm điểm nếu có
        if(arrValue[0].length > 0)
            GetKetQuaKiemTra();
    }

    function GetKetQuaKiemTra() {
        iframeProcess_LoadKetQuaKiemTra.location = '/iframe.aspx?page=KSCL_ThucHien_Process&idhoso='+$('#hdHoSoID').val()+'&action=getthkqkt';
    }
    
    function DrawDataKetQuaKiemTra(dataBaoCao, arrDataKTKT) {
        var arrValue = dataBaoCao.split(';#');
        var baoCaoTongHopId = arrValue[0];
        $('#txtTongDiemKTHT').val(arrValue[1]);
        $('#spanXepLoaiKTHT').html(arrValue[2]);
        $('#txtDiemTrungBinhKTKT').val(arrValue[3]);
        $('#ddlXepLoaiKyThuat').val(arrValue[4]);
        $('#txtDiemTrungBinhKSCL').val(arrValue[5]);
        $('#ddlXepLoaiChung').val(arrValue[6]);
        $('#txtKetLuanChung').val(arrValue[7]);
        $('#spanTenFileBienBan').html(arrValue[8]);
        $('#btnDownload').prop('href', '/admin.aspx?page=getfile&id='+baoCaoTongHopId+'&type=<%= ListName.Table_KSCLBaoCaoTongHop %>');
        if(baoCaoTongHopId.length > 0) {
            $('#btnDownload').html('Tải về');
            $('#btnDelete').css('display', '');
        } else {
            $('#btnDownload').html('');
            $('#btnDelete').css('display', 'none');
        }
        
        var tbodyMain = document.getElementById('TBodyDanhSachCauHoi');
        var trs = tbodyMain.getElementsByTagName("tr");
        for (var i = 0; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrDataKTKT.length > 0) {
            for(var i = 0; i < arrDataKTKT.length; i ++) {
                var idBaoCaoKTKT = arrDataKTKT[i][0];
                var tenKHKT = arrDataKTKT[i][1];
                var tenBGD = arrDataKTKT[i][2];
                var tenKTV = arrDataKTKT[i][3];
                var bctcNam = arrDataKTKT[i][4];
                var tongDiemKTKT = arrDataKTKT[i][5];
                var xepLoai = arrDataKTKT[i][6];
                var xepLoai1 = '';
                var xepLoai2 = '';
                var xepLoai3 = '';
                var xepLoai4 = '';
                if(xepLoai == '1')
                    xepLoai1 = 'x';
                if(xepLoai == '2')
                    xepLoai2 = 'x';
                if(xepLoai == '3')
                    xepLoai3 = 'x';
                if(xepLoai == '4')
                    xepLoai4 = 'x';

                var tr = document.createElement('TR');
                var arrColumn = [(i + 1), tenKHKT, tenBGD, tenKTV, bctcNam, tongDiemKTKT, xepLoai1, xepLoai2, xepLoai3, xepLoai4];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    if(j == 0)
                        td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                
                tbodyMain.appendChild(tr);
            }
        }
    }
    
    function SubmitForm() {
        $('#hdAction').val('update');
        if($('#spanTenFileBienBan').html() != '')
            $('#FileBienBan').rules('remove');
        $('#Form1').submit();
    }
    
    function Delete() {
        if(confirm('Bạn chắc chắn muốn xóa báo cáo này chứ?')){
            $('#hdAction').val('delete');
                $('#FileBienBan').rules('remove');
            
            $('#Form1').submit();
        }
    }
    
    jQuery(document).ready(function () {
        $('#tblTongHopketQuaKiemTraKyThuat').stickyTableHeaders();
    });
    
    <% CheckPermissionOnPage(); %>
</script>
<div id="DivDanhSachHoSo">
</div>
