﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_KetThuc_XuLySaiPham : System.Web.UI.UserControl
{
    protected string tenchucnang = "Xử lý sai phạm theo quyết định của BTC và VACPA";
    protected string _listPermissionOnFunc_XuLySaiPham = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected string _IDLopHoc = "";
    Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
            _listPermissionOnFunc_XuLySaiPham = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_KetThuc_XuLySaiPham, cm.connstr);
            if (_listPermissionOnFunc_XuLySaiPham.Contains("XEM|"))
            {
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "update")
                    Save();
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "delete")
                    Delete();
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "approve")
                    Approve();
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "reject")
                    Reject();
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "backapprove")
                    BackApprove();
            }
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + " !</div>"));
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }

    protected void CheckPermissionOnPage()
    {
        if (!_listPermissionOnFunc_XuLySaiPham.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");

        if (!_listPermissionOnFunc_XuLySaiPham.Contains("KETXUAT|"))
            Response.Write("$('#btnExport').remove();");

        if (!_listPermissionOnFunc_XuLySaiPham.Contains("THEM|"))
            Response.Write("$('#btnSave').remove();");

        if (!_listPermissionOnFunc_XuLySaiPham.Contains("DUYET|"))
        {
            Response.Write("$('#btnApprove').remove();");
            Response.Write("$('#btnReject').remove();");
            Response.Write("$('#btnBackApprove').remove();");
        }

        if (!_listPermissionOnFunc_XuLySaiPham.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");
    }

    private void Save()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            SqlKsclXuLySaiPhamProvider pro = new SqlKsclXuLySaiPhamProvider(ListName.ConnectionString, false, string.Empty);
            NameValueCollection nvc = Request.Form;
            bool success = false;
            if (!string.IsNullOrEmpty(nvc["hdDanhSachID"]))
            {
                KsclXuLySaiPham obj;
                string xuLySaiPhamId = "";
                string query = "SELECT XuLySaiPhamID FROM " + ListName.Table_KSCLXuLySaiPham + " WHERE DSKiemTraTrucTiepID = " + nvc["hdDanhSachID"];
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                    xuLySaiPhamId = listData[0]["XuLySaiPhamID"].ToString();
                if (string.IsNullOrEmpty(xuLySaiPhamId)) // insert
                {
                    obj = new KsclXuLySaiPham();
                    obj.DsKiemTraTrucTiepId = Library.Int32Convert(nvc["hdDanhSachID"]);
                }
                else
                {
                    obj = pro.GetByXuLySaiPhamId(Library.Int32Convert(xuLySaiPhamId));
                }
                obj.SoQuyetDinh = nvc["txtSoQuyetDinh"];
                if (!string.IsNullOrEmpty(nvc["txtNgayRaQuyetDinh"]))
                    obj.NgayQuyetDinh = Library.DateTimeConvert(nvc["txtNgayRaQuyetDinh"], "dd/MM/yyyy");
                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_ChoDuyet, _db));
                if (string.IsNullOrEmpty(xuLySaiPhamId)) // insert
                {
                    if (pro.Insert(obj))
                        success = true;
                }
                else
                {
                    if (pro.Update(obj))
                        success = true;
                }

                js = "<script type='text/javascript'>" + Environment.NewLine;
                if (success)
                    js += "CallAlertSuccessFloatRightBottom('Lưu thông tin xử lý sai phạm thành công.');" + Environment.NewLine;
                else
                    js += "CallAlertErrorFloatRightBottom('Không thể lưu thông tin xử lý sai phạm.');" + Environment.NewLine;
                js += "$('#txtMaDanhSach').val('" + nvc["txtMaDanhSach"] + "');" + Environment.NewLine;
                js += "iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=loadttdanhsach&madanhsach=" + nvc["txtMaDanhSach"] + "';" + Environment.NewLine;
                js += "</script>";
                Page.RegisterStartupScript("SaveSucess", js);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Delete()
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(Request.Form["hdDanhSachID"]))
            {
                string query = "SELECT XuLySaiPhamID FROM " + ListName.Table_KSCLXuLySaiPham + " WHERE DSKiemTraTrucTiepID = " + Request.Form["hdDanhSachID"];
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    string xuLySaiPhamId = listData[0]["XuLySaiPhamID"].ToString();
                    SqlKsclXuLySaiPhamProvider pro = new SqlKsclXuLySaiPhamProvider(ListName.ConnectionString, false, string.Empty);
                    // Kiểm tra ngày xóa < ngày kết thúc kiểm tra
                    bool flag = false; // Biến để xác định có được xóa dữ liệu ko
                    KsclXuLySaiPham obj = pro.GetByXuLySaiPhamId(Library.Int32Convert(xuLySaiPhamId));
                    if (obj != null && obj.XuLySaiPhamId > 0)
                    {
                        //if (obj.TinhTrangId)
                        //{
                        //    string js = "<script type='text/javascript'>" + Environment.NewLine;
                        //    js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Hồ sơ đã hoàn thành. Không được phép xóa dữ liệu tổng hợp kết quả kiểm tra.\").show();" + Environment.NewLine;
                        //    js += "$('#txtMaHoSo').val('" + Request.Form["txtMaHoSo"] + "');" + Environment.NewLine;
                        //    js += "iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso=" + Request.Form["txtMaHoSo"] + "&action=loadinforhoso';" + Environment.NewLine;
                        //    js += "</script>";
                        //    Page.RegisterStartupScript("DeleteFail", js);
                        //    return;
                        //}
                    }

                    if (pro.Delete(Library.Int32Convert(xuLySaiPhamId)))
                    {
                        string js = "<script type='text/javascript'>" + Environment.NewLine;
                        js += "jQuery('#thongbaothanhcong_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Đã xóa báo cáo tổng hợp kết quả kiểm tra.\").show();" + Environment.NewLine;
                        js += "</script>";
                        Page.RegisterStartupScript("DeleteSuccess", js);
                    }
                    else
                    {
                        string js = "<script type='text/javascript'>" + Environment.NewLine;
                        js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Không thể xóa báo cáo tổng hợp kết quả kiểm tra.\").show();" + Environment.NewLine;
                        js += "</script>";
                        Page.RegisterStartupScript("DeleteFail", js);
                    }
                }
                else
                {
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Hồ sơ này chưa được tổng hợp báo cáo hoặc không tìm thấy báo cáo tổng hợp.\").show();" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("DeleteFail", js);
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }

    private void Approve()
    {
        ChangeStatus(ListName.Status_DaPheDuyet);
    }

    private void Reject()
    {
        ChangeStatus(ListName.Status_KhongPheDuyet);
    }

    private void BackApprove()
    {
        ChangeStatus(ListName.Status_ThoaiDuyet);
    }

    private void ChangeStatus(string statusCode)
    {
        SqlKsclXuLySaiPhamProvider pro = new SqlKsclXuLySaiPhamProvider(ListName.ConnectionString, false, string.Empty);
        NameValueCollection nvc = Request.Form;

        if (!string.IsNullOrEmpty(nvc["hdDanhSachID"]))
        {
            try
            {
                _db.OpenConnection();
                string xuLySaiPhamId = "";
                string query = "SELECT XuLySaiPhamID FROM " + ListName.Table_KSCLXuLySaiPham +
                               " WHERE DSKiemTraTrucTiepID = " + nvc["hdDanhSachID"];
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                    xuLySaiPhamId = listData[0]["XuLySaiPhamID"].ToString();
                KsclXuLySaiPham obj = pro.GetByXuLySaiPhamId(Library.Int32Convert(xuLySaiPhamId));

                obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(statusCode, _db));
                if (pro.Update(obj))
                {
                    cm.ghilog(ListName.Func_KSCL_KetThuc_XuLySaiPham,
                              "Cập nhật trạng thái bản ghi có ID \"" + obj.XuLySaiPhamId + "\" thành " +
                              utility.GetStatusNameById(statusCode, _db) + " của danh mục " + tenchucnang);
                    string js = "<script type='text/javascript'>" + Environment.NewLine;
                    js += "CallAlertSuccessFloatRightBottom('Cập nhật trạng thái thành công.');" + Environment.NewLine;
                    js += "$('#txtMaDanhSach').val('" + nvc["txtMaDanhSach"] + "');" + Environment.NewLine;
                    js += "iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=loadttdanhsach&madanhsach=" + nvc["txtMaDanhSach"] + "';" + Environment.NewLine;
                    js += "</script>";
                    Page.RegisterStartupScript("ApproveSuccess", js);
                }
            }
            catch
            {
            }
            finally
            {
                _db.CloseConnection();
            }
        }
    }
}