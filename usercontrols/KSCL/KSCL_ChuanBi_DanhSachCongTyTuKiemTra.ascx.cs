﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_DanhSachCongTyTuKiemTra : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách các công ty kiểm toán phải nộp báo cáo tự kiểm tra";
    protected string _perOnFunc_DsCongTyNopBcTuKiemTra = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _perOnFunc_DsCongTyNopBcTuKiemTra = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, cm.connstr);
        if (_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Xem))
        {

            if (!IsPostBack || Request.Form["hdAction"] == "paging")
            {
                LoadDanhSachVungMien();
                LoadDanhSachLoaiHinhCongTy();
                LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();
            }

        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/19
    /// Load List danh sách công ty phải nộp báo cáo tự kiểm tra theo điều kiện tìm kiếm
    /// </summary>
    private void LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra()
    {
        try
        {
            _db.OpenConnection();
            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "ID", "MaDanhSach", "NgayLap", "MaTrangThai", "TenTrangThai", "SoCongTyPhaiNopBaoCao", "SoCongTyDaNopBaoCao", "SoCongTyDaDuocGuiEmail" });

            List<string> listParam = new List<string>{"@DuDKKT_CK", "@DuDKKT_Khac", "@MaHocVienTapThe", "@TenDoanhNghiep", "@LoaiHinhDoanhNghiepID",
                    "@VungMienID", "@DoanhThuTu", "@DoanhThuDen", "@NamHienTai", "@XepHang", "@NgayBatDau", "@NgayKetThuc", "@MaDanhSach", "@MaTrangThai_ChoDuyet", 
                    "@MaTrangThai_TuChoi", "@MaTrangThai_DaDuyet", "@NgayLapBatDau", "@NgayLapKetThuc"};
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DateTime.Now.Year, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value };
            // Dựa theo các tiêu chí tìm kiếm -> đưa giá trị tham số tương ứng vào store
            if (cboDuDieuKienKTCK.Checked)
                listValue[0] = 1;
            if (cboDuDieuKienKTKhac.Checked)
                listValue[1] = 1;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                listValue[2] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                listValue[3] = txtTenCongTy.Text;
            if (ddlLoaiHinh.SelectedValue != "")
                listValue[4] = ddlLoaiHinh.SelectedValue;
            if (ddlVungMien.SelectedValue != "")
                listValue[5] = ddlVungMien.SelectedValue;
            if (!string.IsNullOrEmpty(txtDoanhThuTu.Text) && Library.CheckIsDecimal(txtDoanhThuTu.Text))
                listValue[6] = txtDoanhThuTu.Text;
            if (!string.IsNullOrEmpty(txtDoanhThuDen.Text) && Library.CheckIsDecimal(txtDoanhThuDen.Text))
                listValue[7] = txtDoanhThuDen.Text;
            if (!cboTieuChi_Khac.Checked)
            {
                string xepHang = "";
                if (cboXepHang1.Checked)
                    xepHang += "1,";
                if (cboXepHang2.Checked)
                    xepHang += "2,";
                if (cboXepHang3.Checked)
                    xepHang += "3,";
                if (cboXepHang4.Checked)
                    xepHang += "4,";
                listValue[9] = xepHang;
            }
            if (!string.IsNullOrEmpty(txtTuNam.Text) && Library.CheckIsInt32(txtTuNam.Text))
                listValue[10] = txtTuNam.Text + "-1-1";
            if (!string.IsNullOrEmpty(txtDenNam.Text) && Library.CheckIsInt32(txtDenNam.Text))
                listValue[11] = txtDenNam.Text + "-12-31";
            if (!string.IsNullOrEmpty(txtMaDanhSach.Text))
                listValue[12] = txtMaDanhSach.Text;
            if (cboChoDuyet.Checked)
                listValue[13] = ListName.Status_ChoDuyet;
            if (cboTuChoi.Checked)
                listValue[14] = ListName.Status_KhongPheDuyet;
            if (cboDaDuyet.Checked)
                listValue[15] = ListName.Status_DaPheDuyet;
            if (!string.IsNullOrEmpty(txtNgayLapBatDau.Text) && Library.CheckDateTime(txtNgayLapBatDau.Text, "dd/MM/yyyy"))
                listValue[16] = Library.ConvertDatetime(txtNgayLapBatDau.Text, '/');
            if (!string.IsNullOrEmpty(txtNgayLapKetThuc.Text) && Library.CheckDateTime(txtNgayLapKetThuc.Text, "dd/MM/yyyy"))
                listValue[17] = Library.ConvertDatetime(txtNgayLapKetThuc.Text, '/');

            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra, listParam, listValue);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["DSBaoCaoTuKiemTraID"].ToString();
                    dr["MaDanhSach"] = ht["MaDanhSach"].ToString();
                    dr["NgayLap"] = Library.DateTimeConvert(ht["NgayLap"]);
                    string maTrangThai = ht["MaTrangThai"].ToString().Trim();
                    dr["MaTrangThai"] = maTrangThai;
                    dr["TenTrangThai"] = ht["TenTrangThai"].ToString();
                    if (maTrangThai == ListName.Status_DaPheDuyet)
                        dr["TenTrangThai"] = "<span style='color:green'>" + ht["TenTrangThai"] + "</span>";
                    if (maTrangThai == ListName.Status_KhongPheDuyet)
                        dr["TenTrangThai"] = "<span style='color:red'>" + ht["TenTrangThai"] + "</span>";
                    dr["SoCongTyPhaiNopBaoCao"] = ht["SoCongTyPhaiNopBaoCao"].ToString();
                    dr["SoCongTyDaDuocGuiEmail"] = ht["SoCongTyDaDuocGuiEmail"].ToString();
                    dr["SoCongTyDaNopBaoCao"] = ht["SoCongTyDaNopBaoCao"].ToString();
                    dt.Rows.Add(dr);
                }
            }
            DataView dv = dt.DefaultView;
            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"] + " " + ViewState["sortdirection"];

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            Pager.Items.Clear();
            for (int i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
                hf_TrangHienTai.Value = Request.Form["tranghientai"];
            if (!string.IsNullOrEmpty(hf_TrangHienTai.Value))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(hf_TrangHienTai.Value);
                Pager.SelectedIndex = Convert.ToInt32(hf_TrangHienTai.Value);
            }
            gv_DanhSachCongTyPhaiNopBaoCao.DataSource = objPds;
            gv_DanhSachCongTyPhaiNopBaoCao.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void gv_DanhSachCongTyPhaiNopBaoCao_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_DanhSachCongTyPhaiNopBaoCao.PageIndex = e.NewPageIndex;
        gv_DanhSachCongTyPhaiNopBaoCao.DataBind();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Them))
            Response.Write("$('#btnOpenFormInsert').remove();");
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Sua))
            Response.Write("$('a[data-original-title=\"Xem/Sửa\"]').remove();");
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Xoa))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
            Response.Write("$('#" + btnDelete.ClientID + "').remove();");
        }
        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_Duyet))
        {
            Response.Write("$('#" + btnApprove.ClientID + "').remove();");
            Response.Write("$('#" + btnReject.ClientID + "').remove();");
        }

        if (!_perOnFunc_DsCongTyNopBcTuKiemTra.Contains(ListName.PERMISSION_KetXuat))
            Response.Write("$('#btnExport').remove();");
    }

    protected void gv_DanhSachCongTyPhaiNopBaoCao_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hfMaTrangThai = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("hfMaTrangThai") as HiddenField;
            LinkButton lbtDelete = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("btnDeleteInList") as LinkButton;
            if (hfMaTrangThai != null && lbtDelete != null)
                if (hfMaTrangThai.Value == ListName.Status_DaPheDuyet)
                    lbtDelete.Visible = false;
        }
    }

    protected void gv_DanhSachCongTyPhaiNopBaoCao_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // Xóa từng bản ghi khi bấm nút "Xóa" trên danh sách
        if (e.CommandName == "delete_danhsach")
        {
            string idDanhSach = e.CommandArgument.ToString();
            DeleteDanhSach(new List<string> { idDanhSach });
        }
    }

    /// <summary>
    /// Xóa danh sách công ty phải nộp báo cáo tự kiểm tra
    /// </summary>
    /// <param name="listIdDanhSach">List ID danh sách</param>
    private void DeleteDanhSach(List<string> listIdDanhSach)
    {
        try
        {
            _db.OpenConnection();
            string maDanhSachKoXoaDuoc = "";
            string idTrangThai = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
            foreach (string idDanhSach in listIdDanhSach)
            {
                if (!string.IsNullOrEmpty(idDanhSach) && Library.CheckIsInt32(idDanhSach))
                {
                    SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                    // Kiểm tra tình trạng của bản ghi, nếu đã được duyệt thì không cho xóa
                    KscldsBaoCaoTuKiemTra obj = pro.GetByDsBaoCaoTuKiemTraId(Library.Int32Convert(idDanhSach));
                    if (obj != null && obj.TinhTrangId == Library.Int32Convert(idTrangThai))
                    {
                        maDanhSachKoXoaDuoc += obj.MaDanhSach + ", ";
                        continue;
                    }

                    // Xóa các bản ghi chi tiết danh sách trước
                    string deleteCommand = "DELETE FROM " + ListName.Table_KSCLDSBaoCaoTuKiemTraChiTiet + " WHERE DSBaoCaoTuKiemTraID = " + idDanhSach;
                    if (_db.ExecuteNonQuery(deleteCommand) > 0)
                    {
                        if (pro.Delete(Library.Int32Convert(idDanhSach)))
                        {
                            cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, "Xóa bản ghi có ID \"" + idDanhSach + "\" của danh mục " + tenchucnang);
                        }
                    }
                }
            }
            maDanhSachKoXoaDuoc = maDanhSachKoXoaDuoc.Trim().TrimEnd(',');
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã xóa những bản ghi dữ liệu hợp lệ.";
            if (!string.IsNullOrEmpty(maDanhSachKoXoaDuoc))
                msg += "<br /><span style=\"color:red;\">Không được phép xóa danh sách mã " + maDanhSachKoXoaDuoc + ".</span>";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("DeleteSuccess", js);
            LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Xóa những bản ghi được chọn khi bấm nút "Xóa đồng loạt" ở trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTyPhaiNopBaoCao.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTyPhaiNopBaoCao.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value.Split(new string[]{";#"}, StringSplitOptions.None)[0]);
        }
        DeleteDanhSach(listIdDanhSach);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Duyệt những bản ghi được chọn khi bấm nút "Duyệt đồng loạt" ở phía trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTyPhaiNopBaoCao.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTyPhaiNopBaoCao.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value.Split(new string[] { ";#" }, StringSplitOptions.None)[0]);
        }
        ChangeStatus(listIdDanhSach, ListName.Status_DaPheDuyet, "duyệt");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Từ chối những bản ghi được chọn khi bấm nút "Từ chối đồng loạt ở phía trên"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReject_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSachCongTyPhaiNopBaoCao.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSachCongTyPhaiNopBaoCao.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value.Split(new string[] { ";#" }, StringSplitOptions.None)[0]);
        }
        ChangeStatus(listIdDanhSach, ListName.Status_KhongPheDuyet, "từ chối");
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Cập nhật trạng thái của những bản ghi được chọn
    /// </summary>
    /// <param name="listIdDanhSach">List ID danh sách</param>
    /// <param name="statusCode">Mã trạng thái muốn cập nhật</param>
    /// <param name="action">Tên hành động (VD: duyệt, từ chối)</param>
    private void ChangeStatus(List<string> listIdDanhSach, string statusCode, string action)
    {
        try
        {
            _db.OpenConnection();
            string maDanhSachKoCapNhatDuoc = "";
            string idTrangThai = utility.GetStatusID(ListName.Status_DaPheDuyet, _db);
            foreach (string idDanhSach in listIdDanhSach)
            {
                if (!string.IsNullOrEmpty(idDanhSach))
                {
                    SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                    KscldsBaoCaoTuKiemTra obj = new KscldsBaoCaoTuKiemTra();
                    obj = pro.GetByDsBaoCaoTuKiemTraId(Library.Int32Convert(idDanhSach));
                    // Kiểm tra tình trạng của bản ghi, nếu đã được duyệt thì không cho xử lý nữa
                    if (obj != null && obj.TinhTrangId == Library.Int32Convert(idTrangThai))
                    {
                        maDanhSachKoCapNhatDuoc += obj.MaDanhSach + ", ";
                        continue;
                    }
                    obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(statusCode, _db));
                    if (pro.Update(obj))
                    {
                        cm.ghilog(ListName.Func_KSCL_ChuanBi_DSCongTyKTNopBCTuKiemTra, "Cập nhật trạng thái bản ghi có ID \"" + obj.DsBaoCaoTuKiemTraId + "\" thành " + utility.GetStatusNameById(statusCode, _db) + " của danh mục " + tenchucnang);
                    }
                }
            }
            maDanhSachKoCapNhatDuoc = maDanhSachKoCapNhatDuoc.Trim().TrimEnd(',');
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã " + action + " những bản ghi dữ liệu hợp lệ.";
            if (!string.IsNullOrEmpty(maDanhSachKoCapNhatDuoc))
                msg += "<br /><span style=\"color:red;\">Không được phép " + action + " danh sách mã " + maDanhSachKoCapNhatDuoc + ".</span>";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("ApproveSuccess", js);
            LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách vùng miền đổ vào Dropdownlist
    /// </summary>
    private void LoadDanhSachVungMien()
    {
        ddlVungMien.Items.Clear();
        ddlVungMien.Items.Add(new ListItem("Tất cả", ""));
        SqlDmVungMienProvider vungMienPro = new SqlDmVungMienProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmVungMien> listData = vungMienPro.GetAll();
        foreach (DmVungMien dmVungMien in listData)
        {
            ddlVungMien.Items.Add(new ListItem(dmVungMien.TenVungMien, dmVungMien.VungMienId.ToString()));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách loại hình công ty
    /// </summary>
    private void LoadDanhSachLoaiHinhCongTy()
    {
        ddlLoaiHinh.Items.Clear();
        ddlLoaiHinh.Items.Add(new ListItem("Tất cả", ""));
        SqlDmLoaiHinhDoanhNghiepProvider loaiHinhPro = new SqlDmLoaiHinhDoanhNghiepProvider(ListName.ConnectionString, false, string.Empty);
        TList<DmLoaiHinhDoanhNghiep> listData = loaiHinhPro.GetAll();
        foreach (DmLoaiHinhDoanhNghiep loaiHinh in listData)
        {
            ddlLoaiHinh.Items.Add(new ListItem(loaiHinh.TenLoaiHinhDoanhNghiep, loaiHinh.LoaiHinhDoanhNghiepId.ToString()));
        }
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();

        txtMaDanhSach.Text = "";
        txtNgayLapBatDau.Text = "";
        txtNgayLapKetThuc.Text = "";
        cboChoDuyet.Checked = true;
        cboTuChoi.Checked = true;
        cboDaDuyet.Checked = true;
        cboDuDieuKienKTCK.Checked = false;
        cboDuDieuKienKTKhac.Checked = false;
        cboXepHang1.Checked = false;
        cboXepHang2.Checked = false;
        cboXepHang3.Checked = false;
        cboXepHang4.Checked = false;
        cboTieuChi_Khac.Checked = false;
        txtMaCongTy.Text = "";
        if (ddlLoaiHinh.Items.Count > 0)
            ddlLoaiHinh.SelectedIndex = 0;
        txtDoanhThuTu.Text = "";
        txtDoanhThuDen.Text = "";
        txtTenCongTy.Text = "";
        if (ddlVungMien.Items.Count > 0)
            ddlVungMien.SelectedIndex = 0;
        txtTuNam.Text = "";
        txtDenNam.Text = "";
    }
    protected void gv_DanhSachCongTyPhaiNopBaoCao_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        LoadDanhSachCongTyPhaiNopBaoCaoTuKiemTra();
    }
}