﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_ThucHien_BaoCaoKetQuaKiemTraHeThong : System.Web.UI.UserControl
{
    protected string tenchucnang = "Báo cáo kết quả kiểm tra hệ thống";
    protected string _listPermissionOnFunc_BCKetQuaKiemTraHeThong = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    protected string _IDLopHoc = "";
    Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
            _listPermissionOnFunc_BCKetQuaKiemTraHeThong = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ThucHien_BCKetQuaKTHT, cm.connstr);
            if (_listPermissionOnFunc_BCKetQuaKiemTraHeThong.Contains("XEM|"))
            {
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "update")
                    Save();
                if (!string.IsNullOrEmpty(Request.Form["hdAction"]) && Request.Form["hdAction"] == "submit")
                    Submit();
            }
            else
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + " !</div>"));
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
    }

    protected void CheckPermissionOnPage()
    {
        string maHoSo = Library.CheckNull(Request.QueryString["mahoso"]);
        if (!string.IsNullOrEmpty(maHoSo))
        {
            Response.Write("$('#txtMaHoSo').val('" + maHoSo + "');");
            Response.Write("iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso=" + maHoSo + "&action=loadinforhoso';");
        }

        if (!_listPermissionOnFunc_BCKetQuaKiemTraHeThong.Contains("XEM|"))
            Response.Write("$('#" + Form1.ClientID + "').remove();");

        if (!_listPermissionOnFunc_BCKetQuaKiemTraHeThong.Contains("KETXUAT|"))
            Response.Write("$('#btnExport').remove();");

        if (!_listPermissionOnFunc_BCKetQuaKiemTraHeThong.Contains("THEM|"))
            Response.Write("$('#btnAdd').remove();");

        if (!_listPermissionOnFunc_BCKetQuaKiemTraHeThong.Contains("XOA|"))
            Response.Write("$('#btnDelete').remove();");
    }

    private void Save()
    {
        try
        {
            _db.OpenConnection();
            string js = "";
            SqlKsclBaoCaoKthtProvider pro = new SqlKsclBaoCaoKthtProvider(ListName.ConnectionString, false, string.Empty);
            SqlKsclBaoCaoKthtChiTietProvider proChiTiet = new SqlKsclBaoCaoKthtChiTietProvider(ListName.ConnectionString, false, string.Empty);
            NameValueCollection nvc = Request.Form;

            if (!string.IsNullOrEmpty(nvc["hdHoSoID"]))
            {
                // Xử lý dữ liệu ở bảng KSCLBaoCaoKTHT trước
                KsclBaoCaoKtht obj;
                string baoCaoKTHTId = "";
                string query = "SELECT BaoCaoKTHTID FROM " + ListName.Table_KSCLBaoCaoKTHT + " WHERE HoSoID = " + nvc["hdHoSoID"];
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                    baoCaoKTHTId = listData[0]["BaoCaoKTHTID"].ToString();
                if (string.IsNullOrEmpty(baoCaoKTHTId)) // insert
                {
                    obj = new KsclBaoCaoKtht();
                    obj.HoSoId = Library.Int32Convert(nvc["hdHoSoID"]);
                }
                else
                {
                    obj = pro.GetByBaoCaoKthtid(Library.Int32Convert(baoCaoKTHTId));
                    if (obj.TinhTrangId == Library.Int32Convert(utility.GetStatusID(ListName.Status_DaPheDuyet, _db)))
                    {
                        js = "<script type='text/javascript'>" + Environment.NewLine;
                        js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Báo cáo này đã được nộp. Bạn không được phép sửa nội dung của báo cáo.\").show();" + Environment.NewLine;
                        js += "</script>";
                        Page.RegisterStartupScript("SaveFail", js);
                        return;
                    }
                }
                obj.XepLoai = nvc["ddlXepHang"];
                obj.ThanhVienChamDiem1 = Library.Int32Convert(nvc["ddlThanhVienChamDiem1"]);
                obj.ThanhVienChamDiem2 = Library.Int32Convert(nvc["ddlThanhVienChamDiem2"]);
                obj.SlThanhVienBgd = Library.DecimalConvert(nvc["txtSoLuongThanhVienBGD"]);
                obj.SlktvHanhNghe = Library.DecimalConvert(nvc["txtSoLuongKTVHanhNghe"]);
                obj.SoLuongNvChuyenNghiep = nvc["txtSoLuongNhanVienChuyenNghiep"];
                obj.SlbcktPhatHanh = Library.DecimalConvert(nvc["txtSoLuongBCKT"]);
                if (nvc["cboFileCung"] == "on")
                    obj.HinhThucLuuTaiLieu = "1";
                if (nvc["cboFileMem"] == "on")
                    obj.HinhThucLuuTaiLieu = "2";
                if (nvc["cboFileCung"] == "on" && nvc["cboFileMem"] == "on")
                    obj.HinhThucLuuTaiLieu = "3";
                obj.PhanMemKt = nvc["txtPhanMemKeToan"];
                if (string.IsNullOrEmpty(baoCaoKTHTId)) // insert
                    pro.Insert(obj);
                else
                    pro.Update(obj);

                // Xóa hết dữ liệu ở bảng KSCLBaoCaoKTHTChiTiet theo ID của báo cáo KTHT
                string command = @"DELETE FROM " + ListName.Table_KSCLBaoCaoKTHTChiTiet + " WHERE BaoCaoKTHTID = " + obj.BaoCaoKthtid;
                _db.ExecuteNonQuery(command);

                // Insert dữ liệu KSCLBaoCaoKTHTChiTiet
                string[] arrKeys = nvc.AllKeys;
                for (int i = 0; i < arrKeys.Length; i++)
                {
                    string key = arrKeys[i];
                    if (key.StartsWith("rbtTraLoi_")) // Update vai trò
                    {
                        KsclBaoCaoKthtChiTiet objChiTiet = new KsclBaoCaoKthtChiTiet();
                        string traLoi = nvc[key];
                        string diem = nvc["txtDiemThucTe_" + key.Split('_')[1]];
                        string ghiChu = nvc["txtGhiChu_" + key.Split('_')[1]];
                        objChiTiet.CauHoiKthtid = Library.Int32Convert(nvc["hdCauHoiID_" + key.Split('_')[1]]);
                        objChiTiet.BaoCaoKthtid = obj.BaoCaoKthtid;
                        objChiTiet.TraLoi = traLoi;
                        objChiTiet.DiemThucTe = Library.DecimalConvert(diem.Replace('.', ','));
                        objChiTiet.GhiChu = ghiChu;
                        proChiTiet.Insert(objChiTiet);
                    }
                }

                js = "<script type='text/javascript'>" + Environment.NewLine;
                js += "CallAlertSuccessFloatRightBottom('Lưu báo cáo kết quả kiểm tra thành công.');" + Environment.NewLine;
                js += "$('#txtMaHoSo').val('" + nvc["txtMaHoSo"] + "');" + Environment.NewLine;
                js += "iframeProcess.location = '/iframe.aspx?page=KSCL_ThucHien_Process&mahoso=" + nvc["txtMaHoSo"] + "&action=loadinforhoso';" + Environment.NewLine;
                js += "</script>";
                Page.RegisterStartupScript("SaveSucess", js);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void Submit()
    {
        try
        {
            _db.OpenConnection();
            if (!string.IsNullOrEmpty(Request.Form["hdHoSoID"]))
            {
                string query = "SELECT BaoCaoKTHTID FROM " + ListName.Table_KSCLBaoCaoKTHT + " WHERE HoSoID = " + Request.Form["hdHoSoID"];
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    string baoCaoKTHTId = listData[0]["BaoCaoKTHTID"].ToString();
                    SqlKsclBaoCaoKthtProvider pro = new SqlKsclBaoCaoKthtProvider(ListName.ConnectionString, false, string.Empty);
                    KsclBaoCaoKtht obj = pro.GetByBaoCaoKthtid(Library.Int32Convert(baoCaoKTHTId));
                    if (obj != null && obj.BaoCaoKthtid > 0)
                    {
                        if (obj.TinhTrangId == Library.Int32Convert(utility.GetStatusID(ListName.Status_DaPheDuyet, _db)))
                        {
                            string js = "<script type='text/javascript'>" + Environment.NewLine;
                            js += "jQuery('#thongbaoloi_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Báo cáo này đã được nộp. Bạn không được nộp thêm lần nữa.\").show();" + Environment.NewLine;
                            js += "</script>";
                            Page.RegisterStartupScript("SubmitFail", js);
                        }
                        obj.TinhTrangId = Library.Int32Convert(utility.GetStatusID(ListName.Status_DaPheDuyet, _db));
                        if (pro.Update(obj))
                        {
                            string js = "<script type='text/javascript'>" + Environment.NewLine;
                            js += "jQuery('#thongbaothanhcong_form').html(\"<button class='close' type='button' data-dismiss='alert'>×</button>Đã nộp báo cáo kết quả kiểm tra hệ thống.\").show();" + Environment.NewLine;
                            js += "</script>";
                            Page.RegisterStartupScript("SubmitSuccess", js);
                        }
                    }
                }
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }

    }
}