﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ThucHien_BoCauHoiKiemTraKyThuat.ascx.cs" Inherits="usercontrols_KSCL_ThucHien_BoCauHoiKiemTraKyThuat" %>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<form id="Form1" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<h4 class="widgettitle">
    Bảng câu hỏi kiểm tra kỹ thuật</h4>
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<fieldset class="fsBlockInfor">
    <legend>Câu hỏi</legend>
    <table id="tblThongTinCauHoi" width="800px" border="0" class="formtbl">
        <tr>
            <td style="width: 150px;">
                Ngày lập câu hỏi<span class="starRequired">(*)</span>:
            </td>
            <td style="width: 80px;">
                <input type="text" id="txtNgayLapCauHoi" name="txtNgayLapCauHoi" />
                <asp:HiddenField ID="hdNgayHienTai" runat="server" />
            </td>
            <td style="width: 35px;">
                <img id="imgCalendarNgayLapCauHoi" src="/images/icons/calendar.png" />
            </td>
            <td style="width: 80px;">
                Nhóm câu hỏi<span class="starRequired">(*)</span>:
            </td>
            <td>
                <select id="ddlNhomCauHoi" name="ddlNhomCauHoi">
                    <% GetNhomCauHoi();%>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                Mã câu hỏi:
            </td>
            <td>
                <input type="text" name="txtMaCauHoi" id="txtMaCauHoi" readonly="readonly" />
                <input type="hidden" name="hdCauHoiID" id="hdCauHoiID" />
            </td>
            <td colspan="2">
            </td>
            <td>
                <input type="checkbox" id="cboCoHieuLuc" checked="checked" />&nbsp;Có hiệu lực
            </td>
        </tr>
        <tr>
            <td>
                Câu hỏi<span class="starRequired">(*)</span>:
            </td>
            <td colspan="4">
                <textarea rows="2" name="txtCauHoi" id="txtCauHoi"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                Điểm tối đa<span class="starRequired">(*)</span>:
            </td>
            <td>
                <input type="text" id="txtDiemToiDa" name="txtDiemToiDa" />
            </td>
            <td>
            </td>
            <td>
                Căn cứ:
            </td>
            <td>
                <input type="text" id="txtCanCu" name="txtCanCu" />
            </td>
        </tr>
        <tr>
            <td>
                Hướng dẫn chấm điểm:
            </td>
            <td colspan="4">
                <textarea rows="3" id="txtHuongDanChamDiem"></textarea>
            </td>
        </tr>
    </table>
</fieldset>
<div style="width: 100%; margin: 10px;">
    <a id="A1" href="javascript:;" class="btn btn-rounded" onclick="ResetControl();"><i
        class="iconfa-plus-sign"></i>Thêm</a>&nbsp; <a id="btnAdd" href="javascript:;" class="btn btn-rounded"
            onclick="Save();"><i class="iconfa-save"></i>Lưu</a>&nbsp; <a id="btnDelete" href="javascript:;"
                class="btn btn-rounded" onclick="DeleteCauHoi();"><i class="iconfa-trash">
                </i>Xóa</a>&nbsp;<a id="btnExport" href="/admin.aspx?page=KSCL_BaoCao_BangCauHoiKiemTraKyThuat" target="_blank" class="btn btn-rounded"><i class="iconfa-download">
                </i>Kết xuất bảng câu hỏi</a> <span style="margin-left: 100px; text-align: center; font-weight: bold;">
                    Tổng điểm toàn bảng:&nbsp;&nbsp;&nbsp;<input type="text" readonly="readonly" id="txtTongDiem"
                        style="width: 70px; font-weight: bold;" /></span>
</div>
<fieldset class="fsBlockInfor">
    <legend>Bảng câu hỏi kiểm tra kỹ thuật</legend>
    <div style="width: 100%; text-align: right;">
        <b>Chỉ hiển thị câu hỏi:</b>&nbsp;
        <input type="radio" id="rbtCoHieuLuc" value="1" checked="checked" name="FilterByHieuLuc" onchange="GetDanhSachCauHoi();"/>&nbsp;Có hiệu lực&nbsp;&nbsp;
        <input type="radio" id="rbtHetHieuLuc" value="2" name="FilterByHieuLuc" onchange="GetDanhSachCauHoi();"/>&nbsp;Hết hiệu lực
    </div>
    <table id="tblDanhSachCauHoi" width="100%" border="0" class="formtbl" style="margin-top: 10px;">
        <thead>
            <tr>
                <th>
                    <input type="checkbox" class="checkall" value="all" />
                </th>
                <th>
                    STT
                </th>
                <th>
                    Mã câu hỏi
                </th>
                <th style="min-width: 250px;">
                    Câu hỏi
                </th>
                <th style="width: 80px;">
                    Căn cứ
                </th>
                <th style="width: 50px;">
                    Điểm tối đa
                </th>
                <th style="min-width: 300px;">
                    Hướng dẫn chấm điểm
                </th>
            </tr>
        </thead>
        <tbody id="TBodyDanhSachCauHoi">
        </tbody>
    </table>
</fieldset>
<iframe name="iframe_Save" width="0px" height="0px" src="/iframe.aspx?page=KSCL_ThucHien_Process">
</iframe>
<iframe name="iframe_GetListData" width="0px" height="0px"></iframe>
</form>
<script type="text/javascript">
    $("#txtNgayLapCauHoi").datepicker({ dateFormat: 'dd/mm/yy' });
    $("#imgCalendarNgayLapCauHoi").click(function () {
        $("#txtNgayLapCauHoi").datepicker("show");
    });
    
    // Validate form
    $('#Form1').validate({
        rules: {
            txtCauHoi: {
                required: true
            },
            
            txtNgayLapCauHoi: {
                required: true, dateITA: true
            },
            txtDiemToiDa: {
                required: true, numberVN:true, numberPositive:true
            },
            ddlNhomCauHoi: {
                required: true
            },
                
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }  
    });

    function Save() {
        if($("#Form1").valid()){
            var cauHoiId = $('#hdCauHoiID').val();
            var ngayLap = $('#txtNgayLapCauHoi').val();
            var nhomCauHoi = $('#ddlNhomCauHoi option:selected').val();
            var coHieuLuc = '2';
            if($('#cboCoHieuLuc').prop('checked'))
                coHieuLuc = '1';
            var cauHoi = $('#txtCauHoi').val();
            var diemToiDa = $('#txtDiemToiDa').val();
            var canCu = $('#txtCanCu').val();
            var huongDanChamDiem = $('#txtHuongDanChamDiem').val();
            var arrData = [cauHoiId, ngayLap, nhomCauHoi, coHieuLuc, cauHoi, diemToiDa, canCu, huongDanChamDiem, '2'];
            window.iframe_Save.SaveCauHoi(arrData);
        }
    }
    
    function GetDanhSachCauHoi() {
        var tinhTrangHieuLuc = '1';
        if($('#rbtHetHieuLuc').prop('checked'))
            tinhTrangHieuLuc = '2';
        iframe_GetListData.location = '/iframe.aspx?page=KSCL_ThucHien_Process&action=getlistcauhoi&loai=2&hieuluc=' + tinhTrangHieuLuc;
    }
    
    function Draw_DanhSachCauHoi(arrData, arrDataDiemToiDa, tongSoDiem) {
        if(tongSoDiem != undefined)
            $('#txtTongDiem').val(tongSoDiem);
        var tbodyMain = document.getElementById('TBodyDanhSachCauHoi');
        var trs = tbodyMain.getElementsByTagName("tr");
        for (var i = 0; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrData.length > 0) {
            var tempNhomCauHoiCha = '';
            for(var i = 0; i < arrData.length; i ++) {
                var cauHoiId = arrData[i][0];
                var stt = arrData[i][1];
                var maCauHoi = arrData[i][2];
                var cauHoi = arrData[i][3];
                var canCu = arrData[i][4];
                var diemToiDa = arrData[i][5];
                var huongDanChamDiem = arrData[i][6];
                if(cauHoiId == '') {
                    for (var j = 0; j < arrDataDiemToiDa.length; j++) {
                        if(arrDataDiemToiDa[j][0] == arrData[i][10])
                            diemToiDa = arrDataDiemToiDa[j][1];
                    }
                }
                var tr = document.createElement('TR');
                if(tempNhomCauHoiCha != '' && cauHoiId != ''){
                    tr.className = 'class_' + tempNhomCauHoiCha;
                    tr.style.display = 'none';
                }
                if(cauHoiId == ''){
                    tr.style.fontWeight = 'bold';
                    tempNhomCauHoiCha = (i + 1);
                }
                var tdCheckBox = document.createElement("TD");
                tdCheckBox.style.textAlign = 'center';
                if(cauHoiId != ''){
                    var checkbox = document.createElement('input');
                    checkbox.type = "checkbox";
                    checkbox.value = i + ',' + cauHoiId;
                    checkbox.onclick = function() { GetOneCauHoi(arrData); };
                    tdCheckBox.appendChild(checkbox);
                } else {
                    var link = document.createElement('a');
                    link.href = 'javascript:;';
                    link.innerHTML = '[+]';
                    link.id = 'Display_HiddenData_' + (i + 1);
                    link.onclick = function() { AnHienDanhSachCauHoi(this); };
                    tdCheckBox.appendChild(link);
                }
                tr.appendChild(tdCheckBox); 

                var arrColumn = [stt, maCauHoi, cauHoi, canCu, diemToiDa, huongDanChamDiem];
                for(var j = 0; j < arrColumn.length; j ++){
                    var td = document.createElement("TD");
                    if(j == 0 || j == 4)
                        td.style.textAlign = 'center';
                    td.innerHTML = arrColumn[j];
                    tr.appendChild(td); 
                }
                tbodyMain.appendChild(tr);
            }
        }
    }
    
    function AnHienDanhSachCauHoi(obj) {
        var id = obj.id;
        var index = id.split('Display_HiddenData_')[1];
        if($('.class_' + index).css('display') != undefined){
            if($('.class_' + index).css('display') == 'none'){
                $('#Display_HiddenData_' + index).html('[-]');
                $('.class_' + index).css('display', 'table-row');
            }
            else{
                $('#Display_HiddenData_' + index).html('[+]');
                $('.class_' + index).css('display', 'none');
            }
        }
    }
    
    function GetOneCauHoi(arrData) {
        var countChecked = 0;
        var tempObj;
        $('#tblDanhSachCauHoi :checkbox').each(function () {
            var checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")){
                    countChecked++;
                    tempObj = this;
                }
            });
        if(countChecked == 1 && arrData != undefined) {
            var value = $(tempObj).val().split(',')[0];
            $('#hdCauHoiID').val(arrData[value][0]);
            $('#txtNgayLapCauHoi').val(arrData[value][7]);
            $('#txtMaCauHoi').val(arrData[value][2]);
            $('#ddlNhomCauHoi').val(arrData[value][8]);
            $('#txtCauHoi').val(arrData[value][3]);
            $('#txtDiemToiDa').val(arrData[value][5]);
            $('#txtHuongDanChamDiem').val(arrData[value][6]);
            $('#txtCanCu').val(arrData[value][4]);
            var tinhTrangHieuLuc = arrData[value][9];
            if(tinhTrangHieuLuc == 1)
                $('#cboCoHieuLuc').prop('checked', true);
            else
                $('#cboCoHieuLuc').prop('checked', false);
        } else {
            ResetControl();
        }
    }
    
    function ResetControl() {
        $('#hdCauHoiID').val('');
        $('#txtNgayLapCauHoi').val($('#<%= hdNgayHienTai.ClientID %>').val());
        $('#txtMaCauHoi').val('');
        $('#ddlNhomCauHoi').val($("#ddlNhomCauHoi option:first").val());
        $('#txtCauHoi').val('');
        $('#txtDiemToiDa').val('');
        $('#txtHuongDanChamDiem').val('');
        $('#txtCanCu').val('');
        $('#cboCoHieuLuc').prop('checked', true);
        jQuery("#thongbaoloi_form").hide();
    }
    
    function DeleteCauHoi() {
        if(!confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?'))
            return;
        var listId = '';
        $('#tblDanhSachCauHoi :checkbox').each(function () {
            var checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")){
                    listId += $(this).val().split(',')[1] + ',';
                }
            });
        if(listId.length > 0)
            iframe_GetListData.location = '/iframe.aspx?page=KSCL_ThucHien_Process&action=deletecauhoi&listid=' + listId;
        else
            alert('Phải chọn ít nhất một bản ghi để xóa!');
    }
    
    jQuery(document).ready(function () {
        $('#tblDanhSachCauHoi').stickyTableHeaders();
        // dynamic table      
        $("#tblDanhSachCauHoi .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#tblDanhSachCauHoi :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });
    
    <% CheckPermissionOnPage();%>
</script>