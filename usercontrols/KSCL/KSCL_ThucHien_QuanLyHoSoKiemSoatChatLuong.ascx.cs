﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ThucHien_QuanLyHoSoKiemSoatChatLuong : System.Web.UI.UserControl
{
    protected string tenchucnang = "Quản lý hồ sơ kiểm soát chất lượng";
    protected string _perOnFunc_DsHSKiemSoatChatLuong = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    private Commons cm = new Commons();
    private Db _db = new Db(ListName.ConnectionString);
    private Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

        _perOnFunc_DsHSKiemSoatChatLuong = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_KSCL_ThucHien_QuanLyHoSoKSCL, cm.connstr);
        if (_perOnFunc_DsHSKiemSoatChatLuong.Contains(ListName.PERMISSION_Xem))
        {

            if (!IsPostBack || Request.Form["hdAction"] == "paging")
            {
                txtMaCongTy.Text = Library.CheckNull(Request["macongty"]);
                LoadDanhSachHoSo();
            }

        }
        else
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Không có quyền xem dữ liệu " + tenchucnang + "!</div>"));
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/31
    /// Load List hồ sơ theo điều kiện tìm kiếm
    /// </summary>
    private void LoadDanhSachHoSo()
    {
        try
        {
            _db.OpenConnection();
            DataTable dt = new DataTable();
            Library.GenColums(dt, new string[] { "ID", "MaHoSo", "TenCongTy", "TenVietTat", "TuNgay", "DenNgay", "TrangThai", "SoFileTaiLieu" });

            List<string> listParam = new List<string> { "@NgayBatDauKiemTraTu", "@NgayBatDauKiemTraDen", "@MaDoanKiemTra", "@MaCongTy", "@TenCongTy", "@MaHoiVienCaNhan", 
                "@TenHoiVienCaNhan", "@MaThanhVien", "@TenThanhVien", "@SoChungChiKTV", "@NgayCapChungChiKTV" };
            List<object> listValue = new List<object> { DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value,
                DBNull.Value, DBNull.Value, DBNull.Value };

            if (!string.IsNullOrEmpty(txtThoiGianKiemTraTu.Text))
                listValue[0] = Library.DateTimeConvert(txtThoiGianKiemTraTu.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtThoiGianKiemTraDen.Text))
                listValue[1] = Library.DateTimeConvert(txtThoiGianKiemTraDen.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtMaDoanKiemTra.Text))
                listValue[2] = txtMaDoanKiemTra.Text;
            if (!string.IsNullOrEmpty(txtMaCongTy.Text))
                listValue[3] = txtMaCongTy.Text;
            if (!string.IsNullOrEmpty(txtTenCongTy.Text))
                listValue[4] = txtTenCongTy.Text;
            if (!string.IsNullOrEmpty(txtMaHVCN.Text))
                listValue[5] = txtMaHVCN.Text;
            if (!string.IsNullOrEmpty(txtTenKiemToanVien.Text))
                listValue[6] = txtTenKiemToanVien.Text;
            if (!string.IsNullOrEmpty(txtMaThanhVienDKT.Text))
                listValue[7] = txtMaThanhVienDKT.Text;
            if (!string.IsNullOrEmpty(txtTenThanhVienDKT.Text))
                listValue[8] = txtTenThanhVienDKT.Text;
            if (!string.IsNullOrEmpty(txtSoChungChiKTV.Text))
                listValue[9] = txtSoChungChiKTV.Text;
            if (!string.IsNullOrEmpty(txtNgayCap.Text))
                listValue[10] = Library.DateTimeConvert(txtNgayCap.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");

            List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SearchDanhSachHoSoKiemSoatChatLuong, listParam, listValue);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    dr["ID"] = ht["HoSoID"].ToString();
                    dr["MaHoSo"] = ht["MaHoSo"].ToString();
                    dr["TenCongTy"] = ht["TenDoanhNghiep"].ToString();
                    dr["TenVietTat"] = ht["TenVietTat"].ToString();
                    DateTime tuNgay = Library.DateTimeConvert(ht["TuNgayThucTe"]);
                    DateTime denNgay = Library.DateTimeConvert(ht["DenNgayThucTe"]);
                    dr["TuNgay"] = !string.IsNullOrEmpty(ht["TuNgayThucTe"].ToString()) ? tuNgay.ToString("dd/MM/yyyy") : "";
                    dr["DenNgay"] = !string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()) ? denNgay.ToString("dd/MM/yyyy") : "";
                    if (string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()) || denNgay.AddDays(1) > DateTime.Now)
                        dr["TrangThai"] = "Đang thực hiện";
                    if (!string.IsNullOrEmpty(ht["DenNgayThucTe"].ToString()) && denNgay.AddDays(1) < DateTime.Now)
                        dr["TrangThai"] = "Đã hoàn thành";
                    dr["SoFileTaiLieu"] = ht["SoFileTaiLieu"];
                    dt.Rows.Add(dr);
                }
            }
            DataView dv = dt.DefaultView;
            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"] + " " + ViewState["sortdirection"];
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            Pager.Items.Clear();
            for (int i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
                hf_TrangHienTai.Value = Request.Form["tranghientai"];
            if (!string.IsNullOrEmpty(hf_TrangHienTai.Value))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(hf_TrangHienTai.Value);
                Pager.SelectedIndex = Convert.ToInt32(hf_TrangHienTai.Value);
            }
            gv_DanhSach.DataSource = objPds;
            gv_DanhSach.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void gv_DanhSach_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_DanhSach.PageIndex = e.NewPageIndex;
        gv_DanhSach.DataBind();
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Kiểm tra quyền trên trang đối với các chức năng
    /// </summary>
    protected void CheckPermissionOnPage()
    {
        //Response.Write("$('#" + txtThoiGianKiemTraTu.ClientID + "').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');");
        //Response.Write("$('#" + txtThoiGianKiemTraDen.ClientID + "').val('" + DateTime.Now.ToString("dd/MM/yyyy") + "');");

        if (!_perOnFunc_DsHSKiemSoatChatLuong.Contains(ListName.PERMISSION_Xem))
            Response.Write("$('#" + Form1.ClientID + "').remove();");
        if (!_perOnFunc_DsHSKiemSoatChatLuong.Contains(ListName.PERMISSION_Sua))
            Response.Write("$('a[data-original-title=\"Xem/Sửa\"]').remove();");
        if (!_perOnFunc_DsHSKiemSoatChatLuong.Contains(ListName.PERMISSION_Xoa))
        {
            //Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }
    }

    protected void gv_DanhSach_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hf_SoFileTaiLieu = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("hf_SoFileTaiLieu") as HiddenField;
            LinkButton lbtDelete = e.Row.Cells[e.Row.Cells.Count - 1].FindControl("btnDeleteInList") as LinkButton;
            if (hf_SoFileTaiLieu != null && lbtDelete != null)
                if (Library.Int32Convert(hf_SoFileTaiLieu.Value) > 0)
                    lbtDelete.Visible = false;
        }
    }

    protected void gv_DanhSach_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // Xóa từng bản ghi khi bấm nút "Xóa" trên danh sách
        if (e.CommandName == "deleteHoSo")
        {
            string idDanhSach = e.CommandArgument.ToString();
            DeleteHoSo(new List<string> { idDanhSach });
        }
    }

    /// <summary>
    /// Xóa hồ sơ
    /// </summary>
    /// <param name="listId">List ID hồ sơ</param>
    private void DeleteHoSo(List<string> listId)
    {
        try
        {
            _db.OpenConnection();
            string maHoSoKoXoaDuoc = "";
            foreach (string id in listId)
            {
                if (!string.IsNullOrEmpty(id) && Library.CheckIsInt32(id))
                {
                    SqlKsclHoSoProvider pro = new SqlKsclHoSoProvider(ListName.ConnectionString, false, string.Empty);
                    // Kiểm tra nếu hồ sơ có file đính kèm thì ko được xóa
                    string query = @"SELECT HoSoFileID, HS.MaHoSo FROM " + ListName.Table_KSCLHoSoFile + @" HSF LEFT JOIN " + ListName.Table_KSCLHoSo + @" HS
                                         ON HSF.HoSoID = HS.HoSoID WHERE HSF.HoSoID = " + id;
                    List<Hashtable> listData = _db.GetListData(query);
                    if (listData.Count > 0)
                    {
                        maHoSoKoXoaDuoc += listData[0]["MaHoSo"] + ", ";
                        continue;
                    }

                    if (pro.Delete(Library.Int32Convert(id)))
                    {
                        cm.ghilog(ListName.Func_KSCL_ThucHien_QuanLyHoSoKSCL, "Xóa bản ghi có ID \"" + id + "\" của danh mục " + tenchucnang);
                    }
                }
            }
            maHoSoKoXoaDuoc = maHoSoKoXoaDuoc.Trim().TrimEnd(',');
            string js = "<script type='text/javascript'>" + Environment.NewLine;
            string msg = "Đã xóa những bản ghi dữ liệu hợp lệ.";
            if (!string.IsNullOrEmpty(maHoSoKoXoaDuoc))
                msg += "<br /><span style=\"color:red;\">Không được phép xóa hồ sơ mã " + maHoSoKoXoaDuoc + ".</span>";
            js += "CallAlertSuccessFloatRightBottom('" + msg + "');" + Environment.NewLine;
            js += "</script>";
            Page.RegisterStartupScript("DeleteSuccess", js);
            LoadDanhSachHoSo();
        }
        catch
        {
            _db.CloseConnection();
        }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/18
    /// Xóa những bản ghi được chọn khi bấm nút "Xóa đồng loạt" ở trên
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        // Lấy danh sách các bản ghi được chọn
        List<string> listIdDanhSach = new List<string>();
        for (int i = 0; i < gv_DanhSach.Rows.Count; i++)
        {
            HtmlInputCheckBox checkbox = gv_DanhSach.Rows[i].Cells[0].FindControl("checkbox") as HtmlInputCheckBox;
            if (checkbox != null && checkbox.Checked)
                listIdDanhSach.Add(checkbox.Value);
        }
        DeleteHoSo(listIdDanhSach);
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadDanhSachHoSo();

        txtMaDoanKiemTra.Text = "";
        txtMaCongTy.Text = "";
        txtTenCongTy.Text = "";
        txtThoiGianKiemTraTu.Text = "";
        txtThoiGianKiemTraDen.Text = "";
        txtMaHVCN.Text = "";
        txtTenKiemToanVien.Text = "";
        txtMaThanhVienDKT.Text = "";
        txtTenThanhVienDKT.Text = "";
        txtSoChungChiKTV.Text = "";
        txtNgayCap.Text = "";
    }
    protected void gv_DanhSach_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        LoadDanhSachHoSo();
    }
}