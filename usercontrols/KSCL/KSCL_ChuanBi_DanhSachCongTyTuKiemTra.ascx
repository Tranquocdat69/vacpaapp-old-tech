﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_DanhSachCongTyTuKiemTra.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_DanhSachCongTyTuKiemTra" %>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .ui-datepicker
    {
        z-index: 1003 !important;
    }
</style>
<form id="Form1" ClientIDMode="Static" name="Form1" runat="server">
<h4 class="widgettitle">
    Danh sách công ty kiểm toán nộp báo cáo tự kiểm tra</h4>
<div class="dataTables_length">
    <a id="btnOpenFormInsert" href="/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra_lapdanhsach"
        class="btn btn-rounded"><i class="iconfa-plus-sign"></i>Thêm mới</a>
    <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn" OnClick="btnDelete_Click"
        OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-trash"></i>Xóa</asp:LinkButton>
    <asp:LinkButton ID="btnApprove" runat="server" CssClass="btn" OnClick="btnApprove_Click"
        OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-thumbs-up"></i>Duyệt</asp:LinkButton>
    <asp:LinkButton ID="btnReject" runat="server" CssClass="btn" OnClick="btnReject_Click" OnClientClick="if(confirm('Bạn chắc chắn muốn thực hiện thao tác này chứ?')){ return CheckHasChooseItem();} else {return false;}"><i class="iconfa-remove-sign"></i>Từ chối</asp:LinkButton>
    <a id="btnSendEmail" href="javascript:;" class="btn btn-rounded" onclick="if(CheckHasChooseItem()) OpenDanhSachGuiEmail();">
        <i class="iconfa-inbox"></i>Gửi email</a> <a id="btnExport" href="javascript:;" class="btn btn-rounded" onclick="CallReportPage();">
            <i class="iconfa-save"></i>Kết xuất</a> <a href="javascript:;" id="btn_search" class="btn btn-rounded"
                onclick="OpenFormSearch();"><i class="iconfa-search"></i>Tìm kiếm</a>
</div>
<div>
    <fieldset class="fsBlockInfor">
        <legend>Danh sách công ty kiểm toán phải nộp báo cáo tự kiểm tra</legend>
        <asp:GridView ClientIDMode="Static" ID="gv_DanhSachCongTyPhaiNopBaoCao" runat="server"
            AutoGenerateColumns="False" class="table table-bordered responsive dyntable"
            AllowPaging="False" AllowSorting="True" OnPageIndexChanging="gv_DanhSachCongTyPhaiNopBaoCao_PageIndexChanging"
            OnRowCommand="gv_DanhSachCongTyPhaiNopBaoCao_RowCommand" 
            OnRowDataBound="gv_DanhSachCongTyPhaiNopBaoCao_RowDataBound" 
            onsorting="gv_DanhSachCongTyPhaiNopBaoCao_Sorting">
            <Columns>
                <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />'
                    ItemStyle-Width="30px">
                    <ItemTemplate>
                        <input type="checkbox" id="checkbox" runat="server" class="colcheckbox" value='<%# Eval("ID") + ";#" + Eval("MaDanhSach")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mã danh sách" SortExpression="MaDanhSach">
                    <ItemTemplate>
                        <%# Eval("MaDanhSach")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày lập danh sách" SortExpression="NgayLap">
                    <ItemTemplate>
                        <%# Library.DateTimeConvert(Eval("NgayLap")).ToString("dd/MM/yyyy")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số lượng công ty phải nộp báo cáo" SortExpression="SoCongTyPhaiNopBaoCao">
                    <ItemTemplate>
                        <%# Eval("SoCongTyPhaiNopBaoCao")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số công ty gửi báo cáo)" SortExpression="SoCongTyDaNopBaoCao">
                    <ItemTemplate>
                        <%# Eval("SoCongTyDaNopBaoCao") + "/" + Eval("SoCongTyPhaiNopBaoCao")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tình trạng bản ghi" SortExpression="TenTrangThai">
                    <ItemTemplate>
                        <%# Eval("TenTrangThai") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tình trạng gửi Email" SortExpression="SoCongTyDaDuocGuiEmail">
                    <ItemTemplate>
                        <%# Eval("SoCongTyDaDuocGuiEmail") + "/" + Eval("SoCongTyPhaiNopBaoCao")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Thao tác" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <div class="btn-group">
                            <a href='/admin.aspx?page=kscl_chuanbi_danhsachcongtytukiemtra_lapdanhsach&iddanhsach=<%# Eval("ID") %>'
                                data-placement="top" data-rel="tooltip" data-original-title="Xem/Sửa" rel="tooltip"
                                class="btn"><i class="iconsweets-create"></i></a>
                            <asp:LinkButton ID="btnDeleteInList" runat="server" CommandArgument='<%# Eval("ID") %>'
                                CommandName="delete_danhsach" data-placement="top" data-rel="tooltip" data-original-title="Xóa"
                                rel="tooltip" class="btn" OnClientClick="return confirm('Bạn chắc chắn muốn thực hiện thao tác này không?');"><i class="iconsweets-trashcan"></i></asp:LinkButton>
                            <asp:HiddenField ID="hfMaTrangThai" runat="server" Value='<%# Eval("MaTrangThai") %>' />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hf_TrangHienTai" runat="server" />
        <div class="dataTables_info" id="dyntable_info">
    <div class="pagination pagination-mini">
        Chuyển đến trang:
        <ul>
            <li><a href="javascript:;" onclick="$('#tranghientai').val(0);$('#hdAction').val('paging'); $('#user_search').submit();"><<
                Đầu tiên</a></li>
            <li><a href="javascript:;" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager option:selected').val()-1); $('#hdAction').val('paging'); $('#user_search').submit();}">
                < Trước</a>
            </li>
            <li><a style="border: none; background-color: #eeeeee">
                <asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" Style="width: 55px;
                    height: 22px;" onchange="$('#tranghientai').val(this.value);$('#hdAction').val('paging'); $('#user_search').submit();">
                </asp:DropDownList>
            </a></li>
            <li style="margin-left: 5px;"><a href="javascript:;" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) { $('#tranghientai').val(parseInt($('#Pager option:selected').val())+1); $('#hdAction').val('paging'); $('#user_search').submit();} ;"
                style="border-left: 1px solid #ccc;">Sau ></a></li>
            <li><a href="javascript:;" onclick="$('#tranghientai').val($('#Pager option').length-1);$('#hdAction').val('paging'); $('#user_search').submit();">
                Cuối cùng >></a></li>
        </ul>
    </div>
</div>
    </fieldset>
</div>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<script type="text/javascript">
    // Hiển thị tooltip
    if (jQuery('#gv_DanhSachCongTyPhaiNopBaoCao').length > 0) jQuery('#gv_DanhSachCongTyPhaiNopBaoCao').tooltip({ selector: "a[rel=tooltip]" });

    jQuery(document).ready(function () {
        // dynamic table      

        $("#gv_DanhSachCongTyPhaiNopBaoCao .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#gv_DanhSachCongTyPhaiNopBaoCao :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });

    });
    // Created by NGUYEN MANH HUNG - 2014/12/12
    // Hàm mở Dialog tìm kiếm
    function OpenFormSearch() {
        $("#DivSearch").dialog({
            resizable: true,
            width: 800,
            height: 420,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm danh sách công ty kiểm toán phải nộp báo cáo tự kiểm tra</b>",
            modal: true,
            zIndex: 1000
        });
        $("#DivSearch").parent().appendTo($("#Form1"));
    }

    function CheckHasChooseItem() {
        var flag = false;
        $('#gv_DanhSachCongTyPhaiNopBaoCao :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all') {
                flag = true;
                return;
            }
        });
        if (!flag)
            alert('Phải chọn ít nhất một bản ghi để thực hiện thao tác!');
        return flag;
    }

    // Created by NGUYEN MANH HUNG - 2014/12/18
    // Mở Form xem danh sách các công ty cần gửi Email
    function OpenDanhSachGuiEmail() {
        var listIdDanhSach = '';
        // Lấy List ID danh sách cần gửi Email
        $('#gv_DanhSachCongTyPhaiNopBaoCao :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all')
                listIdDanhSach += $(this).val().split(';#')[0] + ',';
        });

        $("#DivDanhSachGuiEmail").empty();
        $("#DivDanhSachGuiEmail").append($("<iframe width='100%' height='100%' id='ifDanhSachGuiEmail' name='ifDanhSachGuiEmail' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_ChuanBi_XacNhanDanhSachGuiEmail&iddanhsach=" + listIdDanhSach));
        $("#DivDanhSachGuiEmail").dialog({
            resizable: true,
            width: 900,
            height: 700,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Gửi email đến công ty nộp báo cáo tự kiểm tra</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Gửi Email": function () {
                    window.ifDanhSachGuiEmail.ChooseAndSendEmail();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivDanhSachGuiEmail').parent().find('button:contains("Gửi Email")').addClass('btn btn-rounded').prepend('<i class="iconfa-share"> </i>&nbsp;');
        $('#DivDanhSachGuiEmail').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function CloseDanhSachGuiEmail() {
        $("#DivDanhSachGuiEmail").dialog('close');
    }

    // Created by NGUYEN MANH HUNG - 2014/12/17
    // Gửi email
    function SendEmail(listIdCompanyNeedAdditional) {
        var listIdDanhSach = '';
        // Lấy List ID danh sách cần gửi Email
        $('#gv_DanhSachCongTyPhaiNopBaoCao :checkbox').each(function () {
            if ($(this).prop("checked") && $(this).val() != 'all')
                listIdDanhSach += $(this).val().split(';#')[0] + ',';
        });

        CloseDanhSachGuiEmail();
        $('#btnSendEmail').html('<img src="/images/loaders/loader1.gif" />Hệ thống đang gửi Email. Hãy chờ trong giây lát.');
        iframeProcess.location = '/iframe.aspx?page=KSCL_ChuanBi_Process&iddanhsach=' + listIdDanhSach + '&listcompany=' + listIdCompanyNeedAdditional + '&action=sendmail&type=bctukiemtra';
    }

    function SendEmail_Success(success, listCompanyFail) {
        $('#btnSendEmail').html('<i class="iconfa-inbox"></i>Gửi email');
        var arrCompanyFail = listCompanyFail.split(';#');
        var html = "";
        if (listCompanyFail.length > 0)
            html = "<br />Những công ty dưới đây chưa đăng ký địa chỉ Email hoặc có sự cố khi gửi: <br />";
        for (var i = 0; i < arrCompanyFail.length; i++) {
            if (arrCompanyFail[i].length > 0)
                html += "- " + arrCompanyFail[i] + "<br />";
        }
        if (success == 1) {
            CallAlertSuccessFloatRightBottom('Đã gửi Email tới các đơn vị trong danh sách.' + html);
        } else {
            CallAlertErrorFloatRightBottom('Có sự cố trong quá trình gửi Email.' + html);
        }
    }
</script>
<div id="DivSearch" style="display: none;">
    <fieldset class="fsBlockInfor">
        <legend>Thông tin tìm kiếm</legend>
        <table width="100%" border="0">
            <tr>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Mã danh sách:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMaDanhSach" runat="server" ViewStateMode="Disabled"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ngày lập danh sách:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNgayLapBatDau" runat="server" Width="100px" CssClass="dateITA" ViewStateMode="Disabled"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtNgayLapKetThuc" runat="server" Width="100px" CssClass="dateITA" ViewStateMode="Disabled"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Tình trạng:
                            </td>
                            <td>
                                <asp:CheckBox ID="cboChoDuyet" runat="server" Checked="True" />Chờ duyệt
                                <asp:CheckBox ID="cboTuChoi" runat="server" Checked="True" />Từ chối
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:CheckBox ID="cboDaDuyet" runat="server" Checked="True" />Đã duyệt
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset class="fsBlockInforMini" style="width: 95%; height: 100px;">
                        <legend>Tiêu chí công ty đủ điều kiện kiểm toán cho đơn vị có lợi ích công chúng</legend>
                        <asp:CheckBox ID="cboDuDieuKienKTCK" runat="server" />Trong lĩnh vực chứng khoán<br />
                        <asp:CheckBox ID="cboDuDieuKienKTKhac" runat="server" />Đơn vị có lợi ích công chúng
                        khác
                    </fieldset>
                </td>
                <td>
                    <fieldset class="fsBlockInforMini" style="width: 95%; height: 100px;">
                        <legend>Tiêu chí công ty phải nộp báo cáo tự kiểm tra</legend>
                        <table style="width: 100%;">
                        <tr>
                            <td>
                                Xếp loại năm trước:
                            </td>
                            <td>
                                <asp:CheckBox ID="cboXepHang1" runat="server" />Tốt&nbsp;<asp:CheckBox ID="cboXepHang2"
                                    runat="server" />Đạt yêu cầu<br />
                                <asp:CheckBox ID="cboXepHang3" runat="server" />Không đạt yêu cầu&nbsp;<asp:CheckBox
                                    ID="cboXepHang4" runat="server" />Yếu kém<br />
                            </td>
                        </tr>
                    </table>
                        <asp:CheckBox ID="cboTieuChi_Khac" runat="server" />Các tiêu chí khác
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Mã HVTC/CTKT:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMaCongTy" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Loại hình công ty:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlLoaiHinh" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Doanh thu:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDoanhThuTu" runat="server" Width="100px" onchange="CheckIsNumber(this);"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtDoanhThuDen" runat="server" Width="100px" onchange="CheckIsNumber(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 49%;">
                    <table width="95%" border="0" class="formtblInforWithoutBorder">
                        <tr>
                            <td>
                                Tên công ty kiểm toán:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTenCongTy" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Vùng miền:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlVungMien" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Số năm chưa được kiểm tra:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTuNam" runat="server" Width="60px" onchange="CheckIsInteger(this);"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtDenNam" runat="server" Width="60px" onchange="CheckIsInteger(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </fieldset>
    <div style="width: 100%; margin-top: 10px; text-align: right;">
        <asp:LinkButton ID="lbtTruyVan" runat="server" CssClass="btn" OnClick="lbtTruyVan_Click"
            OnClientClick="return CheckValidFormSearch();"><i class="iconfa-search"></i>Tìm</asp:LinkButton>
        <a href="javascript:;" id="A4" class="btn btn-rounded" onclick="$('#DivSearch').dialog('close');">
            <i class="iconfa-off"></i>Bỏ qua</a>
    </div>
</div>
<div id="DivDanhSachGuiEmail">
</div>
<script type="text/javascript">
    $('#<%= txtNgayLapBatDau.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $('#<%= txtNgayLapKetThuc.ClientID %>').datepicker({ dateFormat: 'dd/mm/yy' });
    $(document).ready(function() {
        $("#Form1").validate({
            onsubmit: false
        });
    });

    function CheckValidFormSearch() {
        var isValid = $("#Form1").valid();
        if(isValid) {
            $('#DivSearch').dialog('close');
            return true;
        } else {
            return false;
        }
    }
    
    $("#DivSearch").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtTruyVan.ClientID %>').attr('href'));
        }
    });
    
    function CallReportPage() {
        var arr = [];
        $('#gv_DanhSachCongTyPhaiNopBaoCao :checkbox').each(function () {
                var checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) {
                    arr.push($(this).val().split(';#')[1]);
                }
            });

        if(arr.length > 0) {
            if(arr.length > 1) {
                alert('Chỉ được chọn 1 danh sách để kết xuất báo cáo!');
            } else {
                    var url = "/admin.aspx?page=KSCL_BaoCao_DanhSachCongTyKTPhaiNopBaoCaoTuKiemTra&mads=" + arr[0];
                    window.open(url, '_blank');
                    return;
            }
        } else {
            alert('Phải chọn 1 danh sách để kết xuất báo cáo!');
        }
    }

    <% CheckPermissionOnPage();%>
</script>
</form>
<form id="user_search" method="post" enctype="multipart/form-data">
    <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
    <input type="hidden" value="0" id="hdAction" name="hdAction" />
</form>