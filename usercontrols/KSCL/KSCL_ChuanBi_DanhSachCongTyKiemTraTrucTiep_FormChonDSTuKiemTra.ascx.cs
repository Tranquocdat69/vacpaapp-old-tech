﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_KSCL_ChuanBi_DanhSachCongTyKiemTraTrucTiep_FormChonDSTuKiemTra : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    private string _idDanhSach = "", _type;
    protected void Page_Load(object sender, EventArgs e)
    {
        _type = Library.CheckNull(Request.QueryString["type"]);
        if (!IsPostBack)
        {
            LoadData(_type);
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Load danh sách công ty theo điều kiện tìm kiếm
    /// </summary>
    /// <param name="type"></param>
    private void LoadData(string type)
    {
        DataTable dt = new DataTable();
        Library.GenColums(dt, new string[] { "ID", "MaDanhSach", "NgayLap", "SoCongTy" });
        try
        {
            _db.OpenConnection();
            string procName = "";
            if (type == "dstukiemtra")
                procName = ListName.Proc_KSCL_SearchDanhSachYeuCauNopBaoCaoTuKiemTra_Mini1;
            if (type == "dskiemtravuviec")
                procName = ListName.Proc_KSCL_SearchDanhSachCongTyKTTheoYCKTVuViec_Mini1;
            List<string> listParams = new List<string> { "@MaDanhSach", "@NgayLap" };
            List<object> listValues = new List<object> { DBNull.Value, DBNull.Value };

            if (!string.IsNullOrEmpty(txtMaDanhSach.Text))
                listValues[0] = txtMaDanhSach.Text;
            if (!string.IsNullOrEmpty(txtNgayLap.Text))
                listValues[1] = Library.DateTimeConvert(txtNgayLap.Text, "dd/MM/yyyy").ToString("yyyy-MM-dd");

            List<Hashtable> listData = _db.GetListData(procName, listParams, listValues);
            if (listData.Count > 0)
            {
                DataRow dr;
                foreach (Hashtable ht in listData)
                {
                    dr = dt.NewRow();
                    if (type == "dstukiemtra")
                        dr["ID"] = ht["DSBaoCaoTuKiemTraID"];
                    if (type == "dskiemtravuviec")
                        dr["ID"] = ht["DSYeuCauKiemTraVuViecID"];
                    dr["MaDanhSach"] = ht["MaDanhSach"];
                    dr["NgayLap"] = !string.IsNullOrEmpty(ht["NgayLap"].ToString())
                                        ? Library.DateTimeConvert(ht["NgayLap"]).ToString("dd/MM/yyyy")
                                        : "";
                    dr["SoCongTy"] = ht["SoCongTy"];
                    dt.Rows.Add(dr);
                }
            }
            rpDanhSach.DataSource = dt.DefaultView;
            rpDanhSach.DataBind();
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    protected void lbtTruyVan_Click(object sender, EventArgs e)
    {
        LoadData(_type);
    }
}