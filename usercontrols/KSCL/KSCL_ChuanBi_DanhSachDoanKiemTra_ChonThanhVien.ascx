﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_ChuanBi_DanhSachDoanKiemTra_ChonThanhVien.ascx.cs" Inherits="usercontrols_KSCL_ChuanBi_DanhSachDoanKiemTra_ChonThanhVien" %>

<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<form id="Form1" name="Form1" runat="server">
<fieldset class="fsBlockInfor">
    <legend>Thông tin tìm kiếm</legend>
    <table style="min-width: 700px;" border="0" class="formtblInforWithoutBorder">
        <tr>
            <td>
                Đơn vị công tác:
            </td>
            <td>
                <asp:TextBox ID="txtDonViCongTac" runat="server"></asp:TextBox>
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td>
                Mã thành viên:
            </td>
            <td>
                <asp:TextBox ID="txtMaThanhVien" runat="server" Width="100px"></asp:TextBox>
            </td>
            <td>
                Tên thành viên:
            </td>
            <td>
                <asp:TextBox ID="txtTenThanhVien" runat="server"></asp:TextBox>
            </td>
        </tr>
        <%--<tr>
            <td>
                ID.HVCN:
            </td>
            <td>
                <asp:TextBox ID="txtMaHoiVienCaNhan" runat="server" Width="100px"></asp:TextBox>
                <input type="button" id="Button2" value="---" onclick="OpenFormChonHVCN();" style="border: 1px solid gray;" />
            </td>
            <td>
                Tên hội viên cá nhân:
            </td>
            <td>
                <asp:TextBox ID="txtTenHoiVienCaNhan" runat="server"></asp:TextBox>
            </td>
        </tr>--%>
    </table>
</fieldset>
<div style="margin-top: 10px; text-align: center; width: 100%;">
    <asp:LinkButton ID="lbtTruyVanPopup" runat="server" CssClass="btn" OnClick="lbtTruyVan_Click"><i class="iconfa-search"></i>Tìm kiếm</asp:LinkButton>
</div>
<fieldset class="fsBlockInfor">
    <legend>Danh sách thành viên đoàn kiểm tra</legend>
    <table id="tblDanhSachThanhVien" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th style="width: 30px;">
                    <input type="checkbox" class="checkall" value="all" />
                </th>
                <th style="width: 30px;">
                    STT
                </th>
                <th style="min-width: 120px;">
                    Tên thành viên
                </th>
                <th style="max-width: 30px;">
                    Năm sinh
                </th>
                <th style="width: 100px;">
                    Chức vụ
                </th>
                <th style="width: 100px;">
                    Đơn vị công tác
                </th>
                <th style="width: 50px;">
                    Số chứng chỉ kiểm toán viên
                </th>
                <th style="width: 80px;">
                    Số điện thoại
                </th>
                <th style="min-width: 100px;">
                    Email
                </th>
                <th style="width: 30px;">
                    HVCN
                </th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rpDanhSachThanhVien" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="vertical-align: middle;">
                            <input type="checkbox" value='<%# Eval("ID") + "," + Eval("HVCN") %>' />
                        </td>
                        <td>
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td>
                            <%# Eval("TenThanhVien") %>
                        </td>
                        <td>
                            <%# Eval("NamSinh") %>
                        </td>
                        <td><%# Eval("TenChucVu") %>
                        </td>
                        <td><%# Eval("DonViCongTac") %>
                        </td>
                        <td><%# Eval("SoChungChiKTV")%>
                        </td>
                        <td><%# Eval("Mobile")%>
                        </td>
                        <td><%# Eval("Email")%>
                        </td>
                        <td><%# Eval("HVCN")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
</fieldset>
</form>
<iframe name="iframeProcess_Add" width="0px" height="0px"></iframe>
<script type="text/javascript">
    $("#<%= Form1.ClientID %>").keypress(function (event) {
        if (event.which == 13) {
            eval($('#<%= lbtTruyVanPopup.ClientID %>').attr('href'));
        }
    });

    $(document).ready(function () {
        $('#tblDanhSachThanhVien').stickyTableHeaders();

        $("#tblDanhSachThanhVien .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#tblDanhSachThanhVien :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });

    function Choose() {
        var listID = '';
        $('#tblDanhSachThanhVien :checkbox').each(function () {
            if ($(this).prop("checked") == true && $(this).val() != 'all') {
                listID += $(this).val() + '-';
            }
        });
        if (listID.length > 0)
            parent.AddThanhVienToList(listID);
        else {
            alert('Phải chọn ít nhất một thành viên để thêm vào danh sách!');
        }
    }

    function OpenFormChonHVCN() {
        $("#DivDanhSachHVCN").empty();
        $("#DivDanhSachHVCN").append($("<iframe width='100%' height='100%' id='ifDanhSachThanhVien' name='ifDanhSachThanhVien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=HoiVienCaNhan_MiniList&iddoan=" + $('#hdDoanKiemTraID').val()));
        $("#DivDanhSachHVCN").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách hội viên cá nhân</b>",
            modal: true,
            zIndex: 1000
        });
    }

    function DisplayInforHocVien(value) {
        var arrValue = value.split(';#');
    }

    // Created by NGUYEN MANH HUNG - 2014/11/26
    // Hàm đóng Dialog danh sách học viên (Gọi hàm này từ trang con)
    function CloseFormDanhSachHocVien() {
        $("#DivDanhSachHVCN").dialog('close');
    }
</script>

<div id="DivDanhSachHVCN"></div>