﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_KSCL_ChuanBi_Process : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    Utility utility = new Utility();
    Commons cm = new Commons();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string idDanhSach = Library.CheckNull(Request.QueryString["iddanhsach"]);
            string idDoanKiemTra = Library.CheckNull(Request.QueryString["iddoan"]);
            string action = Library.CheckNull(Request.QueryString["action"]);
            string type = Library.CheckNull(Request.QueryString["type"]);
            if (action == "loadct")
            {
                string maCongTy = Library.CheckNull(Request.QueryString["mact"]);
                GetInforCongTy(maCongTy);
            }
            if (action == "loaddsycnopbcchuanop")
            {
                string idCongTy = Library.CheckNull(Request.QueryString["idct"]);
                GetDanhSachPhaiNopBCTuKiemTraTheoCongTy(idCongTy);
            }
            if (action == "loaddsctdaadd")
                LoadDanhSachCongTyDaAdd(idDanhSach, type);
            if (action == "addcompany")
            {
                string listIdCongTy = Library.CheckNull(Request.QueryString["listid"]);
                AddCompanyToList(idDanhSach, listIdCongTy, type);
            }
            if (action == "removecompany")
            {
                string listIdChiTietDanhSach = Library.CheckNull(Request.QueryString["listidchitietdanhsach"]);
                RemoveCompanyFromList(listIdChiTietDanhSach, type);
            }
            if (action == "sendmail")
            {
                string listCompanyNeedAdditional = Library.CheckNull(Request.QueryString["listcompany"]);
                SendEmail(listCompanyNeedAdditional);
            }
            if (action == "loaddstukiemtra")
            {
                string maDanhSach = Library.CheckNull(Request.QueryString["madanhsach"]);
                GetInforDanhSachPhaiNopBCTuKiemTra(maDanhSach);
            }
            if (action == "loaddskiemtravuviec")
            {
                string maDanhSach = Library.CheckNull(Request.QueryString["madanhsach"]);
                GetInforDanhSachYeuCauKiemTraVuViec(maDanhSach);
            }
            if (action == "addthanhvien")
            {
                string listId = Library.CheckNull(Request.QueryString["listid"]);
                AddThanhVienVaoDoanKiemTra(idDoanKiemTra, listId);
            }
            if (action == "dsthanhvien")
            {
                GetDanhSachThanhVienTrongDoanKiemTra(idDoanKiemTra);
            }
            if (action == "removethanhvien")
            {
                string listId = Library.CheckNull(Request.QueryString["listid"]);
                RemoveThanhVienKhoiDoanKiemTra(listId);
            }
            if (action == "addcongtyvaodoankiemtra")
            {
                string listId = Library.CheckNull(Request.QueryString["listid"]);
                AddCongTyVaoDoanKiemTra(idDoanKiemTra, listId);
            }
            if (action == "loaddscongtytrongdoankiemtra")
            {
                GetDanhSachCongTyTrongDoanKiemTra(idDoanKiemTra);
            }
            if (action == "removecongtykhoidoankiemtra")
            {
                string listId = Library.CheckNull(Request.QueryString["listid"]);
                RemoveCongTyKhoiDoanKiemTra(listId);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/26
    /// Load thông tin công ty theo mã công ty
    /// </summary>
    /// <param name="maCongTy">Mã công ty</param>
    private void GetInforCongTy(string maCongTy)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#;#;#;#;#;#;#;#;#;#;#;#;#';" + Environment.NewLine;
        if (!string.IsNullOrEmpty(maCongTy))
        {
            string query = @"SELECT HoiVienTapTheID, MaHoiVienTapThe, TenDoanhNghiep, TenVietTat, TenTiengAnh, DiaChi, MaSoThue, 
	                                SoGiayChungNhanDKKD, NgayGiaNhap, NgayCapGiayChungNhanDKKD, NguoiDaiDienLL_Ten, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email,
	                                tblDMVungMien.TenVungMien, 
	                                Year((SELECT TOP 1 b.DenNgayThucTe FROM tblKSCLHoSo AS b WHERE b.HoiVienTapTheID = a.HoiVienTapTheID ORDER BY b.DenNgayThucTe DESC)) AS LanKiemTraGanNhat,
	                                (SELECT TOP 1 tblKSCLBaoCaoTongHop.XepLoaiChung FROM tblKSCLHoSo
				                                INNER JOIN tblKSCLBaoCaoTongHop ON tblKSCLHoSo.HoSoID = tblKSCLBaoCaoTongHop.HoSoID
				                                WHERE tblKSCLHoSo.HoiVienTapTheID = a.HoiVienTapTheID
				                                ORDER BY tblKSCLHoSo.DenNgayThucTe DESC) AS XepLoai
	                                FROM tblHoiVienTapThe a
	                                LEFT JOIN tblDMTinh ON a.DiaChi_TinhID = tblDMtinh.TinhID 
	                                LEFT JOIN tblDMVungMien ON tblDMTinh.VungMienID = tblDMVungMien.VungMienID
	                                WHERE MaHoiVienTapThe = '" + maCongTy + @"' ORDER BY TenDoanhNghiep";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                Hashtable ht = listData[0];
                js += "value = '" + ht["HoiVienTapTheID"] + ";#" + ht["MaHoiVienTapThe"] + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"])
                         + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenVietTat"]) + ";#" + ht["MaSoThue"] + ";#" + ht["SoGiayChungNhanDKKD"]
                         + ";#" + (!string.IsNullOrEmpty(ht["NgayGiaNhap"].ToString()) ? Library.DateTimeConvert(ht["NgayGiaNhap"]).ToString("dd/MM/yyyy") : "")
                         + ";#" + (!string.IsNullOrEmpty(ht["NgayCapGiayChungNhanDKKD"].ToString()) ? Library.DateTimeConvert(ht["NgayCapGiayChungNhanDKKD"]).ToString("dd/MM/yyyy") : "")
                         + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienLL_Ten"]) + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienLL_DiDong"]) + ";#" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienLL_Email"]) + ";#" + ht["TenVungMien"] + ";#" + ht["LanKiemTraGanNhat"] + ";#" + ht["XepLoai"] + "';" + Environment.NewLine;
            }
            else
                js += "parent.alert('Không tìm thấy thông tin với mã vừa nhập!');" + Environment.NewLine;
        }
        js += "parent.DisplayInforCongTy(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/26
    /// Load những danh sách yêu cầu nộp báo cáo tự kiểm tra mà Công ty chưa nộp
    /// </summary>
    /// <param name="idCongTy">ID công ty</param>
    private void GetDanhSachPhaiNopBCTuKiemTraTheoCongTy(string idCongTy)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrDanhSachYCNopBCTuKiemTra = [];" + Environment.NewLine;
        if (!string.IsNullOrEmpty(idCongTy))
        {
            string query = @"SELECT b.DSBaoCaoTuKiemTraID, b.MaDanhSach, b.NgayLap, b.HanNop FROM tblKSCLDSBaoCaoTuKiemTraChiTiet AS a
                                    INNER JOIN tblKSCLDSBaoCaoTuKiemTra b ON a.DSBaoCaoTuKiemTraID = b.DSBaoCaoTuKiemTraID
                                    WHERE a.HoiVienTapTheID = " + idCongTy + @" AND (a.MaBaoCao is NULL OR a.MaBaoCao = '') ORDER BY b.NgayLap DESC";
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                int index = 0;
                foreach (Hashtable ht in listData)
                {
                    js += "arrDanhSachYCNopBCTuKiemTra[" + index + "] = ['" + ht["DSBaoCaoTuKiemTraID"] + "', '" + ht["MaDanhSach"] + "<i> (Ngày lập: " + (!string.IsNullOrEmpty(Library.CheckNull(ht["NgayLap"])) ? Library.DateTimeConvert(ht["NgayLap"]).ToString("dd/MM/yyyy") : "") + ")</i>" + " '];" + Environment.NewLine;
                    index++;
                }
            }
        }
        js += "parent.DisplayListDanhSachYcNopBCTuKiemTra(arrDanhSachYCNopBCTuKiemTra);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/15
    /// Load danh sách nhưng công ty đã được thêm vào danh sách theo ID danh sách
    /// </summary>
    /// <param name="idDanhSach">ID danh sách</param>
    private void LoadDanhSachCongTyDaAdd(string idDanhSach, string type)
    {
        string query = "";
        if (type == "bctukiemtra")
            query = @"SELECT a.DSBaoCaoTuKiemTraChiTietID AS ChiTietID, b.MaHoiVienTapThe, b.TenDoanhNghiep, b.TenVietTat, b.DiaChi, b.Email, b.DienThoai, b.NguoiDaiDienPL_Ten, b.NguoiDaiDienPL_DiDong  FROM " + ListName.Table_KSCLDSBaoCaoTuKiemTraChiTiet + @" AS a
                             LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID
                             WHERE DSBaoCaoTuKiemTraID = " + idDanhSach;
        if (type == "bckiemtravuviec")
            query = @"SELECT a.DSKiemTraVuViecChiTiet AS ChiTietID, b.MaHoiVienTapThe, b.TenDoanhNghiep, b.TenVietTat, b.DiaChi, b.Email, b.DienThoai, b.NguoiDaiDienPL_Ten, b.NguoiDaiDienPL_DiDong  FROM " + ListName.Table_KSCLDSKiemTraVuViecChiTiet + @" AS a
                             LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID
                             WHERE DSYeuCauKiemTraVuViecID = " + idDanhSach;
        if (type == "bckiemtratructiep")
            query = @"SELECT a.DSKiemTraTrucTiepChiTiet AS ChiTietID, b.MaHoiVienTapThe, b.TenDoanhNghiep, b.TenVietTat, b.DiaChi, b.Email, b.DienThoai, b.NguoiDaiDienPL_Ten, b.NguoiDaiDienPL_DiDong,
                            (SELECT COUNT(DSBaoCaoTuKiemTraChiTietID) FROM tblKSCLDSBaoCaoTuKiemTraChiTiet WHERE DSBaoCaoTuKiemTraID = c.DSBaoCaoTuKiemTraID AND HoiVienTapTheID = a.HoiVienTapTheID) AS CountBCTuKiemTra,
                            (SELECT COUNT(DSKiemTraVuViecChiTiet) FROM tblKSCLDSKiemTraVuViecChiTiet WHERE DSYeuCauKiemTraVuViecID = c.DSYeuCauKiemTraVuViecID AND HoiVienTapTheID = a.HoiVienTapTheID) AS CountBCKiemTraVuViec
                              FROM tblKSCLDSKiemTraTrucTiepChiTiet AS a
                            LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID
                            LEFT JOIN tblKSCLDSKiemTraTrucTiep AS c ON a.DSKiemTraTrucTiepID = c.DSKiemTraTrucTiepID
                            WHERE a.DSKiemTraTrucTiepID = " + idDanhSach;
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;

        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in listData)
            {
                js += "arrData[" + index + "] = ['" + ht["ChiTietID"] + "', '" + ht["MaHoiVienTapThe"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + "'," +
                      " '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenVietTat"]) + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["DiaChi"]) + "', '" + ht["Email"] + "', '" + ht["DienThoai"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienPL_Ten"]) + "'," +
                      " '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienPL_DiDong"]) + "'";
                if (type == "bckiemtratructiep")
                {
                    string dienGiai = "";
                    if (Library.Int32Convert(ht["CountBCTuKiemTra"]) > 0)
                        dienGiai = "Nộp báo cáo tự kiểm tra, ";
                    if (Library.Int32Convert(ht["CountBCKiemTraVuViec"]) > 0)
                        dienGiai += "Kiểm tra theo yêu cầu";
                    js += ",'" + dienGiai.Trim().TrimEnd(',') + "'";
                }
                js += "];" + Environment.NewLine;
                index++;
            }

        }
        js += "parent.DrawData_DangKyCongTyDaAdd(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Add công ty vào danh sách tự kiểm tra
    /// </summary>
    /// <param name="idDanhSach">ID danh sách tự kiểm tra</param>
    /// <param name="listIdCongTy">Danh sách ID công ty</param>
    private void AddCompanyToList(string idDanhSach, string listIdCongTy, string type)
    {
        if (type == "bctukiemtra")
        {
            SqlKscldsBaoCaoTuKiemTraChiTietProvider pro = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
            if (!string.IsNullOrEmpty(listIdCongTy) && !string.IsNullOrEmpty(idDanhSach))
            {
                string[] arrIdCongTy = listIdCongTy.Split(',');
                for (int i = 0; i < arrIdCongTy.Length; i++)
                {
                    if (!string.IsNullOrEmpty(arrIdCongTy[i]))
                    {
                        KscldsBaoCaoTuKiemTraChiTiet obj = new KscldsBaoCaoTuKiemTraChiTiet();
                        obj.HoiVienTapTheId = Library.Int32Convert(arrIdCongTy[i]);
                        obj.DsBaoCaoTuKiemTraId = Library.Int32Convert(idDanhSach);
                        obj.TinhTrangGuiEmailThongBao = "2";
                        pro.Insert(obj);
                    }
                }
            }
        }
        if (type == "bckiemtravuviec")
        {
            SqlKscldsKiemTraVuViecChiTietProvider pro = new SqlKscldsKiemTraVuViecChiTietProvider(ListName.ConnectionString, false, string.Empty);
            if (!string.IsNullOrEmpty(listIdCongTy) && !string.IsNullOrEmpty(idDanhSach))
            {
                string[] arrIdCongTy = listIdCongTy.Split(',');
                for (int i = 0; i < arrIdCongTy.Length; i++)
                {
                    if (!string.IsNullOrEmpty(arrIdCongTy[i]))
                    {
                        KscldsKiemTraVuViecChiTiet obj = new KscldsKiemTraVuViecChiTiet();
                        obj.HoiVienTapTheId = Library.Int32Convert(arrIdCongTy[i]);
                        obj.DsYeuCauKiemTraVuViecId = Library.Int32Convert(idDanhSach);
                        pro.Insert(obj);
                    }
                }
            }
        }
        if (type == "bckiemtratructiep")
        {
            SqlKscldsKiemTraTrucTiepChiTietProvider pro = new SqlKscldsKiemTraTrucTiepChiTietProvider(ListName.ConnectionString, false, string.Empty);
            if (!string.IsNullOrEmpty(listIdCongTy) && !string.IsNullOrEmpty(idDanhSach))
            {
                string[] arrIdCongTy = listIdCongTy.Split(',');
                for (int i = 0; i < arrIdCongTy.Length; i++)
                {
                    if (!string.IsNullOrEmpty(arrIdCongTy[i]))
                    {
                        KscldsKiemTraTrucTiepChiTiet obj = new KscldsKiemTraTrucTiepChiTiet();
                        obj.HoiVienTapTheId = Library.Int32Convert(arrIdCongTy[i]);
                        obj.DsKiemTraTrucTiepId = Library.Int32Convert(idDanhSach);
                        pro.Insert(obj);
                    }
                }
            }
        }
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CloseDanhSachCongTyKiemToan();" + Environment.NewLine;
        js += "parent.GetDanhSachCongTyDaAdd();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/16
    /// Xóa công ty khỏi danh sách tự kiểm tra
    /// </summary>
    /// <param name="listIdChiTietDanhSach">Danh sách ID của các bản ghi chi tiết báo cáo tự kiểm tra</param>
    private void RemoveCompanyFromList(string listIdChiTietDanhSach, string type)
    {
        if (type == "bctukiemtra")
        {
            SqlKscldsBaoCaoTuKiemTraChiTietProvider pro = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
            if (!string.IsNullOrEmpty(listIdChiTietDanhSach))
            {
                string[] arrIdCongTy = listIdChiTietDanhSach.Split(',');
                for (int i = 0; i < arrIdCongTy.Length; i++)
                {
                    if (!string.IsNullOrEmpty(arrIdCongTy[i]))
                    {
                        pro.Delete(Library.Int32Convert(arrIdCongTy[i]));
                    }
                }
            }
        }
        if (type == "bckiemtravuviec")
        {
            SqlKscldsKiemTraVuViecChiTietProvider pro = new SqlKscldsKiemTraVuViecChiTietProvider(ListName.ConnectionString, false, string.Empty);
            if (!string.IsNullOrEmpty(listIdChiTietDanhSach))
            {
                string[] arrIdCongTy = listIdChiTietDanhSach.Split(',');
                for (int i = 0; i < arrIdCongTy.Length; i++)
                {
                    if (!string.IsNullOrEmpty(arrIdCongTy[i]))
                    {
                        pro.Delete(Library.Int32Convert(arrIdCongTy[i]));
                    }
                }
            }
        }
        if (type == "bckiemtratructiep")
        {
            SqlKscldsKiemTraTrucTiepChiTietProvider pro = new SqlKscldsKiemTraTrucTiepChiTietProvider(ListName.ConnectionString, false, string.Empty);
            if (!string.IsNullOrEmpty(listIdChiTietDanhSach))
            {
                string[] arrIdCongTy = listIdChiTietDanhSach.Split(',');
                for (int i = 0; i < arrIdCongTy.Length; i++)
                {
                    if (!string.IsNullOrEmpty(arrIdCongTy[i]))
                    {
                        pro.Delete(Library.Int32Convert(arrIdCongTy[i]));
                    }
                }
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.GetDanhSachCongTyDaAdd();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/17
    /// Gửi Email tới các công ty trong danh sách yêu cầu nộp báo cáo tự kiểm tra
    /// </summary>
    /// <param name="strCompanyNeedSendEmail">Danh sách id bản ghi BaoCaoTuKiemTraChiTiet của các công ty cần gửi Email</param>
    private void SendEmail(string strCompanyNeedSendEmail)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        //js += "parent.SendEmail_Success(1, '');"; // Thành công
        string listCompanyFail = "";
        strCompanyNeedSendEmail = strCompanyNeedSendEmail.TrimEnd(',');
        List<string> listCompanyNeedSendEmail = strCompanyNeedSendEmail.Split(',').ToList<string>();
        try
        {
            if (listCompanyNeedSendEmail.Count > 0)
            {
                SqlKscldsBaoCaoTuKiemTraChiTietProvider proChiTiet = new SqlKscldsBaoCaoTuKiemTraChiTietProvider(ListName.ConnectionString, false, string.Empty);
                SqlKscldsBaoCaoTuKiemTraProvider pro = new SqlKscldsBaoCaoTuKiemTraProvider(ListName.ConnectionString, false, string.Empty);
                //KscldsBaoCaoTuKiemTra obj = pro.GetByDsBaoCaoTuKiemTraId(Library.Int32Convert(idDanhSach));

                string clause = "";
                foreach (string idChiTiet in listCompanyNeedSendEmail)
                {
                    if (!string.IsNullOrEmpty(idChiTiet.Trim()))
                    {
                        if (!string.IsNullOrEmpty(clause))
                            clause += " OR ";
                        clause += "a.DSBaoCaoTuKiemTraChiTietID = " + idChiTiet;
                    }
                }
                if (!string.IsNullOrEmpty(clause))
                    clause = " (" + clause + ")";

                string query = "SELECT a.DSBaoCaoTuKiemTraChiTietID, a.TinhTrangGuiEmailThongBao, Email,LoaiHoiVienTapThe, b.HoiVienTapTheID, TenDoanhNghiep, c.HanNop FROM tblKSCLDSBaoCaoTuKiemTraChiTiet AS a LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID" +
                               " LEFT JOIN tblKSCLDSBaoCaoTuKiemTra AS c ON a.DSBaoCaoTuKiemTraID = c.DSBaoCaoTuKiemTraID WHERE " + clause;
                List<Hashtable> listData = _db.GetListData(query);
                if (listData.Count > 0)
                {
                    List<string> listMailAddress = new List<string>(); // List Địa chỉ email hợp lệ sẽ được gửi
                    List<string> listIDSentSuccess = new List<string>(); // List ID bản ghi chi tiết danh sách tự kiểm tra đc gưi Email -> dùng để Cập nhật lại trạng thái gửi Email
                    List<string> listSubject = new List<string>();
                    List<string> listContent = new List<string>();
                    List<string> listHoiVienTapTheID = new List<string>();
                    List<string> listLoaiHvtt = new List<string>();
                    foreach (Hashtable ht in listData)
                    {
                        string emailAddress = ht["Email"].ToString();
                        if (!string.IsNullOrEmpty(emailAddress) && Library.CheckIsValidEmail(emailAddress))
                        {
                            listHoiVienTapTheID.Add(ht["HoiVienTapTheID"].ToString());
                            if (ht["LoaiHoiVienTapThe"].ToString() == "0" || ht["LoaiHoiVienTapThe"].ToString() == "2")
                                listLoaiHvtt.Add("3");
                            else if (ht["LoaiHoiVienTapThe"].ToString() == "1")
                                listLoaiHvtt.Add("4");
                            else
                                listLoaiHvtt.Add("");
                            listIDSentSuccess.Add(ht["DSBaoCaoTuKiemTraChiTietID"].ToString());
                            listMailAddress.Add(emailAddress);
                            listSubject.Add("VACPA: Thông báo nộp báo cáo tự kiểm tra");
                            listContent.Add("Yêu cầu đơn vị gửi Báo cáo tự kiểm tra. Hạn nộp vào ngày " + Library.DateTimeConvert(ht["HanNop"]).ToString("dd/MM/yyyy"));
                        }
                        else
                        {
                            listCompanyFail += Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + ";#";
                        }
                    }
                    if (listMailAddress.Count > 0)
                    {
                        if (utility.SendEmail(listMailAddress, listSubject, listContent))
                        {
                            for (int i = 0; i < listMailAddress.Count; i++)
                            {
                                cm.GuiThongBao(listHoiVienTapTheID[i], listLoaiHvtt.Count > i ? listLoaiHvtt[i] : "", listSubject[0], listContent[0]);
                            }
                            foreach (string id in listIDSentSuccess)
                            {
                                KscldsBaoCaoTuKiemTraChiTiet objChiTiet = proChiTiet.GetByDsBaoCaoTuKiemTraChiTietId(Library.Int32Convert(id));
                                if (objChiTiet != null && objChiTiet.DsBaoCaoTuKiemTraChiTietId > 0)
                                {
                                    objChiTiet.TinhTrangGuiEmailThongBao = "1";
                                    proChiTiet.Update(objChiTiet);
                                }
                            }
                            js += "parent.SendEmail_Success(1, '" + listCompanyFail + "');"; // Thành công
                        }
                        else
                            js += "parent.SendEmail_Success(0, '" + listCompanyFail + "');"; // Thất bại
                    }
                }
            }
        }
        catch
        {
            js += "parent.SendEmail_Success(0, '" + listCompanyFail + "');";
        }
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/29
    /// Get thông tin chi tiết của danh sách phải nộp báo cáo tự kiểm tra theo mã danh sách
    /// </summary>
    /// <param name="maDanhSach">Mã danh sách</param>
    private void GetInforDanhSachPhaiNopBCTuKiemTra(string maDanhSach)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#';" + Environment.NewLine;
        string query = @"SELECT DSBaoCaoTuKiemTraID, MaDanhSach, tblDMTrangThai.MaTrangThai,
                                 (SELECT COUNT(DSKiemTraTrucTiepID) FROM tblKSCLDSKiemTraTrucTiep WHERE DSBaoCaoTuKiemTraID = a.DSBaoCaoTuKiemTraID) AS DaDuocAddVaoDSKiemTraTrucTiep
                                  FROM tblKSCLDSBaoCaoTuKiemTra AS a LEFT JOIN tblDMTrangThai ON a.TinhTrangID = tblDMTrangThai.TrangThaiID
                                  WHERE a.MaDanhSach = '" + maDanhSach + @"'";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            bool flag = true;
            if (ht["DaDuocAddVaoDSKiemTraTrucTiep"].ToString() != "0")
            {
                js += "parent.alert('Danh sách với mã vừa nhập đã được sử dụng trong một danh sách kiểm tra trực tiếp khác!');" + Environment.NewLine;
                flag = false;
            }
            if (ht["MaTrangThai"].ToString().Trim() != ListName.Status_DaPheDuyet)
            {
                js += "parent.alert('Danh sách với mã vừa nhập chưa được phê duyệt!');" + Environment.NewLine;
                flag = false;
            }
            if (flag)
                js += "value = '" + ht["DSBaoCaoTuKiemTraID"] + ";#" + ht["MaDanhSach"] + "';" + Environment.NewLine;
        }
        else
            js += "parent.alert('Không tìm thấy danh sách với mã vừa nhập!');" + Environment.NewLine;
        js += "parent.DisplayInforDsTuKiemTra(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2014/12/29
    /// Get thông tin chi tiết của danh sách kiểm tra theo yêu cầu theo mã danh sách
    /// </summary>
    /// <param name="maDanhSach">Mã danh sách</param>
    private void GetInforDanhSachYeuCauKiemTraVuViec(string maDanhSach)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = ';#';" + Environment.NewLine;
        string query = @"SELECT DSYeuCauKiemTraVuViecID, MaDanhSach,
		                            (SELECT COUNT(DSKiemTraTrucTiepID) FROM tblKSCLDSKiemTraTrucTiep WHERE DSYeuCauKiemTraVuViecID = a.DSYeuCauKiemTraVuViecID) AS DaDuocAddVaoDSKiemTraTrucTiep
                            FROM tblKSCLDSKiemTraVuViec AS a
                            WHERE a.MaDanhSach = '" + maDanhSach + @"'";
        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            Hashtable ht = listData[0];
            if (ht["DaDuocAddVaoDSKiemTraTrucTiep"].ToString() != "0")
                js += "parent.alert('Danh sách với mã vừa nhập đã được sử dụng trong một danh sách kiểm tra trực tiếp khác!');" + Environment.NewLine;
            else
                js += "value = '" + ht["DSYeuCauKiemTraVuViecID"] + ";#" + ht["MaDanhSach"] + "';" + Environment.NewLine;
        }
        else
            js += "parent.alert('Không tìm thấy danh sách với mã vừa nhập!');" + Environment.NewLine;
        js += "parent.DisplayInforDsKiemTraVuViec(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void AddThanhVienVaoDoanKiemTra(string idDoanKiemTra, string listId)
    {
        SqlKsclDoanKiemTraThanhVienProvider pro = new SqlKsclDoanKiemTraThanhVienProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(listId) && !string.IsNullOrEmpty(idDoanKiemTra))
        {
            string[] arrId = listId.Split('-');
            for (int i = 0; i < arrId.Length; i++)
            {
                if (!string.IsNullOrEmpty(arrId[i]))
                {
                    KsclDoanKiemTraThanhVien obj = new KsclDoanKiemTraThanhVien();
                    if (!string.IsNullOrEmpty(arrId[i].Split(',')[1]))
                        obj.HoiVienCaNhanId = Library.Int32Convert(arrId[i].Split(',')[0]);
                    else
                        obj.ThanhVienDoanKiemTraId = Library.Int32Convert(arrId[i].Split(',')[0]);
                    obj.DoanKiemTraId = Library.Int32Convert(idDoanKiemTra);
                    obj.VaiTro = "3";
                    pro.Insert(obj);
                }
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CloseDanhSachThanhVien();" + Environment.NewLine;
        js += "parent.GetDanhSachThanhVienTrongDoanKiemTra();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void GetDanhSachThanhVienTrongDoanKiemTra(string idDoanKiemTra)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;
        List<Hashtable> listData = _db.GetListData(ListName.Proc_KSCL_SelectDanhSachThanhVienTrongDoanKiemTra, new List<string> { "@IdDoanKiemTra" }, new List<object> { idDoanKiemTra });
        for (int i = 0; i < listData.Count; i++)
        {
            Hashtable ht = listData[i];
            js += "arrData[" + i + "] = ['" + ht["DoanKiemTraThanhVienID"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenThanhVien"]) + "', '" + ht["NamSinh"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenChucVu"]) + "'," +
                  " '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["DonViCongTac"]) + "', '" + ht["SoChungChiKTV"] + "', '" + (!string.IsNullOrEmpty(Library.CheckNull(ht["NgayCapChungChiKTV"])) ? Library.DateTimeConvert(ht["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy") : "") + "', '" + ht["SoLanThamGia"] + "', '" + ht["VaiTro"] + "', " +
                  " '" + ht["SoGioKT"] + "','" + ht["SoGioDD"] + "','" + ht["SoGioKhac"] + "', '" + (!string.IsNullOrEmpty(Library.CheckNull(ht["NgayCapNhat"])) ? Library.DateTimeConvert(ht["NgayCapNhat"]).ToString("dd/MM/yyyy") : "") + "'];" + Environment.NewLine;
        }
        js += "parent.DrawData_DanhSachThanhVienTrongDoanKiemTra(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    /// <summary>
    /// Created by NGUYEN MANH HUNG - 2015/01/05
    /// Xóa thành viên khỏi đoàn kiểm tra
    /// </summary>
    /// <param name="listIdDoanKiemTraThanhVien">Danh sách ID của các bản ghi đoàn kiểm tra thành viên</param>
    private void RemoveThanhVienKhoiDoanKiemTra(string listIdDoanKiemTraThanhVien)
    {
        SqlKsclDoanKiemTraThanhVienProvider pro = new SqlKsclDoanKiemTraThanhVienProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(listIdDoanKiemTraThanhVien))
        {
            string[] arrId = listIdDoanKiemTraThanhVien.Split(',');
            for (int i = 0; i < arrId.Length; i++)
            {
                if (!string.IsNullOrEmpty(arrId[i]))
                {
                    pro.Delete(Library.Int32Convert(arrId[i]));
                }
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.GetDanhSachThanhVienTrongDoanKiemTra();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void AddCongTyVaoDoanKiemTra(string idDoanKiemTra, string listId)
    {
        SqlKsclDoanKiemTraCongTyProvider pro = new SqlKsclDoanKiemTraCongTyProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(listId) && !string.IsNullOrEmpty(idDoanKiemTra))
        {
            string[] arrId = listId.Split(',');
            for (int i = 0; i < arrId.Length; i++)
            {
                if (!string.IsNullOrEmpty(arrId[i]))
                {
                    KsclDoanKiemTraCongTy obj = new KsclDoanKiemTraCongTy();
                    obj.HoiVienTapTheId = Library.Int32Convert(arrId[i]);
                    obj.DoanKiemTraId = Library.Int32Convert(idDoanKiemTra);
                    pro.Insert(obj);
                }
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.CloseDanhSachCongTyKiemToan();" + Environment.NewLine;
        js += "parent.GetDanhSachCongTyDaAdd();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void GetDanhSachCongTyTrongDoanKiemTra(string idDoanKiemTra)
    {
        string query = @"SELECT a.DoanKiemTraCongTyID AS ChiTietID, b.MaHoiVienTapThe, b.TenDoanhNghiep, b.TenVietTat, b.DiaChi, b.Email, b.DienThoai, b.NguoiDaiDienPL_Ten, b.NguoiDaiDienPL_DiDong, a.TuNgay, a.DenNgay  FROM " + ListName.Table_KSCLDoanKiemTraCongTy + @" AS a
                                LEFT JOIN tblHoiVienTapThe AS b ON a.HoiVienTapTheID = b.HoiVienTapTheID
                                WHERE DoanKiemTraID = " + idDoanKiemTra;
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "var arrData = [];" + Environment.NewLine;

        List<Hashtable> listData = _db.GetListData(query);
        if (listData.Count > 0)
        {
            int index = 0;
            foreach (Hashtable ht in listData)
            {
                js += "arrData[" + index + "] = ['" + ht["ChiTietID"] + "', '" + ht["MaHoiVienTapThe"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenDoanhNghiep"]) + "'," +
                      " '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["TenVietTat"]) + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["DiaChi"]) + "', '" + ht["Email"] + "', '" + ht["DienThoai"] + "', '" + Library.RemoveSpecCharaterWhenSendByJavascript(ht["NguoiDaiDienPL_Ten"]) + "'," +
                      " '" + ht["NguoiDaiDienPL_DiDong"] + "'," +
                      " '" + (!string.IsNullOrEmpty(Library.CheckNull(ht["TuNgay"])) ? Library.DateTimeConvert(ht["TuNgay"]).ToString("dd/MM/yyyy") : "") + "'," +
                      " '" + (!string.IsNullOrEmpty(Library.CheckNull(ht["DenNgay"])) ? Library.DateTimeConvert(ht["DenNgay"]).ToString("dd/MM/yyyy") : "") + "'";
                js += "];" + Environment.NewLine;
                index++;
            }

        }
        js += "parent.DrawData_DangKyCongTyDaAdd(arrData);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }

    private void RemoveCongTyKhoiDoanKiemTra(string listId)
    {
        SqlKsclDoanKiemTraCongTyProvider pro = new SqlKsclDoanKiemTraCongTyProvider(ListName.ConnectionString, false, string.Empty);
        if (!string.IsNullOrEmpty(listId))
        {
            string[] arrId = listId.Split(',');
            for (int i = 0; i < arrId.Length; i++)
            {
                if (!string.IsNullOrEmpty(arrId[i]))
                {
                    pro.Delete(Library.Int32Convert(arrId[i]));
                }
            }
        }

        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "parent.GetDanhSachCongTyDaAdd();" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }
}