﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KSCL_KetThuc_XuLySaiPham_NoiDung.ascx.cs"
    Inherits="usercontrols_KSCL_KetThuc_XuLySaiPham_NoiDung" %>
<script src="/js/jquery.stickytableheaders.min.js" type="text/javascript"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
<style type="text/css">
    .tdInput
    {
        width: 120px;
    }
</style>
<form id="Form1" clientidmode="Static" runat="server" method="post" enctype="multipart/form-data">
<div id="thongbaoloi_form" name="thongbaoloi_form" style="display: none; margin-top: 5px;"
    class="alert alert-error">
</div>
<div id="thongbaothanhcong_form" name="thongbaothanhcong_form" style="display: none;
    margin-top: 5px;" class="alert alert-success">
</div>
<div id="Div1" style="width: 100%; margin: 5px 0px;">
    <a id="btnAdd" href="javascript:;" class="btn" onclick="SubmitForm();"><i class="iconfa-save">
    </i>Lưu</a> <a id="A4" href="javascript:;" class="btn" onclick="parent.CloseFormNoiDung();">
        <i class="iconfa-off"></i>Đóng</a>
</div>
<fieldset class="fsBlockInfor">
    <legend>Thông tin chung</legend>
    <input type="hidden" name="hdAction" id="hdAction" />
    <input type="hidden" name="hdCongTyID" id="hdCongTyID" />
    <input type="hidden" name="hdXuLySaiPhamCongTyID" id="hdXuLySaiPhamCongTyID" />
    <table id="tblThongTinChung" width="800px" border="0" class="formtbl">
        <tr>
            <td style="width: 180px;">
                ID.HVTT/ID.CTKT<span class="starRequired">(*)</span>:
            </td>
            <td style="width: 150px;">
                <select id="ddlCongTy" name="ddlCongTy" onchange="CallActionGetInforXuLySaiPham();">
                    <% GetDanhSachCongTy();%>
                </select>
            </td>
            <td style="width: 120px;">
                Tên công ty:
            </td>
            <td>
                <span id="spanTenCongTy"></span>
            </td>
        </tr>
        <tr>
            <td>
                Nội dung sai phạm:
            </td>
            <td colspan="3">
                <textarea rows="3" name="txtNoiDungSaiPham" id="txtNoiDungSaiPham"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                Quyết định xử lý của BTC:
            </td>
            <td colspan="3">
                <textarea rows="3" name="txtQuyetDinhXuLyCuaBTC" id="txtQuyetDinhXuLyCuaBTC"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                Quyết định xử lý của VACPA:
            </td>
            <td colspan="3">
                <textarea rows="3" name="txtQuyetDinhXuLyCuaVACPA" id="txtQuyetDinhXuLyCuaVACPA"></textarea>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="fsBlockInfor" id="fsDanhSachKiemToanVien" style="display: none;">
    <legend>Danh sách kiểm toán viên</legend>
    <div style="margin: 5px 0;">
        <a id="A1" href="javascript:;" class="btn" onclick="OpenDanhSachHoiVienCaNhan();"><i
            class="iconfa-plus-sign"></i>Thêm</a>&nbsp; <a id="A3" href="javascript:;" class="btn"
                onclick="DeleteDanhSachHoiVienCaNhan();"><i class="iconfa-trash"></i>Xóa</a>
    </div>
    <table id="tblDanhSachKiemToanVien" width="100%" border="0" class="formtbl">
        <thead>
            <tr>
                <th style="width: 30px;">
                    <input type="checkbox" value="all" class="checkall" />
                </th>
                <th style="width: 35px;">
                    STT
                </th>
                <th style="width: 200px;">
                    Họ và tên hội viên cá nhân/kiểm toán viên
                </th>
                <th style="width: 80px;">
                    Hội viên
                </th>
                <th style="min-width: 200px;">
                    Nội dung sai phạm
                </th>
                <th style="min-width: 150px;">
                    Quyết định xử lý của BTC
                </th>
                <th style="min-width: 150px;">
                    Quyết định xử lý của VACPA
                </th>
                <th>
                    Sửa
                </th>
            </tr>
        </thead>
        <tbody id="TBodyDanhSachKiemToanVien">
        </tbody>
    </table>
</fieldset>
<iframe name="iframeProcess" width="0px" height="0px"></iframe>
<iframe name="iframeProcess_LoadKetQuaKiemTra" width="0px" height="0px"></iframe>
</form>
<script type="text/javascript">
    // Validate form
    $('#Form1').validate({
        rules: {
            dllCongTy: {
                required: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form").html(e).show();

            } else {
                jQuery("#thongbaoloi_form").hide();
            }
        }  
    });
    
    function OpenDanhSachHoiVienCaNhan() {
        $("#DivHoiVienCaNhan").empty();
        $("#DivHoiVienCaNhan").append($("<iframe width='100%' height='100%' id='ifDanhSachHoiVienCaNhan' name='ifDanhSachHoiVienCaNhan' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_KetThuc_XuLySaiPham_ChonHoiVienCaNhan&idct=" + $('#ddlCongTy option:selected').val() + "&idxulysaipham=" + getParameterByName('idxlsp')));
        $("#DivHoiVienCaNhan").dialog({
            resizable: true,
            width: 800,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Danh sách hội viên cá nhân</b>",
            modal: true,
            zIndex: 1000,
            buttons: {

                "Chọn": function () {
                    window.ifDanhSachHoiVienCaNhan.ChooseHocVien();
                },

                "Đóng": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#DivHoiVienCaNhan').parent().find('button:contains("Chọn")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#DivHoiVienCaNhan').parent().find('button:contains("Đóng")').addClass('btn btn-rounded').prepend('<i class="iconfa-off"> </i>&nbsp;');
    }
    
    function CloseFormDanhSachHoiVienCaNhan() {
        $("#DivHoiVienCaNhan").dialog('close');
    }
    
    function CallActionGetInforXuLySaiPham() {
        var id = $('#ddlCongTy option:selected').val();
        if(id.length > 0) {
            
            $('#hdCongTyID').val(id);
            iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=loadxulysaiphamcongty&idcongty=' + id + '&idxulysaipham=' + getParameterByName('idxlsp');
        } else {
            
        }
        
    }
    
    function DisplayInforXuLySaiPhamCongTy(value, tenCongTy, arrData) {
        $('#spanTenCongTy').html(tenCongTy);
        var arrValue = value.split(';#');
        var tbodyMain = document.getElementById('TBodyDanhSachKiemToanVien');
        var trs = tbodyMain.getElementsByTagName("tr");
        for (var i = 0; i < trs.length; i++) {
            trs[i].parentNode.removeChild(trs[i]);
            i--;
        }
        if(arrValue[0].length == 0) {
            $('#hdXuLySaiPhamCongTyID').val('');
            $('#txtNoiDungSaiPham').val('');
            $('#txtQuyetDinhXuLyCuaBTC').val('');
            $('#txtQuyetDinhXuLyCuaVACPA').val('');
            $('#fsDanhSachKiemToanVien').css('display', 'none');
        } else {
            $('#fsDanhSachKiemToanVien').css('display', '');
            if(arrData.length > 0) {
                for(var i = 0; i < arrData.length; i ++) {
                    var xuLySaiPhamChiTietId = arrData[i][0];
                    var hoiVienCaNhanId = arrData[i][1];
                    var tenHoiVienCaNhan = arrData[i][2];
                    var noiDungSaiPham = arrData[i][3];
                    var quyetDinhBtc = arrData[i][4];
                    var quyetDinhVacpa = arrData[i][5];
                    var loaiHoiVien = arrData[i][6];
                    var laHoiVien = '';
                    if(loaiHoiVien == "1")
                        laHoiVien = 'x';
                    var linkEdit = '<a href="javascript:;" class="btn" onclick="OpenFormUpdateChoHVCN('+xuLySaiPhamChiTietId+');"><i class="iconsweets-create"></i></a>';
                
                    var tr = document.createElement('TR');
                    var tdCheckBox = document.createElement("TD");
                    tdCheckBox.style.textAlign = 'center';
                    tdCheckBox.className = 'firstColumn';
                    var checkbox = document.createElement('input');
                    checkbox.type = "checkbox";
                    checkbox.value = xuLySaiPhamChiTietId;
                    tdCheckBox.appendChild(checkbox);
                    tr.appendChild(tdCheckBox);
                
                    var arrColumn = [(i + 1), tenHoiVienCaNhan, laHoiVien, noiDungSaiPham, quyetDinhBtc, quyetDinhVacpa, linkEdit];
                    for(var j = 0; j < arrColumn.length; j ++){
                        var td = document.createElement("TD");
                        if(j == 0)
                            td.style.textAlign = 'center';
                        td.innerHTML = arrColumn[j];
                        tr.appendChild(td); 
                    }
                
                    tbodyMain.appendChild(tr);
                }
            }
        }
        $('#hdXuLySaiPhamCongTyID').val(arrValue[0]);
        $('#txtNoiDungSaiPham').val(arrValue[1]);
        $('#txtQuyetDinhXuLyCuaBTC').val(arrValue[2]);
        $('#txtQuyetDinhXuLyCuaVACPA').val(arrValue[3]);
    }
    
    function OpenFormUpdateChoHVCN(id) {
        $("#DivUpdateChoHVCN").empty();
        $("#DivUpdateChoHVCN").append($("<iframe width='100%' height='100%' id='ifUpdateChoHVCN' name='ifUpdateChoHVCN' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=KSCL_KetThuc_XuLySaiPham_UpdateHoiVienCaNhan&id=" + id));
        $("#DivUpdateChoHVCN").dialog({
            resizable: true,
            width: 700,
            height: 600,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Cập nhật cho kiểm toán viên</b>",
            modal: true,
            zIndex: 1000
        });
    }
    
    function CloseFormUpdateChoHVCN() {
        $("#DivUpdateChoHVCN").dialog('close');
    }

    function SubmitForm() {
        $('#hdAction').val('update');
        $('#Form1').submit();
    }
    
    function Delete() {
        if(confirm('Bạn chắc chắn muốn xóa báo cáo này chứ?')){
            $('#hdAction').val('delete');
            $('#Form1').submit();
        }
    }
    
    function InsertDanhSachHoiVienCaNhan(listId) {
        iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=insertxulysaiphamcongty&idxulysaiphamchitiet=' + listId + '&idcongty=' + $('#ddlCongTy option:selected').val() + '&idxulysaipham=' + getParameterByName('idxlsp');
    }
    
    function DeleteDanhSachHoiVienCaNhan() {
        if(confirm('Bạn chắc chắn muốn xóa các bản ghi này chứ?')) {
            var listId = '';
            var flag = false;
            $('#tblDanhSachKiemToanVien :checkbox').each(function () {
                var checked = $(this).prop("checked");
                if (checked && $(this).val() != 'all') {
                    listId += $(this).val() + ',';
                    flag = true;
                }
            });
            if (!flag)
                alert('Phải chọn ít nhất một bản ghi để thực hiện thao tác!');
            else
                iframeProcess.location = '/iframe.aspx?page=KSCL_KetThuc_Process&action=deletexulysaiphamcongty&idxulysaiphamchitiet=' + listId;
        } 
    }
    
    jQuery(document).ready(function () {
        $('#tblDanhSachKiemToanVien').stickyTableHeaders();
        $("#tblDanhSachKiemToanVien .checkall").bind("click", function () {
            var checked = $(this).prop("checked");
            $('#tblDanhSachKiemToanVien :checkbox').each(function () {
                $(this).prop("checked", checked);
            });
        });
    });
    
    <% CheckPermissionOnPage(); %>
</script>
<div id="DivHoiVienCaNhan">
</div>
<div id="DivUpdateChoHVCN">
</div>
