﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_adminleftmenu : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public string SoLuongYeuCauChuaDuyet;

    protected void checkmenu(string pages, int HaveChild = 0)
    {
        string url = Request.Path + "?" + Request.QueryString.ToString();

        if (url.Contains("dmchung"))
        {
            if (HaveChild == 0)
            {
                if (pages.Contains("|" + url + "|")) Response.Write(" class='active' ");
            }
            if (HaveChild == 1)
            {
                if (pages.Contains("|" + url + "|")) Response.Write(" class='dropdown active' ");
                if (!pages.Contains("|" + url + "|")) Response.Write(" class='dropdown' ");
            }
            if (HaveChild == 2)
            {
                if (pages.Contains("|" + url + "|")) Response.Write("  style='display: block;' ");
            }
        }
        else
        {
            if (HaveChild == 0)
            {
                if (pages.Contains("|" + url.Split('&')[0] + "|")) Response.Write(" class='active' ");
            }
            if (HaveChild == 1)
            {
                if (pages.Contains("|" + url.Split('&')[0] + "|")) Response.Write(" class='dropdown active' ");
                if (!pages.Contains("|" + url.Split('&')[0] + "|")) Response.Write(" class='dropdown' ");
            }
            if (HaveChild == 2)
            {
                if (pages.Contains("|" + url.Split('&')[0] + "|")) Response.Write("  style='display: block;' ");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = @"SELECT SUM(X.SL)
FROM (
SELECT COUNT(*) as SL FROM tblCapNhatHoSoHVCN WHERE TinhTrangID = 3
UNION ALL
SELECT COUNT(*) as SL FROM tblCapNhatHoSoHVTT WHERE TinhTrangID = 3) X";
        SqlCommand cmd = new SqlCommand(sql);

        if (DataAccess.DLookup(cmd) != "0")
            SoLuongYeuCauChuaDuyet = "(" + DataAccess.DLookup(cmd) + ")";
    }
}