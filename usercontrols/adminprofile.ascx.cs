﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using VACPA.Data.SqlClient;
using VACPA.Data.Bases;
using VACPA.Entities;

public partial class usercontrols_adminprofile : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
 
    protected void Page_Load(object sender, EventArgs e)
    {
        Loaddrop_bophancongtac(0);

        SqlNguoiDungProvider tk_provider = new SqlNguoiDungProvider(cm.connstr, true, "");
        NguoiDung tk = tk_provider.GetByNguoiDungId(Convert.ToInt32(Request.QueryString["id"]));
          

        if (!string.IsNullOrEmpty(Request.Form["TenDangNhap"]))
        {
            try
            {
                string lastid;
                if (check_user(Request.Form["TenDangNhap"], Request.Form["Email"]))
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Tài khoản đã tồn tại trong hệ thống! Hãy chọn một tên tài khoản hoặc địa chỉ email khác!</div>"));
                    return;
                }

                tk.HoVaTen =Request.Form["HoVaTen"];
                tk.TenDangNhap = Request.Form["TenDangNhap"];
                tk.BoPhanCongTac = Convert.ToInt32(Request.Form["BoPhanCongTac"]);
                tk.DienThoai = Request.Form["DienThoai"];
                tk.ChucVu = Request.Form["ChucVu"];
                tk.Email = Request.Form["Email"];



                
                if (!string.IsNullOrEmpty(Request.Form["MatKhau"]))
                {
                    SqlCommand sql = new SqlCommand();    
                    sql.CommandText = "SELECT MatKhau FROM TBLNguoiDung WHERE NguoiDungID="+cm.Admin_NguoiDungID;
                    string matkhaucu = DataAccess.DLookup(sql);
                    
                    if (matkhaucu != Request.Form["MatKhauHienTai"])
                        Response.Write("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Mật khẩu hiện tại nhập không chính xác. Mật khẩu mới không được cập nhật!</div>");
                    else
                        tk.MatKhau = Request.Form["MatKhau"];

                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                }

                tk_provider.Update(tk);

             cm = new Commons();
             cm.ghilog("SuaThongTinTaiKhoan", "Tài khoản \"" + Request.Form["TenDangNhap"]+"\" sửa thông tin cá nhân");
              
                

                //Response.Clear();
                Response.Write("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>");



            }
            catch (Exception ex)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
            }

        }
    }

    protected void Load_user()
    {
        try
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT * FROM tblNguoiDung WHERE NguoiDungID=" + Request.QueryString["id"];
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                Response.Write("$('#HoVaTen').val('" + dt.Rows[0]["HoVaTen"] + "');" + System.Environment.NewLine);
                Response.Write("$('#TenDangNhap').val('" + dt.Rows[0]["TenDangNhap"] + "');" + System.Environment.NewLine);
                Response.Write("$('#Email').val('" + dt.Rows[0]["Email"] + "');" + System.Environment.NewLine);
                Response.Write("$('#DienThoai').val('" + dt.Rows[0]["DienThoai"] + "');" + System.Environment.NewLine);
                Response.Write("$('#BoPhanCongTac').val('" + dt.Rows[0]["BoPhanCongTac"] + "');" + System.Environment.NewLine);
                Response.Write("$('#ChucVu').val('" + dt.Rows[0]["ChucVu"] + "');" + System.Environment.NewLine);
            }

            if (Request.QueryString["mode"] == "view")
            {
                Response.Write("$('#form_themuser input,select,textarea').attr('disabled', true);");
            }


           

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }





    bool check_user(string login, string email)
    {
        bool check = false;
        try
        {

            SqlCommand sql = new SqlCommand();


            if (!string.IsNullOrEmpty(email))
                sql.CommandText = "SELECT NguoiDungID FROM tblNguoiDung WHERE (UPPER(TenDangNhap)=@TenDangNhap OR UPPER(Email)=@Email) AND (NguoiDungID<>" + Request.QueryString["id"] + ")";
            else
                sql.CommandText = "SELECT NguoiDungID FROM tblNguoiDung WHERE UPPER(TenDangNhap)=@TenDangNhap AND (NguoiDungID<>" + Request.QueryString["id"] + ")";

            sql.Parameters.AddWithValue("@TenDangNhap", login.ToUpper());
            sql.Parameters.AddWithValue("@Email", email.ToUpper());

            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            if (ds.Tables[0].Rows.Count > 0) check = true;
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
        return check;
    }


    protected void Loaddrop_bophancongtac(int pid, int level = 0)
    {
        try
        {

            SqlCoCauToChucProvider CoCauToChuc_provider = new SqlCoCauToChucProvider(cm.connstr, true, "");
            TList<CoCauToChuc> CoCauToChuc_data;

            VACPA.Data.Bases.CoCauToChucParameterBuilder filter = new VACPA.Data.Bases.CoCauToChucParameterBuilder();
            if (pid == 0)
                filter.AppendIsNull(CoCauToChucColumn.CoCauToChucCapTrenId);
            else
                filter.AppendInQuery(CoCauToChucColumn.CoCauToChucCapTrenId, "SELECT CoCauToChucCapTrenID FROM tblCoCauToChuc WHERE CoCauToChucCapTrenID = " + pid.ToString());

            CoCauToChuc_data = CoCauToChuc_provider.Find(filter);



            foreach (CoCauToChuc CoCauToChuc in CoCauToChuc_data)
            {
                string sep = new String('−', level * 2);


                BoPhanCongTac.Controls.Add(new LiteralControl("<option value='" + CoCauToChuc.CoCauToChucId.ToString() + "'>" + sep + CoCauToChuc.TenBoPhan + "</option>"));

                Loaddrop_bophancongtac(CoCauToChuc.CoCauToChucId, level + 1);
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }
}