﻿<%@ control language="C#" autoeventwireup="true" CodeFile="elements.ascx.cs" inherits="usercontrols_elements" %>

<script type="text/javascript" src="js/jquery.jgrowl.js"></script>
<script type="text/javascript" src="js/elements.js"></script>

  <div class="row-fluid">
                    <div class="span6">
                        <h4 class="subtitle2">Growl Notifications</h4>
                        <br />
                        <a id="growl" class="btn"><small>Basic growl</small></a> &nbsp;
                   	<a id="growl2" class="btn"><small>Long live growl message</small></a>
                        
                        <br /><br />
                        <h4 class="subtitle2">Alert Boxes</h4>
                        <br />
                        <a class="btn alertboxbutton"><small>Basic Alert</small></a> &nbsp;
                        <a class="btn confirmbutton"><small>Confirm Box</small></a> &nbsp;
                        <a class="btn promptbutton"><small>Prompt Box</small></a> &nbsp;
                        <a class="btn alerthtmlbutton"><small>Dialog with HTML support</small></a>
                        
                        <br /><br />
                        
                        <a class="btn btn-danger alertdanger"><small>Alert Danger</small></a> &nbsp;
                        <a class="btn btn-warning alertwarning"><small>Alert Warning</small></a> &nbsp;
                        <a class="btn btn-success alertsuccess"><small>Alert Success</small></a> &nbsp;
                        <a class="btn btn-info alertinfo"><small>Alert Info</small></a> &nbsp;
                        <a class="btn btn-inverse alertinverse"><small>Alert Inverse</small></a> &nbsp;
                        
                    </div><!--span6-->
                    
                    <div class="span6">
                        <h4 class="subtitle2">Ajax Loaders</h4>
                        <br />
                        <div class="loaders">
                            <img src="images/loaders/loader1.gif" alt="" />
                            <img src="images/loaders/loader2.gif" alt="" />
                            <img src="images/loaders/loader3.gif" alt="" />
                            <img src="images/loaders/loader4.gif" alt="" />
                            <img src="images/loaders/loader8.gif" alt="" />
                            <img src="images/loaders/loader9.gif" alt="" />
                            <img src="images/loaders/loader5.gif" alt="" />
                            <img src="images/loaders/loader6.gif" alt="" />
                            <img src="images/loaders/loader7.gif" alt="" />
                            <img src="images/loaders/loader10.gif" alt="" />
                            <img src="images/loaders/loader11.gif" alt="" />
                            <img src="images/loaders/loader12.gif" alt="" />
                            <img src="images/loaders/loader13.gif" alt="" />
                            <img src="images/loaders/loader14.gif" alt="" />
                            <img src="images/loaders/loader15.gif" alt="" />
                            <img src="images/loaders/loader16.gif" alt="" />
                            <img src="images/loaders/loader17.gif" alt="" />
                            <img src="images/loaders/loader18.gif" alt="" />
                            <img src="images/loaders/loader19.gif" alt="" />
                            <img src="images/loaders/loader20.gif" alt="" />
                            <img src="images/loaders/loader21.gif" alt="" />
                            <img src="images/loaders/loader22.gif" alt="" />
                            <img src="images/loaders/loader23.gif" alt="" />
                            <img src="images/loaders/loader24.gif" alt="" />
                            <img src="images/loaders/loader25.gif" alt="" />
                            <img src="images/loaders/loader26.gif" alt="" />
                            <img src="images/loaders/loader27.gif" alt="" />
                            <img src="images/loaders/loader28.gif" alt="" />
                            <img src="images/loaders/loader29.gif" alt="" />
                            <img src="images/loaders/loader30.gif" alt="" />                            
                        </div><!--loaders-->
                    </div><!--span6-->
                </div><!--row-fluid-->
                
                <br />
                <h4 class="subtitle2">Tabbed Widgets</h4>
                <br />
                <div class="row-fluid">
                    <div class="span6">
                        <div class="tabbedwidget">
                            <ul>
                                <li><a href="#tabs-1">Tab 1</a></li>
                                <li><a href="#tabs-2">Tab 2</a></li>
                                <li><a href="#tabs-3">Tab 3</a></li>
                            </ul>
                            <div id="tabs-1">
                                Your content goes here for tab 1
                            </div>
                            <div id="tabs-2">
                                Your content goes here for tab 2
                            </div>
                            <div id="tabs-3">
                                Your content goes here for tab 3 
                            </div>
                        </div><!--tabbedwidget-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;tabbedwidget&quot;&gt;&lt;ul&gt;...&lt;/ul&gt;&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="tabbedwidget tab-primary">
                            <ul>
                                <li><a href="#a-1">Tab 1</a></li>
                                <li><a href="#a-2">Tab 2</a></li>
                                <li><a href="#a-3">Tab 3</a></li>
                            </ul>
                            <div id="a-1">
                                Your content goes here for tab 1
                            </div>
                            <div id="a-2">
                                Your content goes here for tab 2
                            </div>
                            <div id="a-3">
                                Your content goes here for tab 3 
                            </div>
                        </div><!--tabbedwidget-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;tabbedwidget tab-primary&quot;&gt;&lt;ul&gt;...&lt;/ul&gt;&lt;/div&gt;</pre>
                        <br />
                        <div class="tabbedwidget tab-danger">
                            <ul>
                                <li><a href="#b-1">Tab 1</a></li>
                                <li><a href="#b-2">Tab 2</a></li>
                                <li><a href="#b-3">Tab 3</a></li>
                            </ul>
                            <div id="b-1">
                                Your content goes here for tab 1
                            </div>
                            <div id="b-2">
                                Your content goes here for tab 2
                            </div>
                            <div id="b-3">
                                Your content goes here for tab 3 
                            </div>
                        </div><!--tabbedwidget-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;tabbedwidget tab-danger&quot;&gt;&lt;ul&gt;...&lt;/ul&gt;&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="tabbedwidget tab-inverse">
                            <ul>
                                <li><a href="#c-1">Tab 1</a></li>
                                <li><a href="#c-2">Tab 2</a></li>
                                <li><a href="#c-3">Tab 3</a></li>
                            </ul>
                            <div id="c-1">
                                Your content goes here for tab 1
                            </div>
                            <div id="c-2">
                                Your content goes here for tab 2
                            </div>
                            <div id="c-3">
                                Your content goes here for tab 3 
                            </div>
                        </div><!--tabbedwidget-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;tabbedwidget tab-inverse&quot;&gt;&lt;ul&gt;...&lt;/ul&gt;&lt;/div&gt;</pre>
                        
                    
                    </div><!--span6-->
                    
                    <div class="span6">
                                                
                        <div class="tabbedwidget tab-warning">
                            <ul>
                                <li><a href="#d-1">Tab 1</a></li>
                                <li><a href="#d-2">Tab 2</a></li>
                                <li><a href="#d-3">Tab 3</a></li>
                            </ul>
                            <div id="d-1">
                                Your content goes here for tab 1
                            </div>
                            <div id="d-2">
                                Your content goes here for tab 2
                            </div>
                            <div id="d-3">
                                Your content goes here for tab 3 
                            </div>
                        </div><!--tabbedwidget-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;tabbedwidget tab-warning&quot;&gt;&lt;ul&gt;...&lt;/ul&gt;&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="tabbedwidget tab-success">
                            <ul>
                                <li><a href="#e-1">Tab 1</a></li>
                                <li><a href="#e-2">Tab 2</a></li>
                                <li><a href="#e-3">Tab 3</a></li>
                            </ul>
                            <div id="e-1">
                                Your content goes here for tab 1
                            </div>
                            <div id="e-2">
                                Your content goes here for tab 2
                            </div>
                            <div id="e-3">
                                Your content goes here for tab 3 
                            </div>
                        </div><!--tabbedwidget-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;tabbedwidget tab-success&quot;&gt;&lt;ul&gt;...&lt;/ul&gt;&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="tabbedwidget tab-info">
                            <ul>
                                <li><a href="#f-1">Tab 1</a></li>
                                <li><a href="#f-2">Tab 2</a></li>
                                <li><a href="#f-3">Tab 3</a></li>
                            </ul>
                            <div id="f-1">
                                Your content goes here for tab 1
                            </div>
                            <div id="f-2">
                                Your content goes here for tab 2
                            </div>
                            <div id="f-3">
                                Your content goes here for tab 3 
                            </div>
                        </div><!--tabbedwidget-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;tabbedwidget tab-info&quot;&gt;&lt;ul&gt;...&lt;/ul&gt;&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="tabbedwidget tab-white">
                            <ul>
                                <li><a href="#g-1">Tab 1</a></li>
                                <li><a href="#g-2">Tab 2</a></li>
                                <li><a href="#g-3">Tab 3</a></li>
                            </ul>
                            <div id="g-1">
                                Your content goes here for tab 1
                            </div>
                            <div id="g-2">
                                Your content goes here for tab 2
                            </div>
                            <div id="g-3">
                                Your content goes here for tab 3 
                            </div>
                        </div><!--tabbedwidget-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;tabbedwidget tab-white&quot;&gt;&lt;ul&gt;...&lt;/ul&gt;&lt;/div&gt;</pre>
                    
                    </div><!--span6-->                    
                </div><!--row-fluid-->
                
                <br />
                <h4 class="subtitle2">Accordion Widgets</h4>
                <br />
                <div class="row-fluid">
                    <div class="span6">
                        <div class="accordion">
                            <h3><a href="#">Section 1</a></h3>
                            <div>
                                <p>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </p>
                            </div>
                            <h3><a href="#">Section 2</a></h3>
                            <div>
                                <p>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                                    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                                    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                                    suscipit faucibus urna.
                                </p>
                            </div>
                            <h3><a href="#">Section 3</a></h3>
                            <div>
                                <p>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                                    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                                    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </p>
                            </div>
                            <h3><a href="#">Section 4</a></h3>
                            <div>
                                <p>
                                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                                    et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                                    faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                                    mauris vel est.
                                </p>
                            </div>
                        </div><!--#accordion-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;accordion&quot;&gt;...&lt;/div&gt;</pre>
                        <br />
                                                
                        <div class="accordion accordion-primary">
                            <h3><a href="#">Section 1</a></h3>
                            <div>
                                <p>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </p>
                            </div>
                            <h3><a href="#">Section 2</a></h3>
                            <div>
                                <p>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                                    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                                    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                                    suscipit faucibus urna.
                                </p>
                            </div>
                            <h3><a href="#">Section 3</a></h3>
                            <div>
                                <p>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                                    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                                    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </p>
                            </div>
                            <h3><a href="#">Section 4</a></h3>
                            <div>
                                <p>
                                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                                    et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                                    faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                                    mauris vel est.
                                </p>
                            </div>
                        </div><!--#accordion-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;accordion accordion-primary&quot;&gt;...&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="accordion accordion-danger">
                            <h3><a href="#">Section 1</a></h3>
                            <div>
                                <p>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </p>
                            </div>
                            <h3><a href="#">Section 2</a></h3>
                            <div>
                                <p>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                                    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                                    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                                    suscipit faucibus urna.
                                </p>
                            </div>
                            <h3><a href="#">Section 3</a></h3>
                            <div>
                                <p>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                                    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                                    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </p>
                            </div>
                            <h3><a href="#">Section 4</a></h3>
                            <div>
                                <p>
                                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                                    et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                                    faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                                    mauris vel est.
                                </p>
                            </div>
                        </div><!--#accordion-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;accordion accordion-danger&quot;&gt;...&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="accordion accordion-inverse">
                            <h3><a href="#">Section 1</a></h3>
                            <div>
                                <p>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </p>
                            </div>
                            <h3><a href="#">Section 2</a></h3>
                            <div>
                                <p>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                                    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                                    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                                    suscipit faucibus urna.
                                </p>
                            </div>
                            <h3><a href="#">Section 3</a></h3>
                            <div>
                                <p>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                                    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                                    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </p>
                            </div>
                            <h3><a href="#">Section 4</a></h3>
                            <div>
                                <p>
                                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                                    et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                                    faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                                    mauris vel est.
                                </p>
                            </div>
                        </div><!--#accordion-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;accordion accordion-inverse&quot;&gt;...&lt;/div&gt;</pre>
                    
                    </div><!--span6-->
                    
                    <div class="span6">
                        
                        <div class="accordion accordion-warning">
                            <h3><a href="#">Section 1</a></h3>
                            <div>
                                <p>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </p>
                            </div>
                            <h3><a href="#">Section 2</a></h3>
                            <div>
                                <p>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                                    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                                    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                                    suscipit faucibus urna.
                                </p>
                            </div>
                            <h3><a href="#">Section 3</a></h3>
                            <div>
                                <p>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                                    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                                    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </p>
                            </div>
                            <h3><a href="#">Section 4</a></h3>
                            <div>
                                <p>
                                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                                    et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                                    faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                                    mauris vel est.
                                </p>
                            </div>
                        </div><!--#accordion-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;accordion accordion-warning&quot;&gt;...&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="accordion accordion-success">
                            <h3><a href="#">Section 1</a></h3>
                            <div>
                                <p>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </p>
                            </div>
                            <h3><a href="#">Section 2</a></h3>
                            <div>
                                <p>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                                    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                                    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                                    suscipit faucibus urna.
                                </p>
                            </div>
                            <h3><a href="#">Section 3</a></h3>
                            <div>
                                <p>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                                    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                                    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </p>
                            </div>
                            <h3><a href="#">Section 4</a></h3>
                            <div>
                                <p>
                                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                                    et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                                    faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                                    mauris vel est.
                                </p>
                            </div>
                        </div><!--#accordion-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;accordion accordion-success&quot;&gt;...&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="accordion accordion-info">
                            <h3><a href="#">Section 1</a></h3>
                            <div>
                                <p>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </p>
                            </div>
                            <h3><a href="#">Section 2</a></h3>
                            <div>
                                <p>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                                    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                                    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                                    suscipit faucibus urna.
                                </p>
                            </div>
                            <h3><a href="#">Section 3</a></h3>
                            <div>
                                <p>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                                    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                                    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </p>
                            </div>
                            <h3><a href="#">Section 4</a></h3>
                            <div>
                                <p>
                                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                                    et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                                    faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                                    mauris vel est.
                                </p>
                            </div>
                        </div><!--#accordion-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;accordion accordion-info&quot;&gt;...&lt;/div&gt;</pre>
                        <br />
                        
                        <div class="accordion accordion-white">
                            <h3><a href="#">Section 1</a></h3>
                            <div>
                                <p>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                                    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                                    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                                    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </p>
                            </div>
                            <h3><a href="#">Section 2</a></h3>
                            <div>
                                <p>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                                    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                                    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                                    suscipit faucibus urna.
                                </p>
                            </div>
                            <h3><a href="#">Section 3</a></h3>
                            <div>
                                <p>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                                    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                                    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </p>
                            </div>
                            <h3><a href="#">Section 4</a></h3>
                            <div>
                                <p>
                                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                                    et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                                    faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                                    mauris vel est.
                                </p>
                            </div>
                        </div><!--#accordion-->
                        
                        <pre class="prettyprint linenums">&lt;div class=&quot;accordion accordion-white&quot;&gt;...&lt;/div&gt;</pre>
                        
                    </div><!--span6-->
                </div><!--row-fluid-->

