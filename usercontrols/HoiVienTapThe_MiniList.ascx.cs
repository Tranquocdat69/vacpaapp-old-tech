﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Project_VACPA.LibraryCode;

namespace Project_VACPA.usercontrols
{
    public partial class HoiVienTapThe_MiniList : System.Web.UI.UserControl
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// Created by NGUYEN MANH HUNG - 2014/11/28
        /// Load danh sách "Công ty" theo điều kiện Search
        /// </summary>
        private void LoadListCongTy()
        {
            Db db = new Db(ListName.ConnectionString);
            try
            {
                db.OpenConnection();
                string query = @"SELECT HoiVienTapTheID, MaHoiVienTapThe, TenDoanhNghiep, TenVietTat, TenTiengAnh, DiaChi, MaSoThue, 
                                SoGiayChungNhanDKKD, NgayGiaNhap, NgayCapGiayChungNhanDKKD, NguoiDaiDienLL_Ten, NguoiDaiDienLL_DiDong, NguoiDaiDienLL_Email
	                            FROM tblHoiVienTapThe
	                            WHERE {MaHoiVienTapThe} AND (TenDoanhNghiep LIKE N'%" + txtTenCongTy.Text + @"%' OR TenTiengAnh LIKE N'%" + txtTenCongTy.Text + @"%')
	                            AND {LoaiHoiVienTapThe} ORDER BY TenDoanhNghiep";

                query = !string.IsNullOrEmpty(txtMaCongTy.Text)
                            ? query.Replace("{MaHoiVienTapThe}", "MaHoiVienTapThe = '" + txtMaCongTy.Text + "'")
                            : query.Replace("{MaHoiVienTapThe}", "1=1");
                query = ddlPhanLoai.SelectedValue != ""
                            ? query.Replace("{LoaiHoiVienTapThe}", "LoaiHoiVienTapThe = '" + ddlPhanLoai.SelectedValue + "'")
                            : query.Replace("{LoaiHoiVienTapThe}", "1=1");
                DataTable dt = db.GetDataTable(query);
                rpCongTy.DataSource = dt.DefaultView;
                rpCongTy.DataBind();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                db.CloseConnection();
            }
        }

        /// <summary>
        /// Created by NGUYEN MANH HUNG - 2014/11/28
        /// Su kien khi bam nut "Tim kiem"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtSearch_Click(object sender, EventArgs e)
        {
            LoadListCongTy();
        }
    }
}