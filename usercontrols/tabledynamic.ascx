﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="tabledynamic.ascx.cs" Inherits="usercontrols_tabledynamic" %>




<form runat="server">
   <h4 class="widgettitle">Bảng có phân trang (ASP.NET control)</h4>

      <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" class="table table-bordered responsive" 
       AllowPaging="True">
    <Columns>
        <asp:BoundField DataField="KY_HIEU" HeaderText="Ký hiệu" 
            SortExpression="KY_HIEU" />
        <asp:BoundField DataField="TEN_CHI_TIEU_VN" HeaderText="Tên chỉ tiêu" 
            SortExpression="TEN_CHI_TIEU_VN" />
        <asp:BoundField DataField="DON_VI_TINH" HeaderText="Đơn vị tính" 
            SortExpression="DON_VI_TINH" />
        
    </Columns>
</asp:GridView>
            	<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:TestConnectionString %>" 
    SelectCommand="SELECT TOP 100 [KY_HIEU], [TEN_CHI_TIEU_VN], [DON_VI_TINH] FROM [CG_HE_THONG_CHI_TIEU]">
</asp:SqlDataSource>
<br /><br /><br />

<h4 class="widgettitle">Bảng có phân trang (HTML control)</h4>
                <table id="dyntable" class="table table-bordered">
                   
                    <thead>
                        <tr>
                          	<th width="3%" class="head0"><input type="checkbox"  class="checkall" value="all" /></th>
                            <th width="15%" class="head1">Rendering engine</th>
                            <th width="19%" class="head0">Browser</th>
                            <th width="17%" class="head1">Platform(s)</th>
                            <th width="13%" class="head0">Engine version</th>
                            <th width="10%" class="head1">CSS grade</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="gradeX">
                          <td class="aligncenter"><span class="center">
                            <input type="checkbox" class="colcheckbox" value="1" />
                          </span></td>
                            <td>Trident</td>
                            <td>Internet Explorer 4.0</td>
                            <td>Win 95+</td>
                            <td class="center">4</td>
                            <td class="center">X</td>
                        </tr>
                        <tr class="gradeC">
                          <td class="aligncenter"><span class="center">
                            <input type="checkbox" class="colcheckbox" value="2"  />
                          </span></td>
                            <td>Trident</td>
                            <td>Internet Explorer 5.0</td>
                            <td>Win 95+</td>
                            <td class="center">5</td>
                            <td class="center">C</td>
                        </tr>
                        <tr class="gradeA">
                          <td class="aligncenter"><span class="center">
                            <input type="checkbox" class="colcheckbox" value="3"  />
                          </span></td>
                            <td>Trident</td>
                            <td>Internet Explorer 5.5</td>
                            <td>Win 95+</td>
                            <td class="center">5.5</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td class="aligncenter"><span class="center">
                            <input type="checkbox" class="colcheckbox"  value="4"  />
                          </span></td>
                            <td>Trident</td>
                            <td>Internet Explorer 6</td>
                            <td>Win 98+</td>
                            <td class="center">6</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td class="aligncenter"><span class="center">
                            <input type="checkbox" class="colcheckbox"  value="5"  />
                          </span></td>
                            <td>Trident</td>
                            <td>Internet Explorer 7</td>
                            <td>Win XP SP2+</td>
                            <td class="center">7</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td class="aligncenter"><span class="center">
                            <input type="checkbox" class="colcheckbox"  value="6"  />
                          </span></td>
                            <td>Trident</td>
                            <td>AOL browser (AOL desktop)</td>
                            <td>Win XP</td>
                            <td class="center">6</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td class="aligncenter"><span class="center">
                            <input type="checkbox" class="colcheckbox"  value="7"  />
                          </span></td>
                            <td>Gecko</td>
                            <td>Firefox 1.0</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td class="center">1.7</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td class="aligncenter"><span class="center">
                            <input type="checkbox" class="colcheckbox" value="8"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Firefox 1.5</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td class="center">1.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="9"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Firefox 2.0</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td class="center">1.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="10"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Firefox 3.0</td>
                            <td>Win 2k+ / OSX.3+</td>
                            <td class="center">1.9</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="11"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Camino 1.0</td>
                            <td>OSX.2+</td>
                            <td class="center">1.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="12"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Camino 1.5</td>
                            <td>OSX.3+</td>
                            <td class="center">1.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="13"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Netscape 7.2</td>
                            <td>Win 95+ / Mac OS 8.6-9.2</td>
                            <td class="center">1.7</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="14"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Netscape Browser 8</td>
                            <td>Win 98SE+</td>
                            <td class="center">1.7</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="15"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Netscape Navigator 9</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td class="center">1.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="16"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.0</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td class="center">1</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="17"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.1</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td class="center">1.1</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="18"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.2</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td class="center">1.2</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="19"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.3</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td class="center">1.3</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="20"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.4</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td class="center">1.4</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" value="21"   />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.5</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td class="center">1.5</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.6</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td class="center">1.6</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.7</td>
                            <td>Win 98+ / OSX.1+</td>
                            <td class="center">1.7</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Gecko</td>
                            <td>Mozilla 1.8</td>
                            <td>Win 98+ / OSX.1+</td>
                            <td class="center">1.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Gecko</td>
                            <td>Seamonkey 1.1</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td class="center">1.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Gecko</td>
                            <td>Epiphany 2.20</td>
                            <td>Gnome</td>
                            <td class="center">1.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Webkit</td>
                            <td>Safari 1.2</td>
                            <td>OSX.3</td>
                            <td class="center">125.5</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Webkit</td>
                            <td>Safari 1.3</td>
                            <td>OSX.3</td>
                            <td class="center">312.8</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Webkit</td>
                            <td>Safari 2.0</td>
                            <td>OSX.4+</td>
                            <td class="center">419.3</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Webkit</td>
                            <td>Safari 3.0</td>
                            <td>OSX.4+</td>
                            <td class="center">522.1</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Webkit</td>
                            <td>OmniWeb 5.5</td>
                            <td>OSX.4+</td>
                            <td class="center">420</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Webkit</td>
                            <td>iPod Touch / iPhone</td>
                            <td>iPod</td>
                            <td class="center">420.1</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Webkit</td>
                            <td>S60</td>
                            <td>S60</td>
                            <td class="center">413</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Opera 7.0</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Opera 7.5</td>
                            <td>Win 95+ / OSX.2+</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Opera 8.0</td>
                            <td>Win 95+ / OSX.2+</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Opera 8.5</td>
                            <td>Win 95+ / OSX.2+</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Opera 9.0</td>
                            <td>Win 95+ / OSX.3+</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Opera 9.2</td>
                            <td>Win 88+ / OSX.3+</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Opera 9.5</td>
                            <td>Win 88+ / OSX.3+</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Opera for Wii</td>
                            <td>Wii</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Nokia N800</td>
                            <td>N800</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Presto</td>
                            <td>Nintendo DS browser</td>
                            <td>Nintendo DS</td>
                            <td class="center">8.5</td>
                            <td class="center">C/A<sup>1</sup></td>
                        </tr>
                        <tr class="gradeC">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>KHTML</td>
                            <td>Konqureror 3.1</td>
                            <td>KDE 3.1</td>
                            <td class="center">3.1</td>
                            <td class="center">C</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>KHTML</td>
                            <td>Konqureror 3.3</td>
                            <td>KDE 3.3</td>
                            <td class="center">3.3</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>KHTML</td>
                            <td>Konqureror 3.5</td>
                            <td>KDE 3.5</td>
                            <td class="center">3.5</td>
                            <td class="center">A</td>
                        </tr>
                        <tr class="gradeX">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Tasman</td>
                            <td>Internet Explorer 4.5</td>
                            <td>Mac OS 8-9</td>
                            <td class="center">-</td>
                            <td class="center">X</td>
                        </tr>
                        <tr class="gradeC">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Tasman</td>
                            <td>Internet Explorer 5.1</td>
                            <td>Mac OS 7.6-9</td>
                            <td class="center">1</td>
                            <td class="center">C</td>
                        </tr>
                        <tr class="gradeC">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Tasman</td>
                            <td>Internet Explorer 5.2</td>
                            <td>Mac OS 8-X</td>
                            <td class="center">1</td>
                            <td class="center">C</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Misc</td>
                            <td>NetFront 3.1</td>
                            <td>Embedded devices</td>
                            <td class="center">-</td>
                            <td class="center">C</td>
                        </tr>
                        <tr class="gradeA">
                          <td><span class="center">
                            <input type="checkbox" class="colcheckbox" />
                          </span></td>
                            <td>Misc</td>
                            <td>NetFront 3.4</td>
                            <td>Embedded devices</td>
                            <td class="center">-</td>
                            <td class="center">A</td>
                        </tr>
                    </tbody>
                </table>
                
                <br /><br />
                
<input type="button" value="Checked ID" onClick="alert(dyntable_selected);">

<script type="text/javascript">

    var dyntable_selected = new Array();

    var dyntable2_selected = new Array();







    jQuery(document).ready(function () {
        // dynamic table
        var dyntable_oTable = jQuery('#dyntable').dataTable({
            "sPaginationType": "full_numbers",
            "aaSortingFixed": [[0, 'asc']]
        });



        $("#dyntable .checkall").bind("click", function () {
            dyntable_selected = [];
            checked = $(this).prop("checked");
            $("input[type=checkbox]", dyntable_oTable.fnGetNodes()).each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) dyntable_selected.push($(this).val());
            });
        });

        $("input[type=checkbox]", dyntable_oTable.fnGetNodes()).each(function () {
            $(this).bind("click", function () {
                removeItem(dyntable_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) dyntable_selected.push($(this).val());
            });
        });




    });
	
	

</script>

</form>