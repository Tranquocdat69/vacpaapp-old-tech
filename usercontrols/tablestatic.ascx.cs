﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_tablestatic : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Tên chức năng
        string tenchucnang = "Bảng cố định";
        // Icon CSS Class  
        string IconClass = "iconfa-table";

        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"
     <li>Bảng biểu <span class=""separator""></span></li> <!-- Nhóm chức năng cấp trên -->
     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));            
         

    }
}