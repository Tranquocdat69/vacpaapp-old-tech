﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Project_VACPA.LibraryCode;

namespace Project_VACPA.usercontrols
{
    public partial class Export_Process : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string content = Request.Form["hdContent"];
            string typeExport = Request.Form["hdTypeExport"];
            string fileName = Request.Form["hdFileName"];
            if(!string.IsNullOrEmpty(content))
            {
                if(typeExport.ToLower() == "excel")
                {
                    Library.ExportToExcel(fileName, content);
                }
            }
        }
    }
}