﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_thongbaonophoiphi : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dtb = new DataTable();

    public string quyen = "ThongBaoNopHoiPhi";

    public string tenchucnang = "Thông báo nộp hội phí";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";
    public int iTinhTrang = 0;

    public string id_canhan = "HoiVienCaNhanId";
    public string ma_canhan = "MaHoiVienCaNhan";
    public string stt_canhan = "STT";
    public string ten_canhan = "HoVaTen";
    public string sochungchi_canhan = "SoChungChiKTV";
    public string ngaycapchungchi_canhan = "NgayCapChungChiKTV";
    public string donvicongtac_canhan = "DonViCongTac";
    public string email_canhan = "Email";
    public string hoiphiphainopnamnay_canhan = "HoiPhiPhaiNopNamNay";
    public string hoiphiconnonamtruoc_canhan = "HoiPhiConNoNamTruoc";
    public string tonghoiphiphainop_canhan = "TongHoiPhiPhaiNop";
    public string tonghoiphidanop_canhan = "TongHoiPhiDaNop";
    public string tonghoiphiconphainop_canhan = "TongHoiPhiConPhaiNop";
    public int iPhiIDCaNhan;
    public string PhiIDCaNhan = "";
    public string strThoiHanCaNhan = "";

    public string id_congty = "HoiVienTapTheId";
    public string ma_tapthe = "MaHoiVienTapThe";
    public string stt_congty = "STT";
    public string ten_congty = "TenDoanhNghiep";
    public string tenviettat_congty = "TenVietTat";
    public string email_tapthe = "Email";
    public string hoiphiphainopnamnay_congty = "HoiPhiPhaiNopNamNay";
    public string hoiphiconnonamtruoc_congty = "HoiPhiConNoNamTruoc";
    public string tonghoiphiphainop_congty = "TongHoiPhiPhaiNop";
    public string tonghoiphidanop_congty = "TongHoiPhiDaNop";
    public string tonghoiphiconphainop_congty = "TongHoiPhiConPhaiNop";
    public int iPhiIDTapThe;
    public string PhiIDTapThe = "";
    public string strThoiHanTapThe = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));


            if (!string.IsNullOrEmpty(Request.Form["ThoiHanNopPhiCaNhan"]))
                strThoiHanCaNhan = Request.Form["ThoiHanNopPhiCaNhan"].ToString();
            if (string.IsNullOrEmpty(Request.QueryString["thongbao"]))
            {
                LoadThongTin();
            }
            else if (Request.QueryString["thongbao"] == "thanhcong")
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script>");
                sb.Append(@"jQuery(document).ready(function () {");
                sb.Append(@"  thongbaothanhcong();}) ");
                sb.Append(@"</script>");
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
                LoadThongTin();
            }
            else if (Request.QueryString["thongbao"] == "loi")
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script>");
                sb.Append(@"jQuery(document).ready(function () {");
                sb.Append(@"  thongbaoloi();}) ");
                sb.Append(@"</script>");
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
                LoadThongTin();
            }


            //if (!string.IsNullOrEmpty(Request.Form["guimail"]))
            //{
            //    if (Request.Form["guimail"] == "1")
            //    {
            //        v_guimailthongbaonophoiphi();
            //    }
            //}
            //else
            //    LoadThongTin();

        }
        catch
        { }
    }

//    protected void v_guimailthongbaonophoiphi()
//    {
//        try
//        {
//            string sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

//            string mess = "";
//            string body = "";
//            SqlCommand sql = new SqlCommand();
//            int PhiIDCaNhan = 0;
//            string strPhiIdCaNhan = Request.QueryString["phiidcanhan"];
//            if (!string.IsNullOrEmpty(Request.QueryString["phiidcanhan"]))
//            {
//                try
//                {
//                    PhiIDCaNhan = Convert.ToInt32(Request.QueryString["phiidcanhan"]);
//                }
//                catch
//                {
//                    PhiIDCaNhan = 0;
//                }
//            }
//            string ThoiHanCaNhan = strThoiHanCaNhan;

//            string dsmailcanhan = "";
//            if (string.IsNullOrEmpty(Request.QueryString["emailcanhan"]))
//                dsmailcanhan = "0";
//            else
//                dsmailcanhan = Request.QueryString["emailcanhan"];
//            //sql.CommandText = "SELECT DISTINCT EMAIL FROM TBLHOIVIENCANHAN WHERE HOIVIENCANHANID IN (" + Request.QueryString["emailcanhan"] + ")";
//            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_CaNhan]" + PhiIDCaNhan + ", " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", '','" + dsmailcanhan + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
//            DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
//            if (dtb.Rows.Count > 0)
//            {
//                for (int i = 0; i < dtb.Rows.Count; i++)
//                {
//                    if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
//                    {
//                        string HoiPhiPhaiNopNamNay = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["HoiPhiPhaiNopNamNay"]));
//                        string HoiPhiConNoNamTruoc = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["HoiPhiConNoNamTruoc"]));
//                        string TongHoiPhiPhaiNop = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiPhaiNop"]));
//                        string TongHoiPhiDaNop = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiDaNop"]));
//                        string TongHoiPhiConPhaiNop = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiConPhaiNop"]));

//                        body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
//                        body += @"VACPA xin thông báo đến anh/chị tình hình thanh toán phí như sau: <br>
//    -	Loại phí: Hội phí  <br>
//    -	Số phí phải nộp năm nay: " + HoiPhiPhaiNopNamNay + @"<br>
//    -	Số phí còn nợ năm trước: " + HoiPhiConNoNamTruoc + @"<br>
//    -	Tổng số phí phải nộp: " + TongHoiPhiPhaiNop + @"<br>
//    -	Số phí đã nộp: " + TongHoiPhiDaNop + @"<br>
//    -	Số phí còn phải nộp: " + TongHoiPhiConPhaiNop + @"<br>
//    Mời anh/chị thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHanCaNhan;
//                        //SmtpMail.Send(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, ref mess);
//                        cm.ghilog("ThongBaoNopHoiPhi", mess);
//                    }
//                }
//            }

//            sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

//            mess = "";
//            sql = new SqlCommand();
//            int PhiIDTapThe = 0;
//            string strPhiIdTapThe = Request.QueryString["phiidtapthe"];
//            if (!string.IsNullOrEmpty(Request.QueryString["phiidtapthe"]))
//            {
//                try
//                {
//                    PhiIDTapThe = Convert.ToInt32(Request.QueryString["phiidtapthe"]);
//                }
//                catch
//                {
//                    PhiIDTapThe = 0;
//                }
//            }
//            string strThoiHanTapThe = Request.QueryString["thoihantapthe"];

//            string dsmailtapthe = "";
//            if (string.IsNullOrEmpty(Request.QueryString["emailtapthe"]))
//                dsmailtapthe = "0";
//            else
//                dsmailtapthe = Request.QueryString["emailtapthe"];
//            //sql.CommandText = "SELECT DISTINCT EMAIL FROM TBLHOIVIENCANHAN WHERE HOIVIENCANHANID IN (" + Request.QueryString["emailcanhan"] + ")";
//            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_TapThe]" + PhiIDTapThe + ", " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienTapThe + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", '','" + dsmailtapthe + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
//            dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
//            if (dtb.Rows.Count > 0)
//            {
//                for (int i = 0; i < dtb.Rows.Count; i++)
//                {
//                    if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
//                    {
//                        string HoiPhiPhaiNopNamNay = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["HoiPhiPhaiNopNamNay"]));
//                        string HoiPhiConNoNamTruoc = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["HoiPhiConNoNamTruoc"]));
//                        string TongHoiPhiPhaiNop = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiPhaiNop"]));
//                        string TongHoiPhiDaNop = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiDaNop"]));
//                        string TongHoiPhiConPhaiNop = string.Format("{0:n2}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiConPhaiNop"]));


//                        body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
//                        body += @"VACPA xin thông báo đến quý công ty tình hình thanh toán phí như sau: <br>
//    -	Loại phí: Hội phí  <br>
//    -	Số phí phải nộp năm nay: " + HoiPhiPhaiNopNamNay + @"<br>
//    -	Số phí còn nợ năm trước: " + HoiPhiConNoNamTruoc + @"<br>
//    -	Tổng số phí phải nộp: " + TongHoiPhiPhaiNop + @"<br>
//    -	Số phí đã nộp: " + TongHoiPhiDaNop + @"<br>
//    -	Số phí còn phải nộp: " + TongHoiPhiConPhaiNop + @"<br>
//    Mời quý công ty thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHanTapThe;
//                        //SmtpMail.Send(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, ref mess);
//                        cm.ghilog("ThongBaoNopHoiPhi", mess);
//                    }
//                }
//            }
//            cm.ghilog("ThongBaoNopHoiPhi", "Gửi mail thông báo nộp phí hội viên thành công");
//            Response.Redirect("admin.aspx?page=thongbaonophoiphi&thongbao=thanhcong", false);
//        }
//        catch (Exception ex)
//        {
//            cm.ghilog("ThongBaoNopHoiPhi", "Lỗi xảy ra khi gửi mail thông báo nộp phí hội viên: " + ex.ToString());
//            Response.Redirect("admin.aspx?page=thongbaonophoiphi&thongbao=loi", false);
//        }
//    }

    protected void LoadThongTin()
    {
        try
        {
            grvThongBaoPhiCongTy.Columns[1].HeaderText = "STT";
            grvThongBaoPhiCongTy.Columns[1].SortExpression = stt_congty;
            grvThongBaoPhiCongTy.Columns[2].HeaderText = "ID HVTC";
            grvThongBaoPhiCongTy.Columns[2].SortExpression = ma_tapthe;
            grvThongBaoPhiCongTy.Columns[3].HeaderText = "Tên công ty";
            grvThongBaoPhiCongTy.Columns[3].SortExpression = ten_congty;
            grvThongBaoPhiCongTy.Columns[4].HeaderText = "Tên viết tắt";
            grvThongBaoPhiCongTy.Columns[4].SortExpression = tenviettat_congty;
            grvThongBaoPhiCongTy.Columns[5].HeaderText = "Hội phí phải nộp năm nay";
            grvThongBaoPhiCongTy.Columns[5].SortExpression = hoiphiphainopnamnay_congty;
            grvThongBaoPhiCongTy.Columns[6].HeaderText = "Hội phí còn nợ năm trước";
            grvThongBaoPhiCongTy.Columns[6].SortExpression = hoiphiconnonamtruoc_congty;
            grvThongBaoPhiCongTy.Columns[7].HeaderText = "Tổng hội phí phải nộp";
            grvThongBaoPhiCongTy.Columns[7].SortExpression = tonghoiphiphainop_congty;
            grvThongBaoPhiCongTy.Columns[8].HeaderText = "Tổng hội phí đã nộp";
            grvThongBaoPhiCongTy.Columns[8].SortExpression = tonghoiphidanop_congty;
            grvThongBaoPhiCongTy.Columns[9].HeaderText = "Tổng hội phí còn phải nộp";
            grvThongBaoPhiCongTy.Columns[9].SortExpression = tonghoiphiconphainop_congty;

            grvthongbaophihoivien_canhan.Columns[1].HeaderText = "STT";
            grvthongbaophihoivien_canhan.Columns[1].SortExpression = stt_canhan;
            grvthongbaophihoivien_canhan.Columns[2].HeaderText = "ID HVCN";
            grvthongbaophihoivien_canhan.Columns[2].SortExpression = ma_canhan;
            grvthongbaophihoivien_canhan.Columns[3].HeaderText = "Họ và tên";
            grvthongbaophihoivien_canhan.Columns[3].SortExpression = ten_canhan;
            grvthongbaophihoivien_canhan.Columns[4].HeaderText = "Số chứng chỉ KTV";
            grvthongbaophihoivien_canhan.Columns[4].SortExpression = sochungchi_canhan;
            grvthongbaophihoivien_canhan.Columns[5].HeaderText = "Ngày cấp chứng chỉ KTV";
            grvthongbaophihoivien_canhan.Columns[5].SortExpression = ngaycapchungchi_canhan;
            grvthongbaophihoivien_canhan.Columns[6].HeaderText = "Đơn vị công tác";
            grvthongbaophihoivien_canhan.Columns[6].SortExpression = donvicongtac_canhan;
            grvthongbaophihoivien_canhan.Columns[7].HeaderText = "Hội phí phải nộp năm nay";
            grvthongbaophihoivien_canhan.Columns[7].SortExpression = hoiphiphainopnamnay_canhan;
            grvthongbaophihoivien_canhan.Columns[8].HeaderText = "Hội phí còn nợ năm trước";
            grvthongbaophihoivien_canhan.Columns[8].SortExpression = hoiphiconnonamtruoc_canhan;
            grvthongbaophihoivien_canhan.Columns[9].HeaderText = "Tổng hội phí phải nộp";
            grvthongbaophihoivien_canhan.Columns[9].SortExpression = tonghoiphiphainop_canhan;
            grvthongbaophihoivien_canhan.Columns[10].HeaderText = "Tổng hội phí đã nộp";
            grvthongbaophihoivien_canhan.Columns[10].SortExpression = tonghoiphidanop_canhan;
            grvthongbaophihoivien_canhan.Columns[11].HeaderText = "Tổng hội phí còn phải nộp";
            grvthongbaophihoivien_canhan.Columns[11].SortExpression = tonghoiphiconphainop_canhan;

            int i;
            Commons cm = new Commons();

            SqlCommand sql = new SqlCommand();
            sql.CommandTimeout = 120;
            //Lấy phí tổ chức
            sql.CommandText = "SELECT PhiID, ThoiHanNgay, ThoiHanThang from tblTTDMPhiHoiVien A left join tblDMDoiTuongNopPhi B on A.DoiTuongApDungID = b.DoiTuongNopPhiID where A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND B.LoaiDoiTuong = " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienTapThe + "  AND A.NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND ((CASE WHEN A.NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE A.NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "') order by ngayduyet DESC";
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            if (dtbTemp.Rows.Count != 0)
            {
                iPhiIDTapThe = 0;
                for (int k = 0; k < dtbTemp.Rows.Count; k++)
                {
                    iPhiIDTapThe = int.Parse(dtbTemp.Rows[k][0].ToString());
                    //if (dtbTemp.Rows.Count > 0)
                    //{
                    //    iPhiIDTapThe = Convert.ToInt32(dtbTemp.Rows[0][0]);
                    //    string Ngay = dtbTemp.Rows[0]["ThoiHanNgay"].ToString();
                    //    if (string.IsNullOrEmpty(Ngay))
                    //        Ngay = DateTime.Now.Day.ToString();
                    //    if (Ngay.Length == 1)
                    //        Ngay = "0" + Ngay;

                    //    string Thang = dtbTemp.Rows[0]["ThoiHanThang"].ToString();
                    //    if (string.IsNullOrEmpty(Thang))
                    //        Thang = DateTime.Now.Month.ToString();
                    //    if (Thang.Length == 1)
                    //        Thang = "0" + Thang;
                    //    strThoiHanTapThe= Ngay + "/" + Thang + "/" + DateTime.Now.Year.ToString();
                    //}
                    //else
                    //    strThoiHanTapThe = DateTime.Now.ToString("dd/MM/yyyy");
                    if (iPhiIDTapThe != 0)
                    {
                        PhiIDTapThe += iPhiIDTapThe.ToString() + ";";
                        // Response.Write("$('#ThoiHanNopPhiTapThe').val('" + Convert.ToDateTime(dtbTemp.Rows[0]["NgayApDung"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                        // ThoiHanNopPhiTapThe.Value = Convert.ToDateTime(dtbTemp.Rows[0]["NgayApDung"]).ToString("dd/MM/yyyy");
                        string TimKiemTapThe = "";
                        if (!string.IsNullOrEmpty(Request.Form["TimKiemTapThe"]))
                            TimKiemTapThe = "%" + Request.Form["TimKiemTapThe"] + "%";
                        sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_TapThe]" + iPhiIDTapThe + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ",N'" + TimKiemTapThe + "', ''," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                        if (k == 0)
                            dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                        else
                            dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                        sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_TapThe]" + iPhiIDTapThe + "," + (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ",N'" + TimKiemTapThe + "', ''," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                        dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);



                    }
                }
                PhiIDTapThe = PhiIDTapThe.TrimEnd(';');
                //if (dtb.Rows.Count == 0)
                //{
                //    PagerTapThe.Enabled = false;
                //    return;
                //}
                //else
                //    PagerTapThe.Enabled = true;
                DataView dv_tt = dtb.DefaultView;
                dv_tt.Sort = "SoHieu ASC";
                //if (ViewState["sortexpressioncty"] != null)
                //    dv_tt.Sort = ViewState["sortexpressioncty"].ToString() + " " + ViewState["sortdirectioncty"].ToString();
                //else
                //dv_tt.Sort = "STT ASC";
                //dtb = dv.ToTable();
                // Phân trang
                PagedDataSource objPds_tt = new PagedDataSource();
                objPds_tt.DataSource = dv_tt;
                objPds_tt.AllowPaging = true;
                objPds_tt.PageSize = 99999;

                // danh sách trang
                for (i = 0; i < objPds_tt.PageCount; i++)
                {
                    ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                    if (!PagerTapThe.Items.Contains(a))
                        PagerTapThe.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
                }
                // Chuyển trang
                if (!string.IsNullOrEmpty(Request.Form["tranghientaitapthe"]))
                {
                    //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                    if (Convert.ToInt32(Request.Form["tranghientaitapthe"]) <= objPds_tt.PageCount)
                    {
                        objPds_tt.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientaitapthe"]);
                        PagerTapThe.SelectedIndex = Convert.ToInt32(Request.Form["tranghientaitapthe"]);
                    }
                    else
                    {
                        objPds_tt.CurrentPageIndex = 0;
                        PagerTapThe.SelectedIndex = 0;
                    }
                }

                //kết xuất danh sách ra excel
                if (!string.IsNullOrEmpty(Request.Form["ketxuattapthe"]))
                {
                    if (Request.Form["ketxuattapthe"] == "1")
                    {
                        try
                        {
                            Response.Clear();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();

                            string FileName = quyen + "TapThe" + DateTime.Now + ".xls";
                            StringWriter strwritter = new StringWriter();
                            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                            Response.ContentEncoding = System.Text.Encoding.UTF8;
                            Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                            objPds_tt.AllowPaging = false;

                            GridView exportgrid = new GridView();

                            exportgrid.DataSource = objPds_tt;
                            exportgrid.DataBind();
                            exportgrid.RenderControl(htmltextwrtter);
                            exportgrid.Dispose();

                            Response.Write(strwritter.ToString());
                            Response.End();
                        }
                        catch (System.Threading.ThreadAbortException a)
                        { }
                    }
                }
                grvThongBaoPhiCongTy.DataSource = dv_tt;
                grvThongBaoPhiCongTy.DataBind();
            }

            //Lấy phí cá nhân
            sql.CommandText = "SELECT PhiID, ThoiHanThang, ThoiHanNgay from tblTTDMPhiHoiVien A left join tblDMDoiTuongNopPhi B on A.DoiTuongApDungID = b.DoiTuongNopPhiID where A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND B.LoaiDoiTuong = " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + "  AND A.NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND ((CASE WHEN A.NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE A.NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "') order by ngayduyet DESC";
            DataTable dtbTempCaNhan = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            if (dtbTempCaNhan.Rows.Count != 0)
            {
                iPhiIDCaNhan = 0;
                for (int j = 0; j < dtbTempCaNhan.Rows.Count; j++)
                {
                    iPhiIDCaNhan = int.Parse(dtbTempCaNhan.Rows[j][0].ToString());
                    //if (dtbTempCaNhan.Rows.Count > 0)
                    //{
                    //    iPhiIDCaNhan = Convert.ToInt32(dtbTempCaNhan.Rows[0][0]);
                    //    string Ngay = dtbTempCaNhan.Rows[0]["ThoiHanNgay"].ToString();
                    //    if (string.IsNullOrEmpty(Ngay))
                    //        Ngay = DateTime.Now.Day.ToString();
                    //    if (Ngay.Length == 1)
                    //        Ngay = "0" + Ngay;

                    //    string Thang = dtbTempCaNhan.Rows[0]["ThoiHanThang"].ToString();
                    //    if (string.IsNullOrEmpty(Thang))
                    //        Thang = DateTime.Now.Month.ToString();
                    //    if (Thang.Length == 1)
                    //        Thang = "0" + Thang;
                    //    strThoiHanCaNhan = Ngay + "/" + Thang + "/" + DateTime.Now.Year.ToString();
                    //}
                    //else
                    //    strThoiHanCaNhan = DateTime.Now.ToString("dd/MM/yyyy");
                    if (iPhiIDCaNhan != 0)
                    {
                        PhiIDCaNhan += iPhiIDCaNhan.ToString() + ";";
                        //System.Web.HttpContext.Current.Response.Write("$('#ThoiHanNopPhiCaNhan').val('" + Convert.ToDateTime(dtbTempCaNhan.Rows[0]["NgayApDung"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                        sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_CaNhan]" + iPhiIDCaNhan + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", N'%" + Request.Form["TimKiemCaNhan"] + "%', ''," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Request.Form["tk_CTKiemToan"] + "'";
                        if (j == 0)
                            dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                        else
                            dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                        sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_CaNhan]" + iPhiIDCaNhan + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", N'%" + Request.Form["TimKiemCaNhan"] + "%', ''," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Request.Form["tk_CTKiemToan"] + "'";
                        dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                        //if (dtb.Rows.Count == 0)
                        //{
                        //    PagerCaNhan.Enabled = false;
                        //    return;
                        //}
                        //else
                        //    PagerCaNhan.Enabled = true;
                    }
                }
                PhiIDCaNhan = PhiIDCaNhan.TrimEnd(';');

                DataView dv = dtb.DefaultView;
                dv.Sort = "MaHoiVienCaNhan ASC";
                if (ViewState["sortexpression"] != null)
                    dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
                //else
                //dv.Sort = "STT ASC";
                //dtb = dv.ToTable();
                // Phân trang
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = dv;
                objPds.AllowPaging = true;
                objPds.PageSize = 99999;

                // danh sách trang
                for (i = 0; i < objPds.PageCount; i++)
                {
                    ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                    if (!PagerCaNhan.Items.Contains(a))
                        PagerCaNhan.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
                }
                // Chuyển trang
                if (!string.IsNullOrEmpty(Request.Form["tranghientaicanhan"]))
                {
                    //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                    if (Convert.ToInt32(Request.Form["tranghientaicanhan"]) <= objPds.PageCount)
                    {
                        objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientaicanhan"]);
                        PagerCaNhan.SelectedIndex = Convert.ToInt32(Request.Form["tranghientaicanhan"]);
                    }
                    else
                    {
                        objPds.CurrentPageIndex = 0;
                        PagerCaNhan.SelectedIndex = 0;
                    }
                }

                // kết xuất danh sách ra excel
                if (!string.IsNullOrEmpty(Request.Form["ketxuatcanhan"]))
                {
                    if (Request.Form["ketxuatcanhan"] == "1")
                    {
                        try
                        {
                            Response.Clear();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();

                            string FileName = quyen + "CaNhan" + DateTime.Now + ".xls";
                            StringWriter strwritter = new StringWriter();
                            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                            Response.ContentEncoding = System.Text.Encoding.UTF8;
                            Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                            objPds.AllowPaging = false;

                            GridView exportgrid = new GridView();

                            exportgrid.DataSource = objPds;
                            exportgrid.DataBind();
                            exportgrid.RenderControl(htmltextwrtter);
                            exportgrid.Dispose();

                            Response.Write(strwritter.ToString());
                            Response.End();
                        }
                        catch (System.Threading.ThreadAbortException a)
                        { }
                    }
                }

                grvthongbaophihoivien_canhan.DataSource = dv;
                grvthongbaophihoivien_canhan.DataBind();
            }

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            // ds = null;
            dtb = null;

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_guiemail' href='#none' class='btn btn-rounded' onclick='guiemail();'><i class='iconfa-envelope'></i> Gửi email</a>
            <a id='btn_ketxuatcanhan' href='#none' class='btn btn-rounded' onclick='export_excel_canhan();'><i class='iconfa-external-link'></i> Kết xuất danh sách cá nhân</a>
            <a id='btn_ketxuattapthe' href='#none' class='btn btn-rounded' onclick='export_excel_tapthe();'><i class='iconfa-external-link'></i> Kết xuất danh sách tổ chức</a>";
        output_html += "</div>";
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void LoadThoiGian()
    {
        //Bỏ mặc định thời gian = null
        //        string output_html = @"  $(function () { $('#ThoiHanNopPhiTapThe').datepicker('setDate', '" + strThoiHanTapThe + @"') });
        //    $(function () { $('#ThoiHanNopPhiCaNhan').datepicker('setDate', '" + strThoiHanCaNhan+ "') });";
        //        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("GUIEMAIL|"))
        {
            Response.Write("$('#btn_guiemail').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuatcanhan').remove();");
            Response.Write("$('#btn_ketxuattapthe').remove();");
        }
    }

    protected void grvthongbaophihoivien_canhan_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        LoadThongTin();
    }

    protected void grvThongBaoPhiCongTy_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirectioncty"] == null)
            ViewState["sortdirectioncty"] = "asc";
        else
        {
            if (ViewState["sortdirectioncty"].ToString() == "asc")
                ViewState["sortdirectioncty"] = "desc";
            else
                ViewState["sortdirectioncty"] = "asc";
        }
        ViewState["sortexpressioncty"] = e.SortExpression;
        LoadThongTin();
    }

    public void DonVi(string ma = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM (SELECT A.HoiVienTapTheID, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu + ' - ' + A.TenDoanhNghiep ELSE '--- ' + ISNULL(A.SoHieu,LTRIM(RTRIM(A.MaHoiVienTapThe))) + ' - ' + A.TenDoanhNghiep END AS TenDoanhNghiep, CASE ISNULL(A.HoiVienTapTheChaID,0) WHEN 0 THEN A.SoHieu ELSE B.SoHieu END AS SoHieu FROM tblHoiVienTapThe A LEFT JOIN tblHoiVienTapThe B ON A.HoiVienTapTheChaID = B.HoiVienTapTheID) AS X ORDER BY X.SoHieu ASC";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += "<option value=0>Tất cả</option>";
        foreach (DataRow Tinh in dt.Rows)
        {
            if (ma == Tinh["HoiVienTapTheID"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + Tinh["HoiVienTapTheID"].ToString().Trim() + @""">" + Tinh["TenDoanhNghiep"] + @"</option>";
            }
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }
}