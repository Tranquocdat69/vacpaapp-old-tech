﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="adminleftmenu.ascx.cs" Inherits="usercontrols_adminleftmenu" %>

<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header" style="cursor:pointer" title="Ẩn menu" onclick="close_leftpanel();">Menu chức năng <span  class="iconsweets-arrowleft"></span></li>
                <li <% checkmenu("|/admin.aspx?|"); %> ><a href="admin.aspx"><span class="iconfa-home"></span> Trang chủ</a></li>
                



                <li <% checkmenu("|/admin.aspx?page=nhapthongtinktv|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=timkiemsualoidulieu|/admin.aspx?page=khoiphucdulieu|/admin.aspx?page=saoluudulieu|/admin.aspx?page=nhatkysudung|/admin.aspx?page=role|/admin.aspx?page=account|/admin.aspx?page=cauhinhhethong|/admin.aspx?page=khaibaoquytrinh|/admin.aspx?page=user|",1); %> ><a href=""><span class="iconfa-wrench"></span> Quản trị hệ thống</a>
                	<ul <% checkmenu("|/admin.aspx?page=nhapthongtinktv|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=timkiemsualoidulieu|/admin.aspx?page=khoiphucdulieu|/admin.aspx?page=saoluudulieu|/admin.aspx?page=nhatkysudung|/admin.aspx?page=role|/admin.aspx?page=account|/admin.aspx?page=cauhinhhethong|/admin.aspx?page=khaibaoquytrinh|/admin.aspx?page=user|",2); %> >
                        <li id="cauhinhhethong" <% checkmenu("|/admin.aspx?page=cauhinhhethong|"); %> ><a href="admin.aspx?page=cauhinhhethong">Thiết lập tham số hệ thống</a></li>
                        <li id="khaibaoquytrinh" <% checkmenu("|/admin.aspx?page=khaibaoquytrinh|"); %> ><a href="admin.aspx?page=khaibaoquytrinh">Khai bao quy trình hoạt động</a></li>
                    	<li id="user" <% checkmenu("|/admin.aspx?page=user|"); %> ><a href="admin.aspx?page=user">Quản lý tài khoản người sử dụng</a></li>
                        <li id="account" <% checkmenu("|/admin.aspx?page=account|"); %> ><a href="admin.aspx?page=account">Quản lý tài khoản hội viên</a></li>
                        <li id="role" <% checkmenu("|/admin.aspx?page=role|"); %> ><a href="admin.aspx?page=role">Quản lý nhóm quyền</a></li>                        
                        <li id="nhatkysudung" <% checkmenu("|/admin.aspx?page=nhatkysudung|"); %> ><a href="admin.aspx?page=nhatkysudung">Nhật ký sử dụng phần mềm</a></li>                        
                        <li id="saoluudulieu" <% checkmenu("|/admin.aspx?page=saoluudulieu|"); %> ><a href="admin.aspx?page=saoluudulieu">Sao lưu dữ liệu</a></li>
                        <li id="khoiphucdulieu" <% checkmenu("|/admin.aspx?page=khoiphucdulieu|"); %> ><a href="admin.aspx?page=khoiphucdulieu">Khôi phục dữ liệu</a></li>
                        <li id="timkiemsualoidulieu" <% checkmenu("|/admin.aspx?page=timkiemsualoidulieu|"); %> ><a href="admin.aspx?page=timkiemsualoidulieu">Tìm kiếm, sửa lỗi dữ liệu</a></li>

                        <li class="dropdown"    ><a href="#">Nhập thông tin ban đầu ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=nhapthongtincongty|/admin.aspx?page=nhapthongtinktv|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=nhapthongtincongty|"); %> ><a href="admin.aspx?page=nhapthongtincongty">Cho công ty kiểm toán</a></li>
                            <li <% checkmenu("|/admin.aspx?page=nhapthongtinktv|"); %> ><a href="admin.aspx?page=nhapthongtinktv">Cho kiểm toán viên</a></li>                            
                        </ul>

                        </li>                                               
                    </ul>
                </li>


 
 
 <li <% checkmenu("|/admin.aspx?page=dmcocautochuc|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|/admin.aspx?page=dmchung&dm=dmnganhang|/admin.aspx?page=dmchung&dm=dmloaitien|/admin.aspx?page=dmchung&dm=dmloaiphi|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|/admin.aspx?page=dmchung&dm=dmhocvi|/admin.aspx?page=dmchung&dm=dmhocham|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|/admin.aspx?page=dmchung&dm=dmchungchi|/admin.aspx?page=dmchung&dm=dmchucvu|/admin.aspx?page=dmtinh|/admin.aspx?page=dmhuyen|/admin.aspx?page=dmxa|/admin.aspx?page=dmchung&dm=dmcauhoibimat|/admin.aspx?page=dmgiangvien|/admin.aspx?page=dmloaigiayto|/admin.aspx?page=icon|/admin.aspx?page=dmsothich|/admin.aspx?page=dmdangykienkt|/admin.aspx?page=dmloaitailieu|",1); %> ><a href=""><span class="icon-th-list"></span> Quản trị danh mục</a>
                	<ul <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|/admin.aspx?page=dmcocautochuc|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|/admin.aspx?page=dmchung&dm=dmnganhang|/admin.aspx?page=dmchung&dm=dmloaitien|/admin.aspx?page=dmchung&dm=dmloaiphi|/admin.aspx?page=dmchung&dm=dmhocvi|/admin.aspx?page=dmchung&dm=dmhocham|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|/admin.aspx?page=dmchung&dm=dmchungchi|/admin.aspx?page=dmchung&dm=dmchucvu|/admin.aspx?page=dmtinh|/admin.aspx?page=dmhuyen|/admin.aspx?page=dmxa|/admin.aspx?page=dmchung&dm=dmcauhoibimat|/admin.aspx?page=dmgiangvien|/admin.aspx?page=dmloaigiayto|/admin.aspx?page=icon|/admin.aspx?page=dmsothich|/admin.aspx?page=dmdangykienkt|/admin.aspx?page=dmloaitailieu|",2); %> >
                    	<li id="dmtinh" <% checkmenu("|/admin.aspx?page=dmtinh|"); %> ><a href="admin.aspx?page=dmtinh">Danh mục Tỉnh / Thành phố</a></li>
                        <li id="dmhuyen" <% checkmenu("|/admin.aspx?page=dmhuyen|"); %> ><a href="admin.aspx?page=dmhuyen">Danh mục Quận / Huyện</a></li>
                        <li id="dmxa" <% checkmenu("|/admin.aspx?page=dmxa|"); %> ><a href="admin.aspx?page=dmxa">Danh mục Phường / Xã</a></li>                        
                        <li id="dmcauhoibimat" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmcauhoibimat|"); %> ><a href="admin.aspx?page=dmchung&dm=dmcauhoibimat">Danh mục Câu hỏi bí mật</a></li>                        
                        <li id="dmcocautochuc" <% checkmenu("|/admin.aspx?page=dmcocautochuc|"); %> ><a href="admin.aspx?page=dmcocautochuc">Cơ cấu tổ chức của VACPA</a></li>                        
                        <li id="dmchucvu" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmchucvu|"); %> ><a href="admin.aspx?page=dmchung&dm=dmchucvu">Danh mục Chức vụ</a></li>  
                        <li id="dmchungchi" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmchungchi|"); %> ><a href="admin.aspx?page=dmchung&dm=dmchungchi">Danh mục Chứng chỉ</a></li>  
                        <li id="dmchuyennganhdaotao" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmchuyennganhdaotao|"); %> ><a href="admin.aspx?page=dmchung&dm=dmchuyennganhdaotao">Danh mục Chuyên ngành đào tạo</a></li>  
                        <li id="dmhinhthuckhenthuong" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong|"); %> ><a href="admin.aspx?page=dmchung&dm=dmhinhthuckhenthuong">Danh mục Hình thức khen thưởng</a></li>  
                        <li id="dmhinhthuckyluat" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmhinhthuckyluat|"); %> ><a href="admin.aspx?page=dmchung&dm=dmhinhthuckyluat">Danh mục Hình thức kỷ luật</a></li>  
                        <li id="dmhocham" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmhocham|"); %> ><a href="admin.aspx?page=dmchung&dm=dmhocham">Danh mục Học hàm</a></li>  
                        <li id="dmhocvi" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmhocvi|"); %> ><a href="admin.aspx?page=dmchung&dm=dmhocvi">Danh mục Học vị</a></li>  
                        <li id="dmloaihinhdoanhnghiep" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep|"); %> ><a href="admin.aspx?page=dmchung&dm=dmloaihinhdoanhnghiep">Danh mục Loại hình doanh nghiệp</a></li>  
                        <li id="dmloaiphi" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaiphi|"); %> ><a href="admin.aspx?page=dmchung&dm=dmloaiphi">Danh mục Loại phí</a></li>  
                        <li id="dmloaitien" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmloaitien|"); %> ><a href="admin.aspx?page=dmchung&dm=dmloaitien">Danh mục Loại tiền</a></li>  
                        <li id="dmnganhang" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmnganhang|"); %> ><a href="admin.aspx?page=dmchung&dm=dmnganhang">Danh mục Ngân hàng</a></li>  
                        <li id="dmtrinhdochuyenmon" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon|"); %> ><a href="admin.aspx?page=dmchung&dm=dmtrinhdochuyenmon">Danh mục Trình độ chuyên môn</a></li>  
                        <li id="dmtruongdaihoc" <% checkmenu("|/admin.aspx?page=dmchung&dm=dmtruongdaihoc|"); %> ><a href="admin.aspx?page=dmchung&dm=dmtruongdaihoc">Danh mục Trường đại học</a></li>  
                        <li id="dmgiangvien" <% checkmenu("|/admin.aspx?page=dmgiangvien|"); %> ><a href="admin.aspx?page=dmgiangvien">Danh mục Giảng viên</a></li>                                               
                        <li id="dmloaigiayto" <% checkmenu("|/admin.aspx?page=dmloaigiayto|"); %> ><a href="admin.aspx?page=dmloaigiayto">Danh mục Loại giấy tờ</a></li>                                               
                        <li id="dmicon" <% checkmenu("|/admin.aspx?page=icon|"); %> ><a href="admin.aspx?page=icon">Danh mục icon</a></li>                                               
                        <li id="Li1" <% checkmenu("|/admin.aspx?page=dmdangykienkt|"); %> ><a href="admin.aspx?page=dmdangykienkt">Danh mục Dạng ý kiến kiểm toán</a></li>                                               
                        <li id="Li2" <% checkmenu("|/admin.aspx?page=dmsothich|"); %> ><a href="admin.aspx?page=dmsothich">Danh mục sở thích của kiểm toán viên</a></li>                                               
                        <li id="Li3" <% checkmenu("|/admin.aspx?page=dmloaitailieu|"); %> ><a href="admin.aspx?page=dmloaitailieu">Danh mục loại tài liệu</a></li>                                               
                    </ul>
                </li>





         <li <% checkmenu("|/admin.aspx?page=idnguoiquantam|/admin.aspx?page=idkiemtoanvien|/admin.aspx?page=idhoiviencanhan|/admin.aspx?page=idhoivientapthe|",1); %> ><a href=""><span class="iconfa-credit-card"></span> Quản lý ID, đối tượng cấp ID</a>
               <ul <% checkmenu("|/admin.aspx?page=idnguoiquantam|/admin.aspx?page=idkiemtoanvien|/admin.aspx?page=idhoiviencanhan|/admin.aspx?page=idhoivientapthe|",2); %> >
                     	<li id="idhoivientapthe" <% checkmenu("|/admin.aspx?page=idhoivientapthe|"); %> ><a href="admin.aspx?page=idhoivientapthe">ID. Hội viên tập thể</a></li>
                        <li id="idhoiviencanhan" <% checkmenu("|/admin.aspx?page=idhoiviencanhan|"); %> ><a href="admin.aspx?page=idhoiviencanhan">ID. Hội viên cá nhân</a></li>
                        <li id="idkiemtoanvien" <% checkmenu("|/admin.aspx?page=idkiemtoanvien|"); %> ><a href="admin.aspx?page=idkiemtoanvien">ID. Kiểm toán viên</a></li>                        
                        <li id="idnguoiquantam" <% checkmenu("|/admin.aspx?page=idnguoiquantam|"); %> ><a href="admin.aspx?page=idnguoiquantam">ID. Người quan tâm</a></li>                        
                </ul>
         </li>


            <li <% checkmenu("|/admin.aspx?page=ketnaphoiviencanhan|/admin.aspx?page=ketnaphoivientapthe|",1); %> ><a href=""><span class="iconfa-group"></span> Quản lý hội viên</a>
               <ul <% checkmenu("|/admin.aspx?page=ketnaphoiviencanhan|/admin.aspx?page=ketnaphoivientapthe|",2); %> >
                     	<li id="ketnaphoivientapthe" <% checkmenu("|/admin.aspx?page=ketnaphoivientapthe|"); %> ><a href="admin.aspx?page=ketnaphoivientapthe">Kết nạp Hội viên tập thể</a></li>
                        <li id="ketnaphoiviencanhan" <% checkmenu("|/admin.aspx?page=ketnaphoiviencanhan|"); %> ><a href="admin.aspx?page=ketnaphoiviencanhan">Kết nạp Hội viên cá nhân</a></li>                                           
                </ul>
         </li>



                <li <% checkmenu("|/admin.aspx?page=tracuuhosodaotao|/admin.aspx?page=capgcnchohocvien|/admin.aspx?page=tonghopgioconthieu|/admin.aspx?page=tonghopgiocnkt|/admin.aspx?page=tonghopgiohoc|/admin.aspx?page=dangkythamdukhoahoc|/admin.aspx?page=thongtinkhoahoc|/admin.aspx?page=chuongtrinhdaotaotheonam|/admin.aspx?page=dmgiangvien|/admin.aspx?page=capidnqt|/admin.aspx?page=capidktv|",1); %> ><a href=""><span class="iconfa-book"></span> Quản lý đào tạo CNKT</a>
                	<ul <% checkmenu("|/admin.aspx?page=tracuuhosodaotao|/admin.aspx?page=capgcnchohocvien|/admin.aspx?page=tonghopgioconthieu|/admin.aspx?page=tonghopgiocnkt|/admin.aspx?page=tonghopgiohoc|/admin.aspx?page=dangkythamdukhoahoc|/admin.aspx?page=thongtinkhoahoc|/admin.aspx?page=chuongtrinhdaotaotheonam|/admin.aspx?page=dmgiangvien|/admin.aspx?page=capidnqt|/admin.aspx?page=capidktv|",2); %> >
                        <li id="capidktv" <% checkmenu("|/admin.aspx?page=capidktv|"); %> ><a href="admin.aspx?page=capidktv">Cấp ID.KTV</a></li>
                        <li id="capidnqt" <% checkmenu("|/admin.aspx?page=capidnqt|"); %> ><a href="admin.aspx?page=capidnqt">Cấp ID.NQT</a></li>
                    	<li id="dmgiangvien" <% checkmenu("|/admin.aspx?page=dmgiangvien|"); %> ><a href="admin.aspx?page=dmgiangvien">Danh mục giảng viên</a></li>
                        
                        
                        <li class="dropdown"    ><a href="#">Quản lý đào tạo và CNKT ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=capgcnchohocvien|/admin.aspx?page=tonghopgioconthieu|/admin.aspx?page=tonghopgiocnkt|/admin.aspx?page=tonghopgiohoc|/admin.aspx?page=dangkythamdukhoahoc|/admin.aspx?page=thongtinkhoahoc|/admin.aspx?page=chuongtrinhdaotaotheonam|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=chuongtrinhdaotaotheonam|"); %> ><a href="admin.aspx?page=chuongtrinhdaotaotheonam">Quản lý chương trình đào tạo theo năm</a></li>
                            <li <% checkmenu("|/admin.aspx?page=thongtinkhoahoc|"); %> ><a href="admin.aspx?page=thongtinkhoahoc">Quản lý thông tin về khóa học</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=dangkythamdukhoahoc|"); %> ><a href="admin.aspx?page=dangkythamdukhoahoc">Đăng ký tham dự khóa học</a></li>
                            <li <% checkmenu("|/admin.aspx?page=tonghopgiohoc|"); %> ><a href="admin.aspx?page=tonghopgiohoc">Tổng hợp giờ học của từng học viên và phiếu đánh giá</a></li>
                            <li <% checkmenu("|/admin.aspx?page=tonghopgiocnkt|"); %> ><a href="admin.aspx?page=tonghopgiocnkt">Tổng hợp giờ CNKT theo từng HV trong năm</a></li>
                            <li <% checkmenu("|/admin.aspx?page=tonghopgioconthieu|"); %> ><a href="admin.aspx?page=tonghopgioconthieu">Tổng hợp số giờ còn thiếu theo từng học viên trong năm</a></li>
                            <li <% checkmenu("|/admin.aspx?page=capgcnchohocvien|"); %> ><a href="admin.aspx?page=capgcnchohocvien">Lập và cấp GCN cho Học viên</a></li>
                        </ul>

                        <li id="tracuuhosodaotao" <% checkmenu("|/admin.aspx?page=tracuuhosodaotao|"); %> ><a href="admin.aspx?page=tracuuhosodaotao">Tra cứu Hồ sơ đào tạo và CNKT online</a></li>

                        </li>                                               
                    </ul>
                </li>


 <li <% checkmenu("|/admin.aspx?page=xulysaipham|/admin.aspx?page=baocaotonghopkiemsoatchatluong|/admin.aspx?page=bienbanketquakiemtra|/admin.aspx?page=baocaoketquakiemtrabaocaotaichinh|/admin.aspx?page=baocaoketquakiemtrahethong|/admin.aspx?page=bocauhoikiemtrabaocaotaichinh|/admin.aspx?page=bocauhoikiemtrahethong|/admin.aspx?page=quanlyhosokiemsoatchatluong|/admin.aspx?page=camketdoclapvabaomatthongtin|/admin.aspx?page=capnhatgiodaotao|/admin.aspx?page=danhsachdoankiemtra|/admin.aspx?page=lapdoankiemtra|/admin.aspx?page=lapdanhsachcongtykiemtratructiep|/admin.aspx?page=capnhatyeucaukiemtravuviec|/admin.aspx?page=danhsachcongtykiemtoanphaibaocaotukiemtra|/admin.aspx?page=lapdanhsachcongtykiemtoanphaibaocaotukiemtra|",1); %> ><a href=""><span class="iconfa-legal"></span> Quản lý kiểm soát chất lượng</a>
                	<ul <% checkmenu("|/admin.aspx?page=xulysaipham|/admin.aspx?page=baocaotonghopkiemsoatchatluong|/admin.aspx?page=bienbanketquakiemtra|/admin.aspx?page=baocaoketquakiemtrabaocaotaichinh|/admin.aspx?page=baocaoketquakiemtrahethong|/admin.aspx?page=bocauhoikiemtrabaocaotaichinh|/admin.aspx?page=bocauhoikiemtrahethong|/admin.aspx?page=quanlyhosokiemsoatchatluong|/admin.aspx?page=camketdoclapvabaomatthongtin|/admin.aspx?page=capnhatgiodaotao|/admin.aspx?page=danhsachdoankiemtra|/admin.aspx?page=lapdoankiemtra|/admin.aspx?page=lapdanhsachcongtykiemtratructiep|/admin.aspx?page=capnhatyeucaukiemtravuviec|/admin.aspx?page=danhsachcongtykiemtoanphaibaocaotukiemtra|/admin.aspx?page=lapdanhsachcongtykiemtoanphaibaocaotukiemtra|",2); %> >
                         
                        
                        <li class="dropdown"    ><a href="#">Giai đoạn chuẩn bị kiểm tra ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=capnhatgiodaotao|/admin.aspx?page=danhsachdoankiemtra|/admin.aspx?page=lapdoankiemtra|/admin.aspx?page=lapdanhsachcongtykiemtratructiep|/admin.aspx?page=capnhatyeucaukiemtravuviec|/admin.aspx?page=danhsachcongtykiemtoanphaibaocaotukiemtra|/admin.aspx?page=lapdanhsachcongtykiemtoanphaibaocaotukiemtra|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=lapdanhsachcongtykiemtoanphaibaocaotukiemtra|"); %> ><a href="admin.aspx?page=lapdanhsachcongtykiemtoanphaibaocaotukiemtra">Danh sách công ty kiểm toán phải báo cáo tự kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=danhsachcongtykiemtoanphaibaocaotukiemtra|"); %> ><a href="admin.aspx?page=danhsachcongtykiemtoanphaibaocaotukiemtra">Truy vấn danh sách công ty kiểm toán phải báo cáo tự kiểm tra</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=capnhatyeucaukiemtravuviec|"); %> ><a href="admin.aspx?page=capnhatyeucaukiemtravuviec">Cập nhật yêu cầu kiểm tra vụ việc</a></li>
                            <li <% checkmenu("|/admin.aspx?page=lapdanhsachcongtykiemtratructiep|"); %> ><a href="admin.aspx?page=lapdanhsachcongtykiemtratructiep">Lập danh sách Công ty kiểm tra trực tiếp</a></li>
                            <li <% checkmenu("|/admin.aspx?page=lapdoankiemtra|"); %> ><a href="admin.aspx?page=lapdoankiemtra">Lập đoàn kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=danhsachdoankiemtra|"); %> ><a href="admin.aspx?page=danhsachdoankiemtra">Truy vấn thông tin danh sách đoàn kiểm tra</a></li>
                            <li <% checkmenu("|/admin.aspx?page=capnhatgiodaotao|"); %> ><a href="admin.aspx?page=capnhatgiodaotao">Cập nhật giờ đào tạo</a></li>
                        </ul>

                        
                        <li class="dropdown"    ><a href="#">Giai đoạn thực hiện kiểm tra tại công ty kiểm toán ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=bienbanketquakiemtra|/admin.aspx?page=baocaoketquakiemtrabaocaotaichinh|/admin.aspx?page=baocaoketquakiemtrahethong|/admin.aspx?page=bocauhoikiemtrabaocaotaichinh|/admin.aspx?page=bocauhoikiemtrahethong|/admin.aspx?page=quanlyhosokiemsoatchatluong|/admin.aspx?page=camketdoclapvabaomatthongtin|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=camketdoclapvabaomatthongtin|"); %> ><a href="admin.aspx?page=camketdoclapvabaomatthongtin">Cam kết độc lập và bảo mật thông tin</a></li>
                            <li <% checkmenu("|/admin.aspx?page=quanlyhosokiemsoatchatluong|"); %> ><a href="admin.aspx?page=quanlyhosokiemsoatchatluong">Quản lý hồ sơ kiểm soát chất lượng</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=bocauhoikiemtrahethong|"); %> ><a href="admin.aspx?page=bocauhoikiemtrahethong">Bộ câu hỏi kiểm tra hệ thống</a></li>
                            <li <% checkmenu("|/admin.aspx?page=bocauhoikiemtrabaocaotaichinh|"); %> ><a href="admin.aspx?page=bocauhoikiemtrabaocaotaichinh">Bộ câu hỏi kiểm tra báo cáo tài chính</a></li>
                            <li <% checkmenu("|/admin.aspx?page=baocaoketquakiemtrahethong|"); %> ><a href="admin.aspx?page=baocaoketquakiemtrahethong">Báo cáo kết quả kiểm tra hệ thống</a></li>
                            <li <% checkmenu("|/admin.aspx?page=baocaoketquakiemtrabaocaotaichinh|"); %> ><a href="admin.aspx?page=baocaoketquakiemtrabaocaotaichinh">Báo cáo kết quả kiểm tra báo cáo tài chính</a></li>
                            <li <% checkmenu("|/admin.aspx?page=bienbanketquakiemtra|"); %> ><a href="admin.aspx?page=bienbanketquakiemtra">Biên bản kết quả kiểm tra</a></li>
                        </ul>

                        <li class="dropdown"    ><a href="#">Giai đoạn kết thúc kiểm tra ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=xulysaipham|/admin.aspx?page=baocaotonghopkiemsoatchatluong|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=baocaotonghopkiemsoatchatluong|"); %> ><a href="admin.aspx?page=baocaotonghopkiemsoatchatluong">Báo cáo tổng hợp kiểm soát chất lượng và đề xuất xử lý sai phạm</a></li>
                            <li <% checkmenu("|/admin.aspx?page=xulysaipham|"); %> ><a href="admin.aspx?page=xulysaipham">Xử lý sai phạm của BTC và VACPA</a></li>                                                        
                        </ul>


                        </li>                                               
                    </ul>
                </li>

                    
  
  
  
<li <% checkmenu("|/admin.aspx?page=quanlyphihoivien|/admin.aspx?page=quanlyphidanglogo|/admin.aspx?page=quanlyphikiemsoatchatluong|/admin.aspx?page=quanlyphikhac|/admin.aspx?page=quanlyphidanhsachphi|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicapnhatkienthuc|/admin.aspx?page=thongbaonophoiphi|/admin.aspx?page=quanlyphi|/admin.aspx?page=qlhv_danhsachcongtydangkydanglogo|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicnkt|/admin.aspx?page=thongbaonophoiphi|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thanhtoanphi_quanly|",1); %> ><a href=""><span class="iconfa-money"></span> Thanh toán</a>
                	<ul <%checkmenu("|/admin.aspx?page=quanlyphihoivien|/admin.aspx?page=quanlyphidanglogo|/admin.aspx?page=quanlyphikiemsoatchatluong|/admin.aspx?page=quanlyphikhac|/admin.aspx?page=quanlyphidanhsachphi|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicapnhatkienthuc|/admin.aspx?page=thongbaonophoiphi|/admin.aspx?page=quanlyphi|/admin.aspx?page=qlhv_danhsachcongtydangkydanglogo|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicnkt|/admin.aspx?page=thongbaonophoiphi|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thanhtoanphi_quanly|",2); %> >
                         
                        <li class="dropdown"    ><a href="#">Quản lý phí ►</a>
                        <ul <% checkmenu("|/admin.aspx?page=quanlyphihoivien|/admin.aspx?page=quanlyphidanglogo|/admin.aspx?page=quanlyphikiemsoatchatluong|/admin.aspx?page=quanlyphikhac|/admin.aspx?page=quanlyphidanhsachphi|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=quanlyphihoivien|"); %> ><a href="admin.aspx?page=quanlyphihoivien">Phí hội viên</a></li>
                            <li <% checkmenu("|/admin.aspx?page=quanlyphidanglogo|"); %> ><a href="admin.aspx?page=quanlyphidanglogo">Phí đăng logo</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=quanlyphikiemsoatchatluong|"); %> ><a href="admin.aspx?page=quanlyphikiemsoatchatluong">Phí kiểm soát chất lượng</a></li>
                            <li <% checkmenu("|/admin.aspx?page=quanlyphikhac|"); %> ><a href="admin.aspx?page=quanlyphikhac">Phí khác</a></li>
                            <li <% checkmenu("|/admin.aspx?page=quanlyphidanhsachphi|"); %> ><a href="admin.aspx?page=quanlyphidanhsachphi">Danh sách phí</a></li>                            
                        </ul>
                        <li id="Li4" <% checkmenu("|/admin.aspx?page=qlhv_danhsachcongtydangkydanglogo|"); %> ><a href="admin.aspx?page=qlhv_danhsachcongtydangkydanglogo">Danh sách công ty đăng ký đăng logo</a></li>   
                        <li class="dropdown"    ><a href="#">Thông báo nộp phí ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=thongbaonopphidanglogo|/admin.aspx?page=thongbaonopphikiemsoatchatluong|/admin.aspx?page=thongbaonopphicapnhatkienthuc|/admin.aspx?page=thongbaonophoiphi|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=thongbaonophoiphi|"); %> ><a href="admin.aspx?page=thongbaonophoiphi">Thông báo nộp hội phí</a></li>
                            <li <% checkmenu("|/admin.aspx?page=thongbaonopphicapnhatkienthuc|"); %> ><a href="admin.aspx?page=thongbaonopphicapnhatkienthuc">Thông báo nộp phí cập nhật kiến thức</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=thongbaonopphikiemsoatchatluong|"); %> ><a href="admin.aspx?page=thongbaonopphikiemsoatchatluong">Thông báo nộp phí kiểm soát chất lượng</a></li>
                            <li <% checkmenu("|/admin.aspx?page=thongbaonopphidanglogo|"); %> ><a href="admin.aspx?page=thongbaonopphidanglogo">Thông báo nộp phí đăng logo</a></li>
                        </ul>

                        
                        <li class="dropdown"    ><a href="#">Thanh toán phí ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=thanhtoanphicanhan|/admin.aspx?page=thanhtoanphitapthe|/admin.aspx?page=thanhtoanphi_quanly|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=thanhtoanphicanhan|"); %> ><a href="admin.aspx?page=thanhtoanphicanhan">Thanh toán phí cá nhân</a></li>
                            <li <% checkmenu("|/admin.aspx?page=thanhtoanphitapthe|"); %> ><a href="admin.aspx?page=thanhtoanphitapthe">Thanh toán phí tập thể</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=thanhtoanphi_quanly|"); %> ><a href="admin.aspx?page=thanhtoanphi_quanly">Quản lý giao dịch thanh toán</a></li>                            
                        </ul>

                        

                        </li>                                               
                    </ul>
                </li>  
  
  


   <li <% checkmenu("|/admin.aspx?page=congvanguibotaichinhvevieckiemtracongtykiemtoan|/admin.aspx?page=baocaoveviectochuclophoccnkt|/admin.aspx?page=baocaoketquatochuclophoccnkt|/admin.aspx?page=baocaovacpa|",1); %> ><a href=""><span class="iconfa-bar-chart"></span> Báo cáo</a>
                	<ul <% checkmenu("|/admin.aspx?page=congvanguibotaichinhvevieckiemtracongtykiemtoan|/admin.aspx?page=baocaoveviectochuclophoccnkt|/admin.aspx?page=baocaoketquatochuclophoccnkt|/admin.aspx?page=baocaovacpa|",2); %> >
                         
                        <li id="baocaovacpa" <% checkmenu("|/admin.aspx?page=baocaovacpa|"); %> ><a href="admin.aspx?page=baocaovacpa">Báo cáo chi tiết theo yêu cầu quản lý của VACPA</a></li>
                        <li class="dropdown"    ><a href="#">Báo cáo Bộ tài chính ►</a>
                        
                        <ul <% checkmenu("|/admin.aspx?page=congvanguibotaichinhvevieckiemtracongtykiemtoan|/admin.aspx?page=baocaoveviectochuclophoccnkt|/admin.aspx?page=baocaoketquatochuclophoccnkt|",2); %> >
                            <li <% checkmenu("|/admin.aspx?page=baocaoveviectochuclophoccnkt|"); %> ><a href="admin.aspx?page=baocaoveviectochuclophoccnkt">Báo cáo về việc tổ chức lớp học cập nhật kiến thức </a></li>
                            <li <% checkmenu("|/admin.aspx?page=baocaoketquatochuclophoccnkt|"); %> ><a href="admin.aspx?page=baocaoketquatochuclophoccnkt">Báo cáo tổng hợp kết quả tổ chức lớp học cập nhật kiến thức kiểm toán viên (năm)</a></li>                            
                            <li <% checkmenu("|/admin.aspx?page=congvanguibotaichinhvevieckiemtracongtykiemtoan|"); %> ><a href="admin.aspx?page=congvanguibotaichinhvevieckiemtracongtykiemtoan">Công văn gửi Bộ Tài chính về việc kiểm tra trực tiếp công ty kiểm toán</a></li>
                        </ul>

                        
                   
                        

                        </li>                                               
                    </ul>
                </li>  
  
            
               
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->


    <script type="text/javascript">
<%
 
 

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyTaiKhoanCB", cm.connstr).Contains("XEM|"))
    {
        Response.Write("$('#mnu_tkcb').remove();");        
    }  
    

    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyTaiKhoanKH", cm.connstr).Contains("XEM|"))
    {
        Response.Write("$('#mnu_tkkh').remove();");        
    }  

       if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "QuanLyNhomQuyen", cm.connstr).Contains("XEM|"))
    {
        Response.Write("$('#mnu_nhomquyen').remove();");        
    }  
 %>
 </script>




