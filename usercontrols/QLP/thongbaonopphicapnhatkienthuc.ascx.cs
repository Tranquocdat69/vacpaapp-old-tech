﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_thongbaonopphicapnhatkienthuc : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dtb = new DataTable();

    public string quyen = "ThongBaoNopPhiCNKT";

    public string tenchucnang = "Thông báo nộp phí cập nhật kiến thức";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";
    public int iTinhTrang = 0;

    public string id_canhan = "HoiVienCaNhanId";
    public string ma_canhan = "MaHoiVienCaNhan";
    public string stt_canhan = "STT";
    public string ten_canhan = "HoVaTen";
    public string sochungchi_canhan = "SoChungChiKTV";
    public string ngaycapchungchi_canhan = "NgayCapChungChiKTV";
    public string donvicongtac_canhan = "DonViCongTac";
    public string quequan_canhan = "QueQuan";
    public string email_canhan = "Email";
    public string hoiphiconnonamtruoc_canhan = "PhiCNKTConNoNamTruoc";

    public int iPhiIDCaNhan;
    public string PhiIDCaNhan = "";
    public string strThoiHanCaNhan = "";


    public string id_congty = "HoiVienTapTheId";
    public string ma_tapthe = "MaHoiVienTapThe";
    public string stt_congty = "STT";
    public string ten_congty = "TenDoanhNghiep";
    public string tenviettat_congty = "TenVietTat";
    public string email_tapthe = "Email";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            if (string.IsNullOrEmpty(Request.QueryString["thongbao"]))
            {
                //LoadThongTin();
            }
            else if (Request.QueryString["thongbao"] == "thanhcong")
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script>");
                sb.Append(@"jQuery(document).ready(function () {");
                sb.Append(@"  thongbaothanhcong();}) ");
                sb.Append(@"</script>");
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
                //LoadThongTin();
            }
            else if (Request.QueryString["thongbao"] == "loi")
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script>");
                sb.Append(@"jQuery(document).ready(function () {");
                sb.Append(@"  thongbaoloi();}) ");
                sb.Append(@"</script>");
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
                //LoadThongTin();
            }
        }
        catch
        { }
    }

    protected void LoadThongTin()
    {
        try
        {

            grvThongBaoPhiCongTy.Columns[1].HeaderText = "STT";
            grvThongBaoPhiCongTy.Columns[1].SortExpression = stt_congty;
            grvThongBaoPhiCongTy.Columns[2].HeaderText = "ID HVTC";
            grvThongBaoPhiCongTy.Columns[2].SortExpression = ma_tapthe;
            grvThongBaoPhiCongTy.Columns[3].HeaderText = "Tên công ty";
            grvThongBaoPhiCongTy.Columns[3].SortExpression = ten_congty;
            grvThongBaoPhiCongTy.Columns[4].HeaderText = "Tên viết tắt";
            grvThongBaoPhiCongTy.Columns[4].SortExpression = tenviettat_congty;


            grvthongbaophihoivien_canhan.Columns[1].HeaderText = "STT";
            grvthongbaophihoivien_canhan.Columns[1].SortExpression = stt_canhan;
            grvthongbaophihoivien_canhan.Columns[2].HeaderText = "ID";
            grvthongbaophihoivien_canhan.Columns[2].SortExpression = ma_canhan;
            grvthongbaophihoivien_canhan.Columns[3].HeaderText = "Họ và tên";
            grvthongbaophihoivien_canhan.Columns[3].SortExpression = ten_canhan;
            grvthongbaophihoivien_canhan.Columns[4].HeaderText = "Số chứng chỉ KTV";
            grvthongbaophihoivien_canhan.Columns[4].SortExpression = sochungchi_canhan;
            grvthongbaophihoivien_canhan.Columns[5].HeaderText = "Ngày cấp chứng chỉ KTV";
            grvthongbaophihoivien_canhan.Columns[5].SortExpression = ngaycapchungchi_canhan;
            grvthongbaophihoivien_canhan.Columns[6].HeaderText = "Đơn vị công tác";
            grvthongbaophihoivien_canhan.Columns[6].SortExpression = donvicongtac_canhan;
            grvthongbaophihoivien_canhan.Columns[7].HeaderText = "Quê quán";
            grvthongbaophihoivien_canhan.Columns[7].SortExpression = quequan_canhan;
            grvthongbaophihoivien_canhan.Columns[8].HeaderText = "Phí CNKT còn nợ";
            grvthongbaophihoivien_canhan.Columns[8].SortExpression = hoiphiconnonamtruoc_canhan;

            int i;
            Commons cm = new Commons();

            SqlCommand sql = new SqlCommand();

            int iLopHocID = 0;
            dtb = null;
            if (!string.IsNullOrWhiteSpace(MaLopHoc.Text))
            {
                sql.CommandText = "SELECT TOP 1 LopHocID, TenLopHoc, MaLopHoc FROM tblCNKTLopHoc WHERE TinhTrangID = " +
                                  (int) EnumVACPA.TinhTrang.DaPheDuyet + " AND MaLopHoc like N'%" + MaLopHoc.Text + "%'";
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtbTemp.Rows.Count > 0)
                {
                    LopHocID.Value = dtbTemp.Rows[0]["LopHocID"].ToString();
                    MaLopHoc.Text = dtbTemp.Rows[0]["MaLopHoc"].ToString();
                    TenLopHoc.Text = dtbTemp.Rows[0]["TenLopHoc"].ToString();
                    iLopHocID = Convert.ToInt32(LopHocID.Value);
                }
            }

            //Lấy phí cá nhân
            //sql.CommandText = "SELECT TOP 1 PhiID, NgayApDung from tblTTDMPhiHoiVien A left join tblDMDoiTuongNopPhi B on A.DoiTuongApDungID = b.DoiTuongNopPhiID where A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND B.LoaiDoiTuong = " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + "  AND A.NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND ((CASE WHEN A.NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE A.NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            //DataTable dtbTempCaNhan = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            //iPhiIDCaNhan = 0;
            //if (dtbTempCaNhan.Rows.Count > 0)
            //{
            //    iPhiIDCaNhan = Convert.ToInt32(dtbTempCaNhan.Rows[0][0]);
            //    strThoiHanCaNhan = Convert.ToDateTime(dtbTempCaNhan.Rows[0][1]).ToString("dd/MM/yyyy");
            //}
            //else
            // strThoiHanCaNhan = DateTime.Now.ToString("dd/MM/yyyy");
            //if (iPhiIDCaNhan != 0)
            //{
            //    PhiIDCaNhan = iPhiIDCaNhan.ToString();
            //System.Web.HttpContext.Current.Response.Write("$('#ThoiHanNopPhiCaNhan').val('" + Convert.ToDateTime(dtbTempCaNhan.Rows[0]["NgayApDung"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);

            string strNgayLap = "";
            if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                strNgayLap = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
            else
                strNgayLap = DateTime.Now.ToString("yyyy-MM-dd");
            //Phí tổ chức
            string TimKiemTapThe = "";
            if (!string.IsNullOrEmpty(Request.Form["TimKiemTapThe"]))
                TimKiemTapThe = "%" + Request.Form["TimKiemTapThe"] + "%";

            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_TapThe]" + iLopHocID + ", N'" + TimKiemTapThe + "',''";
            dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            
            if (dtb.Rows.Count > 0)
            {
                //if (dtb.Rows.Count == 0)
                //{
                //    PagerCaNhan.Enabled = false;
                //    return;
                //}
                //else
                //    PagerCaNhan.Enabled = true;
                DataView dv = dtb.DefaultView;

                if (ViewState["sortexpressioncty"] != null)
                    dv.Sort = ViewState["sortexpressioncty"].ToString() + " " + ViewState["sortdirectioncty"].ToString();
                else
                    dv.Sort = "STT ASC";
                //dtb = dv.ToTable();
                // Phân trang
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = dv;
                objPds.AllowPaging = true;
                objPds.PageSize = 99999;

                // danh sách trang
                for (i = 0; i < objPds.PageCount; i++)
                {
                    ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                    if (!PagerTapThe.Items.Contains(a))
                        PagerTapThe.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
                }
                // Chuyển trang
                if (!string.IsNullOrEmpty(Request.Form["tranghientaitapthe"]))
                {
                    //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                    if (Convert.ToInt32(Request.Form["tranghientaitapthe"]) <= objPds.PageCount)
                    {
                        objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientaitapthe"]);
                        PagerTapThe.SelectedIndex = Convert.ToInt32(Request.Form["tranghientaitapthe"]);
                    }
                    else
                    {
                        objPds.CurrentPageIndex = 0;
                        PagerTapThe.SelectedIndex = 0;
                    }
                }



                grvThongBaoPhiCongTy.DataSource = dv;
                grvThongBaoPhiCongTy.DataBind();
            }
        
        //Phí cá nhân
            string TimKiemTen = "";
            if (!string.IsNullOrEmpty(Request.Form["TimKiemCaNhan"]))
                TimKiemTen = "%" + Request.Form["TimKiemCaNhan"] + "%";

            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", N'" + TimKiemTen + "', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + iLopHocID;
            dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", N'" + TimKiemTen + "', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + iLopHocID;
            dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", N'" + TimKiemTen + "', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + iLopHocID;
            dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            //if (dtb.Rows.Count == 0)
            //{
            //    PagerCaNhan.Enabled = false;
            //    return;
            //}
            //else
            //    PagerCaNhan.Enabled = true;
            if (dtb.Rows.Count > 0)
            {
                DataView dv = dtb.DefaultView;

                if (ViewState["sortexpression"] != null)
                    dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
                else
                    dv.Sort = "STT ASC";
                //dtb = dv.ToTable();
                // Phân trang
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = dv;
                objPds.AllowPaging = true;
                objPds.PageSize = 99999;

                // danh sách trang
                for (i = 0; i < objPds.PageCount; i++)
                {
                    ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                    if (!PagerCaNhan.Items.Contains(a))
                        PagerCaNhan.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
                }
                // Chuyển trang
                if (!string.IsNullOrEmpty(Request.Form["tranghientaicanhan"]))
                {
                    //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                    if (Convert.ToInt32(Request.Form["tranghientaicanhan"]) <= objPds.PageCount)
                    {
                        objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientaicanhan"]);
                        PagerCaNhan.SelectedIndex = Convert.ToInt32(Request.Form["tranghientaicanhan"]);
                    }
                    else
                    {
                        objPds.CurrentPageIndex = 0;
                        PagerCaNhan.SelectedIndex = 0;
                    }
                }

                // kết xuất danh sách ra excel
                if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
                {
                    if (Request.Form["ketxuat"] == "1")
                    {
                        Response.Clear();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();

                        string FileName = quyen + "CaNhan" + DateTime.Now + ".xls";
                        StringWriter strwritter = new StringWriter();
                        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                        Response.ContentEncoding = System.Text.Encoding.UTF8;
                        Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                        objPds.AllowPaging = false;

                        GridView exportgrid = new GridView();

                        exportgrid.DataSource = objPds;
                        exportgrid.DataBind();
                        exportgrid.RenderControl(htmltextwrtter);
                        exportgrid.Dispose();

                        Response.Write(strwritter.ToString());
                        Response.End();
                    }
                }

                grvthongbaophihoivien_canhan.DataSource = dv;
                grvthongbaophihoivien_canhan.DataBind();
            }

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            // ds = null;
            dtb = null;

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_guiemail' href='#none' class='btn btn-rounded' onclick='guiemail();'><i class='iconfa-envelope'></i> Gửi email</a>
            <a id='btn_ketxuat' href='#none' class='btn btn-rounded' onclick='export_excel();'><i class='iconfa-external-link'></i> Kết xuất</a>";
        output_html += "</div>";
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void LoadThoiGian()
    {
        //string output_html = @"$(function () { $('#ThoiHanNopPhiCaNhan').datepicker('setDate', '" + strThoiHanCaNhan + "') });";
        //System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("GUIEMAIL|"))
        {
            Response.Write("$('#btn_guiemail').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void grvthongbaophihoivien_canhan_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        LoadThongTin();
    }

    protected void grvThongBaoPhiCongTy_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirectioncty"] == null)
            ViewState["sortdirectioncty"] = "asc";
        else
        {
            if (ViewState["sortdirectioncty"].ToString() == "asc")
                ViewState["sortdirectioncty"] = "desc";
            else
                ViewState["sortdirectioncty"] = "asc";
        }
        ViewState["sortexpressioncty"] = e.SortExpression;
        LoadThongTin();
    }

    protected void MaLopHoc_OnTextChanged(object sender, EventArgs e)
    {
       LoadThongTin();
    }

    protected void btnChonLopHoc_Click(object sender, EventArgs e)
    {
        MaLopHoc_OnTextChanged(null, null);
    }
}