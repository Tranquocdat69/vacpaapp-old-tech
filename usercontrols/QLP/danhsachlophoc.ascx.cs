﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_danhsachlophoc : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        grvDanhSach.Columns[0].HeaderText = "Mã Lớp học";
        grvDanhSach.Columns[1].HeaderText = "Tên Lớp học";
        grvDanhSach.Columns[2].HeaderText = "Chọn";

        SqlCommand sql = new SqlCommand();
        sql.CommandText = @"SELECT LopHocID, MaLopHoc, TenLopHoc FROM tblCNKTLopHoc where TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
        DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
        grvDanhSach.DataSource = dtbTemp;
        grvDanhSach.DataBind();
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]) || !string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"SELECT LopHocID, MaLopHoc, TenLopHoc FROM tblCNKTLopHoc where TinhTrangID = " +
                              (int) EnumVACPA.TinhTrang.DaPheDuyet;
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
            {
                sql.CommandText += " AND MaLopHoc like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
            }
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            {
                sql.CommandText += " AND TenLopHoc like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
            }
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            grvDanhSach.DataSource = dtbTemp;
            grvDanhSach.DataBind();
        }
    }
}