﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="thanhtoanphicanhan_chitiet.ascx.cs" Inherits="usercontrols_thanhtoanphicanhan_chitiet" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 15%;
    }
    .style2
    {
        width: 35%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }

</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>

    <script type="text/javascript">


        function submitform() {
            jQuery("#form_thanhtoanphicanhan_chitiet").submit();
        }

        //        jQuery(function ($) {
        //            $('.auto').autoNumeric('init');
        //            // $('#txtTyLe>').autoNumeric('init');
        //            alert('auto');
        //        });

        function ganso() {            
            $('.auto').autoNumeric('init');
        }

        function capnhattong() {
            document.getElementById('<%= btntong.ClientID %>').click();
        }

        function loadgrid() {
            document.getElementById('<%= lbtSearch.ClientID %>').click();
        }


    </script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


    <%--<form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphicanhan_chitiet" runat="server"   method="post">
    <asp:ScriptManager ID="ScriptManagerPhiCaNhanChiTiet" runat="server"></asp:ScriptManager>
    <div id="thongbaoloi_form_thanhtoanphicanhan_chitiet" name="thongbaoloi_form_thanhtoanphicanhan_chitiet" style="display:none" class="alert alert-error"></div>
     
      <table id="Table1" width="920px" border="0" class="formtbl" >
      <tr>
          <td class="style1"><asp:Label ID="lblIDKhachHang" runat="server" Text = 'ID khách hàng:' ></asp:Label>
          <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2" >
        <asp:HiddenField runat ="server" id="HoiVienCaNhanID"></asp:HiddenField>
        <asp:HiddenField runat ="server" id="HoVaTen"></asp:HiddenField>
        <asp:HiddenField runat ="server" id="SoCCKTV"></asp:HiddenField>
        <asp:HiddenField runat ="server" id="NgayCap"></asp:HiddenField>
        <asp:HiddenField runat ="server" id="DonViCongTac"></asp:HiddenField>
        
        <input type = "text" onchange = "loadgrid();" name="MaHoiVienCaNhan" id="MaHoiVienCaNhan" value = "<%=Request.Form["MaHoiVienCaNhan"]%>" /></td>
        <td class="style3"><a id="btn_them"  href="#none" class="btn btn-rounded" onclick="openKhachHang();">...</a></td>
        <td><div ID="HoVaTenKhachHang" name = "HoVaTenKhachHang"><%=Request.Form["HoVaTenKhachHang"]%></div>
            </td>
      </tr>
      </table>

    <asp:UpdatePanel ID="UpdatePanelPhiCaNhanChiTiet" runat="server"    >
       <ContentTemplate>

            <div style="width: 100%; text-align: center; margin-top: 5px;">
    <asp:LinkButton ID="lbtSearch" runat="server" CssClass="btn" 
        onclick="lbtSearch_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton></div>
   <div><br /><b>Các loại phí phải nộp</b></div>
       <asp:GridView ClientIDMode="Static" ID="thanhtoanphicanhanchitiet_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="thanhtoanphicanhanchitiet_grv_PageIndexChanging" 
       onsorting="thanhtoanphicanhanchitiet_grv_Sorting"
       OnDataBound = "thanhtoanphicanhanchitiet_grv_OnDataBound"
       style = "width: 920px"
       DataKeyNames = "LoaiPhi"
       >
          <Columns>
                <asp:TemplateField  >
                <HeaderStyle Width="50px" HorizontalAlign = "Center"/>
                <ItemStyle HorizontalAlign = "Center"/>
                  <ItemTemplate>
                      <span><%# Container.DataItemIndex + 1%></span>
                  </ItemTemplate>
              </asp:TemplateField>
               <asp:TemplateField  >
               <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(truongmaphi)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="420px" />
                  <ItemTemplate>
                     <span><%# Eval(truongloaiphi)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                     <asp:Label name = "TruongSoPhiPhaiNop" ID = "TruongSoPhiPhaiNop" runat = "server" Text= <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N2}", Eval(truongsophiphainop))%>></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>
                                     
              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                  <asp:TextBox onchange = "capnhattong();" name = "TruongSoPhiNop" ID = "TruongSoPhiNop" Text = <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N2}", Eval(truongsophinop))%> runat ="server" class = "auto" data-a-sep="." data-a-dec="," data-v-min="0" data-v-max="<%# Eval(truongsophiphainop)%>" onkeypress="if(event.keyCode<48 || event.keyCode>57){event.returnValue=false;} else {ganso();}"></asp:TextBox>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField Visible = "false">
              <HeaderStyle Width="50"/>
                  <ItemTemplate>
                      <asp:Label name = "LoaiPhi" ID = "LoaiPhi" runat = "server" Text =<%# Eval("LoaiPhi")%>></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>

     <div style = "display: none">
         <asp:LinkButton ID="btnrefresh" runat="server" CssClass="btn" 
        onclick="btnrefresh_OnClick"><i class="iconfa-search">
    </i>Ref</asp:LinkButton>
    <asp:LinkButton ID="btntong" runat="server" CssClass="btn" 
        onclick="btntong_OnClick"><i class="iconfa-search">
    </i>Ref</asp:LinkButton>
    </div>

      </ContentTemplate>

      </asp:UpdatePanel>
 
    </form>
                    <div id = "div_danhsachhoivien"></div>       
<iframe name="iframe_ajax" width="0px" height="0px"></iframe>
<script type="text/javascript">

    function fill_thongtinhoivien(id) {
        var timestamp = Number(new Date());

        $.get('noframe.aspx?page=danhsachhoiviencanhan_ajax&id=' + id + '&time=' + timestamp, function (data) {
            eval(data);
            // Xóa các giá trị cũ trên form và cập nhật bằng giá trị nhận được từ AJAX response

            document.getElementById('<%=HoiVienCaNhanID.ClientID %>').value = "";
            document.getElementById('<%=HoiVienCaNhanID.ClientID %>').value = HoiVienCaNhanID;
            document.getElementById('<%=HoVaTen.ClientID %>').value = "";
            document.getElementById('<%=HoVaTen.ClientID %>').value = HoTen;
            document.getElementById('<%=SoCCKTV.ClientID %>').value = "";
            document.getElementById('<%=SoCCKTV.ClientID %>').value = SoChungChiKTV;
            document.getElementById('<%=NgayCap.ClientID %>').value = "";
            document.getElementById('<%=NgayCap.ClientID %>').value = NgayCapChungChiKTV;
            document.getElementById('<%=DonViCongTac.ClientID %>').value = "";
            document.getElementById('<%=DonViCongTac.ClientID %>').value = DonViCongTac;
            document.getElementById('MaHoiVienCaNhan').value = "";
            document.getElementById('MaHoiVienCaNhan').value = MaHoiVienCaNhan;
            $('#HoVaTenKhachHang').html('Họ và tên: ' + HoTen);
        });
    }

    function dong() {
        $(this).close();
    }
//    function fill_thongtinhoivien(id) {
//        funLoadGrid();
//    }
</script>
<script type="text/javascript">

    function openKhachHang() {
        var timestamp = Number(new Date());
        $("#div_danhsachhoivien").empty();
        $("#div_danhsachhoivien").append($("<iframe width='100%' height='100%' scrolling='no' frameborder='0' />").attr("src", "iframe.aspx?page=danhsachoiviencanhan&time=" + timestamp));

        $("#div_danhsachhoivien").dialog({
            autoOpen: false,
            title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách hội viên cá nhân</b>",
            modal: true,
            width: "640",
            heigth: "480",
            buttons: [
                    {
                        text: "Bỏ qua",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                    ]
        }).dialog("open");
    }






    </script>
