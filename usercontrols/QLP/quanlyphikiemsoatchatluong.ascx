﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="quanlyphikiemsoatchatluong.ascx.cs" Inherits="usercontrols_quanlyphikiemsoatchatluong" %>
<style>
    .require {color:Red;}
    .error {color:Red; font-size:11px;}
    .style1
    {
        width: 152px;
    }.style2
     {
         width: 30%;
     }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }
    .style5
    {
        width: 20%;
    }
    .style6
    {
        width: 27%;
    }
    .style7
    {
        width: 246px;
    }
    
    div#leftdivDSCty
    {
        float:left;
        width:50%;
    }
    
    div#rightdivDSCty
    {
        float:right;
        width:50%;
    }
    
    div#divTableDuLieuDS
    {
        clear:both;
        width:100%
    }
    /*table#t02, table#t02 tr, table#t02 td, table#t02 th
    {
        border: 1px solid black;
        border-collapse: collapse;
        padding:5px;
    }*/ 
    
    /*table#t02
    {
        width:100%;
    }

    table#t08, table#t08 tr, table#t08 td, table#t08 th
    {
        border: 1px solid black;
        border-collapse: collapse;
        padding:5px;
    } 

    th
    {
        background-color:Menu;
    }*/
    
    p#p04
    {
        display:inline;
        width:100%
    }
    .col_stt, .col_btn {
        text-align: center;
    }
    .col_btn a {text-decoration: none; color: #000; }
  
    div#lefdivTTDSCty
    {
        float:left;
    }
  
    table#tbTruong
    {
        float:right;
    }
    div#divTableDuLieuDS
    {clear:both;}
  
</style>
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<script type="text/javascript">
    function xoa(id) {
        window.location = 'admin.aspx?page=quanlyphikiemsoatchatluong&id=' + id + '&mode=view&tinhtrang=xoa';

    };

    function duyet(id) {
        window.location = 'admin.aspx?page=quanlyphikiemsoatchatluong&id=' + id + '&mode=view&tinhtrang=duyet';
    };

    function tuchoi(id) {

        window.location = 'admin.aspx?page=quanlyphikiemsoatchatluong&id=' + id + '&mode=view&tinhtrang=tuchoi';

    };

    function thoaiduyet(id) {
        window.location = 'admin.aspx?page=quanlyphikiemsoatchatluong&id=' + id + '&mode=view&tinhtrang=thoaiduyet';

    };

</script>
<script type="text/javascript">
    function reCalljScript() {

        $('.child').hide();
        $('.btn-collapse').click(function () {
            if ($(this).text() == '+') {
                $(this).text('-');
                $(this).parent().parent().nextUntil('.parent').show();
            } else {
                $(this).text('+');
                $(this).parent().parent().nextUntil('.parent').hide();
            }
        });
      
    }
</script>

<script type = "text/javascript">
    function functionx(evt) {
        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
            alert("Dữ liệu chỉ nhận kiểu số!");
            return false;
        }
    }
     </script>

     <script type = "text/javascript">
         function boEnter(evt) {
             if (evt.charCode == 13) {
                 //alert("Dữ liệu chỉ nhận kiểu số!");
                 return false;
             }
         }
     </script>

<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_quanlyphikiemsoatchatluong" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_quanlyphikiemsoatchatluong" clientidmode="Static" runat="server"   method="post">
    <asp:HiddenField ID="RandomList" Value="" runat="server" />
     <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
     <%--<asp:UpdatePanel ID="UpdatePanelPhiTapThe" runat="server">
       <ContentTemplate>--%>
     <h4 class="widgettitle"><%=tenchucnang%></h4>
    <div id="thongbaoloi_form_quanlyphikiemsoatchatluong" name="thongbaoloi_form_quanlyphikiemsoatchatluong" style="display:none" class="alert alert-error"></div>
        <table id="tblThongBao" width="100%" border="0" class="formtbl">
        <tr>
         <td>
         <div ID="TinhTrangBanGhi" name = "TinhTrangBanGhi"></div>
        </td>
        </tr>
        </table>
        <p style="font-weight:bold; color:Blue; margin-top:10px">Thông tin phí</p>
      <table id="Table1" width="100%" border="0" class="formtbl" >
      <tr>
          <td class = "style1"><asp:Label ID="lblMaPhi" runat="server" Enabled = "false" Text = 'Mã phí KSCL:' ></asp:Label></td>
        <td colspan = "2" class="style2"><input type="text" name="MaPhi" id="MaPhi" disabled = "disabled" /></td>
        <td class = "style1" ><asp:Label ID="lblNgayLap" runat="server" Text = 'Ngày lập:' ></asp:Label>
            <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td><input type="text" name="NgayLap" id="NgayLap" /></td>
      </tr>
       <tr>
          <td class = "style1"><asp:Label ID="lblDienGiai" runat="server" Text = 'Diễn giải:' ></asp:Label></td>
        <td colspan ="4"><input size = "100%" type="text" name="DienGiai" id="DienGiai" onkeypress="return boEnter(event)" /></td>
      </tr>
       
      <tr>
        <td class = "style1"><asp:Label ID="lblIDDanhSachKiemTraTrucTiep" runat="server" Text = 'Mã danh sách công ty kiểm tra trực tiếp:' ></asp:Label>
        <asp:Label ID="Label3" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
          <asp:HiddenField runat ="server" id="DanhSachKiemTraTrucTiepID"></asp:HiddenField>   
          <asp:HiddenField runat ="server" id="DanhSachKiemTraTrucTiepMa"></asp:HiddenField>
          <asp:HiddenField runat ="server" id="HoiVienCaNhanIdBiXoa"></asp:HiddenField>   
        <td><asp:TextBox name="DanhSachKiemTraTrucTiep" id="DanhSachKiemTraTrucTiep" runat ="server" ReadOnly = "true" OnTextChanged = "DanhSachKiemTraTrucTiep_OnTextChanged" AutoPostBack = "true" /></td>
        <%--<td class = "style3"><asp:Button name = "ChonDanhSach" ID = "ChonDanhSach" runat = "server" OnClientClick="javascript:alert('Đang hoàn thiện')" OnClick = "ChonDanhSach_OnClick" Text = "..."/></td>--%>
        <td class = "style3"><a id="akickChon" onclick="open_ChonCongTy()" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Thêm HVCN"  rel="tooltip" class="btnabc"><input type="button" value = "..." id="btThemCongTy" name="btThemCongTy" /></a></td>
        <td class = "style1"><asp:Label ID="lblThoiHanNopPhi" runat="server" Text = 'Thời hạn nộp phí:' ></asp:Label>
        <asp:Label ID="Label2" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td><input type="text" name="ThoiHanNopPhi" id="ThoiHanNopPhi" /></td>

      </tr>

      </table>

           <div style = "display: none">
    <asp:LinkButton ID="btnLuu" runat="server" CssClass="btn" 
        onclick="btnLuu_OnClick"><i class="iconfa-search">
    </i>btnTestLuu</asp:LinkButton>
    </div>
        
        
               <div style = "display: none">
    <asp:LinkButton ID="btnThoaiDuyet" runat="server" CssClass="btn" 
        onclick="btnThoaiDuyet_Click"><i class="iconfa-search">
    </i>btnTestLuu</asp:LinkButton>
    </div>
        
           
        <div style = "display: none">
    <asp:LinkButton ID="btnCapNhat" runat="server" CssClass="btn" 
        onclick="btnCapNhat_OnClick"><i class="iconfa-search">
    </i>btnTestLuu</asp:LinkButton>
    </div>    
          
           
        <div style = "display: none">
    <asp:LinkButton ID="btnXoaHoiVienCaNhan" runat="server" CssClass="btn" 
        onclick="btnXoaHoiVienCaNhan_OnClick"><i class="iconfa-search">
    </i>btnXoaHoiVienCaNhan</asp:LinkButton>
    </div>    
        
               <div style = "display: none">
    <asp:LinkButton ID="btnXoa" runat="server" CssClass="btn" 
        onclick="btnXoa_Click"><i class="iconfa-search">
    </i>btnTestLuu</asp:LinkButton>
    </div>
        
        
        <div style = "display: none">
    <asp:LinkButton ID="btnDuyet" runat="server" CssClass="btn" 
        onclick="btnDuyet_Click"><i class="iconfa-search">
    </i>btnTestLuu</asp:LinkButton>
    </div>
        
        <div style = "display: none">
    <asp:LinkButton ID="btnTuChoi" runat="server" CssClass="btn" 
        onclick="btnTuChoi_Click"><i class="iconfa-search">
    </i>btnTestLuu</asp:LinkButton>
    </div>

      <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  >
       <ContentTemplate>
       <script type="text/javascript" language="javascript">
           Sys.Application.add_load(reCalljScript);
     </script>
      <p style="font-weight:bold; color:Blue; margin-top:10px">Danh sách công ty kiểm toán và hội viên cá nhân</p>
      <div id="divTTDSCty">
      
          <%-- <table id="Table2" width="100%" border="0" class="formtbl" >
      <tr>
          <td class = "style7"><asp:Label ID="Label4" runat="server" Enabled = "false" Text = 'Gán nhanh phí cho công ty:' style="font-weight:bold" ></asp:Label></td>
            <td><asp:TextBox name="MaPhiCTy" id="idPhiCty"  runat ="server" AutoPostBack = "false" onkeypress="return functionx(event)" ></asp:TextBox></td>
            <td>VND</td>
            <td class = "style1" ><asp:Label ID="Label5" runat="server" Text = 'Gán nhanh phí cho HVCN:' style="font-weight:bold"></asp:Label></td>
            <td><asp:TextBox name="MaPhiHVCN" id="idPhiHVCN" runat ="server" AutoPostBack = "false" onkeypress="return functionx(event)" ></asp:TextBox></td>
            <td>VND</td>
      </tr>
       <tr>
          <td colspan="3"><asp:LinkButton ID="btnThemNS123" runat="server" CssClass="btn" 
        onclick="btnThemNS123_OnClick" >
            <input type="button" id = "btCapNhatPhi123" name = "nameCapNhatPhi123"
            value = "Cập nhật phí" style="background-color:#ccc; border:1px black solid;" /></asp:LinkButton></td>
        <td colspan ="3"><asp:TextBox placeholder="<Tìm kiếm>" name="TimKiem" ID="IDTimKiem" runat ="server" OnTextChanged = "TimKiem_OnTextChanged" AutoPostBack = "true" /></td>
        
      </tr>
       
      

      </table>--%>
<%--         <div id="lefdivTTDSCty" width="100%">--%>
           
            <table width="100%" border="0" class="formtbl">
                <tr>
                <td class = "style5"><asp:Label ID="Label4" runat="server" Text = 'Gán nhanh phí cho công ty:' style="font-weight:bold" ></asp:Label></td>
           
                <td class="style6"><asp:TextBox name="MaPhiCTy" id="idPhiCty"  runat ="server" AutoPostBack = "false" class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="999999999999999999"></asp:TextBox></td>
            <td>VNĐ</td>
                 <td class = "style5" ><asp:Label ID="Label5" runat="server" Text = 'Gán nhanh phí cho KTV:' style="font-weight:bold"></asp:Label></td>
            <%--<td><asp:TextBox name="MaPhiHVCN" id="idPhiHVCN" runat ="server" AutoPostBack = "false" onkeypress="return functionx(event)" ></asp:TextBox></td>--%>
                <td class="style6"><asp:TextBox name="MaPhiHVCN" id="idPhiHVCN" runat ="server" AutoPostBack = "false"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="999999999999999999"></asp:TextBox></td>
            <td>VNĐ</td>
            </tr>
            <tr width="100%">
                <td colspan="3"><a id='btn_capnhatphi' href='#none' class='btn btn-rounded' onclick='capnhat();'><i class='iconfa-plus-sign'></i> Cập nhật phí</a></td>
                <td colspan ="3">
                    <asp:TextBox placeholder="<Tìm kiếm>" name="TimKiem" 
                        ID="IDTimKiem" runat ="server" OnTextChanged = "TimKiem_OnTextChanged" 
                        AutoPostBack = "true" /></td>
            </tr>
          </table>
    <%--     </div>--%> <%--dong div rigdivTTDSCty--%>

      </div> <%--dong div divDSCty--%>
      <%--<asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  >
       <ContentTemplate>--%>
           <style>
               .table.table-bordered th {border-color: #fff}

           </style>
      <div id = "divTableDuLieuDS">
            <table id="t02" class="table table-bordered responsive dyntable" >
                <tr>
                    <th width="10%" class="col_stt">STT</th>
                    <th width="10%"></th>
                    <th width="40%">Tên cty kiểm toán</th>
                    <th width="30%">Phí kiểm soát</th>
                    <th width="10%"></th>
                </tr>

               
            <asp:Repeater runat="server" ID="rptCha"  onitemdatabound="rptCha_ItemDataBound" 
                    onitemcommand="rptCha_ItemCommand">
            <ItemTemplate>
                <%--2 vòng lặp lồng nhau để hiển thị Cha-Con --%>
                <tr class="parent">
                    <td class="col_stt"><%#Eval("STT") %></td>
                    <td class="col_btn"><a class="btn-collapse" href="javascript:void(0)">+</a></td>
                    
                    <td><%#Eval("TenDoanhNghiep")%></td>
                    <td><%#Eval("PhiKiemSoat")%></td>
                    <td>
                        <%--<asp:Button ID="btThemMoiNS" runat="server" CommandName="ThemMoiNS" CommandArgument='<%#Eval("HoiVienTapTheID") %>' Text="Thêm HVCN"/>--%>
                        <a class="idKickChon2" onclick="open_ThemHoiVien_add(<%#Eval("HoiVienTapTheID") %>)" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Thêm KTV"  rel="tooltip" class="btnabc"><input type="button" value = "Thêm KTV"  <%#Eval("txtdis") %> class="btThemTrongTB" /></a>
                        <%--<input type="button" id="btThemMoiNS" CommandName="ThemMoiNS" CommandArgument='<%#Eval("HoiVienTapTheID") %>' value="Thêm HVCN" OnClick = "ThemHoiVien_Click"/>--%>
                    </td>
                </tr>

                <asp:Repeater runat="server" ID="rptCon" >
            <ItemTemplate>
                <tr class="child">
                    <td></td>
                    <td class="col_btn"><input type="checkbox" name="" /></td>
                    <td><%#Eval("HoTen") %></td>
                    <td><%#Eval("PhiKiemSoat")%></td>
                    <td>
                         <a class="idKickChon2" onclick="Delete_KTV('<%#Eval("MaNhanSu")%>', '<%#Eval("HoTen")%>')" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xóa"  rel="tooltip" class="btnabc"><input type="button" value = "Xóa" class="btThemTrongTB" /></a>
                    </td>
                </tr>
                </ItemTemplate>
        </asp:Repeater>

            </ItemTemplate>
        </asp:Repeater>
            </table>
      </div> <%--dong div divTableDuLieuDS--%>
      
      </ContentTemplate></asp:UpdatePanel>
      <div  class='dataTables_length'>
      
      <%--<a id='btn_luu' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>--%>
<%--   <asp:Button runat='server' width = "100px" ID = 'bt' Text = 'Lưu' OnClick='Luu_Click' Enabled="true" />
   <asp:Button runat='server' width = "80px" ID = 'btXoa' Text = 'Xóa' OnClick='Xoa_Click' Enabled="true" />
   <asp:Button runat='server' width = "80px" ID = 'btDuyet' Text = 'Duyệt' OnClick='Duyet_Click' Enabled="true" />
   <asp:Button runat='server' width = "80px" ID = 'btTuChoi' Text = 'Từ chối' OnClick='TuChoi_Click' Enabled="true" />
   <asp:Button runat='server' width = "90px" ID = 'btThoaiDuyet' Text = 'Thoái duyệt' OnClick='ThoaiDuyet_Click' Enabled="true" />--%>
</div> <%-- dong div nut btnluu--%>
    <%--</ContentTemplate>
      </asp:UpdatePanel>  --%>  
      
      <div id="div_dmNhanSuHoiVien_add"></div>
  
  <div style = "display: none">
    <asp:LinkButton ID="btnChay" runat="server" CssClass="btn" 
        onclick="btnChay_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>
    
    </form>
<%LoadNut(); %>
        <%AnNut(); %>
    <%-- <script type="text/javascript">
         $(document).ready(function () {
             $('.child').hide();
             $('.btn-collapse').click(function () {
                 if ($(this).text() == '+') {
                     $(this).text('-');
                     $(this).parent().parent().nextUntil('.parent').show();
                 } else {
                     $(this).text('+');
                     $(this).parent().parent().nextUntil('.parent').hide();
                 }
             });
         });
        </script>--%>


        <script type="text/javascript">
    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });
      
    <%LoadThongTin(); %>
</script>
                                                    
<script type="text/javascript">
       

        function submitform() {        
                jQuery("#form_quanlyphikiemsoatchatluong").submit();
            };

            function luu() {        
                document.getElementById('<%= btnLuu.ClientID %>').click();
        };
//        $.datepicker.setDefaults($.datepicker.regional['vi']);

    
        

       $(function () {  $('#NgayLap, #ThoiHanNopPhi').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });

    $(function () {$('#NgayLap, #ThoiHanNopPhi').datepicker('setDate', new Date())});

   
</script>

<script type = "text/javascript">
//    function open_ChonCongTy() {
//        var timestamp = Number(new Date());
//       
//        $("#div_dmNhanSuHoiVien_add").empty();
//        $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_ChonCongTy' name='iframe_ChonCongTy' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmChonCongTy&time=" + timestamp));
//        $("#div_dmNhanSuHoiVien_add").dialog({
//            resizable: true,
//            width: 500,
//            height: 600,
//            title: "Thêm <%=tenchucnang%>",
//            modal: true,
//            close: function (event, ui) { window.location = 'admin.aspx?page=quanlyphikiemsoatchatluong&choncongty=ok'; },
//            //close: function (event, ui) { },
//            buttons: {

//                "Ghi": function () {
//                    $("#iframe_dmChonCongTy").contents().find('form').submit();
//                    $(this).dialog("close");
//                    //                    window.frames['iframe_dmNhanSuHoiVien_add'].chonhoso();
//                    //                    
//                    //                    $(this).dialog("close");
//                    //                    location.reload();
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_dmNhanSuHoiVien_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_dmNhanSuHoiVien_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    //    }

    //function open_ChonCongTy() {
    //    var timestamp = Number(new Date());
    //    $("#div_dmNhanSuHoiVien_add").empty();
    //    $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoiviencanhan' name='iframe_hoiviencanhan' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=quanlyphikscl_danhsachcongty_canhan&type=tapthe&mode=iframe&time=" + timestamp));
    //    $("#div_dmNhanSuHoiVien_add").dialog({
    //        autoOpen: false,
    //        title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách hội viên</b>",
    //        modal: true,
    //        width: "640",
    //        height: "480",
    //        buttons: [
    //             {
    //                 text: "Chọn",
    //                 click: function () {
    //                     chonhoiviencanhan();
    //                 }
    //             },
    //                {
    //                    text: "Đóng",
    //                    click: function () {
    //                        $(this).dialog("close");
    //                    }
    //                }
    //        ]
    //    }).dialog("open");
    //}
    function open_ChonCongTy() {
        var timestamp = Number(new Date());
        $("#div_dmNhanSuHoiVien_add").empty();
        $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoivien' name='iframe_hoivien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=danhsachcongtykiemtratructiep&mode=iframe&time=" + timestamp));
        $("#div_dmNhanSuHoiVien_add").dialog({
            autoOpen: false,
            // title: "<img src='images/icons/color/application_form_magnify.png'> <b>Danh sách kiểm tra trực tiếp</b>",
            title: "<b>Danh sách kiểm tra trực tiếp</b>",
            modal: true,
            width: "640",
            height: "480",
            buttons: [
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
            ]
        }).dialog("open");
    }
    
    function chondanhsach(id, ma) {
        document.getElementById('<%=DanhSachKiemTraTrucTiepID.ClientID %>').value = id;
        document.getElementById('<%=DanhSachKiemTraTrucTiepMa.ClientID %>').value = ma;
        $("#div_dmNhanSuHoiVien_add").dialog('close');
            document.getElementById('<%= btnChay.ClientID %>').click();
        }
</script>

<script type = "text/javascript">

    function open_ThemHoiVien_add(id) {
        var timestamp = Number(new Date());
        $("#div_dmNhanSuHoiVien_add").empty();
        $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_hoiviencanhan' name='iframe_hoiviencanhan' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=quanlyphikscl_danhsachcongty_canhan&id=" + id + "&type=canhan&mode=iframe&time=" + timestamp));
        $("#div_dmNhanSuHoiVien_add").dialog({
            autoOpen: false,
            title: "<b>Danh sách hội viên</b>",
            modal: true,
            width: "640",
            height: "480",
            buttons: [
                 {
                     text: "Chọn",
                     click: function () {
                         chonhoiviencanhan();
                     }
                 },
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
            ]
        }).dialog("open");
    }
    
    function Delete_KTV(id, ten) {
        if (confirm('Xóa KTV ' + ten + "?")) {
            document.getElementById('<%=HoiVienCaNhanIdBiXoa.ClientID %>').value = id;
            document.getElementById('<%= btnXoaHoiVienCaNhan.ClientID %>').click();
        }
    }

//    function open_ThemHoiVien_add(id) {
//        var timestamp = Number(new Date());

//        $("#div_dmNhanSuHoiVien_add").empty();
//        $("#div_dmNhanSuHoiVien_add").append($("<iframe width='100%' height='100%' id='iframe_dmNhanSuHoiVien_add' name='iframe_dmNhanSuHoiVien_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmNhanSuHoiVien_add&id=" + id + "&mode=add&time=" + timestamp));
//        $("#div_dmNhanSuHoiVien_add").dialog({
//            resizable: true,
//            width: 500,
//            height: 600,
//            title: "Thêm <%=tenchucnang%>",
//            modal: true,
//            close: function (event, ui) { window.location = 'admin.aspx?page=quanlyphikiemsoatchatluong&idThem=ok&choncongty=ok'; },
//            //close: function (event, ui) { },
//            buttons: {

//                "Ghi": function () {
//                    $("#iframe_dmNhanSuHoiVien_add").contents().find('form').submit();
//                                        $(this).dialog("close");
////                    window.frames['iframe_dmNhanSuHoiVien_add'].chonhoso();
////                    
////                    $(this).dialog("close");
////                    location.reload();
//                },

//                "Bỏ qua": function () {
//                    $(this).dialog("close");
//                }
//            }

//        });

//        $('#div_dmNhanSuHoiVien_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
//        $('#div_dmNhanSuHoiVien_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    //}
</script>

<script type="text/javascript">
    function chonhoiviencanhan() {
        $("#div_dmNhanSuHoiVien_add").dialog('close');
        document.getElementById('<%= btnChay.ClientID %>').click();
    }
</script>

<%--<script type ="text/javascript">
    function luu(id) {
    
        <%Luu_Click(id); %>
        
    };

    function xoa(id) {
        <%Xoa_Click(id); %>

    };

    function duyet(id) {
        <%Duyet_Click(id); %>
    };

    function tuchoi(id) {

        <%TuChoi_Click(id); %>

    };

    function thoaiduyet(id) {
        <%ThoaiDuyet_Click(id); %>

    };

</script>--%>

<script type="text/javascript">
        <%LoadThongTin(); %>

         var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function() {
     $('#NgayLap, #ThoiHanNopPhi').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');

        $('#NgayLap').datepicker('setDate', new Date())
        $('#ThoiHanNopPhi').datepicker('setDate', new Date())
});

        </script>

        <script type="text/javascript">
            <%--function capnhattong() {
                document.getElementById('<%= btnThemNS123.ClientID %>').click();
            }--%>
            

            function capnhat() {
                document.getElementById('<%= btnCapNhat.ClientID %>').click();
            }

            function disableNut() {
                document.getElementById("btThemCongTy")[0].disabled = true; 
            }

            function enableNut() {
                document.getElementById("btThemCongTy")[0].disabled = false;
            }
        </script>

<%--<script type="text/javascript">
    <%AnNut(); %>  
</script>--%>