﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class usercontrols_BaoCaoDanhSachKiemToanVienTrongCaNuoc_ChiTiet : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách Kiểm toán viên trong cả nước (Chi tiết)";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            if (this.IsPostBack)
                setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void LoadVungMien(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT VungMienID, TenVungMien FROM tblDMVungMien";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["VungMienID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["VungMienID"] + @""">" +
                               dtr["TenVungMien"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["VungMienID"] + @""">" + dtr["TenVungMien"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadTinhThanh(string VungMienId = "", string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where TinhID in (" + Id + ")";
        sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where 0 = 0 AND MATINH IS NOT NULL";
        if (VungMienId != "")
            sql.CommandText += " AND VungMienID In (" + VungMienId + ")";
        //if (Id != "")
        //    sql.CommandText += " AND MaTinh In (" + Id + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["MaTinh"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["MaTinh"] + @""">" +
                               dtr["TenTinh"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["MaTinh"] + @""">" + dtr["TenTinh"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);

    }

    public void LoadTenCongTy(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheId, TenDoanhNghiep FROM tblHoiVienTapThe";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["HoiVienTapTheId"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["HoiVienTapTheId"] + @""">" +
                               dtr["TenDoanhNghiep"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["HoiVienTapTheId"] + @""">" + dtr["TenDoanhNghiep"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadChungChiQuocTe(string Id = "")
    {
        string output_html = "";
        //-1: tất. 0: không có. 1: Có
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        if (Id.Contains("1"))
            output_html += @"<option selected=""selected"" value=1>Có chứng chỉ Quốc tế</option>";
        else
            output_html += @"<option value=1>Có chứng chỉ Quốc tế</option>";
        if (Id.Contains("0"))
            output_html += @"<option selected=""selected"" value=0>Không có chứng chỉ Quốc tế</option>";
        else
            output_html += @"<option value=0>Không có chứng chỉ Quốc tế</option>";
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadChucDanh(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT ChucVuID, TenChucVu FROM tblDMChucVu";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["ChucVuID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["ChucVuID"] + @""">" +
                               dtr["TenChucVu"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["ChucVuID"] + @""">" + dtr["TenChucVu"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void setdataCrystalReport()
    {
        List<objDanhSachKiemToanVienTrongCaNuoc_ChiTiet> lst = new List<objDanhSachKiemToanVienTrongCaNuoc_ChiTiet>();
        string strTenCongTy = Request.Form["TenCongTy"];
        string strChungChiQuocTe = Request.Form["ChungChiQuocTe"];
        string strChucDanh = Request.Form["ChucDanh"];
        string strVungMien = Request.Form["VungMien"];
        string strTinhThanh = Request.Form["TinhThanh"];

        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = @"select *, CASE WHEN SoHieu IS NULL THEN 'zzzzzzzzzzz' when SoHieu IS NOT NULL THEN SoHieu END as ABC from (select A.*, TenChucVu, TenTinh, TenQuocTich, SoHieu, TenDoanhNghiep
, C.VungMienID, (datepart(yyyy,GETDATE())  - YEAR(NgaySinh)) as DoTuoi, F.SoChungChiQuocTe from tblHoiVienCaNhan A 
left join tblDMChucVu B on A.ChucVuID = B.ChucVuID 
left join tblDMTinh C on A.QueQuan_TinhID = C.TinhID
left join tblDMQuocTich D on A.QuocTichID = D.QuocTichID
left join tblHoiVienTapThe E on A.HoiVienTapTheID = E.HoiVienTapTheID 
left join (select Count(ChungChiQuocTeID) as SoChungChiQuocTe, HoiVienCaNhanID from tblHoiVienCaNhanChungChiQuocTe
group by HoiVienCaNhanID)
as F
on F.HoiVienCaNhanID = A.HoiVienCaNhanID 
where LoaiHoiVienCaNhan <> 0) as Temp
where (0 = 0
";

        //Lọc theo Tên công ty được chọn khác tất cả
        if (!string.IsNullOrEmpty(strTenCongTy) && !strTenCongTy.Contains("-1"))
            sql.CommandText += " AND (HoiVienTapTheID in (" + strTenCongTy + "))";
        //Lọc theo Chứng chỉ được chọn khác tất cả
        if (!string.IsNullOrEmpty(strChungChiQuocTe) && !strChungChiQuocTe.Contains("-1"))
        {
            if (strChungChiQuocTe == "1")
                sql.CommandText += " AND (SoChungChiQuocTe > 0)";
            else if (strChungChiQuocTe == "0")
                sql.CommandText += " AND (SoChungChiQuocTe IS NULL OR SoChungChiQuocTe = 0)";
        }
        //Lọc theo Tỉnh thành được chọn khác tất cả
        if (!string.IsNullOrEmpty(strTinhThanh) && !strTinhThanh.Contains("-1"))
            sql.CommandText += " AND DiaChi_TinhID in (" + strTinhThanh + ")";
        else if (strTinhThanh == "-1")
        {
            //Lọc theo Vùng miền được chọn với Tỉnh bằng tất cả và vùng miền khác tất cả
            if (!string.IsNullOrEmpty(strVungMien) && !strVungMien.Contains("-1"))
                sql.CommandText += " AND VungMienID in (" + strVungMien + ")";
        }

        //Lọc theo Chức danh được chọn khác tất cả
        if (!string.IsNullOrEmpty(strChucDanh) && !strChucDanh.Contains("-1"))
            sql.CommandText += " AND (ChucVuID in (" + strChucDanh + "))";

        //Lọc theo Ngày sinh
        if (Request.Form["ngaysinhtu"] != null)
        {
            string Tu = Request.Form["ngaysinhtu"].ToString();
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (NgaySinh >= '" +
                                   (DateTime.ParseExact(Request.Form["ngaysinhtu"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }
        if (Request.Form["ngaysinhden"] != null)
        {
            string Den = Request.Form["ngaysinhden"].ToString();
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (NgaySinh <= '" +
                                   (DateTime.ParseExact(Request.Form["ngaysinhden"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }

        //Lọc theo độ tuổi
        if (Request.Form["dotuoitu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["dotuoitu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (DoTuoi >= " + Tu + ") ";
        }
        if (Request.Form["dotuoiden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["dotuoiden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (DoTuoi <= " + Den + ") ";
        }


        if (((Request.Form["danghanhnghe"] != null) && (Request.Form["khonghanhnghe"] != null))
            || ((Request.Form["danghanhnghe"] == null) && (Request.Form["khonghanhnghe"] == null)))
        {
        }
        else
        {
            //Nếu không check đang hành nghề => lọc bỏ hành nghề
            if (Request.Form["danghanhnghe"] != null)
            {
                sql.CommandText += " AND ((HanCapTu IS NULL OR HanCapTu <= GETDATE()) AND HanCapDen >= GETDATE()) ";
            }
                //Nếu không check hội viên => bỏ lọc hội viên
            else if (Request.Form["khonghanhnghe"] != null)
            {
                sql.CommandText +=
                    " AND (HanCapTu > GETDATE() OR HanCapDen IS NULL OR HanCapDen < GETDATE())";
            }
        }

        sql.CommandText += ") order by ABC, HoiVienCaNhanID";
        ds = DataAccess.RunCMDGetDataSet(sql);
       

        DataTable dt = ds.Tables[0];

        int i = 0;
        foreach (DataRow tem in dt.Rows)
        {
            i++;
            objDanhSachKiemToanVienTrongCaNuoc_ChiTiet obj = new objDanhSachKiemToanVienTrongCaNuoc_ChiTiet();
            obj.SoHieuCongTy = tem["SoHieu"].ToString();
            obj.TenCongTy = tem["TenDoanhNghiep"].ToString();
            obj.STT = i.ToString();
            obj.IDHVCN_IDKTV = tem["MaHoiVienCaNhan"].ToString();
            obj.HoVaTen = tem["HoDem"].ToString() + " " + tem["Ten"].ToString();
            string NgaySinhNam = "";
            string NgaySinhNu = "";
            try
            {
                if (tem["GioiTinh"].ToString() == "F")
                    if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                        NgaySinhNu = Convert.ToDateTime(tem["NgaySinh"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            try
            {
                if (tem["GioiTinh"].ToString() == "M")
                    if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                        NgaySinhNam = Convert.ToDateTime(tem["NgaySinh"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }
            
            obj.NgaySinhNam = NgaySinhNam;
            obj.NgaySinhNu = NgaySinhNu;

            //Chưa có trong DB
            //obj.SoQuyetDinhKetNapHVCN = "";
            //if (!string.IsNullOrEmpty(tem[""].ToString()))
            //obj.NgayKyQuyetDinhKetNapHVCN = "";

            string QueQuanQuocTich = "";
            if (!string.IsNullOrEmpty(tem["TenTinh"].ToString()))
                QueQuanQuocTich = tem["TenTinh"].ToString();
            if (!string.IsNullOrEmpty(tem["TenQuocTich"].ToString()))
                if (!string.IsNullOrEmpty(tem["TenTinh"].ToString()))
                    QueQuanQuocTich += "/" + tem["TenQuocTich"].ToString();
                else
                    QueQuanQuocTich = tem["TenQuocTich"].ToString();
            obj.QueQuanQuocTich = QueQuanQuocTich;
            obj.DiaChiNoiOHienTai = tem["DiaChi"].ToString();


            string strDaiHoc = "";
            if (tem["TruongDaiHocID"] != null)
            {
                if (!string.IsNullOrEmpty(tem["TruongDaiHocID"].ToString()))
                {
                    sql.CommandText = "Select * from tblDMTruongDaiHoc where TruongDaiHocID =" +
                                      tem["TruongDaiHocID"].ToString();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                        strDaiHoc =
                            ds.Tables[0].Rows[0]["TenTruongDaiHoc"].ToString();
                }
            }
            obj.TDCM_TotNghiepDaiHoc = strDaiHoc;

            string strChuyenNganh = "";
            if (tem["ChuyenNganhDaoTaoID"] != null)
            {
                if (!string.IsNullOrEmpty(tem["ChuyenNganhDaoTaoID"].ToString()))
                {
                    sql.CommandText = "Select * from tblDMChuyenNganhDaoTao where ChuyenNganhDaoTaoID =" +
                                      tem["ChuyenNganhDaoTaoID"].ToString();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                        strChuyenNganh =
                            ds.Tables[0].Rows[0]["TenChuyenNganhDaoTao"].ToString();
                }
            }
            obj.TDCM_ChuyenNganh = strChuyenNganh;
            obj.TDCM_NamTotNghiep = tem["ChuyenNganhNam"].ToString();

            string strHocVi = "";
            if (tem["HocViID"] != null)
            {
                if (!string.IsNullOrEmpty(tem["HocViID"].ToString()))
                {
                    sql.CommandText = "Select * from tblDMHocVi where HocViID =" +
                                      tem["HocViID"].ToString();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                        strHocVi =
                            ds.Tables[0].Rows[0]["TenHocVi"].ToString();
                }
            }
            obj.TDCM_HocVi = strHocVi;
            obj.TDCM_NamHocVi = tem["HocViNam"].ToString();

            string strHocHam = "";
            if (tem["HocHamID"] != null)
            {
                if (!string.IsNullOrEmpty(tem["HocHamID"].ToString()))
                {
                    sql.CommandText = "Select * from tblDMHocHam where HocHamID =" +
                                      tem["HocHamID"].ToString();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                        strHocHam =
                            ds.Tables[0].Rows[0]["TenHocHam"].ToString();
                }
            }
            obj.TDCM_HocHam = strHocHam;
            obj.TDCM_NamHocHam = tem["HocHamNam"].ToString();
            obj.SoCMND_HoChieu = tem["SoCMND"].ToString();
            obj.NgayCapCMND_HoChieu = tem["CMND_NgayCap"].ToString();
            obj.SoTaiKhoanNganHang = tem["SoTaiKhoanNganHang"].ToString();

            string strNganHang = "";
            if (!string.IsNullOrEmpty(tem["NganHangID"].ToString()))
            {
                if (!string.IsNullOrEmpty(tem["NganHangID"].ToString()))
                {
                    sql.CommandText = "Select * from tblDMNganHang where NganHangID =" +
                                      tem["NganHangID"].ToString();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                        strNganHang =
                            ds.Tables[0].Rows[0]["TenNganHang"].ToString();
                }
            }
            obj.TaiNganHang = strNganHang;
            obj.SoChungChiKTV = tem["SoChungChiKTV"].ToString();
            try
            {
                if (!string.IsNullOrEmpty(tem["NgayCapChungChiKTV"].ToString()))
                    obj.NgayCapChungChiKTV = Convert.ToDateTime(tem["NgayCapChungChiKTV"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }
            
            obj.SoGiayCNDKHNKiT = tem["SoGiayChungNhanDKHN"].ToString();
            try
            {
                if (!string.IsNullOrEmpty(tem["NgayCapGiayChungNhanDKHN"].ToString()))
                    obj.NgayCapGiayCNDKHNKiT = Convert.ToDateTime(tem["NgayCapGiayChungNhanDKHN"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            try
            {
                if (!string.IsNullOrEmpty(tem["HanCapTu"].ToString()))
                    obj.HanCapGiayCNDKHNKT_TuNgay = Convert.ToDateTime(tem["HanCapTu"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }
            try
            {
                if (!string.IsNullOrEmpty(tem["HanCapDen"].ToString()))
                    obj.HanCapGiayCNDKHNKT_DenNgay = Convert.ToDateTime(tem["HanCapDen"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }



            string strTenChungChiQuocTe = "";
            string strNgayCapChungChiQuocTe = "";
            if (string.IsNullOrEmpty(tem["HoiVienCaNhanID"].ToString()))
            {
                if (!string.IsNullOrEmpty(tem["HoiVienCaNhanID"].ToString()))
                {
                    sql.CommandText = "Select * from tblHoiVienCaNhanChungChiQuocTe where HoiVienCaNhanID =" +
                                      tem["HoiVienCaNhanID"].ToString();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int j = 0;
                        foreach (DataRow dtr in ds.Tables[0].Rows)
                        {
                            j++;
                            strTenChungChiQuocTe += j + ". " + dtr["SoChungChi"].ToString() + "\r\n";
                            try
                            {
                                strNgayCapChungChiQuocTe += j + ". " +
                                                            Convert.ToDateTime(dtr["NgayCap"]).ToString("dd/MM/yyyy") +
                                                            "\r\n";
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
            }
            obj.TenChungChiNuocNgoai = strTenChungChiQuocTe;
            obj.NgayCapChungChiNuocNgoai = strNgayCapChungChiQuocTe;

            string strChucVu = "";
            if (!string.IsNullOrEmpty(tem["ChucVuID"].ToString()))
            {
                if (!string.IsNullOrEmpty(tem["ChucVuID"].ToString()))
                {
                    sql.CommandText = "Select * from tblDMChucVu where ChucVuID =" +
                                      tem["ChucVuID"].ToString();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                        strChucVu =
                            ds.Tables[0].Rows[0]["TenChucVu"].ToString();
                }
            }
            obj.ChucVuHienNay = strChucVu;
            obj.Email = tem["Email"].ToString();
            string Mobile_CoDinh = "";
            if (!string.IsNullOrEmpty(tem["Mobile"].ToString()))
                Mobile_CoDinh = tem["Mobile"].ToString();
            if (!string.IsNullOrEmpty(tem["DienThoai"].ToString()))
                if (!string.IsNullOrEmpty(tem["DienThoai"].ToString()))
                    Mobile_CoDinh += "/" + tem["DienThoai"].ToString();
                else
                    Mobile_CoDinh = tem["DienThoai"].ToString();
            obj.Mobile_DienThoaiCoDinh = Mobile_CoDinh;


            
            string strSoThich = "";
            if (!string.IsNullOrEmpty(tem["SoThichID"].ToString()))
            {
                if (!string.IsNullOrEmpty(tem["SoThichID"].ToString()))
                {
                    sql.CommandText = "Select * from tblDMSoThich where SoThichID =" +
                                      tem["SoThichID"].ToString();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (ds.Tables[0].Rows.Count > 0)
                        strSoThich =
                            ds.Tables[0].Rows[0]["TenSoThich"].ToString();
                }
            }
            obj.SoThich = strSoThich;

            //obj.KQKSCL_TongBaoCaoKiemToanDaKyTrongNam = "";
            //obj.KQKSCL_SoLuongBaoCaoKiemToanChoDonViCoLoiIchCongChung = "";
            //obj.KSKSCL_SoLuongBaoCaoKiemToanDuocKiemTra = "";

            //Đợi Hằng PTH
            //string strTongSoBaoCao1 = "";

            //sql.CommandText = "Select * from tblKSCLDSBaoCaoTuKiemTraChiTiet where HoiVienCaNhanID = " +
            //                  tem["HoiVienCaNhanID"] + " AND datepart(yyyy,GETDATE()) = " + DateTime.Now.Year;
            //DataTable dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            //if (dtbtemp.Rows.Count > 0)
            //{
            //    strTongSoBaoCao1 = dtbtemp.Rows[0]["SoBaoCaoKiemToanPH"].ToString();
            //}

            //obj.KQKSCL_TongBaoCaoKiemToanDaKyTrongNam = strTongSoBaoCao1;

            //string strTongSoBaoCao2 = "";

            //sql.CommandText =
            //    "select count(HoSoID) as SoLuong from tblKSCLBaoCaoKTKT where HoSoID in( Select HoSoID from tblKSCLHoSo where HoiVienCaNhanID = " +
            //    tem["HoiVienCaNhanID"] + " and (DATEPART(YYYY, DenNgayThucTe) = " + DateTime.Now.Year + ")) AND DonViChungKhoan = 1";
            //dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            //if (dtbtemp.Rows.Count > 0)
            //{
            //    strTongSoBaoCao2 = dtbtemp.Rows[0]["SoLuong"].ToString();

            //}
            //obj.KQKSCL_SoLuongBaoCaoKiemToanChoDonViCoLoiIchCongChung = strTongSoBaoCao2;

            string strTongSoBaoCao3 = "";
            sql.CommandText =
                "select count(HoSoID) as SoLuong from tblKSCLBaoCaoKTKT where KTVTen = N'" +
                tem["HoDem"] + ' '+ tem["Ten"] + "' and (DATEPART(YYYY, NgayBaoCaoKT) = " + DateTime.Now.Year + ")";
            DataTable dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            if (dtbtemp.Rows.Count > 0)
            {
                strTongSoBaoCao3 = dtbtemp.Rows[0]["SoLuong"].ToString();

            }
            obj.KSKSCL_SoLuongBaoCaoKiemToanDuocKiemTra = strTongSoBaoCao3;

            string strKhenThuongNgayThang = "";
            string strHinhThucKhenThuong = "";
            string strLyDoKhenThuong = "";
            sql.CommandText = @"select A.*, B.TenHinhThucKhenThuong from tblKhenThuongKyLuat A
left join tblDMHinhThucKhenThuong B on A.HinhThucKhenThuongID = B.HinhThucKhenThuongID
 where Loai = 1 and HoiVienCaNhanID = " + tem["HoiVienCaNhanID"].ToString();

            dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            if (dtbtemp.Rows.Count > 0)
            {
                int j = 0;
                foreach (DataRow dtr in dtbtemp.Rows)
                {
                    j++;
                    try
                    {
                        strKhenThuongNgayThang += j + ". " + Convert.ToDateTime(dtbtemp.Rows[0]["NgayThang"]).ToString("dd/MM/yyyy") + "\r\n";
                    }
                    catch (Exception)
                    {
                    }
                    strHinhThucKhenThuong += j + ". " + dtbtemp.Rows[0]["TenHinhThucKhenThuong"].ToString() + "\r\n";
                    strLyDoKhenThuong += j + ". " + dtbtemp.Rows[0]["LyDo"].ToString() + "\r\n";
                }
            }

            obj.KhenThuong_NgayThang = strKhenThuongNgayThang;
            obj.KhenThuong_CapVaHinhThucKhenThuongKyLuat = strHinhThucKhenThuong;
            obj.KhenThuong_LyDo = strLyDoKhenThuong;

            string strKyLuatNgayThang = "";
            string strHinhThucKyLuat = "";
            string strLyDoKyLuat = "";
            sql.CommandText = @"select A.*, B.TenHinhThucKyLuat from tblKhenThuongKyLuat A
left join tblDMHinhThucKyLuat B on A.HinhThucKyLuatID = B.HinhThucKyLuatID
 where Loai = 2 and HoiVienCaNhanID = " + tem["HoiVienCaNhanID"].ToString();

            dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            if (dtbtemp.Rows.Count > 0)
            {
                int j = 0;
                foreach (DataRow dtr in dtbtemp.Rows)
                {
                    j++;
                    try
                    {
                        strKyLuatNgayThang += j + ". " + Convert.ToDateTime(dtbtemp.Rows[0]["NgayThang"]).ToString("dd/MM/yyyy") + "\r\n";
                    }
                    catch (Exception)
                    {
                    }
                    strHinhThucKyLuat += j + ". " + dtbtemp.Rows[0]["TenHinhThucKyLuat"].ToString() + "\r\n";
                    strLyDoKyLuat += j + ". " + dtbtemp.Rows[0]["LyDo"].ToString() + "\r\n";
                }
            }

            obj.KyLuat_NgayThang = strKyLuatNgayThang;
            obj.KyLuat_CapVaHinhThucKhenThuongKyLuat = strHinhThucKyLuat;
            obj.KyLuat_LyDo = strLyDoKyLuat;
            lst.Add(obj);
        }
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ReportDocument rpt = new ReportDocument();
        rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
        rpt.Load(Server.MapPath("Report/QuanLyThongTinChung/DanhSachKiemToanVienTrongCaNuoc_ChiTiet.rpt"));

        //rpt.SetDataSource(lst);
        rpt.Database.Tables["objDanhSachKiemToanVienTrongCaNuoc_ChiTiet"].SetDataSource(lst);
        //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

        CrystalReportViewer1.ReportSource = rpt;
        CrystalReportViewer1.DataBind();

        if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachKiemToanVienTrongCaNuoc_ChiTiet");
        if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
        {
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachKiemToanVienTrongCaNuoc_ChiTiet");
            //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //xlsFormatOptions.ShowGridLines = true;
            //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

        }
        if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
        //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true,
        //                         "DanhSachCongTyKiemToanTrongCaNuoc");
        {
            string strNgayTinh = "(Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            string strNgayBaoCao = DateTime.Now.ToString("dd/MM/yyyy");
            ExportToExcel("DanhSachKiemToanVienTrongCaNuoc_ChiTiet", lst, strNgayTinh, strNgayBaoCao);
        }
    }

    private void ExportToExcel(string strName, List<objDanhSachKiemToanVienTrongCaNuoc_ChiTiet> lst, string NgayTinh, string NgayBC)
    {
        string outHTML = "";

        outHTML += "<table border = '1' cellpadding='5' cellspacing='0' style='font-family:Arial; font-size:8pt; color:#003366'>";
        outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        outHTML += "<td colspan='2' style='border-style:none'></td>";
        //outHTML += "<td colspan='5' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/logo.png' alt='' border='0px'/></td>";
        outHTML += "<td colspan='10' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/ten.png' alt='' border='0px'/></td>";
        outHTML += "<td colspan='29' style='border-style:none'></td>";

        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH CÔNG TY KIỂM TOÁN TRONG NƯỚC</b></td>";
        outHTML += "<td colspan='29'></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i>" + NgayTinh + "</i></td>";
        outHTML += "<td colspan='29'></td>";
        outHTML += "</tr>";
        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "    <th rowspan = '2' width='30px' height ='150'>STT</th>";
        outHTML += "    <th rowspan = '2'>ID.HVCN, ID.KTV</th>";
        outHTML += "    <th rowspan = '2'>Họ và tên</th>";
        outHTML += "    <th colspan = '2'>Ngày sinh</th>";
        outHTML += "    <th rowspan = '2'>Số quyết định kết nạp HVCN</th>";
        outHTML += "    <th rowspan = '2'>Ngày ký Quyết định kết nạp HVCN</th>";
        outHTML += "    <th rowspan = '2'>Quê quán/quốc tịch</th>";
        outHTML += "    <th rowspan = '2'>Địa chỉ Nơi ở hiện tại</th>";
        outHTML += "    <th colspan = '7'>Trình độ chuyên môn</th>";
        outHTML += "    <th rowspan = '2'>Số CMTND/Hộ chiếu</th>";
        outHTML += "    <th rowspan = '2'>Ngày cấp CMTND/Hộ chiếu</th>";
        outHTML += "    <th rowspan = '2'>Số tài khoản Ngân hàng</th>";
        outHTML += "    <th rowspan = '2'>Tại Ngân hàng</th>";
        outHTML += "    <th rowspan = '2'>Số chứng chỉ KTV</th>";
        outHTML += "    <th rowspan = '2'>Ngày cấp chứng chỉ KTV</th>";
        outHTML += "    <th rowspan = '2'>Số giấy CN ĐKHN KiT</th>";
        outHTML += "    <th rowspan = '2'>Ngày cấp giấy CN ĐKHN KiT</th>";
        outHTML += "    <th colspan = '2'>Hạn cấp giấy CN ĐKHN KiT</th>";
        outHTML += "    <th rowspan = '2'>Tên chứng chỉ nước ngoài</th>";
        outHTML += "    <th rowspan = '2'>Ngày cấp chứng chỉ nước ngoài</th>";
        outHTML += "    <th rowspan = '2'>Chức vụ hiện nay</th>";
        outHTML += "    <th rowspan = '2'>Email</th>";
        outHTML += "    <th rowspan = '2'>Mobile/Điện thoại cố định</th>";
        outHTML += "    <th rowspan = '2'>Sở thích</th>";
        outHTML += "    <th colspan = '3'>Kết quả kiểm soát chất lượng</th>";
        outHTML += "    <th colspan = '3'>Khen thưởng</th>";
        outHTML += "    <th colspan = '3'>Kỷ luật</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th>Nam</th>";
        outHTML += "    <th>Nữ</th>";
        outHTML += "    <th>Tốt nghiệp đại học</th>";
        outHTML += "    <th>Chuyên ngành</th>";
        outHTML += "    <th>Năm tốt nghiệp</th>";
        outHTML += "    <th>Học vị (Cử nhân, Thạc sỹ, Tiến sỹ, PGS/GS)</th>";
        outHTML += "    <th>Năm</th>";
        outHTML += "    <th>Học hàm</th>";
        outHTML += "    <th>Năm</th>";
        outHTML += "    <th>Từ ngày</th>";
        outHTML += "    <th>Đến ngày</th>";
        outHTML += "    <th>Tổng số báo cáo kiểm toán đã ký trong năm</th>";
        outHTML += "    <th>Số lượng báo cáo kiểm toán cho đơn vị có lợi ích công chúng trong lĩnh vực chứng khoán</th>";
        outHTML += "    <th>Số lượng báo cáo kiểm toán được kiểm tra</th>";
        outHTML += "    <th>Ngày tháng</th>";
        outHTML += "    <th>Cấp và hình thức khen thưởng, kỷ luật</th>";
        outHTML += "    <th>Lý do</th>";
        outHTML += "    <th>Ngày tháng</th>";
        outHTML += "    <th>Cấp và hình thức khen thưởng, kỷ luật</th>";
        outHTML += "    <th>Lý do</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th width='30px'>1</th>";
        outHTML += "    <th width='300px'>2</th>";
        outHTML += "    <th width='80px'>3</th>";
        outHTML += "    <th width='80px'>4</th>";
        outHTML += "    <th width='150px'>5</th>";
        outHTML += "    <th width='200px'>6</th>";
        outHTML += "    <th width='200px'>7</th>";
        outHTML += "    <th width='200px'>8</th>";
        outHTML += "    <th width='200px'>9</th>";
        outHTML += "    <th width='200px'>10</th>";
        outHTML += "    <th width='200px'>11</th>";
        outHTML += "    <th width='200px'>12</th>";
        outHTML += "    <th width='200px'>13</th>";
        outHTML += "    <th width='200px'>14</th>";
        outHTML += "    <th width='200px'>15</th>";
        outHTML += "    <th width='200px'>16</th>";
        outHTML += "    <th width='200px'>17</th>";
        outHTML += "    <th width='200px'>18</th>";
        outHTML += "    <th width='200px'>19</th>";
        outHTML += "    <th width='200px'>20</th>";
        outHTML += "    <th width='200px'>21</th>";
        outHTML += "    <th width='200px'>22</th>";
        outHTML += "    <th width='200px'>23</th>";
        outHTML += "    <th width='200px'>24</th>";
        outHTML += "    <th width='200px'>25</th>";
        outHTML += "    <th width='200px'>26</th>";
        outHTML += "    <th width='200px'>27</th>";
        outHTML += "    <th width='200px'>28</th>";
        outHTML += "    <th width='200px'>29</th>";
        outHTML += "    <th width='200px'>30</th>";
        outHTML += "    <th width='200px'>31</th>";
        outHTML += "    <th width='200px'>32</th>";
        outHTML += "    <th width='200px'>33</th>";
        outHTML += "    <th width='200px'>34</th>";
        outHTML += "    <th width='200px'>35</th>";
        outHTML += "    <th width='200px'>36</th>";
        outHTML += "    <th width='200px'>37</th>";
        outHTML += "    <th width='200px'>38</th>";
        outHTML += "    <th width='200px'>39</th>";
        outHTML += "    <th width='200px'>40</th>";
        outHTML += "    <th width='200px'>41</th>";
        outHTML += "</tr>";

        var q = lst.Select(m => new { m.SoHieuCongTy, m.TenCongTy }).Distinct();

        int i = 0;
        foreach (var temp in q)
        {
            outHTML += "<tr>";
            outHTML += "<td colspan='41' align='left'>" + temp.SoHieuCongTy.Replace("<", "_").Replace(">", "") + " " + temp.TenCongTy.Replace("<", "_").Replace(">", "") + "</td>";
            outHTML += "</tr>";

            var qChiTiet = lst.Where(t => t.SoHieuCongTy == temp.SoHieuCongTy);
            foreach (var tChiTiet in qChiTiet)
            {
                i++;
                outHTML += "<tr>";
                outHTML += "<td align='center'>" + i.ToString() + "</td>";
                outHTML += "<td>" + tChiTiet.IDHVCN_IDKTV + "</td>";
                outHTML += "<td>" + tChiTiet.HoVaTen + "</td>";
                outHTML += "<td>" + tChiTiet.NgaySinhNam + "</td>";
                outHTML += "<td>" + tChiTiet.NgaySinhNu + "</td>";
                outHTML += "<td>" + tChiTiet.SoQuyetDinhKetNapHVCN + "</td>";
                outHTML += "<td>" + tChiTiet.NgayKyQuyetDinhKetNapHVCN + "</td>";
                outHTML += "<td>" + tChiTiet.QueQuanQuocTich + "</td>";
                outHTML += "<td>" + tChiTiet.DiaChiNoiOHienTai + "</td>";
                outHTML += "<td>" + tChiTiet.TDCM_TotNghiepDaiHoc + "</td>";
                outHTML += "<td>" + tChiTiet.TDCM_ChuyenNganh + "</td>";
                outHTML += "<td>" + tChiTiet.TDCM_NamTotNghiep + "</td>";
                outHTML += "<td>" + tChiTiet.TDCM_HocVi + "</td>";
                outHTML += "<td>" + tChiTiet.TDCM_NamHocVi + "</td>";
                outHTML += "<td>" + tChiTiet.TDCM_HocHam + "</td>";
                outHTML += "<td>" + tChiTiet.TDCM_NamHocHam + "</td>";
                outHTML += "<td>" + tChiTiet.SoCMND_HoChieu + "</td>";
                outHTML += "<td>" + tChiTiet.NgayCapCMND_HoChieu + "</td>";
                outHTML += "<td>" + tChiTiet.SoTaiKhoanNganHang + "</td>";
                outHTML += "<td>" + tChiTiet.TaiNganHang + "</td>";
                outHTML += "<td>" + tChiTiet.SoChungChiKTV + "</td>";
                outHTML += "<td>" + tChiTiet.NgayCapChungChiKTV + "</td>";
                outHTML += "<td>" + tChiTiet.SoGiayCNDKHNKiT + "</td>";
                outHTML += "<td>" + tChiTiet.NgayCapGiayCNDKHNKiT + "</td>";
                outHTML += "<td>" + tChiTiet.HanCapGiayCNDKHNKT_TuNgay + "</td>";
                outHTML += "<td>" + tChiTiet.HanCapGiayCNDKHNKT_DenNgay + "</td>";
                outHTML += "<td>" + tChiTiet.TenChungChiNuocNgoai + "</td>";
                outHTML += "<td>" + tChiTiet.NgayCapChungChiNuocNgoai + "</td>";
                outHTML += "<td>" + tChiTiet.ChucVuHienNay + "</td>";
                outHTML += "<td>" + tChiTiet.Email + "</td>";
                outHTML += "<td>" + tChiTiet.Mobile_DienThoaiCoDinh + "</td>";
                outHTML += "<td>" + tChiTiet.SoThich + "</td>";
                outHTML += "<td>" + tChiTiet.KQKSCL_TongBaoCaoKiemToanDaKyTrongNam + "</td>";
                outHTML += "<td>" + tChiTiet.KQKSCL_SoLuongBaoCaoKiemToanChoDonViCoLoiIchCongChung + "</td>";
                outHTML += "<td>" + tChiTiet.KSKSCL_SoLuongBaoCaoKiemToanDuocKiemTra + "</td>";
                outHTML += "<td>" + tChiTiet.KhenThuong_NgayThang + "</td>";
                outHTML += "<td>" + tChiTiet.KhenThuong_CapVaHinhThucKhenThuongKyLuat + "</td>";
                outHTML += "<td>" + tChiTiet.KhenThuong_LyDo + "</td>";
                outHTML += "<td>" + tChiTiet.KyLuat_NgayThang + "</td>";
                outHTML += "<td>" + tChiTiet.KyLuat_CapVaHinhThucKhenThuongKyLuat + "</td>";
                outHTML += "<td>" + tChiTiet.KyLuat_LyDo + "</td>";
                outHTML += "</tr>";
            }
        }

        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none'></td>";
        outHTML += "<td colspan='29' align='center' style='border-style:none'><b>" + NgayBC + "</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='12' style='border-style:none'></td>";
        outHTML += "<td colspan='29' align='center' style='border-style:none'><b>NGƯỜI LẬP BIỂU<b></td>";
        outHTML += "</tr>";

        outHTML += "</table>";

        Library.ExportToExcel(strName, outHTML);
    }

    protected string strBoChamPhay(string p)
    {
        int leng = p.Length;
        if (p.LastIndexOf(',') != -1)
            leng = p.LastIndexOf(',');
        string strOut = p.Substring(0, leng);
        strOut = strOut.Replace(".", string.Empty);
        return strOut;
    }

    protected void LoadCheckBox()
    {
        if (this.IsPostBack)
        {
            if (Request.Form["danghanhnghe"] != null)
            {
                Response.Write("$('#danghanhnghe').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#danghanhnghe').attr('checked', false); ");
            }
            if (Request.Form["khonghanhnghe"] != null)
            {
                Response.Write("$('#khonghanhnghe').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#khonghanhnghe').attr('checked', false); ");
            }
        }
    }
}