﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Globalization;

public partial class usercontrols_thanhtoanphi_quanly : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    DataTable dtb = new DataTable();

    // Tên chức năng
    public string tenchucnang = "Quản lý giao dịch";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";

    public string quyen = "QuanLyGiaoDich";

    public string truongma = "MaGiaoDich";
    public string truongloai = "DoiTuongNopPhi";
    public string truongid = "GiaoDichID"; 
    public string tinhtrang = "TenTrangThai";
    public string tinhtrangid = "TinhTrangID";

    //public string NgayApDungTu 


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Commons cm = new Commons();
                if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

                danhsachphi_grv.Columns[1].HeaderText = "Mã giao dịch";
                danhsachphi_grv.Columns[1].SortExpression = "MAGIAODICH";
                danhsachphi_grv.Columns[2].HeaderText = "Diễn giải";
                danhsachphi_grv.Columns[2].SortExpression = "DIENGIAI";
                danhsachphi_grv.Columns[3].HeaderText = "Ngày nộp";
                danhsachphi_grv.Columns[3].SortExpression = "NGAYNOP";
                danhsachphi_grv.Columns[4].HeaderText = "Đối tượng nộp";
                danhsachphi_grv.Columns[4].SortExpression = "DOITUONGNOPPHI";
                danhsachphi_grv.Columns[5].HeaderText = "Tình trạng";
                danhsachphi_grv.Columns[5].SortExpression = "TENTINHTRANG";
                danhsachphi_grv.Columns[6].HeaderText = "Thao tác";

                Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

                Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>Danh mục " + tenchucnang + @"</h1> </div>"));


                //// Phân quyền
                //if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
                //{
                //    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
                //    Form1.Visible = false;
                //    return;
                //}

                /////////////

                if (Request.QueryString["act"] == "delete")
                {
                    string[] dsxoa = Request.QueryString["id"].Split(',');

                    foreach (string id in dsxoa)
                    {
                        string[] idloai = id.Split('|');
                        if (idloai.Length > 1)
                        {
                            //Thanh toán phí cá nhân
                            if (idloai[1] == "0")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText =
                                    "SELECT TinhTrangID, MaGiaoDich from tblTTNopPhiCaNhan where GiaoDichID = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải khác ở trạng thái Đã duyệt  mới cho xóa
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int) EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "DELETE tblTTNopPhiCaNhan WHERE GiaoDichID = " + idloai[0];
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("QuanLyGiaoDich",
                                              "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] +
                                              "\" của danh mục QuanLyGiaoDich");
                                }
                            }
                                //Thanh toán phí tổ chức
                            else if (idloai[1] == "1")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText =
                                    "SELECT TinhTrangID, MaGiaoDich from tblTTNopPhiTapThe where GiaoDichID = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải khác ở trạng thái Đã duyệt  mới cho xóa
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "DELETE tblTTNopPhiTapThe WHERE GiaoDichID = " + idloai[0];
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("QuanLyGiaoDich",
                                              "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] +
                                              "\" của danh mục QuanLyGiaoDich");
                                }
                            }
                        }
                    }
                    Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                }
                else if (Request.QueryString["act"] == "duyet")
                {
                    string[] dsduyet = Request.QueryString["id"].Split(',');

                    foreach (string id in dsduyet)
                    {
                        string[] idloai = id.Split('|');
                        if (idloai.Length > 1)
                        {
                            //Thanh toán cá nhân
                            if (idloai[1] == "0")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangID, MaGiaoDich from tblTTNopPhiCaNhan where GiaoDichID = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTNopPhiCaNhan set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE GiaoDichID = @GiaoDichID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                    sql.Parameters.AddWithValue("@GiaoDichID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("QuanLyGiaoDich", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục QuanLyGiaoDich");
                                }
                            }
                                //Thanh toán tổ chức
                            else if (idloai[1] == "1")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangID, MaGiaoDich from tblTTNopPhiTapThe where GiaoDichID = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTNopPhiTapThe set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE GiaoDichId = @GiaoDichId";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                    sql.Parameters.AddWithValue("@GiaoDichID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("QuanLyGiaoDich", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục QuanLyGiaoDich");
                                }
                            }
                        }
                    }
                    Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                }
                else if (Request.QueryString["act"] == "tuchoi")
                {
                    string[] dsduyet = Request.QueryString["id"].Split(',');
                    foreach (string id in dsduyet)
                    {
                        string[] idloai = id.Split('|');
                        if (idloai.Length > 1)
                        {
                            //Thanh toán cá nhân
                            if (idloai[1] == "0")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangID, MaGiaoDich from tblTTNopPhiCaNhan where GiaoDichID = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTNopPhiCaNhan set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE GiaoDichID = @GiaoDichID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@GiaoDichID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("QuanLyGiaoDich", "Từ chối giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục QuanLyGiaoDich");
                                }
                            }
                            //Thanh toán tổ chức
                            else if (idloai[1] == "1")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangID, MaGiaoDich from tblTTNopPhiTapThe where GiaoDichID = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTNopPhiTapThe set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE GiaoDichId = @GiaoDichId";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@GiaoDichID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("QuanLyGiaoDich", "Từ chối giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục QuanLyGiaoDich");
                                }
                            }
                        }
                    }
                    Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                }
                else if (Request.QueryString["act"] == "thoaiduyet")
                {
                    string[] dsduyet = Request.QueryString["id"].Split(',');
                    foreach (string id in dsduyet)
                    {
                        string[] idloai = id.Split('|');
                        if (idloai.Length > 1)
                        {
                            //Thanh toán cá nhân
                            if (idloai[1] == "0")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangID, MaGiaoDich from tblTTNopPhiCaNhan where GiaoDichID = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái đã duyệt thì mới cho thoái duyệt
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTNopPhiCaNhan set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE GiaoDichID = @GiaoDichID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@GiaoDichID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("QuanLyGiaoDich", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục QuanLyGiaoDich");
                                }
                            }
                            //Thanh toán tổ chức
                            else if (idloai[1] == "1")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangID, MaGiaoDich from tblTTNopPhiTapThe where GiaoDichID = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTNopPhiTapThe set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE GiaoDichId = @GiaoDichId";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@GiaoDichID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("QuanLyGiaoDich", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục QuanLyGiaoDich");
                                }
                            }
                        }
                    }
                    Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                }
                load_data();
            }
        }

        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void load_data()
    {
        try
        {

            int i;
            Commons cm = new Commons();

            SqlCommand sql = new SqlCommand();
            
            sql.CommandText = @"SELECT distinct GiaoDichID, MaGiaoDich, DienGiai, NgayNop, 0 as DoiTuongNopPhi, N'Cá nhân' as TENDOITUONGNOPPHI, TinhTrangID, TenTrangThai, NgayNhap  FROM tblTTNopPhiCaNhan A left join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                sql.CommandText += " AND A.MaGiaoDich like N'%" + Request.Form["timkiem_Ma"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayNopTu"]))
                sql.CommandText += " AND A.NgayNop >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayNopTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayNopDen"]))
                sql.CommandText += " AND A.NgayNop <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayNopDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if ((!string.IsNullOrEmpty(Request.Form["timkiem_canhan"]) && !string.IsNullOrEmpty(Request.Form["timkiem_tapthe"]))
                || (string.IsNullOrEmpty(Request.Form["timkiem_canhan"]) && string.IsNullOrEmpty(Request.Form["timkiem_tapthe"])))
            {
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.Form["timkiem_tapthe"]))
                    sql.CommandText += " AND 0 = 1";
  
            }
            if ((!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) && !string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) && !string.IsNullOrEmpty(Request.Form["timkiem_duyet"]) && !string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
               || (string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) && string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) && string.IsNullOrEmpty(Request.Form["timkiem_duyet"]) && string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"])))
            {
            }
            else
            {
              if (string.IsNullOrEmpty((Request.Form["timkiem_choduyet"])))
              {
                  sql.CommandText += " AND A.TinhTrangID <> " + (int) EnumVACPA.TinhTrang.ChoDuyet;
              }
              if (string.IsNullOrEmpty((Request.Form["timkiem_tuchoi"])))
              {
                  sql.CommandText += " AND A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.KhongPheDuyet;
              }
              if (string.IsNullOrEmpty((Request.Form["timkiem_duyet"])))
              {
                  sql.CommandText += " AND A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
              }
              if (string.IsNullOrEmpty((Request.Form["timkiem_thoaiduyet"])))
              {
                  sql.CommandText += " AND A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.ThoaiDuyet;
              }
            }
           // sql.CommandText += ")";
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            dtb = ds.Tables[0];

            sql.CommandText = @"SELECT distinct GiaoDichID, MaGiaoDich, '' as DienGiai, NgayNhap as NgayNop, 1 as DoiTuongNopPhi, N'Công ty' as TENDOITUONGNOPPHI, TinhTrangID, TenTrangThai, NgayNhap  FROM tblTTNopPhiTapThe A left join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                sql.CommandText += " AND A.MaGiaoDich like N'%" + Request.Form["timkiem_Ma"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayNopTu"]))
                sql.CommandText += " AND A.NgayNhap >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayNopTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayNopDen"]))
                sql.CommandText += " AND A.NgayNhap <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayNopDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if ((!string.IsNullOrEmpty(Request.Form["timkiem_canhan"]) && !string.IsNullOrEmpty(Request.Form["timkiem_tapthe"]))
                || (string.IsNullOrEmpty(Request.Form["timkiem_canhan"]) && string.IsNullOrEmpty(Request.Form["timkiem_tapthe"])))
            {
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.Form["timkiem_canhan"]))
                    sql.CommandText += " AND 0 = 1";
            }
            if ((!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) && !string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) && !string.IsNullOrEmpty(Request.Form["timkiem_duyet"]) && !string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
               || (string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) && string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) && string.IsNullOrEmpty(Request.Form["timkiem_duyet"]) && string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"])))
            {
            }
            else
            {
                if (string.IsNullOrEmpty((Request.Form["timkiem_choduyet"])))
                {
                    sql.CommandText += " AND A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.ChoDuyet;
                }
                if (string.IsNullOrEmpty((Request.Form["timkiem_tuchoi"])))
                {
                    sql.CommandText += " AND A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.KhongPheDuyet;
                }
                if (string.IsNullOrEmpty((Request.Form["timkiem_duyet"])))
                {
                    sql.CommandText += " AND A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                }
                if (string.IsNullOrEmpty((Request.Form["timkiem_thoaiduyet"])))
                {
                    sql.CommandText += " AND A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.ThoaiDuyet;
                }
            }
            ds = DataAccess.RunCMDGetDataSet(sql);
            dtb.Merge(ds.Tables[0]);

            //DataTable dtbtemp = new DataTable();
            //dtbtemp.Columns.Add("PhiID", typeof(int));
            //dtbtemp.Columns.Add("MaPhi", typeof(string));
            //dtbtemp.Columns.Add("TenPhi", typeof(string));
            //dtbtemp.Columns.Add("MucPhi", typeof(decimal));
            //dtbtemp.Columns.Add("NgayApDung", typeof(DateTime));
            //dtbtemp.Columns.Add("NgayHetHieuLuc", typeof(DateTime));
            //dtbtemp.Columns.Add("TinhTrangID", typeof(int));
            //dtbtemp.Columns.Add("NgayLap", typeof(DateTime));
            //dtbtemp.Columns.Add("DoiTuongNopPhi", typeof(string));
            //dtbtemp = ds.Tables[0];

            //dtb.Merge(dtbtemp);

            if (dtb.Rows.Count == 0)
            {
                Pager.Enabled = false;
                return;
            }
            else
                Pager.Enabled = true;
            DataView dv = dtb.DefaultView;

            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
            else
                dv.Sort = "NgayNop DESC";
            //dtb = dv.ToTable();
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                if (!Pager.Items.Contains(a))
                    Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                if (Convert.ToInt32(Request.Form["tranghientai"]) <= objPds.PageCount)
                {
                    objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                    Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                }
                else
                {
                    objPds.CurrentPageIndex = 0;
                    Pager.SelectedIndex = 0;
                }
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = quyen + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }

            danhsachphi_grv.DataSource = objPds;

            danhsachphi_grv.DataBind();
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dtb = null;


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void danhsachphi_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        danhsachphi_grv.PageIndex = e.NewPageIndex;
        danhsachphi_grv.DataBind();
    }

    protected void danhsachphi_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        load_data();
    }

    protected void KiemTraDuyet()
    {
        bool boThongBao = false;
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            string[] dsduyet = Request.QueryString["id"].Split(',');

            foreach (string id in dsduyet)
            {
                string[] idloai = id.Split('|');
                if (idloai.Length > 1)
                {
                    if (idloai[1] == "1")
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + idloai[0];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                        }
                        else
                        {
                            boThongBao = true;
                        }
                    }
                    else if (idloai[1] == "2")
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiDangLogo where PhiId = " + idloai[0];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                        }
                        else
                        {
                            boThongBao = true;
                        }
                    }
                    else if (idloai[1] == "3")
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + idloai[0];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                        }
                        else
                        {
                            boThongBao = true;
                        }
                    }
                }
            }
        }
        string output_html = "";
        if (boThongBao)
            output_html = "true";
        else
            output_html = "false";

        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
        //             if (boThongBao)
        //             {
        //                 output_html += @" $('#div_danhsachphi_khongphuhop').dialog({
        //                resizable: false,
        //                modal: true,
        //                buttons: {
        //                    'Đóng lại': function () {
        //                        $(this).dialog('close');
        //                    }
        //                }
        //            });";
        //             }
        //             else
        //             {
        //                 output_html += "confirm_duyet_danhsachphi(" + Request.Form["id"] + ")";
        //             }
        //             System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);

    }

    //confirm_duyet_danhsachphi(danhsachphi_grv_selected);"

    protected void LoadCheckBox()
    {
        if (Request.Form["timkiem_canhan"] != null)
        {
            Response.Write("$('#timkiem_canhan').attr('checked','checked'); ");
        }
        else
        {
            Response.Write("$('#timkiem_canhan').attr('checked', false); ");
        }
        if (Request.Form["timkiem_tapthe"] != null)
        {
            Response.Write("$('#timkiem_tapthe').attr('checked','checked'); ");
        }
        else
        {
            Response.Write("$('#timkiem_tapthe').attr('checked', false); ");
        }
        if (Request.Form["timkiem_choduyet"] != null)
        {
            Response.Write("$('#timkiem_choduyet').attr('checked','checked'); ");
        }
        else
        {
            Response.Write("$('#timkiem_choduyet').attr('checked', false); ");
        }
        //if (Request.Form["doituong_khongdudieukienkit"] != null)
        //{
        //    Response.Write("$('#doituong_khongdudieukienkit').attr('checked','checked'); ");
        //}
        //else
        //{
        //    Response.Write("$('#doituong_khongdudieukienkit').attr('checked', false); ");
        //}
        //if (Request.Form["doituong_dudieukienkitcongchungkhac"] != null)
        //{
        //    Response.Write("$('#doituong_dudieukienkitcongchungkhac').attr('checked','checked'); ");
        //}
        //else
        //{
        //    Response.Write("$('#doituong_dudieukienkitcongchungkhac').attr('checked', false); ");
        //}
        //if (Request.Form["thanhvienhangkiemtoanquocte"] != null)
        //{
        //    Response.Write("$('#thanhvienhangkiemtoanquocte').attr('checked','checked'); ");
        //}
        //else
        //{
        //    Response.Write("$('#thanhvienhangkiemtoanquocte').attr('checked', false); ");
        //}
    }
}