﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;

public partial class usercontrols_quanlyphikiemsoatchatluong : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();

    public string quyen = "DMPhiKSCL";

    int Id = 0;
   // int ttBanGhi = 0;
    // Tên chức năng
    public string tenchucnang = "Phí kiểm soát chất lượng";
    public string IconClass = "iconfa-star-empty";
    protected void Page_Load(object sender, EventArgs e)
    {
        //List<int> lst = new List<int>();
        //lst = Session["lstIdChon"] as List<int>;
        // đang chờ xử lý phần Thêm HVCN 
        //DataTable dtb = Session["dtb"] as DataTable;
        //if (dtb != null && dtb.Rows.Count != 0)
        //{ }
        //else
        {

            try
            {
                // ScriptManagerPhiCaNhan.RegisterAsyncPostBackControl(grvphikiemsoatchatluong);


                
                if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

                Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

         <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
         "));

                Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
                
            }
            catch (Exception ex)
            {
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
            }

            //if (Request.QueryString["idThem"] != "ok")
            //    Session["lstIdChon"] = null;

            //if (Request.QueryString["choncongty"] != "ok")
            //    Session["CongTyChon"] = null;


           if (string.IsNullOrEmpty(Request.QueryString["id"]))
            {
               //string abc = Request.QueryString["idThem"];
                if (!IsPostBack)
                {
                    //Session["CongTyChon"] = null;
                    //idPhiCty.Text = "";
                    //idPhiHVCN.Text = "";
                    Session["lstIdChon"] = null;
                }
                //else
                //    LoadDSNhanSuCTy(0);
            }
            else
            {
                if (!IsPostBack)
                {
                    lstLuuCty = new List<objHoiVienTapThe>();
                    lstLuuNhanSu = new List<objNhanSurpt>();
                    Session["PhiCaNhan"] = null;
                    Session["PhiCongTy"] = null;

                    if (!string.IsNullOrEmpty(Request.QueryString["tinhtrang"]))//Các hành động từ chối, duyệt, thoái duyệt, xóa
                    {
                        //Từ chối
                        if (Request.QueryString["tinhtrang"] == "tuchoi")
                        {
                            SqlCommand sql = new SqlCommand();
                            sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKSCL where PhiId = " + Request.QueryString["id"];
                            DataSet ds = new DataSet();
                            ds = DataAccess.RunCMDGetDataSet(sql);
                            //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                            if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                            {
                                sql.CommandText = "UPDATE tblTTDMPhiKSCL set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                                sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                                sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                                DataAccess.RunActionCmd(sql);
                                sql.Connection.Close();
                                sql.Connection.Dispose();
                                sql = null;
                                cm.ghilog("DMPhiKSCL", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục DMPhiKSCL");
                                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                            }
                            else
                            {
                                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                            }
                        }
                        else if (Request.QueryString["tinhtrang"] == "xoa") //Xóa
                        {
                            SqlCommand sql = new SqlCommand();
                            sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKSCL where PhiId = " + Request.QueryString["id"];
                            DataSet ds = new DataSet();
                            ds = DataAccess.RunCMDGetDataSet(sql);
                            //Kiểm tra, phải đang ở trạng thái chờ duyệt, thoái duyệt, từ chối thì mới cho xóa
                            if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet || Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.KhongPheDuyet || Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                            {
                                sql.CommandText = "DELETE tblTTDMPhiKSCLChiTiet WHERE PhiId = " + Request.QueryString["id"];
                                DataAccess.RunActionCmd(sql);
                                sql.CommandText = "DELETE tblTTDMPhiKSCL WHERE PhiId = " + Request.QueryString["id"];
                                DataAccess.RunActionCmd(sql);
                                sql.Connection.Close();
                                sql.Connection.Dispose();
                                sql = null;
                                cm.ghilog("DMPhiKSCL", "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục DMPhiKSCL");
                                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                            }
                            else
                            {
                                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                            }
                        }
                        else if (Request.QueryString["tinhtrang"] == "duyet") //duyệt
                        {

                            SqlCommand sql1 = new SqlCommand();
                            sql1.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKSCL where PhiId = " + Request.QueryString["id"];
                            DataSet ds = new DataSet();
                            ds = DataAccess.RunCMDGetDataSet(sql1);

                            string connStr = DataAccess.ConnString;
                            SqlConnection conPhiHoiVien = new SqlConnection(connStr);
                            SqlTransaction transPhiHoiVien;
                            conPhiHoiVien.Open();
                            transPhiHoiVien = conPhiHoiVien.BeginTransaction();

                            //Phải ở trạng thái chờ duyệt thì mới cho duyệt
                            if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                            {
                                try
                                {

                                    SqlCommand sql = new SqlCommand();
                                    sql.CommandText =
                                        "UPDATE tblTTDMPhiKSCL set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet = @NgayDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                    sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                                    sql.Connection = conPhiHoiVien;
                                    sql.Transaction = transPhiHoiVien;
                                    sql.ExecuteNonQuery();

                                  

                                    DataTable dtbHoiVienCaNhanChuaPhatSinhPhi = new DataTable();
                                    DataTable dtbHoiVienCaNhanDaPhatSinhPhi = new DataTable();
                                    DataTable dtbHoiVienTapTheChuaPhatSinhPhi = new DataTable();
                                    DataTable dtbHoiVienTapTheDaPhatSinhPhi = new DataTable();
                                    //if (ds.Tables[0].Rows[0][2].ToString() ==
                                    //    ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString()
                                    //    ||
                                    //    ds.Tables[0].Rows[0][2].ToString() ==
                                    //    ((int)EnumVACPA.LoaiHoiVien.KiemToanVien).ToString())
                                    //{
                                        sql1.CommandText =
                                            @"select A.HoiVienCaNhanId as HoiVienID, B.LoaiHoiVienCaNhan, A.MucPhi from tblTTDMPhiKSCLChiTiet A
                                             left join tblHoiVienCaNhan B on A.HoiVienCaNhanID = B.HoiVienCaNhanID
                                             where A.HoiVienCaNhanID NOT IN" +
                                            " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                            (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong +
                                            "and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                            " or (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + ")" +
                                            " or (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "))" +
                                            " and PhiID = " + Request.QueryString["id"] + ") AND A.PhiID =" + Request.QueryString["id"] +
                                            " and A.HoiVienCaNhanID IS NOT NULL";
                                        dtbHoiVienCaNhanChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                        sql1.CommandText =
                                            @"select A.HoiVienCaNhanId as HoiVienID, B.LoaiHoiVienCaNhan, A.MucPhi from tblTTDMPhiKSCLChiTiet A
                                             left join tblHoiVienCaNhan B on A.HoiVienCaNhanID = B.HoiVienCaNhanID
                                            where A.HoiVienCaNhanID IN" +
                                            " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                            (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong +
                                            "and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                             " or (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + ")" +
                                            " or (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "))" +
                                            " and PhiID = " + Request.QueryString["id"] + ") AND A.PhiID =" + Request.QueryString["id"] +
                                            " and A.HoiVienCaNhanID IS NOT NULL";
                                        dtbHoiVienCaNhanDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                    ////}
                                    ////else if (ds.Tables[0].Rows[0][2].ToString() ==
                                    ////         ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString()
                                    ////         ||
                                    ////         ds.Tables[0].Rows[0][2].ToString() ==
                                    ////         ((int)EnumVACPA.LoaiHoiVien.CongTyKiemToan).ToString())
                                    ////{
                                        sql1.CommandText =
                                        @"select A.HoiVienTapTheId as HoiVienID, B.LoaiHoiVienTapThe, A.MucPhi from tblTTDMPhiKSCLChiTiet A
                                             left join tblHoiVienTapThe B on A.HoiVienTapTheID = B.HoiVienTapTheID
                                             where A.HoiVienTapTheId NOT IN" +
                                        " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                        (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong +
                                        "and (LoaiHoiVien = 3" +
                                            " or (LoaiHoiVien = 4))" +
                                        " and PhiID = " + Request.QueryString["id"] + ") AND A.PhiID =" + Request.QueryString["id"] +
                                        "and A.HoiVienTapTheID IS NOT NULL";
                                        dtbHoiVienTapTheChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                        sql1.CommandText =
                                            @"select A.HoiVienTapTheId as HoiVienID, B.LoaiHoiVienTapThe, A.MucPhi from tblTTDMPhiKSCLChiTiet A
                                             left join tblHoiVienTapThe B on A.HoiVienTapTheID = B.HoiVienTapTheID
                                            where A.HoiVienTapTheId IN" +
                                            " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                            (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong +
                                            "and (LoaiHoiVien = 3" +
                                            " or (LoaiHoiVien = 4))" +
                                            " and PhiID = " + Request.QueryString["id"] + ") AND A.PhiID =" + Request.QueryString["id"] +
                                        "and A.HoiVienTapTheID IS NOT NULL";
                                        dtbHoiVienTapTheDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                    //}

                                    //Tạo bảng Phát sinh Phí
                                    DataTable dtbPhatSinhPhi = new DataTable("tblTTPhatSinhPhi");
                                    dtbPhatSinhPhi.Columns.Add("HoiVienID", typeof(int));
                                    dtbPhatSinhPhi.Columns.Add("LoaiHoiVien", typeof(string));
                                    dtbPhatSinhPhi.Columns.Add("PhiID", typeof(int));
                                    dtbPhatSinhPhi.Columns.Add("DoiTuongPhatSinhPhiID", typeof(int));
                                    dtbPhatSinhPhi.Columns.Add("LoaiPhi", typeof(string));
                                    dtbPhatSinhPhi.Columns.Add("NgayPhatSinh", typeof(DateTime));
                                    dtbPhatSinhPhi.Columns.Add("SoTien", typeof(double));

                                    //Đưa dữ liệu Hội viên cá nhân chưa phát sinh phí vào bảng Phát sinh Phí
                                    foreach (DataRow dtr in dtbHoiVienCaNhanChuaPhatSinhPhi.Rows)
                                    {
                                        DataRow dtrPhatSinhPhi = dtbPhatSinhPhi.NewRow();
                                        dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                        dtrPhatSinhPhi["LoaiHoiVien"] = dtr[1].ToString();
                                        dtrPhatSinhPhi["PhiID"] = Request.QueryString["id"].ToString();
                                        dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                        dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong;
                                        dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                        dtrPhatSinhPhi["SoTien"] = dtr[2].ToString();
                                        dtbPhatSinhPhi.Rows.Add(dtrPhatSinhPhi);
                                    }

                                    //Đưa dữ liệu Hội viên tổ chức chưa phát sinh phí vào bảng Phát sinh Phí
                                    foreach (DataRow dtr in dtbHoiVienTapTheChuaPhatSinhPhi.Rows)
                                    {
                                        DataRow dtrPhatSinhPhi = dtbPhatSinhPhi.NewRow();
                                        dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                        if (dtr[1].ToString() == "0" || dtr[1].ToString() == "2")
                                            dtrPhatSinhPhi["LoaiHoiVien"] = (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan;
                                        else if (dtr[1].ToString() == "1")
                                            dtrPhatSinhPhi["LoaiHoiVien"] = (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe;
                                        else
                                            dtrPhatSinhPhi["LoaiHoiVien"] = DBNull.Value;
                                        dtrPhatSinhPhi["PhiID"] = Request.QueryString["id"].ToString();
                                        dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                        dtrPhatSinhPhi["LoaiPhi"] = (int) EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong;
                                        dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                        dtrPhatSinhPhi["SoTien"] = dtr[2].ToString();
                                        dtbPhatSinhPhi.Rows.Add(dtrPhatSinhPhi);
                                    }

                                    //Copy vào Db
                                    //BulkCopy the data in the DataTable to the temp table
                                    if (dtbPhatSinhPhi.Rows.Count > 0)
                                    {
                                        using (
                                            SqlBulkCopy s = new SqlBulkCopy(conPhiHoiVien, SqlBulkCopyOptions.Default,
                                                                            transPhiHoiVien)
                                            )
                                            //using (SqlBulkCopy s = new SqlBulkCopy(DataAccess.ConnString, SqlBulkCopyOptions.Default))
                                        {
                                            s.BulkCopyTimeout = 99999;
                                            s.DestinationTableName = dtbPhatSinhPhi.TableName;

                                            foreach (var column in dtbPhatSinhPhi.Columns)
                                            {
                                                s.ColumnMappings.Add(column.ToString(), column.ToString());
                                            }
                                            s.WriteToServer(dtbPhatSinhPhi);
                                        }
                                    }
                                    //Tạo DataTable Phát sinh phí Update
                                    DataTable dtbPhatSinhPhiUpdate = new DataTable("tblTTPhatSinhPhi");
                                    dtbPhatSinhPhiUpdate.Columns.Add("PhatSinhPhiID", typeof(int));
                                    dtbPhatSinhPhiUpdate.Columns.Add("HoiVienID", typeof(int));
                                    dtbPhatSinhPhiUpdate.Columns.Add("LoaiHoiVien", typeof(string));
                                    dtbPhatSinhPhiUpdate.Columns.Add("PhiID", typeof(int));
                                    dtbPhatSinhPhiUpdate.Columns.Add("DoiTuongPhatSinhPhiID", typeof(int));
                                    dtbPhatSinhPhiUpdate.Columns.Add("LoaiPhi", typeof(string));
                                    dtbPhatSinhPhiUpdate.Columns.Add("NgayPhatSinh", typeof(DateTime));
                                    dtbPhatSinhPhiUpdate.Columns.Add("SoTien", typeof(double));

                                    //Đưa dữ liệu Hội viên đã phát sinh Phí vào DataTable Phát sinh Phí update
                                    foreach (DataRow dtr in dtbHoiVienCaNhanDaPhatSinhPhi.Rows)
                                    {
                                        DataRow dtrPhatSinhPhi = dtbPhatSinhPhiUpdate.NewRow();
                                        dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                        dtrPhatSinhPhi["LoaiHoiVien"] = dtr[1].ToString();
                                        dtrPhatSinhPhi["PhiID"] = Request.QueryString["id"].ToString();
                                        dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                        dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong;
                                        dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                        dtrPhatSinhPhi["SoTien"] = dtr[2].ToString();
                                        dtbPhatSinhPhiUpdate.Rows.Add(dtrPhatSinhPhi);
                                    }

                                    //Đưa dữ liệu Hội viên tổ chức đã phát sinh Phí vào DataTable Phát sinh Phí update
                                    foreach (DataRow dtr in dtbHoiVienTapTheDaPhatSinhPhi.Rows)
                                    {
                                        DataRow dtrPhatSinhPhi = dtbPhatSinhPhiUpdate.NewRow();
                                        dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                        if (dtr[1].ToString() == "0" || dtr[1].ToString() == "2")
                                            dtrPhatSinhPhi["LoaiHoiVien"] = (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan;
                                        else if (dtr[1].ToString() == "1")
                                            dtrPhatSinhPhi["LoaiHoiVien"] = (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe;
                                        else
                                            dtrPhatSinhPhi["LoaiHoiVien"] = DBNull.Value;
                                        dtrPhatSinhPhi["PhiID"] = Request.QueryString["id"].ToString();
                                        dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                        dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong;
                                        dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                        dtrPhatSinhPhi["SoTien"] = dtr[2].ToString();
                                        dtbPhatSinhPhiUpdate.Rows.Add(dtrPhatSinhPhi);
                                    }


                                    if (dtbPhatSinhPhiUpdate.Rows.Count > 0)
                                    {
                                        //Update vào Db

                                        //Tạo bảng tạm Temp1 có cấu trúc giống hệ bảng vừa update
                                        string strCapNhat = @"create table #Temp1(PhatSinhPhiID int
                                                                       ,HoiVienID int
                                                                       ,LoaiHoiVien nchar(1)
                                                                       ,PhiID int
                                                                       ,DoiTuongPhatSinhPhiID int
                                                                       ,LoaiPhi nchar(1)
                                                                       ,NgayPhatSinh date
                                                                       ,SoTien numeric(18,0)
                                                                       )";
                                        SqlCommand cmdCapNhat = new SqlCommand(strCapNhat, conPhiHoiVien,
                                                                               transPhiHoiVien);
                                        cmdCapNhat.CommandTimeout = 99999;
                                        cmdCapNhat.ExecuteNonQuery();

                                        //Đưa dữ liệu từ DataTable Phát sinh phí update vào bảng tạm Temp1
                                        using (
                                            SqlBulkCopy bulk = new SqlBulkCopy(conPhiHoiVien, SqlBulkCopyOptions.Default,
                                                                               transPhiHoiVien))
                                        {
                                            bulk.BulkCopyTimeout = 99999;
                                            bulk.DestinationTableName = "#Temp1";
                                            bulk.WriteToServer(dtbPhatSinhPhiUpdate);
                                        }

                                        //Thực hiện update dữ liệu giữa bảng Temp1 với bảng tblTTPhatSinhPhi
                                        //string mergeSql1 = "merge into [dbo].[tblTTPhatSinhPhi] as Target " +
                                        //                   "using #Temp1 as Source " +
                                        //                   "on " +
                                        //                   "Target.HoiVienID = Source.HoiVienID AND " +
                                        //                   "Target.LoaiHoiVien = Source.LoaiHoiVien AND " +
                                        //                   "Target.PhiID = Source.PhiID AND " +
                                        //                   "Target.LoaiPhi = Source.LoaiPhi " +
                                        //                   "when matched then " +
                                        //                   "update set Target.SoTien = Source.SoTien," +
                                        //                   "Target.NgayPhatSinh = GETDATE();";
                                        string mergeSql1 = "merge into [dbo].[tblTTPhatSinhPhi] as Target " +
                                                             "using #Temp1 as Source " +
                                                             "on " +
                                                             "Target.HoiVienID = Source.HoiVienID AND " +
                                                             "Target.LoaiHoiVien COLLATE Latin1_General_CS_AS = Source.LoaiHoiVien COLLATE Latin1_General_CS_AS AND " +
                                                             "Target.PhiID = Source.PhiID AND " +
                                                             "Target.LoaiPhi COLLATE Latin1_General_CS_AS = Source.LoaiPhi COLLATE Latin1_General_CS_AS " +
                                                             "when matched then " +
                                                             "update set Target.SoTien = Source.SoTien," +
                                                             "Target.NgayPhatSinh = GETDATE();";
                                        cmdCapNhat.CommandText = mergeSql1;
                                        cmdCapNhat.ExecuteNonQuery();

                                        //Clean up the temp table
                                        cmdCapNhat.CommandText = "drop table #Temp1";
                                        cmdCapNhat.ExecuteNonQuery();
                                    }
                                    transPhiHoiVien.Commit();
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                }
                                catch (Exception ex)
                                {
                                    transPhiHoiVien.Rollback();
                                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
                                    return;
                                }

                                sql1.Connection.Close();
                                sql1.Connection.Dispose();
                                sql1 = null;
                                cm.ghilog("DMPhiKSCL", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục DMPhiKSCL");
                                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                            }
                            else
                            {
                                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                            }
                        }
                        else if (Request.QueryString["tinhtrang"] == "thoaiduyet") //Thoái duyệt
                        {
                            SqlCommand sql = new SqlCommand();
                            sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKSCL where PhiId = " + Request.QueryString["id"];
                            DataSet ds = new DataSet();
                            ds = DataAccess.RunCMDGetDataSet(sql);
                            //Phải đang ở trạng thái duyệt thì mới cho thoái duyệt
                            if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                            {
                                sql.CommandText = "UPDATE tblTTDMPhiKSCL set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                                sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                                sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                                DataAccess.RunActionCmd(sql);
                                sql.Connection.Close();
                                sql.Connection.Dispose();
                                sql = null;
                                cm.ghilog("DMPhiHoiVien", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" vào danh mục TTDMPhiHoiVien");
                                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                            }
                            else
                            {
                                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                            }
                        }
                    }
                }
                Id = Convert.ToInt32(Request.QueryString["id"]);
                LoadDSNhanSuCTy(Id);
               // LoadMaCongTy(Id);
                //LoadThongTin();

                //LoadNut_footer(getTT(Id));

            }

           
        }
    }

    //private void LoadMaCongTy(int Id)
    //{
    //    SqlCommand sql = new SqlCommand();
    //    sql.CommandText = "select MaHoiVienTapThe from tblHoiVienTapThe where HoiVienTapTheID IN (Select HoiVienTapTheID from tblTTDMPhiKSCLChiTiet "
    //    + " where PhiId = " + Id + ")";
    //    DataSet ds = new DataSet();
    //    ds = DataAccess.RunCMDGetDataSet(sql);
    //    dt = ds.Tables[0];
    //    string Ma = "";
    //    if (dt.Rows.Count > 0)
    //    {
    //        for (int i = 0; i < dt.Rows.Count; i++ )
    //        {
    //            if (i==0)
    //                Ma += dt.Rows[i][0].ToString().TrimEnd();
    //            else
    //                Ma += ", " + dt.Rows[i][0].ToString().TrimEnd();
    //        }
    //    }
    //    DanhSachKiemTraTrucTiep.Text = Ma;
    //}

    private int getTT(int Id)
    {
        SqlCommand sql = new SqlCommand();
            sql.CommandText = "select MaPhi, NgayLap, DienGiai, ThoiHanNopPhi, TinhTrangID, TenTrangThai "
                + " from tblTTDMPhiKSCL inner join tblDMTrangThai on tblTTDMPhiKSCL.TinhTrangID = tblDMTrangThai.TrangThaiID "
            + " where PhiId = " + Convert.ToInt16(Request.QueryString["id"]);
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];
            int iTinhTrangBanGhi = 0;
            

            if (dt.Rows.Count > 0)
            {
               iTinhTrangBanGhi = Convert.ToInt32(dt.Rows[0]["TinhTrangID"]);
               
            }
            return iTinhTrangBanGhi;
    }

   
    List<objHoiVienTapThe> lstLuuCty = new List<objHoiVienTapThe>();
    List<objNhanSurpt> lstLuuNhanSu = new List<objNhanSurpt>();
    private class objHoiVienTapThe
    {
        public int STT { set; get; }
        public int HoiVienTapTheID { set; get; }
        public string TenDoanhNghiep { set; get; }
        public string PhiKiemSoat { set; get; }
        public string txtdis { set; get; }
    }

    protected void btnThemNS123_OnClick(object sender, EventArgs e)
    {
       

        LoadDSNhanSuCTy(Id);
    }

    protected void btnCapNhat_OnClick(object sender, EventArgs e)
    {
        LoadDSNhanSuCTy(Id);
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        LoadDSNhanSuCTy(0);
    }

    public void LoadDSNhanSuCTy(int idSua)
    {
        //string yourValue = RandomList.Value;
        if (!string.IsNullOrEmpty(idPhiCty.Text))
            Session["PhiCongTy"] = idPhiCty.Text;
        string phi = idPhiCty.Text;

        if (Id != 0)
            phi = Session["PhiCongTy"] as string;
        SqlCommand sql = new SqlCommand();
        string key = IDTimKiem.Text;


        string strIDDanhSach = "";
        if (idSua == 0)
        {
            strIDDanhSach = DanhSachKiemTraTrucTiepID.Value;
            DanhSachKiemTraTrucTiep.Text = DanhSachKiemTraTrucTiepMa.Value;
        }
        else
        {
            sql.CommandText = "select top 1 A.DSKiemTraTrucTiepID, B.MaDanhSach from tblTTDMPhiKSCL A" +
                              " left join tblKSCLDSKiemTraTrucTiep B" +
                              " on A.DSKiemTraTrucTiepID = B.DSKiemTraTrucTiepID" +
                              " where PhiID = " + idSua;
            DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            if (dtb.Rows.Count > 0)
            {
                DanhSachKiemTraTrucTiepID.Value = dtb.Rows[0][0].ToString();
                DanhSachKiemTraTrucTiepMa.Value = dtb.Rows[0][1].ToString();
                strIDDanhSach = DanhSachKiemTraTrucTiepID.Value;
                DanhSachKiemTraTrucTiep.Text = DanhSachKiemTraTrucTiepMa.Value;
            }
        }
        if (!string.IsNullOrEmpty(strIDDanhSach))
            //if (Session["CongTyChon"] != null)
        {
            //List<int> lstIdChon = new List<int>();
            //lstIdChon = Session["CongTyChon"] as List<int>;
            //string result = string.Join(", ", lstIdChon);

            ////DanhSachKiemTraTrucTiep.Text = result;
            //List<string> lstMaChon = new List<string>();
            //lstMaChon = Session["MaCongTyChon"] as List<string>;
            //string Ma = string.Join(", ", lstMaChon);
            //DanhSachKiemTraTrucTiep.Text = Ma;


            if (idSua != 0) //Sửa
            {
                if (!string.IsNullOrEmpty(key))
                {
                    //////sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, MucPhi, disabled = 'true' from tblHoiVienTapThe a"
                    //////           + " inner join tblTTDMPhiKSCLChiTiet b on a.HoiVienTapTheID = b.HoiVienTapTheID "
                    //////           + " WHERE b.PhiID = " + idSua + " and TenDoanhNghiep like '%" + key + "%'";


                    sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, MucPhi, disabled = 'true' from tblHoiVienTapThe a"
                                      + " inner join tblTTDMPhiKSCLChiTiet b on a.HoiVienTapTheID = b.HoiVienTapTheID "
                                      + " WHERE b.PhiID = " + idSua + " and TenDoanhNghiep like '%" + key + "%'";
                }
                else
                {
                    sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, MucPhi, disabled = 'true' from tblHoiVienTapThe a"
                                      + " inner join tblTTDMPhiKSCLChiTiet b on a.HoiVienTapTheID = b.HoiVienTapTheID "
                                      + " WHERE  b.PhiID = " + idSua;
                }
            }
            else //Thêm mới
            {
                if (!string.IsNullOrEmpty(key))
                {
                    //sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, '', disabled = 'true' from tblHoiVienTapThe a "
                    //                    +" WHERE HoiVienTapTheID IN (" + result + ") AND TenDoanhNghiep like '%" + key + "%'";

                    sql.CommandText =
                        "select a.HoiVienTapTheID, TenDoanhNghiep, '', disabled = 'true' from tblKSCLDSKiemTraTrucTiepChiTiet B" +
                        " left join tblHoiVienTapThe a on B.HoiVienTapTheID = a.HoiVienTapTheID "
                        + " WHERE B.DSKiemTraTrucTiepId = " + strIDDanhSach + " AND TenDoanhNghiep like '%" + key + "%'";
                }
                else
                {
                    //sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, '', disabled = 'true' from tblHoiVienTapThe a "
                    //    + " WHERE HoiVienTapTheID IN (" + result + ")";
                    ;

                    sql.CommandText =
                        "select a.HoiVienTapTheID, TenDoanhNghiep, '', disabled = 'true' from tblKSCLDSKiemTraTrucTiepChiTiet B" +
                        " left join tblHoiVienTapThe a on B.HoiVienTapTheID = a.HoiVienTapTheID "
                        + " WHERE B.DSKiemTraTrucTiepId = " + strIDDanhSach;
                }
            }
            // }

            //else
            //{
            //    if (idSua != 0)
            //    {
            //        if (!string.IsNullOrEmpty(key))
            //        {
            //            sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, MucPhi, disabled = 'false' from tblHoiVienTapThe a"
            //                       + " inner join tblTTDMPhiKSCLChiTiet b on a.HoiVienTapTheID = b.HoiVienTapTheID "
            //                       + " WHERE b.PhiID = " + idSua + " and TenDoanhNghiep like '%" + key + "%'";

            //        }
            //        else
            //        {
            //            sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, MucPhi, disabled = 'false' from tblHoiVienTapThe a "
            //                       + " inner join tblTTDMPhiKSCLChiTiet b on a.HoiVienTapTheID = b.HoiVienTapTheID "
            //                       + " WHERE b.PhiID = " + idSua;
            //        }
            //    }
            //    else
            //    {
            //        if (!string.IsNullOrEmpty(key))
            //        {
            //            sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, '', disabled = 'false'  from tblHoiVienTapThe a "
            //                                + " WHERE HoiVienTapTheID = -1 AND TenDoanhNghiep like '%" + key + "%'";
            //        }
            //        else
            //        {
            //            sql.CommandText = "select a.HoiVienTapTheID, TenDoanhNghiep, '', disabled = 'false'  from tblHoiVienTapThe a WHERE HoiVienTapTheID = -1";
            //            //+ " left join tblTTDMPhiKSCLChiTiet b on a.HoiVienTapTheID = b.HoiVienTapTheID";
            //        }
            //    }
            //}

            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);

            int i = 0;
            List<objHoiVienTapThe> lst = new List<objHoiVienTapThe>();
            foreach (DataRow dtr in ds.Tables[0].Rows)
            {
                i++;

                objHoiVienTapThe new1 = new objHoiVienTapThe();
                new1.STT = i;
                new1.HoiVienTapTheID = Convert.ToInt32(dtr[0]);
                new1.TenDoanhNghiep = dtr[1].ToString();
                if (string.IsNullOrEmpty(phi))
                    //new1.PhiKiemSoat = dtr[2].ToString();
                    new1.PhiKiemSoat = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}",
                                                     dtr[2]);
                else
                    new1.PhiKiemSoat = phi;

                new1.txtdis = dtr[3].ToString();
                //new1.PhiKiemSoat = DateTime.Now.Ticks.ToString();
                lst.Add(new1);
            }
            lstLuuCty = lst;
            Session["lstLuuCongTy"] = lstLuuCty;
            rptCha.DataSource = lst;
            rptCha.DataBind();
        }
    }

    private class objNhanSurpt
    {
        
        public string HoTen { set; get; }
        public string PhiKiemSoat { set; get; }
        public string MaNhanSu { set; get; }        
    }

    //protected void ThemHoiVien_Click(object sender, EventArgs e)
    //{
    //    var argument = ((Button)sender).CommandArgument;
    //}

    protected void rptCha_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        objHoiVienTapThe dv = e.Item.DataItem as objHoiVienTapThe;
        if (dv != null)
        {
            Repeater repeater2 = e.Item.FindControl("rptCon") as Repeater;
            int cid = dv.HoiVienTapTheID;

           // List<objNhanSurpt> lst = new List<objNhanSurpt>();
            repeater2.DataSource = GetNhanSuCTy(cid, Id);
            repeater2.DataBind();
            //DataTable tb = pbl.GetNewestProductByCategoryID(cid);
            //repeater2.DataSource = tb;
            //repeater2.DataBind();

            //Categories cat = cbl.GetCategoryByCategoryID(cid);
            //Label lblCategory = e.Item.FindControl("lblCategory") as Label;
            //lblCategory.Text = cat.CategoryName;
        } 
    }


    private List<objNhanSurpt> GetNhanSuCTy(int cid, int idSua)
    {
       
        List<objNhanSurpt> lst = new List<objNhanSurpt>();
        SqlCommand sql = new SqlCommand();

        List<int> lstIdChon = new List<int>();
        if (Session["lstIdChon"] != null)
            lstIdChon = Session["lstIdChon"] as List<int>;
        string result = string.Join(", ", lstIdChon);
       
        if (idSua != 0)
        {
            //if (!string.IsNullOrEmpty(Key))
            //{
            //    sql.CommandText = "select a.HoiVienCaNhanID, HoDem,Ten, MucPhi from tblHoiVienCaNhan a"
            //                       + " inner join tblTTDMPhiKSCLChiTiet b on a.HoiVienCaNhanID = b.HoiVienCaNhanID"
            //                       + " where a.HoiVienTapTheID = " + cid + " and (HoDem + ' ' + Ten) like '%" + Key + "%'";
            //}
            //else
            //{
                sql.CommandText = "select a.HoiVienCaNhanID, HoDem,Ten, MucPhi from tblHoiVienCaNhan a"
                                   + " inner join tblTTDMPhiKSCLChiTiet b on a.HoiVienCaNhanID = b.HoiVienCaNhanID"
                                   + " where a.HoiVienTapTheID = " + cid + " AND PhiID = " + idSua;
           // }
        }
        else
        {
            //if (!string.IsNullOrEmpty(Key))
            //{
            //    sql.CommandText = "select a.HoiVienCaNhanID, HoDem,Ten, '' from tblHoiVienCaNhan a"
            //                       + " where a.HoiVienTapTheID = " + cid + " and (HoDem + ' ' + Ten) like '%" + Key + "%'";
            //}
            //else
            //{
            if (lstIdChon.Count() != 0)
             {
                sql.CommandText = "select a.HoiVienCaNhanID, HoDem,Ten, '' from tblHoiVienCaNhan a"
                                   + " where a.HoiVienTapTheID = " + cid + " AND HoiVienCaNhanID IN (" + result + ")";
             }
            else
            {//debug
                sql.CommandText = "select a.HoiVienCaNhanID, HoDem,Ten, '' from tblHoiVienCaNhan a"
                                   + " where a.HoiVienTapTheID = -1";
            }
            //}
        }

        DataSet ds = new DataSet();
        ds = DataAccess.RunCMDGetDataSet(sql);

        if (!string.IsNullOrEmpty(idPhiHVCN.Text))
            Session["PhiCaNhan"] = idPhiHVCN.Text;

        string phi = idPhiHVCN.Text;

        if (Id != 0)
            phi = Session["PhiCaNhan"] as string;

        //string phi = idPhiHVCN.Text;

        int i = 0;
        foreach (DataRow dtr in ds.Tables[0].Rows)
        {
            i++;

            objNhanSurpt new1 = new objNhanSurpt();
            new1.HoTen = dtr[1].ToString() + " " + dtr[2].ToString();
            if (string.IsNullOrEmpty(phi))
                //new1.PhiKiemSoat = dtr[3].ToString();
                new1.PhiKiemSoat = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr[3]);
            else
                new1.PhiKiemSoat = phi;
            new1.MaNhanSu = dtr[0].ToString();
            lst.Add(new1);
            lstLuuNhanSu.Add(new1);

            //if (idSua != 0)
            //{
            //    lstIdChon.Add()
            //}
        }

        
        Session["lstLuuNhanSu"] = lstLuuNhanSu;

        
        return lst;
    }

    public void LoadDoiTuong(int iDoiTuongID = 0)
    {
        string output_html = "";
        if (iDoiTuongID == 0)
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT distinct DoiTuongNopPhiID, TenDoiTuong, LoaiDoiTuong FROM tblDMDoiTuongNopPhi";
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            int i = 0;
            foreach (DataRow dtr in ds.Tables[0].Rows)
            {
                i++;
                if (i == 1)
                    output_html += @"<option selected=""selected"" value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
                else
                    output_html += @"<option value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
            }
        }
        else
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT TenDoiTuongFROM tblDMDoiTuongNopPhi where DoiTuongNopPhiID = " + iDoiTuongID;
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            output_html += @"<option selected=""selected"" value='" + ds.Tables[0].Rows[0].ToString() + "'>" + ds.Tables[0].Rows[1].ToString() + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    //public void LoadNut()
    //{
    //    //string output_html = "<div  class='dataTables_length'>";
    //    //if (string.IsNullOrEmpty(Request.QueryString["mode"]))
    //    //    output_html += "<a id='btn_luu'  href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>";
    //    //else if (Request.QueryString["mode"] == "choduyet")
    //    //{
    //    //    output_html += "<a id='btn_xoa'  href='#none' class='btn btn-rounded' onclick='xoa();'><i class='iconfa-remove-sign'></i> Xóa</a>";
    //    //    output_html += "<a id='btn_duyet'  href='#none' class='btn btn-rounded' onclick='duyet();'><i class='iconfa-ok'></i> Duyệt</a>";
    //    //    output_html += "<a id='btn_tuchoi'  href='#none' class='btn btn-rounded' onclick='tuchoi();'><i class='iconfa-remove'></i> Từ chối</a>";
    //    //}
    //    //else if (Request.QueryString["mode"] == "daduyet")
    //    //{
    //    //    output_html += "<a id='btn_thoaiduyet'  href='#none' class='btn btn-rounded' onclick='thoaiduyet();'><i class='iconfa-remove-circle'></i> Thoái duyệt</a>";
    //    //}
    //    //output_html += "</div>";
    //    //System.Web.HttpContext.Current.Response.Write(output_html);
    //}

    protected void AnNut()
    {
        Commons cm = new Commons();

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKSCL", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_luu').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKSCL", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKSCL", cm.connstr).Contains("DUYET|"))
        {
            Response.Write("$('#btn_duyet').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKSCL", cm.connstr).Contains("TUCHOI|"))
        {
            Response.Write("$('#btn_tuchoi').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKSCL", cm.connstr).Contains("THOAIDUYET|"))
        {
            Response.Write("$('#btn_thoaiduyet').remove();");
        }
    }

    protected void LoadThongTin()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
        {

            SqlCommand sql = new SqlCommand();
            sql.CommandText = "select MaPhi, NgayLap, DienGiai, ThoiHanNopPhi, TinhTrangID, TenTrangThai "
                + " from tblTTDMPhiKSCL inner join tblDMTrangThai on tblTTDMPhiKSCL.TinhTrangID = tblDMTrangThai.TrangThaiID "
            + " where PhiId = " + Convert.ToInt16(Request.QueryString["id"]);
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            dt = ds.Tables[0];
            int iTinhTrangBanGhi = 0;
            string strTinhTrangBanGhi = "";

            if (dt.Rows.Count > 0)
            {
                Response.Write("$('#MaPhi').val('" + dt.Rows[0]["MaPhi"] + "');" + System.Environment.NewLine);
                if (dt.Rows[0]["NgayLap"] != DBNull.Value)
                    Response.Write("$('#NgayLap').val('" + Convert.ToDateTime(dt.Rows[0]["NgayLap"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                Response.Write("$('#DienGiai').val('" + dt.Rows[0]["DienGiai"] + "');" + System.Environment.NewLine);
                if (dt.Rows[0]["ThoiHanNopPhi"] != DBNull.Value)
                    Response.Write("$('#ThoiHanNopPhi').val('" + Convert.ToDateTime(dt.Rows[0]["ThoiHanNopPhi"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);

                
                iTinhTrangBanGhi = Convert.ToInt32(dt.Rows[0]["TinhTrangID"]);
                
                strTinhTrangBanGhi = dt.Rows[0]["TenTrangThai"].ToString();

                if (Request.QueryString["mode"] == "view" && Request.QueryString["tinhtrang"] != "xoa" && iTinhTrangBanGhi != Convert.ToInt32(EnumVACPA.TinhTrang.DaPheDuyet))
                {
                    //Response.Write("$('#form_quanlyphikiemsoatchatluong input,select,td,a').attr('disabled', true);");
                    Response.Write("$('#form_quanlyphikiemsoatchatluong input,select,td').attr('disabled', true);");
                    //Response.Write("$('#akickChon').attr('disabled', true);");
                    //Response.Write("$('#btThemCongTy').attr('disabled', true);");
                    //Response.Write("$('.idKickChon2').attr('disabled', true);");
                    //Response.Write("$('.btThemTrongTB').attr('disabled', true);");
                }

                if (Request.QueryString["mode"] == "edit" || Request.QueryString["tinhtrang"] == "xoa" || (Request.QueryString["mode"] == "view" && iTinhTrangBanGhi == Convert.ToInt32(EnumVACPA.TinhTrang.DaPheDuyet)))
                {
                    Response.Write("$('#akickChon').attr('disabled', true);");
                    Response.Write("$('#btThemCongTy').attr('disabled', true);");
                    Response.Write("$('.idKickChon2').attr('disabled', true);");
                    Response.Write("$('.btThemTrongTB').attr('disabled', true);");


                }

                //else if (Request.QueryString["mode"] == "edit")
                //    Response.Write("$('#form_quanlyphikiemsoatchatluong input,select,td,a).attr('disabled', true);");

               // LoadNut_footer(iTinhTrangBanGhi);
            }

            string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
});";
            Response.Write(a + System.Environment.NewLine);
//            if (Request.QueryString["mode"] == "choduyet")
//            {
//                string a = @"
//            $(function () {
//$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
//});";
//                Response.Write(a + System.Environment.NewLine);
//            }
//            else if (Request.QueryString["mode"] == "daduyet")
//            {
//                string a = @"
//            $(function () {
//$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
//});";
//                Response.Write(a + System.Environment.NewLine);
//            }
            if (Request.QueryString["mode"] == "view")
            {
                Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                Response.Write("$('#form_quanlyphihoivien input,select,textarea').attr('disabled', true);");
                Response.Write("$('#btn_capnhatphi').remove();" + System.Environment.NewLine);
                if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ChoDuyet)
                {
                    //Hiện Từ chối, Duyệt, Xóa
                    //Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                    //Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                    //Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                   //
                }
                else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                {
                    //Hiện thoái duyệt và cập nhật
                    Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                    //Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                    
                }
                else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                {
                    //Hiện Xóa
                    Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                    //Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                }
            }

                  //Nếu đang ở chế độ sửa
            else if (Request.QueryString["mode"] == "edit")
            {
                //Nếu tình trạng bản ghi là đã phê duyệt
                //Thì không cho làm gì hết
                if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                {
                    Response.Write("$('#form_quanlyphihoivien input,select,textarea').attr('disabled', true);");
                    Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_capnhatphi').remove();" + System.Environment.NewLine);

                }
                //Nếu tình trạng bản ghi không phải là đã phê duyệt => chỉ hiện sửa
                else
                {
                    Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                  
                    //Có thể sửa Ngày lập, ngày áp dụng
                    string output_html = @"$(function () {  $('#NgayLap, #ThoiHanNopPhi').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-75:+25',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
                    Response.Write(output_html + System.Environment.NewLine);
                }
            }
        }
        else //Thêm mới
        {
            string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + "Chờ duyệt" + @"</i>')
});";
            Response.Write(a + System.Environment.NewLine);

            string output_html = @"$(function () {  $('#NgayLap, #ThoiHanNopPhi').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = "$(function () {$('#NgayLap, #ThoiHanNopPhi').datepicker('setDate', new Date())});";
            Response.Write(output_html + System.Environment.NewLine);

            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
        }
    }

    protected void btnLuu_OnClick(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            //Chạy function tự sinh mã phí
            string Nam = "";
            if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                Nam =
                    DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy",
                                        System.Globalization.CultureInfo.InvariantCulture).Year.ToString();
            Nam = Nam.Substring(2);

            //int DoiTuongApDung = Convert.ToInt32(Request.Form["DoiTuongApDung"]);

            SqlCommand sql1 = new SqlCommand();
            sql1.CommandText = "select [dbo].[LaySoChayDanhMucPhiKiemSoatChatLuong] (" + Nam + ")";
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql1);

            string MaPhi = ds.Tables[0].Rows[0][0].ToString();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;

            string DienGiai = Request.Form["DienGiai"];

            var modified = new StringBuilder();


            DateTime? NgayLap = null;
            if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                NgayLap = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy",
                                              System.Globalization.CultureInfo.InvariantCulture);
            DateTime? ThoiHanNopPhi = null;
            if (!string.IsNullOrEmpty(Request.Form["ThoiHanNopPhi"]))
                ThoiHanNopPhi = DateTime.ParseExact(Request.Form["ThoiHanNopPhi"], "dd/MM/yyyy",
                                                    System.Globalization.CultureInfo.InvariantCulture);

            DateTime NgayNhap = DateTime.Now;


            string connStr = DataAccess.ConnString;
            SqlConnection conKSCL = new SqlConnection(connStr);
            SqlTransaction transKSCL;
            conKSCL.Open();
            transKSCL = conKSCL.BeginTransaction();

            try
            {


                SqlCommand sql = new SqlCommand();

                // EnumVACPA.TinhTrang TinhTrang = new EnumVACPA.TinhTrang();
                int TinhTrang = Convert.ToInt32(EnumVACPA.TinhTrang.ChoDuyet);
                string DSKTID = DanhSachKiemTraTrucTiepID.Value;
                ////sql.CommandText =
                ////    "INSERT INTO tblTTDMPhiKSCL(MaPhi, NgayLap, ThoiHanNopPhi, DienGiai, TinhTrangID, NgayNhap, NguoiNhap) " +
                ////    " VALUES (@MaPhi, @NgayLap, @ThoiHanNopPhi, @DienGiai, @TinhTrang, @NgayNhap, @Admin_TenDangNhap)";

                sql = new SqlCommand("[dbo].[tblTTDMPhiKSCL_Insert]", conKSCL, transKSCL);
                sql.CommandType = CommandType.StoredProcedure;

                sql.Parameters.AddWithValue("@MaPhi", MaPhi);
                if (NgayLap != null)
                    sql.Parameters.AddWithValue("@NgayLap", NgayLap);
                else
                    sql.Parameters.AddWithValue("@NgayLap", DBNull.Value);
                if (ThoiHanNopPhi != null)
                    sql.Parameters.AddWithValue("@ThoiHanNopPhi", ThoiHanNopPhi);
                else
                    sql.Parameters.AddWithValue("@ThoiHanNopPhi", DBNull.Value);
                sql.Parameters.AddWithValue("@DSKiemTraTrucTiepID", DSKTID);
                sql.Parameters.AddWithValue("@DienGiai", DienGiai);
                sql.Parameters.AddWithValue("@TinhTrangId", TinhTrang);
                sql.Parameters.AddWithValue("@NgayNhap", DateTime.Now);
                sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);
                sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);

                sql.Parameters.Add("@PhiID", SqlDbType.Int);
                sql.Parameters["@PhiID"].Direction = ParameterDirection.Output;

                //Sử dụng trans
                //sql.Connection = conKSCL;
                //sql.Transaction = transKSCL;
                sql.ExecuteNonQuery();
                //DataAccess.RunActionCmd(sql);
                //////sql.Connection.Close();
                //////sql.Connection.Dispose();
                //////sql = null;

                cm.ghilog("quanlyphikiemsoatchatluong", "Thêm giá trị \"" + MaPhi + "\" vào danh mục tblTTDMPhiKSCL");

                //SqlCommand sql2 = new SqlCommand();

                //sql2.CommandText = "SELECT MAX(PhiID) FROM tblTTDMPhiKSCL";
                //DataSet ds2 = new DataSet();

                //ds2 = DataAccess.RunCMDGetDataSet(sql2);

                //MinhLP sửa code lấy danh sách

                //sql2.Connection = conKSCL;
                //sql.Transaction = transKSCL;

                ////SqlDataAdapter adapter = new SqlDataAdapter();
                ////SqlCommandBuilder cmdBuilder = new SqlCommandBuilder();
                ////adapter = new SqlDataAdapter(sql2.CommandText, conKSCL);
                ////cmdBuilder = new SqlCommandBuilder(adapter);
                ////adapter.Fill(ds);

                //sql.ExecuteNonQuery();

                //int IdKS = Convert.ToInt32(ds2.Tables[0].Rows[0][0]);
                //////sql.Connection.Close();
                //////sql.Connection.Dispose();
                //////sql = null;

                //int IdKS = Convert.ToInt32(ds2.Tables[0].Rows[0][0]);
                int? IdKS = null;
                if (sql.Parameters["@PhiID"] != null)
                    IdKS = (int)sql.Parameters["@PhiID"].Value;
                List<objHoiVienTapThe> lst = new List<objHoiVienTapThe>();
                if (Session["lstLuuCongTy"] != null)
                    lst = Session["lstLuuCongTy"] as List<objHoiVienTapThe>;
                List<objHoiVienTapThe> lstTapTheGhiLog = new List<objHoiVienTapThe>();
                if (lst.Count() != 0)
                {
                    foreach (var temcty in lst)
                    {
                        sql = new SqlCommand();

                        if (!string.IsNullOrEmpty(temcty.PhiKiemSoat))
                        {
                            sql.CommandText = "INSERT INTO tblTTDMPhiKSCLChiTiet(PhiID, HoiVienTapTheID, MucPhi) " +
                                              " VALUES ('" + IdKS + "', '" + temcty.HoiVienTapTheID + "', '" +
                                              strBoChamPhay(temcty.PhiKiemSoat) + "')";
                            lstTapTheGhiLog.Add(temcty);
                        }
                        else
                        {
                            sql.CommandText = "INSERT INTO tblTTDMPhiKSCLChiTiet(PhiID, HoiVienTapTheID, MucPhi) " +
                   " VALUES ('" + IdKS + "', '" + temcty.HoiVienTapTheID + "', '0')";
                            lstTapTheGhiLog.Add(temcty);
                        }

                        sql.Connection = conKSCL;
                        sql.Transaction = transKSCL;
                        sql.ExecuteNonQuery();

                        //DataAccess.RunActionCmd(sql);
                        ////sql.Connection.Close();
                        ////sql.Connection.Dispose();
                        ////sql = null;

                        //////cm.ghilog("quanlyphikiemsoatchatluong",
                        //////          "Thêm giá trị hội viên cty \"" + temcty.HoiVienTapTheID + " mức phí " +
                        //////          temcty.PhiKiemSoat + "\" vào danh mục tblTTDMPhiKSCLChiTiet");
                    }
                }
                List<objNhanSurpt> lstNS = new List<objNhanSurpt>();
                if (Session["lstLuuNhanSu"] != null)
                    lstNS = Session["lstLuuNhanSu"] as List<objNhanSurpt>;
                List<objNhanSurpt> lstGhiLogCaNhan = new List<objNhanSurpt>();
                if (lstNS.Count() != 0)
                {
                    foreach (var temcNS in lstNS)
                    {
                        sql = new SqlCommand();

                        if (!string.IsNullOrEmpty(temcNS.PhiKiemSoat))
                        {
                            sql.CommandText = "INSERT INTO tblTTDMPhiKSCLChiTiet(PhiID, HoiVienCaNhanID, MucPhi) " +
                                              " VALUES ('" + IdKS + "', '" + temcNS.MaNhanSu + "', '" +
                                              strBoChamPhay(temcNS.PhiKiemSoat) + "')";
                            lstGhiLogCaNhan.Add(temcNS);
                        }
                        else
                        {
                            sql.CommandText = "INSERT INTO tblTTDMPhiKSCLChiTiet(PhiID, HoiVienCaNhanID, MucPhi) " +
                                             " VALUES ('" + IdKS + "', '" + temcNS.MaNhanSu + "', '0')";
                            lstGhiLogCaNhan.Add(temcNS);
                        }

                        sql.Connection = conKSCL;
                        sql.Transaction = transKSCL;
                        sql.ExecuteNonQuery();

                        //DataAccess.RunActionCmd(sql);
                        //sql.Connection.Close();
                        //sql.Connection.Dispose();
                        //sql = null;

                        //cm.ghilog("quanlyphikiemsoatchatluong",
                        //          "Thêm giá trị hội viên cá nhân \"" + temcNS.MaNhanSu + " mức phí " +
                        //          temcNS.PhiKiemSoat + "\" vào danh mục tblTTDMPhiKSCLChiTiet");
                    }
                }
                transKSCL.Commit();
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                //Ghi log công ty
                foreach (var temcty1 in lstTapTheGhiLog)
                {
                    cm.ghilog("quanlyphikiemsoatchatluong",
                                  "Thêm giá trị hội viên cty \"" + temcty1.HoiVienTapTheID + " mức phí " +
                                  temcty1.PhiKiemSoat + "\" vào danh mục tblTTDMPhiKSCLChiTiet");
                }
                //Ghi log cá nhân
                foreach (var temcNS1 in lstGhiLogCaNhan)
                {
                    cm.ghilog("quanlyphikiemsoatchatluong",
                                  "Thêm giá trị hội viên cá nhân \"" + temcNS1.MaNhanSu + " mức phí " +
                                  temcNS1.PhiKiemSoat + "\" vào danh mục tblTTDMPhiKSCLChiTiet");
                }
                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
            }
            catch (Exception ex)
            {
                transKSCL.Rollback();
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
                return;
            }
        }
        else
        {
            string connStr = DataAccess.ConnString;
            SqlConnection conKSCL = new SqlConnection(connStr);
            SqlTransaction transKSCL;
            conKSCL.Open();
            transKSCL = conKSCL.BeginTransaction();

            try
            {
                SqlCommand sql = new SqlCommand();

                int idPhi = Convert.ToInt32(Request.QueryString["id"]);
                string DienGiai = Request.Form["DienGiai"];

                //var modified = new StringBuilder();


                DateTime? NgayLap = null;
                if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                    NgayLap = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy",
                                                  System.Globalization.CultureInfo.InvariantCulture);
                DateTime? ThoiHanNopPhi = null;
                if (!string.IsNullOrEmpty(Request.Form["ThoiHanNopPhi"]))
                    ThoiHanNopPhi = DateTime.ParseExact(Request.Form["ThoiHanNopPhi"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture);

                DateTime NgayNhap = DateTime.Now;

                sql = new SqlCommand();

                // EnumVACPA.TinhTrang TinhTrang = new EnumVACPA.TinhTrang();
                int TinhTrang = Convert.ToInt32(EnumVACPA.TinhTrang.ChoDuyet);

                sql.CommandText =
                    "UPDATE tblTTDMPhiKSCL set TinhTrangID = @TinhTrang, NgayLap = @NgayLap, ThoiHanNopPhi = @ThoiHanNopPhi, DienGiai = @DienGiai WHERE PhiID = @idPhi";
                sql.Parameters.AddWithValue("@idPhi", idPhi);
                if (NgayLap != null)
                    sql.Parameters.AddWithValue("@NgayLap", NgayLap);
                else
                    sql.Parameters.AddWithValue("@NgayLap", DBNull.Value);
                if (ThoiHanNopPhi != null)
                    sql.Parameters.AddWithValue("@ThoiHanNopPhi", ThoiHanNopPhi);
                else
                    sql.Parameters.AddWithValue("@ThoiHanNopPhi", DBNull.Value);

                sql.Parameters.AddWithValue("@DienGiai", Request.Form["DienGiai"]);
                sql.Parameters.AddWithValue("@TinhTrang", TinhTrang);

                sql.Connection = conKSCL;
                sql.Transaction = transKSCL;
                sql.ExecuteNonQuery();

                //DataAccess.RunActionCmd(sql);
                ////sql.Connection.Close();
                ////sql.Connection.Dispose();
                ////sql = null;

                
                List<objHoiVienTapThe> lst = new List<objHoiVienTapThe>();
                if (Session["lstLuuCongTy"] != null)
                    lst = Session["lstLuuCongTy"] as List<objHoiVienTapThe>;

                if (lst.Count() != 0)
                {
                    foreach (var temcty in lst)
                    {
                        sql = new SqlCommand();

                        sql.CommandText = "SELECT COUNT(*) FROM tblTTDMPhiKSCLChiTiet WHERE PhiID = " + idPhi +
                                          " AND HoiVienTapTheID = " + temcty.HoiVienTapTheID;
                        DataSet ds2 = new DataSet();
                        ds2 = DataAccess.RunCMDGetDataSet(sql);

                        int IDCheck = Convert.ToInt32(ds2.Tables[0].Rows[0][0]);
                        //sql.Connection.Close();
                        //sql.Connection.Dispose();
                        //sql = null;
                        if (IDCheck == 0)
                        {
                            sql = new SqlCommand();
                            try
                            {
                                sql.CommandText = "INSERT INTO tblTTDMPhiKSCLChiTiet(PhiID, HoiVienTapTheID, MucPhi) " +
                                                  " VALUES ('" + idPhi + "', '" + temcty.HoiVienTapTheID + "', '" +
                                                  strBoChamPhay(temcty.PhiKiemSoat) + "')";

                                DataAccess.RunActionCmd(sql);
                                ////sql.Connection.Close();
                                ////sql.Connection.Dispose();
                                ////sql = null;

                                cm.ghilog("quanlyphikiemsoatchatluong",
                                          "Thêm giá trị hội viên cty (bằng cách sửa PhiID: " + idPhi + ") \"" +
                                          temcty.HoiVienTapTheID + " mức phí " + temcty.PhiKiemSoat +
                                          "\" vào danh mục tblTTDMPhiKSCLChiTiet");
                            }
                            catch
                            {
                                //////sql.Connection.Close();
                                //////sql.Connection.Dispose();
                                //////sql = null;
                            }
                        }
                        else
                        {

                            sql = new SqlCommand();
                            try
                            {
                                sql.CommandText = "UPDATE tblTTDMPhiKSCLChiTiet SET MucPhi = " +
                                                  Convert.ToDouble(strBoChamPhay(temcty.PhiKiemSoat)) + " WHERE PhiID = " + idPhi +
                                                  " AND HoiVienTapTheID = " + temcty.HoiVienTapTheID;

                                DataAccess.RunActionCmd(sql);
                                ////sql.Connection.Close();
                                ////sql.Connection.Dispose();
                                ////sql = null;

                                cm.ghilog("quanlyphikiemsoatchatluong",
                                          "Sửa giá trị hội viên cty (bằng cách sửa PhiID: " + idPhi + ") \"" +
                                          temcty.HoiVienTapTheID + " mức phí " + temcty.PhiKiemSoat +
                                          "\" vào danh mục tblTTDMPhiKSCLChiTiet");
                            }
                            catch
                            {
                                ////sql.Connection.Close();
                                ////sql.Connection.Dispose();
                                ////sql = null;
                            }
                        }
                    }
                }

                List<objNhanSurpt> lstNS = new List<objNhanSurpt>();
                if (Session["lstLuuNhanSu"] != null)
                    lstNS = Session["lstLuuNhanSu"] as List<objNhanSurpt>;

                if (lstNS.Count() != 0)
                {
                    foreach (var temcNS in lstNS)
                    {
                        sql = new SqlCommand();

                        sql.CommandText = "SELECT COUNT(*) FROM tblTTDMPhiKSCLChiTiet WHERE PhiID = " + idPhi +
                                          " AND HoiVienCaNhanID = " + temcNS.MaNhanSu;
                        DataSet ds2 = new DataSet();
                        ds2 = DataAccess.RunCMDGetDataSet(sql);

                        int IDCheck = Convert.ToInt32(ds2.Tables[0].Rows[0][0]);
                        //sql.Connection.Close();
                        //sql.Connection.Dispose();
                        //sql = null;
                        if (IDCheck == 0)
                        {
                            try
                            {
                                sql = new SqlCommand();
                                sql.CommandText = "INSERT INTO tblTTDMPhiKSCLChiTiet(PhiID, HoiVienCaNhanID, MucPhi) " +
                                                  " VALUES ('" + idPhi + "', '" + temcNS.MaNhanSu + "', '" +
                                                  strBoChamPhay(temcNS.PhiKiemSoat) + "')";

                                DataAccess.RunActionCmd(sql);
                                //sql.Connection.Close();
                                //sql.Connection.Dispose();
                                //sql = null;

                                cm.ghilog("quanlyphikiemsoatchatluong",
                                          "Thêm giá trị hội viên cá nhân (bằng cách sửa PhiID: " + idPhi + ") \"" +
                                          temcNS.MaNhanSu + " mức phí " + temcNS.PhiKiemSoat +
                                          "\" vào danh mục tblTTDMPhiKSCLChiTiet");
                            }
                            catch
                            {
                                //sql.Connection.Close();
                                //sql.Connection.Dispose();
                                //sql = null;
                            }
                        }
                        else
                        {
                            sql = new SqlCommand();
                            try
                            {
                                sql.CommandText = "UPDATE tblTTDMPhiKSCLChiTiet SET MucPhi = " +
                                                  Convert.ToDouble(strBoChamPhay(temcNS.PhiKiemSoat)) + " WHERE PhiID = " + idPhi +
                                                  " AND HoiVienCaNhanID = " + temcNS.MaNhanSu;

                                DataAccess.RunActionCmd(sql);
                                //sql.Connection.Close();
                                //sql.Connection.Dispose();
                                //sql = null;

                                cm.ghilog("quanlyphikiemsoatchatluong",
                                          "Sửa giá trị hội viên cá nhân (bằng cách sửa PhiID: " + idPhi + ") \"" +
                                          temcNS.MaNhanSu + " mức phí " + temcNS.PhiKiemSoat +
                                          "\" vào danh mục tblTTDMPhiKSCLChiTiet");
                            }
                            catch
                            {
                                //////sql.Connection.Close();
                                //////sql.Connection.Dispose();
                                //////sql = null;
                            }
                        }
                    }
                }
                transKSCL.Commit();
                try
                {
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    cm.ghilog("quanlyphikiemsoatchatluong",
                              "Sửa thông tin PhiID \"" + idPhi + "\" vào danh mục tblTTDMPhiKSCL");

                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                }
                catch (Exception)
                {
                    
                }
            }
            catch (Exception ex)
            {
                transKSCL.Rollback();
                ErrorMessage.Controls.Add(
                    new LiteralControl(
                        "<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" +
                        ex.Message.ToString() + "</div>"));
                return;
            }
        }
    }

    protected void ChonDanhSach_OnClick(object sender, EventArgs e)
    {

    }

    protected void grvphikiemsoatchatluong_OnDataBound(object sender, EventArgs e)
    {

    }

    protected void DanhSachKiemTraTrucTiep_OnTextChanged(object sender, EventArgs e)
    {
        //string[] danhsachmacongty = DanhSachKiemTraTrucTiep.Text.Split(',');
        //foreach (string i in danhsachmacongty)
        //{

        //}
        //UpdatePanel1.Triggers.Add(new AsyncPostBackTrigger()
        //{
        //    ControlID = grvphikiemsoatchatluong.UniqueID,
        //    //  EventName = "OnDataBound", // this may be optional
        //});

        //grvphikiemsoatchatluong.Columns[0].HeaderText = "STT";
        //grvphikiemsoatchatluong.Columns[0].SortExpression = "";
        //grvphikiemsoatchatluong.Columns[2].HeaderText = "Tên công ty kiểm toán và hội viên cá nhân";
        //grvphikiemsoatchatluong.Columns[2].SortExpression = "";
        //grvphikiemsoatchatluong.Columns[3].HeaderText = "Phí kiểm soát chất lượng";
        //grvphikiemsoatchatluong.Columns[3].SortExpression = "";

        int iHoiVienID = 0;
        try
        {
            //string a = HoiVienTapTheID.Value;
            string macongty = DanhSachKiemTraTrucTiep.Text;
            if (!string.IsNullOrWhiteSpace(macongty))
            {
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "SELECT TOP 1 HoiVienTapTheId, TenDoanhNghiep, MaHoiVienTapThe FROM tblHoiVienTapThe WHERE MaHoiVienTapThe like N'%" + macongty + "%'";
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtbTemp.Rows.Count > 0)
                {
                    DanhSachKiemTraTrucTiep.Text = dtbTemp.Rows[0]["MaHoiVienTapThe"].ToString();
                    iHoiVienID = Convert.ToInt32(dtbTemp.Rows[0]["HoiVienTapTheId"]);
                }
            }
        }
        catch
        { }
        if (iHoiVienID == 0)
        {
            return;
        }
        else
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT '1' as STT, HoiVienTapTheId, TenDoanhNghiep, MaHoiVienTapThe, '' as PhiKSCL FROM tblHoiVienTapThe WHERE HoiVienTapTheId = " + iHoiVienID;
            DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            //grvphikiemsoatchatluong.DataSource = dtb;
            //grvphikiemsoatchatluong.DataBind();
        }
    }

    protected void TimKiem_OnTextChanged(object sender, EventArgs e)
    {
        LoadDSNhanSuCTy(Id);
    }

    protected void rptCha_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "ThemMoiNS")
        {
            // id hoi vien nó sẽ lấy ở CommandArgument='<%#Eval("HoiVienTapTheID") %>'
            
            int HoiVienTapTheID = int.Parse(e.CommandArgument.ToString());
            
        }
    }

    public void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_luu' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_sua' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_xoa' href='#none' class='btn btn-rounded' onclick='xoa(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-sign'></i> Xóa</a>
            <a id='btn_duyet' href='#none' class='btn btn-rounded' onclick='duyet(" + Request.QueryString["id"] + @");'><i class='iconfa-ok'></i> Duyệt</a>
            <a id='btn_tuchoi' href='#none' class='btn btn-rounded' onclick='tuchoi(" + Request.QueryString["id"] + @");'><i class='iconfa-remove'></i> Từ chối</a>
        <a id='btn_thoaiduyet' href='#none' class='btn btn-rounded' onclick='thoaiduyet(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Thoái duyệt</a>";
        output_html += "</div>";
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    public void LoadNut_footer(int TT)
    {
        
        ////else
        ////{
        ////    Response.Write("$('#akickChon').attr('disabled', false);");
        ////    Response.Write("$('#btThemCongTy').attr('disabled', false);");
        ////    Response.Write("$('.idKickChon2').attr('disabled', false);");
        ////    Response.Write("$('.btThemTrongTB').attr('disabled', false);");
            
        ////}

        //if (Request.QueryString["tinhtrang"] == "xoa")
        //{
        //    if ((TT != Convert.ToInt32(EnumVACPA.TinhTrang.DaPheDuyet)))
        //    {
        //        bt.Enabled = false;
        //        btXoa.Enabled = true;
        //        btDuyet.Enabled = false;
        //        btTuChoi.Enabled = false;
        //        btThoaiDuyet.Enabled = false;
        //    }
        //    else
        //    {
        //        bt.Enabled = false;
        //        btXoa.Enabled = false;
        //        btDuyet.Enabled = false;
        //        btTuChoi.Enabled = false;
        //        btThoaiDuyet.Enabled = false;
        //    }
        //}
        //else
        //{
            
        //    if (TT == Convert.ToInt32(EnumVACPA.TinhTrang.ChoDuyet))
        //    {
        //        bt.Enabled = true;
        //        btXoa.Enabled = true;
        //        btDuyet.Enabled = true;
        //        btTuChoi.Enabled = true;
        //        btThoaiDuyet.Enabled = false;
        //    }
        //    else if (TT == Convert.ToInt32(EnumVACPA.TinhTrang.DaPheDuyet))
        //    {
        //        bt.Enabled = false;
        //        btXoa.Enabled = false;
        //        btDuyet.Enabled = false;
        //        btTuChoi.Enabled = false;
        //        btThoaiDuyet.Enabled = true;
        //    }
        //    else if (TT == Convert.ToInt32(EnumVACPA.TinhTrang.KhongPheDuyet) || TT == Convert.ToInt32(EnumVACPA.TinhTrang.ThoaiDuyet))
        //    {
        //        bt.Enabled = true;
        //        btXoa.Enabled = true;
        //        btDuyet.Enabled = false;
        //        btTuChoi.Enabled = false;
        //        btThoaiDuyet.Enabled = false;
        //    }
            
        //        //string mode = Request.QueryString["mode"];
        //        //if (mode == "daduyet")
        //        //{

        //        //    btXoa.Enabled = false;
        //        //    btDuyet.Enabled = false;
        //        //    btTuChoi.Enabled = false;
        //        //    btThoaiDuyet.Enabled = true;
        //        //}
        //        //else if (mode == "choduyet")
        //        //{
        //        //    btXoa.Enabled = true;
        //        //    btDuyet.Enabled = true;
        //        //    btTuChoi.Enabled = true;
        //        //    btThoaiDuyet.Enabled = false;
        //        //}
            
        //}
    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {
        if (Id !=0)
        {
            SqlCommand sql = new SqlCommand();

            try
            {
                sql.CommandText = "DELETE tblTTDMPhiKSCLChiTiet WHERE PhiID = " + Id;

                DataAccess.RunActionCmd(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                sql = new SqlCommand();
                sql.CommandText = "DELETE tblTTDMPhiKSCL WHERE PhiID = " + Id;

                DataAccess.RunActionCmd(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                cm.ghilog("quanlyphikiemsoatchatluong", "Xóa tblTTDMPhiKSCL với ID: " + Id + ") \"" + "" + "\" xóa trong danh mục tblTTDMPhiKSCL");
                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
            }
            catch
            {
                //sql.Connection.Close();
                //sql.Connection.Dispose();
                //sql = null;

                //Xử lý hiện thông báo không thành công
            }
        }
    }

    protected void btnDuyet_Click(object sender, EventArgs e)
    {
        if (Id != 0)
        {
            SqlCommand sql = new SqlCommand();

            try
            {
                
                sql.CommandText = "UPDATE tblTTDMPhiKSCL SET TinhTrangID = " + Convert.ToInt32(EnumVACPA.TinhTrang.DaPheDuyet) + ", NgayDuyet = '" + DateTime.Now + "', NguoiDuyet = '" + cm.Admin_TenDangNhap + "'" + " WHERE PhiID = " + Id;

                DataAccess.RunActionCmd(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                cm.ghilog("quanlyphikiemsoatchatluong", "Duyệt  tblTTDMPhiKSCL với ID: " + Id + ") \"" + "" + "\" duyệt trong danh mục tblTTDMPhiKSCL");
                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
            }
            catch
            { }
        }
    }

    protected void btnDuyet_OnClick(object sender, EventArgs e)
    {
        if (Id != 0)
        {
            SqlCommand sql = new SqlCommand();

            try
            {

                sql.CommandText = "UPDATE tblTTDMPhiKSCL SET TinhTrangID = " + Convert.ToInt32(EnumVACPA.TinhTrang.DaPheDuyet) + ", NgayDuyet = '" + DateTime.Now + "', NguoiDuyet = '" + cm.Admin_TenDangNhap + "'" + " WHERE PhiID = " + Id;

                DataAccess.RunActionCmd(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                cm.ghilog("quanlyphikiemsoatchatluong", "Duyệt  tblTTDMPhiKSCL với ID: " + Id + ") \"" + "" + "\" duyệt trong danh mục tblTTDMPhiKSCL");
                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
            }
            catch
            { }
        }
    }

    protected void btnTuChoi_Click(object sender, EventArgs e)
    {
        if (Id != 0)
        {
            SqlCommand sql = new SqlCommand();

            try
            {

                sql.CommandText = "UPDATE tblTTDMPhiKSCL SET TinhTrangID = " + Convert.ToInt32(EnumVACPA.TinhTrang.KhongPheDuyet) + " WHERE PhiID = " + Id;

                DataAccess.RunActionCmd(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                cm.ghilog("quanlyphikiemsoatchatluong", "Từ chối duyệt  tblTTDMPhiKSCL với ID: " + Id + ") \"" + "" + "\" không phê duyệt trong danh mục tblTTDMPhiKSCL");
                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
            }
            catch
            { }
        }
    }

    protected void btnThoaiDuyet_Click(object sender, EventArgs e)
    {
        if (Id != 0)
        {
            SqlCommand sql = new SqlCommand();

            try
            {

                sql.CommandText = "UPDATE tblTTDMPhiKSCL SET TinhTrangID = " + Convert.ToInt32(EnumVACPA.TinhTrang.ThoaiDuyet) + ", NgayDuyet = NULL, NguoiDuyet = NULL WHERE PhiID = " + Id;

                DataAccess.RunActionCmd(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;

                cm.ghilog("quanlyphikiemsoatchatluong", "Thoái duyệt  tblTTDMPhiKSCL với ID: " + Id + ") \"" + "" + "\" thoái duyệt trong danh mục tblTTDMPhiKSCL");
                Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
            }
            catch
            { }
        }
    }

    protected string strBoChamPhay(string p)
    {
        int leng = p.Length;
        if (p.LastIndexOf(',') != -1)
            leng = p.LastIndexOf(',');
        string strOut = p.Substring(0, leng);
        strOut = strOut.Replace(".", string.Empty);
        return strOut;
    }

    protected void btnXoaHoiVienCaNhan_OnClick(object sender, EventArgs e)
    {
        List<int> lstIdChon = new List<int>();
        if (Session["lstIdChon"] != null)
            lstIdChon = Session["lstIdChon"] as List<int>;
        int HoiVienBiXoa = 0;
        try
        {
            HoiVienBiXoa = Convert.ToInt32(HoiVienCaNhanIdBiXoa.Value);
            if (lstIdChon.Contains(HoiVienBiXoa))
                lstIdChon.Remove(HoiVienBiXoa);
            Session["lstIdChon"] = lstIdChon;
            LoadDSNhanSuCTy(0);
        }
        catch (Exception)
        {
        }
    }
}