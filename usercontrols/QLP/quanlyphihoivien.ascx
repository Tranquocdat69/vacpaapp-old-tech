﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="quanlyphihoivien.ascx.cs" Inherits="usercontrols_quanlyphihoivien" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 152px;
    }
    .style2
    {
        width: 30%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }

</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_quanlyphihoivien" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_quanlyphihoivien" clientidmode="Static" runat="server"   method="post">
    <h4 class="widgettitle"><%=tenchucnang%></h4>
    <div id="thongbaoloi_form_quanlyphihoivien" name="thongbaoloi_form_quanlyphihoivien" style="display:none" class="alert alert-error"></div>
        <table id="tblThongBao" width="100%" border="0" class="formtbl">
        <tr>
         <td width = "50%">
         <div ID="TinhTrangBanGhi" name = "TinhTrangBanGhi"></div>
        </td>
         <td width = "50%">
        <div ID="TinhTrangHieuLuc" name = "TinhTrangHieuLuc"></div>
        </td>
        </tr>
        </table>
        <div><br /><b> Thông tin phí</b></div>
      <table id="Table1" width="100%" border="0" class="formtbl" >
      <tr>
          <td class="style1"><asp:Label ID="lblMaPhi" runat="server" Text = 'Mã phí:' ></asp:Label></td>
        <td class="style2" ><input type="text" name="MaPhi" id="MaPhi" disabled = "disabled" ></td>
        <td>&nbsp;</td>
        <td class="style1" ><asp:Label ID="lblNgayLap" runat="server" Text = 'Ngày lập:' ></asp:Label>
            <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td colspan ="3" style ="width:35%"><input type="text" name="NgayLap" id="NgayLap"/></td>
      </tr>
       <tr>
          <td class="style1"><asp:Label ID="Label6" runat="server" Text = 'Tên phí:' ></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td colspan ="6"><input size = "100%" type="text" name="TenPhi" id="TenPhi"></td>
      </tr>
       
      <tr>
          <td class="style1"><asp:Label ID="Label7" runat="server" Text = 'Ngày áp dụng:' ></asp:Label>
            <asp:Label ID="Label9" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2"><input type="text" name="NgayApDung" id="NgayApDung"/></td>
         <td>&nbsp;</td>
        <td class="style1"><asp:Label ID="Label10" runat="server" Text = 'Ngày hết hiệu lực:' ></asp:Label></td>
        <td colspan ="3" class ="style2"><input type="text" name="NgayHetHieuLuc" id="NgayHetHieuLuc"/></td>

      </tr>

      <tr>
          <td class="style1"><asp:Label ID="lblDoiTuongApDung" runat="server" Text ='Đối tượng áp dụng:' ></asp:Label>
          </td>
          <td>
          <select id="selLoaiHoiVien" name="selLoaiHoiVien" onchange="changeselLoaiHoiVien()">
              <option value="0" selected>Hội viên cá nhân</option>
              <option value="1">Hội viên tổ chức</option>
          </select>
              
          </td>
          <td colspan ="5">
              <select id="selLoaiHoiVienChiTiet" name="selLoaiHoiVienChiTiet">
              <option value="-2">- Hội viên chính thức</option>                    
                        <option value="0" selected="selected">++ Hội viên chính thức</option>                       
                        <option value="2">++ Hội viên cao cấp</option>                       
                        <option value="4">++ Hội viên làm việc chuyên trách tại các Văn phòng VACPA</option>
                        <option value="5">++ Hội viên đã nghỉ hưu và không làm việc tại một công ty nào khác</option>
                        <option value="6">++ Hội viên đang làm việc tại cơ quan quản lý Nhà nước có liên quan đến nghề nghiệp, hoạt động của VACPA</option>
                        
                    <option value="1">- Hội viên liên kết</option>
                    <option value="3">- Hội viên danh dự</option>
          </select>
          </td>
      </tr>
          <tr>
          <td class="style1">
          </td>
          <td>
          <select name="DoiTuongApDung" id="DoiTuongApDung">
          <%  
                  LoadDoiTuong();
              %>
          </select>
          </td>
          <td colspan ="5">
              
          </td>
      </tr>
          <tr>
          <td class="style1"><asp:Label ID="Label5" runat="server" Text ='Mức phí chi tiết:' ></asp:Label>
          </td>
          <td colspan="6">
              <div style="margin-bottom:5px">
                  <input type="hidden" id="somucphi" name="somucphi" value="1" />
                                        <a id="ctl00_Button1" class="btn btn-rounded" href="javascript: add_mucphi()">
                                           <i class="iconfa-plus-sign"></i> Thêm</a></div>
                                    <div>
                                        <table class="table table-bordered responsive dyntable" rules="all" id="dsmucphi"
                                            style="border-collapse: collapse;" border="1" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <th scope="col">
                                                        STT
                                                    </th>
                                                    <th scope="col">
                                                        Thời gian từ
                                                    </th>
                                                    <th scope="col">
                                                        Đến
                                                    </th>
                                                    <th scope="col">
                                                        Mức phí (%)
                                                    </th>
                                                    
                                                    <th scope="col" style="width:65px">
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                                <% loadphichitiet(); %>
                                                
                                            </tbody>
                                        </table>
                                        <br>
                                    </div>
          </td>
              </tr>
      <tr>
        <td class="style1"><asp:Label ID="Label3" runat="server" Text = 'Mức phí quy định:' ></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2"><input type="text" name="MucPhi" id="MucPhi" class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="999999999999999999"></td>
        <td class ="style3"><asp:Label ID="Label11" runat="server" Text = 'VNĐ' ></asp:Label>
        </td>
        <td colspan ="4">&nbsp;</td>
        
      </tr>

       <tr>
          <td class="style1"><asp:Label ID="Label12" runat="server" Text = 'Đơn vị thời gian tính phí:' ></asp:Label>
            <asp:Label ID="Label13" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2">
         <select name="DonViThoiGian" id="DonViThoiGian">
          <%  
                  LoadThoiGian();
              %>
          </select></td>
        <td>&nbsp;</td>
        <td class="style1"><asp:Label ID="Label15" runat="server" Text = 'Thời hạn nộp phí: Ngày'></asp:Label>
        <asp:Label ID="Label2" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style4"> <select name="NgayNop" id="NgayNop">
          <%  
                  LoadNgayNop31();
              %>
          </select></td>
        <td class="style4"><asp:Label ID="Label16" runat="server" Text = 'Tháng:'></asp:Label></td>
        <td class="style4"> <select name="ThangNop" id="ThangNop"/>
          <%  
                  LoadThangNop();
              %>
          </td>
      </tr>
      
      </table>
       <%LoadNut(); %>
    </form>

                                                    
<script type="text/javascript">
    function changeselLoaiHoiVien() {
        $('#selLoaiHoiVienChiTiet').empty();
        if ($('#selLoaiHoiVien').val() == "0")
        {
            $('#selLoaiHoiVienChiTiet').append('<option value="-2">- Hội viên chính thức</option>');
            $('#selLoaiHoiVienChiTiet').append('<option value="0" selected="selected">++ Hội viên chính thức</option>');
            $('#selLoaiHoiVienChiTiet').append('<option value="2">++ Hội viên cao cấp</option>');
            $('#selLoaiHoiVienChiTiet').append('<option value="4">++ Hội viên làm việc chuyên trách tại các Văn phòng VACPA</option>');
            $('#selLoaiHoiVienChiTiet').append('<option value="5">++ Hội viên đã nghỉ hưu và không làm việc tại một công ty nào khác</option>');
            $('#selLoaiHoiVienChiTiet').append('<option value="6">++ Hội viên đang làm việc tại cơ quan quản lý Nhà nước có liên quan đến nghề nghiệp, hoạt động của VACPA</option>');
            $('#selLoaiHoiVienChiTiet').append('<option value="1">- Hội viên liên kết</option>');
            $('#selLoaiHoiVienChiTiet').append('<option value="3">- Hội viên danh dự</option>');
        }
        else
        {
            $('#selLoaiHoiVienChiTiet').append('<option value="0" selected>Hội viên chính thức</option>');
            $('#selLoaiHoiVienChiTiet').append('<option value="1">Hội viên liên kết</option>');
        }
        return false;
    }

    var sodong = 1;

    function add_mucphi() {
        sodong++;
        $('#dsmucphi > tbody:last').append("<tr id=\"dong_" + sodong + "\"><td align=\"center\" valign=\"middle\"></td><td >    <input name=\"TuNgay_" + sodong + "\" id=\"TuNgay_" + sodong + "\" style=\"width:200px;\" type=\"text\" />    </td><td  > <input name=\"DenNgay_" + sodong + "\" id=\"DenNgay_" + sodong + "\"  type=\"text\" /> </td><td > <input name=\"MucPhi_" + sodong + "\" id=\"MucPhi_" + sodong + "\"  type=\"text\"  style=\"width:80px;\" onkeypress=\"return NumberOnly()\"> </td><td align=\"center\">  <a href=\"#none\" class=\"btn btn-rounded\" onclick=\"if (confirm ('Xác nhận xóa dòng này?')) {sodong--; $('#dong_" + sodong + "').remove(); danhsodong();  }\"><i class=\"iconfa-remove-sign\"></i> Xóa</a> </td></tr>");

        $("#somucphi").val(parseInt($("#somucphi").val()) + 1);
        danhsodong();

        $('#TuNgay_' + sodong + ', #DenNgay_' + sodong).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-75:+25'
        });
    }

    function danhsodong() {
        idx = -1;
        $("#dsmucphi tr").each(function () {
            idx++;
            if (idx != 0) $(this).children(":eq(0)").html(idx);
        });

    }

        function submitform() {        
                jQuery("#form_quanlyphihoivien").submit();
        }

        function NumberOnly() {
            var AsciiValue = event.keyCode
            if ((AsciiValue >= 48 && AsciiValue <= 57) || (AsciiValue == 8 || AsciiValue == 127 || AsciiValue == 44))
                event.returnValue = true;
            else
                event.returnValue = false;
        }

        jQuery("#form_quanlyphihoivien").validate({
            rules: {
                NgayLap: {
                    required: true
                },
                TenPhi: {
                    required: true
                },
                NgayApDung: {
                    required: true
                },
                DoiTuongApDung: {
                    required: true
                },
                MucPhi: {
                    required: true
                },
                DonViThoiGian: {
                    required: true
                },
                NgayNop: {
                    required: true
                },
            },
            invalidHandler: function (f, d) {
                var g = d.numberOfInvalids();

                if (g) {
                    var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                    jQuery("#thongbaoloi_form_quanlyphihoivien").html(e).show()

                } else {
                    jQuery("#thongbaoloi_form_quanlyphihoivien").hide()
                }
            }
        });


    $('input:radio[name="LoaiHoiVien"]').change(
    function(){
    var vGioiTinh = document.getElementById('LoaiHoiVien');
        if ($(this).is(':checked') && $(this).val() == 'CN') {
            vGioiTinh.value = 'CN';
        }
        else if ($(this).is(':checked') && $(this).val() == 'TT') {
            vGioiTinh.value = 'TT';
        }
    });


    $.datepicker.setDefaults($.datepicker.regional['vi']);

</script>

<script type="text/javascript">

    function luu() {
        jQuery("#form_quanlyphihoivien").submit();
    };

    function xoa(id) {
        window.location = 'admin.aspx?page=quanlyphihoivien&id=' + id + '&mode=view&tinhtrang=xoa';

    };

    function duyet(id) {
        window.location = 'admin.aspx?page=quanlyphihoivien&id=' + id + '&mode=view&tinhtrang=duyet';
    };

    function tuchoi(id) {

        window.location = 'admin.aspx?page=quanlyphihoivien&id=' + id + '&mode=view&tinhtrang=tuchoi';

    };

    function thoaiduyet(id) {
        window.location = 'admin.aspx?page=quanlyphihoivien&id=' + id + '&mode=view&tinhtrang=thoaiduyet';

    };

    function capnhatphatsinhphi(id) {
        window.location = 'admin.aspx?page=quanlyphihoivien&id=' + id + '&mode=view&tinhtrang=capnhatphatsinhphi';

    };

</script>
<script type="text/javascript">
    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
        $('#TuNgay_1, #DenNgay_1').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-75:+25'
        });
    });
      
    <%LoadThongTin(); %>

    var sodong = $('#dsmucphi tr').length - 1;
    $("#somucphi").val(sodong);
</script>
<script type="text/javascript">
    <%AnNut(); %>  
</script>
<script type="text/javascript">
    <%DoiNgay(); %>
</script>

