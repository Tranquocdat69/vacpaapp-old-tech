﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="danhsachoiviencanhan.ascx.cs" Inherits="usercontrols_danhsachoiviencanhan" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript">
</script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

    <%--<form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">
      <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
           <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  ChildrenAsTriggers = "true"  >
       <ContentTemplate>
            <table id="Table1" border="0" class="formtbl">
     <tr style ="width: 586px">
        <td style="width:180px"><label>Họ và tên: </label></td>
        <td  style="width:380px"><input type="text" name="timkiem_Ten" id="timkiem_Ten" value="<%=Request.Form["timkiem_Ten"]%>"  class="input-xlarge" />
        
        </td>
    </tr>
     <tr style ="width: 586px">
        <td style="width:180px"><label>Mã hội viên: </label></td>
        <td  style="width:380px"><input type="text" name="timkiem_Ma" id="timkiem_Ma" value="<%=Request.Form["timkiem_Ma"]%>"  class="input-xlarge" />
        
        </td>
    </tr>
         <td colspan="2"><div style="text-align:center;">
 <asp:LinkButton ID="btnTimKiem" runat="server" CssClass="btn" 
        onclick="btnTimKiem_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton></div></td>
      </table>
       <asp:GridView ClientIDMode="Static" ID="grvDanhSach" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="False" AllowSorting="True" 
           Width ="586px"
            DataKeyNames = "HoiVienCaNhanID"
       >
          <Columns>
                    <asp:TemplateField ItemStyle-Width="30px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkEmp" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmp_CheckedChanged" ></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
               <asp:TemplateField  >
                  <HeaderStyle Width="120px" />
                  <ItemTemplate>
                      <span><%# Eval("MaHoiVienCaNhan")%></span>
                  </ItemTemplate>
              </asp:TemplateField>
        
              <asp:TemplateField  >
                  <HeaderStyle Width="280px" />
                  <ItemTemplate>
                      <span><%# Eval("HoVaTen")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval("SoChungChiKTV")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</ContentTemplate>
      </asp:UpdatePanel>     
    </form>

    <script type="text/javascript">
        function close() {
            $(this).closest(".ui-dialog-content").dialog("close");

        }
</script>