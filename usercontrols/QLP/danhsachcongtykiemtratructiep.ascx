﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="danhsachcongtykiemtratructiep.ascx.cs" Inherits="usercontrols_danhsachcongtykiemtratructiep" %>
<%@ Import Namespace="Org.BouncyCastle.Asn1.Ocsp" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
</script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

    <%--<form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">
      <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
           <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  ChildrenAsTriggers = "true"  >
       <ContentTemplate>
            <table id="Table1" border="0" class="formtbl">
     
     <tr style ="width: 586px">
        <td style="width:180px"><label>Mã danh sách: </label></td>
        <td  style="width:380px"><input type="text" name="timkiem_Ma" id="timkiem_Ma" value="<%=Request.Form["timkiem_Ma"]%>"  class="input-xlarge" />
        </td>
    </tr>
       <td colspan="2"><div style="text-align:center;">
 <asp:LinkButton ID="btnTimKiem" runat="server" CssClass="btn" 
        onclick="btnTimKiem_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton></div></td>
      </table>
       <asp:GridView ClientIDMode="Static" ID="grvDanhSach" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="False" AllowSorting="True" 
           Width ="586px"
       >
          <Columns>
   <asp:TemplateField  >
                  <ItemTemplate>
                        <span><%# Container.DataItemIndex + 1%></span>
                  </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField >
                       <HeaderStyle Width="140px" />
              <ItemTemplate>
                 <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:parent.chondanhsach('" + DataBinder.Eval(Container.DataItem, "DSKiemTraTrucTiepID").ToString() +"','" + DataBinder.Eval(Container.DataItem, "MaDanhSach").ToString() + "'); "%>   Text= '<%# Eval("MaDanhSach") %>'  runat="server">
                    
                </asp:HyperLink>
        
             </ItemTemplate>
             <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
             <ItemStyle   HorizontalAlign="Center"     />
          </asp:TemplateField>

              <asp:BoundField DataField="NgayLap"
                            ReadOnly="True" 
                            DataFormatString="{0:dd/MM/yyyy}" />
              
              <asp:TemplateField  >
                <ItemTemplate>
                   <a onclick="parent.chondanhsach('<%# Eval("DSKiemTraTrucTiepID") %>','<%# Eval("MaDanhSach")%>');" data-placement='top' data-rel='tooltip' href='#none' data-original-title='Chọn'  rel='tooltip' class='btn'><i class='iconsweets-trashcan2' ></i></a>
                </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</ContentTemplate>
      </asp:UpdatePanel>     
    </form>

    <script type="text/javascript">
        function close() {
            $(this).closest(".ui-dialog-content").dialog("close");

        }
</script>