﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="danhsachlophoc.ascx.cs" Inherits="usercontrols_danhsachlophoc" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript">
</script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

    <%--<form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">
      <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
           <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  ChildrenAsTriggers = "true"  >
       <ContentTemplate>
            <table id="Table1" border="0" class="formtbl">
     <tr style ="width: 446px">
        <td style="width:140px"><label>Tên lớp học: </label></td>
        <td  style="width:280px"><input type="text" name="timkiem_Ten" id="timkiem_Ten" value="<%=Request.Form["timkiem_Ten"]%>"  class="input-xlarge" />
        
        </td>
    </tr>
     <tr style ="width: 446px">
        <td style="width:140px"><label>Mã lớp học: </label></td>
        <td  style="width:280px"><input type="text" name="timkiem_Ma" id="timkiem_Ma" value="<%=Request.Form["timkiem_Ma"]%>"  class="input-xlarge" />
        
        </td>
    </tr>
        <td colspan="2"><div style="text-align:center;">
 <asp:LinkButton ID="btnTimKiem" runat="server" CssClass="btn" 
        onclick="btnTimKiem_Click"><i class="iconfa-search">
    </i>Tìm kiếm</asp:LinkButton></div></td>
      </table>
       <asp:GridView ClientIDMode="Static" ID="grvDanhSach" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="False" AllowSorting="True" 
           Width ="446px"
       >
          <Columns>

              <asp:TemplateField >
                       <HeaderStyle Width="140px" />
              <ItemTemplate>
                 <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:parent.chonlophoc('" + DataBinder.Eval(Container.DataItem, "LopHocID").ToString() +"','" + DataBinder.Eval(Container.DataItem, "MaLopHoc").ToString() + "'); "%>   Text= '<%# Eval("MaLopHoc") %>'  runat="server">
                    
                </asp:HyperLink>
        
             </ItemTemplate>
             <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
             <ItemStyle   HorizontalAlign="Center"     />
          </asp:TemplateField>


              <asp:TemplateField  >
                  <HeaderStyle Width="250px" />
                  <ItemTemplate>
                      <span><%# Eval("TenLopHoc")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              
              <asp:TemplateField  >
                <ItemTemplate>
                   <a onclick="parent.chonhoivien('<%# Eval("LopHocID") %>','<%# Eval("MaLopHoc")%>');" data-placement='top' data-rel='tooltip' href='#none' data-original-title='Chọn'  rel='tooltip' class='btn'><i class='iconsweets-trashcan2' ></i></a>
                </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</ContentTemplate>
      </asp:UpdatePanel>     
    </form>

    <script type="text/javascript">
        function close() {
            $(this).closest(".ui-dialog-content").dialog("close");

        }
</script>