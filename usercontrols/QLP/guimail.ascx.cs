﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using HiPT.VACPA.DL;
using System.Net.Mail;
using System.IO;

public partial class usercontrols_guimail : System.Web.UI.UserControl
{
    Commons cm = new Commons();
    string sub = "";
    string body = "";
    string mess = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //lstid = Request.QueryString["email"].Split(',');
            //sub = Request.QueryString["sub"];
            //body = Request.QueryString["body"];
            //mess = "";
            string module = Request.QueryString["module"];
            switch (module)
            {
                case "thongbaonophoiphi":
                    v_guimailthongbaonophoiphi();
                    break;
                case "thongbaonopphicnkt":
                    v_guimailthongbaonopphicnkt();
                    break;
                case "thongbaonopphikscl":
                    v_guimailthongbaonopphikscl();
                    break;
                case "thongbaonopphidanglogo":
                    v_guimailthongbaonopphidanglogo();
                    break;
            }
        }
        catch (Exception ex)
        { }

    }

    protected void v_guimailthongbaonophoiphi()
    {
        try
        {
            sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

            mess = "";
            SqlCommand sql = new SqlCommand();
            //int PhiIDCaNhan = 0;
            //string strPhiIdCaNhan = Request.QueryString["phiidcanhan"];
            //if (!string.IsNullOrEmpty(Request.QueryString["phiidcanhan"]))
            //{
            //    try
            //    {
            //        PhiIDCaNhan = Convert.ToInt32(Request.QueryString["phiidcanhan"]);
            //    }
            //    catch
            //    {
            //        PhiIDCaNhan = 0;
            //    }
            //}
            string strThoiHanCaNhan = Request.QueryString["thoihancanhan"];

            string dsmailcanhan = "";
            if (string.IsNullOrEmpty(Request.QueryString["emailcanhan"]))
                dsmailcanhan = "0";
            else
                dsmailcanhan = Request.QueryString["emailcanhan"];
            //sql.CommandText = "SELECT DISTINCT EMAIL FROM TBLHOIVIENCANHAN WHERE HOIVIENCANHANID IN (" + Request.QueryString["emailcanhan"] + ")";
			string[] lstPhiID_CN = Request.QueryString["phiidcanhan"].Split(';');
            int k1 = 0;
			DataTable dtb = new DataTable();
			foreach (string PhiIDCaNhan in lstPhiID_CN)
            {
                sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_CaNhan]" + PhiIDCaNhan + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", '','" + dsmailcanhan + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet;				
				if (k1 == 0)
                    dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                else
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
				k1++;
			}
            if (dtb.Rows.Count > 0)
            {
                for (int i = 0; i < dtb.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
                    {
                        string HoiPhiPhaiNopNamNay = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["HoiPhiPhaiNopNamNay"]));
                        string HoiPhiConNoNamTruoc = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["HoiPhiConNoNamTruoc"]));
                        string TongHoiPhiPhaiNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiPhaiNop"]));
                        string TongHoiPhiDaNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiDaNop"]));
                        string TongHoiPhiConPhaiNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiConPhaiNop"]));

                        body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
                        body += @"VACPA xin thông báo đến anh/chị tình hình thanh toán phí như sau: <br>
<b>Loại phí: Hội phí</b>  <br>
-	Số phí phải nộp năm nay: " + HoiPhiPhaiNopNamNay + @"<br>
-	Số phí còn nợ năm trước: " + HoiPhiConNoNamTruoc + @"<br>
-	Tổng số phí phải nộp: " + TongHoiPhiPhaiNop + @"<br>
-	Số phí đã nộp: " + TongHoiPhiDaNop + @"<br>
-	Số phí còn phải nộp: " + TongHoiPhiConPhaiNop + @"<br>
Mời anh/chị thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHanCaNhan + "<br>";

                        body += @"Phương thức nộp Hội phí:<br>
 + Chuyển khoản:<br>
             Người thụ hưởng: Hội Kiểm toán viên hành nghề Việt Nam<br>
             Số TK: 117000011010 Tại Ngân hàng VietinBank Hai Bà Trưng Hà Nội<br>
+ Hoặc nộp tiền mặt tại VP VACPA Hà Nội hoặc VP VACPA Tp. Hồ Chí Minh";
                        SmtpMail.Send(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, ref mess);
                        cm.ghilog("ThongBaoNopHoiPhi", mess);
                    }
                }
            }

            sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

            mess = "";
            sql = new SqlCommand();
            //int PhiIDTapThe = 0;
            //string strPhiIdTapThe = Request.QueryString["phiidtapthe"];
            //if (!string.IsNullOrEmpty(Request.QueryString["phiidtapthe"]))
            //{
            //    try
            //    {
            //        PhiIDTapThe = Convert.ToInt32(Request.QueryString["phiidtapthe"]);
            //    }
            //    catch
            //    {
            //        PhiIDTapThe = 0;
            //    }
            //}
            string strThoiHanTapThe = Request.QueryString["thoihantapthe"];

            string dsmailtapthe = "";
            if (string.IsNullOrEmpty(Request.QueryString["emailtapthe"]))
                dsmailtapthe = "0";
            else
                dsmailtapthe = Request.QueryString["emailtapthe"];
            //sql.CommandText = "SELECT DISTINCT EMAIL FROM TBLHOIVIENCANHAN WHERE HOIVIENCANHANID IN (" + Request.QueryString["emailcanhan"] + ")";
            string[] lstPhiID = Request.QueryString["phiidtapthe"].Split(';');
            int k = 0;
            foreach (string PhiIDTapThe in lstPhiID)
            {
                sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_TapThe]" + PhiIDTapThe + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", '','" + dsmailtapthe + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                if (k == 0)
                    dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                else
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_TapThe]" + PhiIDTapThe + ", " + (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", '','" + dsmailtapthe + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                k++;
            }
            if (dtb.Rows.Count > 0)
            {
                for (int i = 0; i < dtb.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
                    {
                        string HoiPhiPhaiNopNamNay = (!string.IsNullOrEmpty(dtb.Rows[i]["HoiPhiPhaiNopNamNay"].ToString())) ? string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["HoiPhiPhaiNopNamNay"])) : "0";
                        string HoiPhiConNoNamTruoc = (!string.IsNullOrEmpty(dtb.Rows[i]["HoiPhiConNoNamTruoc"].ToString())) ? string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["HoiPhiConNoNamTruoc"])) : "0";
                        string TongHoiPhiPhaiNop = (!string.IsNullOrEmpty(dtb.Rows[i]["TongHoiPhiPhaiNop"].ToString())) ? string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiPhaiNop"])) : "0";
                        string TongHoiPhiDaNop = (!string.IsNullOrEmpty(dtb.Rows[i]["TongHoiPhiDaNop"].ToString())) ? string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiDaNop"])) : "0";
                        string TongHoiPhiConPhaiNop = (!string.IsNullOrEmpty(dtb.Rows[i]["TongHoiPhiConPhaiNop"].ToString())) ? string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongHoiPhiConPhaiNop"])) : "0";

                        string strIdHoiVien = dtb.Rows[i]["HoiVienTapTheID"].ToString();

                        sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_CaNhanCuaTapThe]" +
                             (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + "," +
                             (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + strIdHoiVien;
                        DataTable dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                        sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_CaNhanCuaTapThe]" +
                                          (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + "," +
                                          (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + strIdHoiVien;
                        dtbtemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                        double douPhiPhaiNopNamNay = 0;
                        double douPhiConNo = 0;
                        double douPhiPhaiNop = 0;
                        double douPhiDaNop = 0;
                        double douPhiConPhaiNop = 0;
                        foreach (DataRow dtr in dtbtemp.Rows)
                        {
                            if (!string.IsNullOrEmpty(dtr["HoiPhiPhaiNopNamNay"].ToString()))
                                douPhiPhaiNopNamNay += Convert.ToDouble(dtr["HoiPhiPhaiNopNamNay"]);
                            if (!string.IsNullOrEmpty(dtr["HoiPhiConNoNamTruoc"].ToString()))
                                douPhiConNo += Convert.ToDouble(dtr["HoiPhiConNoNamTruoc"]);
                            if (!string.IsNullOrEmpty(dtr["TongHoiPhiPhaiNop"].ToString()))
                                douPhiPhaiNop += Convert.ToDouble(dtr["TongHoiPhiPhaiNop"]);
                            if (!string.IsNullOrEmpty(dtr["TongHoiPhiDaNop"].ToString()))
                                douPhiDaNop += Convert.ToDouble(dtr["TongHoiPhiDaNop"]);
                            if (!string.IsNullOrEmpty(dtr["TongHoiPhiConPhaiNop"].ToString()))
                                douPhiConPhaiNop += Convert.ToDouble(dtr["TongHoiPhiConPhaiNop"]);
                        }

                        string strPhiPhaiNopNamNay = string.Format("{0:n0}", douPhiPhaiNopNamNay);
                        string strPhiConNo = string.Format("{0:n0}", douPhiConNo);
                        string strPhiPhaiNop = string.Format("{0:n0}", douPhiPhaiNop);
                        string strPhiDaNop = string.Format("{0:n0}", douPhiDaNop);
                        string strPhiConPhaiNop = string.Format("{0:n0}", douPhiConPhaiNop);

                        body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
                        body += @"VACPA xin thông báo đến quý công ty tình hình thanh toán phí như sau: <br>
<b>Loại phí: Hội phí HVTC:</b>  <br>
-	Số phí phải nộp năm nay: " + HoiPhiPhaiNopNamNay + @" đồng<br>
-	Số phí còn nợ năm trước: " + HoiPhiConNoNamTruoc + @" đồng<br>
-	Tổng số phí phải nộp: " + TongHoiPhiPhaiNop + @" đồng<br>
-	Số phí đã nộp: " + TongHoiPhiDaNop + @" đồng<br>
-	Số phí còn phải nộp: " + TongHoiPhiConPhaiNop + @" đồng<br>
<b>Loại phí: Hội phí HVCN </b>(Chi tiết xem trong file đính kèm)<br>
-	Số phí phải nộp năm nay: " + strPhiPhaiNopNamNay + @" đồng<br>
-	Số phí còn nợ năm trước: " + strPhiConNo + @" đồng<br>
-	Tổng số phí phải nộp: " + strPhiPhaiNop + @" đồng<br>
-	Số phí đã nộp: " + strPhiDaNop + @" đồng<br>
-	Số phí còn phải nộp: " + strPhiConPhaiNop + @" đồng<br>
Mời quý công ty thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHanTapThe + "<br>";

                        body += @"Phương thức nộp Hội phí:<br>
 + Chuyển khoản:<br>
             Người thụ hưởng: Hội Kiểm toán viên hành nghề Việt Nam<br>
             Số TK: 117000011010 Tại Ngân hàng VietinBank Hai Bà Trưng Hà Nội<br>
+ Hoặc nộp tiền mặt tại VP VACPA Hà Nội hoặc VP VACPA Tp. Hồ Chí Minh";

                        string strTenFile = dtb.Rows[i]["MaHoiVienTapThe"].ToString().TrimEnd() + "_HV";
                        byte[] data = GetDataHoiVienCaNhan(strIdHoiVien, (int)EnumVACPA.LoaiPhi.PhiHoiVien);
                        MemoryStream ms = new MemoryStream(data);
                        Attachment at = new Attachment(ms, strTenFile + ".xls", MediaTypeNames.Text.Html);
                        SmtpMail.SendWithAttach(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, at, ref mess);
                        cm.ghilog("ThongBaoNopHoiPhi", mess);
                    }
                }
            }
            cm.ghilog("ThongBaoNopHoiPhi", "Gửi mail thông báo nộp phí hội viên thành công");
            Response.Redirect("admin.aspx?page=thongbaonophoiphi&thongbao=thanhcong", false);
        }
        catch (Exception ex)
        {
            cm.ghilog("ThongBaoNopHoiPhi", "Lỗi xảy ra khi gửi mail thông báo nộp phí hội viên: " + ex.ToString());
            Response.Redirect("admin.aspx?page=thongbaonophoiphi&thongbao=loi", false);
        }
    }

    protected void v_guimailthongbaonopphicnkt()
    {
        try
        {
            sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

            mess = "";
            SqlCommand sql = new SqlCommand();
            int PhiIDCaNhan = 0;
            string strPhiIdCaNhan = Request.QueryString["phiidcanhan"];
            if (!string.IsNullOrEmpty(Request.QueryString["phiidcanhan"]))
            {
                try
                {
                    PhiIDCaNhan = Convert.ToInt32(Request.QueryString["phiidcanhan"]);
                }
                catch
                {
                    PhiIDCaNhan = 0;
                }
            }
            string strNgayLap = "";
            string strNgayLap1 = "";
            if (!string.IsNullOrEmpty(Request.QueryString["ngaylap"]))
            {
                strNgayLap = DateTime.ParseExact(Request.QueryString["ngaylap"], "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
                strNgayLap1 = Request.QueryString["ngaylap"];
            }

            string strThoiHan = Request.QueryString["thoihan"];

            string dsmailcanhan = "";
            if (string.IsNullOrEmpty(Request.QueryString["emailcanhan"]))
                dsmailcanhan = "0";
            else
                dsmailcanhan = Request.QueryString["emailcanhan"];
            //sql.CommandText = "SELECT DISTINCT EMAIL FROM TBLHOIVIENCANHAN WHERE HOIVIENCANHANID IN (" + Request.QueryString["emailcanhan"] + ")";
            string LopHocID = Request.QueryString["lophocid"].ToString();

            string TenLopHoc = "";

            try
            {
                sql.CommandText = "SELECT TenLopHoc from tblCNKTLopHoc where LopHocID = " + LopHocID;
                TenLopHoc = DataAccess.RunCMDGetDataSet(sql).Tables[0].Rows[0][0].ToString();
            }
            catch (NullReferenceException ex)
            {

            }

            if (!string.IsNullOrEmpty(strNgayLap) && !string.IsNullOrEmpty(LopHocID))
            {
                sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '','" + dsmailcanhan + "','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + LopHocID;
                DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '','" + dsmailcanhan + "','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + LopHocID;
                dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '','" + dsmailcanhan + "','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + LopHocID;
                dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                if (dtb.Rows.Count > 0)
                {
                    for (int i = 0; i < dtb.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
                        {
                            string HoiPhiConNoNamTruoc = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["PhiCNKTConNoNamTruoc"]));
                            body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
                            body += @"VACPA xin thông báo đến anh/chị tình hình thanh toán phí như sau: <br>
<b>Loại phí: Phí CNKT Lớp :" + TenLopHoc + @"</b>  <br>
-	Số phí còn nợ: " + HoiPhiConNoNamTruoc + @"<br>
Mời anh/chị thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHan + "<br>";
                            body += @"Phương thức nộp Hội phí:<br>
 + Chuyển khoản:<br>
             Người thụ hưởng: Hội Kiểm toán viên hành nghề Việt Nam<br>
             Số TK: 117000011010 Tại Ngân hàng VietinBank Hai Bà Trưng Hà Nội<br>
+ Hoặc nộp tiền mặt tại VP VACPA Hà Nội hoặc VP VACPA Tp. Hồ Chí Minh";
                            SmtpMail.Send(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, ref mess);
                            cm.ghilog("ThongBaoNopPhiCNKT", mess);
                        }
                    }
                }


                sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

                mess = "";
                sql = new SqlCommand();
                int PhiIDTapThe = 0;
                string strPhiIdTapThe = Request.QueryString["phiidtapthe"];
                if (!string.IsNullOrEmpty(Request.QueryString["phiidtapthe"]))
                {
                    try
                    {
                        PhiIDTapThe = Convert.ToInt32(Request.QueryString["phiidtapthe"]);
                    }
                    catch
                    {
                        PhiIDTapThe = 0;
                    }
                }

                string dsmailtapthe = "";
                if (string.IsNullOrEmpty(Request.QueryString["emailtapthe"]))
                    dsmailtapthe = "0";
                else
                    dsmailtapthe = Request.QueryString["emailtapthe"];

                sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_TapThe]" + LopHocID + ",'', " + dsmailtapthe;
                dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];

                //sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + LopHocID;
                //dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                //sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", N'" + TimKiemTen + "', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + iLopHocID;
                //dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                //sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", N'" + TimKiemTen + "', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + iLopHocID;
                //dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                if (dtb.Rows.Count > 0)
                {
                    for (int i = 0; i < dtb.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
                        {
                            int HoiVienTapTheID = Convert.ToInt32(dtb.Rows[i]["HoiVienTapTheID"]);
                            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + LopHocID + "," + HoiVienTapTheID;
                            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + LopHocID + "," + HoiVienTapTheID;
                            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + LopHocID + "," + HoiVienTapTheID;
                            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

                            double douPhiPhaiNop = 0;

                            foreach (DataRow dtr in dtbTemp.Rows)
                            {
                                douPhiPhaiNop += Convert.ToDouble(dtr["PhiCNKTConNoNamTruoc"]);
                            }

                            string strPhiPhaiNop = string.Format("{0:n0}", douPhiPhaiNop);


                            body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
                            body += @"VACPA xin thông báo đến quý công ty tình hình thanh toán phí như sau: <br>
<b>Loại phí: Phí CNKT Lớp :" + TenLopHoc + @"</b>  <br>
-	Tổng số phí phải nộp: " + strPhiPhaiNop + @" đồng<br>
<Chi tiết xem trong file đính kèm><br>
Mời quý công ty thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHan + "<br>";
                            body += @"Phương thức nộp Hội phí:<br>
 + Chuyển khoản:<br>
             Người thụ hưởng: Hội Kiểm toán viên hành nghề Việt Nam<br>
             Số TK: 117000011010 Tại Ngân hàng VietinBank Hai Bà Trưng Hà Nội<br>
+ Hoặc nộp tiền mặt tại VP VACPA Hà Nội hoặc VP VACPA Tp. Hồ Chí Minh";
                            string strTenFile = dtb.Rows[i]["MaHoiVienTapThe"].ToString().TrimEnd() + "_CNKT";
                            byte[] data = GetDataHoiVienCaNhan(HoiVienTapTheID.ToString(), (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc);
                            MemoryStream ms = new MemoryStream(data);
                            Attachment at = new Attachment(ms, strTenFile + ".xls", MediaTypeNames.Text.Html);
                            SmtpMail.SendWithAttach(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, at, ref mess);
                            cm.ghilog("ThongBaoNopPhiCNKT", mess);
                        }
                    }
                }


                cm.ghilog("ThongBaoNopPhiCNKT", "Gửi mail thông báo nộp phí cập nhật kiến thức thành công");
                Response.Redirect("admin.aspx?page=thongbaonopphicapnhatkienthuc&thongbao=thanhcong", false);
            }
        }
        catch (Exception ex)
        {
            cm.ghilog("ThongBaoNopPhiCNKT", "Lỗi xảy ra khi gửi mail thông báo nộp phí cập nhật kiến thức: " + ex.ToString());
            Response.Redirect("admin.aspx?page=thongbaonopphicapnhatkienthuc&thongbao=loi", false);
        }
    }

    protected void v_guimailthongbaonopphikscl()
    {
        try
        {
            sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

            mess = "";
            SqlCommand sql = new SqlCommand();
            //int PhiID = 0;
            //string strPhiId = Request.QueryString["phiid"];
            //if (!string.IsNullOrEmpty(Request.QueryString["phiid"]))
            //{
            //    try
            //    {
            //        PhiID = Convert.ToInt32(Request.QueryString["phiid"]);
            //    }
            //    catch
            //    {
            //        PhiID = 0;
            //    }
            //}

            string strNgayLap = "";
            if (!string.IsNullOrEmpty(Request.QueryString["ngaylap"]))
            {
                strNgayLap = DateTime.ParseExact(Request.QueryString["ngaylap"], "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
            }

            string strThoiHan = Request.QueryString["thoihan"];

            string dsmailcanhan = "";
            if (string.IsNullOrEmpty(Request.QueryString["emailcanhan"]))
                dsmailcanhan = "0";
            else
                dsmailcanhan = Request.QueryString["emailcanhan"];
            //sql.CommandText = "SELECT DISTINCT EMAIL FROM TBLHOIVIENCANHAN WHERE HOIVIENCANHANID IN (" + Request.QueryString["emailcanhan"] + ")";

            sql.CommandText = "SELECT PhiID, ThoiHanNopPhi from tblTTDMPhiKSCL where TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND ((CASE WHEN ThoiHanNopPhi IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE ThoiHanNopPhi END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            //sql.CommandText = "SELECT Top 1 PhiID, ThoiHanNopPhi from tblTTDMPhiKSCL where TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND ((CASE WHEN ThoiHanNopPhi IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE ThoiHanNopPhi END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            DataTable dtb = new DataTable();
            if (dtbTemp.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTemp.Rows)
                {
                    int PhiID = Convert.ToInt32(dtr[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhan]" + PhiID + ", " +
                                      (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," +
                                      (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", '','" + dsmailcanhan + "'," +
                                      (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhan]" + PhiID + ", " +
                                      (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," +
                                      (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", '','" + dsmailcanhan + "'," +
                                      (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhan]" + PhiID + ", " +
                                      (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," +
                                      (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", '','" + dsmailcanhan + "'," +
                                      (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                }
            }
            if (dtb.Rows.Count > 0)
            {
                for (int i = 0; i < dtb.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
                    {
                        string HoiPhiPhaiNopNamNay = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["PhiKSCLPhaiNopNamNay"]));
                        string HoiPhiConNoNamTruoc = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["PhiKSCLConNoNamTruoc"]));
                        string TongHoiPhiPhaiNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiKSCLPhaiNop"]));
                        string TongHoiPhiDaNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiKSCLDaNop"]));
                        string TongHoiPhiConPhaiNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiKSCLConPhaiNop"]));

                        body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
                        body += @"VACPA xin thông báo đến anh/chị tình hình thanh toán phí như sau: <br>
-	Loại phí: Phí Kiểm soát chất lượng  <br>
-	Số phí phải nộp năm nay: " + HoiPhiPhaiNopNamNay + @"<br>
-	Số phí còn nợ năm trước: " + HoiPhiConNoNamTruoc + @"<br>
-	Tổng số phí phải nộp: " + TongHoiPhiPhaiNop + @"<br>
-	Số phí đã nộp: " + TongHoiPhiDaNop + @"<br>
-	Số phí còn phải nộp: " + TongHoiPhiConPhaiNop + @"<br>
Mời anh/chị thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHan + "<br>";
                        body += @"Phương thức nộp Hội phí:<br>
 + Chuyển khoản:<br>
             Người thụ hưởng: Hội Kiểm toán viên hành nghề Việt Nam<br>
             Số TK: 117000011010 Tại Ngân hàng VietinBank Hai Bà Trưng Hà Nội<br>
+ Hoặc nộp tiền mặt tại VP VACPA Hà Nội hoặc VP VACPA Tp. Hồ Chí Minh";
                        SmtpMail.Send(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, ref mess);

                        cm.ghilog("ThongBaoNopPhiKSCL", mess);
                    }
                }
            }

            sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

            mess = "";
            sql = new SqlCommand();

            string dsmailtapthe = "";
            if (string.IsNullOrEmpty(Request.QueryString["emailtapthe"]))
                dsmailtapthe = "0";
            else
                dsmailtapthe = Request.QueryString["emailtapthe"];
            //sql.CommandText = "SELECT DISTINCT EMAIL FROM TBLHOIVIENCANHAN WHERE HOIVIENCANHANID IN (" + Request.QueryString["emailcanhan"] + ")";
            dtb = new DataTable();
            if (dtbTemp.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTemp.Rows)
                {
                    int PhiID = Convert.ToInt32(dtr[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_TapThe]" + PhiID + ", " +
                                      (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + "," +
                                      (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", '','" + dsmailtapthe + "'," +
                                      (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_TapThe]" + PhiID + ", " +
                                      (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + "," +
                                      (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", '','" + dsmailtapthe + "'," +
                                      (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                }
            }

            if (dtb.Rows.Count > 0)
            {
                for (int i = 0; i < dtb.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
                    {
                        int HoiVienTapTheID = Convert.ToInt32(dtb.Rows[i]["HoiVienTapTheID"]);
                        string PhiPhaiNopNamNay = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["PhiKSCLPhaiNopNamNay"]));
                        string PhiConNoNamTruoc = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["PhiKSCLConNoNamTruoc"]));
                        string TongPhiPhaiNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiKSCLPhaiNop"]));
                        string TongPhiDaNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiKSCLDaNop"]));
                        string TongPhiConPhaiNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiKSCLConPhaiNop"]));


                        string strIdHoiVien = dtb.Rows[i]["HoiVienTapTheID"].ToString();

                        sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhanCuaTapThe]" +
                            (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," +
                             (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + strIdHoiVien;
                        DataTable dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                        sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhanCuaTapThe]" +
                            (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," +
                             (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + strIdHoiVien;

                        double douPhiPhaiNopNamNay = 0;
                        double douPhiConNo = 0;
                        double douPhiPhaiNop = 0;
                        double douPhiDaNop = 0;
                        double douPhiConPhaiNop = 0;
                        foreach (DataRow dtr in dtbtemp.Rows)
                        {
                            douPhiPhaiNopNamNay += Convert.ToDouble(dtr["PhiPhaiNopNamNay"]);
                            douPhiConNo += Convert.ToDouble(dtr["PhiConNoNamTruoc"]);
                            douPhiPhaiNop += Convert.ToDouble(dtr["TongPhiPhaiNop"]);
                            douPhiDaNop += Convert.ToDouble(dtr["TongPhiDaNop"]);
                            douPhiConPhaiNop += Convert.ToDouble(dtr["TongPhiConPhaiNop"]);
                        }

                        string strPhiPhaiNopNamNayCaNhan = string.Format("{0:n0}", douPhiPhaiNopNamNay);
                        string strPhiConNoCaNhan = string.Format("{0:n0}", douPhiConNo);
                        string strPhiPhaiNopCaNhan = string.Format("{0:n0}", douPhiPhaiNop);
                        string strPhiDaNopCaNhan = string.Format("{0:n0}", douPhiDaNop);
                        string strPhiConPhaiNopCaNhan = string.Format("{0:n0}", douPhiConPhaiNop);


                        body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
                        body += @"VACPA xin thông báo đến quý công ty tình hình thanh toán phí như sau: <br>
                            <b>Loại phí: Phí Kiểm soát chất lượng HVTC</b>  <br>
                            -	Số phí phải nộp năm nay: " + PhiPhaiNopNamNay + @"<br>
                            -	Số phí còn nợ năm trước: " + PhiConNoNamTruoc + @"<br>
                            -	Tổng số phí phải nộp: " + TongPhiPhaiNop + @"<br>
                            -	Số phí đã nộp: " + TongPhiDaNop + @"<br>
                            -	Số phí còn phải nộp: " + TongPhiConPhaiNop + @"<br>
                            <b>Loại phí: Phí Kiểm soát chất lượng HVCN</b>  <br>
                            -	Số phí phải nộp năm nay: " + strPhiPhaiNopNamNayCaNhan + @"<br>
                            -	Số phí còn nợ năm trước: " + strPhiConNoCaNhan + @"<br>
                            -	Tổng số phí phải nộp: " + strPhiPhaiNopCaNhan + @"<br>
                            -	Số phí đã nộp: " + strPhiDaNopCaNhan + @"<br>
                            -	Số phí còn phải nộp: " + strPhiConPhaiNopCaNhan + @"<br>
                            Mời quý công ty thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHan + "<br>";
                        body += @"Phương thức nộp Hội phí:<br>
 + Chuyển khoản:<br>
             Người thụ hưởng: Hội Kiểm toán viên hành nghề Việt Nam<br>
             Số TK: 117000011010 Tại Ngân hàng VietinBank Hai Bà Trưng Hà Nội<br>
+ Hoặc nộp tiền mặt tại VP VACPA Hà Nội hoặc VP VACPA Tp. Hồ Chí Minh";

                        //SmtpMail.Send(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, ref mess);

                        string strTenFile = dtb.Rows[i]["MaHoiVienTapThe"].ToString().TrimEnd() + "_KSCL";
                        byte[] data = GetDataHoiVienCaNhan(HoiVienTapTheID.ToString(), (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong);
                        MemoryStream ms = new MemoryStream(data);
                        Attachment at = new Attachment(ms, strTenFile + ".xls", MediaTypeNames.Text.Html);
                        SmtpMail.SendWithAttach(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, at, ref mess);

                        cm.ghilog("ThongBaoNopPhiKSCL", mess);
                    }
                }
            }
            cm.ghilog("ThongBaoNopPhiKSCL", "Gửi mail thông báo nộp phí Kiểm soát chất lượng thành công");
            Response.Redirect("admin.aspx?page=thongbaonopphikiemsoatchatluong&thongbao=thanhcong", false);
        }
        catch (Exception ex)
        {
            cm.ghilog("ThongBaoNopPhiKSCL", "Lỗi xảy ra khi gửi mail thông báo nộp phí Kiểm soát chất lượng: " + ex.ToString());
            Response.Redirect("admin.aspx?page=thongbaonopphikiemsoatchatluong&thongbao=loi", false);
        }
    }

    protected void v_guimailthongbaonopphidanglogo()
    {
        try
        {
            sub = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";

            mess = "";
            SqlCommand sql = new SqlCommand();
            //int PhiID = 0;
            //string strPhiId = Request.QueryString["philogoid"];
            //if (!string.IsNullOrEmpty(Request.QueryString["philogoid"]))
            //{
            //    try
            //    {
            //        PhiID = Convert.ToInt32(Request.QueryString["philogoid"]);
            //    }
            //    catch
            //    {
            //        PhiID = 0;
            //    }
            //}
            string strThoiHan = Request.QueryString["thoihan"];

            string dsmail = "";
            if (string.IsNullOrEmpty(Request.QueryString["email"]))
                dsmail = "0";
            else
                dsmail = Request.QueryString["email"];
            //sql.CommandText = "SELECT DISTINCT EMAIL FROM TBLHOIVIENCANHAN WHERE HOIVIENCANHANID IN (" + Request.QueryString["emailcanhan"] + ")";
            sql.CommandText = "SELECT PhiID, ThoiHanNgay, ThoiHanThang from tblTTDMPhiDangLogo where TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND ((CASE WHEN NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];

            DataTable dtb = new DataTable();
            if (dtbTemp.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTemp.Rows)
                {
                    int PhiID = Convert.ToInt32(dtr[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiDangLogo]" + PhiID + ", " +
                                      (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + "," +
                                      (int)EnumVACPA.LoaiPhi.PhiDangLogo + ", '','" + dsmail + "'," +
                                      (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiDangLogo]" + PhiID + ", " +
                                      (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + "," +
                                      (int)EnumVACPA.LoaiPhi.PhiDangLogo + ", '','" + dsmail + "'," +
                                      (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                }
            }
            if (dtb.Rows.Count > 0)
            {
                for (int i = 0; i < dtb.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(dtb.Rows[i]["Email"].ToString()))
                    {
                        string HoiPhiPhaiNopNamNay = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["PhiDangLogoPhaiNopNamNay"]));
                        string HoiPhiConNoNamTruoc = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["PhiDangLogoConNoNamTruoc"]));
                        string TongHoiPhiPhaiNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiDangLogoPhaiNop"]));
                        string TongHoiPhiDaNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiDangLogoDaNop"]));
                        string TongHoiPhiConPhaiNop = string.Format("{0:n0}", Convert.ToDecimal(dtb.Rows[i]["TongPhiDangLogoConPhaiNop"]));


                        body = "THÔNG BÁO NỘP PHÍ TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM <br><br>";
                        body += @"VACPA xin thông báo đến quý công ty tình hình thanh toán phí như sau: <br>
                        <b>Loại phí: Phí đăng logo</b>  <br>
                        -	Số phí phải nộp năm nay: " + HoiPhiPhaiNopNamNay + @"<br>
                        -	Số phí còn nợ năm trước: " + HoiPhiConNoNamTruoc + @"<br>
                        -	Tổng số phí phải nộp: " + TongHoiPhiPhaiNop + @"<br>
                        -	Số phí đã nộp: " + TongHoiPhiDaNop + @"<br>
                        -	Số phí còn phải nộp: " + TongHoiPhiConPhaiNop + @"<br>
                        Mời quý công ty thanh toán số phí còn phải nộp cho VACPA trước ngày " + strThoiHan + "<br>";
                        body += @"Phương thức nộp Hội phí:<br>
 + Chuyển khoản:<br>
             Người thụ hưởng: Hội Kiểm toán viên hành nghề Việt Nam<br>
             Số TK: 117000011010 Tại Ngân hàng VietinBank Hai Bà Trưng Hà Nội<br>
+ Hoặc nộp tiền mặt tại VP VACPA Hà Nội hoặc VP VACPA Tp. Hồ Chí Minh";
                        SmtpMail.Send(cm.Admin_TenDangNhap, dtb.Rows[i]["Email"].ToString(), sub, body, ref mess);
                        cm.ghilog("ThongBaoNopPhiDangLogo", mess);
                    }
                }
            }
            cm.ghilog("ThongBaoNopPhiDangLogo", "Gửi mail thông báo nộp phí đăng logo thành công");
            Response.Redirect("admin.aspx?page=thongbaonopphidanglogo&thongbao=thanhcong", false);
        }
        catch (Exception ex)
        {
            cm.ghilog("ThongBaoNopPhiDangLogo", "Lỗi xảy ra khi gửi mail thông báo nộp phí đăng logo: " + ex.ToString());
            Response.Redirect("admin.aspx?page=thongbaonopphidanglogo&thongbao=loi", false);
        }
    }

    private byte[] GetDataHoiVienCaNhan(string IdHoiVien, int iLoaiPhi)
    {
        string outHTML = "";

        outHTML += "<table border = '1' cellpadding='5' cellspacing='0' style='font-family:Arial; font-size:8pt; color:#003366'>";
        outHTML += "<tr>";

        SqlCommand sql = new SqlCommand();
        DataTable dtbtemp = new DataTable();
        //Lấy danh sách hội viên tổ chức, vùng miền, số lượng hội viên cá nhân where theo điều kiện lọc
        //sql.CommandText = @"select HoDem + ' ' + Ten as TenHoiVien, '1.000.000' as SoTien from tblHoiVienCaNhan where HoiVienTapTheID = " + IdHoiVien;
        if (iLoaiPhi == (int)EnumVACPA.LoaiPhi.PhiHoiVien)
        {
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_CaNhanCuaTapThe]" +
                              (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + "," +
                              (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + IdHoiVien;
            dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiHoiVien_CaNhanCuaTapThe]" +
                              (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + "," +
                              (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + IdHoiVien;
            dtbtemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        }
        else if (iLoaiPhi == (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc)
        {
            string strNgayLap = DateTime.ParseExact(Request.QueryString["ngaylap"], "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
            string LopHocID = Request.QueryString["lophocid"].ToString();
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + LopHocID + "," + IdHoiVien;
            dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + LopHocID + "," + IdHoiVien;
            dtbtemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiCNKT_CaNhanCuaTapThe]" + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", '', '','" + strNgayLap + "'," + (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + LopHocID + "," + IdHoiVien;
            dtbtemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        }
        else if (iLoaiPhi == (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong)
        {
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhanCuaTapThe]" +
                                (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," +
                                (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + IdHoiVien;
            dtbtemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhanCuaTapThe]" +
                              (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," +
                              (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + IdHoiVien;
            dtbtemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhanCuaTapThe]" +
                              (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," +
                              (int)EnumVACPA.TinhTrang.DaPheDuyet + "," + IdHoiVien;
            dtbtemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        }


        if (iLoaiPhi == (int)EnumVACPA.LoaiPhi.PhiHoiVien)
        {
            outHTML += "<td colspan='7' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH PHÍ HỘI VIÊN CÁ NHÂN</b></td>";

            outHTML += "</tr>";
            outHTML += "<tr>";
            outHTML += "<td colspan='7' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i> Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + "</i></td>";
            outHTML += "</tr>";
            outHTML += "<tr style='border-style:none'></tr>";

            outHTML += "<tr>";
            outHTML += "    <th width='30px'>  STT  </th>";
            outHTML += "    <th width='300px'>       Họ và tên         </th>";
            outHTML += "    <th width='300px'>       Hội phí phải nộp năm nay        </th>";
            outHTML += "    <th width='300px'>       Hội phí còn nợ năm trước      </th>";
            outHTML += "    <th width='300px'>       Tổng hội phí phải nộp     </th>";
            outHTML += "    <th width='300px'>       Tổng hội phí đã nộp      </th>";
            outHTML += "    <th width='300px'>       Tổng hội phí còn phải nộp      </th>";

            outHTML += "</tr>";
            int i = 0;
            foreach (DataRow dtr in dtbtemp.Rows)
            {
                i++;
                outHTML += "<tr>";
                outHTML += "<td>" + i.ToString() + "</td>";
                outHTML += "<td>" + dtr["HoVaTen"] + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["HoiPhiPhaiNopNamNay"]) + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["HoiPhiConNoNamTruoc"]) + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["TongHoiPhiPhaiNop"]) + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["TongHoiPhiDaNop"]) + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["TongHoiPhiConPhaiNop"]) + "</td>";
                outHTML += "</tr>";
            }
        }
        else if (iLoaiPhi == (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc)
        {
            outHTML += "<td colspan='3' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH PHÍ CẬP NHẬT KIẾN THỨC</b></td>";
            outHTML += "</tr>";
            outHTML += "<tr>";
            outHTML += "<td colspan='3' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i> Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + "</i></td>";
            outHTML += "</tr>";
            outHTML += "<tr style='border-style:none'></tr>";

            outHTML += "<tr>";
            outHTML += "    <th width='30px'>  STT  </th>";
            outHTML += "    <th width='300px'>       Họ và tên         </th>";
            outHTML += "    <th width='300px'>       Phí CNKT còn nợ        </th>";

            outHTML += "</tr>";
            int i = 0;
            foreach (DataRow dtr in dtbtemp.Rows)
            {
                i++;
                outHTML += "<tr>";
                outHTML += "<td>" + i.ToString() + "</td>";
                outHTML += "<td>" + dtr["HoVaTen"] + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["PhiCNKTConNoNamTruoc"]) + "</td>";
                outHTML += "</tr>";
            }
        }
        else if (iLoaiPhi == (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong)
        {
            outHTML += "<td colspan='7' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH PHÍ KIỂM SOÁT CHẤT LƯỢNG</b></td>";
            outHTML += "</tr>";
            outHTML += "<tr>";
            outHTML += "<td colspan='7' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i> Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + "</i></td>";
            outHTML += "</tr>";
            outHTML += "<tr style='border-style:none'></tr>";

            outHTML += "<tr>";
            outHTML += "    <th width='30px'>  STT  </th>";
            outHTML += "    <th width='300px'>       Họ và tên         </th>";
            outHTML += "    <th width='300px'>       Phí KSCL phải nộp năm nay        </th>";
            outHTML += "    <th width='300px'>       Phí KSCL còn nợ năm trước      </th>";
            outHTML += "    <th width='300px'>       Tổng phí KSCL phải nộp     </th>";
            outHTML += "    <th width='300px'>       Tổng phí KSCL đã nộp      </th>";
            outHTML += "    <th width='300px'>       Tổng phí KSCL còn phải nộp      </th>";

            outHTML += "</tr>";

            int i = 0;
            foreach (DataRow dtr in dtbtemp.Rows)
            {
                i++;
                outHTML += "<tr>";
                outHTML += "<td>" + i.ToString() + "</td>";
                outHTML += "<td>" + dtr["HoVaTen"] + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["PhiPhaiNopNamNay"]) + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["PhiConNoNamTruoc"]) + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["TongPhiPhaiNop"]) + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["TongPhiDaNop"]) + "</td>";
                outHTML += "<td>" + string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", dtr["TongPhiConPhaiNop"]) + "</td>";
                outHTML += "</tr>";
            }
        }
        outHTML += "</table>";
        byte[] data = Encoding.UTF8.GetBytes(outHTML);
        return data;
    }
}