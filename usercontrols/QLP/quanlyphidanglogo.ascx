﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="quanlyphidanglogo.ascx.cs" Inherits="usercontrols_quanlyphidanglogo" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 152px;
    }
    .style2
    {
        width: 30%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_quanlyphidanglogo" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_quanlyphidanglogo" clientidmode="Static" runat="server"   method="post">
      <h4 class="widgettitle"><%=tenchucnang%></h4>
    <div id="thongbaoloi_form_quanlyphidanglogo" name="thongbaoloi_form_quanlyphidanglogo" style="display:none" class="alert alert-error"></div>
     <table id="tblThongBao" width="100%" border="0" class="formtbl">
        <tr>
         <td width = "50%">
         <div ID="TinhTrangBanGhi" name = "TinhTrangBanGhi"><i></i></div>
        </td>
         <td width = "50%">
        <div ID="TinhTrangHieuLuc" name = "TinhTrangHieuLuc"><i></i></div>
        </td>
        </tr>
        </table>
        <div><br /><b> Thông tin phí</b></div>
      <table id="Table1" width="100%" border="0" class="formtbl" >
      <tr>
          <td class="style1"><asp:Label ID="lblMaPhi" runat="server" Text = 'Mã phí đăng logo:' ></asp:Label></td>
        <td class="style2" ><input type="text" name="MaPhi" id="MaPhi" disabled = "disabled"></td>
        <td>&nbsp;</td>
        <td class="style1" ><asp:Label ID="lblNgayLap" runat="server" Text = 'Ngày lập:' ></asp:Label>
            <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td colspan ="3" style ="width:35%"><input type="text" name="NgayLap" id="NgayLap"></td>
      </tr>
       <tr>
          <td class="style1"><asp:Label ID="Label6" runat="server" Text = 'Tên phí:' ></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td colspan ="6"><input size = "100%" type="text" name="TenPhi" id="TenPhi"></td>
      </tr>
       
      <tr>
          <td class="style1"><asp:Label ID="Label7" runat="server" Text = 'Ngày áp dụng:' ></asp:Label>
            <asp:Label ID="Label9" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2"><input type="text" name="NgayApDung" id="NgayApDung"></td>
         <td>&nbsp;</td>
        <td class="style1"><asp:Label ID="Label10" runat="server" Text = 'Ngày hết hiệu lực:' ></asp:Label></td>
        <td colspan ="3" class ="style2"><input type="text" name="NgayHetHieuLuc" id="NgayHetHieuLuc" ></td>

      </tr>
       <td class="style1"><asp:Label ID="lblKichThuoc" runat="server" Text = 'Kích thước:' ></asp:Label></td>
        <td class="style2" ><input type="text" name="KichThuoc" id="KichThuoc"></td>
        <td>&nbsp;</td>
        <td class="style1" ><asp:Label ID="lblViTri" runat="server" Text = 'Vị trí:' ></asp:Label></td>
        <td colspan ="3" style ="width:35%"><input type="text" name="ViTri" id="ViTri"></td>
      <tr>
          
      </tr>
      <tr>
        <td class="style1"><asp:Label ID="Label3" runat="server" Text = 'Mức phí:' ></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2"><input type="text" name="MucPhi" id="MucPhi" class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="999999999999999999"></td>
        <td class ="style3"><asp:Label ID="Label11" runat="server" Text = 'VNĐ' ></asp:Label>
        </td>
        <td colspan ="4">&nbsp;</td>
      </tr>

        <tr>
          <td class="style1"><asp:Label ID="Label12" runat="server" Text = 'Đơn vị thời gian tính phí:' ></asp:Label>
            <asp:Label ID="Label13" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2">
         <select name="DonViThoiGian" id="DonViThoiGian">
          <%  
                  LoadThoiGian();
              %>
          </select></td>
        <td>&nbsp;</td>
        <td class="style1"><asp:Label ID="Label15" runat="server" Text = 'Thời hạn nộp phí: Ngày'></asp:Label>
        <asp:Label ID="Label5" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style4"> <select name="NgayNop" id="NgayNop">
          <%  
                  LoadNgayNop31();
              %>
          </select></td>
        <td class="style4"><asp:Label ID="Label16" runat="server" Text = 'Tháng:'></asp:Label></td>
        <td class="style4"> <select name="ThangNop" id="ThangNop">
          <%  
                  LoadThangNop();
              %>
          </select></td>
      </tr>

      </table>
        
   </div>
    </form>
   <div>
    <%LoadNut(); %>
        
   </div>
                                                    
<script type="text/javascript">
       

        function submitform() {        
                jQuery("#form_quanlyphidanglogo").submit();
        }


        jQuery("#form_quanlyphidanglogo").validate({
            rules: {
                NgayLap: {
                    required: true
                },
                TenPhi: {
                    required: true
                },
                NgayApDung: {
                    required: true
                },
                MucPhi: {
                    required: true
                },
                DonViThoiGian: {
                    required: true
                },
                ThoiHanNopPhi: {
                    required: true
                },
            },
            invalidHandler: function (f, d) {
                var g = d.numberOfInvalids();

                if (g) {
                    var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                    jQuery("#thongbaoloi_form_quanlyphidanglogo").html(e).show()

                } else {
                    jQuery("#thongbaoloi_form_quanlyphidanglogo").hide()
                }
            }
        });

    $.datepicker.setDefaults($.datepicker.regional['vi']);


       

</script>

<script type="text/javascript">

    function luu() {
        jQuery("#form_quanlyphidanglogo").submit();

    };

    function xoa(id) {
        window.location = 'admin.aspx?page=quanlyphidanglogo&id=' + id + '&mode=view&tinhtrang=xoa';

    };

    function duyet(id) {
        window.location = 'admin.aspx?page=quanlyphidanglogo&id=' + id + '&mode=view&tinhtrang=duyet';
    };

    function tuchoi(id) {

        window.location = 'admin.aspx?page=quanlyphidanglogo&id=' + id + '&mode=view&tinhtrang=tuchoi';

    };

    function thoaiduyet(id) {
        window.location = 'admin.aspx?page=quanlyphidanglogo&id=' + id + '&mode=view&tinhtrang=thoaiduyet';

    };

</script>

<script type="text/javascript">
    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });

    <%LoadThongTin(); %>
        <%AnNut(); %>
        <%DoiNgay(); %>

</script>

