﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;


public partial class usercontrols_dmChonCongTy : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    //DataSet dstt;
    //// Tên chức năng
    //public string tenchucnang = "Giảng viên";

    ////public string ctid = "CongTyId";
    ////public string tencty = "TenCongTy";

    private class objHoiVienCaNhan
    {
        public int STT { set; get; }
        public int HoiVienID { set; get; }
        public string HoTen { set; get; }
        public bool Check { set; get; }
    }

    protected void ok_Click(object sender, EventArgs e)
    {
        List<int> lstIdChon = new List<int>();
        
        foreach (GridViewRow row in gvHoiVien.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("idCheck") as CheckBox);
                string chkRow1 = row.Cells[1].Text;
                if (chkRow.Checked)
                {
                    if (!lstIdChon.Contains(Convert.ToInt32(row.Cells[1].Text)))
                        lstIdChon.Add(Convert.ToInt32(row.Cells[1].Text));
                    //string storname = row.Cells[2].Text;
                    //string state = row.Cells[3].Text;
                    //data = data + storid + " ,  " + storname + " , " + state + "<br>";
                }
            }
        }
        //ScriptManager.RegisterStartupScript(this, GetType(), "submitform", "submitform();", true);
        //Page.ClientScript.RegisterStartupScript(GetType(), "submitform", "submitform();", true);
        Session["CongTyChon"] = lstIdChon;
    }
    protected void Page_Load(object sender, EventArgs e)
    {// lần đầu load này thì nó chạy vào load dữ liệu cũ
        if (!IsPostBack) // lần 2 load này, nó chạy luôn xuống phần mình lấy dữ liệu, bỏ qua phần load
            load_giatricu();
        else
        {

            //string data = "";
            //foreach (GridViewRow row in gvHoiVien.Rows)
            //{
            //    if (row.RowType == DataControlRowType.DataRow)
            //    {
            //        CheckBox chkRow = (row.Cells[0].FindControl("idCheck") as CheckBox);
            //        string chkRow1 = row.Cells[1].Text;
            //        if (chkRow.Checked)
            //        {
            //            string storid = row.Cells[1].Text;
            //            //string storname = row.Cells[2].Text;
            //            //string state = row.Cells[3].Text;
            //            //data = data + storid + " ,  " + storname + " , " + state + "<br>";
            //        }
            //    }
            //}
           // lblmsg.Text = data;
            //DataTable dtb = new DataTable();
            //dtb.Columns.Add("idNS");

           // foreach (RepeaterItem item in rptNhanVien.Items)
            //{
            //    string abc = Request.QueryString["idhoso"];

            //    //SqlCommand sql = new SqlCommand();
            //    //sql.CommandText = "SELECT distinct HoiVienCaNhanID, HoDem, Ten FROM tblHoiVienCaNhan WHERE HoiVienTapTheID = " + Request.QueryString["id"];
            //    //DataSet ds = new DataSet();
            //    //ds = DataAccess.RunCMDGetDataSet(sql);
            //    //string rq = "";
            //    //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //    //{
            //    //    if (!string.IsNullOrEmpty(Request.Form[ds.Tables[0].Rows[i][0] + "_id"]))
            //    //        rq = Request.Form[ds.Tables[0].Rows[i][0] + "_id"].ToString();
            //    //}
            //}
            //load_giatricu();
        }
    }


   
    protected void load_giatricu()
    {
        
        SqlCommand sql = new SqlCommand();

        sql.CommandText = "SELECT distinct HoiVienTapTheID, TenDoanhNghiep FROM tblHoiVienTapThe";
       
        DataSet ds = new DataSet();
        ds = DataAccess.RunCMDGetDataSet(sql);

        //int i = 0;
        //List<objHoiVienCaNhan> lst = new List<objHoiVienCaNhan>();
        //foreach (DataRow dtr in ds.Tables[0].Rows)
        //{
        //    i++;

        //    objHoiVienCaNhan new1 = new objHoiVienCaNhan();
        //    new1.STT = i;
        //    new1.HoiVienID = Convert.ToInt32(dtr[0]);
        //    new1.HoTen = dtr[1].ToString() + " " + dtr[2].ToString();
        //    new1.Check = false;
        //    lst.Add(new1);
        //}
        //rptNhanVien.DataSource = lst;
        //rptNhanVien.DataBind();
        gvHoiVien.DataSource = ds.Tables[0];
        gvHoiVien.DataBind();
    }
}