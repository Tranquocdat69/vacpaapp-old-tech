﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;
public partial class usercontrols_danhsachcongtykiemtratructiep : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        grvDanhSach.Columns[0].HeaderText = "STT";
        grvDanhSach.Columns[1].HeaderText = "Mã danh sách";
        grvDanhSach.Columns[2].HeaderText = "Ngày lập";
        grvDanhSach.Columns[3].HeaderText = "Chọn";

        SqlCommand sql = new SqlCommand();
        sql.CommandText = @"SELECT DSKiemTraTrucTiepID, MaDanhSach, NgayLap FROM tblKSCLDSKiemTraTrucTiep";
        DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
        grvDanhSach.DataSource = dtbTemp;
        grvDanhSach.DataBind();
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"SELECT DSKiemTraTrucTiepID, MaDanhSach, NgayLap FROM tblKSCLDSKiemTraTrucTiep"
                + " WHERE 0 =0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
            {
                sql.CommandText += " AND MaDanhSach like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
            }
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            grvDanhSach.DataSource = dtbTemp;
            grvDanhSach.DataBind();
        }
    }
}