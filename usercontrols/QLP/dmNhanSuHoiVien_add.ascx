﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dmNhanSuHoiVien_add.ascx.cs" Inherits="usercontrols_dmNhanSuHoiVien_add" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     
     table#t08, table#t08 tr, table#t08 td, table#t08 th
    {
        border: 1px solid black;
        border-collapse: collapse;
        padding:5px;
    } 
    
    table.grid{
           border-collapse:collapse;
           border:1px solid black;
           padding: 3px;
           align: center;
        }
        table.grid td, th{
           border:1px solid black;
           padding: 3px;
           align: center;
        }
    
</style>
<script type="text/javascript" src="js/jquery-1.11.2.js">

</script>
<script type="text/javascript"  src="js/jquery.stickytableheaders.min.js"></script>
<%--<script type="text/javascript">
       <% load_giatricu(); %>
</script>--%>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<%--<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>--%>

    <%--<form id="form_dmgiangvien_add" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_dmNhanSuHoiVien_add" clientidmode="Static" runat="server"   method="post">
    
    <div id="thongbaoloi_form_themdmgiangvien" name="thongbaoloi_form_themdmgiangvien" style="display:none" class="alert alert-error"></div>
   

      
      
      
        <%-- <% form_NhanSu(); %>--%>
    <asp:GridView ID="gvHoiVien" runat="server" AutoGenerateColumns="false"
        BackColor="White" BorderColor="#3366CC" CssClass="grid" BorderStyle="Solid" BorderWidth="1px"
        CellPadding="4" Width="462px">
        <Columns>
        <asp:TemplateField  ItemStyle-Width="30px">
                  <ItemTemplate>
                     <%-- <input type="checkbox" class="colcheckbox" value="<%# Eval("HoiVienCaNhanID")%>" />--%>
                      <asp:CheckBox runat="server" ID="idCheck" AutoPostBack="true" OnCheckedChanged = "ok_Click" />
                  </ItemTemplate>


<ItemStyle Width="30px"></ItemStyle>


              </asp:TemplateField>
              <asp:BoundField DataField="HoiVienCaNhanID" HeaderText="ID" HtmlEncode="false" />
            <asp:BoundField DataField="HoTen" HeaderText="Họ tên" HtmlEncode="false"  />
            
            
            
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" BorderWidth = "1px" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" 
            BorderWidth = "1px" />
        <PagerStyle ForeColor="#003399" HorizontalAlign="Left" BackColor="#99CCCC" />
        <RowStyle BackColor="White" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <SortedAscendingCellStyle BackColor="#EDF6F6" />
        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
        <SortedDescendingCellStyle BackColor="#D6DFDF" />
        <SortedDescendingHeaderStyle BackColor="#002876" />
    </asp:GridView>
    
    <%--<div>
    <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="confirm_delete_dmtinh(gvHoiVien_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
    </div>--%>
    </form>


    <script type="text/javascript">
        var gvHoiVien_selected = new Array();

        jQuery(document).ready(function () {
            // dynamic table      

            $("#<%=gvHoiVien.ClientID%> .checkall").bind("click", function () {
                gvHoiVien_selected = new Array();
                checked = $(this).prop("checked");
                $('#<%=gvHoiVien.ClientID%> :checkbox').each(function () {
                    $(this).prop("checked", checked);
                    if ((checked) && ($(this).val() != "all")) gvHoiVien_selected.push($(this).val());
                });
            });

            $('#<%=gvHoiVien.ClientID%> :checkbox').each(function () {
                $(this).bind("click", function () {
                    removeItem(gvHoiVien_selected, $(this).val())
                    checked = $(this).prop("checked");
                    if ((checked) && ($(this).val() != "all")) gvHoiVien_selected.push($(this).val());
                });
            });
        });

//        function chonhoso() {
//            document.cookie = "idhoso=" + gvHoSo_selected;
//            window.parent.$("#div_dmNhanSuHoiVien_add").dialog("close");

//        }
    </script>
                                                    

