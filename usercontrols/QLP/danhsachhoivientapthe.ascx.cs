﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_danhsachhoivientapthe : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        grvDanhSach.Columns[0].HeaderText = "Mã hội viên";
        grvDanhSach.Columns[1].HeaderText = "Tên viết tắt";
        grvDanhSach.Columns[2].HeaderText = "Tên doanh nghiệp";
        grvDanhSach.Columns[3].HeaderText = "Chọn";

         SqlCommand sql = new SqlCommand();
         sql.CommandText = @"SELECT HOIVIENTAPTHEID, MAHOIVIENTAPTHE, TENVIETTAT, SOHIEU + ' - ' + TENDOANHNGHIEP AS TENDOANHNGHIEP FROM TBLHOIVIENTAPTHE ORDER BY SOHIEU ASC";
         DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
         grvDanhSach.DataSource = dtbTemp;
         grvDanhSach.DataBind();
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]) || !string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
        { 
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"SELECT HOIVIENTAPTHEID, MAHOIVIENTAPTHE, TENVIETTAT, SOHIEU + ' - ' + TENDOANHNGHIEP AS TENDOANHNGHIEP FROM TBLHOIVIENTAPTHE "
                + " WHERE 0 =0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                    {
                        sql.CommandText += " AND MAHOIVIENTAPTHE like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                        sql.CommandText += " OR TENVIETTAT like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                    }
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                {
                    sql.CommandText += " AND TENDOANHNGHIEP like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
                }

            sql.CommandText += " ORDER BY SOHIEU ASC";
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            grvDanhSach.DataSource = dtbTemp;
            grvDanhSach.DataBind();
        }
    }
}