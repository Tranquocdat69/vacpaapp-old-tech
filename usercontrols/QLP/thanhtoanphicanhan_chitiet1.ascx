﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="thanhtoanphicanhan_chitiet1.ascx.cs" Inherits="usercontrols_thanhtoanphicanhan_chitiet1" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 15%;
    }
    .style2
    {
        width: 35%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }
    .style5
    {
        width: 50%;
    }

</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript">

    function changeloaiphi() {
        document.getElementById('<%= Test.ClientID %>').click();
    }
    </script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


    <%--<form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphicanhan_chitiet" runat="server"   method="post">
    <asp:ScriptManager ID="ScriptManagerPhiCaNhanChiTiet" runat="server"></asp:ScriptManager>
    
    <div id="thongbaoloi_form_thanhtoanphicanhan_chitiet" name="thongbaoloi_form_thanhtoanphicanhan_chitiet" style="display:none" class="alert alert-error"></div>
     
      <table id="Table1" width="890px" border="0" class="formtbl" >
      <tr>
          <td class="style5"><asp:Label ID="lblIDKhachHang" runat="server" Text = 'ID khách hàng:' ></asp:Label>
        <span><%=strMaHoiVien%></span>
            </td>
             <td class="style5"><asp:Label ID="Label6" runat="server" Text = 'Họ và tên:' ></asp:Label>
            <span><%=strHoTen%></span>
            </td>
      </tr>
           <tr>
          <td class="style5"><asp:Label ID="Label1" runat="server" Text = 'Số chứng chỉ KTV:' ></asp:Label>
        <span><%=strSoCCKTV%></span>
            </td>
             <td class="style5"><asp:Label ID="Label3" runat="server" Text = 'Đơn vị công tác:' ></asp:Label>
            <span><%=strDonViCongTac%></span>
            </td>
      </tr>
      </table>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"    >
       <ContentTemplate>
   <div><br /><b>Các loại phí phải nộp</b></div>
            <div  class="dataTables_length">
        <table>
        <tr>
            <td>&nbsp;</td>
            <td class ="style2"> <asp:DropDownList id ="LoaiPhi" name ="LoaiPhi" DataTextField ="TEN" DataValueField="ID" runat ="server" onchange="changeloaiphi();"></asp:DropDownList></td>
         </tr></table>

        </div>
           <div style="width: 100%; height: 250px; overflow: scroll">
   
            <div style="width: 100%; text-align: center; margin-top: 5px; display:none">
    <asp:LinkButton ID="Test" runat="server" CssClass="btn" 
        onclick="Test_Click" ><i class="iconfa-search">           
    </i>Test</asp:LinkButton>
            </div>
       <asp:GridView ClientIDMode="Static" ID="thanhtoanphicanhanchitiet_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="thanhtoanphicanhanchitiet_grv_PageIndexChanging" 
       onsorting="thanhtoanphicanhanchitiet_grv_Sorting"
       OnDataBound = "thanhtoanphicanhanchitiet_grv_OnDataBound"
       style = "width: 890px"
       >
          <Columns>
                <asp:TemplateField  >
                <HeaderStyle Width="50px" HorizontalAlign = "Center"/>
                <ItemStyle HorizontalAlign = "Center"/>
                  <ItemTemplate>
                      <span><%# Container.DataItemIndex + 1%></span>
                  </ItemTemplate>
              </asp:TemplateField>
               <asp:TemplateField  >
               <HeaderStyle Width="100px" />
                  <ItemTemplate>
                      <span><%# Eval(truongmaphi)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="350px" />
                  <ItemTemplate>
                     <span><%# Eval(truongtenloaiphi)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                     <asp:Label name = "TruongSoPhiPhaiNop" ID = "TruongSoPhiPhaiNop" runat = "server" Text= <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval(truongtongsophiphainop))%>></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>
                          
              <asp:TemplateField  >
              <HeaderStyle Width="120px" />
                  <ItemTemplate>
                     <asp:Label name = "TruongSoPhiDaNop" ID = "TruongSoPhiDaNop" runat = "server" Text= <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval(truongsophidanop))%>></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField  >
              <HeaderStyle Width="120px" />
                  <ItemTemplate>
                     <asp:Label name = "TruongSoPhiConNoHienTai" ID = "TruongSoPhiConNoHienTai" runat = "server" Text= <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval(truongsophiconnohientai))%>></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>           
          </Columns>
</asp:GridView>
               </div>
     
      </ContentTemplate>
      </asp:UpdatePanel>
 
    </form>
                    <div id = "div_danhsachhoivien"></div>       
<iframe name="iframe_ajax" width="0px" height="0px"></iframe>

<script type="text/javascript">

    function fill_thongtinhoivien() {
        loadgrid();
    }

    function fill_thongtin() {
        loadgrid();
    }

    function dong() {
        $(this).close();
    }
    //    function fill_thongtinhoivien(id) {
    //        funLoadGrid();
    //    }
</script>
<script type="text/javascript">

    
    </script>
