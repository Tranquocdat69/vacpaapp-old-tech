﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using HiPT.VACPA.DL;


public partial class usercontrols_doingay : System.Web.UI.UserControl
{
    Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string thang = Request.QueryString["thang"];
            string strNgay = Request.QueryString["ngay"];
            int ngay = Convert.ToInt32(strNgay);
            switch (thang)
            {
                case "1":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop31(ngay)));
                    break;
                case "3":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop31(ngay)));
                    break;
                case "5":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop31(ngay)));
                    break;
                case "7":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop31(ngay)));
                    break;
                case "8":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop31(ngay)));
                    break;
                case "10":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop31(ngay)));
                    break;
                case "12":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop31(ngay)));
                    break;
                case "4":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop30(ngay)));
                    break;
                case "6":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop30(ngay)));
                    break;
                case "9":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop30(ngay)));
                    break;
                case "11":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop30(ngay)));
                    break;
                case "2":
                    quanlyphi.Controls.Add(new LiteralControl(LoadNgayNop28(ngay)));
                    break;
                case "13":
                    quanlyphi.Controls.Add(new LiteralControl(AnThang()));
                    break;
            }
        }
        catch(Exception ex)
        { }

    }

    public string LoadNgayNop31(int Ngay)
    {
        string output_html = "";
        for (int i = 1; i <= 31; i++)
        {
            if (i == Ngay)
                output_html += @"<option selected=""selected"" value='" + i + "'>" + i + "</option>";
            else
                output_html += @"<option value='" + i + "'>" + i + "</option>";
        }
        return output_html;
    }

    public string LoadNgayNop30(int Ngay)
    {
        string output_html = "";
        if (Ngay <= 30)
        {
            for (int i = 1; i <= 30; i++)
            {
                if (i == Ngay)
                    output_html += @"<option selected=""selected"" value='" + i + "'>" + i + "</option>";
                else
                    output_html += @"<option value='" + i + "'>" + i + "</option>";
            }
        }
        else
        {
            for (int i = 1; i < 30; i++)
                    output_html += @"<option value='" + i + "'>" + i + "</option>";
            output_html += @"<option selected=""selected"" value= 30>30</option>";
        }
        return output_html;
    }

    public string LoadNgayNop28(int Ngay)
    {
        string output_html = "";
        if (Ngay <= 28)
        {
            for (int i = 1; i <= 28; i++)
            {
                if (i == Ngay)
                    output_html += @"<option selected=""selected"" value='" + i + "'>" + i + "</option>";
                else
                    output_html += @"<option value='" + i + "'>" + i + "</option>";
            }
        }
        else
        {
            for (int i = 1; i < 28; i++)
                output_html += @"<option value='" + i + "'>" + i + "</option>";
            output_html += @"<option selected=""selected"" value= 28>28</option>";
        }
        return output_html;
    }

    public string AnThang()
    {
        string output_html = "$(this).attr('disabled', true)";
        
        return output_html;
    }

}