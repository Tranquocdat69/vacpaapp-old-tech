﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="thanhtoanphitapthe.ascx.cs" Inherits="usercontrols_thanhtoanphitapthe" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 15%;
    }
    .style2
    {
        width: 35%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }
    .style5
    {
        width: 30%;
    }
    .style6
    {
        width: 40%;
    }
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>


    <script type="text/javascript">

        function ganso() {
            $('.auto').autoNumeric('init');
        }

        function capnhattong() {
            document.getElementById('<%= btntong.ClientID %>').click();
        }

        function capnhattongcanhan() {
            document.getElementById('<%= btntongcanhan.ClientID %>').click();
        }

        function openKhachHang() {
            alert('Chức năng này đang hoàn thiện');
        }
        function thanhtoanchocanhan() {
            document.getElementById('<%= HienThanhToanCaNhan.ClientID %>').click();
        }

        function changeloaiphi() {
            document.getElementById('<%= Test.ClientID %>').click();
        }

        function xoachitiet() {
            if (confirm('Xác nhận xóa những dòng đã đánh dấu?'))
                document.getElementById('<%= btnxoa.ClientID %>').click();
        }

        function chonhoivien(id, ma) {
            document.getElementById('<%=HoiVienTapTheID.ClientID %>').value = id;
              document.getElementById('<%=MaHoiVienTapThe.ClientID %>').value = ma;
            $("#div_hoivien").dialog('close');
            document.getElementById('<%= Test.ClientID %>').click();
        }

        function chonhoiviencanhan() {
            $("#div_hoiviencanhan").dialog('close');
             document.getElementById('<%= btnChay.ClientID %>').click();
        }

        function luu() {
            document.getElementById('<%= btnTestLuu.ClientID %>').click();
        }

        function sua() {
            document.getElementById('<%= btnTestSua.ClientID %>').click();
        }

    </script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_thanhtoanphitapthe" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphitapthe" clientidmode="Static" runat="server"   method="post">
      <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
    <h4 class="widgettitle"><%=tenchucnang%></h4>
    <div id="thongbaoloi_form_thanhtoanphitapthe" name="thongbaoloi_form_thanhtoanphitapthe" style="display:none" class="alert alert-error"></div>
    <div height = "40px">
        <table id="tblThongBao" width="100%" border="0" class="formtbl">
        <tr>
         <td>
         <div ID="TinhTrangBanGhi" name = "TinhTrangBanGhi"></div>
        </td>
        </tr>
        </table>
    </div>
        <div><br /><b> Thông tin chung</b></div>
          <asp:UpdatePanel ID="UpdatePanelPhiTapThe" runat="server">
       <ContentTemplate>
      <table id="Table1" width="100%" border="0" class="formtbl" >
      <tr>
          <td class="style1"><asp:Label ID="lblMaGiaoDich" runat="server" Text = 'Mã giao dịch:' ></asp:Label></td>
        <td class="style1" ><input type="text" name="MaGiaoDich" id="MaGiaoDich" disabled = "disabled" ></td>
        <td class="style1"><asp:Label ID="lblIDKhachHang" runat="server" Text = 'ID.HVTT/ID.CTKT:' ></asp:Label>
          <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style1" >     
        <asp:HiddenField runat ="server" id="HoiVienTapTheID"></asp:HiddenField>   
        <asp:TextBox runat = "server" AutoPostBack = "true" OnTextChanged ="MaHoiVienTapThe_OnTextChanged" name="MaHoiVienTapThe" id="MaHoiVienTapThe"></asp:TextBox></td>
        <td class="style3"><a id="A1"  href="#none" class="btn btn-rounded" onclick="loadhoivien();" >...</a></td>
        <td><asp:Label ID="TenCongTy" name = "TenCongTy" runat = "server"></asp:Label></div>
            </td>
            <input type="hidden" value="0" id="hsubmit" name="hsubmit" /> 
      </tr>
          <tr>
              <td>Chọn năm</td><td><asp:DropDownList ID="drNamThanhToan" runat="server" DataTextField="Nam" DataValueField="Nam" OnSelectedIndexChanged="drNamThanhToan_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
              <td colspan="4">&nbsp;</td>
          </tr>
      </table>
           
        <div><br /><b> Các loại phí phải nộp của công ty</b></div>

         <div style = "display: none">
    <asp:LinkButton ID="btntong" runat="server" CssClass="btn" 
        onclick="btntong_OnClick"><i class="iconfa-search">
    </i>Ref</asp:LinkButton>
    </div>
           <div style = "display: none">
    <asp:LinkButton ID="HienThanhToanCaNhan" runat="server" CssClass="btn" 
        onclick="HienThanhToanCaNhan_Click"><i class="iconfa-search">
    </i>Ref</asp:LinkButton>
    </div>

             <div style = "display: none">
    <asp:LinkButton ID="btnChay" runat="server" CssClass="btn" 
        onclick="btnChay_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>

              <div style = "display: none">
    <asp:LinkButton ID="btntongcanhan" runat="server" CssClass="btn" 
        onclick="btntongcanhan_OnClick"><i class="iconfa-search">
    </i>Ref</asp:LinkButton>
    </div>

           <div style = "display: none">
    <asp:LinkButton ID="btnTestLuu" runat="server" CssClass="btn" 
        onclick="btnTestLuu_OnClick"><i class="iconfa-search">
    </i>btnTestLuu</asp:LinkButton>
    </div>

                  <div style = "display: none">
    <asp:LinkButton ID="btnTestSua" runat="server" CssClass="btn" 
        onclick="btnTestSua_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>

       <asp:GridView ClientIDMode="Static" ID="grvPhiCongTy" runat="server"
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="False" AllowSorting="True" 
       OnDataBound = "grvPhiCongTy_OnDataBound"
       DataKeyNames = "PhatSinhPhiID, PhiID, LoaiPhi"
       >
          <Columns>

             <asp:TemplateField ItemStyle-Width="30px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkEmp" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmp_CheckedChanged"  Enabled=<%# Eval("Active") %>></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>

               <asp:TemplateField >

                  <ItemTemplate>
                      <asp:Label ID = "TenLoaiPhi" name = "TenLoaiPhi" runat = "server" Text = <%# Eval("TenLoaiPhi")%>></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >

                  <ItemTemplate>
                      <asp:Label ID = "TongTien" name = "TongTien" runat = "server" Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval(truongsophiphainop))%>'></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <HeaderStyle Width="200px" />
                  <ItemTemplate>
                      <asp:Label ID = "TienNop" Name = "TienNop" runat="server"  Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TienNop"))%> '></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                          <HeaderStyle Width="200px" />
                  <ItemTemplate>
                      <asp:Label ID = "TienNo" Name = "TienNo"  runat="server" Text ='<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TienNo"))%>'></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

                <asp:TemplateField Visible = "false">
                  <ItemTemplate>
                      <asp:HiddenField ID = "PhatSinhPhiID" runat = "server" Value =<%#Eval("PhatSinhPhiID")%>/>
                  </ItemTemplate>
              </asp:TemplateField>

                <asp:TemplateField>
                  <ItemTemplate>
                      <asp:HiddenField ID = "PhiID" runat = "server" Value =<%#Eval("PhiID")%>/>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField>
                  <ItemTemplate>
                      <asp:HiddenField ID = "LoaiPhi" runat = "server" Value =<%#Eval("LoaiPhi")%>/>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
           <asp:Panel id ="PanelCaNhan" runat="server">
  <div><br /><b>Danh sách nhân viên của công ty</b></div>

                  <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  ChildrenAsTriggers = "true"  >
       <ContentTemplate>
            <div>
        <div  class="dataTables_length">
        <table>
        <tr>
            <td class = "style5">
            <a id="btn_them"  href="#none" class="btn btn-rounded" onclick="loadhoiviencanhan();"><i class="iconfa-plus-sign"></i> Thêm</a>
       <a id="btn_xoa_chitiet"  href="#none" class="btn btn-rounded" onclick="xoachitiet();"><i class="iconfa-remove-sign"></i> Xóa</a></td>

            <td class ="style6">&nbsp;</td>
</table>

        </div>
        
   </div>

   <div style="width: 100%; height: 400px; overflow: scroll">
   
            <div style="width: 100%; text-align: center; margin-top: 5px; display:none">
    <asp:LinkButton ID="Test" runat="server" CssClass="btn" 
        onclick="Test_Click" ><i class="iconfa-search">
            
    </i>Test</asp:LinkButton>
            </div>

       
            <div style="width: 100%; text-align: center; margin-top: 5px; display:none">
    <asp:LinkButton ID="btnxoa" runat="server" CssClass="btn" 
        onclick="btnxoa_Click" ><i class="iconfa-search">
            
    </i>Xóa</asp:LinkButton>
            </div>
      
       <asp:GridView ClientIDMode="Static" ID="thanhtoanphicanhan_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="False" AllowSorting="True" 
       onsorting="thanhtoanphicanhan_grv_Sorting"    
             DataKeyNames = "PhatSinhPhiID, PhiID, LoaiPhi, HoiVienId"
       >
          <Columns>     
                <asp:TemplateField ItemStyle-Width="30px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkboxSelectCaNhanAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectCaNhanAll_CheckedChanged"/>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkEmpCaNhan" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpCaNhan_CheckedChanged" Enabled=<%# Eval("Active") %>></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                <asp:TemplateField  >
                  <ItemTemplate>
                     <asp:Label name ="STT" ID ="STT" Text = <%#Eval(truongstt)%> runat ="server"></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField SortExpression="MaHoivien" >
              <ItemTemplate>
                 <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:open_hoivien_view(" + DataBinder.Eval(Container.DataItem, "HoiVienID").ToString() +"); "%>   Text= '<%# Eval(truongmahoivien) %>'  runat="server">
                    
                </asp:HyperLink>
        
             </ItemTemplate>
             <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
             <ItemStyle   HorizontalAlign="Center"     />
          </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truonghoten)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongsoccktv)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:BoundField DataField="NgayCapChungChiKTV"
                            ReadOnly="True"
                            DataFormatString="{0:dd/MM/yyyy}" />

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval("DienGiai")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

                <asp:TemplateField  >
                  <ItemTemplate>
                      <asp:Label name ="TongTien" ID ="TongTien" Text = <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TongTien"))%> runat ="server"></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <asp:Label name = "TienNop" ID = "TienNop" Text = <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TienNop"))%> runat ="server"></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <asp:Label name = "TienNo" ID = "TienNo" Text = <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval(truongsophiconno))%> runat ="server"></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <HeaderStyle Width="30px" />
                <ItemTemplate>
                    <asp:LinkButton ID="btnXoaGrid" runat="server" AutoPostBack="true" OnClientClick="return confirm('Xác nhận xóa dòng này?');" OnClick="btnXoaGrid_OnClick" data-placement='top' data-rel='tooltip' data-original-title='Xóa'  rel='tooltip' class='btn'><i class='iconsweets-trashcan' ></i></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>

                 <asp:TemplateField Visible = "false">
                  <ItemTemplate>
                      <asp:HiddenField ID = "PhatSinhPhiID" runat = "server" Value =<%#Eval("PhatSinhPhiID")%>/>
                  </ItemTemplate>
              </asp:TemplateField>

                <asp:TemplateField Visible = "false">
                  <ItemTemplate>
                      <asp:HiddenField ID = "PhiID" runat = "server" Value =<%#Eval("PhiID")%>/>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField Visible = "false">
                  <ItemTemplate>
                      <asp:HiddenField ID = "LoaiPhi" runat = "server" Value =<%#Eval("LoaiPhi")%>/>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField Visible = "false">
                  <ItemTemplate>
                      <asp:HiddenField ID = "HoiVienID" runat = "server" Value =<%#Eval("HoiVienID")%>/>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</div>
<div id = "div_thanhtoanphicanhan_chitiet"></div>
</ContentTemplate>
      </asp:UpdatePanel>     
               </asp:Panel>
       
<div  class='dataTables_length' style="display: none">

<asp:Button ID = "btnluu" name = "btnluu" runat = "server" Text = "Lưu" OnClick = "btnluu_OnClick" />
    <asp:Button ID = "btnsua" name = "btnsua" runat = "server" Text = "Sửa" OnClick = "btnsua_OnClick" />
    <asp:Button ID = "btntuchoi" name = "btntuchoi" runat = "server" Text = "Từ chối" OnClick = "btntuchoi_OnClick" />
    <asp:Button ID = "btnduyet" name = "btnduyet" runat = "server" Text = "Duyệt" OnClick = "btnduyet_OnClick" />
    <asp:Button ID = "btnthoaiduyet" name = "btnthoaiduyet" runat = "server" Text = "Thoái duyệt" OnClick = "btnthoaiduyet_OnClick" />
</div>

</ContentTemplate>
      </asp:UpdatePanel>    

    </form>
        <%LoadNut();%>
    <div id="div_danhsachphi_search" style="display:none" >
<form id="frm_submit" method="post" enctype="multipart/form-data" >
        
</form>
</div>
    <div id="div_thanhtoanphi_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?</p>

</div>

<div id="div_hoivien" style="display:none">
</div>
    <div id="div_hoiviencanhan" style="display:none">
              </div>           
<script type="text/javascript">

    // Array ID được check
    var thanhtoanphitapthe_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#thanhtoanphitapthe_grv .checkall").bind("click", function () {
            thanhtoanphitapthe_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#thanhtoanphitapthe_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) thanhtoanphitapthe_grv_selected.push($(this).val());
            });
        });

        $('#thanhtoanphitapthe_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(thanhtoanphitapthe_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) thanhtoanphitapthe_grv_selected.push($(this).val());
            });
        });
    });


    function submitform() {
        jQuery("#form_thanhtoanphitapthe").submit();
    }


    jQuery("#form_thanhtoanphitapthe").validate({
        rules: {
            NgayNop: {
                required: true
            },
            DienGiai: {
                required: true
            }
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();

            if (g) {
                var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                jQuery("#thongbaoloi_form_thanhtoanphitapthe").html(e).show()

            } else {
                jQuery("#thongbaoloi_form_thanhtoanphitapthe").hide()
            }
        }
    });

    $.datepicker.setDefaults($.datepicker.regional['vi']);

    // Xác nhận xóa nhiều
    function confirm_delete_thanhtoanphitapthe(idxoa) {
        if (idxoa == "") {
            $("#div_thanhtoanphi_canhbaonull").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Đóng lại": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $('#div_thanhtoanphi_canhbaonull').parent().find('button:contains("Đóng lại")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
        }
        else {
            $("#div_thanhtoanphi_delete").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Xóa": function () {
                        window.location = 'admin.aspx?page=quanlyphidanhsachphi&act=delete&id=' + idxoa;
                    },
                    "Bỏ qua": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $('#div_thanhtoanphi_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
            $('#div_thanhtoanphi_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');

        }
    }


    //function luu() {
    //    $('#hsubmit').val('1');
    //    jQuery("#form_thanhtoanphitapthe").submit();
    //    $('#hsubmit').val('0');
    //}

</script>

<script type="text/javascript">

    function open_hoivien_view(id) {
        var timestamp = Number(new Date());
        $("#div_thanhtoanphicanhan_chitiet").empty();
        $("#div_thanhtoanphicanhan_chitiet").append($("<iframe width='100%' height='100%' id='iframe_thanhtoanphicanhan_chitiet' name='iframe_thanhtoanphicanhan_chitiet' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=thanhtoanphicanhan_chitiet1&mode=iframe&time=" + timestamp + "&id=" + id));
        $("#div_thanhtoanphicanhan_chitiet").dialog({
            autoOpen: false,
            title: "<b>Thanh toán phí chi tiết</b>",
            modal: true,
            width: "960",
            height: "480",
            buttons: [
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
            ]
        }).dialog("open");
    }

    function xoa(id) {
        window.location = 'admin.aspx?page=thanhtoanphitapthe&id=' + id + '&mode=view&tinhtrang=xoa';
    };

    function duyet(id) {
        window.location = 'admin.aspx?page=thanhtoanphitapthe&id=' + id + '&mode=view&tinhtrang=duyet';
    };

    function tuchoi(id) {
        window.location = 'admin.aspx?page=thanhtoanphitapthe&id=' + id + '&mode=view&tinhtrang=tuchoi';
    };

    function thoaiduyet(id) {
        window.location = 'admin.aspx?page=thanhtoanphitapthe&id=' + id + '&mode=view&tinhtrang=thoaiduyet';
    };

    function guiemail(id) {
        window.location = 'admin.aspx?page=thanhtoanphitapthe&id=' + id + '&mode=view&tinhtrang=thoaiduyet';
    };

</script>

<script type="text/javascript">

</script>

<script type="text/javascript">
    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });
      
    <%LoadThongTin(); %>
</script>
<script type="text/javascript">
    <%AnNut(); %>  
</script>

<script type="text/javascript">
    function themthanhtoanphitapthechitiet() {
        var timestamp = Number(new Date());
        $("#div_thanhtoanphitapthe_chitiet").empty();
        $("#div_thanhtoanphitapthe_chitiet").append($("<iframe width='100%' height='100%' id='iframe_thanhtoanphitapthe_chitiet' name='iframe_thanhtoanphitapthe_chitiet' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=thanhtoanphitapthe_chitiet&mode=iframe&time=" + timestamp));
        $("#div_thanhtoanphitapthe_chitiet").dialog({
            autoOpen: false,
            title: "<b>Thanh toán phí chi tiết</b>",
            modal: true,
            width: "960",
            height: "640",
            buttons: [
                    {
                        text: "Lưu",
                        click: function () {
                            window.frames['iframe_thanhtoanphitapthe_chitiet'].submitform();
                            $(this).dialog("close");
                            fncsave();
                        }
                    },
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                    ]
        }).dialog("open");
    }

    function loadhoivien() {
        var timestamp = Number(new Date());
        $("#div_hoivien").empty();
        $("#div_hoivien").append($("<iframe width='100%' height='100%' id='iframe_hoivien' name='iframe_hoivien' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=danhsachhoivientapthe&mode=iframe&time=" + timestamp));
        $("#div_hoivien").dialog({
            autoOpen: false,
            title: "<b>Danh sách hội viên</b>",
            modal: true,
            width: "640",
            height: "480",
            buttons: [
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
            ]
        }).dialog("open");
    }
    function loadhoiviencanhan() {
        id = document.getElementById('<%= HoiVienTapTheID.ClientID%>').value;
        var timestamp = Number(new Date());
        $("#div_hoiviencanhan").empty();
        $("#div_hoiviencanhan").append($("<iframe width='100%' height='100%' id='iframe_hoiviencanhan' name='iframe_hoiviencanhan' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=danhsachoiviencanhan&type=thanhtoantapthe&id=" + id + "&mode=iframe&time=" + timestamp));
        $("#div_hoiviencanhan").dialog({
            autoOpen: false,
            title: "<b>Danh sách hội viên</b>",
            modal: true,
            width: "640",
            height: "480",
            buttons: [
                 {
                     text: "Chọn",
                     click: function () {
                         chonhoiviencanhan();
                     }
                 },
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
            ]
        }).dialog("open");
    }
  

</script>