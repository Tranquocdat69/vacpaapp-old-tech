﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BaoCaoDanhSachCongTyKiemToanTrongCaNuoc.ascx.cs" Inherits="usercontrols_BaoCaoDanhSachCongTyKiemToanTrongCaNuoc" %>
<%@ Register TagPrefix="CR" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<style type="text/css">
    /*.ui-datepicker
    {
        z-index: 1003 !important;
    }*/
    .ui-dropdownchecklist {
         z-index: 99999 !important;
        background: #ffffff !important;
        border: solid 1PX #cccccc!important;
        margin: 0px;
  height: 22px;
  width:270px;
  -webkit-appearance: menulist;
  box-sizing: border-box;
  border: 1px solid;
  border-image-source: initial;
  border-image-slice: initial;
  border-image-width: initial;
  border-image-outset: initial;
  border-image-repeat: initial;
  white-space: pre;
  -webkit-rtl-ordering: logical;
  color: black;
  background-color: white;
  cursor: default;
    }
    .ui-dropdownchecklist span {
        width:100%;
    }
    .ui-dropdownchecklist span span{
        width:100%;
    }
</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript" src="js/ui.dropdownchecklist-1.5-min.js"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<%--<script type="text/javascript" src="js/jquery-ui.min.js"></script>--%>

<form id="Form1" runat="server" clientidmode="Static">
    <asp:HiddenField ID="hi_LoaiHinh" runat="server" />
    <asp:HiddenField ID="hi_TinhThanh" runat="server" />
    <asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder>
    <h4 class="widgettitle">Danh sách công ty kiểm toán</h4>
    <fieldset class="fsBlockInfor" style="margin-top: 0px;">
        <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>

        <table width="100%" border="0" class="formtbl">
            <tr>
                <td style="width: 152px">
                    <asp:Label ID="lblLoaiHinhDoanhnghiep" runat="server" Text='Loại hình doanh nghiệp:'></asp:Label></td>
                <td colspan="3">
                    <select name="LoaiHinhDoanhNghiep" class="LoaiHinhDoanhNghiep" id="LoaiHinhDoanhNghiep" multiple="multiple" style="width: 100%">
                        <% 
                            try
                            {
                                LoadLoaiHinhDoanhNghiep(Request.Form["LoaiHinhDoanhNghiep"].ToString());
                            }
                            catch
                            {
                                LoadLoaiHinhDoanhNghiep("000");
                            }
                        %>
                    </select>
                </td>
                <td colspan="5">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 152px">
                    <asp:Label ID="Label16" runat="server" Text='Vùng miền:'></asp:Label></td>
                <td colspan="3">
                    <select name="VungMien" id="VungMien" multiple="multiple">
                        <%  
                            try
                            {
                                LoadVungMien(Request.Form["VungMien"].ToString());
                            }
                            catch
                            {
                                LoadVungMien("000");
                            }
                        %>
                    </select>
                </td>
                <td style="width: 152px">
                    <asp:Label ID="Label15" runat="server" Text='Tỉnh/Thành:'></asp:Label></td>
                <td colspan="3">
                    <select name="TinhThanh" id="TinhThanh">
                        <%  
                            try
                            {
                                LoadTinhThanh(Request.Form["VungMien"].ToString());
                            }
                            catch
                            {
                                LoadTinhThanh("00");
                            }
                        %>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="TinhTrangHoatDong" Text="Tình trạng hoạt động:"> </asp:Label></td>
                <td colspan="2">
                    <label>
                        <input type="checkbox" checked="checked" name="tinhtrang_danghoatdong" id="tinhtrang_danghoatdong" class="input-xlarge" />Đang hoạt động</label></td>
                <td>
                    <label>
                        <input type="checkbox" checked="checked"  name="tinhtrang_dunghoatdong" id="tinhtrang_dunghoatdong" class="input-xlarge" />Dừng hoạt động</label></td>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label1" Text="Đối tượng:"> </asp:Label></td>
                <td colspan="3">
                    <label>
                        <input type="checkbox" checked="checked" name="doituong_dudieukienkitlinhvucchungkhoan" id="doituong_dudieukienkitlinhvucchungkhoan" class="input-xlarge" />Đơn vị đủ ĐK KiT lĩnh vực chứng khoán</label></td>
                <td colspan="4">
                    <label>
                        <input type="checkbox" checked="checked" name="doituong_khongdudieukienkit" id="doituong_khongdudieukienkit" class="input-xlarge" />Đơn vị không đủ ĐK KiT đơn vị có lợi ích công chúng</label></td>
            </tr>
            <tr>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label2"> </asp:Label></td>
                <td colspan="3">
                    <label>
                        <input type="checkbox"  checked="checked" name="doituong_dudieukienkitcongchungkhac" id="doituong_dudieukienkitcongchungkhac" class="input-xlarge" />Đơn vị đủ ĐK KiT công chúng khác</label></td>
                <td colspan="4">
                    <label>
                        <input type="checkbox"  checked="checked" name="thanhvienhangkiemtoanquocte" id="thanhvienhangkiemtoanquocte" class="input-xlarge" />Công ty là thành viên Hãng kiểm toán quốc tế</label></td>
            </tr>
            <tr>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label3" Text="Vốn điều lệ:"> </asp:Label></td>
                <td>
                    <input type="text" id="vondieuletu" name="vondieuletu" value="<%=Request.Form["vondieuletu"]%>" class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="999999999999999999"></td>
                <td style="width: 20px">
                    <asp:Label runat="server" ID="Label5" Text="-"> </asp:Label></td>
                <td>
                    <input type="text" id="vondieuleden" name="vondieuleden" value="<%=Request.Form["vondieuleden"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="999999999999999999"></td>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label4" Text="Số lượng hội viên:"> </asp:Label></td>
                <td>
                    <input type="text" id="Text1" name="soluonghoivientu" value="<%=Request.Form["soluonghoivientu"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="9000000000"></td>
                <td style="width: 20px">
                    <asp:Label runat="server" ID="Label6" Text="-"> </asp:Label></td>
                <td>
                    <input type="text" id="Text2" name="soluonghoivienden" value="<%=Request.Form["soluonghoivienden"]%>" class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="9000000000"></td>
            </tr>
            <tr>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label7" Text="Doanh thu:"> </asp:Label></td>
                <td>
                    <input type="text" id="doanhthutu" name="doanhthutu" value="<%=Request.Form["doanhthutu"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="999999999999999999"></td>
                <td style="width: 20px">
                    <asp:Label runat="server" ID="Label8" Text="-"> </asp:Label></td>
                <td>
                    <input type="text" id="doanhthuden" name="doanhthuden" value="<%=Request.Form["doanhthuden"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="999999999999999999"></td>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label9" Text="Số lượng kiểm toán viên:"> </asp:Label></td>
                <td>
                    <input type="text" id="soluongkiemtoanvientu" name="soluongkiemtoanvientu" value="<%=Request.Form["soluongkiemtoanvientu"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="9000000000"></td>
                <td style="width: 20px">
                    <asp:Label runat="server" ID="Label10" Text="-"> </asp:Label></td>
                <td>
                    <input type="text" id="soluongkiemtoanvienden" name="soluongkiemtoanvienden" value="<%=Request.Form["soluongkiemtoanvienden"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="9000000000"></td>
            </tr>
            <tr>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label11" Text="Ngày thành lập:"> </asp:Label></td>
                <td>
                    <input type="text" id="ngaythanhlaptu" name="ngaythanhlaptu" value="<%=Request.Form["ngaythanhlaptu"]%>" /></td>
                <td style="width: 20px">
                    <asp:Label runat="server" ID="Label12" Text="-"> </asp:Label></td>
                <td>
                    <input type="text" id="ngaythanhlapden" name="ngaythanhlapden" value="<%=Request.Form["ngaythanhlapden"]%>" /></td>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label13" Text="Số lượng người lao động:"> </asp:Label></td>
                <td>
                    <input type="text" id="soluongnguoilaodongtu" name="soluongnguoilaodongtu" value="<%=Request.Form["soluongnguoilaodongtu"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="9000000000"></td>
                <td style="width: 20px">
                    <asp:Label runat="server" ID="Label14" Text="-"> </asp:Label></td>
                <td>
                    <input type="text" id="soluongnguoilaodongden" name="soluongnguoilaodongden" value="<%=Request.Form["soluongnguoilaodongden"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="9000000000"></td>
            </tr>
            <tr>
                <td colspan="4"></td>
                <td style="width: 152px">
                    <asp:Label runat="server" ID="Label17" Text="Số lượng khách hàng:"> </asp:Label></td>
                <td>
                    <input type="text" id="soluongkhachhangtu" name="soluongkhachhangtu" value="<%=Request.Form["soluongkhachhangtu"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="9000000000"></td>
                <td style="width: 20px">
                    <asp:Label runat="server" ID="Label18" Text="-"> </asp:Label></td>
                <td>
                    <input type="text" id="soluongkhachhangden" name="soluongkhachhangden" value="<%=Request.Form["soluongkhachhangden"]%>"  class = "auto" data-a-sep="." data-a-dec="," data-v-min="0"
                        data-v-max="9000000000"></td>
            </tr>
        </table>

        <div class="dataTables_length" style="text-align: center;">
            <a id="btnSave" href="javascript:;" class="btn" onclick="View();"><i class="iconfa-plus-sign"></i>Xem thông tin</a>

            <a id="A2" href="javascript:;"
                class="btn btn-rounded" onclick="View('word');"><i class="iconsweets-word2"></i>Kết
                xuất Word</a>
            <a id="A3" href="javascript:;" class="btn btn-rounded" onclick="View('pdf');">
                <i class="iconsweets-pdf2"></i>Kết xuất PDF</a>
<%--            <a id="A4" href="javascript:;" class="btn btn-rounded" onclick="View('excel');">
                <i class="iconsweets-excel2"></i>Kết xuất Excel</a>--%>
            <input type="hidden" id="hdAction" name="hdAction" />
        </div>

    </fieldset>

    <fieldset class="fsBlockInfor" style="width:100%">
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server"
            GroupTreeImagesFolderUrl="" Height="50px" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
            Width="350px" EnableDrillDown="False" HasCrystalLogo="False" HasDrilldownTabs="False"
            HasDrillUpButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False"
            HasToggleParameterPanelButton="False" HasZoomFactorList="False"
            ToolPanelView="None" SeparatePages="False" />
    </fieldset>


</form>

<script type="text/javascript">



    //        $(document).ready(function () {
    //            $("#NgayBatDau").datepicker();
    //            $("#NgayKetThuc").datepicker();

    //            $(".NgayBatDau").datepicker({
    //                dateFormat: 'mm-dd-yy'
    //            }).datepicker('setDate', new Date())

    $(function () {
        $('#ngaythanhlaptu, #ngaythanhlapden').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-500:+0',
            onClose: function (dateText, inst) {
                $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
            }
        }).datepicker('option', '', '');
    });
    $(function () { $('#ngaythanhlapden').datepicker('option', 'minDate', $('#ngaythanhlaptu').val())});
    $('#ngaythanhlaptu').change(function () {

        $('#ngaythanhlapden').datepicker('option', 'minDate', $('#ngaythanhlaptu').val());
    });

    //  });

</script>

<script type="text/javascript">


    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function () {
        $('#NgayBatDau, #NgayKetThuc').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-75:+25',
            onClose: function (dateText, inst) {
                $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
            }
        }).datepicker('option', '', '');

        $('#NgayBatDau').datepicker('setDate', new Date())
        $('#NgayKetThuc').datepicker('setDate', new Date())
    });

</script>

<%--gọi function in bao cao--%>
<script type="text/javascript">
    function View(type) {
        <%--setdataCrystalReport();--%>
        $('#hdAction').val(type);
      
        $('#Form1').submit();
    }

    //$("#LoaiHinhDoanhNghiep").dropdownchecklist({ emptyText: "Tất cả"});
    //$("#VungMien").dropdownchecklist({ emptyText: "Tất cả" });
  
    $("#LoaiHinhDoanhNghiep").dropdownchecklist({ firstItemChecksAll: true });
    $("#VungMien").dropdownchecklist({ firstItemChecksAll: true });
    
    <% LoadCheckBox();%>

   
    var timestamp = Number(new Date());
       //  cm.chondiaphuong_script(Request.Form["VungMien"].ToString(),3); 
    $('#VungMien').change(function() {
        $('#TinhThanh').html('');
        $('#TinhThanh').load('noframe.aspx?page=chondiaphuong&default=0&type=tt&id_vungmien='+$('#VungMien').val()+'&time='+timestamp);	 
        });
    
    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });
</script>
