﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="thongbaonopphicapnhatkienthuc.ascx.cs" Inherits="usercontrols_thongbaonopphicapnhatkienthuc" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 15%;
    }
    .style2
    {
        width: 35%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }

</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript">

    function guiemail() {
        if (grvthongbaophihoivien_canhan_selected == '' && grvthongbaophihoivien_tapthe_selected == '') {
            $('#div_thongbaonull').dialog({
                resizable: false,
                modal: true,
                buttons: {
                    'OK': function () {
                        $(this).dialog('close');
                    }
                }
            });
            $('#div_thongbaonull').parent().find('button:contains("OK")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
        }
        else if ($("#MaLopHoc").val() == "") {
            alert('Cần điền mã lớp học');
        }
        else if ($("#ThoiHanNopPhiCaNhan").val() == "") {
            alert('Cần điền thời hạn nộp phí');
        }
        else {
            var ngaylap = $("#NgayLap").val();
            var thoihan = $("#ThoiHanNopPhiCaNhan").val();
            var iLopHocId = document.getElementById('<%=LopHocID.ClientID %>').value;
            window.location = 'admin.aspx?page=guimail&emailcanhan=' + grvthongbaophihoivien_canhan_selected + '&emailtapthe=' + grvthongbaophihoivien_tapthe_selected + '&module=thongbaonopphicnkt&phiidcanhan=<%=PhiIDCaNhan%>&ngaylap=' + ngaylap + '&thoihan='+thoihan+'&lophocid=' + iLopHocId;
        }
    }

    function thongbaothanhcong() {
        $('#div_thongbaoguimailthanhcong').dialog({
            resizable: false,
            modal: true,
            buttons: {
                'OK': function () {
                    $(this).dialog('close');
                }
            }
        });
        $('#div_thongbaoguimailthanhcong').parent().find('button:contains("OK")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function thongbaoloi() {
        jQuery('#div_thongbaoguimailloi').dialog({
            resizable: false,
            modal: true,
            buttons: {
                'OK': function () {
                    jQuery(this).dialog('close');
                }
            }
        });
        jQuery('#div_thongbaoguimailloi').parent().find('button:contains("OK")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

</script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_thongbaophi" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thongbaophi" clientidmode="Static" runat="server"   method="post">
    <h4 class="widgettitle">Thông báo nộp phí cập nhật kiến thức</h4>
    <div id="thongbaoloi_form_thongbaophi" name="thongbaoloi_form_thongbaophi" style="display:none" class="alert alert-error"></div>
    <div height = "40px">
        <table id="tblThongBao" width="100%" border="0" class="formtbl">
        <tr>
         <td>
         <div ID="TinhTrangBanGhi" name = "TinhTrangBanGhi"></div>
        </td>
        </tr>
        </table>
    </div>
        <div><br /><b> Thông tin chung</b></div>
      <table id="Table1" width="100%" border="0" class="formtbl">
          <tr>
       <td class="style1"><asp:Label ID="Label2" runat="server" Text = 'Mã lớp học:' ></asp:Label>
          <asp:Label ID="Label3" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2" ><asp:HiddenField runat="server" ID ="LopHocID"/>
        <asp:TextBox runat = "server" AutoPostBack = "true" OnTextChanged ="MaLopHoc_OnTextChanged" name="MaLopHoc" id="MaLopHoc"></asp:TextBox></td>
        <td class="style3"><a id="A1"  href="#none" class="btn btn-rounded" onclick="loadlophoc();" >...</a></td>
        <td class="style1" ><asp:Label ID="Label8" runat="server" Text = 'Tên lớp học:' ></asp:Label></td>
        <td class="style2"><asp:Label ID="TenLopHoc" name = "TenLopHoc" runat = "server"></asp:Label>
            </td>
      </tr>
      <tr>
       <td class="style1"><asp:Label ID="Label5" runat="server" Text = 'Thời hạn nộp phí:' ></asp:Label>
          <asp:Label ID="Label7" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2" ><input type="text" name="ThoiHanNopPhiCaNhan" id="ThoiHanNopPhiCaNhan">
        <input type="hidden" name="PhiIDCaNhan" id="PhiIDCaNhan"></td>
          <td class="style3">&nbsp;</td>
        <td class="style1" ><asp:Label ID="lblNgayNop" runat="server" Text = 'Ngày lập:' ></asp:Label>
            <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2"><input type="text" name="NgayLap" id="NgayLap"/>
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> </td>
      </tr>
       <tr>
          <td class="style1"><asp:Label ID="Label6" runat="server" Text = 'Ghi chú:' ></asp:Label></td>
        <td colspan ="4"><input width="100%" type="text" name="GhiChu" id="GhiChu"></td>        
      </tr>
      </table>
        
         <div><br /><b>Phí hội viên tổ chức</b></div>
     <table id="Table2" width="100%" border="0" class="formtbl">
      <tr>
      <td><input type="hidden" name="PhiIDTapThe" id="PhiIDTapThe"></td>
          <td class="style1" ><asp:Label ID="Label10" runat="server" Text = 'Tìm kiếm:' ></asp:Label></td>
        <td class="style2"><input type="text" name="TimKiemTapThe" id="TimKiemTapThe" value="<%=Request.Form["TimKiemTapThe"]%>" onkeydown ="if (event.keyCode == 13) { $('#form_thongbaophi').submit();return false;}" /></td>
      </tr>
      </table>
      <div style="width: 100%; height: 400px; overflow: scroll">
       <asp:GridView ClientIDMode="Static" ID="grvThongBaoPhiCongTy" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="false" AllowSorting="True" 
       style = "width: 1650px"
           onsorting="grvThongBaoPhiCongTy_Sorting"
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval(id_congty)%>" />
                  </ItemTemplate>
              </asp:TemplateField>

             <asp:TemplateField  >
                  <ItemTemplate>
                        <span><%# Container.DataItemIndex + 1%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(ma_tapthe)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(ten_congty)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(tenviettat_congty)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</div>

<div>

<asp:DropDownList ID="PagerTapThe" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientaitapthe').val(this.value); $('#user_search').submit();" 
                                        visible ="false">
                                     </asp:DropDownList>
</div>
        
        
        

 <div><br /><b>Danh sách học viên</b></div>
 <table id="Table3" width="100%" border="0" class="formtbl" >
      <tr>
      <td></td>
      <td class="style1" ><asp:Label ID="Label4" runat="server" Text = 'Tìm kiếm:' ></asp:Label></td>
        <td class="style2"><input type="text" name="TimKiemCaNhan" id="TimKiemCaNhan" value="<%=Request.Form["TimKiemCaNhan"]%>" onkeydown ="if (event.keyCode == 13) { $('#form_thongbaophi').submit();return false;}" /></td>
      </tr>
      </table>
      
            <div style="width: 100%; text-align: center; margin-top: 5px; display:none">
    <asp:LinkButton ID="btnChonLopHoc" runat="server" CssClass="btn" 
        onclick="btnChonLopHoc_Click" ><i class="iconfa-search">
            
    </i>Test</asp:LinkButton>
            </div>

<div style="width: 100%; height: 400px; overflow: scroll">
       <asp:GridView ClientIDMode="Static" ID="grvthongbaophihoivien_canhan" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="false" AllowSorting="True" 
       style = "width: 1100px"
           onsorting="grvthongbaophihoivien_canhan_Sorting"

       >
           <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
              <HeaderStyle Width="50px" />
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval(id_canhan)%>" />
                  </ItemTemplate>
              </asp:TemplateField>

             <asp:TemplateField  >
             <HeaderStyle Width="50px" />
                  <ItemTemplate>
                     <span><%# Container.DataItemIndex + 1%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  >
               <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(ma_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(ten_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(sochungchi_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

                <asp:BoundField DataField="ngaycapchungchiKTV" 
                            ReadOnly="True"
                            DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="150px"/>

               <asp:TemplateField  >
               <HeaderStyle Width="200px" />
                  <ItemTemplate>
                      <span><%# Eval(donvicongtac_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
               <HeaderStyle Width="200px" />
                  <ItemTemplate>
                      <span><%# Eval(quequan_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(hoiphiconnonamtruoc_canhan, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</div>
<div>

<asp:DropDownList ID="PagerCaNhan" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientaicanhan').val(this.value); $('#user_search').submit();" 
                                        visible ="false">
                                     </asp:DropDownList>


</div>

<%LoadNut(); %>


    </form>

<script type ="text/javascript">

    //   $('#TimKiemCaNhan').keypress(function(e) {
    //              if (e.keyCode == '13') {
    //                 e.preventDefault();
    //                 alert('hoho');
    //               }
    //               });​
</script>
                         
<script type="text/javascript">
<%annut(); %>
       var grvthongbaophihoivien_canhan_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#grvthongbaophihoivien_canhan .checkall").bind("click", function () {
            grvthongbaophihoivien_canhan_selected = new Array();
            checked = $(this).prop("checked");
            $('#grvthongbaophihoivien_canhan :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) grvthongbaophihoivien_canhan_selected.push($(this).val());
            });
        });

        $('#grvthongbaophihoivien_canhan :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(grvthongbaophihoivien_canhan_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) grvthongbaophihoivien_canhan_selected.push($(this).val());
            });
        });
    });

    var grvthongbaophihoivien_tapthe_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#grvThongBaoPhiCongTy .checkall").bind("click", function () {
            grvthongbaophihoivien_tapthe_selected = new Array();
            checked = $(this).prop("checked");
            $('#grvThongBaoPhiCongTy :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) grvthongbaophihoivien_tapthe_selected.push($(this).val());
            });
        });

        $('#grvThongBaoPhiCongTy :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(grvthongbaophihoivien_tapthe_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) grvthongbaophihoivien_tapthe_selected.push($(this).val());
            });
        });
    });

        function submitform() {        
                jQuery("#form_thongbaophi").submit();
        }


        jQuery("#form_thongbaophi").validate({
            rules: {
                NgayNop: {
                    required: true
                },
            },
            invalidHandler: function (f, d) {
                var g = d.numberOfInvalids();

                if (g) {
                    var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                    jQuery("#thongbaoloi_form_thongbaophi").html(e).show()

                } else {
                    jQuery("#thongbaoloi_form_thongbaophi").hide()
                }
            }
        });

    $.datepicker.setDefaults($.datepicker.regional['vi']);

    $(function () {  $('#ThoiHanNopPhiCaNhan, #NgayLap').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });

    $(function () {$('#NgayLap').datepicker('setDate', new Date())});

    <%LoadThoiGian(); %>

        function export_excel()
    {
        $('#ketxuat').val('1');
        $('#form_thongbaophi').submit();
        $('#ketxuat').val('0');
    }

        function loadlophoc() {
            var timestamp = Number(new Date());
            $("#div_lophoc").empty();
            $("#div_lophoc").append($("<iframe width='100%' height='100%' id='iframe_lophoc' name='iframe_lophoc' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=danhsachlophoc&mode=iframe&time=" + timestamp));
            $("#div_lophoc").dialog({
                autoOpen: false,
                title: "<b>Danh sách lớp học</b>",
                modal: true,
                width: "500",
                height: "380",
                buttons: [
                        {
                            text: "Đóng",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                ]
            }).dialog("open");
        }

        function chonlophoc(id, ma) {
            document.getElementById('<%=LopHocID.ClientID %>').value = id;
         document.getElementById('<%=MaLopHoc.ClientID %>').value = ma;
            $("#div_lophoc").dialog('close');
         document.getElementById('<%= btnChonLopHoc.ClientID %>').click();
        }
        
</script>


<div id="div_lophoc" style="display:none">
</div>
<div id="div_danhsachphi_search" style="display:none" >
<form id="user_search" method="post" enctype="multipart/form-data" >

</form>
</div>
<div id="div_thongbaoguimailthanhcong" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Gửi email thành công</p>

</div>

<div id="div_thongbaoguimailloi" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Gửi email thất bại</p>

</div>

<div id="div_thongbaonull" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Không có hội viên được chọn</p>

</div>
