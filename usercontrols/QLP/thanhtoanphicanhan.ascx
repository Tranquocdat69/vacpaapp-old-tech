﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="thanhtoanphicanhan.ascx.cs" Inherits="usercontrols_thanhtoanphicanhan" %>

<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 15%;
    }
    .style2
    {
        width: 35%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }
    .style5
    {
        width: 30%;
    }
    .style6
    {
        width: 40%;
    }

</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript">

    function xoachitiet() {

        if (confirm('Xác nhận xóa những dòng đã đánh dấu?'))
        document.getElementById('<%= btntong.ClientID %>').click();
    }

    function changeloaiphi()
    {
        document.getElementById('<%= Test.ClientID %>').click();
    }

    function chonhoiviencanhan() {        
        document.getElementById('<%= btnChay.ClientID %>').click();
    }

    function luu() {
        document.getElementById('<%= btnTestLuu.ClientID %>').click();
    }

    function sua() {
        document.getElementById('<%= btnTestSua.ClientID %>').click();
     }
    </script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">
      <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
    <h4 class="widgettitle"><%=tenchucnang%></h4>
    <div id="thongbaoloi_form_thanhtoanphicanhan" name="thongbaoloi_form_thanhtoanphicanhan" style="display:none" class="alert alert-error"></div>
    <div height = "40px">
        <table id="tblThongBao" width="100%" border="0" class="formtbl">
        <tr>
         <td>
         <div ID="TinhTrangBanGhi" name = "TinhTrangBanGhi"></div>
        </td>
        </tr>
        </table>
    </div>
        <div><br /><b> Thông tin chung</b></div>
      <table id="Table1" width="100%" border="0" class="formtbl" >
      <tr>
          <td class="style1"><asp:Label ID="lblMaGiaoDich" runat="server" Text = 'Mã giao dịch:' ></asp:Label></td>
        <td class="style2" ><input type="text" name="MaGiaoDich" id="MaGiaoDich" disabled = "disabled" ></td>
        <td class="style1" ><asp:Label ID="lblNgayNop" runat="server" Text = 'Ngày nộp:' ></asp:Label>
            <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2"><input type="text" name="NgayNop" id="NgayNop"/></td>
      </tr>
       <tr>
          <td class="style1"><asp:Label ID="Label6" runat="server" Text = 'Diễn giải:' ></asp:Label>
            </td>
        <td colspan ="3"><input width="100%" type="text" name="DienGiai" id="DienGiai"></td>        
      
      </tr>
      
      </table>
     <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"  ChildrenAsTriggers = "true"  >
       <ContentTemplate>
       <div>
        <div  class="dataTables_length">
        <table>
        <tr>
            <td class = "style5" colspan="3">
           <a id="btn_them"  href="#none" class="btn btn-rounded" onclick="loadhoiviencanhan();"><i class="iconfa-plus-sign"></i> Thêm</a>
        <a id="btn_xoa_chitiet"  href="#none" class="btn btn-rounded" onclick="xoachitiet();"><i class="iconfa-remove-sign"></i> Xóa</a></td>
</tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
            
            <td class ="style6">HVCN ID: <asp:TextBox ID="txtTimKiem_ID" runat="server" onkeydown ="if (event.keyCode == 13) { changeloaiphi();return false;}"></asp:TextBox></td>
            <td class ="style6">Họ và tên: <asp:TextBox ID="txtTimKiem_Ten" runat="server" onkeydown ="if (event.keyCode == 13) { changeloaiphi();return false;}"></asp:TextBox></td>
            <td class ="style5"> <asp:DropDownList id ="LoaiPhi" name ="LoaiPhi" DataTextField ="TEN" DataValueField="ID" runat ="server" onchange="changeloaiphi();"></asp:DropDownList></td>
         </tr>
          
        </table>

        </div>
        
   </div>


           
   <div><br /><b>Danh sách nộp phí</b></div>
   <div style="width: 100%; height: 400px; overflow: scroll">
   
            <div style="width: 100%; text-align: center; margin-top: 5px; display:none">
    <asp:LinkButton ID="Test" runat="server" CssClass="btn" 
        onclick="Test_Click" ><i class="iconfa-search">
            
    </i>Test</asp:LinkButton>

                <asp:LinkButton ID="btntong" runat="server" CssClass="btn" 
        onclick="btntong_OnClick" ><i class="iconfa-search">
             </i>Tong</asp:LinkButton>
            </div>

       <div style = "display: none">
    <asp:LinkButton ID="btnChay" runat="server" CssClass="btn" 
        onclick="btnChay_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>
       <div style = "display: none">
    <asp:LinkButton ID="btnTestLuu" runat="server" CssClass="btn" 
        onclick="btnTestLuu_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>

       <div style = "display: none">
    <asp:LinkButton ID="btnTestSua" runat="server" CssClass="btn" 
        onclick="btnTestSua_Click"><i class="iconfa-search">
    </i>btnChay</asp:LinkButton>
    </div>
       
       <div style = "display: none">
    <asp:LinkButton ID="btnGuiMail" runat="server" CssClass="btn" 
        onclick="btnGuiMail_Click"><i class="iconfa-search">
    </i>btnGuiMail</asp:LinkButton>
    </div>


       <asp:GridView ClientIDMode="Static" ID="thanhtoanphicanhan_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="False" AllowSorting="True" 
       onpageindexchanging="thanhtoanphicanhan_grv_PageIndexChanging" 
           onsorting="thanhtoanphicanhan_grv_Sorting"
              DataKeyNames = "PhatSinhPhiID, PhiID, LoaiPhi, HoiVienId"
       >
          <Columns>
               <asp:TemplateField ItemStyle-Width="30px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" Checked="true"/>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="Chon" runat="server" AutoPostBack="true" OnCheckedChanged="Chon_CheckedChanged"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>

             <asp:TemplateField  >
                  <ItemTemplate>
                     <asp:Label name ="STT" ID ="STT" Text = <%#Eval(truongstt)%> runat ="server"></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField SortExpression="MaHoivien" >
              <ItemTemplate>
                 <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:open_hoivien_view(" + DataBinder.Eval(Container.DataItem, "HoiVienID").ToString() +"); "%>   Text= '<%# Eval(truongmahoivien) %>'  runat="server">
                    
                </asp:HyperLink>
        
             </ItemTemplate>
             <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
             <ItemStyle   HorizontalAlign="Center"     />
          </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truonghoten)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongsoccktv)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongdonvicongtac)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval("DienGiai")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                   <HeaderStyle Width="100px" />
                  <ItemTemplate>
                      <asp:Label name ="TongTien" ID ="TongTien" Text = <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TongTien"))%> runat ="server"></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                   <HeaderStyle Width="100px" />
                  <ItemTemplate>
                      <asp:Label name = "TienNop" ID = "TienNop" Text = <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval("TienNop"))%> runat ="server"></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                   <HeaderStyle Width="100px" />
                  <ItemTemplate>
                      <asp:Label name = "TienNo" ID = "TienNo" Text = <%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", Eval(truongsophiconno))%> runat ="server"></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <HeaderStyle Width="30px" />
                <ItemTemplate>
                    <asp:LinkButton ID="btnXoaGrid" runat="server" AutoPostBack="true" OnClientClick="return confirm('Xác nhận xóa dòng này?');" OnClick="btnXoaGrid_OnClick" data-placement='top' data-rel='tooltip' data-original-title='Xóa'  rel='tooltip' class='btn cancel'><i class='iconsweets-trashcan' ></i></asp:LinkButton>                     

                </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</div>
<div id = "div_thanhtoanphicanhan_chitiet"></div>
           <div id = "div_hoiviencanhan"></div>
</ContentTemplate>
      </asp:UpdatePanel>     

<%LoadNut(); %>
    </form>

    <div id="div_thanhtoanphi_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?</p>

</div>

<div id="div_thanhtoanphi_canhbaonull" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Cần chọn bản ghi trước khi thao tác</p>

</div>
    
                         
<script type="text/javascript">
    // Array ID được check
    var thanhtoanphicanhan_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#thanhtoanphicanhan_grv .checkall").bind("click", function () {
            thanhtoanphicanhan_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#thanhtoanphicanhan_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) thanhtoanphicanhan_grv_selected.push($(this).val());
            });
        });

        $('#thanhtoanphicanhan_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(thanhtoanphicanhan_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) thanhtoanphicanhan_grv_selected.push($(this).val());
            });
        });
    });

    function ganso() {
        $('.auto').autoNumeric('init');
    }
        function submitform() {        
                jQuery("#form_thanhtoanphicanhan").submit();
        }


        jQuery("#form_thanhtoanphicanhan").validate({
            rules: {
                NgayNop: {
                    required: true
                },
                 DienGiai: {
                    required: true
                }
            },
            invalidHandler: function (f, d) {
                var g = d.numberOfInvalids();

                if (g) {
                    var e = g == 1 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                    jQuery("#thongbaoloi_form_thanhtoanphicanhan").html(e).show()

                } else {
                    jQuery("#thongbaoloi_form_thanhtoanphicanhan").hide()
                }
            }
        });

        $.datepicker.setDefaults($.datepicker.regional['vi']);

        // Xác nhận xóa nhiều
        function confirm_delete_thanhtoanphicanhan(idxoa) {            
            if (idxoa == "") {
                $("#div_thanhtoanphi_canhbaonull").dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Đóng lại": function () {
                            $(this).dialog("close");
                        }
                    }
                });
                $('#div_thanhtoanphi_canhbaonull').parent().find('button:contains("Đóng lại")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
            }
            else {
                $("#div_thanhtoanphi_delete").dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Xóa": function () {
                            window.location = 'admin.aspx?page=thanhtoanphicanhan&act=delete&id=' + idxoa;
                        },
                        "Bỏ qua": function () {
                            $(this).dialog("close");
                        }
                    }
                });
                $('#div_thanhtoanphi_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
                $('#div_thanhtoanphi_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');

            }
        }

</script>

<script type="text/javascript">

    function funConfirm() {
        if (confirm('Xác nhận xóa dòng này?'))
            return true;
        else
            return false;
    }

    function open_hoivien_view(id) {
        var timestamp = Number(new Date());
        $("#div_thanhtoanphicanhan_chitiet").empty();
        $("#div_thanhtoanphicanhan_chitiet").append($("<iframe width='100%' height='100%' id='iframe_thanhtoanphicanhan_chitiet' name='iframe_thanhtoanphicanhan_chitiet' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=thanhtoanphicanhan_chitiet1&mode=iframe&time=" + timestamp+"&id="+id));
        $("#div_thanhtoanphicanhan_chitiet").dialog({
            autoOpen: false,
            title: "<b>Thanh toán phí chi tiết</b>",
            modal: true,
            width: "960",
            height: "480",
            buttons: [
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
            ]
        }).dialog("open");
    }

    //function luu() {
    //    jQuery("#form_thanhtoanphicanhan").submit();
    //};

    function xoa(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=xoa';
    };

    function duyet(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=duyet';
    };

    function tuchoi(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=tuchoi';
    };

    function thoaiduyet(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=thoaiduyet';
    };

    function guiemail(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=guiemail';
    };

</script>

<script type="text/javascript">

</script>

<script type="text/javascript">
    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });
      
    <%LoadThongTin(); %>
</script>
<script type="text/javascript">
    <%AnNut(); %>  
</script>

<script type="text/javascript">
    function themthanhtoanphicanhanchitiet() {
        var timestamp = Number(new Date());
        $("#div_thanhtoanphicanhan_chitiet").empty();
        $("#div_thanhtoanphicanhan_chitiet").append($("<iframe width='100%' height='100%' id='iframe_thanhtoanphicanhan_chitiet' name='iframe_thanhtoanphicanhan_chitiet' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=thanhtoanphicanhan_chitiet&mode=iframe&time=" + timestamp));
        $("#div_thanhtoanphicanhan_chitiet").dialog({
            autoOpen: false,
            title: "<b>Thanh toán phí chi tiết</b>",
            modal: true,
            width: "960",
            height: "640",
            buttons: [
                    {
                        text: "Lưu",
                        click: function () {
                            window.frames['iframe_thanhtoanphicanhan_chitiet'].submitform();
                            $(this).dialog("close");
                            fncsave();
                        }
                    },
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                    ]
        }).dialog("open");
            }

            function fncsave() {
                document.getElementById('<%= Test.ClientID %>').click();
            }

    function loadhoiviencanhan() {
        var timestamp = Number(new Date());
        $("#div_hoiviencanhan").empty();
        $("#div_hoiviencanhan").append($("<iframe width='100%' height='100%' id='iframe_hoiviencanhan' name='iframe_hoiviencanhan' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=danhsachoiviencanhan&type=thanhtoancanhan&mode=iframe&time=" + timestamp));
        $("#div_hoiviencanhan").dialog({
            autoOpen: false,
            title: "<b>Danh sách hội viên</b>",
            modal: true,
            width: "640",
            height: "480",
            buttons: [
                 {
                     text: "Chọn",
                     click: function () {
                         $(this).dialog("close");
                         chonhoiviencanhan();
                     }
                 },
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
            ]
        }).dialog("open");
    }
    
    function guiemail() {
        document.getElementById('<%= btnGuiMail.ClientID %>').click();
}

</script>