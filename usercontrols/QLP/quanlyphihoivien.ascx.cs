﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using System.Configuration;

public partial class usercontrols_quanlyphihoivien : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();

    public string quyen = "DMPhiHoiVien";

    public string tenchucnang = "Phí hội viên";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";

    public int iTinhTrang = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            //Thêm mới
            //Khi thêm mới thì không có request id,  mã phí          
            if (string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                if (string.IsNullOrEmpty(Request.Form["MaPhi"]) && !string.IsNullOrEmpty(Request.Form["TenPhi"]) && !string.IsNullOrEmpty(Request.Form["MucPhi"]))
                {
                    //Chạy function tự sinh mã phí
                    string Nam = "";
                    if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                        Nam = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).Year.ToString();
                    Nam = Nam.Substring(2);

                    int DoiTuongApDung = Convert.ToInt32(Request.Form["DoiTuongApDung"]);

                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "select [dbo].[LaySoChayDanhMucPhiHoiVienCaNhan] (" + DoiTuongApDung + "," + Nam + ")";
                    DataSet ds = new DataSet();
                    ds = DataAccess.RunCMDGetDataSet(sql);

                    string MaPhi = ds.Tables[0].Rows[0][0].ToString();

                    string strMucPhi = Request.Form["MucPhi"];

                    var modified = new StringBuilder();
                    foreach (char c in strMucPhi)
                    {
                        if (Char.IsDigit(c) || c == ',')
                            modified.Append(c);
                    }

                    decimal MucPhi = decimal.Parse(modified.ToString());
                    DateTime? NgayLap1 = null;
                    if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                        NgayLap1 = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime? NgayApDung1 = null;
                    if (!string.IsNullOrEmpty(Request.Form["NgayApDung"]))
                        NgayApDung1 = DateTime.ParseExact(Request.Form["NgayApDung"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    DateTime? NgayHetHieuLuc1 = null;
                    if (!string.IsNullOrEmpty(Request.Form["NgayHetHieuLuc"]))
                        NgayHetHieuLuc1 = DateTime.ParseExact(Request.Form["NgayHetHieuLuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    DateTime NgayNhap = DateTime.Now;

                    int? ThangNop = null;
                    if (!string.IsNullOrEmpty(Request.Form["ThangNop"]))
                        ThangNop = Convert.ToInt32(Request.Form["ThangNop"]);

                    sql = new SqlCommand();

                    sql.CommandText = "INSERT INTO tblTTDMPhiHoiVien(MaPhi, NgayLap, TenPhi, NgayApDung, NgayHetHieuLuc, LoaiHoiVien, LoaiHoiVienChiTiet, DoiTuongApDungID, "
                    + "MucPhi, DonViThoiGian, ThoiHanThang, ThoiHanNgay, TinhTrangID, NgayNhap, NguoiNhap) " +
                    " VALUES (@MaPhi, @NgayLap, @TenPhi, @NgayApDung, @NgayHetHieuLuc, @LoaiHoiVien, @LoaiHoiVienChiTiet, @DoiTuongApDung, @MucPhi, @DonViThoiGian, @ThoiHanThang, @ThoiHanNgay, @TinhTrangID, @NgayNhap, @NguoiNhap)";
                    sql.Parameters.AddWithValue("@MaPhi", MaPhi);
                    if (NgayLap1 != null)
                        sql.Parameters.AddWithValue("@NgayLap", NgayLap1);
                    else
                        sql.Parameters.AddWithValue("@NgayLap", DBNull.Value);
                    sql.Parameters.AddWithValue("@TenPhi", Request.Form["TenPhi"]);
                    if (NgayApDung1 != null)
                        sql.Parameters.AddWithValue("@NgayApDung", NgayApDung1);
                    else
                        sql.Parameters.AddWithValue("@NgayApDung", DBNull.Value);
                    if (NgayHetHieuLuc1 != null)
                        sql.Parameters.AddWithValue("@NgayHetHieuLuc", NgayHetHieuLuc1);
                    else
                        sql.Parameters.AddWithValue("@NgayHetHieuLuc", DBNull.Value);
                    sql.Parameters.AddWithValue("@LoaiHoiVien ", Request.Form["selLoaiHoiVien"]);
                    sql.Parameters.AddWithValue("@LoaiHoiVienChiTiet ", Request.Form["selLoaiHoiVienChiTiet"]);
                    sql.Parameters.AddWithValue("@DoiTuongApDung ", Request.Form["DoiTuongApDung"]);
                    sql.Parameters.AddWithValue("@MucPhi", MucPhi);
                    sql.Parameters.AddWithValue("@DonViThoiGian", Request.Form["DonViThoiGian"]);
                    if (ThangNop != null)
                        sql.Parameters.AddWithValue("@ThoiHanThang", ThangNop);
                    else
                        sql.Parameters.AddWithValue("@ThoiHanThang", DBNull.Value);
                    sql.Parameters.AddWithValue("@ThoiHanNgay", Request.Form["NgayNop"]);
                    sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                    sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                    sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);
                    DataAccess.RunActionCmd(sql);

                    sql.CommandText = "SELECT PhiID FROM tblTTDMPhiHoiVien WHERE MaPhi = '" + MaPhi + "'";
                    string phiid = DataAccess.DLookup(sql);

                    for (int i = 1; i <= Convert.ToInt32(Request.Form["somucphi"]); i++)
                    {
                        add_phichitiet(phiid, i);
                    }

                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog("DMPhiHoiVien", "Thêm giá trị \"" + MaPhi + "\" vào danh mục TTDMPhiHoiVien");

                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                }
            }
            else
            {
                if (Request.QueryString["mode"] == "edit") //Sửa
                {
                    if (!string.IsNullOrEmpty(Request.Form["TenPhi"]))
                    {
                        string strTenPhi = Request.Form["TenPhi"];
                        string strMucPhi = Request.Form["MucPhi"];

                        var modified = new StringBuilder();
                        foreach (char c in strMucPhi)
                        {
                            if (Char.IsDigit(c) || c == ',')
                                modified.Append(c);
                        }

                        decimal MucPhi = decimal.Parse(modified.ToString());
                        DateTime? NgayLap1 = null;
                        if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                            NgayLap1 = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime? NgayApDung1 = null;
                        if (!string.IsNullOrEmpty(Request.Form["NgayApDung"]))
                            NgayApDung1 = DateTime.ParseExact(Request.Form["NgayApDung"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        DateTime? NgayHetHieuLuc1 = null;
                        if (!string.IsNullOrEmpty(Request.Form["NgayHetHieuLuc"]))
                            NgayHetHieuLuc1 = DateTime.ParseExact(Request.Form["NgayHetHieuLuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        DateTime NgayNhap = DateTime.Now;

                        int? ThangNop = null;
                        if (!string.IsNullOrEmpty(Request.Form["ThangNop"]))
                            ThangNop = Convert.ToInt32(Request.Form["ThangNop"]);

                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);

                        sql.CommandText = @"UPDATE tblTTDMPhiHoiVien set NgayLap =@NgayLap, TenPhi = @TenPhi,
                        NgayApDung = @NgayApDung, NgayHetHieuLuc = @NgayHetHieuLuc, LoaiHoiVien = @LoaiHoiVien, LoaiHoiVienChiTiet = @LoaiHoiVienChiTiet, DoiTuongApDungID = @DoiTuongApDung,
                        MucPhi = @MucPhi, DonViThoiGian = @DonViThoiGian, ThoiHanThang = @ThoiHanThang,
                        ThoiHanNgay =@ThoiHanNgay, TinhTrangID = @TinhTrangID, NgayNhap = @NgayNhap, 
                        NguoiNhap =@NguoiNhap
                        WHERE PhiID = " + Request.QueryString["id"];
                        if (NgayLap1 != null)
                            sql.Parameters.AddWithValue("@NgayLap", NgayLap1);
                        else
                            sql.Parameters.AddWithValue("@NgayLap", DBNull.Value);
                        sql.Parameters.AddWithValue("@TenPhi", Request.Form["TenPhi"]);
                        if (NgayApDung1 != null)
                            sql.Parameters.AddWithValue("@NgayApDung", NgayApDung1);
                        else
                            sql.Parameters.AddWithValue("@NgayApDung", DBNull.Value);
                        if (NgayHetHieuLuc1 != null)
                            sql.Parameters.AddWithValue("@NgayHetHieuLuc", NgayHetHieuLuc1);
                        else
                            sql.Parameters.AddWithValue("@NgayHetHieuLuc", DBNull.Value);
                        sql.Parameters.AddWithValue("@LoaiHoiVien ", Request.Form["selLoaiHoiVien"]);
                        sql.Parameters.AddWithValue("@LoaiHoiVienChiTiet ", Request.Form["selLoaiHoiVienChiTiet"]);
                        sql.Parameters.AddWithValue("@DoiTuongApDung ", Request.Form["DoiTuongApDung"]);
                        sql.Parameters.AddWithValue("@MucPhi", MucPhi);
                        sql.Parameters.AddWithValue("@DonViThoiGian", Request.Form["DonViThoiGian"]);
                        if (ThangNop != null)
                            sql.Parameters.AddWithValue("@ThoiHanThang", ThangNop);
                        else
                            sql.Parameters.AddWithValue("@ThoiHanThang", DBNull.Value);
                        sql.Parameters.AddWithValue("@ThoiHanNgay", Request.Form["NgayNop"]);
                        sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                        sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                        sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);
                        DataAccess.RunActionCmd(sql);

                        sql.CommandText = "DELETE tblTTDMPhiHoiVienChiTiet WHERE PhiID = " + Request.QueryString["id"];
                        DataAccess.RunActionCmd(sql);

                        for (int i = 1; i <= Convert.ToInt32(Request.Form["somucphi"]); i++)
                        {
                            add_phichitiet(Request.QueryString["id"], i);
                        }


                        sql.Connection.Close();
                        sql.Connection.Dispose();
                        sql = null;

                        cm.ghilog("DMPhiHoiVien", "Sửa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" vào danh mục TTDMPhiHoiVien");

                        Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                    }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["tinhtrang"]))//Các hành động từ chối, duyệt, thoái duyệt, xóa
                {
                    //Từ chối
                    if (Request.QueryString["tinhtrang"] == "tuchoi")
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                            sql.CommandText = "UPDATE tblTTDMPhiHoiVien set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                            sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("DMPhiHoiVien", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiHoiVien");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "xoa") //Xóa
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt, thoái duyệt, từ chối thì mới cho xóa
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet || Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.KhongPheDuyet || Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                        {
                            sql.CommandText = "DELETE tblTTDMPhiHoiVienChiTiet WHERE PhiID = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);

                            sql.CommandText = "DELETE tblTTDMPhiHoiVien WHERE PhiId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);

                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("DMPhiHoiVien", "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiHoiVien");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "duyet") //duyệt
                    {

                        SqlCommand sql1 = new SqlCommand();
                        sql1.CommandText = "SELECT TinhTrangId, MaPhi, DoiTuongApDungID, MucPhi, LoaiHoiVienChiTiet from tblTTDMPhiHoiVien where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql1);

                        string connStr = DataAccess.ConnString;
                        SqlConnection conPhiHoiVien = new SqlConnection(connStr);
                        SqlTransaction transPhiHoiVien;
                        conPhiHoiVien.Open();
                        transPhiHoiVien = conPhiHoiVien.BeginTransaction();

                        //Phải ở trạng thái chờ duyệt thì mới cho duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                            try
                            {

                                SqlCommand sql = new SqlCommand();
                                sql.CommandText =
                                    "UPDATE tblTTDMPhiHoiVien set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet = @NgayDuyet  WHERE PhiId = @PhiID";
                                sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                                sql.Connection = conPhiHoiVien;
                                sql.Transaction = transPhiHoiVien;
                                sql.ExecuteNonQuery();

                                //Lấy toàn bộ Hội viên cá nhân/tổ chức

                                DataTable dtbHoiVienChuaPhatSinhPhi = new DataTable();
                                DataTable dtbHoiVienDaPhatSinhPhi = new DataTable();

                                //Hội viên cá nhân làm việc tại CTKT: xác định theo HoiVienTapTheID
                                //Hội viên tổ chức đủ đk KiT lĩnh vực CK: xác định theo truong NamDuDKKT_CK

                                // SonNQ update: gộp lại thành HVCN, ko phân biệt có thuộc HVTC hay ko
                                if (ds.Tables[0].Rows[0][2].ToString() ==
                                    ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString())
                                {
                                    sql1.CommandText =
                                               @"select HoiVienCaNhanId as HoiVienID, LoaiHoiVienCaNhan, CONVERT(VARCHAR(10), NgayGiaNhap, 103) AS NgayGiaNhap from tblHoiVienCaNhan where LoaiHoiVienCaNhanChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienCaNhanID NOT IN" +
                                               " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                               (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                               " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                               " ) and PhiID = " + Request.QueryString["id"] + ") and LoaiHoiVienCaNhan = " +
                                               (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan;
                                    dtbHoiVienChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                    sql1.CommandText =
                                        @"select HoiVienCaNhanId as HoiVienID, LoaiHoiVienCaNhan, CONVERT(VARCHAR(10), NgayGiaNhap, 103) AS NgayGiaNhap from tblHoiVienCaNhan where LoaiHoiVienCaNhanChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienCaNhanID IN" +
                                        " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                               (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                               " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                               " ) and PhiID = " + Request.QueryString["id"] + ") and LoaiHoiVienCaNhan = " +
                                               (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan;
                                    dtbHoiVienDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                }
                                //else if (ds.Tables[0].Rows[0][2].ToString() ==
                                //     ((int)EnumVACPA.DoiTuongApDung.HvcnKhongLamViecTaiCtkt).ToString())
                                //{
                                //    sql1.CommandText =
                                //               @"select HoiVienCaNhanId as HoiVienID, LoaiHoiVienCaNhan from tblHoiVienCaNhan where HoiVienCaNhanID NOT IN" +
                                //               " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                //               (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                //               " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                //               " ) and PhiID = " + Request.QueryString["id"] + ") and LoaiHoiVienCaNhan = " +
                                //               (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + " and (HoiVienTapTheId IS NULL OR HoiVienTapTheID NOT IN (SELECT HoiVienTapTheID from tblHoiVienTapThe))";
                                //    dtbHoiVienChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                //    sql1.CommandText =
                                //        @"select HoiVienCaNhanId as HoiVienID, LoaiHoiVienCaNhan from tblHoiVienCaNhan where HoiVienCaNhanID IN" +
                                //        " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                //               (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                //               " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                //               " ) and PhiID = " + Request.QueryString["id"] + ") and LoaiHoiVienCaNhan = " +
                                //               (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + " and (HoiVienTapTheId IS NULL OR HoiVienTapTheID NOT IN (SELECT HoiVienTapTheID from tblHoiVienTapThe))";
                                //    dtbHoiVienDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                //}
                                else if (ds.Tables[0].Rows[0][2].ToString() ==
                                         ((int)EnumVACPA.DoiTuongApDung.HvttDuDieuKienKiTLinhVucChungKhoan).ToString())
                                {

                                    sql1.CommandText =
                                               @"select HoiVienTapTheId as HoiVienID, LoaiHoiVienTapThe, CONVERT(VARCHAR(10), ISNULL(NgayGiaNhap, NgayNhap), 103) AS NgayGiaNhap from tblHoiVienTapThe where LoaiHoiVienTapTheChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienTapTheChaID IS NULL AND HoiVienTapTheId NOT IN" +
                                               " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                               (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                               " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe +
                                               " ) and PhiID = " + Request.QueryString["id"] + ") and LoaiHoiVienTapThe = " + (int)EnumVACPA.LoaiHoiVienHoiVienCaNhanTapThe.HoiVienTapThe
                                               + " and NamDuDKKT_CK IS NOT NULL and NamDuDKKT_CK <> 0 and NamDuDKKT_CK <> ''";
                                    dtbHoiVienChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                    sql1.CommandText =
                                        @"select HoiVienTapTheId as HoiVienID, LoaiHoiVienTapThe, CONVERT(VARCHAR(10), ISNULL(NgayGiaNhap, NgayNhap), 103) AS NgayGiaNhap from tblHoiVienTapThe where LoaiHoiVienTapTheChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienTapTheChaID IS NULL AND HoiVienTapTheId IN" +
                                         " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                               (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                               " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe +
                                               " ) and PhiID = " + Request.QueryString["id"] + ") and LoaiHoiVienTapThe = " + (int)EnumVACPA.LoaiHoiVienHoiVienCaNhanTapThe.HoiVienTapThe
                                               + " and NamDuDKKT_CK IS NOT NULL and NamDuDKKT_CK <> 0 and NamDuDKKT_CK <> ''";
                                    dtbHoiVienDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                }
                                else if (ds.Tables[0].Rows[0][2].ToString() ==
                                         ((int)EnumVACPA.DoiTuongApDung.HvttKhongDuDieuKienKiTLinhVucChungKhoan).ToString())
                                {
                                    sql1.CommandText =
                                            @"select HoiVienTapTheId as HoiVienID, LoaiHoiVienTapThe, CONVERT(VARCHAR(10), ISNULL(NgayGiaNhap, NgayNhap), 103) AS NgayGiaNhap from tblHoiVienTapThe where LoaiHoiVienTapTheChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienTapTheChaID IS NULL AND HoiVienTapTheId NOT IN" +
                                            " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                            (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                            " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe +
                                            " ) and PhiID = " + Request.QueryString["id"] + ") and LoaiHoiVienTapThe = " + (int)EnumVACPA.LoaiHoiVienHoiVienCaNhanTapThe.HoiVienTapThe
                                            + " and (NamDuDKKT_CK IS NULL OR NamDuDKKT_CK = 0 OR NamDuDKKT_CK = '')";
                                    dtbHoiVienChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                    sql1.CommandText =
                                        @"select HoiVienTapTheId as HoiVienID, LoaiHoiVienTapThe, CONVERT(VARCHAR(10), ISNULL(NgayGiaNhap, NgayNhap), 103) AS NgayGiaNhap from tblHoiVienTapThe where LoaiHoiVienTapTheChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienTapTheChaID IS NULL AND HoiVienTapTheId IN" +
                                         " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                            (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                            " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe +
                                            " ) and PhiID = " + Request.QueryString["id"] + ") and LoaiHoiVienTapThe = " + (int)EnumVACPA.LoaiHoiVienHoiVienCaNhanTapThe.HoiVienTapThe
                                            + " and (NamDuDKKT_CK IS NULL OR NamDuDKKT_CK = 0 OR NamDuDKKT_CK = '')";
                                    dtbHoiVienDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                }
                                //Tạo bảng Phát sinh Phí
                                DataTable dtbPhatSinhPhi = new DataTable("tblTTPhatSinhPhi");
                                dtbPhatSinhPhi.Columns.Add("HoiVienID", typeof(int));
                                dtbPhatSinhPhi.Columns.Add("LoaiHoiVien", typeof(string));
                                dtbPhatSinhPhi.Columns.Add("PhiID", typeof(int));
                                dtbPhatSinhPhi.Columns.Add("DoiTuongPhatSinhPhiID", typeof(int));
                                dtbPhatSinhPhi.Columns.Add("LoaiPhi", typeof(string));
                                dtbPhatSinhPhi.Columns.Add("NgayPhatSinh", typeof(DateTime));
                                dtbPhatSinhPhi.Columns.Add("SoTien", typeof(double));


                                string strLoaiHoiVien = "1";
                                if (ds.Tables[0].Rows[0][2].ToString() ==
                                   ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString()
                                   ||
                                   ds.Tables[0].Rows[0][2].ToString() ==
                                   ((int)EnumVACPA.LoaiHoiVien.KiemToanVien).ToString())
                                {
                                    strLoaiHoiVien = ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString();
                                }
                                else if (ds.Tables[0].Rows[0][2].ToString() ==
                                        ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString()
                                        ||
                                        ds.Tables[0].Rows[0][2].ToString() ==
                                        ((int)EnumVACPA.LoaiHoiVien.CongTyKiemToan).ToString())
                                {
                                    strLoaiHoiVien = ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString();
                                }

                                SqlCommand cmd = new SqlCommand();
                                //Đưa dữ liệu Hội viên chưa phát sinh phí vào bảng Phát sinh Phí
                                foreach (DataRow dtr in dtbHoiVienChuaPhatSinhPhi.Rows)
                                {
                                    DataRow dtrPhatSinhPhi = dtbPhatSinhPhi.NewRow();
                                    dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                    //dtrPhatSinhPhi["LoaiHoiVien"] = ds.Tables[0].Rows[0][2];
                                    dtrPhatSinhPhi["LoaiHoiVien"] = strLoaiHoiVien;
                                    dtrPhatSinhPhi["PhiID"] = Request.QueryString["id"].ToString();
                                    dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                    dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiHoiVien;
                                    dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                    if (!string.IsNullOrEmpty(dtr["NgayGiaNhap"].ToString()))
                                    {
                                        cmd.CommandText = "SELECT MucPhiChiTiet FROM tblTTDMPhiHoiVienChiTiet WHERE PhiID = " + Request.QueryString["id"].ToString() + " AND TuNgay <= CONVERT(datetime, '" + dtr["NgayGiaNhap"].ToString() + "', 103) AND DenNgay >= CONVERT(datetime, '" + dtr["NgayGiaNhap"].ToString() + "', 103)";
                                        string mucphi = DataAccess.DLookup(cmd);

                                        if(!string.IsNullOrEmpty(mucphi))
                                        {
                                            dtrPhatSinhPhi["SoTien"] = (int.Parse(ds.Tables[0].Rows[0][3].ToString()) /100 * (int.Parse(mucphi))).ToString();
                                        }
                                        else
                                            dtrPhatSinhPhi["SoTien"] = ds.Tables[0].Rows[0][3];
                                    }
                                    else
                                        dtrPhatSinhPhi["SoTien"] = ds.Tables[0].Rows[0][3];
                                    dtbPhatSinhPhi.Rows.Add(dtrPhatSinhPhi);
                                }

                                //Copy vào Db
                                //BulkCopy the data in the DataTable to the temp table
                                using (
                                    SqlBulkCopy s = new SqlBulkCopy(conPhiHoiVien, SqlBulkCopyOptions.Default, transPhiHoiVien)
                                    )
                                //using (SqlBulkCopy s = new SqlBulkCopy(DataAccess.ConnString, SqlBulkCopyOptions.Default))
                                {
                                    s.BulkCopyTimeout = 99999;
                                    s.DestinationTableName = dtbPhatSinhPhi.TableName;

                                    foreach (var column in dtbPhatSinhPhi.Columns)
                                    {
                                        s.ColumnMappings.Add(column.ToString(), column.ToString());
                                    }
                                    s.WriteToServer(dtbPhatSinhPhi);
                                }

                                //Tạo DataTable Phát sinh phí Update
                                DataTable dtbPhatSinhPhiUpdate = new DataTable("tblTTPhatSinhPhi");
                                dtbPhatSinhPhiUpdate.Columns.Add("PhatSinhPhiID", typeof(int));
                                dtbPhatSinhPhiUpdate.Columns.Add("HoiVienID", typeof(int));
                                dtbPhatSinhPhiUpdate.Columns.Add("LoaiHoiVien", typeof(string));
                                dtbPhatSinhPhiUpdate.Columns.Add("PhiID", typeof(int));
                                dtbPhatSinhPhiUpdate.Columns.Add("DoiTuongPhatSinhPhiID", typeof(int));
                                dtbPhatSinhPhiUpdate.Columns.Add("LoaiPhi", typeof(string));
                                dtbPhatSinhPhiUpdate.Columns.Add("NgayPhatSinh", typeof(DateTime));
                                dtbPhatSinhPhiUpdate.Columns.Add("SoTien", typeof(double));

                                //Đưa dữ liệu Hội viên đã phát sinh Phí vào DataTable Phát sinh Phí update
                                foreach (DataRow dtr in dtbHoiVienDaPhatSinhPhi.Rows)
                                {
                                    DataRow dtrPhatSinhPhi = dtbPhatSinhPhiUpdate.NewRow();
                                    dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                    //dtrPhatSinhPhi["LoaiHoiVien"] = ds.Tables[0].Rows[0][2];
                                    dtrPhatSinhPhi["LoaiHoiVien"] = strLoaiHoiVien;
                                    dtrPhatSinhPhi["PhiID"] = Request.QueryString["id"].ToString();
                                    dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                    dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiHoiVien;
                                    dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                    if (!string.IsNullOrEmpty(dtr["NgayGiaNhap"].ToString()))
                                    {
                                        cmd.CommandText = "SELECT MucPhiChiTiet FROM tblTTDMPhiHoiVienChiTiet WHERE PhiID = " + Request.QueryString["id"].ToString() + " AND TuNgay <= CONVERT(datetime, '" + dtr["NgayGiaNhap"].ToString() + "', 103) AND DenNgay >= CONVERT(datetime, '" + dtr["NgayGiaNhap"].ToString() + "', 103)";
                                        string mucphi = DataAccess.DLookup(cmd);

                                        if (!string.IsNullOrEmpty(mucphi))
                                        {
                                            dtrPhatSinhPhi["SoTien"] = (int.Parse(ds.Tables[0].Rows[0][3].ToString()) / 100 * (int.Parse(mucphi))).ToString();
                                        }
                                        else
                                            dtrPhatSinhPhi["SoTien"] = ds.Tables[0].Rows[0][3];
                                    }
                                    else
                                        dtrPhatSinhPhi["SoTien"] = ds.Tables[0].Rows[0][3];
                                    dtbPhatSinhPhiUpdate.Rows.Add(dtrPhatSinhPhi);
                                }

                                //Update vào Db

                                //Tạo bảng tạm Temp1 có cấu trúc giống hệ bảng vừa update
                                string strCapNhat = @"create table #Temp1(PhatSinhPhiID int
                                                                       ,HoiVienID int
                                                                       ,LoaiHoiVien nchar(1)
                                                                       ,PhiID int
                                                                       ,DoiTuongPhatSinhPhiID int
                                                                       ,LoaiPhi nchar(1)
                                                                       ,NgayPhatSinh date
                                                                       ,SoTien numeric(18,0)
                                                                       )";
                                SqlCommand cmdCapNhat = new SqlCommand(strCapNhat, conPhiHoiVien, transPhiHoiVien);
                                cmdCapNhat.CommandTimeout = 99999;
                                cmdCapNhat.ExecuteNonQuery();

                                //Đưa dữ liệu từ DataTable Phát sinh phí update vào bảng tạm Temp1
                                using (SqlBulkCopy bulk = new SqlBulkCopy(conPhiHoiVien, SqlBulkCopyOptions.Default, transPhiHoiVien))
                                {
                                    bulk.BulkCopyTimeout = 99999;
                                    bulk.DestinationTableName = "#Temp1";
                                    bulk.WriteToServer(dtbPhatSinhPhiUpdate);
                                }

                                //Thực hiện update dữ liệu giữa bảng Temp1 với bảng tblTTPhatSinhPhi
                                //string mergeSql1 = "merge into [dbo].[tblTTPhatSinhPhi] as Target " +
                                //                   "using #Temp1 as Source " +
                                //                   "on " +
                                //                   "Target.HoiVienID = Source.HoiVienID AND " +
                                //                   "Target.LoaiHoiVien = Source.LoaiHoiVien AND " +
                                //                   "Target.PhiID = Source.PhiID AND " +
                                //                   "Target.LoaiPhi = Source.LoaiPhi " +
                                //                   "when matched then " +
                                //                   "update set Target.SoTien = Source.SoTien," +
                                //                   "Target.NgayPhatSinh = GETDATE();";
                                string mergeSql1 = "merge into [dbo].[tblTTPhatSinhPhi] as Target " +
                                                        "using #Temp1 as Source " +
                                                        "on " +
                                                        "Target.HoiVienID = Source.HoiVienID AND " +
                                                        "Target.LoaiHoiVien COLLATE Latin1_General_CS_AS = Source.LoaiHoiVien COLLATE Latin1_General_CS_AS AND " +
                                                        "Target.PhiID = Source.PhiID AND " +
                                                        "Target.LoaiPhi COLLATE Latin1_General_CS_AS = Source.LoaiPhi COLLATE Latin1_General_CS_AS " +
                                                        "when matched then " +
                                                        "update set Target.SoTien = Source.SoTien," +
                                                        "Target.NgayPhatSinh = GETDATE();";
                                cmdCapNhat.CommandText = mergeSql1;
                                cmdCapNhat.ExecuteNonQuery();

                                //Clean up the temp table
                                cmdCapNhat.CommandText = "drop table #Temp1";
                                cmdCapNhat.ExecuteNonQuery();

                                transPhiHoiVien.Commit();
                                sql.Connection.Close();
                                sql.Connection.Dispose();
                                sql = null;
                            }
                            catch (Exception ex)
                            {
                                transPhiHoiVien.Rollback();
                                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
                                return;
                            }

                            sql1.Connection.Close();
                            sql1.Connection.Dispose();
                            sql1 = null;
                            cm.ghilog("DMPhiHoiVien", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiHoiVien");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "thoaiduyet") //Thoái duyệt
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Phải đang ở trạng thái duyệt thì mới cho thoái duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {
                            // sonnq sửa 15/12/2016, khi thoái duyệt xóa hết phát sinh phí hội viên
                            sql.CommandText = "DELETE tblTTPhatSinhPhi WHERE LoaiPhi = " + (int)EnumVACPA.LoaiPhi.PhiHoiVien + " AND PhiID = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);

                            sql.CommandText = "UPDATE tblTTDMPhiHoiVien set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                            sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("DMPhiHoiVien", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" vào danh mục TTDMPhiHoiVien");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "capnhatphatsinhphi") //Thoái duyệt
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, DoiTuongNopPhi from tblTTDMPhiHoiVien where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Phải đang ở trạng thái duyệt thì mới cho thoái duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {
                            //Lấy danh 
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    private void add_phichitiet(string phiid, int i)
    {
        SqlTtdmPhiHoiVienChiTietProvider phi_provider = new SqlTtdmPhiHoiVienChiTietProvider(cm.connstr, true, "");
        try
        {

            if (Request.Form["MucPhi_" + i.ToString()] != "")
            {
                TtdmPhiHoiVienChiTiet phi = new TtdmPhiHoiVienChiTiet();
                phi.PhiId = int.Parse(phiid);
                if (!string.IsNullOrEmpty(Request.Form["TuNgay_" + i.ToString()]))
                    phi.TuNgay = DateTime.ParseExact(Request.Form["TuNgay_" + i.ToString()], "dd/MM/yyyy", null);
                if (!string.IsNullOrEmpty(Request.Form["DenNgay_" + i.ToString()]))
                    phi.DenNgay = DateTime.ParseExact(Request.Form["DenNgay_" + i.ToString()], "dd/MM/yyyy", null);
                phi.MucPhiChiTiet = int.Parse(Request.Form["MucPhi_" + i.ToString()]);

                phi_provider.Insert(phi);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void LoadDoiTuong(int iDoiTuongID = 0)
    {
        string output_html = "";
        if (iDoiTuongID == 0)
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT distinct DoiTuongNopPhiID, TenDoiTuong, LoaiDoiTuong FROM tblDMDoiTuongNopPhi WHERE HienThi = 1";
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            int i = 0;
            foreach (DataRow dtr in ds.Tables[0].Rows)
            {
                i++;
                if (i == 1)
                    output_html += @"<option selected=""selected"" value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
                else
                    output_html += @"<option value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
            }
        }
        else
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT TenDoiTuongFROM tblDMDoiTuongNopPhi where DoiTuongNopPhiID = " + iDoiTuongID;
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            output_html += @"<option selected=""selected"" value='" + ds.Tables[0].Rows[0].ToString() + "'>" + ds.Tables[0].Rows[1].ToString() + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadThoiGian()
    {
        string output_html = "";
        output_html += @"<option selected=""selected"" value=""1"">Năm</option>";
        output_html += @"<option value=""3"">Tháng</option>";
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadNgayNop31()
    {
        string output_html = "";
        output_html += @"<option selected=""selected"" value=""1"">1</option>";
        for (int i = 2; i <= 31; i++)
        {
            output_html += @"<option value='" + i + "'>" + i + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadThangNop()
    {
        string output_html = "";
        output_html += @"<option selected=""selected"" value=""1"">1</option>";
        for (int i = 2; i <= 12; i++)
        {
            output_html += @"<option value='" + i + "'>" + i + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_luu' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_sua' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_xoa' href='#none' class='btn btn-rounded' onclick='xoa(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-sign'></i> Xóa</a>
            <a id='btn_duyet' href='#none' class='btn btn-rounded' onclick='duyet(" + Request.QueryString["id"] + @");'><i class='iconfa-ok'></i> Duyệt</a>
            <a id='btn_tuchoi' href='#none' class='btn btn-rounded' onclick='tuchoi(" + Request.QueryString["id"] + @");'><i class='iconfa-remove'></i> Từ chối</a>
        <a id='btn_thoaiduyet' href='#none' class='btn btn-rounded' onclick='thoaiduyet(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Thoái duyệt</a>";
        //<a id='btn_capnhatphatsinhphi' href='#none' class='btn btn-rounded' onclick='capnhatphatsinhphi(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Cập nhật phát sinh phí</a>
        output_html += "</div>";
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void AnNut()
    {
        Commons cm = new Commons();

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiHoiVien", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiHoiVien", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiHoiVien", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiHoiVien", cm.connstr).Contains("DUYET|"))
        {
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiHoiVien", cm.connstr).Contains("TUCHOI|"))
        {
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiHoiVien", cm.connstr).Contains("THOAIDUYET|"))
        {
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
        }
    }

    protected void LoadThongTin()
    {
        //Xem hoặc sửa
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
            if (Request.QueryString["mode"] == "view" || Request.QueryString["mode"] == "edit") //Xem hoặc sửa
            {
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select MaPhi, NgayLap, TenPhi, NgayApDung, NgayHetHieuLuc, LoaiHoiVien, LoaiHoiVienChiTiet, DoiTuongApDungID, "
                    + " MucPhi, DonViThoiGian, ThoiHanThang, ThoiHanNgay, TinhTrangID, TenTrangThai "
                    + " from tblTTDMPhiHoiVien inner join tblDMTrangThai on tblTTDMPhiHoiVien.TinhTrangID = tblDMTrangThai.TrangThaiID "
                + " where PhiId = " + Convert.ToInt16(Request.QueryString["id"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                dt = ds.Tables[0];
                int iTinhTrangBanGhi = 0;
                string strTinhTrangBanGhi = "";
                DateTime? NgayCoHieuLuc = new DateTime();
                DateTime? NgayHetHieuLuc = new DateTime();
                DateTime? NgayTraCuu = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                if (dt.Rows.Count > 0)
                {
                    Response.Write("$('#MaPhi').val('" + dt.Rows[0]["MaPhi"] + "');" + System.Environment.NewLine);
                    if (dt.Rows[0]["NgayLap"] != DBNull.Value)
                        Response.Write("$('#NgayLap').val('" + Convert.ToDateTime(dt.Rows[0]["NgayLap"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                    Response.Write("$('#TenPhi').val('" + dt.Rows[0]["TenPhi"] + "');" + System.Environment.NewLine);
                    if (dt.Rows[0]["NgayApDung"] != DBNull.Value)
                    {
                        Response.Write("$('#NgayApDung').val('" + Convert.ToDateTime(dt.Rows[0]["NgayApDung"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                        NgayCoHieuLuc = Convert.ToDateTime(dt.Rows[0]["NgayApDung"]);
                    }
                    else
                        NgayCoHieuLuc = null;
                    if (dt.Rows[0]["NgayHetHieuLuc"] != DBNull.Value)
                    {
                        Response.Write("$('#NgayHetHieuLuc').val('" + Convert.ToDateTime(dt.Rows[0]["NgayHetHieuLuc"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                        NgayHetHieuLuc = Convert.ToDateTime(dt.Rows[0]["NgayHetHieuLuc"]);
                    }
                    else
                        NgayHetHieuLuc = null;
                    Response.Write("$('#selLoaiHoiVien').val('" + dt.Rows[0]["LoaiHoiVien"] + "');" + System.Environment.NewLine);
                    Response.Write("$('#selLoaiHoiVienChiTiet').val('" + dt.Rows[0]["LoaiHoiVienChiTiet"] + "');" + System.Environment.NewLine);
                    Response.Write("$('#DoiTuongApDung').val('" + dt.Rows[0]["DoiTuongApDungID"] + "');" + System.Environment.NewLine);
                    Response.Write("$('#MucPhi').val('" + dt.Rows[0]["MucPhi"] + "');" + System.Environment.NewLine);
                    Response.Write("$('#DonViThoiGian').val('" + dt.Rows[0]["DonViThoiGian"].ToString().Trim() + "');" + System.Environment.NewLine);

                    //Với thời hạn tháng khác null thì load tháng và ngày bình thường
                    if (dt.Rows[0]["ThoiHanThang"] != DBNull.Value)
                    {
                        Response.Write("$('#ThangNop').val('" + dt.Rows[0]["ThoiHanThang"].ToString().Trim() + "');" + System.Environment.NewLine);

                        Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                        Response.Write(@"$('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=1')" + System.Environment.NewLine);
                    }
                    //Với thời hạn tháng = null => xử lý tháng và ngày
                    else
                    {
                        if (Request.QueryString["mode"] == "view")
                        {
                            Response.Write("$('#ThangNop').html('');" + System.Environment.NewLine);
                            Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                        }
                        else
                        {
                            Response.Write("$('#ThangNop').val('');" + System.Environment.NewLine);
                            Response.Write("$('#ThangNop').attr('disabled', true);" + System.Environment.NewLine);

                            Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                            Response.Write(@"$('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=1')" + System.Environment.NewLine);
                        }
                    }
                    iTinhTrangBanGhi = Convert.ToInt32(dt.Rows[0]["TinhTrangID"]);
                    strTinhTrangBanGhi = dt.Rows[0]["TenTrangThai"].ToString();

                }

                string strTinhTrangHieuLuc = "";
                if (NgayCoHieuLuc != null)
                {
                    if (NgayHetHieuLuc != null)
                    {
                        if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet && NgayCoHieuLuc <= NgayTraCuu && NgayHetHieuLuc >= NgayTraCuu)
                            strTinhTrangHieuLuc = "Có hiệu lực";
                        else if (NgayCoHieuLuc > NgayTraCuu)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (NgayHetHieuLuc < NgayTraCuu)
                            strTinhTrangHieuLuc = "Hết hiệu lực";
                        else
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                    }
                    else
                    {
                        if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet && NgayCoHieuLuc <= NgayTraCuu)
                            strTinhTrangHieuLuc = "Có hiệu lực";
                        else if (NgayCoHieuLuc > NgayTraCuu)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                    }
                }
                else
                {
                    strTinhTrangHieuLuc = "Chưa có hiệu lực";
                }
                //if (Request.QueryString["mode"] == "3")
                //{
                string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + strTinhTrangHieuLuc + @"</i>')
});";
                Response.Write(a + System.Environment.NewLine);
                //}
                //            else if (Request.QueryString["mode"] == "5")
                //            {
                //                string a = @"
                //            $(function () {
                //$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
                //$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + strTinhTrangHieuLuc + @"</i>')
                //});";
                //                Response.Write(a + System.Environment.NewLine);
                //            }


                if (Request.QueryString["mode"] == "view")
                {
                    Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                    Response.Write("$('#form_quanlyphihoivien input,select,textarea').attr('disabled', true);");

                    if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ChoDuyet)
                    {
                        //Hiện Từ chối, Duyệt, Xóa
                        //Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_capnhatphatsinhphi').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                    {
                        //Hiện thoái duyệt và cập nhật
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_capnhatphatsinhphi').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                    {
                        //Hiện Xóa
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_capnhatphatsinhphi').remove();" + System.Environment.NewLine);
                    }
                }

                    //Nếu đang ở chế độ sửa
                else if (Request.QueryString["mode"] == "edit")
                {
                    //Nếu tình trạng bản ghi là đã phê duyệt
                    //Thì không cho làm gì hết
                    if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                    {
                        Response.Write("$('#form_quanlyphihoivien input,select,textarea').attr('disabled', true);");
                        Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_capnhatphatsinhphi').remove();" + System.Environment.NewLine);

                    }
                    //Nếu tình trạng bản ghi không phải là đã phê duyệt => chỉ hiện sửa
                    else
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_capnhatphatsinhphi').remove();" + System.Environment.NewLine);
                        //Có thể sửa Ngày lập, ngày áp dụng
                        string output_html = @"$(function () {  $('#NgayLap, #NgayApDung').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-75:+25',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
                        Response.Write(output_html + System.Environment.NewLine);

                        //Có thể sửa Ngày hết hiệu lực
                        output_html = @"$(function () {  $('#NgayHetHieuLuc').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
yearRange: '-75:+25',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
                        Response.Write(output_html + System.Environment.NewLine);

                        //Bắt Ngày hết hiệu lực không được trước Ngày áp dụng
                        output_html = "$(function () { $('#NgayHetHieuLuc').datepicker('option', 'minDate', $('#NgayApDung').val())});";
                        Response.Write(output_html + System.Environment.NewLine);

                        //Khi Ngày Áp dụng thay đổi thì Ngày hết hiệu lực cũng xem xét lại điều kiện
                        output_html = @"$('#NgayApDung').change(function() {		
        
       $('#NgayHetHieuLuc').datepicker('option', 'minDate', $('#NgayApDung').val());
    });";
                        Response.Write(output_html + System.Environment.NewLine);

                    }
                }
            }
            //Nếu không phải 2 chế độ xem hoặc sửa
            else
            {
                Response.Write("$('#form_quanlyphihoivien input,select,textarea').attr('disabled', true);");
                Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_capnhatphatsinhphi').remove();" + System.Environment.NewLine);
            }
        }
        //Khi hiển thị các thông tin thêm mới
        else
        {
            string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + "Chờ duyệt" + @"</i>')
$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + "Chưa có hiệu lực" + @"</i>')
});";
            Response.Write(a + System.Environment.NewLine);

            string output_html = @"$(function () {  $('#NgayLap, #NgayApDung').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
yearRange: '-75:+25',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = @"$(function () { 
            var currentYear = (new Date).getFullYear(); 
            $('#NgayLap').datepicker('setDate', new Date())
$('#NgayApDung').datepicker('setDate', new Date((currentYear-1), 12, 01))
});";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = @"$(function () {  $('#NgayHetHieuLuc').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
yearRange: '-75:+25',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
            Response.Write(output_html + System.Environment.NewLine);

            output_html = "$(function () { $('#NgayHetHieuLuc').datepicker('option', 'minDate', $('#NgayApDung').val())});";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = @"$(function () { 
            var currentYear = (new Date).getFullYear(); 
$('#NgayHetHieuLuc').datepicker('setDate', new Date(currentYear, 11, 31))
});";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = @"$('#NgayApDung').change(function() {		
        
       $('#NgayHetHieuLuc').datepicker('option', 'minDate', $('#NgayApDung').val());
    });";
            Response.Write(output_html + System.Environment.NewLine);

            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_capnhatphatsinhphi').remove();" + System.Environment.NewLine);
        }
    }

    public void DoiNgay()
    {
        //Nếu Đơn vị thời gian thay đổi, xem đang chọn theo Tháng (3) hay Năm (1)
        //Với đơn vị là Tháng thì ẩn Tháng, chỉ hiện ngày (31 ngày)
        //Với đơn vị là năm thì hiện tháng và ngày.
        string output_html = @"
var timestamp = Number(new Date());

$('#DonViThoiGian').change(function() {		

if ($(this).val() == '3') {
            $('#ThangNop').attr('disabled', true);
 $('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=1');
        }
        else if ($(this).val() == '1') {
            $('#ThangNop').attr('disabled', false);
 $('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=' + $('#ThangNop').val());
        }
    });

$('#ThangNop').change(function() {		
        
        $('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=' + $('#ThangNop').val());
    });";
        Response.Write(output_html);
    }

    public void loadphichitiet()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            SqlCommand sql = new SqlCommand();


            int i, j;

            sql.CommandText = "Select CONVERT(VARCHAR(10), TuNgay, 103) AS TuNgay, CONVERT(VARCHAR(10), DenNgay, 103) AS DenNgay, MucPhiChiTiet From tblTTDMPhiHoiVienChiTiet WHERE PhiID=" + Request.QueryString["id"] + " ORDER BY PhiHoiVienChiTietID";
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);

            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                j = i + 1;
                Response.Write(@"<tr id=""dong_" + j + @"""><td valign=""middle"" align=""center"">" + j + @"</td><td>    <input type=""text"" style=""width:200px;"" id=""TuNgay_" + j + @""" name=""TuNgay_" + j + @""" value=""" + ds.Tables[0].Rows[i]["TuNgay"] + @""" >    </td><td> <input type=""text"" id=""DenNgay_" + j + @""" name=""DenNgay_" + j + @""" value=""" + ds.Tables[0].Rows[i]["DenNgay"] + @"""> </td><td> <input type=""text"" style=""width:80px;"" id=""MucPhi_" + j + @""" name=""MucPhi_" + j + @""" onkeypress=""return NumberOnly()"" value=""" + ds.Tables[0].Rows[i]["MucPhiChiTiet"] + @"""> </td><td align=""center"">  <a href=""#none"" class=""btn btn-rounded"" onclick=""if (confirm ('Xác nhận xóa dòng này?')) {sodong--; $('#dong_1').remove(); danhsodong();  }""><i class=""iconfa-remove-sign""></i> Xóa</a> </td></tr>");

            }
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds.Dispose();
        }
        else
        {
            Response.Write(@"<tr id=""dong_1"">
                                                    <td valign=""middle"" align=""center"">
                                                        1
                                                    </td>
                                                    <td>
                                                        <input type=""text"" style=""width: 200px;"" id=""TuNgay_1"" name=""TuNgay_1"">
                                                    </td>
                                                    <td>
                                                        <input type=""text"" id=""DenNgay_1"" name=""DenNgay_1"">
                                                    </td>
                                                    <td>
                                                        <input type=""text"" style=""width: 80px;"" id=""MucPhi_1"" name=""MucPhi_1"" onkeypress=""return NumberOnly()"">
                                                    </td>
                                                    
                                                    <td align=""center"">
                                                     
                                                        <a href=""#none"" class=""btn btn-rounded"" onclick=""if (confirm ('Xác nhận xóa dòng này?')) {sodong--; $('#dong_1').remove(); danhsodong();  }""><i class=""iconfa-remove-sign""></i> Xóa</a>
                                                        
                                                    </td>
                                                </tr>");
        }
    }
}