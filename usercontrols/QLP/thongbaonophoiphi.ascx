﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="thongbaonophoiphi.ascx.cs" Inherits="usercontrols_thongbaonophoiphi" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 15%;
    }
    .style2
    {
        width: 35%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }

</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<script type="text/javascript">

    function guiemail() {
        if (grvthongbaophihoivien_canhan_selected == '' && grvthongbaophihoivien_tapthe_selected == '') {
            $('#div_thongbaonull').dialog({
                resizable: false,
                modal: true,
                buttons: {
                    'OK': function () {
                        $(this).dialog('close');
                    }
                }
            });
            $('#div_thongbaonull').parent().find('button:contains("OK")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
        }
        else if ($('#ThoiHanNopPhiCaNhan').val() == "" && $('#ThoiHanNopPhiTapThe').val() == "") {
            alert('Cần điền đầy đủ thời hạn nộp phí');
        }
        else {
            var thoihancanhan = $('#ThoiHanNopPhiCaNhan').val();
            var thoihantapthe = $('#ThoiHanNopPhiTapThe').val();
            
			
            window.location = 'admin.aspx?page=guimail&emailcanhan=' + grvthongbaophihoivien_canhan_selected + '&emailtapthe=' + grvthongbaophihoivien_tapthe_selected + '&module=thongbaonophoiphi&phiidcanhan=<%=PhiIDCaNhan%>&phiidtapthe=<%=PhiIDTapThe%>&thoihancanhan=' + thoihancanhan + '&thoihantapthe=' + thoihantapthe;
        }
    }

    function thongbaothanhcong() {
        $('#div_thongbaoguimailthanhcong').dialog({
            resizable: false,
            modal: true,
            buttons: {
                'OK': function () {
                    $(this).dialog('close');
                }
            }
        });
        $('#div_thongbaoguimailthanhcong').parent().find('button:contains("OK")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function thongbaoloi() {
        jQuery('#div_thongbaoguimailloi').dialog({
            resizable: false,
            modal: true,
            buttons: {
                'OK': function () {
                    jQuery(this).dialog('close');
                }
            }
        });
        jQuery('#div_thongbaoguimailloi').parent().find('button:contains("OK")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

</script>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_thongbaophi" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thongbaophi" clientidmode="Static" runat="server"   method="post">
    <h4 class="widgettitle">Thông báo nộp hội phí</h4>
    <div id="thongbaoloi_form_thongbaophi" name="thongbaoloi_form_thongbaophi" style="display:none" class="alert alert-error"></div>
    <div height = "40px">
        <table id="tblThongBao" width="100%" border="0" class="formtbl">
        <tr>
         <td>
         <div ID="TinhTrangBanGhi" name = "TinhTrangBanGhi"></div>
        </td>
        </tr>
        </table>
    </div>
        <div><br /><b> Thông tin chung</b></div>
      <table id="Table1" width="100%" border="0" class="formtbl">
      <tr>
        <td class="style1" ><asp:Label ID="lblNgayNop" runat="server" Text = 'Ngày lập:' ></asp:Label>
            <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2"><input type="text" name="NgayLap" id="NgayLap"/></td>
        <td><input type="hidden" value="0" id="ketxuatcanhan" name="ketxuatcanhan" /> 
        <input type="hidden" value="0" id="ketxuattapthe" name="ketxuattapthe" /> </td>
      </tr>
       <tr>
          <td class="style1"><asp:Label ID="Label6" runat="server" Text = 'Ghi chú:' ></asp:Label></td>
        <td colspan ="2"><input width="100%" type="text" name="GhiChu" id="GhiChu"></td>        
      </tr>
      </table>

   <div><br /><b>Phí hội viên tổ chức</b></div>
     <table id="Table2" width="100%" border="0" class="formtbl">
      <tr>
      <td class="style1"><asp:Label ID="lblMaGiaoDich" runat="server" Text = 'Thời hạn nộp phí:' ></asp:Label>
          <%--<asp:Label ID="Label2" runat="server" Text="(*)" ForeColor="Red"></asp:Label>--%></td>
        <td class="style2" ><input type="text" name="ThoiHanNopPhiTapThe" id="ThoiHanNopPhiTapThe">
        <input type="hidden" name="PhiIDTapThe" id="PhiIDTapThe"></td>
          <td class="style1" ><asp:Label ID="Label3" runat="server" Text = 'Tìm kiếm:' ></asp:Label></td>
        <td class="style2"><input type="text" name="TimKiemTapThe" id="TimKiemTapThe" value="<%=Request.Form["TimKiemTapThe"]%>" onkeydown ="if (event.keyCode == 13) { $('#form_thongbaophi').submit();return false;}" /></td>
      </tr>
      </table>
      <div style="width: 100%; height: 400px; overflow: scroll">
       <asp:GridView ClientIDMode="Static" ID="grvThongBaoPhiCongTy" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="false" AllowSorting="True" 
       style = "width: 1650px"
           onsorting="grvThongBaoPhiCongTy_Sorting"
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval(id_congty)%>" />
                  </ItemTemplate>
              </asp:TemplateField>

             <asp:TemplateField  >
                  <ItemTemplate>
                        <span><%# Container.DataItemIndex + 1%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(ma_tapthe)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(ten_congty)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(tenviettat_congty)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(hoiphiphainopnamnay_congty, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(hoiphiconnonamtruoc_congty, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(tonghoiphiphainop_congty, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(tonghoiphidanop_congty, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(tonghoiphiconphainop_congty, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</div>

<div>

<asp:DropDownList ID="PagerTapThe" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientaitapthe').val(this.value); $('#user_search').submit();" 
                                        visible ="false">
                                     </asp:DropDownList>
</div>

 <div><br /><b>Phí hội viên cá nhân</b></div>
 <table id="Table3" width="100%" border="0" class="formtbl" >
      <tr>
      <td class="style1"><asp:Label ID="Label5" runat="server" Text = 'Thời hạn nộp phí:' ></asp:Label>
          <%--<asp:Label ID="Label7" runat="server" Text="(*)" ForeColor="Red"></asp:Label>--%></td>
        <td class="style2" ><input type="text" name="ThoiHanNopPhiCaNhan" id="ThoiHanNopPhiCaNhan">
        <input type="hidden" name="PhiIDCaNhan" id="PhiIDCaNhan"></td>
          <td class="style1" ><asp:Label ID="Label4" runat="server" Text = 'Tìm kiếm:' ></asp:Label></td>
        <td class="style2"><input type="text" name="TimKiemCaNhan" id="TimKiemCaNhan" value="<%=Request.Form["TimKiemCaNhan"]%>" onkeydown ="if (event.keyCode == 13) { $('#form_thongbaophi').submit();return false;}" /></td>
      </tr>
     <tr><td>
         <label>Đơn vị công tác:</label>
         </td>
         <td colspan="3">
             <select  style="width:100%"  name="tk_CTKiemToan" id="tk_CTKiemToan" onchange="$('#form_thongbaophi').submit();">
              <%
                  try{
                      DonVi(Request.Form["tk_CTKiemToan"].ToString());
                  }
                  catch
                  {
                      DonVi("00"); 
                  }
              %>
       </select> 
         </td>
     </tr>
      </table>
      
<div style="width: 100%; height: 400px; overflow: scroll">
       <asp:GridView ClientIDMode="Static" ID="grvthongbaophihoivien_canhan" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="false" AllowSorting="True" 
       style = "width: 1650px"
           onsorting="grvthongbaophihoivien_canhan_Sorting"
       >
           <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
              <HeaderStyle Width="50px" />
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval(id_canhan)%>" />
                  </ItemTemplate>
              </asp:TemplateField>

             <asp:TemplateField  >
             <HeaderStyle Width="50px" />
                  <ItemTemplate>
                  <span><%# Container.DataItemIndex + 1%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  >
               <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(ma_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(ten_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(sochungchi_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

                <asp:BoundField DataField="ngaycapchungchiKTV" 
                            ReadOnly="True"
                            DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="150px"/>

               <asp:TemplateField  >
               <HeaderStyle Width="200px" />
                  <ItemTemplate>
                      <span><%# Eval(donvicongtac_canhan)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(hoiphiphainopnamnay_canhan, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(hoiphiconnonamtruoc_canhan, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(tonghoiphiphainop_canhan, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
              <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(tonghoiphidanop_canhan, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  >
               <HeaderStyle Width="150px" />
                  <ItemTemplate>
                      <span><%# Eval(tonghoiphiconphainop_canhan, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>
</div>
<div>

<asp:DropDownList ID="PagerCaNhan" runat="server" 
                                        style="padding: 1px; width:55px;"
                                        onchange="$('#tranghientaicanhan').val(this.value); $('#user_search').submit();" 
                                        visible ="false">
                                     </asp:DropDownList>


</div>

<div>
<input type="hidden" value="0" id="guimail" name="guimail" />
<input type="hidden" id="phiidcanhan" name="phiidcanhan" />
<input type="hidden" id="phiidtapthe" name="phiidtapthe" />
<input type="hidden" id="thoihancanhan" name="thoihancanhan" />
<input type="hidden" id="thoihantapthe" name="thoihantapthe" />
<input type="hidden" id="emailcanhan" name="emailcanhan" />
<input type="hidden" id="emailtapthe" name="emailtapthe" />
</div>

<%LoadNut(); %>


    </form>

<script type ="text/javascript">

//   $('#TimKiemCaNhan').keypress(function(e) {
//              if (e.keyCode == '13') {
//                 e.preventDefault();
//                 alert('hoho');
//               }
//               });​
</script>
                         
<script type="text/javascript">
<%annut(); %>
       var grvthongbaophihoivien_canhan_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#grvthongbaophihoivien_canhan .checkall").bind("click", function () {
            grvthongbaophihoivien_canhan_selected = new Array();
            checked = $(this).prop("checked");
            $('#grvthongbaophihoivien_canhan :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) grvthongbaophihoivien_canhan_selected.push($(this).val());
            });
        });

        $('#grvthongbaophihoivien_canhan :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(grvthongbaophihoivien_canhan_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) grvthongbaophihoivien_canhan_selected.push($(this).val());
            });
        });
    });

     var grvthongbaophihoivien_tapthe_selected = new Array();

     jQuery(document).ready(function () {
         
        // dynamic table      

        $("#grvThongBaoPhiCongTy .checkall").bind("click", function () {
            grvthongbaophihoivien_tapthe_selected = new Array();
            checked = $(this).prop("checked");
            $('#grvThongBaoPhiCongTy :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) grvthongbaophihoivien_tapthe_selected.push($(this).val());
            });
        });

        $('#grvThongBaoPhiCongTy :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(grvthongbaophihoivien_tapthe_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) grvthongbaophihoivien_tapthe_selected.push($(this).val());
            });
        });
    });



        function submitform() {        
                jQuery("#form_thongbaophi").submit();
        }


        jQuery("#form_thongbaophi").validate({
            rules: {
                NgayNop: {
                    required: true
                },
                //ThoiHanNopPhiTapThe: {
                //    required: true
                //},
                //ThoiHanNopPhiCaNhan: {
                //    required: true
                //},
            },
            invalidHandler: function (f, d) {
                var g = d.numberOfInvalids();

                if (g) {
                    var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                    jQuery("#thongbaoloi_form_thongbaophi").html(e).show()

                } else {
                    jQuery("#thongbaoloi_form_thongbaophi").hide()
                }
            }
        });

    $.datepicker.setDefaults($.datepicker.regional['vi']);

    $(function () {  $('#ThoiHanNopPhiTapThe, #ThoiHanNopPhiCaNhan, #NgayLap').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-75:+25',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });

    $(function () {$('#NgayLap').datepicker('setDate', new Date())});

    <%LoadThoiGian(); %>

        function export_excel_canhan()
    {
        $('#ketxuatcanhan').val('1');
        $('#form_thongbaophi').submit();
        $('#ketxuatcanhan').val('0');
    }

    function export_excel_tapthe()
    {
        $('#ketxuattapthe').val('1');
        $('#form_thongbaophi').submit();
        $('#ketxuattapthe').val('0');
    }


    //function guiemail()
    //{
    // if (grvthongbaophihoivien_canhan_selected == '' && grvthongbaophihoivien_tapthe_selected == '') {
    //        $('#div_thongbaonull').dialog({
    //            resizable: false,
    //            modal: true,
    //            buttons: {
    //                'OK': function () {
    //                    $(this).dialog('close');
    //                }
    //            }
    //        });
    //        $('#div_thongbaonull').parent().find('button:contains("OK")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    //    }
    //    else {
    //        var thoihancanhan = $('#ThoiHanNopPhiCaNhan').val();
    //        var thoihantapthe = $('#ThoiHanNopPhiTapThe').val();
    //        $('#guimail').val('1');
    //        $('#emailcanhan').val(grvthongbaophihoivien_canhan_selected);
    //        $('#emailtapthe').val(grvthongbaophihoivien_tapthe_selected);
    //        $('#phiidcanhan').val(PhiIDCaNhan);
    //        $('#phiidtapthe').val(PhiIDTapThe);
    //        $('#thoihancanhan').val(thoihancanhan);
    //        $('#thoihantapthetapthe').val(thoihantapthe);
    //        $('#form_thongbaophi').submit();
    //        $('#guimail').val('0');
    //    }
    //}

</script>


<div id="div_danhsachphi_search" style="display:none" >
<form id="user_search" method="post" enctype="multipart/form-data" >

</form>
</div>
<div id="div_thongbaoguimailthanhcong" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Gửi email thành công</p>

</div>

<div id="div_thongbaoguimailloi" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Gửi email thất bại</p>

</div>

<div id="div_thongbaonull" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Không có hội viên được chọn</p>

</div>
