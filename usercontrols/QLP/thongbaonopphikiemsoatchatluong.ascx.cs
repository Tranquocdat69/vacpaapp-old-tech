﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_thongbaonopphikiemsoatchatluong : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dtb = new DataTable();

    public string quyen = "ThongBaoNopPhiKSCL";

    public string tenchucnang = "Thông báo nộp phí kiểm soát chất lượng";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";
    public int iTinhTrang = 0;

    public string id_canhan = "HoiVienCaNhanId";
    public string ma_canhan = "MaHoiVienCaNhan";
    public string stt_canhan = "STT";
    public string ten_canhan = "HoVaTen";
    public string sochungchi_canhan = "SoChungChiKTV";
    public string ngaycapchungchi_canhan = "NgayCapChungChiKTV";
    public string donvicongtac_canhan = "DonViCongTac";
    public string email_canhan = "Email";
    public string hoiphiphainopnamnay_canhan = "PhiKSCLPhaiNopNamNay";
    public string hoiphiconnonamtruoc_canhan = "PhiKSCLConNoNamTruoc";
    public string tonghoiphiphainop_canhan = "TongPhiKSCLPhaiNop";
    public string tonghoiphidanop_canhan = "TongPhiKSCLDaNop";
    public string tonghoiphiconphainop_canhan = "TongPhiKSCLConPhaiNop";
    public int iPhiID;
    public string PhiID = "";
    public string strThoiHan = "";

    public string id_congty = "HoiVienTapTheId";
    public string ma_tapthe = "MaHoiVienTapThe";
    public string stt_congty = "STT";
    public string ten_congty = "TenDoanhNghiep";
    public string tenviettat_congty = "TenVietTat";
    public string email_tapthe = "Email";
    public string hoiphiphainopnamnay_congty = "PhiKSCLPhaiNopNamNay";
    public string hoiphiconnonamtruoc_congty = "PhiKSCLConNoNamTruoc";
    public string tonghoiphiphainop_congty = "TongPhiKSCLPhaiNop";
    public string tonghoiphidanop_congty = "TongPhiKSCLDaNop";
    public string tonghoiphiconphainop_congty = "TongPhiKSCLConPhaiNop";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));


            if (string.IsNullOrEmpty(Request.QueryString["thongbao"]))
            {
                LoadThongTin();
            }
            else if (Request.QueryString["thongbao"] == "thanhcong")
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script>");
                sb.Append(@"jQuery(document).ready(function () {");
                sb.Append(@"  thongbaothanhcong();}) ");
                sb.Append(@"</script>");
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
                LoadThongTin();
            }
            else if (Request.QueryString["thongbao"] == "loi")
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script>");
                sb.Append(@"jQuery(document).ready(function () {");
                sb.Append(@"  thongbaoloi();}) ");
                sb.Append(@"</script>");
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
                LoadThongTin();
            }
        }
        catch
        { }
    }

    protected void LoadThongTin()
    {
        try
        {
            grvthongbaophitapthe.Columns[1].HeaderText = "STT";
            grvthongbaophitapthe.Columns[1].SortExpression = stt_congty;
            grvthongbaophitapthe.Columns[2].HeaderText = "ID HVTC";
            grvthongbaophitapthe.Columns[2].SortExpression = ma_tapthe;
            grvthongbaophitapthe.Columns[3].HeaderText = "Tên công ty";
            grvthongbaophitapthe.Columns[3].SortExpression = ten_congty;
            grvthongbaophitapthe.Columns[4].HeaderText = "Tên viết tắt";
            grvthongbaophitapthe.Columns[4].SortExpression = tenviettat_congty;
            grvthongbaophitapthe.Columns[5].HeaderText = "Phí KSCL phải nộp năm nay";
            grvthongbaophitapthe.Columns[5].SortExpression = hoiphiphainopnamnay_congty;
            grvthongbaophitapthe.Columns[6].HeaderText = "Phí KSCL còn nợ năm trước";
            grvthongbaophitapthe.Columns[6].SortExpression = hoiphiconnonamtruoc_congty;
            grvthongbaophitapthe.Columns[7].HeaderText = "Tổng Phí KSCL phải nộp";
            grvthongbaophitapthe.Columns[7].SortExpression = tonghoiphiphainop_congty;
            grvthongbaophitapthe.Columns[8].HeaderText = "Tổng Phí KSCL đã nộp";
            grvthongbaophitapthe.Columns[8].SortExpression = tonghoiphidanop_congty;
            grvthongbaophitapthe.Columns[9].HeaderText = "Tổng Phí KSCL còn phải nộp";
            grvthongbaophitapthe.Columns[9].SortExpression = tonghoiphiconphainop_congty;

            grvthongbaophicanhan.Columns[1].HeaderText = "STT";
            grvthongbaophicanhan.Columns[1].SortExpression = stt_canhan;
            grvthongbaophicanhan.Columns[2].HeaderText = "ID HVTC";
            grvthongbaophicanhan.Columns[2].SortExpression = ma_canhan;
            grvthongbaophicanhan.Columns[3].HeaderText = "Họ và tên";
            grvthongbaophicanhan.Columns[3].SortExpression = ten_canhan;
            grvthongbaophicanhan.Columns[4].HeaderText = "Số chứng chỉ KTV";
            grvthongbaophicanhan.Columns[4].SortExpression = sochungchi_canhan;
            grvthongbaophicanhan.Columns[5].HeaderText = "Ngày cấp chứng chỉ KTV";
            grvthongbaophicanhan.Columns[5].SortExpression = ngaycapchungchi_canhan;
            grvthongbaophicanhan.Columns[6].HeaderText = "Đơn vị công tác";
            grvthongbaophicanhan.Columns[6].SortExpression = donvicongtac_canhan;
            grvthongbaophicanhan.Columns[7].HeaderText = "Phí KSCL phải nộp năm nay";
            grvthongbaophicanhan.Columns[7].SortExpression = hoiphiphainopnamnay_canhan;
            grvthongbaophicanhan.Columns[8].HeaderText = "Phí KSCL còn nợ năm trước";
            grvthongbaophicanhan.Columns[8].SortExpression = hoiphiconnonamtruoc_canhan;
            grvthongbaophicanhan.Columns[9].HeaderText = "Tổng Phí KSCL phải nộp";
            grvthongbaophicanhan.Columns[9].SortExpression = tonghoiphiphainop_canhan;
            grvthongbaophicanhan.Columns[10].HeaderText = "Tổng Phí KSCL đã nộp";
            grvthongbaophicanhan.Columns[10].SortExpression = tonghoiphidanop_canhan;
            grvthongbaophicanhan.Columns[11].HeaderText = "Tổng Phí KSCL còn phải nộp";
            grvthongbaophicanhan.Columns[11].SortExpression = tonghoiphiconphainop_canhan;

            int i;
            Commons cm = new Commons();

            SqlCommand sql = new SqlCommand();
            //Lấy phí tổ chức
            sql.CommandText = "SELECT PhiID, ThoiHanNopPhi from tblTTDMPhiKSCL where TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND ((CASE WHEN ThoiHanNopPhi IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE ThoiHanNopPhi END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            //sql.CommandText = "SELECT Top 1 PhiID, ThoiHanNopPhi from tblTTDMPhiKSCL where TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND ((CASE WHEN ThoiHanNopPhi IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE ThoiHanNopPhi END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            iPhiID = 0;
            if (dtbTemp.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTemp.Rows)
                {
                    iPhiID = Convert.ToInt32(dtr[0]);
                   // iPhiID = Convert.ToInt32(dtbTemp.Rows[0][0]);
                    ////    string Ngay = dtbTemp.Rows[0]["ThoiHanNopPhi"].ToString();
                    ////    if (string.IsNullOrEmpty(Ngay))
                    ////        strThoiHan = DateTime.Now.ToString("dd/MM/yyyy");
                    ////    else
                    ////        strThoiHan = Convert.ToDateTime(dtbTemp.Rows[0]["ThoiHanNopPhi"].ToString()).ToString("dd/MM/yyyy");
                    ////}
                    ////else
                    ////    strThoiHan = DateTime.Now.ToString("dd/MM/yyyy");
                    ////if (iPhiID != 0)
                    ////{
                    ////    PhiID = iPhiID.ToString();
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_TapThe]" + iPhiID + "," +
                                      (int) EnumVACPA.LoaiHoiVien.HoiVienTapThe + "," +
                                      (int) EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ",N'%" +
                                      Request.Form["TimKiemTapThe"] +
                                      "%', ''," + (int) EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_TapThe]" + iPhiID + "," +
                                      (int) EnumVACPA.LoaiHoiVien.CongTyKiemToan + "," +
                                      (int) EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ",N'%" +
                                      Request.Form["TimKiemTapThe"] +
                                      "%', ''," + (int) EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                }
                //if (dtb.Rows.Count == 0)
                //{
                //    PagerTapThe.Enabled = false;
                //    return;
                //}
                //else
                //    PagerTapThe.Enabled = true;
                DataView dv = dtb.DefaultView;

                if (ViewState["sortexpressioncty"] != null)
                    dv.Sort = ViewState["sortexpressioncty"].ToString() + " " + ViewState["sortdirectioncty"].ToString();
                else
                    dv.Sort = "STT ASC";
                //dtb = dv.ToTable();
                // Phân trang
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = dv;
                objPds.AllowPaging = true;
                objPds.PageSize = 99999;

                // danh sách trang
                for (i = 0; i < objPds.PageCount; i++)
                {
                    ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                    if (!PagerTapThe.Items.Contains(a))
                        PagerTapThe.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
                }
                // Chuyển trang
                if (!string.IsNullOrEmpty(Request.Form["tranghientaitapthe"]))
                {
                    //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                    if (Convert.ToInt32(Request.Form["tranghientaitapthe"]) <= objPds.PageCount)
                    {
                        objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientaitapthe"]);
                        PagerTapThe.SelectedIndex = Convert.ToInt32(Request.Form["tranghientaitapthe"]);
                    }
                    else
                    {
                        objPds.CurrentPageIndex = 0;
                        PagerTapThe.SelectedIndex = 0;
                    }
                }

                //kết xuất danh sách ra excel
                if (!string.IsNullOrEmpty(Request.Form["ketxuattapthe"]))
                {
                    if (Request.Form["ketxuattapthe"] == "1")
                    {
                        try
                        {
                            Response.Clear();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();

                            string FileName = quyen + "TapThe" + DateTime.Now + ".xls";
                            StringWriter strwritter = new StringWriter();
                            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                            Response.ContentEncoding = System.Text.Encoding.UTF8;
                            Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                            objPds.AllowPaging = false;

                            GridView exportgrid = new GridView();

                            exportgrid.DataSource = objPds;
                            exportgrid.DataBind();
                            exportgrid.RenderControl(htmltextwrtter);
                            exportgrid.Dispose();

                            Response.Write(strwritter.ToString());
                            Response.End();
                        }
                        catch (System.Threading.ThreadAbortException a)
                        { }
                    }
                }

                grvthongbaophitapthe.DataSource = dv;
                grvthongbaophitapthe.DataBind();
            }

           
            //if (iPhiID != 0)
            dtb = new DataTable();
            if (dtbTemp.Rows.Count > 0)
            {

                foreach (DataRow dtr in dtbTemp.Rows)
                {
                   // PhiID = iPhiID.ToString();
                    //System.Web.HttpContext.Current.Response.Write("$('#ThoiHanNopPhiCaNhan').val('" + Convert.ToDateTime(dtbTempCaNhan.Rows[0]["NgayApDung"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);

                    iPhiID = Convert.ToInt32(dtr[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhan]" + iPhiID + ", " +
                                      (int) EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," +
                                      (int) EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ",N'%" +
                                      Request.Form["TimKiemCaNhan"] + "%', ''," + (int) EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiKSCL_CaNhan]" + iPhiID + ", " +
                                      (int) EnumVACPA.LoaiHoiVien.KiemToanVien + "," +
                                      (int) EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ",N'%" +
                                      Request.Form["TimKiemCaNhan"] + "%', ''," + (int) EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                }
                //if (dtb.Rows.Count == 0)
                //{
                //    PagerCaNhan.Enabled = false;
                //    return;
                //}
                //else
                //    PagerCaNhan.Enabled = true;
                DataView dv = dtb.DefaultView;

                if (ViewState["sortexpression"] != null)
                    dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
                else
                    dv.Sort = "STT ASC";
                //dtb = dv.ToTable();
                // Phân trang
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = dv;
                objPds.AllowPaging = true;
                objPds.PageSize = 99999;

                // danh sách trang
                for (i = 0; i < objPds.PageCount; i++)
                {
                    ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                    if (!PagerCaNhan.Items.Contains(a))
                        PagerCaNhan.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
                }
                // Chuyển trang
                if (!string.IsNullOrEmpty(Request.Form["tranghientaicanhan"]))
                {
                    //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                    if (Convert.ToInt32(Request.Form["tranghientaicanhan"]) <= objPds.PageCount)
                    {
                        objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientaicanhan"]);
                        PagerCaNhan.SelectedIndex = Convert.ToInt32(Request.Form["tranghientaicanhan"]);
                    }
                    else
                    {
                        objPds.CurrentPageIndex = 0;
                        PagerCaNhan.SelectedIndex = 0;
                    }
                }

                // kết xuất danh sách ra excel
                if (!string.IsNullOrEmpty(Request.Form["ketxuatcanhan"]))
                {
                    if (Request.Form["ketxuatcanhan"] == "1")
                    {
                        try
                        {
                            Response.Clear();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();

                            string FileName = quyen + "CaNhan" + DateTime.Now + ".xls";
                            StringWriter strwritter = new StringWriter();
                            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                            Response.ContentEncoding = System.Text.Encoding.UTF8;
                            Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                            objPds.AllowPaging = false;

                            GridView exportgrid = new GridView();

                            exportgrid.DataSource = objPds;
                            exportgrid.DataBind();
                            exportgrid.RenderControl(htmltextwrtter);
                            exportgrid.Dispose();

                            Response.Write(strwritter.ToString());
                            Response.End();
                        }
                        catch (System.Threading.ThreadAbortException a)
                        { }
                    }
                }

                grvthongbaophicanhan.DataSource = dv;
                grvthongbaophicanhan.DataBind();
            }

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            // ds = null;
            dtb = null;

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_guiemail' href='#none' class='btn btn-rounded' onclick='guiemail();'><i class='iconfa-envelope'></i> Gửi email</a>
            <a id='btn_ketxuatcanhan' href='#none' class='btn btn-rounded' onclick='export_excel_canhan();'><i class='iconfa-external-link'></i> Kết xuất danh sách cá nhân</a>
            <a id='btn_ketxuattapthe' href='#none' class='btn btn-rounded' onclick='export_excel_tapthe();'><i class='iconfa-external-link'></i> Kết xuất danh sách tổ chức</a>";
        output_html += "</div>";
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void LoadThoiGian()
    {
        //string output_html = @"  $(function () { $('#ThoiHanNopPhi').datepicker('setDate', '" + strThoiHan + @"') });";
        //System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("GUIEMAIL|"))
        {
            Response.Write("$('#btn_guiemail').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuatcanhan').remove();");
            Response.Write("$('#btn_ketxuattapthe').remove();");
        }
    }

    protected void grvthongbaophicanhan_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        LoadThongTin();
    }

    protected void grvthongbaophitapthe_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirectioncty"] == null)
            ViewState["sortdirectioncty"] = "asc";
        else
        {
            if (ViewState["sortdirectioncty"].ToString() == "asc")
                ViewState["sortdirectioncty"] = "desc";
            else
                ViewState["sortdirectioncty"] = "asc";
        }
        ViewState["sortexpressioncty"] = e.SortExpression;
        LoadThongTin();
    }
}