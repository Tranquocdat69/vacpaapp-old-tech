﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;

public partial class usercontrols_quanlyphikhac : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();

    public string quyen = "DMPhiKhac";

    public string tenchucnang = "Phí khác";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

           //Thêm mới
            //Khi thêm mới thì không có request id,  mã phí          
            if (string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                if (string.IsNullOrEmpty(Request.Form["MaPhi"]) && !string.IsNullOrEmpty(Request.Form["TenPhi"]) && !string.IsNullOrEmpty(Request.Form["MucPhi"]))
                {
                    //Chạy function tự sinh mã phí
                    string Nam = "";
                    if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                        Nam = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).Year.ToString();
                    Nam = Nam.Substring(2);

                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "select [dbo].[LaySoChayDanhMucPhiKhac] (" + Nam + ")";
                    DataSet ds = new DataSet();
                    ds = DataAccess.RunCMDGetDataSet(sql);

                    string MaPhi = ds.Tables[0].Rows[0][0].ToString();

                    string strMucPhi = Request.Form["MucPhi"];

                    var modified = new StringBuilder();
                    foreach (char c in strMucPhi)
                    {
                        if (Char.IsDigit(c) || c == ',')
                            modified.Append(c);
                    }

                    decimal MucPhi = decimal.Parse(modified.ToString());
                    DateTime? NgayLap = null;
                    if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                        NgayLap = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime? NgayApDung = null;
                    if (!string.IsNullOrEmpty(Request.Form["NgayApDung"]))
                        NgayApDung = DateTime.ParseExact(Request.Form["NgayApDung"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    DateTime? NgayHetHieuLuc = null;
                    if (!string.IsNullOrEmpty(Request.Form["NgayHetHieuLuc"]))
                        NgayHetHieuLuc = DateTime.ParseExact(Request.Form["NgayHetHieuLuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    DateTime NgayNhap = DateTime.Now;

                    int? ThangNop = null;
                    if (!string.IsNullOrEmpty(Request.Form["ThangNop"]))
                        ThangNop = Convert.ToInt32(Request.Form["ThangNop"]);

                    sql = new SqlCommand();

                    sql.CommandText = "INSERT INTO tblTTDMPhiKhac(MaPhi, NgayLap, TenPhi, NgayApDung, NgayHetHieuLuc, "
                    + " MucPhi, DonViThoiGian, ThoiHanThang, ThoiHanNgay, TinhTrangID, NgayNhap, NguoiNhap) " +
                    " VALUES (@MaPhi, @NgayLap, @TenPhi, @NgayApDung, @NgayHetHieuLuc, @MucPhi, @DonViThoiGian, @ThoiHanThang, @ThoiHanNgay, @TinhTrangID, @NgayNhap, @NguoiNhap)";
                    sql.Parameters.AddWithValue("@MaPhi", MaPhi);
                    if (NgayLap != null)
                        sql.Parameters.AddWithValue("@NgayLap", NgayLap);
                    else
                        sql.Parameters.AddWithValue("@NgayLap", DBNull.Value);
                    sql.Parameters.AddWithValue("@TenPhi", Request.Form["TenPhi"]);
                    if (NgayApDung != null)
                        sql.Parameters.AddWithValue("@NgayApDung", NgayApDung);
                    else
                        sql.Parameters.AddWithValue("@NgayApDung", DBNull.Value);
                    if (NgayHetHieuLuc != null)
                        sql.Parameters.AddWithValue("@NgayHetHieuLuc", NgayHetHieuLuc);
                    else
                        sql.Parameters.AddWithValue("@NgayHetHieuLuc", DBNull.Value);

                    sql.Parameters.AddWithValue("@MucPhi", MucPhi);
                    sql.Parameters.AddWithValue("@DonViThoiGian", Request.Form["DonViThoiGian"]);
                    if (ThangNop != null)
                        sql.Parameters.AddWithValue("@ThoiHanThang", ThangNop);
                    else
                        sql.Parameters.AddWithValue("@ThoiHanThang", DBNull.Value);
                    sql.Parameters.AddWithValue("@ThoiHanNgay", Request.Form["NgayNop"]);
                    sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                    sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                    sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog("DMPhiKhac", "Thêm giá trị \"" + MaPhi + "\" vào danh mục TTDMPhiKhac");

                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                }
            }
            else
            {
                if (Request.QueryString["mode"] == "edit") //Sửa
                {
                    if (!string.IsNullOrEmpty(Request.Form["TenPhi"]))
                    {
                        string strTenPhi = Request.Form["TenPhi"];
                        string strMucPhi = Request.Form["MucPhi"];

                        var modified = new StringBuilder();
                        foreach (char c in strMucPhi)
                        {
                            if (Char.IsDigit(c) || c == ',')
                                modified.Append(c);
                        }

                        decimal MucPhi = decimal.Parse(modified.ToString());
                        DateTime? NgayLap1 = null;
                        if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                            NgayLap1 = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime? NgayApDung1 = null;
                        if (!string.IsNullOrEmpty(Request.Form["NgayApDung"]))
                            NgayApDung1 = DateTime.ParseExact(Request.Form["NgayApDung"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        DateTime? NgayHetHieuLuc1 = null;
                        if (!string.IsNullOrEmpty(Request.Form["NgayHetHieuLuc"]))
                            NgayHetHieuLuc1 = DateTime.ParseExact(Request.Form["NgayHetHieuLuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        DateTime NgayNhap = DateTime.Now;

                        int? ThangNop = null;
                        if (!string.IsNullOrEmpty(Request.Form["ThangNop"]))
                            ThangNop = Convert.ToInt32(Request.Form["ThangNop"]);

                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        sql.CommandText = @"UPDATE tblTTDMPhiKhac set NgayLap =@NgayLap, TenPhi = @TenPhi,
                        NgayApDung = @NgayApDung, NgayHetHieuLuc = @NgayHetHieuLuc, 
                        MucPhi = @MucPhi, DonViThoiGian = @DonViThoiGian, ThoiHanThang = @ThoiHanThang,
                        ThoiHanNgay =@ThoiHanNgay, TinhTrangID = @TinhTrangID, NgayNhap = @NgayNhap, 
                        NguoiNhap =@NguoiNhap
                        WHERE PhiID = " + Request.QueryString["id"];
                        if (NgayLap1 != null)
                            sql.Parameters.AddWithValue("@NgayLap", NgayLap1);
                        else
                            sql.Parameters.AddWithValue("@NgayLap", DBNull.Value);
                        sql.Parameters.AddWithValue("@TenPhi", Request.Form["TenPhi"]);
                        if (NgayApDung1 != null)
                            sql.Parameters.AddWithValue("@NgayApDung", NgayApDung1);
                        else
                            sql.Parameters.AddWithValue("@NgayApDung", DBNull.Value);
                        if (NgayHetHieuLuc1 != null)
                            sql.Parameters.AddWithValue("@NgayHetHieuLuc", NgayHetHieuLuc1);
                        else
                            sql.Parameters.AddWithValue("@NgayHetHieuLuc", DBNull.Value);

                        sql.Parameters.AddWithValue("@MucPhi", MucPhi);
                        sql.Parameters.AddWithValue("@DonViThoiGian", Request.Form["DonViThoiGian"]);
                        if (ThangNop != null)
                            sql.Parameters.AddWithValue("@ThoiHanThang", ThangNop);
                        else
                            sql.Parameters.AddWithValue("@ThoiHanThang", DBNull.Value);
                        sql.Parameters.AddWithValue("@ThoiHanNgay", Request.Form["NgayNop"]);
                        sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                        sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                        sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);
                        DataAccess.RunActionCmd(sql);
                        sql.Connection.Close();
                        sql.Connection.Dispose();
                        sql = null;

                        cm.ghilog("DMPhiKhac", "Sửa giá trị \"" + Request.Form["MaPhi"] + "\" vào danh mục TTDMPhiKhac");

                        Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                    }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["tinhtrang"]))//Các hành động từ chối, duyệt, thoái duyệt, xóa
                {
                    //Từ chối
                    if (Request.QueryString["tinhtrang"] == "tuchoi")
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                            sql.CommandText = "UPDATE tblTTDMPhiKhac set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                            sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("DMPhiKhac", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKhac");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "xoa") //Xóa
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt, thoái duyệt, từ chối thì mới cho xóa
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet || Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.KhongPheDuyet || Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                        {
                            sql.CommandText = "DELETE tblTTDMPhiKhac WHERE PhiId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("DMPhiKhac", "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKhac");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "duyet") //duyệt
                    {

                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Phải ở trạng thái chờ duyệt thì mới cho duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {

                            sql.CommandText = "UPDATE tblTTDMPhiKhac set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet = @NgayDuyet WHERE PhiId = @PhiID";
                            sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                            sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                            sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                            sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("DMPhiKhac", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKhac");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "thoaiduyet") //Thoái duyệt
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Phải đang ở trạng thái duyệt thì mới cho thoái duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {
                            sql.CommandText = "UPDATE tblTTDMPhiKhac set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                            sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@PhiID", Request.QueryString["id"]);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("DMPhiKhac", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" vào danh mục TTDMPhiKhac");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }

    }

    public void LoadDoiTuong(int iDoiTuongID = 0)
    {
        string output_html = "";
        if (iDoiTuongID == 0)
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT distinct DoiTuongNopPhiID, TenDoiTuong, LoaiDoiTuong FROM tblDMDoiTuongNopPhi";
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            int i = 0;
            foreach (DataRow dtr in ds.Tables[0].Rows)
            {
                i++;
                if (i == 1)
                    output_html += @"<option selected=""selected"" value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
                else
                    output_html += @"<option value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
            }
        }
        else
        {
            SqlCommand sql = new SqlCommand();
            sql.CommandText = "SELECT TenDoiTuongFROM tblDMDoiTuongNopPhi where DoiTuongNopPhiID = " + iDoiTuongID;
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            output_html += @"<option selected=""selected"" value='" + ds.Tables[0].Rows[0].ToString() + "'>" + ds.Tables[0].Rows[1].ToString() + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadThoiGian()
    {
        string output_html = "";
        output_html += @"<option selected=""selected"" value=""1"">Năm</option>";
        output_html += @"<option value=""3"">Tháng</option>";
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadNgayNop31()
    {
        string output_html = "";
        output_html += @"<option selected=""selected"" value=""1"">1</option>";
        for (int i = 2; i <= 31; i++)
        {
            output_html += @"<option value='" + i + "'>" + i + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadThangNop()
    {
        string output_html = "";
        output_html += @"<option selected=""selected"" value=""1"">1</option>";
        for (int i = 2; i <= 12; i++)
        {
            output_html += @"<option value='" + i + "'>" + i + "</option>";
        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_luu' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_sua' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_xoa' href='#none' class='btn btn-rounded' onclick='xoa(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-sign'></i> Xóa</a>
            <a id='btn_duyet' href='#none' class='btn btn-rounded' onclick='duyet(" + Request.QueryString["id"] + @");'><i class='iconfa-ok'></i> Duyệt</a>
            <a id='btn_tuchoi' href='#none' class='btn btn-rounded' onclick='tuchoi(" + Request.QueryString["id"] + @");'><i class='iconfa-remove'></i> Từ chối</a>
        <a id='btn_thoaiduyet' href='#none' class='btn btn-rounded' onclick='thoaiduyet(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Thoái duyệt</a>";
        output_html += "</div>";
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void AnNut()
    {
        Commons cm = new Commons();

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKhac", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_luu').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKhac", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('#btn_sua').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKhac", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKhac", cm.connstr).Contains("DUYET|"))
        {
            Response.Write("$('#btn_duyet').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKhac", cm.connstr).Contains("TUCHOI|"))
        {
            Response.Write("$('#btn_tuchoi').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "DMPhiKhac", cm.connstr).Contains("THOAIDUYET|"))
        {
            Response.Write("$('#btn_thoaiduyet').remove();");
        }
    }

    protected void LoadThongTin()
    {//Xem hoặc sửa
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
            if (Request.QueryString["mode"] == "view" || Request.QueryString["mode"] == "edit") //Xem hoặc sửa
            {
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select MaPhi, NgayLap, TenPhi, NgayApDung, NgayHetHieuLuc, DoiTuongApDungID, "
                    + " MucPhi, DonViThoiGian, ThoiHanThang, ThoiHanNgay, TinhTrangID, TenTrangThai "
                    + " from tblTTDMPhiKhac inner join tblDMTrangThai on tblTTDMPhiKhac.TinhTrangID = tblDMTrangThai.TrangThaiID "
                + " where PhiId = " + Convert.ToInt16(Request.QueryString["id"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                dt = ds.Tables[0];
                int iTinhTrangBanGhi = 0;
                string strTinhTrangBanGhi = "";
                DateTime? NgayCoHieuLuc = new DateTime();
                DateTime? NgayHetHieuLuc = new DateTime();
                DateTime? NgayTraCuu = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                if (dt.Rows.Count > 0)
                {
                    Response.Write("$('#MaPhi').val('" + dt.Rows[0]["MaPhi"] + "');" + System.Environment.NewLine);
                    if (dt.Rows[0]["NgayLap"] != DBNull.Value)
                        Response.Write("$('#NgayLap').val('" + Convert.ToDateTime(dt.Rows[0]["NgayLap"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                    Response.Write("$('#TenPhi').val('" + dt.Rows[0]["TenPhi"] + "');" + System.Environment.NewLine);
                    if (dt.Rows[0]["NgayApDung"] != DBNull.Value)
                    {
                        Response.Write("$('#NgayApDung').val('" + Convert.ToDateTime(dt.Rows[0]["NgayApDung"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                        NgayCoHieuLuc = Convert.ToDateTime(dt.Rows[0]["NgayApDung"]);
                    }
                    else
                        NgayCoHieuLuc = null;
                    if (dt.Rows[0]["NgayHetHieuLuc"] != DBNull.Value)
                    {
                        Response.Write("$('#NgayHetHieuLuc').val('" + Convert.ToDateTime(dt.Rows[0]["NgayHetHieuLuc"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                        NgayHetHieuLuc = Convert.ToDateTime(dt.Rows[0]["NgayHetHieuLuc"]);
                    }
                    else
                        NgayHetHieuLuc = null;
                    Response.Write("$('#DoiTuongApDung').val('" + dt.Rows[0]["DoiTuongApDungID"] + "');" + System.Environment.NewLine);
                    Response.Write("$('#MucPhi').val('" + dt.Rows[0]["MucPhi"] + "');" + System.Environment.NewLine);
                    Response.Write("$('#DonViThoiGian').val('" + dt.Rows[0]["DonViThoiGian"].ToString().Trim() + "');" + System.Environment.NewLine);

                    //Với thời hạn tháng khác null thì load tháng và ngày bình thường
                    if (dt.Rows[0]["ThoiHanThang"] != DBNull.Value)
                    {
                        Response.Write("$('#ThangNop').val('" + dt.Rows[0]["ThoiHanThang"].ToString().Trim() + "');" + System.Environment.NewLine);

                        Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                        Response.Write(@"$('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=1')" + System.Environment.NewLine);
                    }
                    //Với thời hạn tháng = null => xử lý tháng và ngày
                    else
                    {
                        if (Request.QueryString["mode"] == "view")
                        {
                            Response.Write("$('#ThangNop').html('');" + System.Environment.NewLine);
                            Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                        }
                        else
                        {
                            Response.Write("$('#ThangNop').val('');" + System.Environment.NewLine);
                            Response.Write("$('#ThangNop').attr('disabled', true);" + System.Environment.NewLine);

                            Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                            Response.Write(@"$('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=1')" + System.Environment.NewLine);
                        }
                    }
                    iTinhTrangBanGhi = Convert.ToInt32(dt.Rows[0]["TinhTrangID"]);
                    strTinhTrangBanGhi = dt.Rows[0]["TenTrangThai"].ToString();

                }

                string strTinhTrangHieuLuc = "";
                if (NgayCoHieuLuc != null)
                {
                    if (NgayHetHieuLuc != null)
                    {
                        if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet && NgayCoHieuLuc <= NgayTraCuu && NgayHetHieuLuc >= NgayTraCuu)
                            strTinhTrangHieuLuc = "Có hiệu lực";
                        else if (NgayCoHieuLuc > NgayTraCuu)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (NgayHetHieuLuc < NgayTraCuu)
                            strTinhTrangHieuLuc = "Hết hiệu lực";
                        else
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                    }
                    else
                    {
                        if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet && NgayCoHieuLuc <= NgayTraCuu)
                            strTinhTrangHieuLuc = "Có hiệu lực";
                        else if (NgayCoHieuLuc > NgayTraCuu)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                    }
                }
                else
                {
                    strTinhTrangHieuLuc = "Chưa có hiệu lực";
                }
                //if (Request.QueryString["mode"] == "3")
                //{
                string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + strTinhTrangHieuLuc + @"</i>')
});";
                Response.Write(a + System.Environment.NewLine);
                //}
                //            else if (Request.QueryString["mode"] == "5")
                //            {
                //                string a = @"
                //            $(function () {
                //$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
                //$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + strTinhTrangHieuLuc + @"</i>')
                //});";
                //                Response.Write(a + System.Environment.NewLine);
                //            }


                if (Request.QueryString["mode"] == "view")
                {
                    Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                    Response.Write("$('#form_quanlyphikhac input,select,textarea').attr('disabled', true);");

                    if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ChoDuyet)
                    {
                        //Hiện Từ chối, Duyệt, Xóa
                        //Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                    {
                        //Hiện thoái duyệt
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                    {
                        //Hiện Xóa
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        //Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                    }
                }

                    //Nếu đang ở chế độ sửa
                else if (Request.QueryString["mode"] == "edit")
                {
                    //Nếu tình trạng bản ghi là đã phê duyệt
                    //Thì không cho làm gì hết
                    if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                    {
                        Response.Write("$('#form_quanlyphihoivien input,select,textarea').attr('disabled', true);");
                        Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);

                    }
                    //Nếu tình trạng bản ghi không phải là đã phê duyệt => chỉ hiện sửa
                    else
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);


                        //Có thể sửa Ngày lập, ngày áp dụng
                        string output_html = @"$(function () {  $('#NgayLap, #NgayApDung').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
                        Response.Write(output_html + System.Environment.NewLine);

                        //Có thể sửa Ngày hết hiệu lực
                        output_html = @"$(function () {  $('#NgayHetHieuLuc').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
                        Response.Write(output_html + System.Environment.NewLine);

                        //Bắt Ngày hết hiệu lực không được trước Ngày áp dụng
                        output_html = "$(function () { $('#NgayHetHieuLuc').datepicker('option', 'minDate', $('#NgayApDung').val())});";
                        Response.Write(output_html + System.Environment.NewLine);

                        //Khi Ngày Áp dụng thay đổi thì Ngày hết hiệu lực cũng xem xét lại điều kiện
                        output_html = @"$('#NgayApDung').change(function() {		
        
       $('#NgayHetHieuLuc').datepicker('option', 'minDate', $('#NgayApDung').val());
    });";
                        Response.Write(output_html + System.Environment.NewLine);

                    }
                }
            }
            //Nếu không phải 2 chế độ xem hoặc sửa
            else
            {
                Response.Write("$('#form_quanlyphikhac input,select,textarea').attr('disabled', true);");
                Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
            }
        }
        //Khi hiển thị các thông tin thêm mới
        else
        {
            string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + "Chờ duyệt" + @"</i>')
$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + "Chưa có hiệu lực" + @"</i>')
});";
            Response.Write(a + System.Environment.NewLine);

            string output_html = @"$(function () {  $('#NgayLap, #NgayApDung').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = @"$(function () { 
            var currentYear = (new Date).getFullYear(); 
            $('#NgayLap').datepicker('setDate', new Date())
$('#NgayApDung').datepicker('setDate', new Date((currentYear-1), 12, 01))
});";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = @"$(function () {  $('#NgayHetHieuLuc').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
            Response.Write(output_html + System.Environment.NewLine);

            output_html = "$(function () { $('#NgayHetHieuLuc').datepicker('option', 'minDate', $('#NgayApDung').val())});";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = @"$(function () { 
            var currentYear = (new Date).getFullYear(); 
$('#NgayHetHieuLuc').datepicker('setDate', new Date(currentYear, 11, 31))
});";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = @"$('#NgayApDung').change(function() {		
        
       $('#NgayHetHieuLuc').datepicker('option', 'minDate', $('#NgayApDung').val());
    });";
            Response.Write(output_html + System.Environment.NewLine);

            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
        }
    }

    public void DoiNgay()
    {

        //Nếu Đơn vị thời gian thay đổi, xem đang chọn theo Tháng (3) hay Năm (1)
        //Với đơn vị là Tháng thì ẩn Tháng, chỉ hiện ngày (31 ngày)
        //Với đơn vị là năm thì hiện tháng và ngày.
        string output_html = @"
var timestamp = Number(new Date());

$('#DonViThoiGian').change(function() {		

if ($(this).val() == '3') {
            $('#ThangNop').attr('disabled', true);
 $('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=1');
        }
        else if ($(this).val() == '1') {
            $('#ThangNop').attr('disabled', false);
 $('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=' + $('#ThangNop').val());
        }
    });

$('#ThangNop').change(function() {		
        
        $('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=' + $('#ThangNop').val());
    });";
        Response.Write(output_html);
    }
}