﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;

public partial class usercontrols_quanlyphidanhsachphi : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    DataTable dtb = new DataTable();

    // Tên chức năng
    public string tenchucnang = "Danh sách phí";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";

    public string quyen = "QuanlyPhiDanhSachPhi";

    public string truongten = "TenPhi";
    public string truongma = "MaPhi";
    public string truongloai = "LoaiPhi";
    public string truongid = "PhiID";
    public string mucphi = "MucPhi";
    public string ngayapdung = "NgayApDung";
    public string ngayhethieuluc = "NgayHetHieuLuc";
    public string doituongapdung = "DoiTuongNopPhi";
    public string tinhtrang = "TenTrangThai";
    public string tinhtrangid = "TinhTrangID";

    //public string NgayApDungTu 



    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //DataTable dtbLoaiPhi = new DataTable();
                //dtbLoaiPhi.Columns.Add("ID");
                //dtbLoaiPhi.Columns.Add("TEN");
                //dtbLoaiPhi.Rows.Add("0", "Tất cả");
                //dtbLoaiPhi.Rows.Add("1", "Phí Hội viên");
                //dtbLoaiPhi.Rows.Add("1", "Phí đăng Logo");
                ////dtbLoaiPhi.Rows.Add("4", "Phí cập nhật kiến thức");
                //dtbLoaiPhi.Rows.Add("5", "Phí kiểm soát chất lượng");
                //dtbLoaiPhi.Rows.Add("3", "Phí khác");

                //LoaiPhi.DataSource = dtbLoaiPhi;
                //LoaiPhi.DataBind();

                Commons cm = new Commons();
                if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

                danhsachphi_grv.Columns[1].HeaderText = "Tên phí";
                danhsachphi_grv.Columns[1].SortExpression = truongten;
                danhsachphi_grv.Columns[2].HeaderText = "Mã phí";
                danhsachphi_grv.Columns[2].SortExpression = truongma;
                danhsachphi_grv.Columns[3].HeaderText = "Mức phí";
                danhsachphi_grv.Columns[3].SortExpression = mucphi;
                danhsachphi_grv.Columns[4].HeaderText = "Ngày áp dụng";
                danhsachphi_grv.Columns[4].SortExpression = ngayapdung;
                danhsachphi_grv.Columns[5].HeaderText = "Ngày hết hiệu lực";
                danhsachphi_grv.Columns[5].SortExpression = ngayhethieuluc;
                danhsachphi_grv.Columns[6].HeaderText = "Đối tượng áp dụng";
                danhsachphi_grv.Columns[6].SortExpression = tinhtrang;
                danhsachphi_grv.Columns[7].HeaderText = "Tình trạng";
                danhsachphi_grv.Columns[7].SortExpression = tinhtrang;
                danhsachphi_grv.Columns[8].HeaderText = "Thao tác";

                Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>Danh mục " + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

                Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>Danh mục " + tenchucnang + @"</h1> </div>"));


                //// Phân quyền
                //if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
                //{
                //    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
                //    Form1.Visible = false;
                //    return;
                //}

                /////////////

                if (Request.QueryString["act"] == "delete")
                {
                    string[] dsxoa = Request.QueryString["id"].Split(',');

                    foreach (string idxoa in dsxoa)
                    {
                        string[] idloai = idxoa.Split('|');
                        if (idloai.Length > 1)
                        {
                            if (idloai[1] == "1")
                            {
                                SqlCommand sql = new SqlCommand();
                                 sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải khác ở trạng thái Đã duyệt  mới cho xóa
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "DELETE tblTTDMPhiHoiVien WHERE PhiId = " + idloai[0];
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiHoiVien", "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiHoiVien");
                                }
                            }
                            else if (idloai[1] == "2")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiDangLogo where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "DELETE tblTTDMPhiDangLogo WHERE PhiId = " + idloai[0];
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiDangLogo", "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiDangLogo");
                                }
                            }
                            else if (idloai[1] == "3")
                            {
                                SqlCommand sql = new SqlCommand();
                                 sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "DELETE tblTTDMPhiKhac WHERE PhiId = " + idloai[0];
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiKhac", "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKhac");
                                }
                            }

                            else if (idloai[1] == "5")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKSCL where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "DELETE tblTTDMPhiKSCLChiTiet WHERE PhiId = " + idloai[0];
                                    DataAccess.RunActionCmd(sql);
                                    sql.CommandText = "DELETE tblTTDMPhiKSCL WHERE PhiId = " + idloai[0];
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiKSCL", "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKSCL");
                                }
                            }

                            //else if (idloai[1] == "4")
                            //{
                            //    SqlCommand sql = new SqlCommand();
                            //    sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiCNKTTapThe where PhiId = " + idloai[0];
                            //    DataSet ds = new DataSet();
                            //    ds = DataAccess.RunCMDGetDataSet(sql);
                            //    //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                            //    if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                            //    {
                            //        sql.CommandText = "DELETE tblTTDMPhiCNKTTapThe WHERE PhiId = " + idloai[0];
                            //        DataAccess.RunActionCmd(sql);
                            //        sql.Connection.Close();
                            //        sql.Connection.Dispose();
                            //        sql = null;
                            //        cm.ghilog("DMPhiCNKTTapThe", "Xóa giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục DMPhiCNKTTapThe");
                            //    }
                            //}
                        }
                    }
                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                }
                else if (Request.QueryString["act"] == "duyet")
                {
                    string[] dsduyet = Request.QueryString["id"].Split(',');

                    foreach (string id in dsduyet)
                    {
                        string[] idloai = id.Split('|');
                        if (idloai.Length > 1)
                        {
                            if (idloai[1] == "1")
                            {
                                SqlCommand sql1 = new SqlCommand();
                                sql1.CommandText = "SELECT TinhTrangId, MaPhi, DoiTuongApDungID, MucPhi, LoaiHoiVienChiTiet from tblTTDMPhiHoiVien where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql1);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    //sql.CommandText = "UPDATE tblTTDMPhiHoiVien set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                                    //sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                    //sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                    //sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                    //sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    //DataAccess.RunActionCmd(sql);
                                    //sql.Connection.Close();
                                    //sql.Connection.Dispose();
                                    //sql = null;
                                    //cm.ghilog("DMPhiHoiVien", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiHoiVien");


                                    string connStr = DataAccess.ConnString;
                                    SqlConnection conPhiHoiVien = new SqlConnection(connStr);
                                    SqlTransaction transPhiHoiVien;
                                    conPhiHoiVien.Open();
                                    transPhiHoiVien = conPhiHoiVien.BeginTransaction();
                                    try
                                    {
                                        SqlCommand sql = new SqlCommand();
                                        sql.CommandText =
                                            "UPDATE tblTTDMPhiHoiVien set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet = @NgayDuyet  WHERE PhiId = @PhiID";
                                        sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                        sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                        sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                        sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                        sql.Connection = conPhiHoiVien;
                                        sql.Transaction = transPhiHoiVien;
                                        sql.ExecuteNonQuery();

                                        //Lấy toàn bộ Hội viên cá nhân/tổ chức

                                        DataTable dtbHoiVienChuaPhatSinhPhi = new DataTable();
                                        DataTable dtbHoiVienDaPhatSinhPhi = new DataTable();



                                        //Hội viên cá nhân làm việc tại CTKT: xác định theo HoiVienTapTheID
                                        //Hội viên tổ chức đủ đk KiT lĩnh vực CK: xác định theo truong NamDuDKKT_CK

                                        // SonNQ update: gộp lại thành HVCN, ko phân biệt có thuộc HVTC hay ko
                                        if (ds.Tables[0].Rows[0][2].ToString() ==
                                            ((int)EnumVACPA.DoiTuongApDung.HvcnLamViecTaiCtkt).ToString())
                                        {
                                            sql1.CommandText =
                                                       @"select HoiVienCaNhanId as HoiVienID, LoaiHoiVienCaNhan, CONVERT(VARCHAR(10), NgayGiaNhap, 103) AS NgayGiaNhap from tblHoiVienCaNhan where LoaiHoiVienCaNhanChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienCaNhanID NOT IN" +
                                                       " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                                       (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                                       " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                                       " ) and PhiID = " + idloai[0] + ") and LoaiHoiVienCaNhan = " +
                                                       (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan;
                                            dtbHoiVienChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                            sql1.CommandText =
                                                @"select HoiVienCaNhanId as HoiVienID, LoaiHoiVienCaNhan, CONVERT(VARCHAR(10), NgayGiaNhap, 103) AS NgayGiaNhap from tblHoiVienCaNhan where LoaiHoiVienCaNhanChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienCaNhanID IN" +
                                                " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                                       (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                                       " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                                       " ) and PhiID = " + idloai[0] + ") and LoaiHoiVienCaNhan = " +
                                                       (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan;
                                            dtbHoiVienDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                        }
                                        //else if (ds.Tables[0].Rows[0][2].ToString() ==
                                        //     ((int)EnumVACPA.DoiTuongApDung.HvcnKhongLamViecTaiCtkt).ToString())
                                        //{
                                        //    sql1.CommandText =
                                        //               @"select HoiVienCaNhanId as HoiVienID, LoaiHoiVienCaNhan, CONVERT(VARCHAR(10), NgayGiaNhap, 103) AS NgayGiaNhap from tblHoiVienCaNhan where HoiVienCaNhanID NOT IN" +
                                        //               " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                        //               (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                        //               " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                        //               " ) and PhiID = " + idloai[0] + ") and LoaiHoiVienCaNhan = " +
                                        //               (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + " and (HoiVienTapTheId IS NULL OR HoiVienTapTheID NOT IN (SELECT HoiVienTapTheID from tblHoiVienTapThe))";
                                        //    dtbHoiVienChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                        //    sql1.CommandText =
                                        //        @"select HoiVienCaNhanId as HoiVienID, LoaiHoiVienCaNhan, CONVERT(VARCHAR(10), NgayGiaNhap, 103) AS NgayGiaNhap from tblHoiVienCaNhan where HoiVienCaNhanID IN" +
                                        //        " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                        //               (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                        //               " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                        //               " ) and PhiID = " + idloai[0] + ") and LoaiHoiVienCaNhan = " +
                                        //               (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + " and (HoiVienTapTheId IS NULL OR HoiVienTapTheID NOT IN (SELECT HoiVienTapTheID from tblHoiVienTapThe))";
                                        //    dtbHoiVienDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                        //}
                                        else if (ds.Tables[0].Rows[0][2].ToString() ==
                                                 ((int)EnumVACPA.DoiTuongApDung.HvttDuDieuKienKiTLinhVucChungKhoan).ToString())
                                        {

                                            sql1.CommandText =
                                                       @"select HoiVienTapTheId as HoiVienID, LoaiHoiVienTapThe, CONVERT(VARCHAR(10), ISNULL(NgayGiaNhap, NgayNhap), 103) AS NgayGiaNhap from tblHoiVienTapThe where LoaiHoiVienTapTheChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienTapTheChaID IS NULL AND HoiVienTapTheId NOT IN" +
                                                       " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                                       (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                                       " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe +
                                                       " ) and PhiID = " + idloai[0] + ") and LoaiHoiVienTapThe = " + (int)EnumVACPA.LoaiHoiVienHoiVienCaNhanTapThe.HoiVienTapThe
                                                       + " and NamDuDKKT_CK IS NOT NULL and NamDuDKKT_CK <> 0 and NamDuDKKT_CK <> ''";
                                            dtbHoiVienChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                            sql1.CommandText =
                                                @"select HoiVienTapTheId as HoiVienID, LoaiHoiVienTapThe, CONVERT(VARCHAR(10), ISNULL(NgayGiaNhap, NgayNhap), 103) AS NgayGiaNhap from tblHoiVienTapThe where LoaiHoiVienTapTheChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienTapTheChaID IS NULL AND HoiVienTapTheId IN" +
                                                 " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                                       (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                                       " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe +
                                                       " ) and PhiID = " + idloai[0] + ") and LoaiHoiVienTapThe = " + (int)EnumVACPA.LoaiHoiVienHoiVienCaNhanTapThe.HoiVienTapThe
                                                       + " and NamDuDKKT_CK IS NOT NULL and NamDuDKKT_CK <> 0 and NamDuDKKT_CK <> ''";
                                            dtbHoiVienDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                        }
                                        else if (ds.Tables[0].Rows[0][2].ToString() ==
                                                 ((int)EnumVACPA.DoiTuongApDung.HvttKhongDuDieuKienKiTLinhVucChungKhoan).ToString())
                                        {
                                            sql1.CommandText =
                                                    @"select HoiVienTapTheId as HoiVienID, LoaiHoiVienTapThe, CONVERT(VARCHAR(10), ISNULL(NgayGiaNhap, NgayNhap), 103) AS NgayGiaNhap from tblHoiVienTapThe where LoaiHoiVienTapTheChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienTapTheChaID IS NULL AND HoiVienTapTheId NOT IN" +
                                                    " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                                    (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                                    " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe +
                                                    " ) and PhiID = " + idloai[0] + ") and LoaiHoiVienTapThe = " + (int)EnumVACPA.LoaiHoiVienHoiVienCaNhanTapThe.HoiVienTapThe
                                                    + " and (NamDuDKKT_CK IS NULL OR NamDuDKKT_CK = 0 OR NamDuDKKT_CK = '')";
                                            dtbHoiVienChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                            sql1.CommandText =
                                                @"select HoiVienTapTheId as HoiVienID, LoaiHoiVienTapThe, CONVERT(VARCHAR(10), ISNULL(NgayGiaNhap, NgayNhap), 103) AS NgayGiaNhap from tblHoiVienTapThe where LoaiHoiVienTapTheChiTiet = " + ds.Tables[0].Rows[0]["LoaiHoiVienChiTiet"].ToString() + " AND HoiVienTapTheChaID IS NULL AND HoiVienTapTheId IN" +
                                                 " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                                    (int)EnumVACPA.LoaiPhi.PhiHoiVien +
                                                    " and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe +
                                                    " ) and PhiID = " + idloai[0] + ") and LoaiHoiVienTapThe = " + (int)EnumVACPA.LoaiHoiVienHoiVienCaNhanTapThe.HoiVienTapThe
                                                    + " and (NamDuDKKT_CK IS NULL OR NamDuDKKT_CK = 0 OR NamDuDKKT_CK = '')";
                                            dtbHoiVienDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                        }

                                        //Tạo bảng Phát sinh Phí
                                        DataTable dtbPhatSinhPhi = new DataTable("tblTTPhatSinhPhi");
                                        dtbPhatSinhPhi.Columns.Add("HoiVienID", typeof(int));
                                        dtbPhatSinhPhi.Columns.Add("LoaiHoiVien", typeof(string));
                                        dtbPhatSinhPhi.Columns.Add("PhiID", typeof(int));
                                        dtbPhatSinhPhi.Columns.Add("DoiTuongPhatSinhPhiID", typeof(int));
                                        dtbPhatSinhPhi.Columns.Add("LoaiPhi", typeof(string));
                                        dtbPhatSinhPhi.Columns.Add("NgayPhatSinh", typeof(DateTime));
                                        dtbPhatSinhPhi.Columns.Add("SoTien", typeof(double));

                                        //Đưa dữ liệu Hội viên chưa phát sinh phí vào bảng Phát sinh Phí

                                        string strLoaiHoiVien = "1";
                                        if (ds.Tables[0].Rows[0][2].ToString() ==
                                           ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString()
                                           ||
                                           ds.Tables[0].Rows[0][2].ToString() ==
                                           ((int)EnumVACPA.LoaiHoiVien.KiemToanVien).ToString())
                                        {
                                            strLoaiHoiVien = ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString();
                                        }
                                        else if (ds.Tables[0].Rows[0][2].ToString() ==
                                                ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString()
                                                ||
                                                ds.Tables[0].Rows[0][2].ToString() ==
                                                ((int)EnumVACPA.LoaiHoiVien.CongTyKiemToan).ToString())
                                        {
                                            strLoaiHoiVien = ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString();
                                        }

                                        SqlCommand cmd = new SqlCommand();

                                        foreach (DataRow dtr in dtbHoiVienChuaPhatSinhPhi.Rows)
                                        {
                                            DataRow dtrPhatSinhPhi = dtbPhatSinhPhi.NewRow();
                                            dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                            //dtrPhatSinhPhi["LoaiHoiVien"] = ds.Tables[0].Rows[0][2];
                                            dtrPhatSinhPhi["LoaiHoiVien"] = strLoaiHoiVien;
                                            dtrPhatSinhPhi["PhiID"] = idloai[0].ToString();
                                            dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                            dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiHoiVien;
                                            dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                            if (!string.IsNullOrEmpty(dtr["NgayGiaNhap"].ToString()))
                                            {
                                                cmd.CommandText = "SELECT MucPhiChiTiet FROM tblTTDMPhiHoiVienChiTiet WHERE PhiID = " + idloai[0].ToString() + " AND TuNgay <= CONVERT(datetime, '" + dtr["NgayGiaNhap"].ToString() + "', 103) AND DenNgay >= CONVERT(datetime, '" + dtr["NgayGiaNhap"].ToString() + "', 103)";
                                                string mucphi1 = DataAccess.DLookup(cmd);

                                                if (!string.IsNullOrEmpty(mucphi1))
                                                {
                                                    dtrPhatSinhPhi["SoTien"] = (int.Parse(ds.Tables[0].Rows[0][3].ToString()) / 100 * (int.Parse(mucphi1))).ToString();
                                                }
                                                else
                                                    dtrPhatSinhPhi["SoTien"] = ds.Tables[0].Rows[0][3];
                                            }
                                            else
                                                dtrPhatSinhPhi["SoTien"] = ds.Tables[0].Rows[0][3];
                                            dtbPhatSinhPhi.Rows.Add(dtrPhatSinhPhi);
                                        }

                                        //Copy vào Db
                                        //BulkCopy the data in the DataTable to the temp table
                                        using (
                                            SqlBulkCopy s = new SqlBulkCopy(conPhiHoiVien, SqlBulkCopyOptions.Default, transPhiHoiVien)
                                            )
                                        //using (SqlBulkCopy s = new SqlBulkCopy(DataAccess.ConnString, SqlBulkCopyOptions.Default))
                                        {
                                            s.BulkCopyTimeout = 99999;
                                            s.DestinationTableName = dtbPhatSinhPhi.TableName;

                                            foreach (var column in dtbPhatSinhPhi.Columns)
                                            {
                                                s.ColumnMappings.Add(column.ToString(), column.ToString());
                                            }
                                            s.WriteToServer(dtbPhatSinhPhi);
                                        }

                                        //Tạo DataTable Phát sinh phí Update
                                        DataTable dtbPhatSinhPhiUpdate = new DataTable("tblTTPhatSinhPhi");
                                        dtbPhatSinhPhiUpdate.Columns.Add("PhatSinhPhiID", typeof(int));
                                        dtbPhatSinhPhiUpdate.Columns.Add("HoiVienID", typeof(int));
                                        dtbPhatSinhPhiUpdate.Columns.Add("LoaiHoiVien", typeof(string));
                                        dtbPhatSinhPhiUpdate.Columns.Add("PhiID", typeof(int));
                                        dtbPhatSinhPhiUpdate.Columns.Add("DoiTuongPhatSinhPhiID", typeof(int));
                                        dtbPhatSinhPhiUpdate.Columns.Add("LoaiPhi", typeof(string));
                                        dtbPhatSinhPhiUpdate.Columns.Add("NgayPhatSinh", typeof(DateTime));
                                        dtbPhatSinhPhiUpdate.Columns.Add("SoTien", typeof(double));

                                        //Đưa dữ liệu Hội viên đã phát sinh Phí vào DataTable Phát sinh Phí update
                                        foreach (DataRow dtr in dtbHoiVienDaPhatSinhPhi.Rows)
                                        {
                                            DataRow dtrPhatSinhPhi = dtbPhatSinhPhiUpdate.NewRow();
                                            dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                            //dtrPhatSinhPhi["LoaiHoiVien"] = ds.Tables[0].Rows[0][2];
                                            dtrPhatSinhPhi["LoaiHoiVien"] = strLoaiHoiVien;
                                            dtrPhatSinhPhi["PhiID"] = idloai[0].ToString();
                                            dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                            dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiHoiVien;
                                            dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                            if (!string.IsNullOrEmpty(dtr["NgayGiaNhap"].ToString()))
                                            {
                                                cmd.CommandText = "SELECT MucPhiChiTiet FROM tblTTDMPhiHoiVienChiTiet WHERE PhiID = " + idloai[0].ToString() + " AND TuNgay <= CONVERT(datetime, '" + dtr["NgayGiaNhap"].ToString() + "', 103) AND DenNgay >= CONVERT(datetime, '" + dtr["NgayGiaNhap"].ToString() + "', 103)";
                                                string mucphi2 = DataAccess.DLookup(cmd);

                                                if (!string.IsNullOrEmpty(mucphi2))
                                                {
                                                    dtrPhatSinhPhi["SoTien"] = (int.Parse(ds.Tables[0].Rows[0][3].ToString()) / 100 * (int.Parse(mucphi2))).ToString();
                                                }
                                                else
                                                    dtrPhatSinhPhi["SoTien"] = ds.Tables[0].Rows[0][3];
                                            }
                                            else
                                                dtrPhatSinhPhi["SoTien"] = ds.Tables[0].Rows[0][3];
                                            dtbPhatSinhPhiUpdate.Rows.Add(dtrPhatSinhPhi);
                                        }

                                        //Update vào Db

                                        //Tạo bảng tạm Temp1 có cấu trúc giống hệ bảng vừa update
                                        string strCapNhat = @"create table #Temp1(PhatSinhPhiID int
                                                                       ,HoiVienID int
                                                                       ,LoaiHoiVien nchar(1)
                                                                       ,PhiID int
                                                                       ,DoiTuongPhatSinhPhiID int
                                                                       ,LoaiPhi nchar(1)
                                                                       ,NgayPhatSinh date
                                                                       ,SoTien numeric(18,0)
                                                                       )";
                                        SqlCommand cmdCapNhat = new SqlCommand(strCapNhat, conPhiHoiVien, transPhiHoiVien);
                                        cmdCapNhat.CommandTimeout = 99999;
                                        cmdCapNhat.ExecuteNonQuery();

                                        //Đưa dữ liệu từ DataTable Phát sinh phí update vào bảng tạm Temp1
                                        using (SqlBulkCopy bulk = new SqlBulkCopy(conPhiHoiVien, SqlBulkCopyOptions.Default, transPhiHoiVien))
                                        {
                                            bulk.BulkCopyTimeout = 99999;
                                            bulk.DestinationTableName = "#Temp1";
                                            bulk.WriteToServer(dtbPhatSinhPhiUpdate);
                                        }

                                        //Thực hiện update dữ liệu giữa bảng Temp1 với bảng tblTTPhatSinhPhi
                                        //string mergeSql1 = "merge into [dbo].[tblTTPhatSinhPhi] as Target " +
                                        //                   "using #Temp1 as Source " +
                                        //                   "on " +
                                        //                   "Target.HoiVienID  = Source.HoiVienID AND " +
                                        //                   "Target.LoaiHoiVien = Source.LoaiHoiVien AND " +
                                        //                   "Target.PhiID = Source.PhiID AND " +
                                        //                   "Target.LoaiPhi = Source.LoaiPhi " +
                                        //                   "when matched then " +
                                        //                   "update set Target.SoTien = Source.SoTien," +
                                        //                   "Target.NgayPhatSinh = GETDATE();";

                                        string mergeSql1 = "merge into [dbo].[tblTTPhatSinhPhi] as Target " +
                                                         "using #Temp1 as Source " +
                                                         "on " +
                                                         "Target.HoiVienID = Source.HoiVienID AND " +
                                                         "Target.LoaiHoiVien COLLATE Latin1_General_CS_AS = Source.LoaiHoiVien COLLATE Latin1_General_CS_AS AND " +
                                                         "Target.PhiID = Source.PhiID AND " +
                                                         "Target.LoaiPhi COLLATE Latin1_General_CS_AS = Source.LoaiPhi COLLATE Latin1_General_CS_AS " +
                                                         "when matched then " +
                                                         "update set Target.SoTien = Source.SoTien," +
                                                         "Target.NgayPhatSinh = GETDATE();";
                                        cmdCapNhat.CommandText = mergeSql1;
                                        cmdCapNhat.ExecuteNonQuery();

                                        //Clean up the temp table
                                        cmdCapNhat.CommandText = "drop table #Temp1";
                                        cmdCapNhat.ExecuteNonQuery();

                                        transPhiHoiVien.Commit();
                                        sql.Connection.Close();
                                        sql.Connection.Dispose();
                                        sql = null;
                                    }
                                    catch (Exception ex)
                                    {
                                        transPhiHoiVien.Rollback();
                                        ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
                                        return;
                                    }
                                    cm.ghilog("DMPhiHoiVien", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiHoiVien");
                                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                                }
                            }
                            else if (idloai[1] == "2")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiDangLogo where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiDangLogo set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiDangLogo", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiDangLogo");
                                }
                            }
                            else if (idloai[1] == "3")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiKhac set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiKhac", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKhac");
                                }
                            }
                            //else if (idloai[1] == "4")
                            //{
                            //    SqlCommand sql = new SqlCommand();
                            //    sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiCNKTTapThe where PhiId = " + idloai[0];
                            //    DataSet ds = new DataSet();
                            //    ds = DataAccess.RunCMDGetDataSet(sql);
                            //    //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                            //    if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                            //    {
                            //        sql.CommandText = "UPDATE tblTTDMPhiCNKTTapThe set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                            //        sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                            //        sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                            //        sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                            //        sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                            //        DataAccess.RunActionCmd(sql);
                            //        sql.Connection.Close();
                            //        sql.Connection.Dispose();
                            //        sql = null;
                            //        cm.ghilog("DMPhiCNKTTapThe", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục DMPhiCNKTTapThe");
                            //    }
                            //}
                            else if (idloai[1] == "5")
                            {
                                SqlCommand sql1 = new SqlCommand();
                                sql1.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKSCL where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql1);

                                string connStr = DataAccess.ConnString;
                                SqlConnection conPhiHoiVien = new SqlConnection(connStr);
                                SqlTransaction transPhiHoiVien;
                                conPhiHoiVien.Open();
                                transPhiHoiVien = conPhiHoiVien.BeginTransaction();

                                //Phải ở trạng thái chờ duyệt thì mới cho duyệt
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    try
                                    {

                                        SqlCommand sql = new SqlCommand();
                                        sql.CommandText =
                                            "UPDATE tblTTDMPhiKSCL set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet = @NgayDuyet  WHERE PhiId = @PhiID";
                                        sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                                        sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                                        sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                                        sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                        sql.Connection = conPhiHoiVien;
                                        sql.Transaction = transPhiHoiVien;
                                        sql.ExecuteNonQuery();



                                        DataTable dtbHoiVienCaNhanChuaPhatSinhPhi = new DataTable();
                                        DataTable dtbHoiVienCaNhanDaPhatSinhPhi = new DataTable();
                                        DataTable dtbHoiVienTapTheChuaPhatSinhPhi = new DataTable();
                                        DataTable dtbHoiVienTapTheDaPhatSinhPhi = new DataTable();
                                        //if (ds.Tables[0].Rows[0][2].ToString() ==
                                        //    ((int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan).ToString()
                                        //    ||
                                        //    ds.Tables[0].Rows[0][2].ToString() ==
                                        //    ((int)EnumVACPA.LoaiHoiVien.KiemToanVien).ToString())
                                        //{
                                        sql1.CommandText =
                                            @"select A.HoiVienCaNhanId as HoiVienID, B.LoaiHoiVienCaNhan, A.MucPhi from tblTTDMPhiKSCLChiTiet A
                                             left join tblHoiVienCaNhan B on A.HoiVienCaNhanID = B.HoiVienCaNhanID
                                             where A.HoiVienCaNhanID NOT IN" +
                                            " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                            (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong +
                                            "and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                            " or (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + ")" +
                                            " or (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "))" +
                                            " and PhiID = " + idloai[0] + ") AND A.PhiID =" + idloai[0] +
                                            " and A.HoiVienCaNhanID IS NOT NULL";
                                        dtbHoiVienCaNhanChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                        sql1.CommandText =
                                            @"select A.HoiVienCaNhanId as HoiVienID, B.LoaiHoiVienCaNhan, A.MucPhi from tblTTDMPhiKSCLChiTiet A
                                             left join tblHoiVienCaNhan B on A.HoiVienCaNhanID = B.HoiVienCaNhanID
                                            where A.HoiVienCaNhanID IN" +
                                            " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                            (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong +
                                            "and (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan +
                                             " or (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + ")" +
                                            " or (LoaiHoiVien = " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "))" +
                                            " and PhiID = " + idloai[0] + ") AND A.PhiID =" + idloai[0] +
                                            " and A.HoiVienCaNhanID IS NOT NULL";
                                        dtbHoiVienCaNhanDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                        ////}
                                        ////else if (ds.Tables[0].Rows[0][2].ToString() ==
                                        ////         ((int)EnumVACPA.LoaiHoiVien.HoiVienTapThe).ToString()
                                        ////         ||
                                        ////         ds.Tables[0].Rows[0][2].ToString() ==
                                        ////         ((int)EnumVACPA.LoaiHoiVien.CongTyKiemToan).ToString())
                                        ////{
                                        sql1.CommandText =
                                        @"select A.HoiVienTapTheId as HoiVienID, B.LoaiHoiVienTapThe, A.MucPhi from tblTTDMPhiKSCLChiTiet A
                                             left join tblHoiVienTapThe B on A.HoiVienTapTheID = B.HoiVienTapTheID
                                             where A.HoiVienTapTheId NOT IN" +
                                        " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                        (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong +
                                        "and (LoaiHoiVien = 3" +
                                            " or (LoaiHoiVien = 4))" +
                                        " and PhiID = " + idloai[0] + ") AND A.PhiID =" + idloai[0] +
                                        "and A.HoiVienTapTheID IS NOT NULL";
                                        dtbHoiVienTapTheChuaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];

                                        sql1.CommandText =
                                            @"select A.HoiVienTapTheId as HoiVienID, B.LoaiHoiVienTapThe, A.MucPhi from tblTTDMPhiKSCLChiTiet A
                                             left join tblHoiVienTapThe B on A.HoiVienTapTheID = B.HoiVienTapTheID
                                            where A.HoiVienTapTheId IN" +
                                            " (Select HoiVienID from tblTTPhatSinhPhi where LoaiPhi = " +
                                            (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong +
                                            "and (LoaiHoiVien = 3" +
                                            " or (LoaiHoiVien = 4))" +
                                            " and PhiID = " + idloai[0] + ") AND A.PhiID =" + idloai[0] +
                                        "and A.HoiVienTapTheID IS NOT NULL";
                                        dtbHoiVienTapTheDaPhatSinhPhi = DataAccess.RunCMDGetDataSet(sql1).Tables[0];
                                        //}

                                        //Tạo bảng Phát sinh Phí
                                        DataTable dtbPhatSinhPhi = new DataTable("tblTTPhatSinhPhi");
                                        dtbPhatSinhPhi.Columns.Add("HoiVienID", typeof(int));
                                        dtbPhatSinhPhi.Columns.Add("LoaiHoiVien", typeof(string));
                                        dtbPhatSinhPhi.Columns.Add("PhiID", typeof(int));
                                        dtbPhatSinhPhi.Columns.Add("DoiTuongPhatSinhPhiID", typeof(int));
                                        dtbPhatSinhPhi.Columns.Add("LoaiPhi", typeof(string));
                                        dtbPhatSinhPhi.Columns.Add("NgayPhatSinh", typeof(DateTime));
                                        dtbPhatSinhPhi.Columns.Add("SoTien", typeof(double));

                                        //Đưa dữ liệu Hội viên cá nhân chưa phát sinh phí vào bảng Phát sinh Phí
                                        foreach (DataRow dtr in dtbHoiVienCaNhanChuaPhatSinhPhi.Rows)
                                        {
                                            DataRow dtrPhatSinhPhi = dtbPhatSinhPhi.NewRow();
                                            dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                            dtrPhatSinhPhi["LoaiHoiVien"] = dtr[1].ToString();
                                            dtrPhatSinhPhi["PhiID"] = idloai[0].ToString();
                                            dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                            dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong;
                                            dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                            dtrPhatSinhPhi["SoTien"] = dtr[2].ToString();
                                            dtbPhatSinhPhi.Rows.Add(dtrPhatSinhPhi);
                                        }

                                        //Đưa dữ liệu Hội viên tổ chức chưa phát sinh phí vào bảng Phát sinh Phí
                                        foreach (DataRow dtr in dtbHoiVienTapTheChuaPhatSinhPhi.Rows)
                                        {
                                            DataRow dtrPhatSinhPhi = dtbPhatSinhPhi.NewRow();
                                            dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                            if (dtr[1].ToString() == "0" || dtr[1].ToString() == "2")
                                                dtrPhatSinhPhi["LoaiHoiVien"] = (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan;
                                            else if (dtr[1].ToString() == "1")
                                                dtrPhatSinhPhi["LoaiHoiVien"] = (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe;
                                            else
                                                dtrPhatSinhPhi["LoaiHoiVien"] = DBNull.Value;
                                            dtrPhatSinhPhi["PhiID"] = idloai[0].ToString();
                                            dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                            dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong;
                                            dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                            dtrPhatSinhPhi["SoTien"] = dtr[2].ToString();
                                            dtbPhatSinhPhi.Rows.Add(dtrPhatSinhPhi);
                                        }

                                        //Copy vào Db
                                        //BulkCopy the data in the DataTable to the temp table
                                        if (dtbPhatSinhPhi.Rows.Count > 0)
                                        {
                                            using (
                                                SqlBulkCopy s = new SqlBulkCopy(conPhiHoiVien, SqlBulkCopyOptions.Default,
                                                                                transPhiHoiVien)
                                                )
                                            //using (SqlBulkCopy s = new SqlBulkCopy(DataAccess.ConnString, SqlBulkCopyOptions.Default))
                                            {
                                                s.BulkCopyTimeout = 99999;
                                                s.DestinationTableName = dtbPhatSinhPhi.TableName;

                                                foreach (var column in dtbPhatSinhPhi.Columns)
                                                {
                                                    s.ColumnMappings.Add(column.ToString(), column.ToString());
                                                }
                                                s.WriteToServer(dtbPhatSinhPhi);
                                            }
                                        }
                                        //Tạo DataTable Phát sinh phí Update
                                        DataTable dtbPhatSinhPhiUpdate = new DataTable("tblTTPhatSinhPhi");
                                        dtbPhatSinhPhiUpdate.Columns.Add("PhatSinhPhiID", typeof(int));
                                        dtbPhatSinhPhiUpdate.Columns.Add("HoiVienID", typeof(int));
                                        dtbPhatSinhPhiUpdate.Columns.Add("LoaiHoiVien", typeof(string));
                                        dtbPhatSinhPhiUpdate.Columns.Add("PhiID", typeof(int));
                                        dtbPhatSinhPhiUpdate.Columns.Add("DoiTuongPhatSinhPhiID", typeof(int));
                                        dtbPhatSinhPhiUpdate.Columns.Add("LoaiPhi", typeof(string));
                                        dtbPhatSinhPhiUpdate.Columns.Add("NgayPhatSinh", typeof(DateTime));
                                        dtbPhatSinhPhiUpdate.Columns.Add("SoTien", typeof(double));

                                        //Đưa dữ liệu Hội viên đã phát sinh Phí vào DataTable Phát sinh Phí update
                                        foreach (DataRow dtr in dtbHoiVienCaNhanDaPhatSinhPhi.Rows)
                                        {
                                            DataRow dtrPhatSinhPhi = dtbPhatSinhPhiUpdate.NewRow();
                                            dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                            dtrPhatSinhPhi["LoaiHoiVien"] = dtr[1].ToString();
                                            dtrPhatSinhPhi["PhiID"] = idloai[0].ToString();
                                            dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                            dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong;
                                            dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                            dtrPhatSinhPhi["SoTien"] = dtr[2].ToString();
                                            dtbPhatSinhPhiUpdate.Rows.Add(dtrPhatSinhPhi);
                                        }

                                        //Đưa dữ liệu Hội viên tổ chức đã phát sinh Phí vào DataTable Phát sinh Phí update
                                        foreach (DataRow dtr in dtbHoiVienTapTheDaPhatSinhPhi.Rows)
                                        {
                                            DataRow dtrPhatSinhPhi = dtbPhatSinhPhiUpdate.NewRow();
                                            dtrPhatSinhPhi["HoiVienId"] = dtr["HoiVienID"];
                                            if (dtr[1].ToString() == "0" || dtr[1].ToString() == "2")
                                                dtrPhatSinhPhi["LoaiHoiVien"] = (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan;
                                            else if (dtr[1].ToString() == "1")
                                                dtrPhatSinhPhi["LoaiHoiVien"] = (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe;
                                            else
                                                dtrPhatSinhPhi["LoaiHoiVien"] = DBNull.Value;
                                            dtrPhatSinhPhi["PhiID"] = idloai[0].ToString();
                                            dtrPhatSinhPhi["DoiTuongPhatSinhPhiID"] = DBNull.Value;
                                            dtrPhatSinhPhi["LoaiPhi"] = (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong;
                                            dtrPhatSinhPhi["NgayPhatSinh"] = DateTime.Now;
                                            dtrPhatSinhPhi["SoTien"] = dtr[2].ToString();
                                            dtbPhatSinhPhiUpdate.Rows.Add(dtrPhatSinhPhi);
                                        }


                                        if (dtbPhatSinhPhiUpdate.Rows.Count > 0)
                                        {
                                            //Update vào Db

                                            //Tạo bảng tạm Temp1 có cấu trúc giống hệ bảng vừa update
                                            string strCapNhat = @"create table #Temp1(PhatSinhPhiID int
                                                                       ,HoiVienID int
                                                                       ,LoaiHoiVien nchar(1)
                                                                       ,PhiID int
                                                                       ,DoiTuongPhatSinhPhiID int
                                                                       ,LoaiPhi nchar(1)
                                                                       ,NgayPhatSinh date
                                                                       ,SoTien numeric(18,0)
                                                                       )";
                                            SqlCommand cmdCapNhat = new SqlCommand(strCapNhat, conPhiHoiVien,
                                                                                   transPhiHoiVien);
                                            cmdCapNhat.CommandTimeout = 99999;
                                            cmdCapNhat.ExecuteNonQuery();

                                            //Đưa dữ liệu từ DataTable Phát sinh phí update vào bảng tạm Temp1
                                            using (
                                                SqlBulkCopy bulk = new SqlBulkCopy(conPhiHoiVien, SqlBulkCopyOptions.Default,
                                                                                   transPhiHoiVien))
                                            {
                                                bulk.BulkCopyTimeout = 99999;
                                                bulk.DestinationTableName = "#Temp1";
                                                bulk.WriteToServer(dtbPhatSinhPhiUpdate);
                                            }

                                            //Thực hiện update dữ liệu giữa bảng Temp1 với bảng tblTTPhatSinhPhi
                                            //string mergeSql1 = "merge into [dbo].[tblTTPhatSinhPhi] as Target " +
                                            //                   "using #Temp1 as Source " +
                                            //                   "on " +
                                            //                   "Target.HoiVienID = Source.HoiVienID AND " +
                                            //                   "Target.LoaiHoiVien = Source.LoaiHoiVien AND " +
                                            //                   "Target.PhiID = Source.PhiID AND " +
                                            //                   "Target.LoaiPhi = Source.LoaiPhi " +
                                            //                   "when matched then " +
                                            //                   "update set Target.SoTien = Source.SoTien," +
                                            //                   "Target.NgayPhatSinh = GETDATE();";
                                            string mergeSql1 = "merge into [dbo].[tblTTPhatSinhPhi] as Target " +
                                                             "using #Temp1 as Source " +
                                                             "on " +
                                                             "Target.HoiVienID = Source.HoiVienID AND " +
                                                             "Target.LoaiHoiVien COLLATE Latin1_General_CS_AS = Source.LoaiHoiVien COLLATE Latin1_General_CS_AS AND " +
                                                             "Target.PhiID = Source.PhiID AND " +
                                                             "Target.LoaiPhi COLLATE Latin1_General_CS_AS = Source.LoaiPhi COLLATE Latin1_General_CS_AS " +
                                                             "when matched then " +
                                                             "update set Target.SoTien = Source.SoTien," +
                                                             "Target.NgayPhatSinh = GETDATE();";
                                            cmdCapNhat.CommandText = mergeSql1;
                                            cmdCapNhat.ExecuteNonQuery();

                                            //Clean up the temp table
                                            cmdCapNhat.CommandText = "drop table #Temp1";
                                            cmdCapNhat.ExecuteNonQuery();
                                        }
                                        transPhiHoiVien.Commit();
                                        sql.Connection.Close();
                                        sql.Connection.Dispose();
                                        sql = null;
                                    }
                                    catch (Exception ex)
                                    {
                                        transPhiHoiVien.Rollback();
                                        ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
                                        return;
                                    }

                                    sql1.Connection.Close();
                                    sql1.Connection.Dispose();
                                    sql1 = null;
                                    cm.ghilog("DMPhiKSCL", "Duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục DMPhiKSCL");
                                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                                }
                                else
                                {
                                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                                }
                            }
                        }
                    }
                        Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                }
                else if (Request.QueryString["act"] == "tuchoi")
                {
                    string[] dsduyet = Request.QueryString["id"].Split(',');

                    foreach (string id in dsduyet)
                    {
                        string[] idloai = id.Split('|');
                        if (idloai.Length > 1)
                        {
                            if (idloai[1] == "1")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiHoiVien set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("DMPhiHoiVien", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiHoiVien");
                                }
                            }
                            else if (idloai[1] == "2")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiDangLogo where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiDangLogo set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiDangLogo", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiDangLogo");
                                }
                            }
                            else if (idloai[1] == "3")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiKhac set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiKhac", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKhac");
                                }
                            }
                            else if (idloai[1] == "5")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKSCL where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiKSCL set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiKSCL", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKSCL");
                                }
                            }
                            //else if (idloai[1] == "4")
                            //{
                            //    SqlCommand sql = new SqlCommand();
                            //    sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiCNKTTapThe where PhiId = " + idloai[0];
                            //    DataSet ds = new DataSet();
                            //    ds = DataAccess.RunCMDGetDataSet(sql);
                            //    //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                            //    if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                            //    {
                            //        sql.CommandText = "UPDATE tblTTDMPhiCNKTTapThe set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet, NgayDuyet=@NgayDuyet  WHERE PhiId = @PhiID";
                            //        sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.KhongPheDuyet);
                            //        sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                            //        sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            //        sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                            //        DataAccess.RunActionCmd(sql);
                            //        sql.Connection.Close();
                            //        sql.Connection.Dispose();
                            //        sql = null;
                            //        cm.ghilog("DMPhiCNKTTapThe", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục DMPhiCNKTTapThe");
                            //    }
                            //}
                        }
                    }
                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi", false);
                }
                else if (Request.QueryString["act"] == "thoaiduyet")
                {
                    string[] dsduyet = Request.QueryString["id"].Split(',');

                    foreach (string id in dsduyet)
                    {
                        string[] idloai = id.Split('|');
                        if (idloai.Length > 1)
                        {
                            if (idloai[1] == "1")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    // sonnq sửa 15/12/2016, khi thoái duyệt xóa hết phát sinh phí hội viên
                                    sql.CommandText = "DELETE tblTTPhatSinhPhi WHERE LoaiPhi = " + (int)EnumVACPA.LoaiPhi.PhiHoiVien + " AND PhiID = " + idloai[0];
                                    DataAccess.RunActionCmd(sql);

                                    sql.CommandText = "UPDATE tblTTDMPhiHoiVien set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("DMPhiHoiVien", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiHoiVien");
                                }
                            }
                            else if (idloai[1] == "2")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiDangLogo where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiDangLogo set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiDangLogo", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiDangLogo");
                                }
                            }
                            else if (idloai[1] == "3")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiKhac set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiKhac", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKhac");
                                }
                            }
                            else if (idloai[1] == "5")
                            {
                                SqlCommand sql = new SqlCommand();
                                sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKSCL where PhiId = " + idloai[0];
                                DataSet ds = new DataSet();
                                ds = DataAccess.RunCMDGetDataSet(sql);
                                //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                                {
                                    sql.CommandText = "UPDATE tblTTDMPhiKSCL set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                                    sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                                    DataAccess.RunActionCmd(sql);
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTDMPhiKSCL", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTDMPhiKSCL");
                                }
                            }
                        //    else if (idloai[1] == "4")
                        //    {
                        //        SqlCommand sql = new SqlCommand();
                        //        sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiCNKTTapThe where PhiId = " + idloai[0];
                        //        DataSet ds = new DataSet();
                        //        ds = DataAccess.RunCMDGetDataSet(sql);
                        //        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                        //        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        //        {
                        //            sql.CommandText = "UPDATE tblTTDMPhiCNKTTapThe set TinhTrangId = @TinhTrang, NguoiDuyet = @NguoiDuyet  WHERE PhiId = @PhiID";
                        //            sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                        //            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                        //            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                        //            sql.Parameters.AddWithValue("@PhiID", idloai[0]);
                        //            DataAccess.RunActionCmd(sql);
                        //            sql.Connection.Close();
                        //            sql.Connection.Dispose();
                        //            sql = null;
                        //            cm.ghilog("DMPhiCNKTTapThe", "Thoái duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục DMPhiCNKTTapThe");
                        //        }
                        //    }
                        }
                    }
                    Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                }
                load_data();
            }
            else
            {
                load_data();    
            }
            
        }

        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }

    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void load_data()
    {
        try
        {

            int i = -1;
            Commons cm = new Commons();

            SqlCommand sql = new SqlCommand();
            int iLoaiPhi = Convert.ToInt32(Request.Form["LoaiPhi"]);

            //DateTime? NgayApDungTu = null;
            //if ()
            //sql.CommandText = @"SELECT distinct PhiID, 1 as LoaiPhi, MaPhi, TenPhi, MucPhi, NgayApDung, NgayHetHieuLuc, TinhTrangID, TenTrangThai, NgayNhap FROM tblTTDMPhiHoiVien A inner join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";

            //Phí hội viên

            sql.CommandText = @"SELECT distinct PhiID, 1 as LoaiPhi, MaPhi, TenPhi, MucPhi, NgayApDung, NgayHetHieuLuc, TinhTrangID, TenTrangThai, NgayNhap , CAST(CASE C.LoaiDoiTuong WHEN " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + " THEN 'HVCN' WHEN " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienTapThe + " THEN 'CTKT' ELSE  '' END AS NVARCHAR(20)) AS DoiTuongNopPhi FROM tblTTDMPhiHoiVien A left join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId left join tblDMDoiTuongNopPhi C on A.DoiTuongApDungID = C.DoiTuongNopPhiID  WHERE 0 = 0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                sql.CommandText += " AND A.TenPhi like N'%" + Request.Form["timkiem_Ten"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                sql.CommandText += " AND A.MaPhi like N'%" + Request.Form["timkiem_Ma"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayApDungTu"]))
                sql.CommandText += " AND A.NgayApDung >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayApDungTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayApDungDen"]))
                sql.CommandText += " AND A.NgayApDung <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayApDungDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayHetHieuLucTu"]))
                sql.CommandText += " AND A.NgayHetHieuLuc >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayHetHieuLucTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayHetHieuLucDen"]))
                sql.CommandText += " AND A.NgayHetHieuLuc <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayHetHieuLucDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) || !string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
            {
                sql.CommandText += " AND (0 = 1 ";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]))
                    sql.CommandText += "OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ChoDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.KhongPheDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ThoaiDuyet;
                sql.CommandText += ")";
            }
            if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
            {
                sql.CommandText += " AND (0 = 1 ";
                //if (string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]))
                //{
                //    //Không chọn chưa có hiệu lực tức là Tình trạng là đã phê duyệt
                //    //và Ngày áp dụng nhỏ hơn thời gian hiện tại
                //    sql.CommandText += " AND A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                //    sql.CommandText += " AND A.NgayApDung < '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                //}
                //if (string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]))
                //{
                //    sql.CommandText += " AND (A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                //    sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                //    sql.CommandText += " AND (A.NgayApDung >  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                //    sql.CommandText += " OR A.NgayHetHieuLuc <  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')))";
                //}
                //if (string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
                //{
                //    sql.CommandText += " AND (A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                //    sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                //    sql.CommandText += " AND A.NgayHetHieuLuc >  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
                //}
                if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
                {
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]))
                    {
                        sql.CommandText += " OR (A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                        sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                        sql.CommandText += " AND  A.NgayApDung > '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
                    }
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]))
                    {
                        sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                        sql.CommandText += " AND A.NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                        sql.CommandText += " AND ((CASE WHEN A.NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE A.NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
                    }
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
                    {
                        sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                        sql.CommandText += " AND A.NgayHetHieuLuc <  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
                    }
                    sql.CommandText += ")";
                }
            }
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);
            //DataTable dtbTat = new DataTable();
            DataTable dtbPhiHoiVien =new DataTable();
            if (ds.Tables[0] != null)
                dtbPhiHoiVien = ds.Tables[0];
            //dtbTat = ds.Tables[0];


            //Phí đăng logo
            sql = new SqlCommand();
            //sql.CommandText = "SELECT distinct PhiID, 2 as LoaiPhi, MaPhi, TenPhi, MucPhi, NgayApDung, NgayHetHieuLuc, TinhTrangID, TenTrangThai, NgayNhap FROM tblTTDMPhiDangLogo A inner join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";
            sql.CommandText = "SELECT distinct PhiID, 2 as LoaiPhi, MaPhi, TenPhi, MucPhi, NgayApDung, NgayHetHieuLuc, TinhTrangID, TenTrangThai, NgayNhap, '' as DoiTuongNopPhi FROM tblTTDMPhiDangLogo A inner join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                sql.CommandText += " AND A.TenPhi like N'%" + Request.Form["timkiem_Ten"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                sql.CommandText += " AND A.MaPhi like N'%" + Request.Form["timkiem_Ma"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayApDungTu"]))
                sql.CommandText += " AND A.NgayApDung >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayApDungTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayApDungDen"]))
                sql.CommandText += " AND A.NgayApDung <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayApDungDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayHetHieuLucTu"]))
                sql.CommandText += " AND A.NgayHetHieuLuc >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayHetHieuLucTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayHetHieuLucDen"]))
                sql.CommandText += " AND A.NgayHetHieuLuc <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayHetHieuLucDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) || !string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
            {
                sql.CommandText += " AND (0 = 1 ";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]))
                    sql.CommandText += "OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ChoDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.KhongPheDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ThoaiDuyet;
                sql.CommandText += ")";
            }
            if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
            {
                sql.CommandText += " AND (0 = 1 ";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]))
                {
                    sql.CommandText += " OR (A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    sql.CommandText += " AND  A.NgayApDung > '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
                }
                if (!string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]))
                {
                    sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    sql.CommandText += " AND A.NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                    sql.CommandText += " AND ((CASE WHEN A.NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE A.NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
                }
                if (!string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
                {
                    sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    sql.CommandText += " AND A.NgayHetHieuLuc <  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
                }
                sql.CommandText += ")";
            }
            ds = DataAccess.RunCMDGetDataSet(sql);


            DataTable dtbPhiDangLogo = new DataTable();
            if (ds.Tables[0] != null)
                dtbPhiDangLogo = ds.Tables[0];

            //dtbTat.Merge(ds.Tables[0]);


            //SelectQueryBuilder query2 = new SelectQueryBuilder();
            //query2.SelectFromTable("tblTTDMPhiKhac");
            //query2.SelectColumns(new string[] { "PhiID", "MaPhi", "TenPhi", "MucPhi", "NgayApDung", "NgayHetHieuLuc", "TinhTrangID", "NgayNhap" });


            //if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            //    query.AddWhere(truongten, Comparison.Like, "%" + Request.Form["timkiem_Ten"] + "%");

            //sql = new SqlCommand();
            //sql.CommandText = query2.BuildQuery();
            //ds = DataAccess.RunCMDGetDataSet(sql);

            //Phí Khác
            sql = new SqlCommand();
            sql.CommandText = "SELECT distinct PhiID, 3 as LoaiPhi, MaPhi, TenPhi, MucPhi, NgayApDung, NgayHetHieuLuc, TinhTrangID, TenTrangThai, NgayNhap, CAST(CASE C.LoaiDoiTuong WHEN " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + " THEN 'HVCN' WHEN " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienTapThe + " THEN 'CTKT' END AS NVARCHAR(20)) AS DoiTuongNopPhi FROM tblTTDMPhiKhac A left join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId left join tblDMDoiTuongNopPhi C on A.DoiTuongApDungID = C.DoiTuongNopPhiID  WHERE 0 = 0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                sql.CommandText += " AND A.TenPhi like N'%" + Request.Form["timkiem_Ten"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                sql.CommandText += " AND A.MaPhi like N'%" + Request.Form["timkiem_Ma"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayApDungTu"]))
                sql.CommandText += " AND A.NgayApDung >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayApDungTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayApDungDen"]))
                sql.CommandText += " AND A.NgayApDung <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayApDungDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayHetHieuLucTu"]))
                sql.CommandText += " AND A.NgayHetHieuLuc >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayHetHieuLucTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayHetHieuLucDen"]))
                sql.CommandText += " AND A.NgayHetHieuLuc <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayHetHieuLucDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) || !string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
            {
                sql.CommandText += " AND (0 = 1 ";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]))
                    sql.CommandText += "OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ChoDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.KhongPheDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ThoaiDuyet;
                sql.CommandText += ")";
            }
            if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
            {
                sql.CommandText += " AND (0 = 1 ";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]))
                {
                    sql.CommandText += " OR (A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    sql.CommandText += " AND  A.NgayApDung > '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
                }
                if (!string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]))
                {
                    sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    sql.CommandText += " AND A.NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                    sql.CommandText += " AND ((CASE WHEN A.NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE A.NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
                }
                if (!string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
                {
                    sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    sql.CommandText += " AND A.NgayHetHieuLuc <  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
                }
                sql.CommandText += ")";
            }
            ds = DataAccess.RunCMDGetDataSet(sql);

            DataTable dtbPhiKhac = new DataTable();
            if (ds.Tables[0] != null)
                dtbPhiKhac = ds.Tables[0];
            //dtbTat.Merge(ds.Tables[0]);


            //Phí KSCL
            sql = new SqlCommand();
            sql.CommandText = "SELECT distinct PhiID, 5 as LoaiPhi, MaPhi, DienGiai as TenPhi, NgayLap AS NgayApDung, TinhTrangID, TenTrangThai, NgayNhap, '' as DoiTuongNopPhi FROM tblTTDMPhiKSCL A inner join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                sql.CommandText += " AND A.DienGiai like N'%" + Request.Form["timkiem_Ten"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                sql.CommandText += " AND A.MaPhi like N'%" + Request.Form["timkiem_Ma"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) || !string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
            {
                sql.CommandText += " AND (0 = 1 ";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]))
                    sql.CommandText += "OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ChoDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.KhongPheDuyet;
                if (!string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
                    sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ThoaiDuyet;
                sql.CommandText += ")";
            }
            ds = DataAccess.RunCMDGetDataSet(sql);  

            DataTable dtbtemp = new DataTable();
            dtbtemp.Columns.Add("PhiID", typeof(int));
            dtbtemp.Columns.Add("MaPhi", typeof(string));
            dtbtemp.Columns.Add("TenPhi", typeof(string));
            dtbtemp.Columns.Add("MucPhi", typeof(decimal));
            dtbtemp.Columns.Add("NgayApDung", typeof(DateTime));
            dtbtemp.Columns.Add("NgayHetHieuLuc", typeof(DateTime));
            dtbtemp.Columns.Add("TinhTrangID", typeof(int));
            dtbtemp.Columns.Add("NgayLap", typeof(DateTime));
            dtbtemp.Columns.Add("DoiTuongNopPhi", typeof(string));
            dtbtemp = ds.Tables[0];
            DataTable dtbPhiKSCL = new DataTable();
            if (dtbtemp != null)
                dtbPhiKSCL = dtbtemp;


            ////Phí CNKT tổ chức

            //sql.CommandText = @"SELECT distinct PhiID, 4 as LoaiPhi, MaPhi, TenPhi, MucPhi, NgayApDung, NgayHetHieuLuc, TinhTrangID, TenTrangThai, NgayNhap , CAST(CASE C.GiaTri WHEN " + (int)EnumVACPA.DoiTuongNopPhiCNKTTapThe.HoiVienTapThe + " THEN 'HVTT' WHEN " + (int)EnumVACPA.DoiTuongNopPhiCNKTTapThe.CongTyKiemToan + " THEN 'CTKT' ELSE  '' END AS NVARCHAR(20)) AS DoiTuongNopPhi FROM tblTTDMPhiCNKTTapThe A left join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId left join tblDMDoiTuongNopPhiCNKTTapThe C on A.DoiTuongApDungID = C.DoiTuongNopPhiID  WHERE 0 = 0 ";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            //    sql.CommandText += " AND A.TenPhi like N'%" + Request.Form["timkiem_Ten"] + "%'";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
            //    sql.CommandText += " AND A.MaPhi like N'%" + Request.Form["timkiem_Ma"] + "%'";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayApDungTu"]))
            //    sql.CommandText += " AND A.NgayApDung >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayApDungTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayApDungDen"]))
            //    sql.CommandText += " AND A.NgayApDung <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayApDungDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayHetHieuLucTu"]))
            //    sql.CommandText += " AND A.NgayHetHieuLuc >= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayHetHieuLucTu"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_NgayHetHieuLucDen"]))
            //    sql.CommandText += " AND A.NgayHetHieuLuc <= '" + (DateTime.ParseExact(Request.Form["timkiem_NgayHetHieuLucDen"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") + "'";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) || !string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
            //{
            //    sql.CommandText += " AND (0 = 1 ";
            //    if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]))
            //        sql.CommandText += "OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ChoDuyet;
            //    if (!string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]))
            //        sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
            //    if (!string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]))
            //        sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.KhongPheDuyet;
            //    if (!string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
            //        sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ThoaiDuyet;
            //    sql.CommandText += ")";
            //}
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
            //{
            //    sql.CommandText += " AND (0 = 1 ";
              
            //    if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]) || !string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
            //    {
            //        if (!string.IsNullOrEmpty(Request.Form["timkiem_chuacohieuluc"]))
            //        {
            //            sql.CommandText += " OR (A.TinhTrangID <> " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
            //            sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
            //            sql.CommandText += " AND  A.NgayApDung > '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
            //        }
            //        if (!string.IsNullOrEmpty(Request.Form["timkiem_cohieuluc"]))
            //        {
            //            sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
            //            sql.CommandText += " AND A.NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
            //            sql.CommandText += " AND ((CASE WHEN A.NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE A.NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))";
            //        }
            //        if (!string.IsNullOrEmpty(Request.Form["timkiem_hethieuluc"]))
            //        {
            //            sql.CommandText += " OR (A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
            //            sql.CommandText += " AND A.NgayHetHieuLuc <  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            //        }
            //        sql.CommandText += ")";
            //    }
            //}
            //ds = DataAccess.RunCMDGetDataSet(sql);
            ////DataTable dtbTat = new DataTable();
            //DataTable dtbPhiCNKTTapThe = new DataTable();
            //if (ds.Tables[0] != null)
            //    dtbPhiCNKTTapThe = ds.Tables[0];

            //dtbTat.Merge(dtbtemp);

            DataTable dtbTat = new DataTable();
            if (iLoaiPhi == 0)
            {
                dtbTat.Merge(dtbPhiHoiVien);
                dtbTat.Merge(dtbPhiDangLogo);
                dtbTat.Merge(dtbPhiKSCL);
                dtbTat.Merge(dtbPhiKhac);
                //dtbTat.Merge(dtbPhiCNKTTapThe);
            }
            else if (iLoaiPhi == 1)
                dtbTat = dtbPhiHoiVien;
            else if (iLoaiPhi == 2)
                dtbTat = dtbPhiDangLogo;
            else if (iLoaiPhi == 3)
                dtbTat = dtbPhiKhac;
            //else if (iLoaiPhi == 4)
            //    dtbTat = dtbPhiCNKTTapThe;
            else if (iLoaiPhi == 5)
            {
                sql = new SqlCommand();
                sql.CommandText = "SELECT distinct PhiID, 4 as LoaiPhi, '' as MucPhi, '' as NgayHetHieuLuc,  MaPhi, DienGiai as TenPhi, NgayLap AS NgayApDung, TinhTrangID, TenTrangThai, NgayNhap, '' as DoiTuongNopPhi FROM tblTTDMPhiKSCL A inner join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                    sql.CommandText += " AND A.DienGiai like N'%" + Request.Form["timkiem_Ten"] + "%'";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                    sql.CommandText += " AND A.MaPhi like N'%" + Request.Form["timkiem_Ma"] + "%'";
                if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]) || !string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]) || !string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
                {
                    sql.CommandText += " AND (0 = 1 ";
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_choduyet"]))
                        sql.CommandText += "OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ChoDuyet;
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_daduyet"]))
                        sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet;
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_tuchoi"]))
                        sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.KhongPheDuyet;
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_thoaiduyet"]))
                        sql.CommandText += " OR A.TinhTrangID = " + (int)EnumVACPA.TinhTrang.ThoaiDuyet;
                    sql.CommandText += ")";
                }
                ds = DataAccess.RunCMDGetDataSet(sql);
                dtbTat = ds.Tables[0];
            }

            if (dtbTat.Rows.Count == 0)
            {
                //dtbTat.Columns.Add("PhiID", typeof(string));
                //dtbTat.Columns.Add("LoaiPhi", typeof(string));
                //dtbTat.Columns.Add("MaPhi", typeof(string));
                //dtbTat.Columns.Add("TenPhi", typeof(string));
                //dtbTat.Columns.Add("MucPhi", typeof(string));
                //dtbTat.Columns.Add("NgayApDung", typeof(string));
                //dtbTat.Columns.Add("NgayHetHieuLuc", typeof(string));
                //dtbTat.Columns.Add("TinhTrangID", typeof(string));
                //dtbTat.Columns.Add("TenTrangThai", typeof(string));
                //dtbTat.Columns.Add("NgayNhap", typeof(string));
                //dtbTat.Columns.Add("DoiTuongNopPhi", typeof(string));
                Pager.Enabled = false;
                //return;
            }
            else
                Pager.Enabled = true;
            DataView dv = dtbTat.DefaultView;

            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
            else
                dv.Sort = "NgayNhap DESC";
            //dtb = dv.ToTable();
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                if (!Pager.Items.Contains(a))
                    Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                if (Convert.ToInt32(Request.Form["tranghientai"]) <= objPds.PageCount)
                {
                    objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                    Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                }
                else
                {
                    objPds.CurrentPageIndex = 0;
                    Pager.SelectedIndex = 0;
                }
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = quyen + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }

            danhsachphi_grv.DataSource = objPds;

            danhsachphi_grv.DataBind();
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dtb = null;


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void danhsachphi_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        danhsachphi_grv.PageIndex = e.NewPageIndex;
        danhsachphi_grv.DataBind();
    }

    protected void danhsachphi_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        load_data();
    }

    protected void KiemTraDuyet()
    {
         bool boThongBao = false;
         if (!string.IsNullOrEmpty(Request.QueryString["id"]))
         {
             string[] dsduyet = Request.QueryString["id"].Split(',');

             foreach (string id in dsduyet)
             {
                 string[] idloai = id.Split('|');
                 if (idloai.Length > 1)
                 {
                     if (idloai[1] == "1")
                     {
                         SqlCommand sql = new SqlCommand();
                         sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiHoiVien where PhiId = " + idloai[0];
                         DataSet ds = new DataSet();
                         ds = DataAccess.RunCMDGetDataSet(sql);
                         //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                         if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                         {
                         }
                         else
                         {
                             boThongBao = true;
                         }
                     }
                     else if (idloai[1] == "2")
                     {
                         SqlCommand sql = new SqlCommand();
                         sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiDangLogo where PhiId = " + idloai[0];
                         DataSet ds = new DataSet();
                         ds = DataAccess.RunCMDGetDataSet(sql);
                         //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                         if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                         {
                         }
                         else
                         {
                             boThongBao = true;
                         }
                     }
                     else if (idloai[1] == "3")
                     {
                         SqlCommand sql = new SqlCommand();
                         sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiKhac where PhiId = " + idloai[0];
                         DataSet ds = new DataSet();
                         ds = DataAccess.RunCMDGetDataSet(sql);
                         //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                         if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                         {
                         }
                         else
                         {
                             boThongBao = true;
                         }
                     }
                     else if (idloai[1] == "4")
                     {
                         SqlCommand sql = new SqlCommand();
                         sql.CommandText = "SELECT TinhTrangId, MaPhi from tblTTDMPhiCNKTTapThe where PhiId = " + idloai[0];
                         DataSet ds = new DataSet();
                         ds = DataAccess.RunCMDGetDataSet(sql);
                         //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                         if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                         {
                         }
                         else
                         {
                             boThongBao = true;
                         }
                     }
                 }
             }
         }
             string output_html = "";
         if (boThongBao)
             output_html = "true";
         else
             output_html = "false";

         System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
//             if (boThongBao)
//             {
//                 output_html += @" $('#div_danhsachphi_khongphuhop').dialog({
//                resizable: false,
//                modal: true,
//                buttons: {
//                    'Đóng lại': function () {
//                        $(this).dialog('close');
//                    }
//                }
//            });";
//             }
//             else
//             {
//                 output_html += "confirm_duyet_danhsachphi(" + Request.Form["id"] + ")";
//             }
//             System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
         
    }

    protected void btnChangePhi_Click(object sender, EventArgs e)
    {
        load_data();
    }

    protected void LoadLoaiPhi(string id = "0")
    {
        string output_html = "";
        if (id == "0")
            output_html += @"<option selected=""selected"" value='0'>Tất cả</option>";
        else
            output_html += @"<option value='0'>Tất cả</option>";
        if (id == "1")
            output_html += @"<option selected=""selected"" value='1'>Phí hội viên</option>";
        else
            output_html += @"<option value='1'>Phí hội viên</option>";
        if (id == "2")
            output_html += @"<option selected=""selected"" value='2'>Phí đăng logo</option>";
        else
            output_html += @"<option value='2'>Phí đăng logo</option>";
        if (id == "3")
            output_html += @"<option selected=""selected"" value='3'>Phí khác</option>";
        else
            output_html += @"<option value='3'>Phí khác</option>";
        if (id == "4")
            output_html += @"<option selected=""selected"" value='4'>Phí CNKT</option>";
        else
            output_html += @"<option value='4'>Phí CNKT</option>";
        if (id == "5")
            output_html += @"<option selected=""selected"" value='5'>Phí KSCL</option>";
        else
            output_html += @"<option value='5'>Phí KSCL</option>";
        Response.Write(output_html + System.Environment.NewLine);
    }
}