﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using System.IO;

public partial class usercontrols_thanhtoanphicanhan_chitiet : System.Web.UI.UserControl
{
    DataTable dtb = new DataTable();
    string quyen = "TTPhiCaNhan";
    protected string truongid = "HoiVienID";
    protected string truongstt = "STT";
    protected string truongmaphi = "MaPhi";
    protected string truongloaiphi = "TenLoaiPhi";
    protected string truongsophiphainop = "TongTien";
    protected string truongsophinop = "TienNop";
    DataSet ds = new DataSet();
    public string idHoiVienCaNhan { set; get; }
    DataTable dtbThongTinPhiHoiVien { set; get; }



    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {

                thanhtoanphicanhanchitiet_grv.Columns[0].HeaderText = "STT";
                thanhtoanphicanhanchitiet_grv.Columns[0].SortExpression = truongmaphi;
                thanhtoanphicanhanchitiet_grv.Columns[1].HeaderText = "Mã phí";
                thanhtoanphicanhanchitiet_grv.Columns[1].SortExpression = truongmaphi;
                thanhtoanphicanhanchitiet_grv.Columns[2].HeaderText = "Loại phí";
                thanhtoanphicanhanchitiet_grv.Columns[2].SortExpression = truongloaiphi;
                thanhtoanphicanhanchitiet_grv.Columns[3].HeaderText = "Số phí phải nộp";
                thanhtoanphicanhanchitiet_grv.Columns[3].SortExpression = truongsophiphainop;
                thanhtoanphicanhanchitiet_grv.Columns[4].HeaderText = "Số phí nộp";
                thanhtoanphicanhanchitiet_grv.Columns[4].SortExpression = truongsophinop;
            }
            else
            {
                //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //sb.Append(@"<script>");
                //sb.Append(@"jQuery(document).ready(function () {");
                //sb.Append(@"  fill_thongtinhoivien(" + Request.Form["HoiVienCaNhanID.ClientID"] + ");}) ");
                //sb.Append(@"</script>");
                //System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
            }
            
            //load_data();
        }
        catch (Exception ex)
        {
            //Page.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));

            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    protected void load_data()
    {
        try
        {
            //khởi tạo tạm Phí ID (lấy sau)
            int iHoiVienID = 0;
            try
            {
                string a = HoiVienCaNhanID.Value;
                iHoiVienID = Convert.ToInt32(a);
            }
            catch
            { }
            if (iHoiVienID == 0)
            {
                //DataTable dtbtemp = new DataTable();
                //dtbtemp.Columns.Add("");
                //dtbtemp.Columns.Add("MaPhi");
                //dtbtemp.Columns.Add("TenLoaiPhi");
                //dtbtemp.Columns.Add("TongTien");
                //dtbtemp.Columns.Add("TienNop");
                //thanhtoanphicanhanchitiet_grv.DataSource = dtbtemp;
                //thanhtoanphicanhanchitiet_grv.DataBind();
                return;
            }
            else
            {

                SqlCommand sql = new SqlCommand();
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiCaNhan_PhiHoiVien] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + "," + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan;
                dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];

                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKSCL] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan;
                dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiCaNhan_PhiCNKT] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + "," + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan;
                dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKhac] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + "," + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan;
                dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

                double douTongTien = 0;
                foreach (DataRow dtr in dtb.Rows)
                {
                    if (dtr["TongTien"] != null)
                        douTongTien += Convert.ToDouble(dtr["TongTien"]);
                }

                DataRow dtrThem = dtb.NewRow();
                dtrThem["STT"] = dtb.Rows.Count;
                dtrThem["PhatSinhPhiID"] = "0";
                dtrThem["HoiVienID"] = iHoiVienID;
                dtrThem["MaPhi"] = "";
                dtrThem["LoaiPhi"] = "10";
                dtrThem["TenLoaiPhi"] = "Tổng cộng";
                dtrThem["TongTien"] = douTongTien;
                dtrThem["TienNop"] = douTongTien;
                dtb.Rows.Add(dtrThem);
                DataColumn[] columns = new DataColumn[1];
                columns[0] = dtb.Columns["LoaiPhi"];
                dtb.PrimaryKey = columns;

                thanhtoanphicanhanchitiet_grv.DataSource = dtb;
                thanhtoanphicanhanchitiet_grv.DataBind();

                v_GetDuLieu();
                

                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;
                ds = null;
                dtb = null;
            }
        }
        catch (Exception ex)
        {
            //Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    protected void ReLoadDTB()
    {
        if (ViewState["dtbPhiChiTiet"] != null)
            dtb = (DataTable)ViewState["dtbPhiChiTiet"];
        else
            ViewState["dtbPhiChiTiet"] = dtb;
    }

    protected void v_GetDuLieu()
    {
        if (ViewState["dtbPhiChiTiet"] != null)
            dtb = (DataTable)ViewState["dtbPhiChiTiet"];
        else
            ViewState["dtbPhiChiTiet"] = dtb;
        Session["PCNCT_HoiVienID"] = null;
        Session["PCNCT_MaHoiVien"] = null;
        Session["PCNCT_TenHoiVien"] = null;
        Session["PCNCT_SoCCKTV"] = null;
        Session["PCNCT_NgayCapCCKTV"] = null;
        Session["PCNCT_DonViCongTac"] = null;
        Session["PCNCT_SoPhiPhaiNop"] = null;
        
        Session["PCNCT_SoPhiConNo"] = null;
        Session["PCNCT_PhiHoiVienPhaiNop"] = null;
        
        Session["PCNCT_PhiCNKTPhaiNop"] = null;
        
        Session["PCNCT_PhiKSCLPhaiNop"] = null;
        Session["PCNCT_PhiKhacPhaiNop"] = null;
        

        Session["PCNCT_PhatSinhPhiIDPhiHoiVien"] = null;
        Session["PCNCT_PhatSinhPhiIDPhiCNKT"] = null;
        Session["PCNCT_PhatSinhPhiIDPhiKSCL"] = null;
        Session["PCNCT_PhatSinhPhiIDPhiKhac"] = null;

        Session["PCNCT_PhiIDPhiHoiVien"] = null;
        Session["PCNCT_PhiIDPhiCNKT"] = null;
        Session["PCNCT_PhiIDPhiKSCL"] = null;
        Session["PCNCT_PhiIDPhiKhac"] = null;


        Session["PCNCT_HoiVienID"] = HoiVienCaNhanID.Value;
        Session["PCNCT_MaHoiVien"] = Request.Form["MaHoiVienCaNhan"];
        Session["PCNCT_TenHoiVien"] = HoVaTen.Value;
        Session["PCNCT_SoCCKTV"] = SoCCKTV.Value;
        Session["PCNCT_NgayCapCCKTV"] = NgayCap.Value;
        Session["PCNCT_DonViCongTac"] = DonViCongTac.Value;

       // v_GetDuLieuNhapGrid();
        double dPhiPhaiNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows[dtb.Rows.Count - 1]["TongTien"] != null)
                    if (!string.IsNullOrEmpty(dtb.Rows[dtb.Rows.Count - 1]["TongTien"].ToString()))
                        dPhiPhaiNop = Convert.ToDouble(dtb.Rows[dtb.Rows.Count - 1]["TongTien"]);

        }
        catch
        { }

        Session["PCNCT_SoPhiPhaiNop"] = dPhiPhaiNop.ToString();

        double dPhiDaNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows[dtb.Rows.Count - 1]["TienNop"] != null)
                    if (!string.IsNullOrEmpty(dtb.Rows[dtb.Rows.Count - 1]["TienNop"].ToString()))
                        dPhiDaNop = Convert.ToDouble(dtb.Rows[dtb.Rows.Count - 1]["TienNop"]);
        }
        catch
        { }

        Session["PCNCT_SoPhiDaNop"] = dPhiDaNop.ToString();

        double dPhiConNo = dPhiPhaiNop - dPhiDaNop;

        Session["PCNCT_SoPhiConNo"] = dPhiConNo.ToString();


        string PhiHoiVien = ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString();
        string PhiCNKT = ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString();
        string PhiKSCL = ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString();
        string PhiKhac = ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString();

        double dPhiHoiVienPhaiNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiHoiVien) != null)
                    dPhiHoiVienPhaiNop = Convert.ToDouble(dtb.Rows.Find(PhiHoiVien)["TongTien"]);
        }
        catch
        { }
        Session["PCNCT_PhiHoiVienPhaiNop"] = dPhiHoiVienPhaiNop;

        double dPhiHoiVienDaNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiHoiVien) != null)
                    dPhiHoiVienDaNop = Convert.ToDouble(dtb.Rows.Find(PhiHoiVien)["TienNop"]);
        }
        catch
        { }
        Session["PCNCT_PhiHoiVienDaNop"] = dPhiHoiVienDaNop;

        double dPhiCNKTPhaiNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiCNKT) != null)
                    dPhiCNKTPhaiNop = Convert.ToDouble(dtb.Rows.Find(PhiCNKT)["TongTien"]);
        }
        catch
        { }
        Session["PCNCT_PhiCNKTPhaiNop"] = dPhiCNKTPhaiNop;

        double dPhiCNKTDaNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiCNKT) != null)
                    dPhiCNKTDaNop = Convert.ToDouble(dtb.Rows.Find(PhiCNKT)["TienNop"]);
        }
        catch
        { }
        Session["PCNCT_PhiCNKTDaNop"] = dPhiCNKTDaNop;

        double dPhiKSCLPhaiNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiKSCL) != null)
                    dPhiKSCLPhaiNop = Convert.ToDouble(dtb.Rows.Find(PhiKSCL)["TongTien"]);
        }
        catch
        { }
        Session["PCNCT_PhiKSCLPhaiNop"] = dPhiKSCLPhaiNop;

        double dPhiKSCLDaNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiKSCL) != null)
                    dPhiKSCLDaNop = Convert.ToDouble(dtb.Rows.Find(PhiKSCL)["TienNop"]);
        }
        catch
        { }
        Session["PCNCT_PhiKSCLDaNop"] = dPhiKSCLDaNop;


        double dPhiKhacPhaiNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiKhac) != null)
                    dPhiKhacPhaiNop = Convert.ToDouble(dtb.Rows.Find(PhiKhac)["TongTien"]);
        }
        catch
        { }
        Session["PCNCT_PhiKhacPhaiNop"] = dPhiKhacPhaiNop;

        double dPhiKhacDaNop = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiKhac) != null)
                    dPhiKhacDaNop = Convert.ToDouble(dtb.Rows.Find(PhiKhac)["TienNop"]);
        }
        catch
        { }
        Session["PCNCT_PhiKhacDaNop"] = dPhiKhacDaNop;

        int iPhatSinhPhiIDHoiVien = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiHoiVien) != null)
                    iPhatSinhPhiIDHoiVien = Convert.ToInt32(dtb.Rows.Find(PhiHoiVien)["PhatSinhPhiID"]);
        }
        catch
        { }
        Session["PCNCT_PhatSinhPhiIDPhiHoiVien"] = iPhatSinhPhiIDHoiVien;

        int iPhatSinhPhiIDCNKT = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiCNKT) != null)
                    iPhatSinhPhiIDCNKT = Convert.ToInt32(dtb.Rows.Find(PhiCNKT)["PhatSinhPhiID"]);
        }
        catch
        { }
        Session["PCNCT_PhatSinhPhiIDPhiCNKT"] = iPhatSinhPhiIDCNKT;

        int iPhatSinhPhiIDKSCL = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiKSCL) != null)
                    iPhatSinhPhiIDKSCL = Convert.ToInt32(dtb.Rows.Find(PhiKSCL)["PhatSinhPhiID"]);
        }
        catch
        { }
        Session["PCNCT_PhatSinhPhiIDPhiKSCL"] = iPhatSinhPhiIDKSCL;

        int iPhatSinhPhiIDKhac = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiKhac) != null)
                    iPhatSinhPhiIDKhac = Convert.ToInt32(dtb.Rows.Find(PhiKhac)["PhatSinhPhiID"]);
        }
        catch
        { }
        Session["PCNCT_PhatSinhPhiIDPhiKhac"] = iPhatSinhPhiIDKhac;

        int iPhiIDHoiVien = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiHoiVien) != null)
                    iPhiIDHoiVien = Convert.ToInt32(dtb.Rows.Find(PhiHoiVien)["PhiID"]);
        }
        catch
        { }
        Session["PCNCT_PhiIDPhiHoiVien"] = iPhiIDHoiVien;

        int iPhiIDCNKT = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiCNKT) != null)
                    iPhiIDCNKT = Convert.ToInt32(dtb.Rows.Find(PhiCNKT)["PhiID"]);
        }
        catch
        { }
        Session["PCNCT_PhiIDPhiCNKT"] = iPhiIDCNKT;

        int iPhiIDKSCL = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiKSCL) != null)
                    iPhiIDKSCL = Convert.ToInt32(dtb.Rows.Find(PhiKSCL)["PhiID"]);
        }
        catch
        { }
        Session["PCNCT_PhiIDPhiKSCL"] = iPhiIDKSCL;

        int iPhiIDKhac = 0;
        try
        {
            if (dtb.Rows.Count > 0)
                if (dtb.Rows.Find(PhiKhac) != null)
                    iPhiIDKhac = Convert.ToInt32(dtb.Rows.Find(PhiKhac)["PhiID"]);
        }
        catch
        { }
        Session["PCNCT_PhiIDPhiKhac"] = iPhiIDKhac;
    }

    //private void v_GetDuLieuNhapGrid()
    //{
        //Session["PCNCT_SoPhiDaNop"] = null;
        //Session["PCNCT_PhiHoiVienDaNop"] = null;
        //Session["PCNCT_PhiCNKTDaNop"] = null;
        //Session["PCNCT_PhiKSCLDaNop"] = null;

        //string PhiHoiVien = ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString();
        //string PhiCNKT = ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString();
        //string PhiKSCL = ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString();

        //dtb = (DataTable)Session["dtbPhiChiTiet"];
        //double dPhiDaNop = 0;
        //try
        //{
        //    if (thanhtoanphicanhanchitiet_grv.Rows.Count > 0)
        //    {
        //        TextBox a = (TextBox)thanhtoanphicanhanchitiet_grv.Rows[thanhtoanphicanhanchitiet_grv.Rows.Count - 1].Cells[4].FindControl("TruongSoPhiNop");

        //        int leng = a.Text.Length;
        //        if (a.Text.LastIndexOf(',') != -1)
        //            leng = a.Text.LastIndexOf(',');
        //        string tien = a.Text.Substring(0, leng);
        //        tien = tien.Replace(".", string.Empty);
        //        dPhiDaNop = Convert.ToDouble(tien);
        //    }
        //}
        //catch
        //{ }

        //Session["PCNCT_SoPhiDaNop"] = dPhiDaNop.ToString();

        //double dPhiPhaiNop = 0;
        //try
        //{
        //    if (Session["PCNCT_SoPhiPhaiNop"] != null)
        //    {
        //        dPhiPhaiNop = Convert.ToDouble(Session["PCNCT_SoPhiPhaiNop"]);
        //    }
        //}
        //catch
        //{ }

        //double dPhiConNo = dPhiPhaiNop - dPhiDaNop;

        //Session["PCNCT_SoPhiConNo"] = dPhiConNo.ToString();


        
        //double dPhiHoiVienDaNop = 0;
        //try
        //{
        //    if (dtb.Rows.Count > 0)
        //        if (dtb.Rows.Find(PhiHoiVien) != null)
        //            dPhiHoiVienDaNop = Convert.ToDouble(dtb.Rows.Find(PhiHoiVien)["TienNop"]);
        //}
        //catch
        //{ }
        //Session["PCNCT_PhiHoiVienDaNop"] = dPhiHoiVienDaNop;

      
        //double dPhiCNKTDaNop = 0;
        //try
        //{
        //    if (dtb.Rows.Count > 0)
        //        if (dtb.Rows.Find(PhiCNKT) != null)
        //            dPhiCNKTDaNop = Convert.ToDouble(dtb.Rows.Find(PhiCNKT)["TienNop"]);
        //}
        //catch
        //{ }
        //Session["PCNCT_PhiCNKTDaNop"] = dPhiCNKTDaNop;

      
        //double dPhiKSCLDaNop = 0;
        //try
        //{
        //    if (dtb.Rows.Count > 0)
        //        if (dtb.Rows.Find(PhiKSCL) != null)
        //            dPhiKSCLDaNop = Convert.ToDouble(dtb.Rows.Find(PhiKSCL)["TienNop"]);
        //}
        //catch
        //{ }
        //Session["PCNCT_PhiKSCLDaNop"] = dPhiHoiVienDaNop;
    //}

    protected void thanhtoanphicanhanchitiet_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        thanhtoanphicanhanchitiet_grv.PageIndex = e.NewPageIndex;
        thanhtoanphicanhanchitiet_grv.DataBind();
    }

    protected void thanhtoanphicanhanchitiet_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        //load_data();
    }

    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        load_data();
    }

    protected void btnrefresh_OnClick(object sender, EventArgs e)
    {
       //v_GetDuLieuNhapGrid();
    }

    protected void btntong_OnClick(object sender, EventArgs e)
    {
        v_SetTongDaNop();
    }

    protected void thanhtoanphicanhanchitiet_grv_OnDataBound(object sender, EventArgs e)
    {
        int i = thanhtoanphicanhanchitiet_grv.Rows.Count;
        if (i != 0)
        {
            GridViewRow gvRow = thanhtoanphicanhanchitiet_grv.Rows[i-1];
            if (gvRow != null)
            {
                if (gvRow.Cells.Count > 4)
                {
                    gvRow.Cells[0].Text = "Tổng cộng:";
                    gvRow.Cells[1].Text = "";
                    gvRow.Cells[2].Text = "";
                    gvRow.Cells[1].Visible = false;
                    gvRow.Cells[2].Visible = false;
                    gvRow.Cells[0].ColumnSpan = 3;
                    gvRow.Cells[0].ControlStyle.Font.Bold = true;
                    gvRow.Cells[3].ControlStyle.Font.Bold = true;
                    gvRow.Cells[4].ControlStyle.Font.Bold = true;
                    TextBox a = (TextBox)gvRow.Cells[4].FindControl("TruongSoPhiNop");
                    a.ReadOnly = true;
                }
            }
        }
    }

    protected void v_SetTongDaNop()
    {
        try
        {
            int i = thanhtoanphicanhanchitiet_grv.Rows.Count;
            if (i != 0)
            {
                GridViewRow gvRow = thanhtoanphicanhanchitiet_grv.Rows[i - 1];
                if (gvRow != null)
                {
                    if (gvRow.Cells.Count > 4)
                    {
                        double douTongTien = 0;
                        for (int j = 0; j < thanhtoanphicanhanchitiet_grv.Rows.Count - 1; j++)
                        {
                            TextBox a = (TextBox)thanhtoanphicanhanchitiet_grv.Rows[j].Cells[4].FindControl("TruongSoPhiNop");
                            if (a != null)
                            {
                                int leng = a.Text.Length;
                                if (a.Text.LastIndexOf(',') != -1)
                                    leng = a.Text.LastIndexOf(',');
                                string tien = a.Text.Substring(0, leng);
                                tien = tien.Replace(".", string.Empty);
                                douTongTien += Convert.ToDouble(tien);
                                if (((Label)thanhtoanphicanhanchitiet_grv.Rows[j].Cells[5].FindControl("LoaiPhi")).Text == ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString())
                                    Session["PCNCT_PhiHoiVienDaNop"] = tien;
                                else if (((Label)thanhtoanphicanhanchitiet_grv.Rows[j].Cells[5].FindControl("LoaiPhi")).Text == ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString())
                                    Session["PCNCT_PhiKSCLDaNop"] = tien;
                                else if (((Label)thanhtoanphicanhanchitiet_grv.Rows[j].Cells[5].FindControl("LoaiPhi")).Text == ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString())
                                    Session["PCNCT_PhiCNKTDaNop"] = tien;
                                else if (((Label)thanhtoanphicanhanchitiet_grv.Rows[j].Cells[5].FindControl("LoaiPhi")).Text == ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString())
                                    Session["PCNCT_PhiKhacDaNop"] = tien;
                            }
                        }
                        gvRow.Cells[0].Text = "Tổng cộng:";
                        gvRow.Cells[4].Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N2}", douTongTien);
                        Label b = (Label)gvRow.Cells[3].FindControl("TruongSoPhiPhaiNop");
                        double douTongTienNo = 0;
                        int leng1 = b.Text.Length;
                        if (b.Text.LastIndexOf(',') != -1)
                            leng1 = b.Text.LastIndexOf(',');
                        string tien1 = b.Text.Substring(0, leng1);
                        tien1 = tien1.Replace(".", string.Empty);
                        douTongTienNo = Convert.ToDouble(tien1) - douTongTien;
                        Session["PCNCT_SoPhiConNo"] = douTongTienNo;
                        Session["PCNCT_SoPhiDaNop"] = douTongTien;
                    }
                }
            }
        }
        catch
        { }
    }
}