﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;
using System.Configuration;


public partial class usercontrols_thanhtoanphitapthe : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    public DataTable dtbPhiTapTheChiTiet;
    public DataTable dtbPhiTapThe;

    public DataSet ds = new DataSet();
    static string connStr = ConfigurationManager.ConnectionStrings["VACPA"].ConnectionString;
    SqlConnection conn = new SqlConnection(connStr);
    //double douTongTienNop;
    //double douTongTienNo;

    public string quyen = "ThanhToanPhiTapThe";

    public string tenchucnang = "Thanh toán phí tổ chức";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";
    public int iTinhTrang = 0;

    public string truongphiid = "PhiID";
    public string truonghoten = "HoTen";
    public string truongsoccktv = "SoChungChiKTV";
    public string truongdonvicongtac = "DonViCongTac";
    public string truongngaycapchungchi = "NgayCapChungChiKTV";
    //public string truongsophiphainop = "";
    //public string truongsophidanop = "";
    public string truongsophiconno = "SoPhiConNo";

    protected string truonghoivienid = "HoiVienID";
    protected string truongmahoivien = "MaHoiVien";
    protected string truongstt = "STT";
    protected string truongmaphi = "MaPhi";
    protected string truongloaiphi = "TenLoaiPhi";
    protected string truongsophiphainop = "TongTien";
    protected string truongsophidanop = "TienNop";


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            if (!Page.IsPostBack)
            {

                //grvPhiCongTy.Columns[1].HeaderText = "STT";
                //grvPhiCongTy.Columns[1].SortExpression = truongstt;
                grvPhiCongTy.Columns[1].HeaderText = "Loại phí";
                grvPhiCongTy.Columns[1].SortExpression = truongloaiphi;
                grvPhiCongTy.Columns[2].HeaderText = "Số phí phải nộp";
                grvPhiCongTy.Columns[2].SortExpression = truongsophiphainop;
                grvPhiCongTy.Columns[3].HeaderText = "Số phí đã nộp/thanh toán";
                grvPhiCongTy.Columns[3].SortExpression = truongsophidanop;
                grvPhiCongTy.Columns[4].HeaderText = "Số phí còn nợ";
                grvPhiCongTy.Columns[4].SortExpression = truongsophiconno;
                //PanelCaNhan.Visible = false;
                v_AnHienNut();
               // ReLoadDTB();
                //if (dtbPhiTapThe == null)
                //{
                //    TaoDataTableThanhToanPhiTapThe();
                //}
               // RefeshGrvPhiTapThe();
                if (string.IsNullOrEmpty(Request.QueryString["mode"]))
                {
                    LoadLoaiPhi();
                }
                {
                    LoadView();
                }
            }
            else
            {
               // RefeshGrvPhiTapThe();
            }

            //Thêm mới
            //Khi thêm mới thì không có request id,  mã phí          

            
            if (string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                if (!string.IsNullOrEmpty(Request.Form["hsubmit"]))
                {
                    if (Request.Form["hsubmit"] == "1")
                    {
                       
                    }
                }
            }
            else
            {
                if (Request.QueryString["mode"] == "edit") //Sửa
                {
//                    if (!string.IsNullOrEmpty(Request.Form["TenPhi"]))
//                    {
//                        string strTenPhi = Request.Form["TenPhi"];
//                        string strMucPhi = Request.Form["MucPhi"];

//                        var modified = new StringBuilder();
//                        foreach (char c in strMucPhi)
//                        {
//                            if (Char.IsDigit(c) || c == ',')
//                                modified.Append(c);
//                        }

//                        decimal MucPhi = decimal.Parse(modified.ToString());
//                        DateTime? NgayLap1 = null;
//                        if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
//                            NgayLap1 = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
//                        DateTime? NgayApDung1 = null;
//                        if (!string.IsNullOrEmpty(Request.Form["NgayApDung"]))
//                            NgayApDung1 = DateTime.ParseExact(Request.Form["NgayApDung"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

//                        DateTime? NgayHetHieuLuc1 = null;
//                        if (!string.IsNullOrEmpty(Request.Form["NgayHetHieuLuc"]))
//                            NgayHetHieuLuc1 = DateTime.ParseExact(Request.Form["NgayHetHieuLuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

//                        DateTime NgayNhap = DateTime.Now;

//                        int? ThangNop = null;
//                        if (!string.IsNullOrEmpty(Request.Form["ThangNop"]))
//                            ThangNop = Convert.ToInt32(Request.Form["ThangNop"]);

//                        SqlCommand sql = new SqlCommand();

//                        sql.CommandText = @"UPDATE tblTTDMPhiHoiVien set NgayLap =@NgayLap, TenPhi = @TenPhi,
//                        NgayApDung = @NgayApDung, NgayHetHieuLuc = @NgayHetHieuLuc, DoiTuongApDungID = @DoiTuongApDung,
//                        MucPhi = @MucPhi, DonViThoiGian = @DonViThoiGian, ThoiHanThang = @ThoiHanThang,
//                        ThoiHanNgay =@ThoiHanNgay, TinhTrangID = @TinhTrangID, NgayNhap = @NgayNhap, 
//                        NguoiNhap =@NguoiNhap
//                        WHERE PhiID = " + Request.QueryString["id"];
//                        if (NgayLap1 != null)
//                            sql.Parameters.AddWithValue("@NgayLap", NgayLap1);
//                        else
//                            sql.Parameters.AddWithValue("@NgayLap", DBNull.Value);
//                        sql.Parameters.AddWithValue("@TenPhi", Request.Form["TenPhi"]);
//                        if (NgayApDung1 != null)
//                            sql.Parameters.AddWithValue("@NgayApDung", NgayApDung1);
//                        else
//                            sql.Parameters.AddWithValue("@NgayApDung", DBNull.Value);
//                        if (NgayHetHieuLuc1 != null)
//                            sql.Parameters.AddWithValue("@NgayHetHieuLuc", NgayHetHieuLuc1);
//                        else
//                            sql.Parameters.AddWithValue("@NgayHetHieuLuc", DBNull.Value);
//                        sql.Parameters.AddWithValue("@DoiTuongApDung ", Request.Form["DoiTuongApDung"]);
//                        sql.Parameters.AddWithValue("@MucPhi", MucPhi);
//                        sql.Parameters.AddWithValue("@DonViThoiGian", Request.Form["DonViThoiGian"]);
//                        if (ThangNop != null)
//                            sql.Parameters.AddWithValue("@ThoiHanThang", ThangNop);
//                        else
//                            sql.Parameters.AddWithValue("@ThoiHanThang", DBNull.Value);
//                        sql.Parameters.AddWithValue("@ThoiHanNgay", Request.Form["NgayNop"]);
//                        sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
//                        sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
//                        sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);
//                        DataAccess.RunActionCmd(sql);
//                        sql.Connection.Close();
//                        sql.Connection.Dispose();
//                        sql = null;

//                        cm.ghilog("TTDMPhiHoiVien", "Sửa giá trị \"" + Request.Form["MaPhi"] + "\" vào danh mục TTDMPhiHoiVien");

//                        Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                   // }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["tinhtrang"]))//Các hành động từ chối, duyệt, thoái duyệt, xóa
                {
                    //Từ chối
                    if (Request.QueryString["tinhtrang"] == "tuchoi")
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangID from tblTTNopPhiTapThe where GiaoDichId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                            sql.CommandText = @"UPDATE tblTTNopPhiTapThe 
                            set TinhTrangId = @TinhTrang, 
                            NguoiDuyet = @NguoiDuyet, 
                            NgayDuyet = @NgayDuyet 
                            WHERE GiaoDichId = " + Request.QueryString["id"];
                            sql.Parameters.AddWithValue("@TinhTrang", EnumVACPA.TinhTrang.KhongPheDuyet);
                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value); 
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiTapThe", "Từ chối duyệt giá trị \"" + Request.Form["MaGiaoDich"] + "\" của danh mục TTNopPhiTapThe");
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "xoa") //Xóa
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangID from tblTTNopPhiTapThe where GiaoDichId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải khác ở trạng thái duyệt thì mới cho xóa
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {
                            sql.CommandText = "delete tblTTNopPhiTapThe WHERE GiaoDichId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.CommandText = "delete tblTTNopPhiTapTheChiTiet WHERE GiaoDichId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.CommandText = "delete tblTTNopPhiCaNhanChiTiet WHERE DoiTuongNopPhi = 1 AND GiaoDichId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiTapThe", "Xóa giá trị \"" + Request.Form["MaGiaoDich"] + "\" của danh mục TTNopPhiTapThe");
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "duyet") //duyệt
                    {

                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangID from tblTTNopPhiTapThe where GiaoDichId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                            sql.CommandText = @"UPDATE tblTTNopPhiTapThe 
                            set TinhTrangId = @TinhTrang, 
                            NguoiDuyet = @NguoiDuyet, 
                            NgayDuyet = @NgayDuyet 
                            WHERE GiaoDichId = " + Request.QueryString["id"];

                            sql.Parameters.AddWithValue("@TinhTrang", EnumVACPA.TinhTrang.DaPheDuyet);
                            sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                            sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiTapThe", "Duyệt giá trị \"" + Request.Form["MaGiaoDich"] + "\" của danh mục TTNopPhiTapThe");
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "thoaiduyet") //Thoái duyệt
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangID from tblTTNopPhiTapThe where GiaoDichId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái duyệt thì mới cho thoái duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {
                            sql.CommandText = @"UPDATE tblTTNopPhiTapThe 
                            set TinhTrangId = @TinhTrang, 
                            NguoiDuyet = @NguoiDuyet, 
                            NgayDuyet = @NgayDuyet 
                            WHERE GiaoDichId = " + Request.QueryString["id"];

                            sql.Parameters.AddWithValue("@TinhTrang", EnumVACPA.TinhTrang.ThoaiDuyet);
                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);

                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiTapThe", "Thoái duyệt giá trị \"" + Request.Form["MaGiaoDich"] + "\" vào danh mục TTNopPhiTapThe");
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    private void v_AnHienNut()
    {
        if (string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            btnluu.Visible = true;
            btnsua.Visible = false;
            btnduyet.Visible = false;
            btnthoaiduyet.Visible = false;
            btntuchoi.Visible = false;
        }
        else
        {
            //Lay tinh trang
            int iTinhTrang = 0;
            if (iTinhTrang == (int)EnumVACPA.TinhTrang.ChoDuyet)
            {
                btnluu.Visible = false;
                btnsua.Visible = true;
                btnduyet.Visible = true;
                btnthoaiduyet.Visible = false;
                btntuchoi.Visible = true;
            }
            else if (iTinhTrang == (int)EnumVACPA.TinhTrang.KhongPheDuyet)
            {
                btnluu.Visible = false;
                btnsua.Visible = true;
                btnduyet.Visible = false;
                btnthoaiduyet.Visible = false;
                btntuchoi.Visible = false;
            }
            else if (iTinhTrang == (int)EnumVACPA.TinhTrang.DaPheDuyet)
            {
                btnluu.Visible = false;
                btnsua.Visible = false;
                btnduyet.Visible = false;
                btnthoaiduyet.Visible = true;
                btntuchoi.Visible = false;
            }
            else if (iTinhTrang == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
            {
                btnluu.Visible = false;
                btnsua.Visible = true;
                btnduyet.Visible = false;
                btnthoaiduyet.Visible = false;
                btntuchoi.Visible = false;
            }
        }
    }

    public void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_luu' class='btn btn-rounded ' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
             <a id='btn_sua' href='#none' class='btn btn-rounded' onclick='sua();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_xoa' href='#none' class='btn btn-rounded' onclick='xoa(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-sign'></i> Xóa</a>
            <a id='btn_duyet' href='#none' class='btn btn-rounded' onclick='duyet(" + Request.QueryString["id"] + @");'><i class='iconfa-ok'></i> Duyệt</a>
            <a id='btn_tuchoi' href='#none' class='btn btn-rounded' onclick='tuchoi(" + Request.QueryString["id"] + @");'><i class='iconfa-remove'></i> Từ chối</a>
            <a id='btn_thoaiduyet' href='#none' class='btn btn-rounded' onclick='thoaiduyet(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Thoái duyệt</a>
            <a id='btn_guiemail' href='#none' class='btn btn-rounded' onclick='guiemail(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Gửi email</a>";
        output_html += "</div>";
        //<a id='btn_ketxuat' href='#none' class='btn btn-rounded' onclick='ketxuat(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Kết xuất</a>
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void AnNut()
    {
        Commons cm = new Commons();

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiTapThe", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiTapThe", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiTapThe", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiTapThe", cm.connstr).Contains("DUYET|"))
        {
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiTapThe", cm.connstr).Contains("TUCHOI|"))
        {
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiTapThe", cm.connstr).Contains("THOAIDUYET|"))
        {
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiTapThe", cm.connstr).Contains("GUIEMAIL|"))
        {
            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiTapThe", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();" + System.Environment.NewLine);
        }
    }

    protected void LoadThongTin()
    {
        //Xem hoặc sửa
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
            if (Request.QueryString["mode"] == "view" || Request.QueryString["mode"] == "edit") //Xem hoặc sửa
            {
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select GiaoDichId, MaGiaoDich, A.HoiVienTapTheId, MaHoiVienTapThe, TenDoanhNghiep, "
                    + " A.TinhTrangID, TenTrangThai "
                    + " from tblTTNopPhiTapThe A left join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiID " 
                    + " left join tblHoiVienTapThe C on A.HoiVienTapTheId = C.HoiVienTapTheID "
                + " where GiaoDichId = " + Convert.ToInt16(Request.QueryString["id"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                dt = ds.Tables[0];
                int iTinhTrangBanGhi = 0;
                string strTinhTrangBanGhi = "";
                DateTime? NgayTraCuu = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                if (dt.Rows.Count > 0)
                {
                    Response.Write("$('#MaGiaoDich').val('" + dt.Rows[0]["MaGiaoDich"] + "');" + System.Environment.NewLine);
                    HoiVienTapTheID.Value = dt.Rows[0]["HoiVienTapTheId"].ToString();
                    //MaHoiVienTapThe.Text = dt.Rows[0]["MaHoiVienTapThe"].ToString().TrimEnd();
                    Response.Write("$('#MaHoiVienTapThe').val('" + dt.Rows[0]["MaHoiVienTapThe"].ToString().TrimEnd() + "');" + System.Environment.NewLine);
                    TenCongTy.Text = dt.Rows[0]["TenDoanhNghiep"].ToString();
                    iTinhTrangBanGhi = Convert.ToInt32(dt.Rows[0]["TinhTrangID"]);
                    strTinhTrangBanGhi = dt.Rows[0]["TenTrangThai"].ToString();
                }
                string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
});";
                Response.Write(a + System.Environment.NewLine);

                if (Request.QueryString["mode"] == "view")
                {
                    Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_them').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_xoa_chitiet').remove();" + System.Environment.NewLine);
                    
                    Response.Write("$('#form_thanhtoanphitapthe input,select,textarea').attr('disabled', true);" + System.Environment.NewLine);
                    Response.Write("$('#A1').attr('disabled', true);" + System.Environment.NewLine);
                    Response.Write("$('#btnXoaGrid').attr('disabled', true);" + System.Environment.NewLine);
                    if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ChoDuyet)
                    {
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                    }
                }

                    //Nếu đang ở chế độ sửa
                else if (Request.QueryString["mode"] == "edit")
                {
                    //Nếu tình trạng bản ghi khác với chờ duyệt => không được sửa, hiển thị theo tình trạng
                    if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.ChoDuyet)
                    {
                        Response.Write("$('#form_thanhtoanphitapthe input,select,textarea').attr('disabled', true);");
                       // $('a.current-page').click(function() { return false; });
                        Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);

                        //Nếu tình trạng là đã phê duyệt thì sẽ không còn các nút Duyệt/Từ chối/Xóa
                        if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {

                            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        }
                        //Nếu tình trạng là không phê duyệt hoặc đã thoái duyệt thì sẽ không còn các nút Duyệt/Từ chối/Thoái duyệt
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                        {
                            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                        }
                    }
                    //Nếu tình trạng bản ghi là chờ duyệt => có thể sửa. Chỉ hiện nút Lưu, ẩn các nút Duyệt/Từ chối/Thoái Duyệt/Xóa
                    else
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                    }
                }
            }
            //Nếu không phải 2 chế độ xem hoặc sửa
            else
            {
                Response.Write("$('#form_thanhtoanphitapthe input,select,textarea, a').attr('disabled', true);");
                Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
            }
        }
        //Khi hiển thị các thông tin thêm mới
        else
        {
            string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: Chờ duyệt </i>')
});";
            Response.Write(a + System.Environment.NewLine);

            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
        }
    }

    protected void load_data()
    {
        try
        {
            RefeshGrvPhiTapThe(drNamThanhToan.SelectedValue);

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    //MinhLP
    //20150205
    //Khởi tạo dtbPhiTapThe
    protected void TaoDataTableThanhToanPhiTapThe()
    {
        dtbPhiTapThe = new DataTable();
        dtbPhiTapThe.Columns.Add(truongloaiphi);
        dtbPhiTapThe.Columns.Add("LoaiPhi");
        dtbPhiTapThe.Columns.Add("PhiID");
        dtbPhiTapThe.Columns.Add("PhatSinhPhiID");
        dtbPhiTapThe.Columns.Add(truongsophiphainop, typeof (double));
        dtbPhiTapThe.Columns.Add(truongsophidanop, typeof (double));
        dtbPhiTapThe.Columns.Add(truongsophiconno, typeof (double));

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dtbPhiTapThe.Columns[truonghoivienid];
        dtbPhiTapThe.PrimaryKey = columns;
    }

    //MinhLP
    //20150202
    //Khởi tạo dtbPhiTapTheChiTiet
    //protected void TaoDataTableThanhToanPhiTapTheChiTiet()
    //{
    //    dtbPhiTapTheChiTiet = new DataTable();
    //    dtbPhiTapTheChiTiet.Columns.Add(truongstt);
    //    dtbPhiTapTheChiTiet.Columns.Add(truongmaphi);
    //    dtbPhiTapTheChiTiet.Columns.Add(truongloaiphi);
    //    dtbPhiTapTheChiTiet.Columns.Add(truongsophiphainop);
    //    dtbPhiTapTheChiTiet.Columns.Add(truongsophidanop);

    //    DataColumn[] columns = new DataColumn[1];
    //    columns[0] = dtbPhiTapTheChiTiet.Columns[truongmaphi];
    //    dtbPhiTapTheChiTiet.PrimaryKey = columns;
    //}

    //MinhLP
    //20150202
    //Refesh giá trị gridTapThe
    protected void RefeshGrvPhiTapThe(string Nam)
    {
        dtbPhiTapThe = new DataTable();
        dtbPhiTapThe.Columns.Add("PhiID", typeof(int));
        dtbPhiTapThe.Columns.Add("PhatSinhPhiID", typeof(int));
        dtbPhiTapThe.Columns.Add("HoiVienID", typeof(int));
        dtbPhiTapThe.Columns.Add("MaPhi", typeof(string));
        dtbPhiTapThe.Columns.Add("LoaiPhi", typeof(string));
        dtbPhiTapThe.Columns.Add("TenLoaiPhi", typeof(string));
        dtbPhiTapThe.Columns.Add("TongTien", typeof(decimal));
        dtbPhiTapThe.Columns.Add("TienNop", typeof(decimal));
        dtbPhiTapThe.Columns.Add("TienNo", typeof(decimal));
        dtbPhiTapThe.Columns.Add("Active", typeof(bool));
        int iHoiVienID = 0;
        string LoaiHoiVienTapThe = "";
        SqlCommand sql = new SqlCommand();
        try
        {
            //string a = HoiVienTapTheID.Value;


            string a = MaHoiVienTapThe.Text;
            if (!string.IsNullOrWhiteSpace(a))
            {
                sql.CommandText = "SELECT TOP 1 HoiVienTapTheId, TenDoanhNghiep, MaHoiVienTapThe, LoaiHoiVienTapThe FROM tblHoiVienTapThe WHERE MaHoiVienTapThe like N'%" + a + "%'";
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtbTemp.Rows.Count > 0)
                {
                    HoiVienTapTheID.Value = dtbTemp.Rows[0]["HoiVienTapTheId"].ToString();
                    MaHoiVienTapThe.Text = dtbTemp.Rows[0]["MaHoiVienTapThe"].ToString();
                    TenCongTy.Text = dtbTemp.Rows[0]["TenDoanhNghiep"].ToString();
                    iHoiVienID = Convert.ToInt32(HoiVienTapTheID.Value);
                    LoaiHoiVienTapThe = dtbTemp.Rows[0]["LoaiHoiVienTapThe"].ToString();
                }
            }

        }
        catch
        { }
        if (iHoiVienID == 0)
        {
            //DataTable dtbtemp = new DataTable();
            //dtbtemp.Columns.Add("");
            //dtbtemp.Columns.Add("MaPhi");
            //dtbtemp.Columns.Add("TenLoaiPhi");
            //dtbtemp.Columns.Add("TongTien");
            //dtbtemp.Columns.Add("TienNop");
            //thanhtoanphicanhanchitiet_grv.DataSource = dtbtemp;
            //thanhtoanphicanhanchitiet_grv.DataBind();
            return;
        }
        else
        {
            //Nếu loại = 0: CTKT chưa có ID: Không có phí hội viên tổ chức => CTKT (LoaiHoiVien = 3)
            //Nếu loại = 1: Có tất => HVTC (LoaiHoiVien = 4)
            //Nếu loại = 2: CTKT có ID: Không có phí hội viên tổ chức => CTKT (LoaiHoiVien = 3)
            //Nếu loại khác 1,2,3 => return

            if (LoaiHoiVienTapThe != "0" && LoaiHoiVienTapThe != "1" && LoaiHoiVienTapThe != "2")
                return;
            if (LoaiHoiVienTapThe == "1")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiHoiVien] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + ",'" + Nam + "'";
                dtbPhiTapThe = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            }

            if (LoaiHoiVienTapThe == "1")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiKSCL] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }
            else if (LoaiHoiVienTapThe == "0" || LoaiHoiVienTapThe == "2")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiKSCL] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + "," + (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }

            if (LoaiHoiVienTapThe == "1")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiDangLogo] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiDangLogo + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }
            else if (LoaiHoiVienTapThe == "0" || LoaiHoiVienTapThe == "2")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiDangLogo] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiDangLogo + "," + (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }

            if (LoaiHoiVienTapThe == "1")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiKhac] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + "," + (int)EnumVACPA.LoaiHoiVien.HoiVienTapThe + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }
            else if (LoaiHoiVienTapThe == "0" || LoaiHoiVienTapThe == "2")
            {
                sql.CommandText = "exec [dbo].[proc_ThanhToanPhiTapThe_PhiKhac] " + iHoiVienID + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + "," + (int)EnumVACPA.LoaiHoiVien.CongTyKiemToan + ",'" + Nam + "'";
                dtbPhiTapThe.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            }

            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiHoiVien_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            DataTable dtbTempHoiVien = DataAccess.RunCMDGetDataSet(sql).Tables[0];

            Double douTongPhiHoiVienCaNhan = 0;
            Double douTongPhiHoiVienCaNhan_TienNop = 0;
            Double douTongPhiHoiVienCaNhan_ConNo = 0;

            if (dtbTempHoiVien.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTempHoiVien.Rows)
                {
                    douTongPhiHoiVienCaNhan += Convert.ToDouble(dtr["TongTien"]);
                    douTongPhiHoiVienCaNhan_TienNop += Convert.ToDouble(dtr["TienNop"]);
                    douTongPhiHoiVienCaNhan_ConNo += Convert.ToDouble(dtr["SoPhiConNo"]);
                }

                DataRow dtrThem1 = dtbPhiTapThe.NewRow();
                dtrThem1["PhiID"] = 0;
                dtrThem1["PhatSinhPhiID"] = 0;
                dtrThem1["HoiVienID"] = iHoiVienID;
                dtrThem1["MaPhi"] = "";
                dtrThem1["LoaiPhi"] = "11";
                dtrThem1["TenLoaiPhi"] = "Phí HVCN";
                dtrThem1["TongTien"] = douTongPhiHoiVienCaNhan;
                dtrThem1["TienNop"] = douTongPhiHoiVienCaNhan_TienNop;
                dtrThem1["TienNo"] = douTongPhiHoiVienCaNhan_ConNo;
                dtrThem1["Active"] = (douTongPhiHoiVienCaNhan_ConNo == 0) ? false : true;
                dtbPhiTapThe.Rows.Add(dtrThem1);
            }

            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKSCL_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            DataTable dtbTempKSCL = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKSCL_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTempKSCL.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

            Double douTongPhiKSCLCaNhan = 0;
            Double douTongPhiKSCLCaNhan_TienNop = 0;
            Double douTongPhiKSCLCaNhan_ConNo = 0;

            if (dtbTempKSCL.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTempKSCL.Rows)
                {
                    douTongPhiKSCLCaNhan += Convert.ToDouble(dtr["TongTien"]);
                    douTongPhiKSCLCaNhan_TienNop += Convert.ToDouble(dtr["TienNop"]);
                    douTongPhiKSCLCaNhan_ConNo += Convert.ToDouble(dtr["SoPhiConNo"]);
                }
                DataRow dtrThem2 = dtbPhiTapThe.NewRow();
                dtrThem2["PhiID"] = 0;
                dtrThem2["PhatSinhPhiID"] = 0;
                dtrThem2["HoiVienID"] = iHoiVienID;
                dtrThem2["MaPhi"] = "";
                dtrThem2["LoaiPhi"] = "51";
                dtrThem2["TenLoaiPhi"] = "Phí KSCL theo cá nhân";
                dtrThem2["TongTien"] = douTongPhiKSCLCaNhan;
                dtrThem2["TienNop"] = douTongPhiKSCLCaNhan_TienNop;
                dtrThem2["TienNo"] = douTongPhiKSCLCaNhan_ConNo;
                dtrThem2["Active"] = (douTongPhiKSCLCaNhan_ConNo == 0) ? false : true;
                dtbPhiTapThe.Rows.Add(dtrThem2);
            }


            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            DataTable dtbTempCNKT = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTempCNKT.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTempCNKT.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

            Double douTongPhiCNKTCaNhan = 0;
            Double douTongPhiCNKTCaNhan_TienNop = 0;
            Double douTongPhiCNKTCaNhan_ConNo = 0;

            if (dtbTempCNKT.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTempCNKT.Rows)
                {
                    douTongPhiCNKTCaNhan += Convert.ToDouble(dtr["TongTien"]);
                    douTongPhiCNKTCaNhan_TienNop += Convert.ToDouble(dtr["TienNop"]);
                    douTongPhiCNKTCaNhan_ConNo += Convert.ToDouble(dtr["SoPhiConNo"]);
                }

                DataRow dtrThem3 = dtbPhiTapThe.NewRow();
                dtrThem3["PhiID"] = 0;
                dtrThem3["PhatSinhPhiID"] = 0;
                dtrThem3["HoiVienID"] = iHoiVienID;
                dtrThem3["MaPhi"] = "";
                dtrThem3["LoaiPhi"] = "41";
                dtrThem3["TenLoaiPhi"] = "Phí CNKT";
                dtrThem3["TongTien"] = douTongPhiCNKTCaNhan;
                dtrThem3["TienNop"] = douTongPhiCNKTCaNhan_TienNop;
                dtrThem3["TienNo"] = douTongPhiCNKTCaNhan_ConNo;
                dtrThem3["Active"] = (douTongPhiCNKTCaNhan_ConNo == 0) ? false : true;
                dtbPhiTapThe.Rows.Add(dtrThem3);
            }

            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            DataTable dtbTempKhac = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTempKhac.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);

            Double douTongPhiKhacCaNhan = 0;
            Double douTongPhiKhacCaNhan_TienNop = 0;
            Double douTongPhiKhacCaNhan_ConNo = 0;

            if (dtbTempKhac.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTempKhac.Rows)
                {
                    douTongPhiKhacCaNhan += Convert.ToDouble(dtr["TongTien"]);
                    douTongPhiKhacCaNhan_TienNop += Convert.ToDouble(dtr["TienNop"]);
                    douTongPhiKhacCaNhan_ConNo += Convert.ToDouble(dtr["SoPhiConNo"]);
                }
                DataRow dtrThem4 = dtbPhiTapThe.NewRow();
                dtrThem4["PhiID"] = 0;
                dtrThem4["PhatSinhPhiID"] = 0;
                dtrThem4["HoiVienID"] = iHoiVienID;
                dtrThem4["MaPhi"] = "";
                dtrThem4["LoaiPhi"] = "31";
                dtrThem4["TenLoaiPhi"] = "Phí khác theo cá nhân";
                dtrThem4["TongTien"] = douTongPhiKhacCaNhan;
                dtrThem4["TienNop"] = douTongPhiKhacCaNhan_TienNop;
                dtrThem4["TienNo"] = douTongPhiKhacCaNhan_ConNo;
                dtrThem4["Active"] = (douTongPhiKhacCaNhan_ConNo == 0) ? false : true;
                dtbPhiTapThe.Rows.Add(dtrThem4);
            }

            double douTongTien = 0;
            double douTongTien_TienNop = 0;
            double douTongTien_ConNo = 0;
            foreach (DataRow dtr in dtbPhiTapThe.Rows)
            {
                if (dtr["TongTien"] != null)
                    douTongTien += Convert.ToDouble(dtr["TongTien"]);
                if (dtr["TienNop"] != null)
                    douTongTien_TienNop += Convert.ToDouble(dtr["TienNop"]);
                if (dtr["TienNo"] != null)
                    douTongTien_ConNo += Convert.ToDouble(dtr["TienNo"]);
            }

            DataRow dtrThem = dtbPhiTapThe.NewRow();
            dtrThem["PhiID"] = 0;
            dtrThem["PhatSinhPhiID"] = 0;
            dtrThem["HoiVienID"] = iHoiVienID;
            dtrThem["MaPhi"] = "";
            dtrThem["LoaiPhi"] = "10";
            dtrThem["TenLoaiPhi"] = "Tổng cộng";
            dtrThem["TongTien"] = douTongTien;
            dtrThem["TienNop"] = douTongTien_TienNop;
            dtrThem["TienNo"] = douTongTien_ConNo;
            dtrThem["Active"] = (douTongTien_ConNo == 0) ? false : true;
            dtbPhiTapThe.Rows.Add(dtrThem);
            //DataColumn[] columns = new DataColumn[1];
            //columns[0] = dtbPhiTapThe.Columns["LoaiPhi"];
            //dtbPhiTapThe.PrimaryKey = columns;
        }
        grvPhiCongTy.DataSource = dtbPhiTapThe;
        grvPhiCongTy.DataBind();
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;
        ds = null;
        dtbPhiTapThe = null;

        LoadGridCaNhan(Nam);
    }

    protected void ReLoadDTB()
    {
        if (ViewState["dtbPhiTapThe"] != null)
            dtbPhiTapThe = (DataTable)ViewState["dtbPhiTapThe"];
        else
            ViewState["dtbPhiTapThe"] = dtbPhiTapThe;
    }

    //MinhLP
    //20150202
    //Refesh giá trị gridTapThe
    protected void RefeshGrvPhiTapTheChiTiet()
    {
        //if (ViewState["dtbDanhSachVLVPP"] != null)
        //    dtbDanhSachVLVPP = (DataTable)ViewState["dtbDanhSachVLVPP"];
        //else
        //    ViewState["dtbDanhSachVLVPP"] = dtbDanhSachVLVPP;
        //gvDanhSachVLVPP.DataSource = dtbDanhSachVLVPP;
        //gvDanhSachVLVPP.DataBind();
    }

    protected void MaHoiVienTapThe_OnTextChanged(object sender, EventArgs e)
    {
        string sql = "SELECT DISTINCT YEAR(NgayPhatSinh) AS Nam ";
        sql += " FROM tblTTPhatSinhPhi";

        //sql += " WHERE LoaiHoiVien = '" + Session["LoaiHoiVien"].ToString() + "' AND HoiVienID = " + ID;
        sql += " ORDER BY Nam DESC";
        DataSet ds = new DataSet();
        DataTable dtb = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        da.Fill(ds);
        dtb = ds.Tables[0];

        drNamThanhToan.DataSource = dtb;
        drNamThanhToan.DataBind();

        if (Session["NamThanhToan"] != null)
        {
            RefeshGrvPhiTapThe(Session["NamThanhToan"].ToString());
            drNamThanhToan.SelectedValue = Session["NamThanhToan"].ToString();
        }
        else if (!string.IsNullOrEmpty(drNamThanhToan.SelectedValue))
        {
            Session["NamThanhToan"] = drNamThanhToan.SelectedValue;
            RefeshGrvPhiTapThe(drNamThanhToan.SelectedValue);
        }
    }

    private string _dtbPhiTapThe = "dtbPhiTapThe";

    protected void grvPhiCongTy_OnDataBound(object sender, EventArgs e)
    {
        double douTongTienNop = 0;
        double douTongTienNo = 0;

        for (int itemp = 0; itemp < grvPhiCongTy.Rows.Count - 1; itemp++ )
        {
            GridViewRow row = grvPhiCongTy.Rows[itemp];
            Label lblTienNop = (Label)row.FindControl("TienNop");
            douTongTienNop += Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
            Label lblTienNo = (Label)row.FindControl("TienNo");
            douTongTienNo += Convert.ToDouble(strBoChamPhay(lblTienNo.Text));
        }

        int i = grvPhiCongTy.Rows.Count;
        if (i != 0)
        {
            GridViewRow gvRow = grvPhiCongTy.Rows[i - 1];
            if (gvRow != null)
            {
                if (gvRow.Cells.Count > 5)
                {

                    try
                    {
                        CheckBox cbx = (CheckBox)gvRow.Cells[0].FindControl("chkEmp");
                        cbx.Visible = false;
                    }
                    catch
                    { }

                    Label lblTenLoaiPhi = (Label)gvRow.Cells[1].FindControl("TenLoaiPhi");
                    lblTenLoaiPhi.Text = "Tổng";

                    Label lblTongTienNop = (Label)gvRow.Cells[3].FindControl("TienNop");
                    Label lblTongTienNo = (Label)gvRow.Cells[4].FindControl("TienNo");



                    lblTongTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTongTienNop);
                    lblTongTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTongTienNo);

                    gvRow.Cells[1].ControlStyle.Font.Bold = true;
                    gvRow.Cells[2].ControlStyle.Font.Bold = true;
                    gvRow.Cells[3].ControlStyle.Font.Bold = true;
                    gvRow.Cells[4].ControlStyle.Font.Bold = true;
                    //TextBox a = (TextBox)gvRow.Cells[4].FindControl("TienNop");
                    //a.ReadOnly = true;
                    //TextBox b = (TextBox)gvRow.Cells[5].FindControl("TienNo");
                    //b.ReadOnly = true;

                    grvPhiCongTy.Columns[0].HeaderStyle.Width = 30;
                    //grvPhiCongTy.Columns[1].HeaderStyle.Width = 500;
                    //grvPhiCongTy.Columns[2].HeaderStyle.Width = 200;
                    grvPhiCongTy.Columns[3].HeaderStyle.Width = 200;
                    grvPhiCongTy.Columns[4].HeaderStyle.Width = 200;

                    grvPhiCongTy.Columns[5].Visible = false;
                    grvPhiCongTy.Columns[6].Visible = false;
                    grvPhiCongTy.Columns[7].Visible = false;
                    //string[] Key = new string[] {"PhatSinhPhiID", "PhiID", "LoaiPhi"};
                    //grvPhiCongTy.DataKeyNames = Key;
                }
            }


        }
    }

    protected void btntong_OnClick(object sender, EventArgs e)
    {
        v_SetTongDaNop();
    }

    //MinhLP
    //20150205
    //Thay đổi các giá trị khi textbox trong grid thay đổi
    private void v_SetTongDaNop()
    {
        try
        {
            int i = grvPhiCongTy.Rows.Count;
            if (i != 0)
            {
                GridViewRow gvRow = grvPhiCongTy.Rows[i - 1];
                if (gvRow != null)
                {
                    if (gvRow.Cells.Count > 4)
                    {
                        double douTongTienNop = 0;
                        double douTongTienNo = 0;
                        for (int j = 0; j < grvPhiCongTy.Rows.Count - 1; j++)
                        {
                            double douTongTien = 0;
                            double douTienNop = 0;
                            double douTienNo = 0;
                            TextBox a = (TextBox)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                            if (a != null)
                            {
                                int leng = a.Text.Length;
                                if (a.Text.LastIndexOf(',') != -1)
                                    leng = a.Text.LastIndexOf(',');
                                string tien = a.Text.Substring(0, leng);
                                tien = tien.Replace(".", string.Empty);
                                douTongTienNop += Convert.ToDouble(tien);
                                douTienNop = Convert.ToDouble(tien);
                            }
                            Label b = (Label)grvPhiCongTy.Rows[j].Cells[2].FindControl("TongTien");
                            if (b != null && !string.IsNullOrWhiteSpace(b.Text))
                            {
                                int leng = b.Text.Length;
                                if (b.Text.LastIndexOf(',') != -1)
                                    leng = b.Text.LastIndexOf(',');
                                string tien = b.Text.Substring(0, leng);
                                tien = tien.Replace(".", string.Empty);
                                douTongTien = Convert.ToDouble(tien);
                            }
                            douTienNo = douTongTien - douTienNop;
                            douTongTienNo += douTienNo;
                            grvPhiCongTy.Rows[j].Cells[4].Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTienNo);
                        }
                        gvRow.Cells[1].Text = "Tổng cộng:";
                        gvRow.Cells[3].Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTongTienNop);
                        gvRow.Cells[4].Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTongTienNo);
                    }
                }
            }
        }
        catch
        { }
    }

    protected void btnluu_OnClick(object sender, EventArgs e)
    {
        v_Luu();
    }

    private void v_Luu()
    {
        try
        {
            if (string.IsNullOrEmpty(Request.Form["MaGiaoDich"]) && !string.IsNullOrEmpty(MaHoiVienTapThe.Text))
            {
                //Chạy function tự sinh mã phí
                string Nam = "";
                Nam = DateTime.Now.Year.ToString();
                Nam = Nam.Substring(2, 2);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select [dbo].[LaySoChayThanhToanPhiTapThe] (" + Nam + ")";
                ds = DataAccess.RunCMDGetDataSet(cmd);

                string MaGiaoDich = ds.Tables[0].Rows[0][0].ToString();
                DateTime NgayNhap = DateTime.Now;

                using (SqlConnection connection = new SqlConnection(DataAccess.ConnString))
                {
                    connection.Open();
                    SqlCommand sql = connection.CreateCommand();
                    SqlTransaction transaction;
                    transaction = connection.BeginTransaction();
                    sql.Connection = connection;
                    sql.Transaction = transaction;

                    try
                    {
                        sql = new SqlCommand("[dbo].[tblTTNopPhiTapThe_Insert]", connection, transaction);
                        sql.CommandType = CommandType.StoredProcedure;

                        sql.Parameters.AddWithValue("@MaGiaoDich", MaGiaoDich);
                        sql.Parameters.AddWithValue("@HoiVienTapTheId", HoiVienTapTheID.Value);
                        sql.Parameters.AddWithValue("@NopChoNhanVien", "0");
                        sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                        sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                        sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);

                        sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                        sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);

                        sql.Parameters.Add("@GiaoDichID", SqlDbType.Int);
                        sql.Parameters["@GiaoDichID"].Direction = ParameterDirection.Output;

                        //DataAccess.RunActionCmd(sql);
                        sql.ExecuteNonQuery();

                        int? IDGiaoDich = null;
                        if (sql.Parameters["@GiaoDichID"] != null)
                            IDGiaoDich = (int)sql.Parameters["@GiaoDichID"].Value;

                        List<int> lstIDGiaoDichChiTietHoiVien = new List<int>();

                        for (int i = 0; i < grvPhiCongTy.Rows.Count - 1; i++)
                        {
                            string strLoaiPhiCongTy = grvPhiCongTy.DataKeys[i].Values[2].ToString();
                            if (strLoaiPhiCongTy != "11" && strLoaiPhiCongTy != "31" && strLoaiPhiCongTy != "41" && strLoaiPhiCongTy != "51")
                            {
                                CheckBox cbx = (CheckBox)grvPhiCongTy.Rows[i].FindControl("chkEmp");
                                if (cbx.Checked == true)
                                {
                                    double douTienNop = 0;
                                    string strPhatSinhPhiID = "";
                                    string strPhiID = "";
                                    string strLoaiPhi = "";
                                    Label lblTienNop = (Label)grvPhiCongTy.Rows[i].FindControl("TienNop");
                                    douTienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                                    //HiddenField hPhatSinhPhiID = (HiddenField)grvPhiCongTy.Rows[i].Cells[5].FindControl("PhatSinhPhiID");
                                    var PSP = grvPhiCongTy.DataKeys[i].Values[0];
                                    if (PSP != null)
                                    {
                                        strPhatSinhPhiID = PSP.ToString();
                                    }

                                    // HiddenField hPhiID = (HiddenField)grvPhiCongTy.Rows[i].Cells[6].FindControl("PhiID");
                                    var PID = grvPhiCongTy.DataKeys[i].Values[1];
                                    if (PID != null)
                                    {
                                        // strPhiID = hPhiID.Value;
                                        strPhiID = PID.ToString();
                                    }

                                    //HiddenField hLoaiPhi = (HiddenField)grvPhiCongTy.Rows[i].Cells[7].FindControl("LoaiPhi");
                                    var LP = grvPhiCongTy.DataKeys[i].Values[2];
                                    if (LP != null)
                                    {
                                        //strLoaiPhi = hLoaiPhi.Value;
                                        strLoaiPhi = LP.ToString();
                                    }
                                    sql = new SqlCommand("[dbo].[tblTTNopPhiTapTheChiTiet_Insert]", connection, transaction);
                                    sql.CommandType = CommandType.StoredProcedure;
                                    sql.Parameters.AddWithValue("@GiaoDichID", IDGiaoDich);
                                    sql.Parameters.AddWithValue("@HoiVienTapTheID", HoiVienTapTheID.Value);
                                    sql.Parameters.AddWithValue("@PhatSinhPhiID", strPhatSinhPhiID);
                                    sql.Parameters.AddWithValue("@PhiID", strPhiID);
                                    sql.Parameters.AddWithValue("@LoaiPhi", strLoaiPhi);
                                    sql.Parameters.AddWithValue("@SoTienNop", douTienNop);
                                    sql.Parameters.Add("@GiaoDichChiTietID", SqlDbType.Int);
                                    sql.Parameters["@GiaoDichChiTietID"].Direction = ParameterDirection.Output;
                                    sql.ExecuteNonQuery();

                                    if (sql.Parameters["@GiaoDichChiTietID"] != null)
                                    {
                                        int temp = (int)sql.Parameters["@GiaoDichChiTietID"].Value;
                                        lstIDGiaoDichChiTietHoiVien.Add(temp);
                                    }
                                }
                            }
                        }
                        List<int?> lstIdGiaoDichChiTietCaNhan = new List<int?>();
                        //if (ThanhToanChoCaNhan.Checked)
                        //{
                        int? IDGiaoDichChiTiet = null;
                        //DataTable dtbKhoiTao = (DataTable)Session["dtbPhiCaNhan"];

                        for (int i = 0; i < thanhtoanphicanhan_grv.Rows.Count; i++)
                        {
                            GridViewRow row = thanhtoanphicanhan_grv.Rows[i];
                            if (row.Visible)
                            {
                                CheckBox cbx = (CheckBox)row.FindControl("chkEmpCaNhan");
                                if (cbx.Checked)
                                {
                                    double douTienNop = 0;
                                    string strPhatSinhPhiID = thanhtoanphicanhan_grv.DataKeys[i].Values[0].ToString();
                                    string strPhiID = thanhtoanphicanhan_grv.DataKeys[i].Values[1].ToString();
                                    string strLoaiPhi = thanhtoanphicanhan_grv.DataKeys[i].Values[2].ToString(); ;
                                    string strHoiVienID = thanhtoanphicanhan_grv.DataKeys[i].Values[3].ToString();
                                    Label lblTienNop = (Label)thanhtoanphicanhan_grv.Rows[i].FindControl("TienNop");
                                    douTienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                                    sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhanChiTiet_Insert]", connection, transaction);
                                    sql.CommandType = CommandType.StoredProcedure;
                                    sql.Parameters.AddWithValue("@GiaoDichID", IDGiaoDich);
                                    sql.Parameters.AddWithValue("@HoiVienCaNhanID", strHoiVienID);
                                    sql.Parameters.AddWithValue("@PhatSinhPhiID", strPhatSinhPhiID);
                                    sql.Parameters.AddWithValue("@PhiID", strPhiID);
                                    sql.Parameters.AddWithValue("@LoaiPhi", strLoaiPhi);
                                    sql.Parameters.AddWithValue("@SoTienNop", douTienNop);
                                    sql.Parameters.AddWithValue("@DoiTuongNopPhi", (int)EnumVACPA.DoiTuongNopPhi.HoiVienTapThe);
                                    sql.Parameters.Add("@GiaoDichChiTietID", SqlDbType.Int);
                                    sql.Parameters["@GiaoDichChiTietID"].Direction = ParameterDirection.Output;

                                    //DataAccess.RunActionCmd(sql);
                                    sql.ExecuteNonQuery();

                                    if (sql.Parameters["@GiaoDichChiTietID"] != null)
                                    {
                                        IDGiaoDichChiTiet = (int)sql.Parameters["@GiaoDichChiTietID"].Value;
                                        lstIdGiaoDichChiTietCaNhan.Add(IDGiaoDichChiTiet);
                                    }
                                }
                            }
                            //string HoiVienID = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                            //string STT = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                            //string MaHoiVien = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                            //string HoTen = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                            //string SoChungChiKTV = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                            //string DonViCongTac = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                            //string DienGiai = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                            //string TongTien = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                            //string PhatSinhPhiId = dtbKhoiTao.Rows[i]["PhatSinhPhiId"].ToString();
                            //string PhiId = dtbKhoiTao.Rows[i]["PhiId"].ToString();
                            //string LoaiPhi = dtbKhoiTao.Rows[i]["LoaiPhi"].ToString();

                            //TextBox a = (TextBox)thanhtoanphicanhan_grv.Rows[i].Cells[9].FindControl("TruongSoPhiNop");
                            //string TienNop = "";
                            //if (a != null)
                            //{
                            //    int leng = a.Text.Length;
                            //    if (a.Text.LastIndexOf(',') != -1)
                            //        leng = a.Text.LastIndexOf(',');
                            //    TienNop = a.Text.Substring(0, leng);
                            //    TienNop = TienNop.Replace(".", string.Empty);
                            //}
                            //if (!string.IsNullOrEmpty(TienNop))
                            //{

                            //}

                            //}
                        }
                        transaction.Commit();
                        sql.Connection.Close();
                        sql.Connection.Dispose();
                        sql = null;
                        cm.ghilog("TTNopPhiTapThe", "Thêm giá trị \"" + MaGiaoDich + "\" vào danh mục TTNopPhiTapThe");
                        foreach (int temp in lstIDGiaoDichChiTietHoiVien)
                        {
                            cm.ghilog("TTNopPhiTapTheChiTiet", "Thêm bản ghi với ID \"" + temp + "\" vào danh mục TTNopPhiTapTheChiTiet");
                        }
                        foreach (int temp in lstIdGiaoDichChiTietCaNhan)
                        {
                            cm.ghilog("TTNopPhiCaNhanChiTiet", "Thêm bản ghi với ID \"" + temp + "\" vào danh mục TTNopPhiCaNhanChiTiet");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                    }
                }
                Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
            }
        }
        catch
        { }
    }

    private void v_Sua()
    {
        //try
        //{
           
        //}
        //catch
        //{ }
    }

    protected void ThanhToanChoCaNhan_OnCheckedChanged(object sender, EventArgs e)
    {
       
    }

    protected void grvThanhToanPhiTapTheChoCaNhan_OnDataBound(object sender, EventArgs e)
    {

    }

    protected void btnsua_OnClick(object sender, EventArgs e)
    {
        string id = Request.QueryString["Id"].ToString();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT GiaoDichId, TinhTrangID from tblTTNopPhiTapThe where GiaoDichId = " + id;
        DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
        if (dtb.Rows.Count >0)
        {
            if (dtb.Rows[0]["TinhTrangID"].ToString() == ((int)EnumVACPA.TinhTrang.ChoDuyet).ToString())
            {
                //
            }
        }
    }

    protected void btntuchoi_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["mode"] == "view")
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
        string id = Request.QueryString["Id"].ToString();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT GiaoDichId, TinhTrangID from tblTTNopPhiTapThe where GiaoDichId = " + id;
        DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
        if (dtb.Rows.Count > 0)
        {
            if (dtb.Rows[0]["TinhTrangID"].ToString() == ((int)EnumVACPA.TinhTrang.ChoDuyet).ToString())
            {
                sql.CommandText = "UPDATE tblTTNopPhiTapThe set TinhTrangID = @TinhTrang, NgayDuyet = @NgayDuyet, NguoiDuyet = @NguoiDuyet where GiaoDichId = @GiaoDichID";
                sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ChoDuyet);
                sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                sql.Parameters.AddWithValue("@GiaoDichID", dtb.Rows[0]["GiaoDichId"]);
                DataAccess.RunActionCmd(sql);
                cm.ghilog("TTNopPhiTapThe", "Từ chối giao dịch có id = " + dtb.Rows[0]["GiaoDichId"].ToString());
                Response.Redirect("admin.aspx?page=danhsachthanhtoanphi", false);
            }
        }
            }
    }

    protected void btnduyet_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["mode"] == "view")
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                string id = Request.QueryString["Id"].ToString();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "SELECT GiaoDichId, TinhTrangID from tblTTNopPhiTapThe where GiaoDichId = " + id;
                DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtb.Rows.Count > 0)
                {
                    if (dtb.Rows[0]["TinhTrangID"].ToString() == ((int)EnumVACPA.TinhTrang.ChoDuyet).ToString())
                    {
                        sql.CommandText = "UPDATE tblTTNopPhiTapThe set TinhTrangID = @TinhTrang, NgayDuyet = @NgayDuyet, NguoiDuyet = @NguoiDuyet where GiaoDichId = @GiaoDichID";
                        sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.DaPheDuyet);
                        sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                        sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                        sql.Parameters.AddWithValue("@GiaoDichID", dtb.Rows[0]["GiaoDichId"]);
                        DataAccess.RunActionCmd(sql);
                        cm.ghilog("TTNopPhiTapThe", "Duyệt giao dịch có id = " + dtb.Rows[0]["GiaoDichId"].ToString());
                        Response.Redirect("admin.aspx?page=danhsachthanhtoanphi", false);
                    }
                }
            }
    }

    protected void btnthoaiduyet_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["mode"] == "view")
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                string id = Request.QueryString["Id"].ToString();
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "SELECT GiaoDichId, TinhTrangID from tblTTNopPhiTapThe where GiaoDichId = " + id;
                DataTable dtb = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                if (dtb.Rows.Count > 0)
                {
                    if (dtb.Rows[0]["TinhTrangID"].ToString() == ((int)EnumVACPA.TinhTrang.DaPheDuyet).ToString())
                    {
                        sql.CommandText = "UPDATE tblTTNopPhiTapThe set TinhTrangID = @TinhTrang, NgayDuyet = @NgayDuyet, NguoiDuyet = @NguoiDuyet where GiaoDichId = @GiaoDichID";
                        sql.Parameters.AddWithValue("@TinhTrang", (int)EnumVACPA.TinhTrang.ThoaiDuyet);
                        sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                        sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                        sql.Parameters.AddWithValue("@GiaoDichID", dtb.Rows[0]["GiaoDichId"]);
                        DataAccess.RunActionCmd(sql);
                        cm.ghilog("TTNopPhiTapThe", "Thoái duyệt giao dịch có id = " + dtb.Rows[0]["GiaoDichId"].ToString());
                        Response.Redirect("admin.aspx?page=danhsachthanhtoanphi", false);
                    }
                }
            }
    }

    protected void Test_Click(object sender, EventArgs e)
    {
        MaHoiVienTapThe_OnTextChanged(null, null);
    }

    private void LoadGridCaNhan(string Nam)
    {

        thanhtoanphicanhan_grv.Columns[1].HeaderText = "STT";
        thanhtoanphicanhan_grv.Columns[1].SortExpression = truongstt;
        thanhtoanphicanhan_grv.Columns[2].HeaderText = "HVCN ID";
        thanhtoanphicanhan_grv.Columns[2].SortExpression = truongmahoivien;
        thanhtoanphicanhan_grv.Columns[3].HeaderText = "Họ và tên";
        thanhtoanphicanhan_grv.Columns[3].SortExpression = truonghoten;
        thanhtoanphicanhan_grv.Columns[4].HeaderText = "Số CCKTV";
        thanhtoanphicanhan_grv.Columns[4].SortExpression = truongsoccktv;
        thanhtoanphicanhan_grv.Columns[5].HeaderText = "Ngày cấp CCKTV";
        thanhtoanphicanhan_grv.Columns[5].SortExpression = truongngaycapchungchi;
        thanhtoanphicanhan_grv.Columns[6].HeaderText = "Diễn giải";
        thanhtoanphicanhan_grv.Columns[6].SortExpression = "DienGiai";
        thanhtoanphicanhan_grv.Columns[7].HeaderText = "Số phí phải nộp";
        thanhtoanphicanhan_grv.Columns[7].SortExpression = truongsophiphainop;
        thanhtoanphicanhan_grv.Columns[8].HeaderText = "Số phí đã nộp/thanh toán";
        thanhtoanphicanhan_grv.Columns[8].SortExpression = truongsophidanop;
        thanhtoanphicanhan_grv.Columns[9].HeaderText = "Số phí còn nợ";
        thanhtoanphicanhan_grv.Columns[9].SortExpression = truongsophiconno;
        thanhtoanphicanhan_grv.Columns[10].HeaderText = "Thao tác";
      //  string strLoaiPhi = LoaiPhi.SelectedValue;
        DataTable dtbTemp = new DataTable();
        dtbTemp.Columns.Add("HoiVienID");
        dtbTemp.Columns.Add("STT");
        dtbTemp.Columns.Add("MaHoiVien");
        dtbTemp.Columns.Add("HoTen");
        dtbTemp.Columns.Add("SoChungChiKTV");
        dtbTemp.Columns.Add("NgayCapChungChiKTV");
        dtbTemp.Columns.Add("DienGiai");
        dtbTemp.Columns.Add("TongTien", typeof(decimal));
        dtbTemp.Columns.Add("TienNop", typeof(decimal));
        dtbTemp.Columns.Add("SoPhiConNo", typeof(decimal));
        dtbTemp.Columns.Add("PhatSinhPhiId");
        dtbTemp.Columns.Add("PhiId");
        dtbTemp.Columns.Add("LoaiPhi");
        dtbTemp.Columns.Add("Active", typeof(bool));
        //if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString())
        //{
            //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

            //Session["dtbPhiCaNhan"] = dtbTemp;

            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiHoiVien_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiHoiVien + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            
        //}
        //else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString())
        //{
            //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", 1000000, 1000000, "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", 1000000, 1000000, "0", "1", "1", strLoaiPhi);

            //Session["dtbPhiCaNhan"] = dtbTemp;

            sql = new SqlCommand();
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiCNKT_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc + ", " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        //}
        //else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString())
        //{
            //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("3", "3", "3", "C", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

            //Session["dtbPhiCaNhan"] = dtbTemp;
            sql = new SqlCommand();
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKSCL_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKSCL_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        //}
        //else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString())
        //{
            //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("3", "3", "3", "C", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("4", "4", "4", "D", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

            //Session["dtbPhiCaNhan"] = dtbTemp;
            sql = new SqlCommand();
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiTapThe_CaNhan_PhiKhac_All] " + HoiVienTapTheID.Value + "," + (int)EnumVACPA.LoaiPhi.PhiKhac + ", " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",'" + Nam + "'";
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        //}
        //SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT HoiVienID, '1' as STT, B.MaHoiVien, B.HoDem + ' ' + B.Ten as HoTen, "
        //+ "B.SoChungChiKTV, B.DonViCongTac, FROM tblDMDoiTuongNopPhi";
        //DataSet ds = new DataSet();
        //DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
        Session["dtbPhiCaNhan"] = dtbTemp;
        thanhtoanphicanhan_grv.DataSource = dtbTemp;
        thanhtoanphicanhan_grv.DataBind();

        vDanhSoGridCaNhan();
    }

    private void LoadView()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            //DataTable dtbTemp = new DataTable();
            //dtbTemp.Columns.Add("Chon");
            //dtbTemp.Columns.Add("STT");
            //dtbTemp.Columns.Add("MaHoiVien");
            //dtbTemp.Columns.Add("HoTen");
            //dtbTemp.Columns.Add("SoChungChiKTV");
            //dtbTemp.Columns.Add("DonViCongTac");
            //dtbTemp.Columns.Add("DienGiai");
            //dtbTemp.Columns.Add("TongTien", typeof(decimal));
            //dtbTemp.Columns.Add("TienNop", typeof(decimal));
            //dtbTemp.Columns.Add("SoPhiConNo", typeof(decimal));
            //dtbTemp.Columns.Add("PhatSinhPhiId");
            //dtbTemp.Columns.Add("PhiId");
            //dtbTemp.Columns.Add("LoaiPhi");
            //dtbTemp.Columns.Add("HoiVienID");

            SqlCommand sql1 = new SqlCommand();
            sql1.CommandText = "select GiaoDichId, MaGiaoDich, A.HoiVienTapTheId, MaHoiVienTapThe, TenDoanhNghiep, "
                + " A.TinhTrangID, TenTrangThai "
                + " from tblTTNopPhiTapThe A left join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiID "
                + " left join tblHoiVienTapThe C on A.HoiVienTapTheId = C.HoiVienTapTheID "
            + " where GiaoDichId = " + Convert.ToInt16(Request.QueryString["id"]);
            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql1);
            dt = ds.Tables[0];
            int iTinhTrangBanGhi = 0;
            string strTinhTrangBanGhi = "";
            DateTime? NgayTraCuu = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            if (dt.Rows.Count > 0)
            {
               // Response.Write("$('#MaGiaoDich').val('" + dt.Rows[0]["MaGiaoDich"] + "');" + System.Environment.NewLine);
                HoiVienTapTheID.Value = dt.Rows[0]["HoiVienTapTheId"].ToString();
                MaHoiVienTapThe.Text = dt.Rows[0]["MaHoiVienTapThe"].ToString().TrimEnd();
                //Response.Write("$('#MaHoiVienTapThe').val('" + dt.Rows[0]["MaHoiVienTapThe"].ToString().TrimEnd() + "');" + System.Environment.NewLine);
                TenCongTy.Text = dt.Rows[0]["TenDoanhNghiep"].ToString();
                iTinhTrangBanGhi = Convert.ToInt32(dt.Rows[0]["TinhTrangID"]);
                strTinhTrangBanGhi = dt.Rows[0]["TenTrangThai"].ToString();
            }

            thanhtoanphicanhan_grv.Columns[1].HeaderText = "STT";
            thanhtoanphicanhan_grv.Columns[1].SortExpression = truongstt;
            thanhtoanphicanhan_grv.Columns[2].HeaderText = "HVCN ID";
            thanhtoanphicanhan_grv.Columns[2].SortExpression = truongmahoivien;
            thanhtoanphicanhan_grv.Columns[3].HeaderText = "Họ và tên";
            thanhtoanphicanhan_grv.Columns[3].SortExpression = truonghoten;
            thanhtoanphicanhan_grv.Columns[4].HeaderText = "Số CCKTV";
            thanhtoanphicanhan_grv.Columns[4].SortExpression = truongsoccktv;
            thanhtoanphicanhan_grv.Columns[5].HeaderText = "Ngày cấp CCKTV";
            thanhtoanphicanhan_grv.Columns[5].SortExpression = truongngaycapchungchi;
            thanhtoanphicanhan_grv.Columns[6].HeaderText = "Diễn giải";
            thanhtoanphicanhan_grv.Columns[6].SortExpression = "DienGiai";
            thanhtoanphicanhan_grv.Columns[7].HeaderText = "Số phí phải nộp";
            thanhtoanphicanhan_grv.Columns[7].SortExpression = truongsophiphainop;
            thanhtoanphicanhan_grv.Columns[8].HeaderText = "Số phí đã nộp/thanh toán";
            thanhtoanphicanhan_grv.Columns[8].SortExpression = truongsophidanop;
            thanhtoanphicanhan_grv.Columns[9].HeaderText = "Số phí còn nợ";
            thanhtoanphicanhan_grv.Columns[9].SortExpression = truongsophiconno;
            thanhtoanphicanhan_grv.Columns[10].HeaderText = "Thao tác";

            DataTable dtbNopPhiTapTheChiTiet = new DataTable();
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"SELECT *, HoiVienTapTheID as HoiVienID, '' as MaPhi, CASE
WHEN LoaiPhi = "+ (int)EnumVACPA.LoaiPhi.PhiHoiVien + @" THEN N'"+ EnumVACPA.stringValueOf(EnumVACPA.TenLoaiPhi.PhiHoiVien) +@"' 
WHEN LoaiPhi = " + (int)EnumVACPA.LoaiPhi.PhiDangLogo + @" THEN N'" + EnumVACPA.stringValueOf(EnumVACPA.TenLoaiPhi.PhiDangLogo) + @"' 
WHEN LoaiPhi = " + (int)EnumVACPA.LoaiPhi.PhiKhac + @" THEN N'" + EnumVACPA.stringValueOf(EnumVACPA.TenLoaiPhi.PhiKhac) + @"' 
WHEN LoaiPhi = " + (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong + @" THEN N'" + EnumVACPA.stringValueOf(EnumVACPA.TenLoaiPhi.PhiKiemSoatChatLuong) + @"' 
END as TenLoaiPhi,   SoTienNop as TongTien, SoTienNop as TienNop, 0 as TienNo FROM tblTTNopPhiTapTheChiTiet
        where GiaoDichID = " + Request.QueryString["id"];
            dtbNopPhiTapTheChiTiet = DataAccess.RunCMDGetDataSet(sql).Tables[0];

            DataTable dtbTapTheNopChoCaNhan = new DataTable();

            sql.CommandText = @"select ROW_NUMBER() 
        OVER (ORDER BY HoiVienID) as STT, * from (SELECT A.HoiVienCaNhanID as HoiVienID, B.MaHoiVienCaNhan as MaHoiVien, 
                B.HoDem + ' ' + B.Ten as HoTen, B.SoChungChiKTV, B.NgayCapChungChiKTV,
CASE LoaiPhi WHEN 1 THEN N'Phí hội viên' WHEN 3 THEN N'Phí khác' WHEN 5 THEN N'Phí KSCL' when 4 then (select top 1 MaLopHoc from tblCNKTLopHoc where LopHocID = (select top 1 DoiTuongPhatSinhPhiID from tblTTPhatSinhPhi where PhatSinhPhiID = A.PhatSinhPhiID))  END as DienGiai
, PhiID,
                PhatSinhPhiID, LoaiPhi, SoTienNop as TongTien, SoTienNop as TienNop, 0 as SoPhiConNo
                from tblTTNopPhiCaNhanChiTiet A left join tblHoiVienCaNhan B on A.HoiVienCaNhanID = B.HoiVienCaNhanID
                where A.GiaoDichID = " + Request.QueryString["id"] + "" +
                              " and A.DoiTuongNopPhi = "+ (int)EnumVACPA.DoiTuongNopPhi.HoiVienTapThe +") as TEMP";

            dtbTapTheNopChoCaNhan = DataAccess.RunCMDGetDataSet(sql).Tables[0];

            double douTongPhiHoiVien = 0;
            double douTongPhiKhac = 0;
            double douTongPhiCNKT = 0;
            double douTongPhiKSCL = 0;

            foreach (DataRow dtr in dtbTapTheNopChoCaNhan.Rows)
            {
                if (dtr["LoaiPhi"].ToString() == ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString())
                {
                    douTongPhiHoiVien += Convert.ToDouble(dtr["TongTien"]);
                }
                else if (dtr["LoaiPhi"].ToString() == ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString())
                {
                    douTongPhiKhac += Convert.ToDouble(dtr["TongTien"]);
                }
                else if (dtr["LoaiPhi"].ToString() == ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString())
                {
                    douTongPhiCNKT += Convert.ToDouble(dtr["TongTien"]);
                }
                else if (dtr["LoaiPhi"].ToString() == ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString())
                {
                    douTongPhiKSCL += Convert.ToDouble(dtr["TongTien"]);
                }
            }

            if(douTongPhiHoiVien > 0)
            {
                DataRow dtrThem1 = dtbNopPhiTapTheChiTiet.NewRow();
                dtrThem1["PhiID"] = "0";
                dtrThem1["PhatSinhPhiID"] = "0";
                dtrThem1["HoiVienID"] = "0";
                dtrThem1["MaPhi"] = "";
                dtrThem1["LoaiPhi"] = "11";
                dtrThem1["TenLoaiPhi"] = "Phí HVCN";
                dtrThem1["TongTien"] = douTongPhiHoiVien;
                dtrThem1["TienNop"] = douTongPhiHoiVien;
                dtrThem1["TienNo"] = 0;
                dtbNopPhiTapTheChiTiet.Rows.Add(dtrThem1);
            }
            if (douTongPhiKhac > 0)
            {
                DataRow dtrThem1 = dtbNopPhiTapTheChiTiet.NewRow();
                dtrThem1["PhiID"] = "0";
                dtrThem1["PhatSinhPhiID"] = "0";
                dtrThem1["HoiVienID"] = "0";
                dtrThem1["MaPhi"] = "";
                dtrThem1["LoaiPhi"] = "31";
                dtrThem1["TenLoaiPhi"] = "Phí Khác";
                dtrThem1["TongTien"] = douTongPhiKhac;
                dtrThem1["TienNop"] = douTongPhiKhac;
                dtrThem1["TienNo"] = 0;
                dtbNopPhiTapTheChiTiet.Rows.Add(dtrThem1);
            }
            if (douTongPhiCNKT > 0)
            {
                DataRow dtrThem1 = dtbNopPhiTapTheChiTiet.NewRow();
                dtrThem1["PhiID"] = "0";
                dtrThem1["PhatSinhPhiID"] = "0";
                dtrThem1["HoiVienID"] = "0";
                dtrThem1["MaPhi"] = "";
                dtrThem1["LoaiPhi"] = "41";
                dtrThem1["TenLoaiPhi"] = "Phí CNKT";
                dtrThem1["TongTien"] = douTongPhiCNKT;
                dtrThem1["TienNop"] = douTongPhiCNKT;
                dtrThem1["TienNo"] = 0;
                dtbNopPhiTapTheChiTiet.Rows.Add(dtrThem1);
            }
            if (douTongPhiKSCL > 0)
            {
                DataRow dtrThem1 = dtbNopPhiTapTheChiTiet.NewRow();
                dtrThem1["PhiID"] = "0";
                dtrThem1["PhatSinhPhiID"] = "0";
                dtrThem1["HoiVienID"] = "0";
                dtrThem1["MaPhi"] = "";
                dtrThem1["LoaiPhi"] = "51";
                dtrThem1["TenLoaiPhi"] = "Phí KSCL";
                dtrThem1["TongTien"] = douTongPhiKSCL;
                dtrThem1["TienNop"] = douTongPhiKSCL;
                dtrThem1["TienNo"] = 0;
                dtbNopPhiTapTheChiTiet.Rows.Add(dtrThem1);
            }

            double douTongTien = 0;
            foreach (DataRow dtr in dtbNopPhiTapTheChiTiet.Rows)
            {
                if (dtr["TongTien"] != null)
                    douTongTien += Convert.ToDouble(dtr["TongTien"]);
            }

            DataRow dtrThem = dtbNopPhiTapTheChiTiet.NewRow();
            dtrThem["PhiID"] = "0";
            dtrThem["PhatSinhPhiID"] = "0";
            dtrThem["HoiVienID"] = "0";
            dtrThem["MaPhi"] = "";
            dtrThem["LoaiPhi"] = "10";
            dtrThem["TenLoaiPhi"] = "Tổng cộng";
            dtrThem["TongTien"] = douTongTien;
            dtrThem["TienNop"] = douTongTien;
            dtrThem["TienNo"] = "0";
            dtbNopPhiTapTheChiTiet.Rows.Add(dtrThem);

            grvPhiCongTy.DataSource = dtbNopPhiTapTheChiTiet;
            grvPhiCongTy.DataBind();
            thanhtoanphicanhan_grv.DataSource = dtbTapTheNopChoCaNhan;
            thanhtoanphicanhan_grv.DataBind();
            vDanhSoGridCaNhan();
        }
    }

    public void LoadLoaiPhi()
    {
        ////string output_html = "";

        //DataTable dtbLoaiPhi = new DataTable();
        //dtbLoaiPhi.Columns.Add("ID");
        //dtbLoaiPhi.Columns.Add("TEN");
        //dtbLoaiPhi.Rows.Add("1", "Phí Hội viên");
        //dtbLoaiPhi.Rows.Add("4", "Phí cập nhật kiến thức");
        //dtbLoaiPhi.Rows.Add("5", "Phí kiểm soát chất lượng");
        //dtbLoaiPhi.Rows.Add("3", "Phí khác");

        //LoaiPhi.DataSource = dtbLoaiPhi;
        //LoaiPhi.DataBind();
        //LoaiPhi_SelectedIndexChanged(null, null);
        ////    int i = 0;
        ////foreach (DataRow dtr in dtbLoaiPhi.Rows)
        ////    {
        ////        i++;
        ////        if (i == 1)
        ////            output_html += @"<option selected=""selected"" value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
        ////        else
        ////            output_html += @"<option value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
        ////    }
        ////System.Web.HttpContext.Current.Response.Write(output_html);
    }

    protected void LoaiPhi_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LoadGrid();
    }

    protected void thanhtoanphicanhan_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        load_data();
    }

    protected void HienThanhToanCaNhan_Click(object sender, EventArgs e)
    {
        //if (ThanhToanChoCaNhan.Checked)
        //{
        //    PanelCaNhan.Visible = true;
        //    LoadLoaiPhi();
        //}
        //else
        //    PanelCaNhan.Visible = false;
    }

    protected void btntongcanhan_OnClick(object sender, EventArgs e)
    {
        v_SetPhiConNo();
    }

    private void v_SetPhiConNo()
    {
        try
        {
            for (int j = 0; j < thanhtoanphicanhan_grv.Rows.Count; j++)
            {
                string PhaiNop = "";
                string ThanhToan = "";
                string ConNo = "";
                TextBox a = (TextBox)thanhtoanphicanhan_grv.Rows[j].Cells[8].FindControl("TruongSoPhiNop");
                if (a != null)
                {
                    int leng = a.Text.Length;
                    if (a.Text.LastIndexOf(',') != -1)
                        leng = a.Text.LastIndexOf(',');
                    string tien = a.Text.Substring(0, leng);
                    ThanhToan = tien.Replace(".", string.Empty);
                }
                Label b = (Label)thanhtoanphicanhan_grv.Rows[j].Cells[7].FindControl("TruongSoPhiPhaiNop");
                int lengphainop = b.Text.Length;
                if (b.Text.LastIndexOf(',') != -1)
                    lengphainop = b.Text.LastIndexOf(',');
                string tienphainop = b.Text.Substring(0, lengphainop);
                PhaiNop = tienphainop.Replace(".", string.Empty);

                try
                {
                    double douTienNo = Convert.ToDouble(PhaiNop) - Convert.ToDouble(ThanhToan);
                    thanhtoanphicanhan_grv.Rows[j].Cells[9].Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTienNo);
                }
                catch { }
            }
        }
        catch
        { }
    }

    protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)grvPhiCongTy.HeaderRow.FindControl("chkboxSelectAll");
        for (int i = 0; i < grvPhiCongTy.Rows.Count - 1; i ++)
        {
            GridViewRow row = grvPhiCongTy.Rows[i];
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkEmp");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                chkEmp_CheckedChanged(ChkBoxRows, null);
            }
            else
            {
                ChkBoxRows.Checked = false;
                chkEmp_CheckedChanged(ChkBoxRows, null);
            }
        }
        grvPhiCongTy_OnDataBound(null, null);
    }

    protected void chkEmp_CheckedChanged(object sender, EventArgs e)
    {

        CheckBox cbkdangcheck = (CheckBox)sender;
        GridViewRow rowdangcheck = (GridViewRow)cbkdangcheck.NamingContainer;
        int idongdangcheck = Convert.ToInt32(rowdangcheck.RowIndex);

        string b = grvPhiCongTy.DataKeys[idongdangcheck].Values[2].ToString();
        if (b == "11")
        {
            for (int j = 0; j < thanhtoanphicanhan_grv.Rows.Count; j++)
            {
                string c = thanhtoanphicanhan_grv.DataKeys[j].Values[2].ToString();
                if (c == ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString())
                {
                    CheckBox cbxcanhan = (CheckBox)thanhtoanphicanhan_grv.Rows[j].FindControl("chkEmpCaNhan");
                    //Nếu Check ở cty thì hiện các thằng cá nhân lên và ngược lại
                    if (cbkdangcheck.Checked == true)
                    {
                        //thanhtoanphicanhan_grv.Rows[j].Visible = true;
                        if (cbxcanhan.Checked == false && cbxcanhan.Enabled)
                        {
                            cbxcanhan.Checked = true;
                            chkEmpCaNhan_CheckedChanged(cbxcanhan, null);
                        }
                    }
                    else
                    {
                        //thanhtoanphicanhan_grv.Rows[j].Visible = false;
                        if (cbxcanhan.Checked == true)
                        {
                            cbxcanhan.Checked = false;
                            chkEmpCaNhan_CheckedChanged(cbxcanhan, null);
                        }
                    }
                }
            }
            vDanhSoGridCaNhan();
        }
        else if (b == "41")
        {
            for (int j = 0; j < thanhtoanphicanhan_grv.Rows.Count; j++)
            {
                string c = thanhtoanphicanhan_grv.DataKeys[j].Values[2].ToString();
                if (c == ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString())
                {
                    CheckBox cbxcanhan = (CheckBox)thanhtoanphicanhan_grv.Rows[j].FindControl("chkEmpCaNhan");
                    //Nếu Check ở cty thì hiện các thằng cá nhân lên và ngược lại
                    if (cbkdangcheck.Checked == true)
                    {
                        //thanhtoanphicanhan_grv.Rows[j].Visible = true;
                        if (cbxcanhan.Checked == false && cbxcanhan.Enabled)
                        {
                            cbxcanhan.Checked = true;
                            chkEmpCaNhan_CheckedChanged(cbxcanhan, null);
                        }
                    }
                    else
                    {
                        //thanhtoanphicanhan_grv.Rows[j].Visible = false;
                        if (cbxcanhan.Checked == true)
                        {
                            cbxcanhan.Checked = false;
                            chkEmpCaNhan_CheckedChanged(cbxcanhan, null);
                        }
                    }
                }
            }
            vDanhSoGridCaNhan();
        }
        else if (b == "51")
        {
            for (int j = 0; j < thanhtoanphicanhan_grv.Rows.Count; j++)
            {
                string c = thanhtoanphicanhan_grv.DataKeys[j].Values[2].ToString();
                if (c == ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString())
                {
                    CheckBox cbxcanhan = (CheckBox)thanhtoanphicanhan_grv.Rows[j].FindControl("chkEmpCaNhan");
                    //Nếu Check ở cty thì hiện các thằng cá nhân lên và ngược lại
                    if (cbkdangcheck.Checked == true)
                    {
                        //thanhtoanphicanhan_grv.Rows[j].Visible = true;
                        if (cbxcanhan.Checked == false && cbxcanhan.Enabled)
                        {
                            cbxcanhan.Checked = true;
                            chkEmpCaNhan_CheckedChanged(cbxcanhan, null);
                        }
                    }
                    else
                    {
                        //thanhtoanphicanhan_grv.Rows[j].Visible = false;
                        if (cbxcanhan.Checked == true)
                        {
                            cbxcanhan.Checked = false;
                            chkEmpCaNhan_CheckedChanged(cbxcanhan, null);
                        }
                    }
                }
            }
            vDanhSoGridCaNhan();
        }
        else if (b == "31")
        {
            for (int j = 0; j < thanhtoanphicanhan_grv.Rows.Count; j++)
            {
                string c = thanhtoanphicanhan_grv.DataKeys[j].Values[2].ToString();
                if (c == ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString())
                {
                    CheckBox cbxcanhan = (CheckBox)thanhtoanphicanhan_grv.Rows[j].FindControl("chkEmpCaNhan");
                    //Nếu Check ở cty thì hiện các thằng cá nhân lên và ngược lại
                    if (cbkdangcheck.Checked == true)
                    {
                        //thanhtoanphicanhan_grv.Rows[j].Visible = true;
                        if (cbxcanhan.Checked == false && cbxcanhan.Enabled)
                        {
                            cbxcanhan.Checked = true;
                            chkEmpCaNhan_CheckedChanged(cbxcanhan, null);
                        }
                    }
                    else
                    {
                        //thanhtoanphicanhan_grv.Rows[j].Visible = false;
                        if (cbxcanhan.Checked == true)
                        {
                            cbxcanhan.Checked = false;
                            chkEmpCaNhan_CheckedChanged(cbxcanhan, null);
                        }
                    }
                }
            }
            vDanhSoGridCaNhan();
        }

        for (int i = 0; i < grvPhiCongTy.Rows.Count - 1; i++)
        {
            string d = grvPhiCongTy.DataKeys[i].Values[2].ToString();
            if (d != "11" && d!= "31" && d != "41" && d!= "51")
            {
                GridViewRow row = grvPhiCongTy.Rows[i];
                CheckBox cbx = (CheckBox)row.FindControl("chkEmp");
                Label lblTongTien = (Label)row.Cells[2].FindControl("TongTien");
                if (cbx.Checked == true)
                {
                    Label lblTienNop = (Label)row.Cells[3].FindControl("TienNop");
                    Label lblTienNo = (Label)row.Cells[4].FindControl("TienNo");
                    lblTienNop.Text = lblTongTien.Text;
                    lblTienNo.Text = "0";
                }
                else
                {
                    Label lblTienNop = (Label)row.Cells[3].FindControl("TienNop");
                    Label lblTienNo = (Label)row.Cells[4].FindControl("TienNo");
                    lblTienNo.Text = lblTongTien.Text;
                    lblTienNop.Text = "0";
                }
            }
        }

        int k = grvPhiCongTy.Rows.Count;
        if (k != 0)
        {
            grvPhiCongTy_OnDataBound(null, null);
        }
    }

    protected void chkboxSelectCaNhanAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)thanhtoanphicanhan_grv.HeaderRow.FindControl("chkboxSelectCaNhanAll");
        for (int i = 0; i < thanhtoanphicanhan_grv.Rows.Count; i++)
        {
            GridViewRow row = thanhtoanphicanhan_grv.Rows[i];
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkEmpCaNhan");
            if (row.Visible)
            {
                if (ChkBoxHeader.Checked == true)
                {
                    if (ChkBoxRows.Enabled == true && ChkBoxRows.Checked == false)
                    {
                        ChkBoxRows.Checked = true;
                        chkEmpCaNhan_CheckedChanged(ChkBoxRows, null);
                    }
                }
                else
                {
                    if (ChkBoxRows.Enabled == true && ChkBoxRows.Checked == true)
                    {
                        ChkBoxRows.Checked = false;
                        chkEmpCaNhan_CheckedChanged(ChkBoxRows, null);
                    }
                }
            }
        }
       // grvPhiCongTy_OnDataBound(null, null);
    }

    protected void chkEmpCaNhan_CheckedChanged(object sender, EventArgs e)
    {
        //Lấy check box, row và số thứ tự của dòng đang check
        CheckBox cbkdangcheck = (CheckBox)sender;
        GridViewRow rowdangcheck = (GridViewRow)cbkdangcheck.NamingContainer;
        int i = Convert.ToInt32(rowdangcheck.RowIndex);

        //Lấy giá trị tổng tiền
        Label lblTongTien = (Label)rowdangcheck.Cells[7].FindControl("TongTien");

        //Nếu check: Tiền nộp = Tổng tiền, Tiền nợ = 0
        //Nếu không check: Tiền nộp = 0, Tiền nợ = Tổng tiền

        double TienDangCheck = 0;

        if (cbkdangcheck.Checked == true)
        {
            Label lblTienNop = (Label)rowdangcheck.Cells[8].FindControl("TienNop");
            Label lblTienNo = (Label)rowdangcheck.Cells[9].FindControl("TienNo");
            lblTienNop.Text = lblTongTien.Text;
            lblTienNo.Text = "0";

             int lengphainop = lblTienNop.Text.Length;
                if (lblTienNop.Text.LastIndexOf(',') != -1)
                    lengphainop = lblTienNop.Text.LastIndexOf(',');
                string tienphainop = lblTienNop.Text.Substring(0, lengphainop);
                string strNop = tienphainop.Replace(".", string.Empty);
                
                        int lengtienno = lblTienNo.Text.Length;
                if (lblTienNo.Text.LastIndexOf(',') != -1)
                    lengtienno = lblTienNo.Text.LastIndexOf(',');
                string tienno = lblTienNo.Text.Substring(0, lengtienno);
                string strNo = tienno.Replace(".", string.Empty);

            TienDangCheck = Convert.ToDouble(strNop);
        }
        else
        {
            Label lblTienNop = (Label)rowdangcheck.Cells[8].FindControl("TienNop");
            Label lblTienNo = (Label)rowdangcheck.Cells[9].FindControl("TienNo");
            lblTienNo.Text = lblTongTien.Text;
            lblTienNop.Text = "0";

            int lengphainop = lblTienNop.Text.Length;
            if (lblTienNop.Text.LastIndexOf(',') != -1)
                lengphainop = lblTienNop.Text.LastIndexOf(',');
            string tienphainop = lblTienNop.Text.Substring(0, lengphainop);
            string strNop = tienphainop.Replace(".", string.Empty);

            int lengtienno = lblTienNo.Text.Length;
            if (lblTienNo.Text.LastIndexOf(',') != -1)
                lengtienno = lblTienNo.Text.LastIndexOf(',');
            string tienno = lblTienNo.Text.Substring(0, lengtienno);
            string strNo = tienno.Replace(".", string.Empty);

            TienDangCheck = Convert.ToDouble(strNo);
        }

        //Lấy loại phí của dòng đang thanh toán
        string strLoaiPhi = thanhtoanphicanhan_grv.DataKeys[i].Values[2].ToString();

        //Nếu loại phí là phí hội viên: cập nhật ngược lại vào dòng thanh toán phí hội viên cá nhân của cty
        //Nếu loại phí là phí khác: cập nhật ngược lại vào dòng thanh toán phí khác cá nhân  của cty
        //Nếu loại phí là phí CNKT: cập nhật ngược lại vào dòng thanh toán phí CNKT của cty
        //Nếu loại phí là phí KSCL: cập nhật ngược lại vào dòng thanh toán phí KSCL của cty
        if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString())
        {
            for (int j = 0; j < grvPhiCongTy.Rows.Count - 1; j++)
            {
                string strLoaiPhiTrenCongTy = grvPhiCongTy.DataKeys[j].Values[2].ToString();
                //Nếu dòng đang chạy là thanh toán cho Hội viên cá nhân thì xử lý
                if (strLoaiPhiTrenCongTy == "11")
                {
                    //Nếu check => thêm tiền ở dòng tiền nộp, trừ tiền ở dòng tiền nợ
                    //Nếu bỏ check => trừ tiền ở dòng tiền nộp, thêm tiền ở dòng tiền nợ
                    if (cbkdangcheck.Checked == true)
                    {
                        //Lấy ra tiền nộp và tiền nợ ở phí công ty
                        Label lblTienNop = (Label)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                        Label lblTienNo = (Label)grvPhiCongTy.Rows[j].Cells[4].FindControl("TienNo");

                        //Convert sang kiểu double
                        double TienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                        double TienNo = Convert.ToDouble(strBoChamPhay(lblTienNo.Text));

                        //Xử lý
                        TienNop = TienNop + TienDangCheck;
                        TienNo = TienNo - TienDangCheck;

                        //Gán ngược lại
                        lblTienNop.Text =  string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNop);
                        lblTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNo);
                    }
                    else
                    {
                        //Lấy ra tiền nộp và tiền nợ ở phí công ty
                        Label lblTienNop = (Label)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                        Label lblTienNo = (Label)grvPhiCongTy.Rows[j].Cells[4].FindControl("TienNo");

                        //Convert sang kiểu double
                        double TienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                        double TienNo = Convert.ToDouble(strBoChamPhay(lblTienNo.Text));

                        TienNop = TienNop - TienDangCheck;
                        TienNo = TienNo + TienDangCheck;



                        //Gán ngược lại
                        lblTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNop);
                        lblTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNo);
                    }
                    break;
                }
            }
        }
        else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString())
        {
            for (int j = 0; j < grvPhiCongTy.Rows.Count - 1; j++)
            {
                string strLoaiPhiTrenCongTy = grvPhiCongTy.DataKeys[j].Values[2].ToString();
                //Nếu dòng đang chạy là thanh toán cho phí khác cá nhân thì xử lý
                if (strLoaiPhiTrenCongTy == "31")
                {
                    //Nếu check => thêm tiền ở dòng tiền nộp, trừ tiền ở dòng tiền nợ
                    //Nếu bỏ check => trừ tiền ở dòng tiền nộp, thêm tiền ở dòng tiền nợ
                    if (cbkdangcheck.Checked == true)
                    {
                        //Lấy ra tiền nộp và tiền nợ ở phí công ty
                        Label lblTienNop = (Label)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                        Label lblTienNo = (Label)grvPhiCongTy.Rows[j].Cells[4].FindControl("TienNo");

                        //Convert sang kiểu double
                        double TienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                        double TienNo = Convert.ToDouble(strBoChamPhay(lblTienNo.Text));

                        //Xử lý
                        TienNop = TienNop + TienDangCheck;
                        TienNo = TienNo - TienDangCheck;

                        //Gán ngược lại
                        lblTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNop);
                        lblTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNo);
                    }
                    else
                    {
                        //Lấy ra tiền nộp và tiền nợ ở phí công ty
                        Label lblTienNop = (Label)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                        Label lblTienNo = (Label)grvPhiCongTy.Rows[j].Cells[4].FindControl("TienNo");

                        //Convert sang kiểu double
                        double TienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                        double TienNo = Convert.ToDouble(strBoChamPhay(lblTienNo.Text));

                        TienNop = TienNop - TienDangCheck;
                        TienNo = TienNo + TienDangCheck;

                        //Gán ngược lại
                        lblTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNop);
                        lblTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNo);
                    }
                    break;
                }
            }
        }
        else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString())
        {
            for (int j = 0; j < grvPhiCongTy.Rows.Count - 1; j++)
            {
                string strLoaiPhiTrenCongTy = grvPhiCongTy.DataKeys[j].Values[2].ToString();
                //Nếu dòng đang chạy là thanh toán CNKT cá nhân thì xử lý
                if (strLoaiPhiTrenCongTy == "41")
                {
                    //Nếu check => thêm tiền ở dòng tiền nộp, trừ tiền ở dòng tiền nợ
                    //Nếu bỏ check => trừ tiền ở dòng tiền nộp, thêm tiền ở dòng tiền nợ
                    if (cbkdangcheck.Checked == true)
                    {
                        //Lấy ra tiền nộp và tiền nợ ở phí công ty
                        Label lblTienNop = (Label)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                        Label lblTienNo = (Label)grvPhiCongTy.Rows[j].Cells[4].FindControl("TienNo");

                        //Convert sang kiểu double
                        double TienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                        double TienNo = Convert.ToDouble(strBoChamPhay(lblTienNo.Text));

                        //Xử lý
                        TienNop = TienNop + TienDangCheck;
                        TienNo = TienNo - TienDangCheck;

                        //Gán ngược lại
                        lblTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNop);
                        lblTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNo);
                    }
                    else
                    {
                        //Lấy ra tiền nộp và tiền nợ ở phí công ty
                        Label lblTienNop = (Label)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                        Label lblTienNo = (Label)grvPhiCongTy.Rows[j].Cells[4].FindControl("TienNo");

                        //Convert sang kiểu double
                        double TienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                        double TienNo = Convert.ToDouble(strBoChamPhay(lblTienNo.Text));

                        TienNop = TienNop - TienDangCheck;
                        TienNo = TienNo + TienDangCheck;

                        //Gán ngược lại
                        lblTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNop);
                        lblTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNo);
                    }
                    break;
                }
            }
        }
        else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString())
        {
            for (int j = 0; j < grvPhiCongTy.Rows.Count - 1; j++)
            {
                string strLoaiPhiTrenCongTy = grvPhiCongTy.DataKeys[j].Values[2].ToString();
                //Nếu dòng đang chạy là thanh toán KSCL thì xử lý
                if (strLoaiPhiTrenCongTy == "51")
                {
                    //Nếu check => thêm tiền ở dòng tiền nộp, trừ tiền ở dòng tiền nợ
                    //Nếu bỏ check => trừ tiền ở dòng tiền nộp, thêm tiền ở dòng tiền nợ
                    if (cbkdangcheck.Checked == true)
                    {
                        //Lấy ra tiền nộp và tiền nợ ở phí công ty
                        Label lblTienNop = (Label)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                        Label lblTienNo = (Label)grvPhiCongTy.Rows[j].Cells[4].FindControl("TienNo");

                        //Convert sang kiểu double
                        double TienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                        double TienNo = Convert.ToDouble(strBoChamPhay(lblTienNo.Text));

                        //Xử lý
                        TienNop = TienNop + TienDangCheck;
                        TienNo = TienNo - TienDangCheck;

                        //Gán ngược lại
                        lblTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNop);
                        lblTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNo);
                    }
                    else
                    {
                        //Lấy ra tiền nộp và tiền nợ ở phí công ty
                        Label lblTienNop = (Label)grvPhiCongTy.Rows[j].Cells[3].FindControl("TienNop");
                        Label lblTienNo = (Label)grvPhiCongTy.Rows[j].Cells[4].FindControl("TienNo");

                        //Convert sang kiểu double
                        double TienNop = Convert.ToDouble(strBoChamPhay(lblTienNop.Text));
                        double TienNo = Convert.ToDouble(strBoChamPhay(lblTienNo.Text));

                        TienNop = TienNop - TienDangCheck;
                        TienNo = TienNo + TienDangCheck;

                        //Gán ngược lại
                        lblTienNop.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNop);
                        lblTienNo.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", TienNo);
                    }
                    break;
                }
            }
        }

        grvPhiCongTy_OnDataBound(null, null);
    }

    protected string strBoChamPhay(string p)
    {
        int leng = p.Length;
        if (p.LastIndexOf(',') != -1)
            leng = p.LastIndexOf(',');
        string strOut = p.Substring(0, leng);
        strOut = strOut.Replace(".", string.Empty);
        return strOut;
    }

    protected void btnXoaGrid_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        GridViewRow row = (GridViewRow)btn.NamingContainer;
        int i = Convert.ToInt32(row.RowIndex);

        CheckBox cbx = (CheckBox)thanhtoanphicanhan_grv.Rows[i].FindControl("chkEmpCaNhan");
        if (cbx.Checked == true)
        {
            cbx.Checked = false;
            chkEmpCaNhan_CheckedChanged(cbx, null);
        }
        row.Visible = false;
        vDanhSoGridCaNhan();
    }

    private void vDanhSoGridCaNhan()
    {
        int stt = 0;
        for (int j = 0; j < thanhtoanphicanhan_grv.Rows.Count; j++)
        {
            GridViewRow rowtemp = thanhtoanphicanhan_grv.Rows[j];
            Label STT = (Label)rowtemp.Cells[1].FindControl("STT");
            if (STT != null)
            {
                if (rowtemp.Visible == true)
                {
                    stt++;
                    STT.Text = stt.ToString();
                }
            }
        }
    }

    protected void btnxoa_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in thanhtoanphicanhan_grv.Rows)
        {
            if (row.Visible)
            {
                CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkEmpCaNhan");
                if (ChkBoxRows.Checked == true)
                {
                    ChkBoxRows.Checked = false;
                    chkEmpCaNhan_CheckedChanged(ChkBoxRows, null);
                    row.Visible = false;
                }
            }
        }
        vDanhSoGridCaNhan();
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        List<string> lstIdChon = new List<string>();
        if (Session["ThanhToan_IdCaNhanChon"] != null)
        {
            lstIdChon = (List<string>)Session["ThanhToan_IdCaNhanChon"];
        }
        for(int i = 0; i <thanhtoanphicanhan_grv.Rows.Count; i++)
        {
            GridViewRow row = thanhtoanphicanhan_grv.Rows[i];
            if (row.Visible == false)
                if (lstIdChon.Contains(thanhtoanphicanhan_grv.DataKeys[i].Values[3].ToString()))
                    row.Visible = true;
        }
        vDanhSoGridCaNhan();
    }

    protected void btnTestLuu_OnClick(object sender, EventArgs e)
    {
        v_Luu();
    }

    protected void btnTestSua_Click(object sender, EventArgs e)
    {
        v_Sua();
    }

    protected void drNamThanhToan_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["NamThanhToan"] = drNamThanhToan.SelectedValue;
        RefeshGrvPhiTapThe(drNamThanhToan.SelectedValue);
    }
}