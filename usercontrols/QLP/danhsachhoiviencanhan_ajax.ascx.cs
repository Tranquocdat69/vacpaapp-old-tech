﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_danhsachhoiviencanhan_ajax : System.Web.UI.UserControl
{
    Commons cm = new Commons();

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = @"SELECT distinct HoiVienCaNhanID, MaHoiVienCaNhan, HoDem + ' ' + Ten as HoTen, SoChungChiKTV, NgayCapChungChiKTV, DonViCongTac from tblHoiVienCaNhan WHERE HoiVienCaNhanID = " + Request.QueryString["id"];
        
        DataSet ds = new DataSet();
        ds = DataAccess.RunCMDGetDataSet(sql);

        
        // Các biến trả lại sau khi gọi AJAX

        try
        {
            cm.write_ajaxvar("HoiVienCaNhanID", ds.Tables[0].Rows[0]["HoiVienCaNhanID"].ToString());
            cm.write_ajaxvar("MaHoiVienCaNhan", ds.Tables[0].Rows[0]["MaHoiVienCaNhan"].ToString());
            cm.write_ajaxvar("HoTen", ds.Tables[0].Rows[0]["HoTen"].ToString());
            cm.write_ajaxvar("SoChungChiKTV", ds.Tables[0].Rows[0]["SoChungChiKTV"].ToString());
            cm.write_ajaxvar("NgayCapChungChiKTV", ds.Tables[0].Rows[0]["NgayCapChungChiKTV"].ToString());
            cm.write_ajaxvar("DonViCongTac", ds.Tables[0].Rows[0]["DonViCongTac"].ToString());

        }
        catch (Exception Ex)
        {

        }

    }


}
