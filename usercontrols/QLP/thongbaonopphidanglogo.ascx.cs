﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_thongbaonopphidanglogo : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dtb = new DataTable();

    public string quyen = "ThongBaoNopPhiDangLogo";

    public string tenchucnang = "Thông báo nộp phí đăng logo";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";
    public int iTinhTrang = 0;

    public string id_congty = "HoiVienTapTheId";
    public string ma_tapthe = "MaHoiVienTapThe";
    public string stt_congty = "STT";
    public string ten_congty = "TenDoanhNghiep";
    public string tenviettat_congty = "TenVietTat";
    public string email_tapthe = "Email";
    public string hoiphiphainopnamnay_congty = "PhiDangLogoPhaiNopNamNay";
    public string hoiphiconnonamtruoc_congty = "PhiDangLogoConNoNamTruoc";
    public string tonghoiphiphainop_congty = "TongPhiDangLogoPhaiNop";
    public string tonghoiphidanop_congty = "TongPhiDangLogoDaNop";
    public string tonghoiphiconphainop_congty = "TongPhiDangLogoConPhaiNop";

    public int iPhiID;
    public string PhiLogoID = "";
    public string strThoiHan = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            if (string.IsNullOrEmpty(Request.QueryString["thongbao"]))
            {
                LoadThongTin();
            }
            else if (Request.QueryString["thongbao"] == "thanhcong")
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script>");
                sb.Append(@"jQuery(document).ready(function () {");
                sb.Append(@"  thongbaothanhcong();}) ");
                sb.Append(@"</script>");
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
                LoadThongTin();
            }
            else if (Request.QueryString["thongbao"] == "loi")
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script>");
                sb.Append(@"jQuery(document).ready(function () {");
                sb.Append(@"  thongbaoloi();}) ");
                sb.Append(@"</script>");
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
                LoadThongTin();
            }
        }
        catch
        { }
    }

    protected void LoadThongTin()
    {
        try
        {

            grvthongbaophi.Columns[1].HeaderText = "STT";
            grvthongbaophi.Columns[1].SortExpression = stt_congty;
            grvthongbaophi.Columns[2].HeaderText = "ID";
            grvthongbaophi.Columns[2].SortExpression = ma_tapthe;
            grvthongbaophi.Columns[3].HeaderText = "Tên công ty";
            grvthongbaophi.Columns[3].SortExpression = ten_congty;
            grvthongbaophi.Columns[4].HeaderText = "Tên viết tắt";
            grvthongbaophi.Columns[4].SortExpression = tenviettat_congty;
            grvthongbaophi.Columns[5].HeaderText = "Phí đăng logo phải nộp năm nay";
            grvthongbaophi.Columns[5].SortExpression = hoiphiphainopnamnay_congty;
            grvthongbaophi.Columns[6].HeaderText = "Phí đăng logo còn nợ năm trước";
            grvthongbaophi.Columns[6].SortExpression = hoiphiconnonamtruoc_congty;
            grvthongbaophi.Columns[7].HeaderText = "Tổng phí đăng logo phải nộp";
            grvthongbaophi.Columns[7].SortExpression = tonghoiphiphainop_congty;
            grvthongbaophi.Columns[8].HeaderText = "Tổng phí đăng logo đã nộp";
            grvthongbaophi.Columns[8].SortExpression = tonghoiphidanop_congty;
            grvthongbaophi.Columns[9].HeaderText = "Tổng phí đăng logo còn phải nộp";
            grvthongbaophi.Columns[9].SortExpression = tonghoiphiconphainop_congty;

            int i;
            Commons cm = new Commons();

            SqlCommand sql = new SqlCommand();

            //Lấy phí
           // sql.CommandText = "SELECT TOP 1 PhiID, ThoiHanNgay, ThoiHanThang from tblTTDMPhiDangLogo where TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND ((CASE WHEN NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            sql.CommandText = "SELECT PhiID, ThoiHanNgay, ThoiHanThang from tblTTDMPhiDangLogo where TinhTrangID = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " AND NgayApDung <=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND ((CASE WHEN NgayHetHieuLuc IS NULL THEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ELSE NgayHetHieuLuc END) >=  '" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
            DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            //iPhiID = 0;
            //if (dtbTemp.Rows.Count > 0)
            //{
            //    iPhiID = Convert.ToInt32(dtbTemp.Rows[0][0]);
            //    PhiLogoID = iPhiID.ToString();
            //    string Ngay = dtbTemp.Rows[0]["ThoiHanNgay"].ToString();
            //    if (string.IsNullOrEmpty(Ngay))
            //        Ngay = DateTime.Now.Day.ToString();
            //    if (Ngay.Length == 1)
            //        Ngay = "0" + Ngay;

            //    string Thang = dtbTemp.Rows[0]["ThoiHanThang"].ToString();
            //    if (string.IsNullOrEmpty(Thang))
            //        Thang = DateTime.Now.Month.ToString();
            //    if (Thang.Length == 1)
            //        Thang = "0" + Thang;
            //    strThoiHan = Ngay + "/" + Thang + "/" + DateTime.Now.Year.ToString();
            //}
            //else
            //    strThoiHan = DateTime.Now.ToString("dd/MM/yyyy");
            if (dtbTemp.Rows.Count > 0)
            {
                foreach (DataRow dtr in dtbTemp.Rows)
                {
                    iPhiID = Convert.ToInt32(dtr[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiDangLogo]" + iPhiID + "," +
                                      (int) EnumVACPA.LoaiHoiVien.HoiVienTapThe + "," +
                                      (int) EnumVACPA.LoaiPhi.PhiDangLogo +
                                      ",N'%" + Request.Form["TimKiem"] + "%', ''," +
                                      (int) EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                    sql.CommandText = @"exec [dbo].[proc_ThongBaoPhiDangLogo]" + iPhiID + "," +
                                      (int) EnumVACPA.LoaiHoiVien.CongTyKiemToan + "," +
                                      (int) EnumVACPA.LoaiPhi.PhiDangLogo +
                                      ",N'%" + Request.Form["TimKiem"] + "%', ''," +
                                      (int) EnumVACPA.TinhTrang.DaPheDuyet;
                    dtb.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
                }
            }
            //if (dtb.Rows.Count == 0)
            //{
            //    PagerCaNhan.Enabled = false;
            //    return;
            //}
            //else
            //    PagerCaNhan.Enabled = true;
            DataView dv = dtb.DefaultView;

            if (ViewState["sortexpression"] != null)
                dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
            else
                dv.Sort = "STT ASC";
            //dtb = dv.ToTable();
            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = 99999;

            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                ListItem a = new ListItem((i + 1).ToString(), i.ToString());
                if (!PagerCaNhan.Items.Contains(a))
                    PagerCaNhan.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientaicanhan"]))
            {
                //Nếu trang hiện tại nhỏ hơn số trang của Page => chuyển tới trang đầu tiên
                if (Convert.ToInt32(Request.Form["tranghientaicanhan"]) <= objPds.PageCount)
                {
                    objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientaicanhan"]);
                    PagerCaNhan.SelectedIndex = Convert.ToInt32(Request.Form["tranghientaicanhan"]);
                }
                else
                {
                    objPds.CurrentPageIndex = 0;
                    PagerCaNhan.SelectedIndex = 0;
                }
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = quyen + "CaNhan" + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }

            grvthongbaophi.DataSource = dtb;
            grvthongbaophi.DataBind();
            //}

            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            // ds = null;
            dtb = null;

        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_guiemail' href='#none' class='btn btn-rounded' onclick='guiemail();'><i class='iconfa-envelope'></i> Gửi email</a>
            <a id='btn_ketxuat' href='#none' class='btn btn-rounded' onclick='export_excel();'><i class='iconfa-external-link'></i> Kết xuất</a>";
        output_html += "</div>";
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void LoadThoiGian()
    {
        //string output_html = @"$(function () { $('#ThoiHanNopPhi').datepicker('setDate', '" + strThoiHan + "') });";
        //System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("GUIEMAIL|"))
        {
            Response.Write("$('#btn_guiemail').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }
}