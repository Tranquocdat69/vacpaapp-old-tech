﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_thanhtoanphicanhan : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    public DataTable dtbPhiCaNhanChiTiet;
    public DataTable dtbPhiCaNhan;

    public DataSet ds = new DataSet();

    public string quyen = "ThanhToanPhiCaNhan";

    public string tenchucnang = "Thanh toán phí cá nhân";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";
    public int iTinhTrang = 0;
    string id = "";
    public string truongphiid = "PhiID";
    public string truonghoten = "HoTen";
    public string truongsoccktv = "SoChungChiKTV";
    public string truongdonvicongtac = "DonViCongTac";
    //public string truongsophiphainop = "";
    //public string truongsophidanop = "";
    public string truongsophiconno = "SoPhiConNo";

    protected string truonghoivienid = "HoiVienID";
    protected string truongmahoivien = "MaHoiVien";
    protected string truongstt = "STT";
    protected string truongmaphi = "MaPhi";
    protected string truongloaiphi = "TenLoaiPhi";
    protected string truongsophiphainop = "TongTien";
    protected string truongsophidanop = "TienNop";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            ////Hiển thị tổng giá tiền ở cuối trang và nơi đặt nút thanh toán
            //String return_url = "http://118.70.13.63:85/admin.aspx?page=thanhtoanphicanhan";// Địa chỉ trả về 
            //String transaction_info = "Giao dịch cuối cùng";//Thông tin giao dịch
            //String order_code = "MH370";//Mã giỏ hàng 
            //String receiver = "fumivina2311@yahoo.com";//Tài khoản nhận tiền 
            ////String receiver = "hotboy_30_10_87@yahoo.com";//Tài khoản nhận tiền 
            //// String price = ShoppingCart.Instance.GetSubTotal().ToString();//Lấy giá của giỏ hàng
            //String price = "2000";//Lấy giá của giỏ hàng
            //NL_Checkout nl = new NL_Checkout();
            //String url;
            //url = nl.buildCheckoutUrl(return_url, receiver, transaction_info, order_code, price);
            //ImageButton imgBtn = new ImageButton();
            //NganLuong.ImageUrl = "https://www.nganluong.vn/data/images/buttons/11.gif";//source file ảnh
            //NganLuong.PostBackUrl = url;//Gán địa chỉ url cho nút thanh toán

            if (!Page.IsPostBack)
            {
                thanhtoanphicanhan_grv.Columns[1].HeaderText = "STT";
                thanhtoanphicanhan_grv.Columns[1].SortExpression = truongstt;
                thanhtoanphicanhan_grv.Columns[2].HeaderText = "HVCN ID";
                thanhtoanphicanhan_grv.Columns[2].SortExpression = truongmahoivien;
                thanhtoanphicanhan_grv.Columns[3].HeaderText = "Họ và tên";
                thanhtoanphicanhan_grv.Columns[3].SortExpression = truonghoten;
                thanhtoanphicanhan_grv.Columns[4].HeaderText = "Số CCKTV";
                thanhtoanphicanhan_grv.Columns[4].SortExpression = truongsoccktv;
                thanhtoanphicanhan_grv.Columns[5].HeaderText = "Đơn vị công tác";
                thanhtoanphicanhan_grv.Columns[5].SortExpression = truongdonvicongtac;
                thanhtoanphicanhan_grv.Columns[6].HeaderText = "Diễn giải";
                thanhtoanphicanhan_grv.Columns[6].SortExpression = "DienGiai";
                thanhtoanphicanhan_grv.Columns[7].HeaderText = "Số phí phải nộp";
                thanhtoanphicanhan_grv.Columns[7].SortExpression = truongsophiphainop;
                thanhtoanphicanhan_grv.Columns[8].HeaderText = "Số phí thanh toán";
                thanhtoanphicanhan_grv.Columns[8].SortExpression = truongsophidanop;
                thanhtoanphicanhan_grv.Columns[9].HeaderText = "Số phí còn nợ";
                thanhtoanphicanhan_grv.Columns[9].SortExpression = truongsophiconno;
                thanhtoanphicanhan_grv.Columns[10].HeaderText = "Thao tác";
                ////if (dtbPhiCaNhan == null)
                ////{
                //    TaoDataTableThanhToanPhiCaNhan();
                ////}
                //RefeshGrvPhiCaNhan();
                //LoadGrid();
                if (string.IsNullOrEmpty(Request.QueryString["mode"]))
                {
                    LoadLoaiPhi();
                }
                {
                    LoadView();
                }
            }
            //else
            //{
            //    RefeshGrvPhiCaNhan();
            //}

            //Thêm mới
            //Khi thêm mới thì không có request id,  mã phí          
            if (string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                //Thêm mới
            }
            else
            {
                if (Request.QueryString["mode"] == "edit") //Sửa
                {
                    //if (!string.IsNullOrEmpty(Request.Form["DienGiai"]))
                    //{
                    //    if (!Page.IsPostBack)
                    //    {
                    //        V_Sua();
                    //    }
                    //}
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["tinhtrang"]))//Các hành động từ chối, duyệt, thoái duyệt, xóa
                {
                    //Từ chối
                    if (Request.QueryString["tinhtrang"] == "tuchoi")
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangID from tblTTNopPhiCaNhan where GiaoDichId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho update
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                            sql.CommandText = @"UPDATE tblTTNopPhiCaNhan 
                            set TinhTrangId = @TinhTrang, 
                            NguoiDuyet = @NguoiDuyet, 
                            NgayDuyet = @NgayDuyet 
                            WHERE GiaoDichId = " + Request.QueryString["id"];
                            sql.Parameters.AddWithValue("@TinhTrang", EnumVACPA.TinhTrang.KhongPheDuyet);
                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value); 
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiCaNhan", "Từ chối duyệt giá trị \"" + ds.Tables[0].Rows[0][1] + "\" của danh mục TTNopPhiCaNhan");
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "xoa") //Xóa
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangID from tblTTNopPhiCaNhan where GiaoDichId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải khác ở trạng thái duyệt thì mới cho xóa
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {
                            sql.CommandText = "delete tblTTNopPhiCaNhan WHERE GiaoDichId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.CommandText = "delete tblTTNopPhiCaNhanChiTiet WHERE GiaoDichId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiCaNhan", "Xóa giá trị \"" + Request.Form["MaGiaoDich"] + "\" của danh mục TTNopPhiCaNhan");
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "duyet") //duyệt
                    {
                          SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangID from tblTTNopPhiCaNhan where GiaoDichId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái chờ duyệt thì mới cho duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.ChoDuyet)
                        {
                            sql.CommandText = @"UPDATE tblTTNopPhiCaNhan 
                            set TinhTrangId = @TinhTrang, 
                            NguoiDuyet = @NguoiDuyet, 
                            NgayDuyet = @NgayDuyet 
                            WHERE GiaoDichId = " + Request.QueryString["id"];

                            sql.Parameters.AddWithValue("@TinhTrang", EnumVACPA.TinhTrang.DaPheDuyet);
                            sql.Parameters.AddWithValue("@NgayDuyet", DateTime.Now);
                            sql.Parameters.AddWithValue("@NguoiDuyet", cm.Admin_TenDangNhap);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiCaNhan", "Duyệt giá trị \"" + Request.Form["MaGiaoDich"] + "\" của danh mục TTNopPhiCaNhan");
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                    }
                    else if (Request.QueryString["tinhtrang"] == "thoaiduyet") //Thoái duyệt
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = "SELECT TinhTrangID from tblTTNopPhiCaNhan where GiaoDichId = " + Request.QueryString["id"];
                        DataSet ds = new DataSet();
                        ds = DataAccess.RunCMDGetDataSet(sql);
                        //Kiểm tra, phải đang ở trạng thái duyệt thì mới cho thoái duyệt
                        if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {
                            sql.CommandText = @"UPDATE tblTTNopPhiCaNhan 
                            set TinhTrangId = @TinhTrang, 
                            NguoiDuyet = @NguoiDuyet, 
                            NgayDuyet = @NgayDuyet 
                            WHERE GiaoDichId = " + Request.QueryString["id"];

                            sql.Parameters.AddWithValue("@TinhTrang", EnumVACPA.TinhTrang.ThoaiDuyet);
                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiCaNhan", "Thoái duyệt giá trị \"" + Request.Form["MaGiaoDich"] + "\" vào danh mục TTNopPhiCaNhan");
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                        else
                        {
                            Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    private void V_Sua()
    {
        if (thanhtoanphicanhan_grv.Rows.Count > 0)
        {
            //Kiểm tra
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                DateTime? NgayNop = null;
                NgayNop = DateTime.ParseExact(Request.Form["NgayNop"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                DateTime NgayNhap = DateTime.Now;

                using (SqlConnection connection = new SqlConnection(DataAccess.ConnString))
                {
                    connection.Open();
                    SqlCommand sql = connection.CreateCommand();
                    SqlTransaction transaction;
                    transaction = connection.BeginTransaction();
                    sql.Connection = connection;
                    sql.Transaction = transaction;

                    try
                    {
                        string GiaoDichId = Request.QueryString["id"];
                        string MaGiaoDich = Request.Form["MaGiaoDich"].ToString();
                        sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhan_Update]", connection, transaction);
                        sql.CommandType = CommandType.StoredProcedure;

                        sql.Parameters.AddWithValue("@GiaoDichId", GiaoDichId);
                        sql.Parameters.AddWithValue("@MaGiaoDich", MaGiaoDich);
                        if (NgayNop != null)
                            sql.Parameters.AddWithValue("@NgayNop", NgayNop);
                        else
                            sql.Parameters.AddWithValue("@NgayNop", DBNull.Value);
                        sql.Parameters.AddWithValue("@DienGiai", Request.Form["DienGiai"]);

                        sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                        sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                        sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);

                        sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                        sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);

                        sql.ExecuteNonQuery();

                        List<int?> lstIdGiaoDichChiTiet = new List<int?>();
                        List<int> lstPhatSinhPhiIdInGrid = new List<int>();
                        List<int> lstPhatSinhPhiIdInDB = new List<int>();
                        List<int> lstPhatSinhPhiIdInDBNotInGrid = new List<int>();
                        //List<int> lstPhatSinhPhiIdInGridNotInDB = new List<int>();
                        SqlCommand sqltemp = new SqlCommand();
                        sqltemp.CommandText = "SELECT PhatSinhPhiID from tblTTNopPhiCaNhanChiTiet where GiaoDichId = " + Request.QueryString["id"];
                        ds = DataAccess.RunCMDGetDataSet(sqltemp);

                        //lấy danh sách các phát sinh phí Id của bảng Nộp phí cá nhân chi tiết có giao dịch ID trùng giao dịch id đang sửa
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            lstPhatSinhPhiIdInDB.Add(Convert.ToInt32(ds.Tables[0].Rows[i][0]));
                        }

                        //Lấy danh sách các phát sinh phí id đang tồn tại trong grid
                        for (int i = 0; i < thanhtoanphicanhan_grv.Rows.Count; i++)
                        {
                            CheckBox cbx = (CheckBox)thanhtoanphicanhan_grv.Rows[i].FindControl("Chon");
                            if (cbx != null)
                                if (cbx.Checked == true)
                                {
                                    int iPSP = Convert.ToInt32(thanhtoanphicanhan_grv.DataKeys[i][0]);
                                    lstPhatSinhPhiIdInGrid.Add(iPSP);
                                    if (!lstPhatSinhPhiIdInDB.Contains(iPSP))
                                    {
                                        int IDGiaoDichChiTiet = 0;
                                        string PhiId = thanhtoanphicanhan_grv.DataKeys[i][1].ToString();
                                        string LoaiPhi = thanhtoanphicanhan_grv.DataKeys[i][2].ToString();
                                        string HoiVienID = thanhtoanphicanhan_grv.DataKeys[i][3].ToString();

                                        Label a = (Label)thanhtoanphicanhan_grv.Rows[i].FindControl("TienNop");
                                        double douTienNop = 0;
                                        string TienNop = "";
                                        if (a != null)
                                        {
                                            int leng = a.Text.Length;
                                            if (a.Text.LastIndexOf(',') != -1)
                                                leng = a.Text.LastIndexOf(',');
                                            TienNop = a.Text.Substring(0, leng);
                                            TienNop = TienNop.Replace(".", string.Empty);
                                        }
                                        try
                                        {
                                            douTienNop = Convert.ToDouble(TienNop);
                                        }
                                        catch
                                        { }

                                        if (!string.IsNullOrEmpty(TienNop))
                                        {
                                            sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhanChiTiet_Update]", connection, transaction);
                                            sql.CommandType = CommandType.StoredProcedure;
                                            sql.Parameters.AddWithValue("@GiaoDichID", Request.QueryString["id"]);
                                            sql.Parameters.AddWithValue("@MaGiaoDich", Request.Form["MaGiaoDich"]);
                                            sql.Parameters.AddWithValue("@HoiVienCaNhanID", HoiVienID);
                                            sql.Parameters.AddWithValue("@PhatSinhPhiID", iPSP);
                                            sql.Parameters.AddWithValue("@PhiID", PhiId);
                                            sql.Parameters.AddWithValue("@LoaiPhi", LoaiPhi);
                                            sql.Parameters.AddWithValue("@SoTienNop", douTienNop);
                                            sql.Parameters.AddWithValue("@DoiTuongNopPhi", (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan);
                                            sql.Parameters.Add("@GiaoDichChiTietID", SqlDbType.Int);
                                            sql.Parameters["@GiaoDichChiTietID"].Direction = ParameterDirection.Output;

                                            //DataAccess.RunActionCmd(sql);
                                            sql.ExecuteNonQuery();

                                            if (sql.Parameters["@GiaoDichChiTietID"] != null)
                                            {
                                                IDGiaoDichChiTiet = (int)sql.Parameters["@GiaoDichChiTietID"].Value;
                                                lstIdGiaoDichChiTiet.Add(IDGiaoDichChiTiet);
                                            }
                                        }
                                    }
                                }
                        }

                        //Tìm những phí id nằm trong db mà không nằm trong grid thì ghi lại
                        foreach (int i in lstPhatSinhPhiIdInDB)
                        {
                            if (!lstPhatSinhPhiIdInGrid.Contains(i))
                                lstPhatSinhPhiIdInDBNotInGrid.Add(i);
                        }
                        ////Tìm những phí id nằm trong grid mà không nằm trong db thì ghi lại
                        //foreach (int i in lstPhatSinhPhiIdInGrid)
                        //{
                        //    if (!lstPhatSinhPhiIdInDB.Contains(i))
                        //        lstPhatSinhPhiIdInGridNotInDB.Add(i);
                        //}

                        //Xóa những phát sinh phí id không nằm trong grid
                        if (lstPhatSinhPhiIdInDBNotInGrid.Count > 0)
                        {
                            string result = string.Join(", ", lstPhatSinhPhiIdInDBNotInGrid);
                            sql.CommandType = CommandType.Text;
                            sql.CommandText = @"DELETE tblTTNopPhiCaNhanChiTiet where GiaoDichID = " + Request.QueryString["id"] +
                                " AND PhatSinhPhiID IN (" + result + ")";
                            sql.ExecuteNonQuery();
                        }

                        transaction.Commit();
                        sql.Connection.Close();
                        sql.Connection.Dispose();
                        sql = null;
                        cm.ghilog("TTNopPhiCaNhan", "Sửa giá trị \"" + Request.Form["MaGiaoDich"] + "\" vào danh mục TTNopPhiCaNhan");
                        foreach (int id in lstIdGiaoDichChiTiet)
                        {
                            cm.ghilog("TTNopPhiCaNhanChiTiet", "Thêm bản ghi với ID = \"" + id + "\" của danh mục TTNopPhiCaNhanChiTiet");
                        }
                        foreach (int id in lstPhatSinhPhiIdInDBNotInGrid)
                        {
                            cm.ghilog("TTNopPhiCaNhanChiTiet", "Xóa bản ghi với Mã giao dịch = \"" + Request.Form["MaGiaoDich"] + "\", PhatSinhPhiID = \"" + id + "\" của danh mục TTNopPhiCaNhanChiTiet");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                    }
                }

                Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
            }
        }
    }

    public void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_luu' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
                    <a id='btn_sua' href='#none' class='btn btn-rounded' onclick='sua();'><i class='iconfa-plus-sign'></i> Lưu</a>
                    <a id='btn_xoa' href='#none' class='btn btn-rounded' onclick='xoa(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-sign'></i> Xóa</a>
                    <a id='btn_duyet' href='#none' class='btn btn-rounded' onclick='duyet(" + Request.QueryString["id"] + @");'><i class='iconfa-ok'></i> Duyệt</a>
                    <a id='btn_tuchoi' href='#none' class='btn btn-rounded' onclick='tuchoi(" + Request.QueryString["id"] + @");'><i class='iconfa-remove'></i> Từ chối</a>
                    <a id='btn_thoaiduyet' href='#none' class='btn btn-rounded' onclick='thoaiduyet(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Thoái duyệt</a>
                    <a id='btn_guiemail' href='#none' class='btn btn-rounded' onclick='guiemail(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Gửi email</a>";
        output_html += "</div>";
          //<a id='btn_ketxuat' href='#none' class='btn btn-rounded' onclick='ketxuat(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Kết xuất</a>
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void AnNut()
    {
        Commons cm = new Commons();

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("DUYET|"))
        {
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("TUCHOI|"))
        {
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("THOAIDUYET|"))
        {
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("GUIEMAIL|"))
        {
            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();" + System.Environment.NewLine);
        }
    }

    protected void LoadThongTin()
    {
        //Xem hoặc sửa
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
            if (Request.QueryString["mode"] == "view" || Request.QueryString["mode"] == "edit") //Xem hoặc sửa
            {
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select GiaoDichID, MaGiaoDich, DienGiai, NgayNop, "
                    + " TinhTrangID, TenTrangThai "
                    + " from tblTTNopPhiCaNhan inner join tblDMTrangThai on tblTTNopPhiCaNhan.TinhTrangID = tblDMTrangThai.TrangThaiID "
                + " where GiaoDichID = " + Convert.ToInt16(Request.QueryString["id"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                dt = ds.Tables[0];
                int iTinhTrangBanGhi = 0;
                string strTinhTrangBanGhi = "";
                //string NgayNop = "";
                if (dt.Rows.Count > 0)
                {
                    Response.Write("$('#MaGiaoDich').val('" + dt.Rows[0]["MaGiaoDich"] + "');" + System.Environment.NewLine);
                    if (dt.Rows[0]["NgayNop"] != DBNull.Value)
                        //NgayNop = Convert.ToDateTime(dt.Rows[0]["NgayNop"]).ToString("dd/MM/yyyy");
                        Response.Write("$('#NgayNop').val('" + Convert.ToDateTime(dt.Rows[0]["NgayNop"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                    Response.Write("$('#DienGiai').val('" + dt.Rows[0]["DienGiai"] + "');" + System.Environment.NewLine);
                   
                    iTinhTrangBanGhi = Convert.ToInt32(dt.Rows[0]["TinhTrangID"]);
                    strTinhTrangBanGhi = dt.Rows[0]["TenTrangThai"].ToString();

                }

                //if (Request.QueryString["mode"] == "3")
                //{
                //}
                //            else if (Request.QueryString["mode"] == "5")
                //            {
                //                string a = @"
                //            $(function () {
                //$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
                //$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + strTinhTrangHieuLuc + @"</i>')
                //});";
                //                Response.Write(a + System.Environment.NewLine);
                //            }


                if (Request.QueryString["mode"] == "view")
                {
                    Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_them').remove();" + System.Environment.NewLine);
                    Response.Write("$('#btn_xoa_chitiet').remove();" + System.Environment.NewLine);
                    Response.Write("$('#form_thanhtoanphicanhan input,select,textarea').attr('disabled', true);" + System.Environment.NewLine);

                    //Response.Write("document.getElementById('<%= btnXoaGrid.ClientID %>').disabled=true;" + System.Environment.NewLine);
                    Response.Write("$('#btnXoaGrid').attr('disabled', true);" + System.Environment.NewLine);
                    if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ChoDuyet)
                    {
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                    }
                }

                    //Nếu đang ở chế độ sửa
                else if (Request.QueryString["mode"] == "edit")
                {
                    //Nếu tình trạng bản ghi khác với chờ duyệt => không được sửa, hiển thị theo tình trạng
                    if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.ChoDuyet)
                    {
                        Response.Write("$('#form_thanhtoanphicanhan input,select,textarea').attr('disabled', true);");

                        Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);

                        //Nếu tình trạng là đã phê duyệt thì sẽ không còn các nút Duyệt/Từ chối/Xóa
                        if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {

                            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        }
                        //Nếu tình trạng là không phê duyệt hoặc đã thoái duyệt thì sẽ không còn các nút Duyệt/Từ chối/Thoái duyệt
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                        {
                            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                        }
                    }
                    //Nếu tình trạng bản ghi là chờ duyệt => có thể sửa. Chỉ hiện nút Lưu, ẩn các nút Duyệt/Từ chối/Thoái Duyệt/Xóa
                    else
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);

                        //Có thể sửa Ngày nộp
                        string output_html = @"$(function () {  $('#NgayNop').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
yearRange: '-75:+25',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
                        Response.Write(output_html + System.Environment.NewLine);

                        //Response.Write("$('#NgayNop').val('" + NgayNop + "');" +
                        //               System.Environment.NewLine);
                    }
                }
            }
            //Nếu không phải 2 chế độ xem hoặc sửa
            else
            {
                Response.Write("$('#form_thanhtoanphicanhan input,select,textarea').attr('disabled', true);");
                Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
            }
        }
        //Khi hiển thị các thông tin thêm mới
        else
        {
            string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: Chờ duyệt </i>')
});";
            Response.Write(a + System.Environment.NewLine);

            string output_html = @"$(function () {  $('#NgayNop').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
yearRange: '-75:+25',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = "$(function () {$('#NgayNop').datepicker('setDate', new Date())});";
            Response.Write(output_html + System.Environment.NewLine);

            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
        }
    }

    protected void load_data()
    {
        try
        {
            //if (thanhtoanphicanhan_grv.DataSource != null)
            //    dtbPhiCaNhan = (DataTable)thanhtoanphicanhan_grv.DataSource;
            //DataRow dtrTemp = dtbPhiCaNhan.NewRow();
            // dtrTemp[truonghoivienid] = "1";
            // dtrTemp[truongstt] = "1";
            // dtrTemp[truongmahoivien] = "ABC123";
            // dtrTemp[truonghoten] = "ABC";
            // dtrTemp[truongsoccktv] = "123";
            // dtrTemp["ngaycapchungchiktv"] = "23/11/1987";
            // dtrTemp[truongdonvicongtac] = "HiPT";
            // dtrTemp[truongsophiphainop] = "0";
            // dtrTemp[truongsophidanop] = "1000000";
            // dtrTemp[truongsophiconno] = "-1000000";
            // dtbPhiCaNhan.Rows.Add(dtrTemp);

            // thanhtoanphicanhan_grv.DataSource = dtbPhiCaNhan;
            // thanhtoanphicanhan_grv.DataBind();


            RefeshGrvPhiCaNhan();





            //int i;
            //Commons cm = new Commons();

            //SqlCommand sql = new SqlCommand();
            //sql.CommandText = @"SELECT distinct PhiID, 1 as LoaiPhi, MaPhi, TenPhi, MucPhi, NgayApDung, NgayHetHieuLuc, TinhTrangID, TenTrangThai, NgayNhap FROM tblTTDMPhiHoiVien A inner join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            //    sql.CommandText += " AND A.TenPhi like N'%" + Request.Form["timkiem_Ten"] + "%'";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
            //    sql.CommandText += " AND A.MaPhi like N'%" + Request.Form["timkiem_Ma"] + "%'";
            //DataSet ds = new DataSet();
            //ds = DataAccess.RunCMDGetDataSet(sql);

            //dt = ds.Tables[0];


            //if (dt.Rows.Count == 0)
            //{
            //    Pager.Enabled = false;
            //    return;
            //}
            //else
            //    Pager.Enabled = true;
            //DataView dv = dt.DefaultView;

            //if (ViewState["sortexpression"] != null)
            //    dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
            //else
            //    dv.Sort = "NgayNop DESC";
            ////dtb = dv.ToTable();
            //// Phân trang
            //PagedDataSource objPds = new PagedDataSource();
            //objPds.DataSource = dv;
            //objPds.AllowPaging = true;
            //objPds.PageSize = 10;

            //// danh sách trang
            //for (i = 0; i < objPds.PageCount; i++)
            //{
            //    ListItem a = new ListItem((i + 1).ToString(), i.ToString());
            //    if (!Pager.Items.Contains(a))
            //        Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            //}
            //// Chuyển trang
            //if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            //{
            //    string a = Request.Form["tranghientai"];
            //    objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            //    Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            //}

            //// kết xuất danh sách ra excel
            //if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            //{
            //    if (Request.Form["ketxuat"] == "1")
            //    {
            //        Response.Clear();
            //        Response.Buffer = true;
            //        Response.ClearContent();
            //        Response.ClearHeaders();

            //        string FileName = quyen + DateTime.Now + ".xls";
            //        StringWriter strwritter = new StringWriter();
            //        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //        Response.ContentType = "application/vnd.ms-excel";
            //        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            //        Response.ContentEncoding = System.Text.Encoding.UTF8;
            //        Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

            //        objPds.AllowPaging = false;

            //        GridView exportgrid = new GridView();

            //        exportgrid.DataSource = objPds;
            //        exportgrid.DataBind();
            //        exportgrid.RenderControl(htmltextwrtter);
            //        exportgrid.Dispose();

            //        Response.Write(strwritter.ToString());
            //        Response.End();
            //    }
            //}

            //thanhtoanphicanhan_grv.DataSource = objPds;

            //thanhtoanphicanhan_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            //ds = null;
            //dt = null;


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void thanhtoanphicanhan_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        thanhtoanphicanhan_grv.PageIndex = e.NewPageIndex;
        thanhtoanphicanhan_grv.DataBind();
    }

    protected void thanhtoanphicanhan_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        load_data();
    }

    //private void load_datagrid()
    //{
    //    throw new NotImplementedException();
    //}

    //MinhLP
    //20150202
    //Khởi tạo dtbPhiCaNhan
    protected void TaoDataTableThanhToanPhiCaNhan()
    {
        dtbPhiCaNhan = new DataTable();
        //dtbPhiCaNhan.Columns.Add(truonghoivienid);
        //dtbPhiCaNhan.Columns.Add(truongstt);
        //dtbPhiCaNhan.Columns.Add(truongmahoivien);
        //dtbPhiCaNhan.Columns.Add(truonghoten);
        //dtbPhiCaNhan.Columns.Add(truongsoccktv);
        //dtbPhiCaNhan.Columns.Add("ngaycapchungchiktv");
        //dtbPhiCaNhan.Columns.Add(truongdonvicongtac);
        //dtbPhiCaNhan.Columns.Add(truongsophiphainop);
        //dtbPhiCaNhan.Columns.Add(truongsophidanop);
        //dtbPhiCaNhan.Columns.Add(truongsophiconno);
        //dtbPhiCaNhan.Columns.Add("PhiHoiVienPhaiNop");
        //dtbPhiCaNhan.Columns.Add("PhiHoiVienDaNop");
        //dtbPhiCaNhan.Columns.Add("PhiCNKTPhaiNop");
        //dtbPhiCaNhan.Columns.Add("PhiCNKTDaNop");
        //dtbPhiCaNhan.Columns.Add("PhiKSCLPhaiNop");
        //dtbPhiCaNhan.Columns.Add("PhiKSCLDaNop");
        //dtbPhiCaNhan.Columns.Add("PhiKhacPhaiNop");
        //dtbPhiCaNhan.Columns.Add("PhiKhacDaNop");
        //dtbPhiCaNhan.Columns.Add("PhatSinhPhiIDHoiVien");
        //dtbPhiCaNhan.Columns.Add("PhatSinhPhiIDCNKT");
        //dtbPhiCaNhan.Columns.Add("PhatSinhPhiIDKSCL");
        //dtbPhiCaNhan.Columns.Add("PhatSinhPhiIDKhac");
        //dtbPhiCaNhan.Columns.Add("PhiIDHoiVien");
        //dtbPhiCaNhan.Columns.Add("PhiIDCNKT");
        //dtbPhiCaNhan.Columns.Add("PhiIDKSCL");
        //dtbPhiCaNhan.Columns.Add("PhiIDKhac");

        dtbPhiCaNhan.Columns.Add("HoiVienID");
        dtbPhiCaNhan.Columns.Add("STT");
        dtbPhiCaNhan.Columns.Add("MaHoiVien");
        dtbPhiCaNhan.Columns.Add("HoTen");
        dtbPhiCaNhan.Columns.Add("SoChungChiKTV");
        dtbPhiCaNhan.Columns.Add("DonViCongTac");
        dtbPhiCaNhan.Columns.Add("DienGiai");
        dtbPhiCaNhan.Columns.Add("TongTien");
        dtbPhiCaNhan.Columns.Add("TienNop");
        dtbPhiCaNhan.Columns.Add("SoPhiConNo");

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dtbPhiCaNhan.Columns["HoiVienID"];
        dtbPhiCaNhan.PrimaryKey = columns;

        Session["dtbPhiCaNhan"] = dtbPhiCaNhan;
    }

    //MinhLP
    //20150202
    //Khởi tạo dtbPhiCaNhanChiTiet
    //protected void TaoDataTableThanhToanPhiCaNhanChiTiet()
    //{
    //    dtbPhiCaNhanChiTiet = new DataTable();
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongstt);
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongmaphi);
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongloaiphi);
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongsophiphainop);
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongsophidanop);

    //    DataColumn[] columns = new DataColumn[1];
    //    columns[0] = dtbPhiCaNhanChiTiet.Columns[truongmaphi];
    //    dtbPhiCaNhanChiTiet.PrimaryKey = columns;
    //}

    //MinhLP
    //20150202
    //Refesh giá trị gridCaNhan
    protected void RefeshGrvPhiCaNhan()
    {
        ReLoadDTB();
        //DataView dv = dtbPhiCaNhan.DefaultView;
        DataView dv = dtbPhiCaNhan.DefaultView;
        if (ViewState["sortexpression"] != null)
            dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
        else
            dv.Sort = "HoiVienId DESC";

        thanhtoanphicanhan_grv.DataSource = dv;
        thanhtoanphicanhan_grv.DataBind();

    }

    protected void ReLoadDTB()
    {
        if (Session["dtbPhiCaNhan"] != null)
            dtbPhiCaNhan = (DataTable)Session["dtbPhiCaNhan"];
        else
            Session["dtbPhiCaNhan"] = dtbPhiCaNhan;
    }

    //MinhLP
    //20150202
    //Refesh giá trị gridCaNhan
    protected void RefeshGrvPhiCaNhanChiTiet()
    {
        //if (ViewState["dtbDanhSachVLVPP"] != null)
        //    dtbDanhSachVLVPP = (DataTable)ViewState["dtbDanhSachVLVPP"];
        //else
        //    ViewState["dtbDanhSachVLVPP"] = dtbDanhSachVLVPP;
        //gvDanhSachVLVPP.DataSource = dtbDanhSachVLVPP;
        //gvDanhSachVLVPP.DataBind();
    }

    //MinhLP
    //20150202
    //Test
    protected void Test_Click(object sender, EventArgs e)
    {
        //ReLoadDTB();
        //string id = "";
        //string stt = "";
        //string mahoivien = "";
        //string hoten = "";
        //string soccktv = "";
        //string ngaycapccktv = "";
        //string donvicongtac = "";
        //string sophiphainop = "";
        //string sophidanop = "";
        //string sophiconno = "";
        //string phihoivienphainop = "";
        //string phihoiviendanop = "";
        //string phicnktphainop = "";
        //string phicnktdanop = "";
        //string phiksclphainop = "";
        //string phikscldanop = "";
        //string phikhacphainop = "";
        //string phikhacdanop = "";
        //string phatsinhphiidhoivien = "";
        //string phatsinhphiidcnkt = "";
        //string phatsinhphiidkscl = "";
        //string phatsinhphiidkhac = "";
        //string phiidhoivien = "";
        //string phiidcnkt = "";
        //string phiidkscl = "";
        //string phiidkhac = "";

        //if (Session["PCNCT_HoiVienID"] != null)
        //    id = Session["PCNCT_HoiVienID"].ToString();
        //if (Session["PCNCT_MaHoiVien"] != null)
        //    mahoivien = Session["PCNCT_MaHoiVien"].ToString();
        //if (Session["PCNCT_TenHoiVien"] != null)
        //    hoten = Session["PCNCT_TenHoiVien"].ToString();
        //if (Session["PCNCT_SoCCKTV"] != null)
        //    soccktv = Session["PCNCT_SoCCKTV"].ToString();
        //if (Session["PCNCT_DonViCongTac"] != null)
        //    donvicongtac = Session["PCNCT_DonViCongTac"].ToString();
        //if (Session["PCNCT_NgayCapCCKTV"] != null)
        //    try
        //    {
        //        DateTime NgayCap = new DateTime();
        //        NgayCap = Convert.ToDateTime(Session["PCNCT_NgayCapCCKTV"]);
        //        ngaycapccktv = NgayCap.ToString("dd/MM/yyyy");
        //    }
        //    catch
        //    { }

        //if (Session["PCNCT_SoPhiPhaiNop"] != null)
        //    sophiphainop = Session["PCNCT_SoPhiPhaiNop"].ToString();
        //if (Session["PCNCT_SoPhiDaNop"] != null)
        //    sophidanop = Session["PCNCT_SoPhiDaNop"].ToString();
        //if (Session["PCNCT_SoPhiConNo"] != null)
        //    sophiconno = Session["PCNCT_SoPhiConNo"].ToString();

        //if (Session["PCNCT_PhiHoiVienPhaiNop"] != null)
        //    phihoivienphainop = Session["PCNCT_PhiHoiVienPhaiNop"].ToString();
        //if (Session["PCNCT_PhiHoiVienDaNop"] != null)
        //    phihoiviendanop = Session["PCNCT_PhiHoiVienDaNop"].ToString();
        //if (Session["PCNCT_PhiCNKTPhaiNop"] != null)
        //    phicnktphainop = Session["PCNCT_PhiCNKTPhaiNop"].ToString();
        //if (Session["PCNCT_PhiCNKTDaNop"] != null)
        //    phicnktdanop = Session["PCNCT_PhiCNKTDaNop"].ToString();
        //if (Session["PCNCT_PhiKSCLPhaiNop"] != null)
        //    phiksclphainop = Session["PCNCT_PhiKSCLPhaiNop"].ToString();
        //if (Session["PCNCT_PhiKSCLDaNop"] != null)
        //    phikscldanop = Session["PCNCT_PhiKSCLDaNop"].ToString();
        //if (Session["PCNCT_PhiKhacPhaiNop"] != null)
        //    phikhacphainop = Session["PCNCT_PhiKhacPhaiNop"].ToString();
        //if (Session["PCNCT_PhiKhacDaNop"] != null)
        //    phikhacdanop = Session["PCNCT_PhiKhacDaNop"].ToString();

        //if (Session["PCNCT_PhatSinhPhiIDPhiHoiVien"] != null)
        //    phatsinhphiidhoivien = Session["PCNCT_PhatSinhPhiIDPhiHoiVien"].ToString();
        //if (Session["PCNCT_PhatSinhPhiIDPhiCNKT"] != null)
        //    phatsinhphiidcnkt = Session["PCNCT_PhatSinhPhiIDPhiCNKT"].ToString();
        //if (Session["PCNCT_PhatSinhPhiIDPhiKSCL"] != null)
        //    phatsinhphiidkscl = Session["PCNCT_PhatSinhPhiIDPhiKSCL"].ToString();
        //if (Session["PCNCT_PhatSinhPhiIDPhiKhac"] != null)
        //    phatsinhphiidkhac = Session["PCNCT_PhatSinhPhiIDPhiKhac"].ToString();

        //if (Session["PCNCT_PhiIDPhiHoiVien"] != null)
        //    phiidhoivien = Session["PCNCT_PhiIDPhiHoiVien"].ToString();
        //if (Session["PCNCT_PhiIDPhiCNKT"] != null)
        //    phiidcnkt = Session["PCNCT_PhiIDPhiCNKT"].ToString();
        //if (Session["PCNCT_PhiIDPhiKSCL"] != null)
        //    phiidkscl = Session["PCNCT_PhiIDPhiKSCL"].ToString();
        //if (Session["PCNCT_PhiIDPhiKhac"] != null)
        //    phiidkhac = Session["PCNCT_PhiIDPhiKhac"].ToString();

        //string[] KhoaCaNhanMoi = new string[] { id };
        //if (dtbPhiCaNhan.Rows.Find(KhoaCaNhanMoi) == null)
        //{
        //    DataRow dtrTemp = dtbPhiCaNhan.NewRow();
        //    dtrTemp[truonghoivienid] = id;
        //    dtrTemp[truongstt] = dtbPhiCaNhan.Rows.Count + 1;
        //    dtrTemp[truongmahoivien] = mahoivien;
        //    dtrTemp[truonghoten] = hoten;
        //    dtrTemp[truongsoccktv] = soccktv;
        //    dtrTemp["ngaycapchungchiktv"] = ngaycapccktv;
        //    dtrTemp[truongdonvicongtac] = donvicongtac;
        //    dtrTemp[truongsophiphainop] = sophiphainop;
        //    dtrTemp[truongsophidanop] = sophidanop;
        //    dtrTemp[truongsophiconno] = sophiconno;
        //    dtrTemp["PhiHoiVienPhaiNop"] = phihoivienphainop;
        //    dtrTemp["PhiHoiVienDaNop"] = phihoiviendanop;
        //    dtrTemp["PhiCNKTPhaiNop"] = phicnktphainop;
        //    dtrTemp["PhiCNKTDaNop"] = phicnktdanop;
        //    dtrTemp["PhiKSCLPhaiNop"] = phiksclphainop;
        //    dtrTemp["PhiKSCLDaNop"] = phikscldanop;
        //    dtrTemp["PhiKhacPhaiNop"] = phikhacphainop;
        //    dtrTemp["PhiKhacDaNop"] = phikhacdanop;
        //    dtrTemp["PhatSinhPhiIDHoiVien"] = phatsinhphiidhoivien;
        //    dtrTemp["PhatSinhPhiIDCNKT"] = phatsinhphiidcnkt;
        //    dtrTemp["PhatSinhPhiIDKSCL"] = phatsinhphiidkscl;
        //    dtrTemp["PhatSinhPhiIDKhac"] = phatsinhphiidkhac;
        //    dtrTemp["PhiIDHoiVien"] = phiidhoivien;
        //    dtrTemp["PhiIDCNKT"] = phiidcnkt;
        //    dtrTemp["PhiIDKSCL"] = phiidkscl;
        //    dtrTemp["PhiIDKhac"] = phiidkhac;
        //    dtbPhiCaNhan.Rows.Add(dtrTemp);
        //}
        //else
        //{
        //    DataRow dtrTemp = dtbPhiCaNhan.Rows.Find(id);
        //    dtrTemp[truonghoivienid] = id;
        //    dtrTemp[truongstt] = dtbPhiCaNhan.Rows.Count + 1;
        //    dtrTemp[truongmahoivien] = mahoivien;
        //    dtrTemp[truonghoten] = hoten;
        //    dtrTemp[truongsoccktv] = soccktv;
        //    dtrTemp["ngaycapchungchiktv"] = ngaycapccktv;
        //    dtrTemp[truongdonvicongtac] = donvicongtac;
        //    dtrTemp[truongsophiphainop] = sophiphainop;
        //    dtrTemp[truongsophidanop] = sophidanop;
        //    dtrTemp[truongsophiconno] = sophiconno;
        //    dtrTemp["PhiHoiVienPhaiNop"] = phihoivienphainop;
        //    dtrTemp["PhiHoiVienDaNop"] = phihoiviendanop;
        //    dtrTemp["PhiCNKTPhaiNop"] = phicnktphainop;
        //    dtrTemp["PhiCNKTDaNop"] = phicnktdanop;
        //    dtrTemp["PhiKSCLPhaiNop"] = phiksclphainop;
        //    dtrTemp["PhiKSCLDaNop"] = phikscldanop;
        //    dtrTemp["PhiKhacPhaiNop"] = phikhacphainop;
        //    dtrTemp["PhiKhacDaNop"] = phikhacdanop;
        //    dtrTemp["PhatSinhPhiIDHoiVien"] = phatsinhphiidhoivien;
        //    dtrTemp["PhatSinhPhiIDCNKT"] = phatsinhphiidcnkt;
        //    dtrTemp["PhatSinhPhiIDKSCL"] = phatsinhphiidkscl;
        //    dtrTemp["PhatSinhPhiIDKhac"] = phatsinhphiidkhac;
        //    dtrTemp["PhiIDHoiVien"] = phiidhoivien;
        //    dtrTemp["PhiIDCNKT"] = phiidcnkt;
        //    dtrTemp["PhiIDKSCL"] = phiidkscl;
        //    dtrTemp["PhiIDKhac"] = phiidkhac;
        //}
        //RefeshGrvPhiCaNhan();
        LoadGrid();
    }

    private string _dtbPhiCaNhan = "dtbPhiCaNhan";

    protected void OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (sender as CheckBox);
        GridView gv = chk.NamingContainer.Parent.Parent as GridView;
        foreach (GridViewRow row in gv.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                (row.FindControl("CheckBox1") as CheckBox).Checked = chk.Checked;
            }
        }
    }

    protected void OnChildCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (sender as CheckBox);
        GridView gv = chk.NamingContainer.Parent.Parent as GridView;
        bool isAllChecked = true;
        foreach (GridViewRow row in gv.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                if (!(row.FindControl("CheckBox1") as CheckBox).Checked)
                {
                    isAllChecked = false;
                    break;
                }
            }
        }
        CheckBox chkAll = (gv.HeaderRow.FindControl("chkAll") as CheckBox);
        chkAll.Checked = isAllChecked;
    }

    public void LoadLoaiPhi()
    {
        //string output_html = "";

        DataTable dtbLoaiPhi = new DataTable();
        dtbLoaiPhi.Columns.Add("ID");
        dtbLoaiPhi.Columns.Add("TEN");
        dtbLoaiPhi.Rows.Add((int)EnumVACPA.LoaiPhi.PhiHoiVien, "Phí Hội viên");
        dtbLoaiPhi.Rows.Add((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc, "Phí cập nhật kiến thức");
        dtbLoaiPhi.Rows.Add((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong, "Phí kiểm soát chất lượng");
        dtbLoaiPhi.Rows.Add((int)EnumVACPA.LoaiPhi.PhiKhac, "Phí khác");

        LoaiPhi.DataSource = dtbLoaiPhi;
        LoaiPhi.DataBind();
        LoaiPhi_SelectedIndexChanged(null, null);
        //    int i = 0;
        //foreach (DataRow dtr in dtbLoaiPhi.Rows)
        //    {
        //        i++;
        //        if (i == 1)
        //            output_html += @"<option selected=""selected"" value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
        //        else
        //            output_html += @"<option value='" + dtr[0].ToString() + "'>" + dtr[1].ToString() + "</option>";
        //    }
        //System.Web.HttpContext.Current.Response.Write(output_html);
    }

    private void LoadView()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            DataTable dtbTemp = new DataTable();
            dtbTemp.Columns.Add("Chon");
            dtbTemp.Columns.Add("STT");
            dtbTemp.Columns.Add("MaHoiVien");
            dtbTemp.Columns.Add("HoTen");
            dtbTemp.Columns.Add("SoChungChiKTV");
            dtbTemp.Columns.Add("DonViCongTac");
            dtbTemp.Columns.Add("DienGiai");
            dtbTemp.Columns.Add("TongTien", typeof(decimal));
            dtbTemp.Columns.Add("TienNop", typeof(decimal));
            dtbTemp.Columns.Add("SoPhiConNo", typeof(decimal));
            dtbTemp.Columns.Add("PhatSinhPhiId");
            dtbTemp.Columns.Add("PhiId");
            dtbTemp.Columns.Add("LoaiPhi");
            dtbTemp.Columns.Add("HoiVienID");

            SqlCommand sql = new SqlCommand();
//            sql.CommandText = @"SELECT TOP 1 LOAIPHI FROM TBLTTNopPhiCaNhanChiTiet 
//        where DoiTuongNopPhi = "+ (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan +" and GiaoDichID = " + Request.QueryString["id"];

//            string strLoaiPhi = "";
//            DataSet ds = new DataSet();
//            ds = DataAccess.RunCMDGetDataSet(sql);
//            if (ds.Tables[0].Rows.Count > 0)
//                strLoaiPhi = ds.Tables[0].Rows[0][0].ToString();
//            if (!string.IsNullOrEmpty(strLoaiPhi))
//            {
//                DataTable dtbLoaiPhi = new DataTable();
//                dtbLoaiPhi.Columns.Add("ID");
//                dtbLoaiPhi.Columns.Add("TEN");
//                if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString())
//                {
//                    dtbLoaiPhi.Rows.Add((int)EnumVACPA.LoaiPhi.PhiHoiVien, "Phí Hội viên");
//                    //sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiHoiVien_All]" + strLoaiPhi + ", " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
//                    //ds = new DataSet();
//                    //dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
//                }
//                else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString())
//                {
//                    dtbLoaiPhi.Rows.Add((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc, "Phí cập nhật kiến thức");
//                    //sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiCNKT_All]" + strLoaiPhi + ", " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
//                    //ds = new DataSet();
//                    //dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
//                }
//                else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString())
//                {
//                    dtbLoaiPhi.Rows.Add((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong, "Phí kiểm soát chất lượng");
//                    //sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKSCL_All]" + strLoaiPhi + ", " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
//                    //ds = new DataSet();
//                    //dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
//                }
//                else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString())
//                {
//                    dtbLoaiPhi.Rows.Add((int)EnumVACPA.LoaiPhi.PhiKhac, "Phí khác");
//                    //sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKhac_All]" + strLoaiPhi + ", " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
//                    //ds = new DataSet();
//                    //dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
//                }

//                LoaiPhi.DataSource = dtbLoaiPhi;
//                LoaiPhi.DataBind();

                sql.CommandText = @"select ROW_NUMBER() 
        OVER (ORDER BY HoiVienID) as STT, * from (SELECT A.HoiVienCaNhanID as HoiVienID, B.MaHoiVienCaNhan as MaHoiVien, 
                B.HoDem + ' ' + B.Ten as HoTen, B.SoChungChiKTV, B.DonViCongTac,
CASE LoaiPhi WHEN 1 THEN N'Phí hội viên' WHEN 3 THEN N'Phí khác' WHEN 5 THEN N'Phí KSCL' when 4 then (select top 1 MaLopHoc from tblCNKTLopHoc where LopHocID = (select top 1 DoiTuongPhatSinhPhiID from tblTTPhatSinhPhi where PhatSinhPhiID = A.PhatSinhPhiID))  END as DienGiai
, PhiID,
                PhatSinhPhiID, LoaiPhi, SoTienNop as TongTien, SoTienNop as TienNop, 0 as SoPhiConNo
                from tblTTNopPhiCaNhanChiTiet A left join tblHoiVienCaNhan B on A.HoiVienCaNhanID = B.HoiVienCaNhanID
                where A.GiaoDichID = " + Request.QueryString["id"] + "" +
                                  " and A.DoiTuongNopPhi = " + (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan + ") as TEMP";
                ds = new DataSet();
                dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];

                thanhtoanphicanhan_grv.DataSource = dtbTemp;
                thanhtoanphicanhan_grv.DataBind();
                vDanhSoGridCaNhan();
            //}
        }
    }

    public void LoadGrid()
    {
        string strLoaiPhi = LoaiPhi.SelectedValue;
        DataTable dtbTemp = new DataTable();
        dtbTemp.Columns.Add("Chon");
        dtbTemp.Columns.Add("STT");
        dtbTemp.Columns.Add("MaHoiVien");
        dtbTemp.Columns.Add("HoTen");
        dtbTemp.Columns.Add("SoChungChiKTV");
        dtbTemp.Columns.Add("DonViCongTac");
        dtbTemp.Columns.Add("DienGiai");
        dtbTemp.Columns.Add("TongTien", typeof(decimal));
        dtbTemp.Columns.Add("TienNop", typeof(decimal));
        dtbTemp.Columns.Add("SoPhiConNo", typeof(decimal));
        dtbTemp.Columns.Add("PhatSinhPhiId");
        dtbTemp.Columns.Add("PhiId");
        dtbTemp.Columns.Add("LoaiPhi");
        dtbTemp.Columns.Add("HoiVienID");


        if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiHoiVien).ToString())
        {
            //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

            //Session["dtbPhiCaNhan"] = dtbTemp;

            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiHoiVien_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
           // DataSet ds = new DataSet();
            dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            //sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiHoiVien_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet;
            ////DataSet ds = new DataSet();
            //dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        }
        else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc).ToString())
        {
            //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", 1000000, 1000000, "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", 1000000, 1000000, "0", "1", "1", strLoaiPhi);

            //Session["dtbPhiCaNhan"] = dtbTemp;

            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiCNKT_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
           // DataSet ds = new DataSet();
            dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiCNKT_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
            // DataSet ds = new DataSet();
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiCNKT_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
            // DataSet ds = new DataSet();
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        }
        else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong).ToString())
        {
            //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("3", "3", "3", "C", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

            //Session["dtbPhiCaNhan"] = dtbTemp;
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKSCL_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
            //DataSet ds = new DataSet();
            dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKSCL_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
            //DataSet ds = new DataSet();
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        }
        else if (strLoaiPhi == ((int)EnumVACPA.LoaiPhi.PhiKhac).ToString())
        {
            //dtbTemp.Rows.Add("1", "1", "1", "A", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("2", "2", "2", "B", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("3", "3", "3", "C", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);
            //dtbTemp.Rows.Add("4", "4", "4", "D", "0", "HiPT", "", "1000000", "1000000", "0", "1", "1", strLoaiPhi);

            //Session["dtbPhiCaNhan"] = dtbTemp;
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKhac_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.HoiVienCaNhan + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
            //DataSet ds = new DataSet();
            dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKhac_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.KiemToanVien + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
            //DataSet ds = new DataSet();
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
            sql.CommandText = @"exec [dbo].[proc_ThanhToanPhiCaNhan_PhiKhac_All]" + strLoaiPhi + ", " + (int)EnumVACPA.LoaiHoiVien.NguoiQuanTam + "," + (int)EnumVACPA.TinhTrang.DaPheDuyet + ",N'%" + txtTimKiem_ID.Text + "%',N'%" + txtTimKiem_Ten.Text + "%'";
            //DataSet ds = new DataSet();
            dtbTemp.Merge(DataAccess.RunCMDGetDataSet(sql).Tables[0]);
        }
        //SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT HoiVienID, '1' as STT, B.MaHoiVien, B.HoDem + ' ' + B.Ten as HoTen, "
        //+ "B.SoChungChiKTV, B.DonViCongTac, FROM tblDMDoiTuongNopPhi";
        //DataSet ds = new DataSet();
        //DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
     //   dtbTemp.Rows.Add(10, 10, "", "", "", "", "", 10, 10, 10, 10, 10, 10);
        Session["dtbPhiCaNhan"] = dtbTemp;

        DataView dv = dtbTemp.DefaultView;

        if (ViewState["sortexpression"] != null)
            dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
        else
            dv.Sort = "HoiVienID DESC";
        thanhtoanphicanhan_grv.DataSource = dtbTemp;
        thanhtoanphicanhan_grv.DataBind();
        vDanhSoGridCaNhan();
        //foreach (GridViewRow row in thanhtoanphicanhan_grv.Rows)
        //{
        //    CheckBox ChkBoxRows = (CheckBox)row.FindControl("Chon");
        //    ChkBoxRows.Checked = true;
        //} 
    }

    protected void btntong_OnClick(object sender, EventArgs e)
    {
        foreach (GridViewRow row in thanhtoanphicanhan_grv.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("Chon");
            if (ChkBoxRows.Checked == true)
            {
                ChkBoxRows.Checked = false;
                row.Visible = false;
            }
        }
        vDanhSoGridCaNhan();
       // v_SetPhiConNo();
    }

    private void v_SetPhiConNo()
    {
        try
        {
            for (int j = 0; j < thanhtoanphicanhan_grv.Rows.Count; j++)
            {
                string PhaiNop = "";
                string ThanhToan = "";
                string ConNo = "";
                TextBox a = (TextBox)thanhtoanphicanhan_grv.Rows[j].Cells[8].FindControl("TruongSoPhiNop");
                if (a != null)
                {
                    int leng = a.Text.Length;
                    if (a.Text.LastIndexOf(',') != -1)
                        leng = a.Text.LastIndexOf(',');
                    string tien = a.Text.Substring(0, leng);
                    ThanhToan = tien.Replace(".", string.Empty);
                }
                Label b = (Label)thanhtoanphicanhan_grv.Rows[j].Cells[7].FindControl("TruongSoPhiPhaiNop");
                int lengphainop = b.Text.Length;
                if (b.Text.LastIndexOf(',') != -1)
                    lengphainop = b.Text.LastIndexOf(',');
                string tienphainop = b.Text.Substring(0, lengphainop);
                PhaiNop = tienphainop.Replace(".", string.Empty);

                try
                {
                    double douTienNo = Convert.ToDouble(PhaiNop) - Convert.ToDouble(ThanhToan);
                    thanhtoanphicanhan_grv.Rows[j].Cells[9].Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N0}", douTienNo);
                }
                catch { }
            }
        }
        catch
        { }
    }

    protected void btnluu_Click(object sender, EventArgs e)
    {
        v_Luu();
    }

    private void v_Luu()
    {
        //Thêm mới
        if (string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            if (thanhtoanphicanhan_grv.Rows.Count > 0)
            {
                //Kiểm tra
                if (string.IsNullOrEmpty(Request.Form["MaGiaoDich"]) && !string.IsNullOrEmpty(Request.Form["NgayNop"]))
                {
                    //Chạy function tự sinh mã phí
                    string Nam = "";
                    Nam = DateTime.ParseExact(Request.Form["NgayNop"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).Year.ToString();
                    Nam = Nam.Substring(2);
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "select [dbo].[LaySoChayThanhToanPhiCaNhan] (" + Nam + ")";
                    ds = DataAccess.RunCMDGetDataSet(cmd);

                    string MaGiaoDich = ds.Tables[0].Rows[0][0].ToString();

                    DateTime? NgayNop = null;
                    NgayNop = DateTime.ParseExact(Request.Form["NgayNop"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    DateTime NgayNhap = DateTime.Now;

                    using (SqlConnection connection = new SqlConnection(DataAccess.ConnString))
                    {
                        connection.Open();
                        SqlCommand sql = connection.CreateCommand();
                        SqlTransaction transaction;
                        transaction = connection.BeginTransaction();
                        sql.Connection = connection;
                        sql.Transaction = transaction;

                        try
                        {
                            sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhan_Insert]", connection, transaction);
                            sql.CommandType = CommandType.StoredProcedure;

                            sql.Parameters.AddWithValue("@MaGiaoDich", MaGiaoDich);
                            if (NgayNop != null)
                                sql.Parameters.AddWithValue("@NgayNop", NgayNop);
                            else
                                sql.Parameters.AddWithValue("@NgayNop", DBNull.Value);
                            sql.Parameters.AddWithValue("@DienGiai", Request.Form["DienGiai"]);

                            sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                            sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                            sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);

                            sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                            sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);

                            sql.Parameters.Add("@GiaoDichID", SqlDbType.Int);
                            sql.Parameters["@GiaoDichID"].Direction = ParameterDirection.Output;

                            //DataAccess.RunActionCmd(sql);
                            sql.ExecuteNonQuery();

                            int? IDGiaoDich = null;
                            if (sql.Parameters["@GiaoDichID"] != null)
                                IDGiaoDich = (int)sql.Parameters["@GiaoDichID"].Value;

                            int? IDGiaoDichChiTiet = null;

                            //DataTable dtbKhoiTao = (DataTable)Session["dtbPhiCaNhan"];
                            List<int?> lstIdGiaoDichChiTiet = new List<int?>();
                            for (int i = 0; i < thanhtoanphicanhan_grv.Rows.Count; i++)
                            {
                                CheckBox cbx = (CheckBox)thanhtoanphicanhan_grv.Rows[i].FindControl("Chon");
                                if (cbx != null)
                                    if (cbx.Checked == true)
                                    {
                                        //string HoiVienID = dtbKhoiTao.Rows[i]["HoiVienID"].ToString();
                                        //string PhatSinhPhiId = dtbKhoiTao.Rows[i]["PhatSinhPhiId"].ToString();
                                        //string PhiId = dtbKhoiTao.Rows[i]["PhiId"].ToString();
                                        //string LoaiPhi = dtbKhoiTao.Rows[i]["LoaiPhi"].ToString();


                                        
                                        string PhatSinhPhiId = thanhtoanphicanhan_grv.DataKeys[i].Values[0].ToString();
                                        string PhiId = thanhtoanphicanhan_grv.DataKeys[i].Values[1].ToString();
                                        string LoaiPhi = thanhtoanphicanhan_grv.DataKeys[i].Values[2].ToString();
                                        string HoiVienID = thanhtoanphicanhan_grv.DataKeys[i].Values[3].ToString();

                                        Label a = (Label)thanhtoanphicanhan_grv.Rows[i].FindControl("TienNop");
                                        string TienNop = "";
                                        if (a != null)
                                        {
                                            int leng = a.Text.Length;
                                            if (a.Text.LastIndexOf(',') != -1)
                                                leng = a.Text.LastIndexOf(',');
                                            TienNop = a.Text.Substring(0, leng);
                                            TienNop = TienNop.Replace(".", string.Empty);
                                        }
                                        if (!string.IsNullOrEmpty(TienNop))
                                        {
                                            sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhanChiTiet_Insert]", connection, transaction);
                                            sql.CommandType = CommandType.StoredProcedure;
                                            sql.Parameters.AddWithValue("@GiaoDichID", IDGiaoDich);
                                            sql.Parameters.AddWithValue("@HoiVienCaNhanID", HoiVienID);
                                            sql.Parameters.AddWithValue("@PhatSinhPhiID", PhatSinhPhiId);
                                            sql.Parameters.AddWithValue("@PhiID", PhiId);
                                            sql.Parameters.AddWithValue("@LoaiPhi", LoaiPhi);
                                            sql.Parameters.AddWithValue("@SoTienNop", TienNop);
                                            sql.Parameters.AddWithValue("@DoiTuongNopPhi", (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan);
                                            sql.Parameters.Add("@GiaoDichChiTietID", SqlDbType.Int);
                                            sql.Parameters["@GiaoDichChiTietID"].Direction = ParameterDirection.Output;

                                            //DataAccess.RunActionCmd(sql);
                                            sql.ExecuteNonQuery();

                                            if (sql.Parameters["@GiaoDichChiTietID"] != null)
                                            {
                                                IDGiaoDichChiTiet = (int)sql.Parameters["@GiaoDichChiTietID"].Value;
                                                lstIdGiaoDichChiTiet.Add(IDGiaoDichChiTiet);
                                            }
                                        }
                                    }
                            }

                            transaction.Commit();
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTNopPhiCaNhan", "Thêm giá trị \"" + Request.Form["MaGiaoDich"] + "\" vào danh mục TTNopPhiCaNhan");
                            foreach (int id in lstIdGiaoDichChiTiet)
                            {
                                cm.ghilog("TTNopPhiCaNhanChiTiet", "Thêm bản ghi với ID \"" + id + "\" vào danh mục TTNopPhiCaNhanChiTiet");
                            }
                        }
                        catch
                        {
                            transaction.Rollback();
                        }
                    }

                    Response.Redirect("admin.aspx?page=thanhtoanphi_quanly", false);
                }
            }
        }
    }

    protected void LoaiPhi_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadGrid();
    }

    protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)thanhtoanphicanhan_grv.HeaderRow.FindControl("chkboxSelectAll");
        for (int i = 0; i < thanhtoanphicanhan_grv.Rows.Count; i++)
        {
            GridViewRow row = thanhtoanphicanhan_grv.Rows[i];
            if (row.Visible)
            {
                CheckBox ChkBoxRows = (CheckBox) row.FindControl("Chon");
                if (ChkBoxRows.Enabled)
                {
                    if (ChkBoxHeader.Checked == true)
                    {
                        ChkBoxRows.Checked = true;
                        Chon_CheckedChanged(ChkBoxRows, null);
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                        Chon_CheckedChanged(ChkBoxRows, null);
                    }
                }
            }
        }
    }

    protected void Chon_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox btn = (CheckBox)sender;
        GridViewRow row = (GridViewRow)btn.NamingContainer;
        int i = Convert.ToInt32(row.RowIndex);
        Label lblTongTien = (Label)row.Cells[7].FindControl("TongTien");

        if (btn.Checked == true)
        {
            Label lblTienNop = (Label)row.Cells[8].FindControl("TienNop");
            Label lblTienNo = (Label)row.Cells[9].FindControl("TienNo");
            lblTienNop.Text = lblTongTien.Text;
            lblTienNo.Text = "0"; 
        }
        else
        {
            Label lblTienNop = (Label)row.Cells[8].FindControl("TienNop");
            Label lblTienNo = (Label)row.Cells[9].FindControl("TienNo");
            lblTienNo.Text = lblTongTien.Text;
            lblTienNop.Text = "0";
        }
    }

    protected void btnXoaGrid_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        GridViewRow row = (GridViewRow)btn.NamingContainer;
        int i = Convert.ToInt32(row.RowIndex);
        CheckBox ckb = (CheckBox) row.FindControl("Chon");
        if (ckb != null)
            ckb.Checked = false;
        row.Visible = false;
        int stt = i;
        for (int j = i+1; j <thanhtoanphicanhan_grv.Rows.Count; j++)
        {
            GridViewRow rowtemp = thanhtoanphicanhan_grv.Rows[j];
            Label STT = (Label)rowtemp.Cells[1].FindControl("STT");
            if (rowtemp.Visible == true)
            {
                stt++;
                STT.Text = stt.ToString();
            }
        }
    }

    protected void btnXoaChiTiet_Click(object sender, EventArgs e)
    {
        //foreach (GridViewRow row in thanhtoanphicanhan_grv.Rows)
        //{
        //    CheckBox ChkBoxRows = (CheckBox)row.FindControl("Chon");
        //    if (ChkBoxRows.Checked == true)
        //    {
        //        row.Visible = false;
        //    }
        //}

        //int stt = 0;
        //for (int i = 0; i < thanhtoanphicanhan_grv.Rows.Count; i++)
        //{
        //    GridViewRow rowtemp = thanhtoanphicanhan_grv.Rows[i];
        //    Label STT = (Label)rowtemp.Cells[1].FindControl("STT");
        //    if (rowtemp.Visible == true)
        //    {
        //        stt++;
        //        STT.Text = stt.ToString();
        //    }
        //}
    }

    protected void btnChay_Click(object sender, EventArgs e)
    {
        List<string> lstIdChon = new List<string>();
        if (Session["ThanhToan_IdCaNhanChon"] != null)
        {
            lstIdChon = (List<string>)Session["ThanhToan_IdCaNhanChon"];
        }
        for (int i = 0; i < thanhtoanphicanhan_grv.Rows.Count; i++)
        {
            GridViewRow row = thanhtoanphicanhan_grv.Rows[i];
            if (row.Visible == false)
                if (lstIdChon.Contains(thanhtoanphicanhan_grv.DataKeys[i].Values[3].ToString()))
                    row.Visible = true;
        }
        vDanhSoGridCaNhan();
    }

    private void vDanhSoGridCaNhan()
    {
        int stt = 0;
        for (int j = 0; j < thanhtoanphicanhan_grv.Rows.Count; j++)
        {
            GridViewRow rowtemp = thanhtoanphicanhan_grv.Rows[j];
            Label STT = (Label)rowtemp.Cells[1].FindControl("STT");
            if (STT != null)
            {
                if (rowtemp.Visible == true)
                {
                    stt++;
                    STT.Text = stt.ToString();
                }
            }
        }
    }

    protected void btnTestLuu_Click(object sender, EventArgs e)
    {
        v_Luu();
    }

    protected void btnTestSua_Click(object sender, EventArgs e)
    {
        V_Sua();
    }

    protected void btnGuiMail_Click(object sender, EventArgs e)
    {
        vGuiMail();
    }

    private void vGuiMail()
    {
        try
        {
            string sub = "THÔNG BÁO TỪ HỘI KIỂM TOÁN HÀNH NGHỀ VIỆT NAM";
            string mess = "";
            string body = "Hô hô hô hô";
            SqlCommand sql = new SqlCommand();

            SmtpMail.Send(cm.Admin_TenDangNhap, "minhlp.87@gmail.com", sub, body, ref mess);
            cm.ghilog("ThanhToanPhiCaNhan", mess);
            cm.ghilog("ThanhToanPhiCaNhan", "Gửi mail thông báo thanh toán cá nhân thành công");
            //Response.Redirect("admin.aspx?page=thongbaonophoiphi&thongbao=thanhcong", false);
        }
        catch (Exception ex)
        {
            cm.ghilog("ThanhToanPhiCaNhan", "Lỗi xảy ra khi gửi mail thông báo thanh toán cá nhân: " + ex.ToString());
            //Response.Redirect("admin.aspx?page=thongbaonophoiphi&thongbao=loi", false);
        }
    }

    
}

    //private DataTable dtbPhiCaNhan
    //{
    //    get
    //    {
    //        if (ViewState[_dtbPhiCaNhan] == null)
    //        {
    //            DataTable dtbPhiCaNhan = new DataTable();
    //            dtbPhiCaNhan.Columns.Add(truonghoivienid);
    //            dtbPhiCaNhan.Columns.Add(truongstt);
    //            dtbPhiCaNhan.Columns.Add(truongmahoivien);
    //            dtbPhiCaNhan.Columns.Add(truonghoten);
    //            dtbPhiCaNhan.Columns.Add(truongsoccktv);
    //            dtbPhiCaNhan.Columns.Add("ngaycapchungchiktv");
    //            dtbPhiCaNhan.Columns.Add(truongdonvicongtac);
    //            dtbPhiCaNhan.Columns.Add(truongsophiphainop);
    //            dtbPhiCaNhan.Columns.Add(truongsophidanop);
    //            dtbPhiCaNhan.Columns.Add(truongsophiconno);
    //            DataColumn[] columns = new DataColumn[1];
    //            columns[0] = dtbPhiCaNhan.Columns[truonghoivienid];
    //            dtbPhiCaNhan.PrimaryKey = columns;
    //            return dtbPhiCaNhan;
    //        }
    //        return (DataTable)ViewState[_dtbPhiCaNhan];
    //    }
    //    set
    //    {
    //        ViewState[_dtbPhiCaNhan] = value;
    //    }
    //}