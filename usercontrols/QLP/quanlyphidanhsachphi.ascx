﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="quanlyphidanhsachphi.ascx.cs" Inherits="usercontrols_quanlyphidanhsachphi" %>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

   <script type="text/javascript">

       function changeloaiphi(type) {
           document.getElementById('<%= btnChangePhi.ClientID %>').click();
    }
    </script>

   <form id="Form1" name="Form1" runat="server" EnableViewState="true">
   <h4 class="widgettitle">Danh mục <%=tenchucnang%></h4>
   <div>
        <div  class="dataTables_length">
        
        <a id="btn_them"  href="#none" class="btn btn-rounded" onclick="themphikhac($('#LoaiPhi').val());"><i class="iconfa-plus-sign"></i> Thêm</a>
        <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="confirm_delete_danhsachphi(danhsachphi_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
        <a id="btn_duyet"  href="#none" class="btn btn-rounded" onclick="confirm_duyet_danhsachphi(danhsachphi_grv_selected);"><i class="iconfa-ok"></i> Duyệt</a>
        <a id="btn_tuchoi"  href="#none" class="btn btn-rounded" onclick="confirm_tuchoi_danhsachphi(danhsachphi_grv_selected);"><i class="iconfa-remove"></i> Từ chối</a>
        <a id="btn_thoaiduyet"  href="#none" class="btn btn-rounded" onclick="confirm_thoaiduyet_danhsachphi(danhsachphi_grv_selected);"><i class="iconfa-remove-circle"></i> Thoái duyệt</a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
        <a id="btn_timkiem" href="#none" class="btn btn-rounded" onclick="open_danhsachphi_search();"><i class="iconfa-search"></i> Tìm kiếm</a>
        <select name="LoaiPhi" id="LoaiPhi" onchange="changeloaiphi($('#LoaiPhi').val())">
          <%
              try
              {
                  LoadLoaiPhi(Request.Form["LoaiPhi"].ToString());
              }
              catch (Exception)
              {
                  LoadLoaiPhi("0");
              }
  
              %>
          </select>
        </div>
        
   </div>

                   <div style="width: 100%; text-align: center; margin-top: 5px; display:none">
    <asp:LinkButton ID="btnChangePhi" runat="server" CssClass="btn" 
        onclick="btnChangePhi_Click" ><i class="iconfa-search">           
    </i>Test</asp:LinkButton>
            </div>
       
       <input type="hidden" id="hdLoaiPhi" name="hdLoaiPhi" />
<asp:GridView ClientIDMode="Static" ID="danhsachphi_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="danhsachphi_grv_PageIndexChanging" 
       onsorting="danhsachphi_grv_Sorting"
       
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval(truongid)%>|<%# Eval(truongloai)%>"/>
                  </ItemTemplate>
              </asp:TemplateField>

             
               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongten)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  HeaderText="Mã Phí"   SortExpression="MaPhi" >
              <ItemTemplate>
                 <asp:HyperLink ID="linkFileUpload" 
                   
                 NavigateUrl=<%# "Javascript:open_danhsachphi_view(" + DataBinder.Eval(Container.DataItem, "PhiID").ToString() + ","+ DataBinder.Eval(Container.DataItem, "LoaiPhi").ToString()+"); "%>   Text= '<%# Eval("MaPhi") %>'  runat="server">
                    
                </asp:HyperLink>
        
             </ItemTemplate>
             <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
             <ItemStyle   HorizontalAlign="Center"     />
          </asp:TemplateField>

            <%-- <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongma)%></span>
                  </ItemTemplate>
              </asp:TemplateField>--%>
               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(mucphi, "{0:n2}")%></span>
                  </ItemTemplate>
              </asp:TemplateField>    
                              
               <asp:BoundField DataField="ngayapdung" 
                            ReadOnly="True"
                            DataFormatString="{0:dd/MM/yyyy}" />
              <asp:BoundField DataField="ngayhethieuluc" HeaderText="Ngày hết hiệu lực" 
                            ReadOnly="True" SortExpression="NgayHetHieuLuc" 
                            DataFormatString="{0:dd/MM/yyyy}" />
                               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(doituongapdung)%></span>
                  </ItemTemplate>
              </asp:TemplateField>    
                <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(tinhtrang)%></span>
                  </ItemTemplate>
              </asp:TemplateField>     
               
              <asp:TemplateField HeaderText='' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                  <div class="btn-group">
                    <a onclick="open_danhsachphi_view(<%# Eval(truongid)%>,<%# Eval(truongloai)%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xem"  rel="tooltip" class="btn"><i class="iconsweets-trashcan2" ></i></a>
                    <%#(((int)(Eval(tinhtrangid)) != (int)EnumVACPA.TinhTrang.DaPheDuyet) ? "<a onclick='open_danhsachphi_edit(" + Eval(truongid) + "," + Eval(truongloai) + ");' data-placement='top' data-rel='tooltip' href='#none' data-original-title='Sửa'  rel='tooltip' class='btn'><i class='iconsweets-create' ></i></a>" : "")%>
                    <%#(((int)(Eval(tinhtrangid)) != (int)EnumVACPA.TinhTrang.DaPheDuyet) ? "<a onclick='confirm_delete_danhsachphi_1(" + Eval(truongid) + "," + Eval(truongloai) + ");' data-placement='top' data-rel='tooltip' href='#none' data-original-title='Xóa'  rel='tooltip' class='btn'><i class='iconsweets-trashcan' ></i></a>" : "")%>
                        </div>
                                       

                  </ItemTemplate>
              </asp:TemplateField>


          </Columns>
</asp:GridView>


<div class="dataTables_info" id="dyntable_info" >

<div class="pagination pagination-mini">Chuyển đến trang: 

<ul>

 

<li><a href="#" onclick="$('#tranghientai').val(0); $('#user_search').submit();"><< Đầu tiên</a></li>

<li><a href="#" onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager').val()-1); $('#user_search').submit();}" >< Trước</a></li>

<li><a style=" border: none; background-color:#eeeeee"><asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" 

style=" width:55px; height:22px; "

onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >

</asp:DropDownList></a> </li>

<li style="margin-left:5px; "><a href="#" onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) {$('#tranghientai').val(parseInt($('#Pager').val())+1); $('#user_search').submit();} ;" style="border-left: 1px solid #ccc;">Sau ></a></li>

<li><a href="#" onclick="$('#tranghientai').val($('#Pager option').length-1); $('#user_search').submit();">Cuối cùng >></a></li>

</ul>

</div> 

</div>


                                     


</form>





<script type="text/javascript">

    <% annut(); %>


    // Array ID được check
    var danhsachphi_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#danhsachphi_grv .checkall").bind("click", function () {
            danhsachphi_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#danhsachphi_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) danhsachphi_grv_selected.push($(this).val());
            });
        });

        $('#danhsachphi_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(danhsachphi_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) danhsachphi_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa nhiều
    function confirm_delete_danhsachphi(idxoa) {
    if (idxoa == "")
    {
        $("#div_danhsachphi_canhbaonull").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Đóng lại": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $('#div_danhsachphi_canhbaonull').parent().find('button:contains("Đóng lại")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    else
    {
        $("#div_danhsachphi_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                     window.location = 'admin.aspx?page=quanlyphidanhsachphi&act=delete&id=' + idxoa;
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#div_danhsachphi_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_danhsachphi_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
        
    }
    }

     // Xác nhận duyệt nhiều
    function confirm_duyet_danhsachphi(id) {
    if (id == "")
    {
        $("#div_danhsachphi_canhbaonull").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Đóng lại": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $('#div_danhsachphi_canhbaonull').parent().find('button:contains("Đóng lại")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    else 
    {
    $('#div_danhsachphi_duyet').dialog({
            resizable: false,
            modal: true,
            buttons: {
                'Duyệt': function () {
                     window.location = 'admin.aspx?page=quanlyphidanhsachphi&act=duyet&id=' + id;
                },
                'Bỏ qua': function () {
                    $(this).dialog('close');
                }
            }
        });
         $('#div_danhsachphi_duyet').parent().find('button:contains("Duyệt")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_danhsachphi_duyet').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    }

    // Xác nhận từ chối nhiều
    function confirm_tuchoi_danhsachphi(id) {
    if (id == "")
    {
        $("#div_danhsachphi_canhbaonull").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Đóng lại": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $('#div_danhsachphi_canhbaonull').parent().find('button:contains("Đóng lại")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    else 
    {
    $('#div_danhsachphi_tuchoi').dialog({
            resizable: false,
            modal: true,
            buttons: {
                'Từ chối duyệt': function () {
                     window.location = 'admin.aspx?page=quanlyphidanhsachphi&act=tuchoi&id=' + id;
                },
                'Bỏ qua': function () {
                    $(this).dialog('close');
                }
            }
        });
         $('#div_danhsachphi_tuchoi').parent().find('button:contains("Từ chối duyệt")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_danhsachphi_tuchoi').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    }

    // Xác nhận thoái duyệt nhiều
    function confirm_thoaiduyet_danhsachphi(id) {
    if (id == "")
    {
        $("#div_danhsachphi_canhbaonull").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Đóng lại": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $('#div_danhsachphi_canhbaonull').parent().find('button:contains("Đóng lại")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    else 
    {
    $('#div_danhsachphi_thoaiduyet').dialog({
            resizable: false,
            modal: true,
            buttons: {
                'Thoái duyệt': function () {
                     window.location = 'admin.aspx?page=quanlyphidanhsachphi&act=thoaiduyet&id=' + id;
                },
                'Bỏ qua': function () {
                    $(this).dialog('close');
                }
            }
        });
         $('#div_danhsachphi_thoaiduyet').parent().find('button:contains("Thoái duyệt")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_danhsachphi_thoaiduyet').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }
    }

    //Xác nhận xóa 1
    function confirm_delete_danhsachphi_1(idxoa, loai) {

        $("#div_danhsachphi_delete_1").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                if (loai == 1)
                {
                    window.location = 'admin.aspx?page=quanlyphihoivien&tinhtrang=xoa&mode=view&id=' + idxoa;   
                }
                else if (loai == 2)
                {
                    window.location = 'admin.aspx?page=quanlyphidanglogo&tinhtrang=xoa&mode=view&id=' + idxoa;
                }
                else if (loai == 3)
                {
                    window.location = 'admin.aspx?page=quanlyphikhac&tinhtrang=xoa&mode=view&id=' + idxoa;
                }
                else if (loai == 4)
                {
                    window.location = 'admin.aspx?page=quanlyphikiemsoatchatluong&tinhtrang=xoa&mode=view&id=' + idxoa;
                }
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_danhsachphi_delete_1').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_danhsachphi_delete_1').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    // Xem thông tin 
    function open_danhsachphi_view(id, loai) {
        var timestamp = Number(new Date());
        if (loai == 1)
        {
            window.location = "admin.aspx?page=quanlyphihoivien&id=" + id + "&mode=view";
        }
        else if (loai == 2 )
        {
            window.location = "admin.aspx?page=quanlyphidanglogo&id=" + id + "&mode=view";
        }
        else if (loai == 3)
        {
            window.location = "admin.aspx?page=quanlyphikhac&id=" + id + "&mode=view";
        }
        else if (loai == 4)
        {
            window.location = "admin.aspx?page=quanlyphikiemsoatchatluong&id=" + id + "&mode=view";
        }
    }

    // Sửa thông tin 
    function open_danhsachphi_edit(id, loai) {
        var timestamp = Number(new Date());
        if (loai == 1)
        {
            window.location = "admin.aspx?page=quanlyphihoivien&id=" + id + "&mode=edit";
        }
        else if (loai == 2 )
        {
            window.location = "admin.aspx?page=quanlyphidanglogo&id=" + id + "&mode=edit";
        }
        else if (loai == 3)
        {
            window.location = "admin.aspx?page=quanlyphikhac&id=" + id + "&mode=edit";
        }
        else if (loai == 4)
        {
            window.location = "admin.aspx?page=quanlyphikiemsoatchatluong&id=" + id + "&mode=edit";
        }
    }
   
    // Tìm kiếm
    function open_danhsachphi_search() {
        var timestamp = Number(new Date());

        $("#div_danhsachphi_search").dialog({
            resizable: true,
            width: 640,
            height: 480,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm</b>",
            modal: true,
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            },



             open: function(event, ui) {
    $(ui).find('#timkiem_NgayApDungTu, #timkiem_NgayApDungDen, #timkiem_NgayHetHieuLucTu, #timkiem_NgayHetHieuLucDen').datepicker().click(function(){
        $(this).datepicker('show');
    });
 },
 close: function(event,ui) {
    $(ui).find('##timkiem_NgayApDungTu, #timkiem_NgayApDungDen, #timkiem_NgayHetHieuLucTu, #timkiem_NgayHetHieuLucDen').datepicker('destroy');
 }
        });

        $('#div_danhsachphi_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_danhsachphi_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }





    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('#danhsachphi_grv').length > 0) jQuery('#danhsachphi_grv').tooltip({ selector: "a[rel=tooltip]" });

    function themphikhac(loai)
    {
        if(loai == 1)
            window.location = "admin.aspx?page=quanlyphihoivien";
        else if (loai == 2)
            window.location = "admin.aspx?page=quanlyphidanglogo";
        else if (loai == 3)
            window.location = "admin.aspx?page=quanlyphikhac";
        else if (loai == 5)
            window.location = "admin.aspx?page=quanlyphikiemsoatchatluong";
        else
            window.location = "admin.aspx?page=quanlyphikhac";
    };
  
</script>

<script type="text/javascript">

    $('input:checkbox[name="timkiem_choduyet"]').change(
    function () {
        var vChoDuyet = document.getElementById('timkiem_choduyet');
        if ($(this).is(':checked')) {
            vChoDuyet.value = 'checked';
        }
        else {
            vChoDuyet.value = '';
        }
    });

</script>

<script type="text/javascript">



</script>


<script type="text/javascript">

    $(function () {
        $('#timkiem_NgayHetHieuLucTu, #timkiem_NgayApDungTu').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function (dateText, inst) {
                $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
            }
        }).datepicker('option', '', '');
    });


    $(function () {
        $('#timkiem_NgayHetHieuLucDen, #timkiem_NgayApDungDen').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function (dateText, inst) {
                $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
            }
        }).datepicker('option', '', '');
    });


    $(function () { $('#timkiem_NgayApDungDen').datepicker('option', 'minDate', $('#timkiem_NgayApDungTu').val()) });

    $('#timkiem_NgayApDungTu').change(function () {

        $('#timkiem_NgayApDungDen').datepicker('option', 'minDate', $('#timkiem_NgayApDungTu').val());
    });

    $(function () { $('#timkiem_NgayHetHieuLucDen').datepicker('option', 'minDate', $('#timkiem_NgayHetHieuLucTu').val()) });

    $('#timkiem_NgayHetHieuLucTu').change(function () {

        $('#timkiem_NgayHetHieuLucDen').datepicker('option', 'minDate', $('#timkiem_NgayHetHieuLucTu').val());
    });

    function getLoaiPhi(sel) {
        var value = sel.value;
    }

</script>

<div id="div_danhsachphi_add" >
</div>

<div id="div_danhsachphi_search" style="display:none" >
<form id="user_search" method="post" enctype="multipart/form-data" >
<table  width="100%" border="0" class="formtbl"  >
    <tr>
        <td style="width:152px"><label>Tên phí: </label></td>
        <td colspan = "3"><input type="text" name="timkiem_Ten" id="timkiem_Ten" value="<%=Request.Form["timkiem_Ten"]%>"  class="input-xlarge" />
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
        
        </td>
    </tr>
    <tr>
        <td style="width:152px"><label>Mã phí: </label></td>
        <td colspan = "3"><input type="text" name="timkiem_Ma" id="timkiem_Ma" value="<%=Request.Form["timkiem_Ma"]%>"  class="input-xlarge" />
        
        </td>
    </tr>
    <tr>
        <td style="width:152px"><label>Ngày áp dụng: </label></td>
        <td><input type="text" name="timkiem_NgayApDungTu" id="timkiem_NgayApDungTu" value="<%=Request.Form["timkiem_NgayApDungTu"]%>"  class="input-xlarge" />
        
        </td>
        <td style="width:12px"><label>- </label></td>
        <td><input type="text" name="timkiem_NgayApDungDen" id="timkiem_NgayApDungDen" value="<%=Request.Form["timkiem_NgayApDungDen"]%>"  class="input-xlarge" />
        
        </td>
    </tr>

    <tr>
        <td style="width:152px"><label>Ngày hết hiệu lực: </label></td>
        <td><input type="text" name="timkiem_NgayHetHieuLucTu" id="timkiem_NgayHetHieuLucTu" value="<%=Request.Form["timkiem_NgayHetHieuLucTu"]%>"  class="input-xlarge" />
        
        </td>
        <td style="width:12px"><label>- </label></td>
        <td><input type="text" name="timkiem_NgayHetHieuLucDen" id="timkiem_NgayHetHieuLucDen" value="<%=Request.Form["timkiem_NgayHetHieuLucDen"]%>"  class="input-xlarge" />
        
        </td>
    </tr>
    
</table>

<table  width="100%" border="0" class="formtbl"  >
    <tr>
        <td style="width:152px"><label>Tình trạng: </label></td>
        <td><label><input type="checkbox" name="timkiem_choduyet" id="timkiem_choduyet" value="checked"  class="input-xlarge" />Chờ duyệt</label></td>
        <td><label><input type="checkbox" name="timkiem_daduyet" id="timkiem_daduyet" value="checked"  class="input-xlarge" />Duyệt</label></td>
        <td><label><input type="checkbox" name="timkiem_tuchoi" id="timkiem_tuchoi" value="checked"  class="input-xlarge" />Từ chối</label></td>
        <td><label><input type="checkbox" name="timkiem_thoaiduyet" id="timkiem_thoaiduyet" value="checked"  class="input-xlarge"/>Thoái duyệt</label></td>
    </tr>
    <tr>
        <td style="width:152px"><label>Tình trạng hiệu lực: </label></td>
        <td colspan = "2"><label><input type="checkbox" name="timkiem_chuacohieuluc" id="timkiem_chuacohieuluc" value="checked"  class="input-xlarge"/> Chưa có hiệu lực</label></td>
        <td><label><input type="checkbox" name="timkiem_cohieuluc" id="timkiem_cohieuluc" value="checked"  class="input-xlarge"/>Có hiệu lực</label></td>
        <td><label><input type="checkbox" name="timkiem_hethieuluc" id="timkiem_hethieuluc" value="checked"  class="input-xlarge"/>Hết hiệu lực</label></td>
    </tr>
</table>
</form>
</div>

<div id="div_danhsachphi_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn? (Lưu ý: chỉ xóa những bản ghi ở trạng thái chờ duyệt/không phê duyệt/thoái duyệt)?</p>

</div>

<div id="div_danhsachphi_delete_1" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?</p>

</div>

<div id="div_danhsachphi_canhbaonull" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Cần chọn bản ghi trước khi thao tác</p>

</div>

<div id="div_danhsachphi_khongphuhop" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Thông báo</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Có một số bản ghi không phù hợp</p>

</div>



<div id="div_danhsachphi_duyet" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận Duyệt</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Duyệt (các) bản ghi được chọn? (Lưu ý: chỉ duyệt những bản ghi ở trạng thái chờ duyệt)</p>

</div>

<div id="div_danhsachphi_tuchoi" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận Từ chối duyệt</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Từ chối duyệt (các) bản ghi được chọn? (Lưu ý: chỉ từ chối những bản ghi ở trạng thái chờ duyệt)</p>

</div>


<div id="div_danhsachphi_thoaiduyet" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận Thoái duyệt</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Thoái duyệt (các) bản ghi được chọn? (Lưu ý: chỉ thoái duyệt những bản ghi ở trạng thái đã phê duyệt)</p>

</div>



