﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_danhsachoiviencanhan : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Session["ThanhToan_IdCaNhanChon"] = null;
            grvDanhSach.Columns[1].HeaderText = "Mã hội viên";
            grvDanhSach.Columns[2].HeaderText = "Họ và tên";
            grvDanhSach.Columns[3].HeaderText = "Số CCKTV";

            if (Request.QueryString["type"] == "thanhtoantapthe")
                {
                    string id = Request.QueryString["id"];
                    if (!string.IsNullOrEmpty(id))
                    {
                        SqlCommand sql = new SqlCommand();
                        sql.CommandText = @"SELECT HOIVIENCANHANID, MAHOIVIENCANHAN, HODEM + ' ' + TEN AS HOVATEN, SOCHUNGCHIKTV FROM TBLHOIVIENCANHAN WHERE HOIVIENTAPTHEID = " + id;
                        DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                        grvDanhSach.DataSource = dtbTemp;
                        grvDanhSach.DataBind();
                    }
                }
            else if (Request.QueryString["type"] == "thanhtoancanhan")
            {

                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = @"SELECT HOIVIENCANHANID, MAHOIVIENCANHAN, HODEM + ' ' + TEN AS HOVATEN, SOCHUNGCHIKTV FROM TBLHOIVIENCANHAN ";
                    DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                    grvDanhSach.DataSource = dtbTemp;
                    grvDanhSach.DataBind();
                
            }
         }
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]) || !string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
        {
            
        if (Request.QueryString["type"] == "thanhtoantapthe")
            {
                string id = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(id))
                {
                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = @"SELECT * FROM (SELECT HOIVIENCANHANID, MAHOIVIENCANHAN, HODEM + ' ' + TEN AS HOVATEN, SOCHUNGCHIKTV, HOIVIENTAPTHEID FROM TBLHOIVIENCANHAN) as TEMP "
                        + " WHERE HOIVIENTAPTHEID = " + id;
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                    {
                        sql.CommandText += " AND MAHOIVIENCANHAN like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                    }
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                    {
                        sql.CommandText += " AND HOVATEN like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
                    }
                    DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                    grvDanhSach.DataSource = dtbTemp;
                    grvDanhSach.DataBind();
                }
            }
        else if (Request.QueryString["type"] == "thanhtoancanhan")
            {
                SqlCommand sql = new SqlCommand();
                sql.CommandText = @"SELECT * FROM (SELECT HOIVIENCANHANID, MAHOIVIENCANHAN, HODEM + ' ' + TEN AS HOVATEN, SOCHUNGCHIKTV, HOIVIENTAPTHEID FROM TBLHOIVIENCANHAN) as TEMP "
                    + " WHERE 0 = 0 ";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                {
                    sql.CommandText += " AND MAHOIVIENCANHAN like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                }
                if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                {
                    sql.CommandText += " AND HOVATEN like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
                }
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                grvDanhSach.DataSource = dtbTemp;
                grvDanhSach.DataBind();
            }
        }
    }

    protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)grvDanhSach.HeaderRow.FindControl("chkboxSelectAll");
        for (int i = 0; i < grvDanhSach.Rows.Count; i++)
        {
            GridViewRow row = grvDanhSach.Rows[i];
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkEmp");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                chkEmp_CheckedChanged(ChkBoxRows, null);
            }
            else
            {
                ChkBoxRows.Checked = false;
                chkEmp_CheckedChanged(ChkBoxRows, null);
            }
        }
    }

    protected void chkEmp_CheckedChanged(object sender, EventArgs e)
    {
        List<string> lstIdChon = new List<string>();
        if (Session["ThanhToan_IdCaNhanChon"] != null)
        {
            lstIdChon = (List<string>)Session["ThanhToan_IdCaNhanChon"];
        }
        CheckBox cbkdangcheck = (CheckBox)sender;
        GridViewRow rowdangcheck = (GridViewRow)cbkdangcheck.NamingContainer;
        int idongdangcheck = Convert.ToInt32(rowdangcheck.RowIndex);
        if (cbkdangcheck.Checked == true)
        {
            string id = grvDanhSach.DataKeys[idongdangcheck].Value.ToString();
            if (!lstIdChon.Contains(id))
                lstIdChon.Add(id);
        }
        else
        {
            string id = grvDanhSach.DataKeys[idongdangcheck].Value.ToString();
            if (lstIdChon.Contains(id))
                lstIdChon.Remove(id);
        }
        Session["ThanhToan_IdCaNhanChon"] = lstIdChon;
    }
}