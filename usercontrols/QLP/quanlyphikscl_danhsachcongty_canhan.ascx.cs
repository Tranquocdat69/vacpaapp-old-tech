﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using VACPA.Entities;
using VACPA.Data.SqlClient;

public partial class usercontrols_quanlyphikscl_danhsachcongty_canhan : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (Request.QueryString["type"] == "tapthe")
            {
                Session["CongTyChon"] = null;
                Session["lstIdChon"] = null;
                Session["MaCongTyChon"] = null;
            }
            grvDanhSach.Columns[1].HeaderText = "Mã hội viên";
            grvDanhSach.Columns[2].HeaderText = "Họ và tên";
           // grvDanhSach.Columns[3].HeaderText = "Số CCKTV";

            if (Request.QueryString["type"] == "canhan")
                {
                    string id = Request.QueryString["id"];
                    if (!string.IsNullOrEmpty(id))
                    {
                        List<int> lstIdChon = new List<int>();
                        if (Session["lstIdChon"] != null)
                            lstIdChon = Session["lstIdChon"] as List<int>;
                        string result = string.Join(", ", lstIdChon);

                        SqlCommand sql = new SqlCommand();
                        //sql.CommandText = @"SELECT HoiVienCaNhanID, HODEM + ' ' + TEN AS HOTEN FROM tblHoiVienCaNhan WHERE HoiVienTapTheID = " + id;
                        sql.CommandText = @"SELECT MaHoiVienCaNhan AS MAHOIVIEN, HoiVienCaNhanID, HODEM + ' ' + TEN AS HOTEN FROM tblHoiVienCaNhan WHERE HoiVienTapTheID = " + id;
                        if (lstIdChon.Count() != 0)
                        {
                            sql.CommandText += " AND HoiVienCaNhanID NOT IN (" + result + ")";
                        }
                        DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                        grvDanhSach.DataSource = dtbTemp;
                        grvDanhSach.DataBind();
                    }
                }
            else if (Request.QueryString["type"] == "tapthe")
            {

                    SqlCommand sql = new SqlCommand();
                    //sql.CommandText = @"SELECT HoiVienTapTheID AS HoiVienCaNhanID, TenDoanhNghiep AS HOTEN FROM tblHoiVienTapThe ";
                    //sql.CommandText = @"SELECT MaHoiVienTapThe AS MAHOIVIEN, HoiVienTapTheID as HoiVienCaNhanID, TenDoanhNghiep AS HOTEN FROM tblHoiVienTapThe ";
                    sql.CommandText = @"SELECT MaDanhSach AS MAHOIVIEN, DSKiemTraTrucTiepID as HoiVienCaNhanID, MaDanhSach AS HOTEN FROM tblKSCLDSKiemTraTrucTiep ";
                    DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                    grvDanhSach.DataSource = dtbTemp;
                    grvDanhSach.DataBind();
                
            }
         }
        //else
        //{
            
        //}
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]) || !string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
        {
            
        if (Request.QueryString["type"] == "canhan")
            {
                string id = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(id))
                {
                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = @"SELECT HoiVienCaNhanID, HODEM + ' ' + TEN AS HOTEN FROM tblHoiVienCaNhan as TEMP "
                        + " WHERE HoiVienTapTheID = " + id;
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                    {
                        sql.CommandText += " AND HoiVienCaNhanID like '%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                    }
                    if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                    {
                        sql.CommandText += " AND HODEM + ' ' + TEN like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
                    }
                    DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                    grvDanhSach.DataSource = dtbTemp;
                    grvDanhSach.DataBind();
                }
            }
        else if (Request.QueryString["type"] == "tapthe")
            {
                SqlCommand sql = new SqlCommand();
                //sql.CommandText = @"SELECT HoiVienTapTheID AS HoiVienCaNhanID, TenDoanhNghiep AS HOTEN FROM tblHoiVienTapThe ";
                sql.CommandText =
                    @"SELECT MaDanhSach AS MAHOIVIEN, DSKiemTraTrucTiepID as HoiVienCaNhanID, MaDanhSach AS HOTEN FROM tblKSCLDSKiemTraTrucTiep where 0 = 0 "; 
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
                {
                    sql.CommandText += " AND MaDanhSach like N'%" + Request.Form["timkiem_Ma"].ToString().TrimEnd() + "%'";
                }
                if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                {
                    sql.CommandText += " AND MaDanhSach like N'%" + Request.Form["timkiem_Ten"].ToString().TrimEnd() + "%'";
                }
                DataTable dtbTemp = DataAccess.RunCMDGetDataSet(sql).Tables[0];
                grvDanhSach.DataSource = dtbTemp;
                grvDanhSach.DataBind();
            }
        }
    }

    protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)grvDanhSach.HeaderRow.FindControl("chkboxSelectAll");
        for (int i = 0; i < grvDanhSach.Rows.Count; i++)
        {
            GridViewRow row = grvDanhSach.Rows[i];
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkEmp");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                chkEmp_CheckedChanged(ChkBoxRows, null);
            }
            else
            {
                ChkBoxRows.Checked = false;
                chkEmp_CheckedChanged(ChkBoxRows, null);
            }
        }
    }

    protected void chkEmp_CheckedChanged(object sender, EventArgs e)
    {
        
        List<int> lstIdChon = new List<int>();
        List<string> lstMaCongTyChon = new List<string>();
        if (Request.QueryString["type"] == "canhan")
        {
            if (Session["lstIdChon"] != null)
            {
                lstIdChon = (List<int>)Session["lstIdChon"];
            }
        }
        else
        {
            if (Session["CongTyChon"] != null)
            {
                lstIdChon = (List<int>)Session["CongTyChon"];
            }
            if (Session["MaCongTyChon"] != null)
            {
                lstMaCongTyChon = (List<string>)Session["MaCongTyChon"];
            }
        }
        CheckBox cbkdangcheck = (CheckBox)sender;
        GridViewRow rowdangcheck = (GridViewRow)cbkdangcheck.NamingContainer;
        int idongdangcheck = Convert.ToInt32(rowdangcheck.RowIndex);
        if (cbkdangcheck.Checked == true)
        {
            int id = Convert.ToInt32(grvDanhSach.DataKeys[idongdangcheck].Value);
            if (!lstIdChon.Contains(id))
                lstIdChon.Add(id);
            //Mã
            string strMa = ((Label)rowdangcheck.Cells[1].FindControl("MAHOIVIEN")).Text.TrimEnd();
            if (!lstMaCongTyChon.Contains(strMa))
                lstMaCongTyChon.Add(strMa);
        }
        else
        {
            int id = Convert.ToInt32(grvDanhSach.DataKeys[idongdangcheck].Value);
            if (lstIdChon.Contains(id))
                lstIdChon.Remove(id);
            //Mã
            string strMa = ((Label)rowdangcheck.Cells[1].FindControl("MAHOIVIEN")).Text.TrimEnd();
            if (!lstMaCongTyChon.Contains(strMa))
                lstMaCongTyChon.Remove(strMa);
        }
        if (Request.QueryString["type"] == "canhan")
            Session["lstIdChon"] = lstIdChon;
        else
        {
            Session["CongTyChon"] = lstIdChon;
            Session["MaCongTyChon"] = lstMaCongTyChon;
        }
    }
}