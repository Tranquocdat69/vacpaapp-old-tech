﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class usercontrols_BaoCaoDanhSachNguoiQuanTamTrongCaNuoc : System.Web.UI.UserControl
{
    protected string tenchucnang = "Danh sách Người quan tâm trong cả nước";
    protected string _listPermissionOnFunc_QuanLyLopHoc = "";
    private clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public Commons cm = new Commons();
    public DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class='iconfa-star-empty'></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));
        //_listPermissionOnFunc_QuanLyLopHoc = kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), ListName.Func_CNKT_QuanLyLopHoc, cm.connstr);
        if (Session["MsgError"] != null)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + Session["MsgError"] + "</div>"));
            Session.Remove("MsgError");
        }
        try
        {
            if (this.IsPostBack)
                setdataCrystalReport();
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }
        finally
        {
        }
    }

    public void LoadVungMien(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT VungMienID, TenVungMien FROM tblDMVungMien";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["VungMienID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["VungMienID"] + @""">" +
                               dtr["TenVungMien"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["VungMienID"] + @""">" + dtr["TenVungMien"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadTinhThanh(string VungMienId = "", string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where TinhID in (" + Id + ")";
        sql.CommandText = "SELECT MaTinh, TenTinh FROM tblDMTinh where 0 = 0 AND MATINH IS NOT NULL";
        if (VungMienId != "")
            sql.CommandText += " AND VungMienID In (" + VungMienId + ")";
        //if (Id != "")
        //    sql.CommandText += " AND MaTinh In (" + Id + ")";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["MaTinh"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["MaTinh"] + @""">" +
                               dtr["TenTinh"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["MaTinh"] + @""">" + dtr["TenTinh"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);

    }

    public void LoadTenCongTy(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT HoiVienTapTheId, TenDoanhNghiep FROM tblHoiVienTapThe";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["HoiVienTapTheId"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["HoiVienTapTheId"] + @""">" +
                               dtr["TenDoanhNghiep"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["HoiVienTapTheId"] + @""">" + dtr["TenDoanhNghiep"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadChungChiQuocTe(string Id = "")
    {
        string output_html = "";
        //-1: tất. 0: không có. 1: Có
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        if (Id.Contains("1"))
            output_html += @"<option selected=""selected"" value=1>Có chứng chỉ Quốc tế</option>";
        else
            output_html += @"<option value=1>Có chứng chỉ Quốc tế</option>";
        if (Id.Contains("0"))
            output_html += @"<option selected=""selected"" value=0>Không có chứng chỉ Quốc tế</option>";
        else
            output_html += @"<option value=0>Không có chứng chỉ Quốc tế</option>";
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void LoadChucDanh(string Id = "")
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT ChucVuID, TenChucVu FROM tblDMChucVu";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        string output_html = "";
        if (Id.Contains("-1") || Id == "")
            output_html += @"<option value=-1 selected=""selected"">Tất cả</option>";
        else
            output_html += @"<option  value=-1>Tất cả</option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (Id.Contains(dtr["ChucVuID"].ToString()))
                output_html += @"<option selected=""selected"" value=""" + dtr["ChucVuID"] + @""">" +
                               dtr["TenChucVu"] + "</option>";
            else
                output_html += @"<option value=""" + dtr["ChucVuID"] + @""">" + dtr["TenChucVu"] + "</option>";

        }
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void setdataCrystalReport()
    {

        List<objDanhSachNguoiQuanTamTrongCaNuoc> lst = new List<objDanhSachNguoiQuanTamTrongCaNuoc>();
        string strTenCongTy = Request.Form["TenCongTy"];
        string strChungChiQuocTe = Request.Form["ChungChiQuocTe"];
        string strChucDanh = Request.Form["ChucDanh"];
        string strVungMien = Request.Form["VungMien"];
        string strTinhThanh = Request.Form["TinhThanh"];


        DataSet ds = new DataSet();
        SqlCommand sql = new SqlCommand();
        sql.CommandText = @"select * from (select A.*, TenChucVu, TenTinh, TenQuocTich, SoHieu, TenDoanhNghiep
, C.VungMienID, (datepart(yyyy,GETDATE())  - YEAR(NgaySinh)) as DoTuoi, F.SoChungChiQuocTe from tblHoiVienCaNhan A 
left join tblDMChucVu B on A.ChucVuID = B.ChucVuID 
left join tblDMTinh C on A.QueQuan_TinhID = C.TinhID
left join tblDMQuocTich D on A.QuocTichID = D.QuocTichID
left join tblHoiVienTapThe E on A.HoiVienTapTheID = E.HoiVienTapTheID 
left join (select Count(ChungChiQuocTeID) as SoChungChiQuocTe, HoiVienCaNhanID from tblHoiVienCaNhanChungChiQuocTe
group by HoiVienCaNhanID)
as F
on F.HoiVienCaNhanID = A.HoiVienCaNhanID 
where LoaiHoiVienCaNhan = 0) as Temp
where (0 = 0
";

        //Lọc theo Tên công ty được chọn khác tất cả
        if (!string.IsNullOrEmpty(strTenCongTy) && !strTenCongTy.Contains("-1"))
            sql.CommandText += " AND (HoiVienTapTheID in (" + strTenCongTy + "))";
        //Lọc theo Chứng chỉ được chọn khác tất cả
        if (!string.IsNullOrEmpty(strChungChiQuocTe) && !strChungChiQuocTe.Contains("-1"))
        {
            if (strChungChiQuocTe == "1")
                sql.CommandText += " AND (SoChungChiQuocTe > 0)";
            else if (strChungChiQuocTe == "0")
                sql.CommandText += " AND (SoChungChiQuocTe IS NULL OR SoChungChiQuocTe = 0)";
        }
        //Lọc theo Tỉnh thành được chọn khác tất cả
        if (!string.IsNullOrEmpty(strTinhThanh) && !strTinhThanh.Contains("-1"))
            sql.CommandText += " AND DiaChi_TinhID in (" + strTinhThanh + ")";
        else if (strTinhThanh == "-1")
        {
            //Lọc theo Vùng miền được chọn với Tỉnh bằng tất cả và vùng miền khác tất cả
            if (!string.IsNullOrEmpty(strVungMien) && !strVungMien.Contains("-1"))
                sql.CommandText += " AND VungMienID in (" + strVungMien + ")";
        }


        //Lọc theo Ngày sinh
        if (Request.Form["ngaysinhtu"] != null)
        {
            string Tu = Request.Form["ngaysinhtu"].ToString();
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (NgaySinh >= '" +
                                   (DateTime.ParseExact(Request.Form["ngaysinhtu"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }
        if (Request.Form["ngaysinhden"] != null)
        {
            string Den = Request.Form["ngaysinhden"].ToString();
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (NgaySinh <= '" +
                                   (DateTime.ParseExact(Request.Form["ngaysinhden"], "dd/MM/yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture)).ToString
                                       ("yyyy-MM-dd") + "') ";
        }

        //Lọc theo độ tuổi
        if (Request.Form["dotuoitu"] != null)
        {
            string Tu = strBoChamPhay(Request.Form["dotuoitu"].ToString());
            if (!string.IsNullOrEmpty(Tu))
                sql.CommandText += " AND (DoTuoi >= " + Tu + ") ";
        }
        if (Request.Form["dotuoiden"] != null)
        {
            string Den = strBoChamPhay(Request.Form["dotuoiden"].ToString());
            if (!string.IsNullOrEmpty(Den))
                sql.CommandText += " AND (DoTuoi <= " + Den + ") ";
        }


        if (((Request.Form["cotrolyktv"] != null) && (Request.Form["khongtrolyktv"] != null))
            || ((Request.Form["cotrolyktv"] == null) && (Request.Form["khongtrolyktv"] == null)))
        {
        }
        else
        {
            //Nếu không check đang hành nghề => lọc bỏ hành nghề
            if (Request.Form["cotrolyktv"] != null)
            {
                sql.CommandText += " AND (TroLyKTV = 1) ";
            }
            //Nếu không check hội viên => bỏ lọc hội viên
            else if (Request.Form["khongtrolyktv"] != null)
            {
                sql.CommandText += " AND (TroLyKTV = 0) ";
            }
        }

        sql.CommandText += ") order by HoiVienTapTheID, HoiVienCaNhanID";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        DataTable dt = ds.Tables[0];

        int i = 0;
        foreach (DataRow tem in dt.Rows)
        {
            i++;
            objDanhSachNguoiQuanTamTrongCaNuoc obj = new objDanhSachNguoiQuanTamTrongCaNuoc();
            obj.NgayThangNam = "(Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            obj.STT = i.ToString();
            obj.IDNguoiQuanTam = tem["MaHoiVienCaNhan"].ToString();
            obj.HoVaTen = tem["HoDem"].ToString() + " " + tem["Ten"].ToString();
            string NgaySinhNam = "";
            string NgaySinhNu = "";
            try
            {
                if (tem["GioiTinh"].ToString() == "F")
                    if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                        NgaySinhNu = Convert.ToDateTime(tem["NgaySinh"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }

            try
            {
                if (tem["GioiTinh"].ToString() == "M")
                    if (!string.IsNullOrEmpty(tem["NgaySinh"].ToString()))
                        NgaySinhNam = Convert.ToDateTime(tem["NgaySinh"]).ToString("dd/MM/yyyy");
            }
            catch (Exception)
            {
            }
            
            obj.NgaySinhNam = NgaySinhNam;
            obj.NgaySinhNu = NgaySinhNu;
            obj.ChucVu = tem["TenChucVu"].ToString();
            obj.DonViCongTac = tem["DonViCongTac"].ToString();
            obj.TroLyKTVCTKT = tem["TroLyKTV"].ToString();
            string DienThoaiCoDinhDiDong = "";
            if (!string.IsNullOrEmpty(tem["DienThoai"].ToString()))
                DienThoaiCoDinhDiDong = tem["DienThoai"].ToString();
            if (!string.IsNullOrEmpty(tem["Mobile"].ToString()))
                if (!string.IsNullOrEmpty(tem["DienThoai"].ToString()))
                    DienThoaiCoDinhDiDong += "/" + tem["Mobile"].ToString();
                else
                    DienThoaiCoDinhDiDong = tem["Mobile"].ToString();
            obj.DienThoaiCoDinhDiDong = DienThoaiCoDinhDiDong;
            obj.Email = tem["Email"].ToString();
            lst.Add(obj);
        }

        ReportDocument rpt = new ReportDocument();
        rpt = CrystalReportControl.GetReportDocument(rpt.GetType());
        rpt.Load(Server.MapPath("Report/QuanLyThongTinChung/DanhSachNguoiQuanTamTrongCaNuoc.rpt"));

        //rpt.SetDataSource(lst);
        rpt.Database.Tables["objDanhSachNguoiQuanTamTrongCaNuoc"].SetDataSource(lst);
        //rpt.Database.Tables["lstOBJKyLuat"].SetDataSource(lstKyLuat);

        CrystalReportViewer1.ReportSource = rpt;
        CrystalReportViewer1.DataBind();

        if (Library.CheckNull(Request.Form["hdAction"]) == "word")
            rpt.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, "DanhSachNguoiQuanTamTrongCaNuoc");
        if (Library.CheckNull(Request.Form["hdAction"]) == "pdf")
        {
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "DanhSachNguoiQuanTamTrongCaNuoc");
            //CrystalDecisions.Shared.ExcelFormatOptions xlsFormatOptions = new ExcelFormatOptions();
            //xlsFormatOptions.ShowGridLines = true;
            //CrystalDecisions.Shared.ExportOptions CrExportOptions = new ExportOptions();
            //CrExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
            //rpt.ExportToHttpResponse(CrExportOptions.ExportFormatType, Response, true, "ThuMoiThamDuLopDTCNKT");

        }
        if (Library.CheckNull(Request.Form["hdAction"]) == "excel")
        //rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true,
        //                         "DanhSachCongTyKiemToanTrongCaNuoc");
        {
            string strNgayTinh = "(Tính đến ngày " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            string strNgayBaoCao = DateTime.Now.ToString("dd/MM/yyyy");
            ExportToExcel("DanhSachNguoiQuanTamTrongCaNuoc", lst, strNgayTinh, strNgayBaoCao);
        }
    }

    private void ExportToExcel(string strName, List<objDanhSachNguoiQuanTamTrongCaNuoc> lst, string NgayTinh, string NgayBC)
    {
        string outHTML = "";

        outHTML += "<table border = '1' cellpadding='5' cellspacing='0' style='font-family:Arial; font-size:8pt; color:#003366'>";
        outHTML += "<tr>";  //img src='http://" + Request.Url.Authority+ "/images/logo.png'
        outHTML += "<td colspan='1' style='border-style:none'></td>";
        //outHTML += "<td colspan='4' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/logo.png' alt='' border='0px'/></td>";
        outHTML += "<td colspan='8' align='left' width='197px' style='border-style:none' height='81px'><img src='http://" + Request.Url.Authority + "/images/ten.png' alt='' border='0px'/></td>";
        outHTML += "<td colspan='1' style='border-style:none'></td>";

        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='14' style='border-style:none; font-family:Times New Roman; font-size:14pt; color:Black' align='center' ><b>DANH SÁCH CÔNG TY KIỂM TOÁN TRONG NƯỚC</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='14' style='border-style:none; font-family:Times New Roman; font-size:12pt; color:Black' align='center' ><i>" + NgayTinh + "</i></td>";
        outHTML += "</tr>";
        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "    <th rowspan = '2'>STT</th>";
        outHTML += "    <th rowspan = '2'>ID.NQT</th>";
        outHTML += "    <th rowspan = '2'>Họ và tên</th>";
        outHTML += "    <th colspan = '2'>Ngày sinh</th>";
        outHTML += "    <th rowspan = '2'>Chức vụ</th>";
        outHTML += "    <th rowspan = '2'>Đơn vị công tác</th>";
        outHTML += "    <th rowspan = '2'>Trợ lý KTV CTKT</th>";
        outHTML += "    <th rowspan = '2'>Điện thoại cố định/ di động</th>";
        outHTML += "    <th rowspan = '2'>Email</th>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "    <th>Nam</th>";
        outHTML += "    <th>Nữ</th>";

        outHTML += "<tr>";
        outHTML += "    <th width='30px'>1</th>";
        outHTML += "    <th width='30px'>2</th>";
        outHTML += "    <th width='230px'>3</th>";
        outHTML += "    <th width='100px'>4</th>";
        outHTML += "    <th width='200px'>5</th>";
        outHTML += "    <th width='150px'>6</th>";
        outHTML += "    <th width='200px'>7</th>";
        outHTML += "    <th width='200px'>8</th>";
        outHTML += "    <th width='200px'>9</th>";
        outHTML += "    <th width='200px'>10</th>";
        outHTML += "</tr>";

        int i = 0;
        foreach (var temp in lst)
        {
            i++;
            outHTML += "<tr>";
            outHTML += "<td>" + i.ToString() + "</td>";
            outHTML += "<td>" + temp.IDNguoiQuanTam + "</td>";
            outHTML += "<td>" + temp.HoVaTen + "</td>";
            outHTML += "<td>" + temp.NgaySinhNam + "</td>";
            outHTML += "<td>" + temp.NgaySinhNu + "</td>";
            outHTML += "<td>" + temp.ChucVu + "</td>";
            outHTML += "<td>" + temp.DonViCongTac + "</td>";
            outHTML += "<td>" + temp.TroLyKTVCTKT + "</td>";
            outHTML += "<td>" + temp.DienThoaiCoDinhDiDong + "</td>";
            outHTML += "<td>" + temp.Email + "</td>";
            outHTML += "</tr>";
        }

        outHTML += "<tr style='border-style:none'></tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='8' style='border-style:none'></td>";
        outHTML += "<td colspan='2' align='center' style='border-style:none'><b>" + NgayBC + "</b></td>";
        outHTML += "</tr>";
        outHTML += "<tr>";
        outHTML += "<td colspan='8' style='border-style:none'></td>";
        outHTML += "<td colspan='2' align='center' style='border-style:none'><b>NGƯỜI LẬP BIỂU<b></td>";
        outHTML += "</tr>";

        outHTML += "</table>";

        Library.ExportToExcel(strName, outHTML);
    }

    protected string strBoChamPhay(string p)
    {
        int leng = p.Length;
        if (p.LastIndexOf(',') != -1)
            leng = p.LastIndexOf(',');
        string strOut = p.Substring(0, leng);
        strOut = strOut.Replace(".", string.Empty);
        return strOut;
    }

    protected void LoadCheckBox()
    {
        if (this.IsPostBack)
        {
            if (Request.Form["cotrolyktv"] != null)
            {
                Response.Write("$('#cotrolyktv').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#cotrolyktv').attr('checked', false); ");
            }
            if (Request.Form["khongtrolyktv"] != null)
            {
                Response.Write("$('#khongtrolyktv').attr('checked','checked'); ");
            }
            else
            {
                Response.Write("$('#khongtrolyktv').attr('checked', false); ");
            }
        }
    }
}