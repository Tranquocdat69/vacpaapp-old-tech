﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;


public partial class usercontrols_dmgiangvien_add : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    // Tên chức năng
    public string tenchucnang = "Giảng viên";

    public string bangdm = "tblDMGiangVien";
    public string quyen = "DmGiangVien";
    public string truongid = "GiangVienId";
    public string truongten = "TenGiangVien";
    public string truongchucvuid = "ChucVuId";
    public string truongtrinhdochuyenmonid = "TrinhDoChuyenMonId";
    public string truongdonvicongtac = "DonViCongTac";
    public string truongchungchi = "ChungChi";
    public string truonggioitinh = "GioiTinh";
    public string ngaySinh = "NgaySinh";
    public string hoiVienCaNhanId = "HoiVienCaNhanID";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.Form["Ten"]))
            {
                if (Request.Form["Ten"].ToString() != "")
                {
                    SqlCommand sql = new SqlCommand();

                    sql.CommandText = "INSERT INTO " + bangdm + " (" + truongten + "," + truongchucvuid + "," + truongtrinhdochuyenmonid + "," + truongdonvicongtac + "," + truongchungchi + "," + truonggioitinh + "," + ngaySinh + ", " + hoiVienCaNhanId + ") VALUES (@Ten, @ChucVu, @TrinhDo, @DonVi, @ChungChi, @GioiTinh, @NgaySinh, @HoiVienCaNhanID" + ")";
                    sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                    if (!string.IsNullOrEmpty(Request.Form["ChucVuId"]))
                        sql.Parameters.AddWithValue("@ChucVu", Request.Form["ChucVuId"]);
                    else
                        sql.Parameters.AddWithValue("@ChucVu", DBNull.Value);
                    if (!string.IsNullOrEmpty(Request.Form["TrinhDoChuyenMonId"]))
                        sql.Parameters.AddWithValue("@TrinhDo", Request.Form["TrinhDoChuyenMonId"]);
                    else
                        sql.Parameters.AddWithValue("@TrinhDo", DBNull.Value);
                    sql.Parameters.AddWithValue("@DonVi", Request.Form["DonViCongTac"]);
                    sql.Parameters.AddWithValue("@ChungChi", Request.Form["ChungChi"]);
                    if (!string.IsNullOrEmpty(Request.Form["GioiTinh"]))
                        sql.Parameters.AddWithValue("@GioiTinh", Request.Form["GioiTinh"]);
                    else
                        sql.Parameters.AddWithValue("@GioiTinh", DBNull.Value);
                    if (!string.IsNullOrEmpty(Request.Form["txtNgaySinh"]))
                        sql.Parameters.AddWithValue("@NgaySinh", Library.DateTimeConvert(Request.Form["txtNgaySinh"], "dd/MM/yyyy").ToString("yyyy-MM-dd"));
                    else
                        sql.Parameters.AddWithValue("@NgaySinh", DBNull.Value);
                    if (!string.IsNullOrEmpty(Request.Form["hdHocVienID"]))
                        sql.Parameters.AddWithValue("@HoiVienCaNhanID", Request.Form["hdHocVienID"]);
                    else
                        sql.Parameters.AddWithValue("@HoiVienCaNhanID", DBNull.Value);
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog(quyen, "Thêm giá trị \"" + Request.Form["Ten"] + "\" vào danh mục " + tenchucnang);

                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    public void vLoadChucVu(string strIdChucVu)
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM tblDmChucVu";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (strIdChucVu == dtr["ChucVuId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["ChucVuId"].ToString().Trim() + @""">" + dtr["TenChucVu"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["ChucVuId"].ToString().Trim() + @""">" + dtr["TenChucVu"] + @"</option>";
            }
        }


        ds = null;
        dt = null;
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void vLoadTrinhDoChuyenMon(string strIdTrinhDoChuyenMon)
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM tblDmTrinhDoChuyenMon";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (strIdTrinhDoChuyenMon == dtr["TrinhDoChuyenMonId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["TrinhDoChuyenMonId"].ToString().Trim() + @""">" + dtr["TenTrinhDoChuyenMon"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["TrinhDoChuyenMonId"].ToString().Trim() + @""">" + dtr["TenTrinhDoChuyenMon"] + @"</option>";
            }
        }

        ds = null;
        dt = null;
        System.Web.HttpContext.Current.Response.Write(output_html);
    }
}