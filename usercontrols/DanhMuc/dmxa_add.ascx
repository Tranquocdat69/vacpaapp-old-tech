﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dmxa_add.ascx.cs" Inherits="usercontrols_dmxa_add" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_dmxa_add" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_dmxa_add" clientidmode="Static" runat="server"   method="post">
    <div id="thongbaoloi_form_themdmxa" name="thongbaoloi_form_themdmxa" style="display:none" class="alert alert-error"></div>

      <table id="Table1" width="100%" border="0" class="formtbl" >
      <tr>       
                   <td><asp:Label ID="lblTinhThanhPho" runat="server" Text='Tỉnh/Thành phố:'></asp:Label>
                     <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td><select name="TinhID_ToChuc" id="TinhID_ToChuc">
          
          <%  
              try
              {
                  cm.Load_ThanhPho(dt.Rows[0]["DBHC_TinhID_ToChuc"].ToString().Trim());
              }
              catch 
              {
                  cm.Load_ThanhPho("00");
              }
              
              %>
          </select></td>
        </tr>
        <tr>
         <td><asp:Label ID="lblQuanHuyen" runat="server" ></asp:Label>
            <asp:Label ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
            <td ><select name="HuyenID_ToChuc" id="HuyenID_ToChuc">
             <%  
              try
              {
                  cm.Load_QuanHuyen(dt.Rows[0]["DBHC_HuyenID_ToChuc"].ToString().Trim(), dt.Rows[0]["DBHC_TinhID_ToChuc"].ToString().Trim());
              }
              catch 
              {
                  
              }
              
              %>
          </select></td>
        </tr>
        <tr>
          <td><asp:Label ID="lblTenXaPhuong" runat="server" ></asp:Label>
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="6"><input type="text" name="Ten" id="Ten"></td>
        </tr>
         <tr>
          <td><asp:Label ID="lblMaXaPhuong" runat="server" ></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="6"><input type="text" name="Ma" id="Ma"></td>
        </tr>
        
        <tr>
          <td><asp:Label ID="lblHieuLuc" runat="server" ></asp:Label></td>
          <%--<td colspan="6"><asp:CheckBox ID="HieuLuc"  runat="server" /></td>--%>
          <td colspan="6"><input type="checkbox" id="HieuLuc" name = "HieuLuc" onclick="validate()"></td>
        </tr>
        </table>
        
       
        
    </form>



           
                                                    
<script type="text/javascript">
       

        function submitform() {        
                jQuery("#form_dmxa_add").submit();
        }



    jQuery("#form_dmxa_add").validate({
        rules: {
            Ten: {
                required: true
            },
            Ma: {
                required: true
            },
             HuyenID_ToChuc: {
                required: true
            },
             TinhID_ToChuc: {
                required: true
            },
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery("#thongbaoloi_form_themdmxa").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_themdmxa").hide()
            }
        }
    });

      <% cm.chondiaphuong_script("ToChuc",0); %>

      
      function validate(){
   var vHieuLuc = document.getElementById('HieuLuc');
if (vHieuLuc.checked){
          vHieuLuc.value = 0;
}else{
vHieuLuc.value = 1;
}
}


    

</script>
