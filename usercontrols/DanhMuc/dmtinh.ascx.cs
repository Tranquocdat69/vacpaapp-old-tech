﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;

public partial class usercontrols_dmtinh : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();


    // Tên chức năng
    public string tenchucnang = "Tỉnh/Thành phố";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";
    
    public string bangdm = "tblDMTinh";
    public string quyen = "DmTinh";


    public string truongid = "TinhID";
    public string truongten = "TenTinh";
    public string truongma = "MaTinh";
    public string truonghieuluc = "HieuLuc";


    protected void Page_Load(object sender, EventArgs e)
    {
        Commons cm = new Commons();
        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");



        //switch (Request.QueryString["dm"])
        //{
        //    //case "dmtinh":
        //    //    tenchucnang = "Tỉnh/Thành phố";
        //    //    IconClass = "iconfa-star-empty";
        //    //    bangdm = "tblDMTinh";
        //    //    quyen = "DmTinh";
        //    //    truongid = "TinhID";
        //    //    truongten = "TenTinh";
        //    //    break;

        //    case "dmcauhoibimat":
        //        tenchucnang = "Câu hỏi bí mật";
        //        IconClass = "iconfa-question-sign";
        //        bangdm = "tblDMCauHoiBiMat";
        //        quyen = "DmCauHoiBiMat";
        //        truongid = "CauHoiBiMatID";
        //        truongten = "CauHoi";
        //        break;

        //    case "dmchucvu":
        //        tenchucnang = "Chức vụ";
        //        IconClass = "iconfa-star-empty";
        //        bangdm = "tblDMChucVu";
        //        quyen = "DmChucVu";
        //        truongid = "ChucVuID";
        //        truongten = "TenChucVu";
        //        break;

        //    case "dmtinhchi":
        //        tenchucnang = "Chứng chỉ";
        //        IconClass = "iconfa-credit-card";
        //        bangdm = "tblDMChungChi";
        //        quyen = "DmChungChi";
        //        truongid = "ChungChiID";
        //        truongten = "TenChungChi";
        //        break;

        //    case "dmchuyennganhdaotao":
        //        tenchucnang = "Chuyên ngành đào tạo";
        //        IconClass = "iconfa-book";
        //        bangdm = "tblDMChuyenNganhDaoTao";
        //        quyen = "DmChuyenNganhDaoTao";
        //        truongid = "ChuyenNganhDaoTaoID";
        //        truongten = "TenChuyenNganhDaoTao";
        //        break;

        //    case "dmhinhthuckhenthuong":
        //        tenchucnang = "Hình thức khen thưởng";
        //        IconClass = "iconfa-gift";
        //        bangdm = "tblDMHinhThucKhenThuong";
        //        quyen = "DmHinhThucKhenThuong";
        //        truongid = "HinhThucKhenThuongID";
        //        truongten = "TenHinhThucKhenThuong";
        //        break;

        //    case "dmhinhthuckyluat":
        //        tenchucnang = "Hình thức kỷ luật";
        //        IconClass = "iconfa-warning-sign";
        //        bangdm = "tblDMHinhThucKyLuat";
        //        quyen = "DmHinhThucKyLuat";
        //        truongid = "HinhThucKyLuatID";
        //        truongten = "TenHinhThucKyLuat";
        //        break;
        //    case "dmhocham":
        //        tenchucnang = "Học hàm";
        //        IconClass = "iconfa-briefcase";
        //        bangdm = "tblDMHocHam";
        //        quyen = "DmHocHam";
        //        truongid = "HocHamID";
        //        truongten = "TenHocHam";
        //        break;
        //    case "dmhocvi":
        //        tenchucnang = "Học vị";
        //        IconClass = "iconfa-bookmark";
        //        bangdm = "tblDMHocVi";
        //        quyen = "DmHocVi";
        //        truongid = "HocViID";
        //        truongten = "TenHocVi";
        //        break;
        //    case "dmloaihinhdoanhnghiep":
        //        tenchucnang = "Loại hình doanh nghiệp";
        //        IconClass = "iconfa-legal";
        //        bangdm = "tblDMLoaiHinhDoanhNghiep";
        //        quyen = "DmLoaiHinhDoanhNghiep";
        //        truongid = "LoaiHinhDoanhNghiepID";
        //        truongten = "TenLoaiHinhDoanhNghiep";
        //        break;
        //    case "dmtruongdaihoc":
        //        tenchucnang = "Trường đại học";
        //        IconClass = "iconfa-bell";
        //        bangdm = "tblDMTruongDaiHoc";
        //        quyen = "DmTruongDaiHoc";
        //        truongid = "TruongDaiHocID";
        //        truongten = "TenTruongDaiHoc";
        //        break;
        //    case "dmtrinhdochuyenmon":
        //        tenchucnang = "Trình độ chuyên môn";
        //        IconClass = "iconfa-fire";
        //        bangdm = "tblDMTrinhDoChuyenMon";
        //        quyen = "DmTrinhDoChuyenMon";
        //        truongid = "TrinhDoChuyenMonID";
        //        truongten = "TenTrinhDoChuyenMon";
        //        break;
        //    case "dmloaiphi":
        //        tenchucnang = "Loại phí";
        //        IconClass = "iconfa-tags";
        //        bangdm = "tblDMLoaiPhi";
        //        quyen = "DmLoaiPhi";
        //        truongid = "LoaiPhiID";
        //        truongten = "TenLoaiPhi";
        //        break;
        //    case "dmloaitien":
        //        tenchucnang = "Loại tiền";
        //        IconClass = "iconfa-money";
        //        bangdm = "tblDMLoaiTien";
        //        quyen = "DmLoaiTien";
        //        truongid = "LoaiTienID";
        //        truongten = "TenLoaiTien";
        //        break;
        //    case "dmnganhang":
        //        tenchucnang = "Ngân hàng";
        //        IconClass = "iconfa-key";
        //        bangdm = "tblDMNganHang";
        //        quyen = "DmNganHang";
        //        truongid = "NganHangID";
        //        truongten = "TenNganHang";
        //        break;
        //}




        dmtinh_grv.Columns[1].HeaderText = "Tỉnh/Thành phố";
        dmtinh_grv.Columns[1].SortExpression = truongten;
        dmtinh_grv.Columns[2].HeaderText = "Mã Tỉnh";
        dmtinh_grv.Columns[2].SortExpression = truongma;
        dmtinh_grv.Columns[3].HeaderText = "Thao tác";
        


        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>Danh mục " + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>Danh mục " + tenchucnang + @"</h1> </div>"));


        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            Form1.Visible = false;
            return;
        }




        ///////////


        load_data();


        try
        {
            if (Request.QueryString["act"] == "delete")
            {

                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
                {

                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "DELETE FROM " + bangdm + " WHERE " + truongid + " IN (" + Request.QueryString["id"] + ")";
                    DataAccess.RunActionCmd(sql);

                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog(quyen, "Xóa bản ghi có ID \"" + Request.QueryString["id"] + "\" của danh mục " + tenchucnang);

                    Response.Redirect("admin.aspx?page=dmtinh&dm=" + Request.QueryString["dm"]);
                }
            }



        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }



    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void load_data()
    {
        try
        {
            int i;
            Commons cm = new Commons();
            // Tìm kiếm
            SelectQueryBuilder query = new SelectQueryBuilder();
            query.SelectFromTable(bangdm);
            query.SelectAllColumns();


            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                query.AddWhere(truongten, Comparison.Like, "%" + Request.Form["timkiem_Ten"] + "%");

            SqlCommand sql = new SqlCommand();
            sql.CommandText = query.BuildQuery();
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;

            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            // danh sách trang
            Pager.Items.Clear();
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }
            else
            {
                objPds.CurrentPageIndex = 0;
                Pager.SelectedIndex = 0;
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = quyen + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }

            dmtinh_grv.DataSource = objPds;

            dmtinh_grv.DataBind();
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void dmtinh_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        dmtinh_grv.PageIndex = e.NewPageIndex;
        dmtinh_grv.DataBind();
    }

    protected void dmtinh_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }
}