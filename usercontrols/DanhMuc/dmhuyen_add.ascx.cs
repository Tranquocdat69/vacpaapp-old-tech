﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_dmhuyen_add : System.Web.UI.UserControl
{

    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    // Tên chức năng
    public string tenchucnang = "Quận/Huyện";

    public string bangdm = "tblDMHuyen";
    public string quyen = "DmHuyen";
    public string truongid = "HuyenID";
    public string truongten = "TenHuyen";
    public string truongma = "MaHuyen";
    public string truongmatinh = "MaTinh";
    public string truonghieuluc = "HieuLuc";
    public string strHieuLuc = "1";
    protected void Page_Load(object sender, EventArgs e)
    {
        Label1.Text = tenchucnang + ":";
        Label3.Text = "Mã Quận/Huyện";
        Label5.Text = "Hết hiệu lực";
        Label6.Text = "Tỉnh/Thành phố";


        try
        {
            if (!string.IsNullOrEmpty(Request.Form["Ten"]) || !string.IsNullOrEmpty(Request.Form["Ma"]) || !string.IsNullOrEmpty(Request.Form["TinhID_ToChuc"]))
            {
                if (Request.Form["HieuLuc"] != null)
                {
                    strHieuLuc = Request.Form["HieuLuc"];
                }
                SqlCommand sql = new SqlCommand();
                string a = Request.Form["TinhID_ToChuc"];
                sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE (" + truongten + " = @Ten and " + truongmatinh + " = @MaTinh) or " + truongma + " =@Ma";
                sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                sql.Parameters.AddWithValue("@MaTinh", Request.Form["TinhID_ToChuc"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Đã có bản ghi với giá trị \"" + Request.Form["Ten"].Trim() + " hoặc " + Request.Form["Ma"].Trim() + "\"</div>"));
                }
                else
                {
                    sql = new SqlCommand();
                    sql.CommandText = "INSERT INTO " + bangdm + " (" + truongten + "," + truongma + "," + truongmatinh + "," + truonghieuluc + ") VALUES (@Ten, @Ma, @MaTinh, " + strHieuLuc + ")";
                    sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                    sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                    sql.Parameters.AddWithValue("@MaTinh", Request.Form["TinhID_ToChuc"]);
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog(quyen, "Thêm giá trị \"" + Request.Form["Ten"] + "\"" + Request.Form["Ma"] + "\"" + Request.Form["TinhID_ToChuc"] + "\" vào danh mục " + tenchucnang);

                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
}