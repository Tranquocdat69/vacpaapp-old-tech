﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_dmsothich_add : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    // Tên chức năng
    public string tenchucnang = "";

    public string bangdm = "";
    public string quyen = "";
    public string truongid = "";
    public string truongten = "";
    public string truongma = "";

    protected void Page_Load(object sender, EventArgs e)
    {
                tenchucnang = "Sở thích của kiểm toán viên";
                bangdm = "tblDMSoThich";
                quyen = "DMSoThich";
                truongid = "SoThichID";
                truongten = "TenSoThich";
                truongma = "MaSoThich";


        Label1.Text = tenchucnang + ":";

        try
        {

            if (!string.IsNullOrEmpty(Request.Form["Ten"]) && !string.IsNullOrEmpty(Request.Form["Ma"]))
            {

                SqlCommand sql = new SqlCommand();

                sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE " + truongma + " = @Ma";
                sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Đã có bản ghi với giá trị \"" + Request.Form["Ma"].Trim() + "\"</div>"));
                }
                else
                {
                    sql = new SqlCommand();
                    sql.CommandText = "INSERT INTO " + bangdm + " (" + truongten + ", " + truongma + ") VALUES (@Ten, @Ma)";
                    sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                    sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog(quyen, "Thêm giá trị \"" + Request.Form["Ma"] + "\" vào danh mục " + tenchucnang);

                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                }
            }

        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }


}