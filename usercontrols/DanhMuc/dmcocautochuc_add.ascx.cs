﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_dmcocautochuc_add : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    // Tên chức năng
    public string tenchucnang = "Bộ phận";

    public string bangdm = "tblCoCauToChuc";
    public string quyen = "CoCauToChuc";
    public string truongid = "CoCauToChucId";
    public string truongten = "TenBoPhan";
    public string truongma = "MaBoPhan";
    public string truongidcha = "CoCauToChucCapTrenId";
    public string truonghieuluc = "HieuLuc";
    public string strHieuLuc = "1";
    protected void Page_Load(object sender, EventArgs e)
    {
        //Label1.Text = tenchucnang + ":";
        //Label3.Text = "Mã Bộ phận";
        //Label5.Text = "Hết hiệu lực";
        //Label6.Text = "Bộ phận cấp trên";

       // vLoadCoCauToChuc();
        try
        {
            if (!string.IsNullOrEmpty(Request.Form["Ten"]) || !string.IsNullOrEmpty(Request.Form["Ma"]) || !string.IsNullOrEmpty(Request.Form["CoCauToChucCapTrenId"]))
            {
                if (Request.Form["HieuLuc"] != null)
                {
                    strHieuLuc = Request.Form["HieuLuc"];
                }
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE (" + truongten + " = @Ten) or " + truongma + " =@Ma";
                sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Đã có bản ghi với giá trị \"" + Request.Form["Ten"].Trim() + " hoặc " + Request.Form["Ma"].Trim() + "\"</div>"));
                }
                else
                {
                    sql = new SqlCommand();
                    if (Request.Form["CoCauToChucCapTrenId"].ToString() == "0")
                    {
                        sql.CommandText = "INSERT INTO " + bangdm + " (" + truongten + "," + truongma + "," + truongidcha + "," + truonghieuluc + ") VALUES (@Ten, @Ma, NULL, " + strHieuLuc + ")";
                        sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                        sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                    }
                    else
                    {

                        sql.CommandText = "INSERT INTO " + bangdm + " (" + truongten + "," + truongma + "," + truongidcha + "," + truonghieuluc + ") VALUES (@Ten, @Ma, @IdCha, " + strHieuLuc + ")";
                        sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                        sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                        sql.Parameters.AddWithValue("@IdCha", Request.Form["CoCauToChucCapTrenId"].ToString());
                    }
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    cm.ghilog(quyen, "Thêm giá trị \"" + Request.Form["Ten"] + "\"" + Request.Form["Ma"] + "\" vào danh mục " + tenchucnang);
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                    //vLoadCoCauToChuc();
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    public void vLoadCoCauToChuc(string CoCauToChucId = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT CoCauToChucId, MaBoPhan, CoCauToChucCapTrenId, TenBoPhan, HieuLuc FROM tblCoCauToChuc WHERE HieuLuc='1' AND CoCauToChucCapTrenId IS NULL";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        dt = ds.Tables[0];

        output_html += @"<option selected=""selected""" + " value=\"0\"><< Không có Bộ phận cấp trên >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            int level = 0;
            string strIdCha = dtr["CoCauToChucId"].ToString();
            if (CoCauToChucId == dtr["CoCauToChucId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["CoCauToChucId"].ToString().Trim() + @""">" + dtr["TenBoPhan"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["CoCauToChucId"].ToString().Trim() + @""">" + dtr["TenBoPhan"] + @"</option>";
            }
            vLoadCoCauToChuc1(CoCauToChucId, strIdCha,level,ref output_html);
        }


        ds = null;

        SqlCommand sqlDT = new SqlCommand();
        sqlDT.CommandText = "SELECT CoCauToChucId, MaBoPhan, CoCauToChucCapTrenId, TenBoPhan, HieuLuc FROM tblCoCauToChuc WHERE HieuLuc='1'";
        dt = DataAccess.RunCMDGetDataSet(sqlDT).Tables[0];

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    private void vLoadCoCauToChuc1(string strCoCauToChucId, string strIdCha, int level,ref string output_html)
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT CoCauToChucId, MaBoPhan, CoCauToChucCapTrenId, TenBoPhan, HieuLuc FROM tblCoCauToChuc WHERE HieuLuc='1' AND CoCauToChucCapTrenId = '" +strIdCha + "'";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        dt = ds.Tables[0];

        level++;
        foreach (DataRow dtr in dt.Rows)
        {
            string strTenBoPhan = new String('−', level * 2) + dtr["TenBoPhan"].ToString();
            string strId = dtr["CoCauToChucId"].ToString();
            if (strCoCauToChucId == dtr["CoCauToChucId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["CoCauToChucId"].ToString().Trim() + @""">" + strTenBoPhan + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["CoCauToChucId"].ToString().Trim() + @""">" + strTenBoPhan + @"</option>";
            }
            vLoadCoCauToChuc1(strCoCauToChucId, strId, level, ref output_html);
        }


        ds = null;
        //dt = null;
    }
}