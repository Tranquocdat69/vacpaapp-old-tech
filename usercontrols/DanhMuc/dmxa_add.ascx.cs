﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;


public partial class usercontrols_dmxa_add : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    // Tên chức năng
    public string tenchucnang = "Xã/Phường";

    public string bangdm = "tblDMXa";
    public string quyen = "DmXa";
    public string truongid = "XaID";
    public string truongten = "TenXa";
    public string truongma = "MaXa";
    public string truongmahuyen = "MaHuyen";
    public string truongmatinh = "MaTinh";
    public string truonghieuluc = "HieuLuc";
    public string strHieuLuc = "1";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblTenXaPhuong.Text = tenchucnang + ":";
        lblMaXaPhuong.Text = "Mã Xã/Phường";
        lblHieuLuc.Text = "Hết hiệu lực";
        lblTinhThanhPho.Text = "Tỉnh/Thành phố";
        lblQuanHuyen.Text = "Quận/Huyện";

        try
        {
            if (!string.IsNullOrEmpty(Request.Form["Ten"]) || !string.IsNullOrEmpty(Request.Form["Ma"]) || !string.IsNullOrEmpty(Request.Form["HuyenID_ToChuc"]))
            {
                //if (!IsPostBack)
                //{
                    //HieuLuc.Attributes["HieuLuc"]
                if(Request.Form["Ten"].ToString() != "" &&
                    Request.Form["Ma"].ToString() != "")
                {
                    if (Request.Form["HieuLuc"] != null)
                    {
                        strHieuLuc = Request.Form["HieuLuc"];
                    }
                    SqlCommand sql = new SqlCommand();
                    // string a = Request.Form["ctl06$MaHuyen"];
                    sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE (" + truongten + " = @Ten and " + truongmahuyen + " = @MaHuyen) or " + truongma + " =@Ma";
                    sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                    sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                    sql.Parameters.AddWithValue("@MaHuyen", Request.Form["HuyenID_ToChuc"]);
                    DataSet ds = new DataSet();
                    ds = DataAccess.RunCMDGetDataSet(sql);
                    if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
                    {
                        ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Đã có bản ghi với giá trị \"" + Request.Form["Ten"].Trim() + " hoặc " + Request.Form["Ma"].Trim() + "\"</div>"));
                    }
                    else
                    {
                        sql = new SqlCommand();
                        sql.CommandText = "INSERT INTO " + bangdm + " (" + truongten + "," + truongma + "," + truongmahuyen + "," + truongmatinh + "," + truonghieuluc + ") VALUES (@Ten, @Ma, @MaHuyen, @MaTinh, " + strHieuLuc + ")";
                        sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                        sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                        sql.Parameters.AddWithValue("@MaHuyen", Request.Form["HuyenID_ToChuc"]);
                        sql.Parameters.AddWithValue("@MaTinh", Request.Form["TinhID_ToChuc"]);
                        DataAccess.RunActionCmd(sql);
                        sql.Connection.Close();
                        sql.Connection.Dispose();
                        sql = null;

                        cm.ghilog(quyen, "Thêm giá trị \"" + Request.Form["Ten"] + "\"" + Request.Form["Ma"] + "\"" + Request.Form["HuyenID_ToChuc"] + "\"" + Request.Form["TinhID_ToChuc"] + "\" vào danh mục " + tenchucnang);

                        ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    //protected void MaTinh_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //        SqlCommand sqlHuyen = new SqlCommand();

    //        sqlHuyen.CommandText = "SELECT MAHUYEN, TENHUYEN FROM tblDmHuyen WHERE MATINH = @MaTinh";
    //        sqlHuyen.Parameters.AddWithValue("@MaTinh", MaTinh.SelectedValue);
    //        DataSet dsHuyen = new DataSet();
    //        dsHuyen = DataAccess.RunCMDGetDataSet(sqlHuyen);
    //        MaHuyen.DataSource = dsHuyen.Tables[0];
    //        MaHuyen.DataTextField = "TENHUYEN";
    //        MaHuyen.DataValueField = "MAHUYEN";
    //        MaHuyen.DataBind();
    //}

   
}