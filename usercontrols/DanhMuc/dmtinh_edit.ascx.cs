﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_dmtinh_edit : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    // Tên chức năng
    public string tenchucnang = "Tỉnh/Thành phố";

    public string bangdm = "tblDMTinh";
    public string quyen = "DmTinh";
    public string truongid = "TinhID";
    public string truongten = "TenTinh";
    public string truongma = "MaTinh";
    public string truonghieuluc = "HieuLuc";
    public string strHieuLuc = "1";
    protected void Page_Load(object sender, EventArgs e)
    {

        Label1.Text = tenchucnang + ":";
        Label3.Text = "Mã Tỉnh/Thành phố";
        Label5.Text = "Hết hiệu lực";
        try
        {

            if (!string.IsNullOrEmpty(Request.Form["Ten"]) || !string.IsNullOrEmpty(Request.Form["Ma"]))
            {
                if (Request.Form["HieuLuc"] != null)
                {
                    strHieuLuc = Request.Form["HieuLuc"];
                }
                SqlCommand sql = new SqlCommand();

                sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE (" + truongten + " = @Ten OR " + truongma + " = @Ma ) AND " + truongid + " <> " + Request.QueryString["id"];
                sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);

                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Đã có bản ghi với giá trị \"" + Request.Form["Ten"].Trim() + "\"</div>"));
                }
                else
                {
                    sql = new SqlCommand();
                    sql.CommandText = "UPDATE " + bangdm + " SET " + truongten + " = @Ten," + truongma + " = @Ma, " + truonghieuluc + "=@HieuLuc "+ " WHERE " + truongid + " = " + Request.QueryString["id"];
                    sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                    sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                    sql.Parameters.AddWithValue("@HieuLuc", strHieuLuc);
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    cm.ghilog(quyen, "Cập nhật giá trị \"" + Request.Form["Ten"] + "\"" +  Request.Form["Ma"] + "\" vào danh mục " + tenchucnang);
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                }
            }

        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }




    protected void load_giatricu()
    {
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT " + truongten + ", " + truongma + "," + truonghieuluc + " FROM " + bangdm + " WHERE " + truongid + "=" + Request.QueryString["id"];
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);

        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        Response.Write("$('#Ten').val('" + cm.AddSlashes(ds.Tables[0].Rows[0][0].ToString()) + "');" + System.Environment.NewLine);
        Response.Write("$('#Ma').val('" + cm.AddSlashes(ds.Tables[0].Rows[0][1].ToString()) + "');" + System.Environment.NewLine);
        if (ds.Tables[0].Rows[0][2].ToString() == "0")
        {
            Response.Write("$('#HieuLuc').attr('checked','checked'); ");
            Response.Write("$('#HieuLuc').attr('value','0'); ");
        }
        ds.Dispose();

        if (Request.QueryString["mode"] == "view")
        {
            Response.Write("$('#form_dmtinh_add input,select,textarea').attr('disabled', true);");
        }
    }
}