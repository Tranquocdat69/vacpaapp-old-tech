﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
public partial class usercontrols_dmgiangvien_edit : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();

    // Tên chức năng
    public string tenchucnang = "Giảng viên";

    public string bangdm = "tblDMGiangVien";
    public string quyen = "DmGiangVien";
    public string truongid = "GiangVienId";
    public string truongten = "TenGiangVien";
    public string truongchucvuid = "ChucVuId";
    public string truongtrinhdochuyenmonid = "TrinhDoChuyenMonId";
    public string truongdonvicongtac = "DonViCongTac";
    public string truongchungchi = "ChungChi";
    public string truonggioitinh = "GioiTinh";
    public string ngaySinh = "NgaySinh";
    public string hoiVienCaNhanId = "HoiVienCaNhanID";
    protected void Page_Load(object sender, EventArgs e)
    {

        SqlCommand sqlTemp = new SqlCommand();
        sqlTemp.CommandText = "SELECT " + truongid + ", " + truongten + ", " + truongchucvuid + "," + truongtrinhdochuyenmonid + "," + truongdonvicongtac + "," + truongchungchi + "," + truonggioitinh + " FROM " + bangdm + " WHERE " + truongid + "=" + Request.QueryString["id"];
        DataSet dsTemp = DataAccess.RunCMDGetDataSet(sqlTemp);
        dt = dsTemp.Tables[0];
        sqlTemp.Connection.Close();
        sqlTemp.Connection.Dispose();
        sqlTemp = null;

        try
        {

            if (!string.IsNullOrEmpty(Request.Form["Ten"]))
            {

                SqlCommand sql = new SqlCommand();

                sql.CommandText = "UPDATE " + bangdm + " SET " + truongten + " = @Ten," + truongchucvuid + " = @ChucVu, " + truongtrinhdochuyenmonid + "= @TrinhDo, " + truongdonvicongtac + "= @DonVi, " + truongchungchi + " = @ChungChi," + truonggioitinh + "= @GioiTinh, " + ngaySinh + "= @NgaySinh, " + hoiVienCaNhanId + "= @HoiVienCaNhanID" + " WHERE " + truongid + " = " + Request.QueryString["id"];
                sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                if (!string.IsNullOrEmpty(Request.Form["ChucVuId"]))
                    sql.Parameters.AddWithValue("@ChucVu", Request.Form["ChucVuId"]);
                else
                    sql.Parameters.AddWithValue("@ChucVu", DBNull.Value);
                if (!string.IsNullOrEmpty(Request.Form["TrinhDoChuyenMonId"]))
                    sql.Parameters.AddWithValue("@TrinhDo", Request.Form["TrinhDoChuyenMonId"]);
                else
                    sql.Parameters.AddWithValue("@TrinhDo", DBNull.Value);
                sql.Parameters.AddWithValue("@DonVi", Request.Form["DonViCongTac"]);
                sql.Parameters.AddWithValue("@ChungChi", Request.Form["ChungChi"]);
                if (!string.IsNullOrEmpty(Request.Form["GioiTinh"]))
                    sql.Parameters.AddWithValue("@GioiTinh", Request.Form["GioiTinh"]);
                else
                    sql.Parameters.AddWithValue("@GioiTinh", DBNull.Value);
                if (!string.IsNullOrEmpty(Request.Form["txtNgaySinh"]))
                    sql.Parameters.AddWithValue("@NgaySinh", Library.DateTimeConvert(Request.Form["txtNgaySinh"], "dd/MM/yyyy").ToString("yyyy-MM-dd"));
                else
                    sql.Parameters.AddWithValue("@NgaySinh", DBNull.Value);
                if (!string.IsNullOrEmpty(Request.Form["hdHocVienID"]))
                    sql.Parameters.AddWithValue("@HoiVienCaNhanID", Request.Form["hdHocVienID"]);
                else
                    sql.Parameters.AddWithValue("@HoiVienCaNhanID", DBNull.Value);
                DataAccess.RunActionCmd(sql);
                sql.Connection.Close();
                sql.Connection.Dispose();
                sql = null;
                cm.ghilog(quyen, "Cập nhật giá trị \"" + Request.Form["Ten"] + "\" vào danh mục " + tenchucnang);
                ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    protected void load_giatricu()
    {
        SqlCommand sql = new SqlCommand();
        //sql.CommandText = "SELECT " + truongten + ", " + truongma + "," + truongmahuyen + "," + truongmatinh + "," + truonghieuluc + " FROM " + bangdm + " WHERE " + truongid + "=" + Request.QueryString["id"];
        sql.CommandText = "SELECT " + truongten + ", " + bangdm + "." + truongchucvuid + "," + truongtrinhdochuyenmonid + "," + bangdm + "." + truongdonvicongtac + "," + truongchungchi + "," + bangdm + "." + truonggioitinh + "," + bangdm + "." + ngaySinh + ", " + bangdm + "." + hoiVienCaNhanId + ", tblHoiVienCaNhan.MaHoiVienCaNhan FROM " + bangdm + " LEFT JOIN tblHoiVienCaNhan ON " + bangdm + ".HoiVienCaNhanID = tblHoiVienCaNhan.HoiVienCaNhanID WHERE " + truongid + "=" + Request.QueryString["id"];
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        dt = ds.Tables[0];
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        Response.Write("$('#Ten').val('" + cm.AddSlashes(ds.Tables[0].Rows[0][0].ToString()) + "');" + System.Environment.NewLine);
        Response.Write("$('#ChucVuId').val('" + cm.AddSlashes(ds.Tables[0].Rows[0][1].ToString().TrimEnd()) + "');" + System.Environment.NewLine);
        Response.Write("$('#TrinhDoChuyenMonId').val('" + dt.Rows[0][2].ToString().TrimEnd() + "');" + System.Environment.NewLine);
        Response.Write("$('#DonViCongTac').val('" + dt.Rows[0][3].ToString().TrimEnd() + "');" + System.Environment.NewLine);
        Response.Write("$('#ChungChi').val('" + dt.Rows[0][4].ToString().TrimEnd() + "');" + System.Environment.NewLine);
        if (!string.IsNullOrEmpty(dt.Rows[0][6].ToString()))
            Response.Write("$('#txtNgaySinh').val('" + Library.DateTimeConvert(dt.Rows[0][6].ToString().TrimEnd()).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
        Response.Write("$('#hdHocVienID').val('" + dt.Rows[0][7].ToString().TrimEnd() + "');" + System.Environment.NewLine);
        Response.Write("$('#txtMaHoiVienCaNhan').val('" + dt.Rows[0][8].ToString().TrimEnd() + "');" + System.Environment.NewLine);
        //Response.Write("$('#GioiTinh').val('" + dt.Rows[0][5].ToString().TrimEnd() + "');" + System.Environment.NewLine);

        if (ds.Tables[0].Rows[0][5].ToString() == "F")
        {
            Response.Write("$('#GioiTinhNu').attr('checked','checked'); ");
            //Response.Write("$('#GioiTinh').attr('value','F'); ");
        }
        else if (ds.Tables[0].Rows[0][5].ToString() == "M")
        {
            Response.Write("$('#GioiTinhNam').attr('checked','checked'); ");
            //Response.Write("$('#GioiTinh').attr('value','M'); ");
        }
        ds.Dispose();

        if (Request.QueryString["mode"] == "view")
        {
            Response.Write("$('#form_dmhuyen_add input,select,textarea').attr('disabled', true);");
        }
    }

    public void vLoadChucVu(string strIdChucVu)
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM tblDmChucVu";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (strIdChucVu == dtr["ChucVuId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["ChucVuId"].ToString().Trim() + @""">" + dtr["TenChucVu"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["ChucVuId"].ToString().Trim() + @""">" + dtr["TenChucVu"] + @"</option>";
            }
        }


        ds = null;
        dt = null;
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void vLoadTrinhDoChuyenMon(string strIdTrinhDoChuyenMon)
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM tblDmTrinhDoChuyenMon";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (strIdTrinhDoChuyenMon == dtr["TrinhDoChuyenMonId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["TrinhDoChuyenMonId"].ToString().Trim() + @""">" + dtr["TenTrinhDoChuyenMon"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["TrinhDoChuyenMonId"].ToString().Trim() + @""">" + dtr["TenTrinhDoChuyenMon"] + @"</option>";
            }
        }

        ds = null;
        dt = null;
        System.Web.HttpContext.Current.Response.Write(output_html);
    }
}