﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;

public partial class usercontrols_dmgiangvien : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();

    // Tên chức năng
    public string tenchucnang = "Danh mục Giảng viên";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";

    public string bangdm = "tblDMGiangVien";
    public string quyen = "DmGiangVien";


    public string truongid = "GiangVienID";
    public string truongma = "MaGiangVien";
    public string truongten = "TenGiangVien";
    public string truongchucvu = "TenChucVu";
    public string truongtrinhdochuyenmon = "TenTrinhDoChuyenMon";
    public string truongdonvicongtac = "DonViCongTac";
    public string truongchungchi = "ChungChi";
    public string truonggioitinh = "GioiTinh";

    protected void Page_Load(object sender, EventArgs e)
    {
        Commons cm = new Commons();
        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");
        dmgiangvien_grv.Columns[1].HeaderText = "Mã Giảng viên";
        dmgiangvien_grv.Columns[2].HeaderText = "Giảng viên";
        dmgiangvien_grv.Columns[2].SortExpression = truongten;
        dmgiangvien_grv.Columns[3].HeaderText = "Chức vụ";
        dmgiangvien_grv.Columns[4].HeaderText = "Trình độ chuyên môn";
        dmgiangvien_grv.Columns[5].HeaderText = "Đơn vị công tác";
        dmgiangvien_grv.Columns[6].HeaderText = "Chứng chỉ";
        dmgiangvien_grv.Columns[7].HeaderText = "Giới tính";


        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>Danh mục " + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>Danh mục " + tenchucnang + @"</h1> </div>"));


        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            Form1.Visible = false;
            return;
        }




        ///////////


        load_data();


        try
        {
            if (Request.QueryString["act"] == "delete")
            {

                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
                {
                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "DELETE FROM " + bangdm + " WHERE " + truongid + " IN (" + Request.QueryString["id"] + ")";
                    DataAccess.RunActionCmd(sql);
                    cm.ghilog(quyen, "Xóa bản ghi có ID \"" + Request.QueryString["id"] + "\" của danh mục " + tenchucnang);

                    Response.Redirect("admin.aspx?page=dmgiangvien");
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                }
            }
        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }



    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void load_data()
    {
        try
        {
            int i;
            Commons cm = new Commons();
            // Tìm kiếm
            //SelectQueryBuilder query = new SelectQueryBuilder();
            //query.SelectFromTable(bangdm);

            //query.AddJoin(JoinType.LeftJoin, "tblDMChucVu", "ChucVuID", Comparison.Equals, bangdm, "ChucVuID");
            //query.AddJoin(JoinType.LeftJoin, "tblDMTrinhDoChuyenMon", "TrinhDoChuyenMonId", Comparison.Equals, bangdm, "TrinhDoChuyenMonId");
            //query.SelectColumns("GiangVienID","TenGiangVien", "tblDMChucVu.TenChucVu", "TenTrinhDoChuyenMon", "DonViCongTac", "ChungChi", "GioiTinh");
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            //    query.AddWhere(truongten, Comparison.Like, "%" + Request.Form["timkiem_Ten"] + "%");

            //SqlCommand sql = new SqlCommand();

            //sql.CommandText = query.BuildQuery();
            //DataSet ds = DataAccess.RunCMDGetDataSet(sql);

            //minhlp đang làm dở
            SqlCommand sql = new SqlCommand();
            sql.CommandText = @"SELECT GiangVienID, TenGiangVien, MaGiangVien, TenChucVu, TenTrinhDoChuyenMon, DonViCongTac, ChungChi, CAST(CASE A.GioiTinh WHEN 'M' THEN N'Nam' WHEN 'F' THEN N'Nữ' ELSE  '' END AS NVARCHAR(20)) AS GioiTinh, NgaySinh from tblDMGiangVien A left join tblDMChucVu B on A.ChucVuId = B.ChucVuId left join tblDMTrinhDoChuyenMon C on A.TrinhDoChuyenMonId = C.TrinhDoChuyenMonId
            where 0 = 0";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                sql.CommandText += " AND A.TenGiangVien like N'%" + Request.Form["timkiem_Ten"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["timkiem_DonViCongTac"]))
                sql.CommandText += " AND A.DonViCongTac like N'%" + Request.Form["timkiem_DonViCongTac"] + "%'";
            if (!string.IsNullOrEmpty(Request.Form["ChucVuId"]))
                sql.CommandText += " AND A.ChucVuId = " + Request.Form["ChucVuId"] + "";
            if (!string.IsNullOrEmpty(Request.Form["TrinhDoChuyenMonId"]))
                sql.CommandText += " AND A.TrinhDoChuyenMonId = " + Request.Form["TrinhDoChuyenMonId"] + "";

            DataSet ds = new DataSet();
            ds = DataAccess.RunCMDGetDataSet(sql);

            DataView dt = ds.Tables[0].DefaultView;

            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = dt;
            objPds.AllowPaging = false;
            objPds.PageSize = 10;

            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = quyen + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    //objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }

            dmgiangvien_grv.DataSource = objPds;
            dmgiangvien_grv.DataBind();
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void dmgiangvien_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        dmgiangvien_grv.PageIndex = e.NewPageIndex;
        dmgiangvien_grv.DataBind();
    }

    protected void dmgiangvien_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }

    //public string KiemTraXoa()
    //{
    //    if (!string.IsNullOrEmpty(strIdXoa))
    //    {
    //        SqlCommand sql = new SqlCommand();
    //        sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE " + truongidcha + " IN (" + strIdXoa + ")";
    //        DataSet ds = new DataSet();
    //        ds = DataAccess.RunCMDGetDataSet(sql);
    //        if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
    //        {
    //            return "false".ToLower();
    //        }
    //        else
    //        {
    //            return "true".ToLower();
    //        }
    //    }
    //    else
    //        return "false".ToLower();
    //}

    public void vLoadChucVu(string strIdChucVu)
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM tblDmChucVu";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (strIdChucVu == dtr["ChucVuId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["ChucVuId"].ToString().Trim() + @""">" + dtr["TenChucVu"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["ChucVuId"].ToString().Trim() + @""">" + dtr["TenChucVu"] + @"</option>";
            }
        }


        ds = null;
        dt = null;
        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    public void vLoadTrinhDoChuyenMon(string strIdTrinhDoChuyenMon)
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT * FROM tblDmTrinhDoChuyenMon";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        dt = ds.Tables[0];

        output_html += "<option value=\"\"><< Lựa chọn >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            if (strIdTrinhDoChuyenMon == dtr["TrinhDoChuyenMonId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["TrinhDoChuyenMonId"].ToString().Trim() + @""">" + dtr["TenTrinhDoChuyenMon"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["TrinhDoChuyenMonId"].ToString().Trim() + @""">" + dtr["TenTrinhDoChuyenMon"] + @"</option>";
            }
        }

        ds = null;
        dt = null;
        System.Web.HttpContext.Current.Response.Write(output_html);
    }
}