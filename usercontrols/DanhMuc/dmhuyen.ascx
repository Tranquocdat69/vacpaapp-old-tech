﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dmhuyen.ascx.cs" Inherits="usercontrols_dmhuyen" %>

 <asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

   

   <form id="Form1" name="Form1" runat="server">
   <h4 class="widgettitle">Danh mục <%=tenchucnang%></h4>
   <div>
        <div  class="dataTables_length">
        
        <a id="btn_them"  href="#none" class="btn btn-rounded" onclick="open_dmhuyen_add();"><i class="iconfa-plus-sign"></i> Thêm</a>
        <a id="btn_xoa"  href="#none" class="btn btn-rounded" onclick="confirm_delete_dmhuyen(dmhuyen_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
        <a href="#none" class="btn btn-rounded" onclick="open_dmhuyen_search();"><i class="iconfa-search"></i> Tìm kiếm</a>
        <a id="btn_ketxuat"   href="#none" class="btn btn-rounded" onclick="export_excel()" ><i class="iconfa-save"></i> Kết xuất</a>
        </div>
        
   </div>

<asp:GridView ClientIDMode="Static" ID="dmhuyen_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="True" AllowSorting="True" 
       onpageindexchanging="dmhuyen_grv_PageIndexChanging" onsorting="dmhuyen_grv_Sorting"    
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval(truongid)%>" />
                  </ItemTemplate>
              </asp:TemplateField>

             
               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongten)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

             <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongma)%></span>
                  </ItemTemplate>
              </asp:TemplateField>
                
                <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongtentinh)%></span>
                  </ItemTemplate>
              </asp:TemplateField>     
              <asp:TemplateField HeaderText='' ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                  <ItemTemplate>
                      <div class="btn-group">
                      
                      <a onclick="open_dmhuyen_view(<%# Eval(truongid)%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xem"  rel="tooltip" class="btn"><i class="iconsweets-trashcan2" ></i></a>
                      <a onclick="open_dmhuyen_edit(<%# Eval(truongid)%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Sửa"  rel="tooltip" class="btn"><i class="iconsweets-create" ></i></a>
                      <a onclick="confirm_delete_dmhuyen(<%# Eval(truongid)%>);" data-placement="top" data-rel="tooltip" href="#none" data-original-title="Xóa"  rel="tooltip" class="btn" ><i class="iconsweets-trashcan" ></i></a>                                                   
                                        </div>
                  </ItemTemplate>
              </asp:TemplateField>


          </Columns>
</asp:GridView>


<div class="dataTables_info" id="dyntable_info" >
<div class="pagination pagination-mini">Chuyển đến trang: 
                            <ul>
                               
                                <li><a href="#" onclick="$('#tranghientai').val(0); $('#user_search').submit();"><< Đầu tiên</a></li>
                                <li><a href="#"  onclick="if ($('#Pager').val()>0) {$('#tranghientai').val($('#Pager').val()-1); $('#user_search').submit();}" >< Trước</a></li>
                                <li><a style=" border: none;  background-color:#eeeeee"><asp:DropDownList ID="Pager" name="Pager" ClientIDMode="Static" runat="server" 
                                        style=" width:55px; height:22px; "
                                        onchange="$('#tranghientai').val(this.value); $('#user_search').submit();" >
                                     </asp:DropDownList></a>  </li>
                                <li style="margin-left:5px; "><a href="#"  onclick="if ($('#Pager').val() < ($('#Pager option').length - 1)) {$('#tranghientai').val(parseInt($('#Pager').val())+1); $('#user_search').submit();}  ;"   style="border-left: 1px solid #ccc;">Sau ></a></li>
                                <li><a href="#"  onclick="$('#tranghientai').val($('#Pager option').length-1); $('#user_search').submit();">Cuối cùng >></a></li>
                            </ul>
                        </div>                      
</div>



                                     


</form>





<script type="text/javascript">

<% annut(); %>

    // Array ID được check
    var dmhuyen_grv_selected = new Array();

    jQuery(document).ready(function () {
        // dynamic table      

        $("#dmhuyen_grv .checkall").bind("click", function () {
            dmhuyen_grv_selected = new Array();
            checked = $(this).prop("checked");
            $('#dmhuyen_grv :checkbox').each(function () {
                $(this).prop("checked", checked);
                if ((checked) && ($(this).val() != "all")) dmhuyen_grv_selected.push($(this).val());
            });
        });

        $('#dmhuyen_grv :checkbox').each(function () {
            $(this).bind("click", function () {
                removeItem(dmhuyen_grv_selected, $(this).val())
                checked = $(this).prop("checked");
                if ((checked) && ($(this).val() != "all")) dmhuyen_grv_selected.push($(this).val());
            });
        });
    });


    // Xác nhận xóa tỉnh
    function confirm_delete_dmhuyen(idxoa) {

        $("#div_dmhuyen_delete").dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Xóa": function () {
                    window.location = 'admin.aspx?page=dmhuyen&act=delete&id=' + idxoa;
                },
                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });
        $('#div_dmhuyen_delete').parent().find('button:contains("Xóa")').addClass('btn btn-rounded').prepend('<i class="iconfa-trash"> </i>&nbsp;');
        $('#div_dmhuyen_delete').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Sửa thông tin tỉnh
    function open_dmhuyen_edit(id) {
        var timestamp = Number(new Date());

        $("#div_dmhuyen_add").empty();
        $("#div_dmhuyen_add").append($("<iframe width='100%' height='100%' id='iframe_dmhuyen_add' name='iframe_dmhuyen_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmhuyen_edit&id=" + id + "&mode=edit&time=" + timestamp));
        $("#div_dmhuyen_add").dialog({
            resizable: true,
             width: 400,
            height: 290,
            title: "<img src='images/icons/application_form_edit.png'>&nbsp;<b>Sửa <%=tenchucnang%></b>",
            modal: true,
            close: function (event, ui) { window.location = 'admin.aspx?page=dmhuyen'; },
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_dmhuyen_add'].submitform();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_dmhuyen_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_dmhuyen_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }


    // Xem thông tin tỉnh
    function open_dmhuyen_view(id) {
        var timestamp = Number(new Date());

        $("#div_dmhuyen_add").empty();
        $("#div_dmhuyen_add").append($("<iframe width='100%' height='100%' id='iframe_dmhuyen_add' name='iframe_dmhuyen_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmhuyen_edit&id=" + id + "&mode=view&time=" + timestamp));
        $("#div_dmhuyen_add").dialog({
            resizable: true,
             width: 400,
            height: 290,
            title: "<img src='images/icons/application_form_edit.png'>&nbsp;<b>Xem <%=tenchucnang%></b>",
            modal: true,
            buttons: {

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_dmhuyen_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }



    function open_dmhuyen_add() {
        var timestamp = Number(new Date());

        $("#div_dmhuyen_add").empty();
        $("#div_dmhuyen_add").append($("<iframe width='100%' height='100%' id='iframe_dmhuyen_add' name='iframe_dmhuyen_add' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=dmhuyen_add&mode=iframe&time=" + timestamp));
        $("#div_dmhuyen_add").dialog({
            resizable: true,
             width: 400,
            height: 290,
            title: "<img src='images/icons/add.png'>&nbsp;<b>Thêm <%=tenchucnang%></b>",
            modal: true,
            close: function (event, ui) { window.location = 'admin.aspx?page=dmhuyen'; },
            buttons: {

                "Ghi": function () {
                    window.frames['iframe_dmhuyen_add'].submitform();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_dmhuyen_add').parent().find('button:contains("Ghi")').addClass('btn btn-rounded').prepend('<i class="iconfa-ok"> </i>&nbsp;');
        $('#div_dmhuyen_add').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    // Tìm kiếm
    function open_dmhuyen_search() {
        var timestamp = Number(new Date());

        $("#div_dmhuyen_search").dialog({
            resizable: true,
            width: 400,
            height: 290,
            title: "<img src='images/icons/application_form_magnify.png'>&nbsp;<b>Tìm kiếm</b>",
            modal: true,
            buttons: {

                "Tìm kiếm": function () {
                    $(this).find("form#user_search").submit();
                },

                "Bỏ qua": function () {
                    $(this).dialog("close");
                }
            }

        });

        $('#div_dmhuyen_search').parent().find('button:contains("Tìm kiếm")').addClass('btn btn-rounded').prepend('<i class="iconfa-search"> </i>&nbsp;');
        $('#div_dmhuyen_search').parent().find('button:contains("Bỏ qua")').addClass('btn btn-rounded').prepend('<i class="iconfa-minus-sign"> </i>&nbsp;');
    }

    function export_excel()
    {
        $('#ketxuat').val('1');
        $('#user_search').submit();
        $('#ketxuat').val('0');
    }


    // Hiển thị tooltip
    if (jQuery('#dmhuyen_grv').length > 0) jQuery('#dmhuyen_grv').tooltip({ selector: "a[rel=tooltip]" });


   

</script>

<div id="div_dmhuyen_add" >
</div>

<div id="div_dmhuyen_search" style="display:none" >
<form id="user_search" method="post" enctype="multipart/form-data" >
<table  width="100%" border="0" class="formtbl"  >
    <tr>
        <td style="width:200px"><label><%=tenchucnang%>: </label></td>
        <td><input type="text" name="timkiem_Ten" id="timkiem_Ten" value="<%=Request.Form["timkiem_Ten"]%>"  class="input-xlarge" />
        <input type="hidden" value="0" id="tranghientai" name="tranghientai" />
        <input type="hidden" value="0" id="ketxuat" name="ketxuat" /> 
        
        </td>
    </tr>
    
</table>
</form>
</div>

<div id="div_dmhuyen_delete" style="display:none" title="<img src='images/icons/application_form_delete.png'> <b>Xác nhận xóa</b>">

  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Xóa (các) bản ghi được chọn?</p>

</div>




