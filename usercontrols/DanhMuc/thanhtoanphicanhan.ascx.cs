﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
using System.Text;
using CodeEngine.Framework.QueryBuilder;

public partial class usercontrols_thanhtoanphicanhan : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    public DataTable dtbPhiCaNhanChiTiet;
    public DataTable dtbPhiCaNhan;

    public DataSet ds = new DataSet();

    public string quyen = "ThanhToanPhiCaNhan";

    public string tenchucnang = "Thanh toán phí cá nhân";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";
    public int iTinhTrang = 0;
    
    public string truongphiid = "PhiID";
    public string truonghoten = "HoTen";
    public string truongsoccktv = "SoChungChiKTV";
    public string truongdonvicongtac = "DonViCongTac";
    //public string truongsophiphainop = "";
    //public string truongsophidanop = "";
    public string truongsophiconno = "SoPhiConNo";

    protected string truonghoivienid = "HoiVienID";
    protected string truongmahoivien = "MaHoiVien";
    protected string truongstt = "STT";
    protected string truongmaphi = "MaPhi";
    protected string truongloaiphi = "TenLoaiPhi";
    protected string truongsophiphainop = "TongTien";
    protected string truongsophidanop = "TienNop";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Commons cm = new Commons();
            if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

            Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>" + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

            Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>" + tenchucnang + @"</h1> </div>"));

            if (!Page.IsPostBack)
            {
                thanhtoanphicanhan_grv.Columns[1].HeaderText = "STT";
                thanhtoanphicanhan_grv.Columns[1].SortExpression = truongstt;
                thanhtoanphicanhan_grv.Columns[2].HeaderText = "HVCN ID";
                thanhtoanphicanhan_grv.Columns[2].SortExpression = truongmahoivien;
                thanhtoanphicanhan_grv.Columns[3].HeaderText = "Họ và tên";
                thanhtoanphicanhan_grv.Columns[3].SortExpression = truonghoten;
                thanhtoanphicanhan_grv.Columns[4].HeaderText = "Số CCKTV";
                thanhtoanphicanhan_grv.Columns[4].SortExpression = truongsoccktv;
                thanhtoanphicanhan_grv.Columns[5].HeaderText = "Ngày cấp CCKTV";
                thanhtoanphicanhan_grv.Columns[5].SortExpression = "ngaycapchungchiktv";
                thanhtoanphicanhan_grv.Columns[6].HeaderText = "Đơn vị công tác";
                thanhtoanphicanhan_grv.Columns[6].SortExpression = truongdonvicongtac;
                thanhtoanphicanhan_grv.Columns[7].HeaderText = "Số phí phải nộp";
                thanhtoanphicanhan_grv.Columns[7].SortExpression = truongsophiphainop;
                thanhtoanphicanhan_grv.Columns[8].HeaderText = "Số phí đã nộp";
                thanhtoanphicanhan_grv.Columns[8].SortExpression = truongsophidanop;
                thanhtoanphicanhan_grv.Columns[9].HeaderText = "Số phí còn nợ";
                thanhtoanphicanhan_grv.Columns[9].SortExpression = truongsophiconno;
                ReLoadDTB();
                if (dtbPhiCaNhan == null)
                {
                    TaoDataTableThanhToanPhiCaNhan();
                }
                RefeshGrvPhiCaNhan();
            }
            else
            {
                RefeshGrvPhiCaNhan();
            }

                //Thêm mới
                //Khi thêm mới thì không có request id,  mã phí          
                if (string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    if (thanhtoanphicanhan_grv.Rows.Count > 0)
                    {
                        if (string.IsNullOrEmpty(Request.Form["MaGiaoDich"]) && !string.IsNullOrEmpty(Request.Form["NgayNop"]) && !string.IsNullOrEmpty(Request.Form["DienGiai"]))
                        {
                            //Chạy function tự sinh mã phí
                            string Nam = "";
                            Nam = DateTime.ParseExact(Request.Form["NgayNop"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).Year.ToString();
                            Nam = Nam.Substring(2);
                            SqlCommand cmd = new SqlCommand();
                            cmd.CommandText = "select [dbo].[LaySoChayThanhToanPhiCaNhan] (" + Nam + ")";
                            ds = DataAccess.RunCMDGetDataSet(cmd);

                            string MaGiaoDich = ds.Tables[0].Rows[0][0].ToString();

                            DateTime? NgayNop = null;
                            NgayNop = DateTime.ParseExact(Request.Form["NgayNop"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            DateTime NgayNhap = DateTime.Now;

                            using (SqlConnection connection = new SqlConnection(DataAccess.ConnString))
                            {
                                connection.Open();
                                SqlCommand sql = connection.CreateCommand();
                                SqlTransaction transaction;
                                transaction = connection.BeginTransaction();
                                sql.Connection = connection;
                                sql.Transaction = transaction;

                                try
                                {
                                    sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhan_Insert]", connection, transaction);
                                    sql.CommandType = CommandType.StoredProcedure;

                                    sql.Parameters.AddWithValue("@MaGiaoDich", MaGiaoDich);
                                    if (NgayNop != null)
                                        sql.Parameters.AddWithValue("@NgayNop", NgayNop);
                                    else
                                        sql.Parameters.AddWithValue("@NgayNop", DBNull.Value);
                                    sql.Parameters.AddWithValue("@DienGiai", Request.Form["DienGiai"]);

                                    sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                                    sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                                    sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);

                                    sql.Parameters.AddWithValue("@NgayDuyet", DBNull.Value);
                                    sql.Parameters.AddWithValue("@NguoiDuyet", DBNull.Value);

                                    sql.Parameters.Add("@GiaoDichID", SqlDbType.Int);
                                    sql.Parameters["@GiaoDichID"].Direction = ParameterDirection.Output;

                                    //DataAccess.RunActionCmd(sql);
                                    sql.ExecuteNonQuery();

                                    int? IDGiaoDich = null;
                                    if (sql.Parameters["@GiaoDichID"] != null)
                                        IDGiaoDich = (int)sql.Parameters["@GiaoDichID"].Value;

                                    int? IDGiaoDichChiTietHoiVien = null;
                                    int? IDGiaoDichChiTietCNKT = null;
                                    int? IDGiaoDichChiTietKSCL = null;
                                    int? IDGiaoDichChiTietKhac = null;
                                    foreach (DataRow dtr in dtbPhiCaNhan.Rows)
                                    {
                                        if (dtr["PhiHoiVienDaNop"] != null)
                                            if (!string.IsNullOrEmpty(dtr["PhiHoiVienDaNop"].ToString()) && dtr["PhiIDHoiVien"].ToString() != "0")
                                            {
                                        sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhanChiTiet_Insert]", connection, transaction);
                                        sql.CommandType = CommandType.StoredProcedure;
                                        sql.Parameters.AddWithValue("@GiaoDichID", IDGiaoDich);
                                        sql.Parameters.AddWithValue("@HoiVienCaNhanID", dtr[truonghoivienid].ToString());
                                        sql.Parameters.AddWithValue("@PhatSinhPhiID", dtr["PhatSinhPhiIDHoiVien"]);
                                        sql.Parameters.AddWithValue("@PhiID", dtr["PhiIDHoiVien"]);
                                        sql.Parameters.AddWithValue("@LoaiPhi", (int)EnumVACPA.LoaiPhi.PhiHoiVien);
                                        sql.Parameters.AddWithValue("@SoTienNop", dtr["PhiHoiVienDaNop"]);
                                        sql.Parameters.AddWithValue("@DoiTuongNopPhi", (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan);
                                        sql.Parameters.Add("@GiaoDichChiTietID", SqlDbType.Int);
                                        sql.Parameters["@GiaoDichChiTietID"].Direction = ParameterDirection.Output;

                                        //DataAccess.RunActionCmd(sql);
                                        sql.ExecuteNonQuery();

                                        if (sql.Parameters["@GiaoDichChiTietID"] != null)
                                            IDGiaoDichChiTietHoiVien = (int)sql.Parameters["@GiaoDichChiTietID"].Value;

                                            }

                                        if (dtr["PhiCNKTDaNop"] != null)
                                            if (!string.IsNullOrEmpty(dtr["PhiCNKTDaNop"].ToString()) && dtr["PhiCNKTDaNop"].ToString() != "0")
                                            {
                                        sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhanChiTiet_Insert]", connection, transaction);
                                        sql.CommandType = CommandType.StoredProcedure;
                                        sql.Parameters.AddWithValue("@GiaoDichID", IDGiaoDich);
                                        sql.Parameters.AddWithValue("@HoiVienCaNhanID", dtr[truonghoivienid].ToString());
                                        sql.Parameters.AddWithValue("@PhatSinhPhiID", dtr["PhatSinhPhiIDCNKT"]);
                                        sql.Parameters.AddWithValue("@PhiID", DBNull.Value);
                                        sql.Parameters.AddWithValue("@LoaiPhi", (int)EnumVACPA.LoaiPhi.PhiCapNhatKienThuc);
                                        sql.Parameters.AddWithValue("@SoTienNop", dtr["PhiCNKTDaNop"]);
                                        sql.Parameters.AddWithValue("@DoiTuongNopPhi", (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan);
                                        sql.Parameters.Add("@GiaoDichChiTietID", SqlDbType.Int);
                                        sql.Parameters["@GiaoDichChiTietID"].Direction = ParameterDirection.Output;

                                        //DataAccess.RunActionCmd(sql);
                                        sql.ExecuteNonQuery();


                                        if (sql.Parameters["@GiaoDichChiTietID"] != null)
                                            IDGiaoDichChiTietCNKT = (int)sql.Parameters["@GiaoDichChiTietID"].Value;

                                            }

                                        if (dtr["PhiKSCLDaNop"] != null)
                                            if (!string.IsNullOrEmpty(dtr["PhiKSCLDaNop"].ToString()) && dtr["PhiIDKSCL"].ToString() != "0")
                                            {
                                        sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhanChiTiet_Insert]", connection, transaction);
                                        sql.CommandType = CommandType.StoredProcedure;
                                        sql.Parameters.AddWithValue("@GiaoDichID", IDGiaoDich);
                                        sql.Parameters.AddWithValue("@HoiVienCaNhanID", dtr[truonghoivienid].ToString());
                                        sql.Parameters.AddWithValue("@PhatSinhPhiID", dtr["PhatSinhPhiIDKSCL"]);
                                        sql.Parameters.AddWithValue("@PhiID", dtr["PhiIDKSCL"]);
                                        sql.Parameters.AddWithValue("@LoaiPhi", (int)EnumVACPA.LoaiPhi.PhiKiemSoatChatLuong);
                                        sql.Parameters.AddWithValue("@SoTienNop", dtr["PhiKSCLDaNop"]);
                                        sql.Parameters.AddWithValue("@DoiTuongNopPhi", (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan);
                                        sql.Parameters.Add("@GiaoDichChiTietID", SqlDbType.Int);
                                        sql.Parameters["@GiaoDichChiTietID"].Direction = ParameterDirection.Output;

                                        //DataAccess.RunActionCmd(sql);
                                        sql.ExecuteNonQuery();
                                        if (sql.Parameters["@GiaoDichChiTietID"] != null)
                                            IDGiaoDichChiTietKSCL = (int)sql.Parameters["@GiaoDichChiTietID"].Value;

                                            }


                                        if (dtr["PhiKhacDaNop"] != null)
                                            if (!string.IsNullOrEmpty(dtr["PhiKhacDaNop"].ToString()) && dtr["PhiIDKhac"].ToString() != "0")
                                            {
                                                sql = new SqlCommand("[dbo].[tblTTNopPhiCaNhanChiTiet_Insert]", connection, transaction);
                                                sql.CommandType = CommandType.StoredProcedure;
                                                sql.Parameters.AddWithValue("@GiaoDichID", IDGiaoDich);
                                                sql.Parameters.AddWithValue("@HoiVienCaNhanID", dtr[truonghoivienid].ToString());
                                                sql.Parameters.AddWithValue("@PhatSinhPhiID", dtr["PhatSinhPhiIDKhac"]);
                                                sql.Parameters.AddWithValue("@PhiID", dtr["PhiIDKhac"]);
                                                sql.Parameters.AddWithValue("@LoaiPhi", (int)EnumVACPA.LoaiPhi.PhiKhac);
                                                sql.Parameters.AddWithValue("@SoTienNop", dtr["PhiKhacDaNop"]);
                                                sql.Parameters.AddWithValue("@DoiTuongNopPhi", (int)EnumVACPA.DoiTuongNopPhi.HoiVienCaNhan);
                                                sql.Parameters.Add("@GiaoDichChiTietID", SqlDbType.Int);
                                                sql.Parameters["@GiaoDichChiTietID"].Direction = ParameterDirection.Output;

                                                //DataAccess.RunActionCmd(sql);
                                                sql.ExecuteNonQuery();
                                                if (sql.Parameters["@GiaoDichChiTietID"] != null)
                                                    IDGiaoDichChiTietKSCL = (int)sql.Parameters["@GiaoDichChiTietID"].Value;

                                            }
                                    }

                                    transaction.Commit();
                                    sql.Connection.Close();
                                    sql.Connection.Dispose();
                                    sql = null;
                                    cm.ghilog("TTNopPhiCaNhan", "Thêm giá trị \"" + Request.Form["MaGiaoDich"] + "\" vào danh mục TTNopPhiCaNhan");
                                    if (IDGiaoDichChiTietHoiVien != null)
                                        cm.ghilog("TTNopPhiCaNhanChiTiet", "Thêm bản ghi với ID \"" + IDGiaoDichChiTietHoiVien + "\" vào danh mục TTNopPhiCaNhanChiTiet");
                                    if (IDGiaoDichChiTietCNKT != null)
                                        cm.ghilog("TTNopPhiCaNhanChiTiet", "Thêm bản ghi với ID \"" + IDGiaoDichChiTietCNKT + "\" vào danh mục TTNopPhiCaNhanChiTiet");
                                    if (IDGiaoDichChiTietKSCL != null)
                                        cm.ghilog("TTNopPhiCaNhanChiTiet", "Thêm bản ghi với ID \"" + IDGiaoDichChiTietKSCL + "\" vào danh mục TTNopPhiCaNhanChiTiet");
                                    if (IDGiaoDichChiTietKhac != null)
                                        cm.ghilog("TTNopPhiCaNhanChiTiet", "Thêm bản ghi với ID \"" + IDGiaoDichChiTietKhac + "\" vào danh mục TTNopPhiCaNhanChiTiet");
                                }
                                catch
                                {
                                    transaction.Rollback();
                                }
                            }

                            Response.Redirect("admin.aspx?page=thanhtoanphicanhan", false);
                        }
                    }
                }
                else
                {
                    if (Request.QueryString["mode"] == "edit") //Sửa
                    {
                        if (!string.IsNullOrEmpty(Request.Form["TenPhi"]))
                        {
                            string strTenPhi = Request.Form["TenPhi"];
                            string strMucPhi = Request.Form["MucPhi"];

                            var modified = new StringBuilder();
                            foreach (char c in strMucPhi)
                            {
                                if (Char.IsDigit(c) || c == ',')
                                    modified.Append(c);
                            }

                            decimal MucPhi = decimal.Parse(modified.ToString());
                            DateTime? NgayLap1 = null;
                            if (!string.IsNullOrEmpty(Request.Form["NgayLap"]))
                                NgayLap1 = DateTime.ParseExact(Request.Form["NgayLap"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime? NgayApDung1 = null;
                            if (!string.IsNullOrEmpty(Request.Form["NgayApDung"]))
                                NgayApDung1 = DateTime.ParseExact(Request.Form["NgayApDung"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            DateTime? NgayHetHieuLuc1 = null;
                            if (!string.IsNullOrEmpty(Request.Form["NgayHetHieuLuc"]))
                                NgayHetHieuLuc1 = DateTime.ParseExact(Request.Form["NgayHetHieuLuc"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            DateTime NgayNhap = DateTime.Now;

                            int? ThangNop = null;
                            if (!string.IsNullOrEmpty(Request.Form["ThangNop"]))
                                ThangNop = Convert.ToInt32(Request.Form["ThangNop"]);

                            SqlCommand sql = new SqlCommand();

                            sql.CommandText = @"UPDATE tblTTDMPhiHoiVien set NgayLap =@NgayLap, TenPhi = @TenPhi,
                        NgayApDung = @NgayApDung, NgayHetHieuLuc = @NgayHetHieuLuc, DoiTuongApDungID = @DoiTuongApDung,
                        MucPhi = @MucPhi, DonViThoiGian = @DonViThoiGian, ThoiHanThang = @ThoiHanThang,
                        ThoiHanNgay =@ThoiHanNgay, TinhTrangID = @TinhTrangID, NgayNhap = @NgayNhap, 
                        NguoiNhap =@NguoiNhap
                        WHERE PhiID = " + Request.QueryString["id"];
                            if (NgayLap1 != null)
                                sql.Parameters.AddWithValue("@NgayLap", NgayLap1);
                            else
                                sql.Parameters.AddWithValue("@NgayLap", DBNull.Value);
                            sql.Parameters.AddWithValue("@TenPhi", Request.Form["TenPhi"]);
                            if (NgayApDung1 != null)
                                sql.Parameters.AddWithValue("@NgayApDung", NgayApDung1);
                            else
                                sql.Parameters.AddWithValue("@NgayApDung", DBNull.Value);
                            if (NgayHetHieuLuc1 != null)
                                sql.Parameters.AddWithValue("@NgayHetHieuLuc", NgayHetHieuLuc1);
                            else
                                sql.Parameters.AddWithValue("@NgayHetHieuLuc", DBNull.Value);
                            sql.Parameters.AddWithValue("@DoiTuongApDung ", Request.Form["DoiTuongApDung"]);
                            sql.Parameters.AddWithValue("@MucPhi", MucPhi);
                            sql.Parameters.AddWithValue("@DonViThoiGian", Request.Form["DonViThoiGian"]);
                            if (ThangNop != null)
                                sql.Parameters.AddWithValue("@ThoiHanThang", ThangNop);
                            else
                                sql.Parameters.AddWithValue("@ThoiHanThang", DBNull.Value);
                            sql.Parameters.AddWithValue("@ThoiHanNgay", Request.Form["NgayNop"]);
                            sql.Parameters.AddWithValue("@TinhTrangID", EnumVACPA.TinhTrang.ChoDuyet);
                            sql.Parameters.AddWithValue("@NgayNhap", NgayNhap);
                            sql.Parameters.AddWithValue("@NguoiNhap", cm.Admin_TenDangNhap);
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;

                            cm.ghilog("TTDMPhiHoiVien", "Sửa giá trị \"" + Request.Form["MaPhi"] + "\" vào danh mục TTDMPhiHoiVien");

                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                    else if (!string.IsNullOrEmpty(Request.QueryString["tinhtrang"]))//Các hành động từ chối, duyệt, thoái duyệt, xóa
                    {
                        //Từ chối
                        if (Request.QueryString["tinhtrang"] == "tuchoi")
                        {
                            SqlCommand sql = new SqlCommand();
                            sql.CommandText = "UPDATE tblTTDMPhiHoiVien set TinhTrangId = " + (int)EnumVACPA.TinhTrang.KhongPheDuyet + " WHERE PhiId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTDMPhiHoiVien", "Từ chối duyệt giá trị \"" + Request.Form["MaPhi"] + "\" của danh mục TTDMPhiHoiVien");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else if (Request.QueryString["tinhtrang"] == "xoa") //Xóa
                        {
                            SqlCommand sql = new SqlCommand();
                            sql.CommandText = "DELETE tblTTDMPhiHoiVien WHERE PhiId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTDMPhiHoiVien", "Xóa giá trị \"" + Request.Form["MaPhi"] + "\" của danh mục TTDMPhiHoiVien");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                        else if (Request.QueryString["tinhtrang"] == "duyet") //duyệt
                        {

                            SqlCommand sql = new SqlCommand();
                            sql.CommandText = "UPDATE tblTTDMPhiHoiVien set TinhTrangId = " + (int)EnumVACPA.TinhTrang.DaPheDuyet + " WHERE PhiId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTDMPhiHoiVien", "Duyệt giá trị \"" + Request.Form["MaPhi"] + "\" của danh mục TTDMPhiHoiVien");

                        }
                        else if (Request.QueryString["tinhtrang"] == "thoaiduyet") //Thoái duyệt
                        {
                            SqlCommand sql = new SqlCommand();
                            sql.CommandText = "UPDATE tblTTDMPhiHoiVien set TinhTrangId = " + (int)EnumVACPA.TinhTrang.ThoaiDuyet + " WHERE PhiId = " + Request.QueryString["id"];
                            DataAccess.RunActionCmd(sql);
                            sql.Connection.Close();
                            sql.Connection.Dispose();
                            sql = null;
                            cm.ghilog("TTDMPhiHoiVien", "Thoái duyệt giá trị \"" + Request.Form["MaPhi"] + "\" vào danh mục TTDMPhiHoiVien");
                            Response.Redirect("admin.aspx?page=quanlyphidanhsachphi");
                        }
                    }
                }
           
        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

    public void LoadNut()
    {
        string output_html = "<div  class='dataTables_length'>";
        output_html += @"<a id='btn_luu' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_sua' href='#none' class='btn btn-rounded' onclick='luu();'><i class='iconfa-plus-sign'></i> Lưu</a>
            <a id='btn_xoa' href='#none' class='btn btn-rounded' onclick='xoa(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-sign'></i> Xóa</a>
            <a id='btn_duyet' href='#none' class='btn btn-rounded' onclick='duyet(" + Request.QueryString["id"] + @");'><i class='iconfa-ok'></i> Duyệt</a>
            <a id='btn_tuchoi' href='#none' class='btn btn-rounded' onclick='tuchoi(" + Request.QueryString["id"] + @");'><i class='iconfa-remove'></i> Từ chối</a>
            <a id='btn_thoaiduyet' href='#none' class='btn btn-rounded' onclick='thoaiduyet(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Thoái duyệt</a>
            <a id='btn_guiemail' href='#none' class='btn btn-rounded' onclick='guiemail(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Gửi email</a>
            <a id='btn_ketxuat' href='#none' class='btn btn-rounded' onclick='ketxuat(" + Request.QueryString["id"] + @");'><i class='iconfa-remove-circle'></i> Kết xuất</a>";
        output_html += "</div>";
        System.Web.HttpContext.Current.Response.Write(output_html + System.Environment.NewLine);
    }

    protected void AnNut()
    {
        Commons cm = new Commons();

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("DUYET|"))
        {
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("TUCHOI|"))
        {
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("THOAIDUYET|"))
        {
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("GUIEMAIL|"))
        {
            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), "ThanhToanPhiCaNhan", cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();" + System.Environment.NewLine);
        }
    }

    protected void LoadThongTin()
    {
        //Xem hoặc sửa
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
            if (Request.QueryString["mode"] == "view" || Request.QueryString["mode"] == "edit") //Xem hoặc sửa
            {
                SqlCommand sql = new SqlCommand();
                sql.CommandText = "select MaPhi, NgayLap, TenPhi, NgayApDung, NgayHetHieuLuc, DoiTuongApDungID, "
                    + " MucPhi, DonViThoiGian, ThoiHanThang, ThoiHanNgay, TinhTrangID, TenTrangThai "
                    + " from tblTTDMPhiHoiVien inner join tblDMTrangThai on tblTTDMPhiHoiVien.TinhTrangID = tblDMTrangThai.TrangThaiID "
                + " where PhiId = " + Convert.ToInt16(Request.QueryString["id"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                dt = ds.Tables[0];
                int iTinhTrangBanGhi = 0;
                string strTinhTrangBanGhi = "";
                DateTime? NgayCoHieuLuc = new DateTime();
                DateTime? NgayHetHieuLuc = new DateTime();
                DateTime? NgayTraCuu = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                if (dt.Rows.Count > 0)
                {
                    Response.Write("$('#MaPhi').val('" + dt.Rows[0]["MaPhi"] + "');" + System.Environment.NewLine);
                    if (dt.Rows[0]["NgayLap"] != DBNull.Value)
                        Response.Write("$('#NgayLap').val('" + Convert.ToDateTime(dt.Rows[0]["NgayLap"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                    Response.Write("$('#TenPhi').val('" + dt.Rows[0]["TenPhi"] + "');" + System.Environment.NewLine);
                    if (dt.Rows[0]["NgayApDung"] != DBNull.Value)
                    {
                        Response.Write("$('#NgayApDung').val('" + Convert.ToDateTime(dt.Rows[0]["NgayApDung"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                        NgayCoHieuLuc = Convert.ToDateTime(dt.Rows[0]["NgayApDung"]);
                    }
                    else
                        NgayCoHieuLuc = null;
                    if (dt.Rows[0]["NgayHetHieuLuc"] != DBNull.Value)
                    {
                        Response.Write("$('#NgayHetHieuLuc').val('" + Convert.ToDateTime(dt.Rows[0]["NgayHetHieuLuc"]).ToString("dd/MM/yyyy") + "');" + System.Environment.NewLine);
                        NgayHetHieuLuc = Convert.ToDateTime(dt.Rows[0]["NgayHetHieuLuc"]);
                    }
                    else
                        NgayHetHieuLuc = null;
                    Response.Write("$('#DoiTuongApDung').val('" + dt.Rows[0]["DoiTuongApDungID"] + "');" + System.Environment.NewLine);
                    Response.Write("$('#MucPhi').val('" + dt.Rows[0]["MucPhi"] + "');" + System.Environment.NewLine);
                    Response.Write("$('#DonViThoiGian').val('" + dt.Rows[0]["DonViThoiGian"].ToString().Trim() + "');" + System.Environment.NewLine);

                    //Với thời hạn tháng khác null thì load tháng và ngày bình thường
                    if (dt.Rows[0]["ThoiHanThang"] != DBNull.Value)
                    {
                        Response.Write("$('#ThangNop').val('" + dt.Rows[0]["ThoiHanThang"].ToString().Trim() + "');" + System.Environment.NewLine);

                        Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                        Response.Write(@"$('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=1')" + System.Environment.NewLine);
                    }
                    //Với thời hạn tháng = null => xử lý tháng và ngày
                    else
                    {
                        if (Request.QueryString["mode"] == "view")
                        {
                            Response.Write("$('#ThangNop').html('');" + System.Environment.NewLine);
                            Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                        }
                        else
                        {
                            Response.Write("$('#ThangNop').val('');" + System.Environment.NewLine);
                            Response.Write("$('#ThangNop').attr('disabled', true);" + System.Environment.NewLine);

                            Response.Write("$('#NgayNop').val('" + dt.Rows[0]["ThoiHanNgay"].ToString().Trim() + "');" + System.Environment.NewLine);
                            Response.Write(@"$('#NgayNop').load('noframe.aspx?page=doingay&ngay=' + $('#NgayNop').val()+ '&thang=1')" + System.Environment.NewLine);
                        }
                    }
                    iTinhTrangBanGhi = Convert.ToInt32(dt.Rows[0]["TinhTrangID"]);
                    strTinhTrangBanGhi = dt.Rows[0]["TenTrangThai"].ToString();

                }

                string strTinhTrangHieuLuc = "";
                if (NgayCoHieuLuc != null)
                {
                    if (NgayHetHieuLuc != null)
                    {
                        if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet && NgayCoHieuLuc <= NgayTraCuu && NgayHetHieuLuc >= NgayTraCuu)
                            strTinhTrangHieuLuc = "Có hiệu lực";
                        else if (NgayCoHieuLuc > NgayTraCuu)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (NgayHetHieuLuc < NgayTraCuu)
                            strTinhTrangHieuLuc = "Hết hiệu lực";
                        else
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                    }
                    else
                    {
                        if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.DaPheDuyet)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet && NgayCoHieuLuc <= NgayTraCuu)
                            strTinhTrangHieuLuc = "Có hiệu lực";
                        else if (NgayCoHieuLuc > NgayTraCuu)
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                        else
                            strTinhTrangHieuLuc = "Chưa có hiệu lực";
                    }
                }
                else
                {
                    strTinhTrangHieuLuc = "Chưa có hiệu lực";
                }
                //if (Request.QueryString["mode"] == "3")
                //{
                string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + strTinhTrangHieuLuc + @"</i>')
});";
                Response.Write(a + System.Environment.NewLine);
                //}
                //            else if (Request.QueryString["mode"] == "5")
                //            {
                //                string a = @"
                //            $(function () {
                //$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: " + strTinhTrangBanGhi + @"</i>')
                //$('#TinhTrangHieuLuc').html('<i>Tình trạng hiệu lực: " + strTinhTrangHieuLuc + @"</i>')
                //});";
                //                Response.Write(a + System.Environment.NewLine);
                //            }


                if (Request.QueryString["mode"] == "view")
                {
                    Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                    Response.Write("$('#form_thanhtoanphicanhan input,select,textarea').attr('disabled', true);");

                    if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ChoDuyet)
                    {
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                    }
                    else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                    }
                }

                    //Nếu đang ở chế độ sửa
                else if (Request.QueryString["mode"] == "edit")
                {
                    //Nếu tình trạng bản ghi khác với chờ duyệt => không được sửa, hiển thị theo tình trạng
                    if (iTinhTrangBanGhi != (int)EnumVACPA.TinhTrang.ChoDuyet)
                    {
                        Response.Write("$('#form_thanhtoanphicanhan input,select,textarea').attr('disabled', true);");

                        Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);

                        //Nếu tình trạng là đã phê duyệt thì sẽ không còn các nút Duyệt/Từ chối/Xóa
                        if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.DaPheDuyet)
                        {

                            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        }
                        //Nếu tình trạng là không phê duyệt hoặc đã thoái duyệt thì sẽ không còn các nút Duyệt/Từ chối/Thoái duyệt
                        else if (iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.KhongPheDuyet || iTinhTrangBanGhi == (int)EnumVACPA.TinhTrang.ThoaiDuyet)
                        {
                            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
                        }
                    }
                    //Nếu tình trạng bản ghi là chờ duyệt => có thể sửa. Chỉ hiện nút Lưu, ẩn các nút Duyệt/Từ chối/Thoái Duyệt/Xóa
                    else
                    {
                        Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                        Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);

                        //Có thể sửa Ngày nộp
                        string output_html = @"$(function () {  $('#NgayNop').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
                        Response.Write(output_html + System.Environment.NewLine);
                    }
                }
            }
            //Nếu không phải 2 chế độ xem hoặc sửa
            else
            {
                Response.Write("$('#form_thanhtoanphicanhan input,select,textarea').attr('disabled', true);");
                Response.Write("$('#btn_luu').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
                Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
            }
        }
        //Khi hiển thị các thông tin thêm mới
        else
        {
            string a = @"
            $(function () {
$('#TinhTrangBanGhi').html('<i>Tình trạng bản ghi: Chờ duyệt </i>')
});";
            Response.Write(a + System.Environment.NewLine);

            string output_html = @"$(function () {  $('#NgayNop').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            onClose: function(dateText, inst) {
          $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');}              
        }).datepicker('option', '', '');
    });";
            Response.Write(output_html + System.Environment.NewLine);
            output_html = "$(function () {$('#NgayNop').datepicker('setDate', new Date())});";
            Response.Write(output_html + System.Environment.NewLine);

            Response.Write("$('#btn_sua').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_tuchoi').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_duyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_thoaiduyet').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_xoa').remove();" + System.Environment.NewLine);
            Response.Write("$('#btn_guiemail').remove();" + System.Environment.NewLine);
        }
    }

    protected void load_data()
    {
        try
        {
           //if (thanhtoanphicanhan_grv.DataSource != null)
           //    dtbPhiCaNhan = (DataTable)thanhtoanphicanhan_grv.DataSource;
           //DataRow dtrTemp = dtbPhiCaNhan.NewRow();
           // dtrTemp[truonghoivienid] = "1";
           // dtrTemp[truongstt] = "1";
           // dtrTemp[truongmahoivien] = "ABC123";
           // dtrTemp[truonghoten] = "ABC";
           // dtrTemp[truongsoccktv] = "123";
           // dtrTemp["ngaycapchungchiktv"] = "23/11/1987";
           // dtrTemp[truongdonvicongtac] = "HiPT";
           // dtrTemp[truongsophiphainop] = "0";
           // dtrTemp[truongsophidanop] = "1000000";
           // dtrTemp[truongsophiconno] = "-1000000";
           // dtbPhiCaNhan.Rows.Add(dtrTemp);

           // thanhtoanphicanhan_grv.DataSource = dtbPhiCaNhan;
           // thanhtoanphicanhan_grv.DataBind();


            RefeshGrvPhiCaNhan();





            //int i;
            //Commons cm = new Commons();

            //SqlCommand sql = new SqlCommand();
            //sql.CommandText = @"SELECT distinct PhiID, 1 as LoaiPhi, MaPhi, TenPhi, MucPhi, NgayApDung, NgayHetHieuLuc, TinhTrangID, TenTrangThai, NgayNhap FROM tblTTDMPhiHoiVien A inner join tblDMTrangThai B on A.TinhTrangID = B.TrangThaiId WHERE 0 = 0 ";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
            //    sql.CommandText += " AND A.TenPhi like N'%" + Request.Form["timkiem_Ten"] + "%'";
            //if (!string.IsNullOrEmpty(Request.Form["timkiem_Ma"]))
            //    sql.CommandText += " AND A.MaPhi like N'%" + Request.Form["timkiem_Ma"] + "%'";
            //DataSet ds = new DataSet();
            //ds = DataAccess.RunCMDGetDataSet(sql);

            //dt = ds.Tables[0];

            
            //if (dt.Rows.Count == 0)
            //{
            //    Pager.Enabled = false;
            //    return;
            //}
            //else
            //    Pager.Enabled = true;
            //DataView dv = dt.DefaultView;

            //if (ViewState["sortexpression"] != null)
            //    dv.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();
            //else
            //    dv.Sort = "NgayNop DESC";
            ////dtb = dv.ToTable();
            //// Phân trang
            //PagedDataSource objPds = new PagedDataSource();
            //objPds.DataSource = dv;
            //objPds.AllowPaging = true;
            //objPds.PageSize = 10;

            //// danh sách trang
            //for (i = 0; i < objPds.PageCount; i++)
            //{
            //    ListItem a = new ListItem((i + 1).ToString(), i.ToString());
            //    if (!Pager.Items.Contains(a))
            //        Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            //}
            //// Chuyển trang
            //if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            //{
            //    string a = Request.Form["tranghientai"];
            //    objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            //    Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            //}

            //// kết xuất danh sách ra excel
            //if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            //{
            //    if (Request.Form["ketxuat"] == "1")
            //    {
            //        Response.Clear();
            //        Response.Buffer = true;
            //        Response.ClearContent();
            //        Response.ClearHeaders();

            //        string FileName = quyen + DateTime.Now + ".xls";
            //        StringWriter strwritter = new StringWriter();
            //        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //        Response.ContentType = "application/vnd.ms-excel";
            //        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            //        Response.ContentEncoding = System.Text.Encoding.UTF8;
            //        Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

            //        objPds.AllowPaging = false;

            //        GridView exportgrid = new GridView();

            //        exportgrid.DataSource = objPds;
            //        exportgrid.DataBind();
            //        exportgrid.RenderControl(htmltextwrtter);
            //        exportgrid.Dispose();

            //        Response.Write(strwritter.ToString());
            //        Response.End();
            //    }
            //}

            //thanhtoanphicanhan_grv.DataSource = objPds;

            //thanhtoanphicanhan_grv.DataBind();
            //sql.Connection.Close();
            //sql.Connection.Dispose();
            //sql = null;
            //ds = null;
            //dt = null;


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void thanhtoanphicanhan_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        thanhtoanphicanhan_grv.PageIndex = e.NewPageIndex;
        thanhtoanphicanhan_grv.DataBind();
    }

    protected void thanhtoanphicanhan_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
        ViewState["sortexpression"] = e.SortExpression;
        load_data();
    }

    //MinhLP
    //20150202
    //Khởi tạo dtbPhiCaNhan
    protected void TaoDataTableThanhToanPhiCaNhan(){
        dtbPhiCaNhan = new DataTable();
        dtbPhiCaNhan.Columns.Add(truonghoivienid);
        dtbPhiCaNhan.Columns.Add(truongstt);
        dtbPhiCaNhan.Columns.Add(truongmahoivien);
        dtbPhiCaNhan.Columns.Add(truonghoten);
        dtbPhiCaNhan.Columns.Add(truongsoccktv);
        dtbPhiCaNhan.Columns.Add("ngaycapchungchiktv");
        dtbPhiCaNhan.Columns.Add(truongdonvicongtac);
        dtbPhiCaNhan.Columns.Add(truongsophiphainop);
        dtbPhiCaNhan.Columns.Add(truongsophidanop);
        dtbPhiCaNhan.Columns.Add(truongsophiconno);
        dtbPhiCaNhan.Columns.Add("PhiHoiVienPhaiNop");
        dtbPhiCaNhan.Columns.Add("PhiHoiVienDaNop");
        dtbPhiCaNhan.Columns.Add("PhiCNKTPhaiNop");
        dtbPhiCaNhan.Columns.Add("PhiCNKTDaNop");
        dtbPhiCaNhan.Columns.Add("PhiKSCLPhaiNop");
        dtbPhiCaNhan.Columns.Add("PhiKSCLDaNop");
        dtbPhiCaNhan.Columns.Add("PhiKhacPhaiNop");
        dtbPhiCaNhan.Columns.Add("PhiKhacDaNop");
        dtbPhiCaNhan.Columns.Add("PhatSinhPhiIDHoiVien");
        dtbPhiCaNhan.Columns.Add("PhatSinhPhiIDCNKT");
        dtbPhiCaNhan.Columns.Add("PhatSinhPhiIDKSCL");
        dtbPhiCaNhan.Columns.Add("PhatSinhPhiIDKhac");
        dtbPhiCaNhan.Columns.Add("PhiIDHoiVien");
        dtbPhiCaNhan.Columns.Add("PhiIDCNKT");
        dtbPhiCaNhan.Columns.Add("PhiIDKSCL");
        dtbPhiCaNhan.Columns.Add("PhiIDKhac");

        DataColumn[] columns = new DataColumn[1];
        columns[0] = dtbPhiCaNhan.Columns[truonghoivienid];
        dtbPhiCaNhan.PrimaryKey = columns;
    }

    //MinhLP
    //20150202
    //Khởi tạo dtbPhiCaNhanChiTiet
    //protected void TaoDataTableThanhToanPhiCaNhanChiTiet()
    //{
    //    dtbPhiCaNhanChiTiet = new DataTable();
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongstt);
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongmaphi);
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongloaiphi);
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongsophiphainop);
    //    dtbPhiCaNhanChiTiet.Columns.Add(truongsophidanop);

    //    DataColumn[] columns = new DataColumn[1];
    //    columns[0] = dtbPhiCaNhanChiTiet.Columns[truongmaphi];
    //    dtbPhiCaNhanChiTiet.PrimaryKey = columns;
    //}

    //MinhLP
    //20150202
    //Refesh giá trị gridCaNhan
    protected void RefeshGrvPhiCaNhan()
    {
        ReLoadDTB();
        thanhtoanphicanhan_grv.DataSource = dtbPhiCaNhan;
        thanhtoanphicanhan_grv.DataBind();

    }

    protected void ReLoadDTB()
    {
        if (ViewState["dtbPhiCaNhan"] != null)
            dtbPhiCaNhan = (DataTable)ViewState["dtbPhiCaNhan"];
        else
            ViewState["dtbPhiCaNhan"] = dtbPhiCaNhan;
    }

    //MinhLP
    //20150202
    //Refesh giá trị gridCaNhan
    protected void RefeshGrvPhiCaNhanChiTiet()
    {
        //if (ViewState["dtbDanhSachVLVPP"] != null)
        //    dtbDanhSachVLVPP = (DataTable)ViewState["dtbDanhSachVLVPP"];
        //else
        //    ViewState["dtbDanhSachVLVPP"] = dtbDanhSachVLVPP;
        //gvDanhSachVLVPP.DataSource = dtbDanhSachVLVPP;
        //gvDanhSachVLVPP.DataBind();
    }

    //MinhLP
    //20150202
    //Test
    protected void Test_Click(object sender, EventArgs e)
    {
        ReLoadDTB();
        string id = "";
        string stt = "";
        string mahoivien = "";
        string hoten = "";
        string soccktv = "";
        string ngaycapccktv = "";
        string donvicongtac = "";
        string sophiphainop = "";
        string sophidanop = "";
        string sophiconno = "";
        string phihoivienphainop = "";
        string phihoiviendanop = "";
        string phicnktphainop = "";
        string phicnktdanop = "";
        string phiksclphainop = "";
        string phikscldanop = "";
        string phikhacphainop = "";
        string phikhacdanop = "";
        string phatsinhphiidhoivien = "";
        string phatsinhphiidcnkt = "";
        string phatsinhphiidkscl = "";
        string phatsinhphiidkhac = "";
        string phiidhoivien = "";
        string phiidcnkt = "";
        string phiidkscl = "";
        string phiidkhac = "";
        
        if (Session["PCNCT_HoiVienID"] != null)
            id = Session["PCNCT_HoiVienID"].ToString();
        if (Session["PCNCT_MaHoiVien"] != null)
            mahoivien = Session["PCNCT_MaHoiVien"].ToString();
        if (Session["PCNCT_TenHoiVien"] != null)
            hoten = Session["PCNCT_TenHoiVien"].ToString();
        if (Session["PCNCT_SoCCKTV"] != null)
            soccktv = Session["PCNCT_SoCCKTV"].ToString();
        if (Session["PCNCT_DonViCongTac"] != null)
            donvicongtac = Session["PCNCT_DonViCongTac"].ToString();
        if (Session["PCNCT_NgayCapCCKTV"] != null)
            try
            {
                DateTime NgayCap = new DateTime();
                NgayCap = Convert.ToDateTime(Session["PCNCT_NgayCapCCKTV"]);
                ngaycapccktv = NgayCap.ToString("dd/MM/yyyy");
            }
            catch
            { }

        if (Session["PCNCT_SoPhiPhaiNop"] != null)
            sophiphainop = Session["PCNCT_SoPhiPhaiNop"].ToString();
        if (Session["PCNCT_SoPhiDaNop"] != null)
            sophidanop = Session["PCNCT_SoPhiDaNop"].ToString();
        if (Session["PCNCT_SoPhiConNo"] != null)
            sophiconno = Session["PCNCT_SoPhiConNo"].ToString();

        if (Session["PCNCT_PhiHoiVienPhaiNop"] != null)
            phihoivienphainop = Session["PCNCT_PhiHoiVienPhaiNop"].ToString();
        if (Session["PCNCT_PhiHoiVienDaNop"] != null)
            phihoiviendanop = Session["PCNCT_PhiHoiVienDaNop"].ToString();
        if (Session["PCNCT_PhiCNKTPhaiNop"] != null)
            phicnktphainop = Session["PCNCT_PhiCNKTPhaiNop"].ToString();
        if (Session["PCNCT_PhiCNKTDaNop"] != null)
            phicnktdanop = Session["PCNCT_PhiCNKTDaNop"].ToString();
        if (Session["PCNCT_PhiKSCLPhaiNop"] != null)
            phiksclphainop = Session["PCNCT_PhiKSCLPhaiNop"].ToString();
        if (Session["PCNCT_PhiKSCLDaNop"] != null)
            phikscldanop = Session["PCNCT_PhiKSCLDaNop"].ToString();
        if (Session["PCNCT_PhiKhacPhaiNop"] != null)
            phikhacphainop = Session["PCNCT_PhiKhacPhaiNop"].ToString();
        if (Session["PCNCT_PhiKhacDaNop"] != null)
            phikhacdanop = Session["PCNCT_PhiKhacDaNop"].ToString();
        
        if (Session["PCNCT_PhatSinhPhiIDPhiHoiVien"] != null)
            phatsinhphiidhoivien = Session["PCNCT_PhatSinhPhiIDPhiHoiVien"].ToString();
        if (Session["PCNCT_PhatSinhPhiIDPhiCNKT"] != null)
            phatsinhphiidcnkt = Session["PCNCT_PhatSinhPhiIDPhiCNKT"].ToString();
        if (Session["PCNCT_PhatSinhPhiIDPhiKSCL"] != null)
            phatsinhphiidkscl = Session["PCNCT_PhatSinhPhiIDPhiKSCL"].ToString();
        if (Session["PCNCT_PhatSinhPhiIDPhiKhac"] != null)
            phatsinhphiidkhac = Session["PCNCT_PhatSinhPhiIDPhiKhac"].ToString();

        if (Session["PCNCT_PhiIDPhiHoiVien"] != null)
            phiidhoivien = Session["PCNCT_PhiIDPhiHoiVien"].ToString();
        if (Session["PCNCT_PhiIDPhiCNKT"] != null)
            phiidcnkt = Session["PCNCT_PhiIDPhiCNKT"].ToString();
        if (Session["PCNCT_PhiIDPhiKSCL"] != null)
            phiidkscl = Session["PCNCT_PhiIDPhiKSCL"].ToString();
        if (Session["PCNCT_PhiIDPhiKhac"] != null)
            phiidkhac = Session["PCNCT_PhiIDPhiKhac"].ToString();

        string[] KhoaCaNhanMoi = new string[] { id };
        if (dtbPhiCaNhan.Rows.Find(KhoaCaNhanMoi) == null)
        {
            DataRow dtrTemp = dtbPhiCaNhan.NewRow();
            dtrTemp[truonghoivienid] = id;
            dtrTemp[truongstt] = dtbPhiCaNhan.Rows.Count + 1;
            dtrTemp[truongmahoivien] = mahoivien;
            dtrTemp[truonghoten] = hoten;
            dtrTemp[truongsoccktv] = soccktv;
            dtrTemp["ngaycapchungchiktv"] = ngaycapccktv;
            dtrTemp[truongdonvicongtac] = donvicongtac;
            dtrTemp[truongsophiphainop] = sophiphainop;
            dtrTemp[truongsophidanop] = sophidanop;
            dtrTemp[truongsophiconno] = sophiconno;
            dtrTemp["PhiHoiVienPhaiNop"] = phihoivienphainop;
            dtrTemp["PhiHoiVienDaNop"] = phihoiviendanop;
            dtrTemp["PhiCNKTPhaiNop"] = phicnktphainop;
            dtrTemp["PhiCNKTDaNop"] = phicnktdanop;
            dtrTemp["PhiKSCLPhaiNop"] = phiksclphainop;
            dtrTemp["PhiKSCLDaNop"] = phikscldanop;
            dtrTemp["PhiKhacPhaiNop"] = phikhacphainop;
            dtrTemp["PhiKhacDaNop"] = phikhacdanop;
            dtrTemp["PhatSinhPhiIDHoiVien"] = phatsinhphiidhoivien;
            dtrTemp["PhatSinhPhiIDCNKT"] = phatsinhphiidcnkt;
            dtrTemp["PhatSinhPhiIDKSCL"] = phatsinhphiidkscl;
            dtrTemp["PhatSinhPhiIDKhac"] = phatsinhphiidkhac;
            dtrTemp["PhiIDHoiVien"] = phiidhoivien;
            dtrTemp["PhiIDCNKT"] = phiidcnkt;
            dtrTemp["PhiIDKSCL"] = phiidkscl;
            dtrTemp["PhiIDKhac"] = phiidkhac;
            dtbPhiCaNhan.Rows.Add(dtrTemp);
        }
        else
        {
            DataRow dtrTemp = dtbPhiCaNhan.Rows.Find(id);
            dtrTemp[truonghoivienid] = id;
            dtrTemp[truongstt] = dtbPhiCaNhan.Rows.Count + 1;
            dtrTemp[truongmahoivien] = mahoivien;
            dtrTemp[truonghoten] = hoten;
            dtrTemp[truongsoccktv] = soccktv;
            dtrTemp["ngaycapchungchiktv"] = ngaycapccktv;
            dtrTemp[truongdonvicongtac] = donvicongtac;
            dtrTemp[truongsophiphainop] = sophiphainop;
            dtrTemp[truongsophidanop] = sophidanop;
            dtrTemp[truongsophiconno] = sophiconno;
            dtrTemp["PhiHoiVienPhaiNop"] = phihoivienphainop;
            dtrTemp["PhiHoiVienDaNop"] = phihoiviendanop;
            dtrTemp["PhiCNKTPhaiNop"] = phicnktphainop;
            dtrTemp["PhiCNKTDaNop"] = phicnktdanop;
            dtrTemp["PhiKSCLPhaiNop"] = phiksclphainop;
            dtrTemp["PhiKSCLDaNop"] = phikscldanop;
            dtrTemp["PhiKhacPhaiNop"] = phikhacphainop;
            dtrTemp["PhiKhacDaNop"] = phikhacdanop;
            dtrTemp["PhatSinhPhiIDHoiVien"] = phatsinhphiidhoivien;
            dtrTemp["PhatSinhPhiIDCNKT"] = phatsinhphiidcnkt;
            dtrTemp["PhatSinhPhiIDKSCL"] = phatsinhphiidkscl;
            dtrTemp["PhatSinhPhiIDKhac"] = phatsinhphiidkhac;
            dtrTemp["PhiIDHoiVien"] = phiidhoivien;
            dtrTemp["PhiIDCNKT"] = phiidcnkt;
            dtrTemp["PhiIDKSCL"] = phiidkscl;
            dtrTemp["PhiIDKhac"] = phiidkhac;
        }
        RefeshGrvPhiCaNhan();
    }

    private string _dtbPhiCaNhan = "dtbPhiCaNhan";

    //private DataTable dtbPhiCaNhan
    //{
    //    get
    //    {
    //        if (ViewState[_dtbPhiCaNhan] == null)
    //        {
    //            DataTable dtbPhiCaNhan = new DataTable();
    //            dtbPhiCaNhan.Columns.Add(truonghoivienid);
    //            dtbPhiCaNhan.Columns.Add(truongstt);
    //            dtbPhiCaNhan.Columns.Add(truongmahoivien);
    //            dtbPhiCaNhan.Columns.Add(truonghoten);
    //            dtbPhiCaNhan.Columns.Add(truongsoccktv);
    //            dtbPhiCaNhan.Columns.Add("ngaycapchungchiktv");
    //            dtbPhiCaNhan.Columns.Add(truongdonvicongtac);
    //            dtbPhiCaNhan.Columns.Add(truongsophiphainop);
    //            dtbPhiCaNhan.Columns.Add(truongsophidanop);
    //            dtbPhiCaNhan.Columns.Add(truongsophiconno);
    //            DataColumn[] columns = new DataColumn[1];
    //            columns[0] = dtbPhiCaNhan.Columns[truonghoivienid];
    //            dtbPhiCaNhan.PrimaryKey = columns;
    //            return dtbPhiCaNhan;
    //        }
    //        return (DataTable)ViewState[_dtbPhiCaNhan];
    //    }
    //    set
    //    {
    //        ViewState[_dtbPhiCaNhan] = value;
    //    }
    //}
}