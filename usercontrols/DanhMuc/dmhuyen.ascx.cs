﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HiPT.VACPA.DL;
using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;

public partial class usercontrols_dmhuyen : System.Web.UI.UserControl
{
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();


    // Tên chức năng
    public string tenchucnang = "Quận/Huyện";
    // Icon CSS Class  
    public string IconClass = "iconfa-star-empty";

    public string bangdm = "tblDMHuyen";
    public string quyen = "DmHuyen";


    public string truongid = "HuyenID";
    public string truongten = "TenHuyen";
    public string truongma = "MaHuyen";
    public string truonghieuluc = "HieuLuc";

    public string truongtentinh = "TenTinh";

    protected void Page_Load(object sender, EventArgs e)
    {
        Commons cm = new Commons();
        if (String.IsNullOrEmpty(cm.Admin_TenDangNhap)) Response.Redirect("adminlogin.aspx");

        dmhuyen_grv.Columns[1].HeaderText = "Quận/Huyện";
        dmhuyen_grv.Columns[1].SortExpression = truongten;
        dmhuyen_grv.Columns[2].HeaderText = "Mã Quận/Huyện";
        dmhuyen_grv.Columns[2].SortExpression = truongma;
        dmhuyen_grv.Columns[3].HeaderText = "Tỉnh/Thành phố";
        dmhuyen_grv.Columns[4].HeaderText = "Thao tác";
        Page.Master.FindControl("contentpath").Controls.Add(new LiteralControl(@"

     <li>Danh mục " + tenchucnang + @" <span class=""separator""></span></li>                <!-- Tên chức năng --> 
     "));

        Page.Master.FindControl("contentheader").Controls.Add(new LiteralControl(@"<div class=""pageicon""><span class=""" + IconClass + @"""></span></div><div class=""pagetitle""> <h5>" + System.Web.Configuration.WebConfigurationManager.AppSettings["AppTitle"].ToString() + @"</h5> <h1>Danh mục " + tenchucnang + @"</h1> </div>"));


        // Phân quyền
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>"));
            Form1.Visible = false;
            return;
        }




        ///////////


        load_data();


        try
        {
            if (Request.QueryString["act"] == "delete")
            {

                if (kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
                {

                    SqlCommand sql = new SqlCommand();
                    sql.CommandText = "DELETE FROM " + bangdm + " WHERE " + truongid + " IN (" + Request.QueryString["id"] + ")";
                    DataAccess.RunActionCmd(sql);

                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog(quyen, "Xóa bản ghi có ID \"" + Request.QueryString["id"] + "\" của danh mục " + tenchucnang);

                    Response.Redirect("admin.aspx?page=dmhuyen&dm=" + Request.QueryString["dm"]);
                }
            }



        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl("<div class='da-message error' style='margin-bottom:10px'>" + ex.ToString() + "</div>"));
        }



    }

    protected void annut()
    {
        Commons cm = new Commons();
        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("SUA|"))
        {
            Response.Write("$('a[data-original-title=\"Sửa\"]').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('a[data-original-title=\"Xóa\"]').remove();");
        }


        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
        {
            Response.Write("$('#btn_them').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XOA|"))
        {
            Response.Write("$('#btn_xoa').remove();");
        }

        if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("KETXUAT|"))
        {
            Response.Write("$('#btn_ketxuat').remove();");
        }
    }

    protected void load_data()
    {
        try
        {
            int i;
            Commons cm = new Commons();
            // Tìm kiếm
            SelectQueryBuilder query = new SelectQueryBuilder();
            //query.SelectFromTable(bangdm);
            //query.SelectAllColumns();
            query.SelectFromTable(bangdm);
            query.AddJoin(JoinType.LeftJoin, "tblDMTinh", "MaTinh", Comparison.Equals, "tblDMHuyen", "MaTinh");
           // query.SelectAllColumns();
            //query.
            query.SelectColumns("TenTinh", "tblDMHuyen.MaTinh", "MaHuyen", "TenHuyen", "HuyenID", "tblDMHuyen.HieuLuc");

            if (!string.IsNullOrEmpty(Request.Form["timkiem_Ten"]))
                query.AddWhere(truongten, Comparison.Like, "%" + Request.Form["timkiem_Ten"] + "%");

            SqlCommand sql = new SqlCommand();
            sql.CommandText = query.BuildQuery();
            DataSet ds = DataAccess.RunCMDGetDataSet(sql);
            DataView dt = ds.Tables[0].DefaultView;

            if (ViewState["sortexpression"] != null)
                dt.Sort = ViewState["sortexpression"].ToString() + " " + ViewState["sortdirection"].ToString();

            // Phân trang
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = ds.Tables[0].DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 10;

            Pager.Items.Clear();
            // danh sách trang
            for (i = 0; i < objPds.PageCount; i++)
            {
                Pager.Items.Add((new ListItem((i + 1).ToString(), i.ToString())));
            }
            // Chuyển trang
            if (!string.IsNullOrEmpty(Request.Form["tranghientai"]))
            {
                objPds.CurrentPageIndex = Convert.ToInt32(Request.Form["tranghientai"]);
                Pager.SelectedIndex = Convert.ToInt32(Request.Form["tranghientai"]);
            }

            // kết xuất danh sách ra excel
            if (!string.IsNullOrEmpty(Request.Form["ketxuat"]))
            {
                if (Request.Form["ketxuat"] == "1")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    string FileName = quyen + DateTime.Now + ".xls";
                    StringWriter strwritter = new StringWriter();
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

                    objPds.AllowPaging = false;

                    GridView exportgrid = new GridView();

                    exportgrid.DataSource = objPds;
                    exportgrid.DataBind();
                    exportgrid.RenderControl(htmltextwrtter);
                    exportgrid.Dispose();

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }

            dmhuyen_grv.DataSource = objPds;

            dmhuyen_grv.DataBind();
            sql.Connection.Close();
            sql.Connection.Dispose();
            sql = null;
            ds = null;
            dt = null;


        }
        catch (Exception ex)
        {
            Page.Master.FindControl("contentmessage").Controls.Add(new LiteralControl(" <div class=\"alert alert-error\">  <button class=\"close\" type=\"button\" data-dismiss=\"alert\">×</button>   <strong><b>Thông báo:</b> </strong> " + ex.Message + "</div> "));
        }
    }

    protected void dmhuyen_grv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        dmhuyen_grv.PageIndex = e.NewPageIndex;
        dmhuyen_grv.DataBind();
    }

    protected void dmhuyen_grv_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortexpression"] = e.SortExpression;
        if (ViewState["sortdirection"] == null)
            ViewState["sortdirection"] = "asc";
        else
        {
            if (ViewState["sortdirection"].ToString() == "asc")
                ViewState["sortdirection"] = "desc";
            else
                ViewState["sortdirection"] = "asc";
        }
    }
}