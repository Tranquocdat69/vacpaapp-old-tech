﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;

public partial class usercontrols_dmchung_edit : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    // Tên chức năng
    public string tenchucnang = "";

    public string bangdm = "";
    public string quyen = "";
    public string truongid = "";
    public string truongten = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        switch (Request.QueryString["dm"])
        {
            case "dmcauhoibimat":
                tenchucnang = "Câu hỏi bí mật";
                bangdm = "tblDMCauHoiBiMat";
                quyen = "DmCauHoiBiMat";
                truongid = "CauHoiBiMatID";
                truongten = "CauHoi";
                break;
            case "dmchucvu":
                tenchucnang = "Chức vụ";
                bangdm = "tblDMChucVu";
                quyen = "DmChucVu";
                truongid = "ChucVuID";
                truongten = "TenChucVu";
                break;
            case "dmchungchi":
                tenchucnang = "Chứng chỉ";                
                bangdm = "tblDMChungChi";
                quyen = "DmChungChi";
                truongid = "ChungChiID";
                truongten = "TenChungChi";
                break;
            case "dmchuyennganhdaotao":
                tenchucnang = "Chuyên ngành đào tạo";
                bangdm = "tblDMChuyenNganhDaoTao";
                quyen = "DmChuyenNganhDaoTao";
                truongid = "ChuyenNganhDaoTaoID";
                truongten = "TenChuyenNganhDaoTao";
                break;
            case "dmhinhthuckhenthuong":
                tenchucnang = "Hình thức khen thưởng";
                bangdm = "tblDMHinhThucKhenThuong";
                quyen = "DmHinhThucKhenThuong";
                truongid = "HinhThucKhenThuongID";
                truongten = "TenHinhThucKhenThuong";
                break;
            case "dmhinhthuckyluat":
                tenchucnang = "Hình thức kỷ luật";                
                bangdm = "tblDMHinhThucKyLuat";
                quyen = "DmHinhThucKyLuat";
                truongid = "HinhThucKyLuatID";
                truongten = "TenHinhThucKyLuat";
                break;
            case "dmhocham":
                tenchucnang = "Học hàm";
                bangdm = "tblDMHocHam";
                quyen = "DmHocHam";
                truongid = "HocHamID";
                truongten = "TenHocHam";
                break;
            case "dmhocvi":
                tenchucnang = "Học vị";                
                bangdm = "tblDMHocVi";
                quyen = "DmHocVi";
                truongid = "HocViID";
                truongten = "TenHocVi";
                break;
            case "dmloaihinhdoanhnghiep":
                tenchucnang = "Loại hình doanh nghiệp";                
                bangdm = "tblDMLoaiHinhDoanhNghiep";
                quyen = "DmLoaiHinhDoanhNghiep";
                truongid = "LoaiHinhDoanhNghiepID";
                truongten = "TenLoaiHinhDoanhNghiep";
                break;
            case "dmtruongdaihoc":
                tenchucnang = "Trường đại học";
                bangdm = "tblDMTruongDaiHoc";
                quyen = "DmTruongDaiHoc";
                truongid = "TruongDaiHocID";
                truongten = "TenTruongDaiHoc";
                break;
            case "dmtrinhdochuyenmon":
                tenchucnang = "Trình độ chuyên môn";                
                bangdm = "tblDMTrinhDoChuyenMon";
                quyen = "DmTrinhDoChuyenMon";
                truongid = "TrinhDoChuyenMonID";
                truongten = "TenTrinhDoChuyenMon";
                break;
            case "dmloaiphi":
                tenchucnang = "Loại phí";                
                bangdm = "tblDMLoaiPhi";
                quyen = "DmLoaiPhi";
                truongid = "LoaiPhiID";
                truongten = "TenLoaiPhi";
                break;
            case "dmloaitien":
                tenchucnang = "Loại tiền";
                bangdm = "tblDMLoaiTien";
                quyen = "DmLoaiTien";
                truongid = "LoaiTienID";
                truongten = "TenLoaiTien";
                break;
            case "dmnganhang":
                tenchucnang = "Ngân hàng";                
                bangdm = "tblDMNganHang";
                quyen = "DmNganHang";
                truongid = "NganHangID";
                truongten = "TenNganHang";
                break;
            case "dmquytrinh":
                tenchucnang = "Quy trình nghiệp vụ";
                bangdm = "tblDMQuyTrinh";
                quyen = "DmQuyTrinh";
                truongid = "QuyTrinhID";
                truongten = "TenQuyTrinh";
                break;
            case "dmdangykienkt":
                tenchucnang = "Dạng ý kiến kiểm toán";
                bangdm = "tblDMDangYKienKT";
                quyen = "DMDangYKienKT";
                truongid = "DangYKienKTID";
                truongten = "TenDangYKien";
                break;
            case "dmsothich":
                tenchucnang = "Sở thích";
                bangdm = "tblDMSoThich";
                quyen = "DMSoThich";
                truongid = "SoThichID";
                truongten = "TenSoThich";
                break;
            case "dmloaitailieu":
                tenchucnang = "Loại tài liệu";
                bangdm = "tblDMLoaiTaiLieu";
                quyen = "DMLoaiTaiLieu";
                truongid = "LoaiTaiLieuID";
                truongten = "TenLoaiTaiLieu";
                break;
            case "dmquoctich":
                tenchucnang = "Quốc tịch";
                bangdm = "tblDMQuocTich";
                quyen = "DMLoaiTaiLieu";
                truongid = "QuocTichID";
                truongten = "TenQuocTich";
                break;
        }


        Label1.Text = tenchucnang + ":";

        try
        {

            if (!string.IsNullOrEmpty(Request.Form["Ten"]))
            {

                SqlCommand sql = new SqlCommand();

                sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE " + truongten + " = @Ten AND " + truongid + " <> " + Request.QueryString["id"];
                sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Đã có bản ghi với giá trị \"" + Request.Form["Ten"].Trim() + "\"</div>"));
                }
                else
                {
                    sql = new SqlCommand();
                    sql.CommandText = "UPDATE " + bangdm + " SET " + truongten + " = @Ten WHERE " + truongid + " = " + Request.QueryString["id"];
                    sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    cm.ghilog(quyen, "Cập nhật giá trị \"" + Request.Form["Ten"] + "\" vào danh mục " + tenchucnang);
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                }
            }


        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }

 


    protected void load_giatricu()
    {
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT "+ truongten+" FROM "+ bangdm+" WHERE "+ truongid+"=" + Request.QueryString["id"];
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);



        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        

        Response.Write("$('#Ten').val('" + cm.AddSlashes(ds.Tables[0].Rows[0][0].ToString()) + "');" + System.Environment.NewLine);

        ds.Dispose();

        if (Request.QueryString["mode"] == "view")
        {
            Response.Write("$('#form_dmchung_add input,select,textarea').attr('disabled', true);");
        }

    }
}