﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VACPA.Data.SqlClient;
using VACPA.Entities;

public partial class usercontrols_DanhMuc_Process : System.Web.UI.UserControl
{
    Db _db = new Db(ListName.ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _db.OpenConnection();
            string action = Library.CheckNull(Request.QueryString["action"]);
            if (action == "loadchungchiquocte")
            {
                string idHocVien = Library.CheckNull(Request.QueryString["idhv"]);
                GetChungChiQuocTeByHocVien(idHocVien);
            }
        }
        catch { }
        finally
        {
            _db.CloseConnection();
        }
    }

    private void GetChungChiQuocTeByHocVien(string idHocVien)
    {
        string js = "<script type='text/javascript'>" + Environment.NewLine;
        js += "value = '';" + Environment.NewLine;
        if (!string.IsNullOrEmpty(idHocVien))
        {
            string query = @"SELECT SoChungChi FROM tblHoiVienCaNhanChungChiQuocTe WHERE HoiVienCaNhanID = " + idHocVien;
            List<Hashtable> listData = _db.GetListData(query);
            if (listData.Count > 0)
            {
                string value = "";
                foreach (Hashtable ht in listData)
                {
                    value += " " + ht["SoChungChi"].ToString().Trim() + ",";
                }
                js += "value = '" + value.TrimEnd(',') + "';" + Environment.NewLine;
            }
        }
        js += "parent.SetChungChiQuocTe(value);" + Environment.NewLine;
        js += "</script>";
        Response.Write(js);
    }
}