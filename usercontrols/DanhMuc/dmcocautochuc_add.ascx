﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dmcocautochuc_add.ascx.cs" Inherits="usercontrols_dmcocautochuc_add" %>
<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
</style>

<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 

<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("THEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <form id="form_dmcocautochuc_add" clientidmode="Static" runat="server"   method="post">

    <div id="thongbaoloi_form_themdmcocautochuc" name="thongbaoloi_form_themdmcocautochuc" style="display:none" class="alert alert-error"></div>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
      <table id="Table1" width="100%" border="0" class="formtbl" >
      <tr>       
                   <td><asp:Label ID="Label6" runat = "server" Text="Bộ phận cấp trên"></asp:Label>
                     <asp:Label ID="Label7" runat = "server" ForeColor="Red"></asp:Label></td>
          <td><select name="CoCauToChucCapTrenId" id="CoCauToChucCapTrenId">
          
          
          <%  
              try
              {
                  vLoadCoCauToChuc(dt.Rows[0]["CoCauToChucId"].ToString());
              }
              catch 
              {
                  vLoadCoCauToChuc("00");
              }
              
              %>
              
          </select></td>
        </tr>
        <tr>
          <td><asp:Label ID="Label1" runat = "server" Text = "Bộ phận"></asp:Label>
            <asp:Label ID="Label2" runat = "server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="6"><input type="text" name="Ten" id="Ten"></td>
        </tr>
         <tr>
          <td><asp:Label ID="Label3" runat="server" Text = "Mã bộ phận"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
          <td colspan="6"><input type="text" name="Ma" id="Ma"></td>
        </tr>
        
        <tr>
          <td><asp:Label ID="Label5" runat="server" Text = "Hết hiệu lực" ></asp:Label></td>
          <%--<td colspan="6"><asp:CheckBox ID="HieuLuc"  runat="server" /></td>--%>
          <td colspan="6"><input type="checkbox" id="HieuLuc" name = "HieuLuc" onclick="validate()"></td>
        </tr>
        </table>
       </asp:PlaceHolder>  
       
        
    </form>



           
                                                    
<script type="text/javascript">
       

        function submitform() {        
                jQuery("#form_dmcocautochuc_add").submit();
        }



    jQuery("#form_dmcocautochuc_add").validate({
        rules: {
            Ten: {
                required: true
            },
            Ma: {
                required: true
            },
        },
        invalidHandler: function (f, d) {
            var g = d.numberOfInvalids();
            
            if (g) {
                var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";                                
                jQuery("#thongbaoloi_form_themdmcocautochuc").html(e).show()
            } else {
                jQuery("#thongbaoloi_form_themdmcocautochuc").hide()
            }
        }
    });
   
      function validate(){
   var vHieuLuc = document.getElementById('HieuLuc');
if (vHieuLuc.checked){
          vHieuLuc.value = 0;
}else{
vHieuLuc.value = 1;
}
}
    

</script>
