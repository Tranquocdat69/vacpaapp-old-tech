﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;
public partial class usercontrols_dmcocautochuc_edit : System.Web.UI.UserControl
{
    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();
    public DataTable dt = new DataTable();
    //// Tên chức năng
    //public string tenchucnang = "Tỉnh/Thành phố";

    //public string bangdm = "tblDMTinh";
    //public string quyen = "DmTinh";
    //public string truongid = "TinhID";
    //public string truongten = "TenTinh";
    //public string truongma = "MaTinh";
    //public string truonghieuluc = "HieuLuc";

    // Tên chức năng
    public string tenchucnang = "Bộ phận";

    public string bangdm = "tblCoCauToChuc";
    public string quyen = "CoCauToChuc";
    public string truongid = "CoCauToChucId";
    public string truongten = "TenBoPhan";
    public string truongma = "MaBoPhan";
    public string truongidcha = "CoCauToChucCapTrenId";
    public string truonghieuluc = "HieuLuc";
    public string strHieuLuc = "1";

    protected void Page_Load(object sender, EventArgs e)
    {
        Label1.Text = tenchucnang + ":";
        Label3.Text = "Mã Bộ phận";
        Label5.Text = "Hết hiệu lực";
        Label6.Text = "Bộ phận cấp trên";
        //string strHieuLuc = "0";
        //if (!HieuLuc.Checked)
        //    strHieuLuc = "1";
        //else
        //    strHieuLuc = "0";
       // vLoadCoCauToChuc();
        try
        {

            if (!string.IsNullOrEmpty(Request.Form["Ten"]) || !string.IsNullOrEmpty(Request.Form["Ma"]) || !string.IsNullOrEmpty(Request.Form["CoCauToChucCapTrenId"]))
            {
                if (Request.Form["HieuLuc"] != null)
                {
                    strHieuLuc = Request.Form["HieuLuc"];
                }

                SqlCommand sql = new SqlCommand();

                sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE (" + truongten + " = @Ten OR " + truongma + " = @Ma) AND " + truongid + " <> " + Request.QueryString["id"];
                sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);

                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);

                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Đã có bản ghi với giá trị \"" + Request.Form["Ten"].Trim() + " hoặc " + Request.Form["Ma"].Trim() + "\"</div>"));
                }
                else
                {
                    sql = new SqlCommand();
                    if (Request.Form["CoCauToChucCapTrenId"].ToString() == "0")
                    {
                        sql.CommandText = "UPDATE " + bangdm + " SET " + truongten + " = @Ten," + truongma + " = @Ma, CoCauToChucCapTrenId = NULL, " + truonghieuluc + " = " + strHieuLuc + " WHERE " + truongid + " = " + Request.QueryString["id"];
                        sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                        sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                    }
                    else
                    {
                        sql.CommandText = "UPDATE " + bangdm + " SET " + truongten + " = @Ten," + truongma + " = @Ma, " + truongidcha + "= @IdCha, " + truonghieuluc + " = " + strHieuLuc + " WHERE " + truongid + " = " + Request.QueryString["id"];
                        sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                        sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                        sql.Parameters.AddWithValue("@IdCha", Request.Form["CoCauToChucCapTrenId"]);
                    }
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;
                    cm.ghilog(quyen, "Cập nhật giá trị \"" + Request.Form["Ten"] + "\"" + Request.Form["Ma"] + "\"" + Request.Form["TinhID_ToChuc"] + "\" vào danh mục " + tenchucnang);
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                }
            }

        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }




    protected void load_giatricu()
    {
        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT " + truongten + ", " + truongma + "," + truongidcha + "," + truonghieuluc + " FROM " + bangdm + " WHERE " + truongid + "=" + Request.QueryString["id"];
        DataSet ds = DataAccess.RunCMDGetDataSet(sql);
        dt = ds.Tables[0];
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;

        Response.Write("$('#Ten').val('" + cm.AddSlashes(ds.Tables[0].Rows[0][0].ToString()) + "');" + System.Environment.NewLine);
        Response.Write("$('#Ma').val('" + cm.AddSlashes(ds.Tables[0].Rows[0][1].ToString().TrimEnd()) + "');" + System.Environment.NewLine);

        if (ds.Tables[0].Rows[0][2].ToString().Trim() == "")
        {
            Response.Write("$('#CoCauToChucCapTrenId').val('0');" + System.Environment.NewLine);
        }
        else
        {
            Response.Write("$('#CoCauToChucCapTrenId').val('" + ds.Tables[0].Rows[0][2].ToString().Trim() + "');" + System.Environment.NewLine);
        }
        if (ds.Tables[0].Rows[0][3].ToString() == "0")
        {
            Response.Write("$('#HieuLuc').attr('checked','checked'); ");
            Response.Write("$('#HieuLuc').attr('value','0'); ");
        }
        ds.Dispose();

        if (Request.QueryString["mode"] == "view")
        {
            Response.Write("$('#form_dmcocautochuc_add input,select,textarea').attr('disabled', true);");
        }
    }

    public void vLoadCoCauToChuc(string CoCauToChucCapTrenId = "00")
    {
        string output_html = "";
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT CoCauToChucId, MaBoPhan, CoCauToChucCapTrenId, TenBoPhan, HieuLuc FROM tblCoCauToChuc WHERE HieuLuc='1' AND CoCauToChucCapTrenId IS NULL";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        output_html += @"<option selected=""selected""" + " value=\"0\"><< Không có Bộ phận cấp trên >></option>";
        foreach (DataRow dtr in dt.Rows)
        {
            int level = 0;
            string strIdCha = dtr["CoCauToChucId"].ToString();
            if (dtr["CoCauToChucId"].ToString() == Request.QueryString["id"])
            { continue; }
            else if (CoCauToChucCapTrenId == dtr["CoCauToChucId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["CoCauToChucId"].ToString().Trim() + @""">" + dtr["TenBoPhan"] + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["CoCauToChucId"].ToString().Trim() + @""">" + dtr["TenBoPhan"] + @"</option>";
            }
            vLoadCoCauToChuc1(CoCauToChucCapTrenId, strIdCha, level, ref output_html);
        }


        ds = null;
        dt = null;

        System.Web.HttpContext.Current.Response.Write(output_html);
    }

    private void vLoadCoCauToChuc1(string strCoCauToChucCapTrenId, string strIdCha, int level, ref string output_html)
    {
        DataSet ds = new DataSet();

        SqlCommand sql = new SqlCommand();
        sql.CommandText = "SELECT CoCauToChucId, MaBoPhan, CoCauToChucCapTrenId, TenBoPhan, HieuLuc FROM tblCoCauToChuc WHERE HieuLuc='1' AND CoCauToChucCapTrenId = '" + strIdCha + "'";
        ds = DataAccess.RunCMDGetDataSet(sql);
        sql.Connection.Close();
        sql.Connection.Dispose();
        sql = null;


        DataTable dt = ds.Tables[0];

        level++;
        foreach (DataRow dtr in dt.Rows)
        {
            string strTenBoPhan = new String('−', level * 2) + dtr["TenBoPhan"].ToString();
            string strId = dtr["CoCauToChucId"].ToString();
            if (dtr["CoCauToChucId"].ToString() == Request.QueryString["id"])
            { continue; }
            else if (strCoCauToChucCapTrenId == dtr["CoCauToChucId"].ToString().Trim())
            {
                output_html += @"
                     <option selected=""selected"" value=""" + dtr["CoCauToChucId"].ToString().Trim() + @""">" + strTenBoPhan + @"</option>";
            }
            else
            {
                output_html += @"
                     <option value=""" + dtr["CoCauToChucId"].ToString().Trim() + @""">" + strTenBoPhan + @"</option>";
            }
            vLoadCoCauToChuc1(strCoCauToChucCapTrenId, strId, level, ref output_html);
        }


        ds = null;
        dt = null;
    }
}