﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="thanhtoanphicanhan.ascx.cs" Inherits="usercontrols_thanhtoanphicanhan" %>

<style>
     .require {color:Red;}
     .error {color:Red; font-size:11px;}
     .style1
    {
        width: 15%;
    }
    .style2
    {
        width: 35%;
    }
    .style3
    {
        width: 24px;
    }
    .style4
    {
        width: 10%;
    }

</style>
<script type="text/javascript" src="js/numeric/autoNumeric.js"></script>
<asp:PlaceHolder ID="ErrorMessage" runat="server"></asp:PlaceHolder> 


<% 
    
    if (!kiemtraquyen.fcnXDQuyen(Convert.ToInt32(cm.Admin_NguoiDungID), quyen, cm.connstr).Contains("XEM|"))
    {
        Response.Write("<div class='alert alert-error'>  <button class='close' type='button' data-dismiss='alert'>×</button>   <strong><b>Thông báo:</b> </strong> Tài khoản không có quyền thực hiện thao tác!</div>");     
        return;
    }
    
    %>

    <%--<form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">--%>
    <form id="form_thanhtoanphicanhan" clientidmode="Static" runat="server"   method="post">
      <asp:ScriptManager ID="ScriptManagerPhiCaNhan" runat="server"></asp:ScriptManager>
    <h4 class="widgettitle"><%=tenchucnang%></h4>
    <div id="thongbaoloi_form_thanhtoanphicanhan" name="thongbaoloi_form_thanhtoanphicanhan" style="display:none" class="alert alert-error"></div>
    <div height = "40px">
        <table id="tblThongBao" width="100%" border="0" class="formtbl">
        <tr>
         <td>
         <div ID="TinhTrangBanGhi" name = "TinhTrangBanGhi"></div>
        </td>
        </tr>
        </table>
    </div>
        <div><br /><b> Thông tin chung</b></div>
      <table id="Table1" width="100%" border="0" class="formtbl" >
      <tr>
          <td class="style1"><asp:Label ID="lblMaGiaoDich" runat="server" Text = 'Mã giao dịch:' ></asp:Label></td>
        <td class="style2" ><input type="text" name="MaGiaoDich" id="MaGiaoDich" disabled = "disabled" ></td>
        <td class="style1" ><asp:Label ID="lblNgayNop" runat="server" Text = 'Ngày nộp:' ></asp:Label>
            <asp:Label ID="Label1" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td class="style2"><input type="text" name="NgayNop" id="NgayNop"/></td>
      </tr>
       <tr>
          <td class="style1"><asp:Label ID="Label6" runat="server" Text = 'Diễn giải:' ></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="(*)" ForeColor="Red"></asp:Label></td>
        <td colspan ="3"><input width="100%" type="text" name="DienGiai" id="DienGiai"></td>        
      
      </tr>
      
      </table>
      <asp:UpdatePanel ID="UpdatePanelPhiCaNhan" runat="server"    >
       <ContentTemplate>
       <div>
        <div  class="dataTables_length">
        
        <a id="btn_them"  href="#none" class="btn btn-rounded" onclick="themthanhtoanphicanhanchitiet();"><i class="iconfa-plus-sign"></i> Thêm</a>
        <a id="btn_xoa_chitiet"  href="#none" class="btn btn-rounded" onclick="confirm_delete_thanhtoanphicanhan(thanhtoanphicanhan_grv_selected);"><i class="iconfa-remove-sign"></i> Xóa</a>
         </div>
        
   </div>

   <div><br /><b>Danh sách nộp phí</b></div>
   <div style="width: 100%; height: 400px; overflow: scroll">
   
            <div style="width: 100%; text-align: center; margin-top: 5px; display:none">
    <asp:LinkButton ID="Test" runat="server" CssClass="btn" 
        onclick="Test_Click" ><i class="iconfa-search">
    </i>Test</asp:LinkButton></div>
      
       <asp:GridView ClientIDMode="Static" ID="thanhtoanphicanhan_grv" runat="server" 
       AutoGenerateColumns="False" class="table table-bordered responsive dyntable" 
       AllowPaging="False" AllowSorting="True" 
       onpageindexchanging="thanhtoanphicanhan_grv_PageIndexChanging" onsorting="thanhtoanphicanhan_grv_Sorting"    
       >
          <Columns>
              <asp:TemplateField HeaderText='<input type="checkbox"  class="checkall" value="all" />' ItemStyle-Width="30px">
                  <ItemTemplate>
                      <input type="checkbox" class="colcheckbox" value="<%# Eval(truonghoivienid)%>" />
                  </ItemTemplate>
              </asp:TemplateField>

             <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongstt)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

               <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongmahoivien)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truonghoten)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongsoccktv)%></span>
                  </ItemTemplate>
              </asp:TemplateField>
                                     
               <asp:BoundField DataField="ngaycapchungchiktv" 
                            ReadOnly="True"
                            DataFormatString="{0:dd/MM/yyyy}" />

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# Eval(truongdonvicongtac)%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N2}", Eval(truongsophiphainop))%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N2}", Eval(truongsophidanop))%></span>
                  </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField  >
                  <ItemTemplate>
                      <span><%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:N2}", Eval(truongsophiconno))%></span>
                  </ItemTemplate>
              </asp:TemplateField>

          </Columns>
</asp:GridView>

<div id = "div_thanhtoanphicanhan_chitiet"></div>
</ContentTemplate>
      </asp:UpdatePanel>     
</div>
<%LoadNut(); %>


    </form>

    
                         
<script type="text/javascript">
       

        function submitform() {        
                jQuery("#form_thanhtoanphicanhan").submit();
        }


        jQuery("#form_thanhtoanphicanhan").validate({
            rules: {
                NgayNop: {
                    required: true
                },
                 DienGiai: {
                    required: true
                }
            },
            invalidHandler: function (f, d) {
                var g = d.numberOfInvalids();

                if (g) {
                    var e = g == 0 ? "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng trường đang được đánh dấu" : "<button class='close' type='button' data-dismiss='alert'>×</button>Nhập thiếu hoặc sai định dạng " + g + " trường đang được đánh dấu";
                    jQuery("#thongbaoloi_form_thanhtoanphicanhan").html(e).show()

                } else {
                    jQuery("#thongbaoloi_form_thanhtoanphicanhan").hide()
                }
            }
        });

    $.datepicker.setDefaults($.datepicker.regional['vi']);

</script>

<script type="text/javascript">

    function luu() {
        jQuery("#form_thanhtoanphicanhan").submit();
    };

    function xoa(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=xoa';
    };

    function duyet(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=duyet';
    };

    function tuchoi(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=tuchoi';
    };

    function thoaiduyet(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=thoaiduyet';
    };

    function guiemail(id) {
        window.location = 'admin.aspx?page=thanhtoanphicanhan&id=' + id + '&mode=view&tinhtrang=thoaiduyet';
    };

</script>

<script type="text/javascript">
    function confirm_delete_thanhtoanphicanhan(id)
    { };

</script>

<script type="text/javascript">
    jQuery(function ($) {
        $('.auto').autoNumeric('init');
        // $('#txtTyLe>').autoNumeric('init');
    });
      
    <%LoadThongTin(); %>
</script>
<script type="text/javascript">
    <%AnNut(); %>  
</script>

<script type="text/javascript">
    function themthanhtoanphicanhanchitiet() {
        var timestamp = Number(new Date());
        $("#div_thanhtoanphicanhan_chitiet").empty();
        $("#div_thanhtoanphicanhan_chitiet").append($("<iframe width='100%' height='100%' id='iframe_thanhtoanphicanhan_chitiet' name='iframe_thanhtoanphicanhan_chitiet' scrolling='auto' frameborder='0' />").attr("src", "iframe.aspx?page=thanhtoanphicanhan_chitiet&mode=iframe&time=" + timestamp));
        $("#div_thanhtoanphicanhan_chitiet").dialog({
            autoOpen: false,
            title: "<img src='images/icons/color/application_form_magnify.png'> <b>Thanh toán phí chi tiết</b>",
            modal: true,
            width: "960",
            height: "640",
            buttons: [
                    {
                        text: "Lưu",
                        click: function () {
                            window.frames['iframe_thanhtoanphicanhan_chitiet'].submitform();
                            $(this).dialog("close");
                            fncsave();
                        }
                    },
                    {
                        text: "Đóng",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                    ]
        }).dialog("open");
            }

            function fncsave() {
                document.getElementById('<%= Test.ClientID %>').click();
            }


            // Array ID được check
            var thanhtoanphicanhan_grv_selected = new Array();

            jQuery(document).ready(function () {
                // dynamic table      

                $("#thanhtoanphicanhan_grv .checkall").bind("click", function () {
                    thanhtoanphicanhan_grv_selected = new Array();
                    checked = $(this).prop("checked");
                    $('#thanhtoanphicanhan_grv :checkbox').each(function () {
                        $(this).prop("checked", checked);
                        if ((checked) && ($(this).val() != "all")) thanhtoanphicanhan_grv_selected.push($(this).val());
                    });
                });

                $('#thanhtoanphicanhan_grv :checkbox').each(function () {
                    $(this).bind("click", function () {
                        removeItem(thanhtoanphicanhan_grv_selected, $(this).val())
                        checked = $(this).prop("checked");
                        if ((checked) && ($(this).val() != "all")) thanhtoanphicanhan_grv_selected.push($(this).val());
                    });
                });
            });

</script>