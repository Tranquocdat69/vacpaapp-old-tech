﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VACPA.Entities;
using VACPA.Data.SqlClient;
using HiPT.VACPA.DL;


public partial class usercontrols_dmtinh_add : System.Web.UI.UserControl
{

    public Commons cm = new Commons();
    public clsXacDinhQuyen kiemtraquyen = new clsXacDinhQuyen();

    // Tên chức năng
    public string tenchucnang = "Tỉnh/Thành phố";

    public string bangdm = "tblDMTinh";
    public string quyen = "DmTinh";
    public string truongid = "TinhID";
    public string truongten = "TenTinh";
    public string truongma = "MaTinh";
    public string truonghieuluc = "HieuLuc";
    public string strHieuLuc = "1";
    protected void Page_Load(object sender, EventArgs e)
    {

        Label1.Text = tenchucnang + ":";
        Label3.Text = "Mã Tỉnh/Thành phố";
        Label5.Text = "Hết hiệu lực";
        //string strHieuLuc = "0";
        //if (!HieuLuc.Checked)
        //    strHieuLuc = "1";
        //else
        //    strHieuLuc = "0";

        try
        {

            if (!string.IsNullOrEmpty(Request.Form["Ten"]) && !string.IsNullOrEmpty(Request.Form["Ma"]))
            {
                if (Request.Form["HieuLuc"] != null)
                {
                    strHieuLuc = Request.Form["HieuLuc"];
                }
                SqlCommand sql = new SqlCommand();

                sql.CommandText = "SELECT COUNT(*) FROM " + bangdm + " WHERE " + truongten + " = @Ten or " + truongma + " = @Ma";
                sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                DataSet ds = new DataSet();
                ds = DataAccess.RunCMDGetDataSet(sql);
                if (Convert.ToInt16(ds.Tables[0].Rows[0][0]) > 0)
                {
                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>Đã có bản ghi với giá trị \"" + Request.Form["Ten"].Trim() + " hoặc " + Request.Form["Ma"].Trim() + "\"</div>"));
                }
                else
                {
                    sql = new SqlCommand();
                    sql.CommandText = "INSERT INTO " + bangdm + " (" + truongten + "," + truongma + "," + truonghieuluc + ") VALUES (@Ten, @Ma, "+ strHieuLuc +")";
                    sql.Parameters.AddWithValue("@Ten", Request.Form["Ten"]);
                    sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                    //sql.Parameters.AddWithValue("@Ma", Request.Form["Ma"]);
                    DataAccess.RunActionCmd(sql);
                    sql.Connection.Close();
                    sql.Connection.Dispose();
                    sql = null;

                    cm.ghilog(quyen, "Thêm giá trị \"" + Request.Form["Ten"] + "\"" + Request.Form["Ma"] + "\" vào danh mục " + tenchucnang);

                    ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-success' style=''><button class='close' type='button' >×</button>Cập nhật thông tin thành công!</div>"));
                }
            }



        }
        catch (Exception ex)
        {
            ErrorMessage.Controls.Add(new LiteralControl("<div class='alert alert-error' style=''><button class='close' type='button' >×</button>" + ex.Message.ToString() + "</div>"));
        }
    }
}